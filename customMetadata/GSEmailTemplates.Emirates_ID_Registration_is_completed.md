<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Emirates ID Registration is completed</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

We are pleased to inform you that the Emirates ID Registration of applicant {!Step__c.Applicant_Name__c} – SR # {!Step__c.SR__c} has been successfully completed.&lt;br/&gt;&lt;br/&gt;

&lt;html&gt;
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt;
&lt;tr&gt;
&lt;th&gt;SR Number&lt;/th&gt;
&lt;th&gt;Name of Company&lt;/th&gt;
&lt;th&gt;Name of Applicant&lt;/th&gt;
&lt;th&gt;EID Registration Number&lt;/th&gt;
&lt;th&gt;Biometrics&lt;/th&gt;

&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt;
&lt;td&gt;{!step.SR__r.Customer__r.name}&lt;/td&gt;
&lt;td&gt;{!Step__c.Applicant_Name__c}&lt;/td&gt;
&lt;td&gt;{!Step__c.SAP_IDNUM__c}&lt;/td&gt;
&lt;td&gt;{!Step__c.Biometrics_Required__c}&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/html&gt;
&lt;br/&gt;

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;

Please download the Emirates ID Registration. (Click ‘Download/Upload Document’ to view and print.)&lt;br/&gt;&lt;br/&gt;

Please then enroll the applicant for health insurance, using a service provider approved by Dubai Health Authority. (You can find a list of DHA-approved health insurance providers &lt;a target=&quot;_blank&quot; href=&quot;http://www.isahd.ae/Home/PermittedInsuerers&quot;&gt;here&lt;/a&gt;.) Once the insurance is arranged, please upload the insurance certificate to the client portal using the ‘Download/Upload Documents’ section.&lt;br/&gt;&lt;br/&gt;

The DIFC has a Group Health Insurance Scheme ( &lt;a href=&quot;https://difc-healthscheme.com/&quot; target=&quot;_blank&quot;&gt; https://difc-healthscheme.com&lt;/a&gt; ) in place to assist companies in complying with the mandatory health cover for employees including dependents. If you wish to participate in the scheme, please contact our consultant EC3 MEA at 04-565 1609 or write to them on enquiry@difc-heathscheme.com for more information on the products.&lt;br/&gt;&lt;br/&gt;


If EID biometrics is required, (this will be shown in the last column of the table above.) The applicant should then:&lt;br/&gt;&lt;br/&gt;

- Proceed to Al Karama EID enrolment center (Emirates Post Office Building) to complete the biometrics. (Please find the location map and contact details   &lt;a href=&quot;https://www.google.com/maps/place/Emirates+Post+-+Dubai+Central+Post+Office/@25.2446932,55.3070478,17z/data=!3m1!4b1!4m5!3m4!1s0x3e5f42d5cd070fdb:0x291957c5950d16ce!8m2!3d25.2446884!4d55.3092365&quot; target=&quot;_blank&quot;&gt; here. &lt;/a&gt;).Kindly note that as part of Covid 19 precautions, EID Enrollment Centre might ask for the medical fitness test result for applicants above 18 years of age. Medical test result can be downloaded from the client portal in 24 Hours (4 Hrs for Express applications )after medical fitness test is completed at DIFC medical fitness center.&lt;br/&gt;
- Submit the EID Registration form with the stamp ‘Process completed’ along with original passport upon request from DIFC Government Services Office..&lt;br/&gt;&lt;br/&gt;


&lt;b&gt;If you need further help&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, 
This is to inform you that Emirates ID registration for  {!Step__c.Applicant_Name__c} has been completed.An Email has been sent with further details.&lt;br/&gt;
Regards, 
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">DIFC_Sponsorship_Visa_New;Employment_Visa_Govt_to_DIFC;Non_DIFC_Sponsorship_Visa_New;Employment_Visa_from_DIFC_to_DIFC</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Emirates ID Registration is Completed</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CLOSED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Emirates ID Registration is completed. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
