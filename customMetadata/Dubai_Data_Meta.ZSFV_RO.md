<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ZSFV_RO</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">SELECT BUKRS__c, INTRENO__c, SALTNR__c, SGENR__c, SMENR__c, SNUNR__c, SSTOCKW__c, SWENR__c, VALIDFROM__c, VALIDTO__c FROM ZSFV_RO__C</value>
    </values>
</CustomMetadata>
