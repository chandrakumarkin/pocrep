<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ComplianceCalendar-LTD IC</label>
    <protected>false</protected>
    <values>
        <field>Condition__c</field>
        <value xsi:type="xsd:string">Legal_Type_of_Entity__c = LTD IC</value>
    </values>
    <values>
        <field>Share_with_Link__c</field>
        <value xsi:type="xsd:string">https://difc.my.salesforce.com/sfc/p/20000000nkZ5/a/0J000000kmg7/JR4Q5MQRNg5ZQfKBi5Hph0.YlGnWLY6DKuFGDDv41qg</value>
    </values>
</CustomMetadata>
