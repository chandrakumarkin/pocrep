<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TIV0A</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">SELECT SNUNR__c, SPRAS__c, Unique_Id__c, XMBEZ__c FROM TIV0A__c</value>
    </values>
</CustomMetadata>
