<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>getCardsByWildcardSearch</label>
    <protected>false</protected>
    <values>
        <field>End_point_URL__c</field>
        <value xsi:type="xsd:string">http://ktcsupport.dyndns.org:8800/abacuswebservice/serviceoperation.asmx?op=getCardsByWildcardSearch</value>
    </values>
    <values>
        <field>Password__c</field>
        <value xsi:type="xsd:string">MTIzNDU2</value>
    </values>
    <values>
        <field>Service_Namespace__c</field>
        <value xsi:type="xsd:string">http://www.designa.de/</value>
    </values>
    <values>
        <field>Soap_Namespace__c</field>
        <value xsi:type="xsd:string">http://schemas.xmlsoap.org/soap/envelope/</value>
    </values>
    <values>
        <field>Username__c</field>
        <value xsi:type="xsd:string">d2Vic2VydmljZQ==</value>
    </values>
    <values>
        <field>XSD__c</field>
        <value xsi:type="xsd:string">http://www.w3.org/2001/XMLSchema</value>
    </values>
    <values>
        <field>XSI__c</field>
        <value xsi:type="xsd:string">http://www.w3.org/2001/XMLSchema-instance</value>
    </values>
</CustomMetadata>
