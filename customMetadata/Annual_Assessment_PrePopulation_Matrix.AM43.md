<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AM43</label>
    <protected>false</protected>
    <values>
        <field>Api_Field__c</field>
        <value xsi:type="xsd:string">How_long_will_you_keep_such_data__c</value>
    </values>
    <values>
        <field>Api_Value__c</field>
        <value xsi:type="xsd:string">We have no set policy but will develop one</value>
    </values>
    <values>
        <field>Effect_on_risk__c</field>
        <value xsi:type="xsd:string">Enforcing data minimization and retention priniples through such policies reduces the organization&apos;s risk of inadeverently processing PD that is no longer required to be retained either in practice or by law.</value>
    </values>
    <values>
        <field>Likelihood_of_harm__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>Mitigation_measures_to_reduce_or_elimina__c</field>
        <value xsi:type="xsd:string">We will create, adhere to and train our staff on a detailed data retention policy. It will be designed to require archiving PD that is no longer in use, up to date, or unnecessary to fulfill our specific processing purposes. We have incorporated any requirements around putting PD beyond further use as set out in Article 22 of the DP Law 2020.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">8.3</value>
    </values>
    <values>
        <field>Overall_risk__c</field>
        <value xsi:type="xsd:string">Very Limited Assurance</value>
    </values>
    <values>
        <field>Replace_Response__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Residual_risk_help_text__c</field>
        <value xsi:type="xsd:string">Minimal; regular checks on compliance and implementation of any obligations are conducted</value>
    </values>
    <values>
        <field>Risk_Assessment__c</field>
        <value xsi:type="xsd:string">Because our company has no set policy or guidance for data retention periods, we are at risk of actively processing too much PD, PD that is inaccurate or not up to date, or PD that is being used for a purpose for which it was not collected, in direct breach of the data protection principles set out in Article 9 of the DP Law 2020</value>
    </values>
    <values>
        <field>Risk_Description__c</field>
        <value xsi:type="xsd:string">In terms of data retention periods, we have no set policy or guidance for when to archive or delete it.</value>
    </values>
    <values>
        <field>Risk_Type__c</field>
        <value xsi:type="xsd:string">Identify and assess risks</value>
    </values>
    <values>
        <field>Severity_of_harm__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">How long will you keep such data?</value>
    </values>
</CustomMetadata>
