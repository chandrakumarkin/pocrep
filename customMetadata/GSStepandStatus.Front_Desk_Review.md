<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Front Desk Review</label>
    <protected>false</protected>
    <values>
        <field>Status__c</field>
        <value xsi:type="xsd:string">AWAITING_ORIGINALS;VERIFIED;PENDING_REVIEW;REUPLOAD_DOCUMENT;REJECTED;APPROVED;POSTED;REQUIRE_ADDL_INFO</value>
    </values>
</CustomMetadata>
