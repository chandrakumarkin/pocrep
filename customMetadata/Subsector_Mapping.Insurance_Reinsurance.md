<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Insurance &amp; Reinsurance</label>
    <protected>false</protected>
    <values>
        <field>Subsector_Values__c</field>
        <value xsi:type="xsd:string">Insurer/Reinsurer;Takaful / Re-Takaful;Managing General Agent;Coverholder;Insurance/Reinsurance Broker;Captive Manager;Captive;TPA – Insurance</value>
    </values>
</CustomMetadata>
