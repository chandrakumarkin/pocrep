<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ZSFV_BU</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">SELECT BUKRS__c, DBAUGENE__c, GEMEINDE__c, SGENR__c, SWENR__c, Unique_Id__c, VALIDFROM__c, VALIDTO__c, XBAUGENE__c, XGETXT__c FROM ZSFV_BU__C</value>
    </values>
</CustomMetadata>
