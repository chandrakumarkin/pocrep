<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>unitsOpenedForTrading</label>
    <protected>false</protected>
    <values>
        <field>Display_Order__c</field>
        <value xsi:type="xsd:double">8.0</value>
    </values>
    <values>
        <field>Filter_Applicable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order_by__c</field>
        <value xsi:type="xsd:string">8</value>
    </values>
    <values>
        <field>Related_Functionality__c</field>
        <value xsi:type="xsd:string">Management Report</value>
    </values>
    <values>
        <field>SOQL__c</field>
        <value xsi:type="xsd:string">Select id,createdDate ,step_name__c,name,sr__r.name,sr__c,sr__r.Customer__r.Name,sr__r.Customer__c,sr__r.Contractor__r.Name from step__c where sr__r.RecordType.DeveloperName=&apos;Fit_Out_Service_Request&apos; and (Step_Name__c=&apos;Issue Completion Certificate&apos; OR Step_Name__c=&apos;Issue Fit To Occupy Certificate&apos;) and createdDate &gt;=LAST_N_DAYS : 30 and sr__r.Property_Type__c !=&apos;Office&apos; and (Type_of_request__c =&apos;Type A&apos; OR  Type_of_request__c =&apos;Type D With Modification&apos; OR Type_of_request__c =&apos;Type D No Modification&apos;)</value>
    </values>
    <values>
        <field>TestClass_SOQL__c</field>
        <value xsi:type="xsd:string">Select id,step_name__c,name,sr__r.name,sr__c,sr__r.Customer__r.Name,sr__r.Customer__c,sr__r.Contractor__r.Name from step__c limit 1</value>
    </values>
    <values>
        <field>is_Used__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
