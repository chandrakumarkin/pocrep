<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Population_in_DIFC_Freezone_by_JobTitle</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">Select Count__c,JobTitle__c from Population_in_DIFC_Freezone_by_JobTitle__c</value>
    </values>
</CustomMetadata>
