<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Portal User Declaration</label>
    <protected>false</protected>
    <values>
        <field>Application_Type__c</field>
        <value xsi:type="xsd:string">Self-Registration</value>
    </values>
    <values>
        <field>Declaration_Text__c</field>
        <value xsi:type="xsd:string">&lt;p&gt;
By providing this information, I understand that I am applying to be a DIFC Portal user and will communicate with the DIFC through the portal. 

I confirm that I have: 
1. been appointed as a [adminstrator/secondary] portal user;&lt;span&gt;&lt;a href=&quot;#&quot;&gt;see more&lt;/a&gt;&lt;/span&gt;
2. been authorised to access [all/certain] information related to the entity [including / limited to] [company services, employee services and other DIFC services]; and
3. the power to appoint other portal users and designate their level of access, 
by the abovementioned entity.

I hereby consent on behalf of the abovementioned entity, to submit all forms related to the Registrar of Companies and Commissioner of Data Protection (“the Statutory Bodies”) electronically on the DIFC Client Portal.

I  hereby confirm the following on behalf of the abovementioned entity:
1. any forms lodged on the DIFC Client Portal can be used as if the original form was submitted; and 
2. the original form(s) submitted on the DIFC Client Portal shall be retained by the abovementioned entity as evidence of its authenticity, and that if a question of authenticity arises, the relevant Statutory Body may require submission of the original form(s).

I declare that the information given in this form is complete, correct and accurate.

I understand that it is an offence to provide information which is false, misleading or deceptive or to conceal information where the concealment of such information is likely to mislead or deceive.

DIFC collects and processes data in accordance with its Online Data Protection Policy. Please read this policy for more information on how we manage your data. DIFC Data Protection Law (DIFC Law No. 1 of 2007) governs the collection, handling and use of personal data in DIFC. For more information on DIFC, data collection usage and management, please see our Terms of Use and the DIFC Online Data Protection Policy
&lt;/p&gt;</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">i_agree__c</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">HexaBPM__Service_Request__c</value>
    </values>
    <values>
        <field>Record_Type_API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
