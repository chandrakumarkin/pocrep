<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>completed-Business-Visit-Visa</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt; &lt;br/&gt;

We are pleased to inform you that the following visit request has been successfully completed:&lt;br/&gt; &lt;br/&gt;

&lt;html&gt; 
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt; 
&lt;tr&gt; 
&lt;th&gt;Name Service&lt;/th&gt; 
&lt;th&gt;Applicant’s Name&lt;/th&gt; 
&lt;th&gt;Service Request No&lt;/th&gt; 



&lt;/tr&gt; 
&lt;tr&gt; 
&lt;td&gt;{!Step__c.Service_Type__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.Applicant_Name__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt; 

&lt;/tr&gt; 
&lt;/table&gt; 
&lt;/html&gt; 
&lt;br/&gt; &lt;br/&gt; 

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;

Please download the visa from the client portal. (Click ‘Download/Upload Documents’ to view and print the visa.) &lt;br/&gt; &lt;br/&gt;

Please then send a copy of the visa to the applicant before s/he enters the UAE.&lt;br/&gt; &lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, 

This is to inform you that the Visit Visa for {!Step__c.Applicant_Name__c} has been issued. Kindly arrange collection from DIFC GSO. &lt;br/&gt; 


Regards, &lt;br/&gt; 
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">Business_Visit_Visa</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Completed</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CLOSED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Application process completed. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
