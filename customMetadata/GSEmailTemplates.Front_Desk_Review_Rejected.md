<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Front Desk Review- Rejected</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt;&lt;br/&gt;

We regret to inform you that the following request has been rejected and service fee is credited back to the company portal account.&lt;br/&gt;&lt;br/&gt;

&lt;html&gt; 
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt; 
&lt;tr&gt; 
&lt;th&gt;SR Number&lt;/th&gt; 
&lt;th&gt;Company Name&lt;/th&gt; 
&lt;th&gt;Service Name&lt;/th&gt; 
&lt;th&gt;Reason for Rejection&lt;/th&gt; 


&lt;/tr&gt; 
&lt;tr&gt; 
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt; 
&lt;td&gt;{!step.SR__r.Customer__r.name}&lt;/td&gt; 
&lt;td&gt;{!Step__c.Service_Type__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.Rejection_Reason__c}&lt;/td&gt; 
&lt;/tr&gt; 
&lt;/table&gt; 
&lt;/html&gt; 
&lt;br/&gt; &lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt;


We regret to inform you that the request: {!Step__c.SR_Menu_Text__c} with request number {!Step__c.SR__c} has been rejected. Please check DIFC client portal for further details. &lt;br/&gt;



Regards,&lt;br/&gt; 
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:type="xsd:string">Renewal</value>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">GS_ALL</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Front Desk Review</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">REJECTED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Service Request has been rejected. SR#  {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
