<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>FitOutInduction</label>
    <protected>false</protected>
    <values>
        <field>Display_Order__c</field>
        <value xsi:type="xsd:double">6.0</value>
    </values>
    <values>
        <field>Filter_Applicable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order_by__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Related_Functionality__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SOQL__c</field>
        <value xsi:type="xsd:string">Select Target_Completion_Date__c,Customer__r.Active_License__r.License_Issue_Date__c,Customer__r.Exclude_From_Fitout_Management_Report__c,id,Progress__c,recordType.DeveloperName,service_type__c,Location__c,Customer__r.Name,Linked_SR__c,name,External_Status_Name__c,Submitted_Date__c,Type_of_Request__c,Site_Progress__c,Upgraded_Date__c,Property_Type__c,Delayed_Days__c  from service_Request__c where Expiry_Date__c &gt;= LAST_N_DAYS : 45 AND recordType.DeveloperName=&apos;Fit_Out_Induction_Request&apos; and service_type__c =&apos;Fit-Out Induction Request&apos; AND External_status_Name__c =&apos;Induction Approved&apos; AND filters</value>
    </values>
    <values>
        <field>TestClass_SOQL__c</field>
        <value xsi:type="xsd:string">Select Customer__r.Active_License__r.License_Issue_Date__c,Customer__r.Exclude_From_Fitout_Management_Report__c,id,Progress__c,recordType.DeveloperName,service_type__c,Location__c,Customer__r.Name,Linked_SR__c,name,External_Status_Name__c,Submitted_Date__c,Type_of_Request__c,Site_Progress__c,Upgraded_Date__c,Property_Type__c,Delayed_Days__c  from service_Request__c limit 2</value>
    </values>
    <values>
        <field>is_Used__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
