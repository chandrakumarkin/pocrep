<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>TotalArea_by_unitUsageType</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">Select Total_Area__c,Unit_Usage_Type__c from TotalArea_by_unitUsageType__c</value>
    </values>
</CustomMetadata>
