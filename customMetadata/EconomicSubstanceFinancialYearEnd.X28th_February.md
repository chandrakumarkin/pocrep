<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>28th February</label>
    <protected>false</protected>
    <values>
        <field>AllwithBranch2018EndDateTwo__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>AllwithBranch2018EndDate__c</field>
        <value xsi:type="xsd:date">2020-02-28</value>
    </values>
    <values>
        <field>AllwithBranch2018StartDateTwo__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>AllwithBranch2018StartDate__c</field>
        <value xsi:type="xsd:date">2019-03-01</value>
    </values>
    <values>
        <field>EntitiesFromJul2019EndDate__c</field>
        <value xsi:type="xsd:date">2021-02-28</value>
    </values>
    <values>
        <field>EntitiesFromJul2019StartDateTwo__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>EntitiesFromJul2019StartDate__c</field>
        <value xsi:type="xsd:date">2020-02-28</value>
    </values>
    <values>
        <field>EntitiesJantoJun2019EndDate_1__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>EntitiesJantoJun2019EndDate__c</field>
        <value xsi:type="xsd:date">2020-02-28</value>
    </values>
    <values>
        <field>EntitiesJantoJun2019StartDateTwo__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>EntitiesJantoJun2019StartDate__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Financial_Year_End__c</field>
        <value xsi:type="xsd:string">28th February</value>
    </values>
    <values>
        <field>is30Jun2019__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>isJul2019__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
