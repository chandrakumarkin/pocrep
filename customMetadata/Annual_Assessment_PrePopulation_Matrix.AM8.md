<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AM8</label>
    <protected>false</protected>
    <values>
        <field>Api_Field__c</field>
        <value xsi:type="xsd:string">Does_the_collected_data_include_criminal__c</value>
    </values>
    <values>
        <field>Api_Value__c</field>
        <value xsi:type="xsd:string">Yes</value>
    </values>
    <values>
        <field>Effect_on_risk__c</field>
        <value xsi:type="xsd:string">Risk is reduced by reducing ambiguity about obligations around processing criminal history / convictions PD through training, enhanced security and retention policies, and access limitations.</value>
    </values>
    <values>
        <field>Likelihood_of_harm__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>Mitigation_measures_to_reduce_or_elimina__c</field>
        <value xsi:type="xsd:string">When we collect criminal history / convictions PD for processing, we can reduce the risk of loss, breach or negative impact on indivudal rights by complying with Article 11 of the DP Law 2020, and only collecting and allowing limited organizational access to the criminal history / convictions PD necessary to support the specific purpose for which it is obtained, as well as keep it accurate and up to date, and retain as little of it as possible to conduct our processing activities. Where this type of data supports HRP, we will take the actions set out above as well, including DPO appointement and regular DPIA / AA.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Overall_risk__c</field>
        <value xsi:type="xsd:string">Very Limited Assurance</value>
    </values>
    <values>
        <field>Replace_Response__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Residual_risk_help_text__c</field>
        <value xsi:type="xsd:string">Some risk will remain where criminal history / convictions PD must be processed. We must proceed with caution and provide our staff with training and enhanced seucrity policy instructions, and providing the DPO with all necessary tools including resources and training to ensure proper accountability practices.</value>
    </values>
    <values>
        <field>Risk_Assessment__c</field>
        <value xsi:type="xsd:string">As a result of the regular processing of criminal history data, there may be a negative impact on the indivivduals to whom the PD belongs, and thereby causing harm to them if the processing results in a breach, unlawful disclosure or other data loss, and / or is not conducted in compliance with the DP Law 2020 or other applicable laws.</value>
    </values>
    <values>
        <field>Risk_Description__c</field>
        <value xsi:type="xsd:string">We process personal data that contains information about criminal offences committed by an individual on a regular basis in order to conduct our business</value>
    </values>
    <values>
        <field>Risk_Type__c</field>
        <value xsi:type="xsd:string">Identify and assess risks</value>
    </values>
    <values>
        <field>Severity_of_harm__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">Does the collected data include criminal offence data?</value>
    </values>
</CustomMetadata>
