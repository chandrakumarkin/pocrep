<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>data_protection</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select Permit_ID_18_Report__c,Account_ID_18__c,Permit_Type__c,Transferring_Personal_Data_Outside_DIFC__c,Nature_of_Sensitive_Personal_Data__c,Purpose_of_Processing_Data__c,Data_Subjects_Personal_Data__c,Class_of_Personal_Data__c from Permit__c where Account__r.Registration_License_No__c!=&apos;&apos; limit 6000</value>
    </values>
</CustomMetadata>
