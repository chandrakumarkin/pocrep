<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Banking</label>
    <protected>false</protected>
    <values>
        <field>Subsector_Values__c</field>
        <value xsi:type="xsd:string">Corporate Banking;Islamic Banking;Private Banking;Trade Finance;Money/Payment Services;Digital Banking</value>
    </values>
</CustomMetadata>
