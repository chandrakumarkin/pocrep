<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Updates from DIFC Services</label>
    <protected>false</protected>
    <values>
        <field>Notification_Details__c</field>
        <value xsi:type="xsd:string">&lt;b&gt; UPDATES FROM DIFC SERVICES - 7th July 2019 &lt;/b&gt;
&lt;br/&gt;&lt;br/&gt;
•	All GCC Employee cards should be renewed before 30 days from the date of expiry and should be cancelled as soon as the employment contract is terminated
&lt;br/&gt;&lt;br/&gt;
•	For updates on courier collection or deliveries, companies can contact ‘DIFC Services’ courier agents on 04 362 2491 or courierservices@difc.ae
&lt;br/&gt;&lt;br/&gt;
•	Employees with a monthly salary of above AED 4,000 are now entitled to apply for Dependent Visas, regardless their designation or gender
&lt;br/&gt;&lt;br/&gt;
•	As per an announcement from the UAE Federal Authority for Identity and Citizenship (ICA) an applicant’s National ID, issued by their home country authority, is required for applicants from Pakistan, Afghanistan, Iraq and Iran along with new Employment and Dependent Visa applications
&lt;br/&gt;&lt;br/&gt;
•	Temporary Work Permits issued for employees under individual sponsorship are not renewable. If an employee would like to continue their temporary employment, he/she must apply for a new Temporary Work Permit once the existing card is expired. Please note that Temporary Work Permit applications might be rejected by UAE Immigration if a valid Work Permit is found in the system.
&lt;br/&gt;&lt;br/&gt;
•	In the event of transferring an Employment Visa from the Dubai Government or Free Zone entity to DIFC, the Sponsorship Transfer form must be signed and stamped by the current sponsor before signing by the DIFC Authority. Portal Users will receive a notification from DIFC Services once the Transfer Form is ready to be downloaded from the Client Portal. The Transfer Form should then be re-submitted to the DIFC with the signature and stamp of the current sponsor as a hard copy, along with the applicant’s original passport.
&lt;br/&gt;&lt;br/&gt;
•	Applicants that need to submit biometric for their Emirates ID registration, are advised to visit the EID Enrollment Centre in Al Karama (Emirates Post Office Building) instead of Umm Hurair Centre.</value>
    </values>
    <values>
        <field>is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
