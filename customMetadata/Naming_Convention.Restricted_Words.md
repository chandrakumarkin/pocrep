<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Restricted Words</label>
    <protected>false</protected>
    <values>
        <field>Is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">Warning: The selected Name is a restricted word or phrase for companies in DIFC. The name might be rejected but you can submit the entity name for review.</value>
    </values>
    <values>
        <field>Restricted_Words__c</field>
        <value xsi:type="xsd:string">DFSA,Dubai Financial Services Authority, DIFC,Dubai International Financial Center, DIFC Courts, Jumeirah, Zabeel, Burj AlArab, Burj Khalifa, Gate Avenue, Dubai Mall, Emirates Palace, The Gate, Gate, FZC, Emirate/s, GCC, Government and Zayed, Roads and Transport Authority (RTA), Dubai Electricity and Water Authority (DEWA), Dubai Courts, Emirates (Airlines), Etihad Airways, FlyDubai, DP World, Mubadala, Masdar, Dubai Land, Dubai World, Abu Dhabi National Oil Company,
Dubai Financial, Dubai, Financial Services Authority, Courts, AlArab, Al Arab, Khalifa, Burj, Gate, Government, Zayed, Roads and Transport, Transport Authority, Emirates, RTA, DEWA, Emirates Airlines, Dubai Electricity, Electricity and Water, Electricity and Water Authority, Etihad, Airways, DP, Abu Dhabi National, National Oil, National Oil Company, Oil Company</value>
    </values>
    <values>
        <field>Rule_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Verify_Complete_Word__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Warning_Type__c</field>
        <value xsi:type="xsd:string">Warning Message</value>
    </values>
</CustomMetadata>
