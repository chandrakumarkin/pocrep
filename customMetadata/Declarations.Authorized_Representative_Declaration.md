<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Authorized Representative Declaration</label>
    <protected>false</protected>
    <values>
        <field>Application_Type__c</field>
        <value xsi:type="xsd:string">Standard Declaration for Forms</value>
    </values>
    <values>
        <field>Declaration_Text__c</field>
        <value xsi:type="xsd:string">I confirm that the person named has consented to act as an Authorised Representative and shall have a valid U.A.E. visa within three (3) months from the date of incorporation.</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">I_agree_Authorized_Represntative__c</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">HexaBPM_Amendment__c</value>
    </values>
    <values>
        <field>Record_Type_API_Name__c</field>
        <value xsi:type="xsd:string">Individual</value>
    </values>
</CustomMetadata>
