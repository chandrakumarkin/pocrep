<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Lease_Partners</label>
    <protected>false</protected>
    <values>
        <field>Display_Order__c</field>
        <value xsi:type="xsd:double">4.0</value>
    </values>
    <values>
        <field>Filter_Applicable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order_by__c</field>
        <value xsi:type="xsd:string">ORDER BY  Lease__r.RF_Valid_From__c DESC</value>
    </values>
    <values>
        <field>Related_Functionality__c</field>
        <value xsi:type="xsd:string">Management Report</value>
    </values>
    <values>
        <field>SOQL__c</field>
        <value xsi:type="xsd:string">Select id,name,Account__c,Is_License_to_Occupy__c,Lease__c,Lease__r.RF_Valid_From__c,Lease__r.RF_Valid_To__c,Lease__r.Start_date__c ,Unit__c,Unit__r.Unit_Usage_Type__c,Unit__r.Unit_Type_DDP__c, Unit_On_Plan__c from Lease_Partner__c where Lease__r.Start_date__c &lt; LAST_N_DAYS:365 and Unit__r.Third_Party__c =false and Unit__r.Building__r.Company_Code__c !=&apos;5500&apos; and  ((Unit__r.Unit_Type_DDP__c=&apos;retail&apos; and Lease__r.RF_Valid_To__c != Null) OR Unit__r.Unit_Type_DDP__c=&apos;Commercial&apos;) ORDER BY  Unit__r.Unit_Type_DDP__c desc</value>
    </values>
    <values>
        <field>TestClass_SOQL__c</field>
        <value xsi:type="xsd:string">Select id,name,Account__c,Is_License_to_Occupy__c,Lease__c,Lease__r.RF_Valid_From__c,Lease__r.RF_Valid_To__c,Unit__c,Unit__r.Unit_Usage_Type__c,Unit__r.Unit_Type_DDP__c, Unit_On_Plan__c from Lease_Partner__c limit 10</value>
    </values>
    <values>
        <field>is_Used__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
