<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Enactment of the new DIFC Employment Law</label>
    <protected>false</protected>
    <values>
        <field>Notification_Details__c</field>
        <value xsi:type="xsd:string">&lt;b&gt;Enactment of the new DIFC Employment Law&lt;/b&gt;
&lt;br/&gt;&lt;br/&gt;
Following the enactment of the new DIFC Employment Law, kindly note that, as of 28 August 2019, DIFC Employment Permits will be defined as the per the table below.

&lt;table style=&quot;width:100%&quot;&gt;
  &lt;tr&gt;
    &lt;th align=&quot;left&quot;&gt;SR No.&lt;/th&gt;
    &lt;th align=&quot;left&quot;&gt;Type of Employment&lt;/th&gt; 
    &lt;th align=&quot;left&quot;&gt;Definition&lt;/th&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
    &lt;td&gt;1&lt;/td&gt;
    &lt;td&gt;Permanent Sponsored Employee&lt;/td&gt;
    &lt;td&gt;Non-GCC individuals recruited on a permanent basis. Employees can be recruited from outside or inside the UAE. The permanent employment visa is valid for 3 years and can be renewed upon expiry.&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
    &lt;td&gt;2&lt;/td&gt;
    &lt;td&gt;Seconded Sponsored employee&lt;/td&gt;
    &lt;td&gt;Non-GCC individuals recruited on a secondment agreement from outside the UAE. The seconded employment visa is valid for one year and is not renewable.&lt;/td&gt;
  &lt;/tr&gt;
  &lt;tr&gt;
    &lt;td&gt;3&lt;/td&gt;
    &lt;td&gt;UAE/GCC Employee&lt;/td&gt;
    &lt;td&gt;UAE and other GCC National employees recruited on a permanent basis. The GCC employment permit is valid for 3 years or until the applicant’s passport has expired.  The permit can be renewed upon expiry.&lt;/td&gt;
  &lt;/tr&gt;
   &lt;tr&gt;
    &lt;td&gt;4&lt;/td&gt;
    &lt;td&gt;Seconded Employee Card&lt;/td&gt;
    &lt;td&gt;Individuals with a valid UAE employment visa/work permit, who would like to join a DIFC-registered company based on a seconded employment contract. The seconded employee card can be applicable for 3 months, 6 months and 12 months and the renewal of a seconded employee card is subject to approval from the DIFC Authority.&lt;/td&gt;
  &lt;/tr&gt;
   &lt;tr&gt;
    &lt;td&gt;5&lt;/td&gt;
    &lt;td&gt;Contractor Employee Card&lt;/td&gt;
    &lt;td&gt;Individuals with a valid UAE employment visa/work permit, who would like to join a DIFC-registered company based on a service level agreement between the DIFC entity and the sponsoring entity. The contractor employee card can be applicable for 3 months, 6 months and 12 months and can be renewed upon expiry.&lt;/td&gt;
  &lt;/tr&gt;
   &lt;tr&gt;
    &lt;td&gt;6&lt;/td&gt;
    &lt;td&gt;Frequent Visitor Card&lt;/td&gt;
    &lt;td&gt;Individuals that hold a visa from an external company that is related to a DIFC-registered entity, who may need to visit the Centre frequently for administration purposes. The frequent visitor card can be applicable for 3 months, 6 months and 12 months and may be renewed before expiry.&lt;/td&gt;
  &lt;/tr&gt;
   &lt;tr&gt;
    &lt;td&gt;7&lt;/td&gt;
    &lt;td&gt;Non-Sponsored Employee Card (previously Temporary Work Permit)&lt;/td&gt;
    &lt;td&gt;Individuals with a valid UAE residence visa issued in Dubai under ‘individual’ sponsorship. The non-sponsored employee card can be applicable for 1 month, 3 months, 6 months and 12 months but is not renewable&lt;/td&gt;
  &lt;/tr&gt;
   &lt;tr&gt;
    &lt;td&gt;8&lt;/td&gt;
    &lt;td&gt;Internship Card&lt;/td&gt;
    &lt;td&gt;University students who are undertaking an internship at a DIFC-registered company, with a supporting letter from the University. The internship card can be applicable for 1 month or 3 months, but is not renewable.&lt;/td&gt;
  &lt;/tr&gt;
&lt;/table&gt;

&lt;br/&gt;&lt;br/&gt;

For any enquiries or in case of any discrepancies on the DIFC Client Portal, please e-mail gs.helpdesk@difc.ae or call 04 362 2222.</value>
    </values>
    <values>
        <field>is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
