<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DIFC UATFull</label>
    <protected>false</protected>
    <values>
        <field>HexaBPM__Environment_Type__c</field>
        <value xsi:type="xsd:string">Sandbox</value>
    </values>
    <values>
        <field>HexaBPM__Instance_URL__c</field>
        <value xsi:type="xsd:string">https://difc--uatfull.my.salesforce.com</value>
    </values>
    <values>
        <field>HexaBPM__Password__c</field>
        <value xsi:type="xsd:string">Mumbai@123</value>
    </values>
    <values>
        <field>HexaBPM__Security_Token__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>HexaBPM__User_Name__c</field>
        <value xsi:type="xsd:string">c-shaikh.zoebakhtar@difc.ae.uatfull1</value>
    </values>
</CustomMetadata>
