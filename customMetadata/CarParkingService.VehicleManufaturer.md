<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>VehicleManufaturer</label>
    <protected>false</protected>
    <values>
        <field>ListValues__c</field>
        <value xsi:type="xsd:string">Suzuki,Honda,Toyota,Nissan,Hyundai,Mercedes-Benz,Kia,Mitsubishi,BMW,Chevrolet,Ford,Lexus,Audi,Volkswagen,Rolls Royce,Porsche,Mazda,Infinity,GMC,Mini,Jaguar,Dodge,Tesla,Other</value>
    </values>
</CustomMetadata>
