<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>FitOut_OngoingProjectsDelayNotification</label>
    <protected>false</protected>
    <values>
        <field>Display_Order__c</field>
        <value xsi:type="xsd:double">10.0</value>
    </values>
    <values>
        <field>Filter_Applicable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order_by__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Related_Functionality__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SOQL__c</field>
        <value xsi:type="xsd:string">Select SR_Group__c,customer__r.Do_not_send_Fitout_delay_notification__c,Customer_Name__c,Email__c,Email_Address__c,id,Customer__r.Active_License__r.License_Issue_Date__c,Progress__c ,service_type__c ,Customer__r.Exclude_From_Fitout_Management_Report__c ,RecordType.Developername,Linked_SR__r.Linked_SR__c,Linked_SR__c,Location__c,Customer__r.Name,name,External_Status_Name__c,Submitted_Date__c,Type_of_Request__c,Site_Progress__c,Upgraded_Date__c,Property_Type__c,Delayed_Days__c from service_Request__c where (recordType.DeveloperName=&apos;Fit_Out_Service_Request&apos; OR (recordType.DeveloperName=&apos;Fit_Out_Induction_Request&apos; AND Delayed_Days__c &lt; 45) OR (recordType.DeveloperName=&apos;Request_Contractor_Access&apos; AND External_Status_Name__c = &apos;Approved&apos; AND createdDate = THIS_YEAR)) order by createddate DESC</value>
    </values>
    <values>
        <field>TestClass_SOQL__c</field>
        <value xsi:type="xsd:string">Select customer__r.Do_not_send_Fitout_delay_notification__c,Customer_Name__c,Email__c,Email_Address__c,id,Customer__r.Active_License__r.License_Issue_Date__c,Progress__c ,service_type__c ,Customer__r.Exclude_From_Fitout_Management_Report__c ,RecordType.Developername,Linked_SR__r.Linked_SR__c,Linked_SR__c,Location__c,Customer__r.Name,name,External_Status_Name__c,Submitted_Date__c,Type_of_Request__c,Site_Progress__c,Upgraded_Date__c,Property_Type__c,Delayed_Days__c from service_Request__c</value>
    </values>
    <values>
        <field>is_Used__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
