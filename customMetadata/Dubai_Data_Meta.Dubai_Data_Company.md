<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>dubai_data_company</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">Select Name,Account_ID_18_Report__c,Trade_Name__c,Company_Type__c,Registration_License_No__c,ROC_reg_incorp_Date__c,Next_Renewal_Date__c,Legal_Type_of_Entity__c,ROC_Status__c,DNFBP__c,Financial_Year_End__c,No_of_Issued_Shares__c,Sys_Office__c,Office__c,Building_Name__c,Street__c,PO_Box__c,City__c,Emirate__c from account where Registration_License_No__c!=&apos;&apos;</value>
    </values>
</CustomMetadata>
