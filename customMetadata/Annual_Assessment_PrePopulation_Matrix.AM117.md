<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AM117</label>
    <protected>false</protected>
    <values>
        <field>Api_Field__c</field>
        <value xsi:type="xsd:string">What_geographical_area_does_it_cover__c</value>
    </values>
    <values>
        <field>Api_Value__c</field>
        <value xsi:type="xsd:string">South East Asia</value>
    </values>
    <values>
        <field>Effect_on_risk__c</field>
        <value xsi:type="xsd:string">Less risk of breaches or inappropriate data sharing by the importing organization, less risk of our organization contravening the DP Law 2020.</value>
    </values>
    <values>
        <field>Likelihood_of_harm__c</field>
        <value xsi:type="xsd:string">Medium</value>
    </values>
    <values>
        <field>Mitigation_measures_to_reduce_or_elimina__c</field>
        <value xsi:type="xsd:string">Where adequacy recognition has not been provided, we will reduce our risk by executing contracts that include required DP clauses and additional safeguards provided for in Article 27, so that the PD will be treated by the organization receiving it with the same care and diligence as it does in the DIFC.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">10.91</value>
    </values>
    <values>
        <field>Overall_risk__c</field>
        <value xsi:type="xsd:string">Reasonable  Assurance</value>
    </values>
    <values>
        <field>Replace_Response__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Residual_risk_help_text__c</field>
        <value xsi:type="xsd:string">Some risk will remain where PD must be processed outside of the DIFC in these jurisdictions.  We must proceed with caution and provide our staff with training and enhanced security policy instructions, and providing the DPO (if appointed) with all necessary tools including resources and training to ensure proper accountability and data sharing practices.</value>
    </values>
    <values>
        <field>Risk_Assessment__c</field>
        <value xsi:type="xsd:string">Because we must transfer PD to a place that may have / has a compatiable or equivalent DP Law and/ or operating environment to that of the DIFC, we have a moderate risk that it will not be treated by the organization receiving it with the same care and diligence.  Breaches, inappropraite onward transfers, or other cirucmstnaces that directly contravene the DP Law 2020 may occur as a result.</value>
    </values>
    <values>
        <field>Risk_Description__c</field>
        <value xsi:type="xsd:string">Our company transfers PD to moderate risk jurisdictions for processing PD.  They are moderate risk because they have a DP law and / or operating environment that is compatible or equivalent to the DIFC, but there are remaining enforcement or implementation concerns for the exporting entity to be aware of. Onward transfers should be monitored as part of regular review.</value>
    </values>
    <values>
        <field>Risk_Type__c</field>
        <value xsi:type="xsd:string">Identify and assess risks</value>
    </values>
    <values>
        <field>Severity_of_harm__c</field>
        <value xsi:type="xsd:string">Medium</value>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">What geographical area does it cover?</value>
    </values>
</CustomMetadata>
