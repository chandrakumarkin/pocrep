<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Front Desk Review-Return for more inform</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt; &lt;br/&gt;

The following request has been returned because additional information is required, as explained below:&lt;br/&gt; &lt;br/&gt;

&lt;html&gt; 
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt; 
&lt;tr&gt; 
&lt;th&gt;SR Number&lt;/th&gt; 
&lt;th&gt;Company Name&lt;/th&gt; 
&lt;th&gt;Service Name&lt;/th&gt; 
&lt;th&gt;DIFC GSO Comments&lt;/th&gt; 


&lt;/tr&gt; 
&lt;tr&gt; 
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt; 
&lt;td&gt;{!step.SR__r.Customer__r.name}&lt;/td&gt; 
&lt;td&gt;{!Step__c.Service_Type__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.Step_Notes__c}&lt;/td&gt; 
&lt;/tr&gt; 
&lt;/table&gt; 
&lt;/html&gt; 
&lt;br/&gt; &lt;br/&gt; 

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;

Please log on to the client portal and go to ‘Pending Actions’.&lt;br/&gt; &lt;br/&gt;

Click ‘View’, enter the required information under ‘Customer Comments’ and then click ‘Save’.&lt;br/&gt; &lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt; 
Kindly note that your service request with the number {!Step__c.SR__c} has been returned for more information. Please log on to DIFC portal and provide the required information. &lt;br/&gt;Regards,&lt;br/&gt; DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">GS_ALL</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Front Desk Review</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">REQUIRE_ADDL_INFO</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Service Request is returned for more information. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
