<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Bad Words with spaces</label>
    <protected>false</protected>
    <values>
        <field>Is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">Warning: One of the prohibited words is used within a Company and Trading Name</value>
    </values>
    <values>
        <field>Restricted_Words__c</field>
        <value xsi:type="xsd:string">f u c k, f.u.c.k</value>
    </values>
    <values>
        <field>Rule_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Verify_Complete_Word__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Warning_Type__c</field>
        <value xsi:type="xsd:string">Error</value>
    </values>
</CustomMetadata>
