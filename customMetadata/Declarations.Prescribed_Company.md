<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Prescribed Company</label>
    <protected>false</protected>
    <values>
        <field>Application_Type__c</field>
        <value xsi:type="xsd:string">Prescribed Company</value>
    </values>
    <values>
        <field>Declaration_Text__c</field>
        <value xsi:type="xsd:string">I confirm  that, after due and careful enquiry, the Initiator or Director making the confirmation is satisfied that the Structured Financing of the Prescribed Company is not being used and will not be used, whether in the UAE or elsewhere, to: 
(a) circumvent foreign ownership restrictions;
(b) change the nationality of a shareholder or interest holder in an entity to avoid foreign investor registration requirements; or
(c) avoid the imposition of any tax or duty as a consequence of the UAE nationality of the Prescribed Company.</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">I_Agree_Prescribed__c</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">HexaBPM__Service_Request__c</value>
    </values>
    <values>
        <field>Record_Type_API_Name__c</field>
        <value xsi:type="xsd:string">In_Principle</value>
    </values>
</CustomMetadata>
