<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DIFC P.O. BOX RENEWAL</label>
    <protected>false</protected>
    <values>
        <field>Notification_Details__c</field>
        <value xsi:type="xsd:string">&lt;b&gt;DIFC P.O. BOX RENEWAL&lt;/b&gt;&lt;br/&gt; 
We would like to remind all companies that hold a P.O. Box located in DIFC that renewal is due on 31 December 2018. 

Renewal submissions can be made from 2 January 2019 until 31 January 2019. Kindly renew your subscription during this time to avoid interruption in mail delivery or incurrence of any fines.</value>
    </values>
    <values>
        <field>is_Active__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
