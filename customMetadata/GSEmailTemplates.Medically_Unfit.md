<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Medically Unfit</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt; &lt;br/&gt;

We regret to inform you that the visa for applicant {!Step__c.Applicant_Name__c} – SR # {!Step__c.SR__c} has been rejected. This is due to the result of his/her Medical Fitness Test, provided by Dubai Health Authority, as ‘unfit’.&lt;br/&gt; &lt;br/&gt;

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;

Please request a ‘Cancellation of entry permit’ through the client portal. You will need to show a confirmed flight booking to leave the UAE as part of this request. &lt;br/&gt; &lt;br/&gt;

We will then contact you with the next steps.&lt;br/&gt; &lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,

Regret to inform you that the applicant with SR number  {!Step__c.SR__c}  is medically Unfit. Kindly check your email for more information.


Regards,
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">DIFC_Sponsorship_Visa_New;Employment_Visa_Govt_to_DIFC;Non_DIFC_Sponsorship_Visa_New;DIFC_Sponsorship_Visa_Renewal;Non_DIFC_Sponsorship_Visa_Renewal;Visa_from_Individual_sponsor_to_DIFC;Non_DIFC_Sponsorship_Visa_New_Transfer</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Medically Unfit</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CANCELLED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Service Request has been rejected. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
