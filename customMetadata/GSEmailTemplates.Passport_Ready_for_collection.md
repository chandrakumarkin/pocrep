<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Passport Ready for collection</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

Greetings from DIFC &lt;br/&gt;&lt;br/&gt;

We are pleased to inform you that the following request has been successfully completed:.&lt;br/&gt;&lt;br/&gt;

&lt;html&gt; 
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt; 
&lt;tr&gt; 
&lt;th&gt;Name Service&lt;/th&gt; 
&lt;th&gt;Applicant’s Name&lt;/th&gt; 
&lt;th&gt;Service Request No&lt;/th&gt; 
&lt;th&gt;Govt. Fine&lt;/th&gt; 


&lt;/tr&gt; 
&lt;tr&gt; 
&lt;td&gt;{!Step__c.Service_Type__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.Applicant_Name__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.SR__r.Fine_Amount_AED__c}&lt;/td&gt; 
&lt;/tr&gt; 
&lt;/table&gt; 
&lt;/html&gt; 
&lt;br/&gt; &lt;br/&gt; 

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;

If you selected the courier service, the original documents will now be delivered to the consignee. S/he (or a company PRO) will need to show an Identity Card to receive the documents. Please contact our courier service on 04-3622491 or courierservices@difc.ae to follow up. &lt;br/&gt;&lt;br/&gt;

If you did not select the courier service, please contact us to collect the documents.&lt;br/&gt;&lt;br/&gt;

If a fine for late submission is due, please pay this by selecting ‘Government Fine Payment’ in the client portal.&lt;br/&gt;&lt;br/&gt;

&lt;a href=&quot;{!$Label.Community_URL}/srfeedback?id={!Step__c.SRId__c}&quot;&gt;Rate our service&lt;/a&gt;&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt;

Your service request with the number {!Step__c.SR__c} has been completed. Kindly collect the passport from DIFC GSO. &lt;br/&gt;


Regards, &lt;br/&gt;
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">Non_DIFC_Sponsorship_Visa_Amendment</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Passport is Ready for Collection</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CLOSED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Application process completed. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
