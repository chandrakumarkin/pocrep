<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Entity_Histories</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select Entity_History_ID_18_Report__c,Account_ID_18__c,Field_Label__c,Old_Value__c,New_Value__c,CreatedDate from Entity_History__c where Field_Label__c in (&apos;Account Name&apos;,&apos;Legal Type of Entity&apos;,&apos;Trade Name&apos;) and Account__r.Registration_License_No__c!=&apos;&apos; limit 10000</value>
    </values>
</CustomMetadata>
