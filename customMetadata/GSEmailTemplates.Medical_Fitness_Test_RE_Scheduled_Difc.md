<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Medical Fitness Test RE-Scheduled-Difc</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

Greetings from the DIFC &lt;br/&gt;&lt;br/&gt;

We would like to inform you that a medical fitness test has been re-scheduled for the applicant {!Step__c.Applicant_Name__c} - SR# {!Step__c.SR__c} as per the details in the table below.&lt;br/&gt;&lt;br/&gt;

&lt;html&gt;
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt;
&lt;tr&gt;
&lt;th&gt;SR Number&lt;/th&gt;
&lt;th&gt;Applicant’s Name&lt;/th&gt;
&lt;th&gt;Medical appointment date&lt;/th&gt;
&lt;th&gt;Medical Appointment Time&lt;/th&gt;


&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt;
&lt;td&gt;{!Step__c.Applicant_Name__c}&lt;/td&gt;
&lt;td&gt;{!Step__c.Appointment_StartDate}&lt;/td&gt;
&lt;td&gt;{!Step__c.Appointment_StartDate_Time__c}&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/html&gt;
&lt;br/&gt; &lt;br/&gt;

The applicant should visit DIFC Medical Test Centre, located at, level B1, Gate building (Marble Walk) on &lt;b&gt;Scheduled time only&lt;/b&gt;
The test will take approximately 15 minutes.&lt;br/&gt; &lt;br/&gt;

&lt;b&gt;Applicant should wear Mask and Gloves while visiting medical fitness Centre and ensure safe distance is maintained&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;&lt;br/&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">A medical test is re-scheduled for {!Step__c.Applicant_Name__c} on {!Step__c.Appointment_StartDate} at {!Step__c.Appointment_StartDate_Time__c} and an email was sent with the requirements. 

Regards, 
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">DIFC_Sponsorship_Visa_New;Visa_from_Individual_sponsor_to_DIFC;Employment_Visa_Govt_to_DIFC;Non_DIFC_Sponsorship_Visa_New;DIFC_Sponsorship_Visa_Renewal;Non_DIFC_Sponsorship_Visa_Renewal;Non_DIFC_Sponsorship_Visa_New_Transfer</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Medical Fitness Test Scheduled</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">RE-SCHEDULED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Medical Fitness Test is Re-Scheduled. SR#  {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
