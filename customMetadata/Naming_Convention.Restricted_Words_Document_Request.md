<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Restricted Words Document Request</label>
    <protected>false</protected>
    <values>
        <field>Is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">Warning: The names are considered as an authorized body. So you will have to provide evidence that you have permission to use the name.</value>
    </values>
    <values>
        <field>Restricted_Words__c</field>
        <value xsi:type="xsd:string">Government,Ministry,Dubai,Government of Dubai, Government of Dxb, Government of Abu Dhabi or Abu Dhabi or AD, Government of Ajman or Ajman, Government of Fujairah or Fujairah or Fuj, Government of Ras Al Khaimah,RAK, Ras Al Khaima, Government of Sharjah , Sharjah, or Shj, Ministry of Agriculture &amp; Fisheries, Ministry of Cabinet Affairs, Ministry of Communications, 
Ministry of Culture Youth &amp; Community Development, Ministry of Defense, Ministry of Economy, Ministry of Economy &amp; Commerce, Ministry of Education, Ministry of Education &amp; Youth, Ministry of Electricity &amp; Water, Ministry of Energy, Ministry of Environment and Water, Ministry of Finance, Ministry of Finance &amp; Industry, Ministry of Foreign Affairs, Ministry of Foreign Trade, Ministry of Health Ministry of Higher Education and Scientific Research, Ministry of Information &amp; Culture, Ministry of Interior, Ministry of Justice, Ministry of Justice, Islamic Affairs and Awqaf, Ministry of Labour, Ministry of Labor &amp; Social Affairs, Ministry of Petroleum &amp; Mineral Resources, Ministry of Planning, ministry of Presidential Affairs, Ministry of Public Works, Ministry of State, Ministry of State for Cabinet Affairs, Minister of State for Federal National Council Affairs, Minister of State for Foreign Affairs, Al Ain, The Financial Regulator, The Registrar, Central Bank of the United Arab Emirates., Insurance Authority of the United Arab Emirates., Emirates Securities and Commodities Authority</value>
    </values>
    <values>
        <field>Rule_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Verify_Complete_Word__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Warning_Type__c</field>
        <value xsi:type="xsd:string">Warning Message and Upload Doc</value>
    </values>
</CustomMetadata>
