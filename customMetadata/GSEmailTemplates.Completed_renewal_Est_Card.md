<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Completed - renewal Est Card</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

We are pleased to inform you that the application for Renewal of Establishment Card for {!step.SR__r.Customer__r.name} and SR # {!Step__c.SR__c}  has been approved by GDRFA.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;Next Steps&lt;/b&gt;

Please download  &lt;a href = &quot;{!$Label.Community_URL}/apex/DocumentViewer?id={!Step__c.SRId__c}&quot;&gt; the Establishment Card &lt;/a&gt; from the client portal. (Click ‘Download/Upload Documents’ to view and print the Card.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt; 
Kindly note that your service request with the number {!Step__c.SR__c} has been completed. &lt;br/&gt; Regards, &lt;br/&gt;DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:type="xsd:string">Renewal</value>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">Index_Card_Other_Services</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Ready for Collection</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CLOSED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Application process completed. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
