<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Non_DIFC_Sponsorship_Visa_Renewal</label>
    <protected>false</protected>
    <values>
        <field>Documents_Name__c</field>
        <value xsi:type="xsd:string">Affidavit Letter;Applicant&apos;s Birth Certificate;AR Sponsor Salary Certificate - Female;AR Sponsor Salary Certificate - Male;Car Registration Card Copy (2nd Car);Child&apos;s Mother Passport Copy;Child&apos;s Mother Visa Copy;Coloured Photo;Company Establishment Card;Copy of Employment Contract;Copy of Immigration Sticker;Copy of Insurance Certificate;Copy of Sponsor&apos;s visa;Copy of Visa;Cover Letter 13;DEWA Bill;EID Registration form;EID Registration Form after Biometrics;Emirates ID;Entry Permit;Guarantee Payment Receipt;Health Insurance Certificate;Marital Status Confirmation;Marriage Certificate;Medical Insurance certificate;Mother&apos;s passport copy and visa;Other Supporting Document;Passport;Passport Copy;Passport-1 (additional Upload);Salary Certificate;Sponsor Passport;Sponsor&apos;s colour photograph;Sponsor&apos;s marriage Certificate;Sponsor&apos;s spouse Emirates ID;Spouse Passport Copy;Tenancy Contract;University Letter;Visit or Tourist Visa</value>
    </values>
    <values>
        <field>Service_Type__c</field>
        <value xsi:type="xsd:string">Renewal of Dependent Visa Package</value>
    </values>
</CustomMetadata>
