<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Lead Status New</label>
    <protected>false</protected>
    <values>
        <field>Operate_Field_Value__c</field>
        <value xsi:type="xsd:string">0</value>
    </values>
    <values>
        <field>SF_Field_Value__c</field>
        <value xsi:type="xsd:string">New</value>
    </values>
    <values>
        <field>Sf_Field_Name__c</field>
        <value xsi:type="xsd:string">LeadStatus</value>
    </values>
</CustomMetadata>
