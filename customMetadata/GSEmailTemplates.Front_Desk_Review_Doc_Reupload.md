<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Front Desk Review-Doc Reupload</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

You service request, &lt;b&gt; {!Step__c.SR_Menu_Text__c} &lt;/b&gt; SR No. &lt;b&gt;{!Step__c.SR__c}&lt;/b&gt;, has been returned to re-upload certain documents. You request cannot be approved until the requested documents are provided.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;

1. Please visit the ‘Download/Upload Documents’ section of the client portall.&lt;br/&gt;
2. Any document that needs re-uploading will be marked as &apos;Replace&apos;, with corresponding DIFC comments.&lt;br/&gt;
3. Click &apos;Replace&apos; and attach the required document.&lt;br/&gt;
4. Click &apos;Save&apos; at the top of the table..&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">our request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt; 
Kindly note that your service request with the number {!Step__c.SR__c} has been returned for some document to upload. Please log on to DIFC portal and upload the required document(s) 
&lt;br/&gt; 
Regards,&lt;br/&gt; DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">GS_ALL</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Front Desk Review</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">REUPLOAD_DOCUMENT</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Service Request is returned for re-upload documents. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
