<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AuditorAPIServices</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select id,Name,Sys_Office__c,  Building_Name__c, Street__c, City__c, Country__c,ROC_reg_incorp_Date__c,Liquidated_Date__c ,Auditor_Type__c ,Phone,E_mail__c,Website,recordtypeid,Registration_License_No__c  from account where Auditor_Type__c != null and recordtypeid =&apos;0122000000033K1AAI&apos; and Auditor_Type__c != &apos;NA&apos;</value>
    </values>
</CustomMetadata>
