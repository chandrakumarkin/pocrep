<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LLP Blank SubType 2</label>
    <protected>false</protected>
    <values>
        <field>Ends_With__c</field>
        <value xsi:type="xsd:string">LLP</value>
    </values>
    <values>
        <field>OB_Ends_With_Arabic__c</field>
        <value xsi:type="xsd:string">شراكة ذات مسؤولية محدودة</value>
    </values>
    <values>
        <field>OB_SubType_of_Entity__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>OB_Type_Of_Entity__c</field>
        <value xsi:type="xsd:string">Limited Liability Partnership (LLP)</value>
    </values>
</CustomMetadata>
