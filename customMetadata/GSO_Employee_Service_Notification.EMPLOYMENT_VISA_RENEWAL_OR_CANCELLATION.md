<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>.</label>
    <protected>false</protected>
    <values>
        <field>Notification_Details__c</field>
        <value xsi:type="xsd:string">&lt;h5&gt; ADDITIONAL REQUIREMENT FOR NEW EMPLOYMENT&lt;/h5&gt; 


&lt;p&gt; 

The DIFC Government Services Office (GSO) would like to inform you that effective &lt;b&gt;15 February 2018&lt;/b&gt;, an attested &lt;b&gt;Good Conduct Certificate &lt;/b&gt;will be a &lt;b&gt; mandatory &lt;/b&gt;requirement for all new employment visa packages as detailed below: 
&lt;br /&gt;&lt;br /&gt; 
* New Visa Applications (applicant outside the country): Certificate should be issued by the country of origin of the applicant, or the country where he/she is residing. The Certificate should then be attested by the &lt;b&gt;UAE Embassy&lt;/b&gt; in that country 
&lt;br /&gt; 
* New Visa Applications (applicant in the UAE on a visit, tourist or on-arrival visa): Certificate should be issued by the embassy or consulate of applicant’s home country or country where he/she was residing. The Certificate should then be attested by the&lt;b&gt; UAE Ministry of Foreign Affairs&lt;/b&gt; in Dubai 
&lt;br /&gt; 
* New Visa applications (Applicant inside UAE on a cancelled visa or visa transfer) Certificate should be issued by Dubai Police. 
&lt;br /&gt; 
* The Good Conduct Certificate should be issued in &lt;b&gt;English or Arabic&lt;/b&gt; within three months of the submission date. This requirement does not apply to visit visa and dependent visa applications. 

&lt;/p&gt; 



&lt;h5&gt;EMPLOYMENT VISA RENEWAL OR CANCELLATION&lt;/h5&gt; 
&lt;p&gt; 
We would like to remind you that all Employment Visas must be &lt;b&gt;renewed or cancelled within 30 days&lt;/b&gt; from the date of expiry, as instructed by General Directorate of Residency and Foreigners Affairs.&lt;br/&gt; 

Failure to submit a renewal or cancellation request for an expired visa on or before &lt;b&gt;18 January 2018&lt;/b&gt; may result in Employee Services being blocked on the DIFC Client Portal.&lt;br/&gt; 

We request all companies to log on to the Client Portal and navigate to ‘Company Info’ in order to view the expiry date of employee visas.&lt;br/&gt; 
&lt;/p&gt; 

&lt;hr /&gt; 

&lt;p&gt; 
For any enquiries or in the case of discrepancies between employee information noted on the Client Portal and company records, please contact the DIFC Government Services Office Helpdesk on gs.helpdesk@difc.ae or 04 362 2391. 
&lt;/p&gt; 


&lt;p&gt; 

Thank you for your understanding and co-operation.&lt;/p&gt;</value>
    </values>
    <values>
        <field>is_Active__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
