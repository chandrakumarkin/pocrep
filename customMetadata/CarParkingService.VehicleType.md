<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>VehicleType</label>
    <protected>false</protected>
    <values>
        <field>ListValues__c</field>
        <value xsi:type="xsd:string">Hatchback,Sedan,MPV,SUV,Crossover,Coupe,Convertible</value>
    </values>
</CustomMetadata>
