<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>getcardsbywildcardsearch007</label>
    <protected>false</protected>
    <values>
        <field>Field_Name__c</field>
        <value xsi:type="xsd:string">true</value>
    </values>
    <values>
        <field>Namespace__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Parent_Node__c</field>
        <value xsi:type="xsd:string">getCardsByWildcardSearch</value>
    </values>
    <values>
        <field>Request_Name__c</field>
        <value xsi:type="xsd:string">getCardsByWildcardSearch</value>
    </values>
    <values>
        <field>Sequence__c</field>
        <value xsi:type="xsd:double">7.0</value>
    </values>
    <values>
        <field>Source_Object__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Constant</value>
    </values>
    <values>
        <field>Xml_Label__c</field>
        <value xsi:type="xsd:string">onlyCardsInsideCarpark</value>
    </values>
</CustomMetadata>
