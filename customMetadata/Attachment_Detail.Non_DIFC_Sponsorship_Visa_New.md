<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Non_DIFC_Sponsorship_Visa_New</label>
    <protected>false</protected>
    <values>
        <field>Documents_Name__c</field>
        <value xsi:type="xsd:string">AR Sponsor Salary Certificate - Male;Coloured Photo;Copy of Sponsor&apos;s visa;Cover Letter 1;DEWA Bill;Emirates ID;Emirates ID of Sponsor&apos;s Spouse;Entry Permit;Other Supporting Document;Passport;Passport-1 (additional Upload);PR Barcode;re-upload the attestation of the marriage certificate;Re-upload the spouse visa page;Sponsor Passport;Sponsor Passport-1 (additional Upload);Sponsor&apos;s colour photograph;Sponsor&apos;s Coloured Photo;Sponsor&apos;s marriage Certificate;Sponsor&apos;s spouse Emirates ID;Spouse Passport Copy;Spouse Passport Copy-1 (additional Upload);Tenancy Contract;Visit or Tourist Visa</value>
    </values>
    <values>
        <field>Service_Type__c</field>
        <value xsi:type="xsd:string">New Dependent Visa Package</value>
    </values>
</CustomMetadata>
