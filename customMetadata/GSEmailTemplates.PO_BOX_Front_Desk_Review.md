<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>PO BOX-Front Desk Review</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt; 

We are pleased to inform you that the request for &amp;nbsp; {!Step__c.SR_Menu_Text__c} with request no.{!Step__c.SR__c} for client&amp;nbsp; {!Step__c.Client_Name__c} has been approved.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt;&lt;br/&gt; 

To start your PO Box services, please visit the Emirates Post Office located next to DIFC Courts. You will need to take:&lt;br/&gt;
–  A copy of your DIFC commercial licence.&lt;br/&gt;
– A copy of the payment receipt issued for this service request by the DIFC Portal.&lt;br/&gt;
–  The PO Box service fees.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">PO_BOX</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Front Desk Review</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">VERIFIED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Signed PO Box Application is ready. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
