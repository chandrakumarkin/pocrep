<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Converted User Registration</label>
    <protected>false</protected>
    <values>
        <field>Application_Type__c</field>
        <value xsi:type="xsd:string">Adding a new User</value>
    </values>
    <values>
        <field>Declaration_Text__c</field>
        <value xsi:type="xsd:string">I hereby consent on behalf of the abovementioned entity, to submit all forms related to the Registrar of Companies and Commissioner of Data Protection (“the Statutory Bodies”) electronically on the DIFC Client Portal.&lt;br/&gt;&lt;br/&gt;
I hereby confirm the following on behalf of the abovementioned entity:&lt;br/&gt;&lt;br/&gt;

&lt;ul&gt;
&lt;li&gt;1. that any forms lodged on the DIFC Client Portal can be used as if the original form was submitted;&lt;/li&gt;
&lt;li&gt;2. that the original form(s) submitted on the DIFC Client Portal shall be retained by the abovementioned entity as evidence of its authenticity, and that if a question of authenticity arises, the relevant Statutory Body may require submission of the original form(s);&lt;/li&gt;
&lt;li&gt;3. the individual whose name is mentioned above is authorized by the abovementioned entity to file the applicable form(s) and submit any required document(s) on the DIFC Client Portal on its behalf.&lt;/li&gt;
&lt;li&gt;3.1 I understand that it is my responsibility to ensure that the abovementioned individual is duly authorized by the abovementioned entity to be a portal user and file the applicable form(s) and submit any required document(s) on the DIFC Client Portal on its behalf.&lt;/li&gt;
&lt;li&gt;3.2 I understand that it is an offence to provide information which is false, misleading or deceptive or to conceal information where the concealment of such information is likely to mislead or deceive.&lt;/li&gt;
&lt;/ul&gt;</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">i_agree__c</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">HexaBPM__Service_Request__c</value>
    </values>
    <values>
        <field>Record_Type_API_Name__c</field>
        <value xsi:type="xsd:string">Converted_User_Registration</value>
    </values>
</CustomMetadata>
