<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Account_Share_Details</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select Account_Share_ID_18_Report__c,Account_ID_18__c,Name ,Nominal_Value__c,No_of_shares_per_class__c from Account_Share_Detail__c where Status__c=&apos;Active&apos; and Account__r.Registration_License_No__c!=&apos;&apos;</value>
    </values>
</CustomMetadata>
