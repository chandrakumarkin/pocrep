<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>data_relationship</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select Body_Corporate_Or_Individual__c,Relationship_ID_Contact_OR_Account__c,Relationship_ID_18__c,Account_ID_18__c,Relationship_Name__c,Relationship_Type_formula__c,Active__c,Start_Date__c,End_Date__c from Relationship__c where Relationship_Type__c in (&apos;Has Auditor&apos;,&apos;Has Designated Member&apos;,&apos;Has Director&apos;,&apos;Has Founding Member&apos;,&apos;Has General Partner&apos;,&apos;Has Limited Partner&apos;,&apos;Has Manager,Has Member&apos;,&apos;Has Partner&apos;,&apos;Is Shareholder Of&apos;,&apos;Has Company Secretary&apos;) and Hide_Relationship__c =false and Subject_Account__r.Registration_License_No__c!=&apos;&apos; and Start_Date__c!=null  order by Start_Date__c DESC limit 10000</value>
    </values>
</CustomMetadata>
