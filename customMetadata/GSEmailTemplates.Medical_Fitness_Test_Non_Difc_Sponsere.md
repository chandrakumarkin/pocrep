<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Medical Fitness Test - Non Difc Sponsere</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

We are pleased to inform you that the Medical Fitness Test process for applicant {!step.SR__r.Customer__r.name} – SR No # {!Step__c.SR__c} has been completed by Dubai Health Authority.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;

Please download the medical certificate from the client portal. (Click ‘Download/Upload Documents’ to view and print the certificate.)&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;
Kindly note that your service request  with the number {!Step__c.SR__c}  has been completed. Please log on to client portal to download the medical report. &lt;br/&gt; Regards, &lt;br/&gt;DIFC

Regards, 
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:type="xsd:string">Non_Difc</value>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">Medical_Test_for_Non_DIFC_Sponsorship</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Completed</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CLOSED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Application process completed. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
