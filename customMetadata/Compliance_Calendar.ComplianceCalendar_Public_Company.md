<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ComplianceCalendar-Public Company</label>
    <protected>false</protected>
    <values>
        <field>Condition__c</field>
        <value xsi:type="xsd:string">Legal_Type_of_Entity__c = Public Company</value>
    </values>
    <values>
        <field>Share_with_Link__c</field>
        <value xsi:type="xsd:string">https://difc.my.salesforce.com/sfc/p/20000000nkZ5/a/0J000000kmgW/VTvR.6McHqMll7x57SiPRraEf5LDW7o1Y86rK4pG5dE</value>
    </values>
</CustomMetadata>
