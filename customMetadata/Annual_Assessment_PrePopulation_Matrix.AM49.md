<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AM49</label>
    <protected>false</protected>
    <values>
        <field>Api_Field__c</field>
        <value xsi:type="xsd:string">Current_state_of_emerging_or_advanced__c</value>
    </values>
    <values>
        <field>Api_Value__c</field>
        <value xsi:type="xsd:string">Biometric identification - i.e., fingerprints</value>
    </values>
    <values>
        <field>Effect_on_risk__c</field>
        <value xsi:type="xsd:string">TOMs and PET will reduce our risk significantly if implemented correctly and resource is provided to support these measures.</value>
    </values>
    <values>
        <field>Likelihood_of_harm__c</field>
        <value xsi:type="xsd:string">Medium</value>
    </values>
    <values>
        <field>Mitigation_measures_to_reduce_or_elimina__c</field>
        <value xsi:type="xsd:string">We will reduce our security risk around the type of processing we engage in by ensuring appropriate techincal and organizational measures are in place, and / or seeking consultation with IT and security experts on Privacy Enhancing Technologies.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">14.3</value>
    </values>
    <values>
        <field>Overall_risk__c</field>
        <value xsi:type="xsd:string">Very Limited Assurance</value>
    </values>
    <values>
        <field>Replace_Response__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Residual_risk_help_text__c</field>
        <value xsi:type="xsd:string">Some risk will remain where PD must be processed by way of emerging or technology, which may also be considered HRP, or technology that in any case may prevent individuals from exercising their rights. We must proceed with caution and provide our staff with training and enhanced seucrity policy instructions, and providing the DPO (if appointed) with all necessary tools including resources and training to ensure proper accountability and data sharing practices.</value>
    </values>
    <values>
        <field>Risk_Assessment__c</field>
        <value xsi:type="xsd:string">Processing methods we use are have yet to be completely understood by individuals whose data is subject to it, and this lack of understanding may negatively impact data subjects&apos; rights such as erasure, objection or rectification.</value>
    </values>
    <values>
        <field>Risk_Description__c</field>
        <value xsi:type="xsd:string">We engage in the following types of emerging or advanced IT to conduct our business and / or processing activities: (populate with the choices from above)</value>
    </values>
    <values>
        <field>Risk_Type__c</field>
        <value xsi:type="xsd:string">Identify and assess risks</value>
    </values>
    <values>
        <field>Severity_of_harm__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">What is the current state of emerging or advanced technology in this area?</value>
    </values>
</CustomMetadata>
