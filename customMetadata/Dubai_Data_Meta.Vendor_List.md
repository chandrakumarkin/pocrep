<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Vendor List</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select vendor_Name__c,OrganizationType__c,Corporate_Address__c,Postal_Code__c,State__c,Country__c,Corporate_Phone__c,Corporate_Email_Address__c,Corporate_Fax__c,Corporate_URL__c,Year_Founded__c,Preferred_Currency__c,Number_Of_Employees__c from DD_Vendor_List__c</value>
    </values>
</CustomMetadata>
