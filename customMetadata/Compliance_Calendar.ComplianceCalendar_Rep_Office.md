<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ComplianceCalendar-Rep Office</label>
    <protected>false</protected>
    <values>
        <field>Condition__c</field>
        <value xsi:type="xsd:string">License_Activity__c = 066</value>
    </values>
    <values>
        <field>Share_with_Link__c</field>
        <value xsi:type="xsd:string">https://difc.my.salesforce.com/sfc/p/20000000nkZ5/a/0J000000kmgb/2Fj_Xwn3kJdoqgLeB90cAR8OIJ3CC3ppGApQsmY3ku4</value>
    </values>
</CustomMetadata>
