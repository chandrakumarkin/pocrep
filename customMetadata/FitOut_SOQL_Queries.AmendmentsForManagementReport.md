<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AmendmentsForManagementReport</label>
    <protected>false</protected>
    <values>
        <field>Display_Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>Filter_Applicable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order_by__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Related_Functionality__c</field>
        <value xsi:type="xsd:string">Management Report</value>
    </values>
    <values>
        <field>SOQL__c</field>
        <value xsi:type="xsd:string">SELECT Customer__r.Active_License__r.License_Issue_Date__c,Customer__r.Exclude_From_Fitout_Management_Report__c,Id, Name, RecordTypeId, Building_Name__c, Customer__c, Status__c, ServiceRequest__c,Lease_Partner__c,servicerequest__r.Property_Type__c ,ServiceRequest__r.name,ServiceRequest__r.Customer_Name__c,ServiceRequest__r.Location__c,ServiceRequest__r.Progress__c,Lease__r.RF_Valid_From__c,Lease__r.RF_Valid_To__c,Lease__r.Start_date__c,Lease_Partner__r.Unit__c,Lease_Partner__r.Unit__r.Unit_Usage_Type__c FROM Amendment__c where Lease_Partner__r.Unit__c in : unitIdSet AND RecordType.DeveloperName =&apos;Fit_Out_Units&apos; AND ServiceRequest__r.Service_type__c =&apos;Request for Contractor Portal Access&apos; AND ServiceRequest__r.External_Status_Name__c =&apos;Approved&apos;</value>
    </values>
    <values>
        <field>TestClass_SOQL__c</field>
        <value xsi:type="xsd:string">SELECT Customer__r.Active_License__r.License_Issue_Date__c,Customer__r.Exclude_From_Fitout_Management_Report__c,Id, Name, RecordTypeId, Building_Name__c, Customer__c, Status__c, ServiceRequest__c,Lease_Partner__c,servicerequest__r.Property_Type__c ,ServiceRequest__r.name,ServiceRequest__r.Customer_Name__c,ServiceRequest__r.Location__c,ServiceRequest__r.Progress__c,Lease__r.RF_Valid_From__c,Lease__r.RF_Valid_To__c,Lease__r.Start_date__c,Lease_Partner__r.Unit__c,Lease_Partner__r.Unit__r.Unit_Usage_Type__c FROM Amendment__c limit 1</value>
    </values>
    <values>
        <field>is_Used__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
