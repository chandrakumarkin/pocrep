<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DIFC_Sponsorship_Visa_Cancellation</label>
    <protected>false</protected>
    <values>
        <field>Documents_Name__c</field>
        <value xsi:type="xsd:string">Company Establishment Card;Copy of Visa;Copy of Work Permit;Letter to Immigration;Other Supporting Document;Passport;Signed confirmation of employee dues;Signed employee confirmation letter;Signed Employment Cancellation with Dues Paid;Signed Employment Cancellation with Dues Paid -Outside;Signed Employment Cancellation with Pending Dues;Signed Employment Cancellation with Pending Dues-Outside</value>
    </values>
    <values>
        <field>Service_Type__c</field>
        <value xsi:type="xsd:string">Cancellation of Employment Visa</value>
    </values>
</CustomMetadata>
