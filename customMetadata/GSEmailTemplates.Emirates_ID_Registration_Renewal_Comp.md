<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Emirates ID Registration Renewal Comp</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

We are pleased to inform you that the Emirates ID Registration of applicant {!Step__c.Applicant_Name__c} – SR # {!Step__c.SR__c} has been successfully completed.&lt;br/&gt;&lt;br/&gt;


&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt;
&lt;tr&gt;
&lt;th&gt;SR Number&lt;/th&gt;
&lt;th&gt;Name of Company&lt;/th&gt;
&lt;th&gt;Name of Applicant&lt;/th&gt;
&lt;th&gt;EID Registration Number&lt;/th&gt;
&lt;th&gt;Biometrics&lt;/th&gt;

&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt;
&lt;td&gt;{!step.SR__r.Customer__r.name}&lt;/td&gt;
&lt;td&gt;{!Step__c.Applicant_Name__c}&lt;/td&gt;
&lt;td&gt;{!Step__c.SAP_IDNUM__c}&lt;/td&gt;
&lt;td&gt;{!Step__c.Biometrics_Required__c}&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;

&lt;br/&gt;

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;


&lt;ul&gt;If EID biometrics are required, this will be shown in the last column of the table above. The applicant should then: &lt;br /&gt;&lt;br /&gt;
&lt;li&gt; Download the Emirates ID Registration. (Click ‘Download/Upload Document’ to view and print.) &lt;/li&gt;

&lt;li&gt; Proceed to Diyafah Street - Al Bada&apos;a  EID enrolment center (Al Ghazal Shopping Mall) to complete the biometrics. (Please find location map and contact details &lt;a href=&quot;https://www.google.com/maps/contrib/118161995732959167017/place/ChIJVaSxMgBDXz4RTumn7caJyJA/@25.2288143,55.2865388,15z/data=!4m6!1m5!8m4!1e1!2s118161995732959167017!3m1!1e1&quot; target=&quot;_blank&quot;&gt; here. &lt;/a&gt;).Kindly note that as part of Covid 19 precautions, EID Enrollment Centre might ask for the medical fitness test result for applicants above 18 years of age. Medical test result can be downloaded from the client portal in 24 Hours (4 Hrs for Express applications )after medical fitness test is completed at DIFC medical fitness center.&lt;/li&gt;

&lt;li&gt; Submit the EID Registration form with the stamp ‘Process completed’ along with original passport upon request from DIFC Government Services Office &lt;/li&gt;

&lt;/ul&gt;


&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;&lt;br/&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, 
This is to inform you that Emirates ID registration for  {!Step__c.Applicant_Name__c} has been completed.An Email has been sent with further details.&lt;br/&gt;
Regards, 
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">Non_DIFC_Sponsorship_Visa_Renewal;DIFC_Sponsorship_Visa_Renewal</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Emirates ID Registration is Completed</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CLOSED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Emirates ID Registration is completed. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
