<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Application rejected by Immegration</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, 
&lt;br/&gt; &lt;br/&gt; 

We regret to inform you that the visa of the applicant {!Step__c.Applicant_Name__c} – SR # {!Step__c.SR__c} has been rejected by GDRFA.&lt;br/&gt; &lt;br/&gt; &lt;br/&gt; 

If any original documents were submitted along with this service request, kindly arrange to collect them back from DIFC Government Services Office. &lt;br/&gt; &lt;br/&gt; &lt;br/&gt; 

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt; 


Regret to inform you that your request with the number {!Step__c.SR__c} has been rejected by GDRFA. Kindly log on to DIFC portal for more details. &lt;br/&gt; 


Regards,&lt;br/&gt; DIFC&lt;br/&gt;</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">Personal_Visit_Visa;Business_Visit_Visa</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Application Rejected by Immigration</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CANCELLED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Service Request Rejected by GDRFA.SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
