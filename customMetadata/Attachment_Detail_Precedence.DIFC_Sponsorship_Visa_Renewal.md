<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DIFC_Sponsorship_Visa_Renewal</label>
    <protected>false</protected>
    <values>
        <field>Precedence__c</field>
        <value xsi:type="xsd:string">EID Registration Form after Biometrics=&gt;EID Registration form</value>
    </values>
    <values>
        <field>Service_Type__c</field>
        <value xsi:type="xsd:string">Renewal of Employment Visa Package</value>
    </values>
</CustomMetadata>
