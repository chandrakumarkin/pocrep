<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AM5</label>
    <protected>false</protected>
    <values>
        <field>Api_Field__c</field>
        <value xsi:type="xsd:string">What_is_the_source_of_the_data__c</value>
    </values>
    <values>
        <field>Api_Value__c</field>
        <value xsi:type="xsd:string">External sources</value>
    </values>
    <values>
        <field>Effect_on_risk__c</field>
        <value xsi:type="xsd:string">Contractual obligations and consistent practices between organizations reduce the risk created.</value>
    </values>
    <values>
        <field>Likelihood_of_harm__c</field>
        <value xsi:type="xsd:string">Medium</value>
    </values>
    <values>
        <field>Mitigation_measures_to_reduce_or_elimina__c</field>
        <value xsi:type="xsd:string">Overall we can reduce our risk and provide a higher level of assurance that PD will be protected if there are internal policies and procedures in place as well as contracts with the 3rd parties entailing DP obligations, to ensure compliance with DP Law 2020 for safe data processing of PD collected from 3rd parties, we have and our privacy policy reflects all such collection methods and uses.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">3.0</value>
    </values>
    <values>
        <field>Overall_risk__c</field>
        <value xsi:type="xsd:string">Limited Assurance</value>
    </values>
    <values>
        <field>Replace_Response__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Residual_risk_help_text__c</field>
        <value xsi:type="xsd:string">Minimal; regular checks on compliance and implementation of any obligations are conducted</value>
    </values>
    <values>
        <field>Risk_Assessment__c</field>
        <value xsi:type="xsd:string">Collecting PD from a 3rd party is more likely to cause harm to a data subject / individual because they have not handed it over directly, and if there is a breach or unlawful / accidental data loss, the severity of harm is high.</value>
    </values>
    <values>
        <field>Risk_Description__c</field>
        <value xsi:type="xsd:string">We collect data from a 3rd party such as suppliers, vendors, contractors.</value>
    </values>
    <values>
        <field>Risk_Type__c</field>
        <value xsi:type="xsd:string">Identify and assess risks</value>
    </values>
    <values>
        <field>Severity_of_harm__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">What is the source of  the data?</value>
    </values>
</CustomMetadata>
