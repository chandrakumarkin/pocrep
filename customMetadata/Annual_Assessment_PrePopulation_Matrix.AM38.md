<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AM38</label>
    <protected>false</protected>
    <values>
        <field>Api_Field__c</field>
        <value xsi:type="xsd:string">Types_of_processing_in_your_operations__c</value>
    </values>
    <values>
        <field>Api_Value__c</field>
        <value xsi:type="xsd:string">A considerable amount of Personal Data will be Processed (including staff and contractor Personal Data) and where such Processing is likely to result in a high risk to the Data Subject</value>
    </values>
    <values>
        <field>Effect_on_risk__c</field>
        <value xsi:type="xsd:string">Notification to indivudals and appointment of DPO results in transparency and accountability, reducing the risk of our organization contrevening the DP Law 2020 and / or having a negative impact on the rights of individuals.</value>
    </values>
    <values>
        <field>Likelihood_of_harm__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>Mitigation_measures_to_reduce_or_elimina__c</field>
        <value xsi:type="xsd:string">Wherever possible, we will not rely on HRP for PD we collect and store. Where we do have to rely on HRP, we will adequately inform the relevant individuals and appoint a Data Protection Officer, as required under Article 16 of the DP Law 2020, who will be responsible for conducting regular Data Protectin Impact Assessments, Annual Assessment, ensuring accountability of the organization to the individuals and for the senior management&apos;s understand of DP culture and proper data management obligations.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Overall_risk__c</field>
        <value xsi:type="xsd:string">Very Limited Assurance</value>
    </values>
    <values>
        <field>Replace_Response__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Residual_risk_help_text__c</field>
        <value xsi:type="xsd:string">Some risk will remain where HRP must be conducted, due to the nature of the processing or the type of PD processed. We must proceed with caution and provide the DPO with all necessary tools including resources and training to ensure proper accountability practices.</value>
    </values>
    <values>
        <field>Risk_Assessment__c</field>
        <value xsi:type="xsd:string">As a result of HRP activities identified [Replace Selected Response], personal data processing by our company may negatively impact the indivivduals to whom the PD belongs, and thereby cause harm to them if the processing is not conducted in compliance with the DP Law 2020 or other applicable laws.</value>
    </values>
    <values>
        <field>Risk_Description__c</field>
        <value xsi:type="xsd:string">We engage in one or more high risk processing activities</value>
    </values>
    <values>
        <field>Risk_Type__c</field>
        <value xsi:type="xsd:string">Identify and assess risks</value>
    </values>
    <values>
        <field>Severity_of_harm__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">What types of processing in your operations have been identified as likely to be high risk ?</value>
    </values>
</CustomMetadata>
