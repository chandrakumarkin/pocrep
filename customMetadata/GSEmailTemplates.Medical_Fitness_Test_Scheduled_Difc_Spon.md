<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Medical Fitness Test Scheduled-Difc Spon</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

A Medical Fitness Test has been scheduled for {!Step__c.Applicant_Name__c} - SR# {!Step__c.SR__c} as below:&lt;br/&gt;&lt;br/&gt;

&lt;html&gt;
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt;
&lt;tr&gt;
&lt;th&gt;Name of Service&lt;/th&gt;
&lt;th&gt;Name of Company&lt;/th&gt;
&lt;th&gt;Name Of Applicant&lt;/th&gt;
&lt;th&gt;Medical Test Date&lt;/th&gt;
&lt;th&gt;Medical Test Time&lt;/th&gt;
&lt;/tr&gt;
&lt;tr&gt;
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt;
&lt;td&gt;{!step.SR__r.Customer__r.name}&lt;/td&gt;
&lt;td&gt;{!Step__c.Applicant_Name__c}&lt;/td&gt;
&lt;td&gt;{!Step__c.Appointment_StartDate}&lt;/td&gt;
&lt;td&gt;{!Step__c.Appointment_StartDate_Time__c}&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/html&gt;
&lt;br/&gt; &lt;br/&gt;

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;

The applicant should visit DIFC Medical Test Centre, located at, level B1, Gate building (Marble Walk) on &lt;b&gt;Scheduled time only&lt;/b&gt;. The test will take approximately 15 minutes. (Please find the location map &lt;a href=&quot;https://www.google.ae/maps/place/25%C2%B012&apos;50.1%22N+55%C2%B016&apos;55.6%22E/@25.213915,55.2799233,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d25.213915!4d55.282112&quot; target=&quot;_blank&quot;&gt; here. &lt;/a&gt; Applicant can use ‘Visitor Parking at Gate District 2) &lt;br/&gt; &lt;br/&gt;

&lt;b&gt;Applicant should wear Mask and Gloves while visiting medical fitness Centre and ensure safe distance is maintained&lt;/b&gt;
&lt;br/&gt;&lt;br/&gt;
&lt;b&gt; If you need further help &lt;/b&gt;&lt;br/&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">A medical test is scheduled for {!Step__c.Applicant_Name__c} on  {!Step__c.Appointment_StartDate} at {!Step__c.Appointment_StartDate_Time__c}  and an email was sent with the requirements. 

Regards, 
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">DIFC_Sponsorship_Visa_New;Visa_from_Individual_sponsor_to_DIFC;Employment_Visa_Govt_to_DIFC;Non_DIFC_Sponsorship_Visa_New;DIFC_Sponsorship_Visa_Renewal;Non_DIFC_Sponsorship_Visa_Renewal;Non_DIFC_Sponsorship_Visa_New_Transfer;Medical_Test_for_Non_DIFC_Spon</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Medical Fitness Test Scheduled</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">SCHEDULED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Medical Fitness Test is Scheduled. SR#  {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
