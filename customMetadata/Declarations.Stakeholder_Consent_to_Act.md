<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Stakeholder Consent to Act</label>
    <protected>false</protected>
    <values>
        <field>Application_Type__c</field>
        <value xsi:type="xsd:string">Stakeholder Consent to Act</value>
    </values>
    <values>
        <field>Declaration_Text__c</field>
        <value xsi:type="xsd:string">Consent to Act

I confirm that the person named has consented to act as a [Role]</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">I_Agree_Stakeholder__c</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">HexaBPM__Service_Request__c</value>
    </values>
    <values>
        <field>Record_Type_API_Name__c</field>
        <value xsi:type="xsd:string">In_Principle</value>
    </values>
</CustomMetadata>
