<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Cancellation of Seconded Employee</label>
    <protected>false</protected>
    <values>
        <field>Relationship_Type__c</field>
        <value xsi:type="xsd:string">DIFC Seconded,Has DIFC Seconded,Secondment Card – Non- Sponsored</value>
    </values>
</CustomMetadata>
