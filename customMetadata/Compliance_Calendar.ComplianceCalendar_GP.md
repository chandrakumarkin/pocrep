<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ComplianceCalendar-GP</label>
    <protected>false</protected>
    <values>
        <field>Condition__c</field>
        <value xsi:type="xsd:string">Legal_Type_of_Entity__c = GP</value>
    </values>
    <values>
        <field>Share_with_Link__c</field>
        <value xsi:type="xsd:string">https://difc.my.salesforce.com/sfc/p/20000000nkZ5/a/0J000000kmfs/iJNM9skyk2frlKaR5eFK5F0EkOWnRCiTagy.3a.7_0U</value>
    </values>
</CustomMetadata>
