<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Non_DIFC_Sponsorship_Visa_Cancellation</label>
    <protected>false</protected>
    <values>
        <field>Documents_Name__c</field>
        <value xsi:type="xsd:string">Copy of Sponsor&apos;s visa;Copy of Visa;Other Supporting Document;Passport;Sponsor Passport</value>
    </values>
    <values>
        <field>Service_Type__c</field>
        <value xsi:type="xsd:string">Cancellation of Dependent Visa</value>
    </values>
</CustomMetadata>
