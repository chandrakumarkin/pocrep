<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Dubai_Data_Company_Lic_Activities</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select id,Activity_ID_18__c,Account_ID_18__c,Confirm_Cessation__c,License_Activity_Master_ID_18__c,Activity_Name_Eng__c,Start_Date__c,End_Date__c from License_Activity__c where Account__r.Registration_License_No__c!=&apos;&apos;</value>
    </values>
</CustomMetadata>
