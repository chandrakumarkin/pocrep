<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Geo_Category</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">SELECT Month__c, Year__c, country__C, Users__C,Sessions__c FROM Dubai_Data__c where Type__c=&apos;Geo&apos;</value>
    </values>
</CustomMetadata>
