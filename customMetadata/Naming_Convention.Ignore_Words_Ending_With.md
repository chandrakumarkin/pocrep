<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Ignore Words Ending With</label>
    <protected>false</protected>
    <values>
        <field>Is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Restricted_Words__c</field>
        <value xsi:type="xsd:string">Private Company,Pvt. Company,Pvt Company,Private Limited Company ,Private,Public Company,Public,PLC,Public limited company,Foundation,GP,General Partnership,LLP,Limited Liability Partnership,LP,Limited Partnership,LTD IC ,Investment Company,LTD PCC,Protected Cell Company,LTD SPC,Special Purpose Company,NPIO,Non-Profit Incorporated Organisation,RLLP Recognized Limited Limited Liability Partnership,RLP ,Recognized Limited Partnership,RP ,Recognized Partnership,The,Consulting Limited,Ltd,Limited,Ltd., Pvt,Pvt.,Foundation,Partnership,
Holding Limited, Holdings Limited, Holding Ltd, Holdings Ltd, Holdings Ltd., Holdings Ltd,Holding,Holdings</value>
    </values>
    <values>
        <field>Rule_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Verify_Complete_Word__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Warning_Type__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
