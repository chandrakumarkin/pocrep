<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Step_Name</label>
    <protected>false</protected>
    <values>
        <field>Step_Names__c</field>
        <value xsi:type="xsd:string">Visa Stamping Form is Typed,Visa Stamping Application is submitted Online,Visa Stamping Typed,Renewal Form is Typed</value>
    </values>
</CustomMetadata>
