<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Completed -Personal Visit Visa</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt; &lt;br/&gt;

We are pleased to inform you that the following request has been successfully completed: &lt;br/&gt; &lt;br/&gt;

&lt;html&gt; 
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt; 
&lt;tr&gt; 
&lt;th&gt;Name Service&lt;/th&gt; 
&lt;th&gt;Applicant’s Name&lt;/th&gt; 
&lt;th&gt;Service Request No&lt;/th&gt; 
&lt;/tr&gt; 
&lt;tr&gt; 
&lt;td&gt;{!Step__c.Service_Type__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.Applicant_Name__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt; 
&lt;/tr&gt; 
&lt;/table&gt; 
&lt;/html&gt; 
&lt;br/&gt; &lt;br/&gt; 

&lt;b&gt;Next Steps&lt;/b&gt;
Please download &lt;a href = &quot;{!$Label.Community_URL}/apex/DocumentViewer?id={!Step__c.SRId__c}&quot;&gt;the visit visa&lt;/a&gt; from the client portal. (Click on ‘Download/Upload Documents’ to view and print the visa.) Please send a copy of the visa to the applicant before s/he enters the UAE.&lt;br/&gt; &lt;br/&gt;

If you selected the courier service, the sponsor’s original Emirates ID will now be delivered to the consignee. S/he (or a company PRO) will need to show his/her Identity Card to receive the sponsor’s ID Card. Please contact our courier service on 04-3622491 or courierservices@difc.ae to follow up.&lt;br/&gt; &lt;br/&gt;

If you did not select the courier service, please contact us to collect the sponsor’s ID Card.&lt;br/&gt; &lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt;

Kindly note that your service request  with the number {!Step__c.SR__c}  has been completed. Please log on to client portal to download the visa. &lt;br/&gt;
Regards, &lt;br/&gt;
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">Personal_Visit_Visa</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Completed</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CLOSED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Application process completed. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
