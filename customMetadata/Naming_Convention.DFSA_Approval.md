<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DFSA Approval</label>
    <protected>false</protected>
    <values>
        <field>Is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Message__c</field>
        <value xsi:type="xsd:string">Warning: This requires DFSA approval. Please enter the reason for chosing the name</value>
    </values>
    <values>
        <field>Restricted_Words__c</field>
        <value xsi:type="xsd:string">Trust, Savings, Trustee, Bank, Wealth Manager, Insurance, Asset Management, Private Trust, Financial Advisor, Capital</value>
    </values>
    <values>
        <field>Rule_Name__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Verify_Complete_Word__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Warning_Type__c</field>
        <value xsi:type="xsd:string">Warning Message and Reason Text</value>
    </values>
</CustomMetadata>
