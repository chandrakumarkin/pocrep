<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GO-00300</label>
    <protected>false</protected>
    <values>
        <field>Net_Price__c</field>
        <value xsi:type="xsd:double">1075.13</value>
    </values>
    <values>
        <field>Release_Material_Description__c</field>
        <value xsi:type="xsd:string">Entry Permit Form is Typed</value>
    </values>
</CustomMetadata>
