<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ZRE_RODATA</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select id,Name,Company_Code__c,Name_of_Company_Code_or_Company__c,Building_permit__c,Municipality_key__c,Internal_Key_of_Real_Estate_Object__c,Client__c,Number_of_rental_unit_in_existing__c,Number_of_Building__c,Number_of_Rental_Object__c,Usage_Type_of_Rental_Object__c,Floor__c,Business_Entity_Number__c,Date_Object_Valid_From__c,Date_Object_Valid_To__c,Note_on_building_permit__c,Description_of_Building__c,Description_of_external_usage__c,Long_Description_for_Floor__c,Business_Entity_Name__c from ZRE_RODATA__c</value>
    </values>
</CustomMetadata>
