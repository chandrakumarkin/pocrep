<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DIFC_Sponsorship_Visa_Renewal</label>
    <protected>false</protected>
    <values>
        <field>Documents_Name__c</field>
        <value xsi:type="xsd:string">Coloured Photo;Company Establishment Card;Copy of Employment Contract;Copy of Visa;EID Registration Form after Biometrics;EID Registration form;Entry Permit;Medical Insurance certificate;Copy of Insurance Certificate;Health Insurance Certificate;Passport Copy;Passport-1 (additional Upload);Passport;Visit or Tourist Visa</value>
    </values>
    <values>
        <field>Service_Type__c</field>
        <value xsi:type="xsd:string">Renewal of Employment Visa Package</value>
    </values>
</CustomMetadata>
