<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Transfers_of_Personal_Data</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select Account_ID_18__c,Data_Protection_ID_18__c,Name_of_Jurisdiction__c from Data_Protection__c where IsActive__c=true and Account_ID_18__c!=null</value>
    </values>
</CustomMetadata>
