<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>LiquidatorAPIServices</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select id,Name,Phone__c,Email__c,Address__c,Website__c,Date_Of_Cessation__c,Date_Of_Registration__c,License_No__c  from Lookup__c where Type__c=&apos;Liquidator&apos;</value>
    </values>
</CustomMetadata>
