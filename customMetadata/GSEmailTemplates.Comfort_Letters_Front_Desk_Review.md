<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Comfort Letters - Front Desk Review</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt;&lt;br/&gt;

We are pleased to inform you that the following request has been approved:&lt;br/&gt;&lt;br/&gt;

&lt;html&gt; 
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt; 
&lt;tr&gt; 
&lt;th&gt;SR Number&lt;/th&gt; 
&lt;th&gt;Company Name&lt;/th&gt; 
&lt;th&gt;Service Name&lt;/th&gt; 
&lt;/tr&gt; 
&lt;tr&gt; 
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt; 
&lt;td&gt;{!step.SR__r.Customer__r.name}&lt;/td&gt; 
&lt;td&gt;{!Step__c.Service_Type__c}&lt;/td&gt; 
&lt;/tr&gt; 
&lt;/table&gt; 
&lt;/html&gt; 
&lt;br/&gt; &lt;br/&gt; 


&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;

Please note that the issued letter is an electronic letter that does not require collection from the DIFC Government Services Office.&lt;br/&gt; &lt;br/&gt;

Please download the &lt;a href = &quot;{!$Label.Community_URL}/apex/DocumentViewer?id={!Step__c.SRId__c}&quot;&gt;electronic letter&lt;/a&gt; from the portal.&lt;br/&gt; &lt;br/&gt;

&lt;a href=&quot;{!$Label.Community_URL}/srfeedback?id={!Step__c.SRId__c}&quot;&gt;Rate our service&lt;/a&gt;&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt; 
Kindly note that your service request  with the number {!Step__c.SR__c} has been completed. Please log on to client portal to download the letter.&lt;br/&gt; Regards,&lt;br/&gt; DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">GS_Comfort_Letters</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Front Desk Review</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">APPROVED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Service Request Approved. SR#  {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
