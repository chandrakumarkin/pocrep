<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Original Passport Received- Notification</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

We are pleased to inform you that the medical fitness test of {!Step__c.Applicant_Name__c} – SR No # {!Step__c.SR__c} has been completed successfully by Dubai Health Authority and ready for visa stamping.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;Next Steps&lt;/b&gt; &lt;br/&gt;

Please submit applicant’s original passport and Emirates ID registration form with the stamp from Emirates ID Authority (if EID biometrics was required) in order to complete visa stamping.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;Other points to note&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;

If you selected the courier service, please contact our courier agents on +971 4 362 2491 or courierservices@difc.ae when the passport is ready to be collected from your office. &lt;br/&gt;&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;&lt;br/&gt;

Please email us on gs.helpdesk@difc.ae or call us on +971 4 362 2391&lt;br/&gt;
Our working hours are Sunday to Thursday 8:00AM to 3:00PM.&lt;br/&gt; &lt;br/&gt;
Thank you,&lt;/b&gt;&lt;br/&gt;
DIFC Government Services Office</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,
This is to inform you that for the applican {!Step__c.Applicant_Name__c}.

Please submit the applicant’s original passport and Emirates ID registration form with the stamp from Emirates ID Authority (if EID biometrics as required) in order to complete visa stamping.

Regards,
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">GS_ALL</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Original Passport Received</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">AWAITING_REVIEW</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Original Passport Submission required SR # {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
