<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Foreign_entity_details__c</label>
    <protected>false</protected>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">Foreign_entity_details__c</value>
    </values>
    <values>
        <field>Sr_Doc_LIst__c</field>
        <value xsi:type="xsd:string">Foreign_entity_license,Certificate_of_Good_Standing_for_the_foreign_entity</value>
    </values>
</CustomMetadata>
