<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ContractorPortalAccess</label>
    <protected>false</protected>
    <values>
        <field>Display_Order__c</field>
        <value xsi:type="xsd:double">5.0</value>
    </values>
    <values>
        <field>Filter_Applicable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order_by__c</field>
        <value xsi:type="xsd:string">ORDER BY createdDate asc</value>
    </values>
    <values>
        <field>Related_Functionality__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SOQL__c</field>
        <value xsi:type="xsd:string">Select Target_Completion_Date__c,Customer__r.Active_License__r.License_Issue_Date__c,id,Customer__r.Exclude_From_Fitout_Management_Report__c,name from service_request__c where service_type__c =&apos;Request for Contractor Portal Access&apos; and (createdDate  &gt;= THIS_YEAR OR filters)</value>
    </values>
    <values>
        <field>TestClass_SOQL__c</field>
        <value xsi:type="xsd:string">Select Customer__r.Active_License__r.License_Issue_Date__c,id,Customer__r.Exclude_From_Fitout_Management_Report__c,name from service_request__c where recordType.Developername =&apos;Request_Contractor_Access&apos; limit 2</value>
    </values>
    <values>
        <field>is_Used__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
