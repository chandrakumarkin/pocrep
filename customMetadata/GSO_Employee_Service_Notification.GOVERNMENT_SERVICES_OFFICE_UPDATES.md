<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>GOVERNMENT SERVICES OFFICE UPDATES</label>
    <protected>false</protected>
    <values>
        <field>Notification_Details__c</field>
        <value xsi:type="xsd:string">&lt;b&gt;SERVICE CHARGES FOR ONLINE PAYMENTS WAIVED&lt;/b&gt;&lt;br/&gt;&lt;br/&gt; 

In line with our efforts to make DIFC Services more accessible to our community, while enhancing our customer journey and experience, we are pleased to inform you that the 2.5% service charge previously levied on online payments made through the Client Portal will no longer be applied.&lt;br/&gt;&lt;br/&gt; 

This development has come into effect from 1 March 2019, and will make it both faster and more efficient for DIFC-based clients to avail our services.</value>
    </values>
    <values>
        <field>is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
