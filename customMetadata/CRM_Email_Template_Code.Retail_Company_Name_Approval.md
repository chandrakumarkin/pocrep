<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Retail Company Name Approval</label>
    <protected>false</protected>
    <values>
        <field>Account_Company_Type__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Approver_Email__c</field>
        <value xsi:type="xsd:string">ammar.ali@difc.ae</value>
    </values>
    <values>
        <field>Approver_ID__c</field>
        <value xsi:type="xsd:string">0030J00001ySxOg</value>
    </values>
    <values>
        <field>Approver_Name__c</field>
        <value xsi:type="xsd:string">Anmar Ali</value>
    </values>
    <values>
        <field>CC__c</field>
        <value xsi:type="xsd:string">retail@difc.ae;Khushboo.Tahilramani@difc.ae;roc.helpdesk@difc.ae;SpecialtyLeasing@difc.ae</value>
    </values>
    <values>
        <field>Client__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Email_Template_Code__c</field>
        <value xsi:type="xsd:string">Retail_Company_Name_Approval</value>
    </values>
    <values>
        <field>Opportunity_Stage__c</field>
        <value xsi:type="xsd:string">Obtain In Principal Approval</value>
    </values>
    <values>
        <field>Point_of_Origin__c</field>
        <value xsi:type="xsd:string">Company Name Approval</value>
    </values>
    <values>
        <field>Security_Check__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Stage_Steps__c</field>
        <value xsi:type="xsd:string">In Progress</value>
    </values>
    <values>
        <field>Template_ID__c</field>
        <value xsi:type="xsd:string">00X0J000002ZkiL</value>
    </values>
</CustomMetadata>
