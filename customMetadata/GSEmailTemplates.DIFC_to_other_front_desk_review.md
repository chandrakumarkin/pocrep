<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DIFC to other- front desk review</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt; &lt;br/&gt;

We are pleased to inform you that the following Employment Visa Transfer has been approved by the DIFC Authority:&lt;br/&gt; &lt;br/&gt;

&lt;html&gt; 
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt; 
&lt;tr&gt; 
&lt;th&gt;SR Number&lt;/th&gt; 
&lt;th&gt;Company Name&lt;/th&gt; 
&lt;th&gt;Service Name&lt;/th&gt; 



&lt;/tr&gt; 
&lt;tr&gt; 
&lt;td&gt;{!Step__c.SR__c}&lt;/td&gt; 
&lt;td&gt;{!step.SR__r.Customer__r.name}&lt;/td&gt; 
&lt;td&gt;{!Step__c.Service_Type__c}&lt;/td&gt; 

&lt;/tr&gt; 
&lt;/table&gt; 
&lt;/html&gt; 
&lt;br/&gt; &lt;br/&gt; 

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;

Please complete the visa transfer with the new employer. &lt;br/&gt; &lt;br/&gt;

Once the new visa has been issued, upload a copy using the ‘Copy of New Visa’ section on the client portal. &lt;br/&gt; &lt;br/&gt;

The employee file will then be closed.&lt;br/&gt; &lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt;
Kindly note that your service request  with the number {!Step__c.SR__c}  has been approved. Please upload applicant&apos;s visa under new employer in client portal once stamped. &lt;br/&gt;Regards,&lt;br/&gt; DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">Employment_Visa_Transfer_from_DIFC_to_Others</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Front Desk Review</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">VERIFIED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Service Request is approved. SR#  {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
