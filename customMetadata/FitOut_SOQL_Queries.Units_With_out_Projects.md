<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Units_With_out_Projects</label>
    <protected>false</protected>
    <values>
        <field>Display_Order__c</field>
        <value xsi:type="xsd:double">11.0</value>
    </values>
    <values>
        <field>Filter_Applicable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order_by__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Related_Functionality__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SOQL__c</field>
        <value xsi:type="xsd:string">SELECT Id, Name, Account__c, Account__r.Exclude_From_Fitout_Management_Report__c,Account__r.Name, Lease__c, Unit__c,Lease__r.RF_Valid_From__c,Lease__r.RF_Valid_To__c,Unit__r.SAP_Unit_No__c,Unit__r.Building_Name__c,Unit__r.Unit_Type_DDP__c FROM Lease_Partner__c where Lease__r.RF_Valid_From__c != NULL and Unit__r.Building__r.Company_Code__c !=&apos;5500&apos; and Unit__r.Building__r.Name!=&apos;Gate Village Building 09&apos; and Lease__r.RF_Valid_From__c &lt;= today and Account__r.Do_not_send_Fitout_delay_notification__c = false and (Account__r.Active_License__c = Null OR Account__r.Active_License__r.License_Issue_Date__c &gt;= LAST_N_DAYS :365) filters</value>
    </values>
    <values>
        <field>TestClass_SOQL__c</field>
        <value xsi:type="xsd:string">SELECT Id, Name, Account__c, Account__r.Exclude_From_Fitout_Management_Report__c,Account__r.Name, Lease__c, Unit__c,Lease__r.RF_Valid_From__c,Lease__r.RF_Valid_To__c,Unit__r.SAP_Unit_No__c,Unit__r.Building_Name__c,Unit__r.Unit_Type_DDP__c FROM Lease_Partner__c</value>
    </values>
    <values>
        <field>is_Used__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
