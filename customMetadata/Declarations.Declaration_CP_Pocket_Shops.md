<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Declaration_CP_Pocket_Shops</label>
    <protected>false</protected>
    <values>
        <field>Application_Type__c</field>
        <value xsi:type="xsd:string">Standard Declaration for Forms</value>
    </values>
    <values>
        <field>Declaration_Text__c</field>
        <value xsi:type="xsd:string">&lt;p&gt;I hereby confirm that the above mentioned entity will ensure that it maintains valid licenses and permits for all activities it conducts in the DIFC, for the duration of its DIFC Commercial Permission.&lt;/p&gt;
&lt;br/&gt;
&lt;p&gt; I declare that the information contained in this form is true, correct and complete as of the date of submission and that all the requirements of the applicable DIFC laws and regulations have been complied with. I confirm that this form is submitted by a portal user that has the requisite authority to do so.&lt;/p&gt;
&lt;br/&gt;</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">i_agree__c</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">HexaBPM__Service_Request__c</value>
    </values>
    <values>
        <field>Record_Type_API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
