<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Company_by_sector_such_as_retail</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">Select Count__c,Sector__c from Company_by_sector_such_as_retail__c</value>
    </values>
</CustomMetadata>
