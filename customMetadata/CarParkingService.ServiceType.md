<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>ServiceType</label>
    <protected>false</protected>
    <values>
        <field>ListValues__c</field>
        <value xsi:type="xsd:string">New Vehicle Registration,Add Vehicle,Remove Employee Access,Remove Vehicle,Replace Vehicle Details,Replace/Lost Card,Replace/Lost Tag</value>
    </values>
</CustomMetadata>
