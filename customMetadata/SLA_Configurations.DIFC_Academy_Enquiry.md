<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DIFC Academy</label>
    <protected>false</protected>
    <values>
        <field>Business_Hour_Id__c</field>
        <value xsi:type="xsd:string">01m20000000BOf6</value>
    </values>
    <values>
        <field>Queue_Name__c</field>
        <value xsi:type="xsd:string">DIFC Academy</value>
    </values>
    <values>
        <field>SLA_Hours__c</field>
        <value xsi:type="xsd:double">8.0</value>
    </values>
    <values>
        <field>SLA_Minutes__c</field>
        <value xsi:type="xsd:double">480.0</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Enquiry</value>
    </values>
</CustomMetadata>
