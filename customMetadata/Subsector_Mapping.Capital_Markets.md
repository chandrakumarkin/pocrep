<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Capital Markets</label>
    <protected>false</protected>
    <values>
        <field>Subsector_Values__c</field>
        <value xsi:type="xsd:string">Other;Investment Banking;M&amp;A Advisory;Exchanges;Brokerage;Financial Advisory;Islamic Finance;Rating Agency;Crowdfunding Platform – Equity/investment;Crowdfunding Platform – loan;Crowdfunding Platform – Property</value>
    </values>
</CustomMetadata>
