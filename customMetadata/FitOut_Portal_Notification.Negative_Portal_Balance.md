<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Negative Portal Balance</label>
    <protected>false</protected>
    <values>
        <field>Display_Order__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Notification_Details__c</field>
        <value xsi:type="xsd:string">Negative balance means an amount is due to DIFC account, please settle.</value>
    </values>
    <values>
        <field>is_Active__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
