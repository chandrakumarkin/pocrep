<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>internal Transfer-front Desk review</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

Greetings from the DIFC&lt;br/&gt;&lt;br/&gt;

Please be informed that DIFC Government Services has received a request to transfer the visa of &quot;{!Step__c.Applicant_Name__c}&quot; to {!step.SR__r.Customer__r.name}.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">Employment_Visa_from_DIFC_to_DIFC</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Front Desk Review</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">PENDING_REVIEW</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Internal Transfer Notification.</value>
    </values>
</CustomMetadata>
