<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Closed-ended Incorporated Cell Public</label>
    <protected>false</protected>
    <values>
        <field>Ends_With__c</field>
        <value xsi:type="xsd:string">Closed-ended Incorporated Cell</value>
    </values>
    <values>
        <field>OB_Ends_With_Arabic__c</field>
        <value xsi:type="xsd:string">الخلية المسجلة المقفولة</value>
    </values>
    <values>
        <field>OB_SubType_of_Entity__c</field>
        <value xsi:type="xsd:string">Closed-ended Incorporated Cell</value>
    </values>
    <values>
        <field>OB_Type_Of_Entity__c</field>
        <value xsi:type="xsd:string">Public</value>
    </values>
</CustomMetadata>
