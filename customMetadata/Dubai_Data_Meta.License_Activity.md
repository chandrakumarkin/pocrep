<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>License_Activity</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select License_Activity_Master_18_Report__c,Name,Sector_Classification__c,Sys_Activity_Type__c from License_Activity_Master__c where Enabled__c=true</value>
    </values>
</CustomMetadata>
