<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Price Line Item</label>
    <protected>false</protected>
    <values>
        <field>SQL_Values__c</field>
        <value xsi:type="xsd:string">select Parent_Categories__c,Child_Categories__c,Items__c,Non_Retail_Price__c,Retail_Prices__c from Dubai_Data_Price_List__c</value>
    </values>
</CustomMetadata>
