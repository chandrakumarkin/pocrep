<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AM11</label>
    <protected>false</protected>
    <values>
        <field>Api_Field__c</field>
        <value xsi:type="xsd:string">How_often_is_such_data_collected__c</value>
    </values>
    <values>
        <field>Api_Value__c</field>
        <value xsi:type="xsd:string">Automated or recurring basis through regular streaming / file download from a website using bots, scraping or other file transfer mechanism</value>
    </values>
    <values>
        <field>Effect_on_risk__c</field>
        <value xsi:type="xsd:string">Less risk through direct collection and if not then applying through contracts, policies and procedures the same law and ethical requirements around data collection within the organization and in the destination of processing.</value>
    </values>
    <values>
        <field>Likelihood_of_harm__c</field>
        <value xsi:type="xsd:string">Medium</value>
    </values>
    <values>
        <field>Mitigation_measures_to_reduce_or_elimina__c</field>
        <value xsi:type="xsd:string">We can reduce our risk of contravening the DP Law 2020 and negatively impacting individual rights by collection PD directly from individuals who have been provided appropraite notice of processing activities, wherever possible. We also take necessary precautions where collection of data by this method is transferred to a non-DIFC, non-adequate jurisdiction, by applying the same priniciples and obligations in the destintation as would be applied in the DIFC.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">9.0</value>
    </values>
    <values>
        <field>Overall_risk__c</field>
        <value xsi:type="xsd:string">Limited Assurance</value>
    </values>
    <values>
        <field>Replace_Response__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Residual_risk_help_text__c</field>
        <value xsi:type="xsd:string">Minimal; regular checks on compliance and implementation of any obligations are conducted</value>
    </values>
    <values>
        <field>Risk_Assessment__c</field>
        <value xsi:type="xsd:string">Due to the frequency of data collection through the method of  automated or other recurring basis through regular streaming / file download from a website using bots, scraping or other file transfer mechanism, there is a risk that we are breaching the data minimization and / or purpose specification principles by collecting too much data and / or for processing in a manner that is incompatible with what we have notified individuals about.</value>
    </values>
    <values>
        <field>Risk_Description__c</field>
        <value xsi:type="xsd:string">Our company collects data on an automated or other recurring basis through regular streaming / file download from a website using bots, scraping or other file transfer mechanism</value>
    </values>
    <values>
        <field>Risk_Type__c</field>
        <value xsi:type="xsd:string">Identify and assess risks</value>
    </values>
    <values>
        <field>Severity_of_harm__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">How often is such data collected?</value>
    </values>
</CustomMetadata>
