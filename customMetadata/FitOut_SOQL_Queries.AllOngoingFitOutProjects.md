<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AllOngoingFitOutProjects</label>
    <protected>false</protected>
    <values>
        <field>Display_Order__c</field>
        <value xsi:type="xsd:double">2.0</value>
    </values>
    <values>
        <field>Filter_Applicable__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Order_by__c</field>
        <value xsi:type="xsd:string">ORDER BY progress__c</value>
    </values>
    <values>
        <field>Related_Functionality__c</field>
        <value xsi:type="xsd:string">Management Report</value>
    </values>
    <values>
        <field>SOQL__c</field>
        <value xsi:type="xsd:string">Select id,Progress__c ,Target_Completion_Date__c,service_type__c ,Email__c,Email_Address__c,Customer__r.Active_License__r.License_Issue_Date__c,Customer__r.Exclude_From_Fitout_Management_Report__c,RecordType.Developername,Linked_SR__r.Linked_SR__c,Linked_SR__c,Location__c,Customer__r.Name,name,External_Status_Name__c,Submitted_Date__c,Type_of_Request__c,Site_Progress__c,Upgraded_Date__c,Property_Type__c,Delayed_Days__c,
(Select id,name,Closed_Date__c,Step_Name__c,createdDate,sr_Step__r.owner__c,Status__r.Name,SR__c,sr_step__r.Submission_Type__c  from Steps_SR__r ) 
from service_Request__c where recordType.DeveloperName=&apos;Fit_Out_Service_Request&apos; and service_type__c =&apos;Fit - Out Service Request&apos;  and Property_Type__c !=&apos;Retail - General Outlets and Kiosk&apos; and Submitted_Date__c &gt;=2019-01-01</value>
    </values>
    <values>
        <field>TestClass_SOQL__c</field>
        <value xsi:type="xsd:string">Select id,Progress__c ,service_type__c ,Customer__r.Active_License__r.License_Issue_Date__c,Customer__r.Exclude_From_Fitout_Management_Report__c,RecordType.Developername,Linked_SR__r.Linked_SR__c,Linked_SR__c,Location__c,Customer__r.Name,Linked_SR__r.Linked_SR__r.Linked_SR__c,name,External_Status_Name__c,Submitted_Date__c,Type_of_Request__c,Site_Progress__c,Upgraded_Date__c,Property_Type__c,
(Select id,name,Closed_Date__c,Step_Name__c,createdDate,sr_Step__r.owner__c,Status__r.Name,sr_step__r.Submission_Type__c from Steps_SR__r) 
from service_Request__c limit 10</value>
    </values>
    <values>
        <field>is_Used__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
