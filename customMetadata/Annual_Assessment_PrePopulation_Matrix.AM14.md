<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>AM14</label>
    <protected>false</protected>
    <values>
        <field>Api_Field__c</field>
        <value xsi:type="xsd:string">Have_individuals_been_notified_through__c</value>
    </values>
    <values>
        <field>Api_Value__c</field>
        <value xsi:type="xsd:string">No</value>
    </values>
    <values>
        <field>Effect_on_risk__c</field>
        <value xsi:type="xsd:string">Transparency and accountability measures reduces the risk considerably.</value>
    </values>
    <values>
        <field>Likelihood_of_harm__c</field>
        <value xsi:type="xsd:string">Medium</value>
    </values>
    <values>
        <field>Mitigation_measures_to_reduce_or_elimina__c</field>
        <value xsi:type="xsd:string">We will reduce our risk by providing appropriate, easy to access and read privacy notices online and / or in any relevant information about our products and services shared with individuals, so that will understand our obligations and their rights under the DP Law 2020.</value>
    </values>
    <values>
        <field>Order__c</field>
        <value xsi:type="xsd:double">11.0</value>
    </values>
    <values>
        <field>Overall_risk__c</field>
        <value xsi:type="xsd:string">Very Limited Assurance</value>
    </values>
    <values>
        <field>Replace_Response__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>Residual_risk_help_text__c</field>
        <value xsi:type="xsd:string">Minimal; regular checks on compliance and implementation of any obligations are conducted</value>
    </values>
    <values>
        <field>Risk_Assessment__c</field>
        <value xsi:type="xsd:string">Because we do not have an online privacy policy, we put individuals at risk of not understanding what their rights are or how we use their data, which in turn creates a risk for our company of breaching the DIFC DP Law 2020.</value>
    </values>
    <values>
        <field>Risk_Description__c</field>
        <value xsi:type="xsd:string">We do not have an online DP or privacy policy notifying individuals of how we collect, process or share PD, or of their rights to control how we use it or to complain, as per the DP Law 2020.</value>
    </values>
    <values>
        <field>Risk_Type__c</field>
        <value xsi:type="xsd:string">Identify and assess risks</value>
    </values>
    <values>
        <field>Severity_of_harm__c</field>
        <value xsi:type="xsd:string">High</value>
    </values>
    <values>
        <field>UI_Label__c</field>
        <value xsi:type="xsd:string">Have individuals been notified through an online privacy policy or other communication of the collection and processing of personal data that your entity undertakes?</value>
    </values>
</CustomMetadata>
