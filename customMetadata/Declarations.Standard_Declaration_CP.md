<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Standard Declaration CP</label>
    <protected>false</protected>
    <values>
        <field>Application_Type__c</field>
        <value xsi:type="xsd:string">Standard Declaration for Forms</value>
    </values>
    <values>
        <field>Declaration_Text__c</field>
        <value xsi:type="xsd:string">&lt;p&gt;1. I declare that the information given in this form is complete, correct and accurate.&lt;/p&gt;
&lt;p&gt;
2. I declare my understanding that Dubai International Financial Centre Authority (“DIFCA”) may request more detailed information (including but not limited to, personal, educational, employment and financial information) should it be deemed necessary to adequately assess the fitness and propriety of the firm or any person connected to the firm. I consent to the Registrar of Companies (“ROC“) contacting any previous employers, educational institutions, professional organizations or any other organization, to verify any information contained in this form.&lt;/p&gt;

&lt;/p&gt;
3. I declare that the applicant and the individuals listed in the application are fit and proper to perform the proposed activities.&lt;/p&gt;

&lt;p&gt;
4. I declare my understanding that carrying on a Financial Service in or from the Dubai International Financial Centre (“DIFC”) without an appropriate License is prohibited under Article 41 of the Regulatory Law 2004 and that registration does not authorize the firm to carry on any Financial Services in or from the DIFC.&lt;/p&gt;

&lt;p&gt;
5. I confirm that the applicant will not be conducting any activities prohibited in DIFC, or UAE&lt;/P&gt;

&lt;p&gt;
6. I confirm that I have the authority to make this application, to declare as specified above and sign this form for, or on behalf of, the applicant. I also confirm that I have the authority to give the consent specified above.&lt;/p&gt;

&lt;p&gt;
7. I confirm that I have read and understood DIFC Laws, Federal Anti-Money Laundering Law, Counter Terrorist Financing Law, Criminalization of Money Laundering Law (Federal Law No.4 of 2002), Combating Terrorism Offences Law (Federal Law No.1 of 2004), and the Data Protection Law.&lt;/p&gt;

&lt;p&gt;
8. I confirm that the commercial permission requested will be bound by this application form.
&lt;/p&gt;

&lt;p&gt;
9.  For the purposes of the Data Protection Law, the personal data provided and related documents in this form will be processed by the ROC in accordance with Article 9 &amp; 10 of the Data Protection Law 2007. I hereby consent for all of the data contained in this application form and related documents to be processed by DIFCA. I understand and agree that by providing the information contained in this application, including but not limited to personal and sensitive personal data, DIFCA and any sub-processors that it appoints may process such information for the purposes described herein. Any other processing of personal or sensitive personal data that DIFCA collects is conducted in accordance with its Online Data Protection Policy.
&lt;/p&gt;

&lt;p&gt;
10. I understand that pursuant to Article 12 of the Companies Law No.5 of 2018, that the Registrar has the right to refuse any application without providing any reasons.
&lt;/p&gt;</value>
    </values>
    <values>
        <field>Field_API_Name__c</field>
        <value xsi:type="xsd:string">i_agree__c</value>
    </values>
    <values>
        <field>Object_API_Name__c</field>
        <value xsi:type="xsd:string">HexaBPM__Service_Request__c</value>
    </values>
    <values>
        <field>Record_Type_API_Name__c</field>
        <value xsi:nil="true"/>
    </values>
</CustomMetadata>
