<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Entry Permit is issued - Notification</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

We are pleased to inform you that the online visa of {!Step__c.Applicant_Name__c} – SR No # {!Step__c.SR__c} has been approved by GDRFA.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;Next Steps&lt;/b&gt; &lt;br/&gt;

Please &lt;a href = &quot;{!$Label.Community_URL}/apex/DocumentViewer?id={!Step__c.SRId__c}&quot;&gt; download the visa &lt;/a&gt; from the client portal. (Click the link under ‘Preview’ to open and print the visa.).&lt;br/&gt;&lt;br/&gt;

If the applicant is currently:&lt;br/&gt;

&lt;ul&gt;
&lt;li&gt; Outside the UAE: please send a copy of the visa to him/her before he/she enters the UAE. After he/she enters the UAE, upload a copy of the online visa with an airport entry stamp on the client portal.The applicant’s Emirates ID Registration and Medical Fitness Test can then be processed.
&lt;/li&gt;

&lt;li&gt; Inside the UAE: Applicant’s Emirates ID Registration, Change of status and Medical Fitness Test processes will be started. 
&lt;/li&gt;
&lt;li&gt;A notification will be sent to the portal user to submit the original passport after the medical test result is received from the Dubai Health Authority (DHA).  
&lt;/li&gt;

&lt;/ul&gt;



&lt;b&gt;Other points to note&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;

For applications inside the country; the applicant’s change of status will be processed as soon as the Emirates ID registration is completed in order to avoid any overstay fine. If you do not require an immediate change of status, please email gs.helpdesk@difc.ae. &lt;br/&gt;&lt;br/&gt;

After the change of status, the applicant will not be able to travel until the visa stamping process is completed.&lt;br/&gt;&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;&lt;br/&gt;&lt;br/&gt;

Please email us on gs.helpdesk@difc.ae or call us on +971 4 362 2391.&lt;br/&gt;&lt;br/&gt;
Our working hours are Sunday to Thursday 8:00AM to 3:00PM.&lt;br/&gt; &lt;br/&gt;
Thank you,
DIFC Government Services Office</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for  Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, 

This is to inform you that the Entry Permit for {!Step__c.Applicant_Name__c} has been issued and online visa can be downloaded from the client portal


Regards, 
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">GS_ALL</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Entry Permit is Issued</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CLOSED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Entry Permit is issued. SR # {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
