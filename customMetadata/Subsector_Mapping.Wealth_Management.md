<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Wealth Management</label>
    <protected>false</protected>
    <values>
        <field>Subsector_Values__c</field>
        <value xsi:type="xsd:string">Asset Management;Wealth Management;Private Equity;Fund Management;Private Banking;Fund Administration;Venture Capital;Trust Services;Multi Family Office</value>
    </values>
</CustomMetadata>
