<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OngoingFitOutProjects</label>
    <protected>false</protected>
    <values>
        <field>Display_Order__c</field>
        <value xsi:type="xsd:double">1.0</value>
    </values>
    <values>
        <field>Filter_Applicable__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Order_by__c</field>
        <value xsi:type="xsd:string">ORDER BY progress__c</value>
    </values>
    <values>
        <field>Related_Functionality__c</field>
        <value xsi:type="xsd:string">Management Report</value>
    </values>
    <values>
        <field>SOQL__c</field>
        <value xsi:type="xsd:string">Select id,Progress__c ,Target_Completion_Date__c,service_type__c,RecordType.Developername,Linked_SR__r.Linked_SR__c,Linked_SR__c,Location__c,Customer__r.Name,name,External_Status_Name__c,Submitted_Date__c,Type_of_Request__c,Site_Progress__c,Upgraded_Date__c,Property_Type__c,
(Select id,name,Closed_Date__c,Step_Name__c,createdDate,sr_Step__r.owner__c,Status__r.Name from Steps_SR__r where createdDate &lt;= LAST_N_DAYS : 30 ) 
from service_Request__c where recordTypeid=&apos;012200000003F01AAE&apos; and External_Status_Name__c !=&apos;Project Completed&apos; and External_Status_Name__c !=&apos;Project Cancelled&apos; and service_type__c =&apos;Fit - Out Service Request&apos;</value>
    </values>
    <values>
        <field>TestClass_SOQL__c</field>
        <value xsi:type="xsd:string">Select id,Progress__c ,service_type__c ,RecordType.Developername,Linked_SR__r.Linked_SR__c,Linked_SR__c,Location__c,Customer__r.Name,Linked_SR__r.Linked_SR__r.Linked_SR__c,name,External_Status_Name__c,Submitted_Date__c,Type_of_Request__c,Site_Progress__c,Upgraded_Date__c,Property_Type__c,
(Select id,name,Closed_Date__c,Step_Name__c,createdDate,sr_Step__r.owner__c,Status__r.Name  from Steps_SR__r)  from service_Request__c limit 10</value>
    </values>
    <values>
        <field>is_Used__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
