<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>pending- under Process</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt;&lt;br/&gt;

We are unable to complete the service request (SR# {!Step__c.SR__c}) for the reason given below:&lt;br/&gt;&lt;br/&gt;

&lt;html&gt; 
&lt;table height=&quot;400&quot; width=&quot;550&quot; border=&quot;1&quot;&gt; 
&lt;tr&gt; 
&lt;th&gt;Service Name&lt;/th&gt; 
&lt;th&gt;Name of the Applicant&lt;/th&gt; 
&lt;th&gt;Pending Items/Requirements&lt;/th&gt; 
&lt;/tr&gt; 
&lt;tr&gt; 
&lt;td&gt;{!Step__c.Service_Type__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.Applicant_Name__c}&lt;/td&gt; 
&lt;td&gt;{!Step__c.pending__r.comments__c}&lt;/td&gt; 
&lt;/tr&gt; 
&lt;/table&gt; 
&lt;/html&gt; 
&lt;br/&gt; &lt;br/&gt; 

&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;

Please log on to the client portal, and click on ‘Pending Actions’. You can then complete the necessary step(s) as follows:&lt;br/&gt;
–  If more information is required, please click ‘View’, enter the required information under ‘Customer Comments’ and then click ‘Save’.&lt;br/&gt;
–  If a document needs uploading or reloading, please upload the required document in the ‘Download/Upload Documents’ section. Once uploaded, please click ‘Step ID’, ‘Edit’ and enter a comment on ‘Customer Comments’ section. Then click ‘Save’.&lt;br/&gt;
–  If Government fine is to be paid, please pay through the ‘Government Fine Payment’ section.&lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, 

Your service request with the number {!Step__c.SR__c}  is pending due to missing requirements. Kindly log on to DIFC portal for more information. 


Regards, 
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">GS_ALL</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Under process</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">AWAITING_REVIEW</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Service Request is Pending notification. SR#  {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
