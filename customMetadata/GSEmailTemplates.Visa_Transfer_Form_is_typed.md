<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Visa Transfer Form is typed</label>
    <protected>false</protected>
    <values>
        <field>Email_Body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client,&lt;br/&gt; &lt;br/&gt;
 
We are pleased to inform you that the visa transfer for applicant {!Step__c.Applicant_Name__c} – SR # {!Step__c.SR__c} is now ready for signing by the current sponsor. &lt;br/&gt; &lt;br/&gt;


&lt;b&gt;Next Steps&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;

The current sponsor needs to sign the transfer.&lt;br/&gt; &lt;br/&gt;

If you selected the courier service, the transfer form will now be delivered to the consignee. S/he (or a company PRO) will need to show an Identity Card to receive the form. Please contact our courier service on 04-3622491 or courierservices@difc.ae to follow up.&lt;br/&gt; &lt;br/&gt;

If you did not select the courier service, please contact us to collect the form.&lt;br/&gt; &lt;br/&gt;

Once the sponsor has signed, please contact our agents on +971 4 362 2491 to arrange the form’s return. &lt;br/&gt; &lt;br/&gt;

&lt;b&gt;If you need further help&lt;/b&gt;&lt;br/&gt; &lt;br/&gt;</value>
    </values>
    <values>
        <field>SMS_Subject__c</field>
        <value xsi:type="xsd:string">Your request for Phone={!step__c.sr__r.Send_SMS_To_Mobile__c}</value>
    </values>
    <values>
        <field>SMS_body__c</field>
        <value xsi:type="xsd:string">Dear Valued Client, &lt;br/&gt; 

This is to inform you that visa transfer form for {!Step__c.Applicant_Name__c} is ready for sponsor signature. Kindly arrange collection from DIFC GSO. &lt;br/&gt; 


Regards, &lt;br/&gt; 
DIFC</value>
    </values>
    <values>
        <field>Service_Category__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Service_Types__c</field>
        <value xsi:type="xsd:string">Employment_Visa_Govt_to_DIFC</value>
    </values>
    <values>
        <field>Step_Name__c</field>
        <value xsi:type="xsd:string">Visa Transfer Form Issued</value>
    </values>
    <values>
        <field>Step_Status_Code__c</field>
        <value xsi:type="xsd:string">CLOSED</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Visa Transfer form is ready for signature. SR# {!Step__c.SR__c}</value>
    </values>
</CustomMetadata>
