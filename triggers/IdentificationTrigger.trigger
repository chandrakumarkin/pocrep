/******************************************************************************************
 *  DIFC - Identification Trigger
 
 *  Author   : Mudasdir Wani
 *  Date     : 2-March-2020                          
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        	Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0    2-March-2020 	Mudasir       Created                          
*******************************************************************************************/

trigger IdentificationTrigger on Identification__c (after insert, after update) {
    
    //Calling after insert operations
    if(Trigger.isInsert && Trigger.isAfter){
    	IdentificationTriggerHandler.afterInsertOperation(Trigger.newMap);
    } 
    
    //Calling after Update operations
    if(Trigger.isUpdate && Trigger.isAfter){
    	IdentificationTriggerHandler.afterUpdateOperation(Trigger.newMap , Trigger.oldMap);
    }
}