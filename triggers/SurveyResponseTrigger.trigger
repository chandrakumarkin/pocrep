trigger SurveyResponseTrigger on Survey_Response__c (before insert, after insert) {

    if(trigger.isBefore && trigger.isInsert){
        SurveyResponseTriggerHandler.assigntToSurvey(trigger.New);
    }
    
    if(trigger.isAfter && trigger.isInsert){
        SurveyResponseTriggerHandler.CreateCase(trigger.New);
    }
}