/*  
    Created By  :   Ravi
    Description :   This trigger will Send the call out to SAP to create the Partners & Relationships
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No        Date                Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------   
        
*           26th Feb,2015 -         Ravi            Added the logic to sync the Document Detail and Relationsship status
*           3rd May,2015  -         Ravi            Added the logic to update AccountId on contact if the Relationship is not acive for Sponsored Relationship
                                                    and remove the Sponsor field for Dependent Relations      
*           14 Sep, 2015  -         Shabbir         Added the logic to create portal user for marketing v1.3                       
*           01 May,2016   -         Kaavya-         Added the logic to deactivate sector lead by webservice  -- V1.4
*   V1.5    10/06/2016              Ravi            As per #3083 - Added the new logic to update the New Visa SR when the "Sponsored Employee" relationship got created for the contact to mark SR is not in pipeline for calculatting PSA
*   v1.6    Sept 8,2019             Mudasir         As per #7079 - Introduced new relationships - Contract Employee and Frequent Visitor  
*   v1.7    12-02-2020              Selva           Enabled the future validation as same as production 
*   v1.8    16-sept-2020            Mudasir         #DIFC2-10709 - Employee Relationship name to be renamed  
*    v1.8    14-feb-2020            shoaib           #DIFC2-11415 
*   v1.9    15-Mar-2020             Shikha          #DIFC-14093 - Corrected the code for Relationship condition
*/
trigger RelationshipCreationTrg on Relationship__c (before insert,after insert, after update) 
{
    if(Block_Trigger__c.getAll().get('Block') == null || ( Block_Trigger__c.getAll().get('Block') != null && Block_Trigger__c.getAll().get('Block').Block_Relationship_Trg__c == false)){
        
        list<string> lstRelationShipIds = new list<string>();   
        map<string,string> mapContactIds = new map<string,string>();
        map<string,string> mapAccountIds = new map<string,string>();
        
        list<string> lstRelationShipIdUpdates = new list<string>();
        map<string,string> mapRelationShipPartners = new map<string,string>();
        
        list<string> lstRorpRelIds = new list<string>();
        list<string> lstPartnerIds = new list<string>();
        list<string> lstBothPartnerIds = new list<string>();        
        //v1.9 - Corrected condition
        if(trigger.isAfter && GsHelperCls.AlreadyFired == false && system.isFuture() == false){ //v1.7   
            for(Relationship__c objRelationShip : trigger.new){

                if(objRelationShip.Relationship_Group__c != 'RORP'){
                    if(objRelationShip.Relationship_Type__c != 'Data Controller' && objRelationShip.Relationship_Type__c != 'Is Fit Out Contractor'){
                       
                        system.debug('@@###@@@@@'+objRelationShip.Partner_1__c +'((**&^^^))'+objRelationShip.Partner_2__c +'--objRelationShip.Push_to_SAP__c--'+objRelationShip.Push_to_SAP__c);
                        
                        if(objRelationShip.Partner_1__c != null && (objRelationShip.Partner_2__c == null || objRelationShip.Partner_2__c == '') && objRelationShip.Push_to_SAP__c == false){
                            system.debug('@@###@@@@@'+objRelationShip.Object_Contact__c);
                            
                            if(objRelationShip.Object_Contact__c != null){
                                mapContactIds.put(objRelationShip.Object_Contact__c,objRelationShip.Partner_1__c);
                            }

                            if(objRelationShip.Object_Account__c != null){
                                mapAccountIds.put(objRelationShip.Object_Account__c,objRelationShip.Partner_1__c);
                            }
                            lstRelationShipIds.add(objRelationShip.Id);
                        }
                        
                        else if(objRelationShip.Partner_1__c != null && objRelationShip.Partner_2__c != null && objRelationShip.Push_to_SAP__c == false && objRelationShip.Relationship_Type__c!='Has the Employee Responsible'){ // Added condition for Sector Lead as part of V1.4
                            lstRelationShipIdUpdates.add(objRelationShip.Id);

                            if(objRelationShip.Object_Contact__c != null){
                                mapRelationShipPartners.put(objRelationShip.Object_Contact__c,objRelationShip.Partner_2__c);
                            }

                            if(objRelationShip.Object_Account__c != null){
                                mapRelationShipPartners.put(objRelationShip.Object_Account__c,objRelationShip.Partner_2__c);
                            }
                        }
                        else if(objRelationShip.Partner_1__c != null && objRelationShip.Partner_2__c != null && objRelationShip.Relationship_Type__c=='Has the Employee Responsible'){
                            lstRelationShipIdUpdates.add(objRelationShip.Id);
                            if(objRelationShip.User__c != null){ mapRelationShipPartners.put(objRelationShip.User__c,objRelationShip.Partner_2__c); }                           
                        }
                     
                    }
                }else if(trigger.isInsert == false && objRelationShip.Push_to_SAP__c == false && objRelationShip.Relationship_Group__c == 'RORP' && objRelationShip.Subject_Account__c != null && (objRelationShip.Object_Contact__c != null || objRelationShip.Object_Account__c != null)){
                    if(objRelationShip.Partner_1__c != null && objRelationShip.Partner_2__c == null) lstPartnerIds.add(objRelationShip.Object_Contact__c);
                    if(objRelationShip.Partner_1__c == null && objRelationShip.Partner_2__c == null){ lstBothPartnerIds.add(objRelationShip.Subject_Account__c);lstBothPartnerIds.add(objRelationShip.Object_Account__c);lstBothPartnerIds.add(objRelationShip.Object_Contact__c); }
                    lstRorpRelIds.add(objRelationShip.Id);
                }
            }
            
            System.debug('SAPWebServiceDetails======================================lstRelationShipIds=>'+lstRelationShipIds);      
            System.debug('SAPWebServiceDetails======================================mapContactIds=>'+mapContactIds);        
            System.debug('SAPWebServiceDetails======================================mapAccountIds=>'+mapAccountIds);        
        
            if(!mapContactIds.isEmpty()){ SAPWebServiceDetails.CRMPartnerCall(lstRelationShipIds,mapContactIds,null);}

            if(!mapAccountIds.isEmpty()){ SAPWebServiceDetails.CRMPartnerCall(lstRelationShipIds,null,mapAccountIds);}

            system.debug('mapContactIds is : '+mapContactIds);
            system.debug('mapRelationShipPartners is : '+mapRelationShipPartners);
            system.debug('Context is : '+trigger.isInsert);
            system.debug('Context is : '+trigger.isUpdate);

            if(!mapRelationShipPartners.isEmpty()){
                SAPWebServiceDetails.CRMCreateRelationship(lstRelationShipIdUpdates,mapRelationShipPartners);
            }

            if(!lstRorpRelIds.isEmpty() && !system.isFuture() && !system.isBatch()){
                //GsRelationshipCreation.CreateContact(lstRorpRelIds);
                RorpSAPWebServiceDetails.RorpPartnerCreationFuture(lstPartnerIds, null, lstRorpRelIds);
            }

            if(!lstBothPartnerIds.isEmpty() && !system.isFuture() && !system.isBatch()){
                RorpSAPWebServiceDetails.RorpContactCreation(lstRorpRelIds, lstBothPartnerIds, null);
            }
            
            GsHelperCls.AlreadyFired = true;
        }
    }

    if(trigger.isInsert && trigger.isBefore){
        //system.assertEquals(null,trigger.isInsert);
        map<string,string> mapContactIds = new map<string,string>();
        map<string,string> mapContactIdTemp = new map<string,string>();
        for(Relationship__c objRel : trigger.new){
            
            if(objRel.Relationship_Type__c == 'Has DIFC Sponsored Employee' || objRel.Relationship_Type__c == 'ICD Employee' || objRel.Relationship_Type__c == 'DL Employee Card' ||  objRel.Relationship_Type__c == 'Equity Holder Card' || objRel.Relationship_Type__c == 'Has DIFC Seconded' || objRel.Relationship_Type__c == 'Has DIFC Local /GCC Employees' || objRel.Relationship_Type__c == 'Has Temporary Employee' || objRel.Relationship_Type__c == 'Has Frequent Visitor' || objRel.Relationship_Type__c == 'Has Contract Employee' || objRel.Relationship_Type__c == 'Has DIFC Seconded Sponsored Employee' || objRel.Relationship_Type__c == 'DIFC Seconded Sponsored Employee'|| objRel.Relationship_Type__c == 'Employee Card Non Sponsored'){
                mapContactIdTemp.put(objRel.Object_Contact__c,objRel.Subject_Account__c);
            }
        }

        system.debug('Mudasir mapContactIdTemp---'+mapContactIdTemp);

        if(!mapContactIdTemp.isEmpty()){
            for(Document_Details__c objDD : [select Id,EXPIRY_DATE__c,Document_Type__c,Contact__c from Document_Details__c where Contact__r.RecordType.DeveloperName = 'GS_Contact' AND Contact__c IN : mapContactIdTemp.keySet() AND (Document_Type__c = 'Employment Visa' OR Document_Type__c = 'ICD Employee Card'  OR Document_Type__c = 'Seconded Access Card' OR Document_Type__c = 'Employee Card Non Sponsored' OR  Document_Type__c = 'Local / GCC Smart Card' OR Document_Type__c = 'Temporary Work Permit' OR Document_Type__c = 'DL Employee Card') AND (EXPIRY_DATE__c = null OR EXPIRY_DATE__c > TODAY)]){
                mapContactIds.put(objDD.Contact__c,objDD.Document_Type__c);
            }
        }

        if(!mapContactIds.isEmpty()){
            for(Relationship__c objRel : trigger.new){
                system.debug('Mudaisr new relation ---'+objRel.Relationship_Type__c);
                
                if(objRel.Relationship_Type__c == 'Has DIFC Sponsored Employee'){
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'Employment Visa'){
                        objRel.Document_Active__c = true;
                    }
                }else if(objRel.Relationship_Type__c == 'Has DIFC Seconded'){
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'Seconded Access Card'){
                        objRel.Document_Active__c = true;
                    }
                }else if(objRel.Relationship_Type__c == 'Has DIFC Local /GCC Employees'){
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'Local / GCC Smart Card'){
                        objRel.Document_Active__c = true;
                    }
                }else if(objRel.Relationship_Type__c == 'Has Temporary Employee'){
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'Temporary Work Permit'){
                        objRel.Document_Active__c = true;
                    }
                    //Included new relationships - v1.6 - Start                   
                }else if(objRel.Relationship_Type__c == 'Has Frequent Visitor'){
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'Frequent Visitor'){
                        objRel.Document_Active__c = true;
                    }
                }else if(objRel.Relationship_Type__c == 'Has Contract Employee'){
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'Contract Employee'){
                        objRel.Document_Active__c = true;
                    }
                }else if(objRel.Relationship_Type__c == 'Has Intern'){
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'Has Intern'){
                        objRel.Document_Active__c = true;
                    }
                }
                else if(objRel.Relationship_Type__c == 'Has DIFC Seconded Sponsored Employee'){//v1.8
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'DIFC Seconded Sponsored Employee'){
                        objRel.Document_Active__c = true;
                    }
                }
            
                else if(objRel.Relationship_Type__c == 'ICD Employee'){
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'ICD Employee'){
                        objRel.Document_Active__c = true;
                    }
                 }
                else if(objRel.Relationship_Type__c == 'Employee Card Non Sponsored'){
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'Employee Card Non Sponsored'){
                        objRel.Document_Active__c = true;
                    }
                }
                else if(objRel.Relationship_Type__c == 'DL Employee Card'){
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'DL Employee Card'){
                        objRel.Document_Active__c = true;
                    }
                }
                else if(objRel.Relationship_Type__c == 'DIFC Seconded Sponsored Employee'){//1.8
                    if(mapContactIds.get(objRel.Object_Contact__c) == 'DIFC Seconded Sponsored Employee'){
                        objRel.Document_Active__c = true;
                    }
                }
            }
        }
    }
    
    //added by Ravi, for updating the Compliances
    if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
        list<Id> lstIds = new list<Id>();
        map<Id,Id> mapEmpIds = new map<Id,Id>();
        map<Id,Id> mapDependentIds = new map<Id,Id>();
        map<Id,Contact> mapContactsToUpdate = new map<Id,Contact>();
        Contact objContact;
        list<Log__c> lstLogs = new list<Log__c>();
        Log__c objLog;
        
        for(Relationship__c objRel : trigger.new){
            if(objRel.Relationship_Type__c == 'Has Principal User'){
                if(trigger.isInsert) lstIds.add(objRel.Subject_Account__c);
                else if(objRel.Active__c != trigger.oldMap.get(objRel.Id).Active__c){ lstIds.add(objRel.Subject_Account__c); }
            }else if(trigger.isUpdate && objRel.Object_Contact__c != null && objRel.Active__c == false && objRel.Active__c != trigger.oldMap.get(objRel.Id).Active__c){
                //added by Ravi on 3rd June - to update the Contact if the Relationship is not Active
                if(objRel.Relationship_Type__c == 'Has DIFC Sponsored Employee')
                    mapEmpIds.put(objRel.Object_Contact__c,null);
                else if(objRel.Relationship_Type__c == 'Family Has the Member' || objRel.Relationship_Type__c == 'Is Domestic Helper Of')
                    mapDependentIds.put(objRel.Object_Contact__c,null);
            }
        }
        if(!lstIds.isEmpty()){

            map<Id,list<Contact>> mapPortalContacts = new map<Id,list<Contact>>();
            for(Contact objCon : [select Id,AccountId from Contact where AccountId IN : lstIds AND RecordType.DeveloperName='Portal_User' AND Id IN (select ContactId from User where Contact.AccountId IN : lstIds AND IsActive=true AND Community_User_Role__c INCLUDES ('Company Services'))]){
                list<Contact> lst = mapPortalContacts.containsKey(objCon.AccountId) ? mapPortalContacts.get(objCon.AccountId) : new list<Contact>();
                lst.add(objCon);
                mapPortalContacts.put(objCon.AccountId,lst);        
            }

            list<Compliance__c> lstComps = new list<Compliance__c>();
            for(Compliance__c objComp : [select Id,Account__c,Account__r.Id,Account__r.Principal_User__c,Account__r.OwnerID,Account__r.Owner.ManagerID 
                 from Compliance__c where Status__c != 'Fulfilled' AND Account__c IN : lstIds]){

                Compliance__c objC = new Compliance__c(Id=objComp.Id);
                objC = RocCreateComplianceCls.PopulatePortalUsers(objComp.Account__r, objC,mapPortalContacts.get(objComp.Account__c));
                lstComps.add(objC);
            }

            if(!lstComps.isEmpty()) update lstComps;
        }
        if(!mapEmpIds.isEmpty() || !mapDependentIds.isEmpty()){
            for(Relationship__c objRel : [select Id,Object_Contact__c,Object_Contact__r.Name,Subject_Contact__c,Subject_Account__c,Subject_Account__r.Name,Relationship_Type__c from Relationship__c 
                                          where Active__c = true AND (Relationship_Type__c='Has DIFC Sponsored Employee' OR Relationship_Type__c = 'Family Has the Member' OR Relationship_Type__c='Is Domestic Helper Of') 
                                          AND (Object_Contact__c IN : mapEmpIds.keySet() OR Object_Contact__c IN : mapDependentIds.keySet() ) 
                                          AND (End_Date__c = null OR End_Date__c > TODAY )]){

                if(objRel.Relationship_Type__c == 'Has DIFC Sponsored Employee' && mapEmpIds.containsKey(objRel.Object_Contact__c)){
                    if(mapEmpIds.get(objRel.Object_Contact__c) != null){ objLog = new Log__c(); objLog.Type__c = 'Duplicate active relationships found'; objLog.Description__c = 'Duplicate active relationships found for Contact : '+objRel.Object_Contact__r.Name+' under Account : '+objRel.Subject_Account__r.Name+'\nContact Id : '+objRel.Object_Contact__c+'\nAccount Id : '+objRel.Subject_Account__c; lstLogs.add(objLog);}else{mapEmpIds.put(objRel.Object_Contact__c,objRel.Subject_Account__c);}
                    }else if((objRel.Relationship_Type__c == 'Family Has the Member' || objRel.Relationship_Type__c == 'Is Domestic Helper Of') && mapDependentIds.containsKey(objRel.Object_Contact__c)){mapDependentIds.remove(objRel.Object_Contact__c);}
            }
            
            if(!mapEmpIds.isEmpty()){
                for(Id ContactId : mapEmpIds.keySet()){
                    objContact = new Contact(Id=ContactId);
                    objContact.AccountId = mapEmpIds.get(ContactId);
                    mapContactsToUpdate.put(objContact.Id,objContact);
                }
            }
            if(!mapDependentIds.isEmpty()){
                for(Id ContactId : mapDependentIds.keySet()){
                    objContact = new Contact(Id=ContactId);
                    objContact.Sponsor__c = null;
                    mapContactsToUpdate.put(objContact.Id,objContact);
                }
            }
            if(!mapContactsToUpdate.isEmpty()){ update mapContactsToUpdate.values(); }
            if(!lstLogs.isEmpty()) insert lstLogs;
        }
    }
    
    //V1.5
    TriggerFactoryCls.createHandler(Relationship__c.sObjectType);
    
}