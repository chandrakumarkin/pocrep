trigger calculateDiffSLA on Ticket__c(before Update,before insert)
{    
    
    
    List<BusinessHours> BusinessHoursList = [SELECT Id FROM BusinessHours WHERE IsActive=true AND name=:Label.Ticket_SLA_Business_Hour limit 1];
    BusinessHours BusinessHoursId = (BusinessHoursList.size() >0)? BusinessHoursList[0]: new BusinessHours();
    system.debug('=====BusinessHoursId==========='+BusinessHoursId);
    for(Ticket__c tc : trigger.new){
        if(
            (   
                //tc.Completed_Date__c != null && tc.Created_On__c != null && 
                BusinessHoursId != null && BusinessHoursId.Id != null &&
                ( Trigger.isInsert ||
                    (  Trigger.isUpdate && 
                        (
                            tc.Completed_Date__c != trigger.Oldmap.get(tc.id).Completed_Date__c ||
                            tc.Created_On__c != trigger.Oldmap.get(tc.id).Created_On__c
                        )
                    )
                )
            )
        ){
            tc.Hours_Worked__c = null;
            tc.Is_Non_Business_Hour_SLA__c = false;
            if(tc.Completed_Date__c != null && tc.Created_On__c != null){
                Datetime TimeTosolved = tc.Completed_Date__c ;
                Datetime CreatedTime = tc.Created_On__c ; 
                system.debug('====TimeTosolved==========='+TimeTosolved);
                system.debug('=====CreatedTime==========='+CreatedTime);
                boolean isSameDay = CreatedTime.isSameDay(TimeTosolved);
                system.debug('=====isSameDay==========='+isSameDay);

                Decimal diff =0.00;
                Decimal diffHours = BusinessHours.diff(businessHoursId.id, CreatedTime, TimeTosolved);
                diff = diffHours/3600000;  // converting milliseconds into Hours
                system.debug('=====diffHours========'+diffHours);
                system.debug('=====diff==========='+diff);
                //if(isSameDay){              
                if(diff ==0.00 || diff==0){
                    tc.Is_Non_Business_Hour_SLA__c = true;
                    diff  = Integer.valueOf((TimeTosolved.getTime() - CreatedTime.getTime())/(1000*60*60));    
                }        
                //}
                tc.Hours_Worked__c = diff;
                system.debug('=====tc.Hours_Worked__c==========='+tc.Hours_Worked__c);
            }
        }
    }
}