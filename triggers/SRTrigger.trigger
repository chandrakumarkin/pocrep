/******************************************************************************************
 *  DIFC - Service Request Trigger
 
 *  Author   : Durga Prasad
 *  Company  : NSI JLT
 *  Date     : 2014-07-25                          
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    11-Mar-2015 Kaavya        Check if same SR Type is already in DRAFT or Submitted                            
 v1.2    12-Mar-2015 Ravi          Added the code to populate the Nationality Lookup from Natioality Picklist
 V1.3    15-Mar-2015 Saima         Added the SR field mapping for pre-population           
 V1.4    14-Apr-2015 Kaavya        External Status to Pending if active pending 
 V1.5    22-Apr-2015 Kaavya        Additional Charge for amendments
 V1.6    22-Aug-2015 Saima         Added method for RORP validations
 V1.7    26 Aug,2015 Ravi          Added the logic to not check the Open SR for BC, IT & RORP services
 V1.8    31/08/2015  Ravi          Added the logic to change the SR Price Items Status to Invoiced for RORP SRs
 V1.9    03/02/2016  Ravi          Changed the condition to check Open SR, Listing can be raised as many as tehy can
 V1.10   03/04/2016  Ravi          Added the condition to query the GS Pending Steps.
 V1.11   28/03/2016  Swati         Allow multiple submission of SR
 V1.12   04/04/2016  Swati         Allow history tracking of SR
 V1.13   19/04/2016  Claude        Added filter for copying amendments 
 V1.14    14/06/2016   Swati       Fine Process Automation
 V1.15   04/07/2016   Kaavya       Fix for Fitout first step due date      
 V1.16   26/07/2016   CLaude       Added Business Hour logic per #3132
 V1.17    18/08/2016   Swati       check fine at client level during exit process
 V1.18   23/01/2017   Swati        Added as per #3732
 V1.19   27/02/2017  Swati         Added as per #3898
 V2.00   02/04/2017  Claude        Added 'Miscellaneous Services' as part of the list of types to get the number of docs to upload (#3000)
 V2.1    14/05/2017  Sravan        Added code for changing the pricing line status from Added to blocked Tkt # 4042
*******************************************************************************************/
//BEF INSERT: if user selects an employee or document detail in the lookup field , then populate all the fields in SR
//QUERY record types and sr template and queuess
//
//
//
// 
trigger SRTrigger on Service_Request__c (before insert,after insert,before update,after update){
   
   Map<String,id> SRTmplteMap = new Map<String,id>();//Key SRTemplateRecordType API Name and Value as SR TemplateId
   
   map<string,Id> mapStepQueues = new map<String,Id>();//Key as Queue Name and value is Queue-Id
   map<Id,string> mapStepTemplateQueues = new map<Id,string>();//Key as Queue Id and value is Queue-Name
   map<Id,string> mapSRStepQueues = new map<Id,string>();//Key as Queue Id and value is Queue-Name
   map<Id,string> mapSRTemplateQueues = new map<Id,string>();//Key as Queue Id and value is Queue-Name
   
   map<string,string> mapLetterAvailability = new map<string,string>();
   
   if(Letter_Available_to_Download__c.getAll()!=null && Letter_Available_to_Download__c.getAll().size()>0){
        for(Letter_Available_to_Download__c obj:Letter_Available_to_Download__c.getAll().values()){
            mapLetterAvailability.put(obj.Request_RecordType_API_Name__c.toLowerCase(),obj.Request_RecordType_API_Name__c);
        }
   }

   /* commented by durga
   SRTriggerExecutionCls srTriggerCls = new SRTriggerExecutionCls();
   */
   
   map<id,string> mapSRRecType = new map<id,string>();//Key Service_Request RecordType Id and Value as Service_Request RecordType Name
   map<string,id> mapSRRecTypeDeveloperName = new map<string,id>();
   map<id,string> mapLeaseRecIdDevName = new map<id,string>();
    //Insert trigger to set the SR Template
    if(Trigger.isInsert){
        set<id> setSRTemplId_Insert = new set<id>();
        set<id> setSRIdsforDocs = new set<id>();
        set<id> docAvailableSet = new set<id>();
        for(RecordType RT:[select id,Name,DeveloperName,sObjectType from RecordType where sObjectType='Service_Request__c' OR sObjectType='Lease__c']){
            system.debug('RT===>'+RT);
            if(RT.sObjectType=='Service_Request__c'){
                mapSRRecType.put(RT.id,RT.DeveloperName.toLowerCase());
                mapSRRecTypeDeveloperName.put(RT.DeveloperName.toLowerCase(),RT.Id);
                if(RT.DeveloperName=='AMEND_AN_EMPLOYMENT_CONTRACT' || RT.DeveloperName=='New_Employment_Card_For_An_Employee_On_Father_or_Husband_Sponsorship'|| RT.DeveloperName=='New_Employment_Visa_V2' || RT.DeveloperName=='New_Company_Application' || RT.DeveloperName=='Renew_An_Employment_Card_For_An_Employee_On_Father_Husband_Sponsorship' || RT.DeveloperName=='Renew_An_Employee_Access_Card_For_An_Employee_of_a_Related_Company'){
                    docAvailableSet.add(RT.id);
                }
            }
            if(RT.sObjectType=='Lease__c'){
                mapLeaseRecIdDevName.put(RT.Id,RT.DeveloperName.toLowerCase());
            }
        }
        if(Trigger.isBefore){
            system.debug('mapSRRecType===>'+mapSRRecType);
            system.debug('mapSRRecTypeDeveloperName===>'+mapSRRecTypeDeveloperName);
            
            map<string,id> mapCECNo = new map<string,id>();
            map<string,string> mapCECNoContactId = new map<string,string>();
            map<string,string> mapCECNoDocDetailId = new map<string,string>();
   
            list<SR_Status__c> lstStatus = new list<SR_Status__c>();
            lstStatus = [select id,name from SR_Status__c where (Name='Draft' or Name='draft') and ID!=null];
            set<string> setNationality = new set<string>();
            set<string> setPassport = new set<string>();
            
            list<License__c> lstLicense_Application = new list<License__c>();
            
            //added by Ravi on 12th March
            map<string,string> mapNationalitys = new map<string,string>();
            
            /* Code for Copying the Contact and Document Details of the Selected Contact & Document in SR based on the Custom Setting */
                map<string,Manage_SR_Workflows__c> mapSRWorkflows = new map<string,Manage_SR_Workflows__c>();
                map<string,string> mapSRTemplateCodes = new map<string,string>();
                if(Manage_SR_Workflows__c.getAll()!=null){
                    mapSRWorkflows = Manage_SR_Workflows__c.getAll(); 
                    for(Manage_SR_Workflows__c CS:mapSRWorkflows.values()){
                        mapSRTemplateCodes.put(CS.SR_Template_Code__c,CS.SR_Template_Code__c);
                    }
                }
            /* End of Code for Copying the Contact and Document Details */
            
            /* properties for Copying the Account's parent Account Name for Apply_to_change_parent_company_name_domicile */
            set<id> setChildAccountIds = new set<id>();
            map<string,string> mapParentAccountNames = new map<string,string>();
            
            map<id,string> mapSRTemplateRecTypeAPIName = new map<id,string>();
            map<id,string> mapSRTemplate_Code_Id = new map<id,string>();
            map<string,string> mapSRTemplate_Menu = new map<string,string>();
            map<string,string> mapMenu_SRRecrdType = new map<string,string>();
            map<Id,string> mapSRGroup = new map<Id,string>();
            map<Id,boolean> mapSRSubmission = new map<Id,boolean>();
            
            if(mapSRRecType!=null && mapSRRecType.size()>0){
                for(SR_Template__c SRTemp: [SELECT id,Allow_Multiple_SR_Submission__c,SR_RecordType_API_Name__c, Menu__c,SR_Group__c from SR_Template__c where SR_RecordType_API_Name__c IN:mapSRRecType.values() and ID!=null]){
                    SRTmplteMap.put(SRTemp.SR_RecordType_API_Name__c.tolowercase(),SRTemp.id);
                    mapSRTemplateRecTypeAPIName.put(SRTemp.id,SRTemp.SR_RecordType_API_Name__c.tolowercase());
                    mapMenu_SRRecrdType.put(SRTemp.SR_RecordType_API_Name__c.tolowercase(),SRTemp.Menu__c);
                    mapSRTemplate_Menu.put(SRTemp.id,SRTemp.Menu__c);
                    if(SRTemp.SR_RecordType_API_Name__c!=null){
                        mapSRTemplate_Code_Id.put(SRTemp.Id,SRTemp.SR_RecordType_API_Name__c);
                    }
                    mapSRGroup.put(SRTemp.Id,SRTemp.SR_Group__c);
                    mapSRSubmission.put(SRTemp.Id,SRTemp.Allow_Multiple_SR_Submission__c);
                }
            }
            
            system.debug('mapSRTemplateRecTypeAPIName===>'+mapSRTemplateRecTypeAPIName);
            system.debug('SRTmplteMap===>'+SRTmplteMap);
            
            set<id> setConIds_to_be_copied = new set<id>();
            set<id> setDocIds_to_be_copied = new set<id>();
            
            set<id> setDocDetailsIds = new set<id>();
            set<id> setContactIds = new set<id>();
            set<id> setDMSLRelIDs = new set<id>();
            
            for(Service_Request__c ServReq : Trigger.new){
                
                //V1.1 Start                
                //Check Open SR validation
                String strRecordTypeName= mapSRRecType.get(ServReq.recordTypeId);
                string SRTemplateId= SRTmplteMap.get(strRecordTypeName);
                String menu= mapSRTemplate_Menu.get(SRTemplateId);
                
                /*//V1.7 & V1.9 added the condition
                String opensrchk= (mapSRGroup.get(SRTemplateId) != 'RORP' && mapSRGroup.get(SRTemplateId) != 'IT' && mapSRGroup.get(SRTemplateId) != 'BC' && mapSRGroup.get(SRTemplateId) != 'Listing' && mapSRGroup.get(SRTemplateId) != 'Fit-Out & Events') ? CC_Validations.CheckOpenSR('',SRTemplateId,strRecordTypeName,ServReq.Customer__c) : 'Success';
                if(opensrchk!='Success'&& strRecordTypeName!='User Access Form' && menu!= 'Employee Services')
                    ServReq.adderror(opensrchk);                
                //V1.1 End*/
                
                
                //V1.9
                if(mapSRSubmission!=null && mapSRSubmission.get(SRTemplateId)==false){
                    String opensrchk= CC_Validations.CheckOpenSR('',SRTemplateId,strRecordTypeName,ServReq.Customer__c);    
                    if(opensrchk!='Success'){
                        ServReq.adderror(opensrchk);   
                    } 
                }
                //End
                
                if(ServReq.Legal_Structures__c==null)
                    ServReq.Legal_Structures__c = ServReq.Account_Legal_Entity__c;
                    
                if(ServReq.Previous_Entity_Name__c!=null && ServReq.Previous_Trading_Name_1__c==null)
                    ServReq.Previous_Trading_Name_1__c = ServReq.Previous_Entity_Name__c;
                    
                if(ServReq.Pre_Entity_Name_Arabic__c!=null && ServReq.Pre_Trade_Name_1_in_Arab__c==null)
                    ServReq.Pre_Trade_Name_1_in_Arab__c = ServReq.Pre_Entity_Name_Arabic__c;
                    
                if(ServReq.Contact__c!=null){
                    setContactIds.add(ServReq.Contact__c);
                }
                if(ServReq.Document_Detail__c!=null){
                    setDocDetailsIds.add(ServReq.Document_Detail__c);
                }
                if(docAvailableSet.contains(ServReq.RecordTypeId)){
                    //until letter is apprvoed, then prevew link will not be available 
                    ServReq.Letter_Available_for_Customer__c = TRUE; 
                }
               // new change for copying the relationship's contact's values to sr 
               /* commented by durga
                if (ServReq.Person_Name_DMSSL_del__c!=null){
                    setDMSLRelIDs.add(ServReq.Person_Name_DMSSL_del__c);
                }*/
                
                //added by ravi on 27th April
                if(mapSRGroup.containsKey(SRTemplateId))
                    ServReq.SR_Group__c = mapSRGroup.get(SRTemplateId);
                else if(strRecordTypeName == 'DM_SR')
                    ServReq.SR_Group__c = 'ROC';
            }
       
            system.debug('setDocDetailsIds===>'+setDocDetailsIds);
            
            map<id,relationship__c> mapDMSLRels = new map<id,relationship__c>();
            /* commented by durga
            if(setDMSLRelIDs!=null && setDMSLRelIDs.size()>0){
                for (relationship__c dmsrel:[select id, object_contact__c from relationship__c where id in :setDMSLRelIDs]){
                    mapDMSLRels.put(dmsrel.id, dmsrel);
                } 
            }
            */
            map<id,Document_Details__c> mapDocDetails = new map<id,Document_Details__c>();
            if(setDocDetailsIds!=null && setDocDetailsIds.size()>0){
                for(Document_Details__c Ddetails:[select id,Contact__c,Account__c,Contact__r.AccountId from Document_Details__c where ID IN:setDocDetailsIds and Contact__c!=null]){
                    mapDocDetails.put(Ddetails.id,Ddetails);
                }
            }
            system.debug('mapDocDetails===>'+mapDocDetails);
            
            for(Service_Request__c ServReq : Trigger.new){
                if(ServReq.Contact__c==null && ServReq.Document_Detail__c!=null && mapDocDetails.get(ServReq.Document_Detail__c)!=null){
                    if(ServReq.Customer__c!=null && mapDocDetails.get(ServReq.Document_Detail__c).Contact__r.AccountId==ServReq.Customer__c){
                        ServReq.Contact__c = mapDocDetails.get(ServReq.Document_Detail__c).Contact__c;
                    }
                }
                /* commented by durga
                if(ServReq.Contact__c==null && ServReq.Customer__c!=null && ServReq.Passport_No__c!=null && ServReq.Nationality_lookup__c!=null && ServReq.LastName__c!=null){
                    setNationality.add(ServReq.Nationality_lookup__c);
                    setPassport.add(ServReq.Passport_No__c);
                }
                */
                /* Assigning the SR Template based on the RecordType of the SR */
                if(ServReq.SR_Template__c!=null){ 
                    //in case of sub sr or linked sr , sr template id will be available , based on that we will fill rec type id 
                    system.debug('mapSRTemplateRecTypeAPIName==>'+mapSRTemplateRecTypeAPIName);
                    system.debug('mapSRRecTypeDeveloperName==>'+mapSRRecTypeDeveloperName);
                    if(mapSRTemplateRecTypeAPIName.get(ServReq.SR_Template__c)!=null && mapSRRecTypeDeveloperName.get(mapSRTemplateRecTypeAPIName.get(ServReq.SR_Template__c))!=null){
                        ServReq.RecordTypeId = mapSRRecTypeDeveloperName.get(mapSRTemplateRecTypeAPIName.get(ServReq.SR_Template__c));
                    }
                    if(mapSRTemplate_Menu.get(ServReq.SR_Template__c)!=null  && ServReq.Ultimate_SR_Menu_Text__c!=null){
                        system.debug(' mapSRTemplate_Menu ');
                    }else{
                      if(mapSRTemplate_Menu.get(ServReq.SR_Template__c)!= null)
                          ServReq.Ultimate_SR_Menu_Text__c = mapSRTemplate_Menu.get(ServReq.SR_Template__c);
                    }
                }else{
                    // inc ase of main sr rec type id will be availeb, we wil fill the sr template 
                    string RecTypeName = mapSRRecType.get(ServReq.RecordTypeId);
                    system.debug('RecTypeName==>'+RecTypeName);
                    if(SRTmplteMap.get(RecTypeName)!=null){
                        ServReq.SR_Template__c=SRTmplteMap.get(RecTypeName);
                    }
                    if(mapMenu_SRRecrdType.get(RecTypeName)!=null && ServReq.Ultimate_SR_Menu_Text__c!=null){
                        system.debug(' mapMenu_SRRecrdType ');
                    }else{
                      if(mapMenu_SRRecrdType.get(RecTypeName)!= null)
                          ServReq.Ultimate_SR_Menu_Text__c=mapMenu_SRRecrdType.get(RecTypeName);
                    }
                }
                
                /* Start code for holding the account ids to copy the SR Customer's parent company name */
                if(ServReq.Customer__c!=null && ServReq.RecordTypeId!=null && mapSRRecType.get(ServReq.RecordTypeId)!=null && mapSRRecType.get(ServReq.RecordTypeId)=='apply_to_change_parent_company_name_domicile'){
                    setChildAccountIds.add(ServReq.Customer__c);
                }
                /* End code for holding the account ids to copy the SR Customer's parent company name */
                
                if(ServReq.SR_Template__c!=null && mapSRTemplateCodes!=null && mapSRTemplateCodes.size()>0){
                    if(ServReq.Contact__c!=null){
                        setConIds_to_be_copied.add(ServReq.Contact__c);
                    }
                    if(ServReq.Document_Detail__c!=null){
                        setDocIds_to_be_copied.add(ServReq.Document_Detail__c);
                    }
                    //v1.8
                    
                }
                
                //Adhoc_debug(' Update dmsl ' + mapSRRecType.get(ServReq.RecordTypeId));
                if((mapSRRecType.get(ServReq.RecordTypeId)=='Update_DMSSL_Information') || (mapSRRecType.get(ServReq.RecordTypeId)=='Update_DMSSL_Person_Details_DIFC_Internal')){  
                    //Adhoc_debug(' Update dmsl person ' + ServReq.Person_Name_DMSSL_del__c + ' / ' + ServReq.Person_Name_DMSSL_del__r.object_contact__c);
                    /* commented by durga
                    if (ServReq.Person_Name_DMSSL_del__c!=null && mapDMSLRels.containskey(ServReq.Person_Name_DMSSL_del__c) ){
                        relationship__c rels = mapDMSLRels.get(ServReq.Person_Name_DMSSL_del__c);
                        //[select object_contact__c from relationship__c where id = :ServReq.Person_Name_DMSSL_del__c limit 1];
                       // Adhoc_debug(' Update dmsl contact id  ' + rels.object_contact__c);
                        if (rels.Object_Contact__c != null ){
                            setConIds_to_be_copied.add(rels.id);
                            if((mapSRRecType.get(ServReq.RecordTypeId)=='Update_DMSSL_Information'))
                                    ServReq.Contact__c= rels.object_contact__c;
                        }
                    }*/
                }
                    //Adhoc_debug('setConIds_to_be_copied==>'+setConIds_to_be_copied);
                  //Adhoc_debug('setConIds_to_be_copied.size==>'+setConIds_to_be_copied.size());
                //end Update_DMSSL_Information
            }
           //1.6
            map<id,Relationship__c> mapRelation_to_be_Copied = new map<id,Relationship__c>();
            /* commented by durga
            if(setConIds_to_be_copied!=null && setConIds_to_be_copied.size()>0){
                for(Relationship__c rel:[Select r.Relationship_Type__c, r.Relationship_Code__c, r.Object_Contact__r.Passport_No__c, 
                                                r.Object_Contact__r.Passport_Name__c, r.Object_Contact__r.Name,
                                                r.Object_Contact__r.FirstName, r.Object_Contact__r.LastName,
                                                r.Object_Contact__r.Title,
                                                r.Object_Contact__r.Gender__c,
                                                r.Object_Contact__r.Middle_Names__c,
                                                r.Object_Contact__r.Email,
                                                r.Object_Contact__r.MobilePhone,
                                                r.Object_Contact__r.Nationality_lookup__c,
                                                r.Object_Contact__r.Birthdate,
                                                r.Object_Contact__r.Place_of_Birth__c,
                                                r.Object_Contact__r.COUNTRY_OF_BIRTH__c,
                                                r.Object_Contact__r.Date_of_Issue__c,
                                                r.Object_Contact__r.Passport_Expiry_Date__c,
                                                r.Object_Contact__r.Place_of_Issue__c,
                                                r.Object_Contact__r.Issued_Country__c,
                                                r.Object_Contact__r.First_Name_Arabic__c,
                                                r.Object_Contact__r.Middle_Name_Arabic__c,
                                                r.Object_Contact__r.Last_Name_Arabic__c,
                                                r.Object_Contact__r.Passport_Name_Arabic__c,
                                                r.Object_Contact__r.Mother_s_Name_Arabic__c,
                                                r.Object_Contact__r.Place_of_birth_Arabic__c,
                                                r.Object_Contact__r.ADDRESS_LINE1__c,
                                                r.Object_Contact__r.ADDRESS_LINE2__c,
                                                r.Object_Contact__r.City_Res__c,
                                                r.Object_Contact__r.COUNTRY_ENGLISH1__c,
                                                r.Object_Contact__r.POBOX__c, 
                                                r.Object_Contact__r.Address_Line_1_HC__c,
                                                r.Object_Contact__r.Address_Line_2_HC__c,
                                                r.Object_Contact__r.CITY1__c,
                                                r.Object_Contact__r.Country__c,
                                                r.Object_Contact__r.PO_Box_HC__c,
                                                r.Ter_Date__c,
                                                r.Position__c,
                                                r.Appointment_Date__c,
                                                r.Power__c,
                                                r.Object_Contact__c, r.Object_Contact_Detail__c
                                         From Relationship__c r WHERE  id IN:setConIds_to_be_copied AND Active__c = TRUE]){
                                            
                      mapRelation_to_be_Copied.put(rel.id,rel);
                       //Adhoc_debug('object_contact__c ==>'+rel.object_contact__c);
                        //Adhoc_debug('Passport No==>'+rel.Object_Contact__r.Passport_No__c);
                        //Adhoc_debug('Passport Name==>'+rel.Object_Contact__r.Name);
                }
            }
            */
             //Adhoc_debug('mapRelation_to_be_Copied.size==>'+mapRelation_to_be_Copied.size());
             for(Service_Request__c SReq:Trigger.new){
                if ( mapSRRecType.get(SReq.RecordTypeId)== 'Update_DMSSL_Person_Details_DIFC_Internal'){
                     if(mapRelation_to_be_Copied!=null && mapRelation_to_be_Copied.size()>0){
                            //SReq.Change_Contact_Details_Flag__c  = true; 
                            //Adhoc_debug('SReq.Change_Contact_Details_Flag__c  '+SReq.Change_Contact_Details_Flag__c );
                            
                       
                            //  Adhoc_debug('BBBBB mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c) '+mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c));
                           /*   
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null) {
                               if(mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_No__c != null){
                                    Adhoc_debug('Got a Passport_No__c '+ mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_No__c);
                               }else{
                                    Adhoc_debug('Sub If Else Passport_No__c NO DATA');
                               }            
                            }else{
                                    Adhoc_debug('Main ELSE Passport_No__c NO DATA');
                            }
                            */
                            
                            /* commented by durga
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_No__c != null) {
                                SReq.Passport_No__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_No__c;
                                SReq.Changed_Passport_No__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_No__c;
                                
                             }
                            
                           if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_Name__c != null) {
                            SReq.Passport_Name__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_Name__c;
                            SReq.Changed_Name_as_per_passport__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_Name__c;
                            //  Adhoc_debug('Passport_Name__c '+ SReq.Passport_Name__c);
                            //  Adhoc_debug('Changed_Name_as_per_passport__c '+ SReq.Changed_Name_as_per_passport__c);
                          }
                             
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.FirstName != null) {
                                SReq.First_Name__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.FirstName;
                                SReq.Changed_First_Name__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.FirstName;
                             }
                            
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.LastName != null) {
                                SReq.LastName__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.LastName;
                                SReq.Changed_Last_Name__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.LastName;
                             }
                           
                         if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Gender__c != null) {
                            SReq.Gender__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Gender__c;
                            SReq.Changed_Gender__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Gender__c;
                          }
                          
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Title != null) {
                                SReq.Title__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Title;
                                SReq.Changed_Title__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Title;
                             } 
                           
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Middle_Names__c != null) {
                                SReq.Middle_Name__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Middle_Names__c;
                                SReq.Changed_Middle_Name__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Middle_Names__c;
                             } 
                           
                           if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Email != null) {
                                SReq.Person_Email_ID__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Email;
                                SReq.Changed_Applicant_s_Email_ID__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Email;
                             } 
                             
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.MobilePhone != null) {
                                SReq.Person_Mobile_No__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.MobilePhone;
                                SReq.Changed_Applicant_s_Mobile_No__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.MobilePhone;
                             }  
                          
                            // 
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Nationality_lookup__c != null) {
                                SReq.Nationality_lookup__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Nationality_lookup__c;
                                SReq.Changed_Nationality_Country__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Nationality_lookup__c;
                             }  
                          
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_Birth__c != null) {
                                SReq.Place_of_Birth__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_Birth__c;
                                SReq.Changed_Country_of_Birth__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_Birth__c;
                             }  //COUNTRY_OF_BIRTH__c
                            
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Birthdate != null) {
                                SReq.Date_of_Birth__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Birthdate;
                                SReq.Changed_Date_of_Birth__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Birthdate;
                             }  
                            
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_Birth__c != null) {
                                SReq.Birth_Place__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_Birth__c;
                                SReq.Changed_Place_of_Birth__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_Birth__c;
                             }  
                           
                            
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Date_of_Issue__c != null) {
                                SReq.Date_of_Issue__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Date_of_Issue__c;
                                SReq.Changed_Passport_Issue_Date__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Date_of_Issue__c;
                             } 
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_Expiry_Date__c != null) {
                                SReq.Passport_Expiry_Date__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_Expiry_Date__c;
                                SReq.Changed_Passport_Expiry_Date__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_Expiry_Date__c;
                             } 
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_Issue__c != null) {
                                SReq.Place_of_Issue__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_Issue__c;
                                SReq.Changed_Place_of_Issue__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_Issue__c;
                             }
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Issued_Country__c != null) {
                                SReq.Country_Of_Issue__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Issued_Country__c;
                                SReq.Changed_Country_of_Issue__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Issued_Country__c;
                             }  
                             //
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.First_Name_Arabic__c != null) {
                                SReq.First_Name_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.First_Name_Arabic__c;
                                SReq.Changed_First_Name_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.First_Name_Arabic__c;
                             }
                              if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Middle_Name_Arabic__c != null) {
                                SReq.Middle_Name_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Middle_Name_Arabic__c;
                                SReq.Changed_Middle_Name_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Middle_Name_Arabic__c;
                             }
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Last_Name_Arabic__c != null) {
                                SReq.Last_Name_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Last_Name_Arabic__c;
                                SReq.Changed_Last_Name_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Last_Name_Arabic__c;
                             } 
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_Name_Arabic__c != null) {
                                  SReq.Passport_Name_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_Name_Arabic__c;
                                  SReq.Changed_Name_as_per_Passport_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Passport_Name_Arabic__c;
                               }  
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Mother_s_Name_Arabic__c != null) {
                                SReq.Mother_name_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Mother_s_Name_Arabic__c;
                                SReq.Changed_Mother_name_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Mother_s_Name_Arabic__c;
                             }
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_birth_Arabic__c != null) {
                                SReq.Place_of_birth_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_birth_Arabic__c;
                                SReq.Changed_Place_of_birth_Arabic__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Place_of_birth_Arabic__c;
                             } 
                             // 
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.ADDRESS_LINE1__c != null) {
                                SReq.U_A_E_Address__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.ADDRESS_LINE1__c;
                                SReq.Changed_U_A_E_Address__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.ADDRESS_LINE1__c;
                             }
                           
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.ADDRESS_LINE2__c != null) {
                                SReq.U_A_E_Address_2__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.ADDRESS_LINE2__c;
                                SReq.Changed_U_A_E_Address_2__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.ADDRESS_LINE2__c;
                             }
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.City_Res__c != null) {
                                SReq.U_A_E_Address_City__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.City_Res__c;
                                SReq.Changed_U_A_E_Address_City__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.City_Res__c;
                             }
                            
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.POBOX__c != null) {
                                SReq.U_A_E_Address_PO_Box__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.POBOX__c;
                                SReq.Changed_U_A_E_Address_PO_Box__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.POBOX__c;
                             }
                            
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Appointment_Date__c != null) {
                                SReq.Appointment_Date__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Appointment_Date__c;
                                SReq.Changed_Appointment_Date__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Appointment_Date__c;
                             }
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Power__c != null) {
                                SReq.Powers__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Power__c;
                                SReq.Changed_Powers__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Power__c;
                             }
                             
                              if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Ter_Date__c != null) {
                                SReq.Termination_Date__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Ter_Date__c;
                                SReq.Changed_Termination_Date__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Ter_Date__c;
                             }
                             
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Address_Line_1_HC__c != null) {
                                SReq.Permanent_Native_Address_1__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Address_Line_1_HC__c;
                                SReq.Changed_Permanent_Native_Address_1__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Address_Line_1_HC__c;
                             }
                             
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Address_Line_2_HC__c != null) {
                                SReq.Permanent_Native_Address_2__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Address_Line_2_HC__c;
                                SReq.Changed_Permanent_Native_Address_2__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Address_Line_2_HC__c;
                             }
                            if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.CITY1__c != null) {
                                SReq.Permanent_Native_City__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.CITY1__c;
                                SReq.Changed_Permanent_Native_City__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.CITY1__c;
                             }
                             if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Country__c != null) {
                                SReq.Permanent_Native_Country__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Country__c;
                                SReq.Changed_Permanent_Native_Country__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.Country__c;
                             }
                              if(SReq.Person_Name_DMSSL_del__c!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c)!=null && mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.PO_Box_HC__c != null) {
                                SReq.Permanent_Native_POBox_Postal_Zip_Code__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.PO_Box_HC__c;
                                SReq.Changed_Permanent_Native_POBox__c = mapRelation_to_be_Copied.get(SReq.Person_Name_DMSSL_del__c).Object_Contact__r.PO_Box_HC__c;
                             }
                                                  
                            */
                     }  
                }   
             }
            //1.6 End here
            //v1.8
            if(MapCECNo!=null && mapCECNo.size()>0) {
                for(document_details__c docdetail:[select contact__c,id,card_number__c,Document_Card_Number__c,account__c,document_number__c from document_details__c where Document_Type__c = 'CEC Number' and  Document_Number__c in  :MapCECNo.keyset()   limit 10 ] ){
                    if(docdetail.contact__c!=null){
                        mapCECNoContactId.put(docdetail.document_number__c,docdetail.contact__c);
                        mapCECNoDocDetailId.put(docdetail.document_number__c,docdetail.Id);
                   }
                }         
               if( mapCECNoDocDetailId.size() == null ||  mapCECNoDocDetailId.size() == 0) {
                
               }
            }
            
            for(Service_Request__c objSerReqCEC:Trigger.new){
                /* commented by durga
                if(objSerReqCEC.RecordTypeId!=null && mapSRRecType.get(objSerReqCEC.RecordTypeId)=='transfer_an_employee_between_two_difc_companies'){
                    if(objSerReqCEC.CEC_Number__c!=null && mapCECNoContactId.get(objSerReqCEC.CEC_Number__c)!=null){
                        objSerReqCEC.Contact__c = mapCECNoContactId.get(objSerReqCEC.CEC_Number__c);
                        setConIds_to_be_copied.add(objSerReqCEC.Contact__c);
                    } else if (objSerReqCEC.CEC_Number__c!=null && mapCECNoContactId.get(objSerReqCEC.CEC_Number__c)==null){
                        objSerReqCEC.CEC_Number__c.addError('Invalid CEC Number, Please enter a valid CEC Number-6543210');
                    }
                    if(objSerReqCEC.CEC_Number__c!=null && mapCECNoDocDetailId.get(objSerReqCEC.CEC_Number__c)!=null){
                        objSerReqCEC.Document_Detail__c = mapCECNoDocDetailId.get(objSerReqCEC.CEC_Number__c);
                        setDocIds_to_be_copied.add(objSerReqCEC.Document_Detail__c);
                    }
                }*/
            }
            
            
            if(setChildAccountIds!=null && setChildAccountIds.size()>0){
                /* commented by durga
                mapParentAccountNames = srTriggerCls.fetchCustomer_Parent_Company(setChildAccountIds);
                */
            }
            
            map<id,Contact> mapContacts_to_be_Copied = new map<id,Contact>();
            map<id,Document_Details__c> mapDocDetails_to_be_Copied = new map<id,Document_Details__c>();
            
            if(setConIds_to_be_copied!=null && setConIds_to_be_copied.size()>0){
                /* commented by durga
                for(Contact objCon:[select id,Name,Passport_No__c,Passport_Name__c,ABSCONDEE_STATUS__c,ABS_CONF_DATE__c,Employment_Status__c,Fathers_Name__c,Mothers_Name__c,Middle_Names__c,FirstName,LastName,
                Nationality_lookup__c,Place_of_Birth__c,COUNTRY_OF_BIRTH__c,Place_of_Issue__c,Issued_Country__c,Date_of_Issue__c,Passport_Expiry_Date__c,Birthdate,Date_of_Birth_Father__c,Date_of_Birth_Mother__c,
                Join_Date__c,Email,MobilePhone,Gender__c,RELIGION__c,Title,Qualification__c,Marital_Status__c,Spoken_Language1__c,Accomodation_Amount__c,Accomodation__c,Accomodation_Type__c,FOOD__c,FOOD_AMOUNT__c,
                Transport_Amount__c,Transport__c,Executive_Flag_Status__c,Other_Allowance__c,BAN_END_DATE__c,BAN_FROM_DATE__c,Job_Title__c,Address_Line_1_HC__c,Address_Line_2_HC__c,CITY1__c,Country__c,PO_Box_HC__c,
                ADDRESS_LINE1__c,ADDRESS_LINE2__c,City_Res__c,COUNTRY_ENGLISH1__c,POBOX__c,APPLICATION_STATUS_DATE__c,APPLICATION_STATUS__c,REASON__c,Relationship__c,Sponsor_Name__c,Sponsor_s_Passport_No__c,Issue_Date_Sponsor__c,
                CARD_EXPIRY_DATE__c,First_Language__c,CARD_ISSUE_DATE__c,CARD_NUMBER__c,In_UAE__c,Expiry_Period__c , Day_of_salary_transfers__c , Employment_Contract_Type__c, Commencement_Date__c, Probation_Period_in_months__c, 
                Annual_Leave_Days__c, End_Date__c , Termination_Notice_Period__c , Payment_method__c,Employee_Code_of_Conduct__c,Restricted_Area__c,Non_Competition_agreement__c,Non_Competition_Period__c, 
                Annual_Leave_Days_new__c , no_of_days__c,Employment_Contract__c,accountid,account.name from Contact where Id IN:setConIds_to_be_copied
                ]){
                    mapContacts_to_be_Copied.put(objCon.id,objCon);
                }
                */
            }
            
            
            if(setDocIds_to_be_copied!=null && setDocIds_to_be_copied.size()>0){
                for(Document_Details__c objDD:[select Id,Name,DOCUMENT_NUMBER__c,Date_Inactivated__c,Document_Card_Number__c,Account__c,CANCEL_DATE__c,CANCEL_EXIT_DATE__c,Card_Number__c,Contact__c,COUNT__c,
                     DOCUMENT_STATUS__c,DOCUMENT_TYPE__c,EXPIRY_DATE__c,EXT_COUNT__c,ISSUE_DATE__c,MULTIPLE_EXPIRY_DATE__c,Document_Extended_upto_date__c,
                     Document_Issue_Purpose__c,Document_Urgency__c,Document_Validity__c,In_UAE__c,Change_of_visa_status_inside_U_A_E__c,Date_of_Arrival__c,Arrival_Port__c,
                     Country_of_Destination__c,Visa_Duration_Stay__c,Document_Picture_Change_Required__c,Schengen_Multiple_Flag__c,DNRD_Application_No__c,DNRD_Application_Date__c,
                     DNRD_Application_Status__c,Name_of_sponsoring_Company__c,Transfer_from_Company__c,Sponsorship_Reason__c,Sponsor_Relationship__c,Sponsor_Contact_Name__c,
                     Sponsor_Passport_No__c,Sponsor_Passport_Nationality__c,Sponsor_Passport_Issue_Date__c,Sponsor_Passport_Expiry_Date__c,Sponsor_Residence_Permit_No__c,
                     Sponsor_Residence_Permit_Expiry_Date__c,Sponsor_Is_GCC_National__c,Sponsor_Contact_No__c,Sponsor_Address__c,Sponsor_Address_2__c,Sponsor_Emirate__c,
                     Sponsor_Address_City__c,Sponsor_Address_Country__c,Sponsor_PO_Box_Post_Code__c,Join_Date__c,Job_Title__c,Qualification__c,Marital_Status__c,First_Language__c,
                     Accommodation_Type__c,Accommodation_provided__c,Accommodation_allowance__c,Food_Provided__c,Food_Allowance__c,Transport_Provided__c,Transport_allowance__c,
                     Executive_Status__c,Other_allowance__c,Medical_Priority__c,Medical_Fitness__c,Other_Monthly_Allowance__c,Sponsoring_Company_Contact_Name__c,
                     Passport_No__c,Passport_Name__c,Passport_Expiry_Date__c,Place_of_Issue__c,Country_Of_Issue__c,Date_of_Issue__c,Nationality_lookup__c From Document_Details__c d where Id IN:setDocIds_to_be_copied
                     ]){
                        mapDocDetails_to_be_Copied.put(objDD.id,objDD);
                }
            }
            for(Service_Request__c SReq:Trigger.new){
                /* commented by durga
                if(SReq.Customer__c!=null && mapParentAccountNames!=null && mapParentAccountNames.size()>0 && mapParentAccountNames.get(SReq.Customer__c)!=null){
                    SReq.Parent_Company__c = mapParentAccountNames.get(SReq.Customer__c);
                }
                */
                //v1.4 and 1.9 for Request_a_Salary_Certificate_Addressed_to_Consulate_Or_Embassy_English

                if ( (mapSRRecType.get(SReq.RecordTypeId)== 'DIFC_Approval_to_Obtain_Police_Letter_For_Lost_Documents_Arabic' || mapSRRecType.get(SReq.RecordTypeId)=='Request_a_Salary_Certificate_Addressed_to_Consulate_Or_Embassy_English') && SReq.Document_Detail__c!=null){
                        /* commented by durga
                        if(SReq.Document_Issue_Date__c==null){
                            SReq.Document_Issue_Date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).ISSUE_DATE__c;
                        }
                        if(SReq.Document_Number__c==null){
                            SReq.Document_Number_New__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Document_number__c;
                        }
                        */
                }
            //v1.4
                if(SReq.SR_Template__c!=null && mapSRTemplate_Code_Id.get(SReq.SR_Template__c)!=null && mapSRTemplateCodes.get(mapSRTemplate_Code_Id.get(SReq.SR_Template__c))!=null){
                    
                    if(SReq.Contact__c!=null && mapContacts_to_be_Copied!=null && mapContacts_to_be_Copied.size()>0 && mapContacts_to_be_Copied.get(SReq.Contact__c)!=null){
                        /* commented by durga
                        if(SReq.Employment_Status__c==null){
                            SReq.Employment_Status__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Employment_Status__c;
                        }
                        if(SReq.ABSCONDEE_STATUS__c==null){
                            SReq.ABSCONDEE_STATUS__c = mapContacts_to_be_Copied.get(SReq.Contact__c).ABSCONDEE_STATUS__c;
                        }
                        if(SReq.Abs_Conf_Date__c==null){ 
                            SReq.Abs_Conf_Date__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Abs_Conf_Date__c;
                        }
                        if(SReq.Passport_No__c==null){
                            SReq.Passport_No__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Passport_No__c;
                        }
                        if(SReq.Passport_Name__c==null){
                            SReq.Passport_Name__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Passport_Name__c;
                        }
                        if(SReq.Fathers_Name__c==null){
                            SReq.Fathers_Name__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Fathers_Name__c;
                        }
                        if(SReq.Mothers_Name__c==null){
                            SReq.Mothers_Name__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Mothers_Name__c;
                        }
                        if(SReq.Middle_Name__c==null){
                            SReq.Middle_Name__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Middle_Names__c;
                        }
                        if(SReq.First_Name__c==null){
                            SReq.First_Name__c = mapContacts_to_be_Copied.get(SReq.Contact__c).FirstName;
                        }
                        if(SReq.LastName__c==null){
                            SReq.LastName__c = mapContacts_to_be_Copied.get(SReq.Contact__c).LastName;
                        }
                        if(SReq.Nationality_lookup__c==null){
                            SReq.Nationality_lookup__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Nationality_lookup__c;
                        }
                        if(SReq.Birth_Place__c==null){
                            SReq.Birth_Place__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Place_of_Birth__c;
                        }
                        if(SReq.Place_of_Birth__c==null){
                            SReq.Place_of_Birth__c = mapContacts_to_be_Copied.get(SReq.Contact__c).COUNTRY_OF_BIRTH__c;
                        }
                        if(SReq.Place_of_Issue__c==null){
                            SReq.Place_of_Issue__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Place_of_Issue__c;
                        }       
                        if(SReq.Country_Of_Issue__c==null){
                            SReq.Country_Of_Issue__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Issued_Country__c;
                        }               
                        if(SReq.Date_of_Issue__c==null){
                            SReq.Date_of_Issue__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Date_of_Issue__c;
                        }
                        if(SReq.Passport_Expiry_Date__c==null){
                            SReq.Passport_Expiry_Date__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Passport_Expiry_Date__c;
                        }
                        if(SReq.Date_of_Birth__c==null){
                            SReq.Date_of_Birth__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Birthdate;
                        }
                        
                        if(SReq.Person_Email_ID__c==null){
                            SReq.Person_Email_ID__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Email;
                        }
                        if(SReq.Person_Mobile_No__c==null){
                            if(mapContacts_to_be_Copied.get(SReq.Contact__c).MobilePhone!=null && mapContacts_to_be_Copied.get(SReq.Contact__c).MobilePhone.isNumeric()){
                                SReq.Person_Mobile_No__c = mapContacts_to_be_Copied.get(SReq.Contact__c).MobilePhone;
                            }
                        }
                        if(SReq.Gender__c==null){
                            SReq.Gender__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Gender__c;
                        }
                        if(SReq.Religion__c==null){
                            SReq.Religion__c = mapContacts_to_be_Copied.get(SReq.Contact__c).RELIGION__c;
                        }
                        if(SReq.Title__c==null){
                            SReq.Title__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Title;
                        }
                        if(SReq.Marital_Status__c==null){
                            SReq.Marital_Status__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Marital_Status__c;
                        }
                        if(SReq.First_Language1__c==null){
                            SReq.First_Language1__c = mapContacts_to_be_Copied.get(SReq.Contact__c).First_Language__c;
                        } 

                        if(SReq.Qualification__c==null){
                            SReq.Qualification__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Qualification__c;
                        }
                        if(SReq.Marital_Status__c==null){
                            SReq.Marital_Status__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Marital_Status__c;
                        }
                        if(SReq.First_Language__c==null){
                            SReq.First_Language__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Spoken_Language1__c;
                        } 
                        //v1.1
                        if ( mapSRRecType.get(SReq.RecordTypeId)!= 'transfer_an_employee_between_two_difc_companies'){
                            if(SReq.Accommodation_allowance__c==null){
                                SReq.Accommodation_allowance__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Accomodation_Amount__c;
                            }
                            if(SReq.Accommodation_provided__c==null){
                                SReq.Accommodation_provided__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Accomodation__c;
                            }
                            if(SReq.Accomodation_Type__c==null){
                                SReq.Accomodation_Type__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Accomodation_Type__c;
                            }
                            if(SReq.Food_Provided__c==null){
                                SReq.Food_Provided__c = mapContacts_to_be_Copied.get(SReq.Contact__c).FOOD__c;
                            }
                        
                            if(SReq.Food_Allowance__c==null){
                                //SReq.Food_Allowance__c = Decimal.valueOf(mapContacts_to_be_Copied.get(SReq.Contact__c).FOOD_AMOUNT__c);
                                    SReq.Food_Allowance__c = mapContacts_to_be_Copied.get(SReq.Contact__c).FOOD_AMOUNT__c;
                                
                            }
                            
                            if(SReq.Transport_allowance__c==null){
                                SReq.Transport_allowance__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Transport_Amount__c;
                            }
                            if(SReq.Transport_Provided__c==null){
                                SReq.Transport_Provided__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Transport__c;
                            }
                            if(SReq.Executive_Status__c==null){
                                SReq.Executive_Status__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Executive_Flag_Status__c;
                            }
                            if(SReq.Other_allowance__c==null){
                                SReq.Other_allowance__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Other_Allowance__c;
                            }                           
                        }

                        if(SReq.Employment_Contract_Type__c==null && SReq.Employment_Contract_Type__c==''){
                            SReq.Employment_Contract_Type__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Employment_Contract_Type__c;
                        }
                        if(SReq.Commencement_Date__c==null){
                            SReq.Commencement_Date__c =system.today();
                        }
                        if(SReq.Probation_Period_in_months__c==null){
                            SReq.Probation_Period_in_months__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Probation_Period_in_months__c;
                        }           
                        if(SReq.Day_of_salary_transfers__c==null){
                            SReq.Day_of_salary_transfers__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Day_of_salary_transfers__c;
                        }
                        if(SReq.Termination_Notice_Period__c==null){
                            SReq.Termination_Notice_Period__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Termination_Notice_Period__c;
                        }               
                        if(SReq.Payment_method__c==null){
                            SReq.Payment_method__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Payment_method__c;
                        }               
                        if(SReq.Annual_Leave_Days_Calendar__c==null){
                            SReq.No_of_Days__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Annual_Leave_Days__c;
                        }       
                        if(SReq.Non_Competition_agreement__c==null || SReq.Non_Competition_agreement__c != mapContacts_to_be_Copied.get(SReq.Contact__c).Non_Competition_agreement__c ){
                            SReq.Non_Competition_agreement__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Non_Competition_agreement__c;
                        }
                        //system.debug('SReq.Non_Competition_agreement__c ' + SReq.Non_Competition_agreement__c);
                        //system.debug(' mapContacts_to_be_Copied.get(SReq.Contact__c).Non_Competition_agreement__c ' + mapContacts_to_be_Copied.get(SReq.Contact__c).Non_Competition_agreement__c);
                        if(SReq.Period__c==null){
                            SReq.Period__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Non_Competition_Period__c;
                        }   
                        if(SReq.Restricted_Area__c==null){
                            SReq.Restricted_Area__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Restricted_Area__c;
                        }                                   
                        if(SReq.Employee_Code_of_Conduct__c==null){
                            SReq.Employee_Code_of_Conduct__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Employee_Code_of_Conduct__c;
                        }
                        if(SReq.No_of_Days__c==null){
                            SReq.No_of_Days__c = mapContacts_to_be_Copied.get(SReq.Contact__c).No_of_Days__c;
                        }                                                   
                        if(SReq.Annual_Leave_Days_Calendar__c==null){
                            SReq.Annual_Leave_Days_Calendar__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Annual_Leave_Days_new__c;
                        }                                                   
                                
                       //v1.1 ends
                        if(SReq.Person_Internal_Ban_to_Date__c==null){
                            SReq.Person_Internal_Ban_to_Date__c = mapContacts_to_be_Copied.get(SReq.Contact__c).BAN_END_DATE__c;
                        }
                        if(SReq.Person_Internal_Ban_From_Date__c==null){
                            SReq.Person_Internal_Ban_From_Date__c = mapContacts_to_be_Copied.get(SReq.Contact__c).BAN_FROM_DATE__c;
                        }
                        if(SReq.Job_Title__c==null){
                            SReq.Job_Title__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Job_Title__c;
                        }
                        if(SReq.Permanent_Native_Address_1__c==null){
                            SReq.Permanent_Native_Address_1__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Address_Line_1_HC__c;
                        }
                        if(SReq.Permanent_Native_Address_2__c==null){
                            SReq.Permanent_Native_Address_2__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Address_Line_2_HC__c;
                        }
                        if(SReq.Permanent_Native_City__c==null){
                            SReq.Permanent_Native_City__c = mapContacts_to_be_Copied.get(SReq.Contact__c).CITY1__c;
                        }
                        if(SReq.Permanent_Native_Country__c==null){
                            SReq.Permanent_Native_Country__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Country__c;
                        }
                        if(SReq.Permanent_Native_POBox_Postal_Zip_Code__c==null){
                            
                            if(mapContacts_to_be_Copied.get(SReq.Contact__c).PO_Box_HC__c!=null && mapContacts_to_be_Copied.get(SReq.Contact__c).PO_Box_HC__c.isNumeric()){
                                SReq.Permanent_Native_POBox_Postal_Zip_Code__c = mapContacts_to_be_Copied.get(SReq.Contact__c).PO_Box_HC__c;
                            }
                            //SReq.Permanent_Native_POBox_Postal_Zip_Code__c = mapContacts_to_be_Copied.get(SReq.Contact__c).PO_Box_HC__c;
                        }
                        if(SReq.U_A_E_Address__c==null){
                            SReq.U_A_E_Address__c = mapContacts_to_be_Copied.get(SReq.Contact__c).ADDRESS_LINE1__c;
                        }
                        if(SReq.U_A_E_Address_2__c==null){
                            SReq.U_A_E_Address_2__c = mapContacts_to_be_Copied.get(SReq.Contact__c).ADDRESS_LINE2__c;
                        }
                        if(SReq.U_A_E_Address_City__c==null){
                            SReq.U_A_E_Address_City__c = mapContacts_to_be_Copied.get(SReq.Contact__c).City_Res__c;
                        }
                        if(SReq.U_A_E_Address_Emirates__c==null){
                            SReq.U_A_E_Address_Emirates__c = mapContacts_to_be_Copied.get(SReq.Contact__c).COUNTRY_ENGLISH1__c;
                        }
                        if(SReq.U_A_E_Address_PO_Box__c==null){
                            if(mapContacts_to_be_Copied.get(SReq.Contact__c).POBOX__c!=null && mapContacts_to_be_Copied.get(SReq.Contact__c).POBOX__c.isNumeric()){
                                SReq.U_A_E_Address_PO_Box__c = mapContacts_to_be_Copied.get(SReq.Contact__c).POBOX__c;
                            }
                            //SReq.U_A_E_Address_PO_Box__c = mapContacts_to_be_Copied.get(SReq.Contact__c).POBOX__c;
                        }   
                        if(SReq.Employment_Contract__c==null){
                            SReq.Employment_Contract__c = mapContacts_to_be_Copied.get(SReq.Contact__c).Employment_Contract__c;
                        }
                        if(SReq.End_Date__c==null){
                            //if(SReq.Employment_Contract_Type__c=='Limited')
                                //SReq.End_Date__c = system.today()+365;
                        }   
                        system.debug(' transfer to company ' + mapContacts_to_be_Copied.get(SReq.Contact__c).accountid);
                        if ( mapSRRecType.get(SReq.RecordTypeId)== 'transfer_an_employee_between_two_difc_companies'){
                            if(SReq.Transfer_from_Company_Name__c==null){
                                SReq.Transfer_from_Company__c = mapContacts_to_be_Copied.get(SReq.Contact__c).accountid;
                                SReq.Transfer_from_Company_Name__c = mapContacts_to_be_Copied.get(SReq.Contact__c).account.name;
                            }                           
                        }            
                        */
                    } 
                     if(SReq.Document_Detail__c!=null && mapDocDetails_to_be_Copied!=null && mapDocDetails_to_be_Copied.size()>0 && mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c)!=null){
                        /* commented by durga
                        if(SReq.Document_Exit_Date__c==null){
                            SReq.Document_Exit_Date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).CANCEL_EXIT_DATE__c;
                        }
                    
                        if(SReq.Document_Type__c==null){
                            SReq.Document_Type__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).DOCUMENT_TYPE__c;
                        }
                        if(SReq.Document_Extended_upto_date__c==null){
                            SReq.Document_Extended_upto_date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Document_Extended_upto_date__c;
                        }
                        if(SReq.Document_Number_New__c==null){
                            SReq.Document_Number_New__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).DOCUMENT_NUMBER__c;
                        }
                        if(SReq.Document_Cancel_Date__c==null){
                            SReq.Document_Cancel_Date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).CANCEL_DATE__c;
                        }
                
                      if(SReq.Record_Type_Name__c!='Renew_An_Employee_Residence_Permit_Visa'){  //v1.2 - if != is added.
                          if(SReq.Document_Expiry_Date__c==null){
                              SReq.Document_Expiry_Date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).EXPIRY_DATE__c;
                          }
                          if(SReq.Document_Issue_Date__c==null){
                              SReq.Document_Issue_Date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).ISSUE_DATE__c;
                          }
                       }  
                       
                        if(SReq.Document_Issue_Purpose__c==null){
                            SReq.Document_Issue_Purpose__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Document_Issue_Purpose__c;
                        }
                        if(SReq.Is_this_person_in_U_A_E_now__c==null){
                            SReq.Is_this_person_in_U_A_E_now__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).In_UAE__c;
                        }   
                        if(SReq.Document_Validity__c==null){
                            SReq.Document_Validity__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Document_Validity__c;
                        }
                        if(SReq.Document_Urgency__c==null){
                            SReq.Document_Urgency__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Document_Urgency__c;
                        }
                        if(SReq.Change_of_visa_status_inside_U_A_E__c==null){
                            SReq.Change_of_visa_status_inside_U_A_E__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Change_of_visa_status_inside_U_A_E__c;
                        }
                        if(SReq.Date_of_Arrival__c==null){
                            SReq.Date_of_Arrival__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Date_of_Arrival__c;
                        }
                        if(SReq.Arrival_Port__c==null){
                            SReq.Arrival_Port__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Arrival_Port__c;
                        }
                        if(SReq.Country_of_Destination__c==null){
                            SReq.Country_of_Destination__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Country_of_Destination__c;
                        }
                        if(SReq.Visa_Duration_Stay__c==null){
                            SReq.Visa_Duration_Stay__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Visa_Duration_Stay__c;
                        }
                        if(SReq.Document_Picture_Change_Required__c==null){
                            SReq.Document_Picture_Change_Required__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Document_Picture_Change_Required__c;
                        }
                        if(SReq.Schengen_Multiple_Flag__c==null){
                            SReq.Schengen_Multiple_Flag__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Schengen_Multiple_Flag__c;
                        }  
                        if(SReq.DNRD_Application_Date__c==null){//Shireesh asked to commented this on 21-mar-2014 for Entry Permit Renewal 
                            //SReq.DNRD_Application_Date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).DNRD_Application_Date__c;
                        }
                        if(SReq.DNRD_Application_No__c==null){
                            //SReq.DNRD_Application_No__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).DNRD_Application_No__c;
                        }
                        if(SReq.DNRD_Application_Status__c==null && mapSRRecType.get(SReq.RecordTypeId)!='employee_residence_permit_visa_in_a_new_passport_replacement_or_new'){
                            SReq.DNRD_Application_Status__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).DNRD_Application_Status__c;
                        }   
                        if(SReq.Name_of_sponsoring_Company__c==null){
                            SReq.Name_of_sponsoring_Company__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Name_of_sponsoring_Company__c;
                        }
                        if(SReq.Sponsoring_Company_Contact_Name__c==null){
                            SReq.Sponsoring_Company_Contact_Name__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsoring_Company_Contact_Name__c;
                        }
                        if(SReq.Transfer_from_Company__c==null){
                            SReq.Transfer_from_Company__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Transfer_from_Company__c;
                        }
                        if(SReq.Sponsorship_Reason__c==null){
                            SReq.Sponsorship_Reason__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsorship_Reason__c;
                        }     
                        if(SReq.Sponsor_Relationship__c==null){
                            SReq.Sponsor_Relationship__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Relationship__c;
                        }
                        if(SReq.Sponsor_Contact_Name__c==null){
                            SReq.Sponsor_Contact_Name__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Contact_Name__c;
                        }
                        if(SReq.Sponsor_Passport_No__c==null){
                            SReq.Sponsor_Passport_No__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Passport_No__c;
                        }
                        if(SReq.Sponsor_Passport_Nationality__c==null){
                            SReq.Sponsor_Passport_Nationality__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Passport_Nationality__c;
                        }
                        if(SReq.Sponsor_Passport_Issue_Date__c==null){
                            SReq.Sponsor_Passport_Issue_Date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Passport_Issue_Date__c;
                        }
                        if(SReq.Sponsor_Passport_Expiry_Date__c==null){
                            SReq.Sponsor_Passport_Expiry_Date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Passport_Expiry_Date__c;
                        }   
                        if(SReq.Sponsor_Residence_Permit_No__c==null){
                            SReq.Sponsor_Residence_Permit_No__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Residence_Permit_No__c;
                        }
                        if(SReq.Sponsor_Residence_Permit_Expiry_Date__c==null){
                            SReq.Sponsor_Residence_Permit_Expiry_Date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Residence_Permit_Expiry_Date__c;
                        }
                        if(SReq.Sponsor_Is_GCC_National__c==null){
                            SReq.Sponsor_Is_GCC_National__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Is_GCC_National__c;
                        }
                        if(SReq.Sponsor_Contact_No__c==null){
                            SReq.Sponsor_Contact_No__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Contact_No__c;
                        }     
                        if(SReq.Sponsor_Address__c==null){
                            SReq.Sponsor_Address__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Address__c;
                        }
                        if(SReq.Sponsor_Address_2__c==null){
                            SReq.Sponsor_Address_2__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Address_2__c;
                        }
                        if(SReq.Emirate__c==null){
                            SReq.Emirate__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Emirate__c;
                        }
                        if(SReq.Sponsor_Address_City__c==null){
                            SReq.Sponsor_Address_City__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Address_City__c;
                        }
                        if(SReq.Sponsor_Address_Country__c==null){
                            SReq.Sponsor_Address_Country__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_Address_Country__c;
                        }
                        if(SReq.Sponsor_PO_Box_Post_Code__c==null){
                            if(mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_PO_Box_Post_Code__c!=null && mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_PO_Box_Post_Code__c.isNumeric()){
                                SReq.Sponsor_PO_Box_Post_Code__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Sponsor_PO_Box_Post_Code__c;
                            }
                        }
                        if(SReq.Join_Date__c==null){
                            SReq.Join_Date__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Join_Date__c;
                        }
                        if(SReq.Job_Title__c==null){
                            SReq.Job_Title__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Job_Title__c;
                        }
                        if(SReq.Qualification__c==null){
                            SReq.Qualification__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Qualification__c;
                        }
                        if (mapSRRecType.get(SReq.RecordTypeId)!='transfer_an_employee_between_two_difc_companies'){
                            if(SReq.Accommodation_allowance__c==null){
                                SReq.Accommodation_allowance__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Accommodation_allowance__c;
                            }
                            if(SReq.Accommodation_provided__c==null){
                                SReq.Accommodation_provided__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Accommodation_provided__c;
                            }
                            if(SReq.Accomodation_Type__c==null){
                                SReq.Accomodation_Type__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Accommodation_Type__c;
                            }
                            if(SReq.Food_Allowance__c==null){
                                SReq.Food_Allowance__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Food_Allowance__c;
                            }
                            if(SReq.Food_Provided__c==null){
                                SReq.Food_Provided__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Food_Provided__c;
                            }
                            if(SReq.Transport_allowance__c==null){
                                SReq.Transport_allowance__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Transport_allowance__c;
                            }
                            if(SReq.Transport_Provided__c==null){
                                SReq.Transport_Provided__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Transport_Provided__c;
                            }
                            if(SReq.Other_allowance__c==null){
                                SReq.Other_allowance__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Other_allowance__c;
                            }
                            if(SReq.Other_allowance__c==null){
                                SReq.Other_allowance__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Other_allowance__c;
                                                        
                            }
                            if(SReq.Executive_Status__c==null){
                                SReq.Executive_Status__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Executive_Status__c;
                            }
                        }
                        if(SReq.Medical_Priority__c==null){
                            SReq.Medical_Priority__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Medical_Priority__c;
                        }
                        if(SReq.Other_Monthly_Allowance__c==null){
                            SReq.Other_Monthly_Allowance__c = mapDocDetails_to_be_Copied.get(SReq.Document_Detail__c).Other_Monthly_Allowance__c;
                        }
                        ////v1.1 starts
                        if(SReq.Commencement_Date__c==null){
                            SReq.Commencement_Date__c =system.today();
                        }
                        if(SReq.End_Date__c==null){
                            //SReq.End_Date__c =system.today()+365;
                        }       
                        //v1.1 ends             
                        */
                    }   
                    if(mapSRRecType.get(SReq.RecordTypeId)== 'transfer_an_employee_between_two_difc_companies' ||
                       mapSRRecType.get(SReq.RecordTypeId)== 'Update_DMSSL_Information'
                      ){
                        SReq.Document_Detail__c = null;
                        if(mapSRRecType.get(SReq.RecordTypeId)== 'Update_DMSSL_Information'){
                          SReq.Contact__c = null;
                        }
                    }               
                }
                /* Checking whether the Contact Lookup is filled or not and Assigning the Contact if it is not filled */
                
                /*
                if(SReq.Customer__c!=null && SReq.Passport_No__c!=null && SReq.Nationality_lookup__c!=null && SReq.LastName__c!=null && mapExistingCon.get(SReq.Passport_No__c+'_'+SReq.Nationality_lookup__c)!=null){
                    if(SReq.Contact__c==null && mapExistingCon.get(SReq.Passport_No__c+'_'+SReq.Nationality_lookup__c).AccountId==SReq.Customer__c){
                        SReq.Contact__c = mapExistingCon.get(SReq.Passport_No__c+'_'+SReq.Nationality_lookup__c).id;
                    }
                    if(SReq.Sys_Contact__c==null){
                        SReq.Sys_Contact__c = mapExistingCon.get(SReq.Passport_No__c+'_'+SReq.Nationality_lookup__c).id;
                    }
                }
                */
                
                
                /* Converting the Passport Number to UpperCase */
                /* commented by durga
                if(SReq.Passport_No__c!=null && SReq.Passport_No__c!=''){
                    SReq.Passport_No__c = SReq.Passport_No__c.toUpperCase();
                }
                */
                /* Assigning the Status for the SR as Draft */
                if(lstStatus!=null && lstStatus.size()>0){
                    SReq.Internal_SR_Status__c = lstStatus[0].id;
                    SReq.External_SR_Status__c = lstStatus[0].id;
                }
                
                system.debug('SR.RecordTypeId==>'+SReq.RecordTypeId);
            }
            
            map<id,Contact> MapCont = new map<id,Contact>();
            /* commented by durga
            for(Contact objCon:[Select id,Job_Title__c,Accomodation__c,Accomodation_Type__c,Other_Allowance__c,Transport_Amount__c,Accomodation_Amount__c from Contact where Id IN:setContactIds]){
                MapCont.put(objCon.id,objCon);
            }
            */
            for(Service_Request__c SReq:trigger.new){
                if(SReq.Contact__c==null && SReq.Document_Detail__c!=null && mapDocDetails.get(SReq.Document_Detail__c)!=null){
                    SReq.Contact__c = mapDocDetails.get(SReq.Document_Detail__c).Contact__c;
                }
                if(SReq.RecordTypeId!=null && mapSRRecType.get(SReq.RecordTypeId)=='amend_an_employment_contract'){//AMEND_AN_EMPLOYMENT_CONTRACT
                    if(SReq.Contact__c==null){
                        SReq.Contact__c.addError('Contact is Required for this Service Request.');
                    }else if(SReq.Contact__c!=null && MapCont.get(SReq.Contact__c)!=null){
                        /* commented by durga
                        Service_Request__c RevisedSR = srTriggerCls.Amend_Employment_Contract(SReq,MapCont.get(SReq.Contact__c));
                        if(RevisedSR.Job_Title__c!=null){
                            SReq.Job_Title__c = RevisedSR.Job_Title__c;
                        }
                        if(RevisedSR.Accommodation_provided__c!=null){
                            SReq.Accommodation_provided__c = RevisedSR.Accommodation_provided__c;
                        }
                        if(RevisedSR.Accomodation_Type__c!=null){
                            SReq.Accomodation_Type__c = RevisedSR.Accomodation_Type__c;
                        }
                        if(RevisedSR.Accommodation_allowance__c!=null){
                            SReq.Accommodation_allowance__c = RevisedSR.Accommodation_allowance__c;
                        }
                        if(RevisedSR.Other_Allowance__c!=null){
                            SReq.Other_Allowance__c = RevisedSR.Other_Allowance__c;
                        }
                        if(RevisedSR.Transport_allowance__c!=null){
                            SReq.Transport_allowance__c = RevisedSR.Transport_allowance__c;
                        }
                        */
                    }
                }
                /* commented by durga
                if(SReq.Passport_No__c!=null && SReq.Passport_No__c.isAlphaNumeric()==false){
                    SReq.Passport_No__c.addError('Invalid Passport Number. please make sure that Special characters are not allowed.');
                }
                */
            }
            
            //added by Ravi on 19th April to populate the flexidesk flag
            list<Id> lstCustomerIds = new list<Id>();
            map<Id,list<string>> mapCustomerLeaseFunctionTypes = new map<Id,list<string>>();
            for(Service_Request__c objSR : trigger.new)
                lstCustomerIds.add(objSR.Customer__c);
            /* commented by durga
            for(Tenancy__c objTenancy : [select Id,Name,Lease__c,Lease__r.Customer__c,Location__c,Location__r.Function_Type__c,Lease__r.Lease_Status__c,Lease__r.Status__c from Tenancy__c where Lease__r.Customer__c IN : lstCustomerIds AND Lease__r.Lease_Status__c = 'Final' AND Lease__r.Status__c = 'Active' AND Location__r.Active__c = true AND (Tenancy_Termination_Date__c = null OR Tenancy_Termination_Date__c >: system.today() )]){
                if(objTenancy.Lease__r.Customer__c != null){
                    list<string> lst = (mapCustomerLeaseFunctionTypes.containsKey(objTenancy.Lease__r.Customer__c))?mapCustomerLeaseFunctionTypes.get(objTenancy.Lease__r.Customer__c):(new list<string>());
                    lst.add(objTenancy.Location__r.Function_Type__c);
                    mapCustomerLeaseFunctionTypes.put(objTenancy.Lease__r.Customer__c,lst);
                }
            }
            */
            for(Service_Request__c objSR : trigger.new){
                if(objSR.RecordTypeId!=null && mapSRRecType.get(objSR.RecordTypeId)!=null && mapLetterAvailability.get(mapSRRecType.get(objSR.RecordTypeId))!=null){
                    objSR.Letter_Available_for_Customer__c = true;
                }
                if(!mapCustomerLeaseFunctionTypes.isEmpty() && mapCustomerLeaseFunctionTypes.containsKey(objSR.Customer__c)){
                    Boolean isFlexiDesk = true;
                    for(string FunctionType : mapCustomerLeaseFunctionTypes.get(objSR.Customer__c)){
                        if(FunctionType != null && FunctionType.contains('Flexi Desk') == false){
                            isFlexiDesk = false;
                            break;
                        }
                    }
                    /* commented by durga
                    objSR.IsFlexiDeskCustomer__c = isFlexiDesk;*/
                }
            }
            
            //added by Ravi on 12th March
            for(Service_Request__c objSR : trigger.new){
                if(objSR.Nationality_list__c != null && objSR.Record_Type_Name__c != 'Consulate_or_Embassy_Letter'){
                    mapNationalitys.put(objSR.Nationality_list__c,null);
                }else if(objSR.Record_Type_Name__c == 'Consulate_or_Embassy_Letter' && objSR.Name_of_the_Country_GS__c != null){
                    mapNationalitys.put(objSR.Name_of_the_Country_GS__c,null);
                }
            
                //added by Saima on 15th March
                if(objSR.ApplicantId_hidden__c!=null && objSR.ApplicantId_hidden__c!='')
                    objSR.Contact__c = objSR.ApplicantId_hidden__c;
                if(objSR.SponsorId_hidden__c!=null && objSR.SponsorId_hidden__c!='')
                    objSR.Sponsor__c = objSR.SponsorId_hidden__c;
                if(objSR.Docdetail_hidden__c!=null && objSR.Docdetail_hidden__c!='')
                    objSR.Document_Detail__c = objSR.Docdetail_hidden__c;
            }
            if(!mapNationalitys.isEmpty()){
                for(Lookup__c objLp : [select Id,Name from Lookup__c where Type__c='Nationality' AND Name IN : mapNationalitys.keySet()]){
                    mapNationalitys.put(objLp.Name,objLp.Id);
                }
                for(Service_Request__c objSR : trigger.new){
                    if(objSR.Record_Type_Name__c != 'Consulate_or_Embassy_Letter' && objSR.Nationality_list__c != null && mapNationalitys.get(objSR.Nationality_list__c) != null){
                        objSR.Nationality__c = mapNationalitys.get(objSR.Nationality_list__c);
                    }else if(objSR.Record_Type_Name__c == 'Consulate_or_Embassy_Letter' && objSR.Name_of_the_Country_GS__c != null && mapNationalitys.get(objSR.Name_of_the_Country_GS__c) != null){
                        objSR.Nationality__c = mapNationalitys.get(objSR.Name_of_the_Country_GS__c);
                    }
                }
            }
            
            //added by Ravi on 3rd June - to perform the validations
            string result = SRValidations.BeforeInsertValidations(trigger.new);
            if(result != 'Success'){
                trigger.new[0].addError(result);
            }
        }
        
        if(trigger.isAfter){
            //after insert 
            //creating sr doc
            //creating sr price line 
            //
            //
            
            /* get all the additional price line Vs Record type map */          
            map<string,Additional_Pricing_Line__c> mapSRAddtPrLine = new map<string,Additional_Pricing_Line__c>();
            map<string,list<string>> mapRecordTypePrice = new map<string,List<string>>();
            list<string> lstProd;
            if(Additional_Pricing_Line__c.getAll()!=null){
                mapSRAddtPrLine = Additional_Pricing_Line__c.getAll();
                for(Additional_Pricing_Line__c CS:mapSRAddtPrLine.values()){
                    if (mapRecordTypePrice.containskey(cs.Record_Type__c)){
                        lstProd = mapRecordTypePrice.get(cs.Record_Type__c);
                    }else {
                        lstProd = new list<string>();
                    }
                    lstProd.add(cs.Item_name__c);               
                    mapRecordTypePrice.put(cs.Record_Type__c,lstProd);
                }
            }    
            /* get all the additional price line Vs Record type map */
                        
                
                /* Creating Application License for the Amendment */
                // unused 
                list<Service_Request__c> lstLicSR = new list<Service_Request__c>();
                for(Service_Request__c objSRlic:trigger.new){
                    if(objSRlic.Record_Type_Name__c=='Apply_for_license_amendment_or_renewal'){
                        lstLicSR.add(objSRlic);
                    }   
                }
                if(lstLicSR!=null && lstLicSR.size()>0){
                    try{
                        // duplicate license will be created if we uncomment the below line 
                        //string strResult = SRTriggerExecutionCls.CreateApplicationLicense(lstLicSR);
                        //system.debug('strResult==>'+strResult);
                    }catch(Exception e){
                        
                    }
                }
                //
                //to evalute execute contion and action we are initializing st mgr and contex here.
                //
                
                Context ctx = new Context();//Initializing the Context Class
                StateManager SM = new StateManager();//Initializing the State Manager Class 
                
                
                /* Start of Code for Preparing Maps for Pricing related Functionality */
                    
                    map<id,list<Service_Request__c>> mapSRTemplateSR = new map<id,list<Service_Request__c>>();// key as SRTemplate-Id and value as List<Service_Request__c> for Each SRTemplate.
                    map<id,list<SR_Template_Item__c>> mapSRTemplateItems = new map<id,list<SR_Template_Item__c>>();// key as SRTemplate Id and value as List<SR_Template_Item__c> for Each SRTemplate.
                    map<id,list<Pricing_Line__c>> mapProductPricingLines = new map<id,list<Pricing_Line__c>>();// key as Product Id and value as List<Pricing_Line__c> for Each Product.
                    map<id,list<Dated_Pricing__c>> mapPLDatedPricing = new map<id,list<Dated_Pricing__c>>();// key as Pricing_Line__c Id and value as List<Dated_Pricing__c> for Each Pricing Line.
                    map<id,list<Pricing_Range__c>> mapPLPricingRange = new map<id,list<Pricing_Range__c>>();// key as DatedPrice Id and value as List<Pricing_Range__c> for Each Dated Price.
                    map<id,PricebookEntry> MapProductPriceBook = new map<id,PricebookEntry>();// key as Product Id and value as PriceBookEntry for Each Product.
                    
                    map<id,list<SR_Template_Docs__c>> mapSRTempDocs = new map<id,list<SR_Template_Docs__c>>();//Key SR Template Id and value as List of TemplatedDocs
                    map<id,list<Condition__c>> mapDocConditions = new map<id,list<Condition__c>>();//Key SR Template Id and value as List of Doc Conditions
                
                    set<id> setSRIdsforPLQuery = new set<id>();
                    set<id> setPLIds = new set<id>();
                    
                    for(Service_Request__c objsr:trigger.new){
                        if(objsr.SR_Template__c!=null){
                            list<Service_Request__c> lstSR = new list<Service_Request__c>();
                            if(mapSRTemplateSR.get(objsr.SR_Template__c)!=null && mapSRTemplateSR.get(objsr.SR_Template__c).size()>0){
                                lstSR = mapSRTemplateSR.get(objsr.SR_Template__c);
                                lstSR.add(objsr);
                                mapSRTemplateSR.put(objsr.SR_Template__c,lstSR);
                            }else{
                                lstSR.add(objsr);
                                mapSRTemplateSR.put(objsr.SR_Template__c,lstSR);        
                            }       
                            setSRIdsforPLQuery.add(objsr.id);
                        }
                    }
                    
                    if(mapSRTemplateSR!=null && mapSRTemplateSR.size()>0){
                        set<id> setProdId = new set<id>();
                        for(SR_Template_Item__c SRTemplItem:[select id,Product__c,SR_Template__c,On_Submit__c from SR_Template_Item__c where SR_Template__c IN:mapSRTemplateSR.keyset() and Product__c!=null and On_Submit__c=true]){
                            list<SR_Template_Item__c> lstSRTMPItems = new list<SR_Template_Item__c>();
                            if(mapSRTemplateItems.get(SRTemplItem.SR_Template__c)!=null && mapSRTemplateItems.get(SRTemplItem.SR_Template__c).size()>0){
                                lstSRTMPItems = mapSRTemplateItems.get(SRTemplItem.SR_Template__c);
                                lstSRTMPItems.add(SRTemplItem);
                                mapSRTemplateItems.put(SRTemplItem.SR_Template__c,lstSRTMPItems);
                            }else{
                                lstSRTMPItems.add(SRTemplItem);
                                mapSRTemplateItems.put(SRTemplItem.SR_Template__c,lstSRTMPItems);
                            }
                            setProdId.add(SRTemplItem.Product__c);
                        }
                        
                        system.debug('mapSRTempDocs===>'+mapSRTempDocs);
                        system.debug('setProdId===>'+setProdId);
                        
                        system.debug('mapDocConditions===>'+mapDocConditions);
                        if(setProdId!=null && setProdId.size()>0){
                            for(PricebookEntry PBE:[Select Product2Id,Pricebook2Id,Product2.Name,UnitPrice From PricebookEntry where Product2Id IN:setProdId and IsActive=true]){
                                MapProductPriceBook.put(PBE.Product2Id,PBE);
                            }
                            for(Product2 objProd:[select id,Name,(select id,Name,Conditions__c,PL_No__c,Price_Line_Condition__c,Quantity__c,Override_SR_Quantity__c,Priority__c,Product__c,Quantity_based_on__c,Knowledge_Dirham_Applicable__c from Pricing_Lines__r order by Priority__c) from Product2 where ID IN:setProdId]){
                                if(objProd.Pricing_Lines__r!=null){
                                    mapProductPricingLines.put(objProd.id,objProd.Pricing_Lines__r);
                                    for(Pricing_Line__c PL:objProd.Pricing_Lines__r){
                                        setPLIds.add(PL.id);
                                    }
                                }
                            }
                            set<id> setDatedPricingIds = new set<id>();
                            for(Pricing_Line__c objPL:[select id,(select id,Name,Appllication_Type__c,Date_From__c,Date_To__c,Pricing_Line__c,Pricing_Line__r.Product__c,Pricing_Line__r.Knowledge_Dirham_Applicable__c,Pricing_Line__r.Quantity_based_on__c,Unit_Price__c,Use_List_Price__c from Dated_Pricing__r order by Date_From__c) from Pricing_Line__c where ID IN:setPLIds order by PL_No__c]){
                                system.debug('objPL==>'+objPL);
                                if(objPL.Dated_Pricing__r!=null && objPL.Dated_Pricing__r.size()>0){
                                    mapPLDatedPricing.put(objPL.id,objPL.Dated_Pricing__r);
                                    for(Dated_Pricing__c DP:objPL.Dated_Pricing__r){
                                        setDatedPricingIds.add(DP.id);
                                    }
                                }
                            }
                            system.debug('setDatedPricingIds==>'+setDatedPricingIds);
                            if(setDatedPricingIds!=null && setDatedPricingIds.size()>0){
                                for(Pricing_Range__c PRange:[select id,Dated_Pricing__c,Range_Start__c,Range_End__c,RangeNo__c,Unit_Price__c,Fixed_Price__c from Pricing_Range__c where Dated_Pricing__c IN:setDatedPricingIds order by Range_Start__c]){
                                    list<Pricing_Range__c> lstPR = new list<Pricing_Range__c>();
                                    if(mapPLPricingRange.get(PRange.Dated_Pricing__c)!=null && mapPLPricingRange.get(PRange.Dated_Pricing__c).size()>0){
                                        lstPR = mapPLPricingRange.get(PRange.Dated_Pricing__c);
                                        lstPR.add(PRange);
                                        mapPLPricingRange.put(PRange.Dated_Pricing__c,lstPR);
                                    }else{
                                        lstPR.add(PRange);
                                        mapPLPricingRange.put(PRange.Dated_Pricing__c,lstPR);
                                    }
                                }
                            }
                        }
                    }
                    system.debug('mapPLPricingRange==>'+mapPLPricingRange);
                    system.debug('mapSRTemplateItems==>'+mapSRTemplateItems);
                    system.debug('mapProductPricingLines==>'+mapProductPricingLines);
                    system.debug('mapPLDatedPricing==>'+mapPLDatedPricing);
                    system.debug('mapPLPricingRange==>'+mapPLPricingRange);
                    system.debug('MapProductPriceBook==>'+MapProductPriceBook);
                    
                /* End of Code for Preparing Maps for Pricing related Functionality */
                
                
                list<SR_Price_Item__c> lstSRPriceItemsInsert = new list<SR_Price_Item__c>();
                list<Service_Request__c> lstLeaseSR = new list<Service_Request__c>();
                list<Service_Request__c> lstRenLeaseSR = new list<Service_Request__c>();
                list<Service_Request__c> lstTermLeaseSR = new list<Service_Request__c>();
                list<Service_Request__c> lstAmendLeaseSR = new list<Service_Request__c>();
                list<Service_Request__c> SRLstforDoc = new list<Service_Request__c>();
                
                for(Service_Request__c SR : Trigger.new){
                    setSRIdsforDocs.add(SR.id);
                    if(SR.SR_Template__c!=null)
                        setSRTemplId_Insert.add(SR.SR_Template__c);
                    if(SR.Record_Type_Name__c=='Apply_for_license_amendment_or_renewal'){
                        SRLstforDoc.add(SR);
                    }
                }
                
                map<id,list<SR_Template_Docs__c>> mapSRTempDocs_Insert = new map<id,list<SR_Template_Docs__c>>();//Key SR Template Id and value as List of TemplatedDocs
                map<string,list<SR_Template_Docs__c>> mapSRTempDocs_Code = new map<string,list<SR_Template_Docs__c>>();//Key SR Template DMS Index and value as List of TemplatedDocs
                
                map<string,id> mapSRTempDMS_Id = new map<string,id>();//Key SR Template DMS Index and value as Id of SR Template
                
                map<id,list<Condition__c>> mapDocConditions_Insert = new map<id,list<Condition__c>>();//Key SR Template Id and value as List of Doc Conditions
                set<id> setTemplDocIds_Insert = new set<id>();
                // in sr template doc object, "on submit" true means, those docs to be inserted while inserting the sr
                
                // commented by durga Document_Master__r.LetterTemplate__c,
                for(SR_Template_Docs__c TemplDocs:[Select id,Name,Requirement__c,Document_Master__r.LetterTemplate__c,Generate_Document__c,Added_through_Code__c,Conditions__c,SR_Template_Docs_Condition__c,Document_Description_External__c,SR_Template__c,Courier_collected_at__c,Courier_delivered_at__c,Document_Master__c,
                                                  Evaluated_at__c,Additional_Uploads_Needed__c,Number_of_Additional_Uploads__c,Group_No__c,Optional__c,DMS_Document_Category__c, DMS_Document_Index__c, DMS_Document_Section__c, Document_Description__c,Document_Name_for_SR__c,Document_Master__r.Name
                                                  from SR_Template_Docs__c where SR_Template__c IN:setSRTemplId_Insert and On_Submit__c=true and ID!=null]){
                
                    if(TemplDocs.Added_through_Code__c==FALSE){
                        list<SR_Template_Docs__c> lstTemplDocs_Insert = new list<SR_Template_Docs__c>();
                        if(mapSRTempDocs_Insert.get(TemplDocs.SR_Template__c)!=null){
                            lstTemplDocs_Insert = mapSRTempDocs_Insert.get(TemplDocs.SR_Template__c);
                            lstTemplDocs_Insert.add(TemplDocs);
                            mapSRTempDocs_Insert.put(TemplDocs.SR_Template__c,lstTemplDocs_Insert);
                        }else{
                            lstTemplDocs_Insert.add(TemplDocs);
                            mapSRTempDocs_Insert.put(TemplDocs.SR_Template__c,lstTemplDocs_Insert);
                        }
                        setTemplDocIds_Insert.add(TemplDocs.id);
                    }else if(TemplDocs.Added_through_Code__c){
                      if(TemplDocs.DMS_Document_Index__c!=null){    
                        list<SR_Template_Docs__c> lstTemplDocs_Code = new list<SR_Template_Docs__c>();
                        if(mapSRTempDocs_Code.get(TemplDocs.DMS_Document_Index__c)!=null){
                            lstTemplDocs_Code = mapSRTempDocs_Code.get(TemplDocs.DMS_Document_Index__c);
                            lstTemplDocs_Code.add(TemplDocs);
                            mapSRTempDocs_Code.put(TemplDocs.DMS_Document_Index__c,lstTemplDocs_Code);
                        }else{
                            lstTemplDocs_Code.add(TemplDocs);
                            mapSRTempDocs_Code.put(TemplDocs.DMS_Document_Index__c,lstTemplDocs_Code);
                        }
                      }
                    }
                    if(TemplDocs.DMS_Document_Index__c!=null && TemplDocs.SR_Template__c!=null)
                        mapSRTempDMS_Id.put(TemplDocs.DMS_Document_Index__c,TemplDocs.SR_Template__c);
                }
                
                
                if(SRLstforDoc!=null && SRLstforDoc.size()>0 && mapSRTempDocs_Code!=null && mapSRTempDocs_Code.size() > 0){
                     /* commented by durga
                     cla_DocumentsThruCode cls = new cla_DocumentsThruCode();
                     cls.addDocsThruCode(setSRIdsforDocs,SRLstforDoc,mapSRTempDMS_Id,mapSRTempDocs_Code,setSRTemplId_Insert);
                     */
                }
                system.debug('mapSRTempDocs_Insert===>'+mapSRTempDocs_Insert);
                /*
                if(setTemplDocIds_Insert!=null && setTemplDocIds_Insert.size()>0){
                    //Querying the Conditions which are Related to the TemplateDocs of SRTemplate and where the Object NAME not equals to Step__c
                    for(Condition__c docCon:[select id,Class_Name__c,Field_Name__c,Object_Name__c,SR_Template_Docs__c from Condition__c where SR_Template_Docs__c IN:setTemplDocIds_Insert and Object_Name__c!=:null and Field_Name__c!=:null and Object_Name__c!='Step__c' and ID!=null]){
                        list<Condition__c> lstDocConds_Insert = new list<Condition__c>();
                        if(mapDocConditions_Insert.get(docCon.SR_Template_Docs__c)!=null){
                            lstDocConds_Insert = mapDocConditions_Insert.get(docCon.SR_Template_Docs__c);
                            lstDocConds_Insert.add(docCon);
                            mapDocConditions_Insert.put(docCon.SR_Template_Docs__c,lstDocConds_Insert);
                        }else{
                            lstDocConds_Insert.add(docCon);
                            mapDocConditions_Insert.put(docCon.SR_Template_Docs__c,lstDocConds_Insert);
                        }
                    }
                }
                */
                    // the below three calls to context will prepare the global maps such regis, lic etc
                    // whatever the sobjects defined in the custom setting 
                    //
                    if(setPLIds!=null && setPLIds.size()>0 && setSRIdsforPLQuery!=null && setSRIdsforPLQuery.size()>0){
                        ctx.PreparePricingFieldList(setPLIds);
                    }
                        
                    if(setTemplDocIds_Insert!=null && setTemplDocIds_Insert.size()>0){
                        ctx.PrepareDocumentConFields(setTemplDocIds_Insert);
                    }
                    
                    ctx.PreparePricingSR(setSRIdsforDocs);
                    
                    list<SR_Doc__c> lstReqSRDocsInsert_at_Insert = new list<SR_Doc__c>();
                    boolean queryNeededForVisa = false;     
                    list<id> srId = new list<id>();     
                    for(Service_Request__c objSR : trigger.new){        
                        
                        if((objSR.record_type_name__c == 'DIFC_Sponsorship_Visa_Cancellation' || (objSR.record_type_name__c == 'Miscellaneous_Services' && String.isNotBlank(objSr.Type_of_Request__c) && objSr.Type_of_Request__c.equals('Family Hold Upon Cancellation'))) && objSR.Quantity__c!=null){ // V2.00 - Added 'Miscellaneous_Services' Type        
                                system.debug('---objSR.record_type_name__c---'+objSR.record_type_name__c);      
                            queryNeededForVisa=true;        
                            srId.add(objSR.id);     
                            break;      
                        }       
                                
                    }       
                    list<SR_Template_Docs__c> srTempDoc = new list<SR_Template_Docs__c>();      
                    list<SR_Doc__c> srDoc = new list<SR_Doc__c>();      
                    if(queryNeededForVisa==true){       
                        srTempDoc = [Select id,Name,Requirement__c,Document_Master__r.LetterTemplate__c,Generate_Document__c,Added_through_Code__c,Conditions__c,SR_Template_Docs_Condition__c,Document_Description_External__c,SR_Template__c,Courier_collected_at__c,Courier_delivered_at__c,Document_Master__c,      
                                  Evaluated_at__c,Group_No__c,Optional__c,DMS_Document_Category__c, DMS_Document_Index__c, DMS_Document_Section__c, Document_Description__c,Document_Name_for_SR__c,Document_Master__r.Name     
                                  from SR_Template_Docs__c where Document_Master_Code__c =: 'Passport & Visa Copy-Family Members' limit 1];         
                                
                     /*   if(!srTempDoc.isEmpty()){       
                            srDoc = [select id from SR_Doc__c where Service_Request__c IN : srId and SR_Template_Doc__c=:srTempDoc[0].id];      
                        }       
                        if(!srDoc.isEmpty()){       
                            delete srDoc;           
                        }   */    
                    }
                    for(Service_Request__c objSRInsert:trigger.new){
                        if(objSRInsert.SR_Template__c!=null){
                            //
                            if(ctx.getSRFullRecord(string.valueof(objSRInsert.id))!=null){
                                Service_Request__c SR = ctx.getSRFullRecord(string.valueof(objSRInsert.id));
                                /* Start of Executing the code of Evaluating the Pricing Conditions and Preparing the New Pricing Records */
                                list<SR_Price_Item__c> lstSRPriceItems = new list<SR_Price_Item__c>();
                                if(mapSRTemplateItems!=null && mapSRTemplateItems.size()>0)
                                    lstSRPriceItems = SM.fetchSRPriceItems(SR,null,mapSRTemplateItems,mapProductPricingLines,mapPLDatedPricing,mapPLPricingRange,MapProductPriceBook);
                                if(lstSRPriceItems!=null && lstSRPriceItems.size()>0){
                                    lstSRPriceItemsInsert.addALL(lstSRPriceItems);
                                }
                                system.debug('lstSRPriceItemsInsert==>'+lstSRPriceItemsInsert);
                                /* End of Executing the code of Evaluating the Pricing Conditions and Preparing the New Pricing Records */
                                    
                                if(mapSRTempDocs_Insert.get(objSRInsert.SR_Template__c)!=null){
                                    for(SR_Template_Docs__c SRTmpDoc:mapSRTempDocs_Insert.get(objSRInsert.SR_Template__c)){
                                        boolean bCriteriaMet = false;
                                        if(SRTmpDoc.SR_Template_Docs_Condition__c!=null && SRTmpDoc.SR_Template_Docs_Condition__c.trim()!=''){
                                          bCriteriaMet = SM.executeDocumentConditions(SRTmpDoc,SR,null);
                                        }else{
                                          bCriteriaMet = true;
                                        }
                                        system.debug('bCriteriaMet====>'+bCriteriaMet);
                                        SR_Doc__c objSRDoc;
                                        if(bCriteriaMet!=null && bCriteriaMet){
                                            objSRDoc = new SR_Doc__c();
                                            objSRDoc.Name = SRTmpDoc.Document_Master__r.Name;
                                            objSRDoc.Service_Request__c = SR.id;
                                            objSRDoc.SR_Template_Doc__c = SRTmpDoc.Id;
                                            objSRDoc.Status__c = 'Pending Upload';
                                            objSRDoc.Document_Master__c = SRTmpDoc.Document_Master__c;
                                            objSRDoc.DMS_Document_Index__c = SRTmpDoc.DMS_Document_Index__c;
                                            objSRDoc.Group_No__c = SRTmpDoc.Group_No__c;
                                            objSRDoc.Is_Not_Required__c = SRTmpDoc.Optional__c;
                                            objSRDoc.Generate_Document__c = SRTmpDoc.Generate_Document__c;
                                            objSRDoc.Document_Description_External__c = SRTmpDoc.Document_Description_External__c;
                                            if(objSRDoc.Document_Description_External__c==null){
                                                objSRDoc.Document_Description_External__c = SRTmpDoc.Document_Description__c;
                                            }
                                            objSRDoc.Sys_IsGenerated_Doc__c = SRTmpDoc.Generate_Document__c;
                                            objSRDoc.Letter_Template__c = SRTmpDoc.Document_Master__r.LetterTemplate__c;
                                            if(SRTmpDoc.Courier_collected_at__c!=null || SRTmpDoc.Courier_delivered_at__c!=null){
                                                objSRDoc.Is_Not_Required__c = true;
                                                objSRDoc.Courier_Docs__c = true; 
                                            }
                                            //ddp will generate all doc where gen__doc__c = true
                                            // user will not upload these docs
                                            //
                                            if(objSRDoc.Generate_Document__c==true){
                                                objSRDoc.Is_Not_Required__c = true;
                                                objSRDoc.Status__c = 'Generated';
                                                string tempString  = string.valueof(Crypto.getRandomInteger());
                                                objSRDoc.Sys_RandomNo__c = tempString.right(4);
                                            }
                                            if(SRTmpDoc.Requirement__c=='Upload Copy but Original Required'){
                                                objSRDoc.Is_Not_Required__c = false;
                                                objSRDoc.Requirement__c = 'Copy & Original';
                                            }else{
                                                objSRDoc.Requirement__c = SRTmpDoc.Requirement__c;
                                            }
                                            string Gen_or_Upload = '';
                                            if(objSRDoc.Generate_Document__c==true){
                                                Gen_or_Upload = 'Generate';
                                            }else{
                                                Gen_or_Upload = 'Upload';
                                            }
                                            objSRDoc.Unique_SR_Doc__c = SRTmpDoc.Document_Master__r.Name+'_'+SR.id+'_'+Gen_or_Upload;
                                            lstReqSRDocsInsert_at_Insert.add(objSRDoc);
                                            
                                            //V1.18
                                            if(objSRInsert.SR_Group__c == 'GS' && SRTmpDoc.Additional_Uploads_Needed__c == true && SRTmpDoc.Number_of_Additional_Uploads__c>1){
                                        for(integer i=1;i<SRTmpDoc.Number_of_Additional_Uploads__c;i++){
                                          SR_Doc__c objSRDoc1 = new SR_Doc__c();       
                                              objSRDoc1.Name = SRTmpDoc.Document_Master__r.Name+'-'+i+' (additional Upload)';     
                                              objSRDoc1.Service_Request__c = SR.id;        
                                              objSRDoc1.SR_Template_Doc__c = SRTmpDoc.Id;      
                                              objSRDoc1.Status__c = 'Pending Upload';      
                                              objSRDoc1.Document_Master__c = SRTmpDoc.Document_Master__c;      
                                              objSRDoc1.DMS_Document_Index__c = SRTmpDoc.DMS_Document_Index__c;        
                                              objSRDoc1.Group_No__c = SRTmpDoc.Group_No__c;        
                                              objSRDoc1.Is_Not_Required__c = true;     
                                              objSRDoc1.Generate_Document__c = SRTmpDoc.Generate_Document__c;      
                                              objSRDoc1.Document_Description_External__c = 'Please attach additional copy of the '+SRTmpDoc.Document_Master__r.Name;       
                                              objSRDoc1.Sys_IsGenerated_Doc__c = SRTmpDoc.Generate_Document__c;        
                                              objSRDoc1.Letter_Template__c = SRTmpDoc.Document_Master__r.LetterTemplate__c;        
                                              if(SRTmpDoc.Courier_collected_at__c!=null || SRTmpDoc.Courier_delivered_at__c!=null){       
                                                  objSRDoc1.Is_Not_Required__c = true;     
                                                  objSRDoc1.Courier_Docs__c = true;        
                                              }       
                                              if(objSRDoc1.Generate_Document__c==true){        
                                                  objSRDoc1.Is_Not_Required__c = true;     
                                                  objSRDoc1.Status__c = 'Generated';       
                                                  string tempString  = string.valueof(Crypto.getRandomInteger());     
                                                  objSRDoc1.Sys_RandomNo__c = tempString.right(4);     
                                              }       
                                              if(SRTmpDoc.Requirement__c=='Upload Copy but Original Required'){       
                                                  objSRDoc1.Is_Not_Required__c = false;        
                                                  objSRDoc1.Requirement__c = 'Copy & Original';        
                                              }else{      
                                                  objSRDoc1.Requirement__c = SRTmpDoc.Requirement__c;      
                                              }       
                                              string Gen_or_Upload1 = '';      
                                              if(objSRDoc1.Generate_Document__c==true){        
                                                  Gen_or_Upload1 = 'Generate';     
                                              }else{      
                                                  Gen_or_Upload1 = 'Upload';       
                                              }       
                                              objSRDoc1.Unique_SR_Doc__c = SRTmpDoc.Document_Master__r.Name+'_'+i+'_'+SR.id+'_'+Gen_or_Upload1;     
                                              lstReqSRDocsInsert_at_Insert.add(objSRDoc1);     
                                        }
                                      }
                                        }
                                    }
                                }
                                
                                if((objSRInsert.record_type_name__c == 'DIFC_Sponsorship_Visa_Cancellation' || (objSRInsert.record_type_name__c == 'Miscellaneous_Services' && String.isNotBlank(objSRInsert.Type_of_Request__c) && objSRInsert.Type_of_Request__c.equals('Family Hold Upon Cancellation'))) && !srTempDoc.isEmpty() && objSRInsert.Quantity__c>0){ // V2.00 - Claude                               
                                    for(integer i=1;i<=objSRInsert.Quantity__c;i++){        
                                        SR_Doc__c objSRDoc = new SR_Doc__c();       
                                        objSRDoc.Name = srTempDoc[0].Document_Master__r.Name+'-'+i;     
                                        objSRDoc.Service_Request__c = SR.id;        
                                        objSRDoc.SR_Template_Doc__c = srTempDoc[0].Id;      
                                        objSRDoc.Status__c = 'Pending Upload';      
                                        objSRDoc.Document_Master__c = srTempDoc[0].Document_Master__c;      
                                        objSRDoc.DMS_Document_Index__c = srTempDoc[0].DMS_Document_Index__c;        
                                        objSRDoc.Group_No__c = srTempDoc[0].Group_No__c;        
                                        objSRDoc.Is_Not_Required__c = srTempDoc[0].Optional__c;     
                                        objSRDoc.Generate_Document__c = srTempDoc[0].Generate_Document__c;      
                                        objSRDoc.Document_Description_External__c = srTempDoc[0].Document_Description_External__c;      
                                        if(objSRDoc.Document_Description_External__c==null){        
                                            objSRDoc.Document_Description_External__c = srTempDoc[0].Document_Description__c;       
                                        }       
                                        objSRDoc.Sys_IsGenerated_Doc__c = srTempDoc[0].Generate_Document__c;        
                                        objSRDoc.Letter_Template__c = srTempDoc[0].Document_Master__r.LetterTemplate__c;        
                                        if(srTempDoc[0].Courier_collected_at__c!=null || srTempDoc[0].Courier_delivered_at__c!=null){       
                                            objSRDoc.Is_Not_Required__c = true;     
                                            objSRDoc.Courier_Docs__c = true;        
                                        }       
                                        if(objSRDoc.Generate_Document__c==true){        
                                            objSRDoc.Is_Not_Required__c = true;     
                                            objSRDoc.Status__c = 'Generated';       
                                            string tempString  = string.valueof(Crypto.getRandomInteger());     
                                            objSRDoc.Sys_RandomNo__c = tempString.right(4);     
                                        }       
                                        if(srTempDoc[0].Requirement__c=='Upload Copy but Original Required'){       
                                            objSRDoc.Is_Not_Required__c = false;        
                                            objSRDoc.Requirement__c = 'Copy & Original';        
                                        }else{      
                                            objSRDoc.Requirement__c = srTempDoc[0].Requirement__c;      
                                        }       
                                        string Gen_or_Upload = '';      
                                        if(objSRDoc.Generate_Document__c==true){        
                                            Gen_or_Upload = 'Generate';     
                                        }else{      
                                            Gen_or_Upload = 'Upload';       
                                        }       
                                        objSRDoc.Unique_SR_Doc__c = srTempDoc[0].Document_Master__r.Name+'_'+i+'_'+SR.id+'_'+Gen_or_Upload;     
                                        lstReqSRDocsInsert_at_Insert.add(objSRDoc);     
                                    }       
                                }
                            }
                        }
                    }
                    
                    system.debug('lstSRPriceItemsInsert==>'+lstSRPriceItemsInsert);
                    if(lstSRPriceItemsInsert!=null && lstSRPriceItemsInsert.size()>0){
                        try{
                            /*
                            for(integer i=0;i<lstSRPriceItemsInsert.size();i++){
                                lstSRPriceItemsInsert[i].Status__c = 'Added';
                            }
                            */
                            Database.SaveResult[] SRPriceItemsInsertResult = Database.insert(lstSRPriceItemsInsert, false);
                            //insert lstSRPriceItemsInsert;
                        }catch(Exception ex){
                            trigger.new[0].addError(ex.getMessage());
                        }
                    }
                    
                    if(lstReqSRDocsInsert_at_Insert!=null && lstReqSRDocsInsert_at_Insert.size()>0){
                        if(lstReqSRDocsInsert_at_Insert!=null && lstReqSRDocsInsert_at_Insert.size()>0){
                            Database.SaveResult[] SRReqDocInsertResult = Database.insert(lstReqSRDocsInsert_at_Insert, false);
                            system.debug('SRReqDocInsertResult==>'+SRReqDocInsertResult);
                        }
                    }
                    
                    system.debug('lstRenLeaseSR==>'+lstRenLeaseSR);
                    //Call Lease functions Here
                    system.debug('lstLeaseSR==>'+lstLeaseSR);
                    
                list<service_request__c> lstSR = new list<service_request__c>();
                for(Service_Request__c objSR : trigger.new){
                    //if (mapSRRecType.get(objSR.RecordTypeId)== 'Apply_for_license_amendment_or_renewal')
                    if (mapRecordTypePrice.containskey(mapSRRecType.get(objSR.RecordTypeId))) {
                        lstSR.add(objSR);
                    }
                }
                if (lstSr.size() > 0 ) {
                    /* commented by durga
                    string str = LicenseRenewalCC.AddSrPriceItem_SR(lstSR);
                    if (str!='Success') {
                        trigger.new[0].addError(str);
                    }*/
                }
        }
    }
   //
   //in CASE update, delete and reevaluate the condition and re-insert the pricing lines and sr docs
  //at the time of submission teh default step is getting inserted
  //
   if(trigger.isUpdate){
        // V1.4 Changing the External Status to pending if pending is applied
        if(trigger.isBefore){
            map<string,string> mapNationalitys = new map<string,string>();
            list<SR_Status__c> PendingStatus = new list<SR_Status__c>();
            /*List<id> Appids= new List<id>();
            Map<id,id> SRContMap=new Map<id,id>();
            Map<id,Contact> ContIdMap= new Map<id,Contact>();*/
            //V1.10
            Boolean isGs = false;
            for(Service_Request__c SR:trigger.new){
                if(SR.SR_Group__c == 'GS'){
                    isGS = true;
                    break;
                }   
            }
            if(isGs == true)
                PendingStatus = [select id,name from SR_Status__c where Code__c='Pending' limit 1];
            for(Service_Request__c SR:trigger.new){
                if(SR.SR_Group__c == 'GS'){
                    if(SR.Is_Cancelled__c == false){
                        if(SR.Is_Pending__c==true)
                            SR.External_SR_Status__c=PendingStatus[0].id;
                        else if(SR.SR_Group__c != 'GS'){ // for not GS SR's
                            SR.External_SR_Status__c=SR.Internal_SR_Status__c;
                        }else if(SR.SR_Group__c == 'GS' && (SR.Adhering_to_Safe_Horbour__c == false || SR.Avail_Courier_Services__c == 'Yes') ){
                            // Adhering_to_Safe_Horbour__c will be ticked from INFORMATICA for GS if the Email should send at next Day morning same field is using in ROC DP also
                            SR.External_SR_Status__c=SR.Internal_SR_Status__c;
                        }    
                    }
                }    
                /*//Added by Kaavya for V1.5 Additional Charge for amendments      
                system.debug('SR Template==='+SR.Record_Type_Name__c);
                if(SR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Amendment' || SR.Record_Type_Name__c == 'Non_DIFC_Sponsorship_Visa_Amendment'){
                    if(SR.Contact__c!=null){
                        system.debug('IIIInside if');
                        Appids.add(SR.Contact__c);
                        SRContMap.put(SR.Contact__c,SR.id);
                    }
                    
                }*/
            }
            
            /*if(Appids.size()>0){
                List<Contact> Applist=[select id,FirstName,LastName,Job_Title__c,Passport_No__c,Date_of_Issue__c,Passport_Expiry_Date__c from Contact where id in:Appids];
                for(Contact C:Applist){
                    ContIdMap.put(SRContMap.get(C.id),C);
                }
            }*/
            
            for(Service_Request__c objSR : trigger.new){
                /*
                //Added by Kaavya for V1.5 Additional Charge for amendments
                if(objSR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Amendment' || objSR.Record_Type_Name__c == 'Non_DIFC_Sponsorship_Visa_Amendment'){
                    if(objSR.Contact__c!=null){
                        Contact Appl=ContIdMap.get(objSR.id); 
                        integer chgno=0;
                        if(Appl.FirstName != objSR.First_Name__c ||Appl.LastName != objSR.Last_Name__c)
                            chgno++; 
                        system.debug('Name=='+chgno);
                        if(Appl.Job_Title__c != objSR.Occupation_GS__c) 
                            chgno++;  
                        system.debug('Job=='+chgno);              
                        if(Appl.Passport_No__c != objSR.Passport_Number__c ||Appl.Date_of_Issue__c != objSR.Passport_Date_of_Issue__c ||Appl.Passport_Expiry_Date__c != objSR.Passport_Date_of_Expiry__c) 
                            chgno++;
                        system.debug('Passport=='+chgno);
                        if(chgno>1)
                            objSR.Sys_Addl_Charge__c= true;
                        else
                            objSR.Sys_Addl_Charge__c= false;
                    }
                }
                */
                //added by Ravi on 12th March
                if(objSR.Nationality_list__c != null && objSR.Record_Type_Name__c != 'Consulate_or_Embassy_Letter'){
                    mapNationalitys.put(objSR.Nationality_list__c,null);
                }else if(objSR.Record_Type_Name__c == 'Consulate_or_Embassy_Letter' && objSR.Name_of_the_Country_GS__c != null){
                    mapNationalitys.put(objSR.Name_of_the_Country_GS__c,null);
                }
            }
            if(!mapNationalitys.isEmpty()){
                for(Lookup__c objLp : [select Id,Name from Lookup__c where Type__c='Nationality' AND Name IN : mapNationalitys.keySet()]){
                    mapNationalitys.put(objLp.Name,objLp.Id);
                }
                for(Service_Request__c objSR : trigger.new){
                    if(objSR.Record_Type_Name__c != 'Consulate_or_Embassy_Letter' && objSR.Nationality_list__c != null && mapNationalitys.get(objSR.Nationality_list__c) != null){
                        objSR.Nationality__c = mapNationalitys.get(objSR.Nationality_list__c);
                    }else if(objSR.Record_Type_Name__c == 'Consulate_or_Embassy_Letter' && objSR.Name_of_the_Country_GS__c != null && mapNationalitys.get(objSR.Name_of_the_Country_GS__c) != null){
                        objSR.Nationality__c = mapNationalitys.get(objSR.Name_of_the_Country_GS__c);
                    }
                }
            }
            
        } //End of V1.4
        if(trigger.isAfter){
            Context ctx = new Context();//Initializing the Context Class
            StateManager SM = new StateManager();//Initializing the State Manager Class
            
            set<id> setSRSubmittedSrIds = new set<id>();
            set<id> setDraftRecordsEdited = new set<id>();
            
            map<id,list<Service_Request__c>> mapSRTemplateSR = new map<id,list<Service_Request__c>>();// key as SRTemplate-Id and value as List<Service_Request__c> for Each SRTemplate.
            map<id,list<SR_Template_Item__c>> mapSRTemplateItems = new map<id,list<SR_Template_Item__c>>();// key as SRTemplate Id and value as List<SR_Template_Item__c> for Each SRTemplate.
            map<id,list<Pricing_Line__c>> mapProductPricingLines = new map<id,list<Pricing_Line__c>>();// key as Product Id and value as List<Pricing_Line__c> for Each Product.
            map<id,list<Dated_Pricing__c>> mapPLDatedPricing = new map<id,list<Dated_Pricing__c>>();// key as Pricing_Line__c Id and value as List<Dated_Pricing__c> for Each Pricing Line.
            map<id,list<Pricing_Range__c>> mapPLPricingRange = new map<id,list<Pricing_Range__c>>();// key as DatedPrice Id and value as List<Pricing_Range__c> for Each Dated Price.
            map<id,PricebookEntry> MapProductPriceBook = new map<id,PricebookEntry>();// key as Product Id and value as PriceBookEntry for Each Product.
            
            map<id,list<SR_Template_Docs__c>> mapSRTempDocs_Insert = new map<id,list<SR_Template_Docs__c>>();//Key SR Template Id and value as List of TemplatedDocs
            map<string,list<SR_Template_Docs__c>> mapSRTempDocs_Code = new map<string,list<SR_Template_Docs__c>>();//Key SR Template DMS Index and value as List of TemplatedDocs
            list<SR_Doc__c> lstReqSRDocsInsert_at_Update = new list<SR_Doc__c>();
            map<string,id> mapSRTempDMS_Id = new map<string,id>();//Key SR Template DMS Index and value as Id of SR Template
            
            map<id,list<Condition__c>> mapDocConditions_Insert = new map<id,list<Condition__c>>();//Key SR Template Id and value as List of Doc Conditions
            set<id> setTemplDocIds_Insert = new set<id>();

            
            set<id> setSRIdsforPLQuery = new set<id>();
            set<id> setPLIds = new set<id>();
            list<SR_Price_Item__c> lstSRPriceItems_ReInsert = new list<SR_Price_Item__c>();
                
            for(Service_Request__c objsr:trigger.new){
                if(objsr.Internal_Status_Name__c=='Submitted' && (trigger.oldmap.get(objsr.id).Internal_Status_Name__c=='Draft' || trigger.oldmap.get(objsr.id).Internal_Status_Name__c=='Awaiting Sanction Approval') && trigger.oldmap.get(objsr.id).Internal_Status_Name__c!=objsr.Internal_Status_Name__c){
                    setSRSubmittedSrIds.add(objsr.id);
                }
                if(objsr.finalizeAmendmentFlg__c==true && Trigger.oldMap.get(objsr.Id).finalizeAmendmentFlg__c==false){
                    RecursiveControlCls.isUpdatedAlready=false; 
                }
                 
                if(RecursiveControlCls.Do_not_Reevaluate==false && objsr.Internal_Status_Name__c=='Draft' && RecursiveControlCls.isUpdatedAlready==false && trigger.oldmap.get(objsr.id)!=objsr && trigger.oldmap.get(objsr.id).OwnerId==objsr.OwnerId){
                    if(objsr.finalizeAmendmentFlg__c==false || (objsr.finalizeAmendmentFlg__c==true && Trigger.oldMap.get(objsr.Id).finalizeAmendmentFlg__c==false )){
                        setSRIdsforPLQuery.add(objsr.Id);
                    }
                    list<Service_Request__c> lstSR = new list<Service_Request__c>();
                    if(mapSRTemplateSR.get(objsr.SR_Template__c)!=null && mapSRTemplateSR.get(objsr.SR_Template__c).size()>0){
                        lstSR = mapSRTemplateSR.get(objsr.SR_Template__c);
                        lstSR.add(objsr);
                        mapSRTemplateSR.put(objsr.SR_Template__c,lstSR);
                    }else{
                        lstSR.add(objsr);
                        mapSRTemplateSR.put(objsr.SR_Template__c,lstSR);        
                    }
                }
            }
            
            system.debug('setSRIdsforPLQuery >>' + setSRIdsforPLQuery + ' keySet >>' + mapSRTemplateSR.keyset());
            
            //this condition will execute only when SR is Edited with Draft Status 
            if(setSRIdsforPLQuery!=null && setSRIdsforPLQuery.size()>0){
                // commented by durga 
                for(SR_Template_Docs__c TemplDocs:[Select id,Name,Requirement__c,Document_Master__r.LetterTemplate__c,Generate_Document__c,Added_through_Code__c,Conditions__c,SR_Template_Docs_Condition__c,Document_Description_External__c,SR_Template__c,Courier_collected_at__c,Courier_delivered_at__c,Document_Master__c,
                                  Evaluated_at__c,Group_No__c,Additional_Uploads_Needed__c,Number_of_Additional_Uploads__c,Optional__c,DMS_Document_Category__c, DMS_Document_Index__c, DMS_Document_Section__c, Document_Description__c,Document_Name_for_SR__c,Document_Master__r.Name
                                  from SR_Template_Docs__c where SR_Template__c IN:mapSRTemplateSR.keyset() and On_Submit__c=true and ID!=null]){
                        if(TemplDocs.Added_through_Code__c==false){
                            list<SR_Template_Docs__c> lstTemplDocs_Insert = new list<SR_Template_Docs__c>();
                            if(mapSRTempDocs_Insert.get(TemplDocs.SR_Template__c)!=null){
                                lstTemplDocs_Insert = mapSRTempDocs_Insert.get(TemplDocs.SR_Template__c);
                                lstTemplDocs_Insert.add(TemplDocs);
                                mapSRTempDocs_Insert.put(TemplDocs.SR_Template__c,lstTemplDocs_Insert);
                            }else{
                                lstTemplDocs_Insert.add(TemplDocs);
                                mapSRTempDocs_Insert.put(TemplDocs.SR_Template__c,lstTemplDocs_Insert);
                            }
                            setTemplDocIds_Insert.add(TemplDocs.id);
                        }else if(TemplDocs.Added_through_Code__c){
                          if(TemplDocs.DMS_Document_Index__c!=null){
                            list<SR_Template_Docs__c> lstTemplDocs_Code = new list<SR_Template_Docs__c>();
                            if(mapSRTempDocs_Code.get(TemplDocs.DMS_Document_Index__c)!=null){
                                lstTemplDocs_Code = mapSRTempDocs_Code.get(TemplDocs.DMS_Document_Index__c);
                                lstTemplDocs_Code.add(TemplDocs);
                                mapSRTempDocs_Code.put(TemplDocs.DMS_Document_Index__c,lstTemplDocs_Code);
                            }else{
                                lstTemplDocs_Code.add(TemplDocs);
                                mapSRTempDocs_Code.put(TemplDocs.DMS_Document_Index__c,lstTemplDocs_Code);
                            }
                          }
                        }
                        if(TemplDocs.DMS_Document_Index__c!=null && TemplDocs.SR_Template__c!=null)
                            mapSRTempDMS_Id.put(TemplDocs.DMS_Document_Index__c,TemplDocs.SR_Template__c);
                }
                
                
                list<SR_Doc__c> lstSRDocs_to_be_deleted = new list<SR_Doc__c>();
                for(SR_Doc__c objSRDoc:[select id,From_Finalize__c,Doc_ID__c,Status__c,Amendment__c from SR_Doc__c where Service_Request__c != null AND Service_Request__c IN:setSRIdsforPLQuery AND From_Finalize__c=false AND Unique_SR_Doc__c != null]){// and (Doc_ID__c=null or Status__c='Generated')
                    if(objSRDoc.Amendment__c==null && objSRDoc.From_Finalize__c==false && (objSRDoc.Doc_ID__c==null || objSRDoc.Status__c=='Generated')){
                        lstSRDocs_to_be_deleted.add(objSRDoc);
                    }
                }
                //
                //if user already uploaded the doc, then Doc_ID__c will have a value , so  avoid that doc from being deleted 
                // 
                if(lstSRDocs_to_be_deleted!=null && lstSRDocs_to_be_deleted.size()>0){
                    try{
                        delete lstSRDocs_to_be_deleted;
                    }catch(Exception e){
                        
                    }
                }
                
                list<SR_Price_Item__c> lstSRPriceItemsDeletable = [select id,Product__c,Product__r.Name,Product__r.ProductCode,Unique_SR_PriceItem__c from SR_Price_Item__c where ServiceRequest__c IN:setSRIdsforPLQuery and Sys_Added_through_Code__c=false and Non_Reevaluate__c=false];
                if(lstSRPriceItemsDeletable!=null && lstSRPriceItemsDeletable.size()>0){
                     try{
                        delete lstSRPriceItemsDeletable;
                    }catch(Exception e){
                        
                    }
                }
                
                if(mapSRTemplateSR!=null && mapSRTemplateSR.size()>0){
                    set<id> setProdId = new set<id>();
                    for(SR_Template_Item__c SRTemplItem:[select id,Product__c,SR_Template__c,On_Submit__c from SR_Template_Item__c where SR_Template__c IN:mapSRTemplateSR.keyset() and Product__c!=null and On_Submit__c=true]){
                        list<SR_Template_Item__c> lstSRTMPItems = new list<SR_Template_Item__c>();
                        if(mapSRTemplateItems.get(SRTemplItem.SR_Template__c)!=null && mapSRTemplateItems.get(SRTemplItem.SR_Template__c).size()>0){
                            lstSRTMPItems = mapSRTemplateItems.get(SRTemplItem.SR_Template__c);
                            lstSRTMPItems.add(SRTemplItem);
                            mapSRTemplateItems.put(SRTemplItem.SR_Template__c,lstSRTMPItems);
                        }else{
                            lstSRTMPItems.add(SRTemplItem);
                            mapSRTemplateItems.put(SRTemplItem.SR_Template__c,lstSRTMPItems);
                        }
                        setProdId.add(SRTemplItem.Product__c);
                    }
                    
                    system.debug('setProdId===>'+setProdId);
                    
                    if(setProdId!=null && setProdId.size()>0){
                        for(PricebookEntry PBE:[Select Product2Id,Pricebook2Id,Product2.Name,UnitPrice From PricebookEntry where Product2Id IN:setProdId and IsActive=true]){
                            MapProductPriceBook.put(PBE.Product2Id,PBE);
                        }
                        for(Product2 objProd:[select id,Name,(select id,Name,Conditions__c,Knowledge_Dirham_Applicable__c,Quantity__c,Override_SR_Quantity__c,PL_No__c,Price_Line_Condition__c,Priority__c,Product__c,Use_activities_as_license__c,Quantity_based_on__c from Pricing_Lines__r order by Priority__c) from Product2 where ID IN:setProdId]){
                            if(objProd.Pricing_Lines__r!=null){
                                mapProductPricingLines.put(objProd.id,objProd.Pricing_Lines__r);
                                for(Pricing_Line__c PL:objProd.Pricing_Lines__r){
                                    setPLIds.add(PL.id);
                                }
                            }
                        }
                        set<id> setDatedPricingIds = new set<id>();
                        for(Pricing_Line__c objPL:[select id,(select id,Name,Appllication_Type__c,Date_From__c,Date_To__c,Pricing_Line__c,Pricing_Line__r.Knowledge_Dirham_Applicable__c,Pricing_Line__r.Product__c,Pricing_Line__r.Quantity_based_on__c,Unit_Price__c,Use_List_Price__c from Dated_Pricing__r order by Date_From__c) from Pricing_Line__c where ID IN:setPLIds order by PL_No__c]){
                            system.debug('objPL==>'+objPL);
                            if(objPL.Dated_Pricing__r!=null && objPL.Dated_Pricing__r.size()>0){
                                mapPLDatedPricing.put(objPL.id,objPL.Dated_Pricing__r);
                                for(Dated_Pricing__c DP:objPL.Dated_Pricing__r){
                                    setDatedPricingIds.add(DP.id);
                                }
                            }
                        }
                        system.debug('setDatedPricingIds==>'+setDatedPricingIds);
                        if(setDatedPricingIds!=null && setDatedPricingIds.size()>0){
                            for(Pricing_Range__c PRange:[select id,Dated_Pricing__c,Range_Start__c,Range_End__c,RangeNo__c,Unit_Price__c,Fixed_Price__c from Pricing_Range__c where Dated_Pricing__c IN:setDatedPricingIds order by Range_Start__c]){
                                list<Pricing_Range__c> lstPR = new list<Pricing_Range__c>();
                                if(mapPLPricingRange.get(PRange.Dated_Pricing__c)!=null && mapPLPricingRange.get(PRange.Dated_Pricing__c).size()>0){
                                    lstPR = mapPLPricingRange.get(PRange.Dated_Pricing__c);
                                    lstPR.add(PRange);
                                    mapPLPricingRange.put(PRange.Dated_Pricing__c,lstPR);
                                }else{
                                    lstPR.add(PRange);
                                    mapPLPricingRange.put(PRange.Dated_Pricing__c,lstPR);
                                }
                            }
                        }
                    } 
                    
                    system.debug('mapPLPricingRange==>'+mapPLPricingRange);
                    if(setTemplDocIds_Insert!=null && setTemplDocIds_Insert.size()>0){
                        ctx.PrepareDocumentConFields(setTemplDocIds_Insert); 
                    }
                    if(setPLIds!=null && setPLIds.size()>0){
                        ctx.PreparePricingFieldList(setPLIds);
                    }
                    if(setSRIdsforPLQuery!=null && setSRIdsforPLQuery.size()>0){
                        ctx.PreparePricingSR(setSRIdsforPLQuery);   
                    }
                    
                    system.debug('mapSRTemplateItems==>'+mapSRTemplateItems);
                    system.debug('mapProductPricingLines==>'+mapProductPricingLines);
                    system.debug('mapPLDatedPricing==>'+mapPLDatedPricing);
                    system.debug('mapPLPricingRange==>'+mapPLPricingRange);
                    system.debug('MapProductPriceBook==>'+MapProductPriceBook);
                    
                    list<SR_Doc__c> lstReqSRDocsInsert_at_Insert = new list<SR_Doc__c>();       
                    boolean queryNeededForVisa = false;     
                    list<id> srId = new list<id>();     
                    for(Service_Request__c objSR : trigger.new){        
                        if((objSR.record_type_name__c == 'DIFC_Sponsorship_Visa_Cancellation' || (objSR.record_type_name__c == 'Miscellaneous_Services' && String.isNotBlank(objSr.Type_of_Request__c) && objSr.Type_of_Request__c.equals('Family Hold Upon Cancellation') )) && objSR.Quantity__c!=null){        // V2.00
                                system.debug('---objSR.record_type_name__c---'+objSR.record_type_name__c);      
                            queryNeededForVisa=true;        
                            srId.add(objSR.id);     
                            break;      
                        }       
                                
                    }       
                    list<SR_Template_Docs__c> srTempDoc = new list<SR_Template_Docs__c>();      
                    list<SR_Doc__c> srDoc = new list<SR_Doc__c>();      
                    if(queryNeededForVisa==true){       
                        srTempDoc = [Select id,Name,Requirement__c,Document_Master__r.LetterTemplate__c,Generate_Document__c,Added_through_Code__c,Conditions__c,SR_Template_Docs_Condition__c,Document_Description_External__c,SR_Template__c,Courier_collected_at__c,Courier_delivered_at__c,Document_Master__c,      
                                  Evaluated_at__c,Group_No__c,Optional__c,DMS_Document_Category__c, DMS_Document_Index__c, DMS_Document_Section__c, Document_Description__c,Document_Name_for_SR__c,Document_Master__r.Name     
                                  from SR_Template_Docs__c where Document_Master_Code__c =: 'Passport & Visa Copy-Family Members' limit 1];         
                                
                        system.debug('---srTempDoc----'+srTempDoc);
                      /*  if(!srTempDoc.isEmpty()){       
                            srDoc = [select id from SR_Doc__c where Service_Request__c IN : srId and SR_Template_Doc__c=:srTempDoc[0].id];      
                        }       
                        if(!srDoc.isEmpty()){       
                            delete srDoc;           
                        }       */
                    }
                    
                    for(Service_Request__c SRDrafted:trigger.new){
                        if(SRDrafted.SR_Template__c!=null){
                            if(ctx.getSRFullRecord(string.valueof(SRDrafted.id))!=null){
                                Service_Request__c SR = ctx.getSRFullRecord(string.valueof(SRDrafted.id));
                                system.debug('SR===>'+SR);
                                list<SR_Price_Item__c> lstSR_RePriceItems = new list<SR_Price_Item__c>();
                                if(mapSRTemplateItems!=null && mapSRTemplateItems.size()>0)
                                    lstSR_RePriceItems = SM.fetchSRPriceItems(SR,null,mapSRTemplateItems,mapProductPricingLines,mapPLDatedPricing,mapPLPricingRange,MapProductPriceBook);
                                if(lstSR_RePriceItems!=null && lstSR_RePriceItems.size()>0){
                                    lstSRPriceItems_ReInsert.addALL(lstSR_RePriceItems);
                                }
                                
                                system.debug(' lstSR_RePriceItems==> ' + lstSR_RePriceItems);
                                system.debug(' lstSRPriceItems_ReInsert==> ' + lstSRPriceItems_ReInsert);
                                
                                if(mapSRTempDocs_Insert.get(SRDrafted.SR_Template__c)!=null){
                                    for(SR_Template_Docs__c SRTmpDoc:mapSRTempDocs_Insert.get(SRDrafted.SR_Template__c)){
                                        boolean bCriteriaMet = false;
                                        if(SRTmpDoc.SR_Template_Docs_Condition__c!=null && SRTmpDoc.SR_Template_Docs_Condition__c.trim()!=''){
                                          bCriteriaMet = SM.executeDocumentConditions(SRTmpDoc,SR,null);
                                        }else{
                                          bCriteriaMet = true;
                                        }
                                        system.debug('bCriteriaMet====>'+bCriteriaMet);
                                        SR_Doc__c objSRDoc;
                                        if(bCriteriaMet!=null && bCriteriaMet){
                                            objSRDoc = new SR_Doc__c();
                                            objSRDoc.Name = SRTmpDoc.Document_Master__r.Name;
                                            objSRDoc.Service_Request__c = SR.id;
                                            objSRDoc.SR_Template_Doc__c = SRTmpDoc.Id;
                                            objSRDoc.Status__c = 'Pending Upload';
                                            objSRDoc.Document_Master__c = SRTmpDoc.Document_Master__c;
                                            objSRDoc.DMS_Document_Index__c = SRTmpDoc.DMS_Document_Index__c;
                                            objSRDoc.Group_No__c = SRTmpDoc.Group_No__c;
                                            objSRDoc.Is_Not_Required__c = SRTmpDoc.Optional__c;
                                            objSRDoc.Generate_Document__c = SRTmpDoc.Generate_Document__c;
                                            objSRDoc.Document_Description_External__c = SRTmpDoc.Document_Description_External__c;
                                            if(objSRDoc.Document_Description_External__c==null){
                                                objSRDoc.Document_Description_External__c = SRTmpDoc.Document_Description__c;
                                            }
                                            objSRDoc.Sys_IsGenerated_Doc__c = SRTmpDoc.Generate_Document__c;
                                            objSRDoc.Letter_Template__c = SRTmpDoc.Document_Master__r.LetterTemplate__c;
                                            if(SRTmpDoc.Courier_collected_at__c!=null || SRTmpDoc.Courier_delivered_at__c!=null){
                                                objSRDoc.Is_Not_Required__c = true;
                                                objSRDoc.Courier_Docs__c = true;
                                            }
                                            if(objSRDoc.Generate_Document__c==true){
                                                objSRDoc.Is_Not_Required__c = true;
                                                objSRDoc.Status__c = 'Generated';
                                                string tempString  = string.valueof(Crypto.getRandomInteger());
                                                objSRDoc.Sys_RandomNo__c = tempString.right(4);
                                            }
                                            if(SRTmpDoc.Requirement__c=='Upload Copy but Original Required'){
                                                objSRDoc.Is_Not_Required__c = false;
                                                objSRDoc.Requirement__c = 'Copy & Original';
                                            }else{
                                                objSRDoc.Requirement__c = SRTmpDoc.Requirement__c;
                                            }
                                            string Gen_or_Upload = '';
                                            if(objSRDoc.Generate_Document__c==true){
                                                Gen_or_Upload = 'Generate';
                                                objSRDoc.Is_Not_Required__c = true;
                                            }else{
                                                Gen_or_Upload = 'Upload';
                                            } 
                                            objSRDoc.Unique_SR_Doc__c = SRTmpDoc.Document_Master__r.Name+'_'+SR.id+'_'+Gen_or_Upload;
                                            lstReqSRDocsInsert_at_Update.add(objSRDoc);
                                            
                                            //V1.18
                                            if(SR.SR_Group__c == 'GS' && SRTmpDoc.Additional_Uploads_Needed__c == true && SRTmpDoc.Number_of_Additional_Uploads__c>1){
                                          for(integer i=1;i<SRTmpDoc.Number_of_Additional_Uploads__c;i++){
                                            SR_Doc__c objSRDoc1 = new SR_Doc__c();       
                                                objSRDoc1.Name = SRTmpDoc.Document_Master__r.Name+'-'+i+' (additional Upload)';     
                                                objSRDoc1.Service_Request__c = SR.id;        
                                                objSRDoc1.SR_Template_Doc__c = SRTmpDoc.Id;      
                                                objSRDoc1.Status__c = 'Pending Upload';      
                                                objSRDoc1.Document_Master__c = SRTmpDoc.Document_Master__c;      
                                                objSRDoc1.DMS_Document_Index__c = SRTmpDoc.DMS_Document_Index__c;        
                                                objSRDoc1.Group_No__c = SRTmpDoc.Group_No__c;        
                                                objSRDoc1.Is_Not_Required__c = true;     
                                                objSRDoc1.Generate_Document__c = SRTmpDoc.Generate_Document__c;      
                                                objSRDoc1.Document_Description_External__c = 'Please attach additional copy of the '+SRTmpDoc.Document_Master__r.Name;      
                                                objSRDoc1.Sys_IsGenerated_Doc__c = SRTmpDoc.Generate_Document__c;        
                                                objSRDoc1.Letter_Template__c = SRTmpDoc.Document_Master__r.LetterTemplate__c;        
                                                if(SRTmpDoc.Courier_collected_at__c!=null || SRTmpDoc.Courier_delivered_at__c!=null){       
                                                    objSRDoc1.Is_Not_Required__c = true;     
                                                    objSRDoc1.Courier_Docs__c = true;        
                                                }       
                                                if(objSRDoc1.Generate_Document__c==true){        
                                                    objSRDoc1.Is_Not_Required__c = true;     
                                                    objSRDoc1.Status__c = 'Generated';       
                                                    string tempString  = string.valueof(Crypto.getRandomInteger());     
                                                    objSRDoc1.Sys_RandomNo__c = tempString.right(4);     
                                                }       
                                                if(SRTmpDoc.Requirement__c=='Upload Copy but Original Required'){       
                                                    objSRDoc1.Is_Not_Required__c = false;        
                                                    objSRDoc1.Requirement__c = 'Copy & Original';        
                                                }else{      
                                                    objSRDoc1.Requirement__c = SRTmpDoc.Requirement__c;      
                                                }       
                                                string Gen_or_Upload1 = '';      
                                                if(objSRDoc1.Generate_Document__c==true){        
                                                    Gen_or_Upload1 = 'Generate';     
                                                }else{      
                                                    Gen_or_Upload1 = 'Upload';       
                                                }       
                                                objSRDoc1.Unique_SR_Doc__c = SRTmpDoc.Document_Master__r.Name+'_'+i+'_'+SR.id+'_'+Gen_or_Upload1;     
                                                lstReqSRDocsInsert_at_Update.add(objSRDoc1);     
                                          }
                                        }
                                        }
                                        }
                                }
                                
                                if((SRDrafted.record_type_name__c == 'DIFC_Sponsorship_Visa_Cancellation' || (SRDrafted.record_type_name__c == 'Miscellaneous_Services' && String.isNotBlank(SRDrafted.Type_of_Request__c) && SRDrafted.Type_of_Request__c.equals('Family Hold Upon Cancellation'))) && !srTempDoc.isEmpty() && SRDrafted.Quantity__c>0){ // V2.00 - Claude                                       
                                    for(integer i=1;i<=SRDrafted.Quantity__c;i++){      
                                        SR_Doc__c objSRDoc = new SR_Doc__c();       
                                        objSRDoc.Name = srTempDoc[0].Document_Master__r.Name+'-'+i;     
                                        objSRDoc.Service_Request__c = SR.id;        
                                        objSRDoc.SR_Template_Doc__c = srTempDoc[0].Id;      
                                        objSRDoc.Status__c = 'Pending Upload';      
                                        objSRDoc.Document_Master__c = srTempDoc[0].Document_Master__c;      
                                        objSRDoc.DMS_Document_Index__c = srTempDoc[0].DMS_Document_Index__c;        
                                        objSRDoc.Group_No__c = srTempDoc[0].Group_No__c;        
                                        objSRDoc.Is_Not_Required__c = srTempDoc[0].Optional__c;     
                                        objSRDoc.Generate_Document__c = srTempDoc[0].Generate_Document__c;      
                                        objSRDoc.Document_Description_External__c = srTempDoc[0].Document_Description_External__c;      
                                        if(objSRDoc.Document_Description_External__c==null){        
                                            objSRDoc.Document_Description_External__c = srTempDoc[0].Document_Description__c;       
                                        }       
                                        objSRDoc.Sys_IsGenerated_Doc__c = srTempDoc[0].Generate_Document__c;        
                                        objSRDoc.Letter_Template__c = srTempDoc[0].Document_Master__r.LetterTemplate__c;        
                                        if(srTempDoc[0].Courier_collected_at__c!=null || srTempDoc[0].Courier_delivered_at__c!=null){       
                                            objSRDoc.Is_Not_Required__c = true;     
                                            objSRDoc.Courier_Docs__c = true;        
                                        }       
                                        if(objSRDoc.Generate_Document__c==true){        
                                            objSRDoc.Is_Not_Required__c = true;     
                                            objSRDoc.Status__c = 'Generated';       
                                            string tempString  = string.valueof(Crypto.getRandomInteger());     
                                            objSRDoc.Sys_RandomNo__c = tempString.right(4);     
                                        }       
                                        if(srTempDoc[0].Requirement__c=='Upload Copy but Original Required'){       
                                            objSRDoc.Is_Not_Required__c = false;        
                                            objSRDoc.Requirement__c = 'Copy & Original';        
                                        }else{      
                                            objSRDoc.Requirement__c = srTempDoc[0].Requirement__c;      
                                        }       
                                        string Gen_or_Upload = '';      
                                        if(objSRDoc.Generate_Document__c==true){        
                                            Gen_or_Upload = 'Generate';     
                                        }else{      
                                            Gen_or_Upload = 'Upload';       
                                        }       
                                        objSRDoc.Unique_SR_Doc__c = srTempDoc[0].Document_Master__r.Name+'_'+i+'_'+SR.id+'_'+Gen_or_Upload;     
                                        lstReqSRDocsInsert_at_Insert.add(objSRDoc);     
                                    }       
                                }
                            }
                        }
                    }
                    
                    try{
                        if(lstSRPriceItems_ReInsert!=null && lstSRPriceItems_ReInsert.size()>0){
                            for(integer i=0;i<lstSRPriceItems_ReInsert.size();i++){
                                lstSRPriceItems_ReInsert[i].Status__c = 'Added';
                            }
                            Database.SaveResult[] SRPriceItem_ReInsert_Result = Database.insert(lstSRPriceItems_ReInsert, false);
                            //insert lstSRPriceItems_ReInsert;
                            //System.debug('SRPriceItem_ReInsert_Result==>'+SRPriceItem_ReInsert_Result);
                        }
                        
                        if(lstReqSRDocsInsert_at_Update!=null && lstReqSRDocsInsert_at_Update.size()>0){
                            Database.SaveResult[] SRReqDocInsertResult = Database.insert(lstReqSRDocsInsert_at_Update, false);
                            system.debug('SRReqDocInsertResult==>'+SRReqDocInsertResult);
                        }
                        
                        if(lstReqSRDocsInsert_at_Insert!=null && lstReqSRDocsInsert_at_Insert.size()>0){
                            Database.SaveResult[] SRReqDocInsertResult = Database.insert(lstReqSRDocsInsert_at_Insert, false);
                            system.debug('SRReqDocInsertResult==>'+SRReqDocInsertResult);
                        }
                        
                    }catch(Exception ex){
                        trigger.new[0].addError(ex.getMessage());
                    }
                
                }
    
            }
            
            //this condition will execute only when SR is Submitted 
            //
            //create first step is done here.
            //assign it queue for most of the case
            // if queue is not mentiond then operating user will be the owner
            //
            if(setSRSubmittedSrIds!=null && setSRSubmittedSrIds.size()>0){
                set<id> setSRTemId = new set<id>();
                set<id> setSRIds = new set<id>();
                map<id,id> mapSROwner = new map<id,id>();
                set<id> setSROwnerId = new set<id>();
                set<id> setSRAccountId = new set<id>();
                map<id, string>mapUserLicense = new map<id, string>();
                
                map<string,Id> mapStepRecType = new map<string,Id>();//Key Step RecordType API Name and Value as RecordType Id
                for(RecordType RT:[select id,Name,DeveloperName,sObjectType from RecordType where sObjectType='Step__c']){
                    if(RT.sObjectType=='Step__c')
                        mapStepRecType.put(RT.DeveloperName,RT.id);
                }
                for(QueueSobject objQueue : [select Id,SobjectType,QueueId,Queue.Email,Queue.Name from QueueSobject WHERE SobjectType = 'Service_Request__c' OR  SobjectType = 'SR_Steps__c' OR SobjectType = 'Step__c' OR SobjectType = 'SR_Template__c']){
                   if(objQueue.SobjectType=='Step__c'){
                        mapStepQueues.put(objQueue.Queue.Name,objQueue.QueueId);
                   }
                   if(objQueue.SobjectType=='SR_Steps__c'){
                        mapSRStepQueues.put(objQueue.QueueId,objQueue.Queue.Name);
                   }
                   if(objQueue.SobjectType=='Step_Template__c'){
                        mapStepTemplateQueues.put(objQueue.QueueId,objQueue.Queue.Name);
                   }
                   if(objQueue.SobjectType=='SR_Template__c'){
                        mapSRTemplateQueues.put(objQueue.QueueId,objQueue.Queue.Name);
                   }
                }
                for(Service_Request__c SR : Trigger.new){
                    // only when status changes from draft to submitted then trgger all the action 
                    if(SR.Internal_Status_Name__c=='Submitted' && (trigger.oldmap.get(SR.id).Internal_Status_Name__c=='Draft' || trigger.oldmap.get(SR.id).Internal_Status_Name__c=='Awaiting Sanction Approval') && trigger.oldmap.get(SR.id).Internal_Status_Name__c!=SR.Internal_Status_Name__c){
                        setSRIds.add(SR.id);
                        mapSROwner.put(SR.id,SR.ownerId);
                        setSRAccountId.add(SR.Customer__c);
                        setSROwnerId.add(SR.ownerId);
                        if(SR.SR_Template__c!=null)
                            setSRTemId.add(SR.SR_Template__c);
                    }
                }
                for(User eachUser : [Select Id, Name, Profile.UserLicense.Name,Profile.UserLicense.LicenseDefinitionKey From User Where (Profile.UserLicense.LicenseDefinitionKey = 'PID_Partner_Community' OR Profile.UserLicense.LicenseDefinitionKey = 'PID_Partner_Community_Login' OR Profile.UserLicense.LicenseDefinitionKey = 'PID_Customer_Community' OR Profile.UserLicense.LicenseDefinitionKey = 'PID_Customer_Community_Login') AND Id in :setSROwnerId ]){
                   mapUserLicense.put(eachUser.id,eachUser.Profile.UserLicense.LicenseDefinitionKey);
                }
            
               /********************* Start of Code to Create Steps based for each SR_Steps related to the Service_Request's SR_Template *******************************/
                list<Step__c> lstSteps = new list<Step__c>();
                map<Id,list<SR_Steps__c>> mapSRSteps = new map<id,list<SR_Steps__c>>();
                system.debug('setSRTemId==>'+setSRTemId);
                
                set<Id> setBRIds = new set<Id>();
                
                map<string,string> mapCreateCondBRs = new map<string,string>();
                
                if(setSRTemId!=null && setSRTemId.size()>0){
                    for(SR_Steps__c srstp:[select SR_Template__c,Owner__c,Sys_Create_Condition__c,Execute_on_Submit__c,SR_Template__r.Do_not_use_owner__c,SR_Template__r.OwnerId,Do_not_use_owner__c,Estimated_Hours__c,Step_RecordType_API_Name__c,Summary__c,Start_Status__c,Step_No__c,Start_Status__r.Name,Step_Template__c,
                    Step_Template__r.Step_RecordType_API_Name__c,Step_Template__r.OwnerId,OwnerId,(select id,Business_Rule_Condition__c,Text_Condition__c,Action_Type__c from Business_Rules__r where Action_Type__c='Create Conditions' limit 1)
                     from SR_Steps__c where Start_Status__c!=null and (Sys_Create_Condition__c=false or (Execute_on_Submit__c=true and Sys_Create_Condition__c=true)) and Active__c=true and SR_Template__c IN:setSRTemId]){
                        system.debug('srstp==>'+srstp);
                        list<SR_Steps__c> lstSRSteps = new list<SR_Steps__c>();
                        if(mapSRSteps.get(srstp.SR_Template__c)!=null){
                            lstSRSteps = mapSRSteps.get(srstp.SR_Template__c);
                            lstSRSteps.add(srstp);
                            mapSRSteps.put(srstp.SR_Template__c,lstSRSteps);
                        }else{
                            lstSRSteps.add(srstp);
                            mapSRSteps.put(srstp.SR_Template__c,lstSRSteps);
                        }
                        if(srstp.Sys_Create_Condition__c==true && srstp.Execute_on_Submit__c==true && srstp.Business_Rules__r!=null && srstp.Business_Rules__r.size()>0 && srstp.Business_Rules__r[0].Text_Condition__c!=null){
                            mapCreateCondBRs.put(srstp.Id,srstp.Business_Rules__r[0].Text_Condition__c);
                        }
                    }
                }
                system.debug('mapSRSteps==>'+mapSRSteps);
                
                String queueName;
                Boolean checkCustomerQueue;
                Id SROwnerId=null;
                map<id,list<Messaging.SingleEmailMessage>> emailStepMap = new map<id,list<Messaging.SingleEmailMessage>>();
                map<id,list<Messaging.SingleEmailMessage>> allEmailStepMap = new map<id,list<Messaging.SingleEmailMessage>>();
                list<SR_Doc__c> lstReqSRDocsInsert = new list<SR_Doc__c>();
                list<Messaging.SingleEmailMessage> insertMailList = new list<Messaging.SingleEmailMessage>();
               
                list<User> usrList = new list<User>();
                map<id,list<User>>userMap = new map<id,list<User>>();
                list<EmailTemplate> ETLst = [select id,name,developername from EmailTemplate where developername = 'Notify_Step'];
                for(User eachUsr :[SELECT id,User_Email__c,Community_User_Role__c,IsActive,ContactId,Contact.AccountId FROM User WHERE Contact.AccountId IN :setSRAccountId AND IsActive=true and Contact.Email!=null and Community_User_Role__c!=null]){
                    usrList = new list<User>();
                    if(eachUsr.ContactId!=null && userMap.containsKey(eachUsr.Contact.AccountId)){
                        usrList = userMap.get(eachUsr.Contact.AccountId);
                    }
                    usrList.add(eachUsr);
                    userMap.put(eachUsr.Contact.AccountId,usrList);
                }
                if(setSRSubmittedSrIds!=null && mapCreateCondBRs!=null && mapCreateCondBRs.size()>0){
                    ctx.PreparePricingSR(setSRSubmittedSrIds);
                }
                system.debug('mapSRSteps==>'+mapSRSteps);
                for(Service_Request__c objsr:trigger.new){
                   
                    if(objsr.Internal_Status_Name__c=='Submitted' && (trigger.oldmap.get(objsr.id).Internal_Status_Name__c=='Draft' || trigger.oldmap.get(objsr.id).Internal_Status_Name__c=='Awaiting Sanction Approval') && trigger.oldmap.get(objsr.id).Internal_Status_Name__c!=objsr.Internal_Status_Name__c){
                        if(objsr.SR_Template__c!=null && mapSRSteps.get(objsr.SR_Template__c)!=null){
                            for(SR_Steps__c srstp:mapSRSteps.get(objsr.SR_Template__c)){
                                boolean bConditionmet = true;
                                if(mapCreateCondBRs!=null && mapCreateCondBRs.get(srstp.Id)!=null ){
                                    Service_Request__c FullSR = ctx.getSRFullRecord(string.valueof(objsr.id));
                                    boolean bResult = SM.ExecuteSRCreateCondition(objsr.Id,mapCreateCondBRs.get(srstp.Id),FullSR);
                                    if(bResult!=null && bResult==true){
                                        bConditionmet = true;
                                    }else{
                                        bConditionmet = false;
                                    }
                                }
                                if(bConditionmet==true){
                                    Step__c stp = new Step__c(); 
                                    stp.Step_Template__c = srstp.Step_Template__c;
                                    stp.Status__c = srstp.Start_Status__c;
                                    stp.Client_Name__c = objsr.Client_Name__c;
                                    stp.SR_Step__c = srstp.id; 
                                    if(srstp.Step_RecordType_API_Name__c!=null){
                                        if(mapStepRecType.get(srstp.Step_RecordType_API_Name__c)!=null)
                                            stp.RecordTypeId = mapStepRecType.get(srstp.Step_RecordType_API_Name__c);
                                    }else{
                                        if(srstp.Step_Template__c!=null && srstp.Step_Template__r.Step_RecordType_API_Name__c!=null && mapStepRecType.get(srstp.Step_Template__r.Step_RecordType_API_Name__c)!=null){
                                            stp.RecordTypeId = mapStepRecType.get(srstp.Step_Template__r.Step_RecordType_API_Name__c);
                                        }
                                    }
                                    queueName='';
                                    checkCustomerQueue=false;
                                    SROwnerId=null;
                                    if(srstp.Do_not_use_owner__c==false){
                                      if(string.valueOf(srstp.OwnerId).startsWith('005')){
                                        stp.OwnerId = srstp.OwnerId;
                                      }else{
                                        if(mapSRStepQueues.containsKey(srstp.OwnerId)){
                                            queueName = mapSRStepQueues.get(srstp.OwnerId);
                                        }
                                        if(queueName.contains('Client')){
                                           stp.OwnerId = mapStepQueues.get(queueName);  
                                           if(userMap.containsKey(objsr.Customer__c)){
                                             usrList = userMap.get(objsr.Customer__c);
                                             emailStepMap = SM.mailListPrepareEmails(usrList,stp.SR_Step__c,objsr.Ultimate_SR_Menu_Text__c,ETLst,queueName);
                                             if(emailStepMap!=null && emailStepMap.size()>0){
                                                allEmailStepMap.putAll(emailStepMap);
                                             }
                                           }        
                                        }else
                                        if(mapStepQueues.containsKey(queueName)){
                                            stp.OwnerId = mapStepQueues.get(queueName);
                                        }
                                      }   
                                    }else if(srstp.SR_Template__c!=null && srstp.SR_Template__r.Do_not_use_owner__c==false){
                                      if(string.valueOf(srstp.SR_Template__r.OwnerId).startsWith('005')){
                                         stp.OwnerId = srstp.SR_Template__r.OwnerId;
                                      }else{
                                        if(mapSRTemplateQueues.containsKey(srstp.OwnerId)){
                                            queueName = mapSRTemplateQueues.get(srstp.OwnerId);
                                        }
                                        if(queueName.contains('Client')){
                                           stp.OwnerId = mapStepQueues.get(queueName);  
                                           if(userMap.containsKey(objsr.Customer__c)){
                                             usrList = userMap.get(objsr.Customer__c);
                                             emailStepMap = SM.mailListPrepareEmails(usrList,stp.SR_Step__c,objsr.Ultimate_SR_Menu_Text__c,ETLst,queueName);
                                              if(emailStepMap!=null && emailStepMap.size()>0){
                                                allEmailStepMap.putAll(emailStepMap);
                                             }
                                           }            
                                        }else
                                        if(mapStepQueues.containsKey(queueName)){
                                            stp.OwnerId = mapStepQueues.get(queueName);
                                        }
                                      }  
                                    }else if(srstp.Step_Template__c!=null){
                                      if(string.valueOf(srstp.Step_Template__r.OwnerId).startsWith('005')){
                                         stp.OwnerId = srstp.Step_Template__r.OwnerId;
                                      }else{
                                        if(mapStepTemplateQueues.containsKey(srstp.OwnerId)){
                                            queueName = mapStepTemplateQueues.get(srstp.OwnerId);
                                        }
                                        if(queueName.contains('Client')){
                                           stp.OwnerId = mapStepQueues.get(queueName);  
                                           if(userMap.containsKey(objsr.Customer__c)){
                                             usrList = userMap.get(objsr.Customer__c);
                                             emailStepMap = SM.mailListPrepareEmails(usrList,stp.SR_Step__c,objsr.Ultimate_SR_Menu_Text__c,ETLst,queueName);
                                              if(emailStepMap!=null && emailStepMap.size()>0){
                                                allEmailStepMap.putAll(emailStepMap);
                                             }
                                           }            
                                        }else
                                        if(mapStepQueues.containsKey(queueName)){
                                            stp.OwnerId = mapStepQueues.get(queueName);
                                        }
                                      }   
                                    }
                                    stp.Start_Date__c = system.today();
                                    stp.Step_No__c = srstp.Step_No__c;
                                    stp.Sys_Step_Loop_No__c = srstp.Step_No__c+'_1';
                                    stp.SR__c = objsr.id;
                                    stp.Summary__c = srstp.Summary__c;
                                    
                                    //*** V1.15 Start -Added by Kaavya - Correction for fit out business hours
                                    
                                    //ID BHId = id.valueOf(Label.Business_Hours_Id);
                                    //ID BHId = objsr.SR_Group__c.equals('Fit-Out & Events') ? Business_Hours__c.getInstance('Fit-Out').Business_Hours_Id__c : id.valueOf(Label.Business_Hours_Id);
                                    /* V1.16 - Claude - Start */
                                    Cls_StepBusinessHoursUtils.checkOwner(srstp.Owner__c);
                                    ID BHId = String.isNotBlank(objsr.SR_Group__c) && objsr.SR_Group__c.equals('Fit-Out & Events') ? Cls_StepBusinessHoursUtils.getBusinessHoursId() : id.valueOf(Label.Business_Hours_Id);
                                    /* V1.16 - Claude - End */
                                    
                                    //V1.15 End ****
                                   
                                    if(srstp.Estimated_Hours__c!=null){
                                        Long sla = srstp.Estimated_Hours__c.longvalue();
                                        sla=sla*60*60*1000L;
                                        datetime CreatedTime = system.now(); 
                                        stp.Due_Date__c=BusinessHours.add(BHId,CreatedTime,sla);
                                    }
                                   
                                    system.debug('stp from SR trigger=>'+stp);
                                    lstSteps.add(stp);
                                }
                                
                            }
                        }
                    }
                }
                boolean hasException = false;
                try{
                    if(!lstSteps.isEmpty() && lstSteps!=null){
                        insert lstSteps;        
                    }
                    list<Messaging.SingleEmailMessage> mailList = new list<Messaging.SingleEmailMessage>();
                    for(Step__c eachStp : lstSteps){
                      if(allEmailStepMap.containsKey(eachStp.SR_Step__c)){
                        mailList = allEmailStepMap.get(eachStp.SR_Step__c);
                        for(Messaging.SingleEmailMessage email: mailList){
                          email.setWhatId(eachStp.Id);
                          insertMailList.add(email);
                        }    
                      }
                    }
                    if(insertMailList!=null && insertMailList.size() > 0){
                        Messaging.sendEmail(insertMailList);
                    }
                }catch(DMLException ex){
                    system.debug('Exception===>'+ex.getMessage() + '; Line Number' + ex.getLineNumber());
                    trigger.new[0].addError(ex.getdmlMessage(0));
                    hasException = true;
                }catch(CommonCustomException ex){
                    
                }catch(Exception e){
                    
                }
                
                
                
               /********************* End of Code to Create Steps based for each SR_Steps related to the Service_Request's SR_Template *******************************/
            }
            
            /* To check if all the steps are closed on the cosure of the SR */
                list<Service_Request__c> lstClosedSRs = new list<Service_Request__c>();
                set<id> setClosedSRIds = new set<id>();
                for(Service_Request__c objsr:trigger.new){
                    system.debug('Old Internal Status Name==>'+trigger.oldmap.get(objsr.id).Internal_Status_Name__c);
                    system.debug('New Internal Status Name==>'+objsr.Internal_Status_Name__c);
                    system.debug('Is Closed Status==>'+objsr.isClosedStatus__c);
                    if(trigger.oldmap.get(objsr.id).Internal_Status_Name__c!=objsr.Internal_Status_Name__c && objsr.isClosedStatus__c){
                        setClosedSRIds.add(objsr.id);
                        lstClosedSRs.add(objsr);
                    }
                }
                
                if(lstClosedSRs!=null && lstClosedSRs.size()>0){
                    list<SR_Price_Item__c> SRPriceLst = new list<SR_Price_Item__c>();
                    set<string> estRocRectypes = new set<string>{'Application_of_Registration','License_Renewal','Change_of_Entity_Name'};
                    for(SR_Price_Item__c SRPrice : [SELECT id,Status__c,ServiceRequest__c,Product__c,Product__r.Family,ServiceRequest__r.RecordType.DeveloperName from SR_Price_Item__c WHERE (Status__c='Added' OR Status__c='Blocked') AND ServiceRequest__c IN :lstClosedSRs]){
                       
                        // SR_Price_Item__c updateSRPrice = new SR_Price_Item__c(id=SRPrice.id,Status__c='Consumed');  Commented for V2.1
                        //Start V2.1
                        SR_Price_Item__c updateSRPrice;
                        if(estRocRectypes.contains(SRPrice.ServiceRequest__r.RecordType.DeveloperName) && SRPrice.Product__r.Family=='GS' && (SRPrice.Status__c=='Added' || SRPrice.Status__c=='Blocked')) // V2.1
                            updateSRPrice = new SR_Price_Item__c(id=SRPrice.id,Status__c='Blocked');
                        else
                             updateSRPrice = new SR_Price_Item__c(id=SRPrice.id,Status__c='Consumed');
                        // End V2.1
                        SRPriceLst.add(updateSRPrice);
                    }
                    if(SRPriceLst!=null && SRPriceLst.size()>0){
                        try{
                            update SRPriceLst;
                        }catch(Exception e){
                            trigger.new[0].addError(e.getMessage());
                        }
                    }
                }
            /* End - To check if all the steps are closed on the cosure of the SR */
        }
    }
    
    System.debug(Trigger.new.Size());
    
    //V1.6 - added by Saima on 22-Aug-2015 - RORP Validations
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
        
        System.debug('Entered >> BeforeInsUpValidations');
        
        string result = SRValidations.BeforeInsUpValidations(trigger.new,trigger.oldMap);
        if(result != 'Success'){
            trigger.new[0].addError(result);
        }
        
        //V1.19
        /*Map<string,Restricted_Nationality__c> RestrictedNationality = Restricted_Nationality__c.getAll();
        for(Service_Request__c objSR : trigger.new){
            if((trigger.isInsert || objSR.External_Status_Name__c == 'Draft' ) && objSR.sr_group__c == 'GS' && objSR.Type_of_Visit_Visa__c == 'Short Term (30 Days)' && (objSR.record_type_name__c == 'Business_Visit_Visa' || objSR.record_type_name__c == 'Personal_Visit_Visa')){
                if(RestrictedNationality.ContainsKey(objSR.Nationality_list__c)){
                    //trigger.new[0].addError('Short Term - 30 days visit visa is not applicable for the selected nationality');    
                }       
            }
        }*/
    }
    //added by Ravi for Internal Transfer DIFC to DIFC
    if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate) && GsHelperCls.AlreadyEntered != true ){
        GsHelperCls.AlreadyEntered = true;
        Service_Request__c objTempSR;
        list<Id> Appids= new list<Id>();
        list<Id> Sprids= new list<Id>();
        //map<Id,Id> SRContMap=new map<Id,Id>();
        map<Id,Contact> ContIdMap = new map<Id,Contact>();
        map<Id,Contact> SprIdMap = new map<Id,Contact>();
        list<Id> lstConIds = new list<Id>();
        map<Id,Service_Request__c> mapRelatedSR = new map<Id,Service_Request__c>();
        
        for(Service_Request__c objSR : trigger.new){
            if(( trigger.isInsert || objSR.External_Status_Name__c == 'Draft' ) && objSR.Record_Type_Name__c == 'Employment_Visa_from_DIFC_to_DIFC'){
                if(objSR.Pre_GoLive__c != true)
                    objTempSR = objSR;
            }else if(( trigger.isInsert || objSR.External_Status_Name__c == 'Draft' ) && objSR.Record_Type_Name__c == 'Access_Card' && objSR.Service_Category__c == 'New Seconded Employee Card' && objSR.Type_of_Request__c == 'Secondment within DIFC'){
                //added by Ravi on 20th May,2015 -
                if(objSR.Pre_GoLive__c != true)
                    objTempSR = objSR;
            }
            if(objSR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Amendment' || objSR.Record_Type_Name__c == 'Non_DIFC_Sponsorship_Visa_Amendment' || objSR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Renewal'){
                if(objSR.Contact__c!=null){
                    system.debug('IIIInside if');
                    Appids.add(objSR.Contact__c);
                    //SRContMap.put(objSR.Contact__c,objSR.id);
                }
            }
            //Added by Kaavya for Sponsor Paid Family File-  Apr27th,2015
            if(objSR.Sponsor__c!=null){
                Sprids.add(objSR.Sponsor__c);
            }
            //added by Ravi on 28th April to populate the fields on SR for DNRD e-form
            if(trigger.isUpdate){
                System.debug(objSR.Linked_SR__c);
                System.debug(objSR.Record_Type_Name__c);    
            }
            
            if((trigger.isInsert || (trigger.isUpdate && objSR.Linked_SR__c != trigger.oldMap.get(objSR.Id).Linked_SR__c)) && objSR.Linked_SR__c != null && (objSR.Record_Type_Name__c == 'Re_issue_Entry_Permit' || objSR.Record_Type_Name__c == 'Re_issue_Family_Entry_Permit')){
                lstConIds.add(objSR.Linked_SR__c);
            }
        }
        if(objTempSR != null){
            Boolean isExist = false,isClientExist = false;
            string PreviousSponsorId;
            for(Account objAcc : [select Id,Name,Index_Card__c,Index_Card__r.Id_No__c,Active_License__c,Active_License__r.Name from Account where Active_License__r.Name =: objTempSR.DIFC_License_No__c AND Name =: objTempSR.Company_Name__c AND Index_Card__r.Id_No__c =: objTempSR.Sponsor_Establishment_No__c]){
                if(objAcc.Id == objTempSR.Customer__c){
                    objTempSR.addError('Current sponsor and sponsor are same.');
                }
                isClientExist = true;
                PreviousSponsorId = objAcc.Id;
                //objTempSR.Previous_Sponsor_Principle_User__c = objAcc.Principal_User__c;
            }
            if(isClientExist == false){
                objTempSR.addError('Current sponsor is not found in system with provided details.');
            }
            
            for(Contact objCon : [select id,FirstName,LastName,Job_Title__c,Passport_No__c,Date_of_Issue__c,Passport_Expiry_Date__c from Contact where Account.Active_License__r.Name =: objTempSR.DIFC_License_No__c AND Id IN (select Contact__c from Document_Details__c where Document_Type__c ='Employment Visa' AND Document_Number__c=:objTempSR.Residence_Visa_No__c)]){
                isExist = true;
                ContIdMap.put(objCon.Id,objCon);
                objTempSR.Contact__c = objCon.Id;
            }
            if(isExist == false){
                objTempSR.addError('Employee not found with the provided License number and Visa number.');
            }
            if(objTempSR.Record_Type_Name__c == 'Employment_Visa_from_DIFC_to_DIFC'){
                for(User objUser : [select Id,ContactId from User where Contact.AccountId =: PreviousSponsorId AND IsActive = true AND Community_User_Role__c INCLUDES ('Employee Services') ]){
                    objTempSR.Previous_Sponsor_Principle_User__c = objUser.ContactId;
                }
            }
            for(Service_Request__c objSR : trigger.new){
                if((trigger.isInsert || objSR.External_Status_Name__c == 'Draft') && objSR.Record_Type_Name__c == 'Employment_Visa_from_DIFC_to_DIFC'){
                    objSR.Contact__c = objTempSR.Contact__c;
                    objSR.Previous_Sponsor_Principle_User__c = objTempSR.Previous_Sponsor_Principle_User__c;
                }
            }
        }
        
        if(Appids.size()>0){
            List<Contact> Applist=[select id,FirstName,LastName,Job_Title__c,Passport_No__c,Date_of_Issue__c,Passport_Expiry_Date__c from Contact where id in:Appids];
            for(Contact C:Applist){
                //ContIdMap.put(SRContMap.get(C.id),C);
                ContIdMap.put(C.Id,C);
            }
        }
        //Added by Kaavya for Sponsor Paid Family File-  Apr27th,2015
        if(Sprids.size()>0){
            List<Contact> Sprlist=[select id,Paid_Family_File__c,FirstName,LastName,Job_Title__c,Passport_No__c,Date_of_Issue__c,Passport_Expiry_Date__c from Contact where id in:Sprids];
            for(Contact C:Sprlist){
                //ContIdMap.put(SRContMap.get(C.id),C);
                SprIdMap.put(C.Id,C);
            }
        }
        //added by Ravi on 28th April
        if(!lstConIds.isEmpty()){
            for(Service_Request__c objContact : [select Id,Gender__c,Passport_Type__c,Religion__c,Marital_Status__c,Mother_Full_Name__c,First_Language__c,Second_Language__c,Third_Language__c,
                                Sponsor_Gender__c,Sponsor_Nationality__c,Relation__c,Sponsor_Date_of_Birth__c,Sponsor_Passport_Expiry__c,Sponsor_Passport_No__c,Sponsor_Monthly_Salary_AED__c,
                                Sponsor_Mother_Full_Name__c,Sponsor__c
                                from Service_Request__c where Id IN : lstConIds]){
                //ContIdMap.put(objContact.Id,objContact);
                mapRelatedSR.put(objContact.Id,objContact);
            }
        }
        for(Service_Request__c objSR : trigger.new){
            //Added by Kaavya for V1.5 Additional Charge for amendments
            if(objSR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Amendment' || objSR.Record_Type_Name__c == 'Non_DIFC_Sponsorship_Visa_Amendment' || objSR.Record_Type_Name__c == 'Employment_Visa_from_DIFC_to_DIFC'){
                if(objSR.Contact__c != null && ContIdMap.containsKey(objSR.Contact__c)){
                    Contact Appl = ContIdMap.get(objSR.Contact__c); 
                    integer chgno=0;
                    if(Appl.FirstName != objSR.First_Name__c ||Appl.LastName != objSR.Last_Name__c)
                        chgno++; 
                    system.debug('Name=='+chgno);
                    if(Appl.Job_Title__c != objSR.Occupation_GS__c) 
                        chgno++;  
                    system.debug('Job=='+chgno);              
                    if(Appl.Passport_No__c != objSR.Passport_Number__c ||Appl.Date_of_Issue__c != objSR.Passport_Date_of_Issue__c ||Appl.Passport_Expiry_Date__c != objSR.Passport_Date_of_Expiry__c) 
                        chgno++;
                    system.debug('Passport=='+chgno);
                    if(chgno>1 ||(objSR.Record_Type_Name__c == 'Employment_Visa_from_DIFC_to_DIFC' && chgno>=1))
                        objSR.Sys_Addl_Charge__c= true;
                    else
                        objSR.Sys_Addl_Charge__c= false;
                }
            }
            if(objSR.Record_Type_Name__c == 'Employment_Visa_from_DIFC_to_DIFC' || objSR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Renewal'){
                //added by Ravi on 25th April 2015 as per Kaavya Comments 
                //- If there is Job Title change in then Update the "Change Address" flag
                if(objSR.Contact__c != null && ContIdMap.containsKey(objSR.Contact__c)){
                    Contact Appl = ContIdMap.get(objSR.Contact__c);
                    if(Appl.Job_Title__c != objSR.Occupation_GS__c){
                        objSR.Change_Address__c = true;
                    }
                }
            }
            //Added by Kaavya for Sponsor Paid Family File-  Apr27th,2015
            if(objSR.Sponsor__c!=null && SprIdMap.containsKey(objSR.Sponsor__c)){
                Contact spon =SprIdMap.get(objSR.Sponsor__c);
                if(spon.Paid_Family_File__c==true)
                    objSR.Sponsor_Paid_Family_File__c=true;
            }
            //added by Ravi on 28th April
            if(!mapRelatedSR.isEmpty() && objSR.Linked_SR__c != null && mapRelatedSR.containsKey(objSR.Linked_SR__c) && (objSR.Record_Type_Name__c == 'Re_issue_Entry_Permit' || objSR.Record_Type_Name__c == 'Re_issue_Family_Entry_Permit')){ 
                objSR.Gender__c = mapRelatedSR.get(objSR.Linked_SR__c).Gender__c;
                objSR.Passport_Type__c = mapRelatedSR.get(objSR.Linked_SR__c).Passport_Type__c;
                objSR.Religion__c = mapRelatedSR.get(objSR.Linked_SR__c).Religion__c;
                objSR.Marital_Status__c = mapRelatedSR.get(objSR.Linked_SR__c).Marital_Status__c;
                objSR.Mother_Full_Name__c = mapRelatedSR.get(objSR.Linked_SR__c).Mother_Full_Name__c;
                objSR.First_Language__c = mapRelatedSR.get(objSR.Linked_SR__c).First_Language__c;
                objSR.Second_Language__c = mapRelatedSR.get(objSR.Linked_SR__c).Second_Language__c;
                objSR.Third_Language__c = mapRelatedSR.get(objSR.Linked_SR__c).Third_Language__c;
                
                if(objSR.Record_Type_Name__c == 'Re_issue_Family_Entry_Permit'){
                    objSR.Sponsor_Gender__c = mapRelatedSR.get(objSR.Linked_SR__c).Sponsor_Gender__c;
                    objSR.Sponsor_Nationality__c = mapRelatedSR.get(objSR.Linked_SR__c).Sponsor_Nationality__c;
                    objSR.Relation__c = mapRelatedSR.get(objSR.Linked_SR__c).Relation__c;
                    objSR.Sponsor_Date_of_Birth__c = mapRelatedSR.get(objSR.Linked_SR__c).Sponsor_Date_of_Birth__c;
                    objSR.Sponsor_Passport_Expiry__c = mapRelatedSR.get(objSR.Linked_SR__c).Sponsor_Passport_Expiry__c;
                    objSR.Sponsor_Passport_No__c = mapRelatedSR.get(objSR.Linked_SR__c).Sponsor_Passport_No__c;
                    objSR.Sponsor_Monthly_Salary_AED__c = mapRelatedSR.get(objSR.Linked_SR__c).Sponsor_Monthly_Salary_AED__c;
                    objSR.Sponsor_Mother_Full_Name__c = mapRelatedSR.get(objSR.Linked_SR__c).Sponsor_Mother_Full_Name__c;
                    objSR.Sponsor__c = mapRelatedSR.get(objSR.Linked_SR__c).Sponsor__c;
                }
            }
        }
    }
    if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
        //added by Ravi on 24th April, to create the the Contact - SR History
        map<string,Contact_Field_Tracking__c> mapConFieldTrack = new map<string,Contact_Field_Tracking__c>();
        map<Id,Contact> mapConSrIds = new map<Id,Contact>();
        list<Contact_Tracking__c> lstContactHistory = new list<Contact_Tracking__c>();
        Contact_Tracking__c objCT;
        boolean isFineProcess = false; //V1.14
        boolean clearFine = false;
        boolean isExitFine = false;
        if(Contact_Field_Tracking__c.getAll() != null && Contact_Field_Tracking__c.getAll().isEmpty() == false){
            for(Contact_Field_Tracking__c objCS : Contact_Field_Tracking__c.getAll().values()){
                if(objCS.Contact_Field__c != null && objCS.Contact_Field__c != '' && objCS.SR_Field__c != null && objCS.SR_Field__c != '' ){
                    mapConFieldTrack.put(objCS.SR_Field__c,objCS);
                }
            }
        }
        if(!mapConFieldTrack.isEmpty()){
            set<string> setTypes = new set<string>();
            for(Service_Request__c objSR : trigger.new){
                if(objSR.Contact__c != null && Contact_History_Record_Types__c.getAll().containsKey(objSR.Record_Type_Name__c)){ // objSR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Renewal' || objSR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Amendment'
                    mapConSrIds.put(objSR.Contact__c,null);
                }
            }
            if(!mapConSrIds.isEmpty()){
                string sQuery = 'select ';
                for(Contact_Field_Tracking__c cField : mapConFieldTrack.values()){
                    sQuery += cField.Contact_Field__c+',';
                }
                list<Id> lst = new list<Id>();
                lst.addAll(mapConSrIds.keySet());
                sQuery += 'Id from Contact where Id IN :lst ';
                for(Contact objCon : Database.query(sQuery)){
                    mapConSrIds.put(objCon.Id,objCon);
                }
                
                for(Service_Request__c objSR : trigger.new){
                    if(mapConSrIds.containsKey(objSR.Contact__c) && Contact_History_Record_Types__c.getAll().containsKey(objSR.Record_Type_Name__c)){
                        Contact objCon = mapConSrIds.get(objSR.Contact__c);
                        for(string sField : mapConFieldTrack.keySet()){
                            string cField = mapConFieldTrack.get(sField).Contact_Field__c;
                            if(objSR.get(sField) != objCon.get(cField) && (trigger.isInsert || objSR.get(sField) != trigger.oldMap.get(objSR.Id).get(sField))){
                                objCT = new Contact_Tracking__c();
                                objCT.Service_Request__c = objSR.Id;
                                objCT.Contact_Value__c = ''+objCon.get(cField);
                                objCT.SR_Value__c = ''+objSR.get(sField);
                                objCT.Name = mapConFieldTrack.get(sField).Name;
                                lstContactHistory.add(objCT);
                            }
                        }
                    }
                }
                if(!lstContactHistory.isEmpty())
                    insert lstContactHistory;
            }
        }
        //end on 24th April
        //Added by Ravi on 28th April 
        if(trigger.isInsert){
            
            String IUId;
            map<Id,Service_Request__c> mapSRs = new map<Id,Service_Request__c>();
            list<amendment__c> insuranceDetails = new list<amendment__c>();
            for(User objUser : [select Id from User where (Username='difcadmin@nsigulf.com.uat' OR Username='integration@difc.com' OR Username='integration@difc.com.uatfull')]) //integration@difc.com
                IUId = objUser.Id;
            if(IUId != null){
                for(Service_Request__c obj : trigger.new){
                 // obj.OwnerId = IUId;
                    mapSRs.put(obj.Id,new Service_Request__c(id=obj.Id,OwnerId=IUId));// <-- V1.13 Commented by Claude. 19-4-2016. Replaced by additional logic
                    /*if(obj.Record_Type_Name__c.equals('Renewal_Fit_Out_Permit')){ // V1.13 - Claude - Add only when the record type of the SR is 'Renewal of Fit out Permit'
                        mapSRs.put(obj.Id,new Service_Request__c(id=obj.Id,OwnerId=IUId));  
                    }*/
                    
                }
            }
            
            if(!mapSRs.isEmpty()){ //v.13 - Claude - Additional filter for 'Renewal of Fit-out Permit' Type 
                //added by Ravi on 3rd May,2015 
                map<string,String> mapSRTemplateGroups = new map<string,string>();
                list<Id> lstInductionSRIds = new list<Id>();
                for(Service_Request__c objSR : trigger.new){
                    if(objSR.Record_Type_Name__c != null && (objSR.SR_Group__c == null || objSR.SR_Group__c == '') ){
                        mapSRTemplateGroups.put(objSR.Record_Type_Name__c,'');
                    }
                    if(objSR.Linked_SR__c != null)      
                        lstInductionSRIds.add(objSR.Linked_SR__c);
                }
                if(!mapSRTemplateGroups.isEmpty() && test.isRunningTest() == false){
                    for(SR_Template__c objTemp : [select Id,SR_Group__c,SR_RecordType_API_Name__c from SR_Template__c where SR_RecordType_API_Name__c IN : mapSRTemplateGroups.keySet()])
                        mapSRTemplateGroups.put(objTemp.SR_RecordType_API_Name__c,objTemp.SR_Group__c);
                }
                
                System.debug('The induction IDs: ' + lstInductionSRIds);
                System.debug('The new SRs ->' + trigger.new); 
                
                //System.assertEquals(false,true);
                
                //added by Swati on 23 jan 2016     
                if(!lstInductionSRIds.isempty()){      
                    List<Service_Request__c> tempList = new List<Service_Request__c>();     
                    tempList = [select id, Record_Type_Name__c,(select id,recordTypeId,Amendment_Type__c,ServiceRequest__c,start_date__c,end_date__c from amendments__r where record_type_name__c= 'Contractor_Insurance')      
                                from Service_Request__c where id in : lstInductionSRIds];
                    System.debug('The list of SRs ->' + tempList);       
                    if(!tempList.isEmpty()){        
                        for(Service_Request__c tempObj : tempList){
                            System.debug('@@ Getting Amendments');
                            insuranceDetails = tempObj.amendments__r;
                        }       
                    }  
                }       
                
                if(!insuranceDetails.isEmpty()){       
                    System.debug('@@ Getting Amendments');
                    for(Service_Request__c objSR : trigger.new){   
                        if(objSr.Record_Type_Name__c.equals('Renewal_Fit_Out_Permit')){     
                            list<amendment__c> tempList = new list<amendment__c>(); 
                            for(amendment__c obj : insuranceDetails){       
                                amendment__c newAmendment = new amendment__c();     
                                newAmendment.ServiceRequest__c = objSR.id;      
                                newAmendment.start_date__c = obj.start_date__c;     
                                newAmendment.end_date__c = obj.end_date__c;         
                                newAmendment.recordTypeId = obj.recordTypeId;              
                                newAmendment.Amendment_Type__c = obj.Amendment_Type__c;         
                                tempList.add(newAmendment);     
                            }  
                            insert tempList;        
                        }
                    }      
                }        
                //End
                if(!mapSRTemplateGroups.isEmpty()){
                    for(Service_Request__c objSR : trigger.new){
                        Service_Request__c objTemp = mapSRs.containsKey(objSR.Id) ? mapSRs.get(objSR.Id) : new Service_Request__c(Id=objSR.Id);
                        if(mapSRTemplateGroups.containsKey(objSR.Record_Type_Name__c)){
                            objTemp.SR_Group__c = mapSRTemplateGroups.get(objSR.Record_Type_Name__c);
                        }
                        mapSRs.put(objTemp.Id,objTemp);
                    }
                }
                
                if(!mapSRs.isEmpty())
                    update mapSRs.values();
            }
            //v.13 - Claude - End    
            
            //V1.14
            
            for(Service_Request__c objSR : trigger.new){
              if(objSR.record_type_name__c == 'Issue_Fine' && objSR.Fine_Amount_AED__c!=null && objSR.Type_of_Request__c=='Other'){
                isFineProcess = true;
              }  
            }  
        }
        
        //V1.8
        if(trigger.isUpdate){
            list<Id> lstIds = new list<Id>();
            list<Id> lstErrorIds = new list<Id>();
            for(Service_Request__c objSR : trigger.new){
                if(objSR.SR_Group__c == 'RORP' && objSR.SAP_OBELNR__c != null && trigger.oldMap.get(objSR.Id).SAP_OBELNR__c != objSR.SAP_OBELNR__c){
                    lstIds.add(objSR.Id);
                }
                if(objSR.SR_Group__c == 'RORP' && objSR.Is_Error_From_SAP__c && trigger.oldMap.get(objSR.Id).Is_Error_From_SAP__c != objSR.Is_Error_From_SAP__c){
                    lstErrorIds.add(objSR.Id);
                }
            }
            string res = lstIds.isEmpty() ? 'Success' :SRValidations.StepPriceItemStatusChange(lstIds);
            if(res != 'Success'){
                throw new CommonCustomException(res);
            }
            if(!lstErrorIds.isEmpty()){
                res = lstErrorIds.isEmpty() ? 'Success' :SRValidations.UpdateSAPError(lstErrorIds);
                if(res != 'Success'){
                    throw new CommonCustomException(res);
                }
            }
            
            //V1.12 & V1.14
            
            map<string,string> mapHistoryFields = new map<string,string>();
            map<string,History_Tracking__c> mapHistoryCS = new map<string,History_Tracking__c>();
            if(History_Tracking__c.getAll()!=null){
                mapHistoryCS = History_Tracking__c.getAll();//Getting the Objects from Custom Setting which has to be displayed in the screen for the users
                for(History_Tracking__c objHisCS:mapHistoryCS.values()){
                    if(objHisCS.Object_Name__c!=null && objHisCS.Object_Name__c.tolowercase()=='service_request__c' && objHisCS.Field_Name__c!=null){
                        mapHistoryFields.put(objHisCS.Field_Name__c,objHisCS.Field_Label__c);
                    }
                }
            }
            list<Service_Request_History__c> lstEntHist = new list<Service_Request_History__c>();
            for(service_request__c acc:trigger.new){
                if(RecursiveControlCls.runOnlyOnceToTrackHistory==false && mapHistoryFields!=null && mapHistoryFields.size()>0 && acc!=trigger.oldMap.get(acc.Id)){
                    for(string AccFld:mapHistoryFields.keyset()){
                        if(acc.get(AccFld) != trigger.oldMap.get(acc.Id).get(AccFld)){
                            Service_Request_History__c objHist = new Service_Request_History__c();
                            objHist.Service_Request__c = acc.Id;
                            objHist.Last_Modified_Date__c = system.now();
                            objHist.Last_Modified_By__c = userinfo.getUserId();
                            objHist.Modified_Field_Name__c = mapHistoryFields.get(AccFld);
                            objHist.Modified_Field_Label__c = AccFld;
                            if(trigger.oldMap.get(acc.Id).get(AccFld)!=null){
                                objHist.Old_Value__c = trigger.oldMap.get(acc.Id).get(AccFld)+'';
                            }else{
                                objHist.Old_Value__c = 'blank';
                            }
                            if(acc.get(AccFld)!=null){
                                objHist.New_Value__c = acc.get(AccFld)+'';
                            }else{
                                objHist.New_Value__c = 'blank';
                            }
                            lstEntHist.add(objHist);
                        }
                    }
                }
            }
            
            if(lstEntHist!=null && lstEntHist.size()>0){
                try{
                    insert lstEntHist;
                    RecursiveControlCls.runOnlyOnceToTrackHistory = true;
                }catch(Exception e){
                
                }   
            }
            //v.13 - Claude - Added validation when a fit-out permit is updated
            
            //V1.14
            list<id> accountId = new list<id>();
            for(Service_Request__c objSR : trigger.new){//V1.17
              if(objSR.record_type_name__c == 'Issue_Fine'  && 
                  objSR.Fine_Amount_AED__c!=null && objSR.Fine_Amount_AED__c != trigger.oldmap.get(objSR.id).Fine_Amount_AED__c){
                
                isFineProcess = true;
              }  
              //V1.17
              else if((objSR.record_type_name__c == 'Transfer_from_DIFC' || 
                  objSR.record_type_name__c == 'De_Registration' || objSR.record_type_name__c == 'Dissolution') && 
                  objSR.Fine_Amount_AED__c!=null && objSR.Fine_Amount_AED__c != trigger.oldmap.get(objSR.id).Fine_Amount_AED__c &&
                  objSR.External_Status_Name__c == 'Submitted'){
                isExitFine = true;
                accountId.add(objSR.customer__c);
              }
              else if((objSR.record_type_name__c == 'Issue_Fine' || objSR.record_type_name__c == 'Transfer_from_DIFC' || 
                     objSR.record_type_name__c == 'De_Registration' || objSR.record_type_name__c == 'Dissolution') && objSR.Fine_Amount_AED__c==null 
                     && objSR.Fine_Amount_AED__c != trigger.oldmap.get(objSR.id).Fine_Amount_AED__c &&
                     objSR.External_Status_Name__c == 'Submitted'){
                clearFine = true;  
              }
              else if((objSR.record_type_name__c == 'Transfer_from_DIFC' || 
                  objSR.record_type_name__c == 'De_Registration' || objSR.record_type_name__c == 'Dissolution') && 
                  objSR.External_Status_Name__c == 'Pending Dissolution' && objSR.Fine_Amount_AED__c != trigger.oldmap.get(objSR.id).Fine_Amount_AED__c
                  && objSR.External_Status_Name__c == trigger.oldmap.get(objSR.id).External_Status_Name__c){
                  objSR.addError('Step has already been verified by ROC officer fine cannot be modified');  
              }
            } 
            //V1.17
            if(isExitFine == true && accountId!=null && !accountId.isEmpty()){
              map<id,Account> mapOfFine = new map<id,Account>();
            for(Account obj : [select id,fine_not_Paid__c from Account where Id IN : accountId]){
              if(string.isBlank(obj.fine_not_Paid__c)){
                trigger.new[0].addError('No Fine exist at client level in field Fine Not Paid, Please clear Fine Amount (AED)');  
              }
              else{
                isFineProcess = true;  
              }
            }  
            }
            
            
            /*Set<Id> srIds = new Set<Id>();
            Map<Id,Service_Request__c> srNewMap = new Map<Id,Service_Request__c>();

            for(Service_Request__c srObj : Trigger.New){
                srIds.add(srObj.Id);
            }
            
            // get the related amendments   
            List<Service_Request__c> srSimilar = [select id,(select id,recordTypeId,Amendment_Type__c,ServiceRequest__c,start_date__c,end_date__c from amendments__r where record_type_name__c=: 'Contractor_Insurance')      
                                from Service_Request__c where id in : srIds];
                                
            // map the similar records
            for(Service_Request__c srRecord : srSimilar){
                srNewMap.put(srRecord.Id,srRecord);
            }
            
            // iterate through each of the new SRs to check the date
            for(Service_Request__c srObj : Trigger.New){
                
                if(srObj.Record_Type_Name__c.equals('Renewal_Fit_Out_Permit')){
                    CC_cls_FitOutandEventCustomCode.checkRenewalDates(srObj,srNewMap.get(srObj.Id));    
                }
            }*/
            
        }
        
       //V1.14
       
        if(isFineProcess == true){
          list <SR_Price_Item__c> insertFine = new list <SR_Price_Item__c>();
          list<product2> objProd = [select id,(select id,Material_Code__c from Pricing_Lines__r where Material_Code__c='ROCP-004') from product2 where ProductCode=:'Fine' and IsActive=:true];
          list<SR_Price_Item__c> deleteFine = [select id from SR_Price_Item__c where servicerequest__c IN : trigger.new and Material_Code__c =: 'ROCP-004' and Status__c =: 'Added'];
          if(objProd!=null && !objProd.isEmpty()){
            for(Service_Request__c objSR : trigger.new){
              SR_Price_Item__c objPriceItem = new SR_Price_Item__c();
                objPriceItem.product__c = objProd[0].id;
                objPriceItem.ServiceRequest__c = objSR.id;
                objPriceItem.Status__c = 'Added';
                objPriceItem.Price__c = objSR.Fine_Amount_AED__c;
                objPriceItem.Pricing_Line__c = objProd[0].Pricing_Lines__r[0].id;
                insertFine.add(objPriceItem);
            }
            if(deleteFine!=null && !deleteFine.isEmpty()){
                  deleteFine[0].Status__c = 'Cancelled';
                  update deleteFine;
              }
            if(insertFine!=null && !insertFine.isEmpty()){
                  insert  insertFine;
              }
          }
        }
        
        if(clearFine == true){
          list<SR_Price_Item__c> deleteFine = [select id from SR_Price_Item__c where servicerequest__c IN : trigger.new and Material_Code__c =: 'ROCP-004' and Status__c =: 'Added'];  
          if(deleteFine!=null && !deleteFine.isEmpty()){
                deleteFine[0].Status__c = 'Cancelled';
                update deleteFine;
            }
        }
    }
}