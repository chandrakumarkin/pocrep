trigger AttachmentValidationTrigger on Attachment (before insert,before update) {
    //Check custom label stored user id same as running user id ? if not the same run the trigger 
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled)
    {
        if(trigger.isBefore){
            if(trigger.isInsert || trigger.isUpdate){
                AttachmentTriggerHandler.validateExtension(trigger.new);
            }
            if(trigger.isInsert){
                //v1.1: Digital Onboarding - create content document for attachments created by drawloop
                AttachmentTriggerHandler.CreateContentVersion(trigger.new);
            }
        }
    }
}