/*
 	Author		:Rajil Ravindran
	Date		:01-Sep-2019
	Description	:Trigger for TrainingMaster
*/
trigger TrainingMasterTrigger on Training_Master__c (before insert,before update, after insert, after update) {
    if(trigger.isBefore){
        if(trigger.isInsert)
            TrainingMasterTriggerHandler.Execute_BI(Trigger.New);
        if(trigger.isUpdate)
            TrainingMasterTriggerHandler.Execute_BU(Trigger.New, Trigger.oldMap);
    }
    if(trigger.isAfter){
        if(trigger.isInsert)
            TrainingMasterTriggerHandler.Execute_AI(Trigger.New);
        if(trigger.isUpdate)
            TrainingMasterTriggerHandler.Execute_AU();
    }
}