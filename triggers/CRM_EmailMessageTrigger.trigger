trigger CRM_EmailMessageTrigger on EmailMessage (before insert) {
	
	CRM_cls_OpportunityUtils.validateOpportunityEmailMessages(Trigger.New);
    
}