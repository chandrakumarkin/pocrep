/******************************************************************************************
 *  Name    : TRG_Create_Receipt 
 *  Author      : Kaavya Raghuram
 *  Company     : NSI JLT
 *  Date        : 2014-09-17
 *  Description : This trigger is to call the class upon successful payment
           which calls the webservice to create a receipt in SAP                         
--------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------           
    V1.0    31-03-2016  Swati         Added the logic to send notification to portal user on recharge.
*/

trigger TRG_Create_Receipt on Receipt__c ( before insert, before update, after update, after insert) {
    if(system.isFuture()) return;
    if(Block_Trigger__c.getAll().get('Block') == null || ( Block_Trigger__c.getAll().get('Block') != null && Block_Trigger__c.getAll().get('Block').Block_Receipt_Trg__c == false)){
        if(trigger.isBefore){
            
            for(Receipt__c objReceipt : trigger.new){
                if(objReceipt.Payment_Status__c == 'Success' && objReceipt.Transaction_Date__c == null){
                    objReceipt.Transaction_Date__c = system.now();
                }
                if(objReceipt.Payment_Status__c == 'Success' && objReceipt.Pushed_to_SAP__c != true)
                    objReceipt.Moved_to_SAP_by__c = Userinfo.getUserId();
            }
        }else if(trigger.isAfter){
            list<Id> lstReceiptIds = new list<Id>();
            for(Receipt__c objReceipt :trigger.new){
                if(objReceipt.Pushed_to_SAP__c != true && objReceipt.Payment_Status__c == 'Success' && objReceipt.Moved_to_SAP__c != true){
                    lstReceiptIds.add(objReceipt.Id);
                }
            }
            system.debug('RRRRIIIDDD'+lstReceiptIds);
            if(lstReceiptIds.size()>0 && system.isFuture() == false && system.isBatch() == false){
                SAPWebServiceDetails.ECCReceiptCall(lstReceiptIds,true);
            }
        }
        
        //Added by Swati - V1.0
        if(trigger.isBefore && Trigger.isUpdate ){
            list<Id> lstAccountIds                   = new list<Id>();
            list<Id> lstAccountIdsMailAllowed        = new list<Id>();
            list<Receipt__c> recieptIds              = new list<Receipt__c>();
            map<Id,list<string>> mapEmpServiceUsers  = new map<Id,list<string>>();
            list<Receipt__c> lstContactInsert        = new list<Receipt__c>();
            
            for(Receipt__c objReceipt :trigger.new){
                system.debug('-----objReceipt.Pushed_to_SAP__c------'+objReceipt.Pushed_to_SAP__c);
                if(objReceipt.Payment_Status__c == 'Success'){
                    lstAccountIds.add(objReceipt.Customer__c);
                    recieptIds.add(objReceipt);
                }
            }
            
            if(!lstAccountIds.isEmpty()){
                for(Account obj : [select id from account where id IN : lstAccountIds and Allow_Portal_Recharge_Mail__c=true]){
                    lstAccountIdsMailAllowed.add(obj.id);   
                }   
            }
            
            if(!lstAccountIdsMailAllowed.isEmpty()){
                List<User> usr            = new List<User>();
                Set<String> contactIds    = new Set<String>(); 
                Map<String,AccountContactRelation> accountContactMap = new  Map<String,AccountContactRelation>();
                
                for(AccountContactRelation ACR: [Select Id,contactId,Roles from AccountContactRelation where AccountId IN: lstAccountIdsMailAllowed  and  isActive = true ]){
                    contactIds.add(ACR.contactId);
                    accountContactMap.put(ACR.contactId,ACR);
                }

                for(User thisUser : [select Id,Email,Contact.AccountId,ContactId,Community_User_Role__c FROM User where ContactId IN : contactIds and IsActive = true and Allow_Portal_Recharge_Mail__c = true ]){
                    if(accountContactMap.containsKey(thisUser.ContactId) && accountContactMap.get(thisUser.ContactId).Roles != null ){
                                
                                list<string> lstUserIds = mapEmpServiceUsers.containsKey(thisUser.Contact.AccountId) ? mapEmpServiceUsers.get(thisUser.Contact.AccountId) : new list<string>();
                                lstUserIds.add(thisUser.Email); mapEmpServiceUsers.put(thisUser.Contact.AccountId,lstUserIds);
                            }
                }
               
                for(Receipt__c objCon : trigger.new){
                    list<string> lst = mapEmpServiceUsers.get(objCon.customer__c);
                    if(lst != null && !lst.isEmpty()){
                        objCon.Sys_Send_Email__c = true;
                        if(lst.size() > 0) objCon.Portal_User_1__c = lst[0];
                        if(lst.size() > 1) objCon.Portal_User_2__c = lst[1];
                        if(lst.size() > 2) objCon.Portal_User_3__c = lst[2];
                        if(lst.size() > 3) objCon.Portal_User_4__c = lst[3];
                        if(lst.size() > 4) objCon.Portal_User_5__c = lst[4];
                    }
                }
            }   
        }
    }
}