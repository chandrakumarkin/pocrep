/******************************************************************************************
 *  Name    : Trg_ComplianceRequirements
 *  Author  : Swati Sehrawat
 *  Company : NSI JLT
 *  Date    : 2016-13-16
 *  Description : Trigger is used for all compliance related requirement.                         
--------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date            Updated By    Description
    
    V1.0    5th June 2016   Swati         Notification renewal template
    V1.1    22ndJune 2016   Swati         allotment of shares renewal template
    V1.2    24-12-2016      Sravan        Changed the email template body for Annual Return 
    V1.3    26-01-2017      Swati         As per ticket 3819
    V1.4    01-05-2017      Sravan       changed License Renewal Email text as per # 3980
    v1.5    17-12-2017      Arun          Removed filter  
    v1.6    20-Oct-2020     Arun           Replace Contact with Related contacts   
    --------------------------------------------------------------------------------------------------------------------------           
 
*/

trigger Trg_ComplianceRequirements on Compliance__c (before insert,before update) {

 if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled)
    {
    
    list<compliance__c> listLicense = new list<compliance__c>();
    list<compliance__c> listAnnualReturn = new list<compliance__c>();
    list<compliance__c> listNotificationRenewal = new list<compliance__c>();//V1.0
    list<compliance__c> listShares = new list<compliance__c>(); //V1.1
    list<compliance__c> listOther = new list<compliance__c>();
   
    //V1.3
    set<id> accountSet = new set<id>();
    list<compliance__c> listUpdatePortalUser = new list<compliance__c>();
    
    string complianceType1 = 'License Renewal';
    string complianceType2 = 'Notification Renewal';
    string complianceType3 = 'Annual Return';
    string complianceType4 = 'Annual Reporting';
    string complianceType5 = 'Appointing an Auditor';
    string complianceType6 = 'Allotment of Shares/Membership Interest';
    string complianceType7 = 'Amendment of the Articles of Association';
    string complianceType8 = 'Filing Financial Statements';
    
    string emailBody = '';
    
    if(Trigger.isInsert){
        for(compliance__c obj : trigger.new){
            if(obj.name == complianceType1){
                listLicense.add(obj);
            }
            else if(obj.name == complianceType2){//V1.0
                listNotificationRenewal.add(obj);
            }
            else if(obj.name == complianceType3){
                listAnnualReturn.add(obj);
            }
            else if(obj.name == complianceType6){
                listShares.add(obj);
            }
            else if(obj.name == complianceType4 || obj.name == complianceType5 || 
                    obj.name == complianceType7 || obj.name == complianceType8){
                listOther.add(obj);
            }
        }
    }
    
    if(Trigger.isUpdate){
        for(compliance__c obj : trigger.new)
        {
        if(obj.Relationship_Manager__c==null)
        obj.Relationship_Manager__c=obj.RM_ID__c;
        
            if(obj.Update_Email_Template_Body__c == True)
            {
                if(obj.name == complianceType1){
                    listLicense.add(obj);
                }
                else if(obj.name == complianceType2 ){//V1.0
                    listNotificationRenewal.add(obj);
                }
                else if(obj.name == complianceType3){
                    listAnnualReturn.add(obj);
                }
                else if(obj.name == complianceType6){
                    listShares.add(obj);
                }
                else if(obj.name == complianceType4 || obj.name == complianceType5 ||
                        obj.name == complianceType7 || obj.name == complianceType8){
                    listOther.add(obj);
                }
            }
            
            if(obj.defaulted_date__c!=null && obj.exception_date__c!=null && obj.exception_date__c != trigger.oldMap.get(obj.id).exception_date__c && obj.exception_date__c>obj.defaulted_date__c)
            {
                obj.defaulted_date__c = null;
                obj.status__c = 'Open';
            }
            
            //V1.3
            //v1.5 
            if(obj.status__c != trigger.oldMap.get(obj.id).status__c || obj.Update_Email_Template_Body__c == True)
            {
                accountSet.add(obj.Account__c);     
                listUpdatePortalUser.add(obj);
            }
        }
    }
    
    if(listLicense!=null && !listLicense.isEmpty()){
        for(compliance__c obj : listLicense){
            if(string.isNotBlank(obj.Name) && string.isNotBlank(obj.company_name__c) && obj.Sys_Actual_End_Date__c!=null){
                obj.Email_Template_Body__c  = 'Dear Valued Client,<br/><br/>'+
            
                                             +'Greetings from the DIFC.<br/><br/>'+
                                             
                                           //  +'Please note that the “'+obj.Name+'” for “'+obj.company_name__c+'” is due on '+obj.Sys_Actual_End_Date__c.format()+'.<br/><br/>'+  V1.4 
                                             + 'Please note that the "License Renewal" for '+obj.company_name__c+' will expire on '+obj.End_Date__c.format()+'.<br/><br/>'+ 
                                            
                                             +'Please ensure that the “'+obj.Name+'” service request is submitted on the DIFC Client Portal within 30 days from the expiry date to avoid fines.<br/><br/>'+
                                            
                                             +'Thank You<br/>'+
                                             +'Registry Services<br/><br/>'+
                                             
                                             +'Kindly ignore this email in case you have already submitted the required request.<br/><br/>'+
                                             
                                             +'<p style="color:blue">Your license renewal is now available on the Client Portal App. It is simple, go to your Mobile App store and search for DIFC Client Portal Mobile Application. With the Mobile App you can apply, view and track the status of your renewal as well as request for certificates and letters.</p>';       
            }
        }   
    }
    
    //V1.0
    if(listNotificationRenewal!=null && !listNotificationRenewal.isEmpty()){
        for(compliance__c obj : listNotificationRenewal){
            if(string.isNotBlank(obj.Name) && string.isNotBlank(obj.company_name__c) && obj.Sys_Actual_End_Date__c!=null){
                obj.Email_Template_Body__c  = 'Dear Valued Client,<br/><br/>'+
    
                                               +'Please note that the Notification Renewal for '+obj.company_name__c+' will expire on '+obj.End_Date__c.format()+'.<br/><br/>'+//  V1.4 
                                            
                                               +'Please ensure that the Notification Renewal service request is submitted on the DIFC Client Portal within 30 days from the expiry date to avoid fines.<br/><br/>'+
                                            
                                               +'Kindly note that the submission of notification renewal is part of the license renewal service request and is not a separate service request on the Client portal.<br/><br/>'+
                                            
                                               +'Should you need further assistance, please contact us on 04 362 2222 or email us on portal@difc.ae.<br/><br/>'+
                                            
                                               +'Thank You<br/>'+
                                               +'DIFC Registry Services<br/><br/><br/>'+
                                            
                                            
                                               +'This is a system automated reminder. Kindly ignore this email if you have already submitted the request.';        
            }
        }
    }
    
    /*
    if(listAnnualReturn!=null && !listAnnualReturn.isEmpty()){
        for(compliance__c obj : listAnnualReturn){
            if(string.isNotBlank(obj.Name) && string.isNotBlank(obj.company_name__c) && obj.Sys_Actual_End_Date__c!=null){
                obj.Email_Template_Body__c  = 'Dear Valued Client,<br/><br/>'+
    
                                               +'Please note that the Annual Return submission for '+obj.company_name__c+' is due and must be submitted during the period from 1st of January '+obj.Start_Date__c.year()+' to 31st of March '+obj.Sys_Actual_End_Date__c.year()+'.<br/><br/>'+    // V1.2 Replaced System.today().Year() with +obj.Start_Date__c.year()+' to 31st of March '+obj.Sys_Actual_End_Date__c.year()+'
                                            
                                               +'Failure to submit the Annual Return by 31st of March '+obj.Sys_Actual_End_Date__c.year()+' will result in fines as per the DIFC Companies Law.<b>Please note that your Annual Return is different from your Audited Accounts.</b><br/><br/>'+
                                            
                                               +'To submit the Annual Return, kindly visit the DIFC Client Portal. Please ensure that enough balance is available on the entity account.<br/><br/>'+
                                            
                                               +'Should you need further assistance, please contact us on 04 362 2222 or email us on portal@difc.ae.<br/><br/>'+
                                            
                                               +'Thank You<br/>'+
                                               +'DIFC Registry Services<br/><br/><br/>'+
                                            
                                            
                                               +'This is a system automated reminder. Kindly ignore this email if you have already submitted your Annual Return.';        
            }
        }
    }
    */
    if(listShares!=null && !listShares.isEmpty()){
        for(compliance__c obj : listShares){
            if(string.isNotBlank(obj.Name) && string.isNotBlank(obj.company_name__c) && obj.Sys_Actual_End_Date__c!=null){
                obj.Email_Template_Body__c  = 'Dear Valued Client,<br/><br/>'+
                
                                             +'Greetings from the DIFC.<br/><br/>'+
                                             
                                             +'Please note that the “Initial Allotment of Shares/Membership Interest” for “'+obj.company_name__c+'” is due on '+obj.Sys_Actual_End_Date__c.format()+'.<br/><br/>'+
                                            
                                             +'Please ensure that the “Initial Allotment of Shares/Membership Interest” service request is submitted on the DIFC Client Portal within the due date.<br/><br/>'+
                                            
                                             +'Thank You<br/>'+
                                             +'Registry Services<br/><br/>'+
                                             
                                             +'Kindly ignore this email in case you have already submitted the required request.';        
            }
        } 
    }
    
    if(listOther!=null && !listOther.isEmpty()){
        for(compliance__c obj : listOther){
            if(string.isNotBlank(obj.Name) && string.isNotBlank(obj.company_name__c) && obj.Sys_Actual_End_Date__c!=null){
                obj.Email_Template_Body__c  = 'Dear Valued Client,<br/><br/>'+
                
                                             +'Greetings from the DIFC.<br/><br/>'+
                                             
                                             +'Please note that the “'+obj.Name+'” for “'+obj.company_name__c+'” is due on '+obj.Sys_Actual_End_Date__c.format()+'.<br/><br/>'+
                                            
                                             +'Please ensure that the “'+obj.Name+'” service request is submitted on the DIFC Client Portal within the due date.<br/><br/>'+
                                            
                                             +'Thank You<br/>'+
                                             +'Registry Services<br/><br/>'+
                                             
                                             +'Kindly ignore this email in case you have already submitted the required request.';        
            }
        } 
    }
    
    //V1.3
    if(accountSet!=null && !accountSet.isEmpty())
    {
    System.debug('===accountSet==='+accountSet);
       // list<Contact> lstContacts = [select Id,accountId from Contact where AccountId IN : accountSet AND RecordType.DeveloperName='Portal_User' AND //Id IN (select ContactId from User where Contact.AccountId IN : accountSet AND IsActive=true AND Community_User_Role__c INCLUDES ('Company //Services')) order by createdDate ];    
        
        Map<String,List<id>> mapAccountWiseContact = new Map<String,List<id>>();//v1.6  
        for(AccountContactRelation cont : [select ContactId,AccountId from AccountContactRelation where AccountId in :accountSet and Contact.RecordType.DeveloperName='Portal_User'  and IsActive=true and Roles INCLUDES ('Company Services') order by createdDate])
        {
            List<id> lstCont;
            if(mapAccountWiseContact.containsKey(cont.accountId)) lstCont = mapAccountWiseContact.get(cont.accountId);
                    else      lstCont = new List<id>();
             
             lstCont.add(cont.ContactId);
                mapAccountWiseContact.put(cont.accountId,lstCont);
        }
        
        

        if(mapAccountWiseContact!=null && !mapAccountWiseContact.isEmpty() && listUpdatePortalUser!=null && !listUpdatePortalUser.isEmpty())
        {
            for(compliance__c obj : listUpdatePortalUser)
            {
                
                //V1.5 Reset Notification user list 
                 obj.Principal_User__c = null;obj.Principal_User_2__c = null; obj.Principal_User_3__c = null; obj.Principal_User_4__c = null;  obj.Principal_User_5__c = null;
                 
                if(mapAccountWiseContact.get(obj.Account__c)!=null && mapAccountWiseContact.get(obj.Account__c).size()>0)
                {
                    if(mapAccountWiseContact.get(obj.Account__c).size() > 0)obj.Principal_User__c = mapAccountWiseContact.get(obj.Account__c)[0];
                    if(mapAccountWiseContact.get(obj.Account__c).size() > 1) obj.Principal_User_2__c = mapAccountWiseContact.get(obj.Account__c)[1];
                    if(mapAccountWiseContact.get(obj.Account__c).size() > 2) obj.Principal_User_3__c = mapAccountWiseContact.get(obj.Account__c)[2];
                    if(mapAccountWiseContact.get(obj.Account__c).size() > 3) obj.Principal_User_4__c = mapAccountWiseContact.get(obj.Account__c)[3];
                    if(mapAccountWiseContact.get(obj.Account__c).size() > 4)  obj.Principal_User_5__c = mapAccountWiseContact.get(obj.Account__c)[4];      
                }   
            }   
        }
    
    }
    }
    
}