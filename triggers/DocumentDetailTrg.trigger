trigger DocumentDetailTrg on Document_Details__c (after update,after insert) {
	list<Relationship__c> lstRelations = new list<Relationship__c>();
	map<string,Boolean> mapContactIds = new map<string,Boolean>();
	list<Id> lstContactIds = new list<Id>();
	
	map<Id,string> mapConIdsToSR = new map<Id,string>();
	
	for(Document_Details__c objDD : trigger.new){
		if(trigger.isUpdate){
			if(objDD.Contact__c != null && (objDD.Document_Type__c == 'Employment Visa' || objDD.Document_Type__c == 'Seconded Access Card' || objDD.Document_Type__c == 'Local / GCC Smart Card' || objDD.Document_Type__c == 'Temporary Work Permit') && objDD.EXPIRY_DATE__c != trigger.oldMap.get(objDD.Id).EXPIRY_DATE__c){
				if(objDD.EXPIRY_DATE__c <= system.today()){
					mapContactIds.put(objDD.Contact__c+'-'+objDD.Document_Type__c,false);
				}else{
					mapContactIds.put(objDD.Contact__c+'-'+objDD.Document_Type__c,true);
				}
				lstContactIds.add(objDD.Contact__c);
			}
		}else if(trigger.isInsert){
			if((objDD.Document_Type__c == 'Employment Visa' || objDD.Document_Type__c == 'Seconded Access Card' || objDD.Document_Type__c == 'Local / GCC Smart Card' || objDD.Document_Type__c == 'Temporary Work Permit') && ( (objDD.EXPIRY_DATE__c != null && objDD.EXPIRY_DATE__c > system.today()) || objDD.EXPIRY_DATE__c == null) ){
				mapContactIds.put(objDD.Contact__c+'-'+objDD.Document_Type__c,true);
				lstContactIds.add(objDD.Contact__c);
			}
			if(objDD.Document_Type__c == 'Entry Permit' && objDD.Document_Number__c != null && objDD.Contact__c != null){
				mapConIdsToSR.put(objDD.Contact__c,objDD.Document_Number__c);
			}
		}
	}
	
	if(!lstContactIds.isEmpty()){
		for(Relationship__c objRel : [select Id,Object_Contact__c,Relationship_Type__c from Relationship__c where Object_Contact__r.RecordType.DeveloperName = 'GS_Contact' AND Object_Contact__c IN : lstContactIds AND (Relationship_Type__c = 'Has DIFC Sponsored Employee' OR Relationship_Type__c = 'Has DIFC Seconded' OR Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee')]){ // OR Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has DIFC Seconded'
			if(objRel.Relationship_Type__c == 'Has DIFC Sponsored Employee' && mapContactIds.containsKey(objRel.Object_Contact__c+'-Employment Visa'))
				lstRelations.add(new Relationship__c(Id=objRel.Id,Document_Active__c=mapContactIds.get(objRel.Object_Contact__c+'-Employment Visa')));				
			else if(objRel.Relationship_Type__c == 'Has DIFC Seconded' && mapContactIds.containsKey(objRel.Object_Contact__c+'-Seconded Access Card'))
				lstRelations.add(new Relationship__c(Id=objRel.Id,Document_Active__c=mapContactIds.get(objRel.Object_Contact__c+'-Seconded Access Card')));
			else if(objRel.Relationship_Type__c == 'Has DIFC Local /GCC Employees' && mapContactIds.containsKey(objRel.Object_Contact__c+'-Local / GCC Smart Card'))
				lstRelations.add(new Relationship__c(Id=objRel.Id,Document_Active__c=mapContactIds.get(objRel.Object_Contact__c+'-Local / GCC Smart Card')));
			else if(objRel.Relationship_Type__c == 'Has Temporary Employee' && mapContactIds.containsKey(objRel.Object_Contact__c+'-Temporary Work Permit'))
				lstRelations.add(new Relationship__c(Id=objRel.Id,Document_Active__c=mapContactIds.get(objRel.Object_Contact__c+'-Temporary Work Permit')));
		}
		if(!lstRelations.isEmpty())
			update lstRelations;
	}
	
	//added by Ravi on 21st April to update the EntryPermit # on SR for DNRD auto fill
	if(!mapConIdsToSR.isEmpty()){
		map<Id,Service_Request__c> mapSRs = new map<Id,Service_Request__c>();
		for(Contact objCon : [select Id,SR__c from Contact where Id IN : mapConIdsToSR.keySet() AND SR__c != null]){
			mapSRs.put(objCon.SR__c,new Service_Request__c(Id=objCon.SR__c,Residence_Visa_No__c=mapConIdsToSR.get(objCon.Id)));
		}
		if(!mapSRs.isEmpty())
			update mapSRs.values();
	}
	
}