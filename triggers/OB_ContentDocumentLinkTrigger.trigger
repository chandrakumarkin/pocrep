/*
    Author      : Durga Prasad
    Date        : 30-Oct-2019
    Description : Trigger on ContentDocumentLink object
    ----------------------------------------------------------------
*/
trigger OB_ContentDocumentLinkTrigger on ContentDocumentLink (after insert) 
{
    
    // To by pass validation.
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled)
    {
         if(trigger.IsInsert && trigger.IsAfter)
    		OB_ContentDocumentLinkTriggerHandler.onAfterInsert(Trigger.new);
    }
    
}