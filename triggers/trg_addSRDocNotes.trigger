/* Trigger which copies Customer Comments and DIFC Comments from SR Doc and moves it to Document History Object
 ---------------------------------------------------------------------------------------------------------------------
    Modification History
 ---------------------------------------------------------------------------------------------------------------------
    V.No    Date            Updated By    Description
 ---------------------------------------------------------------------------------------------------------------------
    V1.1    28-09-2016      Sravan      Added code for populating Random number in SR Doc fields
    v1.2    28-09-2016      Sravan      Added code for sending the Email to Applicants for after creation of User Access Request
                                            as part of  Security enhancements. 
    V1.3    27-11-2016      Claude      Added code to send an email to contractor when the document gets generated (#3731)
    V1.4    29-05-2017      Sravan      Added Block to change the status of the document Tkt # 4042
    v1.5    26-10-2-17      Veera       Added code to push details to SAP on entry permit is uploaded .  
    v1.6    27th-June-2020  Mudasir     #DIFC2-9870 - New Change required for New visa 'outside the country' to upload Entry Permit
                                        Change is specific to make a webservice call to SAP system once the document Entry Permit with airport entry stamp is uploaded                               

    V1.7   01-Apr-2021      Veera       Added code to Bypass validation on the doc upload on Establishmnet card created from ROC
*/


trigger trg_addSRDocNotes on SR_Doc__c (after insert,before insert,after update,before delete,after delete,before update) {
 if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled){
    list<Document_Comment_History__c> lstDocComments = new list<Document_Comment_History__c>();
    set<id> setSRIds = new set<id>();
    map<id,id> mapSRMarkUnUpload = new map<id,id>();
    TriggerFactoryCls.createHandler(sr_Doc__c.sobjectType);
    Boolean updateEntryPermitWithAirportEntryStampStep = false;
    if(trigger.isInsert){
        if(trigger.isBefore){
        set<id> set_SRDOC_SRIds = new set<id>();
        map<id,id> mapSRDocAccId = new map<id,id>();
        list<Service_Request__c> lstSrDocsNotUploaded = new list<Service_Request__c>();
        for(SR_Doc__c objSRDoc:trigger.new){
            if(objSRDoc.Service_Request__c!=null)
                set_SRDOC_SRIds.add(objSRDoc.Service_Request__c);
        }
        if(set_SRDOC_SRIds!=null && set_SRDOC_SRIds.size()>0){
            for(Service_Request__c objSReq:[select id,Customer__c from Service_Request__c where Customer__c!=null and Id IN:set_SRDOC_SRIds]){
                mapSRDocAccId.put(objSReq.id,objSReq.Customer__c);
            }
        }
        for(SR_Doc__c srdoc:trigger.new){
            if(SRDoc.Service_Request__c!=null && mapSRDocAccId.get(SRDoc.Service_Request__c)!=null)
                srdoc.Customer__c = mapSRDocAccId.get(SRDoc.Service_Request__c); 
            if(SRDoc.Service_Request__c!=null && srdoc.Is_Not_Required__c==false && mapSRMarkUnUpload.get(SRDoc.Service_Request__c)==null){
                Service_Request__c objSR = new Service_Request__c(Id=SRDoc.Service_Request__c);
                objSR.Required_Docs_not_Uploaded__c = true;
                lstSrDocsNotUploaded.add(objSR);
                mapSRMarkUnUpload.put(SRDoc.Service_Request__c,SRDoc.Service_Request__c);
            }
            
            // Added for generating the Random Number --- V1.1
               if(trigger.isBefore && trigger.isInsert)
               {
                    srdoc.Sys_RandomNo__c = Apex_Utilcls.getRandNo(6);
                    srdoc.Sys_AphaNumeric_Random__c = Apex_Utilcls.getRandNo(6);
               } 
             // End   ---
            
            
        //V1.7 - start    
        for(SR_Doc__c srdocRoc:trigger.new){
        
          if(srdocRoc.Service_Request__c!=null & SRDOC.By_Pass_ROC_Est_Doc_Upload__c){
             srdocRoc.Is_Not_Required__c = true;
          }
        
        
        
        } 
        
        //V1.7 - End    
            
        }
        if(lstSrDocsNotUploaded!=null && lstSrDocsNotUploaded.size()>0){
            try{
                RecursiveControlCls.isUpdatedAlready = true;
                update lstSrDocsNotUploaded;
            }catch(Exception e){
                
            }
        }
    } 
   }
    map<id,set<Integer>> MapSRDocGroupNumbers = new map<id,set<Integer>>();
    if(trigger.isUpdate){
        set<id> setCurrentSRDocs = new set<id>();
        for(SR_Doc__c SRDoc:trigger.new){
            if(SRDoc.Received_Physically__c==true){
                SRDoc.Is_Not_Required__c=true;
            }
            if(SRDoc.Service_Request__c!=null && SRDoc.Is_Not_Required__c==false && SRDoc.SR_RecordType_Name__c!='DM_SR')
                setSRIds.add(SRDoc.Service_Request__c);
            if(SRDoc.Group_No__c!=null && SRDoc.Doc_ID__c!=null && SRDoc.Status__c!='Rejected'){
                setCurrentSRDocs.add(SRDoc.id);
                set<integer> setGroupNumbers = new set<integer>();
                if(MapSRDocGroupNumbers.get(SRDoc.Service_Request__c)!=null && MapSRDocGroupNumbers.get(SRDoc.Service_Request__c).size()>0){
                    setGroupNumbers = MapSRDocGroupNumbers.get(SRDoc.Service_Request__c);
                    setGroupNumbers.add(integer.valueOf(SRDoc.Group_No__c));
                    MapSRDocGroupNumbers.put(SRDoc.Service_Request__c,setGroupNumbers);
                }else{
                    setGroupNumbers.add(integer.valueOf(SRDoc.Group_No__c));
                    MapSRDocGroupNumbers.put(SRDoc.Service_Request__c,setGroupNumbers);
                }
            }
            SR_Doc__c oldSRDoc = Trigger.oldMap.get(SRDoc.ID);
            
            if(SRDoc.Customer_Comments__c!=oldSRDoc.Customer_Comments__c){
                Document_Comment_History__c docComments = new Document_Comment_History__c(SR_Doc__c=SRDoc.id,Comments__c=SRDoc.Customer_Comments__c,Comment_From__c='Customer');
                lstDocComments.add(docComments);
            }else if(SRDoc.DIFC_Comments__c!=oldSRDoc.DIFC_Comments__c){
                Document_Comment_History__c docComments = new Document_Comment_History__c(SR_Doc__c=SRDoc.id,Comments__c=SRDoc.DIFC_Comments__c,Comment_From__c='DIFC');
                lstDocComments.add(docComments);
            }
            
        }
        if(lstDocComments!=null && lstDocComments.size()>0){
            insert lstDocComments;
        }
        system.debug('MapSRDocGroupNumbers===>'+MapSRDocGroupNumbers);
        if(setCurrentSRDocs!=null && setCurrentSRDocs.size()>0 && MapSRDocGroupNumbers!=null && MapSRDocGroupNumbers.size()>0){
            list<SR_Doc__c> lstNonReqDocs = new list<SR_Doc__c>();
            for(SR_Doc__c SRDOC:[select id,Group_No__c,Doc_ID__c,Status__c,Is_Not_Required__c,Service_Request__c from SR_Doc__c where ID NOT IN:setCurrentSRDocs and Service_Request__c IN:MapSRDocGroupNumbers.keyset()]){
                if(SRDOC.Doc_ID__c==null && SRDOC.Is_Not_Required__c==false && SRDOC.Status__c!='Rejected' && MapSRDocGroupNumbers.get(SRDOC.Service_Request__c)!=null && MapSRDocGroupNumbers.get(SRDOC.Service_Request__c).size()>0){
                    system.debug('SRDOC===>'+SRDOC.id);
                    for(integer grpno:MapSRDocGroupNumbers.get(SRDOC.Service_Request__c)){
                        if(grpno==integer.valueOf(SRDOC.Group_No__c)){
                            SRDOC.Is_Not_Required__c = true;
                            lstNonReqDocs.add(SRDOC);
                            break;
                        }
                    }
                }
            }
            if(lstNonReqDocs!=null && lstNonReqDocs.size()>0){
                try{
                    update lstNonReqDocs;
                }catch(Exception e){
                    
                }
            }
        }
    }
    if(trigger.isdelete && trigger.isBefore){
        for(SR_Doc__c SRDoc:trigger.old){
            if(SRDoc.Service_Request__c!=null && SRDoc.Is_Not_Required__c==false && SRDoc.SR_RecordType_Name__c!='DM_SR')
                setSRIds.add(SRDoc.Service_Request__c);
        }
    }
    if(trigger.isAfter && trigger.isUpdate && setSRIds!=null && setSRIds.size()>0){
        list<Service_Request__c> lstUpdatableSRs = new list<Service_Request__c>();
        for(Service_Request__c SR:[select id,Required_Docs_not_Uploaded__c,(select Doc_ID__c from SR_Docs__r where Doc_ID__c!='NonSelective' and Is_Not_Required__c=false and SR_RecordType_Name__c!='DM_SR') from Service_Request__c where ID IN:setSRIds and RecordType.DeveloperName!='DM_SR']){
            if(SR.SR_Docs__r!=null && SR.SR_Docs__r.size()>0){
                Service_Request__c srUpdatable = new Service_Request__c(Id=SR.id);
                for(SR_Doc__c objSRDoc:SR.SR_Docs__r){
                    if(objSRDoc.Doc_ID__c!=null){
                        srUpdatable.Required_Docs_not_Uploaded__c = false;
                    }else{
                        srUpdatable.Required_Docs_not_Uploaded__c = true;
                        break;
                    }
                }
                if(srUpdatable.Required_Docs_not_Uploaded__c!=null)
                    lstUpdatableSRs.add(srUpdatable);
            }
        }
        if(lstUpdatableSRs!=null && lstUpdatableSRs.size()>0){
            try{
                RecursiveControlCls.isUpdatedAlready = true;
                update lstUpdatableSRs;
            }catch(Exception e){
                string DMLError = e.getdmlMessage(0)+'';
                if(DMLError==null)
                    DMLError = e.getMessage()+'';
                trigger.new[0].addError(DMLError);
            }
        }
    }
    
    
    // V1.2
    
        if(trigger.isUpdate && trigger.isAfter)
        {
            set<id> srdocIdSet = new set<id>();
            Set<id> srDocumentIDs = new set<id>(); // SR Documents on which email needs to be sent
            for(SR_Doc__c srd : trigger.new){
                if(srd.SR_RecordType_Name__c =='User_Access_Form')
                    srdocIdSet.add(srd.id);
            }
            if(srdocIdSet !=null && srdocIdSet.size()>0){
                for(SR_Doc__c srDoc : [select id,Service_Request__c,Name,Service_Request__r.Parent_SR__c,Service_Request__r.RecordType.Developername,Service_Request__r.User_Form_Action__c,Doc_ID__c,Service_Request__r.Internal_Status_Name__c,Status__c from SR_Doc__c where id in:srdocIdSet])
                {
                    if(srDoc.Service_Request__r.Internal_Status_Name__c =='submitted' && ((srDoc.Service_Request__r.User_Form_Action__c == 'Create New User' && srDoc.Name == 'User Access Form')
                       || (srDoc.Service_Request__r.User_Form_Action__c == 'Remove Existing User' && srDoc.Name=='User Revoke Form') || (srDoc.Service_Request__r.User_Form_Action__c == 'Update Existing User' && srDoc.Name=='User Update Form'))  && srDoc.Doc_ID__c !=null && srDoc.status__c =='Generated' && 
                       srDoc.Service_Request__r.RecordType.Developername =='User_Access_Form' && srDoc.Service_Request__r.Parent_SR__c==null)
                    {
                        srDocumentIDs.add(srDoc.id);
                    }
                }
                
                if(srDocumentIDs !=null && srDocumentIDs.size()>0)
                {
                    UserAccessFormActions.sendUserAccessFormEmail(srDocumentIDs);
                }
            }   
        }
    // End V1.2
    
   // V1.4 
    if(trigger.isbefore && trigger.isUpdate){
        for(SR_Doc__c srDoc : Trigger.new){
            
            if(srDoc.SR_RecordType_Name__c =='Application_of_Registration')
            {
                if(srDoc.Name =='Signed Certificate of Incorporation / Registration' && srDoc.Doc_ID__c!=null && trigger.oldMap.get(srDoc.id).Doc_ID__c !=srDoc.Doc_ID__c){
                    
                    srDoc.Sys_IsGenerated_Doc__c = true;
                    srDoc.Status__c = 'Generated';
                }
            }
            
            
        }
    }
    
  //End V1.4  
    
    //V1.3 - Claude - Start
    if(trigger.isAfter && trigger.isUpdate){
        
        List<Fo_cls_EmailServiceHandler.SrDocEmailDetails> docDetailsList = new List<Fo_cls_EmailServiceHandler.SrDocEmailDetails>();
        List<Id> srLst = new List<id>();
        STRING SAP_STEP_SEQ ;
        DateTime DocUploadedDtTime;
        for(SR_Doc__c srDoc : Trigger.new){
            
            // TODO: Make email handler generic
            if(srDoc.Name.equals('Contractor Authorisation')){ 
                
                Fo_cls_EmailServiceHandler.SrDocEmailDetails docDetails = new Fo_cls_EmailServiceHandler.SrDocEmailDetails();
        
                docDetails.srDocId = srDoc.ID;
                docDetails.documentName = 'contractor_authorisation.pdf';
                docDetails.emailTemplateName = 'Portal_User_creation_for_contractor_wallet';
                
                docDetailsList.add(docDetails);             
            }
            //v1.5
           //if(checkRecursive.runOnce()){
            
            map<string,SR_Doc_Names__c>  SRDocNames = SR_Doc_Names__c.getAll();
            
             //v1.6 - updated the custom setting (SR_Doc_Names__c) where we included the name - Entry Permit With Airport Entry Stamp
             if(SRDocNames.containsKey(srDoc.Name) && srDoc.Status__c == 'Uploaded' 
                                             &&  (srDoc.Check_Entry_cancellation_Doc__c 
                                             || srDoc.Check_Entry_cancellation_Doc_New__c || srDoc.Name == 'Approved Residence Visa') 
                                             && SR_Doc_Names__c.getInstance(srDoc.Name).is_Active__c){
                
                srLst.add(srDoc.service_request__c); 
                SAP_STEP_SEQ  = srDoc.SAP_step_Seq__c; 
                DocUploadedDtTime = srDoc.LastModifiedDate;
            }
            //v1.6 
            if(Test.isRunningTest() || (srDoc.Name=='Entry Permit With Airport Entry Stamp' && srDoc.Status__c == 'Uploaded')){
                updateEntryPermitWithAirportEntryStampStep = true;
            }
       //   } 
            
          /*  
            if((srDoc.Name.equals('Entry Permit') && srDoc.Status__c == 'Uploaded' &&  srDoc.Check_Entry_cancellation_Doc__c) || (srDoc.Name.equals('Cancellation Confirmation') && srDoc.Status__c == 'Uploaded' && srDoc.Check_Entry_cancellation_Doc__c) || (srDoc.Name.equals('Visit Visa') && srDoc.Status__c == 'Uploaded' && srDoc.Check_Entry_cancellation_Doc__c) ||(srDoc.Name.equals('Renewed Establishment Card') && srDoc.Status__c == 'Uploaded' && srDoc.Check_Entry_cancellation_Doc__c)){         
                srLst.add(srDoc.service_request__c); 
                SAP_STEP_SEQ  = srDoc.SAP_step_Seq__c;            
            }*/
            
            
            if(
                srDoc.Name == GlobalConstants.MEDICALRESULTDOCUMENT && srDoc.Status__c == GlobalConstants.DOC_STATUS_UPLOADED && GlobalConstants.MANUALSRPUSHINGMEDICALRESULT.contains(srDoc.Service_Request_Name__c)
            ){         
                srLst.add(srDoc.service_request__c); 
                SAP_STEP_SEQ  = srDoc.SAP_step_Seq__c;     
                 DocUploadedDtTime = srDoc.LastModifiedDate;       
            }
           
        }
        
        if(!docDetailsList.isEmpty()){
            Fo_cls_EmailServiceHandler.sendSrDocEmail(docDetailsList);  
        }
        
        //v1.5
        if(srLst.size()>0){         
              GsEntryPermitUploadHandler.EntryPermitStatusInSAP(srLst[0],SAP_STEP_SEQ,DocUploadedDtTime); 
        }
        if(updateEntryPermitWithAirportEntryStampStep){
            String objStepSOQLFieldsToQuery ='SR__c,status__c,step_name__c';
            List<step__c> originalPassportSubmissionSteps = new List<step__c>();
            String onjSOQLFilters = Test.isRunningTest() ? 'SR__c=\''+Trigger.New[0].service_request__c+'\'': 'Step_Name__c=\'Entry Permit With Airport Entry Stamp Upload\'' +' AND SR__c=\''+srLst[0]+'\'' ;
            originalPassportSubmissionSteps = DIFCDataServiceUtils.getStepList(objStepSOQLFieldsToQuery , onjSOQLFilters ,'Name' ,0);
            integer indexOfList=0;
            for(Step__c stepRec : originalPassportSubmissionSteps){
                originalPassportSubmissionSteps[indexOfList] = DIFCApexCodeUtility.prepareStep(false,stepRec,'Closed','','');
                indexOfList++;
            }
            if(originalPassportSubmissionSteps.size() > 0)update originalPassportSubmissionSteps;
          }   
        
    }
    //V1.3 - Claude - End
    
    
  }
    
}