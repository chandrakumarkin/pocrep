/*
    Author      : Salma Rasheed
    Company     : PwC
    Date        : 15-Dec-2019
    Description : ChatTranscriptTrigger-This trigger is used to close the related case when the chat is ended by the visitor/agent
    -------------------------------------------------------------------------
*/
trigger ChatTranscriptTrigger on LiveChatTranscript (after update) {
    List<Case> CaseList = new List<Case>();
    Set<ID> CaseIds = new Set<ID>();
    for(LiveChatTranscript chat : Trigger.new){
        if(chat.Status=='Completed' && chat.CaseId!=null){
            CaseIds.add(chat.CaseId); 
        }    
    }
    if(!CaseIds.isEmpty()){
        for(Id CaseId : CaseIds){
            CaseList.add(new Case(id=CaseId,Status='Closed - Resolved'));
        }
        if(!CaseList.isEmpty()) update CaseList;
    }                    
}