/*
    Created By  : Ravi on 29th Oct,2014
    Description : This trigger will check,
                    1. Whether the Account has any Sector Classification or not
                    2. Are there have any Activities
                    3. Is there any Portal Contact exist &
                    4. Whether the BP No exist or not.
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 V1.0   29/10/2014      Ravi            Created
 V1.1   28/06/2015      Ravi            Added the logic to updated the Account Status to Strike Off and remove the access to Portal Users - Exit Process
 V1.2   24/08/2015      Ravi            Added the logic to send the Singnage email to SR when Account Status is Active
 V1.3   25/05/2016      Swati           As per Ticket 2822
 V1.4   15/06/2016      Swati           As per ticket #2693
 V1.5   26/10/2016      Claude          Added 'Portal_User' record type for Contacts
 V1.6   07/12/2016      Sravan          Changed logic to populate the Account Name and Trade Name into sys fields as the
                                        length is changed as per Tkt # 3593.
 V1.7   21/02/2017      Sravan          modified code that splits Account name and Trade name tkt # 3920                                      
 V1.5    01-10-217      Arun            Added a code to disable trigger for specific user
 V1.9   01-Apr-2018     Danish          update compliance owner id when account's owner updat tkt # 5100
 V2.0   01-Apr-2018     Mudasir         Added in order to put a validation for the change of Contractor Account to Event Organizer account #4502
 V2.1   04-July-2018    Mudasir         Added a code to update Landlord NOC step once the license is updated
 V2.2   28-Aug-2019     Arun            Changed code for Strike Off #7257
 V2.3   30-DEC-2019     Sai   
 V2.4   06-Jul-2020     Sai             DIFC2-10163
           
 ----------------------------------------------------------------------------------------
*/
trigger AccountTrg on Account (before insert,before update,after update,after insert) {
    
    //Check custom label stored user id same as running user id ? if not the same run the trigger 
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled || test.isRunningtest())
    {
        
    if(trigger.isBefore){
        for(Account objAccount : trigger.new){
        

       
        
            string AccountName = objAccount.Name;          if(objAccount.Discount_Applied__c=='Yes' && objAccount.Override_Calculated_Visa_Quota__c!=null ) objAccount.Calculated_Visa_Quota__c=objAccount.Override_Calculated_Visa_Quota__c;
           
            if(AccountName != null && AccountName.length() > 40){
                if(AccountName.lastIndexOf(' ') > 40 || AccountName.lastIndexOf(' ') != -1){
                    string str1 = AccountName.substring(0,41);
                    if(str1.lastIndexOf(' ') != 40 || str1.lastIndexOf(' ') != 39 || str1.lastIndexOf(' ') != 41)
                        str1 = str1.substring(0,str1.lastIndexOf(' '));
                        string str2; // V1.6
                        if(AccountName.length() <=80 && str1.length() < 40)
                            //str2 = AccountName.substring(str1.length(),(AccountName.length()-(40-str1.length())));  // V1.6
                           str2 =  (AccountName.length() - str1.length()) >40 ? AccountName.substring(str1.length(),(AccountName.length()-(40-str1.length()))) : AccountName.substring(str1.length(),AccountName.length());  // V1.7
                        else if(AccountName.length() <=80 && str1.length() == 40)
                            str2 = AccountName.substring(str1.length(),(AccountName.length()));   // V1.6
                        else if(AccountName.length() > 80)                                        // V1.6
                            str2 = AccountName.substring(str1.length(),(80-(40-str1.length())));  // V1.6
                                
                    objAccount.Sys_Name_Org1__c = str1;
                    objAccount.Sys_Name_Org2__c = str2;
                }else{
                    objAccount.Sys_Name_Org1__c = AccountName.substring(0, 40); //V1.7
                    objAccount.Sys_Name_Org2__c = AccountName.substring(40,AccountName.length());    // V1.7                  
                }
            }else{
                objAccount.Sys_Name_Org1__c = objAccount.Name;
                objAccount.Sys_Name_Org2__c = '';
            }
            
            if(objAccount.Trade_Name__c != null){
                string AccountTradeName = objAccount.Trade_Name__c;
                if(AccountTradeName.length() > 40){
                    if(AccountTradeName.lastIndexOf(' ') > 40 || AccountTradeName.lastIndexOf(' ') != -1){
                        string str1 = AccountTradeName.substring(0,41);
                        if(str1.lastIndexOf(' ') != 40 || str1.lastIndexOf(' ') != 39 || str1.lastIndexOf(' ') != 41)
                            str1 = str1.substring(0,str1.lastIndexOf(' '));
                            string str2; // V1.6
                         if(AccountTradeName.length() <=80 && str1.length() < 40)    // V1.6
                            //str2 = AccountTradeName.substring(str1.length(),(AccountTradeName.length()-(40-str1.length())));  // V1.6
                            str2 = (AccountTradeName.length()-str1.length())>40 ? AccountTradeName.substring(str1.length(),(AccountTradeName.length()-(40-str1.length()))) : AccountTradeName.substring(str1.length(),AccountTradeName.length()) ;  // V1.7
                         else if(AccountTradeName.length() <=80 && str1.length() == 40)      // V1.6
                              str2 = AccountTradeName.substring(str1.length(),(AccountTradeName.length()));   // V1.6
                        else if(AccountTradeName.length() >80) // v1.6
                            str2 = AccountTradeName.substring(str1.length(),(80-(40-str1.length())));     // v1.6
                        objAccount.Sys_Name_Org3__c = str1;
                        objAccount.Sys_Name_Org4__c = str2;
                    }else{
                        objAccount.Sys_Name_Org3__c = AccountTradeName.substring(0, 40);                        
                        objAccount.Sys_Name_Org4__c = AccountTradeName.substring(40,AccountTradeName.length());                             
                    }
                }else{
                    objAccount.Sys_Name_Org3__c = AccountTradeName;
                    objAccount.Sys_Name_Org4__c = '';
                }
            }else{
                objAccount.Sys_Name_Org3__c = '';
                objAccount.Sys_Name_Org4__c = '';
            }
        }
    }
    
    if(trigger.isUpdate && trigger.isBefore){
        map<Id,Account> mapAccounts = new map<Id,Account>();
        map<Id,string> mapContactEmails = new map<Id,string>();
        boolean allowNameEdit = false; //V1.4
        
        //added by Ravi on 26th Nov, to inform the users when updating the Company Type / Sector Classification before registration to delete the existing Activities associated with client
        list<Id> lstChangedIds = new list<Id>();
        
        //v1.1
        list<Id> lstAccIds = new list<Id>();
        Id contractorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Contractor Account').getRecordTypeId();
        
        //added by Swati to update license dates on SR
        list<id> accountIDforCon = new list<Id>();
        list<service_request__c> srToUpdate = new list<service_request__c>();
        //V2.0 - Start
        Id eventAccountRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Event Account').getRecordTypeId();
        Map<id,Account> eventOrganizerAccounts = new Map<Id,Account>();
        //V2.0 - End
        for(Account objAccount : trigger.new){
            if(objAccount.Contractor_License_Expiry_Date__c != trigger.oldMap.get(objAccount.Id).Contractor_License_Expiry_Date__c){
                accountIDforCon.add(objAccount.id);
            }
            //V2.0 - Start - Check if account is updated to Event Organizer from any other type(Contractor Account)
            if(objAccount.recordTypeId == eventAccountRecordTypeId && objAccount.recordTypeId != trigger.oldMap.get(objAccount.Id).recordTypeId){eventOrganizerAccounts.put(objAccount.id,objAccount);}
            //V2.0 - End
        }
        //V2.0 - Start
        if(eventOrganizerAccounts != Null && eventOrganizerAccounts.size() > 0 && !Test.isRunningTest() && RecursiveControlCls.isAccountTrgExecuted == false){ AccountTriggerHelper.handleAfterUpdateAction(eventOrganizerAccounts); RecursiveControlCls.isAccountTrgExecuted = true;}
        //V2.0 - End
        Map<id,List<service_request__c>> mapOfAccandSR = new Map<id,List<service_request__c>>();
        if(accountIDforCon!=null && !accountIDforCon.isEmpty()){
            for(service_request__c sa :[SELECT id,Contractor__c,Tenant_Trade_License_Expiry_Date__c,Current_License_Expiry_Date__c From service_request__c where contractor__c IN : accountIDforCon and record_type_name__c=:'Fit_Out_Service_Request' and External_Status_Code__c!=:'PROJECT_COMPLETED']){         
                if(mapOfAccandSR.containsKey(sa.contractor__c)){
                    List<service_request__c> SAList = mapOfAccandSR.get(sa.contractor__c);
                    SAList.add(sa);       
                    mapOfAccandSR.put(sa.contractor__c,SAList);
                }
                else{
                    List<service_request__c> SAList = new List<service_request__c>();
                    SAList.add(sa);
                    mapOfAccandSR.put(sa.contractor__c,SAList);
                }
            }
        }
        
        for(Account objAccount : trigger.new){
            if(objAccount.Send_Portal_Email__c == true && trigger.oldMap.get(objAccount.Id).Send_Portal_Email__c == false){
                if(objAccount.BP_No__c == null || objAccount.BP_No__c == ''){
                    trigger.newMap.get(objAccount.Id).addError('BP Number is not exist on this Customer.');
                    break;
                }
                if(objAccount.Sector_Classification__c == null || objAccount.Sector_Classification__c == ''){
                    trigger.newMap.get(objAccount.Id).addError('Please select the Sector Classification.');
                    break;
                }
                mapAccounts.put(objAccount.Id,objAccount);
            }
            if((objAccount.Registration_License_No__c == null || objAccount.Registration_License_No__c == '') && (objAccount.Company_Type__c != trigger.oldMap.get(objAccount.Id).Company_Type__c || objAccount.Sector_Classification__c != trigger.oldMap.get(objAccount.Id).Sector_Classification__c)){
                lstChangedIds.add(objAccount.Id);
            }
            
            //v1.1//v2.2
            if(objAccount.ROC_Status__c!=null)
            if((objAccount.ROC_Status__c.Contains('Struck Off') || objAccount.ROC_Status__c == 'Dissolved') && objAccount.ROC_Status__c != trigger.oldMap.get(objAccount.Id).ROC_Status__c){
                //objAccount.ROC_Status__c = 'Inactive - Struck Off';
                lstAccIds.add(objAccount.Id);
            }
            
            //added by Swati to calculate contractor blackpoints
            if(objAccount.recordTypeId == contractorRecordTypeId && objAccount.IDAMA_BlackPoints__c!=null && trigger.oldMap.get(objAccount.id).IDAMA_BlackPoints__c!=objAccount.IDAMA_BlackPoints__c){
                if(objAccount.Blacklist_Point_3_months__c!=null){
                    objAccount.Blacklist_Point_3_months__c = objAccount.Blacklist_Point_3_months__c+objAccount.IDAMA_BlackPoints__c;
                }
                else{
                    objAccount.Blacklist_Point_3_months__c = objAccount.IDAMA_BlackPoints__c;
                }                   
            }
            
            if(mapOfAccandSR!=null && !mapOfAccandSR.isEmpty()){
                for(service_Request__c obj : mapOfAccandSR.get(objAccount.id)){
                    obj.Current_License_Expiry_Date__c = objAccount.Contractor_License_Expiry_Date__c;
                    srToUpdate.add(obj);    
                }
            }
            
            //V1.4
            if(RecursiveControlCls.isThroughCustomCode == false && objAccount.Registration_License_No__c!=null){
                if((objAccount.name!=null && objAccount.name!=trigger.oldMap.get(objAccount.Id).name) || (objAccount.Trade_Name__c!=null && objAccount.Trade_Name__c!=trigger.oldMap.get(objAccount.Id).Trade_Name__c)){
                    allowNameEdit = true;   
                }   
            }
        }
        
        if(srToUpdate!=null && !srToUpdate.isEmpty()){
            CC_cls_FitOutandEventCustomCode.isFitOutCustomCode = true;
            update srToUpdate;
        }
        
        //V1.4 
        if(allowNameEdit == true){
            boolean isAdmin = false;
            Allowed_to_Update_Account_Name__c setting = Allowed_to_Update_Account_Name__c.getValues(userinfo.getusername());
            system.debug('---setting--'+setting);
            if(setting!=null){
                isAdmin = true; 
            }
            if(isAdmin==false){
                trigger.new[0].addError('Insufficient privileges to edit the Account Name or Trade Name.');
            }
        }
        
        if(!lstChangedIds.isEmpty()){
            //V2.3 . added AND Account__r.Has_open_AOR__c=true condition
            Integer iC = [select count() from License_Activity__c where Account__c IN : lstChangedIds AND Account__r.Has_open_AOR__c=true];
            if(iC != null && iC > 0){
                trigger.new[0].addError('Please delete the associated activites prior to change the company type/sector classification.');
            }
        }
        
        if(!mapAccounts.isEmpty()){
            
            List<Account> listAccounts = Test.isRunningTest() ? [select Id, Company_Type__c, Check_Portal_User__c,(select Id,Email from Contacts where Email != null),(select Id from License_Activities__r where End_Date__c = null OR End_Date__c >:system.today()) from Account where Id IN : mapAccounts.keySet()] :
                                            [select Id,Company_Type__c,Check_Portal_User__c,(select Id,Email from Contacts where RecordType.DeveloperName = 'Portal_User' AND Email != null),(select Id from License_Activities__r where End_Date__c = null OR End_Date__c >:system.today()) from Account where Id IN : mapAccounts.keySet()];
                                            
            for(Account objAccount : listAccounts){ // V1.5 - Claude - Added 'Portal_User' record type in contact query
                if((objAccount.Contacts == null || objAccount.Contacts.size() == 0) && objAccount.Check_Portal_User__c){    //v2.2 Company_Type__c != 'Retail' , added in above query also
                    trigger.newMap.get(objAccount.Id).addError('Please add a portal contact for this customer.');
                    break;
                }
                if(objAccount.License_Activities__r == null || objAccount.License_Activities__r.size() == 0){
                    if(trigger.newMap.get(objAccount.Id).Company_Type__c == 'Financial - related')
                        trigger.newMap.get(objAccount.Id).addError('Please add at least one license activity for this customer.');
                    else
                        trigger.newMap.get(objAccount.Id).addError('Please add at least one sector classification for this customer.');
                    break;
                }
                if(objAccount.Contacts != null && objAccount.Contacts.size() > 0){
                    mapContactEmails.put(objAccount.Id,objAccount.Contacts[0].Email);
                }
            }
        }
        
        if(!mapContactEmails.isEmpty()){
            for(Account objAccount : trigger.new){
                if(mapContactEmails.containsKey(objAccount.Id)){
                    objAccount.Initial_Contact_Email__c = mapContactEmails.get(objAccount.Id);
                    if(objAccount.E_mail__c == null || objAccount.E_mail__c == '')
                        objAccount.E_mail__c = mapContactEmails.get(objAccount.Id);
                }
            }
        }
        
        //v1.3,v1.1 - Remove the Portal Access for the Customer
        if(!lstAccIds.isEmpty()){
            CC_DissolutionCls.RemovePortalAccess(lstAccIds);
        }
    }
    if(trigger.isAfter && trigger.isUpdate){
        map<string,string> mapHistoryFields = new map<string,string>();
        map<string,History_Tracking__c> mapHistoryCS = new map<string,History_Tracking__c>();
        if(History_Tracking__c.getAll()!=null){
            mapHistoryCS = History_Tracking__c.getAll();//Getting the Objects from Custom Setting which has to be displayed in the screen for the users
            for(History_Tracking__c objHisCS:mapHistoryCS.values()){
                if(objHisCS.Object_Name__c!=null && objHisCS.Object_Name__c.tolowercase()=='account' && objHisCS.Field_Name__c!=null){
                    mapHistoryFields.put(objHisCS.Field_Name__c,objHisCS.Field_Label__c);
                }
            }
        }
        Map<ID,Account> mapOfUpdateOwnerAcc = new Map<ID,Account>();
        Id ownerOldID;
        Id ownerNewID;
        list<Entity_History__c> lstEntHist = new list<Entity_History__c>();
        Set<id> accountIds = new Set<id>();
        for(Account acc:trigger.new){
            // V2.1 Code added by Mudasir 
            if(acc.Active_License__c!=Null && acc.Active_License__c != trigger.oldMap.get(acc.id).Active_License__c){ accountIds.add(acc.id);}
            if(RecursiveControlCls.isAccountTrgExecuted==false && mapHistoryFields!=null && mapHistoryFields.size()>0 && acc!=trigger.oldMap.get(acc.Id)){
                for(string AccFld:mapHistoryFields.keyset()){
                    if(acc.get(AccFld) != trigger.oldMap.get(acc.Id).get(AccFld)){
                        Entity_History__c objHist = new Entity_History__c();
                        objHist.Account__c = acc.Id;
                        objHist.Field_Name__c = AccFld;
                        objHist.Date_of_Change__c = system.now();
                        objHist.User__c = userinfo.getUserId();
                        objHist.Field_Label__c = mapHistoryFields.get(AccFld);
                        if(trigger.oldMap.get(acc.Id).get(AccFld)!=null){
                            objHist.Old_Value__c = trigger.oldMap.get(acc.Id).get(AccFld)+'';
                        }else{
                            objHist.Old_Value__c = 'blank';
                        }
                        if(acc.get(AccFld)!=null){
                            objHist.New_Value__c = acc.get(AccFld)+'';
                        }else{
                            objHist.New_Value__c = 'blank';
                        }
                        lstEntHist.add(objHist);
                    }
                }
            }
            //v 1.9 start 
            ownerNewID = trigger.newMap.get(acc.Id).OwnerID;
            ownerOldID = trigger.oldMap.get(acc.Id).OwnerID;
            
            if((ownerOldID != ownerNewID && RecursiveControlCls.isAccountTrgExecuted == false) || test.isRunningTest()){
                
            
                mapOfUpdateOwnerAcc.put(acc.Id, acc);
                    system.debug('%%%%%' +mapOfUpdateOwnerAcc);
            }
        }
        //Code added by Mudasir on 4th July 2018
        if(accountIds != Null && accountIds.size() > 0){AccountTriggerHelper.updatePendingLandlordNocStep(accountIds);}        
        if(!mapOfUpdateOwnerAcc.isEmpty()){
            
            Map<ID,ID> mapMangerID = new Map<ID,ID>();
            for(Account acc : [Select Id, Owner.ManagerID from Account where ID in : mapOfUpdateOwnerAcc.keySet()
                            AND owner.ManagerID != NULL]){
                                
                mapMangerID.put(acc.ID,acc.Owner.ManagerID);
            }
            
            List<Compliance__c> lstupdatedCompliance = new List<Compliance__c>();
            
            for(Compliance__c iComp : [Select Id,Relationship_Manager__c, OwnerId,Manager__c,Account__c from Compliance__c 
                                        where Account__c in : mapOfUpdateOwnerAcc.keySet()
                                        AND status__c != 'Fullfilled']){
                
                iComp.Relationship_Manager__c = mapOfUpdateOwnerAcc.get(iComp.Account__c).OwnerId;
                
                if(mapMangerID.containsKey(iComp.Account__c))
                    iComp.Manager__c = mapMangerID.get(iComp.Account__c);
                else
                    iComp.Manager__c = null;
                
                lstupdatedCompliance.add(iComp);
            }
                system.debug('%%%****%%' +lstupdatedCompliance);
            if(!lstupdatedCompliance.isEmpty()){
                
                try{
                    update lstupdatedCompliance;
                    RecursiveControlCls.isAccountTrgExecuted = true;
                }catch(Exception ex){
                    
                    Log__c objLog = new Log__c();
                    objLog.Account__c = lstupdatedCompliance[0].Account__c;
                    objLog.Description__c = ex.getMessage() +'   '+ex.getLineNumber();
                    objLog.Type__c = 'Compliance Updation (when account trigger tries to update compliance owner)';
                    insert objLog;
                }   
            }
             //v 1.9 end 
        }
        
        
        if(lstEntHist!=null && lstEntHist.size()>0){
            try{
                insert lstEntHist;
                RecursiveControlCls.isAccountTrgExecuted = true;
            }catch(Exception e){
            
            }   
        }
        list<Id> lstAccIds = new list<Id>();        
        /* Commeented by Ravi on 15th Sep, system will send Compliance Notifications to all the Portal users
        //added by ravi on 15th Jan, 2014 - whenever the Principal User got changed update the same on Active Compliance records also
        list<Compliance__c> lstCompliances = new list<Compliance__c>();
        for(Account objAccount : trigger.new){
            if(objAccount.Principal_User__c != trigger.oldMap.get(objAccount.Id).Principal_User__c){
                lstAccIds.add(objAccount.Id);
            }
        }
        if(!lstAccIds.isEmpty()){
            for(Compliance__c objComp : [select Id,Principal_User__c,Account__c from Compliance__c where Account__c IN : lstAccIds AND Status__c != 'Fulfilled']){
                objComp.Principal_User__c = trigger.newMap.get(objComp.Account__c).Principal_User__c;
                lstCompliances.add(objComp);
            }
            if(!lstCompliances.isEmpty())
                update lstCompliances;
        }*/
        
        //v1.2
        map<Id,Id> mapSRIds = new map<Id,Id>();
        lstAccIds = new list<Id>();
        map<Id,Service_Request__c> mapSRs = new map<Id,Service_Request__c>();
        Service_Request__c objSR;
        map<string,string>  MapAccountIds  = new map<string,string>();
        for(Account objA : trigger.new){
            if(objA.ROC_Status__c == 'Active' && trigger.oldMap.get(objA.Id).ROC_Status__c != objA.ROC_Status__c ){
                lstAccIds.add(objA.Id);
            }
            //veera added 
            system.debug('!!@@!!@@!'+objA.ROC_Status__c);
            if(objA.ROC_Status__c == 'Under Formation' && trigger.oldMap.get(objA.Id).ROC_Status__c != objA.ROC_Status__c){             
                MapAccountIds.put(objA.id,objA.id);
            }
        }
        
        //veera adeed
        system.debug('!!@@@!!!'+MapAccountIds);
        if(MapAccountIds.size()>0 ){
            
            SAPWebServiceDetails.CRMPartnerExtensionCall(MapAccountIds);
           // SAPWebServiceDetails.CRMPartnerCall(null,null,MapAccountIds);
        }
        
        if(!lstAccIds.isEmpty()){
            for(Tenancy__c objTen : [select Id,Lease__c,Unit__c from Tenancy__c where Unit__c != null AND Lease__c IN (select Id from Lease__c where Account__c IN : lstAccIds AND Lease_Types__c = 'Business Centre Lease' AND Status__c = 'Active' )]){
                mapSRIds.put(objTen.Unit__c,null);
            }
            
            if(!mapSRIds.isEmpty()){
                for(Amendment__c objAmd : [select Id,ServiceRequest__c,Lease_Unit__c from Amendment__c where Lease_Unit__c != null AND Lease_Unit__c IN : mapSRIds.keySet() AND ServiceRequest__r.Notify_Signage__c = false AND ServiceRequest__r.Customer__c IN : lstAccIds]){
                    mapSRIds.put(objAmd.Lease_Unit__c,objAmd.ServiceRequest__c);
                }
                
                for(Id SRId : mapSRIds.values()){
                    if(SRId != null){
                        objSR = new Service_Request__c(Id=SRId);
                        objSR.Notify_Signage__c = true;
                        mapSRs.put(objSR.Id,objSR);
                    }
                }
                
                if(!mapSRs.isEmpty()){
                    update mapSRs.values();
                }
            }
        }
    }  
    // V2.4  
    if(trigger.isAfter && RecursiveControlCls.isBPGenerated == false){
        //Logic execute when we create a new Account through Lead. For Self registration we are creating BP only when RM assignment is done
        Map<String,String> mapAccountIdsBP = new Map<String,String>();
        for(Account eachAccount:trigger.new){
            mapAccountIdsBP.put(eachAccount.ID,'ConvertedAccount');
            if(eachAccount.BP_No__c == null && ((trigger.isInsert && eachAccount.Created_by_Agent__c == null) || 
            (trigger.isUpdate && eachAccount.Update_BP_for_Guest_users__c != trigger.oldmap.get(eachAccount.Id).Update_BP_for_Guest_users__c
             && eachAccount.Update_BP_for_Guest_users__c == true)) 
            && eachAccount.ROC_Status__c !=null){
                SAPWebServiceDetails.CRMPartnerCall(null,null,mapAccountIdsBP); 
            }
        } 
        
    }
        // Below Logic added by Sai. Salesforce to Mashreq Integration
        system.debug('!!@@##'+RecursiveControlCls.isDataSenttoMashreq);
        if(trigger.isAfter && trigger.isupdate && RecursiveControlCls.isDataSenttoMashreq == false){
            List<Account> lstAccounts = new List<Account>();
            
            for(Account eachAccount:trigger.new){
                if(String.isNotBlank(eachAccount.Bank_Name__c) && eachAccount.Wish_to_create_a_Bank_account_via_DIFC__c == 'Yes' &&  trigger.oldmap.get(eachAccount.ID).Welcome_email_to_Customer__c != true &&
                    eachAccount.Welcome_email_to_Customer__c == true ){
                    lstAccounts.add(eachAccount);
                }
            }
            system.debug('!!@###@'+lstAccounts);
            if(lstAccounts !=null && lstAccounts.size() >0){
                AccountTriggerHelper.createaBankAccount(lstAccounts);
            }
            
        }
    }//End Disable user check

     if(trigger.isAfter && trigger.isupdate){
         AccountTriggerHelper.changeRelationshipManageronAOR(trigger.newMap,trigger.oldMap);
     }
}