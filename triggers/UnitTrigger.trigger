/***********************************************************************
* Class : UnitTrigger
* Description : Unit trigger to check the unit availability :Commercial Unit Offer Process 5633
* Created Date : 06/09/2018
************************************************************************/
trigger UnitTrigger on Unit__c (After update) {
    if(trigger.IsAfter && trigger.IsUpdate){
        if(!UnitAvailabilityUtilityClass.isUnitstatusCheked){
            UnitAvailabilityUtilityClass.checkunitStatus(trigger.new);
        }
    }
}