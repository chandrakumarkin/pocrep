/**************************************************************************************************
* Name               : OB_CompanyNameTrigger                                                           
* Description        : Trigger on Company Name SObject.                                        
* Created Date       : 27th February 2020                                                                *
* Created By         : Leeba Shibu (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author    Date           Comment                                                                *
  
**************************************************************************************************/
trigger OB_CompanyNameTrigger on Company_Name__c (before insert,after insert,after update) 
{
    
    //Check custom label stored user id same as running user id ? if not the same run the trigger 
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled || test.isRunningtest())
    {
        if(trigger.isAfter && trigger.isUpdate)
            OB_CompanyNameTriggerHandler.OnAfterUpdate(trigger.old,trigger.new,Trigger.oldMap,Trigger.newMap);

    }
    
}