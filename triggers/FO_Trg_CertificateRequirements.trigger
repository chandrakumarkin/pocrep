trigger FO_Trg_CertificateRequirements on Certificate__c (before update) {
	
	List<id> SRList = new List<id>();
	list<Amendment__c> amendmentList = new list<Amendment__c>();
	for(Certificate__c obj : Trigger.new){
		SRList.add(obj.Service_Request__c);
	}
	
	if(!SRList.isEmpty()){
    	amendmentList = [select id,Amendment_Type__c,end_date__c from Amendment__c where ServiceRequest__c IN : SRList and record_type_name__c=:'Contractor_Insurance'];		
	}
	
	for(Certificate__c obj : Trigger.new){
		if(obj.end_date__c!=null){
			if(obj.Type__c=='Fit-Out Permit'){
				obj.notification_date__c = date.valueOf(CC_cls_FitOutandEventCustomCode.calculateStartDate(obj.end_date__c,5));	
			}
			if(obj.Type__c=='Design Approval'){
				obj.notification_date__c = date.valueOf(CC_cls_FitOutandEventCustomCode.calculateStartDate(obj.end_date__c,10));	
			}	
		}
		if(obj.type__c != 'Fit-Out Permit' && obj.Exception_Date__c!=null && obj.Exception_Date__c!=trigger.oldMap.get(obj.id).Exception_Date__c){
			obj.end_date__c = 	obj.Exception_Date__c;	
		}
		else if(obj.type__c == 'Fit-Out Permit' && obj.Exception_Date__c!=null && obj.Exception_Date__c!=trigger.oldMap.get(obj.id).Exception_Date__c){
			if(!amendmentList.isEmpty()){
				for(Amendment__c objAmen : amendmentList){
            		if(objAmen.Amendment_Type__c == 'All Risk Insurance' && objAmen.end_date__c<obj.Exception_Date__c){
            			obj.addError('Exception Date should be less then Contractor All Risk Insurance End Date');		
            		}
            		else if(objAmen.Amendment_Type__c == 'Third Party Liability' && objAmen.end_date__c<obj.Exception_Date__c){
            			obj.addError('Exception Date should be less then Contractor Third Party Liability certificate End Date');	
            		}
            		else if(objAmen.Amendment_Type__c == 'Workmen Compensation Insurance Policy' && objAmen.end_date__c<obj.Exception_Date__c){
            			obj.addError('Exception Date should be less then Contractor Workmen Compensation Insurance Policy End Date');	
            		}
            	}
            	obj.end_date__c = 	obj.Exception_Date__c;
			}
		}
		
	}    
}