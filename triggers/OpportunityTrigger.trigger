/********************************************
* Trigger Name : OpportunityTrigger 
* Description : if the lead converted with Offer Process ,all the related offer process will be linked with opportunity
* Author : Selva
*  Date : 16-jul-2018 , Lead and Offer Process flow
****************Version History************************************************************************************************
Author          Date            Version         Description
Rajil          15-01-2020         V1.1         Added the Trigger Handler to populate the opportunity on Application
*****************************************************************************************************************************/
trigger OpportunityTrigger on Opportunity (before insert,after insert,Before update) {

    //Check custom label stored user id same as running user id ? if not the same run the trigger 
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled)
    {
        String commercialID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
        String retailID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('BD Retail').getRecordTypeId();
        String specialtyID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('BD Specialty').getRecordTypeId();
    
    
        //changing the default stage and record type value if the opportunity created from offer process lead.
        if(trigger.isInsert && trigger.isBefore){
            
            for(opportunity itr:trigger.new){
                if(itr.Lead_Reference_Number__c!=null && itr.Lead_Id__c!=null && itr.Oppty_Rec_Type__c=='Leasing OfferProcess'){
                    itr.RecordTypeID = commercialID;
                    itr.StageName = 'Confirmed Unit';
                }
                if(itr.Lead_Reference_Number__c!=null && itr.Lead_Id__c!=null && itr.Oppty_Rec_Type__c=='Retail'){
                    itr.RecordTypeID = retailID;
                    itr.StageName = 'Confirmed Unit';
                }
                if(itr.Lead_Reference_Number__c!=null && itr.Lead_Id__c!=null && itr.Oppty_Rec_Type__c=='Special Leasing'){
                    itr.RecordTypeID = specialtyID;
                    itr.StageName = 'Confirmed Unit';
                }
            }
        }
        
        //update the opportuniy on offer process records
        if(trigger.isAfter && trigger.isInsert){
            Map<Id,string> leadId = new Map<Id,string>();
            for(Opportunity itr:trigger.new){
                if(itr.Lead_Id__c!=null && itr.RecordTypeID == commercialID){
                    leadId.put(string.valueof(itr.Lead_Id__c),string.valueof(itr.Id));
                }
                else if(itr.Lead_Id__c!=null && itr.RecordTypeID == retailID){
                    leadId.put(string.valueof(itr.Lead_Id__c),string.valueof(itr.Id));
                }
                else if(itr.Lead_Id__c!=null && itr.RecordTypeID == specialtyID){
                    leadId.put(string.valueof(itr.Lead_Id__c),string.valueof(itr.Id));
                }
            }
            if(leadId.size()>0 && !OpportunityTriggerUtility.isOfferProcessupdated){
                OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
            }

            //V1.1: Onboarding Trigger Handler 
            OB_OpportunityTriggerHandler.Execute_AI(trigger.new);
        }
        //check the attachment in each stage for offer process record type
        if(trigger.isBefore && trigger.isUpdate){
            OpportunityTriggerUtility.isAttched(trigger.new);
        }
    }
}