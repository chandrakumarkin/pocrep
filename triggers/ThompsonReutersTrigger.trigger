/******************************************************************
Created By  : Selva Created on 14-Jan-2020
Description: When TR case closed by scheduler, the related AML step will be updated
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
********************************************************************/

trigger ThompsonReutersTrigger on Thompson_Reuters_Check__c (After update) {
    //if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled){
    
       if(trigger.isAfter && trigger.isUpdate){
           system.debug('in TR');
           Boolean isCaseOpen = false;
           set<Id> srId = new set<Id>();
           Map<Id,string> TRcasecomments = new Map<Id,string>();
           List<step__c> updateSteplst = new List<step__c>();
            for(Thompson_Reuters_Check__c itr : trigger.new){
                system.debug('--case status--'+itr.TR_Case_Status__c);
                if(itr.TR_Case_Status__c=='CloseCase' && itr.TRCase_No__c!=null){
                    
                    if(itr.ServiceRequest__c!=null){
                        srId.add(itr.ServiceRequest__c);
                        if(itr.Case_Comments__c!=null){
                            TRcasecomments.put(itr.ServiceRequest__c,itr.Case_Comments__c);
                        }
                    }
                }
            }
            List<Thompson_Reuters_Check__c> trCaseClosedList = [select id,Case_Comments__c,TRCase_No__c,TR_Case_Status__c from Thompson_Reuters_Check__c where ServiceRequest__c IN:srId];
            integer casecount = 0;
            integer closedCase = 0;
            Boolean allcaseClosed = false;
            for(Thompson_Reuters_Check__c itr:trCaseClosedList){
                if(itr.TRCase_No__c!=null){
                    casecount  = casecount+1;
                }
                if(itr.TR_Case_Status__c=='CloseCase'){
                    closedCase  = closedCase+1;
                }
            }
            if(casecount==closedCase){
                allcaseClosed = true;
            }
            
            system.debug('---line 32--'+trCaseClosedList.size()+'---'+casecount+'----'+closedCase);
            
            if(srId.size()>0 && allcaseClosed){
                map<string,string> srAutoApprovemap = new map<string,string>();

                List <step__c> stpList = [select id,sr__r.Customer__r.AML_Rating__c,SR__c,Status__c,Step_Status__c,SR_Record_Type_Text__c ,Step_Code__c,TR_case_comments__c from step__c where step_code__c = 'AML_APPROVAL' and sr__c IN:srId];
                string srRecType = null;
                if(stpList.size()>0){
                    srRecType = stpList[0].sr__r.Customer__r.AML_Rating__c;
                    Boolean isHighSanctioned = CreateConditionExtension.checkAMLNationality(stpList[0].sr__c);
                    Boolean isUBOHighSanctioned = CreateConditionExtension.checkUBONationality(stpList[0].sr__c);
                    Boolean isAddReq =  CC_CreateConditions.Check_IS_Add_in_SR_Amendment(stpList[0].SR__c);
                    if(!isUBOHighSanctioned && !isHighSanctioned && ((srRecType!='High') || (srRecType=='High' && !isAddReq))){
                    //if((((!CreateConditionExtension.checkAMLNationality(stpList[0].sr__c) && (srRecType!='High')) && (srRecType=='High' && CC_CreateConditions.Check_IS_Add_in_SR_Amendment(stepList[0].SR__c))))){
                        if(stpList.size()>0){
                            Status__c stpStatus = [select id from Status__c where code__c = 'Approved'];
                            for(step__c itr:stpList){
                              if(TRcasecomments.containsKey(itr.sr__c) && (itr.Step_Status__c!='Closed' || itr.Step_Status__c!='Rejected')){
                                  //itr.TR_case_comments__c = TRcasecomments.get(itr.sr__c);
                                   string casecomments = TRcasecomments.get(itr.sr__c);
                                    Integer maxSize = 200;
                                    if(casecomments.length() > maxSize ){
                                        itr.TR_case_comments__c= casecomments.substring(0, maxSize);
                                    }else{
                                        itr.TR_case_comments__c= casecomments;
                                    }
                                  itr.Status__c = stpStatus.Id;
                                  updateSteplst.add(itr);
                              }
                            }
                            if(updateSteplst.size()>0){
                              update updateSteplst;
                            }
                        }
                    }
                }
                  /*********
                  else{
                        //SR auto approve for not matching TR nationality anf TR cases created //#9733
                        List<Service_Request__c> srList= [select id,Record_Type_Name__c from Service_Request__c where id IN:srId];
                        Map<string,TR_Check_Status__c> trCheckStatus = TR_Check_Status__c.getAll();
                        Boolean istypetwo = false;
                        for(TR_Check_Status__c tcs:trCheckStatus.values()){
                            for(Service_Request__c itr:srList){
                                if(tcs.SR_Record_Type__c == itr.Record_Type_Name__c && tcs.Type__c =='2'){
                                    CreateConditionExtension.SRAutoApproveTwo(itr.id);
                                }
                            }
                            
                        }
                  }**********/
            }
       }
    //}
}