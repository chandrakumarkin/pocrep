/*
    Created By  : RB Nagaboina on 16th Sep,2015
    Description : This trigger will update Lease Information on Lease, for RORP Leases all the information will store in Lease Partner object
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 V1.0   16/09/2015      Ravi            Created
 V1.1	09/11/2016		Ravi			a. Added the logic to create operating locations from lease as per #2516
 										b. For 4 Series BP's Status will be blank from from INFORMATICA, and this trigger will update the Status from Lease
 ----------------------------------------------------------------------------------------
*/
trigger LeasePartnerTrg on Lease_Partner__c (before insert,before update,after insert,after update,after delete,after undelete) {
    
    map<Id,string[]> mapLeaseInfo = new map<Id,string[]>();
    map<Id,list<Date>> mapLeaseStartDates = new map<Id,list<Date>>();
    map<Id,list<Date>> mapLeaseEndDates = new map<Id,list<Date>>();
    map<Id,set<string>> mapLeaseStatus = new map<Id,set<string>>();
    map<Id,Boolean> mapLeaseTypes = new map<Id,Boolean>();
    map<string,string> mapPartners = new map<string,string>();
    map<Id,Boolean> mapLicToOcc = new map<Id,Boolean>();
    list<Date> lstSD;
    list<Date> lstED;
    
    list<Id> lstLeaseIds = new list<Id>();
    list<Lease__c> lstLeases = new list<Lease__c>();
    Lease__c objLease;
    //1.1b
	map<Id,String> mapLeastStatus = new map<Id,String>();
    
    map<string,Tenancy__c> mapTenancies = new map<string,Tenancy__c>();
    map<string,Tenancy__c> mapExitingTens = new map<string,Tenancy__c>();
    
    for(Lease_Partner__c objLP : (trigger.isDelete ? trigger.old : trigger.new) ){
        if(objLP.Lease__c != null && objLP.Is_6_Series__c)
            lstLeaseIds.add(objLP.Lease__c);
        if(objLP.SAP_PARTNER__c != null && objLP.SAP_PARTNER__c != '' && (objLP.Account__c == null && objLP.Contact__c == null))
            mapPartners.put(objLP.SAP_PARTNER__c,null);
    	if(objLP.Lease__c != null && objLP.Is_6_Series__c == false){
    		if(trigger.isInsert)
    			mapLeastStatus.put(objLP.Lease__c,null);
    		else if(trigger.isUpdate && objLP.Status__c != objLP.Lease_Status__c){
    			mapLeastStatus.put(objLP.Lease__c,objLP.Lease_Status__c);
    		}	
    	}	
    }
    if(trigger.isBefore){
        if(!mapPartners.isEmpty()){
            map<string,string> mapAccountIds = new map<string,string>();
            map<string,string> mapContactIds = new map<string,string>();
            
            for(Account objA : [select Id,BP_No__c from Account where BP_No__c IN : mapPartners.keySet() AND BP_No__c != null]){
                mapAccountIds.put(objA.BP_No__c,objA.Id);
            }
            for(Contact objC : [select Id,BP_No__c from Contact where BP_No__c IN : mapPartners.keySet() AND BP_No__c != null]){
                mapContactIds.put(objC.BP_No__c,objC.Id);
            }
            for(Lease_Partner__c objLP : trigger.new){
                if(objLP.SAP_PARTNER__c != null && mapPartners.containsKey(objLP.SAP_PARTNER__c)){
                    if(mapContactIds.get(objLP.SAP_PARTNER__c) != null)
                        objLP.Contact__c = mapContactIds.get(objLP.SAP_PARTNER__c);
                    else if(mapAccountIds.get(objLP.SAP_PARTNER__c) != null)
                        objLP.Account__c = mapAccountIds.get(objLP.SAP_PARTNER__c);
                }   
            }
        }
        if(mapLeastStatus.isEmpty() == false ){
        	for(Lease__c objL : [select Id,Status__c from Lease__c where Id IN : mapLeastStatus.keySet()]){
        		mapLeastStatus.put(objL.Id,objL.Status__c);
        	}
        	for(Lease_Partner__c objLP : trigger.new){
                if(objLP.Lease__c != null && objLP.Is_6_Series__c == false && mapLeastStatus.containsKey(objLP.Lease__c)){
                	objLP.Status__c = mapLeastStatus.get(objLP.Lease__c);
                } 
            }
        }
    }else if(trigger.isAfter){
        if(!lstLeaseIds.isEmpty()){
            for(Tenancy__c objTen : [select Id,Lease__c,Unit__c from Tenancy__c where Lease__c IN : lstLeaseIds AND Lease__c != null AND Unit__c != null]){
                mapExitingTens.put(objTen.Lease__c+'-'+objTen.Unit__c,objTen);
            }
            for(Lease_Partner__c objLP : [select Id,Name,Account__c,Contact__c,End_Date__c,Industry__c,Lease__c,Lease_Types__c,Square_Feet__c,Start_date__c,Status__c,
                                            Type_of_Lease__c,Unit__c,Is_Sub_Lease__c,Is_License_to_Occupy__c 
                                            from Lease_Partner__c where Lease__c IN : lstLeaseIds]){
                string[] LeaseInfo = mapLeaseInfo.containsKey(objLP.Lease__c) ? mapLeaseInfo.get(objLP.Lease__c) : new string[]{'','','',''};
                // LeaseInfo[0] - Square Feet
                // LeaseInfo[1] - Lease Types
                // LeaseInfo[2] - Industry
                // LeaseInfo[3] - Type of Lease
                LeaseInfo[0] = objLP.Square_Feet__c != null ? objLP.Square_Feet__c : LeaseInfo[0];
                LeaseInfo[1] = objLP.Lease_Types__c != null ? objLP.Lease_Types__c : LeaseInfo[1];
                LeaseInfo[2] = objLP.Industry__c != null ? objLP.Industry__c : LeaseInfo[2];
                LeaseInfo[3] = objLP.Type_of_Lease__c != null ? objLP.Type_of_Lease__c : LeaseInfo[3];
                mapLeaseInfo.put(objLP.Lease__c,LeaseInfo);
                
                if(objLP.Start_date__c != null){
                    lstSD = mapLeaseStartDates.containsKey(objLP.Lease__c) ? mapLeaseStartDates.get(objLP.Lease__c) : new list<Date>() ;
                    lstSD.add(objLP.Start_date__c);
                    mapLeaseStartDates.put(objLP.Lease__c,lstSD);
                }
                if(objLP.End_Date__c != null){
                    lstED = mapLeaseEndDates.containsKey(objLP.Lease__c) ? mapLeaseEndDates.get(objLP.Lease__c) : new list<Date>() ;
                    lstED.add(objLP.End_Date__c);
                    mapLeaseEndDates.put(objLP.Lease__c,lstED);
                }
                set<string> setTemp = mapLeaseStatus.containsKey(objLP.Lease__c) ? mapLeaseStatus.get(objLP.Lease__c) : new set<string>();
                setTemp.add(objLP.Status__c);
                mapLeaseStatus.put(objLP.Lease__c,setTemp);
                
                if(objLP.Unit__c != null && mapExitingTens.containsKey(objLP.Lease__c+'-'+objLP.Unit__c) == false){ //there is no Tenancy for Lease 
                    //Create the tenancy
                    Tenancy__c objT = new Tenancy__c();
                    objT.Lease__c = objLP.Lease__c;
                    objT.Unit__c = objLP.Unit__c;
                    if(objLP.Square_Feet__c != null && objLP.Square_Feet__c != '')
                        objT.Square_Feet__c = decimal.valueOf(objLP.Square_Feet__c.replaceAll('-',''));
                    mapTenancies.put(objLP.Lease__c+'-'+objLP.Unit__c,objT);
                    mapExitingTens.put(objLP.Lease__c+'-'+objLP.Unit__c,objT);
                }
                if(objLP.Status__c == 'Active')
                    mapLeaseTypes.put(objLP.Lease__c,objLP.Is_Sub_Lease__c);
                if(objLP.Lease__c != null && (mapLicToOcc.containsKey(objLP.Lease__c) == false || mapLicToOcc.get(objLP.Lease__c) == false))
                    mapLicToOcc.put(objLP.Lease__c,objLP.Is_License_to_Occupy__c);
            }
            if(!mapLeaseInfo.isEmpty()){
                for(Id LeaseId : mapLeaseInfo.keySet()){
                    string[] LeaseInfo = mapLeaseInfo.get(LeaseId);
                    objLease = new Lease__c(Id=LeaseId);
                    objLease.Square_Feet__c = LeaseInfo[0];
                    objLease.Lease_Types__c = LeaseInfo[1];
                    objLease.Industry__c = LeaseInfo[2];
                    objLease.Type__c = LeaseInfo[3];
                    if(mapLeaseTypes.containsKey(LeaseId))
                        objLease.Is_Sub_Lease__c = mapLeaseTypes.get(LeaseId);
                    if(mapLeaseStartDates.containsKey(LeaseId) && mapLeaseStartDates.get(LeaseId) != null){
                        lstSD = mapLeaseStartDates.get(LeaseId);
                        lstSD.sort();
                        objLease.Start_date__c = lstSD[0];
                    }
                    if(mapLeaseEndDates.containsKey(LeaseId) && mapLeaseEndDates.get(LeaseId) != null){
                        lstED = mapLeaseEndDates.get(LeaseId);
                        lstED.sort();
                        objLease.End_Date__c = lstED[lstED.size()-1];
                    }
                    if(mapLeaseStatus.containsKey(LeaseId)){
                        if(mapLeaseStatus.get(LeaseId).contains('Active') && mapLeaseStatus.get(LeaseId).contains('Inactive') == false){
                            objLease.Status__c = 'Active';
                        }else{
                            for(string s : mapLeaseStatus.get(LeaseId)){
                                objLease.Status__c = s;
                                break;
                            }
                        }
                    }
                    if(mapLicToOcc.containsKey(LeaseId)){
                        objLease.Is_License_to_Occupy__c = mapLicToOcc.get(LeaseId);
                    }
                    lstLeases.add(objLease);
                }
                if(!lstLeases.isEmpty())
                    update lstLeases;
            }
            if(!mapTenancies.isEmpty()){
                insert mapTenancies.values();
            }
        }
    }   
    
    //v1.1
    TriggerFactoryCls.createHandler(Lease_Partner__c.sObjectType);
    
}