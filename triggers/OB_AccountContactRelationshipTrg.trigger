trigger OB_AccountContactRelationshipTrg on AccountContactRelation (before insert,before update,after insert,after update,before delete) {
     
    //Check custom label stored user id same as running user id ? if not the same run the trigger 
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled || test.isRunningtest())
    {
        //Mudasir added the below action
        if(trigger.isBefore && trigger.isInsert)
            OB_AcctContactTriggerHandler.onBeforeInsert(trigger.new);
            
        if(trigger.isAfter && trigger.isInsert)
            OB_AcctContactTriggerHandler.onAfterInsert(trigger.new);
        if(trigger.isAfter && trigger.isUpdate)
            OB_AcctContactTriggerHandler.onAfterUpdate(trigger.old,trigger.new,Trigger.oldMap,Trigger.newMap);
        if(trigger.isBefore && trigger.isDelete)
            OB_AcctContactTriggerHandler.onBeforeDelete(trigger.old);     
    }
    
      
}