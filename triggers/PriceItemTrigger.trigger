/*
    Author      :   Shoaib Tariq
    Description :   This class is update SAP Unique Id for Miscellaneous_Services
    --------------------------------------------------------------------------------------------------------------------------
  Modification History
   --------------------------------------------------------------------------------------------------------------------------
  V.No  Date    Updated By      Description
  --------------------------------------------------------------------------------------------------------------------------             
   V1.0    17-02-2020  shoaib      Created
   v1.1    22-04-2021  Veera       to update PSA Pipeline amount not pushed to SAP
*/
trigger PriceItemTrigger on SR_Price_Item__c (after update) {
    if(trigger.isAfter && Trigger.isUpdate){
        PriceItemTriggerHandler.UpdateSAPUniqueField(Trigger.new);
        
        List<SR_Price_Item__c> ListSRPriceItems = new List<SR_Price_Item__c>();
        
        if(system.label.PriceItemTriggerLogicisRun == 'True'){
        for(SR_Price_Item__c  PriceItem : Trigger.new ){
        
        
        
           if(PriceItem.SR_Group__c =='GS' && PriceItem.Product_Name__c =='PSA' && (PriceItem.Status__c =='Blocked' || PriceItem.Status__c =='Invoiced' ||  PriceItem.Status__c =='Cancelled')){       
             
              ListSRPriceItems.add(PriceItem);
              
           
           }
        
        }
        if(ListSRPriceItems.size()>0) 
         PriceItemTriggerHandler.UpdatePipelinePSAonAccount(ListSRPriceItems);
        }
     }   
    
}