/*
    Author       :  Durga Prasad
    Trigger Name :  Amnd_Recordtype_AssgnTrg
    Description  :  This trigger will assigns the recordtype for the amendment based on the relationship type.

--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 V1.0   -               Durga           Created                            
 v1.1   9/06/2015       RB Nagabiona    Added the logic to validate the record for Liquidator
 v1.2   29/06/2015      RB Nagaboina    Added the logic to populate the linked SR for Liquidator
 V1.3   20/08/2015      Saima Hidayat   Added Validation for Business Centre forms
 V1.4   30/08/2015      RB Nagaboina    Added the logic to update the SR - RORP Tenant Type
 V1.5   08/Sep/2015     Shabbir         Added the logic to the SR - RORP Doc generation
 V1.6   25/Oct/2015     Kaavya          Added the logic for RORP Freehold
 V1.7   28/Oct/2015     Shabbir         Added the logic for RORP Freehold Doc generation
 V1.8   09/11/2015      Saima           Changes for DIFC Registered Validtaion only for Lic No
 V1.9   17/11/2015      Kaavya          Changes for RORP Shareholder Company
 V1.10  18/11/2015      Kaavya          Added check for including issuing authority
 V1.11  06/Jan/2016     Shabbir         Added the logic for GSO Authorized Signatory passport, visa document generation
 V1.12  07/01/2016      Saima           Changes to allow landlord in Sub-Lease
 V1.13  24/01/2016      Swati           Changes to update Unit for Contractor Access
 V1.14  25/01/2016      Shabbir         Added logic for RORP - Freehold registration document generation if seller is developer
 V1.15  20/03/2016      Claude          Added logic for checking existing fit-out contact
 V1.16  21/03/2016      Claude          Made 'Architect / Interior Designer' and 'Consultant / Project Manager' documents optional
 V1.17  20/03/2016      Shabbir         Added lease validation for business centre to include notices leases in new lease registration request
 V1.18  18/04/2016      Kaavya          Added RORP Shareholder Company check for DIFC Registered Company
 V1.19  30/06/2016      Kaavya          For FitOut -Modified condition for checking closed SRs for units inc ontractor access
 V1.20  23/08/2016      Ravi            Removed/Modified the code to support Multiple Units Lease Registration
 V1.21  16/10/2016      Ravi            Modified the code to support Mortgage process
 V1.22  28/12/2016      Kaavya          Fix when Fit Out Units are updated    
 V1.23  11/01/2017      Claude          Added try-catch block to wrap messages (#3825); Added floor for Fit-out Units (#3818)
 V1.24  23/01/2017      Claude          Added changes for floor and unit text (#3840)
 V1.25  10/05/2017      Sravan          Including the record type for GSO Authorized signatory for Establishment card changes as per # 4042
 V1.26  04-Dec-2018    Mudasir        #4214 added filter in the query to exclude 'Fit to Occupy Certificate Issued' and Completion Certificate Issued projects
 V1.27  14-Jan-2019    Mudasir        #6199 added filter in the query to exclude 'Look and Feel' units for Lease Partner usage.
 V1.28  07-May-2020     Sai             Commented DFSA Approval / Provisional Approval document for Under formation compaines.
 V1.29  23-Jun-2021     Suchita         #DIFC2-16775 - Document name update to Passport copy & signature page
 V1.30  4-Aug-2021		Suchita			#DIFC2-17716 - Ticket 16830 
 ******************************************************************************************
*/
trigger Amnd_Recordtype_AssgnTrg on Amendment__c (before insert,before update,after insert,after update, after delete) {
    
    try{
        if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
            Schema.DescribeSObjectResult Amnd = Schema.SObjectType.Amendment__c; 
            Map<String,Schema.RecordTypeInfo> rtMapByName = Amnd.getRecordTypeInfosByName();
            map<string,string> mapAmndRecTypes = new map<string,string>();
            for(string RecTypeName:rtMapByName.keyset()){
                mapAmndRecTypes.put(RecTypeName,rtMapByName.get(RecTypeName).getRecordTypeId());
            }
            system.debug('mapAmndRecTypes==>'+mapAmndRecTypes);
            
            map<string,string> map_Amnd_Rel_SAPRel = new map<string,string>{'CEO'=>'Has CEO/Regional Head/Chairman','Personal Assistant'=>'Has Personal Assistant','Senior Executive Officer (SEO)'=>'Has Senior Executive Officer (SEO)','Office Manager'=>'Has Office Manager',
                'Corporate Communication'=>'Has Corporate Communication','IT contact'=>'Has IT contact','IT Data Center contact'=>'Has IT Data Center contact'};
            
            for(Amendment__c objAmnd:trigger.new){
                system.debug('objAmnd '+objAmnd.Record_Type_Name__c);
                if(objAmnd.Amendment_Type__c=='Operating Location' && mapAmndRecTypes.get('Operating Location')!=null){
                    objAmnd.RecordTypeId = mapAmndRecTypes.get('Operating Location');
                }else if(objAmnd.Relationship_Type__c!=null && map_Amnd_Rel_SAPRel.get(objAmnd.Relationship_Type__c)!=null){
                    objAmnd.RecordTypeId = mapAmndRecTypes.get(objAmnd.Relationship_Type__c);
                }else{
                    if(objAmnd.Relationship_Type__c!=null && (objAmnd.Amendment_Type__c=='Individual' || objAmnd.Amendment_Type__c=='Body Corporate')){
                        if(objAmnd.Relationship_Type__c=='Auditor'){
                            if(mapAmndRecTypes.get('Auditor')!=null)
                                objAmnd.RecordTypeId = mapAmndRecTypes.get('Auditor');
                        }else if(objAmnd.Relationship_Type__c=='UBO'){
                            if(mapAmndRecTypes.get('UBO')!=null)
                                objAmnd.RecordTypeId = mapAmndRecTypes.get('UBO');
                        }else if(objAmnd.Relationship_Type__c=='Authorized Signatory'){
                            if(mapAmndRecTypes.get('Authorized Signatory')!=null)
                                objAmnd.RecordTypeId = mapAmndRecTypes.get('Authorized Signatory');
                        }else if(objAmnd.Relationship_Type__c=='Limited Partner' || objAmnd.Relationship_Type__c=='General Partner'){
                            if(objAmnd.Amendment_Type__c=='Individual'){
                                if(mapAmndRecTypes.get('Partner Individual')!=null)
                                    objAmnd.RecordTypeId = mapAmndRecTypes.get('Partner Individual');
                            }else{
                                if(mapAmndRecTypes.get('Partner BC')!=null)
                                    objAmnd.RecordTypeId = mapAmndRecTypes.get('Partner BC');
                            }
                        }else if(objAmnd.Relationship_Type__c=='Shareholder' || objAmnd.Relationship_Type__c=='Member'){
                            if(objAmnd.Amendment_Type__c=='Individual'){
                                if(mapAmndRecTypes.get('Shareholder/Member Individual')!=null)
                                    objAmnd.RecordTypeId = mapAmndRecTypes.get('Shareholder/Member Individual');
                            }else{
                                if(mapAmndRecTypes.get('Shareholder/Member BC')!=null)
                                    objAmnd.RecordTypeId = mapAmndRecTypes.get('Shareholder/Member BC');
                            }
                        }else if(objAmnd.Relationship_Type__c=='GSO Authorized Signatory' && mapAmndRecTypes.containsKey('GSO Authorized Signatory')){ // V1.25 
                            objAmnd.RecordTypeId = mapAmndRecTypes.get('GSO Authorized Signatory');
                        }else{
                            if(objAmnd.Amendment_Type__c=='Individual'){
                                if(mapAmndRecTypes.get('Individual')!=null)
                                    objAmnd.RecordTypeId = mapAmndRecTypes.get('Individual');
                            }else if(objAmnd.Amendment_Type__c=='Body Corporate'){
                                if(mapAmndRecTypes.get('Body Corporate')!=null)
                                    objAmnd.RecordTypeId = mapAmndRecTypes.get('Body Corporate');
                            }
                        }
                    }
                }
            }
        }
        
        /* Added by Ravi on 9th June, 2015 for exist process */
        if(trigger.isBefore && (trigger.isInsert || trigger.isUpdate)){
            list<Id> lstLequidatorIds = new list<Id>();
            list<Id> lstSRIds = new list<Id>();
            map<string,list<Amendment__c>> mapSRAmends = new map<string,list<Amendment__c>>();
            map<string,string> mapSRUnitType = new map<string,string>();
            map<string,string> mapSRLeaseType = new map<string,string>();
            map<string,string> mapUnitActiveTen = new map<string,string>();
            map<string,string> mapLiquidators = new map<string,string>();
            map<string,string> mapLeasePartners = new map<string,string>();
            string SRID = '';
            set<string> recType = new set<string>();
            set<string> unitId = new set<string>();//string unitId = '';
            set<string> BCCLNo = new set<string>();//string BCCLNo = '';
            //set<string> BCCompName = new set<string>();//string BCCompName='';
            set<string> BCIssuAuth = new set<string>();                         //v1.10 Added by Kaavya 18/11/2015 - For checking issuing authority
            set<string> IndPass = new set<string>();//string IndPass = '';
            set<string> IndNat = new set<string>();//string IndNat = '';
            string SRstatus = '';
            set<string> landlordType = new set<string>();//string landlordType ='';
            for(Amendment__c objAmd : trigger.new ){
                if(objAmd.Record_Type_Name__c == 'Liquidator' && objAmd.ServiceRequest__c != null && objAmd.Liquidator__c != null && (trigger.isInsert || (trigger.isUpdate && trigger.oldMap.get(objAmd.Id).Liquidator__c != objAmd.Liquidator__c))){
                    lstLequidatorIds.add(objAmd.Liquidator__c);
                    lstSRIds.add(objAmd.ServiceRequest__c);
                }
                
                /*if(trigger.isUpdate){
                    if(objAmd.Record_Type_Name__c == 'Liquidator' && objAmd.Contact__c != null && ( (objAmd.Enable_Portal_Access__c != trigger.oldMap.get(objAmd.Id).Enable_Portal_Access__c) || objAmd.Liquidator__c != trigger.oldMap.get(objAmd.Id).Liquidator__c) ){
                        objAmd.addError('You cannot do this change, Liquidator is already got portal access.');
                        return;
                    }
                }*/
                //v1.2
                if(objAmd.Liquidation_SR__c == null && objAmd.Record_Type_Name__c == 'Liquidator'){
                    objAmd.Liquidation_SR__c = objAmd.ServiceRequest__c;
                }
                SRID = objAmd.ServiceRequest__c;
                SRstatus = objAmd.SR_Status__c;
                recType.add(objAmd.Record_Type_Name__c);
                landlordType.add(objAmd.Type_of_Landlord__c);
                if(recType.size()>0 && recType.contains('Business_Centre_Office'))
                    unitId.add(objAmd.Lease_Unit__c);
                if(recType.size()>0 && (recType.contains('Body_Corporate_Tenant') || recType.contains('DIFCI_Tenant') || recType.contains('Company_Buyer')|| recType.contains('RORP_Shareholder_Company') || (recType.contains('Landlord') && objAmd.Type_of_Landlord__c=='Company')) && objAmd.CL_Certificate_No__c!=null && objamd.Company_Name__c!=null){ //V1.6 Modified by kaavya to include RORP Freehold // V1.18 Added RORP Shareholder Company
                    BCCLNo.add(objAmd.CL_Certificate_No__c);                
                    //BCCompName.add(objAmd.Company_Name__c); //V1.8
                    /****V1.10 Added by Kaavya 18/11/2015 for including issuing authority check**/
                    if(objAmd.IssuingAuthority__c=='Others')
                        BCIssuAuth.add(objAmd.Issuing_Authority_Other__c); 
                    else
                        BCIssuAuth.add(objAmd.IssuingAuthority__c);
                    /***/   
                }
                if(recType.size()>0 && (recType.contains('Individual_Tenant') || recType.contains('Individual_Buyer') || (recType.contains('Landlord') && objAmd.Type_of_Landlord__c=='Individual')) && objAmd.Passport_No__c!=null && objamd.Nationality_list__c!=null){ //V1.6 Modified by kaavya to include RORP Freehold
                    IndPass.add(objAmd.Passport_No__c);
                    IndNat.add(objAmd.Nationality_list__c);
                }
                /*if(objAmd.Lease_Unit__c!=null && objAmd.Unit_Area__c!=null && objAmd.Rent_AED_per_sq_ft__c!=null){
                    objAmd.Unit_Rent__c = (objAmd.Unit_Area__c * objAmd.Rent_AED_per_sq_ft__c)/12;
                }*/
            }
            if(!lstLequidatorIds.isEmpty()){
                for(Amendment__c objAmd : [select Id,ServiceRequest__c,Liquidator__c from Amendment__c where Liquidator__c IN : lstLequidatorIds AND ServiceRequest__c IN : lstSRIds]){
                    mapLiquidators.put(objAmd.ServiceRequest__c+'-'+objAmd.Liquidator__c,objAmd.ServiceRequest__c+'-'+objAmd.Liquidator__c);
                }
            }
            for(Amendment__c objAmd : trigger.new ){
                if(objAmd.Record_Type_Name__c == 'Liquidator' && objAmd.ServiceRequest__c != null && objAmd.Liquidator__c != null && (trigger.isInsert || (trigger.isUpdate && trigger.oldMap.get(objAmd.Id).Liquidator__c != objAmd.Liquidator__c))){
                    if(mapLiquidators.containsKey(objAmd.ServiceRequest__c+'-'+objAmd.Liquidator__c)){
                        objAmd.addError('Selected Liquidator is already added to the request');
                        return;
                    }
                }
            }
            
            //V1.3 - RORP validations for IT Forms - Active Lease
            Boolean flag = false;
            if(SRID!=null && SRID!=''){
                list<Amendment__c> lstAmnd =new list<Amendment__c>();
                //map<string,string> mapAccounts = new map<string,string>();
                map<string,Account> mapAccounts = new map<string,Account>();
                map<string,string> mapContacts = new map<string,string>();
                map<string,string> mapExistingUnit = new map<string,string>();
                map<string,string> mapExistingTenant = new map<string,string>();
                map<string,Amendment__c> mapSubleaseTenants = new map<string,Amendment__c>();
                list<Service_Request__c> srlist = new list<Service_Request__c>();
                list<Service_Request__c> bcSRlist = new list<Service_Request__c>(); //v1.17
                map<string,Tenancy__c> mapExistingUnitWithTenancy = new map<string,Tenancy__c>(); //v1.17            
                if(recType.size()>0 && recType.contains('Data_Center_Requestor')){
                    srlist = [Select id,name,customer__c,Customer__r.Company_Code__c,(Select id,Name,Record_Type_Name__c from Amendments__r where Record_Type_Name__c='Data_Center_Requestor') from Service_Request__c where id=:SRID];
                    if(srlist.size()>0)
                        for(Lease__c actLease:[Select id,name,Account__c,Status__c from Lease__c where Account__c=:srlist[0].customer__c and Status__c='Active' and Lease_Types__c='Data Centre Lease' and Status__c='Active']){
                            flag = true;
                        }
                }else if(recType.size()>0 && recType.contains('Business_Centre_Office')){
                    bcSRlist = [Select id,Lease_Commencement_Date__c from Service_Request__c where id=:SRID]; //v1.17
                    for(Amendment__c amnd : [Select id,Name,Record_Type_Name__c,Lease_Unit__c,Unit__c,Building__c,Lease_Unit__r.Unit_Usage_Type__c from Amendment__c where Record_Type_Name__c='Business_Centre_Office' and ServiceRequest__c=:SRID order by createdDate asc]){
                        lstAmnd.add(amnd);
                        mapExistingUnit.put(amnd.Lease_Unit__c,amnd.id);
                    }
                    //v1.17 - added Lease__r.End_Date__c and Notice_Active__c = false condition in the SOQL
                    if(unitId!=null && unitId.size()>0){ 
                        for(Tenancy__c ten : [Select id,name,Unit__c,Lease__c,Lease__r.Account__c,Lease__r.Status__c,Lease__r.Lease_Types__c,Lease__r.End_Date__c,Lease__r.Notice_Active__c
                            from Tenancy__c where Unit__c IN :unitId AND Unit__c != null AND Lease__c != null 
                            AND Lease__r.Status__c = 'Active' AND Lease__r.Type__c='Leased']){
                            
                            //v1.17 
                            system.debug('bcSRlist ' + bcSRlist);
                            system.debug('ten ' + ten);
                            if(ten.Lease__r.Notice_Active__c){
                                if(bcSRlist.size() > 0 && bcSRlist[0].Lease_Commencement_Date__c <= ten.Lease__r.End_Date__c){
                                    mapUnitActiveTen.put(ten.Unit__c,ten.id);
                                }           
                            }else{
                                mapUnitActiveTen.put(ten.Unit__c,ten.id);
                            }
                            system.debug('mapUnitActiveTen ' + mapUnitActiveTen);
                             //mapUnitActiveTen.put(ten.Unit__c,ten.id); v1.17
                        }
                    }
                }else if(recType.size()>0 && (recType.contains('Body_Corporate_Tenant') || recType.contains('RORP_Shareholder_Company')|| recType.contains('DIFCI_Tenant') || recType.contains('Individual_Tenant') || recType.contains('Landlord')|| recType.contains('Company_Buyer')||recType.contains('Individual_Buyer'))){ //V1.6 Modified by kaavya to include RORP Freehold // V1.18 Added for RORP Shareholder Company Check
                    lstAmnd = new list<Amendment__c>();
                    //get SR fields for RORP
                    srlist = [Select id,name,customer__c,Unit__c,Type_of_Lease__c,Unit__r.Unit_Usage_Type__c,(select id,Name,Unit__c,Unit__r.Unit_Usage_Type__c,Amendment_Type__c from Amendments__r where Record_Type_Name__c!='Landlord') from Service_Request__c where id=:SRID];
                    unitId.clear();
                    for(Service_Request__c sr: srlist){
                        mapSRAmends.put(sr.id,sr.Amendments__r);
                        mapSRUnitType.put(sr.id,sr.Unit__r.Unit_Usage_Type__c);
                        mapSRLeaseType.put(sr.id,sr.Type_of_Lease__c);
                        unitId.add(sr.Unit__c);
                    }
                    if((recType.contains('Individual_Tenant') || recType.contains('Individual_Buyer') || (recType.contains('Landlord') && landlordType.contains('Individual'))) && IndPass.size()>0 && IndNat.size()>0){//&& SRstatus=='Draft'  //V1.6 Modified by kaavya to include RORP Freehold
                        for(Contact con : [select id,Name,Passport_No__c,Nationality__c from Contact where Passport_No__c IN :IndPass and Nationality__c IN :IndNat and RecordType.DeveloperName='RORP_Contact'])
                            mapContacts.put(con.Passport_No__c.toLowerCase()+con.Nationality__c.toLowerCase(),con.id);
                    }
                    system.debug('recType:::'+recType);
                    if((recType.contains('Body_Corporate_Tenant') || recType.contains('DIFCI_Tenant')|| recType.contains('Company_Buyer') || recType.contains('RORP_Shareholder_Company') || (recType.contains('Landlord') && landlordType.contains('Company'))) && BCCLNo.size()>0 && BCIssuAuth.size()>0){ // && BCCompName.size()>0 //v1.10 Added BCIssuAuth for issuing authority check 
                        for(Account acc : [Select id,Active_License__r.Name,Name,ROC_Status__c,RORP_License_No__c from Account where ((Active_License__r.Name IN :BCCLNo AND (ROC_Status__c != null AND Registration_License_No__c != null )) OR (RORP_License_No__c IN :BCCLNo AND Issuing_Authority__c IN : BCIssuAuth))]){ // AND Name IN:BCCompName
                            string licNo = (acc.Active_License__r.Name!='' && acc.Active_License__r.Name!=null) ? 'ROC-'+acc.Active_License__r.Name : 'RORP-'+acc.RORP_License_No__c;
                            mapAccounts.put(licNo,acc); //V1.8 - removed company name
                        }
                    }
                    //system.debug('mapContacts '+mapContacts);
                    //system.debug('mapAccounts '+mapAccounts);
                    for(Amendment__c amndSR : [Select id,Name,Type_of_Landlord__c,Record_Type_Name__c,Company_Name__c,Passport_No__c,Nationality_list__c from Amendment__c where ServiceRequest__c=:SRID]){
                        lstAmnd.add(amndSR);
                        if((amndSR.Record_Type_Name__c=='Individual_Tenant' ||amndSR.Record_Type_Name__c=='Individual_Buyer' ||  (amndSR.Record_Type_Name__c=='Landlord' && amndSR.Type_of_Landlord__c=='Individual')) && amndSR.Passport_No__c!=null && amndSR.Nationality_list__c!=null)//V1.6 Modified by kaavya to include RORP Freehold
                            mapExistingTenant.put(amndSR.Passport_No__c+amndSR.Nationality_list__c,amndSR.id);
                        else if(amndSR.Record_Type_Name__c=='Body_Corporate_Tenant' || amndSR.Record_Type_Name__c=='Company_Buyer' || amndSR.Record_Type_Name__c=='DIFCI_Tenant'||(amndSR.Record_Type_Name__c=='Landlord' && amndSR.Type_of_Landlord__c=='Company'))//V1.6 Modified by kaavya to include RORP Freehold
                            mapExistingTenant.put(amndSR.Company_Name__c,amndSR.id);
                    }
                    if(unitId!=null && unitId.size()>0){
                        for(Lease_Partner__c LP:[select id,Unit__c,Contact__c,Contact__r.Passport_No__c,Contact__r.Nationality__c,Account__c,Account__r.Name,CL_Certificate_No__c,Type__c from Lease_Partner__c where Unit__c IN :unitId AND Type__c='Landlord']){
                            if(LP.Account__c!= null && LP.Account__r.Name!='' && LP.CL_Certificate_No__c!=null && LP.CL_Certificate_No__c!=''){
                                mapLeasePartners.put(LP.Account__r.Name+LP.CL_Certificate_No__c.toLowerCase(),LP.Account__c);
                            }
                            if(LP.Contact__c!=null && LP.Contact__r.Passport_No__c!=null && LP.Contact__r.Nationality__c!=null){
                                mapLeasePartners.put(LP.Contact__r.Passport_No__c.toLowerCase()+LP.Contact__r.Nationality__c.toLowerCase(),LP.Contact__c);
                            }
                        }
                    }
                }
                
                for(Amendment__c objAmd : trigger.new ){
                    if(objAmd.Record_Type_Name__c == 'Data_Center_Requestor' && srlist.size()>0 && !srlist.isEmpty()){
                        if(objAmd.ServiceRequest__c != null && (trigger.isInsert || trigger.isUpdate) && (srlist[0].Customer__r.Company_Code__c ==null || srlist[0].Customer__r.Company_Code__c=='')){
                            if(!flag){
                                objAmd.addError('No active lease found for the selected data centre location under your company.');
                                return;
                            }
                            if(srlist[0].amendments__r.size()>=5){
                                objAmd.addError('Maximum five individuals can be added to the request.');
                                return;
                            }
                        }
                    }else if(objAmd.Record_Type_Name__c == 'Business_Centre_Office'){//V1.4 - Validation for Business Centre forms                  
                        if(mapExistingUnit!=null){
                            if(mapExistingUnit.containskey(objAmd.Lease_Unit__c) && trigger.isInsert){
                                objAmd.addError('Same unit cannot be added twice.');
                                return;
                            }
                            if(lstAmnd.size()>1 && lstAmnd!=null){
                                if((objAmd.Building__c!=null && objAmd.Building__c !=lstAmnd[0].Building__c) || objAmd.Unit_Usage_Type__c!=lstAmnd[0].Lease_Unit__r.Unit_Usage_Type__c){
                                    objAmd.addError('The unit must be selected of the same building and usage type.');
                                    return;
                                }
                            }
                        }
                        if(!mapUnitActiveTen.isEmpty() && mapUnitActiveTen!=null && mapUnitActiveTen.containsKey(objAmd.Lease_Unit__c)){
                            objAmd.addError('The selected unit is not available for registration.');
                            return;
                        }
                    }else if((objAmd.Record_Type_Name__c =='Body_Corporate_Tenant' || objAmd.Record_Type_Name__c =='DIFCI_Tenant' ||objAmd.Record_Type_Name__c =='Company_Buyer' || objAmd.Record_Type_Name__c =='RORP_Shareholder_Company' ||(objAmd.Record_Type_Name__c=='Landlord' && objAmd.Type_of_Landlord__c=='Company')) && objAmd.IssuingAuthority__c!=null){
                        if(objAmd.IssuingAuthority__c=='DIFC - Registered'){
                            Account obj = mapAccounts.get('ROC-'+objAmd.CL_Certificate_No__c);//V1.8 - removed company name
                            if(objAmd.SR_Record_Type__c == 'Lease_Registration' || objAmd.SR_Record_Type__c == 'Freehold_Transfer_Registration'){
                                if( obj == null || (obj != null && obj.ROC_Status__c != 'Active' && obj.ROC_Status__c != 'Not Renewed') ){
                                    objAmd.addError('No active DIFC registered company found with license number '+objAmd.CL_Certificate_No__c);
                                    return;
                                }
                            }else if(objAmd.SR_Record_Type__c == 'Surrender_Termination_of_Lease_and_Sublease'){
                                if( obj == null){
                                    objAmd.addError('No DIFC registered company found with license number '+objAmd.CL_Certificate_No__c);
                                    return;
                                }
                            }
                            
                        }
                        if(!mapAccounts.isEmpty() && mapAccounts!=null){
                            if(mapAccounts.containsKey('ROC-'+objAmd.CL_Certificate_No__c)){ //V1.8 - removed company name
                                if(objAmd.IssuingAuthority__c!='Others')                                
                                    objAmd.Shareholder_Company__c = mapAccounts.get('ROC-'+objAmd.CL_Certificate_No__c).Id;
                            }else if(mapAccounts.containsKey('RORP-'+objAmd.CL_Certificate_No__c)){
                                if(objAmd.IssuingAuthority__c =='Others')
                                    objAmd.Shareholder_Company__c = mapAccounts.get('RORP-'+objAmd.CL_Certificate_No__c).Id;
                            }
                        }
                    }
                    if((objAmd.Record_Type_Name__c=='Individual_Tenant' ||objAmd.Record_Type_Name__c=='Individual_Buyer'|| (objAmd.Record_Type_Name__c=='Landlord' && objAmd.Type_of_Landlord__c=='Individual')) && objAmd.Passport_No__c!=null && objAmd.Nationality_list__c!=null){//V1.6 Modified by kaavya to include RORP Freehold
                        if(!mapContacts.isEmpty() && mapContacts.containsKey(objAmd.Passport_No__c.toLowerCase()+objAmd.Nationality_list__c.toLowerCase())){
                            objAmd.Contact__c = mapContacts.get(objAmd.Passport_No__c.toLowerCase()+objAmd.Nationality_list__c.toLowerCase());
                        }
                    }
                    if((objAmd.Record_Type_Name__c =='Body_Corporate_Tenant' || objAmd.Record_Type_Name__c =='DIFCI_Tenant' ||objAmd.Record_Type_Name__c =='Company_Buyer' ||(objAmd.Record_Type_Name__c=='Landlord' && objAmd.Type_of_Landlord__c=='Company')) && SRstatus=='Draft'){
                        if(mapExistingTenant.containsKey(objAmd.Company_Name__c) && trigger.isInsert){
                            if(objAmd.Record_Type_Name__c !='Company_Buyer')
                                objAmd.addError('The tenant / landlord by the entered company name is already added.');
                            else 
                                objAmd.addError('The buyer with the entered company name is already added.');
                            return;
                        }else if(mapExistingTenant.get(objAmd.Company_Name__c)!=objAmd.id && mapExistingTenant.containsKey(objAmd.Company_Name__c) && trigger.isUpdate){
                            if(objAmd.Record_Type_Name__c !='Company_Buyer')
                                objAmd.addError('The tenant / landlord by the entered company name is already added.');
                            else 
                                objAmd.addError('The buyer with the entered company name is already added.');
                            return;
                        }
                    }
                    if((objAmd.Record_Type_Name__c =='Individual_Tenant'||objAmd.Record_Type_Name__c =='Individual_Buyer'|| (objAmd.Record_Type_Name__c=='Landlord' && objAmd.Type_of_Landlord__c=='Individual')) && SRstatus=='Draft'){
                        if(mapExistingTenant.containsKey(objAmd.Passport_No__c+objAmd.Nationality_list__c) && trigger.isInsert){
                            if(objAmd.Record_Type_Name__c !='Individual_Buyer')
                                objAmd.addError('The tenant with the entered passport number and nationality is already added.');
                            else
                                objAmd.addError('The buyer with the entered passport number and nationality is already added.');
                            return;
                        }else if(mapExistingTenant.get(objAmd.Passport_No__c+objAmd.Nationality_list__c)!=objAmd.id && mapExistingTenant.containsKey(objAmd.Passport_No__c+objAmd.Nationality_list__c) && trigger.isUpdate){
                            if(objAmd.Record_Type_Name__c !='Individual_Buyer')
                                objAmd.addError('The tenant / landlord with the entered passport number and nationality is already added.');
                            else
                                objAmd.addError('The buyer with the entered passport number and nationality is already added.');
                            return;
                        }
                    }
                    if(!mapLeasePartners.isEmpty() && mapLeasePartners!=null && objAmd.Amendment_Type__c!='Landlord' && objAmd.IssuingAuthority__c!='Under Formation' && SRstatus=='Draft'){
                        if(objAmd.Record_Type_Name__c =='Individual_Tenant' && objAmd.Passport_No__c!=null && objAmd.Nationality_list__c!=null){
                            if(mapLeasePartners.containsKey(objAmd.Passport_No__c.toLowerCase()+objAmd.Nationality_list__c.toLowerCase())){
                                objAmd.addError('The tenant cannot be same as the landlord.');
                                return;
                            }
                        }
                        if((objAmd.Record_Type_Name__c =='Body_Corporate_Tenant' || objAmd.Record_Type_Name__c =='DIFCI_Tenant') && objAmd.CL_Certificate_No__c!=null && objAmd.Company_Name__c!=null && SRstatus=='Draft'){
                            if(mapLeasePartners.containsKey(objAmd.Company_Name__c+objAmd.CL_Certificate_No__c.toLowerCase())){
                                objAmd.addError('The tenant cannot be same as the landlord.');
                                return;
                            }
                        }
                    }
                    system.debug('objAmd.ServiceRequest__c '+objAmd.ServiceRequest__c);
                    system.debug('mapSRUnitType '+mapSRUnitType);
                    system.debug('mapSRLeaseType '+mapSRLeaseType);
                    //V1.20 - changed the below if condition accordingly
                    //if(!mapSRUnitType.isEmpty() && trigger.isInsert && mapSRUnitType.containsKey(objAmd.ServiceRequest__c) && (mapSRUnitType.get(objAmd.ServiceRequest__c).contains('Commercial') || mapSRUnitType.get(objAmd.ServiceRequest__c).contains('Retail')) && !mapSRLeaseType.isEmpty() && mapSRLeaseType.containsKey(objAmd.ServiceRequest__c))
                    if(mapSRUnitType != null && trigger.isInsert && mapSRUnitType.get(objAmd.ServiceRequest__c) != null && (mapSRUnitType.get(objAmd.ServiceRequest__c).contains('Commercial') || mapSRUnitType.get(objAmd.ServiceRequest__c).contains('Retail')) && mapSRLeaseType != null && mapSRLeaseType.containsKey(objAmd.ServiceRequest__c)){
                        if(!mapSRAmends.isEmpty() && mapSRAmends.containsKey(objAmd.ServiceRequest__c) && mapSRAmends.get(objAmd.ServiceRequest__c).size()==1 && mapSRLeaseType.get(objAmd.ServiceRequest__c)=='Lease' && objAmd.Record_Type_Name__c !='Landlord'){
                            objAmd.addError('Only one tenant can be added for the Commercial or Retail units');
                            return;
                        }
                        if(mapSRLeaseType.get(objAmd.ServiceRequest__c) == 'Sub-lease'){
                            if(!mapSRAmends.isEmpty() && mapSRAmends.containsKey(objAmd.ServiceRequest__c)){
                                for(Amendment__c amnd : mapSRAmends.get(objAmd.ServiceRequest__c)){
                                    if(amnd.Amendment_Type__c=='Tenant' && !mapSubleaseTenants.containsKey('Tenant'))
                                        mapSubleaseTenants.put(amnd.Amendment_Type__c,amnd);
                                    if(amnd.Amendment_Type__c=='Sub-tenant' && !mapSubleaseTenants.containsKey('Sub-tenant'))
                                        mapSubleaseTenants.put(amnd.Amendment_Type__c,amnd);
                                }
                                if(mapSubleaseTenants.containsKey('Tenant') && mapSubleaseTenants.containsKey('Sub-tenant') && objAmd.Amendment_Type__c!='Landlord'){//V1.12 - Changes for Landlord Amendment type
                                    objAmd.addError('Only one tenant with a sub-tenant can be added for the Commercial or Retail units');
                                    return;
                                }else if(mapSubleaseTenants.containsKey('Tenant') && !mapSubleaseTenants.containsKey('Sub-tenant') && objAmd.Amendment_Type__c=='Tenant'){
                                    objAmd.addError('Only one tenant with a sub-tenant can be added for the Commercial or Retail units');
                                    return;
                                }else if(!mapSubleaseTenants.containsKey('Tenant') && mapSubleaseTenants.containsKey('Sub-tenant') && objAmd.Amendment_Type__c=='Sub-tenant'){
                                    objAmd.addError('Only one tenant with a sub-tenant can be added for the Commercial or Retail units');
                                    return;
                                }
                            }
                        }
                        if(objAmd.Record_Type_Name__c =='Individual_Tenant'){
                            objAmd.addError('Individual type of tenants cannot be added for the Commercial or Retail units');
                            return;
                        }
                    }
                    //V1.4 - RORP Changes
                    if(objAmd.Non_Reg_Company__c!=null && trigger.isUpdate && trigger.oldMap.get(objAmd.Id).Non_Reg_Company__c != objAmd.Non_Reg_Company__c)
                        objAmd.Shareholder_Company__c = objAmd.Non_Reg_Company__c;
                }
            }//V1.4
        }
        
        if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
            list<Id> lstSRIds = new list<Id>();
            map<Id,Id> mapSRIds = new map<Id,Id>();
            list<SR_Doc__c> lstDocuments = new list<SR_Doc__c>();
            map<String,Document_Master__c> mapDocMaster = new map<String,Document_Master__c>();
            string SRid ='';
            string AmndId = '';
            string SRrecType='';
            map<string,SR_Doc__c> mapSRdoc = new map<string,SR_Doc__c>();
            for(Amendment__c objAmd : trigger.new){
                SRid = objAmd.ServiceRequest__c;
                AmndId = objAmd.id;
                if(objAmd.Record_Type_Name__c == 'Liquidator' && objAmd.ServiceRequest__c != null && objAmd.SR_Status__c != 'Draft' && objAmd.Contact__c == null && objAmd.Liquidator__c != null){
                    //Create the Liquidator Protal user, Assuming that Liquidator creation will be always from standard Screen. If not, the below code needs to change to support bulk
                    string result = CC_DissolutionCls.ProcessLiquidators(objAmd.ServiceRequest__c);
                    if(result != 'Success'){
                        objAmd.addError(CustomCode_UtilCls.PrepareExceptionMsg(result));
                    }
                }
                if(objAmd.Record_Type_Name__c == 'Liquidator' && objAmd.ServiceRequest__c != null && objAmd.Liquidator__c != null){
                    lstSRIds.add(objAmd.ServiceRequest__c);
                }
                
                if(objAmd.Record_Type_Name__c == 'Individual_Tenant' || objAmd.Record_Type_Name__c == 'Body_Corporate_Tenant' || objAmd.Record_Type_Name__c == 'DIFCI_Tenant' || objAmd.Record_Type_Name__c == 'Individual_Buyer'||objAmd.Record_Type_Name__c == 'Individual_Seller' || objAmd.Record_Type_Name__c == 'Company_Buyer' || objAmd.Record_Type_Name__c == 'Company_Seller'){
                    mapSRIds.put(objAmd.ServiceRequest__c,objAmd.ServiceRequest__c);
                }
                
            }
            if(SRid!=null && SRid!=''){
                for(Service_Request__c SRList : [select id,Name,Record_Type_Name__c from Service_Request__c where id=:SRid]){
                    SRrecType = SRList.Record_Type_Name__c;
                }
                for(SR_Doc__c srDocs : [Select name,Service_Request__c,SR_RecordType_Name__c,Unique_SR_Doc__c,Amendment__c,Document_Master__r.Code__c from SR_Doc__c where Amendment__c=:AmndId AND Service_Request__c=:SRid]){
                    mapSRdoc.put(srDocs.Unique_SR_Doc__c,srDocs);
                }
            }
            if(SRrecType!='' && (SRrecType=='Lease_Registration'|| SRrecType=='Freehold_Transfer_Registration' || SRrecType=='Add_Remove_GSO_Authorized_Signatory' || SRrecType=='Mortgage_Registration' || SRrecType == 'New_Index_Card' || SRrecType == 'Refund_of_The_Security_Deposit' || SRrecType == 'Surrender_Termination_of_Lease_and_Sublease')){ //V1.6 Modified for including RORP Freehold , //V1.11 Modified for GSO authroized signatory //v1.21 - Mortgage_Registration
                for(Document_Master__c docmstr:[select id,Name,Code__c,Description__c from Document_Master__c where Code__c!=null]){
                            mapDocMaster.put(docmstr.Code__c,docmstr);
                }
                system.debug('----SRrecType--'+SRrecType);
                for(Amendment__c objAmd : trigger.new){
                    if(objAmd.Record_Type_Name__c == 'Individual_Tenant' || objAmd.Record_Type_Name__c == 'Body_Corporate_Tenant' || objAmd.Record_Type_Name__c == 'DIFCI_Tenant' || objAmd.Record_Type_Name__c == 'Landlord'){
                        /* Commented and moved up
                        for(Document_Master__c docmstr:[select id,Name,Code__c,Description__c from Document_Master__c where Code__c!=null]){
                            mapDocMaster.put(docmstr.Code__c,docmstr);
                        }*/
                    }
                    
                    if((((objAmd.Record_Type_Name__c == 'Individual_Tenant' || objAmd.Record_Type_Name__c == 'Individual_Buyer' || objAmd.Record_Type_Name__c == 'Individual_Seller'  || (objAmd.Record_Type_Name__c == 'GSO_Authorized_Signatory' && objAmd.Is_Applicant_Salary_Above_AED_20_000__c ==false) || 
                    objAmd.Record_Type_Name__c == 'RORP_Shareholder' || objAmd.Record_Type_Name__c == 'RORP_Authorized_Signatory'|| (objAmd.Record_Type_Name__c=='Landlord' && objAmd.Type_of_Landlord__c =='Individual') || objAmd.Record_Type_Name__c == 'Individual_Mortgager' )
                     &&  SRrecType !='Surrender_Termination_of_Lease_and_Sublease' )|| objAmd.Document_Flag__c == true) // //v1.21 - Individual_Mortgager // V1.25 added Is_Applicant_Salary_Above_AED_20_000__c
                        && trigger.isAfter && trigger.isInsert){ //V1.6 Modified for including RORP Freehold , //V1.11 Modified for GSO authroized signatory  //V1.30 Modified for Surrender_Termination_of_Lease_and_Sublease            
                       
                        SR_Doc__c objSRDoc = new SR_Doc__c();
                        objSRDoc.Service_Request__c = objAmd.ServiceRequest__c;
                        objSRDoc.Amendment__c = objAmd.Id;
                        //objSRDoc.Name = objAmd.Given_Name__c.left(60)+'-Passport Copy';//v1.29
                        objSRDoc.Name = objAmd.Given_Name__c.left(60)+'-Passport copy & signature page';//v1.29
                        if(mapDocMaster.get('Passport Copy')!=null){
                            objSRDoc.Document_Master__c = mapDocMaster.get('Passport Copy').Id;
                            objSRDoc.Document_Description_External__c = mapDocMaster.get('Passport Copy').Description__c;
                        }
                        //if(objSRDoc.Document_Description_External__c==null)
                            objSRDoc.Document_Description_External__c = 'Please upload the personal details and signature page of the passport ('+objAmd.Amendment_Type__c+')';  //V1.6 Modified for including RORP Freehold (ticket# 1860 )                 
                        objSRDoc.Status__c = 'Pending Upload';
                        objSRDoc.Is_Not_Required__c = false;
                        //objSRDoc.From_Finalize__c = true;
                        objSRDoc.Requirement__c = 'Copy Required';
                        objSRDoc.Unique_SR_Doc__c = 'PPNo_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                        
                        if(objAmd.Record_Type_Name__c == 'Individual_Tenant' || (objAmd.Emirates_ID_Number__c!=null && (objAmd.Record_Type_Name__c == 'Individual_Buyer' || objAmd.Record_Type_Name__c == 'Individual_Seller'))){ //V1.6 Modified for including RORP Freehold
                            //Emirates ID dc to be uploaded
                            SR_Doc__c objSREMDoc = new SR_Doc__c();
                            objSREMDoc.Service_Request__c = objAmd.ServiceRequest__c;
                            objSREMDoc.Amendment__c = objAmd.Id;
                            objSREMDoc.Name = objAmd.Given_Name__c.left(60)+'-Emirates ID';
                            if(mapDocMaster.get('Emirates ID')!=null){
                                objSREMDoc.Document_Master__c = mapDocMaster.get('Emirates ID').Id;
                                objSREMDoc.Document_Description_External__c = mapDocMaster.get('Emirates ID').Description__c;
                            }
                            if(objSREMDoc.Document_Description_External__c==null)
                                objSREMDoc.Document_Description_External__c = 'Please upload a copy of both sides of Emirates ID.';
                            objSREMDoc.Status__c = 'Pending Upload';
                            objSREMDoc.Is_Not_Required__c = false;
                            //objSRDoc.From_Finalize__c = true;
                            objSREMDoc.Requirement__c = 'Copy Required';
                            objSREMDoc.Unique_SR_Doc__c = 'EID_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                            lstDocuments.add(objSREMDoc);
                            
                            if(objAmd.First_Name__c!=null && objAmd.Passport_No_Occupant__c!=null && objAmd.Passport_No_Occupant__c!='' && objAmd.Nationality_Occupant__c!=null && objAmd.Nationality_Occupant__c!=''){
                                //Occupant Passport Copy
                                SR_Doc__c objSROccDoc = new SR_Doc__c();
                                objSROccDoc.Service_Request__c = objAmd.ServiceRequest__c;
                                objSROccDoc.Amendment__c = objAmd.Id;
                                //objSROccDoc.Name = objAmd.First_Name__c.left(60)+'-Passport Copy';//V1.29
                                objSROccDoc.Name = objAmd.First_Name__c.left(60)+'-Passport copy & signature page';//v1.29
                                if(mapDocMaster.get('Passport Copy')!=null){
                                    objSROccDoc.Document_Master__c = mapDocMaster.get('Passport Copy').Id;
                                    objSROccDoc.Document_Description_External__c = mapDocMaster.get('Passport Copy').Description__c;
                                }
                                if(objSROccDoc.Document_Description_External__c==null)
                                    objSROccDoc.Document_Description_External__c = 'Please upload the first and last page of the passport copy ('+objAmd.Amendment_Type__c+')';
                                objSROccDoc.Status__c = 'Pending Upload';
                                objSROccDoc.Is_Not_Required__c = false;
                                //objSRDoc.From_Finalize__c = true;
                                objSROccDoc.Requirement__c = 'Copy Required';
                                objSROccDoc.Unique_SR_Doc__c = 'OccPPNo_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                                lstDocuments.add(objSROccDoc);
                                
                                //Occupant Emirates ID dc to be uploaded
                                SR_Doc__c objSROccEMDoc = new SR_Doc__c();
                                objSROccEMDoc.Service_Request__c = objAmd.ServiceRequest__c;
                                objSROccEMDoc.Amendment__c = objAmd.Id;
                                objSROccEMDoc.Name = objAmd.First_Name__c.left(60)+'-Emirates ID';
                                if(mapDocMaster.get('Passport Copy')!=null){
                                    objSROccEMDoc.Document_Master__c = mapDocMaster.get('Emirates ID').Id;
                                    objSROccEMDoc.Document_Description_External__c = mapDocMaster.get('Emirates ID').Description__c;
                                }
                                if(objSROccEMDoc.Document_Description_External__c==null)
                                    objSROccEMDoc.Document_Description_External__c = 'Please upload a copy of both sides of Emirates ID.';
                                objSROccEMDoc.Status__c = 'Pending Upload';
                                objSROccEMDoc.Is_Not_Required__c = false;
                                //objSRDoc.From_Finalize__c = true;
                                objSROccEMDoc.Requirement__c = 'Copy Required';
                                objSROccEMDoc.Unique_SR_Doc__c = 'OccEID_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                                lstDocuments.add(objSROccEMDoc);
                            }
                            
                        }
                        
                        lstDocuments.add(objSRDoc);
                    }
                    if((objAmd.Record_Type_Name__c == 'Body_Corporate_Tenant' || objAmd.Record_Type_Name__c == 'DIFCI_Tenant' ||objAmd.Record_Type_Name__c == 'Company_Buyer' ||objAmd.Record_Type_Name__c == 'Company_Seller' || objAmd.Record_Type_Name__c == 'RORP_Shareholder_Company' || 
                        objAmd.Record_Type_Name__c == 'Company_Mortgager' || (objAmd.Record_Type_Name__c=='Landlord' && objAmd.Type_of_Landlord__c =='Company') ) && trigger.isAfter && trigger.isInsert){ //V1.6 Modified for including RORP Freehold //v1.21 - Company_Mortgager
                       // SR_Doc__c objSRDoc = new SR_Doc__c();
                       /* if(objAmd.IssuingAuthority__c=='DIFC - Under Formation'){
                            objSRDoc = new SR_Doc__c();
                            objSRDoc.Service_Request__c = objAmd.ServiceRequest__c;
                            objSRDoc.Amendment__c = objAmd.Id;
                            objSRDoc.Name = objAmd.Company_Name__c.left(40)+'-DFSA Approval / Provisional Approval';
                            if(mapDocMaster.get('DFSA Provisional Approval')!=null){
                                objSRDoc.Document_Master__c = mapDocMaster.get('DFSA Provisional Approval').Id;
                                objSRDoc.Document_Description_External__c = mapDocMaster.get('DFSA Provisional Approval').Description__c;
                            }
                            if(objSRDoc.Document_Description_External__c==null)
                                objSRDoc.Document_Description_External__c = 'Please upload the copy of DFSA Approval / Provisional Approval ('+objAmd.Amendment_Type__c+')';
                            objSRDoc.Status__c = 'Pending Upload';
                            objSRDoc.Is_Not_Required__c = false;
                            //objSRDoc.From_Finalize__c = true;
                            objSRDoc.Requirement__c = 'Copy Required';
                            objSRDoc.Unique_SR_Doc__c = 'DFSA_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                        }*/
                       // else{
                       if(objAmd.IssuingAuthority__c !='DIFC - Under Formation'){
                            SR_Doc__c objSRDoc = new SR_Doc__c();
                            objSRDoc.Service_Request__c = objAmd.ServiceRequest__c;
                            objSRDoc.Amendment__c = objAmd.Id;
                            objSRDoc.Name = objAmd.Company_Name__c.left(45)+'-Certificate/Commercial Licence';
                            if(mapDocMaster.get('Certificate or Commercial Licence')!=null){
                                objSRDoc.Document_Master__c = mapDocMaster.get('Certificate or Commercial Licence').Id;
                                objSRDoc.Document_Description_External__c = mapDocMaster.get('Certificate or Commercial Licence').Description__c;
                            }
                            if(objSRDoc.Document_Description_External__c==null)
                                objSRDoc.Document_Description_External__c = 'Please upload the copy of Certificate or Commercial License ('+objAmd.Amendment_Type__c+')';
                            objSRDoc.Status__c = 'Pending Upload';
                            objSRDoc.Is_Not_Required__c = false;
                            //objSRDoc.From_Finalize__c = true;
                            objSRDoc.Requirement__c = 'Copy Required';
                            objSRDoc.Unique_SR_Doc__c = 'CL_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                            lstDocuments.add(objSRDoc);
                        }
                        if(objAmd.Name_of_Declaration_Signatory__c!=null){
                            //Authorized Signatory Passport Copy
                            SR_Doc__c objAuthDoc = new SR_Doc__c();
                            objAuthDoc.Service_Request__c = objAmd.ServiceRequest__c;
                            objAuthDoc.Amendment__c = objAmd.Id;
                            //objAuthDoc.Name = objAmd.Name_of_Declaration_Signatory__c.left(60)+'-Passport Copy';//v1.29
                            objAuthDoc.Name = objAmd.Name_of_Declaration_Signatory__c.left(60)+'-Passport copy & signature page';//v1.29
                            if(mapDocMaster.get('Passport Copy')!=null){
                                objAuthDoc.Document_Master__c = mapDocMaster.get('Passport Copy').Id;
                                objAuthDoc.Document_Description_External__c = mapDocMaster.get('Passport Copy').Description__c;
                            }
                            if(objAuthDoc.Document_Description_External__c==null)
                                objAuthDoc.Document_Description_External__c = 'Please upload the first and last page of the passport copy ('+objAmd.Amendment_Type__c+')';
                            objAuthDoc.Status__c = 'Pending Upload';
                            objAuthDoc.Is_Not_Required__c = false;
                            if(SRrecType == 'Surrender_Termination_of_Lease_and_Sublease'){
                                objAuthDoc.Name = objAmd.Name_of_Declaration_Signatory__c.left(60)+'-Authorized signatory passport copy or EID';
                                objAuthDoc.Document_Description_External__c ='Kindly provide passport copy with signature page or EID (both side) for the authorized signatory who signed on behalf of the tenant company.';
                            }
                            //objSRDoc.From_Finalize__c = true;
                            objAuthDoc.Requirement__c = 'Copy Required';
                            objAuthDoc.Unique_SR_Doc__c = 'AuthPPNo_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                            
                            lstDocuments.add(objAuthDoc);
                        }

                        //Start 16830
                        if(objAmd.Record_Type_Name__c == 'Body_Corporate_Tenant' && SRrecType == 'Surrender_Termination_of_Lease_and_Sublease'){
                            //Authorized Signatory Power of Attorney
                            SR_Doc__c objPowerDoc = new SR_Doc__c();
                            objPowerDoc.Service_Request__c = objAmd.ServiceRequest__c;
                            objPowerDoc.Amendment__c = objAmd.Id;
                            objPowerDoc.Name = 'Power of Attorney / board resolution';
                            if(mapDocMaster.get('Power of Attorney')!=null){
                                objPowerDoc.Document_Master__c = mapDocMaster.get('Power of Attorney').Id;
                            }
                            objPowerDoc.Document_Description_External__c = 'Please upload a copy of Power of Attorney of authorized signatory who signed on behalf of the tenant company.';
                            objPowerDoc.Status__c = 'Pending Upload';
                            objPowerDoc.Is_Not_Required__c = true;
                            objPowerDoc.Requirement__c = 'Copy Required';
                            objPowerDoc.Unique_SR_Doc__c = 'Power_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                            
                            lstDocuments.add(objPowerDoc);
                        }
                        //End 16830
                        
                        if(objAmd.Record_Type_Name__c == 'Company_Buyer'){
                            SR_Doc__c objULDoc = new SR_Doc__c();
                            objULDoc.Service_Request__c = objAmd.ServiceRequest__c;
                            objULDoc.Amendment__c = objAmd.Id;
                            objULDoc.Name = objAmd.Company_Name__c.left(45)+'-Signed Undertaking Letter';
                            if(mapDocMaster.get('Signed Undertaking Letter')!=null){
                                objULDoc.Document_Master__c = mapDocMaster.get('Signed Undertaking Letter').Id;
                                objULDoc.Document_Description_External__c = mapDocMaster.get('Signed Undertaking Letter').Description__c;
                            }
                            if(objULDoc.Document_Description_External__c==null)
                                objULDoc.Document_Description_External__c = 'Please upload the copy of Signed Undertaking Letter ('+objAmd.Amendment_Type__c+')';
                            objULDoc.Status__c = 'Pending Upload';
                            objULDoc.Is_Not_Required__c = false;
                            //objSRDoc.From_Finalize__c = true;
                            objULDoc.Requirement__c = 'Copy Required';
                            objULDoc.Unique_SR_Doc__c = 'UL_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;   
                            lstDocuments.add(objULDoc);
                        }
                        if(objAmd.Record_Type_Name__c == 'Company_Seller'){
                            SR_Doc__c objAuthPPDoc = new SR_Doc__c();
                            objAuthPPDoc.Service_Request__c = objAmd.ServiceRequest__c;
                            objAuthPPDoc.Amendment__c = objAmd.Id;
                            objAuthPPDoc.Name = objAmd.Company_Name__c.left(45)+'- Authorized Signatory Passport';
                            if(mapDocMaster.get('Authorized Signatory Passport')!=null){
                                objAuthPPDoc.Document_Master__c = mapDocMaster.get('Authorized Signatory Passport').Id;
                                objAuthPPDoc.Document_Description_External__c = mapDocMaster.get('Authorized Signatory Passport').Description__c;
                            }
                            //if(objAuthPPDoc.Document_Description_External__c==null)
                                objAuthPPDoc.Document_Description_External__c = 'Please upload the  passport copy of the authorized person to execute the transaction from the seller side';
                            objAuthPPDoc.Status__c = 'Pending Upload';
                            objAuthPPDoc.Is_Not_Required__c = false;
                            //objSRDoc.From_Finalize__c = true;
                            objAuthPPDoc.Requirement__c = 'Copy Required';
                            objAuthPPDoc.Unique_SR_Doc__c = 'UL_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;   
                            lstDocuments.add(objAuthPPDoc);
                        }
                        
                    }
                }
                if(trigger.isAfter && trigger.isUpdate){
                    if(!mapSRdoc.isEmpty() && mapSRdoc!=null){
                        for(Amendment__c objAmd : trigger.new){
                            if(objamd.SR_Status__c =='Draft'){
                                if((objAmd.Record_Type_Name__c == 'Individual_Tenant'||objAmd.Record_Type_Name__c == 'Individual_Buyer'||objAmd.Record_Type_Name__c == 'Individual_Seller' ||objAmd.Record_Type_Name__c == 'RORP_Shareholder'||objAmd.Record_Type_Name__c == 'RORP_Authorized_Signatory'|| (objAmd.Record_Type_Name__c=='Landlord' && objAmd.Type_of_Landlord__c =='Individual')) && trigger.oldMap.get(objAmd.id).Given_Name__c!=objAmd.Given_Name__c){ //V1.6 Modified for including RORP Freehold
                                    if(mapSRdoc.containsKey('PPNo_'+objAmd.ServiceRequest__c+'_'+objAmd.Id)){
                                        SR_Doc__c objSRDoc = new SR_Doc__c(id=mapSRdoc.get('PPNo_'+objAmd.ServiceRequest__c+'_'+objAmd.Id).id);
                                        //objSRDoc.Name = objAmd.Given_Name__c.left(60)+'-Passport Copy';v1.29
                                        objSRDoc.Name = objAmd.Given_Name__c.left(60)+'-Passport copy & signature page';//v1.29
                                        lstDocuments.add(objSRDoc);
                                    }
                                    if((objAmd.Record_Type_Name__c == 'Individual_Tenant'|| (objAmd.Emirates_ID_Number__c!=null && (objAmd.Record_Type_Name__c == 'Individual_Buyer' || objAmd.Record_Type_Name__c == 'Individual_Seller'))) && mapSRdoc.containsKey('EID_'+objAmd.ServiceRequest__c+'_'+objAmd.id)){ //V1.6 Modified for including RORP Freehold
                                        SR_Doc__c objEMDoc = new SR_Doc__c(id=mapSRdoc.get('EID_'+objAmd.ServiceRequest__c+'_'+objAmd.id).id);
                                        objEMDoc.Name = objAmd.Given_Name__c.left(60)+'-Emirates ID';
                                        lstDocuments.add(objEMDoc);
                                    }
                                } 
                                if(objAmd.Record_Type_Name__c == 'Individual_Tenant' && trigger.oldMap.get(objAmd.id).First_Name__c!=objAmd.First_Name__c){
                                    if(mapSRdoc.containsKey('OccPPNo_'+objAmd.ServiceRequest__c+'_'+objAmd.Id)){
                                        SR_Doc__c objOccSRDoc = new SR_Doc__c(id=mapSRdoc.get('OccPPNo_'+objAmd.ServiceRequest__c+'_'+objAmd.Id).id);
                                        //objOccSRDoc.Name = objAmd.First_Name__c.left(60)+'-Passport Copy';//v1.29
                                        objOccSRDoc.Name = objAmd.First_Name__c.left(60)+'-Passport copy & signature page';//v1.29
                                        lstDocuments.add(objOccSRDoc);
                                    }
                                    if(objAmd.Record_Type_Name__c == 'Individual_Tenant' && mapSRdoc.containsKey('OccEID_'+objAmd.ServiceRequest__c+'_'+objAmd.id)){
                                        SR_Doc__c objOccEMDoc = new SR_Doc__c(id=mapSRdoc.get('OccEID_'+objAmd.ServiceRequest__c+'_'+objAmd.id).id);
                                        objOccEMDoc.Name = objAmd.First_Name__c.left(60)+'-Emirates ID';
                                        lstDocuments.add(objOccEMDoc);
                                    }
                                } 
                                if((objAmd.Record_Type_Name__c == 'Body_Corporate_Tenant' || objAmd.Record_Type_Name__c == 'DIFCI_Tenant' || objAmd.Record_Type_Name__c == 'Company_Buyer' || objAmd.Record_Type_Name__c == 'Company_Seller'|| objAmd.Record_Type_Name__c == 'RORP_Shareholder_Company'|| (objAmd.Record_Type_Name__c=='Landlord' && objAmd.Type_of_Landlord__c =='Company'))){ //V1.6 Modified for including RORP Freehold
                                    if(trigger.oldMap.get(objAmd.id).IssuingAuthority__c!=objAmd.IssuingAuthority__c || trigger.oldMap.get(objAmd.id).Company_Name__c!=objAmd.Company_Name__c){
                                        if(trigger.oldMap.get(objAmd.id).IssuingAuthority__c!='DIFC - Under Formation' && objAmd.IssuingAuthority__c=='DIFC - Under Formation' && mapSRdoc.containsKey('CL_'+objAmd.ServiceRequest__c+'_'+objAmd.id)){
                                            SR_Doc__c objDFSADoc = new SR_Doc__c(id=mapSRdoc.get('CL_'+objAmd.ServiceRequest__c+'_'+objAmd.id).id);
                                            objDFSADoc.Name = objAmd.Company_Name__c.left(40)+'-DFSA Approval / Provisional Approval';
                                            objDFSADoc.Unique_SR_Doc__c = 'DFSA_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                                            if(mapDocMaster.get('DFSA Provisional Approval')!=null){
                                                objDFSADoc.Document_Master__c = mapDocMaster.get('DFSA Provisional Approval').Id;
                                                objDFSADoc.Document_Description_External__c = mapDocMaster.get('DFSA Provisional Approval').Description__c;
                                            }
                                             if(objDFSADoc.Document_Description_External__c==null)
                                                objDFSADoc.Document_Description_External__c = 'Please upload the copy of DFSA Approval / Provisional Approval ('+objAmd.Amendment_Type__c+')';
                                            lstDocuments.add(objDFSADoc);
                                        }else if(objAmd.IssuingAuthority__c!='DIFC - Under Formation' && trigger.oldMap.get(objAmd.id).IssuingAuthority__c=='DIFC - Under Formation' && mapSRdoc.containsKey('DFSA_'+objAmd.ServiceRequest__c+'_'+objAmd.id)){
                                            SR_Doc__c objCLDoc = new SR_Doc__c(id=mapSRdoc.get('DFSA_'+objAmd.ServiceRequest__c+'_'+objAmd.id).id);
                                            objCLDoc.Name = objAmd.Company_Name__c.left(45)+'-Certificate/Commercial Licence';
                                            if(mapDocMaster.get('Certificate or Commercial Licence')!=null){
                                                objCLDoc.Document_Master__c = mapDocMaster.get('Certificate or Commercial Licence').Id;
                                                objCLDoc.Document_Description_External__c = mapDocMaster.get('Certificate or Commercial Licence').Description__c;
                                            }
                                            if(objCLDoc.Document_Description_External__c==null)
                                                objCLDoc.Document_Description_External__c = 'Please upload the copy of Certificate or Commercial License ('+objAmd.Amendment_Type__c+')';
                                            objCLDoc.Status__c = 'Pending Upload';
                                            objCLDoc.Is_Not_Required__c = false;
                                            //objSRDoc.From_Finalize__c = true;
                                            objCLDoc.Requirement__c = 'Copy Required';
                                            objCLDoc.Unique_SR_Doc__c = 'CL_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                                            lstDocuments.add(objCLDoc);
                                        }
                                        if(objAmd.Record_Type_Name__c == 'Company_Buyer'){
                                            SR_Doc__c objULDoc = new SR_Doc__c();
                                            objULDoc.Service_Request__c = objAmd.ServiceRequest__c;
                                            objULDoc.Amendment__c = objAmd.Id;
                                            objULDoc.Name = objAmd.Company_Name__c.left(45)+'-Signed Undertaking Letter';
                                            if(mapDocMaster.get('Signed Undertaking Letter')!=null){
                                                objULDoc.Document_Master__c = mapDocMaster.get('Signed Undertaking Letter').Id;
                                                objULDoc.Document_Description_External__c = mapDocMaster.get('Signed Undertaking Letter').Description__c;
                                            }
                                            if(objULDoc.Document_Description_External__c==null)
                                                objULDoc.Document_Description_External__c = 'Please upload the copy of Signed Undertaking Letter ('+objAmd.Amendment_Type__c+')';
                                            objULDoc.Status__c = 'Pending Upload';
                                            objULDoc.Is_Not_Required__c = false;
                                            //objSRDoc.From_Finalize__c = true;
                                            objULDoc.Requirement__c = 'Copy Required';
                                            objULDoc.Unique_SR_Doc__c = 'UL_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;
                                            lstDocuments.add(objULDoc); 
                                        }
                                        if(objAmd.Record_Type_Name__c == 'Company_Seller'){
                                        SR_Doc__c objAuthPPDoc = new SR_Doc__c();
                                        objAuthPPDoc.Service_Request__c = objAmd.ServiceRequest__c;
                                        objAuthPPDoc.Amendment__c = objAmd.Id;
                                        objAuthPPDoc.Name = objAmd.Company_Name__c.left(45)+'- Authorized Signatory Passport';
                                        if(mapDocMaster.get('Authorized Signatory Passport')!=null){
                                            objAuthPPDoc.Document_Master__c = mapDocMaster.get('Authorized Signatory Passport').Id;
                                            objAuthPPDoc.Document_Description_External__c = mapDocMaster.get('Authorized Signatory Passport').Description__c;
                                        }
                                        if(objAuthPPDoc.Document_Description_External__c==null)
                                            objAuthPPDoc.Document_Description_External__c = 'Please upload the passport copy of Authorized Signatory ('+objAmd.Amendment_Type__c+')';
                                        objAuthPPDoc.Status__c = 'Pending Upload';
                                        objAuthPPDoc.Is_Not_Required__c = false;
                                        //objSRDoc.From_Finalize__c = true;
                                        objAuthPPDoc.Requirement__c = 'Copy Required';
                                        objAuthPPDoc.Unique_SR_Doc__c = 'UL_'+objAmd.ServiceRequest__c+'_'+objAmd.Id;   
                                        lstDocuments.add(objAuthPPDoc);
                                    }
                                    }
                                    if(objAmd.Name_of_Declaration_Signatory__c!=null && trigger.oldMap.get(objAmd.id).Name_of_Declaration_Signatory__c!=objAmd.Name_of_Declaration_Signatory__c && mapSRdoc.containsKey('AuthPPNo_'+objAmd.ServiceRequest__c+'_'+objAmd.Id)){
                                        SR_Doc__c objAuthDoc = new SR_Doc__c(id=mapSRdoc.get('AuthPPNo_'+objAmd.ServiceRequest__c+'_'+objAmd.Id).id);
                                        //objAuthDoc.Name = objAmd.Name_of_Declaration_Signatory__c.left(60)+'-Passport Copy';v1.29
                                        objAuthDoc.Name = objAmd.Name_of_Declaration_Signatory__c.left(60)+'-Passport copy & signature page';//v1.29
                                        lstDocuments.add(objAuthDoc);
                                    }
                                }
                            }
                        }
                    }
                }
                system.debug('!!@#$$#@@'+lstDocuments);
                if(lstDocuments.size()>0 && !lstDocuments.isEmpty())
                    upsert lstDocuments;
            }
            if(!lstSRIds.isEmpty()){
                list<Service_Request__c> lstSRs = new list<Service_Request__c>();
                list<Id> lstIds = new list<Id>();
                for(Service_Request__c objSR : [select Id,Change_Address__c from Service_Request__c where Id IN : lstSRIds AND Change_Address__c=false]){
                    objSR.Change_Address__c = true;
                    lstSRs.add(objSR);
                    lstIds.add(objSR.Id);
                }
                if(!lstSRs.isEmpty()){
                    update lstSRs;
                    //string result = CC_DissolutionCls.CheckDissolutionCustomerClosureStep(lstIds);
                }   
            }
            if(!mapSRIds.isEmpty()){ //V1.4
                list<Service_Request__c> lstSRs = new list<Service_Request__c>();
                for(Service_Request__c objSR : [select Id,Tenant_Type_RORP__c,(select Id,Record_Type_Name__c from Amendments__r where Record_Type_Name__c = 'Individual_Tenant' OR Record_Type_Name__c = 'Body_Corporate_Tenant' OR Record_Type_Name__c = 'Individual_Buyer' OR Record_Type_Name__c = 'Company_Buyer') from Service_Request__c where Id IN : mapSRIds.keySet() AND (Record_Type_Name__c = 'Lease_Registration' OR Record_Type_Name__c = 'Freehold_Transfer_Registration') AND External_Status_Code__c='DRAFT']){
                    string tType = '';
                    for(Amendment__c objAmd : objSR.Amendments__r){
                        if(objAmd.Record_Type_Name__c == 'Individual_Tenant'){
                            tType += tType.contains('Individual') ? '' : ' Individual ';
                        }else if(objAmd.Record_Type_Name__c == 'Body_Corporate_Tenant' || objAmd.Record_Type_Name__c == 'DIFCI_Tenant'){
                            tType += tType.contains('Body_Corporate') ? '' : ' Body_Corporate ';
                            //V1.6 Added for RORP Freehold
                        }else if(objAmd.Record_Type_Name__c == 'Individual_Buyer'){
                            tType += tType.contains('Individual') ? '' : ' Individual ';
                        }else if(objAmd.Record_Type_Name__c == 'Company_Buyer'){
                            tType += tType.contains('Company') ? '' : ' Company ';
                        }
                    }
                    if(tType != null){
                        objSR.Tenant_Type_RORP__c = tType;
                        lstSRs.add(objSR);
                    }               
                }
                if(!lstSRs.isEmpty())
                    update lstSRs;
            }
        }
            
       //v1.5 - start , v1.7 , v1.11 , V1.14
        if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate || trigger.isDelete)){
            map<Id,Service_Request__c> mapSR = new map<Id,Service_Request__c>();
            map<Id,string> mapSRIdWithFreeHoldTypeReq = new map<Id,string>();
            
            // v1.7 Added RORP - Freehold recrod types 'Individual_Seller' ,'Company_Seller', 'Developer' ,'Individual_Buyer' , 'Company_Buyer'
            // v1.11 Added GSO Authorized Signatory - GSO_Authorized_Signatory
            set<string> setAmendRecordTypeName = new set<string>{'Individual_Tenant' , 'Body_Corporate_Tenant' , 'Individual_Seller' ,'Company_Seller', 'Developer' ,'Individual_Buyer' , 'Company_Buyer' , 'GSO_Authorized_Signatory'};
            
            // v1.7 Added RORP - Freehold recrod types 'Freehold_Transfer_Registration'
            // v1.11 Added GSO Authorized Signatory - GSO_Authorized_Signatory
            set<string> setSRRecordTypeName = new set<string>{'Lease_Registration' , 'Surrender_Termination_of_Lease_and_Sublease' , 'Freehold_Transfer_Registration' , 'Add_Remove_GSO_Authorized_Signatory', 'New_Index_Card'};
           
            if(trigger.isInsert || trigger.isUpdate){
                for(Amendment__c objAmd : Trigger.New){
                    if((setAmendRecordTypeName.contains(objAmd.Record_Type_Name__c) && setSRRecordTypeName.contains(objAmd.SR_Record_Type__c) 
                        && objAmd.SR_Status__c == 'DRAFT') ){ //v1.11 removed objAmd.SR_Group__c == 'RORP' filter
                        if(trigger.isInsert || (trigger.isUpdate && (objAmd.LastModifiedDate <> trigger.oldMap.get(objAmd.Id).LastModifiedDate) )){
                            if(!mapSR.containsKey(objAmd.ServiceRequest__c)){
                                Service_Request__c objSR = new Service_Request__c(Id = objAmd.ServiceRequest__c);
                                objSR.Change_Activity__c = true;
                                //v1.14 - start
                                if(objAmd.Record_Type_Name__c == 'Developer' && objAmd.SR_Record_Type__c == 'Freehold_Transfer_Registration'){
                                    objSR.Store_in_DIFC__c = true;  
                                }
                                //v1.14 - end                            
                                mapSR.put(objAmd.ServiceRequest__c,objSR);
                            }                   
                            // v1.7
                            if(objAmd.SR_Record_Type__c == 'Freehold_Transfer_Registration'){
                                if(mapSRIdWithFreeHoldTypeReq.containsKey(objAmd.ServiceRequest__c)){
                                    string recordType = mapSRIdWithFreeHoldTypeReq.get(objAmd.ServiceRequest__c);
                                    recordType+= ';' + objAmd.Record_Type_Name__c;  
                                    mapSRIdWithFreeHoldTypeReq.put(objAmd.ServiceRequest__c,recordType);
                                }else{
                                    mapSRIdWithFreeHoldTypeReq.put(objAmd.ServiceRequest__c,objAmd.Record_Type_Name__c);
                                }   
                        }               
                    }
                }
            }
            }
            else if(trigger.isDelete && trigger.isAfter){
                for(Amendment__c objAmd : Trigger.old){
                    if((setAmendRecordTypeName.contains(objAmd.Record_Type_Name__c) && setSRRecordTypeName.contains(objAmd.SR_Record_Type__c) 
                        && objAmd.SR_Status__c == 'DRAFT') ){ //v1.11 removed objAmd.SR_Group__c == 'RORP' filter
                        if(!mapSR.containsKey(objAmd.ServiceRequest__c)){
                            Service_Request__c objSR = new Service_Request__c(Id = objAmd.ServiceRequest__c);
                            objSR.Change_Activity__c = true;
                            //v1.14 - start
                            if(objAmd.Record_Type_Name__c == 'Developer' && objAmd.SR_Record_Type__c == 'Freehold_Transfer_Registration'){
                                objSR.Store_in_DIFC__c = false; 
                            }
                            //v1.14 - end                          
                            mapSR.put(objAmd.ServiceRequest__c,objSR);
                        }                               
                        // v1.7
                        if(objAmd.SR_Record_Type__c == 'Freehold_Transfer_Registration'){
                            if(mapSRIdWithFreeHoldTypeReq.containsKey(objAmd.ServiceRequest__c)){
                                string recordType = mapSRIdWithFreeHoldTypeReq.get(objAmd.ServiceRequest__c);
                                recordType+= ';' + objAmd.Record_Type_Name__c;  
                                mapSRIdWithFreeHoldTypeReq.put(objAmd.ServiceRequest__c,recordType);
                            }else{
                                mapSRIdWithFreeHoldTypeReq.put(objAmd.ServiceRequest__c,objAmd.Record_Type_Name__c);
                    }
                }           
            }
                }           
            }
            system.debug('mapSR' + mapSR);
            if(!mapSR.isEmpty()){
                
                //Integer countAuthSign =  [SELECT count() FROM Amendment__c WHERE ServiceRequest__c = :mapSR.keySet()];
                //system.debug('countAuthSign' + countAuthSign);
                
                for(Service_Request__c sr : [Select Id,Change_Activity__c,Summary__c,Record_Type_Name__c, Quantity__c from Service_Request__c where Id In: mapSR.keySet()]){
                    if(sr.Record_Type_Name__c <> 'Freehold_Transfer_Registration' ){
                        if(sr.Change_Activity__c && mapSR.containsKey(sr.Id)){
                            mapSR.remove(sr.Id);
                        }                       
                        /*
                        if(sr.Record_Type_Name__c == 'Add_Remove_GSO_Authorized_Signatory'){
                            system.debug('sr.Quantity__c ' + sr.Quantity__c);
                            if(countAuthSign == sr.Quantity__c){
                                Service_Request__c addedSR = new Service_Request__c();
                                addedSR = mapSR.get(sr.Id);
                                addedSR.Sponsor_Paid_Family_File__c = true;
                                mapSR.put(sr.Id,addedSR);
                                system.debug('mapSR in addedSR ' + mapSR);
                            }else if(sr.Change_Activity__c && mapSR.containsKey(sr.Id)){
                                mapSR.remove(sr.Id);
                            }   
                        }else{
                            if(sr.Change_Activity__c && mapSR.containsKey(sr.Id)){
                                mapSR.remove(sr.Id);
                            }                       
                        }*/
                            
                    }
                    
                }
                if(!mapSR.isEmpty()){
                    update mapSR.values();
                }
            }
        } //v1.5 - end           
      
        
        //V1.13 Fit - Out Conditions added by Swati
        if(trigger.IsAfter && (Trigger.isInsert || Trigger.isUpdate)){
            list<id> leaseIdList = new list<id>();
            list<id> SRIdList = new list<id>();
            list<id> amenIdList = new list<id>();
            map<id,Lease_Partner__c> leaseMap = new map<id,Lease_Partner__c>();
            list<amendment__c> tempList = new list<amendment__c> ();
            map<id,id> UnitAndSrMap = new map<id,id>();
            map<id,id> UnitAndBuildingMap = new map<id,id>();
            boolean runOnlyForContractorAccess = false;
            Service_request__c objSr = new Service_request__c ();
            id building;
            string buildingname;
            String unitnames='';
            // V1.13 - Start
            for(Amendment__c tempObj:trigger.new){
                if(tempObj.Record_Type_Name__c == 'Fit_Out_Units'){
                    leaseIdList.add(tempObj.Lease_Partner__c);
                    SRIdList.add(tempObj.ServiceRequest__c);
                    amenIdList.add(tempObj.id);
                }
            }
            
            if(!SRIdList.isEmpty()){
                objSr = [select id,record_type_name__c,Current_Registered_Office_No__c,Customer__r.name from Service_request__c where id in : SRIdList];
                tempList = [select id, Record_Type_Name__c,Lease_Partner__c,Lease_Partner__r.unit__r.building__c,
                                   ServiceRequest__c,Lease_Partner__r.unit__c,Lease_Partner__r.unit__r.building__r.name,Lease_Partner__r.unit__r.name
                            from Amendment__c where Record_Type_Name__c =: 'Fit_Out_Units' and ServiceRequest__c = : objSr.id and id IN : amenIdList];
                if(!tempList.isEmpty()){
                    building =  tempList[0].Lease_Partner__r.unit__r.building__c;
                    buildingname =  tempList[0].Lease_Partner__r.unit__r.building__r.name; //Added for V1.22
                }
                if(objSr!=null && objSr.record_type_name__c == 'Request_Contractor_Access'){
                    runOnlyForContractorAccess = true;
                    if(!leaseIdList.isEmpty()){
                        for(Lease_Partner__c temp : [select id,unit__c,unit__r.building__c from Lease_Partner__c where id IN : leaseIdList]){
                            leaseMap.put(temp.id,temp); 
                        }   
                    }
                    for(Amendment__c temp : [select id,Building__c,Lease_Unit__c, Lease_Partner__c,Lease_Partner__r.unit__r.building__c,ServiceRequest__c from Amendment__c where Record_Type_Name__c =: 'Fit_Out_Units' and ServiceRequest__c = : objSr.id and Lease_Partner__r.unit__r.building__c!=:building]){
                        UnitAndBuildingMap.put(temp.Lease_Partner__r.unit__r.building__c,temp.ServiceRequest__c);   
                    } 
                    //V1.26 #4214 Mudasir updated the below code and added AND ServiceRequest__r.External_Status_Name__c!='Fit to Occupy Certificate Issued'
                    //Exclude the look and feel requests 
                    // V1.27 - Added the filter to filter the Look and Feel requests from the Unit amendments    
                    for(Amendment__c temp : [select id, Lease_Partner__c,ServiceRequest__c from Amendment__c where Record_Type_Name__c =: 'Fit_Out_Units' and ServiceRequest__c!=null and ServiceRequest__c !=: objSr.id and ServiceRequest__r.isClosedStatus__c=false AND ServiceRequest__r.External_Status_Name__c!='Rejected' AND ServiceRequest__r.External_Status_Name__c!='Fit to Occupy Certificate Issued' AND ServiceRequest__r.External_Status_Name__c!='Completion Certificate Issued' AND Service_Type__c !='Look and Feel Approval Request']){ // V1.19 By Kaavya - Modified condition for checking closed SRs --(ServiceRequest__r.External_Status_Name__c!='Project Completed' and ServiceRequest__r.External_Status_Name__c!='Induction Completed')])
                        UnitAndSrMap.put(temp.Lease_Partner__c,temp.ServiceRequest__c);
                    }
                }
            }
            
            for(amendment__c objAmnd : tempList){
                
                try { // V1.23 - Claude
                    
                    if(objAmnd.Record_Type_Name__c == 'Fit_Out_Units' && runOnlyForContractorAccess==true && RecursiveControlCls.runOnlyOnce==false){
                        RecursiveControlCls.runOnlyOnce = true;
                        if(UnitAndSrMap.isEmpty() && !leaseMap.isEmpty()){
                            if(objAmnd.Lease_Partner__c != null && leaseMap.size()>0 && UnitAndBuildingMap.size()==0){
                                objAmnd.Lease_Unit__c = leaseMap.get(objAmnd.Lease_Partner__c).unit__c;
                                update objAmnd;
                            }
                        }
                        if(!UnitAndSrMap.isEmpty() && !UnitAndSrMap.containsKey(objAmnd.Lease_Partner__c)){
                            if(objAmnd.Lease_Partner__c != null && leaseMap.size()>0 && UnitAndBuildingMap.size()==0){
                                objAmnd.Lease_Unit__c = leaseMap.get(objAmnd.Lease_Partner__c).unit__c;
                                update objAmnd;
                            }
                            else if(objAmnd.Lease_Partner__c != null && leaseMap.size()>0 && UnitAndBuildingMap.size()>0 && UnitAndBuildingMap.containsKey(building)){
                                objAmnd.Lease_Unit__c = leaseMap.get(objAmnd.Lease_Partner__c).unit__c;
                                update objAmnd;
                            }
                            else{
                                for(amendment__c temp : trigger.new){
                                    temp.AddError('Unit should be from same building.');
                                }
                            }
                        }else if(!UnitAndSrMap.isEmpty() && UnitAndSrMap.containsKey(objAmnd.Lease_Partner__c)){
                            for(amendment__c temp : trigger.new){
                              //Check if the service request service type of this unit is not Look and feel request. 
                                //if(UnitAndSrMap.get(objAmnd.Lease_Partner__c).Service_Type__c !='Look and Feel Approval Request') 
                                temp.AddError('Unit is already being Used.');
                            }
                        } 
                        //V1.22 Start
                        
                        /* - Commenting the previous logic
                        if(objSR.Current_Registered_Office_No__c!=null){
                            
                          
                            objSR.Current_Registered_Office_No__c = objSR.Current_Registered_Office_No__c+' - '+objAmnd.Lease_Partner__r.unit__r.name;
                            
                        }
                        else{
                            objSR.Current_Registered_Office_No__c = objSR.Customer__r.name +' - '+objAmnd.Lease_Partner__r.unit__r.building__r.name+' - '+objAmnd.Lease_Partner__r.unit__r.name;
                        } */               
                        
                        //V1.22 End
                    }
                } catch (DMLException e){ // V1.23 - Claude
                    
                    for(amendment__c temp : trigger.new){
                        temp.AddError(e.getDmlMessage(0)); // V1.23 - Claude - Added the message instead of the common error
                    }
                }
            }
            
            if(runOnlyForContractorAccess==true){
                
                //V1.22 Start
                
                String unitDesc = '';
                String floorDesc = '';
                
                Set<String> floorNums = new Set<String>();
                
                for(Amendment__c temp: [select id, Record_Type_Name__c,Lease_Partner__c,Lease_Partner__r.Unit__r.Floor__c,Lease_Partner__r.unit__r.building__c,ServiceRequest__c,Lease_Partner__r.unit__c,Lease_Partner__r.unit__r.building__r.name,Lease_Partner__r.unit__r.name // V1.23 - Claude - Added 'Floor__c' field for units
                                       from Amendment__c where Record_Type_Name__c =: 'Fit_Out_Units' and ServiceRequest__c = : objSr.id]){
                    
                    String floorNum = String.isNotBlank(temp.Lease_Partner__r.unit__r.Floor__c) ? ' Level ' + temp.Lease_Partner__r.Unit__r.Floor__c + '-' : ''; // V1.23 - Claude     
                    
                    unitnames+= '-' + floorNum + temp.Lease_Partner__r.unit__r.name; // V1.24
                    
                    unitDesc +=  temp.Lease_Partner__r.unit__r.name + ' & ';         // V1.24
                    
                    //floorDesc +=  String.isNotBlank(temp.Lease_Partner__r.unit__r.Floor__c) ? temp.Lease_Partner__r.Unit__r.Floor__c + ' & ' : ''; // V1.24
                    if(String.isNotBlank(temp.Lease_Partner__r.unit__r.Floor__c)){
                        floorNums.add(temp.Lease_Partner__r.Unit__r.Floor__c);
                    }
                }
               
                system.debug('UUUU'+unitnames);
                
                for(String s : floorNums){
                    floorDesc += s + ' & ';
                }
                
                objSR.Current_Registered_Office_No__c = objSR.Customer__r.name +' - '+ buildingname + unitnames; 
                //objSr.Address_Details__c = unitDesc.substringbeforeLast('&'); // V1.24
                objSr.Sys_Registered_Address_CL__c = unitDesc.substringbeforeLast('&'); // V1.24
                objSr.Foreign_Registered_Level__c = floorDesc.substringbeforeLast('&'); // V1.24
                //V.22 End
                
                update objSR;
            }
        }
        
        if(trigger.IsBefore && (Trigger.isInsert || Trigger.isUpdate)){
            list<amendment__c> amenList = new list<amendment__c>();
            boolean isContractorInsurance = false;
            list<amendment__c> tempList = new list<amendment__c> ();
            list<id> SRIdList = new list<id>();
            for(Amendment__c tempObj:trigger.new){
                if(tempObj.Record_Type_Name__c == 'Contractor_Insurance'){
                    amenList.add(tempObj);
                    SRIdList.add(tempObj.ServiceRequest__c);
                    isContractorInsurance = true;
                }
            }
            
            if(isContractorInsurance == true && !SRIdList.isEmpty() && !amenList.isEmpty()){
                tempList = [select id, Record_Type_Name__c,Amendment_Type__c
                            from Amendment__c where Record_Type_Name__c =: 'Contractor_Insurance' 
                            and ServiceRequest__c IN : SRIdList 
                            and Amendment_Type__c=: amenList[0].Amendment_Type__c]; 
                    
            }
            
            if(trigger.isInsert){
                if(!tempList.isEmpty()){
                    for(amendment__c temp : trigger.new){
                        temp.AddError(temp.Amendment_Type__c+' certificate already exist.');
                    }
                }   
            }
            
            if(trigger.isUpdate){
                if(!tempList.isEmpty()){
                    for(amendment__c temp : trigger.new){
                        if(temp.Amendment_Type__c!=trigger.oldmap.get(temp.id).Amendment_Type__c){
                            temp.AddError(temp.Amendment_Type__c+' certificate already exist.');
                        }
                    }
                }
            }
        }
        //V1.13 - End
        
        //V1.15 - start, Validation for existing fit-out contacts
        if(trigger.IsBefore && Trigger.isInsert){
            
            //V1.16 - Removed two types per new requirement Set<String> amendmentTypeSet = new Set<String>{'Tenant Authorized Representative','Tenant On - Site Fit - Out Representative','Architect/Interior Designer','Consultant/Project Manager','Emergency Contacts'};
            Set<String> amendmentTypeSet = new Set<String>{'Tenant Authorized Representative','Tenant On - Site Fit - Out Representative','Emergency Contacts','Technical Clarification form'}; // V1.16 removed 'Architect/Interior Designer' and 'Consultant/Project Manager' as mandatory fields
            
            Set<Id> relatedRequestIds = new Set<Id>();  // set to store the related request ids 
            
            /*
             * Get the ServiceRequest__c field from each new record
             */
            for(Amendment__c amendmentRecord : trigger.new){
                relatedRequestIds.add(amendmentRecord.ServiceRequest__c);
            }

            // query the existing records        
            List<Amendment__c> existingAmendments = [SELECT Id, Amendment_Type__c, RecordType.Name, ServiceRequest__c FROM Amendment__c WHERE ServiceRequest__c in :relatedRequestIds];
            
            existingAmendments.addAll(trigger.new); // add the new records
            
            // map to store the related amendments per service request
            Map<Id,List<Amendment__c>> amendmentRecordMap = new Map<Id,List<Amendment__c>>();
            
            for(Amendment__c existingAmendment : existingAmendments){
                
                List<Amendment__c> listOfAmendments = amendmentRecordMap.get(existingAmendment.ServiceRequest__c) == null ? new List<Amendment__c>() : amendmentRecordMap.get(existingAmendment.ServiceRequest__c);
                
                System.debug('Adding record to map >> ' + existingAmendment);
                
                listOfAmendments.add(existingAmendment); 
                
                System.debug('@@ Adding to map');
                
                amendmentRecordMap.put(existingAmendment.ServiceRequest__c,listOfAmendments);
            } 
            
            /*
             * Iterate through each new record and get a count of each type
             */
            
            for(Id serviceRequestId : amendmentRecordMap.keySet()){
                
                List<Amendment__c> amendmentRecords  = amendmentRecordMap.get(serviceRequestId);
                
                Map<String,Integer> amendmentTypeCountMap = new Map<String,Integer>(); // map to store the count per fit-out contact type
                
                for(Amendment__c amendmentRecord : amendmentRecords){
                                    
                    String amendmentType = amendmentRecord.Amendment_Type__c;
                    
                    System.debug('Checking: ' + amendmentType);
                    System.debug('The record type: ' + amendmentRecord.RecordType.Name);
                    
                    if(amendmentTypeSet.contains(amendmentType) || amendmentTypeSet.contains(amendmentRecord.RecordType.Name)){
                        
                        Integer contactTypeCount = 0;
                        if(amendmentRecord.RecordType.Name != null && amendmentRecord.RecordType.Name.equals('Technical Clarification form') ){
                            contactTypeCount = amendmentTypeCountMap.get(amendmentRecord.RecordType.Name) == null ? 0 : amendmentTypeCountMap.get(amendmentRecord.RecordType.Name);
                        } else {
                            contactTypeCount = amendmentTypeCountMap.get(amendmentType) == null ? 0 : amendmentTypeCountMap.get(amendmentType);
                        }
                        
                        contactTypeCount++;
                        
                        System.debug('Counting: ' + amendmentType);
                        System.debug('Type Count: ' + contactTypeCount);
                    
                        if(amendmentRecord.RecordType.Name != null && amendmentRecord.RecordType.Name.equals('Technical Clarification form')){
                            System.debug('Getting special form ->');
                            amendmentTypeCountMap.put(amendmentRecord.RecordType.Name,contactTypeCount);
                        } else {
                            amendmentTypeCountMap.put(amendmentType,contactTypeCount);  
                        }
                        
                    }
                }
                
                /*
                 * Iterate through each new record, and check if there already 
                 * an instance of the type
                 */
                for(Amendment__c amendmentRecord : trigger.New){
                    
                    String amendmentType = amendmentRecord.Amendment_Type__c;
                    System.debug('The record type name: ' + amendmentRecord.RecordType.Name);
                    System.debug('The map: ' + amendmentTypeCountMap);
                    if(
                        (amendmentTypeCountMap.containsKey(amendmentType) && amendmentTypeCountMap.get(amendmentType) > 1) ||
                        (amendmentTypeCountMap.containsKey('Technical Clarification form') && amendmentTypeCountMap.get('Technical Clarification form') >= 1)  
                        ){
                        amendmentRecord.addError('You can only add one for each type.');
                    }
                    
                }
                
            } 
            
        }
        //V1.15 - end 
        
       
    	/*
        if(trigger.isBefore){
            integer i=0;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
           
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
             i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
           
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
           
        }
    	*/
    }
    
    catch(Exception ex){
        
        for (Amendment__c acc : trigger.New){
            string errorMsg = CustomErrorClass.GetCustomMessage(ex.getMessage());
            acc.adderror(errorMsg);                                                                                       
        }
        
        
    }
    
    
    
        
}