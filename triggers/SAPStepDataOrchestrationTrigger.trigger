trigger SAPStepDataOrchestrationTrigger on Temp_SAP_Step_Data__c (after insert, after update) {
    if(Trigger.isAfter){
        
        if(Trigger.IsInsert && system.Label.step_Integration=='Yes'){
            try{
                SAPStepDataOrchestrationTriggerHandler.afterInsertOperation(Trigger.new);
                system.debug('SAPStepDataOrchestrationTrigger after Insert');
            }catch(Exception e){
                system.debug('Exception in IsInsert  was--'+e.getMessage());
            }
        }
        else if(Trigger.IsUpdate && system.Label.step_Integration=='Yes'){
            try{
                SAPStepDataOrchestrationTriggerHandler.afterUpdateOperation(Trigger.oldMap, Trigger.newMap);
                system.debug('SAPStepDataOrchestrationTrigger after Update');
            }catch(Exception e){
                system.debug('Exception in IsUpdate was--'+e.getMessage());
            }
        }
        
    }
}