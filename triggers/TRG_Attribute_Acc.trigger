trigger TRG_Attribute_Acc on SAP_Attribute__c (after insert,after update) {
    
    Map<Id,String> HomeCountryVal= new Map<Id,String>();
    Map<Id,String> BizActyVal= new Map<Id,String>();
    Map<Id,String> DFSAApprVal= new Map<Id,String>();
    Map<Id,String> DataCentVal= new Map<Id,String>();
    Map<Id,String> OpTypeVal= new Map<Id,String>();
    Map<Id,String> ITTeamVal= new Map<Id,String>();
    Map<Id,String> DataCentAccessVal= new Map<Id,String>();
    Map<Id,String> FormerNameVal= new Map<Id,String>();
    Map<Id,String> PendingDateVal= new Map<Id,String>();
    Map<Id,String> FinYrEndVal= new Map<Id,String>();
    List<Id> AccIds = new List<Id>();
    
    for(SAP_Attribute__c Obj :trigger.new){
        AccIds.add(Obj.Account__c);
        if(Obj.Home_Country__c!=null)
        HomeCountryVal.put(Obj.Account__c,Obj.Home_Country__c);
        if(Obj.Business_Activity__c!=null)
        BizActyVal.put(Obj.Account__c,Obj.Business_Activity__c);
        if(Obj.DFSA_Approval__c!=null)
        DFSAApprVal.put(Obj.Account__c,Obj.DFSA_Approval__c);
        if(Obj.DIFC_Data_Center__c!=null)
        DataCentVal.put(Obj.Account__c,Obj.DIFC_Data_Center__c);
        if(Obj.DIFC_IT_Team_Members__c!=null)
        ITTeamVal.put(Obj.Account__c,Obj.DIFC_IT_Team_Members__c);
        if(Obj.DIFC_Type_Operation__c!=null)
        OpTypeVal.put(Obj.Account__c,Obj.DIFC_Type_Operation__c);
        if(Obj.Data_Center_Access__c!=null)
        DataCentAccessVal.put(Obj.Account__c,Obj.Data_Center_Access__c);
        if(Obj.Former_Name__c!=null)
        FormerNameVal.put(Obj.Account__c,Obj.Former_Name__c);
        if(Obj.Pending_Dissolution_Date__c!=null)
        PendingDateVal.put(Obj.Account__c,Obj.Pending_Dissolution_Date__c);
        system.debug('OO==>'+Obj.Pending_Dissolution_Date__c);
        system.debug('PPP==>'+PendingDateVal);
        if(Obj.Financial_Year_End__c!=null)
        FinYrEndVal.put(Obj.Account__c,Obj.Financial_Year_End__c);
    }
    
    List<Account> AccList=[select id,Financial_Year_End__c,Pending_Dissolution_Date__c,Place_of_Registration__c,Sector_Classification__c,Former_Name__c,Home_Country__c,Data_Center_Access__c,DFSA_Approval__c,DIFC_Data_Center__c,DIFC_IT_Team_Members__c,DIFC_Type_Operation__c from Account where id in: AccIds];
    for(Account acc : AccList){
        if(HomeCountryVal.get(acc.id)!=null)
        acc.Home_Country__c=HomeCountryVal.get(acc.id);
        if(BizActyVal.get(acc.id)!=null)
        acc.Sector_Classification__c=BizActyVal.get(acc.id);
        if(DFSAApprVal.get(acc.id)!=null)
        acc.DFSA_Approval__c=DFSAApprVal.get(acc.id);
        if(DataCentVal.get(acc.id)!=null)
        acc.DIFC_Data_Center__c=DataCentVal.get(acc.id);
        if(ITTeamVal.get(acc.id)!=null)
        acc.DIFC_IT_Team_Members__c=ITTeamVal.get(acc.id);
        if(OpTypeVal.get(acc.id)!=null)
        acc.DIFC_Type_Operation__c=OpTypeVal.get(acc.id);
        if(DataCentAccessVal.get(acc.id)!=null)
        acc.Data_Center_Access__c=DataCentAccessVal.get(acc.id);
        if(FormerNameVal.get(acc.id)!=null)
        acc.Former_Name__c=FormerNameVal.get(acc.id);
        if(FinYrEndVal.get(acc.id)!=null)
        acc.Financial_Year_End__c=FinYrEndVal.get(acc.id);
        String pendate='';
        if(PendingDateVal.get(acc.id)!=null)
        pendate=PendingDateVal.get(acc.id);
        system.debug('PPDD==>'+pendate);
        if(pendate!=''&& pendate!=null){
            /*
            List<String>pendatestring= pendate.split('.');
            system.debug(pendatestring+'PPPPPPPP');
            Integer y=Integer.valueof(pendatestring[2]);
            Integer m=Integer.valueof(pendatestring[1]);
            Integer d=Integer.valueof(pendatestring[0]);
            */
            Integer y=Integer.valueof(pendate.right(4));
            system.debug('YYY=='+y);
            Integer m=Integer.valueof(pendate.substring(3,5));
            system.debug('MMM=='+m);
            Integer d=Integer.valueof(pendate.left(2));
            system.debug('DDD=='+d);
            Date PenD = Date.newinstance(y,m,d);
            system.debug('DatePEND==='+PenD);
            acc.Pending_Dissolution_Date__c=PenD;
        }
        
    }
    
    update AccList;
    
    

}