trigger CommunicationTrailTrigger on CommunicationTrail__c (before insert, before update, after insert, after update) {
	
    if((trigger.isInsert || trigger.isUpdate) && trigger.isBefore){
        system.debug('in before trigger');
        try{
        	CommunicationTriggerHandler.onbeforeInsert(trigger.new);    
        }
        catch(Exception e){
            system.debug('Exception occured '+e.getMessage());
        }
    	
    }
    
    if(trigger.isAfter && trigger.isInsert){
        try{
         	CommunicationTriggerHandler.onafterInsertUpdate(trigger.newMap,trigger.new);   
        }       
        catch(Exception e){
            system.debug('Exception occured '+e.getMessage());
        }
    }
    
    if(trigger.isafter && trigger.isUpdate){
        Map<Id,CommunicationTrail__c> updatedCommTrailMap = new Map<Id,CommunicationTrail__c>();
        List<CommunicationTrail__c> commTrailList = new List<CommunicationTrail__c>();
        try{
        for(CommunicationTrail__c commTrail : trigger.new){
            CommunicationTrail__c oldCommTrail = Trigger.oldMap.get(commTrail.Id);
            if(commTrail.Status__c == 'Draft' && ((oldCommTrail.Building_Name__c != commTrail.Building_Name__c) || (oldCommTrail.Entity_Type__c != commTrail.Entity_Type__c))){
                system.debug('### building Name '+oldCommTrail.Building_Name__c);
                updatedCommTrailMap.put(commTrail.Id,commTrail);
                commTrailList.add(commTrail);
            }
        }
        if(!commTrailList.isEmpty()){
            system.debug('### commTrail '+commTrailList);
            CommunicationTriggerHandler.onafterInsertUpdate(updatedCommTrailMap,commTrailList);
        }
        }
        catch(Exception e){
            system.debug('Exception occured '+e.getMessage());
        }
    }
}