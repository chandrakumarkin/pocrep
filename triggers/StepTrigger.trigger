/******************************************************************************************
 *  Step Trigger
 *  Author   : Shadiya Moideen/Durga Prasad
 *  Company  : NSI JLT
 *  Date     : 2013-10-02                           
 ----------------------------------------------------------------------
   Version  Date         Author             Remarks                                                 
  =======   ==========   =============  ==================================
  v1.1      05-Jun-14                   Parent Sr price line - Consumption 
  v1.2      14-Sep-14                   SR PriceItem Evaluated & Consumed at on Same Step
  v1.3      06-Apr-15    Saima          Added to populate the Step Close Date on 06/04/2015
  v1.4      12-Apr-15    Saima          Added to populate step owner id before insert
  V1.5      12-Oct-15    Ravindra       Added the logic for GS Courier Process
  V1.6      06-Dec-15    Ravindra       Added the logic to update Shipment Status for ROC, if ROC team closes the Step manually
  V1.7      06-Jan-16    Ravi           Added the logic to close the shipments if Step getting closed from SAP/Salesforce.
  V1.8      08-03-2016   Ravi           Added the condition for, when create conditions are firing for the same step again it's not creating the step as per #2165
                                        Check the code for Custom Conditions and Actions and modified the Create Conditions logic accordingly
  V1.9      06-Apr-2016   Claude      Added logic to update SR when snag fix has not been done (Ticket #2740)     
  V2.0    14-Apr-2016   Claude         Added 'Final Inspection Planning' as part of the 'Client Entry User' queue                               
  V2.1      18-Apr-2016   Claude     Added validation for DEWA and DCD Inspection dates
  V2.2      03-May-2016  Claude      Added 'Project Cancellation' and 'Event Service Request' per Ticket #2752
  V2.3    31-May-2016   Claude      Added bypass field for steps that do not require documents per Ticket #2976
  V2.4    04-Jun-2016   Claude      Added method to calculate business hours from start to closed date per Ticket #3057
  v2.5    21-Jun-2016   Swati      StepTrgHandler and ticket 2521
  V2.6    11-Jul-2016   Claude      Bug fix for Ticket #3142 on mapSRSteps
  v2.7    25-Jul-2016  Claude      Added logic for the Business Hours for IDAMA and FIT-OUT
  v2.8    15-Aug-2016   Claude      Commented logic that copies Comments to Customer comments field
  v2.9    03-Nov-2016   Claude      Change 'IDAMA' to 'FOSP' per Phase 2
  v3.0    02-Feb-2017   Claude      Added logic for SITE-visit reports (#3864)
  v3.1    06-Jun-2017   Claude        Added default action to copy record type name into step for sharing rules 
  v3.2      21-Jan-2018  Veera          Added validation on Emirates ID Registration is Completed duplicate Id Numner tkt # 938
*******************************************************************************************/

trigger StepTrigger on Step__c (before insert,after insert,before update,after update){
    //
    //in the sr trigger, we will check onsubmit = true and insert sr docs and price lines
    //whreas here in step trigger we will check evaluated at  and original sighted at and insert those sr docs and price
    //
    StateManager SM = new StateManager();
    Context ctx = new Context(); 
         
    Boolean BRResultFlag;
    
    set<Id> BRIdSet = new set<Id>();
    set<id> setProdId = new set<id>();
    set<id> setStepIds = new set<id>();//Set to store the Step Ids which are to be sent for BusinessRules Execution.
    set<id> setStep_TemplId = new set<id>();//Set to store the Step Template Ids which are to be sent for BusinessRules Execution.
    set<id> setPLIds = new set<id>();
    
    map<string,Id> mapStepQueues = new map<String,Id>();//Key as Queue Name and value is Queue-Id
    map<Id,string> mapStepTemplateQueues = new map<Id,string>();//Key as Queue Id and value is Queue-Name
    map<Id,string> mapSRStepQueues = new map<Id,string>();//Key as Queue Id and value is Queue-Name
    map<Id,string> mapSRTemplateQueues = new map<Id,string>();//Key as Queue Id and value is Queue-Name
    map<id,id> mapSROwner = new map<id,id>();
    set<id> setSROwnerId = new set<id>();
    map<id,string> mapUserLicense = new map<id, string>();
    list<SR_Price_Item__c> updateSRPriceItemLst = new list<SR_Price_Item__c>();
    map<string,SR_Price_Item__c> exstngSRPriceItmsMap = new map<string,SR_Price_Item__c>();
    list<SR_Doc__c> lstDocAddedThroughCode = new list<SR_Doc__c>();
    
    List<Step__c> foSteps = new List<Step__c>(); // V2.6 - Claude - Stores the FO Steps 
    
    //v2.5
    TriggerFactoryCls.createHandler(Step__c.sObjectType);
    
    /*Start of Before Insert Trigger logic to be Executed*/
    // V1.4 Added to populate step owner id before insert on 12 April, 2015
    if(trigger.isBefore && trigger.isInsert){
      
      
        
        for(Step__c stp:Trigger.New){
            
            if(stp!=null && stp.OwnerId!=null && (stp.Sys_Step_OwnerId__c==null || stp.Sys_Step_OwnerId__c==''))
                stp.Sys_Step_OwnerId__c = stp.OwnerId;
            
            //added on 25th April,2015 by Ravi - inorder to populate the SR Group on Step
            stp.SR_Group__c = stp.Sys_SR_Group__c;
            
            if(stp.SR_Template__c == 'Fit_Out_Service_Request'){        
                stp.notification_date__c = date.valueOf(CC_cls_FitOutandEventCustomCode.calculateEndDate(system.today(),14));           
                CC_cls_FitOutandEventCustomCode.checkDewaDcdInspectionDates(stp); // V2.1 - Claude - Added validation for DEWA and DCD inspection dates
            }
                   
            if(stp.SR_Template__c == 'Event_Service_Request'){      
                stp.notification_date__c = date.valueOf(CC_cls_FitOutandEventCustomCode.calculateEndDate(system.today(),3));            
            }
            
            //V3.1 - Claude - Start
            if(String.isNotBlank(stp.SR_Template__c)) stp.SR_Record_Type_Text__c = stp.SR_Template__c;
            //V3.1 - Claude - End
            
            
        }        
        
        
        
    }
    
    
    
    /* Start of Insert Trigger logic to be Executed */
    if(Trigger.isAfter && Trigger.IsInsert){
        exstngSRPriceItmsMap = new map<string,SR_Price_Item__c>();
        
        map<string,SR_Template_Item__c> mapConsumedPriceItems = new map<string,SR_Template_Item__c>();//v1.2
        
        map<id,list<Service_Request__c>> mapSRTemplateSR = new map<id,list<Service_Request__c>>();// key as SRTemplate-Id and value as List<Service_Request__c> for Each SRTemplate.
        map<id,list<SR_Template_Item__c>> mapSRTemplateItems = new map<id,list<SR_Template_Item__c>>();// key as SRTemplate Id and value as List<SR_Template_Item__c> for Each SRTemplate.
        map<id,list<Pricing_Line__c>> mapProductPricingLines = new map<id,list<Pricing_Line__c>>();// key as Product Id and value as List<Pricing_Line__c> for Each Product.
        map<id,list<Dated_Pricing__c>> mapPLDatedPricing = new map<id,list<Dated_Pricing__c>>();// key as Pricing_Line__c Id and value as List<Dated_Pricing__c> for Each Pricing Line.
        map<id,list<Pricing_Range__c>> mapPLPricingRange = new map<id,list<Pricing_Range__c>>();// key as DatedPrice Id and value as List<Pricing_Range__c> for Each DatedPrice.
        map<id,PricebookEntry> MapProductPriceBook = new map<id,PricebookEntry>();// key as Product Id and value as PriceBookEntry for Each Product.
        map<id,list<id>> mapSRConsumeTemplateItems = new map<id,list<id>>();// key as SRTemplate Id and value as List<SR_Template_Item__c> for Each SRTemplate.
        set<id> setSRIds = new set<id>();
        set<id> srTemplIds = new set<id>();
        
        map<id,SR_Template_Item__c> mapProdSRtemplItem = new map<id,SR_Template_Item__c>();
        map<id,list<SR_Template_Docs__c>> mapSRTempDocs = new map<id,list<SR_Template_Docs__c>>();//Key SR Template Id and value as List of TemplatedDocs
        map<string,list<SR_Template_Docs__c>> mapSRTempDocs_Added_by_Code = new map<string,list<SR_Template_Docs__c>>();//Key SR Template Id and value as List of TemplatedDocs
        map<id,list<Condition__c>> mapDocConditions = new map<id,list<Condition__c>>();//Key SR Template Id and value as List of Doc Conditions
        set<id> setTemplDocIds = new set<id>();
        set<id> setSRStepIds = new set<id>();
        
        
        for(Step__c stp:Trigger.New){
            if(stp.Step_Template__c!=null){
                setStepIds.add(stp.id);
                setStep_TemplId.add(stp.Step_Template__c);
            }
            if(stp.SR__c!=null){
                setSRIds.add(stp.SR__c);
                setStepIds.add(stp.id);
            }
            if(stp.SR_Step__c!=null){
                setSRStepIds.add(stp.SR_Step__c);
            }
        }
        map<id,id> mapExistingSRDocs = new map<id,id>();
        if(setSRIds!=null && setSRIds.size()>0){
          for(SR_Doc__c objSRDoc:[select id,SR_Template_Doc__c from SR_Doc__c where Service_Request__c IN:setSRIds]){
            if(objSRDoc.SR_Template_Doc__c!=null)
              mapExistingSRDocs.put(objSRDoc.SR_Template_Doc__c,objSRDoc.SR_Template_Doc__c); // to avoid loadig sr doc again, we check this map == null
          }
        }
        
        
        map<id,id> mapSR_SRTemplate = new map<id,id>();//Key as SR id and Value is SR's SRTemplate Id
        set<id> setDefaultActBrIds = new set<id>();
        set<id> setCustomConditionBrIds = new set<id>();
        set<id> setTotalBRIds = new set<id>();
        set<id> SR_StepIds = new set<id>();
        map<string,list<SR_Template_Docs__c>> mapSRTempDocs_Code = new map<string,list<SR_Template_Docs__c>>();//Key SR Template DMS Index and value as List of TemplatedDocs
        map<string,id> mapSRTempDMS_Id = new map<string,id>();//Key SR Template DMS Index and value as Id of SR Template
        map<id,list<Business_Rule__c>> mapSRTempRelatedBRS = new map<id,list<Business_Rule__c>>();//key SR_Template Id and value as List of Business Rules for that SR_Template
        map<id,list<SR_Steps__c>> mapSRTempRelatedSRSteps = new map<id,list<SR_Steps__c>>();//key SR_Template Id and value as List of SR_Steps for that SR_Template
        list<SR_Doc__c> lstSRDocInsert = new list<SR_Doc__c>();
        if(setSRIds!=null && setSRIds.size()>0){
            for(Service_Request__c objSR:[select id,SR_Template__c from Service_Request__c where ID IN:setSRIds and SR_Template__c!=null]){
                srTemplIds.add(objSR.SR_Template__c);
                mapSR_SRTemplate.put(objSR.id,objSR.SR_Template__c);
            }
        }
        if(srTemplIds!=null && srTemplIds.size()>0){
            for(SR_Template__c SRTempl:[select id,(select id,SR_Template__c,Step_No__c,Estimated_Hours__c,Start_Status__c,Owner__c from SR_Steps_SR_Template__r) from SR_Template__c where ID IN:srTemplIds]){ // V2.6 - Claude - Added Owner Field in query
                if(SRTempl.SR_Steps_SR_Template__r!=null && SRTempl.SR_Steps_SR_Template__r.size()>0){
                    mapSRTempRelatedSRSteps.put(SRTempl.id,SRTempl.SR_Steps_SR_Template__r); //loading all sr steps into this map to evaluate condition for creating a particual step 
                    for(SR_Steps__c SRstp:SRTempl.SR_Steps_SR_Template__r){
                        SR_StepIds.add(SRstp.id);
                    }
                }
            }
            if(setSRStepIds!=null && setSRStepIds.size()>0){
                //for SR Docs
                //sr docs which are to be inserted at this step OR 
                //some of the docs orignal have to submittted at the counter - ( Original_Sighted_at__c )
                
                //Commented by Durga 
                for(SR_Template_Docs__c TemplDocs:[Select id,Name,Document_Master__r.LetterTemplate__c,Requirement__c,Generate_Document__c,Added_through_Code__c,Conditions__c,SR_Template_Docs_Condition__c,Document_Description_External__c,SR_Template__c,Courier_collected_at__c,Courier_delivered_at__c,Document_Master__c,
                          Evaluated_at__c,Original_Sighted_at__c,Group_No__c,Optional__c,DMS_Document_Category__c, DMS_Document_Index__c, DMS_Document_Section__c, Document_Description__c,Document_Name_for_SR__c,Document_Master__r.Name from SR_Template_Docs__c where SR_Template__c IN:srTemplIds and On_Submit__c=false and (Evaluated_at__c IN:setSRStepIds OR Original_Sighted_at__c IN:setSRStepIds)]){
                   if(TemplDocs.Added_through_Code__c==false){   
                      list<SR_Template_Docs__c> lstTemplDocs_Insert = new list<SR_Template_Docs__c>();
                      if(mapSRTempDocs.get(TemplDocs.SR_Template__c)!=null){
                          lstTemplDocs_Insert = mapSRTempDocs.get(TemplDocs.SR_Template__c);
                          lstTemplDocs_Insert.add(TemplDocs);
                          //THESE are framework docs which will be inserted later after evaluating the conditions
                          mapSRTempDocs.put(TemplDocs.SR_Template__c,lstTemplDocs_Insert);
                      }else{
                          lstTemplDocs_Insert.add(TemplDocs);
                          mapSRTempDocs.put(TemplDocs.SR_Template__c,lstTemplDocs_Insert);
                      }
                      setTemplDocIds.add(TemplDocs.id);
                   }else if(TemplDocs.Added_through_Code__c){
                      // Added_through_Code__c
                      list<SR_Template_Docs__c> lstTemplDocs_Added_By_Code = new list<SR_Template_Docs__c>();
                      if(TemplDocs.DMS_Document_Index__c!=null){
                        if(mapSRTempDocs_Code.get(TemplDocs.DMS_Document_Index__c)!=null){
                            lstTemplDocs_Added_By_Code = mapSRTempDocs_Code.get(TemplDocs.DMS_Document_Index__c);
                            lstTemplDocs_Added_By_Code.add(TemplDocs);
                            mapSRTempDocs_Code.put(TemplDocs.DMS_Document_Index__c,lstTemplDocs_Added_By_Code);
                        }else{
                            lstTemplDocs_Added_By_Code.add(TemplDocs);
                            mapSRTempDocs_Code.put(TemplDocs.DMS_Document_Index__c,lstTemplDocs_Added_By_Code);
                        }
                      }
                   }
                   mapSRTempDMS_Id.put(TemplDocs.DMS_Document_Index__c,TemplDocs.SR_Template__c);
                }
                
                //for price line
                //
                //insert price lines for evaluated at 
                //
                for(SR_Template_Item__c SRTemplItem:[Select id,Product__c,Evaluated_at__c,SR_Template__c,On_Submit__c from SR_Template_Item__c where SR_Template__c IN:srTemplIds and On_Submit__c=false and Evaluated_at__c IN:setSRStepIds]){
                    list<SR_Template_Item__c> lstSRTMPItems = new list<SR_Template_Item__c>();
                    if(mapSRTemplateItems.get(SRTemplItem.SR_Template__c)!=null && mapSRTemplateItems.get(SRTemplItem.SR_Template__c).size()>0){
                        lstSRTMPItems = mapSRTemplateItems.get(SRTemplItem.SR_Template__c);
                        lstSRTMPItems.add(SRTemplItem);
                        mapSRTemplateItems.put(SRTemplItem.SR_Template__c,lstSRTMPItems);
                    }else{
                        lstSRTMPItems.add(SRTemplItem);
                        mapSRTemplateItems.put(SRTemplItem.SR_Template__c,lstSRTMPItems);
                    }
                    mapProdSRtemplItem.put(SRTemplItem.Product__c,SRTemplItem);
                    setProdId.add(SRTemplItem.Product__c);
                }
              //for price line Consumed status
                list<SR_Price_Item__c> SRPriceItemList = new list<SR_Price_Item__c>();
                set<id>prodIDSet = new set<id>();
                set<id>SRTempIDSet = new set<id>();
        set<id>SetParChildSRs = new set<id>();
        //v1.1         
        for(service_request__c eaSR: [select id, 
                                             parent_sr__c,
                                             parent_sr__r.parent_sr__c,
                                             SR_Template__c,
                                             parent_sr__r.SR_Template__c ,
                                             parent_sr__r.parent_sr__r.SR_Template__c 
                                        from service_request__c where  (id in :setSRIds or parent_sr__c  in :setSRIds or parent_sr__r.parent_sr__c in :setSRIds  )]){
          SetParChildSRs.add(eaSR.id);
          SRTempIDSet.add(easr.SR_Template__c);
          if (eaSR.parent_sr__c!=null){
              SetParChildSRs.add(eaSR.parent_sr__c);
              SRTempIDSet.add(easr.parent_sr__r.SR_Template__c);
          }
          if (eaSR.parent_sr__r.parent_sr__c!=null){
              SetParChildSRs.add(eaSR.parent_sr__r.parent_sr__c);
              SRTempIDSet.add(easr.parent_sr__r.parent_sr__r.SR_Template__c);
          }          
        }
        //v1.1
               
               // converting the price item status to consumed
               
                for(SR_Template_Item__c SRTemplItem:[Select id,Product__c,Consumed_at__c,SR_Template__c,On_Submit__c from SR_Template_Item__c where SR_Template__c IN:SRTempIDSet and Consumed_at__c IN:setSRStepIds]){
                  //SR_Template__c IN:srTemplIds and
                    prodIDSet.add(SRTemplItem.Product__c);
                    mapConsumedPriceItems.put(SRTemplItem.Id,SRTemplItem);//v1.2
                    //SRTempIDSet.add(SRTemplItem.SR_Template__c);
                }
        
                if(prodIDSet!=null && prodIDSet.size()>0){
                    SRPriceItemList = [SELECT id,Product__c,Status__c,ServiceRequest__c,ServiceRequest__r.SR_Template__c FROM SR_Price_Item__c WHERE ServiceRequest__c IN:SetParChildSRs AND  ServiceRequest__r.SR_Template__c IN :SRTempIDSet AND Status__c='Blocked' AND Product__c IN :prodIDSet];
                    //ServiceRequest__c IN:setSRIds AND
                }
                if(SRPriceItemList!=null && SRPriceItemList.size()>0){
                    for(SR_Price_Item__c eachPriceItem : SRPriceItemList){
                        SR_Price_Item__c updateSRPriceItem = new SR_Price_Item__c(id=eachPriceItem.id,Status__c='Consumed');
                        updateSRPriceItemLst.add(updateSRPriceItem);
                    }
                }
            }
                
            //
            //Query all pricing related master data for creating the price line 
            //
            if(setProdId!=null && setProdId.size()>0){
                for(PricebookEntry PBE:[Select Product2Id,Pricebook2Id,Product2.Name,UnitPrice From PricebookEntry where Product2Id IN:setProdId and IsActive=true]){
                    MapProductPriceBook.put(PBE.Product2Id,PBE);
                }
                for(Product2 objProd:[select id,Name,(select id,Name,Conditions__c,Quantity__c,Override_SR_Quantity__c,PL_No__c,Price_Line_Condition__c,Priority__c,Product__c,Use_activities_as_license__c,Knowledge_Dirham_Applicable__c from Pricing_Lines__r order by Priority__c) from Product2 where ID IN:setProdId]){
                    if(objProd.Pricing_Lines__r!=null){
                        mapProductPricingLines.put(objProd.id,objProd.Pricing_Lines__r);
                        for(Pricing_Line__c PL:objProd.Pricing_Lines__r){
                            setPLIds.add(PL.id);
                        }
                    }
                }
                set<id> setDatedPricingIds = new set<id>();
                for(Pricing_Line__c objPL:[select id,(select id,Name,Appllication_Type__c,Date_From__c,Date_To__c,Pricing_Line__r.Quantity_based_on__c,Pricing_Line__r.Knowledge_Dirham_Applicable__c,Pricing_Line__c,Pricing_Line__r.Product__c,Unit_Price__c,Use_List_Price__c from Dated_Pricing__r order by Date_From__c) from Pricing_Line__c where ID IN:setPLIds order by PL_No__c]){
                    if(objPL.Dated_Pricing__r!=null && objPL.Dated_Pricing__r.size()>0){
                        mapPLDatedPricing.put(objPL.id,objPL.Dated_Pricing__r);
                        for(Dated_Pricing__c DP:objPL.Dated_Pricing__r){
                            setDatedPricingIds.add(DP.id);
                        }
                    }
                }
                
                if(setDatedPricingIds!=null && setDatedPricingIds.size()>0){
                    for(Pricing_Range__c PRange:[select id,Dated_Pricing__c,Range_Start__c,Range_End__c,RangeNo__c,Unit_Price__c,Fixed_Price__c from Pricing_Range__c where Dated_Pricing__c IN:setDatedPricingIds order by Range_Start__c]){
                        list<Pricing_Range__c> lstPR = new list<Pricing_Range__c>();
                        if(mapPLPricingRange.get(PRange.Dated_Pricing__c)!=null && mapPLPricingRange.get(PRange.Dated_Pricing__c).size()>0){
                            lstPR = mapPLPricingRange.get(PRange.Dated_Pricing__c);
                            lstPR.add(PRange);
                            mapPLPricingRange.put(PRange.Dated_Pricing__c,lstPR);
                        }else{
                            lstPR.add(PRange);
                            mapPLPricingRange.put(PRange.Dated_Pricing__c,lstPR);
                        }
                    }
                }
            }
            
            if(setTemplDocIds!=null && setTemplDocIds.size()>0){
                //Querying the Conditions which are Related to the TemplateDocs of SRTemplate and where the Object NAME not equals to Step__c
                // because sr doc does not support condition written on step level
                for(Condition__c docCon:[select id,Class_Name__c,Field_Name__c,Object_Name__c,SR_Template_Docs__c from Condition__c where SR_Template_Docs__c IN:setTemplDocIds and Object_Name__c!=:null and Field_Name__c!=:null and Object_Name__c!='Step__c']){
                    list<Condition__c> lstDocConds = new list<Condition__c>();
                    if(mapDocConditions.get(docCon.SR_Template_Docs__c)!=null){
                        lstDocConds = mapDocConditions.get(docCon.SR_Template_Docs__c);
                        lstDocConds.add(docCon);
                        mapDocConditions.put(docCon.SR_Template_Docs__c,lstDocConds);
                    }else{
                        lstDocConds.add(docCon);
                        mapDocConditions.put(docCon.SR_Template_Docs__c,lstDocConds);
                    }
                }
            }
            //Preparing the map with the fields which mentioned in the Pricing Conditions
            if(setPLIds!=null && setPLIds.size()>0){
                ctx.PreparePricingFieldList(setPLIds);
            }
            
            if(setTemplDocIds!=null && setTemplDocIds.size()>0){
                ctx.PrepareDocumentConFields(setTemplDocIds); //initialising the global maps for the fields referred inthe conditions and prepare the step query 
            }
        }
        if(SR_StepIds!=null && SR_StepIds.size()>0){
            for(SR_Steps__c SRStep:[select id,SR_Template__c,Owner__c,(select id,Execute_on_Update__c,SR_STeps__r.Owner__c,Execute_on_Insert__c,SR_Steps__r.Execute_on_Submit__c,SR_Steps__r.OwnerId,SR_Steps__r.Do_not_use_owner__c,Business_Rule_Condition__c,SR_Steps__c,SR_Steps__r.Summary__c,SR_Steps__r.Estimated_Hours__c,SR_Steps__r.Step_Template__c,SR_Steps__r.SR_Template__c,SR_Steps__r.SR_Template__r.ownerId,SR_Steps__r.SR_Template__r.Do_not_use_owner__c,SR_Steps__r.Step_Template__r.ownerId,SR_Steps__r.Step_no__c,SR_Steps__r.Start_Status__c,SR_Steps__r.Start_Status__r.Name,SR_Steps__r.Step_RecordType_API_Name__c,SR_Steps__r.Step_Template__r.Step_RecordType_API_Name__c,Text_Condition__c,Action_Type__c from Business_Rules__r) from SR_Steps__c where ID IN:SR_StepIds]){
                if(SRStep.Business_Rules__r!=null && SRStep.Business_Rules__r.size()>0){
                    for(Business_Rule__c BR:SRStep.Business_Rules__r){
                        if(BR.Action_Type__c=='Default Actions'){
                            setDefaultActBrIds.add(BR.id);
                        }else if(BR.Action_Type__c=='Custom Conditions Actions'){
                            setCustomConditionBrIds.add(BR.id);
                        }
                        setTotalBRIds.add(BR.id);
                    }
                    if(mapSRTempRelatedBRS.get(SRStep.SR_Template__c)!=null && mapSRTempRelatedBRS.get(SRStep.SR_Template__c).size()>0){
                        list<Business_Rule__c> lstBR = mapSRTempRelatedBRS.get(SRStep.SR_Template__c);
                        lstBR.addALL(SRStep.Business_Rules__r);
                        mapSRTempRelatedBRS.put(SRStep.SR_Template__c,lstBR);
                    }else{
                        mapSRTempRelatedBRS.put(SRStep.SR_Template__c,SRStep.Business_Rules__r);
                    }
                }
            }
        }
        
        
        ctx.Prepare_StepQuery(setTotalBRIds,setStepIds);
        
        //recalculate pricing
        for(SR_Price_Item__c eachSRPriceItem : [SELECT id,product__c, ServiceRequest__c, Unique_SR_PriceItem__c,ServiceRequest__r.SR_Template__c,Price__c FROM SR_Price_Item__c WHERE ServiceRequest__c IN :setSRIds]){
          list<SR_Price_Item__c> exstngSRPriceItmsLst = new list<SR_Price_Item__c>();
          exstngSRPriceItmsMap.put(eachSRPriceItem.Unique_SR_PriceItem__c,eachSRPriceItem);
        }
        
        list<SR_Price_Item__c> lstSRPriceItems_To_be_Insert = new list<SR_Price_Item__c>();
        list<Service_Request__c> SRLstforDoc = new list<Service_Request__c>();
        list<step__c> tempListToUpdate = new list<step__c>();
        for(Step__c stp:Trigger.New){
            BRIdSet = new set<id>();
            if(stp.SR__c!=null && mapSR_SRTemplate.get(stp.SR__c)!=null){
                Id SRTemplateID = mapSR_SRTemplate.get(stp.SR__c);
                //returns full record of the step
                //here we excluding the rich text whcih are defined in sr
                Step__c Currentstep = ctx.ExecuteStepcontext(stp);
                SRLstforDoc.add(Currentstep.SR__r);
                if(Currentstep!=null && Currentstep.id!=null && SRTemplateID!=null){
                    list<SR_Price_Item__c> lstSRPriceItems = new list<SR_Price_Item__c>();
                    if(mapSRTemplateItems!=null && mapSRTemplateItems.size()>0)
                    //
                    //pass th step and all pricing maps into SM and it will returnt eh list of lines to be addd after evaluating the conditions
                    //
                    lstSRPriceItems = SM.fetchSRPriceItems(null,Currentstep,mapSRTemplateItems,mapProductPricingLines,mapPLDatedPricing,mapPLPricingRange,MapProductPriceBook);
                    if(lstSRPriceItems!=null && lstSRPriceItems.size()>0){
                        lstSRPriceItems_To_be_Insert.addALL(lstSRPriceItems);
                    }
                    
                    //v1.2
                    if(mapConsumedPriceItems!=null && lstSRPriceItems_To_be_Insert!=null && lstSRPriceItems_To_be_Insert.size()>0){
                        for(integer i=0;i<lstSRPriceItems_To_be_Insert.size();i++){
                            if(lstSRPriceItems_To_be_Insert[i].Pricing_Line__c!=null && mapConsumedPriceItems.get(lstSRPriceItems_To_be_Insert[i].Pricing_Line__c)!=null && lstSRPriceItems_To_be_Insert[i].Product__c!=null && mapConsumedPriceItems.get(lstSRPriceItems_To_be_Insert[i].Pricing_Line__c).Product__c==lstSRPriceItems_To_be_Insert[i].Product__c){
                                lstSRPriceItems_To_be_Insert[i].Status__c = 'Consumed';
                            }
                        }
                    }
                    
                }
                
                if(mapSRTempDocs.get(SRTemplateID)!=null){
                    for(SR_Template_Docs__c SRTmpDoc:mapSRTempDocs.get(SRTemplateID)){
                        if(SRTmpDoc.Evaluated_at__c==stp.SR_Step__c || SRTmpDoc.Original_Sighted_at__c==stp.SR_Step__c){
                            boolean bCriteriaMet = false;
                            if(SRTmpDoc.SR_Template_Docs_Condition__c!=null && SRTmpDoc.SR_Template_Docs_Condition__c.trim()!=''){
                                bCriteriaMet = SM.executeDocumentConditions(SRTmpDoc,null,Currentstep);
                            }else{
                                bCriteriaMet = true;
                            }
                            SR_Doc__c objSRDoc;
                            if(bCriteriaMet!=null && bCriteriaMet && mapExistingSRDocs.get(SRTmpDoc.Id)==null){
                                objSRDoc = new SR_Doc__c();
                                objSRDoc.Name = SRTmpDoc.Document_Master__r.Name;
                                objSRDoc.Service_Request__c = stp.SR__c;
                                objSRDoc.SR_Template_Doc__c = SRTmpDoc.Id;
                                objSRDoc.Document_Master__c = SRTmpDoc.Document_Master__c;
                                objSRDoc.DMS_Document_Index__c = SRTmpDoc.DMS_Document_Index__c;
                                objSRDoc.Step__c = stp.Id;
                                objSRDoc.Status__c = 'Pending Upload';
                                objSRDoc.Group_No__c = SRTmpDoc.Group_No__c;
                                objSRDoc.Is_Not_Required__c = SRTmpDoc.Optional__c;
                                objSRDoc.Generate_Document__c = SRTmpDoc.Generate_Document__c;
                                objSRDoc.Sys_IsGenerated_Doc__c = SRTmpDoc.Generate_Document__c;
                                objSRDoc.Letter_Template__c = SRTmpDoc.Document_Master__r.LetterTemplate__c;
                                objSRDoc.Document_Description_External__c = SRTmpDoc.Document_Description_External__c;
                                if(objSRDoc.Document_Description_External__c==null){
                                    objSRDoc.Document_Description_External__c = SRTmpDoc.Document_Description__c;
                                }
                                if(SRTmpDoc.Courier_collected_at__c!=null || SRTmpDoc.Courier_delivered_at__c!=null){
                                    objSRDoc.Is_Not_Required__c = true;
                                    objSRDoc.Courier_Docs__c = true;
                                }
                                //
                                //for ddp documents "is not required flag" = true 
                                //
                               
                                if(objSRDoc.Generate_Document__c==true){
                                   objSRDoc.Is_Not_Required__c = true;
                                   objSRDoc.Status__c = 'Generated';
                                   //auto number / random for every sr doc
                                   //this is stored and used printed in every doc footer - 
                                   //this is used in force.com in document search screen to verify documents
                                    
                                   string tempString  = string.valueof(Crypto.getRandomInteger());
                                   objSRDoc.Sys_RandomNo__c = tempString.right(4);
                                }
                                
                                if(SRTmpDoc.Requirement__c=='Upload Copy but Original Required'){
                                    // user has to upload the document , it is mandatory
                                    objSRDoc.Is_Not_Required__c = false;
                                    objSRDoc.Requirement__c = 'Copy & Original';
                                }else{
                                  objSRDoc.Requirement__c = SRTmpDoc.Requirement__c;
                                }
                                string Gen_or_Upload = '';
                                //if it is geneted by ddp, then user do not need to upload
                                if(objSRDoc.Generate_Document__c==true){
                                   objSRDoc.Is_Not_Required__c = true;
                                  Gen_or_Upload = 'Generate';
                                }else{
                                  Gen_or_Upload = 'Upload';
                                }
                                objSRDoc.Unique_SR_Doc__c = SRTmpDoc.Document_Master__r.Name+'_'+stp.SR__c+'_'+stp.Id+'_'+Gen_or_Upload;
                                lstSRDocInsert.add(objSRDoc);
                            }
                        }
                    }
                }
                
                if(SRTemplateID!=null && mapSRTempRelatedBRS.get(SRTemplateID)!=null){
                    
                    Id CurrentSRStepId;
                    String srStepOwner = ''; // V2.6 - Added by Claude; variable to store the owner
                    
                    if(mapSRTempRelatedSRSteps.get(SRTemplateID)!=null){
                        for(SR_Steps__c srStp:mapSRTempRelatedSRSteps.get(SRTemplateID)){
                            if(srStp.Step_No__c==stp.Step_No__c){
                                CurrentSRStepId = srStp.id;
                                Cls_StepBusinessHoursUtils.checkOwner(srStp.Owner__c);
                            }
                        }
                    }
                    
                    //
                    //this map (mapSRTempRelatedBRS) contains all bus rules of that template, 
                    //the below condition checks only bus rules tied to the current step 
                    //
                    for(Business_Rule__c eachbr:mapSRTempRelatedBRS.get(SRTemplateID)){
                        if(eachbr.Action_Type__c=='Default Actions' && CurrentSRStepId==eachbr.SR_Steps__c){
                           //
                           //for default action there is no condition to check so just execute it 
                           //
                            try{
                                SM.executeAction(eachbr,Currentstep,mapSRTemplateItems,mapProductPricingLines,mapPLDatedPricing,mapPLPricingRange,MapProductPriceBook,exstngSRPriceItmsMap,null,mapStepQueues,mapStepTemplateQueues,mapSRStepQueues,mapSRTemplateQueues,mapSROwner,mapUserLicense,null,null);
                            }catch(CommonCustomException ex){
                                stp.addError(ex.getMessage());
                            }
                        }else if(eachbr.Action_Type__c=='Custom Conditions Actions' && eachbr.Execute_on_Insert__c==true && CurrentSRStepId==eachbr.SR_Steps__c){
                            //execute each BR and checks whether the Conditions met or not
                            try{
                                BRResultFlag = SM.executeBusinessRules(eachBR,ctx,Currentstep,null,null);
                            }catch(CommonCustomException ex){
                                stp.addError(ex.getMessage());
                            }
                            //
                            //if the condition is met the excecute the action 
                            //
                            if(BRResultFlag==true){
                                try{
                                    SM.executeAction(eachbr,Currentstep,mapSRTemplateItems,mapProductPricingLines,mapPLDatedPricing,mapPLPricingRange,MapProductPriceBook,exstngSRPriceItmsMap,null,mapStepQueues,mapStepTemplateQueues,mapSRStepQueues,mapSRTemplateQueues,mapSROwner,mapUserLicense,null,null);
                                }catch(CommonCustomException ex){
                                    stp.addError(ex.getMessage());
                                }
                            }
                        }
                    }
                }
            }
            //execute actions for all the business rules that are set to true
            
            
        }
        if(mapSRTempDocs_Code!=null && mapSRTempDocs_Code.size() > 0){
            /* commented by durga
            cla_DocumentsThruCode cls = new cla_DocumentsThruCode();
            cls.addDocsThruCode(setSRIds,SRLstforDoc,mapSRTempDMS_Id, mapSRTempDocs_Code, srTemplIds);
            */
       }
        // inserting price items and sr docs
        if(lstSRPriceItems_To_be_Insert!=null && lstSRPriceItems_To_be_Insert.size()>0){
            try{
                Database.SaveResult[] SRPriceItemsInsertResult = Database.insert(lstSRPriceItems_To_be_Insert, false);
            }catch(Exception e){
                trigger.new[0].addError('DB Exception:' + e.getmessage());
            }
        }
        if(lstSRDocInsert!=null && lstSRDocInsert.size()>0){
            try{
                Database.SaveResult[] SRReqDocInsertResult = Database.insert(lstSRDocInsert, false);
            }catch(Exception e){
                trigger.new[0].addError('DB Exception:' + e.getmessage());
            }
        }
        //for updating SR Price Items status to Consumed
        if(updateSRPriceItemLst!=null && updateSRPriceItemLst.size()>0){
            try{
                update updateSRPriceItemLst;
            }catch(Exception e){
                trigger.new[0].addError('DB Exception:' + e.getmessage());
            }
        }
        //
        //if a particular document is set as "Is_Not_Required__c" = true and validated_at_step is the currrent 
        //then we will update that doc to required and mandaotory to be uploaded 
        //
        
        for(SR_Doc__c objSRDoc:[select id,Is_Not_Required__c from SR_Doc__c where Service_Request__c IN:setSRIds and Is_Not_Required__c=true and SR_Template_Doc__c!=null and SR_Template_Doc__r.Validated_at__c!=null and SR_Template_Doc__r.Validated_at__c IN:setSRStepIds]){
          SR_Doc__c RequiredDocs = objSRDoc;
          RequiredDocs.Is_Not_Required__c = false;
          lstDocAddedThroughCode.add(RequiredDocs);
        }
        if(lstDocAddedThroughCode!=null && lstDocAddedThroughCode.size()>0){
          try{
            update lstDocAddedThroughCode;
          }catch(Exception e){
            
          }
        }
        
    }
    /* End of Insert Trigger logic to be Executed */
    if(Trigger.IsUpdate && Trigger.isBefore){
        
        map<string,string> mapStepNotesStp = new map<String,string>();
        map<string,string> mapStepStatus = new map<String,string>();
        map<string,string> mapCustomerPendings = new map<String,string>();
        map<string,string> existingReuploadDocs = new map<String,string>();
        //added by Ravi on 15th June,2015 for Exit Process
        //map<string,string> mapEPClosure = new map<String,string>(); // Exist Process Closure 
        
        //added by Swati
        // boolean isFOQuery = false; // V3.0 - Claude - Replaced by general SOQL 
        
        // list<Status__c> notFixedStatus = new list<Status__c>(); //// v3.0 - Replaced with general list to avoid SOQL limit
        
        Map<String,String> stepStatusMap = new Map<String,String>(); // V3.0 - Claude - New Map for step statuses
        Map<String,String> srStatusMap = new Map<String,String>();   // V3.0 - Claude - New Map for SR statuses
        
        List<Service_Request__c> srsToUpdate = new List<Service_Request__c>(); // V3.0 - Claude - List to store the SRs to be updated
        
        //Set<String> stepStatusCodes = new Set<String>{'NOT FIXED'};                      // V3.0 - List of step status codes to be used for the Trigger
        //Set<String> srStatusCodes = new Set<String>{'Snags Not Rectified','Site Instruction Not Completed'}; // V3.0 - List of SR status codes to be used for the Trigger
        
        Set<String> stepStatusCodes = new Set<String>(); // V3.0 - List of step status codes to be used for the Trigger
        Set<String> srStatusCodes = new Set<String>();   // V3.0 - List of SR status codes to be used for the Trigger
        
        /* This will prevent the queries from firing on other types of SR */
        for(Step__c objStp : trigger.new){
          
          if(objStp.SR_Type_Step__c == 'Fit - Out Service Request' || 
               objStp.SR_Type_Step__c == 'Event Service Request' || 
               objStp.SR_Type_Step__c == 'Project Cancellation' || 
               objStp.SR_Type_Step__c == 'Site Visit Report for Fit Out' || // V3.0 - Claude - Added SITE VISIT TYPES  
               objStp.SR_Type_Step__c == 'Site Visit Report for Events'    // V3.0 - Claude - Added SITE VISIT TYPES
               ){ 
                 
                 if(String.isNotBlank(objStp.Exceptional_Remarks__c) && String.isNotBlank(objStp.SR_Type_Step__c)){
                 
                   if(objStp.Exceptional_Remarks__c.equals('Snag Fix Pending') || 
                            objStp.Exceptional_Remarks__c.equals('Not Fixed')){
            stepStatusCodes.add('NOT FIXED');
          }
                   
          if(objStp.Exceptional_Remarks__c.equals('Snag Fix Pending') && 
                  srStatusMap.containsKey('Snags Not Rectified')){
                srStatusCodes.add('Snags Not Rectified');
              }
                
              if(objStp.Exceptional_Remarks__c.equals('Not Fixed') && 
                (objStp.SR_Type_Step__c == 'Site Visit Report for Fit Out' || // V3.0 - Claude - Added SITE VISIT TYPES  
                 objStp.SR_Type_Step__c == 'Site Visit Report for Events') &&
                  srStatusMap.containsKey('Site Instruction Not Completed') ){
                srStatusCodes.add('Site Instruction Not Completed');
                }
                   
                 }
              
            }
            
            // v3.2
            if(((objStp.ID_Type__c != null || objStp.ID_Type__c != '') && objStp.ID_Type__c == 'Emirates ID' && system.label.checkEIDDup=='true' && objStp.No_PR__c == false &&  objStp.SAP_IDNUM__c!= System.trigger.oldMap.get(objStp.id).SAP_IDNUM__c )){
            
               if(objStp.SAP_IDNUM__c != null ){
                    for(Document_Details__c objDD : [select Id from Document_Details__c where Document_Number__c=:objStp.SAP_IDNUM__c AND Document_Type__c=:objStp.ID_Type__c AND Contact__c!=null]){
                        objStp.addError( 'Provided '+objStp.ID_Type__c+' number is already exist in the system.');                        
                    }
                }else{
                    objStp.addError('Please enter the ID Number before closing with the Step');
                   
                }
            
            }
               
            // v3.2  
          
        }
        
        
        /* V3.0 - Claude - Query the statuses */
        if(!stepStatusCodes.isEmpty()){
        
          for( Status__c stepStatus : [SELECT Id, Code__c FROM Status__c WHERE Code__c IN :stepStatusCodes] ){
            stepStatusMap.put(stepStatus.Code__c,stepStatus.Id);
          }
            
        }
        
        if(!srStatusCodes.isEmpty()){
        
          for( SR_Status__c srStatus : [SELECT Id, Name FROM SR_Status__c WHERE Name IN :srStatusCodes] ){
            srStatusMap.put(srStatus.Name,srStatus.Id);
          }
            
        }
        
        list<User> tempUserList = new list<User>();
        
        for(Step__c objStp:trigger.new){
          
            /* V3.0 - Claude - Replaced by general SOQL 
            if(objStp.SR_Type_Step__c == 'Fit - Out Service Request' && objStp.comments__c!=null && objStp.comments__c=='Snag Fix Pending'){
                isFOQuery = true;
            }*/
            
            if(String.isNotBlank(objStp.Sys_SR_Group__c) && objStp.Sys_SR_Group__c == 'Fit-Out & Events' && String.isNotBlank(objStp.Step_Name__c) && objStp.Step_Name__c.equals('Notify FOSP about DCD and DEWA inspection dates')){ // V2.9 - Claude - Changed from 'IDAMA' to 'FOSP' <= All V2.9 changes apply
              CC_cls_FitOutandEventCustomCode.checkDewaDcdInspectionDates(objStp); // V2.1 - Claude - Added validation for DEWA and DCD inspection dates  
            }
            
        }
        
        /* V3.0 - Claude - Replaced by general SOQL 
        if(isFOQuery == true){
            notFixedStatus = [select id from Status__c where Code__c =: 'NOT FIXED' and Type__c=:'End'];
        }*/
        
        tempUserList = [select id,contactid,Community_User_Role__c,isactive,contact.Role__c from User where id=:userinfo.getUserId() and isactive=:true limit 1];
        //End
        
        for(Step__c objStp:trigger.new){
            /* Commented by Ravi on 12th April, 2015 as per Alya's Comments - Steps Notes checkbox should not be there
            if(objStp.SR_Step__c!=null && (objStp.Step_Note_Added__c==true || objStp.Notes_Updated__c==true) && objStp.Status_Code__c=='PENDING_CUSTOMER_INPUTS' && objStp.Step_Notes__c!=null && objStp.Step_Notes__c!=''){
                mapStepNotesStp.put(objStp.Id,objStp.SR_Step__c);
                mapStepStatus.put(objStp.Id,objStp.Status__c);
            }*/
            if(objStp.SR_Step__c!=null && objStp.Status_Code__c=='PENDING_CUSTOMER_INPUTS' && objStp.Step_Notes__c!=null && objStp.Step_Notes__c!='' && objStp.Customer_Comments__c != null && objStp.Customer_Comments__c != ''){
                mapStepNotesStp.put(objStp.Id,objStp.SR_Step__c);
                mapStepStatus.put(objStp.Id,objStp.Status__c);
            }else if(objStp.Pending__c != null && objStp.Status_Code__c=='PENDING_CUSTOMER_INPUTS' && objStp.Customer_Comments__c != null && objStp.Customer_Comments__c != '' && objStp.Step_Notes__c != null && objStp.Step_Notes__c != ''){ //added by Ravi on 15th April to close the Customer Step for Pending
                mapCustomerPendings.put(objStp.SR__c,objStp.Pending__c);
            }else if(objStp.Step_Name__c == 'Company Closure Information' && objStp.Status_Code__c=='PENDING_CUSTOMER_INPUTS' && objStp.First_Date_of_Publication__c != null && trigger.oldMap.get(objStp.Id).First_Date_of_Publication__c != objStp.First_Date_of_Publication__c){
                //added by Ravi on 15th June,2015 for Exit Process
                //mapEPClosure.put(objStp.Id,objStp.Id);
                mapStepNotesStp.put(objStp.Id,objStp.SR_Step__c);
                mapStepStatus.put(objStp.Id,objStp.Status__c);
            }
            else if(objStp.SR_Step__c!=null && objStp.Status_Code__c=='APPROVED' && objStp.Sys_SR_Group__c=='Fit-Out & Events'){
                existingReuploadDocs.put(objStp.SR__c,objStp.Id);
            }
            if(objStp.SR_Type_Step__c == 'Fit - Out Service Request' || 
               objStp.SR_Type_Step__c == 'Event Service Request' || 
               objStp.SR_Type_Step__c == 'Project Cancellation' || // V2.2 - Claude - Added SR Types 'Event Service Request' and 'Project Cancellation'
               objStp.SR_Type_Step__c == 'Site Visit Report for Fit Out' || // V3.0 - Claude - Added SITE VISIT TYPES  
               objStp.SR_Type_Step__c == 'Site Visit Report for Events'    // V3.0 - Claude - Added SITE VISIT TYPES
               ){ 
                //added by Swati
                if(!stepStatusMap.isEmpty()){
                  
                  // v3.0 - Claude - Start
                  /* Logic used for Exceptional Remarks field */
                  if(String.isNotBlank(objStp.Exceptional_Remarks__c)){ 
                        
                        if(!stepStatusMap.isEmpty()){
                          
                          if(stepStatusMap.containsKey('NOT FIXED') && 
                            (objStp.Exceptional_Remarks__c.equals('Snag Fix Pending') || 
                            objStp.Exceptional_Remarks__c.equals('Not Fixed'))){
                            
                            objStp.status__c = stepStatusMap.get('NOT FIXED');  
                            
                            // V3.0 - Claude - Start
                    /* Get the Service Request IDs */
                    if(String.isNotBlank(objStp.SR__c) ){
                      
                      Service_Request__c relatedSr = new Service_Request__c(Id = objStp.SR__c);
                      
                      if(objStp.Exceptional_Remarks__c.equals('Snag Fix Pending') && 
                        srStatusMap.containsKey('Snags Not Rectified')){
                        
                        //V1.9 - Start - Claude - Added logic to update related Service Request STatus 
                        relatedSr.External_SR_Status__c = srStatusMap.get('Snags Not Rectified');
                    relatedSr.Internal_SR_Status__c = srStatusMap.get('Snags Not Rectified');
                    //V1.9 - End  
                      }
                      
                      if(objStp.Exceptional_Remarks__c.equals('Not Fixed') && 
                        (objStp.SR_Type_Step__c == 'Site Visit Report for Fit Out' || // V3.0 - Claude - Added SITE VISIT TYPES  
                             objStp.SR_Type_Step__c == 'Site Visit Report for Events') &&
                        srStatusMap.containsKey('Site Instruction Not Completed') ){
                        
                        relatedSr.External_SR_Status__c = srStatusMap.get('Site Instruction Not Completed');
                    relatedSr.Internal_SR_Status__c = srStatusMap.get('Site Instruction Not Completed');
                      }
                      
                      Boolean isFound = false;
                      
                      if(!srsToUpdate.isEmpty()){
                        
                        for(Service_Request__c sr : srsToUpdate){
                          
                          if(sr.Id.equals(objStp.SR__c)){
                            isFound = true;
                            break;
                          }
                          
                        }
                      }
                      
                      if(!isFound){
                        srsToUpdate.add(relatedSr);  
                      }
                      
                      
                    }  
                      
                          }
                          
                                               
                        }
                        
                    }
                  // v3.0 - Claude - End
                  
                    //if(objStp.comments__c!=null && objStp.comments__c=='Snag Fix Pending'){ <-- V2.0 Commented - replaced comments__c with Exceptional_Remarks__c per Ticket #2847
                    /* V3.0  - Claude - Replaced by revised logic
                    if(objStp.Exceptional_Remarks__c!=null && objStp.Exceptional_Remarks__c=='Snag Fix Pending'){ 
                        if(!notFixedStatus.isEmpty()){
                            objStp.status__c = notFixedStatus[0].id;
                            
                            //V1.9 - Start - Claude - Added logic to update related Service Request STatus 
              Service_Request__c relatedSr = [SELECT External_SR_Status__c, Internal_SR_Status__c FROM Service_Request__c WHERE Id =:objStp.SR__c]; // Query the related SR
              Id snagNotFixedSTatus = [SELECT id FROM SR_Status__c WHERE Name = 'Snags Not Rectified'].Id;
              relatedSr.External_SR_Status__c = snagNotFixedSTatus;
              relatedSr.Internal_SR_Status__c = snagNotFixedSTatus;
              update relatedSr;
              //V1.9 - End                            
                        }
                    }*/
                }
                
                if(objStp.Step_Name__c == 'Require More Information From User' || 
                   objStp.Step_Name__c == 'Re-upload Document' ||
                   objStp.Step_Name__c == 'Upload Detailed Design Documents' ||
                   objStp.Step_Name__c == 'Mobilization and setting up of Protection/Hoarding' ||
                   objStp.Step_Name__c == 'Upload Approved Drawings' ||
                   objStp.Step_Name__c == 'Site Works' ||
                   objStp.Step_Name__c == 'Reinstatement Works' ||
                   objStp.Step_Name__c == 'Fit - Out Works' ||
                   objStp.Step_Name__c == 'Client Approval For First Fix Inspection' ||
                   objStp.Step_Name__c == 'Partial Completion of Fit - Out Works' ||
                   objStp.Step_Name__c == 'Notify FOSP about DCD and DEWA inspection dates' || // V2.9
                   objStp.Step_Name__c == 'Request for final inspection' ||
                   objStp.Step_Name__c == 'Client Approval For Final Inspection' ||
                   objStp.Step_Name__c == 'DCD certificate & DEWA inspection approval submission' ||
                   objStp.Step_Name__c == 'Completion of Minor Snags' ){
                    //added by Swati
                    if(objstp.Status__c != trigger.oldMap.get(objstp.Id).Status__c){
                        if(!tempUserList.isEmpty()){
                            if(tempUserList[0].Community_User_Role__c!=null && 
                               tempUserList[0].Community_User_Role__c.contains('Company Services') && 
                               tempUserList[0].contact!=null && tempUserList[0].contact.Role__c != 'Fit-Out Services' && 
                               objStp.Assigned_to_client__c==false && objStp.Owner__c=='Contractor'){
                                objStp.addError('Only Contractor can work on this step.');
                            }
                            if(tempUserList[0].contact!=null && tempUserList[0].contact.Role__c == 'Fit-Out Services' && 
                               objStp.Assigned_to_client__c==true && objStp.Owner__c=='Client'){
                                objStp.addError('Only Client can work on this step.');
                            }
                        }
                    }
                }
            }
            //End
        }
        
    // V3.0 - Claude - Start
    if(!srsToUpdate.isEmpty()){
      update srsToUpdate;
    }
    // V3.0 - Claude  - End
    
    map<string,Step_Transition__c> mapTransition = new map<string,Step_Transition__c>();
        if(mapStepNotesStp!=null && mapStepNotesStp.size()>0){
            for(Step_Transition__c stpTrans:[select SR_Status_Internal__c,SR_Step__c,SR_Status_External__c,Transition__r.To__c,Transition__r.To__r.Type__c from Step_Transition__c where SR_Step__c IN:mapStepNotesStp.values() and Transition__r.From__c IN:mapStepStatus.values()]){
                mapTransition.put(stpTrans.SR_Step__c,stpTrans);
            }
        }
        //added by Ravi on 15th April to close the Customer Step for Pending
        string StatusId;
        
        //added by swati to not allow step to approved if documents are pending
        if(!existingReuploadDocs.isEmpty()){
            for(SR_Doc__c objSRDoc : [select Id,Status__c,Service_Request__c from SR_Doc__c where Service_Request__c IN : existingReuploadDocs.keySet() AND (Status__c = 'Re-upload') AND Is_Not_Required__c = false ]){
                trigger.new[0].addError('Some documents needs to be reuploaded, step cannot be approved.');
            }   
        }
        if(!mapCustomerPendings.isEmpty()){
            //added the code on 26th April, 2015 to check for any pending doc
            for(SR_Doc__c objSRDoc : [select Id,Status__c,Service_Request__c from SR_Doc__c where Service_Request__c IN : mapCustomerPendings.keySet() AND (Status__c = 'Re-upload' OR Status__c = 'Pending Upload') AND Is_Not_Required__c = false ]){
                trigger.new[0].addError('To upload the required document, please go to the Service Request and click on \'Download/Upload Doc\'.');
            }
            for(Status__c obj : [select Id from Status__c where Code__c='REQUIRED_INFO_UPDATED']){
                StatusId = obj.Id;
            }
            if(StatusId != null){
                for(Step__c objStp:trigger.new){
                    if(objStp.Pending__c != null && objStp.Status_Code__c=='PENDING_CUSTOMER_INPUTS' && objStp.Customer_Comments__c != ''){
                        objStp.Status__c = StatusId;
                        objstp.Closed_Date__c = system.today();
                        objstp.Closed_Date_Time__c = system.now();
                    }
                }
            }
        }
        list<Service_Request__c> lstSRStatusUpdate = new list<Service_Request__c>();
    
    
       
    
    
        
        for(Step__c objStp:trigger.new){
            if(objStp.Sys_SR_Group__c=='Fit-Out & Events' && objStp.Comments__c!=null){
                //objStp.Customer_Comments__c = objStp.Comments__c; // v2.8 - Commented by Claude - Removed for Fit-out to prevent maximum length error 
            }
            if(mapStepNotesStp.get(objStp.Id)!=null && objStp.SR_Step__c!=null && mapTransition.get(objStp.SR_Step__c)!=null){
                objStp.Status__c = mapTransition.get(objStp.SR_Step__c).Transition__r.To__c;
                
                if(mapTransition.get(objStp.SR_Step__c).SR_Status_Internal__c!=null || mapTransition.get(objStp.SR_Step__c).SR_Status_External__c!=null){
                    Service_Request__c objSR = new Service_Request__c(Id=objStp.SR__c);
                    if(mapTransition.get(objStp.SR_Step__c).SR_Status_Internal__c!=null)
                        objSR.Internal_SR_Status__c = mapTransition.get(objStp.SR_Step__c).SR_Status_Internal__c;
                    if(mapTransition.get(objStp.SR_Step__c).SR_Status_External__c!=null)
                        objSR.External_SR_Status__c = mapTransition.get(objStp.SR_Step__c).SR_Status_External__c;
                    lstSRStatusUpdate.add(objSR);
                }
            }
            
            //added by Saima to populate the Step Close Date on 06/04/2015
            //V2.4 - Claude - Start
            /*if(objstp.Is_Closed__c==true && trigger.oldMap.get(objstp.Id).Is_Closed__c!=true && objstp.Status_Type__c!=null){
                objstp.Closed_Date__c = system.today();
                objstp.Closed_Date_Time__c = system.now();
            }else if(objstp.Status__c!=null && objstp.Status__c != trigger.oldMap.get(objstp.Id).Status__c && trigger.oldMap.get(objstp.Id).Status__c!= null){
                objstp.Closed_Date__c = system.today();
                objstp.Closed_Date_Time__c = system.now();
            }*/
            
            if((objstp.Is_Closed__c==true && trigger.oldMap.get(objstp.Id).Is_Closed__c!=true && objstp.Status_Type__c!=null) ||
              (objstp.Status__c!=null && objstp.Status__c != trigger.oldMap.get(objstp.Id).Status__c && trigger.oldMap.get(objstp.Id).Status__c!= null)){
              objstp.Closed_Date__c = system.today();
                objstp.Closed_Date_Time__c = system.now();
                
                //V2.4 - Added Condtion for Fit-out and Events
                /*
                 * NOTE:
                 * Add other conditions if you want to use other business
                 * hours IDs
                 */
                if(String.isNotBlank(objStp.SR_Group__c) && objStp.SR_Group__c.equals('Fit-Out & Events')){
                  foSteps.add(objStp);
                }
                //V2.4 - Claude - End 
        
            }
            /*
            if(objstp.Is_Closed__c==true && trigger.oldMap.get(objstp.Id).Is_Closed__c!=true && mapTransition.containsKey(objStp.SR_Step__c) && mapTransition.get(objStp.SR_Step__c).Transition__r.To__r.Type__c == 'End'){
                objstp.Closed_Date__c = system.today();
                objstp.Closed_Date_Time__c = system.now();
            }else if(objstp.Is_Closed__c==true && trigger.oldMap.get(objstp.Id).Is_Closed__c!=true && objstp.Status_Type__c=='End'){
                objstp.Closed_Date__c = system.today();
                objstp.Closed_Date_Time__c = system.now();
            }
            */
        }
        
        try{
          
          /* v2.7 - Claude - This will handle the step duration */ 
          if(!foSteps.isEmpty()){
            Cls_StepBusinessHoursUtils.calculateStepDuration(foSteps); // V2.6 - Claude - Bulkified method for getting step duration  
          }
          
            if(lstSRStatusUpdate!=null && lstSRStatusUpdate.size()>0){
              update lstSRStatusUpdate;
            }
      
      
            
        }catch(Exception e){
            string DMLError = e.getdmlMessage(0)+'';
            if(DMLError==null)
                DMLError = e.getMessage()+'';
            trigger.new[0].addError(DMLError);
        }
        mapStepNotesStp = new map<String,string>();
        mapStepStatus = new map<String,string>();
    }
    
    /* Start Code which is to be Executed at Step Updates */
    if(Trigger.IsUpdate && Trigger.isAfter){
        
        set<id> setStepStatusChangeIds = new set<id>();
        set<id> setOriginalVerifiedDocSRIds = new set<id>();
        set<id> setSRStepIds = new set<id>();
        map<id,id> map_SRStep_StepId = new map<id,id>();
        map<id,list<SR_Doc__c>> mapOriginalVerified_SRDocs = new map<id,list<SR_Doc__c>>();
        
        //added by Ravi on 15th June,2015 for Exit Process
        map<Id,Service_Request__c> mapEPSRs = new map<Id,Service_Request__c>();
        Service_Request__c objEPSR;
        
        for(Step__c stp:Trigger.New){
        
            if(stp.Status__c!=null && stp.Status__c!=trigger.oldMap.get(stp.id).Status__c){
                setStepStatusChangeIds.add(stp.id);
                if(stp.SR__c!=null){
                    // all the documents to be verfied at this step are to be verified by the counter user as per a flag variable set up sr template doc
                    // this is set is used for validating that   
                  setOriginalVerifiedDocSRIds.add(stp.SR__c);
                }
                if(stp.SR_Step__c!=null){
                  setSRStepIds.add(stp.SR_Step__c);
                  map_SRStep_StepId.put(stp.SR_Step__c,stp.Id);
                }
                
                //added by Ravi on 15th June,2015 for Exit Process
                if(stp.Step_Name__c == 'Company Closure Information' && stp.First_Date_of_Publication__c != null){
                    objEPSR = new Service_Request__c(Id=stp.SR__c);
                    objEPSR.First_Date_of_Publication__c = stp.First_Date_of_Publication__c;
                    objEPSR.Second_Date_of_Publication__c = stp.Second_Date_of_Publication__c;
                    objEPSR.Third_Date_of_Publication__c = stp.Third_Date_of_Publication__c;
                    objEPSR.Name_of_the_Publication__c = stp.Name_of_the_Publication__c;
                    objEPSR.DFSA_License_Withdrawal_Date__c = stp.DFSA_License_Withdrawal_Date__c;
                    objEPSR.Final_General_Meeting_Date__c = stp.Final_General_Meeting_Date__c;
                    objEPSR.Certificate_of_Continuation_issue_Date__c = stp.Certificate_of_Continuation_issue_Date__c;
                    mapEPSRs.put(objEPSR.Id,objEPSR);
                }
            }
        }
        if(!mapEPSRs.isEmpty())
            update mapEPSRs.values();
        
        /* Start of Step Status Change Execution Trigger*/ 
        if(setStepStatusChangeIds!=null && setStepStatusChangeIds.size()>0){
          
          /* Start of code for Preparing the list of SRDocs whose Original Verification has to be done */
            for(SR_Doc__c objSrDoc:[select id,Original_Verified__c,SR_Template_Doc__r.Original_Sighted_at__c from SR_Doc__c where Service_Request__c IN:setOriginalVerifiedDocSRIds and SR_Template_Doc__r.Original_Sighted_at__c IN:setSRStepIds]){
              if(objSrDoc.SR_Template_Doc__c!=null && objSrDoc.SR_Template_Doc__r.Original_Sighted_at__c!=null){
                list<SR_Doc__c> lstSRDoc = new list<SR_Doc__c>();
                if(mapOriginalVerified_SRDocs.get(objSrDoc.SR_Template_Doc__r.Original_Sighted_at__c)!=null){
                  lstSRDoc = mapOriginalVerified_SRDocs.get(objSrDoc.SR_Template_Doc__r.Original_Sighted_at__c);
                  lstSRDoc.add(objSrDoc);
                  mapOriginalVerified_SRDocs.put(objSrDoc.SR_Template_Doc__r.Original_Sighted_at__c,lstSRDoc);
                }else{
                  lstSRDoc.add(objSrDoc);
                  // map of sr step ids and list of documents tobe verified 
                  mapOriginalVerified_SRDocs.put(objSrDoc.SR_Template_Doc__r.Original_Sighted_at__c,lstSRDoc);
                }
              }
            }
            /* End of code for Preparing the list of SRDocs whose Original Verification has to be done */
            
            map<string,Id> mapStepRecType = new map<string,Id>();//Key Step RecordType API Name and Value as RecordType Id
            for(RecordType RT:[select id,Name,DeveloperName,sObjectType from RecordType where sObjectType='Step__c']){
                mapStepRecType.put(RT.DeveloperName,RT.id);
            }
            
            exstngSRPriceItmsMap = new map<string,SR_Price_Item__c>();
            
            setStepIds = new set<id>();//Set to store the Step Ids which are to be sent for BusinessRules Execution.
            setStep_TemplId = new set<id>();
            map<id,list<Step__c>> mapSRExistingSteps = new map<id,list<Step__c>>();
            set<id> setSRIds = new set<id>();
            set<id> srTemplIds = new set<id>();
            set<id> setSRIdsforPLQuery = new set<id>();
            set<id> setAccountId = new set<id>();
            set<string> setSRAccountId = new set<string>();
            
            map<id,list<Service_Request__c>> mapSRTemplateSR = new map<id,list<Service_Request__c>>();// key as SRTemplate-Id and value as List<Service_Request__c> for Each SRTemplate.
            map<id,list<SR_Template_Item__c>> mapSRTemplateItems = new map<id,list<SR_Template_Item__c>>();// key as SRTemplate Id and value as List<SR_Template_Item__c> for Each SRTemplate.
            // when user select recalculate pricing the we need all this map 
            map<id,list<Pricing_Line__c>> mapProductPricingLines = new map<id,list<Pricing_Line__c>>();// key as Product Id and value as List<Pricing_Line__c> for Each Product.
            map<id,list<Dated_Pricing__c>> mapPLDatedPricing = new map<id,list<Dated_Pricing__c>>();// key as Pricing_Line__c Id and value as List<Dated_Pricing__c> for Each Pricing Line.
            map<id,list<Pricing_Range__c>> mapPLPricingRange = new map<id,list<Pricing_Range__c>>();// key as DatedPricing Id and value as List<Pricing_Range__c> for Each DatedPrice.
            map<id,PricebookEntry> MapProductPriceBook = new map<id,PricebookEntry>();// key as Product Id and value as PriceBookEntry for Each Product.
            map<id,list<Step__c>> mapSRSteps = new map<id,list<Step__c>>();//key as SR and list of its steps
            String queueName;
            Boolean checkCustomerQueue;
            Id SROwnerId=null;
                
            for(QueueSobject objQueue : [select Id,SobjectType,QueueId,Queue.Email,Queue.Name from QueueSobject WHERE SobjectType = 'Service_Request__c' OR  SobjectType = 'SR_Steps__c' OR SobjectType = 'Step__c' OR SobjectType = 'SR_Template__c']){
                if(objQueue.SobjectType=='Step__c'){
                    mapStepQueues.put(objQueue.Queue.Name,objQueue.QueueId);
                }
                if(objQueue.SobjectType=='SR_Steps__c'){
                    mapSRStepQueues.put(objQueue.QueueId,objQueue.Queue.Name);
                }
                if(objQueue.SobjectType=='Step_Template__c'){
                    mapStepTemplateQueues.put(objQueue.QueueId,objQueue.Queue.Name);
                }
                if(objQueue.SobjectType=='SR_Template__c'){
                    mapSRTemplateQueues.put(objQueue.QueueId,objQueue.Queue.Name);
                }
            }
            for(Step__c stp:Trigger.New){
                if(stp.Status__c!=null && stp.Status__c!=trigger.oldMap.get(stp.id).Status__c){
                  // check if the currenst step is part of :original to be verfied step"
                  //if yes then check if thre is any doc with Original_Verified__c = false
                  // then thro error
                  if(stp.SR_Step__c!=null && mapOriginalVerified_SRDocs.get(stp.SR_Step__c)!=null ){
                    list<SR_Doc__c> lstSRDoc_to_be_Verified = mapOriginalVerified_SRDocs.get(stp.SR_Step__c);
                    if(lstSRDoc_to_be_Verified!=null && lstSRDoc_to_be_Verified.size()>0){
                      for(SR_Doc__c objSRDoc:lstSRDoc_to_be_Verified){
                        if(objSRDoc.Original_Verified__c==false){
                          stp.addError('Please verify the original documents to proceed.');
                        }
                      }
                    }
                  }
                  
                    if(stp.Step_Template__c!=null){
                        setStepIds.add(stp.id);
                        setStep_TemplId.add(stp.Step_Template__c);
                    }
                    if(stp.SR__c!=null){
                        setSRIds.add(stp.SR__c);
                        setStepIds.add(stp.id);
                        setSROwnerId.add(id.valueOf(stp.SROwnerId__c));
                        mapSROwner.put(stp.SR__c,id.valueOf(stp.SROwnerId__c));
                        //setSRAccountId.add(stp.Customer_Name__c);
                        if(stp.Customer_Name__c!=null){
                            string cstmr = stp.Customer_Name__c.subString(0,15);
                            setSRAccountId.add(cstmr);
                        }
                    }
                }
            }
            
            for(User eachUser : [Select Id, Name, Profile.UserLicense.Name,Profile.UserLicense.LicenseDefinitionKey From User Where (Profile.UserLicense.LicenseDefinitionKey = 'PID_Partner_Community' OR Profile.UserLicense.LicenseDefinitionKey = 'PID_Partner_Community_Login' OR Profile.UserLicense.LicenseDefinitionKey = 'PID_Customer_Community' OR Profile.UserLicense.LicenseDefinitionKey = 'PID_Customer_Community_Login') AND Id in :setSROwnerId ]){
               mapUserLicense.put(eachUser.id,eachUser.Profile.UserLicense.LicenseDefinitionKey);
            }
        
            //recalculate pricing
            for(SR_Price_Item__c eachSRPriceItem : [SELECT id,product__c,Unique_SR_PriceItem__c, ServiceRequest__c, ServiceRequest__r.SR_Template__c,Price__c FROM SR_Price_Item__c WHERE ServiceRequest__c IN :setSRIds]){
                list<SR_Price_Item__c>exstngSRPriceItmsLst = new list<SR_Price_Item__c>();
                exstngSRPriceItmsMap.put(eachSRPriceItem.Unique_SR_PriceItem__c,eachSRPriceItem);
            }
            
            /*
            //method returns if any unclosed steps for SR
            mapSRSteps = SM.getSRSteps(setSRIds);
            if(mapSRTemplateSR!=null && mapSRTemplateSR.size()>0){
              for(SR_Template_Item__c SRTemplItem:[select id,Product__c,SR_Template__c from SR_Template_Item__c where SR_Template__c IN:mapSRTemplateSR.keyset() and Product__c!=null]){
                list<SR_Template_Item__c> lstSRTMPItems = new list<SR_Template_Item__c>();
                if(mapSRTemplateItems.get(SRTemplItem.SR_Template__c)!=null && mapSRTemplateItems.get(SRTemplItem.SR_Template__c).size()>0){
                    lstSRTMPItems = mapSRTemplateItems.get(SRTemplItem.SR_Template__c);
                    lstSRTMPItems.add(SRTemplItem);
                    mapSRTemplateItems.put(SRTemplItem.SR_Template__c,lstSRTMPItems);
                }else{
                    lstSRTMPItems.add(SRTemplItem);
                    mapSRTemplateItems.put(SRTemplItem.SR_Template__c,lstSRTMPItems);
                }
                setProdId.add(SRTemplItem.Product__c);
              }
              if(setProdId!=null && setProdId.size()>0){
                    for(PricebookEntry PBE:[Select Product2Id,Pricebook2Id,Product2.Name,UnitPrice From PricebookEntry where Product2Id IN:setProdId and IsActive=true]){
                        MapProductPriceBook.put(PBE.Product2Id,PBE);
                    }
                    for(Product2 objProd:[select id,Name,(select id,Name,Conditions__c,PL_No__c,Price_Line_Condition__c,Priority__c,Product__c,Use_activities_as_license__c from Pricing_Lines__r) from Product2 where ID IN:setProdId]){
                        if(objProd.Pricing_Lines__r!=null){
                            mapProductPricingLines.put(objProd.id,objProd.Pricing_Lines__r);
                            for(Pricing_Line__c PL:objProd.Pricing_Lines__r){
                                setPLIds.add(PL.id);
                            }
                        }
                    }
                    set<id> setDatedPricingIds = new set<id>();
                    for(Pricing_Line__c objPL:[select id,(select id,Name,Appllication_Type__c,Date_From__c,Date_To__c,Pricing_Line__c,Pricing_Line__r.Product__c,Unit_Price__c,Use_List_Price__c from Dated_Pricing__r order by Date_From__c) from Pricing_Line__c where ID IN:setPLIds order by PL_No__c]){
                        system.debug('objPL==>'+objPL);
                        if(objPL.Dated_Pricing__r!=null && objPL.Dated_Pricing__r.size()>0){
                            mapPLDatedPricing.put(objPL.id,objPL.Dated_Pricing__r);
                            for(Dated_Pricing__c DP:objPL.Dated_Pricing__r){
                                setDatedPricingIds.add(DP.id);
                            }
                        }
                    }
                    
                    if(setDatedPricingIds!=null && setDatedPricingIds.size()>0){
                        for(Pricing_Range__c PRange:[select id,Dated_Pricing__c,Range_Start__c,Range_End__c,RangeNo__c,Unit_Price__c,Fixed_Price__c from Pricing_Range__c where Dated_Pricing__c IN:setDatedPricingIds order by Range_Start__c]){
                            list<Pricing_Range__c> lstPR = new list<Pricing_Range__c>();
                            if(mapPLPricingRange.get(PRange.Dated_Pricing__c)!=null && mapPLPricingRange.get(PRange.Dated_Pricing__c).size()>0){
                                lstPR = mapPLPricingRange.get(PRange.Dated_Pricing__c);
                                lstPR.add(PRange);
                                mapPLPricingRange.put(PRange.Dated_Pricing__c,lstPR);
                            }else{
                                lstPR.add(PRange);
                                mapPLPricingRange.put(PRange.Dated_Pricing__c,lstPR);
                            }
                        }
                    }
                }
          }
          */
          
          
          if(setPLIds!=null && setPLIds.size()>0 && setSRIdsforPLQuery!=null && setSRIdsforPLQuery.size()>0){
                ctx.PreparePricingFieldList(setPLIds);//global maps will be initialised which holds the fields mentioned inthe conditions and include in the query 
          }
            /* End of Code for Preparing Maps for Pricing related Functionality */
            list<SR_Price_Item__c> lstSRPriceItemsInsert = new list<SR_Price_Item__c>();
            if(setSRIds!=null && setSRIds.size()>0){
                for(Step__c stp:[select id,SR__c,SR_Step__r.Owner__c,step_no__c,Step_Status__c,Status_Type__c,Status__c,Sys_Step_Loop_No__c from Step__c where SR__c IN:setSRIds order by lastModifiedDate]){//Status_Name__c//
                    list<Step__c> lstStp = new list<Step__c>();
                    if(mapSRExistingSteps.get(stp.SR__c)!=null){
                        lstStp = mapSRExistingSteps.get(stp.SR__c); 
                        // 1.for the loop task , we need to maintain the uniqueness, and find no of times the step is inserted, we need to prepare all the steps in this map
                        // 2. for task transition, to checkc whether the existing step or previous step status is meeeting the create condition of a particular step in step template, we need all steps and statuses
                        lstStp.add(stp);
                        mapSRExistingSteps.put(stp.SR__c,lstStp);
                    }else{
                        lstStp.add(stp);
                        mapSRExistingSteps.put(stp.SR__c,lstStp);
                    }
                    
                }
            }
            map<id,id> mapSR_SRTemplate = new map<id,id>();//Key as SR id and Value is SR's SRTemplate Id
            set<id> setDefaultActBrIds = new set<id>();
            set<id> setCustomConditionBrIds = new set<id>();
            set<id> setTotalBRIds = new set<id>();
            set<id> SR_StepIds = new set<id>();
            map<id,list<Business_Rule__c>> mapSRTempRelatedBRS = new map<id,list<Business_Rule__c>>();//key SR_Template Id and value as List of Business Rules for that SR_Template
            map<id,list<SR_Steps__c>> mapSRTempRelatedSRSteps = new map<id,list<SR_Steps__c>>();//key SR_Template Id and value as List of SR_Steps for that SR_Template
            
            if(setSRIds!=null && setSRIds.size()>0){
                for(Service_Request__c objSR:[select id,SR_Template__c from Service_Request__c where ID IN:setSRIds and SR_Template__c!=null]){
                    srTemplIds.add(objSR.SR_Template__c);
                    mapSR_SRTemplate.put(objSR.id,objSR.SR_Template__c);
                }
            }
            
            if(srTemplIds!=null && srTemplIds.size()>0){
        for(SR_Template__c SRTempl:[select id,(select id,Owner__c,SR_Template__c,Step_No__c,Step_Template__c,Start_Status__c,Estimated_Hours__c,OwnerId,Do_not_use_owner__c,Summary__c,Step_Template__r.ownerId,SR_Template__r.ownerId,SR_Template__r.Do_not_use_owner__c,Step_RecordType_API_Name__c,Step_Template__r.Step_RecordType_API_Name__c from SR_Steps_SR_Template__r) from SR_Template__c where ID IN:srTemplIds]){
                    if(SRTempl.SR_Steps_SR_Template__r!=null && SRTempl.SR_Steps_SR_Template__r.size()>0){
                        mapSRTempRelatedSRSteps.put(SRTempl.id,SRTempl.SR_Steps_SR_Template__r);
                        for(SR_Steps__c SRstp:SRTempl.SR_Steps_SR_Template__r){
                            SR_StepIds.add(SRstp.id);
                        }
                    }
                }
            }
            // prepare map to pass sr temlate and return all the business rules 
            if(SR_StepIds!=null && SR_StepIds.size()>0){
              // v2.7 - Claude - Added Owner__c to the SR Step query
                for(SR_Steps__c SRStep:[select id,Owner__c,SR_Template__c,Estimated_Hours__c,(select id,Execute_on_Insert__c,Execute_on_Update__c,Business_Rule_Condition__c,SR_Steps__r.Execute_on_Submit__c,SR_Steps__r.Owner__C,SR_Steps__r.OwnerId,SR_Steps__r.Do_not_use_owner__c,Text_Condition__c,Action_Type__c,SR_Steps__c,SR_Steps__r.Summary__c,SR_Steps__r.Step_Template__c,SR_Steps__r.Step_Template__r.ownerId,SR_Steps__r.SR_Template__c,SR_Steps__r.SR_Template__r.ownerId,SR_Steps__r.SR_Template__r.Do_not_use_owner__c,SR_Steps__r.Estimated_Hours__c,SR_Steps__r.Step_no__c,SR_Steps__r.Start_Status__c,SR_Steps__r.Step_RecordType_API_Name__c,SR_Steps__r.Step_Template__r.Step_RecordType_API_Name__c from Business_Rules__r) from SR_Steps__c where ID IN:SR_StepIds]){
                    if(SRStep.Business_Rules__r!=null && SRStep.Business_Rules__r.size()>0){
                        for(Business_Rule__c BR:SRStep.Business_Rules__r){
                            if(BR.Action_Type__c=='Default Actions'){
                                setDefaultActBrIds.add(BR.id);
                            }else if(BR.Action_Type__c=='Custom Conditions Actions'){
                                setCustomConditionBrIds.add(BR.id);
                            }
                            setTotalBRIds.add(BR.id);
                        }
                        if(mapSRTempRelatedBRS.get(SRStep.SR_Template__c)!=null && mapSRTempRelatedBRS.get(SRStep.SR_Template__c).size()>0){
                            list<Business_Rule__c> lstBR = mapSRTempRelatedBRS.get(SRStep.SR_Template__c);
                            lstBR.addALL(SRStep.Business_Rules__r);
                            mapSRTempRelatedBRS.put(SRStep.SR_Template__c,lstBR);
                        }else{
                            mapSRTempRelatedBRS.put(SRStep.SR_Template__c,SRStep.Business_Rules__r);
                        }
                    }
                }
            }
            
            list<Service_Request__c> lstStatusUpdatableSR = new list<Service_Request__c>();
            list<Service_Request__c> lstStatusUpdatableParentSR = new list<Service_Request__c>();
            list<Step__c> lstStatusUpdatableParentStep = new list<Step__c>();
            list<Step__c> lstStepsTobeInserted = new list<Step__c>();
            ctx.Prepare_StepQuery(setTotalBRIds,setStepIds);
           // user management step , notify step 
           
            list<Messaging.SingleEmailMessage> insertMailList = new list<Messaging.SingleEmailMessage>();
            map<id,list<Messaging.SingleEmailMessage>> emailStepMap = new map<id,list<Messaging.SingleEmailMessage>>();
            map<id,list<Messaging.SingleEmailMessage>> allEmailStepMap = new map<id,list<Messaging.SingleEmailMessage>>();
            list<User> usrList = new list<User>();
            map<id,list<User>>userMap = new map<id,list<User>>();
            list<EmailTemplate> ETLst = [select id, name from EmailTemplate where developername = 'Notify_Step'];
           
            usrList = new list<User>();
            /* commented by durga
            for(User eachUsr :[SELECT id,User_Email__c,Community_User_Role__c,IsActive,ContactId,AccountName__c,Account_ID__c FROM User WHERE Account_ID__c IN :setSRAccountId AND IsActive=true and Contact.Email!=null and Community_User_Role__c!=null]){
              if(eachUsr.ContactId!=null && userMap.containsKey(eachUsr.Account_ID__c)){
                usrList = userMap.get(eachUsr.Account_ID__c);
              }else{
                usrList = new list<User>();
              }
              usrList.add(eachUsr);
              userMap.put(eachUsr.Account_ID__c,usrList);
            }*/
            for(Step__c stp:Trigger.New){
                if(stp.Status__c!=null && stp.Status__c!=trigger.oldMap.get(stp.id).Status__c){
                    // excecute the below only when step status changes
                    // then do the below: 1. create a new step 
                    // 2. invoke custom cond and actions 
                    // 3. validations like verification of doc, upload of doc
                    //ctx.ExecuteContext(null,stp);
                    BRIdSet = new set<id>();
                    if(stp.SR__c!=null && mapSR_SRTemplate.get(stp.SR__c)!=null){
                        Id SRTemplateID = mapSR_SRTemplate.get(stp.SR__c);
                        if(SRTemplateID!=null && mapSRTempRelatedBRS.get(SRTemplateID)!=null){
                            Step__c Currentstep = ctx.ExecuteStepcontext(stp);
                            // Required_Docs_not_Uploaded__c flag is updated whenever user uploads a document into sr doc
                            // if it false , that means user already uploaded the doc
                            // when a particular becomes mandatory at a particular step then we are setting sr doc flag to optional = false , then sr doc trigger will update the flag in SR that Required_Docs_not_Uploaded__c = true
                            if(userinfo.getUserType()!='Standard' && Currentstep.SR__c!=null && Currentstep.SR__r.Parent_SR__c!=null && Currentstep.SR__r.Parent_SR__r.Required_Docs_not_Uploaded__c==true){
                                stp.addError('Some required documents of Parent SR are to be uploaded to proceed.');
                            }
                            if(userinfo.getUserType()!='Standard' && Currentstep.SR__c!=null && Currentstep.SR__r.Required_Docs_not_Uploaded__c==true && Currentstep.Bypass_Doc_Check__c == false){ //V2.3 - Claude - Added Bypass_Doc_Check__c field to prevent validation from firing for certain steps per Ticket #2976
                                stp.addError('Some required documents are to be uploaded to proceed.');
                            }
                            Id CurrentSRStepId;
                            SR_Steps__c currentSRSTep = new SR_Steps__c();
                            
                            if(mapSRTempRelatedSRSteps.get(SRTemplateID)!=null){
                                for(SR_Steps__c srStp:mapSRTempRelatedSRSteps.get(SRTemplateID)){
                                    if(srStp.Step_No__c==stp.Step_No__c){
                                        CurrentSRStepId = srStp.id;
                                        currentSRSTep = srStp;
                                    }
                                }
                            }
                            for(Business_Rule__c eachbr:mapSRTempRelatedBRS.get(SRTemplateID)){
                                string strStepNumber = string.valueOf(stp.Step_No__c);
                                
                                /*//on 15th June,2015 Ravi - kept the exceution of Custom Conditions Actions first than Create Conditions
                                if(eachbr.Action_Type__c=='Custom Conditions Actions' && eachbr.Execute_on_Update__c==true && CurrentSRStepId==eachbr.SR_Steps__c){
                                    //execute each BR and checks whether the Conditions met or not
                                    system.debug('Currentstep Step Status==>'+Currentstep.Step_Status__c);
                                    BRResultFlag = SM.executeBusinessRules(eachBR,ctx,Currentstep,null,null);
                                    system.debug('BRResultFlag==>'+BRResultFlag);
                                    if(BRResultFlag==true){
                                        list<Step__c> lstStep = mapSRExistingSteps.get(stp.SR__c);
                                        list<SR_Steps__c> lstSRSteps = mapSRTempRelatedSRSteps.get(SRTemplateID);
                                        BRIdSet.add(eachbr.id);
                                        try{
                                            SM.executeAction(eachbr,Currentstep,mapSRTemplateItems,mapProductPricingLines,mapPLDatedPricing,mapPLPricingRange,MapProductPriceBook,exstngSRPriceItmsMap,lstStep,mapStepQueues,mapStepTemplateQueues,mapSRStepQueues,mapSRTemplateQueues,mapSROwner,mapUserLicense,mapStepRecType,lstSRSteps);
                                        }catch(CommonCustomException ex){
                                            stp.addError(ex.getMessage());
                                        }
                                    }
                                }*/
                                if(eachbr.Text_Condition__c!=null && eachbr.SR_Steps__r.Execute_on_Submit__c==false && strStepNumber!=null && eachbr.Action_Type__c=='Create Conditions' && eachbr.Text_Condition__c.Contains(strStepNumber)){
                                    boolean createCondition;
                                    
                                    map<string,string> mapStepNoStepName = new map<String,string>();
                                    map<string,string> mapStepNoStatusType = new map<String,string>();
                                    map<string,string> mapLoopStepNo = new map<String,string>();//05-02-2014
                                    integer maxLoopStepNo = 0;
                                    integer LoopStepNo;
                                    
                                    integer CurrentloopNumber;
                                    //Sys_Step_Loop_No__c is used for holding the loop value of the step
                                    if(stp.Sys_Step_Loop_No__c!=null && stp.Sys_Step_Loop_No__c.indexOf('_')>-1){
                                        list<string> lstSplitLoop = stp.Sys_Step_Loop_No__c.split('_');
                                        if(lstSplitLoop!=null && lstSplitLoop.size()==2){
                                            CurrentloopNumber = integer.valueOf(lstSplitLoop[1]);
                                        }
                                    }
                                    
                                   // each time a loop step is repeated, _1, _2 etc is added
                                   // to understand the latest of loop steps which is to be passed to SM to execute conditions
                                   
                                    if(mapSRExistingSteps.get(stp.SR__c)!=null && mapSRExistingSteps.get(stp.SR__c).size()>0){
                                        for(Step__c step:mapSRExistingSteps.get(stp.SR__c)){
                                            if(step.Sys_Step_Loop_No__c!=null){
                                                LoopStepNo = integer.valueOf(step.Sys_Step_Loop_No__c.substringAfter('_'));
                                                if(maxLoopStepNo < LoopStepNo){
                                                    maxLoopStepNo = LoopStepNo;
                                                } 
                                            }
                                            //if(step.Step_No__c!=null && CurrentloopNumber!=null && LoopStepNo!=null && LoopStepNo>=CurrentloopNumber){
                                                /* Durga Removed the above if condition */
                                                mapStepNoStepName.put(string.valueof(step.Step_No__c),step.Step_Status__c);
                                                if(step.Step_No__c!=null && CurrentloopNumber!=null && LoopStepNo!=null && LoopStepNo>=CurrentloopNumber){
                                                    mapStepNoStatusType.put(string.valueof(step.Step_No__c),step.Status_Type__c);
                                                    mapLoopStepNo.put(step.Sys_Step_Loop_No__c,step.Sys_Step_Loop_No__c);
                                                }
                                            //}
                                        }
                                    }
                                    
                                    
                                    /*
                                    map<decimal,integer> mapStepLatestLoopNo = new map<decimal,integer>();
                                    if(mapSRExistingSteps.get(stp.SR__c)!=null && mapSRExistingSteps.get(stp.SR__c).size()>0){
                                        for(Step__c step:mapSRExistingSteps.get(stp.SR__c)){
                                            if(step.Sys_Step_Loop_No__c!=null){
                                                integer RecLoopNo = integer.valueOf(step.Sys_Step_Loop_No__c.substringAfter('_'));
                                                if(mapStepLatestLoopNo!=null && mapStepLatestLoopNo.get(step.Step_No__c)!=null){
                                                    integer PrevMaxNumber = mapStepLatestLoopNo.get(step.Step_No__c);
                                                    if(RecLoopNo>PrevMaxNumber){
                                                        mapStepLatestLoopNo.put(step.Step_No__c,RecLoopNo);
                                                    }
                                                }else{
                                                    mapStepLatestLoopNo.put(step.Step_No__c,RecLoopNo);
                                                }
                                            }
                                        }
                                    }
                                    
                                    if(mapSRExistingSteps.get(stp.SR__c)!=null && mapSRExistingSteps.get(stp.SR__c).size()>0){
                                        for(Step__c step:mapSRExistingSteps.get(stp.SR__c)){
                                            if(step.Sys_Step_Loop_No__c!=null){
                                                LoopStepNo = integer.valueOf(step.Sys_Step_Loop_No__c.substringAfter('_'));
                                                if(maxLoopStepNo < LoopStepNo){
                                                    maxLoopStepNo = LoopStepNo;
                                                } 
                                            }
                                            if(step.Step_No__c!=null && CurrentloopNumber!=null && LoopStepNo!=null && LoopStepNo>=mapStepLatestLoopNo.get(step.Step_No__c)){
                                                mapStepNoStepName.put(string.valueof(step.Step_No__c),step.Step_Status__c);
                                                mapStepNoStatusType.put(string.valueof(step.Step_No__c),step.Status_Type__c);
                                                mapLoopStepNo.put(step.Sys_Step_Loop_No__c,step.Sys_Step_Loop_No__c);
                                            }
                                        }
                                    }
                                    */
                                    
                                    try{
                                        createCondition = SM.executeBusinessRules(eachBR,ctx,Currentstep,mapStepNoStepName,mapStepNoStatusType);
                                        if(system.test.isRunningTest()){
                                            // code coverage purpose
                                          createCondition = true;
                                        }  
                                    }catch(CommonCustomException ex){
                                        stp.addError(ex.getMessage());
                                    }
                                    if(createCondition!=null && createCondition==true){
                                        //create condition for part step is true then create all the steps in the loop of BR {mapSRTempRelatedBRS}
                                        Step__c insStp = new Step__c();
                                        insStp.SR__c = stp.SR__c;
                                        insStp.Returned_By__c = '';
                                        insStp.Client_Name__c = Currentstep.SR__r.Client_Name__c;
                                        if(stp.Reviewed_By_User__c=='Quick_Review_by_Employee'){
                                            insStp.Returned_By__c = 'Quick Review';
                                        }else if(stp.Reviewed_By_User__c=='Security_Email_Approval'){
                                            insStp.Returned_By__c = 'Security';
                                        }else if(stp.Reviewed_By_User__c=='Detailed_Review_by_Employee'){
                                            insStp.Returned_By__c = 'Detailed Review';
                                        }else if(stp.Reviewed_By_User__c=='Data_Protection_Commissioner_Approval'){
                                            insStp.Returned_By__c = 'Data Protection';
                                        }else if(stp.Reviewed_By_User__c=='Line_Manager_Approval'){
                                            insStp.Returned_By__c = 'Line Manager';
                                        }else if(stp.Reviewed_By_User__c=='Registrar_Approval'){
                                            insStp.Returned_By__c = 'Registrar';
                                        }else if(stp.Reviewed_By_User__c=='Employee_Decision' || stp.Reviewed_By_User__c=='Require_More_Information_from_Customer' || stp.Reviewed_By_User__c=='RE_UPLOAD_DOCUMENT'){
                                            insStp.Returned_By__c = stp.Returned_By__c;
                                        }
                                        //added By Swati for Fit-Out        
                                        else if(stp.Reviewed_By_User__c=='CONCEPTUAL_APPROVAL_BY_FOSP' && (stp.Status_Code__c == 'REQUIRE_ADDL_INFO' || stp.Status_Code__c == 'REUPLOAD_DOCUMENT') && stp.SR_Record_Type__c == 'Fit_Out_Service_Request'){     
                                            insStp.Returned_By__c = 'Conceptual Approval By FOSP';     
                                        }else if(stp.Reviewed_By_User__c=='CONCEPTUAL_APPROVAL_BY_DIFC' && (stp.Status_Code__c == 'REQUIRE_ADDL_INFO' || stp.Status_Code__c == 'REUPLOAD_DOCUMENT') && stp.SR_Record_Type__c == 'Fit_Out_Service_Request'){     
                                            insStp.Returned_By__c = 'Conceptual Approval By DIFC';      
                                        }else if(stp.Reviewed_By_User__c=='REVIEW_AND_ISSUE_LANDLORD_NOC' && (stp.Status_Code__c == 'REQUIRE_ADDL_INFO' || stp.Status_Code__c == 'REUPLOAD_DOCUMENT') && stp.SR_Record_Type__c == 'Fit_Out_Service_Request'){       
                                            insStp.Returned_By__c = 'Review & Issue Landlord NOC';      
                                        }else if(stp.Reviewed_By_User__c=='DETAILED_DESIGN_APPROVAL_BY_FOSP' && (stp.Status_Code__c == 'REQUIRE_ADDL_INFO' || stp.Status_Code__c == 'REUPLOAD_DOCUMENT') && stp.SR_Record_Type__c == 'Fit_Out_Service_Request'){       
                                            insStp.Returned_By__c = 'Detailed Design Approval by FOSP';        
                                        }else if(stp.Reviewed_By_User__c=='VERIFICATION_OF _DCD_CERTIFICATE_DEWA_INSPECTION_APPROVAL' && (stp.Status_Code__c == 'REQUIRE_ADDL_INFO' || stp.Status_Code__c == 'REUPLOAD_DOCUMENT') && stp.SR_Record_Type__c == 'Fit_Out_Service_Request'){       
                                            insStp.Returned_By__c = 'Verification of DCD Certificate & DEWA Inspection Approval';       
                                        }else if(stp.Reviewed_By_User__c=='VERIFY_APPROVE_FOSP' && (stp.Status_Code__c == 'REQUIRE_ADDL_INFO' || stp.Status_Code__c == 'REUPLOAD_DOCUMENT') && stp.SR_Record_Type__c == 'Fit_Out_Service_Request'){        
                                            insStp.Returned_By__c = 'Verify & Approve by FOSP';        
                                        }else if(stp.Reviewed_By_User__c=='FINAL_INSPECTION_PLANNING' && stp.SR_Record_Type__c == 'Fit_Out_Service_Request'){   //V2.0 - Claude - Added additional return by value for 'Final Inspection Planning'
                                          insStp.Returned_By__c = 'Final Inspection Planning';
                                        }
                                        insStp.Step_Template__c = eachBR.SR_Steps__r.Step_Template__c;
                                        insStp.Status__c = eachBR.SR_Steps__r.Start_Status__c;
                                        queueName='';
                                        checkCustomerQueue=false;
                                        SROwnerId=null;
                                        // IF Do_not_use_owner__c = true , it will go to higher level and check sr template owner
                                        //if Do_not_use_owner__c = false then we can use the sr  step owner as step owner
                                        // under that, if it assigned to the user directly then it starts with 005, so direct assignment
                                        //  else if it is assigned for a queue , we will check if queue name contains 'client' , then send email to client saying that new step is assigned for you
                                        // for invidual user we are not sending mail for new step creation , we will send email only for queue
                                        if(eachBR.SR_Steps__r.Do_not_use_owner__c==false){
                                           if(string.valueOf(eachBR.SR_Steps__r.OwnerId).startsWith('005')){
                                                 insStp.OwnerId = eachBR.SR_Steps__r.OwnerId;
                                           }else{
                                               if(mapSRStepQueues.containsKey(eachBR.SR_Steps__r.OwnerId)){
                                                    queueName = mapSRStepQueues.get(eachBR.SR_Steps__r.OwnerId);
                                               }
                                              if(queueName.contains('Client')){
                                                    if(stp.SR_Record_Type__c != 'Fit_Out_Service_Request'){
                                                        insStp.Step_Notes__c = stp.Step_Notes__c;
                                                    }
                                                    insStp.OwnerId = mapStepQueues.get(queueName);  
                                                     /* commented by durga
                                                     if(userMap.containsKey(stp.Customer_Name__c)){
                                                        usrList = userMap.get(stp.Customer_Name__c);
                                                         emailStepMap = SM.mailListPrepareEmails(usrList,eachBR.SR_Steps__c,stp.Ultimate_SR_Menu_Text__c,ETLst,queueName);
                                                         if(emailStepMap!=null && emailStepMap.size()>0){
                                                            allEmailStepMap.putAll(emailStepMap);
                                                         }
                                                     }      
                                                     */           
                                               }else{
                                                   if(mapStepQueues.containsKey(queueName)){
                                                          insStp.OwnerId = mapStepQueues.get(queueName);
                                                   }
                                               }
                                           }      
                                        }// if we can not use the sr tempalte owner then go to higher level
                                         //  here also Do_not_use_owner__c owner check is available
                                         // for invidual user we are not sending mail for new step creation , we will send email only for queue
                                         else if(eachBR.SR_Steps__r.SR_Template__c!=null && eachBR.SR_Steps__r.SR_Template__r.Do_not_use_owner__c==false){
                                           if(string.valueOf(eachBR.SR_Steps__r.SR_Template__r.OwnerId).startsWith('005')){
                                                  insStp.OwnerId = eachBR.SR_Steps__r.SR_Template__r.OwnerId;
                                           }else{
                                                if(mapSRTemplateQueues.containsKey(eachBR.SR_Steps__r.OwnerId)){
                                                    queueName = mapSRTemplateQueues.get(eachBR.SR_Steps__r.OwnerId);
                                                }
                                                 if(queueName.contains('Client')){
                                                    if(stp.SR_Record_Type__c != 'Fit_Out_Service_Request'){
                                                     insStp.Step_Notes__c = stp.Step_Notes__c;
                                                    }
                                                     insStp.OwnerId = mapStepQueues.get(queueName);
                                                     /* commented by durga
                                                     if(userMap.containsKey(stp.Customer_Name__c)){
                                                        usrList = userMap.get(stp.Customer_Name__c);
                                                         emailStepMap = SM.mailListPrepareEmails(usrList,eachBR.SR_Steps__c,stp.Ultimate_SR_Menu_Text__c,ETLst,queueName);
                                                         if(emailStepMap!=null && emailStepMap.size()>0){
                                                            allEmailStepMap.putAll(emailStepMap);
                                                         }
                                                     }*/
                                                }else
                                                    if(mapStepQueues.containsKey(queueName)){
                                                        insStp.OwnerId = mapStepQueues.get(queueName);
                                                }
                                           }     
                                        }else if(eachBR.SR_Steps__r.Step_Template__c!=null){
                                          if(string.valueOf(eachBR.SR_Steps__r.Step_Template__r.OwnerId).startsWith('005')){
                                               insStp.OwnerId = eachBR.SR_Steps__r.Step_Template__r.OwnerId;
                                          }else{
                                               if(mapStepTemplateQueues.containsKey(eachBR.SR_Steps__r.OwnerId)){
                                                  queueName = mapStepTemplateQueues.get(eachBR.SR_Steps__r.OwnerId);
                                               }
                                                if(queueName.contains('Client')){
                                                    if(stp.SR_Record_Type__c != 'Fit_Out_Service_Request'){
                                                     insStp.Step_Notes__c = stp.Step_Notes__c;
                                                    }
                                                     insStp.OwnerId = mapStepQueues.get(queueName);  
                                                     /* commented by durga
                                                     if(userMap.containsKey(stp.Customer_Name__c)){
                                                        usrList = userMap.get(stp.Customer_Name__c);
                                                         emailStepMap = SM.mailListPrepareEmails(usrList,eachBR.SR_Steps__c,stp.Ultimate_SR_Menu_Text__c,ETLst,queueName);
                                                        
                                                         if(emailStepMap!=null && emailStepMap.size()>0){
                                                            allEmailStepMap.putAll(emailStepMap);
                                                         }
                                                     }*/
                                                }else{
                                                    if(mapStepQueues.containsKey(queueName)){
                                                        insStp.OwnerId = mapStepQueues.get(queueName);
                                                    }
                                                }
                                            }     
                                        }
                                        // record type defines the layout for inidividual
                                        // 
                                        //
                                        if(eachBR.SR_Steps__r.Step_RecordType_API_Name__c!=null){
                                          if(mapStepRecType.get(eachBR.SR_Steps__r.Step_RecordType_API_Name__c)!=null)
                                            insStp.RecordTypeId = mapStepRecType.get(eachBR.SR_Steps__r.Step_RecordType_API_Name__c);
                                        }else{
                                            if(eachBR.SR_Steps__r.Step_Template__c!=null && eachBR.SR_Steps__r.Step_Template__r.Step_RecordType_API_Name__c!=null && mapStepRecType.get(eachBR.SR_Steps__r.Step_Template__r.Step_RecordType_API_Name__c)!=null){
                                                insStp.RecordTypeId = mapStepRecType.get(eachBR.SR_Steps__r.Step_Template__r.Step_RecordType_API_Name__c);
                                            }
                                        }
                                        insStp.SR_Step__c = eachBR.SR_Steps__c;
                                        insStp.Start_Date__c = system.today();
                                        insStp.Summary__c = eachBR.SR_Steps__r.Summary__c;
                                        insStp.Step_No__c = eachBR.SR_Steps__r.Step_No__c;
                                        //V1.8
                                        maxLoopStepNo = maxLoopStepNo+1;
                                        //insStp.Sys_Step_Loop_No__c = (mapLoopStepNo.containsKey(string.valueof(insStp.Step_No__c))) ? (string.valueOf(mapLoopStepNo.get(string.valueof(insStp.Step_No__c))+1)) : 1+'';
                                        //insStp.Sys_Step_Loop_No__c = insStp.Step_No__c+''
                                        insStp.Sys_Step_Loop_No__c = string.valueOf(insStp.Step_No__c)+'_'+maxLoopStepNo;
                                        
                                        //ID BHId = id.valueOf(Label.Business_Hours_Id);
                    //V2.0 - Start - Claude                                        
                                        //ID BHId = stp.SR_Group__c.equals('Fit-Out & Events') ? Business_Hours__c.getInstance('Fit-Out').Business_Hours_Id__c : id.valueOf(Label.Business_Hours_Id);
                                        //V2.0 - End
                                        
                                        //V2.7 - Claude - replaced with new logic
                                        Cls_StepBusinessHoursUtils.checkOwner(eachBR.SR_Steps__r.Owner__c);
                                        ID BHId = stp.SR_Group__c.equals('Fit-Out & Events') ? Cls_StepBusinessHoursUtils.getBusinessHoursId() : id.valueOf(Label.Business_Hours_Id);
                                        //V2.7 - Claude - End
                                        if(eachBR.SR_Steps__r.Estimated_Hours__c!=null){
                                            Long sla = eachBR.SR_Steps__r.Estimated_Hours__c.longvalue();
                                            sla=sla*60*60*1000L;
                                            datetime CreatedTime = system.now(); 
                                            insStp.Due_Date__c=BusinessHours.add(BHId,CreatedTime,sla);
                                            //insStp.Step_Notes__c = BHId; // Remove before deployment
                                        }
                                        //added by Ravi on 26 April 2015 to assign the step to user who assigned it
                                        if(queueName.contains('Client') == false && stp.Sys_Owned_by_Client__c == false && stp.SR_Group__c!='Fit-Out & Events' &&(stp.Step_Status__c == 'Required Information Updated' || stp.Step_Status__c == 'Document Re-uploaded')){
                                            insStp.Sys_Step_OwnerId__c = insStp.OwnerId;
                                            insStp.OwnerId = stp.CreatedById;
                                        }
                                        if(string.isNotBlank(insStp.Returned_By__c) && (stp.Status_Code__c == 'REQUIRE_ADDL_INFO' || stp.Status_Code__c == 'REUPLOAD_DOCUMENT') && stp.SR_Record_Type__c == 'Fit_Out_Service_Request'){
                                            if(insStp.Returned_By__c == 'Conceptual Approval By DIFC' || 
                                               insStp.Returned_By__c == 'Detailed Design Approval by FOSP' ||
                                               insStp.Returned_By__c == 'Verification of DCD Certificate & DEWA Inspection Approval' ||
                                               insStp.Returned_By__c == 'Final Inspection Planning' || // V2.0 - Claude - Added return by field for final inspection planning
                                               insStp.Returned_By__c == 'Verify & Approve by FOSP' ){
                                                insStp.OwnerId = mapStepQueues.get('Client Entry User');
                                            }
                                            if(insStp.Returned_By__c == 'Conceptual Approval By DIFC' ){
                                                //insStp.OwnerId = mapStepQueues.get('IDAMA Manager'); // Commented by Claude 30/03/2016
                                                insStp.OwnerId = mapStepQueues.get('FOSP Fit-Out Manager'); // V2.9
                                                
                                            }
                                            if(insStp.Returned_By__c == 'Review & Issue Landlord NOC' ){
                                                insStp.OwnerId = mapStepQueues.get('FOSP');
                                            }
                                            insStp.Step_Notes__c = stp.Step_Notes__c;
                                        }
                                        if(mapLoopStepNo.get(insStp.Sys_Step_Loop_No__c)==null){
                                            lstStepsTobeInserted.add(insStp);
                                        }
                                    }
                                }
                                if(eachbr.Action_Type__c=='Custom Conditions Actions' && eachbr.Execute_on_Update__c==true && CurrentSRStepId==eachbr.SR_Steps__c){
                                    //execute each BR and checks whether the Conditions met or not
                                    System.debug(eachBr);
                                    BRResultFlag = SM.executeBusinessRules(eachBR,ctx,Currentstep,null,null);
                                    System.debug(BRResultFlag);
                                    if(BRResultFlag==true){
                                        list<Step__c> lstStep = mapSRExistingSteps.get(stp.SR__c);
                                        list<SR_Steps__c> lstSRSteps = mapSRTempRelatedSRSteps.get(SRTemplateID);
                                        BRIdSet.add(eachbr.id);
                                        try{
                                            SM.executeAction(eachbr,Currentstep,mapSRTemplateItems,mapProductPricingLines,mapPLDatedPricing,mapPLPricingRange,MapProductPriceBook,exstngSRPriceItmsMap,lstStep,mapStepQueues,mapStepTemplateQueues,mapSRStepQueues,mapSRTemplateQueues,mapSROwner,mapUserLicense,mapStepRecType,lstSRSteps);
                                        }catch(CommonCustomException ex){
                                            stp.addError(ex.getMessage());
                                        }
                                    }
                                }
                            }
                        }
                    }
                }  
            }
            
            if(lstStepsTobeInserted!=null && lstStepsTobeInserted.size()>0){
                try{
                    insert lstStepsTobeInserted;
                    list<Messaging.SingleEmailMessage> mailList = new list<Messaging.SingleEmailMessage>();
                    for(Step__c eachStp : lstStepsTobeInserted){
                      if(allEmailStepMap.containsKey(eachStp.SR_Step__c)){
                        mailList = allEmailStepMap.get(eachStp.SR_Step__c);
                        for(Messaging.SingleEmailMessage email: mailList){
                          email.setWhatId(eachStp.Id);
                          insertMailList.add(email);
                        }    
                      }
                    }
                  
                    if(insertMailList!=null && insertMailList.size() > 0){
                     Messaging.sendEmail(insertMailList);
                    }
                }catch(Exception e){
                  
                    /*
                    string ErrorMessage = string.valueOf(e.getMessage());
                    if(ErrorMessage.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION,')>-1){
                        list<string> lstErrorlst = ErrorMessage.split('FIELD_CUSTOM_VALIDATION_EXCEPTION,');
                        if(lstErrorlst!=null && lstErrorlst.size()==2){
                            ErrorMessage = lstErrorlst[1];
                            if(ErrorMessage.indexof(': []')>-1){
                                ErrorMessage = ErrorMessage.replaceAll(': []','');
                            }
                        }
                    }
                    */
                    
                    
                    string strDMLError = string.valueOf(e.getMessage());
                    if(strDMLError!=null && strDMLError!='' && strDMLError.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION')>-1){
                        list<string> lstMsg = strDMLError.split('FIELD_CUSTOM_VALIDATION_EXCEPTION');
                        if(lstMsg!=null && lstMsg.size()>=2){
                            strDMLError = lstMsg[1];
                        }
                    }
                    if(strDMLError!=null && strDMLError!='')
                        strDMLError = strDMLError.replace(': []','');
                        
                    if(strDMLError!=null && strDMLError!='')
                        strDMLError = strDMLError.replace(',','');

                    trigger.new[0].addError(strDMLError);
                }
            }
        }
        /* End of Step Status Change Execution Trigger*/
    }
    
    //v1.5
    if(trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
        list<Step__c> lst = new list<Step__c>();
        list<Id> lstAOs = new list<Id>();
        list<Id> lstRoCIds = new list<Id>();
        
        for(Step__c obj : trigger.new){
          
            if(obj.SR_Group__c == 'GS' && obj.Is_Courier_Enabled__c && obj.Pending__c == null){
                if(trigger.isInsert){
                    if((obj.SAP_DOCDL__c == 'X' || obj.SAP_DOCRC__c == 'X' || obj.SAP_DOCCO__c == 'X' || obj.SAP_DOCCR__c == 'X') || ( obj.Is_Courier_Enabled__c && ( (obj.Is_Letter__c && (obj.Step_Name__c == 'Document Collected' || obj.Step_Name__c == 'Collected')) || (obj.Step_Name__c == 'Document Collected' && (obj.SR_Record_Type__c == 'PO_BOX' || obj.SR_Record_Type__c == 'Add_a_New_Service_on_the_Index_Card')) ) ) ){
                        lst.add(obj);
                    }   
                }
                if( trigger.isUpdate){
                    if(obj.Step_Name__c == 'Front Desk Review'){
                        if(obj.Status_Code__c == 'AWAITING_ORIGINALS' && trigger.oldMap.get(obj.Id).Status_Code__c == 'PENDING_REVIEW')
                            lst.add(obj);
                        else if((obj.Status_Code__c == 'VERIFIED' || obj.Status_Code__c == 'REJECTED') && trigger.oldMap.get(obj.Id).Status_Code__c == 'AWAITING_ORIGINALS')
                            lstAOs.add(obj.Id);
                    }else if((obj.Is_Letter__c || obj.SR_Record_Type__c == 'PO_BOX' || obj.SR_Record_Type__c == 'Add_a_New_Service_on_the_Index_Card') && (obj.Step_Name__c == 'Document Collected' || obj.Step_Name__c == 'Collected') && obj.Status_Code__c == 'COLLECTED' && trigger.oldMap.get(obj.Id).Status_Code__c == 'PENDING_COLLECTION'){
                        lstAOs.add(obj.Id);
                    }
                    //V1.7
                    if((obj.SAP_DOCDL__c == 'X' || obj.SAP_DOCRC__c == 'X' || obj.SAP_DOCCO__c == 'X' || obj.SAP_DOCCR__c == 'X') && obj.Is_Courier_Enabled__c && obj.Status_Code__c == 'CLOSED' && trigger.oldMap.get(obj.Id).Status_Code__c != 'CLOSED'){
                        lstAOs.add(obj.Id);
                    }
                    
                }
            }else if(trigger.isUpdate && obj.SR_Group__c == 'ROC' && obj.Step_Status__c == 'Delivered' && trigger.oldMap.get(obj.Id).Step_Status__c != obj.Step_Status__c){
              
                lstRoCIds.add(obj.Id);
            }
            
            
            //GSPortalUserEmailHandler.sendEmailToPortal(obj);
            
           if(trigger.isInsert){           //veera 
             GSStepEmailLogCreationHandler.CreateEmailLogOnInsert(obj);
             if(obj.Step_Name__c == 'Medical Fitness Test scheduled' ){
               AppointmentCreationHandler.MedicalAppointmentCreate(obj);
             }
            } 
             
            if(trigger.isUpdate){
            
              if(obj.status__c != System.trigger.oldMap.get(obj.id).status__c  ){  
              
               List<Status__c> status = new List<status__c>();               
                if(obj.step_name__c  != 'Medical Fitness Test scheduled')           
                GSStepEmailLogCreationHandler.CreateEmailLogOnUpdate(obj);
                else if(obj.step_name__c  == 'Medical Fitness Test scheduled' && obj.Status_Code__c == 'SCHEDULED'){                              
                   GSStepEmailLogCreationHandler.CreateEmailLogOnUpdate(obj);
                }
              }
              if(obj.step_name__c  == 'Medical Fitness Test scheduled' && obj.status__c != System.trigger.oldMap.get(obj.id).status__c && obj.Status_Code__c != 'CLOSED' && obj.Status_Code__c != 'RE-SCHEDULED'){              
                GsMedicalTestScheduledHandler.SendMedicalServiceToSAP(obj); 
              }
            
            } //Veera
           
            
            
        }
        
        if(!lst.isEmpty()){
            string res = GsCourierDetails.CreateCourierStep(lst);
            if(res != 'Success'){
                trigger.new[0].addError(res);
            }
        }
        if(!lstAOs.isEmpty()){
            GsCourierDetails.CloseAwaitingOriginals(lstAOs);
        }
        if(!lstRoCIds.isEmpty()){
          GsCourierDetails.CloseRocShipments(lstRoCIds);
        }
            
    }
}