trigger OfficeRMDPush on Office_RND_Membership__c (after Update,after insert){
    
    List<Office_RND_Membership__c> updateList = new  List<Office_RND_Membership__c>();
    for(Office_RND_Membership__c tc : trigger.new){
        if(
            (   
                tc.End_Date__c != null && tc.Start_Date__c != null && 
                ( Trigger.isInsert ||
                    (  Trigger.isUpdate && 
                        (
                            (tc.End_Date__c != null && tc.End_Date__c != trigger.Oldmap.get(tc.id).End_Date__c ) ||
                            (tc.Start_Date__c != null && tc.Start_Date__c != trigger.Oldmap.get(tc.id).Start_Date__c )
                        )
                    )
                )
            )
        ){
           updateList.add(tc);
        }
    }
    if(updateList.size() >0)System.enqueueJob(new OfficeRndAttachmentQueueable(updateList));
}