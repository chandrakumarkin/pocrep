/*
    Created By  : RB Nagaboina on 9th Nov,2016
    Description : 
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 V1.0   16/09/2015      Ravi            Created  #2516
 ----------------------------------------------------------------------------------------
*/
trigger LeaseTrigger on Lease__c (after insert, after update) {
    
    //v1.1
    TriggerFactoryCls.createHandler(Lease__c.sObjectType);
    
    Map<ID, decimal> mapOfLeaseReserve = new Map<ID, decimal>();
    Map<ID, decimal> mapOfLeaseUnReserve = new Map<ID, decimal>();
    Set<ID> setOfAccountID =  new Set<ID>();
    
    for(Lease__c iLease : Trigger.New){
        if(string.isNotBlank(iLease.Account__c))
            setOfAccountID.add(iLease.Account__c);
    }
    
    List<Lease__c> listLease = [Select ID ,Total_Reserve_Parking__c,Total_Un_Reserve_Parking__c,Account__c FROM 
                                Lease__c where Account__c IN : setOfAccountID AND Status__c = 'Active'];
    
    if(listLease.isEmpty()) return;
    
    
    Map<ID,Account> mapOfAcocunt = new Map<ID,Account>();
    for(Lease__c iLease : listLease){
        
        if(mapOfAcocunt.containsKey(iLease.Account__c)){
            
            Account acc = mapOfAcocunt.get(iLease.Account__c);
            acc.Total_Reserve_Parkingc__c += iLease.Total_Reserve_Parking__c;
            acc.Total_Un_Reserve_Parkingc__c += iLease.Total_Un_Reserve_Parking__c;
            continue;
        }
        
        Account acc = new Account();
        acc.ID = iLease.Account__c;
        acc.Total_Reserve_Parkingc__c = iLease.Total_Reserve_Parking__c;
        acc.Total_Un_Reserve_Parkingc__c = iLease.Total_Un_Reserve_Parking__c;
        mapOfAcocunt.put(acc.ID,acc);
    }
    
    if(!mapOfAcocunt.isEmpty()) update mapOfAcocunt.values();
   
    
    
}