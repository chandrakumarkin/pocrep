trigger Update_Rec_Juri on Permit__c (before update) {
    for(Permit__c P: trigger.new){
        if(trigger.oldmap.get(P.id).Recognized_Jurisdictions__c!=null){
            P.Recognized_Jurisdictions__c=trigger.oldmap.get(P.id).Recognized_Jurisdictions__c+';'+P.Recognized_Jurisdictions__c;
            
        }
    }
}