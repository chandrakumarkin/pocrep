/******************************************************************************************
 *  Name        : TRG_CreateBP 
 *  Author      : Kaavya Raghuram
 *  Company     : NSI JLT
 *  Date        : 2016-04-20
 *  Description : This trigger is to send the account to SAP for BP creation
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   20-04-2016   Kaavya        Created
 V1.1   01-05-2016   Claude        Added validation for account record type
 V1.2   23-05-2016   Claude        Revised sector lead logic
 V1.3   29-05-2016   Claude        Bulkified user retrieval; added logic to update Account
 V1.4   30-05-2016   Claude        Added logic to re-parent contact records to trigger relationship flow; Moved all logic to CLS_LeadUtils class
 V1.5   01-10-2017   Arun          Added a code to disable trigger for specific user
 v1.6   20-03-2020   Durga         Added code to invoke LeadTriggerHandler
*******************************************************************************************/
trigger TRG_CreateBP on Lead(after update){
    //Check custom label stored user id same as running user id ? if not the same run the trigger 
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled){
    	if(trigger.isUpdate && trigger.isAfter){//v1.6
	        set<Id> setLeadId = new set<Id>();
					Map<Id,Id> mapContentDocument = new Map<Id,Id>();
					
					OB_LeadTriggerHandler.Execute_AU(Trigger.New,Trigger.oldMap);//v1.6
					
	        if(CLS_LeadUtils.isLeadsForConversion(Trigger.New)){
						new CLS_CreateBP().afterConvertion(Trigger.New,Trigger.newMap,Trigger.oldMap);  
					}
	            
	        
	        for(Lead eachLead:trigger.new){
	            setLeadId.add(eachLead.ID);
	        }
	        for(ContentDocumentLink eachDocument: [SELECT ContentDocumentID, LinkedEntity.type, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN:setLeadId AND LinkedEntity.type ='Lead']){
	            mapContentDocument.put(eachDocument.LinkedEntityId,eachDocument.ContentDocumentID);
	        }  
	        for(Lead eachDocument:trigger.new){
	            if(mapContentDocument.get(eachDocument.Id) == null && eachDocument.status =='LOI Received' && !system.test.isrunningtest())
	                eachDocument.addError('Please upload LOI document');
	        }
    	}
    }
}