trigger TrainingRequestTrigger on Training_Request__c (after insert, after update, after delete, after undelete) {
     if(trigger.isAfter){
            if(trigger.isInsert)
                TrainingRequestTriggerHandler.Execute_AI(Trigger.New);
            if(trigger.isUpdate)
                TrainingRequestTriggerHandler.Execute_AU(Trigger.New);
            if(trigger.isDelete)
                TrainingRequestTriggerHandler.Execute_AU(Trigger.Old);
             if(trigger.isUnDelete)
                 TrainingRequestTriggerHandler.Execute_AUnDelete(Trigger.New);
      }
}