trigger PendingTrg on Pending__c (before insert,after insert, after update) {
    if(trigger.isBefore){
        map<string,string> mapStepIds = new map<string,string>();
        for(Pending__c objP : trigger.new){
            if(objP.Step__c != null){
                mapStepIds.put(objP.Step__c,null);
            }
        }
        string account;
        if(!mapStepIds.isEmpty()){
            for(Step__c objStep : [select Id,Step_Id__c,sr__r.Customer__c from Step__c where Step_Id__c != null AND Id IN : mapStepIds.keySet()]){
                mapStepIds.put(objStep.Id,objStep.Step_Id__c);
                account = objStep.sr__r.Customer__c;
               
            }
            for(Pending__c objP : trigger.new){
                if(mapStepIds.get(objP.Step__c) != null && objP.Start_Date__c != null){
                    string str = GsPendingInfo.PendingKeyFormat(objP.Start_Date__c);
                    objP.Pending_Unique_No__c = mapStepIds.get(objP.Step__c)+'_'+str;
                    if(objP.Internal_Pending__c == false){
                    objP.Account__c = account;
                    
                     system.debug(objP.Account__c +'Account');
                    }
                }
            }
        }
    }else if(trigger.isAfter){
        map<Id,Service_Request__c> mapSRs = new map<Id,Service_Request__c>();
        map<Id,String> mapCustomers = new map<Id,String>();
        
        map<id,string> pendingMap = new map<id,string>();
        
        for(Pending__c objP : trigger.new){
            if(trigger.isInsert){
                if(objP.Start_Date__c != null && objP.End_Date__c == null && objP.Service_Request__c != null){
                    if(objP.Internal_Pending__c != true)
                    mapSRs.put(objP.Service_Request__c,new Service_Request__c(Id=objP.Service_Request__c,Is_Pending__c=true));
                    step__c step = [select id,sr__r.Customer__c,Sys_SR_Group__c ,status__c ,SR__c,Step_Name__c,Status_Code__c,SR_Template__c  from step__c where id =:objP.step__C ];
                     if(objP.Internal_Pending__c != true){
                    pendingMap.put(objP.id,step.sr__r.Customer__c);
                   GSStepEmailLogCreationHandler.CreateEmailLogOnInsert(step);
                   }
                }
            }else if(trigger.isUpdate){
                if(objP.Start_Date__c != null && objP.End_Date__c != null && objP.Service_Request__c != null && objP.End_Date__c != trigger.oldMap.get(objP.Id).End_Date__c){
                    mapSRs.put(objP.Service_Request__c,new Service_Request__c(Id=objP.Service_Request__c,Is_Pending__c=false));
                }
            }
            if(objP.Service_Request__c != null)
                mapCustomers.put(objP.Service_Request__c,null);
        }
        if(!mapSRs.isEmpty())
            update mapSRs.values();
        
        system.debug(pendingMap +'pendingMap');
            
        if(!pendingMap.isEmpty())    {
        List<pending__c> listPending = new List<pending__c>();
          pending__c pending = new pending__c();
        
         // for(pending__c p:[select id from pending__c where id in:pendingMap.Keyset()]){
             
             for(ID p: pendingMap.Keyset()){
          
             pending.id = p;
             pending.account__c = pendingMap.get(p);
             listPending.add(pending);
          
          }
          if(listPending.size()>0)
          update listPending;
           
        }
           
            
        if(!mapCustomers.isEmpty()){
            for(Service_Request__c objSR : [select Id,Customer__r.Name from Service_Request__c where Id IN : mapCustomers.keySet()]){
                mapCustomers.put(objSR.Id,objSR.Customer__r.Name);
            }
        }
        for(Pending__c objPending : trigger.new){
            if((trigger.isInsert && objPending.Start_Date__c != null && objPending.End_Date__c == null) ||
                (trigger.isUpdate && objPending.Start_Date__c != null && objPending.End_Date__c != null)  ){
                if(objPending.Service_Request__c != null && objPending.Internal_Pending__c!=true )
                    GsPendingInfo.CreateCustomerStep(objPending,mapCustomers.get(objPending.Service_Request__c));
            }
        }   
    }
}