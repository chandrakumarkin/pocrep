/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 15/12/2019
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    15/12/2019  shoaib    Created
**********************************************************************************************************************/
trigger InspectionTrigger on Inspection__c (before update,before insert,after update) {
    Map<id,Inspection__c > oldMap = Trigger.oldMap;
    if(Trigger.isBefore) {
        //validation
        if(Trigger.isUpdate){
             InspectionTriggerHandler.validateInspectionRecord(Trigger.new,oldMap); 
        }
        
    }
    //update status to completed once approved.this is done in future method for tracking purpose.
         if(Trigger.isAfter && Trigger.isUpdate ){
             Set<Id> inspectionIds = new  Set<Id>();
             for(Inspection__c thisInspection : Trigger.new){
                  if(thisInspection.Status__c == 'Approved'){
                      inspectionIds.add(thisInspection.Id);
                  }
             }
             if(!inspectionIds.isEmpty()){
                 InspectionTriggerHandler.updateStatusToApproved(inspectionIds); 
             }
       }
  }