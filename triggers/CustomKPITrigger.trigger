trigger CustomKPITrigger on Custom_KPI_Report__c (before insert,before update) {
    
    ID BHid = id.valueOf(Label.Business_Hours_Id);
    string businessHourName = 'GS';
         
    ID businessHoursId = String.isNotBlank(businessHourName) && Business_Hours__c.getAll().containsKey(businessHourName) ?  Id.valueOf(Business_Hours__c.getInstance(businessHourName).Business_Hours_Id__c) : Id.ValueOf( [SELECT Id FROM BusinessHours WHERE Name =: businessHourName limit 1].Id);
    BHId = businessHoursId ;
    
    Decimal businessHourDecimal ;
    Decimal CompleteDurationDifference;
         
    long BUSINESS_HOUR_MILLISECONDS = 3600000; 
    
    for(Custom_KPI_Report__c thisReport : Trigger.New){ 
        
        if(thisReport.End_Date__c != null){        
          businessHourDecimal        = BUSINESS_HOUR_MILLISECONDS + 0.0;
           
          Decimal PausedHours   = 0;
          Decimal PausedMinutes = 0;
            
          if(thisReport.Resume_Date__c != null && thisReport.Pause_Date__c != null){
            Long inputMillisecs                 = BusinessHours.diff(BHId,thisReport.Pause_Date__c,thisReport.Resume_Date__c);
            Integer total_duration_in_seconds   = (inputMillisecs/1000).intValue();
            PausedMinutes                       = math.mod(math.floor(total_duration_in_seconds/60).intValue(),60);
            PausedHours                         = math.mod(math.floor(total_duration_in_seconds/3600).intValue(),24); 
           
          }
           
          Decimal CourierMinutes    = 0;
          Decimal CourierHours      = 0;
            
          if(thisReport.Awaiting_Original_Stop_Date__c != null && thisReport.Awaiting_Original_Start_Date__c != null){
            Long inputMillisecs                 = BusinessHours.diff(BHId,thisReport.Awaiting_Original_Start_Date__c,thisReport.Awaiting_Original_Stop_Date__c);
            Integer total_duration_in_seconds           = (inputMillisecs/1000).intValue();
            CourierMinutes                      = math.mod(math.floor(total_duration_in_seconds/60).intValue(),60);
            CourierHours                        = math.mod(math.floor(total_duration_in_seconds/3600).intValue(),24);
          }
            
         Decimal CompleteDurationMinutes    = 0;
         Decimal CompleteDurationHours      = 0;  
            
         if(thisReport.Creation_Date__c != null && thisReport.End_Date__c != null){
             
            Long inputMillisecs                      = BusinessHours.diff(BHId,thisReport.Creation_Date__c,thisReport.End_Date__c);
            Integer total_duration_in_seconds        = (inputMillisecs/1000).intValue();
            CompleteDurationMinutes                  = math.mod(math.floor(total_duration_in_seconds/60).intValue(),60);
            CompleteDurationHours                    = math.mod(math.floor(total_duration_in_seconds/3600).intValue(),24);
        
         }
            if(!test.isRunningTest()){
          		thisReport.KPI_Totals_Time_in_Hour_s__c = String.valueOf((CompleteDurationHours -(CourierHours + PausedHours ))) + '.' + String.valueOf((CompleteDurationMinutes  - (PausedMinutes+CourierMinutes)));
            }
         }
    }
 }