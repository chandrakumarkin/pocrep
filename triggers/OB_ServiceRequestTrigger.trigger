/******************************************************************************************
 *  Author      : Durga Prasad
 *  Company     : PwC
 *  Date        : 14-Dec-2019
 *  Description : Trigger on HexaBPM__Service_Request__c i.e Application object
 *  Modification 
        History :
*******************************************************************************************/
trigger OB_ServiceRequestTrigger on HexaBPM__Service_Request__c (before insert, before update,after insert, after update) 
{
    if(UserInfo.getUserName()!=label.User_WF_Trigger_Disabled)
    {
            if(trigger.isBefore && trigger.isInsert)
                OB_ServiceRequestTriggerHandler.OnBeforeInsert(trigger.new);
            
            if(trigger.isAfter && trigger.isInsert)
                OB_ServiceRequestTriggerHandler.onAfterInsert(trigger.new); 
            
            if(trigger.isBefore && trigger.isUpdate)
                 OB_ServiceRequestTriggerHandler.OnBeforeUpdate(trigger.old,trigger.new,Trigger.oldMap,Trigger.newMap);
            
            if(trigger.isAfter && trigger.isUpdate)
                 OB_ServiceRequestTriggerHandler.OnAfterUpdate(trigger.old,trigger.new,Trigger.oldMap,Trigger.newMap);
    }
    
    
}