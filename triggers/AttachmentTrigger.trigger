/******************************************************************************************
 *  Trigger  : Attachment Trigger
 *  Author   : Durga Prasad
 *  Company  : NSI JLT
 *  Date     : 2014-12-Aug                           
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    12-01-2016  Durga         Created
V2.0    20-12-2016  Claude        Added exemption for System Admins (#3789)
V2.1    13-06-2017  Sravan        Copying the attachment content in the base64 format an updating the text on the SR doc 
v2.2    14-04-2021  Arun          Added GDRFA__RESIZE150_DIFC_WB_API_Data condition for GDRFA project
****************************************************************************************************************/
trigger AttachmentTrigger on Attachment (after insert,before delete) {
    if(trigger.isInsert){
        map<id,SR_Doc__c> updateSRDocMap = new  map<id,SR_Doc__c>();
        list<User_SR__c> lstUserSRs = new list<User_SR__c>();
        map<id,string> binaryAttachmentMap = new map<id,string>();
        map<id,SR_Doc__c> srDocSet = new map<id,SR_Doc__c>();
        for(Attachment att : trigger.New){        
            if(att.ParentId.getSobjectType() == SR_Doc__c.SobjectType){
            
            SR_Doc__c updateSRDoc;
               // --v2.2- Arun
                if(att.Name.startsWith('GDRFA__RESIZE150_DIFC_WB_API_Data')==false)
                 updateSRDoc = new SR_Doc__c(Id=att.ParentId,Doc_ID__c=att.Id);                
             else
                  updateSRDoc = new SR_Doc__c(Id=att.ParentId,GDRFA_Compressed_Doc_Id__c=att.Id);                
              
                updateSRDocMap.put(att.ParentId,updateSRDoc);   
                
            }
            
            if(att.ParentId.getSobjectType() == User_SR__c.SobjectType && att.Name =='New User Access Form.pdf'){
                lstUserSRs.add(new User_SR__c(Id=att.ParentId,DDP_Status__c = 'Generated',Sys_Attach_Id__c=att.Id));
            }
        }
        
        if(updateSRDocMap!=null && updateSRDocMap.size()>0){        
            update updateSRDocMap.values();
            for(SR_Doc__c srd : [select name,id,Doc_ID__c,Create_Picture__c,Service_Request__c,Service_Request__r.customer__c,Service_Request__r.Name,Service_Request__r.Submitted_Date__c from SR_Doc__c where id in : updateSRDocMap.KeySet() ]){
                if(srd.Create_Picture__c)
                    srDocSet.put(srd.id,srd);
            }
            
            for(Attachment att : trigger.new){
                if(att.body !=null && srDocSet.containsKey(att.ParentID))//v2.1
                    binaryAttachmentMap.put(att.id,string.valueOf(EncodingUtil.base64Encode(att.Body)));
            }
            
            if([SELECT Id FROM Custom_Attachment__c WHERE SR_Doc__c IN : srDocSet.Keyset()].size() > 0) { DELETE [SELECT Id FROM Custom_Attachment__c WHERE SR_Doc__c IN: srDocSet.Keyset()];}
            
            Custom_Attachment__c ca;
            long noOfloops;
            integer maxSizeOfText = 131072;
            boolean upsertCustomAttch=false;
            List<Custom_Attachment__c> cAttachToUpdate = new List<Custom_Attachment__c>();
            list<Log__c> logList = new List<log__c>();
            
            for(SR_Doc__c srd : srDocSet.values()){
                            
                upsertCustomAttch=false;
                
                if(srd.Create_Picture__c ==true &&  binaryAttachmentMap.containsKey(srd.Doc_ID__c) && binaryAttachmentMap.get(srd.Doc_ID__c).length() >0){
                    if(srd.Service_Request__r.Submitted_Date__c !=null)
                        upsertCustomAttch = true;
                    ca = new Custom_Attachment__c(SR_Doc__c =srd.id ,Service_Request__c =srd.Service_Request__c,SRDoc_EXT__c =srd.id);
                    ca.Picture_Length__c =  binaryAttachmentMap.get(srd.Doc_ID__c).length();
                    noOfloops = (ca.Picture_Length__c/maxSizeOfText).round(System.RoundingMode.CEILING);
                    if(noOfloops <= 12){ 
                        for(integer i = 1;i <=12;i++){
                            if(i < noOfloops)    
                                ca.put('Picture_in_Text'+i+'__c',binaryAttachmentMap.get(srd.Doc_ID__c).substring(maxSizeOfText*(i-1), maxSizeOfText*i));
                            else if(i == noOfloops) 
                                ca.put('Picture_in_Text'+i+'__c',binaryAttachmentMap.get(srd.Doc_ID__c).substring(maxSizeOfText*(i-1),binaryAttachmentMap.get(srd.Doc_ID__c).length()));
                            else if(i>noOfloops)
                                ca.put('Picture_in_Text'+i+'__c','');
                                
                        }
                            if(upsertCustomAttch)
                                cAttachToUpdate.add(ca);
                    }
                    else{
                        Log__c binaryattLog = new Log__c();
                        binaryattLog.Account__c = srd.Service_Request__r.customer__c;
                        binaryattLog.Description__c =  'Unable to convert picture to binary format.Reached maximum limit SR No - '+srd.Service_Request__r.Name+' SR Doc -'+srd.Name;
                        binaryattLog.Type__c     = 'Failure - Converting Attachment  to Binary format';
                        logList.add(binaryattLog);
                    }
                }
                
                                
            }       
            

                if(cAttachToUpdate !=null && !cAttachToUpdate.isEmpty()) Upsert cAttachToUpdate SRDoc_EXT__c;
                if(logList !=null && !logList.isEmpty()) insert logList;                
            
        }
        
        if(!lstUserSRs.isEmpty())
            update lstUserSRs;
    }
    if(trigger.isDelete && trigger.isBefore){
        for(attachment obj : trigger.old){
            if(obj.createdById != userinfo.getuserId()){
                // V2.0 - Claude - Start
                Map<Id,Profile> systemAdminProfiles = new Map<Id,Profile>([SELECT Id FROM Profile WHERE Name LIKE 'System Administrator%']);

                if(!systemAdminProfiles.isEmpty() && !systemAdminProfiles.containsKey(UserInfo.getProfileId())){
                    obj.addError('Only owner can delete the attachment.');  
                }
                //obj.addError('Only owner can delete the attachment.');  
                // V2.0 - Claude - End
            }   
        }   
    }
}