/*
    Author          :   Durga Prasad
    Trigger Name    :   AccountHistoryTrg
    Description     :   This trigger will track the field history and stores that
                        history in the object called Entity_History__c
*/
trigger AccountHistoryTrg on Account (after update) {
    /*
    if(trigger.isAfter && trigger.isUpdate){
        map<string,string> mapHistoryFields = new map<string,string>();
        map<string,History_Tracking__c> mapHistoryCS = new map<string,History_Tracking__c>();
        if(History_Tracking__c.getAll()!=null){
            mapHistoryCS = History_Tracking__c.getAll();//Getting the Objects from Custom Setting which has to be displayed in the screen for the users
            for(History_Tracking__c objHisCS:mapHistoryCS.values()){
                if(objHisCS.Object_Name__c!=null && objHisCS.Object_Name__c.tolowercase()=='account' && objHisCS.Field_Name__c!=null){
                    mapHistoryFields.put(objHisCS.Field_Name__c,objHisCS.Field_Label__c);
                }
            }
        }
        list<Entity_History__c> lstEntHist = new list<Entity_History__c>();
        for(Account acc:trigger.new){
            if(mapHistoryFields!=null && mapHistoryFields.size()>0 && acc!=trigger.oldMap.get(acc.Id)){
                for(string AccFld:mapHistoryFields.keyset()){
                    if(acc.get(AccFld) != trigger.oldMap.get(acc.Id).get(AccFld)){
                        Entity_History__c objHist = new Entity_History__c();
                        objHist.Account__c = acc.Id;
                        objHist.Field_Name__c = AccFld;
                        objHist.Date_of_Change__c = system.now();
                        objHist.User__c = userinfo.getUserId();
                        objHist.Field_Label__c = mapHistoryFields.get(AccFld);
                        if(trigger.oldMap.get(acc.Id).get(AccFld)!=null){
                            objHist.Old_Value__c = trigger.oldMap.get(acc.Id).get(AccFld)+'';
                        }else{
                            objHist.Old_Value__c = 'blank';
                        }
                        if(acc.get(AccFld)!=null){
                            objHist.New_Value__c = acc.get(AccFld)+'';
                        }else{
                            objHist.New_Value__c = 'blank';
                        }
                        lstEntHist.add(objHist);
                    }
                }
            }
        }
        
        if(lstEntHist!=null && lstEntHist.size()>0){
            try{
                insert lstEntHist;
            }catch(Exception e){
            
            }   
        }
    }
    */
}