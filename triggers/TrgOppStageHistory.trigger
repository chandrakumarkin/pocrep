trigger TrgOppStageHistory on Opportunity_Stage_History__c (before insert, after insert, before update, after update, before delete, after delete, after undelete) {

    CRM_cls_OppStageHistoryHandler.getInstance().handle();
    
}