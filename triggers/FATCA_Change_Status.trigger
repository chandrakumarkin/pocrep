trigger FATCA_Change_Status on FATCA__c (before update) {
    for(FATCA__c fatca : Trigger.New){
        if(fatca.Status__c == 'Rejected' && (fatca.First_Name__c != Trigger.OldMap.get(fatca.Id).First_Name__c
                                             || fatca.First_Name__c != Trigger.OldMap.get(fatca.Id).First_Name__c
                                             || fatca.Last_Name__c != Trigger.OldMap.get(fatca.Id).Last_Name__c
                                             || fatca.Entity_Name__c != Trigger.OldMap.get(fatca.Id).Entity_Name__c
                                             || fatca.Account_Holder_Type__c != Trigger.OldMap.get(fatca.Id).Account_Holder_Type__c
                                             || fatca.Birth_Date__c != Trigger.OldMap.get(fatca.Id).Birth_Date__c
                                             || fatca.Address__c != Trigger.OldMap.get(fatca.Id).Address__c
                                             || fatca.Country_Code_Address__c != Trigger.OldMap.get(fatca.Id).Country_Code_Address__c
                                             || fatca.US_TIN__c != Trigger.OldMap.get(fatca.Id).US_TIN__c
                                             || fatca.Account_Number__c != Trigger.OldMap.get(fatca.Id).Account_Number__c
                                             || fatca.Account_Balance__c != Trigger.OldMap.get(fatca.Id).Account_Balance__c
                                             || fatca.Payment_Amount__c != Trigger.OldMap.get(fatca.Id).Payment_Amount__c
                                             || fatca.Payment_Type__c != Trigger.OldMap.get(fatca.Id).Payment_Type__c
                                             || fatca.Account_Type__c != Trigger.OldMap.get(fatca.Id).Account_Type__c
                                            ))
            fatca.Status__c = 'Updated';
    }
}