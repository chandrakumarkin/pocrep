trigger FO_Trg_calculateBlackPoint on Violation__c (before insert, before update, after update) {
	
	list<id> listofIdSR = new list<id>();
	decimal countOfViolation = 0;
	map<id,list<Violation__c>> mapViolationandSR = new map<id,list<Violation__c>>();
	
	id contractor;
	id mainSR;
	
	if(trigger.isInsert || trigger.isUpdate){
		for(Violation__c objViolation : trigger.new){
			listofIdSR.add(objViolation.Related_Request__c);
		}  
	}
	else{
		for(Violation__c objViolation : trigger.old){
			listofIdSR.add(objViolation.Related_Request__c);
		}
	}
	system.debug('----listofIdSR---'+listofIdSR);
	Map<Id, Integer> violationQuestionCountMap = new Map<Id, Integer>();
	if(!listofIdSR.isEmpty()){
		list<Service_Request__c> tempList = [select id,Linked_SR__r.contractor__c,linked_SR__c from Service_Request__c where id IN : listofIdSR];
		if(!tempList.isEmpty()){
			contractor = tempList[0].Linked_SR__r.contractor__c;
			mainSR = tempList[0].linked_SR__c;
		}
		system.debug('----contractor---'+contractor);
		system.debug('----mainSR---'+mainSR);
		for(Violation__c sa :[SELECT id,Contractor__c,Count_of_Violations__c,Fine_Amount__c,Related_Request__c,
							  		Service_Request__c,Violation_Action__c,Violations__c,Waive_Black_Points__c,
							  		Waive_Fine_Amount__c,Service_Request__r.Linked_SR__r.contractor__c
				   		 	  from Violation__c where Service_Request__c =: mainSR]){         
	        if(violationQuestionCountMap.get(sa.Violations__c) != null){
	          Integer questionCount = violationQuestionCountMap.get(sa.Violations__c);
	          violationQuestionCountMap.put(sa.Violations__c, questionCount+1);
	        }else{
	            violationQuestionCountMap.put(sa.Violations__c, 1);
	        }
    	}
	}
	system.debug('----violationQuestionCountMap---'+violationQuestionCountMap);
	if(trigger.isInsert){
		for(Violation__c objViolation : trigger.new){
			if(string.isNotBlank(string.valueOf(contractor))){
				objViolation.contractor__c = contractor;
				objViolation.Service_Request__c = mainSR;
			}
			if(!violationQuestionCountMap.isEmpty() && violationQuestionCountMap.get(objViolation.Violations__c)!=null){
				objViolation.Count_of_Violations__c = violationQuestionCountMap.get(objViolation.Violations__c)+1;
			}
			else{
				objViolation.Count_of_Violations__c=1;
			}
	    }
	}
	
	/* V1.0 - Claude - Start */
	// If the violation is waived, an attachment must be added
	if(Trigger.isUpdate){
		
		// List<Attachment> violationAttachments = 
// 				[SELECT ParentId, 
// 						Name 
// 						FROM Attachment 
// 						WHERE ParentID IN: Trigger.newMap.keyset()]; // Query the attachments
		
// 		Set<id> attachmentParentIds = new Set<Id>(); // Set to store the parent Ids
		
// 		/* Get the parent Ids */
// 		for(Attachment violationAttachment : violationAttachments){
// 			attachmentParentIds.add(violationAttachment.ParentId);
// 		}
		
// 		/* Iterate through the keyset and check if the id is inside the keyset */
// 		for(Id k : Trigger.newMap.keyset()){
			
// 			Violation__c violationRecord = Trigger.newMap.get(k); // Get the violation record 
			
// 			if(!attachmentParentIds.contains(k) && 
// 				(violationRecord.Waive_Black_Points__c || violationRecord.Waive_Fine_Amount__c )){
// 				violationRecord.addError('Please attach a document to support the waiver.'); // Get the record where the id was not found, and trigger a validation if any of
// 																							 // the checkboxes are ticked
// 			}
// 		}
//
// 		CC_cls_FitOutandEventCustomCode.updateSrPriceLineItem(Trigger.newMap);

		if(Trigger.isAfter && CC_cls_FitOutandEventCustomCode.hasViolationAttachments(Trigger.newMap,true)){
			CC_cls_FitOutandEventCustomCode.updateSrPriceLineItem(Trigger.newMap);	
		}
	}
	
	/* V1.0 - Claude - End */
}