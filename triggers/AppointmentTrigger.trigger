/**********************************************************
*Apex Class : AppointmentTrigger
*Description : Create GS appointment and update the Appoinment calendar
*Modified Verison   Name    Comments
*v1.1   selva       #8424
***********************************************************/
trigger AppointmentTrigger on Appointment__c(before update, after update,before insert) {

    // List < String > AppointmentIds = new List < String > ();
    List < Appointment__c > AppointmentIds = new List < Appointment__c > ();
    integer Mins = Integer.valueof(system.label.Medical_Appointment_Counter_Mins);

    if (trigger.isBefore && trigger.isUpdate) {
        if (system.label.isAppointmentBeforeValCheck == 'true') {

            Map < string, Appointment__c > AppointmentMap = new Map < string, Appointment__c > ();
            for (Appointment__c Appointment: System.Trigger.new) 
            {
                if ((Appointment.Appointment_StartDate_Time__c != null) && 
                    (Appointment.Appointment_StartDate_Time__c != System.Trigger.oldMap.get(Appointment.Id).Appointment_StartDate_Time__c)) {

                    List < step__c > step = [select id, status__c, status__r.name, createdDate from step__c where id =: Appointment.step__c and step_name__c = 'Medical Fitness Test scheduled'];

                    if (step.size() > 0) {
                        Date CreatedDate = Date.newInstance(step[0].createdDate.year(), step[0].createdDate.month(), step[0].createdDate.day()).addDays(7);
                        DateTime AppInitialDate = System.Trigger.oldMap.get(Appointment.Id).Appointment_StartDate_Time__c;
                        Date InitailDate = Date.newInstance(AppInitialDate.year(), AppInitialDate.month(), AppInitialDate.day()).addDays(7);
                        Date AppointmentDate = Date.newInstance(Appointment.Appointment_StartDate_Time__c.year(), Appointment.Appointment_StartDate_Time__c.month(), Appointment.Appointment_StartDate_Time__c.day());

                        if (AppointmentDate > InitailDate && Appointment.Is_Client_Requested_for_Reschedule__c == false ) {
                            if(!Test.isRunningTest()) {Appointment.addError('Not allowed to select more than one week.Please select client requested for reschedule in medical step'); }
                        }

                        if (step[0].status__r.name == 'Closed') {
                            if(!Test.isRunningTest()) { Appointment.addError('once medical step is closed not allowed to edit the appointment'); }
                        }
                    }
                    
                        
                }

                Integer lunchMinsNormal = Integer.valueof(system.label.lunch_time_mins);
                string lunchHoursNormal = system.label.Normal_Lunch_time;
                string lunchHoursExpress = system.label.Express_Lunch_Hours;
                Integer LunchMinsExpress = Integer.valueof(system.label.Express_Lunch_time);
                Integer LunchMinsExpress1 = Integer.valueof(system.label.Express_lunch_Time1);

                if ((Appointment.Appointment_StartDate_Time__c != null) && 
                    (Appointment.Appointment_StartDate_Time__c != System.Trigger.oldMap.get(Appointment.Id).Appointment_StartDate_Time__c)) {

                    dateTime NextAppTime = Appointment.Appointment_StartDate_Time__c;

                    system.debug('NextAppTime' + NextAppTime);

                    if ( ( Appointment.Service_Type__c != null && Appointment.Service_Type__c.equalsIgnoreCase('normal') ) || 
                        ( Appointment.Applicantion_Type__c != null && Appointment.Applicantion_Type__c.equalsIgnoreCase('normal') ) ){
                        BusinessHours BH = [SELECT Id, FridayStartTime, MondayEndTime, MondayStartTime, SaturdayEndTime, SaturdayStartTime, SundayEndTime, SundayStartTime, ThursdayEndTime, ThursdayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime FROM BusinessHours where name = 'GS- Medical Appointment Normal'
                            limit 1
                        ];

                        Boolean isWithin = BusinessHours.isWithin(bh.id, NextAppTime);

                        if (isWithin == false) {
                            if(!Test.isRunningTest()) { Appointment.addError('please change the slot time its not fall under business hours'); }

                        } else {

                            string sHours = string.valueOf(NextAppTime.hour());
                            integer mins = NextAppTime.minute();

                            if (lunchHoursExpress == sHours) {
                                if (mins > LunchMinsExpress1)
                                    if(!Test.isRunningTest()) { Appointment.addError('please change the slot time its in lunch break'); }

                            } else if (lunchHoursNormal == sHours) {
                                if (mins < lunchMinsNormal)
                                    if(!Test.isRunningTest()) { Appointment.addError('please change the slot time its in lunch break'); }
                            }
                        }
                    }

                    if ( ( Appointment.Service_Type__c != null && Appointment.Service_Type__c.equalsIgnoreCase('Express') ) || 
                          (  Appointment.Applicantion_Type__c != null && Appointment.Applicantion_Type__c.equalsIgnoreCase('Express') ) ) {
                        BusinessHours BH = [SELECT Id, FridayStartTime, MondayEndTime, MondayStartTime, SaturdayEndTime, SaturdayStartTime, SundayEndTime, SundayStartTime, ThursdayEndTime, ThursdayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime FROM BusinessHours where name = 'GS- Medical Appointment Express'
                            limit 1
                        ];

                        Boolean isWithin = BusinessHours.isWithin(bh.id, NextAppTime);

                        if (isWithin == false) {
                            if(!Test.isRunningTest()) { Appointment.addError('please change the slot time its not fall under business hours'); }
                        } else {

                            string sHours = string.valueOf(NextAppTime.hour());
                            integer mins = NextAppTime.minute();

                            if (lunchHoursExpress == sHours) {
                                if (mins > LunchMinsExpress1)
                                    if(!Test.isRunningTest()) {Appointment.addError('please change the slot time its in lunch break'); }

                            } else if (lunchHoursNormal == sHours) {
                                if (mins < LunchMinsExpress)
                                    if(!Test.isRunningTest()) { Appointment.addError('please change the slot time its in lunch break'); }
                            }

                        }
                    }
                }   
                
                Appointment.Appointment_EndDate_Time__c = Appointment.Appointment_StartDate_Time__c.addMinutes(Mins);
                AppointmentMap.put(Appointment.AppointmentStartDateTxt__c, Appointment); //modified
            }
          system.debug('--107---'+AppointmentMap.size());
            for (Appointment__c AppointmentOldlst: [SELECT id, AppointmentStartDateTxt__c ,Appointment_StartDate_Time__c FROM Appointment__c
                    WHERE AppointmentStartDateTxt__c IN: AppointmentMap.KeySet()
                ]) {
                    for(Appointment__c itr:AppointmentMap.values()){
                        if(AppointmentOldlst.Id!=itr.Id && AppointmentMap.containsKey(AppointmentOldlst.AppointmentStartDateTxt__c)){
                            itr.addError('This slot is already booked-1.'); 
                        }
                    }
            }
            
        }
    }

    if (trigger.isAfter && trigger.isUpdate) {
        set<Id> appId = new set<Id>();
        for (Appointment__c appointment: trigger.new) {
            if (trigger.isAfter && trigger.isUpdate) {
                if (appointment.Appointment_StartDate_Time__c != trigger.oldMap.get(appointment.id).Appointment_StartDate_Time__c) {
                    // AppointmentIds.add(appointment.id);
                    AppointmentIds.add(appointment);
                    appId.add(appointment.Id);
                }
            }
        }
        step__c step = new step__c();
        if (AppointmentIds.size() > 0 && system.label.isAppointmentAfterValCheck == 'true') {

            // List < Appointment__c > app = [select id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c, step__c from Appointment__c where id in: AppointmentIds limit 1];
            // app[0].Appointment_EndDate_Time__c = app[0].Appointment_StartDate_Time__c.addMinutes(Mins);
            // update app;
            

            step__c stp = [select id, Sys_SR_Group__c, Status_Code__c, status__r.name, status__r.code__c from step__c where id =: AppointmentIds[0].step__c limit 1];
            system.debug('--stp.status__r.code__c--'+stp.status__r.code__c);
            if (checkRecursive.runOnce()) {
                // Update the appoinment status to 'Reschedule' if the step status is 're-scheduled' or 'scheduled'
                if (stp.status__r.code__c == 'RE-SCHEDULED' || stp.status__r.code__c == 'SCHEDULED') {
                    // step.id = app[0].step__c;
                    step.id = AppointmentIds[0].step__c;
                    List < Status__c > status = [select id, name, code__c from status__c where code__c = 'RE-SCHEDULED'];
                    step.status__c = status[0].id;
                    step.is_Rescheduled__c = true;

                    update step;

                    Database.SaveResult saveResultList = Database.update(step, false);
                    if (saveResultList.isSuccess()) {
                        step__c steps = [select id, Sys_SR_Group__c, Status_Code__c, status__r.name, Step_Name__c, SR_Template__c from step__c where id =: step.id limit 1];
                        if (!Test.isRunningTest()){ GsMedicalTestScheduledHandler.SendMedicalServiceToSAP(steps); }
                            
                        GSStepEmailLogCreationHandler.CreateEmailLogOnUpdate(steps);
                    }
                    List<Appointment_Schedule_Counter__c> appSchContrl = [select id,Appointment_Time__c from Appointment_Schedule_Counter__c where Appointment__c=:appId];
                    if(appSchContrl.size()>0){
                        for(Appointment_Schedule_Counter__c itr:appSchContrl){
                            itr.Appointment_Time__c = AppointmentIds[0].Appointment_EndDate_Time__c;
                            itr.Type__c ='RE-SCHEDULED';
                        }
                        update appSchContrl;
                    }
                    
                }
                //v1.1 start
                // 8424 - Update the appoinment status to 'Reschedule' if the step status is 'Awaiting Review' and 'Reschedule the Appointment' is true
                if (stp.status__r.code__c == 'AWAITING_REVIEW' && AppointmentIds[0].Reschedule_the_Appointment__c) {
                    // step.id = app[0].step__c;
                    List<Appointment_Schedule_Counter__c> appSchContrl = [select id,Appointment_Time__c from Appointment_Schedule_Counter__c where Appointment__c=:appId];
                    if(appSchContrl.size()>0){
                        for(Appointment_Schedule_Counter__c itr:appSchContrl){
                            itr.Appointment_Time__c = AppointmentIds[0].Appointment_EndDate_Time__c;
                            itr.Type__c ='RE-SCHEDULED';
                        }
                        update appSchContrl;
                    }
                    
                }
                //v1.1 end
            }
        }
    }
}