/******************************************************************************************************
*  Author   : Kumar Utkarsh
*  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* This class contains logic of VISA Stamping Form Creation and GDRFA callout.
*******************************************************************************************************/
public class WebServIntegrationGdrfaSubResClass {
    
    @InvocableMethod 
    public static void invokeSubmitRes(List<Id> SrIds)
    {
        System.debug('List<Id> SrIds:'+SrIds);
        Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
        for(Id SrId :SrIds){
        GDRFASubmitResidenceCallout(String.valueOf(SrId),  StatusReference.Id);
        }
    }
    
    public class MandateDocuments {
        public String applicationId;
        public List<Documents> Documents;
    }
    
    public class Documents {
        public String documentTypeId;
        public Boolean IsMandatory;
        public String documentNameEn;
        public String documentNameAr;
    } 
    
    public class UploadDoc{
        public Boolean Success{get;set;}
    }    
    
    //Fetched the SR's for Submit Residency Request and build the JSON for submission
    public static String SubmitResidence(String ID)
    {
        Step__c visaStampStep;
        Step__c entryPermitStep;
        String finalVisaStampJSON;
        String YearOfResidence;
        String DeliveryOption;
        Service_Request__c SR = [SELECT Id, Service_Type__c,Record_Type_Name__c, Nationality__c, Previous_Nationality__c, Country_of_Birth__c, Gender__c, Marital_Status__c, Religion__c, Qualification__c, Occupation__c, Passport_Type__c,
                                 First_Name__c, First_Name_Arabic__c, Middle_Name__c, Middle_Name_Arabic__c, Location__c, Last_Name__c, Last_Name_Arabic__c, Mother_Full_Name__c, P_O_Box_Emirate__c, Email__c,
                                 Mother_s_full_name_Arabic__c, Date_of_Birth__c, Place_of_Birth__c, Place_of_Birth_Arabic__c, First_Language__c, Passport_Number__c, Passport_Date_of_Issue__c, Passport_Date_of_Expiry__c,
                                 Passport_Place_of_Issue__c, Building__c, Emirates_Id_Number__c, Area__c, Street_Address__c, Current_Registered_Street__c, Mobile_Number__c, Sponsor_Mobile_No__c, Address_Details__c, Would_you_like_to_opt_our_free_couri__c, 
                                 Passport_Country_of_Issue__c, Type_of_Request__c, Identical_Business_Domicile__c, City_Town__c, Emirate__c, City__c, Building_Name__c, Phone_No_Outside_UAE__c, Establishment_Card_No__c, Visa_Duration__c
                                 FROM Service_Request__c WHERE Id =: ID];
        If(SR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_New'){
            entryPermitStep = [SELECT Id, SAP_IDNUM__c, DNRD_ID_Number__c, DNRD_ID_Value__c FROM Step__c WHERE SR__c =: ID AND Step_Status__c = 'Closed' AND Step_Name__c = 'Entry Permit is Issued' ];
            visaStampStep = [SELECT Id, DNRD_Receipt_No__c, Step_Name__c, Step_Status__c, SR__c FROM Step__c WHERE SR__c =: ID AND (Step_Status__c = 'Awaiting Review' OR Step_Status__c = 'Repush to GDRFA') AND Step_Name__c = 'VISA Stamping Form is typed' ];
        }
        if(SR.Visa_Duration__c != null){
            if(SR.Visa_Duration__c == '1 Year'){
                YearOfResidence = '1';
            }
            if(SR.Visa_Duration__c == '2 Years'){
                YearOfResidence = '2';
            }
            if(SR.Visa_Duration__c == '3 Years'){
                YearOfResidence = '3';
            }
            if(SR.Would_you_like_to_opt_our_free_couri__c == 'Yes'){
                DeliveryOption = 'Zajel Delivery';
            }
            if(SR.Would_you_like_to_opt_our_free_couri__c == 'No'){
                DeliveryOption = 'Self Collection';
            }
        }else if(SR.Visa_Duration__c == null){
            YearOfResidence = '3';
        }
        String GDRFAAreaIdLookUp;
        
        If(visaStampStep != null){
            JSONGenerator jsonVisaStampObj = JSON.createGenerator(true);
            jsonVisaStampObj.writeStartObject();
            if(visaStampStep.DNRD_Receipt_No__c != null && visaStampStep.DNRD_Receipt_No__c != ''){
                jsonVisaStampObj.writeStringField('ApplicationNumber', visaStampStep.DNRD_Receipt_No__c);
            }
            if(entryPermitStep.DNRD_ID_Number__c != null && entryPermitStep.DNRD_ID_Number__c != ''){
                jsonVisaStampObj.writeStringField('permitNo', entryPermitStep.DNRD_ID_Number__c.replaceAll('/',''));
            }else if((entryPermitStep.DNRD_ID_Number__c == null || entryPermitStep.DNRD_ID_Number__c == '') && entryPermitStep.DNRD_ID_Value__c != null && entryPermitStep.DNRD_ID_Value__c != '0' && entryPermitStep.DNRD_ID_Value__c != ''){
                jsonVisaStampObj.writeStringField('permitNo', entryPermitStep.DNRD_ID_Value__c.replaceAll('/',''));
            }
            jsonVisaStampObj.writeFieldName('applicationData');
            jsonVisaStampObj.writeStartObject();
            jsonVisaStampObj.writeFieldName('Application');
            jsonVisaStampObj.writeStartObject();
            jsonVisaStampObj.writeFieldName('ApplicantDetails');
            jsonVisaStampObj.writeStartObject();
            if(SR.First_Name__c != null ) {
                if(SR.Emirate__c != null ) {
                    jsonVisaStampObj.writeStringField('EmirateId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Emirates',SR.Emirate__c));
                    jsonVisaStampObj.writeStringField('CityId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Cities',SR.Emirate__c));
                }
                if(SR.Area__c != null ) {
                    String GdrfaAreaId =  WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas',SR.Area__c);
                    if(GdrfaAreaId != null && GdrfaAreaId != ''){
                        jsonVisaStampObj.writeStringField('AreaId', GdrfaAreaId);
                    }else if (GdrfaAreaId == null || GdrfaAreaId == ''){
                        jsonVisaStampObj.writeStringField('AreaId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas','Zabeel 1'));
                    } 
                }
                else if(SR.Area__c == null || SR.Area__c == ''){
                    jsonVisaStampObj.writeStringField('AreaId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas','Zabeel 1'));
                }
                if(SR.Street_Address__c != null ) {
                    jsonVisaStampObj.writeStringField('Street', SR.Street_Address__c);
                }
                if(SR.Building_Name__c != null ) {
                    jsonVisaStampObj.writeStringField('Building', SR.Building_Name__c);
                }
                if(SR.Current_Registered_Street__c != null ) {
                    jsonVisaStampObj.writeStringField('Landmark', 'DIFC');
                }
                jsonVisaStampObj.writeStringField('MakaniNo', '');
                if(SR.Sponsor_Mobile_No__c != null ) {
                    jsonVisaStampObj.writeStringField('MobileNo', SR.Sponsor_Mobile_No__c);
                }
                if(YearOfResidence != null ) {
                    jsonVisaStampObj.writeStringField('ResidenceRequestYear', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Years Of Residence',YearOfResidence));
                }
            }
            jsonVisaStampObj.writeEndObject();
            jsonVisaStampObj.writeFieldName('SponsorDetails');
            jsonVisaStampObj.writeStartObject();
            if(SR.Email__c != null){
                jsonVisaStampObj.writeStringField('SponsorEmail', 'gdrfanotifications@difc.ae');
            }
            if(SR.Sponsor_Mobile_No__c != null){
                jsonVisaStampObj.writeStringField('MobileNo', SR.Sponsor_Mobile_No__c);
            }
            if(SR.Establishment_Card_No__c != null){
                jsonVisaStampObj.writeStringField('EstCode', SR.Establishment_Card_No__c.replaceAll('/',''));
            }
            jsonVisaStampObj.writeEndObject();
            jsonVisaStampObj.writeFieldName('ApplicationDetails');
            jsonVisaStampObj.writeStartObject();
            jsonVisaStampObj.writeStringField('ResidencyPickup', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Delivery Method','Self Collection'));
            jsonVisaStampObj.writeStringField('DeliveryEmirateId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Emirates','Dubai'));
            jsonVisaStampObj.writeStringField('DeliveryCityId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Cities','Dubai'));
            jsonVisaStampObj.writeStringField('DeliveryAreaId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas','Zabeel 1'));
            jsonVisaStampObj.writeStringField('DeliveryStreet', 'Shaikh Zayed Road');
            jsonVisaStampObj.writeStringField('DeliveryBuilding', 'DIFC');
            jsonVisaStampObj.writeStringField('DeliveryFloorNo', '14');
            jsonVisaStampObj.writeStringField('DeliveryFlatNo', '14');
            jsonVisaStampObj.writeStringField('DeliveryLandmark', 'Emirates Tower');
            jsonVisaStampObj.writeStringField('DeliveryMakaniNo', '');
            jsonVisaStampObj.writeBooleanField('isDraft', False);
            jsonVisaStampObj.writeEndObject();
            jsonVisaStampObj.writeEndObject();
            jsonVisaStampObj.writeEndObject();
            jsonVisaStampObj.writeEndObject();
            finalVisaStampJSON = jsonVisaStampObj.getAsString();
            System.debug('DEBUG Full Request finalJSON : ' +(jsonVisaStampObj.getAsString()));
        }
        WebServGDRFAAuthFeePaymentClass.masterDataMap = null;
        return finalVisaStampJSON; 
    }
    
    @future(callout=true)
    //Callout method for Submit Residence
    public static void GDRFASubmitResidenceCallout(String ID, String StatusReference) 
    { 
        Step__c visaStampStep;
        map<Id,Id> mapStepStepTemplate = new map<Id,Id>();
        String visaStampJson;
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf('Error before Json Creation');
        attachment.Name = 'Visa Stamp Json.txt';
        attachment.ParentId = ID;
        attachment.ContentType = 'text/plain';
        //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
        try{
            String finalDocJSON;
            Boolean isAllUploadSuccess = false;
            MandateDocuments responseDocList;
            visaStampStep = [SELECT Id, DNRD_Receipt_No__c, Step_Template__c, SR_Step__c, Step_Name__c, Step_Status__c, SR__c FROM Step__c WHERE SR__c =: ID AND (Step_Status__c = 'Awaiting Review' OR Step_Status__c = 'Repush to GDRFA') AND Step_Name__c = 'VISA Stamping Form is typed' ];
            WebServGDRFAAuthFeePaymentClass.ResponseBasicAuth ObjResponseAuth = WebServGDRFAAuthFeePaymentClass.GDRFAAuth();
            for(SR_Steps__c obj: [select id, Step_Template__c from SR_Steps__c where Step_Template_Code__c IN('Online entry permit is typed', 'Entry permit form is typed', 'Renewal form is typed', 'Cancellation Form Typed', 'Entry Permit is Issued', 'Completed', 'Passport is Ready for Collection', 'Visa Stamping Form is Typed') AND SR_Template__r.SR_Template_Code__c IN ('DIFC_Sponsorship_Visa_New','DIFC_Sponsorship_Visa_Renewal', 'DIFC_Sponsorship_Visa_Cancellation') ]){
                mapStepStepTemplate.put(obj.Step_Template__c,obj.Id);                                                                        
            }
            if(mapStepStepTemplate.containsKey(visaStampStep.Step_Template__c) && visaStampStep.SR_Step__c == null){
                visaStampStep.SR_Step__c = mapStepStepTemplate.get(visaStampStep.Step_Template__c);
            }
            List<Log__c> ErrorObjLogList = new List<Log__c>();
            //Get authrization code 
            string access_token= ObjResponseAuth.access_token;
            string token_type = ObjResponseAuth.token_type;
            //WebService to Push the Submit Residence Form
            HttpRequest request = new HttpRequest();
            request.setEndPoint(System.label.GDRFA_Submit_Residence_Endpoint); 
            visaStampJson = SubmitResidence(ID);
            attachment.Body = Blob.valueOf(visaStampJson);
            attachment.ParentId = visaStampStep.Id;
            request.setBody(visaStampJson);
            request.setTimeout(120000);
            request.setHeader('apikey', System.label.GDRFA_API_Key);
            request.setHeader('Content-Type', 'application/json');
            request.setMethod('POST');
            request.setHeader('Authorization',token_type+' '+access_token);
            HttpResponse response = new HTTP().send(request);
            System.debug('responseStringresponse:'+response.toString());
            System.debug('STATUS:'+response.getStatus());
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('strJSONresponseSubmitResidence:'+response.getBody());
            request.setTimeout(101000);
            if (response.getStatusCode() == 200)
            {
                responseDocList = (MandateDocuments) System.JSON.deserialize(response.getBody(), MandateDocuments.class);
                System.debug('applicationId:'+responseDocList.applicationId);
                System.debug('Documents:'+responseDocList.Documents);
                Boolean mandateDocsExist = false;
                Set<String> setGDRFADocId = new Set<String>();
                map<Id, String> mapSRDocIdGDRFADocId = new map<Id, String>();
                map<Id, String> mapSRDocIdGDRFAname = new map<Id, String>();
                map<Id, String> mapSRDocIdGDRFAArabicName = new map<Id, String>();
                map<String, String> mapGdrfaDocIdGDRFAname = new map<String, String>();
                map<String, String> mapGdrfaDocIdGDRFAArabicname = new map<String, String>();
                map<String, Boolean> mapGdrfaDocIdGdrfaIsMandate = new map<String, Boolean>();
                map<String, Boolean> mapSRDocIdGdrfaIsMand = new map<String, Boolean>();
                map<Id, Boolean> mapSRDocMandate = new map<Id, Boolean>();
                if(responseDocList.Documents != null){
                    For(Documents docList : responseDocList.Documents){
                        setGDRFADocId.add(docList.documentTypeId);
                        mapGdrfaDocIdGdrfaIsMandate.put(docList.documentTypeId, docList.IsMandatory);
                        mapGdrfaDocIdGDRFAname.put(docList.documentTypeId, docList.documentNameEn);
                        mapGdrfaDocIdGDRFAArabicname.put(docList.documentTypeId, docList.documentNameAr);
                    }
                }
                System.debug('mapGdrfaDocIdGdrfaIsMandate:'+mapGdrfaDocIdGdrfaIsMandate);
                for(SR_Doc__c objDoc:  [SELECT GDRFA_Attchment_Doc_Id__c, GDRFA_Parent_Document_Id__c FROM SR_Doc__c
                                        WHERE Service_Request__c =: ID AND GDRFA_Parent_Document_Id__c IN: setGDRFADocId AND GDRFA_Parent_Document_Id__c != null]){
                                            mapSRDocIdGDRFADocId.put(objDoc.GDRFA_Attchment_Doc_Id__c, objDoc.GDRFA_Parent_Document_Id__c);
                                            mapSRDocIdGDRFAname.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGDRFAname.get(objDoc.GDRFA_Parent_Document_Id__c));
                                            mapSRDocIdGdrfaIsMand.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGdrfaIsMandate.get(objDoc.GDRFA_Parent_Document_Id__c));
                                            mapSRDocIdGDRFAArabicName.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGDRFAArabicname.get(objDoc.GDRFA_Parent_Document_Id__c));
                                        }
                for(Attachment objAttch: [SELECT Id, Name, ContentType, Body from Attachment where Id IN:mapSRDocIdGDRFADocId.keySet()]){
                    //Webservice to push Sr docs
                    JSONGenerator docJsonObj = JSON.createGenerator(true);
                    docJsonObj.writeStartObject();
                    docJsonObj.writeFieldName('Documents');
                    docJsonObj.writeStartObject();
                    System.debug('objAttch:'+objAttch);
                    System.debug('mapSRDocIds1:'+mapSRDocIdGDRFADocId.get(objAttch.Id));
                    docJsonObj.writeObjectField('documentTypeId', mapSRDocIdGDRFADocId.get(objAttch.Id));
                    if(objAttch.Body != null){
                        docJsonObj.writeBlobField('document', objAttch.Body);  
                    }
                    docJsonObj.writeBooleanField('IsMandatory', mapSRDocIdGdrfaIsMand.get(objAttch.Id)); 
                    if(objAttch.ContentType != null){
                        docJsonObj.writeStringField('MineType', objAttch.ContentType);
                    }
                    docJsonObj.writeStringField('documentNameEn', mapSRDocIdGDRFAname.get(objAttch.Id)); 
                    docJsonObj.writeStringField('documentNameAr', mapSRDocIdGDRFAArabicName.get(objAttch.Id)); 
                    docJsonObj.writeStringField('FileName', objAttch.Name); 
                    docJsonObj.writeEndObject();
                    docJsonObj.writeStringField('applicationId', responseDocList.applicationId);
                    docJsonObj.writeEndObject();
                    finalDocJSON = docJsonObj.getAsString();
                    System.debug('DEBUG Full Request finalJSON : ' +(docJsonObj.getAsString()));
                    HttpRequest requestUploadDoc = new HttpRequest();
                    requestUploadDoc.setEndPoint(System.label.GDRFA_Document_Upload_Endpoint);        
                    requestUploadDoc.setBody(docJsonObj.getAsString());
                    requestUploadDoc.setTimeout(120000);
                    requestUploadDoc.setHeader('apikey', System.label.GDRFA_API_Key);
                    requestUploadDoc.setHeader('Content-Type', 'application/json');
                    requestUploadDoc.setMethod('POST');
                    requestUploadDoc.setHeader('Authorization',token_type+' '+access_token);
                    HttpResponse responseUploadDoc = new HTTP().send(requestUploadDoc);
                    System.debug('responseUploadDoc:'+responseUploadDoc.toString());
                    System.debug('STATUS:'+responseUploadDoc.getStatus());
                    System.debug('STATUS_CODE:'+responseUploadDoc.getStatusCode());
                    System.debug('strJSONresponseUploadDoc:'+responseUploadDoc.getBody());
                    if (responseUploadDoc.getStatusCode() == 200)
                    {
                        UploadDoc docUpload = (UploadDoc) System.JSON.deserialize(responseUploadDoc.getBody(), UploadDoc.class);
                        System.debug('IsSuccess:'+docUpload.Success);
                        if(docUpload.Success){
                            isAllUploadSuccess = true;   
                        }else{
                            isAllUploadSuccess = false;
                        }   
                    }
                    else{
                        Log__c ErrorobjLog = new Log__c();
                        ErrorobjLog.Type__c = objAttch.Id+'-'+mapSRDocIdGDRFAname.get(objAttch.Id)+'-'+objAttch.Id+'-Document Upload Error';
                        ErrorobjLog.Description__c = 'Response: '+responseUploadDoc.getBody();
                        ErrorobjLog.SR_Step__c = visaStampStep.Id;
                        ErrorObjLogList.add(ErrorobjLog);
                        isAllUploadSuccess = false;    
                    }
                }
                if(!ErrorObjLogList.isEmpty() && ErrorObjLogList != null){
                    try{
                        insert ErrorObjLogList;
                    }catch(Exception ex) {
                        System.Debug(ex);
                    }
                    isAllUploadSuccess = false;
                    //insert attachment;
                    //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
                    visaStampStep.Status__c = StatusReference;
                    visaStampStep.OwnerId = UserInfo.getUserId();
                    visaStampStep.Step_Notes__c = 'Document Upload Error';
                    try{
                        Update visaStampStep;
                    }catch(Exception ex) {
                        insert LogDetails.CreateLog(null, 'Error on VISA Stamping', 'SR Id is ' + visaStampStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                    }
                    
                }   
            }
            else {
                WebServGDRFAAuthFeePaymentClass.ErrorClass ErrorLog = (WebServGDRFAAuthFeePaymentClass.ErrorClass) System.JSON.deserialize(response.getBody(), WebServGDRFAAuthFeePaymentClass.ErrorClass.class);
                Log__c objLog = new Log__c();objLog.Type__c = 'Submit Residence Error ';objLog.Description__c = 'Request: '+visaStampJson +'Response: '+ response.getBody();objLog.SR_Step__c = visaStampStep.Id;insert objLog; 
                //insert attachment;
                //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
                visaStampStep.Status__c = StatusReference;
                visaStampStep.OwnerId = UserInfo.getUserId();
                system.debug('ErrorLog:'+ErrorLog);
                if(ErrorLog != null){
                    if(ErrorLog.errorMsg[0].messageEn != null){
                        visaStampStep.Step_Notes__c = ErrorLog.errorMsg[0].messageEn;
                    }
                }
                try{
                    update visaStampStep;
                }catch(Exception ex) {
                    insert LogDetails.CreateLog(null, '-Error on VISA Stamping-', 'SR Id is ' + visaStampStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                }
                
            }
            //Webservice to get the Application Fee 
            if(isAllUploadSuccess){
                WebServGDRFAAuthFeePaymentClass.getApplicationFeeAPI(responseDocList.applicationId, access_token, token_type, ID, visaStampStep, StatusReference);
                //insert attachment;
            }
            insert attachment;
        }catch(exception e){
            insert attachment;
            system.debug('Error:- ' + e); 
            visaStampStep.Status__c = StatusReference;
            visaStampStep.OwnerId = UserInfo.getUserId();
            visaStampStep.Step_Notes__c = 'Error:- ' + e;
            try{
                Update visaStampStep; 
            }catch(Exception ex) {
                insert LogDetails.CreateLog(null, '-Error on VISA Stamping Exception-', 'SR Id is ' + visaStampStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
            }
            Log__c objLog = new Log__c();objLog.Type__c = 'VISA Stamp' + e;objLog.Description__c = 'Error:- ' + e;objLog.SR_Step__c = visaStampStep.Id;insert objLog; 
        }
    }
}