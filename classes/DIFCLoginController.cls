/***************************************************************************************************************************
    Name        :   DIFCLoginController
    Author      :   Bilal
    Date        :   21-12-2014
    Description :   An apex page controller that exposes the site login functionality
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    12-Nov-2015    Diana        Added new method to redirect to mobile page on session valid
    V1.1    01-Sep-2016    Kaavya       Updated the controller to allow direct login to different page
    
*****************************************************************************************************************************/
 
global with sharing class DIFCLoginController {
    global String username{get;set;}
    global String password{get;set;}
    global String selectlstVal{get;set;}
    global DIFCLoginController () {}
    global PageReference forwardToCustomAuthPage() {
         
       
        /*return new PageReference( Label.Community_URL + '/signin');*/        
        PageReference landingPage = new PageReference( Site.getBaseSecureUrl() + '/signin');
       
        landingPage.setRedirect(true);
        return landingPage; 
    }
    global PageReference login() {
        String startUrl = System.currentPageReference().getParameters().get('startURL'); // V1.1 uncommented this line
        //v1.1 start
        //return Site.login(username, password, '/DIFCLanding');
        String addurl='';
        if (startUrl!=null){
            startUrl=startUrl.remove('/customers');
            addurl='?add='+startUrl;
        }
        return Site.login(username, password, '/DIFCLanding'+addurl);
        
        //v1.1 end
        
        
    }
    
   
    public PageReference redirectToMobilePage(){
                PageReference landingPage = null;
        //Diana:Added below line to redirect to mobile loginpage if session is invalid

        System.Cookie cookie = ApexPages.currentPage().getCookies().get('mobile_cookie');
        System.debug('mobile entered');
        if(null!=cookie)
        System.debug('mobile'+cookie.getValue());
        
        if(null!=cookie && cookie.getValue() != null){
            return new PageReference(Site.getBaseSecureUrl() + '/MobileCustomLogin');
        }
        return null;
        
    }
    
    public PageReference RedirectToUserPage(){
        PageReference redirectPage;
        if(selectlstVal=='DIFC Clients'){
            redirectPage = new PageReference(Site.getpathprefix()+'/User_Access_Form');
        }else if(selectlstVal=='IT services'){
            redirectPage = new PageReference(Site.getpathprefix()+'/ITAccessForm');
        }else if(selectlstVal=='Non DIFC Clients'){
            redirectPage = new PageReference(Site.getpathprefix()+'/RORPUserForm');
        }else if(selectlstVal=='Non DIFC Clients - Property Listing'){
            redirectPage = new PageReference(Site.getpathprefix()+'/Property_Listing_User_Form');
        }else if(selectlstVal=='Events'){
            redirectPage = new PageReference(Site.getpathprefix()+'/EventRequestAccessForm');
        }
        redirectPage.setRedirect(true);
        return redirectPage;
    }
}