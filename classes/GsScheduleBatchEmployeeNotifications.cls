//V1.1  Swati   05/10/2016  As per ticket 2851
//V1.2  Swati   15/10/2016  As per ticket 3764
//V1.2  Shoaib  15/03/2021  As per ticket DIFC2-14586

global with sharing class GsScheduleBatchEmployeeNotifications implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable {
    
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new GsScheduleBatchEmployeeNotifications(), 100);
    }
    
    global list<Contact> start(Database.BatchableContext BC ){
        Date expiryDate5                  = system.today().addDays(5);
        Date expiryDate                   = system.today();
        Date expiryDate15                 = system.today().addDays(-15);
        Date expiryDateTemp               = system.today().addMonths(1);
        
        checkEstablishmentCardExpiry();
        
        list<Contact> lstContacts = [select Id,Name,AccountId from Contact where RecordType.DeveloperName = 'GS_Contact' 
            AND Id IN (select Contact__c from Document_Details__c where (EXPIRY_DATE__c =: expiryDate5 OR EXPIRY_DATE__c =: expiryDate OR EXPIRY_DATE__c =:expiryDate15) AND SAP_CHGTYP__c  != 'D' AND Document_Type__c = 'Employment Visa' AND Contact__c != null)
            AND Id NOT IN (select Contact__c from Document_Details__c where (EXPIRY_DATE__c >: expiryDateTemp) AND Document_Type__c = 'Employment Visa' AND Date_Active__c = true AND Contact__c != null)
            AND AccountId != null AND Is_Absconder__c = false];
        return lstContacts;
    }
    
    global void execute(Database.BatchableContext BC, list<Contact> lstContacts){
    
        list<Id> lstAccountIds                  = new list<Id>();
        list<Id> contactIdss                     = new list<Id>();
        map<Id,list<string>> mapEmpServiceUsers = new map<Id,list<string>>();
        list<Contact> lstContactInsert          = new list<Contact>();
        
        for(Contact objCon : lstContacts){
            contactIdss.add(objCon.Id);
        }
        
        for(Relationship__c  thisRel : [SELECT Id,Object_Contact__c,
                                                Subject_Account__c
                                                FROM Relationship__c 
                                                WHERE Relationship_Type__c ='Has DIFC Sponsored Employee' 
                                                AND Object_Contact__c IN : contactIdss]){
              lstAccountIds.add(thisRel.Subject_Account__c); 
                        
         }
         
        
        List<User> usr                                       = new List<User>();
        Set<String> contactIds                               = new Set<String>(); 
        Map<String,AccountContactRelation> accountContactMap = new  Map<String,AccountContactRelation>();
             
        for(AccountContactRelation ACR: [Select Id,contactId,Roles from AccountContactRelation where AccountId  IN : lstAccountIds and isActive = true ]){
            contactIds.add(ACR.contactId);
            accountContactMap.put(ACR.contactId,ACR);
         }
 
        for(User thisUser : [select Id,ContactId,Contact.AccountId,Community_User_Role__c FROM User where ContactId IN : contactIds and IsActive = true ]){
            
            if(accountContactMap.containsKey(thisUser.ContactId) &&
               accountContactMap.get(thisUser.ContactId).Roles != null &&
               accountContactMap.get(thisUser.ContactId).Roles.Contains('Employee Services')){
                            
               list<string> lstUserIds = mapEmpServiceUsers.containsKey(thisUser.Contact.AccountId) ? mapEmpServiceUsers.get(thisUser.Contact.AccountId) : new list<string>();
               lstUserIds.add(thisUser.Id);
                     
               if(lstUserIds!=null && !lstUserIds.isEmpty()){
                   mapEmpServiceUsers.put(thisUser.Contact.AccountId,lstUserIds);
                }
                     
               }
         }
            
             
        for(Contact objCon : lstContacts){
            list<string> lst = mapEmpServiceUsers.get(objCon.AccountId);
            if(lst != null && !lst.isEmpty()){
                Contact objTemp = new Contact(Id=objCon.Id);
                objTemp.Sys_Send_Email__c = true;
                if(lst.size() > 0)
                    objTemp.Portal_User_1__c = lst[0];
                if(lst.size() > 1)
                    objTemp.Portal_User_2__c = lst[1];
                if(lst.size() > 2)
                    objTemp.Portal_User_3__c = lst[2];
                if(lst.size() > 3)
                    objTemp.Portal_User_4__c = lst[3];
                if(lst.size() > 4)
                    objTemp.Portal_User_5__c = lst[4];
                lstContactInsert.add(objTemp);
            }
        }
        if(!lstContactInsert.isEmpty()){
            update lstContactInsert;
        }
    }
    
    global void finish(Database.BatchableContext BC){
    }
    
    public void checkEstablishmentCardExpiry(){//V1.1
        list<Id> lstAccountIds                                        = new list<Id>();
        map<Id,list<string>> mapEmpServiceUsers                       = new map<Id,list<string>>();
        list<Identification__c> lstIdentificationUpdate               = new list<Identification__c>();
        
        Date expiryDate5                     = system.today().addDays(5);
        Date expiryDate                      = system.today();
        Date expiryDate15                    = system.today().addDays(15);
        Date expiryDateTemp                  = system.today().addMonths(1);
        
        list<Identification__c> lstIdentification    = [select Account__c from Identification__c where (Valid_To__c =: expiryDate5 OR Valid_To__c =: expiryDate OR Valid_To__c =:expiryDate15) and Account__r.Index_Card_Status__c =: 'Active'];//V1.2
        
        if(lstIdentification!=null && !lstIdentification.isEmpty()){
        
            for(Identification__c obj : lstIdentification){
                lstAccountIds.add(obj.Account__c);
            }
            
            if(lstAccountIds!=null && !lstAccountIds.isEmpty()){
             List<User> usr            = new List<User>();
             Set<String> contactIds    = new Set<String>(); 
             
             Map<String,AccountContactRelation> accountContactMap = new  Map<String,AccountContactRelation>();
             
              for(AccountContactRelation ACR: [Select Id,contactId,Roles from AccountContactRelation where AccountId  IN : lstAccountIds and  isActive = true ]){
                  contactIds.add(ACR.contactId);
                  accountContactMap.put(ACR.contactId,ACR);
               }
 
                for(User thisUser : [select Id,ContactId,Contact.AccountId,Community_User_Role__c FROM User where ContactId IN : contactIds and IsActive = true ]){
                     if(accountContactMap.containsKey(thisUser.ContactId) &&
                            accountContactMap.get(thisUser.ContactId).Roles != null &&
                            accountContactMap.get(thisUser.ContactId).Roles.Contains('Employee Services')){
                            
                     list<string> lstUserIds = mapEmpServiceUsers.containsKey(thisUser.Contact.AccountId) ? mapEmpServiceUsers.get(thisUser.Contact.AccountId) : new list<string>();
                     lstUserIds.add(thisUser.Id);
                     
                     if(lstUserIds!=null && !lstUserIds.isEmpty()){
                        mapEmpServiceUsers.put(thisUser.Contact.AccountId,lstUserIds);
                     }
                     
                   }
                 }
             }   
            
            if(mapEmpServiceUsers!=null && !mapEmpServiceUsers.isEmpty()){
                for(Identification__c obj : lstIdentification){
                    list<string> lst = mapEmpServiceUsers.get(obj.Account__c);
                    if(lst != null && !lst.isEmpty()){
                        Identification__c objTemp = new Identification__c(Id=obj.Id);
                        objTemp.Send_Expiry_Mail__c = true;
                        if(lst.size() > 0)
                            objTemp.Portal_User_1__c = lst[0];
                        if(lst.size() > 1)
                            objTemp.Portal_User_2__c = lst[1];
                        if(lst.size() > 2)
                            objTemp.Portal_User_3__c = lst[2];
                        if(lst.size() > 3)
                            objTemp.Portal_User_4__c = lst[3];
                        if(lst.size() > 4)
                            objTemp.Portal_User_5__c = lst[4];
                        lstIdentificationUpdate.add(objTemp);
                    }
                }
            }
            
            if(lstIdentificationUpdate!=null && !lstIdentificationUpdate.isEmpty()){
                update lstIdentificationUpdate;
            }
        }
    }
}