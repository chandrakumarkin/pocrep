@isTest
public class InspectionTriggerTest {
    
    @testSetup static void setupData() {
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc121';
        objAcc.RecordTypeId = [select Id,Name,DeveloperName from RecordType where DeveloperName ='Contractor_Account' AND SobjectType='Account' limit 1].id;
        insert objAcc;
        
        Inspection__c thisInspection             = new Inspection__c();
        thisInspection.Company_Name__c           = objAcc.Id;
        thisInspection.Date_of_Inspection__c     = system.today() + 5;
        thisInspection.Routine_Inspection__c     = 'Yes';
        thisInspection.Special_Inspection_at_ROC_s_Request__c = 'Yes';
        thisInspection.Status__c   = 'Draft';
        thisInspection.NonRegulated_Q1__c 		= 'Yes';
        thisInspection.NonRegulated_Q2__c 		= 'Yes';
        thisInspection.NonRegulated_Q3__c 		= 'Yes';
        thisInspection.NonRegulated_Q4__c 		= 'Yes';
        thisInspection.LLP_Q11__c 				= 'Yes';
        thisInspection.LLP_Q10__c 				= 'Yes';
        thisInspection.NonRegulated_Q5__c 		= 'Yes';
        thisInspection.NonRegulated_Q6__c 		= 'Yes';
        thisInspection.NonRegulated_Q7__c 		= 'Yes';
        thisInspection.NonRegulated_Q8__c 		= 'Yes';
        thisInspection.NonRegulated_Q9__c 		= 'Yes';
        thisInspection.NonRegulated_Q10__c		= 'Yes';
        thisInspection.NonRegulated_Q11__c      = 'Yes';
        thisInspection.NonRegulated_Q12__c      = 'Yes';
        thisInspection.NonRegulated_Q13__c      = 'Yes';
        thisInspection.NonRegulated_Q14__c      = 'Yes';
        thisInspection.Is_the_auditor_appointed__c = 'Yes';
        thisInspection.Please_request_for_the_name_and_details__c = 'Yes';
        thisInspection.Is_it_approved_or_registered_auditor__c = 'Yes';
        thisInspection.Is_the_entity_maintaining_its_accounting__c = 'Yes';
        thisInspection.NonRegulated_Q19__c = 'Yes';
        thisInspection.NonRegulated_Q20__c = 'Yes';
        thisInspection.NonRegulated_Q21__c = 'Yes';
        thisInspection.NonRegulated_Q22__c = 'Yes';
        thisInspection.NonRegulated_Q23__c = 'Yes';
        thisInspection.NonRegulated_Q24__c = 'Yes';
        thisInspection.FRCNonRegulated_Q9__c = 'Yes';
        thisInspection.FRCNonRegulated_Q10__c = 'Yes';
        thisInspection.FRCNonRegulated_Q13__c = 'Yes';
        insert thisInspection;
       }
    //unit testing for updateStatusToApproved method
    @isTest  static void testInspectionUpdate() {
         test.startTest();
             Inspection__c thisInspection  = [SELECT Id,Status__c FROM Inspection__c WHERE Company_Name__r.Name = 'Test Acc121' LIMIT 1];
             thisInspection.Status__c ='Approved';
             update thisInspection;
         test.stopTest();
    }
    //unit testing for updateStatusToApproved method
    @isTest  static void testInspectionvalidation1() {
         test.startTest();
             Inspection__c thisInspection  = [SELECT Id,Status__c FROM Inspection__c WHERE Company_Name__r.Name = 'Test Acc121' LIMIT 1];
             thisInspection.Status__c ='Awaiting for Client Response';
             update thisInspection;
          try{
             thisInspection.Status__c = 'Submitted for Approval';
             update thisInspection;
          }
          catch(Exception ex){
                  
           }
         test.stopTest();
    }
    //unit testing for updateStatusToApproved method
    @isTest  static void testInspectionvalidation2() {
         test.startTest();
             Inspection__c thisInspection  = [SELECT Id,Status__c FROM Inspection__c WHERE Company_Name__r.Name = 'Test Acc121' LIMIT 1];
             thisInspection.Status__c ='Awaiting for Client Response';
             thisInspection.NonRegulated_Q24__c ='NO';
             update thisInspection;
          try{
             thisInspection.Status__c = 'Submitted for Approval';
             update thisInspection;
          }
          catch(Exception ex){
                  
           }
         test.stopTest();
    }
}