@isTest
public with sharing class OB_DocumentPreviewControllerTest {
    
    public static testmethod void testMethod1(){
            
            /* create test data here */ 
    
            // create account
            List<Account> insertNewAccounts = new List<Account>();
            insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
            insert insertNewAccounts;
    
            //create contact
            Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
            insert con; 
    
            test.startTest();
            
            
            Id profileToTestWith = [select id from profile where name='DIFC Customer Community User Custom'].id;      
            
            
            User user = new User(alias = 'test123', email='test123@noemail.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profileToTestWith, country='United States',IsActive =true,
                                 ContactId = con.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user;
            system.runAs(user){
                
                 OB_DocumentPreviewController.RequestWrapper reqWrapper = new OB_DocumentPreviewController.RequestWrapper();
                OB_DocumentPreviewController.RespondWrap repWrapper = new OB_DocumentPreviewController.RespondWrap();
                
                reqWrapper.srId = 'test';
                String requestString = JSON.serialize(reqWrapper);
                
                repWrapper = OB_DocumentPreviewController.getSRDocs(requestString); 
  
            }
                                    
        }
}