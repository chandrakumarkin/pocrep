/*
        Author      :   Arun 
        Description :   Class to suport Lease Registration Security Deposit Service request 
        
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    29-06-2020 Arun Singh  Created
    
*/    

public class PaySecurityDepositcontroller {

public string access_key{get;set;}
public string profile_id{get;set;}
public string transaction_uuid{get;set;}
public string unsigned_field_names{get;set;}
public string signed_date_time{get;set;}
public string reference_number{get;set;}
public string amount{get;set;}
public string currencyvall{get;set;}
public string signature{get;set;}
private Map<String,String> oPassedParams = new Map<String,String>();

public Boolean IsValid{get;set;}


public string message{get;set;}

string secretKey=Label.ADCBsecretKey;//'862d5cf7c4cb4832bb9e3212e5c90e7c6088602a286a4abda783eb3ae5da6304bcdf2461495941f39f756c255d21e412c157163a0c454b6eb1b8203d648daf5b29941a24b0024fab9b76cad1c41f198a30c8e4f415d946c9ba87fc51da63e2a951fb65d7c76d4e5b99bc89e89b7b74bd82ced19e583c46e2bce7f10ff2e16480';

String SRID;
//string secretKey='8b6b614b9a46422bb254585ea14d0d19f2f830d48118407ba8fc24f27c1f95b232da82c53a124fa9925f7dc1c6c8d86982c2c855402e4d2bbd53de9c92f46a4e1264c6c2739242ffa30b7727bfce801e5311e89446eb4a0880f54fbeeb86498acc5e068d33c14a57a07ab6f9442ebfafd8d3481b633541be83b7102c76691358';
 public PaySecurityDepositcontroller ()
 {  
     SRID= ApexPages.currentPage().getParameters().get('id');
     IsValid=true;
  }  
    

public Void PageLoad()
{

Service_Request__c ObjSR=[select Sys_Estimated_Share_Capital__c,Name,Id from Service_Request__c where id=:SRID];

// List<Step__c> ObjStep=[select Id from Step__c where SR__c=:ObjSR.id and Step_Code__c='SECURITY_DEPOSIT_PAYMENT' and Closed_Date_Time__c=null limit 1];
  //         if(!ObjStep.isEmpty())
          // {
               
               //oPassedParams.put('access_key','ac12abb2254f3af781877d4767b957c9');
    //oPassedParams.put('access_key','ac12abb2254f3af781877d4767b957c9');
 

access_key=Label.ADCB_access_key;//'ac12abb2254f3af781877d4767b957c9';
profile_id=Label.ADCB_profile_id;//'F852B06D-14BB-4F28-A1FB-E850222BFB51';

transaction_uuid=getUUID();
signed_date_time= getUTCDateTime();

reference_number=ObjSR.Id;
amount=string.valueOf(ObjSR.Sys_Estimated_Share_Capital__c);
currencyvall='aed';
unsigned_field_names='';

    oPassedParams.put('access_key',access_key);
    oPassedParams.put('profile_id',profile_id);
    oPassedParams.put('transaction_uuid',transaction_uuid);

    oPassedParams.put('signed_field_names','access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency');

    oPassedParams.put('unsigned_field_names',unsigned_field_names);
    oPassedParams.put('signed_date_time',signed_date_time);
    oPassedParams.put('locale','en');
    oPassedParams.put('transaction_type','sale');
    oPassedParams.put('reference_number',reference_number);
    oPassedParams.put('amount',amount);
    oPassedParams.put('currency',currencyvall);
  // oPassedParams.put('submit','Submit');

    
//message=CreateData();
// Blob data = crypto.generateMac('HmacSHA256', Blob.valueOf(message), EncodingUtil.convertFromHex(secretKey));
  //signature=EncodingUtil.base64Encode(data);
  
  //message=buildDataToSign(oPassedParams);
 //signature = EncodingUtil.base64Encode(Crypto.generateMac('HmacSHA256', Blob.valueOf(message), Blob.valueOf(secretKey)));
  
               
          // }
           //else
          // {
              // IsValid=false;
           //}
    
  
  

}

 public String getSignedData(){
        String result = '';
        result += '\n <input type="hidden" id="signature" name="signature" value="' + sign(buildDataToSign(oPassedParams),secretKey) + '"/>';
        system.debug('-- getSignedData' + result);
        return result;
    }
    
    private static String sign(String data, String secretKey) 
    {
        String result = EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(data), Blob.valueOf(secretKey)));
        return result;
    }
    
 public String getParametersValuesHidden(){
        String result = '';
        for (String oKey : oPassedParams.KeySet()){ 
            result += '\n <input type="hidden" id="' + oKey + '" name="' + oKey + '" value="' + oPassedParams.get(oKey) + '" />';
        }
        system.debug('--ParametersValuesHidden' + result);
        return result;
    }

  private static String buildDataToSign(Map<string,string> paramsArray) 
  {
        String[] signedFieldNames = paramsArray.get('signed_field_names').Split(',');
        List<string> dataToSign = new List<string>();

        for (String oSignedFieldName : signedFieldNames){
             dataToSign.Add(oSignedFieldName + '=' + paramsArray.get(oSignedFieldName));
        }
        return commaSeparate(dataToSign);
    }
      private static String commaSeparate(List<string> dataToSign) {
        String result = '';
        for(String Str : dataToSign) {
            result += (result==''?'':',')+Str;
        }
        return result;                         
    }
    
private static String getUTCDateTime() 
{
        DateTime oUTSDateTime = System.now().addHours(-4);
        String strUTCDateTime = oUTSDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        return strUTCDateTime;
   } 

   public String getUUID(){
        Blob b = Crypto.generateAesKey(128);
        String h = EncodingUtil.convertToHex(b);
        String guid = h.substring(0,8) + '-' + h.substring(8,12) + '-' + h.substring(12,16) + '-' + h.substring(16,20) + '-' + h.substring(20);       
        return guid;
    }
    
}