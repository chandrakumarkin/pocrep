/*
* Test Class: OB_EntityUBOContainerController
*
**/
@isTest
public with sharing class OB_DataProtectionControllerTest {
    
    @isTest
    private static void initDataProtect(){
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Identify ultimate beneficial owners';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;

        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        //insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        //insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule.HexaBPM__Section_Detail__c = sec.id;
        objPgNavRule.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule.HexaBPM__Section__c=section.id;
        objPgNavRule.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule;
        
        HexaBPM__Page_Flow_Condition__c ObjPgFlowCond = new HexaBPM__Page_Flow_Condition__c();
        ObjPgFlowCond.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        ObjPgFlowCond.HexaBPM__Field_Name__c = 'Id';
        ObjPgFlowCond.HexaBPM__Operator__c = '!='; 
        ObjPgFlowCond.HexaBPM__Value__c = 'null';
        ObjPgFlowCond.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id; 
        insert ObjPgFlowCond;
        
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id;
        insert ObjPgFlowAction;

        HexaBPM_Amendment__c objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        objAmendment2.Nationality_list__c = 'India';
        objAmendment2.Registration_No__c = '1234';
        objAmendment2.Role__c = 'DPO';
        objAmendment2.ServiceRequest__c = insertNewSRs[0].Id;
        insert objAmendment2;
        
        Data_Protection__c datProtect = new Data_Protection__c();
        datProtect.Assessed_the_Jurisdiction__c='tedt';
        datProtect.Name_of_Jurisdiction__c = 'tesdt';
        datProtect.Description_of_Safeguards__c = 'asd';
        datProtect.recordtypeID = OB_QueryUtilityClass.getRecordtypeID('Data_Protection__c', 'Jurisdiction');
        insert datProtect;

        test.startTest();
        OB_DataProtectionController.RequestWrapper reqWrapper = new OB_DataProtectionController.RequestWrapper();
        reqWrapper.flowId = listPageFlow[0].Id;
        reqWrapper.pageId = listPage[0].Id;
        reqWrapper.srId = insertNewSRs[0].Id;
        String requestString = JSON.serialize(reqWrapper);
        OB_DataProtectionController.ResponseWrapper getDataControllerRecord = OB_DataProtectionController.getDataController(requestString);
        system.debug('=========getDataControllerRecord====='+getDataControllerRecord);
        getDataControllerRecord.srWrap.srObj.Purpose_of_Notification__c = 'To inform the Commissioner of Data Protection that you do process personal data';
        getDataControllerRecord.srWrap.srObj.Purpose_of_Processing_Data__c ='Other';
        getDataControllerRecord.srWrap.srObj.Data_Subjects_Personal_Data__c ='Other';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_3__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_12_11__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_14__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_15__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_16_17_18__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_23_24_25__c ='Yes';
        getDataControllerRecord.srWrap.srObj.Transferring_Personal_Data_outside_DIFC__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_26__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_27__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_40__c ='No';
        reqWrapper.srWrap= getDataControllerRecord.srWrap;
        reqWrapper.dpoAmendWrap= getDataControllerRecord.dpoAmendWrap;
        OB_DataProtectionController.onAmedSaveDB(JSON.serialize(reqWrapper));
        
        test.stopTest();
    }

    @isTest
    private static void initDataProtect1(){
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Identify ultimate beneficial owners';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;

        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        //insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        //insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule.HexaBPM__Section_Detail__c = sec.id;
        objPgNavRule.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule.HexaBPM__Section__c=section.id;
        objPgNavRule.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule;
        
        HexaBPM__Page_Flow_Condition__c ObjPgFlowCond = new HexaBPM__Page_Flow_Condition__c();
        ObjPgFlowCond.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        ObjPgFlowCond.HexaBPM__Field_Name__c = 'Id';
        ObjPgFlowCond.HexaBPM__Operator__c = '!='; 
        ObjPgFlowCond.HexaBPM__Value__c = 'null';
        ObjPgFlowCond.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id; 
        insert ObjPgFlowCond;
        
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id;
        insert ObjPgFlowAction;

        HexaBPM_Amendment__c objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        objAmendment2.Nationality_list__c = 'India';
        objAmendment2.Registration_No__c = '1234';
        objAmendment2.Role__c = 'DPO';
        objAmendment2.ServiceRequest__c = insertNewSRs[0].Id;
        insert objAmendment2;
        
        Data_Protection__c datProtect = new Data_Protection__c();
        datProtect.Assessed_the_Jurisdiction__c='tedt';
        datProtect.Name_of_Jurisdiction__c = 'tesdt';
        datProtect.Description_of_Safeguards__c = 'asd';
        datProtect.Application__c = insertNewSRs[0].Id;
        datProtect.IsActive__c = true;
        datProtect.recordtypeID = OB_QueryUtilityClass.getRecordtypeID('Data_Protection__c', 'Jurisdiction');
        insert datProtect;

        test.startTest();
        OB_DataProtectionController.RequestWrapper reqWrapper = new OB_DataProtectionController.RequestWrapper();
        reqWrapper.flowId = listPageFlow[0].Id;
        reqWrapper.pageId = listPage[0].Id;
        reqWrapper.srId = insertNewSRs[0].Id;
        String requestString = JSON.serialize(reqWrapper);
        OB_DataProtectionController.ResponseWrapper getDataControllerRecord = OB_DataProtectionController.getDataController(requestString);
        getDataControllerRecord.srWrap.srObj.Purpose_of_Notification__c = 'To inform the Commissioner of Data Protection that you do process personal data';
        getDataControllerRecord.srWrap.srObj.Purpose_of_Processing_Data__c ='Other';
        getDataControllerRecord.srWrap.srObj.Data_Subjects_Personal_Data__c ='Other';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_3__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_12_11__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_14__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_15__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_16_17_18__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_23_24_25__c ='Yes';
        getDataControllerRecord.srWrap.srObj.Transferring_Personal_Data_outside_DIFC__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_26__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_27__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_40__c ='No';
        reqWrapper.srWrap= getDataControllerRecord.srWrap;
        reqWrapper.dpoAmendWrap= getDataControllerRecord.dpoAmendWrap;
        OB_DataProtectionController.onAmedSaveDB(JSON.serialize(reqWrapper));
        
        OB_DataProtectionController.getButtonAction(insertNewSRs[0].Id,sec.Id,listPage[0].Id);
        
        OB_DataProtectionController.ResponseWrapper res = OB_DataProtectionController.initDataProtectRecord(JSON.serialize(reqWrapper));
        res.jurisdictionWrap.dataProtectObj.Assessed_the_Jurisdiction__c = 'Yes';
        res.jurisdictionWrap.dataProtectObj.Name_of_Jurisdiction__c = 'Yes';
        res.jurisdictionWrap.dataProtectObj.Description_of_Safeguards__c = 'Yes';
        reqWrapper.jurisdictionWrap = res.jurisdictionWrap;
        res = OB_DataProtectionController.onDataProtectionSaveDB(JSON.serialize(reqWrapper));

        reqWrapper.jurisdictionId = res.jurisdictionWrap.dataProtectObj.Id;
        res = OB_DataProtectionController.removeDataProtection(JSON.serialize(reqWrapper));
        
        test.stopTest();
    }
    @isTest
    private static void initDataProtect2(){
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Identify ultimate beneficial owners';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;

        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        //insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        //insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule.HexaBPM__Section_Detail__c = sec.id;
        objPgNavRule.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule.HexaBPM__Section__c=section.id;
        objPgNavRule.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule;
        
        HexaBPM__Page_Flow_Condition__c ObjPgFlowCond = new HexaBPM__Page_Flow_Condition__c();
        ObjPgFlowCond.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        ObjPgFlowCond.HexaBPM__Field_Name__c = 'Id';
        ObjPgFlowCond.HexaBPM__Operator__c = '!='; 
        ObjPgFlowCond.HexaBPM__Value__c = 'null';
        ObjPgFlowCond.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id; 
        insert ObjPgFlowCond;
        
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id;
        insert ObjPgFlowAction;

        
        Data_Protection__c datProtect = new Data_Protection__c();
        datProtect.Assessed_the_Jurisdiction__c='tedt';
        datProtect.Name_of_Jurisdiction__c = 'tesdt';
        datProtect.Description_of_Safeguards__c = 'asd';
        datProtect.recordtypeID = OB_QueryUtilityClass.getRecordtypeID('Data_Protection__c', 'Jurisdiction');
        insert datProtect;

        test.startTest();
        OB_DataProtectionController.RequestWrapper reqWrapper = new OB_DataProtectionController.RequestWrapper();
        reqWrapper.flowId = listPageFlow[0].Id;
        reqWrapper.pageId = listPage[0].Id;
        reqWrapper.srId = insertNewSRs[0].Id;
        String requestString = JSON.serialize(reqWrapper);
        OB_DataProtectionController.ResponseWrapper getDataControllerRecord = OB_DataProtectionController.getDataController(requestString);
        getDataControllerRecord.srWrap.srObj.Purpose_of_Notification__c = 'The Commissioner of Data Protection that you do process personal data';
        getDataControllerRecord.srWrap.srObj.Purpose_of_Processing_Data__c ='Other';
        getDataControllerRecord.srWrap.srObj.Data_Subjects_Personal_Data__c ='Other';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_3__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_12_11__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_14__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_15__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_16_17_18__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_23_24_25__c ='Yes';
        getDataControllerRecord.srWrap.srObj.Transferring_Personal_Data_outside_DIFC__c ='Yes';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_26__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_27__c ='No';
        getDataControllerRecord.srWrap.srObj.In_accordance_with_Article_40__c ='No';
        reqWrapper.srWrap= getDataControllerRecord.srWrap;
        reqWrapper.dpoAmendWrap= getDataControllerRecord.dpoAmendWrap;
        OB_DataProtectionController.onAmedSaveDB(JSON.serialize(reqWrapper));
        
        test.stopTest();
    }
}