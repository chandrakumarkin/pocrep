/*
* @Author       : Azfer Pervaiz
* @CreateD Date : 13th Jan 2019
* @Description  : This controller will be used to check if the logged user is part of DHA Team or not.
*                 If the user is part of the DHA Team. A list view will be displayed else a page message.
**********************************************************************************************************
* Version        Modified by        Description             
**********************************************************************************************************
* v1.0           Azfer              Created
*/
public class AppointmentDetailsCtrl {
   
    //properties which will be used on page
    public Boolean ShowListView { get;set; }
    public Boolean ShowMessage  { get;set; }
    public Id ListViewId { get;set; }
    
    public AppointmentDetailsCtrl(){
        ShowListView = false;
        ShowMessage = false;
        
        //Getting use information from labels
        Id CurrentUserId = UserInfo.getUserId();
        Id GroupId = Id.ValueOf( Label.DHA_Team_Public_Group );
        
        // Checking membership of the user from the group.
        List<GroupMember> ListGroupMember = [SELECT Id, Group.Name, Group.DeveloperName FROM GroupMember WHERE UserOrGroupId =: CurrentUserId AND GroupId =: GroupId];
        
        // if user is part of the Group show the list view else show the message.
        if( ListGroupMember.size() > 0 ){
			ShowListView = true;                                     
            ListViewId = Id.ValueOf( Label.DHA_Team_Listview );
        }else{
            ShowMessage = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.DHA_Team_Message ));
        }
        
    }
}