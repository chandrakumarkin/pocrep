/*************************************************
* Class : UBO_Ultimate_Beneficial_Owner_Cls_AOR 
* Description : Class to Separate the UBO process from amendment to AOR data.Assembla #6484
* Date : 24/02/2019 
    
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By      Description
--------------------------------------------------------------------------------------------------------------------------            
 
**************************************************/    
public without sharing class UBO_Ultimate_Beneficial_Owner_Cls_AOR extends Cls_ProcessFlowComponents 
{
   
   

public List<UBO_Data__c> ListofUBOs{get;set;}
public UBO_Data__c ObjUBO{get;set;}
private Map<string,id> RecordTypeIds;
public string UBOType{get;set;}
private account AccountObj{get;set;}
public string srID {get;set;}

public Id UboId {get;set;}
public date RemovedDate{get;set;}

public UBO_Data__c ObjRemoveUBO{get;set;}

public boolean AllowtoAdd{get;set;}
public string FormMode {get;set;}
public string Listtype {get;set;}//Field to control what to show to screen 

public boolean IsShowSaveButton {get;set;}//Field to control what to show to screen 



    public List<UBO_Data__c> getListofAll()
{
      
       
        system.debug('objSr.Id===>'+objSr.Id);
        ListofUBOs=  [select Place_of_Registration__c,Nationality__c,Reason_for_exemption__c,RecordType.Name,RecordType.ID,Passport_Number__c,Passport_Expiry_Date__c,Apartment_or_Villa_Number__c,BC_UBO__c,Building_Name__c,City_Town__c,Country__c,Date_Of_Becoming_a_UBO__c,
         Date_of_Birth__c,Date_Of_Registration__c,Email__c,Emirate_State_Province__c,Entity_Name__c,Last_Name__c,First_Name__c,Type__c,Entity_Type__c from UBO_Data__c where Service_Request__c=:objSr.Id and Recordtype.name='Individual' and Status__c!='Removed'];
            system.debug('ListofUBOs==>'+ListofUBOs);
         
         for(UBO_Data__c Obj:ListofUBOs)
                 {
                 
                    if(Obj.Entity_Type__c!=null)
                      if(Obj.Entity_Type__c.contains('deemed'))
                         {
                            AllowtoAdd=false;
                         }
                         
                }
        return ListofUBOs;
                 
                 
    
}

   public List<UBO_Data__c> getListofBCAll()
{
    return  [select Place_of_Registration__c,Nationality__c,Reason_for_exemption__c,RecordType.Name,RecordType.ID,Passport_Number__c,Passport_Expiry_Date__c,Apartment_or_Villa_Number__c,BC_UBO__c,Building_Name__c,City_Town__c,Country__c,Date_Of_Becoming_a_UBO__c,
         Date_of_Birth__c,Date_Of_Registration__c,Email__c,Emirate_State_Province__c,Entity_Name__c,Last_Name__c,First_Name__c,Entity_Type__c from UBO_Data__c where Service_Request__c=:objSr.Id and Recordtype.name!='Individual' and Status__c!='Removed'];
    
}




public UBO_Ultimate_Beneficial_Owner_Cls_AOR()
{


        ObjRemoveUBO=new UBO_Data__c ();
        IsShowSaveButton=false;//Do not save button
        FormMode='Add';
        AllowtoAdd=true;
        srID = ApexPages.currentPage().getParameters().get('SRid');
        //UBOType = ApexPages.currentPage().getParameters().get('UBOType');
        
         RecordTypeIds=new Map<string,id>();
          for(RecordType objRT:[select Id,Name,DeveloperName from RecordType where  SobjectType='UBO_Data__c'])
          {
              RecordTypeIds.put(objRT.DeveloperName,objRT.id);
          }
         
         
         
         if(ListofUBOs==null)
         ListofUBOs=new List<UBO_Data__c>();
     
    
        
 }
 
 
   public override pagereference DynamicButtonAction()
   {
        
        if(IsValidInfo()==true || !isNext())
        //if(!isNext())
        {
            upsert ListofUBOs;
            return getButtonAction();
        }
       else  
       {
           
       }
        clearAction();
        
        return null;
    }
  
 public boolean IsValidInfo()
 {
     
     
     Boolean LastValueSelectedFound=false;
      Boolean NotLastValueSelectedFound=false;
     boolean IsValidData=true;
     
        for(UBO_Data__c Obj:getListofAll())
        {
            if(Obj.Entity_Type__c!=null)
            {
                if(Obj.Entity_Type__c.contains('deemed'))
                         {
                             LastValueSelectedFound=true;
                         }
                         else
                             NotLastValueSelectedFound=true;
                         
                    if(LastValueSelectedFound==true && NotLastValueSelectedFound==true)
                    {
                            IsValidData=false;
                            setMessage('Members of the governing body cannot be deemed as a UBO if the entity has already identified natural persons as UBOs');
        
                    }
            
                
            }
          
        }
        
        
        
        if(getListofAll().isEmpty() && getListofBCAll().isEmpty())
        {
                setMessage('Please add at least one Ultimate Beneficial Owner.');
                 IsValidData=false;
        }
            
     
     System.debug('IsValidData===>'+IsValidData);
     
    return IsValidData;
 } 
    
    
 public PageReference AddUBO()
 {
    
     
    ObjUBO =new UBO_Data__c();
    UBOType='Exempt_entities_UBO';
    return null;
 }

 public PageReference CancelUBO()
 {
 
    ObjUBO =null;
    EntityTypeSeleted=null;
    ListRelCompanies=null;
    getListofAll();
    IsShowSaveButton=false;
    return null;
 }
  
 public void SaveUBO()
 {
     
     try
     {
         
     
 system.debug('---save--'+ListofUBOs.size());
 if(EntityTypeSeleted!='ListShare')
 {
      //if(FormMode!='Edit')
     //ListofUBOs.add(ObjUBO); 
     //setInfoMessage('Record save successfully');
     upsert ObjUBO;
     
 }
 else
 {
     
     for(CompnayRelationships ObjRel:ListRelCompanies)
     {
         if(ObjRel.IsSelected==true)
         {
            UBO_Data__c TempObjUBO =new UBO_Data__c();
            TempObjUBO.RecordTypeId=RecordTypeIds.get(UBOType);
            TempObjUBO.Entity_Type__c=ObjUBO.Entity_Type__c;
            TempObjUBO.Service_Request__c = objSr.Id;
           
            TempObjUBO=SetUBOData(TempObjUBO,ObjRel.ComRecords,null);
            System.debug('==================TempObjUBO============>'+TempObjUBO);
            ListofUBOs.add(TempObjUBO); 
           
             
         }
         
        
         
     }
     System.debug('==================ListofUBOs============>'+ListofUBOs);
     //setInfoMessage('Record save successfully');
      upsert ListofUBOs;
 }
     
   CancelUBO();// clear all the values 
     //PageReference pg= new PageReference(ApexPages.currentPage().getParameters().get('retURL')); 
     //return null;
     }
     catch(DmlException e)
     {
           //Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getMessage()));
     }
     
 }
  
   @TestVisible private UBO_Data__c SetUBOData(UBO_Data__c TempObjUBO, Amendment__c ObjContact,date DateUBO)
  {
      
            TempObjUBO.Title__c=ObjContact.Title_new__c;
            TempObjUBO.First_Name__c=ObjContact.Given_Name__c;
            TempObjUBO.Last_Name__c=ObjContact.Family_Name__c;
            TempObjUBO.Gender__c=ObjContact.Gender__c;
            TempObjUBO.Date_of_Birth__c=ObjContact.Date_of_Birth__c;
            TempObjUBO.Place_of_Birth__c=ObjContact.Place_of_Birth__c;
            
            TempObjUBO.Passport_Number__c=ObjContact.Passport_No__c;
            TempObjUBO.Passport_Expiry_Date__c=ObjContact.Passport_Expiry_Date__c;
            TempObjUBO.Passport_Data_of_Issuance__c=ObjContact.Passport_Issue_Date__c;
            TempObjUBO.Nationality__c=ObjContact.Nationality_list__c;
            TempObjUBO.Phone_Number__c=ObjContact.Phone__c;
            TempObjUBO.Email__c=ObjContact.Person_Email__c;
            TempObjUBO.Date_Of_Becoming_a_UBO__c=DateUBO;
            TempObjUBO.RecordTypeId=RecordTypeIds.get('Individual');
            
            TempObjUBO.Apartment_or_Villa_Number__c=ObjContact.Apt_or_Villa_No__c;
            TempObjUBO.Building_Name__c=ObjContact.Building_Name__c;
            TempObjUBO.PO_Box__c=ObjContact.PO_Box__c;
            
            TempObjUBO.Emirate_State_Province__c=ObjContact.Emirate_State__c;
            TempObjUBO.Post_Code__c=ObjContact.Post_Code__c;
            TempObjUBO.Country__c=ObjContact.Permanent_Native_Country__c;
            return TempObjUBO;
            
      
  }
  
 
  
 public PageReference AddBCUBO()
 {
     IsShowSaveButton=true;
     ObjUBO =new UBO_Data__c();
     ObjUBO.Legal_Type_of_Entity__c=objSR.Legal_Structures__c;
     UBOType='Body_Corporation_UBO';
     ObjUBO.RecordTypeId=RecordTypeIds.get(UBOType);
     ObjUBO.I_am_Exempt_entities__c='Yes';
      ObjUBO.Service_Request__c = objSr.Id;
     return null;
 }
 public PageReference AddIndUBO()
 {
     
    ObjUBO =new UBO_Data__c();
    UBOType='Individual';
    EntityTypeSeleted=null;
     ObjUBO.Legal_Type_of_Entity__c=objSR.Legal_Structures__c;
    //EntityTypeSeleted='Openform';
    ObjUBO.RecordTypeId=RecordTypeIds.get(UBOType);
    ObjUBO.I_am_Exempt_entities__c='No';
     ObjUBO.Service_Request__c = objSr.Id;
     return null;
 
 }
 
 public string EntityTypeSeleted{get;set;}// 
 public List<CompnayRelationships> ListRelCompanies{get;set;}
 
  public void AddIndTypeSeletedNew()
 {
     IsShowSaveButton=true;
     List<String> RelCodes ;
     system.debug('--str--'+ObjUBO.Entity_Type__c);
      system.debug('--str--'+ObjUBO.Type__c);
     if(ObjUBO.Type__c!=null)
     {
         
         if(ObjUBO.Type__c=='Direct')
         {
                EntityTypeSeleted='ListShare';
                Listtype='LastindexShare';
                RelCodes = new List<String>{'Is Shareholder Of'};
                ListRelCompanies=new List<CompnayRelationships>();
                for(Amendment__c Obj:getRelationships(RelCodes))
                {
                    CompnayRelationships ObjCom=new CompnayRelationships();
                    ObjCom.ComRecords=Obj;
                    ObjCom.IsSelected=false;
                    ListRelCompanies.add(ObjCom);
                }
        
             
         }
         else
         {
              EntityTypeSeleted='Openform';
         }
         
     }
     else if(ObjUBO.Entity_Type__c.contains('deemed'))
     {
          Listtype='Lastindex';
         /*
         In case the clients selects “The individual members of the governing body who are deemed to be UBO”, 
         the system will pre-populate the details of the 
         director /general partners/member/founding members/council members as applicable
         */
        EntityTypeSeleted='ListShare';
        
         RelCodes = new List<String>{'Has Director', 'Has General Partner','Has Member','Has Founding Member','has Council Member'};
         
        
        ListRelCompanies=new List<CompnayRelationships>();
        for(Amendment__c Obj:getRelationships(RelCodes)){
            CompnayRelationships ObjCom=new CompnayRelationships();
            ObjCom.ComRecords=Obj;
            ObjCom.IsSelected=true;
            ListRelCompanies.add(ObjCom);
        }
        
         
     }
     else
          EntityTypeSeleted='Openform';
         
       
      
      
 }
 
 
public List<Amendment__c> getRelationships(List<String> RelCodes)
{
    return [select id,
         Title_new__c,
         Given_Name__c,
         Family_Name__c,
         Gender__c,
         
         Date_of_Birth__c,
         Place_of_Birth__c,
         Person_Email__c,
         Phone__c,
         
         Passport_No__c,
         Passport_Issue_Date__c,
         Passport_Expiry_Date__c,
         Nationality_list__c,
         
         Apt_or_Villa_No__c,
         Building_Name__c,
         Street__c,
         Emirate_State__c,
         Permanent_Native_City__c,
         Emirate__c,
         PO_Box__c,
         Post_Code__c,
         Permanent_Native_Country__c
         from Amendment__c where ServiceRequest__c=:objSr.Id and SAP_Relationship_Type__c in :RelCodes and Amendment_Type__c='Individual' and Status__c='Draft'];
    
}
public class CompnayRelationships
{
    public boolean IsSelected{get;set;}
    public Amendment__c ComRecords{get;set;}
    
}  
 
  

 
public List<SelectOption> EntyTypeoptions{get;set;}
public List<string> EntyTypestring{get;set;}    

 
    public void setInfoMessage(String message){
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,message));
      }
      
      
    public PageReference EditUBOInd() 
    {
        
         FormMode='Edit';
        SetUBOData(UboId);
        
         UBOType='Individual';
           //EntityTypeSeleted='Openform';
        EntityTypeSeleted='Openform';
        IsShowSaveButton =true;
    
        return null;
        
    }
    public PageReference EditUBOBC() 
    {
        FormMode='Edit';
        SetUBOData(UboId);
         UBOType='Body_Corporation_UBO';
         IsShowSaveButton =true;
        return null;
        
    }
    
   @TestVisible private void SetUBOData(Id TempObjUBOId)
  {
      //Set values when customer click on Edit 
        string strContactQuery = Apex_Utilcls.getAllFieldsSOQL('UBO_Data__c');
        strContactQuery = strContactQuery+' where Service_Request__c=\''+objSr.Id+'\'  and  Id =\''+TempObjUBOId+'\'';
        System.debug('strContactQuery===>'+strContactQuery);
      ObjUBO=database.query(strContactQuery);
       ObjUBO.Legal_Type_of_Entity__c=objSR.Legal_Structures__c;
          
            
  }
  
    public PageReference removeUBOFromList() 
    {
      
     try
     {
        UBO_Data__c ObjUBO_Data =new UBO_Data__c(Id = UboId);
        ObjUBO_Data.Status__c='Removed';
        ObjUBO_Data.Date_of_Cessation__c=RemovedDate;
        update ObjUBO_Data;
        getListofAll();
         //AllowtoAdd=true;
     } catch(DmlException e)
     {
         
     }
     
        return null;
    }
    
    public PageReference removeUBO() 
    {
        
        integer index = 0;
        for(UBO_Data__c itr:ListofUBOs){
            if(itr.Id == UboId){
                ListofUBOs.remove(index);
                break;
            }
             index ++ ;
        }
        delete new UBO_Data__c(Id = UboId);
        getListofAll();
        return null;
    }
    
    
}