public virtual class Cls_RegisteredAddress
{

 
     

    public Account ObjAccount{get;set;}    //V2.4
    public Records_kept__c ObjRecordskept{get;set;}    //V2.4
    public map<string,string> mapParameters;
    /**
    * The current Service Request ID
    */
    public  Attachment ObjAttachment{get;set;}
      public Service_Request__c objSr        {get; set;}
      
    
     string AccountId = '';
     public string RecordTypeId;
    
     private User objUser;                // V1.6
      public string strSRId {get;set;}

        public void RecordskeptLoad()
       {
           ObjAttachment=new Attachment();
       
         ObjRecordskept=new Records_kept__c();
                     System.debug('==objSr==>'+objSr);
         if(objSr==null & objSr.id==null  )
         {
            objSr=new Service_Request__c();
            objSr.RecordTypeId=RecordTypeId;

        }
        
        System.debug('==RecordTypeId==>'+RecordTypeId);
        
        objUser = new User();

                    
        
         for(User objUsr:[select id,ContactId,Email,Phone,Contact.AccountId,Contact.Account.Company_Type__c,Contact.Account.Currency__c,Contact.Account.Name,Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  from User where Id=:userinfo.getUserId()])
            objUser = objUsr;
        
        
                for(Service_Request__c SR:[select Customer__c,Client_Name__c,Declaration_Signatory_Name__c,Declaration_Signatory_Capacity__c ,Email__c,Send_SMS_To_Mobile__c from  Service_Request__c where Id=:strSRId])
                {
                       objSr=SR;
                      
    
                }
                
                //objSR.Customer__c='0012000001H3yij';
                
                 if(objSr.Customer__c==null && objUser.Contact.AccountId!=null)
                {
                    objSR.Customer__c = objUser.Contact.AccountId;
                    objSR.Customer__r=objUser.Contact.Account;
                    objSR.Email__c = objUser.Email;
                    objSR.Send_SMS_To_Mobile__c = objUser.Phone;

                }
         
                    System.debug('==========objSR==>'+objSR);
                
                    if(strSRId!=null)
                    ObjRecordskept.Service_Request__c=strSRId;
                    
                    ObjRecordskept.Status__c='Draft';
                    ObjRecordskept.Account__c=objSR.Customer__c;
                    
                    ObjAccount=[select id,Place_register_and_records_kept__c from account where id=:objSR.Customer__c];
                            
                    List<Records_kept__c> ListRecord=[select Account__c,Building_Name__c,PO_Box__c,Post_Code__c,Street_Address__c,Place_register_and_records_kept__c ,Unite_Number__c,City__c,Country__c,Name_of_the_holder__c,State__c,Status__c from Records_kept__c where Service_Request__c=:strSRId and Service_Request__c!=null];
                    
                    if(ListRecord.isEmpty())
                         for(Records_kept__c ObjKeep:[select Account__c,Building_Name__c,PO_Box__c,Post_Code__c,Street_Address__c ,Place_register_and_records_kept__c ,Unite_Number__c,City__c,Country__c,Name_of_the_holder__c,State__c,Status__c from Records_kept__c where Account__c!=null and Account__c=:objSR.Customer__c and Status__c='Active'])
                            {
                                ObjRecordskept=ObjKeep.clone(false, false, false, false);
                                ObjRecordskept.Service_Request__c=strSRId;
                                ObjRecordskept.Status__c='Draft';
                                
                                //ObjRecordskept.Place_register_and_records_kept__c=ObjAccount.Place_register_and_records_kept__c;
                            }
                    else
                    {
                        ObjRecordskept=ListRecord[0];
                    }
                                
                                
          
       
       }
}