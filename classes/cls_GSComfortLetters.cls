/*****************************************************************************************************************************
    Author      :   Swati Sehrawat
    Date        :   21st July 2016
    Description :   This class is used as a controller for page GSComfortLetters
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date            Updated By    Description
    V1.0    25th Oct 2016   Swati         Change in arabic version of Umm Al Quwain
    v1.1    05-12-2016      Swati         as per ticket 851,3502,3203
    v1.2    32-01-2017      Swati         as per ticket 3847
    v1.3    10-12-2017      Veera         as per ticket 4104
    V1.4    09-Feb-2020     Arun          Tikect #7278  
    v1.5    01-Aug-2021     Veera         Ticket #DIFC2-12278
 
    --------------------------------------------------------------------------------------------------------------------------             
*****************************************************************************************************************************/

public without sharing class cls_GSComfortLetters {
    
    //is looged in from portal or backend
    public boolean isPortal {get; set;}
    
    //is it new/Edit or detail page, used for rendering conditions
    public boolean isDetailPage {get; set;}
    
    //is it new page or edit page
    public boolean isNewPage {get; set;}
    
    //can request original for SR
    public boolean canRequestOriginal {get; set;}
    
    //Main SR object used on page and in class
    public service_request__c objSR {get; set;}
    
    public cls_SRWrapperClassForFields objSRWrap {get; set;}
    
    //Main SR template Object used on page to show description
    public SR_Template__c objSRTemplate {get; set;}
    
    public cls_SRGenericClassForMethods classObj;
    
    map<string, string> emirateArabicConversion;
    map<string, string> consulateArabicConversion;
    map<string, string> consulateArabicConversion1;
    
    public string recordTypeName;
    
    
    //veera
    public string Designation {get;set;}
    public string nationality {get;set;}
    public string passportNo {get;set;}
    public boolean isReadOnly {get;set;}
    
    
    
    public string Designation1 {get;set;}
    
    
    
    public String selectedCompanyId        {get; set;}  // Veera added
    
    public PageReference GetContactInfo(){ 
      isReadOnly  = true;        
      system.debug('!!@###'+objSR.Contact__c);
      contact con =[select id,Occupation__c,Nationality__c,Passport_No__c,Job_Title__r.name from contact where id =: objSR.Contact__c limit 1];
      Designation = (con.Occupation__c == '' || con.Occupation__c == null) ? con.Job_Title__r.name : con.Occupation__c;
      nationality  = con.Nationality__c; 
      passportNo = con.Passport_No__c; 
     
      return null;
    
    }
    
    
    
    //Constructor defined
    public cls_GSComfortLetters (){
        
    }
    
    string recordType;  
    
    public cls_GSComfortLetters(Apexpages.Standardcontroller con){
        //get values from URL
        isReadOnly   = false;
        recordType = Apexpages.currentPage().getParameters().get('RecordType');
        recordTypeName = Apexpages.currentPage().getParameters().get('RecordTypeName');
        string srID = Apexpages.currentPage().getParameters().get('id');
        string isDetail = Apexpages.currentPage().getParameters().get('isDetail');
        isPortal = false;
        isNewPage = true;
        canRequestOriginal = false;
        objSR = new service_request__c();
        objSRWrap = new cls_SRWrapperClassForFields(recordTypeName);
        objSR.recordTypeId = recordType;
      
        classObj = new cls_SRGenericClassForMethods();
       
        
        if(string.isNotBlank(isDetail) && isDetail == 'true'){
            isDetailPage = true;    
        }
        else{
            isDetailPage = false;
        }
        
        if(string.isNotBlank(srID)){
            isNewPage = false;
            objSR = classObj.getServiceRequest(srID);   // get service request from the SR object
            if(objSR!=null){
                initializePopulatedValuesFromSR(objSR); // initialize values on page from SR
            }
        }
       //v1.5
       if((recordType =='' || recordType == null) && !Test.isRunningTest()){
        
         objSR.recordTypeId = system.label.GS_Comfort_Letter_RecordType_ID;
         recordTypeName = 'GS_Comfort_Letters';
         objSRWrap = new cls_SRWrapperClassForFields('GS_Comfort_Letters');
        }
        // v1.5  
        
        if(Userinfo.getUserType() != 'Standard'){
            isPortal = true;
            user tempObj = classObj.getPortalDetails();
            objSR.Customer__c = tempObj.contact.AccountId;
            objSR.License_Number__c = tempObj.contact.Account.Registration_License_No__c;
            objSR.Establishment_Card_No__c = tempObj.contact.Account.Index_Card_Number__c;
            objSR.Send_SMS_To_Mobile__c = tempObj.phone;
          
            objSR.Email__c = tempObj.email;
            if(tempObj.contact.Account.Can_request_original_Letter__c == true){
                canRequestOriginal = true;  
            }
            
        }
        
        if(isDetailPage == false){
            objSRTemplate = new SR_Template__c();
            objSRTemplate = classObj.getSRTemplate(recordTypeName); //get SR Template details to show on page
        }   
        
        //Map created to save Urdu format in fields used for SR documents
        emirateArabicConversion = new map<string, string>();
        emirateArabicConversion.put('Dubai','دبي');
        emirateArabicConversion.put('Abu Dhabi','أبوظبي');
        emirateArabicConversion.put('Ajman','عجمان');
        emirateArabicConversion.put('Al Fujairah','الفجيرة');
        emirateArabicConversion.put('Ras Al Khaimah','رأس الخيمة');
        emirateArabicConversion.put('Sharjah','الشارقة');
        emirateArabicConversion.put('Umm Al Quwain','أم القيوين');//V1.0
        
        consulateArabicConversion = new map<string, string>();
        consulateArabicConversion.put('Consulate General Of','القنصل العام');
        consulateArabicConversion.put('Consulate Of','القنصل');
        consulateArabicConversion.put('Embassy Of','السفير');
        
        consulateArabicConversion1 = new map<string, string>();
        consulateArabicConversion1.put('Consulate General Of','القنصلية');
        consulateArabicConversion1.put('Consulate Of','القنصلية');
        consulateArabicConversion1.put('Embassy Of','السفارة');
    }
    
    
   
    
    
    
      public void setPrimaryTenantDetails(){
          
          Account selectedAccount = [SELECT Id, Name, Active_License__r.License_Issue_Date__c, Registration_License_No__c FROM Account WHERE Id = :selectedCompanyId];
          
          objSR.Middle_Name_Arabic__c = selectedAccount.name;
          
          
      }
    
   

/*************************************************************Public Methods used on Page******************************************************************************************/    
    
    /************************************************************************************************************
    Method Name: saveRequest
    Functionality: save or update SR Record and redirect to detail page of SR
    **************************************************************************************************************/
    public pageReference saveRequest(){
        pageReference redirectPage = null;
        try{
            string message = 'Success';
            if(objSR!=null){
                message = checkValidationsBeforeSaving();
                if(message.contains('Success')){
                    redirectPage = populateAndSaveSR(); 
                }
                else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,message));
                }
            }
            if(redirectPage!=null){
                redirectPage.setRedirect(true); 
                return redirectPage;
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,message));
            }
        }catch(DmlException e) {
            system.debug('----msg---'+e.getMessage());  
            system.debug('----lineNumber---'+e.getLineNumber());  
            Integer numErrors = e.getNumDml();
            for(Integer i=0;i<numErrors;i++) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(i)));
            }
        } 
        catch(exception ex){
            system.debug('----msg---'+ex.getMessage()); 
            system.debug('----lineNumber---'+ex.getLineNumber());   
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        return redirectPage;
    }
    
    /************************************************************************************************************
    Method Name: cancelRequest
    Functionality: cancel and redirect to home page or standard page depending on logged in user
    **************************************************************************************************************/
    public pageReference cancelRequest(){//v1.1
        pageReference objRef = null;
        try{
            if(IsDetailPage == true){ //Detail Page Button
                if(objSR.Submitted_Date__c != null && objSR.External_Status_Name__c!='Approved'){
                    string result = GsServiceCancellation.ProcessCancellation(objSR.id);    
                    if(result!='Cancellation submitted successfully'){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,result));   
                    }
                    else{
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,result));
                    }
                    if(result=='Cancellation submitted successfully'){
                        objRef = new Pagereference('/'+objSR.Id);   
                    }
                }
                
                else{
                    delete objSR;
                    if(isPortal)
                        objRef = new Pagereference('/apex/home');
                    else
                        objRef = new Pagereference('/home/home.jsp');
                }
            }else if(IsDetailPage == false){ //Edit Page Button
                if(objSR.Id != null)
                    objRef = new Pagereference('/'+objSR.Id);
                else{
                    if(isPortal)
                        objRef = new Pagereference('/apex/home');
                    else
                        objRef = new Pagereference('/home/home.jsp');
                }
            }
            if(objRef != null)
                objRef.setRedirect(true);
            return objRef;
        }catch(Exception ex){
            system.debug('-----');
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please contact support.'));
        }
        return objRef;
    }
    
    public pageReference editRequest(){
        pageReference redirectPage = null;
        try{
            if(objSR!=null){
                redirectPage = classObj.EditServiceRequest(objSR);
                if(redirectPage!=null){
                    redirectPage.setRedirect(true); 
                }
            }
        }catch(exception ex){
            system.debug('----msg---'+ex.getMessage()); 
            system.debug('----lineNumber---'+ex.getLineNumber());   
        }
        return redirectPage;
    }
    
    public pageReference submitRequest(){
        pageReference redirectPage = null;
        try{
            if(objSR!=null){
                redirectPage = classObj.SubmitServiceRequest(objSR);
                redirectPage.setRedirect(true);
            }
        }catch(exception ex){
            system.debug('----msg---'+ex.getMessage()); 
            system.debug('----lineNumber---'+ex.getLineNumber());   
        }
        return redirectPage;
    }
    
    public void makeSectionVisible(){
    }
    
    public void onlyOriginalAllowedMethod(){ //v1.1
        if(objSRWrap.originalOrSoftCopy=='Original'){
            objSRWrap.confirmation = false;
            objSRWrap.NOCboat = false;
            objSRWrap.NOCdriving = false;
            objSRWrap.NOCmarine = false;
            objSRWrap.NOCpassport = false;
            objSRWrap.NOCtemporary = false;
            objSRWrap.NOCCompanyVehicle = false;
            objSRWrap.NOCcompany = false;
            objSRWrap.NOChajj = false;
            objSRWrap.NOCumrah = false;
            objSRWrap.NOCPortEntry = false;
            objSRWrap.NOCMagrothercomp = false;//V1.3
        }
    }
    
     //1.4 
     public List<SelectOption> getSponsorList()
     {
        
         List<SelectOption> options = new List<SelectOption>();
         options.add(new SelectOption('','--None--'));
        for(Relationship__c contSpn : [select id,Object_Contact__c,Relationship_Name__c from Relationship__c where Subject_Account__c=:objSR.Customer__c and Object_Contact__c!=null and  (Relationship_Type__c='Has DIFC Sponsored Employee' OR  Relationship_Type__c='HAS DIFC Seconded Sponsored Employee') and Active__c=true order by Relationship_Name__c])
       options.add(new SelectOption(contSpn.Object_Contact__c,contSpn.Relationship_Name__c));
 
         return options;

         
    }
    
    public pageReference confirmUpgrade(){
        pageReference redirectPage = null;
        try{
            if(objSR!=null){
                string result = GsUpgradeDetails.UpgradeToVIP(objSR.id);
                if(result == 'Success'){
                    redirectPage = classObj.upgradeSR(objSR,recordTypeName);
                    redirectPage.setRedirect(true);
                }
            }
        }catch(exception ex){
            system.debug('----msg---'+ex.getMessage()); 
            system.debug('----lineNumber---'+ex.getLineNumber());   
        }
        return redirectPage;
    }
    
/*************************************************************Private Methods used within class******************************************************************************************/  
    
    /************************************************************************************************************
    Method Name: initializePopulatedValuesFromSR
    Functionality: initialize dropdown values and checkboxes again on page from values that are saved in SR record
    **************************************************************************************************************/
    private void initializePopulatedValuesFromSR(Service_request__c objSR){
        if(objSR!=null){
            objSR.recordTypeId = classObj.getRecordTypeId('GS Comfort Letters');
            if(objSR.Type_of_Service__c!=null){
                if(objSR.Type_of_Service__c.contains('Employment Confirmation Letter'))
                    objSRWrap.confirmation = true;
                if(objSR.Type_of_Service__c.contains('NOC for Boat Registration'))
                    objSRWrap.NOCboat = true;
                if(objSR.Type_of_Service__c.contains('NOC for Driving License'))
                    objSRWrap.NOCdriving = true;
                if(objSR.Type_of_Service__c.contains('NOC for Marine Driving License'))
                    objSRWrap.NOCmarine = true;
                if(objSR.Type_of_Service__c.contains('NOC for Passport Lost'))
                    objSRWrap.NOCpassport = true;
                if(objSR.Type_of_Service__c.contains('NOC for Temporary Work Permit'))
                    objSRWrap.NOCtemporary = true;
                if(objSR.Type_of_Service__c.contains('NOC for Company Vehicle Registration'))
                    objSRWrap.NOCCompanyvehicle = true;
                if(objSR.Type_of_Service__c.contains('NOC for Personal Vehicle Registration'))
                    objSRWrap.NOCPersonalvehicle = true;
                if(objSR.Type_of_Service__c.contains('NOC to Set up a Company'))
                    objSRWrap.NOCcompany = true;
                if(objSR.Type_of_Service__c.contains('Non-Objection for Hajj'))
                    objSRWrap.NOChajj = true;
                if(objSR.Type_of_Service__c.contains('Non-Objection for Umrah'))
                    objSRWrap.NOCumrah = true;
                if(objSR.Type_of_Service__c.contains('Salary Certificate to Immigration'))
                    objSRWrap.NOCimmigration = true;
                if(objSR.Type_of_Service__c.contains('Salary Certificate (Liquor Permit)'))
                    objSRWrap.NOCliquor = true;
                if(objSR.Type_of_Service__c.contains('NOC for port entry pass'))
                    objSRWrap.NOCPortEntry = true;
                if(objSR.Type_of_Service__c.contains('NOC to be appointed as a manager in another company'))//V1.3
                    objSRWrap.NOCMagrothercomp= true;
            }
            
            if(string.isNotBlank(objSR.entity_name__c)) 
                objSRWrap.requestLicense = objSR.entity_name__c;
            if(string.isNotBlank(objSR.company_name__c))    
                objSRWrap.authorityCompany = objSR.company_name__c;
            if(string.isNotBlank(objSR.business_name__c))   
                objSRWrap.authorityTemporary = objSR.business_name__c;
            if(string.isNotBlank(objSR.Position__c))    
                objSRWrap.letterHajj = objSR.Position__c;
            if(string.isNotBlank(objSR.Post_Code__c))   
                objSRWrap.letterUmrah = objSR.Post_Code__c;
            if(string.isNotBlank(objSR.Sponsor_Street__c))  
                objSRWrap.emirateConfirmation = objSR.Sponsor_Street__c;
            if(string.isNotBlank(objSR.Sponsor_First_Name__c))  
                objSRWrap.emirateLicense = objSR.Sponsor_First_Name__c;
            if(string.isNotBlank(objSR.Sponsor_Last_Name__c))   
                objSRWrap.emirateCompanyVehicle = objSR.Sponsor_Last_Name__c;
            if(string.isNotBlank(objSR.City_Town__c))   
                objSRWrap.emiratePersonalVehicle = objSR.City_Town__c;
            if(string.isNotBlank(objSR.Sponsor_Middle_Name__c)) 
                objSRWrap.emirateHajj = objSR.Sponsor_Middle_Name__c;
            if(string.isNotBlank(objSR.Sponsor_Building__c))    
                objSRWrap.emirateBoat = objSR.Sponsor_Building__c;
            if(string.isNotBlank(objSR.Sponsor_Mother_Full_Name__c))    
                objSRWrap.emirateMarine = objSR.Sponsor_Mother_Full_Name__c;
            if(string.isNotBlank(objSR.Sponsor_P_O_Box__c)) 
                objSRWrap.emirateCompany = objSR.Sponsor_P_O_Box__c;
            if(string.isNotBlank(objSR.Sponsor_Passport_No__c)) 
                objSRWrap.emirateUmrah = objSR.Sponsor_Passport_No__c;
            if(string.isNotBlank(objSR.Commercial_Activity_Previous_Sponsor__c))    
                objSRWrap.emiratePort = objSR.Commercial_Activity_Previous_Sponsor__c;
            if(string.isNotBlank(objSR.Foreign_Registered_Level__c))    
                objSRWrap.emirateImmigration = objSR.Foreign_Registered_Level__c;
            if(string.isNotBlank(objSR.Name_of_Addressee__c))   
                objSRWrap.addresseNameConfirmation = objSR.Name_of_Addressee__c;
            if(string.isNotBlank(objSR.Current_Registered_Building__c)) 
                objSRWrap.addresseNamePort = objSR.Current_Registered_Building__c;
            if(string.isNotBlank(objSR.Street_Address__c))  
                objSRWrap.originalOrSoftCopy = objSR.Street_Address__c; //v1.1
            if(string.isNotBlank(objSR.Cage_No__c)) 
                objSRWrap.formatCompany = objSR.Cage_No__c;
            if(string.isNotBlank(objSR.Usage_Type__c))  
                objSRWrap.formatConfirmation = objSR.Usage_Type__c;
            if(string.isNotBlank(objSR.Commercial_Activity__c)) 
                objSRWrap.serviceCompanyVehicle = objSR.Commercial_Activity__c;
            if(string.isNotBlank(objSR.DIFC_store_specifications__c))   
                objSRWrap.servicePersonalVehicle = objSR.DIFC_store_specifications__c;
            
            if(string.isNotBlank(objSR.Car_Registration_Number__c)) //v1.3   
                objSRWrap.managerAuthority  = objSR.Car_Registration_Number__c;
                
            if(string.isNotBlank(objSR.Incorporated_Organization_Name__c )) //v1.3   
                objSRWrap.managerEmirates   = objSR.Incorporated_Organization_Name__c ;  
                
                
             // objSR.Mother_s_Full_Name_Arabic__c= nationality;
             //  objSR.Place_of_Birth_Arabic__c= Designation;   
             // objSR.Passport_No_Previous_Sponsor__c = passportNo;
                
                
          
        
        }   
    }
    
    public pageReference populateAndSaveSR(){
        pageReference redirectPage = null;
        recordTypeName ='GS_Comfort_Letters';  //v1.5
        objSR.recordTypeId = system.label.GS_Comfort_Letter_RecordType_ID; // v1.5
        integer counter = 0;
        objSR.Type_of_Service__c = '';
        objSR.UID_Number__c = '';
         
         objSR.Mother_s_Full_Name_Arabic__c    = nationality;
         objSR.Place_of_Birth_Arabic__c        = Designation;
         objSR.Passport_No_Previous_Sponsor__c = passportNo;
         
         Designation1 = nationality;
        
        if(objSRWrap.confirmation == true){
            objSR.Type_of_Service__c = 'Employment Confirmation Letter; ';
            objSR.UID_Number__c = 'A';
            
            if(string.isNotBlank(objSRWrap.emirateConfirmation)){
                objSR.Sponsor_Street__c = objSRWrap.emirateConfirmation;
                objSR.Emirate_State__c = emirateArabicConversion.get(objSRWrap.emirateConfirmation);
            }
            if(string.isNotBlank(objSRWrap.formatConfirmation))
                objSR.Usage_Type__c = objSRWrap.formatConfirmation; 
            if(string.isNotBlank(objSRWrap.addresseNameConfirmation))
                objSR.Name_of_Addressee__c = objSRWrap.addresseNameConfirmation;
            counter++;
        }
        else{
            objSR.Sponsor_Street__c = '';
            objSR.Emirate_State__c = '';
            objSR.Usage_Type__c = '';
            objSR.Name_of_Addressee__c = '';
            objSR.Date_of_DIFC_Visa_Issuance__c = null;
        }
        
        
        if(objSRWrap.NOCboat == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Boat Registration; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', B';
            if(string.isNotBlank(objSRWrap.emirateBoat)){
                objSR.Sponsor_Building__c = objSRWrap.emirateBoat;
                objSR.Folio_Number__c = emirateArabicConversion.get(objSRWrap.emirateBoat);
            }
            else{
                objSR.Sponsor_Building__c = '';
                objSR.Folio_Number__c = '';
            }
            counter++;
        }
        
        
        if(objSRWrap.NOCdriving == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Driving License; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', C';
            if(string.isNotBlank(objSRWrap.emirateLicense)){
                objSR.Sponsor_First_Name__c = objSRWrap.emirateLicense;
                objSR.Emirates_Id_Number__c = emirateArabicConversion.get(objSRWrap.emirateLicense);
            }
            if(string.isNotBlank(objSRWrap.requestLicense))
                objSR.entity_name__c = objSRWrap.requestLicense;
            counter++;
        }
        else{
            objSR.Sponsor_First_Name__c = '';
            objSR.Emirates_Id_Number__c = '';
            objSR.entity_name__c = '';
        }
        
        if(objSRWrap.NOCmarine == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Marine Driving License; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', D';
            if(string.isNotBlank(objSRWrap.emirateMarine)){
                objSR.Sponsor_Mother_Full_Name__c = objSRWrap.emirateMarine;
                objSR.Health_Card_Number__c = emirateArabicConversion.get(objSRWrap.emirateMarine);
            }
            counter++;
        }
        else{
            objSR.Sponsor_Mother_Full_Name__c = '';
            objSR.Health_Card_Number__c = '';
        }
        
        if(objSRWrap.NOCpassport == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Passport Lost; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', E';
            counter++;
        }
        else{
            objSR.Name_of_Police_Station__c = '';
        }
        
        if(objSRWrap.NOCtemporary == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Temporary Work Permit;';
            objSR.UID_Number__c = objSR.UID_Number__c + ', F';
            if(string.isNotBlank(objSRWrap.authorityTemporary))
                objSR.business_name__c = objSRWrap.authorityTemporary;
            counter++;
        }
        else{
            objSR.business_name__c = '';    
        }
        
        if(objSRWrap.NOCCompanyvehicle == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Company Vehicle Registration; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', G';
            if(string.isNotBlank(objSRWrap.emirateCompanyVehicle)) {
                objSR.Sponsor_Last_Name__c = objSRWrap.emirateCompanyVehicle;
                objSR.Event_Name__c = emirateArabicConversion.get(objSRWrap.emirateCompanyVehicle);
            }
            if(string.isNotBlank(objSRWrap.serviceCompanyVehicle))
                objSR.Commercial_Activity__c = objSRWrap.serviceCompanyVehicle; 
            counter++;
        }
        else{
            objSR.Sponsor_Last_Name__c = '';
            objSR.Commercial_Activity__c = '';
            objSR.Event_Name__c = '';
        }
        
        
        if(objSRWrap.NOCPersonalvehicle == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Personal Vehicle Registration; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', H';
            if(string.isNotBlank(objSRWrap.emiratePersonalVehicle)){
                objSR.City_Town__c = objSRWrap.emiratePersonalVehicle;
                objSR.FAX__c = emirateArabicConversion.get(objSRWrap.emiratePersonalVehicle);
            }
            if(string.isNotBlank(objSRWrap.originalOrSoftCopy)){//v1.1
                objSR.Street_Address__c = objSRWrap.originalOrSoftCopy;
            }else{
                objSR.Street_Address__c = 'Soft Copy';
            }
            if(string.isNotBlank(objSRWrap.servicePersonalVehicle))
                objSR.DIFC_store_specifications__c = objSRWrap.servicePersonalVehicle;
            counter++;
        }
        else{
            objSR.City_Town__c = '';
            objSR.FAX__c = '';
            objSR.DIFC_store_specifications__c = '';
            //v1.2
        }
        
        
        if(objSRWrap.NOCcompany == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC to Set up a Company; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', I';
            if(string.isNotBlank(objSRWrap.authorityCompany))
                objSR.company_name__c = objSRWrap.authorityCompany;
            if(string.isNotBlank(objSRWrap.emirateCompany)){
                objSR.Sponsor_P_O_Box__c = objSRWrap.emirateCompany;
                objSR.Kindly_Note__c = emirateArabicConversion.get(objSRWrap.emirateCompany);
            }
            if(string.isNotBlank(objSRWrap.formatCompany))
                objSR.Cage_No__c = objSRWrap.formatCompany;
            counter++;
        }
        else{
            objSR.company_name__c = '';
            objSR.Sponsor_P_O_Box__c = '';
            objSR.Kindly_Note__c = '';
            objSR.Cage_No__c = '';
            objSR.Others_Please_Specify__c = '';
        }
        
        
        if(objSRWrap.NOChajj == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Non-Objection for Hajj; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', J';
            if(string.isNotBlank(objSRWrap.emirateHajj)){
                objSR.Sponsor_Middle_Name__c = objSRWrap.emirateHajj;
                objSR.Floor__c = emirateArabicConversion.get(objSRWrap.emirateHajj);
            }
            if(string.isNotBlank(objSRWrap.letterHajj)){
                objSR.Position__c = objSRWrap.letterHajj;
                objSR.Declaration_Signatory_Capacity__c = consulateArabicConversion.get(objSRWrap.letterHajj);
                objSR.First_Name_Arabic_Previous_Sponsor__c = consulateArabicConversion1.get(objSRWrap.letterHajj);
            }
            counter++;
        }
        else{
            objSR.Sponsor_Middle_Name__c = '';
            objSR.Floor__c = '';
            objSR.Position__c = '';
            objSR.Declaration_Signatory_Capacity__c = '';
            objSR.First_Name_Arabic_Previous_Sponsor__c = '';
        }
        
        if(objSRWrap.NOCumrah == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Non-Objection for Umrah; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', K';
            if(string.isNotBlank(objSRWrap.emirateUmrah)){
                objSR.Sponsor_Passport_No__c = objSRWrap.emirateUmrah;
                objSR.Last_Name_Arabic__c = emirateArabicConversion.get(objSRWrap.emirateUmrah);
            }
            if(string.isNotBlank(objSRWrap.letterUmrah)){
                objSR.Post_Code__c = objSRWrap.letterUmrah;
                objSR.Client_s_Representative_in_Charge__c = consulateArabicConversion.get(objSRWrap.letterUmrah);
                objSR.Current_Registered_Postcode__c = consulateArabicConversion1.get(objSRWrap.letterUmrah);
            }
            
            counter++;
        }
        else{
            objSR.Sponsor_Passport_No__c = '';
            objSR.Last_Name_Arabic__c = '';
            objSR.Post_Code__c = '';
            objSR.Client_s_Representative_in_Charge__c = '';
            objSR.Current_Registered_Postcode__c = '';
        }
        
        if(objSRWrap.NOCimmigration == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Salary Certificate to Immigration; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', L';
            if(string.isNotBlank(objSRWrap.emirateImmigration)){
                objSR.Foreign_Registered_Level__c = objSRWrap.emirateImmigration;
                objSR.Foreign_Registered_Building__c = emirateArabicConversion.get(objSRWrap.emirateImmigration);
            }
            if(string.isNotBlank(objSRWrap.originalOrSoftCopy)){//v1.1
                objSR.Street_Address__c = objSRWrap.originalOrSoftCopy;
            }else{
                objSR.Street_Address__c = 'Soft Copy';
            }
            counter++;
        }
        else{
            objSR.Foreign_Registered_Level__c = '';
            objSR.Foreign_Registered_Building__c = '';
        }
        
        
        if(objSRWrap.NOCliquor == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Salary Certificate (Liquor Permit); ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', M';
            if(string.isNotBlank(objSRWrap.originalOrSoftCopy)){//v1.1
                objSR.Street_Address__c = objSRWrap.originalOrSoftCopy;
            }else{
                objSR.Street_Address__c = 'Soft Copy';
            }
            counter++;
        }
        else{
            objSR.Type_of_Unit__c = '';
        }
        
        if(objSRWrap.NOCPortEntry == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for port entry pass; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', N';
            if(string.isNotBlank(objSRWrap.emiratePort)){
                objSR.Commercial_Activity_Previous_Sponsor__c = objSRWrap.emiratePort;
                objSR.Name_of_Informer__c = emirateArabicConversion.get(objSRWrap.emiratePort);
            }
            if(string.isNotBlank(objSRWrap.addresseNamePort))
                objSR.Current_Registered_Building__c = objSRWrap.addresseNamePort;
            
            counter++;
        }
         
        else{
            objSR.Commercial_Activity_Previous_Sponsor__c = '';
            objSR.Name_of_Informer__c = '';
        }
        //V1.3
         if(objSRWrap.NOCMagrothercomp == true){
            objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC to be appointed as a manager in another company; ';
            objSR.UID_Number__c = objSR.UID_Number__c + ', O';
            
            if(string.isNotBlank(objSRWrap.managerAuthority))                 
                    objSR.Car_Registration_Number__c = objSRWrap.managerAuthority;  
                
            if(string.isNotBlank(objSRWrap.managerEmirates))    
                objSR.Incorporated_Organization_Name__c = objSRWrap.managerEmirates;
                objSR.Health_Card_Number__c = emirateArabicConversion.get(objSRWrap.managerEmirates);  
            counter++;
        }
        
        else{
             objSR.Car_Registration_Number__c ='';
             objSR.Health_Card_Number__c = '' ;             
        }
         
        //V1.3
        
        
        //v1.1
        if(objSR.Monthly_Salary_AED__c!=null && objSR.Monthly_Salary_AED__c>0 && (objSR.External_Status_Name__c == 'Draft' || string.isBlank(objSR.External_Status_Name__c))){ 
            objSR.Salary_Confidential__c = string.valueOf(objSR.Monthly_Salary_AED__c);
        }
        
        if(string.isNotBlank(objSR.Salary_Confidential__c) && !(objSR.Salary_Confidential__c.contains('/-'))){
            objSR.Salary_Confidential__c = objSR.Salary_Confidential__c+'/-';   
        }
        
        if(string.isNotBlank(objSR.Salary_Confidential__c) && objSR.Salary_Confidential__c.contains('.')){
            if(objSR.Salary_Confidential__c.contains('.00/-')){
                objSR.Salary_Confidential__c = objSR.Salary_Confidential__c.remove('.00');
            }
            else{
                objSR.Salary_Confidential__c = objSR.Salary_Confidential__c.remove('/-');       
            }
        }
        
        redirectPage = classObj.upsertServiceRequest(objSR,recordTypeName);
        insertPriceItems(counter);
        return redirectPage;
    }
    
    
    private string checkValidationsBeforeSaving(){
        string errorMessage = 'Success';
        if(objSRWrap.confirmation == false && objSRWrap.NOCboat == false && objSRWrap.NOCdriving == false && objSRWrap.NOCmarine == false &&
           objSRWrap.NOCpassport == false && objSRWrap.NOCtemporary == false && objSRWrap.NOCPersonalvehicle == false && objSRWrap.NOCCompanyvehicle == false && 
           objSRWrap.NOCcompany == false && objSRWrap.NOChajj == false && objSRWrap.NOCumrah == false && objSRWrap.NOCimmigration == false && objSRWrap.NOCliquor == false && objSRWrap.NOCPortEntry == false && objSRWrap.NOCMagrothercomp== false){
                
                errorMessage = 'No comfort letter selected, Please select any one.';
        }
                
        if((objSRWrap.NOCcompany == true && objSRWrap.authorityCompany == 'Others' && objSR.Others_Please_Specify__c=='') || (objSRWrap.NOCMagrothercomp == true && objSRWrap.managerAuthority == 'Others' && objSR.Building_Previous_Sponsor__c=='')){
            errorMessage = 'If Name of Authority is "Others", Please provide Others details.';
        }
        
    
        
        if(objSR.Is_Applicant_Salary_Above_AED_20_000__c == 'No'){
            if(objSR.Monthly_Salary_AED__c <=0 || objSR.Monthly_Salary_AED__c==null){
                errorMessage = 'Monthly Salary should be greater than zero.';
            }
            if(objSR.Monthly_Salary_AED__c >25000){
                errorMessage = 'Monthly Salary should be less than 25000.';
            }
            
        }
        return errorMessage;
    }
    
    private void insertPriceItems(integer counter){
        list<SR_Price_Item__c> priceItemInsert = new list<SR_Price_Item__c>();
        list<Product2> productList = [select id from Product2 where ProductCode = 'GS Letters'];
        if(objSR.id!=null && (objSR.External_Status_Name__c == 'Draft' || string.isBlank(objSR.External_Status_Name__c))){
            list<SR_Price_Item__c> tempListToDeletePrice = new list<SR_Price_Item__c>();
            tempListToDeletePrice = [select id from SR_Price_Item__c where ServiceRequest__c=:objSR.id];
            if(tempListToDeletePrice!=null && tempListToDeletePrice.size()>0){
                delete tempListToDeletePrice;
            }
            
            Pricing_Line__c pricingLine = new Pricing_Line__c();
            if(objSR.Express_Service__c ==  true){
                list<Pricing_Line__c> tempList = [select id,(select Unit_Price__c,Unit_Price_in_USD__c from Dated_Pricing__r) from Pricing_Line__c where Material_Code__c=:'GO-VIP112'];
                if(tempList!=null && !tempList.isEmpty()){
                    pricingLine = tempList[0];
                }
            }
            else{
                list<Pricing_Line__c> tempList = [select id,(select Unit_Price__c,Unit_Price_in_USD__c from Dated_Pricing__r) from Pricing_Line__c where Material_Code__c=:'GO-00112'];
                if(tempList!=null && !tempList.isEmpty()){
                    pricingLine = tempList[0];
                }
            }
            for(integer i=0 ; i<counter; i++){
                SR_Price_Item__c tempObj = new SR_Price_Item__c();
                tempObj.ServiceRequest__c = objSR.id;
                tempObj.Product__c = productList[0].id; 
                tempObj.Pricing_Line__c = pricingLine.id;
                tempObj.Price__c = pricingLine.Dated_Pricing__r[0].Unit_Price__c;
                tempObj.Price_in_USD__c = pricingLine.Dated_Pricing__r[0].Unit_Price_in_USD__c;
                priceItemInsert.add(tempObj);
            }
            //v1.1
            if(objSR.Avail_Courier_Services__c!=null && objSR.Street_Address__c!=null && objSR.Avail_Courier_Services__c.contains('Yes') && objSR.Street_Address__c.contains('Original') ){
                list<Product2> productList1 = [select id from Product2 where ProductCode = 'Courier Charges'];
                list<Pricing_Line__c> tempList = [select id,(select Unit_Price__c,Unit_Price_in_USD__c from Dated_Pricing__r) from Pricing_Line__c where Material_Code__c=:'GO-00230']; 
                SR_Price_Item__c tempObj = new SR_Price_Item__c();
                tempObj.ServiceRequest__c = objSR.id;
                tempObj.Product__c = productList1[0].id; 
                tempObj.Pricing_Line__c = tempList[0].id;
                tempObj.Price__c = tempList[0].Dated_Pricing__r[0].Unit_Price__c;
                tempObj.Price_in_USD__c = tempList[0].Dated_Pricing__r[0].Unit_Price_in_USD__c;
                priceItemInsert.add(tempObj);
            }
            if(priceItemInsert!=null && !priceItemInsert.isEmpty()){
                upsert priceItemInsert;
            }
        }
    }
        
}