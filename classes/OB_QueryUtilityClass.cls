/*******************************************
 * 
 *  Merul V 1.0 made changes to getAccountRec.
 * 
 *  
 ********************************************/

public class OB_QueryUtilityClass{
	/*
	 * check Service request status is Draft or not
	 */

	public static Boolean shareholderPageValidationCheck(HexaBPM__Service_Request__c sr) {

		boolean check = true;
		integer ShareholderCount = 0;
		integer GeneralPartnerShareholderCount = 0;
		integer LimitedPartnerShareholderCount = 0;
		integer DesignatedMemberCount= 0;
		for (HexaBPM_Amendment__c amendObj :[SELECT ID,partner_type__c, First_Name__c,Is_this_member_a_designated_Member__c, Last_Name__c, Passport_No__c, Role__c
										     FROM HexaBPM_Amendment__c WHERE ServiceRequest__c = :sr.Id AND Role__c != null AND Role__c includes ('Shareholder')]) {
													ShareholderCount += 1;
													if(amendObj.Is_this_member_a_designated_Member__c == 'Yes'){
														DesignatedMemberCount += 1;
													}

													if(amendObj.partner_type__c == 'Limited Partner'){
														LimitedPartnerShareholderCount = LimitedPartnerShareholderCount + 1;
													}
													
											else if(amendObj.partner_type__c == 'General Partner'){
												GeneralPartnerShareholderCount = GeneralPartnerShareholderCount + 1;
											}
													
												 }

		if(sr.Business_Sector__c == 'Non Profit Incorporated Organisation (NPIO)' && ShareholderCount < 3){
			check = false;
		}		
		if(sr.Type_of_Entity__c == 'General Partnership (GP)' && ShareholderCount < 2){
			check = false;
		}		
		if(sr.Type_of_Entity__c == 'Recognized Partnership (RP)' && ShareholderCount < 2) {
			check = false;
		}	
		if(sr.Type_of_Entity__c == 'Recognized Limited Liability Partnership (RLLP)' && ShareholderCount < 2) {
			check = false;
		}
		if(sr.Type_of_Entity__c == 'Recognized Limited Partnership (RLP)') {
			if(GeneralPartnerShareholderCount < 1 || LimitedPartnerShareholderCount < 1){
				check = false;
			}	
		}		
		
		if(sr.Type_of_Entity__c == 'Limited Liability Partnership (LLP)') {
                if(DesignatedMemberCount < 1 || ShareholderCount < 2){
									check = false;
								}
							}
		return 	check;									 

	}

	public static Boolean checkSRStatusDraft(HexaBPM__Service_Request__c serObj) {

		Boolean isDraft = false;

		if((String.IsBlank(serObj.HexaBPM__Internal_Status_Name__c) || (String.IsNotBlank(serObj.HexaBPM__Internal_Status_Name__c)
		    && serObj.HexaBPM__Internal_Status_Name__c.equalsIgnorecase('Draft')))
		    || (String.IsBlank(serObj.HexaBPM__External_SR_Status__c) || (String.IsNotBlank(serObj.HexaBPM__External_SR_Status__c)
		    && serObj.HexaBPM__External_SR_Status__r.Name.equalsIgnorecase('Draft')))) { 
			// draft service request

			isDraft = true;

		}
		return isDraft;


		//return null;
	}


	public static Boolean minRecValidationCheck(string srid, string roleName) {


		boolean containsMinOneRec = false;
		for (HexaBPM_Amendment__c amendObj :[SELECT ID, First_Name__c, Last_Name__c, Passport_No__c, Role__c
										     FROM HexaBPM_Amendment__c WHERE ServiceRequest__c = :srid]) {
			if(amendObj.Role__c != null) {
				list<string> amendRoles = amendObj.Role__c.split(';');
				if(amendRoles.contains(roleName)) {
					containsMinOneRec = true;
					break;
				}
			}

		}

		return containsMinOneRec;


	}


	public static Boolean checkSRStatusDraftById(Id serObjId) {

		Boolean isDraft = false;
		HexaBPM__Service_Request__c serObj = new HexaBPM__Service_Request__c();
		for(HexaBPM__Service_Request__c serObjTemp :[SELECT id, 
												      HexaBPM__Internal_SR_Status__c, 
												      HexaBPM__External_SR_Status__c, 
												      HexaBPM__Internal_Status_Name__c, 
												      HexaBPM__EXternal_Status_Name__c
												      FROM HexaBPM__Service_Request__c
												      WHERE id = :serObjId
		     ]) {
			serObj = serObjTemp;
		}

		if((String.IsBlank(serObj.HexaBPM__Internal_Status_Name__c) || (String.IsNotBlank(serObj.HexaBPM__Internal_Status_Name__c)
		    && serObj.HexaBPM__Internal_Status_Name__c.equalsIgnorecase('Draft')))
		    || (String.IsBlank(serObj.HexaBPM__External_SR_Status__c) || (String.IsNotBlank(serObj.HexaBPM__External_SR_Status__c)
		    && serObj.HexaBPM__EXternal_Status_Name__c.equalsIgnorecase('Draft')))) {
			// draft service request

			isDraft = true;

		}
		return isDraft;

		//return null;

	}



	/*
	 * get Record type ID based on developer name
	 */
	public static Id getRecordtypeID(String sObjectName, String developerName) {
		Id recordTypeId = null;
		// query Shareholder record type
		List<RecordType> ShareholderRecordtypeIDList = [Select Id From RecordType where sobjecttype = :sObjectName
													    and developername = :developerName];
		System.debug('==============ShareholderRecordtypeIDList========' + ShareholderRecordtypeIDList);
		if(ShareholderRecordtypeIDList != null && ShareholderRecordtypeIDList.size() != 0) {
			recordTypeId = ShareholderRecordtypeIDList [0].Id;
		}
		return recordTypeId;
	}

	public static HexaBPM__Service_Request__c setPageTracker(HexaBPM__Service_Request__c objRequest, string srId, string pageId) {
		if(srId != null && String.IsNotBlank(PageId)) {
			for(HexaBPM__Service_Request__c SRTBU :[Select Id, Page_Tracker__c from HexaBPM__Service_Request__c where Id = :srId]) {
				objRequest.Page_Tracker__c = SRTBU.Page_Tracker__c;
				objRequest.Id = SRTBU.Id;
			}
			if(objRequest.Page_Tracker__c != null && objRequest.Page_Tracker__c != '') {
				if(objRequest.Page_Tracker__c.indexOf(PageId) ==-1) {
					objRequest.Page_Tracker__c = objRequest.Page_Tracker__c + ',' + PageId;
				}
			} else {
				objRequest.Page_Tracker__c = PageId;
			}
		} else {
			objRequest.Page_Tracker__c = PageId;
		}
		return objRequest;
	}
	/*********************************************************************************************
	 * @Description : Function to save file                                                       *
	 * @Params      : Id, String, String, String                                                  *
	 * @Return      : Id                                                                          *
	 *********************************************************************************************/

	public static Id saveTheFile(Id parentId, String fileName, String base64Data, String contentType) {
		/*
		 system.debug('############ saveTheFile-- ' + fileName);
		 try{
		 // this is used if we pass the doument master then bind the content version with SR DOC else with service request

		 ContentVersion conVer = new ContentVersion()
		 ;
		 conVer.ContentLocation = 'S'; // S specify this document is in SF, use E for external files
		 conVer.PathOnClient = fileName; // The files name, extension is very important here which will help the file in preview.
		 conVer.Title = fileName; // Display name of the files
		 conver.IsMajorVersion = false;
		 base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
		 conVer.VersionData = EncodingUtil.base64Decode(base64Data); // converting your binary string to Blob
		 insert conVer;

		 // First get the content document Id from ContentVersion
		 Id conDocID = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :conVer.Id].ContentDocumentId;

		 //Create ContentDocumentLink
		 ContentDocumentLink cDe = new ContentDocumentLink();
		 cDe.ContentDocumentId = conDocID;
		 cDe.LinkedEntityId = parentId; // you can use objectId,GroupId etc
		 cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
		 cDe.Visibility = 'AllUsers';
		 insert cDe;


		 system.debug('=== cDe.LinkedEntityId===' + cDe.LinkedEntityId);
		 return conVer.ID;
		 } catch(Exception ex) {
		 system.debug(ex);
		 // LoggerUtils.createErrorLog('ContentVersion', 'FileUploadController', 'saveTheFile', ex.getMessage() + ' - ' + ex.getStackTraceString());
		 }
		 */
		return null;

	}

	/*********************************************************************************************
	 * @Description : Function to append the rest of the chunk to already uploaded part of file   *
	 * @Params      : Id, String                                                                  *
	 * @Return      : void                                                                        *
	 *********************************************************************************************/
	public static void appendToFile(Id fileId, String base64Data) {
		/*
		 system.debug(fileId);
		 base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
		 try{
		 ContentVersion a = [SELECT Id, VersionData
		 FROM ContentVersion
		 WHERE Id = :fileId
		 ];

		 String existingBody = EncodingUtil.base64Encode(a.VersionData);
		 a.VersionData = EncodingUtil.base64Decode(existingBody + base64Data);

		 update a;
		 } catch(Exception ex) {
		 system.debug(ex);
		 // LoggerUtils.createErrorLog('ContentVersion', 'FileUploadController', 'appendToFile', ex.getMessage() + ' - ' + ex.getStackTraceString());
		 }
		 */
	}
	/*********************************************************************************************
	 * @Description : Function to create SR Doc for CompanyName    *
	 * @Params      : string srId, string companyNameId, list<string> documentMasterCode                                                                *
	 * @Return      : map<string,string>
	 * To be modified
	 THIS METHOD IS ALSO USED FOR REPARENTING LOGIC IN QUALIFIED APPLICATION                                                              *
	 *********************************************************************************************/
	public static list<HexaBPM__SR_Doc__c> createCompanySRDoc(string srId, string companyNameId, map<string, string> docContentMap) {

		String srTemplateId;
		list<HexaBPM__SR_Doc__c> lstSRDocs = new list<HexaBPM__SR_Doc__c>();
		Map<String, HexaBPM__SR_Template_Docs__c> docMasterSRTempDocMap = new Map<String, HexaBPM__SR_Template_Docs__c>();
		list<ContentDocumentLink> lstContentDocumentLink = new list<ContentDocumentLink>();

		for(HexaBPM__Service_Request__c sr :[SELECT id, 
										     HexaBPM__SR_Template__c
										     FROM HexaBPM__Service_Request__c
										     WHERE id = :srId
										     LIMIT 1]) {
			srTemplateId = sr.HexaBPM__SR_Template__c;
		}
		// getting all SR template docs from SR.
		for(HexaBPM__SR_Template_Docs__c srTempDocs :[SELECT id, 
												      HexaBPM__Document_Master__r.Name, 
												      HexaBPM__Document_Master__c, 
												      HexaBPM__Document_Master__r.HexaBPM__Code__c
												      FROM HexaBPM__SR_Template_Docs__c
												      WHERE HexaBPM__SR_Template__c = :srTemplateId
												      AND HexaBPM__Added_through_Code__c = true
												      AND HexaBPM__Document_Master__r.HexaBPM__Code__c IN :docContentMap.keySet()
		    ]  ) {
			docMasterSRTempDocMap.put(srTempDocs.HexaBPM__Document_Master__r.HexaBPM__Code__c, srTempDocs);
                
		}
		system.debug('=====createSRDoc======');
		for(string documentMasterCode :docContentMap.keySet()) {
			if(docMasterSRTempDocMap.containsKey(documentMasterCode)) {
				HexaBPM__SR_Template_Docs__c srTempDocs = docMasterSRTempDocMap.get(documentMasterCode);
				if(srTempDocs != NULL) {
					HexaBPM__SR_Doc__c srDocsObj = new HexaBPM__SR_Doc__c();
					srDocsObj.Name = srTempDocs.HexaBPM__Document_Master__r.Name;
					srDocsObj.HexaBPM__Service_Request__c = srId;
					srDocsObj.HexaBPM__Doc_ID__c = docContentMap.get(documentMasterCode);
					srDocsObj.HexaBPM__Status__c = 'Uploaded';
					srDocsObj.OB_CompanyName__c = companyNameId;
					srDocsObj.HexaBPM__From_Finalize__c = true;
					srDocsObj.HexaBPM__Document_Master__c = srTempDocs.HexaBPM__Document_Master__c;
					srDocsObj.HexaBPM__SR_Template_Doc__c = srTempDocs.Id;
					lstSRDocs.add(srDocsObj);
				}
			}
		}
		system.debug('doc inserted ');
		system.debug(lstSRDocs);
		if(lstSRDocs != null && lstSRDocs.size() > 0)
			insert lstSRDocs;

		for(HexaBPM__SR_Doc__c srDoc :lstSRDocs) {
			ContentDocumentLink cDe = new ContentDocumentLink();
			cDe.ContentDocumentId = srDoc.HexaBPM__Doc_ID__c;
			cDe.LinkedEntityId = srDoc.Id; // you can use objectId,GroupId etc
			cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
			cDe.Visibility = 'AllUsers';
			lstContentDocumentLink.add(cDe);
		}
		if(lstContentDocumentLink != null && lstContentDocumentLink.size() > 0)
			insert lstContentDocumentLink;

		//Delete the DocumentLink from the SR - Cleanup of Files
		if(docContentMap != null && docContentMap.size() > 0)
			delete [select id from ContentDocumentLink where ContentDocumentId in :docContentMap.values() and LinkedEntityId = :srId];
		return lstSRDocs;
	}
	/*********************************************************************************************
	 * @Description : Function to create SR Doc for Amendments    *
	 * @Params      : string srId, string amendmentId, string documentMasterCode                                                                *
	 * @Return      : Id                                                                        *
	 *********************************************************************************************/
	public static Id createSRDoc(string srId, string amendmentId, string documentMasterCode) {
		Id srDocId;
		system.debug('=====createSRDoc======');
		for(HexaBPM__Document_Master__c docMaster :[select Id, Name, HexaBPM__Code__c from HexaBPM__Document_Master__c
												    where HexaBPM__Code__c = :documentMasterCode]) {
			system.debug('===inside==createSRDoc======');
			HexaBPM__SR_Doc__c srDoc = new HexaBPM__SR_Doc__c();
			srDoc.Name = docMaster.Name;
			srDoc.HexaBPM__Service_Request__c = srId;
			if(String.IsNotBlank(amendmentId)) {
				srDoc.HexaBPM_Amendment__c = amendmentId;
			}
			srDoc.HexaBPM__Document_Master__c = docMaster.Id;
			insert srDoc;
			srDocId = srDoc.Id;
		}
		system.debug('=====srDocId======' + srDocId);
		return srDocId;
	}
	public static HexaBPM__Service_Request__c QueryFullSR(string SRID) {
		HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c();
		map<String, Schema.SObjectType> m = Schema.getGlobalDescribe();
		SObjectType  objtype;
		DescribeSObjectResult objDef1;
		map<String, SObjectField> fieldmap;
		if(m.get('HexaBPM__Service_Request__c') != null)
			objtype = m.get('HexaBPM__Service_Request__c');
		if(objtype != null)
			objDef1 = objtype.getDescribe();
		if(objDef1 != null)
			fieldmap = objDef1.fields.getmap();
		set<string> setSRFields = new set<string>();
		if(fieldmap != null) {
			for(Schema.SObjectField strFld :fieldmap.values()) {
				Schema.DescribeFieldResult fd = strFld.getDescribe();
				if(fd.isCustom()) {
					setSRFields.add(string.valueOf(strFld).toLowerCase());
				}
			}
		}
		string SRqry = 'Select Id,Name,Recordtype.DeveloperName';
		if(setSRFields.size() > 0) {
			for(string srField :setSRFields) {
				SRqry += ',' + srField;
			}
		}
		SRqry += ' From HexaBPM__Service_Request__c where Id=:SRID';
		for(HexaBPM__Service_Request__c srRequest :database.query(SRqry)) {
			objSR = srRequest;
		}
		return objSR;
	}
	public static string getAccountId(string userId) {
		string strAccountId;
		for(User user :[select contact.AccountId from User where id = :userId])
			strAccountId = user.contact.AccountId;
		return strAccountId;
	}


	public static Account getAccountRec(string userId) 
	{
		string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        Account accRec = new Account();
        //  v 1.0  Changes in loadPriceItem
        List<Account> accLst = [ 
                                  SELECT id,
                                         Name 
                                    FROM Account
                                   WHERE id =:accountId  
                                   LIMIT 1
                               ];
       
        if( 
            accLst != NULL 
                && accLst.size() > 0 
          )
        {
            accRec = accLst[0];
		}
		
		return accRec;
	}

	
	public static string getMessageFromDMLException(DMLException e) {
		string DMLError = '';
		Integer numErrors = e.getNumDml();

		for(Integer i = 0; i < numErrors; i ++) {
			String errorMsg = (e.getDmlMessage(i) != NULL ? e.getDmlMessage(i) :'');
			DMLError = DMLError + errorMsg + '\n';
		}

		if(DMLError == null) {
			DMLError = e.getMessage() + '';
		}

		return DMLError;

	}




	public static Boolean reviewPageButtonShowCheck(HexaBPM__Service_Request__c serObj) {

		if(serObj.HexaBPM__Internal_Status_Name__c == 'Draft') {
			return true;
		} else {
			return false;
		}

	}


	public static Decimal treatNullAsZero(Decimal numberDecimal) {
		return (numberDecimal == null ? 0 :numberDecimal);
	}

	/*

	 public class RequestWrapper {

	 public string communityName { get; set; }
	 public string currentCommunityPageId { get; set; }
	 public string communityReviewPageName { get; set; }
	 public string sitePageName { get; set; }
	 public string strBaseUrl { get; set; }
	 public string srId { get; set; }
	 public string flowId { get; set; }
	 public string pageId { get; set; }


	 public RequestWrapper() {
	 }
	 }

	 public class ResponseWrapper {

	 //public boolean isSRLocked {get;set;}
	 //public string communityReviewPageURL  {get;set;}



	 public ResponseWrapper() {
	 }
	 }
	 */

}