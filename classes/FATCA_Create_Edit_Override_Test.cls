@isTest
private class FATCA_Create_Edit_Override_Test{
    static testmethod void testFatca(){
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.GIIN__c = '98Q96B.00000.LE.250';
        insert objAccount;
        
        FATCA_Submission__c fsub = new FATCA_Submission__c(Reporting_FI__c = objAccount.Id);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(fsub);
        FATCA_Create_Edit_Override_Controller ctlr = new FATCA_Create_Edit_Override_Controller(sc);
        
        Integer i = 0;
        for(Fatca__c f : ctlr.fatcaMap.values()){
            f.First_Name__c = 'Test'+i;
            f.Last_Name__c = 'Test'+i; 
            f.US_TIN__c = '0';
            f.Account_Holder_Type__c = 'abc'+i;
            f.Account_Number__c = 'test'+i;
            f.Account_Balance__c = 50000;
            f.Payment_Amount__c = 50000;
            f.Payment_Type__c = 'abc'+i;
            f.Account_Type__c = 'abc'+i;
            f.Birth_Date__c = System.Today();
            f.Address__c = 'Dubai';
            f.City__c = 'Dubai';
            f.Country_Code_Address__c = 'UAE';
            
            i++;
        }
        for(Fatca__c f : ctlr.fatcaBMap.values()){
            f.Entity_Name__c = 'TestB'+i; 
            f.US_TIN__c = '0';
            f.Account_Holder_Type__c = 'abcb'+i;
            f.Account_Number__c = 'testb'+i;
            f.Account_Balance__c = 250000;
            f.Payment_Amount__c = 250000;
            f.Payment_Type__c = 'abcb'+i;
            f.Account_Type__c = 'abcb'+i;
            f.Address__c = 'Dubai';
            f.city__c = 'Dubai';
            f.Country_Code_Address__c = 'UAE';
            
            i++;
        }
        
        ctlr.save();
        
        ctlr.fatcaMap.values()[0].Status__c = 'Rejected';
        ctlr.save();
        
        ctlr.fatcaMap.values()[0].Account_Type__c = 'Test Acc Type';
        ctlr.save();
        
        fsub.Status__c = 'Approved';
        ctlr.save();
        
        FATCA_Detail_Override_Controller ctlrDetail = new FATCA_Detail_Override_Controller(sc);
        fsub.Status__c = 'Submitted';
        ctlrDetail.submit();
        
        FATCA_Build_Xml.generateXml('test');
        
        
        ctlrDetail.CreateFATCAAuditRecord();
        ctlrDetail.resubmit();
        
        fsub.Status__c = 'Resubmitted';
        ctlr.fatcaMap.values()[0].Status__c = 'Resubmitted';
        ctlr.save();
        
        FATCA_Build_Xml.generateUpdatedXml('test');
        ctlr = new FATCA_Create_Edit_Override_Controller(sc);
        ctlr.add();
        ctlr.addB();
        ctlr.selectedKey = '1';
        ctlr.selectedKeyB = '1';
        ctlr.del();
        ctlr.delB();
        ctlr.deleterows();
        ctlr.add();
        ctlr.save();
        ctlr.CreateFATCAAuditRecord();
        ctlr.giin = 'test';
        ctlr.errorMsg = false;
        ctlr.duplicateErrorMessage = 'a';
        ctlr.noData = true;
        ctlr.validationErrorMessage = 'a';
        ctlr.showValError = true;
        ctlr = new FATCA_Create_Edit_Override_Controller(sc);
        
    }
}