public class ctlrProgressBar { 
public static list<Accountwrapper> Accountwrapperlist {get;set;} 

    @auraEnabled
    public static List<Accountwrapper> getVisitHistory(String recordId){
    List<String> srtypelist = new List<String>();
    srtypelist.add('Application for Incorporation / Registration');
    srtypelist.add('Lease Registration');
    srtypelist.add('User Access Request Form');
    Map<String,Service_Request__c> SrMap = new Map<String,Service_Request__c>();    
    List<Account> acclist =  [select id,Transferred_to_ROC__c,ROC_Status__c,Transferred_to_ROC_Date__c,createdDate,ROC_reg_incorp_Date__c,(Select id,External_Status_Name__c,Name,Submitted_DateTime__c,Completed_Date_Time__c,Service_Type__c from Service_Requests__r where Service_Type__c in :srtypelist and External_Status_Name__c != 'Rejected' order by createdDate asc) from Account where id = :recordId];
        for(Service_Request__c servRec : acclist[0].Service_Requests__r){     SrMap.put(servRec.Service_Type__c,servRec);
        }
        system.debug('SrMap'+SrMap);
         Service_Request__c sraccs = new Service_Request__c();
         Service_Request__c srApp = new Service_Request__c();
         Service_Request__c srLs = new Service_Request__c();        
        
        if(srMap.containskey('Application for Incorporation / Registration')){  srApp = srMap.get('Application for Incorporation / Registration'); 
            system.debug('srApp'+srApp);
        }
        
        if(srMap.containskey('Lease Registration')){  srLs = srMap.get('Lease Registration');
            system.debug('srLs'+srLs);
        }
        if(srMap.containskey('User Access Request Form')){  sraccs = srMap.get('User Access Request Form');
            system.debug('sraccs'+sraccs);
        }
      
        
        list<Accountwrapper> Accountwrapperlist = new list<Accountwrapper>();
         Accountwrapper accwrpRec = new Accountwrapper('Open',date.newinstance(acclist[0].createdDate.year(), acclist[0].createdDate.month(), acclist[0].createdDate.day()),Null,'selected');
         Accountwrapperlist.add(accwrpRec); 
         Accountwrapper accwrpRec1;
         if(acclist[0].Transferred_to_ROC__c == True){
         accwrpRec1 = new Accountwrapper('Transfer to ROC',Date.newInstance(acclist[0].Transferred_to_ROC_Date__c.year(),
                                                                                           
                                       acclist[0].Transferred_to_ROC_Date__c.month(), acclist[0].Transferred_to_ROC_Date__c.day()),Null,'selected');
         Accountwrapperlist.add(accwrpRec1); 
         }    
         Accountwrapper accwrpRec2;
         if(sraccs != Null && sraccs.Submitted_DateTime__c !=Null && sraccs.Completed_Date_Time__c != Null){
         accwrpRec2 = new Accountwrapper(sraccs.Service_Type__c,Date.newInstance(sraccs.Submitted_DateTime__c.year(), sraccs.Submitted_DateTime__c.month(), sraccs.Submitted_DateTime__c.day()),Date.newInstance(sraccs.Completed_Date_Time__c.year(), sraccs.Completed_Date_Time__c.month(), sraccs.Completed_Date_Time__c.day()),'selected');                   
         Accountwrapperlist.add(accwrpRec2);                     
         } 
         Accountwrapper accwrpRec3;
         if(srLs != Null && srLs.Submitted_DateTime__c !=Null && srLs.Completed_Date_Time__c != Null){
         accwrpRec3 = new Accountwrapper(srLs.Service_Type__c,Date.newInstance(srLs.Submitted_DateTime__c.year(), srLs.Submitted_DateTime__c.month(), srLs.Submitted_DateTime__c.day()),Date.newInstance(srLs.Completed_Date_Time__c.year(), srLs.Completed_Date_Time__c.month(), srLs.Completed_Date_Time__c.day()),'selected');                   
         Accountwrapperlist.add(accwrpRec3);                     
         }
         Accountwrapper accwrpRec4;
         if(srApp != Null && srApp.Submitted_DateTime__c !=Null && srApp.Completed_Date_Time__c != Null){
         accwrpRec4 = new Accountwrapper('AOR',Date.newInstance(srApp.Submitted_DateTime__c.year(), srApp.Submitted_DateTime__c.month(), srApp.Submitted_DateTime__c.day()),Date.newInstance(srApp.Completed_Date_Time__c.year(), srApp.Completed_Date_Time__c.month(), srApp.Completed_Date_Time__c.day()),'selected');                   
         Accountwrapperlist.add(accwrpRec4);                     
         }
        Accountwrapper accwrpRec5;
         if(acclist[0].ROC_Status__c == 'Active' && acclist[0].ROC_reg_incorp_Date__c != Null){
         accwrpRec5 = new Accountwrapper('Closed',date.newinstance(acclist[0].ROC_reg_incorp_Date__c.year(), acclist[0].ROC_reg_incorp_Date__c.month(), acclist[0].ROC_reg_incorp_Date__c.day()),Null,'selected');
         Accountwrapperlist.add(accwrpRec5); 
         }
         system.debug('Accountwrapperlist'+Accountwrapperlist); 
		
        if(acclist[0].ROC_Status__c == 'Active'){
         accwrpRec5.selected = 'true';
        }
        else if(srApp != Null && srApp.Submitted_DateTime__c !=Null && srApp.Completed_Date_Time__c != Null){  accwrpRec4.selected = 'true';            
        }
        else if(srLs != Null && srLs.Submitted_DateTime__c !=Null && srLs.Completed_Date_Time__c != Null){  accwrpRec3.selected = 'true';            
        }         
        else if(sraccs != Null && sraccs.Submitted_DateTime__c !=Null && sraccs.Completed_Date_Time__c != Null){  accwrpRec2.selected = 'true';            
        }
        else if(acclist[0].Transferred_to_ROC__c == True){ accwrpRec1.selected = 'true';            
        }
        else{  accwrpRec.selected = 'true';            
        }
        
        return Accountwrapperlist;     
    }
      class Accountwrapper{
        @auraEnabled public string name{get;set;}
        @auraEnabled public Date submittedDate{get;set;}
        @auraEnabled public Date approvedDate{get;set;}
        @auraEnabled public string selected{get;set;}
        
          Accountwrapper(string name,Date submittedDate,Date approvedDate,string selected){
        	this.name = name;
        	this.submittedDate = submittedDate;
        	this.approvedDate = approvedDate;
            this.selected=selected;
        }
    } 
}