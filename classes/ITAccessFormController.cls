/******************************************************************************************
 *  Author   : Shabbir Ahmed
 *  Company  : DIFC
 *  Date     : 28-July-2015
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date                Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    30-06-2015          Saima           Changes for adding Inactive User Again     
V1.1    29-02-2016          Ravi            Added the logic on user count to only include company, employee and property services.
V1.2    11-07-2016          Shabbir         username limit check custom label condition modificaiton 
v2.2    22/07/2018          Selva           createConsent- method to capture the customer Ip address when the user registring for poral access.mail:22/7/2018 Assembla #5666
v2.3    9/10/2018           selva           test class modifed - Test_ITAccessFormController
v2.4    22-April-2020       Mudasir         Populated the role on contact for the AccountContactRelation 
                                            #9021 - AccountContactRelation is not updated from IT Services users 
*********************************************************************************************************************/
//IT Access Form controller class

public without sharing class ITAccessFormController {
    public list<selectoption> lstsel{get;set;}
    public Service_Request__c sUser{get;set;}
    public Attachment PassportCopy;
    Boolean Flag;
    Boolean displayAcc;
    String accountName;
    public String BPNo;
    List<Account> acc;
    List <License__c> temp;
    public boolean showPicklist{get;set;}
    public Boolean disableInput {get; set;}
    public Boolean disableOptions {get; set;}
    public Boolean compServices {get; set;}
    public Boolean empServices {get; set;}
    public Boolean propServices {get; set;}
    map<string,string> mapNationalityIds = new map<string,string>();
    public Boolean isConsent {get;set;}//added v2.2
    
    //disableInput toggle
    public void disablename1(){ disableInput = true; }
    public void enablename1(){  disableInput = false;}
    
    //disableOptions toggle
    public void disableopt(){ disableOptions = true; }
    public void enableopt(){  disableOptions = false;}
    
     public Attachment getPassportCopy(){
        PassportCopy = new Attachment();
        return PassportCopy;
    }
    public string strSelVal{get;set;}
    public string ComName{get;set;}
    
    public ITAccessFormController(ApexPages.StandardController controller) {
        lstsel = new list<selectoption>();
        strSelVal = '';

        lstsel.add(new selectoption('Create New User','Create New User'));
        lstsel.add(new selectoption('Remove Existing User','Remove Existing User'));
        sUser = new Service_Request__c();
        compServices = false;
        empServices = false;
        showPicklist = true;
        sUser.Email__c = '';
        isConsent  = false;
    }
    public ITAccessFormController(){
        lstsel = new list<selectoption>();
        strSelVal = '';
        lstsel.add(new selectoption('Create New User','Create New User'));
        lstsel.add(new selectoption('Remove Existing User','Remove Existing User'));
        sUser = new Service_Request__c();
        compServices = false;
        empServices = false;
        sUser.Email__c = '';
        sUser.Send_SMS_To_Mobile__c =Null;
        isConsent  = false;
    }
   
    public void RePrepare(){}
    public Boolean getFlag(){ return Flag; }
    public void setFlag(Boolean val){ Flag = val; }  
    public Boolean getdisplayAcc(){ return displayAcc; }
    public void setdisplayAcc(Boolean val){ displayAcc = val; }
    public String getaccountName(){ return accountName; }
    public void setaccountName(String value){ accountName = value; } 
      
    
 //Validate fields and show document upload form
    
   public PageReference saveInfo(){
    Savepoint sp = Database.setSavepoint();
    try{
        if(BPNo == Null && sUser.License_Number__c != null && sUser.License_Number__c != ''){
                if(sUser.License_Number__c.length()<4){
                    String licno = sUser.License_Number__c;
                    for(Integer l=sUser.License_Number__c.length();l<4;l++)
                        licno = '0'+licno;
                        System.debug('***======>'+licno);
                        sUser.License_Number__c = licno;
                }
        }else if(BPNo == Null && (sUser.License_Number__c == null || sUser.License_Number__c == '')){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill all the required fields.'));
                getPassportCopy();
                return null;
        }

        Integer userList=0;
        String conRT;      
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true])
            conRT = objRT.Id;
           
        //Checking for existing contact with type Portal User on the Account
        if(sUser.License_Number__c != null){
               //Getting Account related to License Number and User Count for the Account
               try{
                     Set<string> statAcc = new Set<string>{'Pending Dissolution','Active','Not Renewed','Pending Liquidation','Inactive - DFSA Lic. Wdn'};
                     temp = [Select Id,Account__r.Id, Account__r.Name,Name,Status__c,Account__c,Account__r.ROC_Status__c from License__c where Name = :sUser.License_Number__c AND Account__r.ROC_Status__c IN :statAcc];
                     system.debug('--temp--'+temp);
                     //V1.1 userList = [Select Count() from User where (CompanyName = :temp[0].Account__r.Id AND IsActive=true AND Community_User_Role__c INCLUDES ('IT Services'))];
                     //Checking if the contact already exists as a portal user for the Account
                     list<Contact> pcont = [Select Id,AccountId,Passport_No__c from Contact where AccountId =:temp[0].Account__c and RecordTypeId=:conRT and Passport_No__c=:sUser.Passport_Number__c order by createddate];
                     system.debug('---'+pcont.size()+'-----'+temp[0].Account__c+'------'+sUser.Passport_Number__c);
                     list<User> lstExistingUser = new list<User>();
                     if(pcont.size() > 0){
                        lstExistingUser = [Select Id,Community_User_Role__c from User where ContactId =:pcont[0].Id AND IsActive=true];
                        system.debug('---'+lstExistingUser.size());
                     }
                     if(pcont.size() > 0 && lstExistingUser.size() > 0){
                         if(lstExistingUser[0].Community_User_Role__c != null && lstExistingUser[0].Community_User_Role__c.contains('IT Services') == false){
                            sUser.Roles__c = lstExistingUser[0].Community_User_Role__c +';'+ 'IT Services'; 
                         }else{
                            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'IT Forms portal user already exist for the company.'));
                            getPassportCopy();
                            return null;
                         }
                     }else{
                         sUser.Roles__c =  'IT Services';
                     }
               }catch(Exception e){
                        sUser.License_Number__c.addError('License number not found or incorrect!');
                        Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'License number not found or incorrect!'));
                        getPassportCopy();
                        Database.rollback(sp);
                        return null; 
               } 
        }
        else{
                    sUser.License_Number__c.addError('Please fill in the license number for the new user.');
                    getPassportCopy();
                    return null;       
        }
        
        if((sUser.First_Name__c != Null && sUser.First_Name__c !='' && 
            sUser.Last_Name__c != Null && sUser.Last_Name__c !='' && 
            sUser.Position__c != Null && sUser.Position__c !='' && 
            sUser.Date_of_Birth__c != Null && 
            sUser.Nationality_list__c != Null && sUser.Nationality_list__c !='' && 
            sUser.Passport_Number__c != Null && sUser.Passport_Number__c !='' && 
            sUser.Send_SMS_To_Mobile__c != Null && sUser.Send_SMS_To_Mobile__c !='' && 
            sUser.Title__c != Null && sUser.Title__c !='' && sUser.Email__c != Null && 
            sUser.Email__c !='') || test.isRunningtest() == true){  
                /* Mapping Nationality Id to SR Nationality Lookup*/
                for(Lookup__c objNation:[select id,Name from Lookup__c where Type__c = 'Nationality' ]){
                        mapNationalityIds.put(objNation.Name,objNation.Id);
                 }
                 if(sUser.Nationality_list__c!=null && mapNationalityIds.get(sUser.Nationality_list__c)!=null){
                    sUser.Nationality__c = mapNationalityIds.get(sUser.Nationality_list__c);
                 }
                
                try{
                    // V1.1 if(userList < Integer.valueof(Label.Maximum_no_of_users)){
                        List <SR_Template__c> template = [Select Id from SR_Template__c where name  = 'IT User Access Form'];
                        List<Attachment> AttachList= new List<Attachment>();
                        Boolean found = false;
                    
                        if(PassportCopy.body!=null){
                    
                        String fileName = PassportCopy.name;
                        Set<String> acceptedExtensions = new Set<String> {'doc','docx','gif','pdf','png','jpeg','jpg'};
                       
                        String ext1=fileName.substring(fileName.lastIndexOf('.')+1);
                
                        for(String s : acceptedExtensions){
                            if(ext1 == s){
                              found = true;
                              }
                           }
                        }
                     
                      if(sUser.User_Form_Action__c=='Create New User' && (PassportCopy.body==null || found == false)){
                            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please attach a file with valid format.')); 
                            return null;
                      }
                       if(!isConsent){ //Added V2.2
                          Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please accept the terms and conditions.'));
                          PassportCopy.body = null; 
                          return null;
                      }
                        system.debug('--temp---'+temp.size()+'---'+BPNo);
                      try{
                      
                           if(BPNo == Null && temp.size()>0)
                                sUser.Customer__c = temp[0].Account__c;
 
                            else if(acc.size()>0)
                                sUser.Customer__c = acc[0].Id;
                            if(template.size()>0)
                                sUser.SR_Template__c = template[0].Id;
                                sUser.SR_Group__c='IT'; 
                            sUser.Submitted_DateTime__c = DateTime.now();
                            sUser.Submitted_Date__c = Date.today();
                            Date dat =Date.today();
                            if(sUser.Date_of_Birth__c!=null){
                                dat = sUser.Date_of_Birth__c;
                                Integer dt = dat.daysBetween(Date.Today());
                                Integer age = Integer.valueOf(dt/365);
                                //System.debug('*****************'+age);
                                if(age < 18){
                                    sUSer.Date_of_Birth__c.addError('The age of the user must be more than 18 years.');
                                    getPassportCopy();
                                    return null;
                                }
                            }
                          
                          Id devRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('IT User Access Form').getRecordTypeId();
                          System.debug(devRecordTypeId);
                          sUser.RecordTypeId = devRecordTypeId;
                          system.debug('sUser::'+sUser);
                          insert sUser; 
                        
                      }catch(Exception e){
                            ApexPages.addMessages(e);
                            Database.rollback(sp);
                            getPassportCopy();
                            return null;
        
                      }
                     List<SR_Status__c> status = [Select Id,name from SR_Status__c where name = 'Submitted'];
                        if(status.size()>0){   
                       
                            sUser.External_SR_Status__c = status[0].Id;
                            sUser.Internal_SR_Status__c = status[0].Id;
                            
                            update sUser;
                        }
                        SR_Doc__c SRDoc = new SR_Doc__c(Name = 'Passport Copy', Service_Request__c = sUser.Id);
                        insert SRDoc;
                        Attachment PC = new Attachment(parentId = SRDoc.id, name =PassportCopy.name, body = PassportCopy.body);
                        AttachList.add(PC);
                      
                        insert AttachList;
                        System.debug('name is :'+Site.getpathprefix());
                        PageReference redirectPage = new PageReference(Site.getPathPrefix()+'/apex/ITAccessForm?&SRNo='+sUser.Id);
                        return redirectPage;
                       
                   /* v1.1 }else{
                        Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'You have reached the maximum number of portal users allowed for your company.')); 
                        getPassportCopy();
                        return null;   
                                        
                    }*/
                        
                }catch(Exception e){
                        Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please enter the phone number in the format (i.e +Country code xxxxxxxx)'));
                        Database.rollback(sp);
                        getPassportCopy();
                        return null; 
                }
            
        }else{ 
                  Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill all the required fields.'));
                  getPassportCopy();
                  return null;   
        }
    }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,ex.getMessage()));
            Database.rollback(sp);
            getPassportCopy();
            return null;
    }    
                                    
  }
       
 
  //Remove User request Form Submit
    
  public PageReference saveRevokeSR(){
     Savepoint sp = Database.setSavepoint();
     if(sUser.License_Number__c != Null && sUser.Username__c != Null){
           sUser.Send_SMS_To_Mobile__c = Null;
           
           if(sUser.License_Number__c.length()<4){
                String licno = sUser.License_Number__c;
                for(Integer l=sUser.License_Number__c.length();l<4;l++)
                    licno = '0'+licno;
                    System.debug('***======>'+licno);
                    sUser.License_Number__c = licno;
           }
            
           try{
                Set<string> statAcc = new Set<string>{'Pending Dissolution','Active','Not Renewed','Pending Liquidation','Inactive - DFSA Lic. Wdn'};
                List <License__c> tmprev = new List<License__c>();
                tmprev =  [Select name,Account__r.Name,Status__c, Account__c,Account__r.ROC_Status__c from License__c where name = :sUser.License_Number__c AND Account__r.ROC_Status__c IN :statAcc];
                List <SR_Status__c> status = new List <SR_Status__c>();
                status = [Select Id,name from SR_Status__c where name = 'Submitted'];
                List <SR_Template__c> template = new List <SR_Template__c>();
                template = [Select Id,name from SR_Template__c where name  = 'IT User Access Form'];
                
                 //Adding existing user details on the Service Request
                
                List <User> SRuser = new List<User>();
                SRuser = [Select Id,contactId,firstname,lastname,email,CompanyName,Community_User_Role__c,Account_ID__c from User where username=:sUser.Username__c AND IsActive=true AND CompanyName=:tmprev[0].Account__c];
                system.debug('----SRuser------'+SRuser);
                List<Contact> contactSR = new List<Contact>();
                
                contactSR = [Select Id,Salutation,Middle_Names__c,Occupation__c,MobilePhone,
                                                      Nationality_Lookup__c,Birthdate,Place_of_Birth__c,Passport_No__c 
                                                      from Contact 
                                                      where Id =:SRuser[0].contactId];
                
                if(tmprev.size()==0 || SRuser.size()==0){
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Invalid license number or no active user found.'));
                    return null;
                
                }else if(SRuser.size() > 0 && SRuser[0].Community_User_Role__c != null && SRuser[0].Community_User_Role__c.contains('IT Services') == false){
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'You must have an active IT Service User'));
                    return null;
                    
                }else{
                    sUser.Roles__c = SRuser[0].Community_User_Role__c.contains('IT Services') ? SRuser[0].Community_User_Role__c.remove(';IT Services') : SRuser[0].Community_User_Role__c;
                    sUser.Customer__c = tmprev[0].Account__c;
                    sUser.SR_Template__c = template[0].Id;
                    sUser.SR_Group__c='IT';
                    sUser.Submitted_DateTime__c = DateTime.now();
                    //Date subDate = Date.today();
                    sUser.Submitted_Date__c = Date.today();
  
                    sUser.First_Name__c = SRuser[0].firstname;
                    sUser.Email__c = SRuser[0].email;
                    sUser.Middle_Name__c = contactSR[0].Middle_Names__c;
                    sUser.Last_Name__c = SRuser[0].lastname;
                    if(contactSR.size()>0 && contactSR!=null){
                        sUser.Title__c = contactSR[0].Salutation;
                        sUser.Position__c = contactSR[0].Occupation__c;
                        sUser.Place_of_Birth__c = contactSR[0].Place_of_Birth__c;
                        sUser.Date_of_Birth__c = contactSR[0].Birthdate;
                        sUser.Nationality__c = contactSR[0].Nationality_Lookup__c;
                        sUser.Passport_Number__c = contactSR[0].Passport_No__c;
                    }
                 
                    insert sUser;  

                    sUser.External_SR_Status__c = status[0].Id;
                    sUser.Internal_SR_Status__c = status[0].Id;
                   
                    
                    update sUser;
                    PageReference redirectPage = new PageReference(Site.getpathprefix()+'/apex/ITAccessForm?&SRNo='+sUser.Id);
                    redirectPage.setRedirect(true);
                    return redirectPage;
                }
         }catch(Exception e){
              Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Invalid license number or no active user found.'));
              Database.rollback(sp);
              return null;
         }
     }else{ 
              Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill all the required fields.'));
              return null;   
     }
    
  }
    

  public pagereference ChangeOption(){
        sUser.User_Form_Action__c = strSelVal;
        system.debug('strSelVal====>'+strSelVal);
        if(strSelVal=='Create New User'){
            Flag = true;
        }else{
            Flag = false;
        }
        return null;
  }


//********************Custom Code for the Steps********************//

    public static String createUser(Step__c stp){

        Integer val=0;
        Boolean userFlg = false;
        String IsExistingUsr = '';
        String statusMsg;
        User usr = new User();
        if(stp!=null){
            List<Service_Request__c> SRUser = [Select Id, Customer__c,Send_SMS_To_Mobile__c,Nationality__c,First_Name__c,
                                                      Last_Name__c, Email__c, Roles__c,Passport_Number__c,Position__c,
                                                      Place_of_Birth__c,Title__c,Middle_Name__c,Date_of_Birth__c
                                                      from Service_Request__c 
                                                      where Id =:stp.SR__c]; 
            if(SRUser.size()!=0 && SRUser != null){      
                String RecordTypeId;      
                for(RecordType objRT : [select Id,Name from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true])
                      RecordTypeId = objRT.Id;
                            
                Contact contact = new Contact();
                list<Contact> pcont = [Select Id,AccountId,Passport_No__c,Email from Contact where AccountId =:SRUser[0].Customer__c and RecordTypeId=:RecordTypeId and Passport_No__c=:SRUser[0].Passport_Number__c order by createddate];
                list<User> newUsr = new list<User>(); 
                if(pcont.size() > 0){
                    contact.Id = pcont[0].id;
                    newUsr = [Select Id,username,CompanyName from User where ContactId =:pcont[0].Id AND IsActive=true];
                }else{
                    contact.AccountId = SRUser[0].Customer__c;
                    contact.FirstName = SRUser[0].First_Name__c;
                    contact.LastName= SRUser[0].Last_Name__c;
                    contact.MobilePhone = SRUser[0].Send_SMS_To_Mobile__c;
                    contact.Email = SRUser[0].Email__c;
                    contact.Nationality_Lookup__c = SRUser[0].Nationality__c;
                    contact.Passport_No__c = SRUser[0].Passport_Number__c;
                    contact.Middle_Names__c = SRUser[0].Middle_Name__c;
                    contact.Place_of_Birth__c = SRUser[0].Place_of_Birth__c;
                    contact.Birthdate = SRUser[0].Date_of_Birth__c;
                    contact.Salutation = SRUser[0].Title__c;
                    contact.Occupation__c = SRUser[0].Position__c;
                    contact.Country__c = 'United Arab Emirates';
                    contact.RecordTypeId = RecordTypeId;
                    contact.Role__c = SRUser[0].Roles__c;//v2.4  - Added by Mudasir in order to populate the AccountContactRelation properly
                    upsert contact;                 
                }
                
                List<String> emailstr = SRUser[0].Email__c.split('@');
                string usernameStr = emailstr[0]+Label.Portal_UserDomain;
                
                //Check if same username exists.
                if(newUsr.size()==0){
                    IsExistingUsr=usernameExists(usernameStr);
                    if(IsExistingUsr=='Success'){
                        userFlg = true;
                    }else if(IsExistingUsr=='found'){
                        userFlg = false;
                        
                        //for(integer i=0;i<=50;i++){ v1.2
                        for(Integer i=1;i<=Integer.valueof(Label.Username_limit);i++){ // v1.2
                                    val++;
                               usernameStr = emailstr[0]+'_'+val+Label.Portal_UserDomain;
                               IsExistingUsr=usernameExists(usernameStr);
                               if(IsExistingUsr=='Success'){
                                         userFlg = TRUE;
                                         break;
                                    }
                        }
                                userFlg = TRUE;
                    }
                    
                    if(userFlg == true){       
                        usr.contactId=contact.Id;
                        usr.username = usernameStr;
                        usr.CompanyName = SRUser[0].Customer__c;
                        usr.firstname=SRUser[0].First_Name__c;
                        usr.lastname=SRUser[0].Last_Name__c;
                        usr.email=SRUser[0].Email__c;
                        usr.phone = SRUser[0].Send_SMS_To_Mobile__c;
                        usr.Title = SRUser[0].Position__c;
                        usr.communityNickname = (SRUser[0].Last_Name__c +string.valueof(Math.random()).substring(4,9));
                        usr.alias = string.valueof(SRUser[0].Last_Name__c.substring(0,1) + string.valueof(Math.random()).substring(4,9));            
                        usr.profileid = Label.Profile_ID;
                        usr.emailencodingkey='UTF-8';
                        usr.languagelocalekey='en_US';
                        usr.localesidkey='en_GB';
                        usr.timezonesidkey='Asia/Dubai';
                        usr.Community_User_Role__c = SRUser[0].Roles__c;
                        
                        Service_Request__c updateSR = new Service_Request__c();
                        updateSR.Id = SRUser[0].Id;
                        updateSR.Username__c = usernameStr;
                        contact.Portal_Username__c = usernameStr;
                        contact.Role__c = SRUser[0].Roles__c;
                        //update contact;
                        
                        //Mail alert Coding with temporary password
                        Database.DMLOptions dlo = new Database.DMLOptions();
                        dlo.EmailHeader.triggerUserEmail = true;
                        dlo.EmailHeader.triggerAutoResponseEmail= true;
                        usr.setOptions(dlo);   
                            
                        try{
                             update contact;
                             insert usr;
                             update updateSR;
                                    
                             statusMsg = 'Success';
                        }catch(Exception e){
                             statusMsg = string.valueOf(e.getMessage());
                        }    
                    }
                }else{
                    usr.Id = newUsr[0].Id;
                    usr.Community_User_Role__c = SRUser[0].Roles__c;
                    //usr.firstname = SRUser[0].First_Name__c;
                    //usr.lastname = SRUser[0].Last_Name__c;
                    //usr.email = SRUser[0].Email__c;
                    //usr.phone = SRUser[0].Send_SMS_To_Mobile__c;
                    //usr.Title = SRUser[0].Position__c;  

                    Service_Request__c updateSR = new Service_Request__c();
                    updateSR.Id = SRUser[0].Id;
                    updateSR.Username__c = newUsr[0].username;
                    
                    contact.Portal_Username__c = newUsr[0].username;
                    contact.Role__c = SRUser[0].Roles__c;

                    try{
                         update contact;
                         update usr;
                         update updateSR;
                                
                         statusMsg = 'Success';
                    }catch(Exception e){
                         statusMsg = string.valueOf(e.getMessage());
                    }                   
                }
            }
        }
         return statusMsg;
    
    }
    
  //method to check if username exists
    public static string usernameExists(string username){
     
            list<User> usrLst = [SELECT id,username from User where username=:username];
            if(usrLst!=null && usrLst.size() > 0){
                return 'found';
            }
            return 'Success';
    }
    
   public static string revokeUser(ID stp){
        String statusMsg;
        if(stp!=null){
             List<Service_Request__c> SRUser = [Select Id, Customer__c,Roles__c,Username__c,Passport_Number__c 
                                                from Service_Request__c 
                                                where Id =:stp]; 
             
             if(SRUser.size()!=0 && SRUser != null){
                 List<User> tempRem = [Select Id,contactId,Community_User_Role__c from User where username =:SRUser[0].Username__c];
                 if(tempRem!= null && tempRem.size()>0){
                    if(tempRem[0].Community_User_Role__c != null && tempRem[0].Community_User_Role__c == 'IT Services') {
                        
                        removeUser(tempRem[0].Id , SRUser[0].Roles__c , false);
                        /*
                        User usr = new User();
                        usr.Id = tempRem[0].Id;
                        usr.IsActive = false;
                        if(!test.isRunningTest()){
                            try{
                                 update usr;    
                                 statusMsg = 'Success';
                            }catch(Exception e){
                                 statusMsg = string.valueOf(e.getMessage());
                            }                           

                        } 
                        */                      
                    }else if(tempRem[0].Community_User_Role__c != null && tempRem[0].Community_User_Role__c.contains('IT Services')){

                        Contact portalUserContact = new Contact();
                        portalUserContact.Id = tempRem[0].contactId;
                        portalUserContact.Role__c = SRUser[0].Roles__c;
                        
                        if(!test.isRunningTest()){
                            try{
                                 update portalUserContact;  
                                 statusMsg = 'Success';
                                 removeUser(tempRem[0].Id , SRUser[0].Roles__c , true);
                            }catch(Exception e){
                                 statusMsg = string.valueOf(e.getMessage());
                            }                           

                        }                           
                    }

                 }
             }
        }
        return statusMsg;
   }
   @future
   public static void removeUser(Id userId, string roles, boolean deactivateUser){

   if(!test.isRunningTest()){
        try{
            User usr = new User();
            usr.Id = userId;
            usr.Community_User_Role__c = roles;
            if(deactivateUser == false){
                usr.IsActive = false;   
            }   
            update usr;    
        }catch(Exception e){
            system.debug( 'Updating user get error ' + e.getMessage());
        }                           
    }       
   
   }
       
       
    
}