/**************************************************************************************************
  Author    :  Durga Prasad
  Company    :  NSI
  Class Name  :  Security_EmailCls
  Description  :  This is an Custom code which will sends the Security check email for the Approvers.
    ---------------------------------------------------------------------------------------------------------------------
    Modification History
    ---------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    ---------------------------------------------------------------------------------------------------------------------             
    V1.0    31-05-2015  Saima         ToEmailAddress custom settings for setCCaddress
    V1.1    04-04-2016  Sravan        Added Excel attachment for the outgoing email with Individual Details
    V1.2    28-06-2016  Sravan        Added Contais check to eliminate null pointer Exception
    V1.3    01-02-2017  Sravan        Added Excel attachment for the outgoing email with Body Corporate Details and Email for Recognized companies # 3736
    V1.4    11/04/2017  Sravan        Modified Document fetch logic for Recognized companies as per email from business.
    V1.5    15/04/2017  Sravan        Added certificate of Incorporation for Body Corporate UBO for Recognized and Non Recognized companies
    V1.6    16/04/2017  Sravan        Re-route the emails based on attachment Size as per tkt # 3989 
    V1.7    13/06/2017  Sravan        Added logic to collect documents when the same person existing with different Relationship Types
    V1.8    19/11/2017  Arun        Added code to send email when UBO of BC is added .
    V1.9    06/02/2018  Arun        email will go without UBO documents
    V2.1    10/Feb/2019 Arun        Merge Recognized and non Recognized company security email.
    V2.2    27/May/2019 Arun        Fixed issue for #6866 
    
**************************************************************************************************/ 
global without sharing class Security_EmailCls {

    webservice static void Send_Email_for_Verification(Step__c stp)
    {
        /* V2.1
       system.debug('++++++++++++++++++++Async Test'+stp);       
       if(stp.SR__r.Legal_Structures__c =='FRC' || stp.SR__r.Legal_Structures__c =='RLLP' || stp.SR__r.Legal_Structures__c =='RP' || stp.SR__r.Legal_Structures__c =='RLP')
        Security_EmailCls.SendSecurityEmailforRecognizedComp(stp.id);
       else 
           */
         Security_EmailCls.Send_Email_for_Verification_Async(stp.id);   
    }   
    
    @future(callout=true)
    public static void Send_Email_for_Verification_Async(id stpID)
    {
        
        // V1.1        
        SObjectType objTyp = Schema.getGlobalDescribe().get('Step__c');
        Map<String,Schema.SObjectField> mfields = objTyp.getDescribe().fields.getMap();
        system.debug('+++++*Dyn Query'+mfields);
        string stepQuery ='select SR__r.Legal_Structures__c,SR__r.Customer__c,SR__r.Name';
        integer attachmentsSize=0; // V1.6 
        
        for(string field : mfields.keySet())
        {
            stepQuery += ','+field ;
        }        
        stepQuery += ' from step__c where id=:stpID';        
        //system.debug('Step Query String++'+stepQuery);        
        Step__c stp = database.query(stepQuery); // V1.1  End  
        
        //V1.7 Collecting SR Docs
    List<SR_Doc__c> srDocsAvailable = new List<SR_Doc__c>();
    map<string,SR_Doc__c> individualPassportCopies = new map<string,SR_Doc__c>();
    
    for(SR_Doc__c objSR_Doc:[select id,Doc_ID__c,Name,Document_Master__c,Document_Master__r.Code__c,Amendment__c,Amendment__r.Amendment_Type__c,Amendment__r.Passport_No__c,Amendment__r.Relationship_Type__c,Amendment__r.Customer__r.name,Amendment__r.Customer__r.Company_Type__c,Amendment__r.ServiceRequest__r.Legal_Structures__c,amendment__r.Name_of_incorporating_Shareholder__c from SR_Doc__c where Doc_ID__c!=null and Service_Request__c=:stp.SR__c and (Amendment__r.Amendment_Type__c ='Individual' or Amendment__r.Amendment_Type__c ='Body Corporate')]){
      srDocsAvailable.add(objSR_Doc);
      if(objSR_Doc.Amendment__r.Amendment_Type__c == 'Individual' && objSR_Doc.Amendment__r.Passport_No__c!=null && objSR_Doc.Name.contains('Passport'))
          individualPassportCopies.put(objSR_Doc.Amendment__r.Passport_No__c,objSR_Doc);
        }
    
    //V1.7 Collecting Amendments
    
    list<Attachment> lstAttachments = new list<Attachment>();
        set<id> setAttachIds = new set<id>();        
        map<string,string> mapDocumentName = new map<string,string>();        
        map<string,list<string>> mapPassportCopy = new map<string,list<string>>();
        Map<string,set<string>> bodyCorporateRelationTyp = new map<string,set<string>>(); // V1.5
        boolean bSendEmail = false;
    list<string> lstPassport;
    List<Amendment__c> amendmentList = new List<Amendment__c>();
    
    for(Amendment__c amnd:[select id,Passport_No__c,Relationship_Type__c,Amendment_Type__c,Company_Name__c,ServiceRequest__r.Legal_Structures__c,Name_of_incorporating_Shareholder__c from Amendment__c where ServiceRequest__c=:stp.SR__c and Relationship_Type__c!=null and (Amendment_Type__c='Individual' or Amendment_Type__c='Body Corporate')]){  
          if(amnd.Amendment_Type__c == 'Individual'){
           lstPassport = new list<string>();
          if(mapPassportCopy.get(amnd.Relationship_Type__c)!=null)
            lstPassport = mapPassportCopy.get(amnd.Relationship_Type__c);
        lstPassport.add(amnd.Passport_No__c);
             mapPassportCopy.put(amnd.Relationship_Type__c,lstPassport);
          }else  if(amnd.Amendment_Type__c == 'Body Corporate' && (amnd.Relationship_Type__c == 'Shareholder' || amnd.Relationship_Type__c == 'Member' || amnd.Relationship_Type__c == 'General Partner' || amnd.Relationship_Type__c == 'Limited Partner' || amnd.Relationship_Type__c == 'Founding Member')){  //V1.5 Start
              set<string> bcRelation = new set<string>();
      if(bodyCorporateRelationTyp.get(amnd.Relationship_Type__c) !=null)
        bcRelation = bodyCorporateRelationTyp.get(amnd.Company_Name__c.toLowerCase());
        bcRelation.add(amnd.Relationship_Type__c);
        bodyCorporateRelationTyp.put(amnd.Company_Name__c.toLowerCase(),bcRelation);    // Relationship types with Companies
           } // End V1.5   
       amendmentList.add(amnd);
    }

     set<string> IndShareholdertype=new Set<string>{'Designated Member','Shareholder','Member','Limited Partner','General Partner','Founding Member','Founder'};
     
    // Processing Individual Amendment Docs
        for(Amendment__c amnd: amendmentList)
        {    
       // Collecting individual passport Copies V1.7 //V2.2
       if(amnd.Amendment_Type__c == 'Individual' && IndShareholdertype.contains(amnd.Relationship_Type__c) && individualPassportCopies.containsKey(amnd.Passport_No__c))
       {
        setAttachIds.add(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c);
        mapDocumentName.put(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c,individualPassportCopies.get(amnd.Passport_No__c).Name);
        bSendEmail = true;
       }
       /*
       else if(amnd.Amendment_Type__c == 'Individual' && amnd.ServiceRequest__r.Legal_Structures__c == 'Public Company' && amnd.Relationship_Type__c =='Shareholder' &&  individualPassportCopies.containsKey(amnd.Passport_No__c))
       {
        setAttachIds.add(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c);
        mapDocumentName.put(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c,individualPassportCopies.get(amnd.Passport_No__c).Name);
        bSendEmail = true;  
       }else if(amnd.Amendment_Type__c == 'Individual' && amnd.ServiceRequest__r.Legal_Structures__c == 'LP' && (amnd.Relationship_Type__c =='Limited Partner' || amnd.Relationship_Type__c=='General Partner') && individualPassportCopies.containsKey(amnd.Passport_No__c)){
        setAttachIds.add(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c);
        mapDocumentName.put(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c,individualPassportCopies.get(amnd.Passport_No__c).Name);
        bSendEmail = true;   
      }else if(amnd.Amendment_Type__c == 'Individual' && amnd.ServiceRequest__r.Legal_Structures__c == 'GP' && amnd.Relationship_Type__c =='General Partner' && individualPassportCopies.containsKey(amnd.Passport_No__c)){
        setAttachIds.add(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c);
        mapDocumentName.put(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c,individualPassportCopies.get(amnd.Passport_No__c).Name);
        bSendEmail = true;  
      }else if(amnd.Amendment_Type__c == 'Individual' && amnd.ServiceRequest__r.Legal_Structures__c == 'NPIO' && amnd.Relationship_Type__c =='Founding Member' && individualPassportCopies.containsKey(amnd.Passport_No__c)){
        setAttachIds.add(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c);
        mapDocumentName.put(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c,individualPassportCopies.get(amnd.Passport_No__c).Name);
        bSendEmail = true;
      }
      else if(amnd.Amendment_Type__c == 'Individual' && amnd.ServiceRequest__r.Legal_Structures__c == 'LLP' && amnd.Relationship_Type__c =='Designated Member' && individualPassportCopies.containsKey(amnd.Passport_No__c))
      {
        setAttachIds.add(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c);
        mapDocumentName.put(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c,individualPassportCopies.get(amnd.Passport_No__c).Name);
        bSendEmail = true;
      }
      else if(amnd.Amendment_Type__c == 'Individual' && amnd.ServiceRequest__r.Legal_Structures__c == 'RLLP' && amnd.Relationship_Type__c =='Designated Member' && individualPassportCopies.containsKey(amnd.Passport_No__c))
      {
        setAttachIds.add(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c);
        mapDocumentName.put(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c,individualPassportCopies.get(amnd.Passport_No__c).Name);
        bSendEmail = true;
      }*/
      

        if(amnd.Relationship_Type__c!=null && amnd.Relationship_Type__c == 'UBO' && amnd.Amendment_Type__c == 'Individual' && amnd.Passport_No__c!=null)
        {
              
              //Email will go without UBO documents #4748
               bSendEmail = true;
               /*
                if(individualPassportCopies.containsKey(amnd.Passport_No__c) && amnd.Name_of_incorporating_Shareholder__c !=null &&  bodyCorporateRelationTyp.containsKey(amnd.Name_of_incorporating_Shareholder__c.toLowerCase()) && 
                                    ((stp.SR__r.Legal_Structures__c=='NPIO' && bodyCorporateRelationTyp.get(amnd.Name_of_incorporating_Shareholder__c.toLowerCase()).contains('Founding Member'))||
                                                        (stp.SR__r.Legal_Structures__c=='GP'  && bodyCorporateRelationTyp.get(amnd.Name_of_incorporating_Shareholder__c.toLowerCase()).contains('General Partner')) ||
                                                        (stp.SR__r.Legal_Structures__c=='LP' && (bodyCorporateRelationTyp.get(amnd.Name_of_incorporating_Shareholder__c.toLowerCase()).contains('Limited Partner') || bodyCorporateRelationTyp.get(amnd.Name_of_incorporating_Shareholder__c.toLowerCase()).contains('General Partner'))) ||
                                                        (stp.SR__r.Legal_Structures__c=='LLC' && bodyCorporateRelationTyp.get(amnd.Name_of_incorporating_Shareholder__c.toLowerCase()).contains('Member')) ||
                                                        (stp.SR__r.Legal_Structures__c.indexOf('LTD')>-1 && bodyCorporateRelationTyp.get(amnd.Name_of_incorporating_Shareholder__c.toLowerCase()).contains('Shareholder')) ||
                                                        (stp.SR__r.Legal_Structures__c=='LLP' && bodyCorporateRelationTyp.get(amnd.Name_of_incorporating_Shareholder__c.toLowerCase()).contains('Designated Member'))))                                        
                {                  
          setAttachIds.add(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c);
          mapDocumentName.put(individualPassportCopies.get(amnd.Passport_No__c).Doc_ID__c,individualPassportCopies.get(amnd.Passport_No__c).Name);
          bSendEmail = true;
                
                }    */
                           
            }
            
                //V1.8 
                if(amnd.Relationship_Type__c!=null && amnd.Relationship_Type__c == 'UBO' && amnd.Amendment_Type__c == 'Body Corporate' && amnd.Passport_No__c==null)
                {
                     bSendEmail = true;
                }
        }  
    
    // Processing Body Corporate Amendment Docs     
        for(SR_Doc__c objSR_Doc: srDocsAvailable)
        {
            
            system.debug('Rel Type==>'+objSR_Doc.Amendment__r.Relationship_Type__c);
            if(objSR_Doc.Amendment__r.Amendment_Type__c == 'Body Corporate' && objSR_Doc.Amendment__c !=null)
            { 
        
        //   V2.0
            if(objSR_Doc.Name.contains('Certificate of Incorporation'))
            {
              setAttachIds.add(objSR_Doc.Doc_ID__c);
              mapDocumentName.put(objSR_Doc.Doc_ID__c,objSR_Doc.Name);
              bSendEmail = true;   
            }
        
        //V1.9 Logicall not required when 
        /*
        if(objSR_Doc.Amendment__r.ServiceRequest__r.Legal_Structures__c.indexOf('LTD')>-1 && objSR_Doc.Amendment__r.Relationship_Type__c =='Shareholder' && objSR_Doc.Name.contains('Certificate of Incorporation'))
        {
          setAttachIds.add(objSR_Doc.Doc_ID__c);
          mapDocumentName.put(objSR_Doc.Doc_ID__c,objSR_Doc.Name);
          bSendEmail = true;   
        }
        
        else if(objSR_Doc.Amendment__r.ServiceRequest__r.Legal_Structures__c == 'LLC' && objSR_Doc.Amendment__r.Relationship_Type__c =='Member' && objSR_Doc.Name.contains('Certificate of Incorporation')){
          setAttachIds.add(objSR_Doc.Doc_ID__c);
          mapDocumentName.put(objSR_Doc.Doc_ID__c,objSR_Doc.Name);
          bSendEmail = true;   
        }else if(objSR_Doc.Amendment__r.ServiceRequest__r.Legal_Structures__c == 'LP' && (objSR_Doc.Amendment__r.Relationship_Type__c =='Limited Partner' || objSR_Doc.Amendment__r.Relationship_Type__c =='General Partner') && objSR_Doc.Name.contains('Certificate of Incorporation')){
          setAttachIds.add(objSR_Doc.Doc_ID__c);
          mapDocumentName.put(objSR_Doc.Doc_ID__c,objSR_Doc.Name);
          bSendEmail = true;   
        }else if(objSR_Doc.Amendment__r.ServiceRequest__r.Legal_Structures__c == 'GP' && objSR_Doc.Amendment__r.Relationship_Type__c =='General Partner' && objSR_Doc.Name.contains('Certificate of Incorporation')){
          setAttachIds.add(objSR_Doc.Doc_ID__c);
          mapDocumentName.put(objSR_Doc.Doc_ID__c,objSR_Doc.Name);
          bSendEmail = true;   
        }else if(objSR_Doc.Amendment__r.ServiceRequest__r.Legal_Structures__c == 'NPIO' && objSR_Doc.Amendment__r.Relationship_Type__c =='Founding Member' && objSR_Doc.Name.contains('Certificate of Incorporation')){
          setAttachIds.add(objSR_Doc.Doc_ID__c);
          mapDocumentName.put(objSR_Doc.Doc_ID__c,objSR_Doc.Name);
          bSendEmail = true;
        }else if(objSR_Doc.Amendment__r.ServiceRequest__r.Legal_Structures__c == 'LLP' && objSR_Doc.Amendment__r.Relationship_Type__c =='Designated Member' && objSR_Doc.Name.contains('Certificate of Incorporation')){
          setAttachIds.add(objSR_Doc.Doc_ID__c);
          mapDocumentName.put(objSR_Doc.Doc_ID__c,objSR_Doc.Name);
          bSendEmail = true;
        }     
            */      
           
          } // V1.5 End
            
            
            
        }
        
        system.debug('setAttachIds==>'+setAttachIds);
        
        string strTemplateId = '';
        for(EmailTemplate objTempl:[select id from EmailTemplate where DeveloperName='Email_to_Security_Team']){
            strTemplateId = objTempl.Id;
        }
        
        transient list<Messaging.EmailFileAttachment> lstMailAttachments = new list<Messaging.EmailFileAttachment>();
        list<Messaging.SingleEmailMessage> LstEmailsToSend = new list<Messaging.SingleEmailMessage>();
        
        for(Attachment objAtch:[select id,Name,Body,ContentType,BodyLength from Attachment where ID IN:setAttachIds]){
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setBody(objAtch.Body);
            string strFileName = mapDocumentName.get(objAtch.Id);
            if(strFileName!=null && strFileName!=''){
                if(objAtch.Name.toLowerCase().indexOf('.pdf')>-1){
                    strFileName = strFileName+'.pdf';
                }else if(objAtch.Name.toLowerCase().indexOf('.jpeg')>-1){
                    strFileName = strFileName+'.jpeg';
                }else if(objAtch.Name.toLowerCase().indexOf('.jpg')>-1){
                    strFileName = strFileName+'.jpg';
                }else if(objAtch.Name.toLowerCase().indexOf('.png')>-1){
                    strFileName = strFileName+'.png';
                }else if(objAtch.Name.toLowerCase().indexOf('.bmp')>-1){
                    strFileName = strFileName+'.bmp';
                }else if(objAtch.Name.toLowerCase().indexOf('.doc')>-1){
                    strFileName = strFileName+'.doc';
                }else if(objAtch.Name.toLowerCase().indexOf('.docx')>-1){
                    strFileName = strFileName+'.docx';
                }
            }else{
                strFileName = objAtch.Name;
            }
            attachment.setFileName(strFileName);
            attachment.setinline(false);
            attachmentsSize += objAtch.BodyLength; //V1.6 
            lstMailAttachments.add(attachment); 
        }
        
       
        Savepoint sp = Database.SetSavePoint();
       
        Contact securityContact = new Contact();
        securityContact.LastName = 'Security Check';
        securityContact.FirstName = 'Contact';
        if(attachmentsSize <= 3145728) //V1.6 
          securityContact.email = label.Security_Email;
        else
            securityContact.email = label.ROC_help_desk_Email;  
        insert securityContact;
        
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'portal@difc.ae'];//noreply.portaluat@difc.ae
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        
        //V1.0 Added by Saima on 31-05-2015
        String[] ToEmails= new String[]{};
        list<To_Email_Address__c> em = To_Email_Address__c.getall().values();
        system.debug('Emailssss===>>  '+em);
        for(To_Email_Address__c emSet:em){
            ToEmails.add(emSet.Email_To__c);
        }//V1.0
        if(ToEmails.size() > 0)
            mail.setCCAddresses(ToEmails);
        //mail.setCCAddresses(new String[] {'Alya.AlZarouni@difc.ae','Hanaa.Almunaifi@difc.ae','muthahara.niamathullah@difc.ae','Rowena.Ricafort@difc.ae'});
        mail.setReplyTo('portal@difc.ae');
        system.debug('sent email to '+mail);
        if(strTemplateId!=null && strTemplateId!=''){
            mail.setWhatId(stp.SR__c);
            mail.setTargetObjectId(securityContact.id);
            mail.setTemplateId(strTemplateId);
        }else{
            mail.setSubject('Security Check Email');
            mail.setPlainTextBody('Please Review the passport copies.');
        }
        if(owea!=null && owea.size()>0)
        mail.setOrgWideEmailAddressId(owea.get(0).Id);
        
        //  V1.1 
        pagereference pr = new pagereference('/apex/Securityemaildoc_Non_Recognized_vf?SRID='+stp.SR__c+'&contentTyp=xls');
        Messaging.EmailFileAttachment securityAttachment = new Messaging.EmailFileAttachment();
       
        if(!test.isRunningTest())            
            securityAttachment.setBody(pr.getContent());
        else
             securityAttachment.setBody(blob.valueOf('Unit Test'));    
        securityAttachment.setContentType('application/vnd.ms-excel#Securitymailtemplate.xls');
        securityAttachment.setInline(false);
        securityAttachment.setFileName('Security mail template.xls');
        lstMailAttachments.add(securityAttachment);
        //  V1.1 
        
       
        
       // system.debug('Email Attachments++s'+securityAttachment);
        
        if(lstMailAttachments!=null && lstMailAttachments.size()>0)
            mail.setFileAttachments(lstMailAttachments);
            
        system.debug('bSendEmail=>'+bSendEmail);
        LstEmailsToSend.add(mail);
        system.debug('LstEmailsToSend=>'+LstEmailsToSend);
        
        if(LstEmailsToSend!=null && LstEmailsToSend.size()>0)
        {
            
        try
        {
              if(bSendEmail)
             {
             SR_Doc__c objSRDoc = new SR_Doc__c();
             objSRDoc.Service_Request__c = stp.SR__c;
             objSRDoc.Name = 'Security Email';
             objSRDoc.Status__c = 'Generated';
             objSRDoc.Step__c = stp.id;
             objSRDoc.Unique_SR_Doc__c = stp.sr__c+'_'+stp.Id+'_'+'SecurityEmail';
             insert objSRDoc;
             system.debug('------>objSRDoc'+objSRDoc.id);
            
             pagereference securityDoc = new pagereference('/apex/Securityemaildoc_Non_Recognized_vf?SRID='+stp.sr__c+'&contentTyp=pdf');         
             Attachment securityEmailAttachment = new Attachment();
             securityEmailAttachment.Name = 'Security Email.pdf';
             if(!test.isRunningTest())
               securityEmailAttachment.Body = securityDoc.getContentAsPDF();
             else 
               securityEmailAttachment.Body = blob.valueOf('Test');
             securityEmailAttachment.parentID = objSRDoc.id;
             securityEmailAttachment.ContentType = '.pdf';
             insert securityEmailAttachment;     
             system.debug('Security Email Attachment------->'+securityEmailAttachment); 
                  
                  List<Messaging.SendEmailResult> results = Messaging.sendEmail(LstEmailsToSend);
                  if(results.size() >0 && !results[0].isSuccess()){
                      //Database.RollBack(sp);
                      List<messaging.SendEmailError> emailError = results[0].getErrors();                    
                      if(emailError !=null && emailError[0].getMessage() !=null){                     
                        Log__c emailFailurelog = new Log__c();
                        emailFailurelog.Account__c = stp.SR__r.customer__c;
                        emailFailurelog.Description__c =  'Unable to send security email for Service Request :'+stp.SR__r.Name +' Account :'+stp.Customer_Name__c+' Error Text:'+emailError[0].getMessage();
                        emailFailurelog.Type__c   = 'Security Email';
                        insert emailFailurelog;
                      }
                             
                 }
                  
                  system.debug('email send1->');
                  delete securityContact;
              }else if(!bsendEmail || LstEmailsToSend.isEmpty()){
                Database.RollBack(sp);  
                Log__c log = new Log__c();
                log.Account__c = stp.SR__r.customer__c;
                log.Description__c =  'Unable to send security email for Service Request :'+stp.SR__r.Name +' Account :'+stp.Customer_Name__c;
                log.Type__c   = 'Security Email';
                insert log;
              }else
                  delete securityContact;
              
                
                
               
            }catch(Exception e){
         database.RollBack(SP);  
               Log__c log = new Log__c();
                log.Account__c = stp.SR__r.customer__c;
                log.Description__c =  'Unable to send security email for Service Request :'+stp.SR__r.Name +' Account :'+e.getMessage();
                log.Type__c   = 'Security Email';
                insert log;
            }
        }      
    } 
    //V1.3 Start
    @future(callout=true)
     Public static void SendSecurityEmailforRecognizedComp(id stepID)
     {
        
        //V2.0 Start
        /*
        
         step__c stp = [select id,SR__c,sr__r.name,sr__r.Customer__c from step__c where id= : stepID]  ;       
         string SRID = stp.SR__c;
          map<string,Amendment__c> bcAmmendmentDetails = new map<string,Amendment__c>();
          List<amendment__c> amendmentTempDtls = new List<amendment__c>();         
          List<amendment__c> amendmentDtls = new List<amendment__c>();
          List<Messaging.EmailFileAttachment> MailAttachments = new List<Messaging.EmailFileAttachment>();
      map<id,string> uboAmendments = new map<id,string>(); // V1.4
      map<string,set<id>> amendWithPassPrtc= new map<string,set<id>>(); // V1.4
      map<id,string> amdwithPassprtNo = new map<id,string>(); // V1.4
      
       
     
      integer attachmentSize = 0; // V1.6 
          
         for(Amendment__c amnd :[select ServiceRequest__r.Foreign_Registration_Name__c,Company_Name__c,Customer__r.name,Customer__r.Company_Type__c,Customer__c,Given_Name__c,Family_Name__c,id,name,Relationship_Type__c,ServiceRequest__c,Amendment_Type__c,Passport_No__c,
                                ServiceRequest__r.Legal_Structures__c,Nationality_list__c,Phone__c,Gender__c,Shareholder_Company__c,Shareholder_Company__r.Name,Name_of_incorporating_Shareholder__c,Name_of_Body_Corporate_Shareholder__c
                                from Amendment__c where ServiceRequest__c =:SRID and (ServiceRequest__r.Legal_Structures__c ='FRC' OR ServiceRequest__r.Legal_Structures__c ='RLLP' or ServiceRequest__r.Legal_Structures__c ='RLP' or ServiceRequest__r.Legal_Structures__c = 'RP')])
         {            
            if(amnd.Relationship_Type__c =='UBO' && amnd.Amendment_Type__c=='Individual' && amnd.ServiceRequest__r.Foreign_Registration_Name__c == amnd.Name_of_incorporating_Shareholder__c){
                bcAmmendmentDetails.put(amnd.Name_of_incorporating_Shareholder__c,amnd);
                amendmentDtls.add(amnd);
        amdwithPassprtNo.put(amnd.id,amnd.Passport_No__c.toLowerCase()); // V1.4
            }    
         
            //V1.4    
      if(amnd.Passport_No__c !=null && amendWithPassPrtc.containsKey(amnd.Passport_No__c.toLowerCase()) && amnd.Amendment_Type__c=='Individual')
        amendWithPassPrtc.get(amnd.Passport_No__c.toLowerCase()).add(amnd.id);
      else if(amnd.Passport_No__c !=null && !amendWithPassPrtc.containsKey(amnd.Passport_No__c.toLowerCase())){
          amendWithPassPrtc.put(amnd.Passport_No__c.toLowerCase(),new set<id>());
        amendWithPassPrtc.get(amnd.Passport_No__c.toLowerCase()).add(amnd.id);
      }  
      
      // End V1.4
         }
     
        map<id,string> DocumentNames = new map<id,string>();
        
        for(SR_Doc__c srd : [select id,Doc_ID__c,name,amendment__c,amendment__r.Passport_No__c,amendment__r.ServiceRequest__r.Foreign_Registration_Name__c,amendment__r.company_Name__c,amendment__r.amendment_type__c,SR_Template_Doc__r.Document_Master_Code__c from SR_Doc__c where Doc_ID__c !=null and Service_Request__c=:SRID and ((amendment__c in: amendmentDtls and Amendment__c !=null) or SR_Template_Doc__r.Document_Master_Code__c ='Certificate of Incorporation-FRC') and Doc_ID__c !=null]){                          
            if(amendWithPassPrtc.containsKey(srd.amendment__c) && srd.amendment__r.amendment_type__c =='Individual'){
        DocumentNames.put(srd.Doc_ID__c,srd.Name);  
                amdwithPassprtNo.remove(srd.amendment__c); //V1.4 
          
          }
          
          if( srd.SR_Template_Doc__r.Document_Master_Code__c =='Certificate of Incorporation-FRC')
            DocumentNames.put(srd.Doc_ID__c,srd.Name);
        }    //V1.4
    if(amdwithPassprtNo.size()>0){
      set<id> amdndDetailsTemp = new set<id>();
      for(string s: amdwithPassprtNo.values()){
        if(amendWithPassPrtc.containsKey(s))
          amdndDetailsTemp.addAll(amendWithPassPrtc.get(s));
      }
      
      for(SR_Doc__c srd : [select id,Doc_ID__c,name,amendment__c,amendment__r.Passport_No__c from SR_Doc__c where Doc_ID__c !=null and Service_Request__c=:SRID and amendment__c in: amdndDetailsTemp  and Doc_ID__c !=null and Amendment__c !=null]){                          
        if(srd.name.contains('Passport Copy'))
          DocumentNames.put(srd.Doc_ID__c,srd.Name);
      }
    }
    // End V1.4
        string TemplateId = '';
        for(EmailTemplate objTempl:[select id from EmailTemplate where DeveloperName='Email_to_Security_Team_BC']){
            TemplateId = objTempl.Id;
        }
        System.debug('DocumentNames==>'+DocumentNames);
         for(Attachment objAtch:[select id,Name,Body,ContentType,BodyLength from Attachment where ID IN:DocumentNames.keySet()]){
            Messaging.EmailFileAttachment attachment = new Messaging.EmailFileAttachment();
            attachment.setBody(objAtch.Body);
            string strFileName = DocumentNames.get(objAtch.id);
            if(strFileName!=null && strFileName!=''){
                if(objAtch.Name.toLowerCase().indexOf('.pdf')>-1){
                    strFileName = strFileName+'.pdf';
                }else if(objAtch.Name.toLowerCase().indexOf('.jpeg')>-1){
                    strFileName = strFileName+'.jpeg';
                }else if(objAtch.Name.toLowerCase().indexOf('.jpg')>-1){
                    strFileName = strFileName+'.jpg';
                }else if(objAtch.Name.toLowerCase().indexOf('.png')>-1){
                    strFileName = strFileName+'.png';
                }else if(objAtch.Name.toLowerCase().indexOf('.bmp')>-1){
                    strFileName = strFileName+'.bmp';
                }else if(objAtch.Name.toLowerCase().indexOf('.doc')>-1){
                    strFileName = strFileName+'.doc';
                }else if(objAtch.Name.toLowerCase().indexOf('.docx')>-1){
                    strFileName = strFileName+'.docx';
                }
            }else{
                strFileName = objAtch.Name;
            }
            attachmentSize += objAtch.BodyLength; // V1.6 
            attachment.setFileName(strFileName);
            attachment.setinline(false);
            MailAttachments.add(attachment); 
        }
         
        
      if(MailAttachments.size()>0){
           SavePoint sp;
        try{
           
            Contact securityContact = new Contact();
            securityContact.LastName = 'Security Check';
            securityContact.FirstName = 'Contact';
            if(attachmentSize <=3145728)
              securityContact.email = label.Security_Email; //V1.6 
            else
              securityContact.email = label.ROC_help_desk_Email; 
                
            sp = database.setSavepoint();      
            insert securityContact;
        
         
            
            list<Messaging.SingleEmailMessage> LstEmailsToSend = new list<Messaging.SingleEmailMessage>();
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'portal@difc.ae'];//noreply.portaluat@difc.ae
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] ToEmails= new String[]{};
            list<To_Email_Address__c> em = To_Email_Address__c.getall().values();        
            for(To_Email_Address__c emSet:em){
                ToEmails.add(emSet.Email_To__c);
            }
            if(ToEmails.size() > 0)
                mail.setCCAddresses(ToEmails); 
            mail.setReplyTo('portal@difc.ae');
        
        if(TemplateId!=null && TemplateId!=''){
            mail.setWhatId(SRID);
            mail.setTargetObjectId(securityContact.id);
            mail.setTemplateId(TemplateId);
        }else{
            mail.setSubject('Security Check Email');
            mail.setPlainTextBody('Please Review the passport copies.');
        }
        if(owea!=null && owea.size()>0)
        mail.setOrgWideEmailAddressId(owea.get(0).Id);
        
       
        pagereference prr = new pagereference('/apex/Securityemaildoc_Non_Recognized_vf?SRID='+SRID+'&contentTyp=xls');
        Messaging.EmailFileAttachment securityAttachmentBC = new Messaging.EmailFileAttachment();
        if(!test.isRunningTest())
            securityAttachmentBC.setBody(prr.getContent());
        else
             securityAttachmentBC.setBody(blob.valueOf('Unit Test'));    
        securityAttachmentBC.setContentType('application/vnd.ms-excel#Securitymailtemplate.xls');
        securityAttachmentBC.setInline(false);
        securityAttachmentBC.setFileName('Security mail template.xls');
        MailAttachments.add(securityAttachmentBC);      
        
       
        
        if(MailAttachments!=null && MailAttachments.size()>0)
            mail.setFileAttachments(MailAttachments);
            
       
        LstEmailsToSend.add(mail);
        
        if(LstEmailsToSend!=null && LstEmailsToSend.size()>0){
      
      SR_Doc__c objSRDoc = new SR_Doc__c();
      objSRDoc.Service_Request__c = stp.SR__c;
      objSRDoc.Name = 'Security Email';
      objSRDoc.Status__c = 'Generated';
      objSRDoc.Step__c = stp.id;
      objSRDoc.Unique_SR_Doc__c = stp.sr__c+'_'+stp.Id+'_'+'SecurityEmail';
      insert objSRDoc;
         
      pagereference securityDoc = new pagereference('/apex/Securityemaildoc_Non_Recognized_vf?SRID='+stp.sr__c+'&contentTyp=pdf');         
      Attachment securityEmailAttachment = new Attachment();
      securityEmailAttachment.Name = 'Security Email.pdf';
      securityEmailAttachment.Body = securityDoc.getContentAsPDF();
      securityEmailAttachment.parentID = objSRDoc.id;
      securityEmailAttachment.ContentType = '.pdf';
      insert securityEmailAttachment;         
      
    
           List<Messaging.SendEmailResult> results  =  Messaging.sendEmail(LstEmailsToSend); 
           system.debug('JKJKJKJKJK'+results); 
           List<messaging.SendEmailError> emailError;
           if(results!=null && results.size() >0 && !results[0].isSuccess())
              emailError = results[0].getErrors();
            if(emailError !=null && emailError[0].getMessage() !=null){
        database.RollBack(SP);
              Log__c emailFailurelog = new Log__c();
                emailFailurelog.Account__c = stp.SR__r.customer__c;
                emailFailurelog.Description__c =  'Unable to send security email for Service Request :'+stp.SR__r.Name +' Account :'+stp.Customer_Name__c+' Error Text:'+emailError[0].getMessage();
                emailFailurelog.Type__c   = 'Security Email';
                insert emailFailurelog;
             }                   
            delete securityContact;           
        }
    }
    catch(exception e){
     database.RollBack(sp); 
     Log__c objLog = new Log__c();
         objLog.Account__c = stp.sr__r.Customer__c;
         objLog.Type__c = 'Security Email';
         objLog.Description__c = 'Unable to send security email for Service Request :'+stp.SR__r.name+' Exception:'+e.getMessage();
         insert objLog; 
    }  
        
    }else{
      Log__c objLog = new Log__c();
         objLog.Account__c = stp.sr__r.Customer__c;
         objLog.Type__c = 'Security Email';
         objLog.Description__c = 'Unable to send security email for Service Request :'+stp.SR__r.name+' Error : Required documents not found';
         insert objLog;
    }  
    */
        
   }  
        
       
  
    
   // End V1.3  
    
  
}