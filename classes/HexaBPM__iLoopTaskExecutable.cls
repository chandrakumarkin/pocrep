/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global interface iLoopTaskExecutable {
    String GenerateLoopTask(List<HexaBPM__Step__c> param0, Map<String,Id> param1, Map<Id,String> param2, Map<Id,String> param3, Map<Id,String> param4, Map<Id,Id> param5, Map<Id,String> param6, Map<String,Id> param7, List<HexaBPM__SR_Steps__c> param8, HexaBPM__Action__c param9, HexaBPM__Step__c param10);
}
