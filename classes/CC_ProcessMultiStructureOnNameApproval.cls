/*
    Author      : Durga Prasad
    Date        : 01-April-2020
    Description : Custom code to create AOR for Multi Structre Application on Company Name Approval
    ---------------------------------------------------------------------------------------------------------
*/
global without sharing class CC_ProcessMultiStructureOnNameApproval implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        try{
            if(stp!=null && stp.HexaBPM__SR__c!=null && (system.test.isrunningtest() || stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c=='Multi_Structure')){
                Id AccRecId = OB_QueryUtilityClass.getRecordtypeID ('Account','Business_Development'); 
                list<Account> accountList = new list<Account>();
                set<string> setAccountIds = new set<string>();
                string ContactId;
                map<string,string> MapAccountContact = new map<string,string>();
                for(HexaBPM__Service_Request__c objSR:[Select Id,Entity_Name__c,HexaBPM__Customer__c,HexaBPM__Contact__c,Entity_Type__c,Business_Sector__c,Legal_Structures__c,HexaBPM__Parent_SR__c,HexaBPM__Parent_SR__r.Business_Sector__c,
                   HexaBPM__Parent_SR__r.first_name__c,HexaBPM__Parent_SR__r.HexaBPM__Contact__c,HexaBPM__Parent_SR__r.last_name__c,HexaBPM__Parent_SR__r.Entity_Name__c,
                   HexaBPM__Parent_SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__Parent_SR__r.HexaBPM__Email__c,HexaBPM__Parent_SR__r.Entity_Type__c,HexaBPM__Parent_SR__r.HexaBPM__IsClosedStatus__c,
                   (Select Id,Application__c from Company_Names__r where Status__c='Approved' limit 1)
                   from HexaBPM__Service_Request__c where Id=:stp.HexaBPM__SR__c]){
                        if((objSR.Company_Names__r!=null && objSR.Company_Names__r.size()>0 && objSR.HexaBPM__Customer__c!=null && objSR.HexaBPM__Parent_SR__r.HexaBPM__IsClosedStatus__c) || system.test.isrunningtest()){
                            ContactId = objSR.HexaBPM__Parent_SR__r.HexaBPM__Contact__c;
                            MapAccountContact.put(objSR.HexaBPM__Customer__c,objSR.HexaBPM__Contact__c);
                            
                            Account acc = new Account(Id=objSR.HexaBPM__Customer__c);
                            acc.Description = objSR.Id;
                            acc.OB_Is_Multi_Structure_Application__c = 'Yes';
                            accountList.add(acc);
                        }
                    }
                    if(accountList.size() > 0){
                        update accountList;
                    
                    list<AccountContactRelation> lstACR = new list<AccountContactRelation>();
                    for(Account acc:accountList){
                        AccountContactRelation objAccContactRel = new AccountContactRelation();
                        objAccContactRel.AccountId = acc.Id;
                        if(MapAccountContact.get(acc.Id)!=null)
                            objAccContactRel.ContactId = MapAccountContact.get(acc.Id);
                        else if(ContactId!=null)
                            objAccContactRel.ContactId = ContactId;
                        objAccContactRel.Multi_Structure_Account__c = true;
                        //objAccContactRel.Relationship_with_entity__c = stp.HexaBPM__SR__r.Relationship_with_entity__c;
                        objAccContactRel.IsActive = true;
                        objAccContactRel.Access_Level__c = 'Read/Write';
                        objAccContactRel.Roles = 'Super User;Company Services;Property Services';
                        lstACR.add(objAccContactRel);
                        setAccountIds.add(acc.Id);
                    }
                    if(lstACR.size()>0)
                        insert lstACR;
                }
                if(setAccountIds.size()>0)
                    CC_ProcessMultiStructureSR.processMultiStructSR(stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c,setAccountIds);
            }
        }catch(Exception e){
            insert LogDetails.CreateLog(null, 'CC_ProcessMultiStructureOnNameApproval', 'Line Number : '+e.getLineNumber()+'\nException is : '+e.getMessage()); 
            return e.getMessage();
        }
        return strResult;
    }
}