/*
  V.No    Date            Updated By   		Description
  V1.1    16-Dec-2020      Abbas       		 "Pay By Credit Card" Adcb Payment Integration on submit request page.
  V1.2    01-Aug-2021      Sabeeha Syed 	 Updating SubmittedDate on SR for ticket - DIFC2-17145
*
*/
//Catch Gateway response.
public  class CLS_ADCB_PayementConfirm {
    
    
    public  Map<String,String> Parameter{get;set;}
    string req_reference_number{get;set;}
    string transaction_id{get;set;}
    public boolean IsAdded{get;set;}
    public CLS_ADCB_PayementConfirm (){  
        
        
        Parameter= ApexPages.currentPage().getParameters();
        IsAdded=false;
        
        //System.debug('ZZZ RESP-->Parameter JSON-->'+JSON.serialize(Parameter));
        
        
    } 
    public void updateServiceRequest(string SId,Map<String,String> tempParameter) 
    {
        
        try
        {        
            Service_Request__c ObjSR=[select Bank_Name__c,Sys_Estimated_Share_Capital__c,Customer__c,Name,Id from Service_Request__c where id=:SId];
            List<Amendment__c> ListAmdUnit=[select Nominal_Value__c,id from Amendment__c where Amendment_Type__c='Unit' and ServiceRequest__c=:ObjSR.id];
            List<Amendment__c> ListAmd=[select id from Amendment__c where Amendment_Type__c='Tenant' and ServiceRequest__c=:ObjSR.id];
            List<Receipt__c> ListRec=new List<Receipt__c>();
            
            System.debug(LoggingLevel.Error,'ZZZ ObjSR===>'+ObjSR);
            System.debug(LoggingLevel.Error,'ZZZ ListAmdUnit===>'+ListAmdUnit);
            System.debug(LoggingLevel.Error,'ZZZ ListAmd===>'+ListAmd);
            System.debug(LoggingLevel.Error,'ZZZ tempParameter-->'+tempParameter);
            
            
            
            
            List<Receipt__c> ListRecAdded=[select id from Receipt__c where Service_Request__c=:SId];//to avoid duplicate inserts 
            
            System.debug('ZZZ ListRecAdded===>'+ListRecAdded);
            
            
            if(ListRecAdded.isEmpty() && IsAdded==false)
            {
                for(Amendment__c ObjAmd:ListAmdUnit)
                {
                    Receipt__c ObjRec=new Receipt__c();
                    ObjRec.Receivable_Type__c='Lease Security Deposit';
                    ObjRec.Amount__c=ObjAmd.Nominal_Value__c;
                    
                    ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
                    
                    if(tempParameter.get('decision')=='ACCEPT')         
                        ObjRec.Payment_Status__c='Success';
                    //else
                      //  ObjRec.Payment_Status__c='Failure';
                    
                    if(tempParameter.get('score_card_scheme')!=null)
                        ObjRec.Card_Type__c=tempParameter.get('score_card_scheme');
                    
                    //updR.PG_Error_Code__c=Parameter.get('decision');
                    ObjRec.PG_Error_Message__c=tempParameter.get('message');
                    
                    
                    ObjRec.Customer__c=ObjSR.Customer__c;
                    ObjRec.Receipt_Type__c='Card';
                    ObjRec.Amendment__c=ObjAmd.Id;
                    ObjRec.Tenant__c=ListAmd[0].id;
                    ObjRec.Service_Request__c=SId;
                    
                    if(tempParameter.get('transaction_id')!=null)
                    {
                        ObjRec.Payment_Gateway_Ref_No__c=tempParameter.get('transaction_id'); 
                        ObjRec.Bank_Ref_No__c=tempParameter.get('transaction_id');
                    }
                    
                    ObjRec.Transaction_Date__c=Datetime.now();
                    
                    ListRec.add(ObjRec);
                    
                }
                insert ListRec;
                //Update only if accepted 
                if(tempParameter.get('decision')=='ACCEPT')
                {
                    ObjSR.Bank_Name__c='Yes';
                    update ObjSR;
                }
                
                //if(!ListRec.isEmpty())
                // {
                
                /*
insert ListRec;
List<Step__c> ObjStep=[select Id from Step__c where SR__c=:SId and Step_Code__c='SECURITY_DEPOSIT_PAYMENT' and Closed_Date_Time__c=null limit 1];
if(!ObjStep.isEmpty())
Cls_DocumentReupload_StatusChange.UpdateStatus(ObjStep[0].id, 'a1M20000001iL9S');
*/
                // }
                IsAdded=true;
            }
            
        }catch(Exception e)
        {
            LogCreate(e.getMessage(),'Security Deposit Payment','');
            
            
        }
        
        
    }
    
    
    
    
    //V1.1
    public void updateTopupReceiptAndSR(string receiptId,String srId,Map<String,String> tempParameter) 
    {
        try{
            
            Receipt__c ObjRec = new Receipt__c( Id = receiptId );
            //ObjRec.Receivable_Type__c='';
            
            //ObjRec.Amount__c=ObjAmd.Nominal_Value__c;
            //ObjRec.Amount__c= tempParameter.get('req_amount');
            //ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
            
            if(tempParameter.get('decision')=='ACCEPT')         
                ObjRec.Payment_Status__c='Success';
            //else
              //  ObjRec.Payment_Status__c='Failure';
            
            if(tempParameter.get('score_card_scheme')!=null)
                ObjRec.Card_Type__c=tempParameter.get('score_card_scheme');
            
            //updR.PG_Error_Code__c=Parameter.get('decision');
            ObjRec.PG_Error_Message__c=tempParameter.get('message');
            
            
            //ObjRec.Customer__c = ObjSR.Customer__c;
            //ObjRec.Receipt_Type__c = 'Card';
            
            if(tempParameter.get('transaction_id')!=null)
            {
                ObjRec.Payment_Gateway_Ref_No__c = tempParameter.get('transaction_id'); 
                ObjRec.Bank_Ref_No__c            = tempParameter.get('transaction_id');
            }
            if(tempParameter.get('card_type_name') != null){
                ObjRec.Card_Type__c = tempParameter.get('card_type_name');
            }
            
            //ObjRec.Transaction_Date__c = Datetime.now();
            
            System.debug('ZZZ ->updateTopupReceipt_M->Receipt Rec2Update-->'+ObjRec);
            if(String.isNotBlank(ObjRec.Id)){
                update ObjRec;
            }
            
            
            if(String.isNotBlank(srId)){
                
                              HexaBPM__Service_Request__c updateSR = new HexaBPM__Service_Request__c(Id=srId);     
                String submittedSRStatusId = [Select Id FROM HexaBPM__SR_Status__c WHERE (Name='Submitted' OR HexaBPM__Code__c='SUBMITTED') LIMIT 1]?.Id;       
                updateSR.HexaBPM__Internal_SR_Status__c = submittedSRStatusId;         
                updateSR.HexaBPM__External_SR_Status__c = submittedSRStatusId;
                
                //Sabeeha -DIFC2-17145 - Added update of SR Submission Date
                updateSR.HexaBPM__Submitted_Date__c = system.today();
                updateSR.HexaBPM__Submitted_DateTime__c = system.now();
                //DIFC2-17145 update ends here
                
                System.debug('ZZZ ->updateTopupReceipt_M->SR SR2Update-->'+updateSR);
                if(String.isNotBlank(updateSR.Id) && String.isNotBlank(updateSR.HexaBPM__External_SR_Status__c)){
                    update updateSR;
                }
                
                
            }
            
            
        } catch(Exception e){
            LogCreate(e.getMessage(),'Topup Recharge Payment.','');
           
        }         
    }
    
    //V1.1
    public void updateReceiptForTopup(string receiptId,Map<String,String> tempParameter) 
    {
        try{
                        
            System.debug('ZZZ In updateReceiptForTopup_M !!!');
            Receipt__c ObjRec = new Receipt__c( Id = receiptId );
            //ObjRec.Receivable_Type__c='';
            
            //ObjRec.Amount__c=ObjAmd.Nominal_Value__c;
            //ObjRec.Amount__c= tempParameter.get('req_amount');
            //ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
            
            if(tempParameter.get('decision')=='ACCEPT')         
                ObjRec.Payment_Status__c='Success';
            else
                ObjRec.Payment_Status__c='Failure';
            
            if(tempParameter.get('score_card_scheme')!=null)
                ObjRec.Card_Type__c=tempParameter.get('score_card_scheme');
            
            //updR.PG_Error_Code__c=Parameter.get('decision');
            ObjRec.PG_Error_Message__c=tempParameter.get('message');
            
            
            //ObjRec.Customer__c = ObjSR.Customer__c;
            //ObjRec.Receipt_Type__c = 'Card';
            
            if(tempParameter.get('transaction_id')!=null)
            {
                ObjRec.Payment_Gateway_Ref_No__c = tempParameter.get('transaction_id'); 
                ObjRec.Bank_Ref_No__c            = tempParameter.get('transaction_id');
            }
            
            if(tempParameter.get('card_type_name') != null){
                ObjRec.Card_Type__c = tempParameter.get('card_type_name');
            }
            
            //ObjRec.Transaction_Date__c = Datetime.now();
            
            System.debug('ZZZ ->updateReceiptForTopup_M->Receipt Rec2Update-->'+ObjRec);
            if(String.isNotBlank(ObjRec.Id)){
                update ObjRec;
            }
            
            
        } catch(Exception e){
          
            LogCreate(e.getMessage(),'Security Deposit Payment','');
        }         
    }
    
    
    //V1.2
    public void ReceiptForPayperservice(String srId,Map<String,String> tempParameter) 
    {
        try{
        
          string accountId;
            boolean isFitOutContractor = false;
            
         
           for(User usr : [Select Contact.AccountId,contact.Contractor__c,Contact.Account.RecordType.DeveloperName from User where Id =:UserInfo.getUserId()]){
            //accountId = usr.Contact.AccountID;
            if(usr.contact.Contractor__c != null){
                isFitOutContractor = (String.isNotBlank(usr.Contact.Contractor__c) && !usr.Contact.Contractor__c.equals(usr.Contact.AccountId) ) || 
                                usr.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account');
            }
            accountId = isFitOutContractor && String.isNotBlank(usr.Contact.Contractor__c) ? usr.Contact.Contractor__c : usr.Contact.AccountId;
        }

           // User[] usr = [ Select Id,Name,Contact.AccountId From User Where Id = :UserInfo.getUserId() ];
            Service_Request__c ObjSR=[select Bank_Name__c,Sys_Estimated_Share_Capital__c,Customer__c,Name,Id from Service_Request__c where id=:srId];

           // String receiptCardRecTypeId = Schema.SobjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
            Receipt__c ObjRec = new Receipt__c();
              
            ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();

            if(tempParameter.get('decision')=='ACCEPT')         
                ObjRec.Payment_Status__c='Success';
            if(tempParameter.get('score_card_scheme')!=null)
             ObjRec.Card_Type__c=tempParameter.get('score_card_scheme');
            
            ObjRec.PG_Error_Message__c=tempParameter.get('message');
            ObjRec.Customer__c = accountId ;
            ObjRec.Receipt_Type__c = 'Card';
            ObjRec.Amount__c = 0;

            if(tempParameter.get('req_amount')!=null)
            ObjRec.Amount__c =Decimal.ValueOf( tempParameter.get('req_amount'));

            ObjRec.Transaction_Date__c = System.now();
           
            ObjRec.Service_Request__c = srId;


            if(tempParameter.get('transaction_id')!=null)
            {
                ObjRec.Payment_Gateway_Ref_No__c = tempParameter.get('transaction_id'); 
                ObjRec.Bank_Ref_No__c            = tempParameter.get('transaction_id');
            }
            if(tempParameter.get('card_type_name') != null){
                ObjRec.Card_Type__c = tempParameter.get('card_type_name');
            }
            
          //  System.debug('ZZZ ->updateTopupReceipt_M->Receipt Rec2Update-->'+ObjRec);
                insert ObjRec;    Attachment attachment = new Attachment();         attachment.Body = Blob.valueOf(JSON.serialize(tempParameter));       attachment.Name = String.valueOf('payementresponsce.txt');          attachment.ParentId = ObjRec.id;          insert attachment;
            
            

        } 
        catch(Exception e){
            LogCreate(e.getMessage(),'pay per service Recharge Payment.','');
            
            
        }   
    }
    
    public void LogCreate(string Description,string type,string ReceiptId)
    {
        Log__c ObjLog = new Log__c();
        ObjLog.Description__c=Description; 
          ObjLog.Type__c=type; 
          if(ReceiptId!='')
          ObjLog.Receipt__c=ReceiptId;
        insert ObjLog;
         
    }
    
    public PageReference updateReceipt() 
    {
        profile p = [Select Name from Profile where Id =: userinfo.getProfileid()];
        
        
        //System.debug(LoggingLevel.Error,'ZZZ ->updateReceipt_M->Parameter-->'+Parameter); 
        
        string userLic=p.Name;   //avoid duplicate calls
        Id sobjectRecId;
        String reqRefNumber;
        Boolean payStatus = false;
        if(Parameter.get('req_reference_number')!=null && userLic.contains('Plus'))//=='DIFC Customer Community Plus User Custom')
        {
            //V1.1
            //Get the transation receipt record for the payment done on the portal sidebar
            
            reqRefNumber = Parameter.get('req_reference_number');
            sobjectRecId = ( reqRefNumber.length() > 18 ? reqRefNumber.split('_')[0] : reqRefNumber );
            String sobjectType = sobjectRecId.getSObjectType().getDescribe().getName();
            System.debug('ZZZ ->updateReceipt_M->reqRefNumber-->'+reqRefNumber);
            System.debug('ZZZ ->updateReceipt_M->sobjectRecId-->'+sobjectRecId);
            System.debug('ZZZ ->updateReceipt_M->sobjectType-->'+sobjectType);
            
            
            if(Parameter.get('decision')=='ACCEPT') 
            {
                //V1.1
                payStatus = true;
                If( reqRefNumber.length() >= 18 && 
                    reqRefNumber.contains('TOPUP') &&
                    sobjectType == 'Receipt__c'){//For Top-up pay referenceid will be receiptId
                    //V1.1
                    //updateTopupReceiptAndSR(Parameter.get('req_reference_number'),Parameter);    
                    updateTopupReceiptAndSR(sobjectRecId,'',Parameter); 
                        
                }else{
                
                  if(reqRefNumber.length() > 18 &&  sobjectType == 'Service_Request__c')
                    {
                        ReceiptForPayperservice(reqRefNumber.split('_')[0],Parameter);

                    }
                    //V1.1 && !reqRefNumber.contains('TOPUP')
                    else if(reqRefNumber.length() > 18 && !reqRefNumber.contains('CPSR')){
                        updateReceiptForTopup(reqRefNumber.split('_')[1],Parameter);
                    }else if(reqRefNumber.length() > 18 && reqRefNumber.contains('CPSR')){
                        updateTopupReceiptAndSR(reqRefNumber.split('_')[1],reqRefNumber.split('_')[0],Parameter);
                    }else{
                        updateServiceRequest(Parameter.get('req_reference_number'),Parameter);        
                    }
                    
                }

            }
            else
            {
                //V1.1
                payStatus = false;
                If(sobjectType == 'Receipt__c'){
                   

                    LogCreate(JSON.serialize(Parameter),'Portal Top-up payment',sobjectRecId);

                }else{
                    //Lease Security Payment
                    LogCreate(JSON.serialize(Parameter),'Security Deposit Payment','');
                   
                }
  
            }
            
            
            
            
        }
        
        System.debug('ZZZ ->updateReceipt_M->payStatus-->'+payStatus);
        System.debug('ZZZ ->updateReceipt_M->reqRefNumber-->'+reqRefNumber);

        //V1.1
        PageReference pageRef;
        if( reqRefNumber != null && reqRefNumber.length() > 18 && !reqRefNumber.contains('TOPUP')  && !reqRefNumber.contains('CPSR')){
            pageRef = new PageReference(Label.Community_URL+'/SRSubmissionPage?id='+reqRefNumber.split('_')[0]+'&payStatus='+payStatus); 
        }else if(reqRefNumber != null && reqRefNumber.length() > 18 && reqRefNumber.contains('TOPUP') && !reqRefNumber.contains('CPSR')){
            //pageRef = new PageReference(Label.Community_URL+'/topupresult');      
            pageRef = new PageReference(Label.ADCB_Portal_URL+'/s/topupresult?s='+(payStatus ? 'y' :'n')+'&rid='+EncodingUtil.base64Encode(Blob.valueOf(reqRefNumber.split('_')[0])));
        }else if(reqRefNumber != null && reqRefNumber.length() > 18 && reqRefNumber.contains('CPSR') &&  !reqRefNumber.contains('TOPUP')){     
            pageRef = new PageReference(Label.ADCB_Portal_URL+'/s/topupresult?s='+(payStatus ? 'y' :'n')+'&rid='+EncodingUtil.base64Encode(Blob.valueOf(reqRefNumber.split('_')[1])));
        }else{
            pageRef = new PageReference(Label.Community_URL+'/'+Parameter.get('req_reference_number')); 
        }
        //Return to the Portal Home Page     
        //PageReference pageRef = new PageReference(Label.Community_URL+'/'+Parameter.get('req_reference_number'));
        //System.debug('ZZZ updateReceipt_M-->Label.Community_URL -->'+Label.Community_URL);
        System.debug('ZZZ updateReceipt_M-->pageRef -->'+pageRef);
        return pageRef ; 
        
        
        
    }
    
    /*
      public PageReference updateReceipt() 
    {
        if(Parameter.get('req_reference_number')!=null)
        {
                  //Get the transation receipt record for the payment done on the portal sidebar
           List<Receipt__c> ListRec =[select id,name,Amount__c,Card_Type__c,Payment_Status__c,Payment_Gateway_Ref_No__c,PG_Error_Code__c,PG_Error_Message__c 
                from Receipt__c where name=:Parameter.get('req_reference_number')];
        
        if(!ListRec.IsEmpty())
        {
          Receipt__c updR =ListRec[0];
      
            if(Parameter.get('transaction_id')!=null)
            {
                updR.Payment_Gateway_Ref_No__c=Parameter.get('transaction_id'); 
                 updR.Bank_Ref_No__c=Parameter.get('transaction_id');
            }
           
            
            if(Parameter.get('decision')=='ACCEPT')         
            updR.Payment_Status__c='Success';
            else
            updR.Payment_Status__c='Failure';
            
        
           updR.Transaction_Date__c=Datetime.now();
        
           if(Parameter.get('score_card_scheme')!=null)
            updR.Card_Type__c=Parameter.get('score_card_scheme');
            
            //updR.PG_Error_Code__c=Parameter.get('decision');
            updR.PG_Error_Message__c=Parameter.get('message');
            
            update updR;
            //Return to the Portal Home Page     
            //PageReference pageRef = new PageReference(Label.Community_URL+'/'+updR.id);
           // return pageRef; 
           }
        }
        
    return null;
    
            
       
    }
    
    */
    
    
    
}