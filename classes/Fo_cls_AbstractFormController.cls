/******************************************************************************************
 *  Author   	: Claude Manahan
 *  Company  	: NSI DMCC 
 *  Date     	: 2016-07-08      
 *  Description : This abstract class can be used for controllers of custom SR forms
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By        Description
 ----------------------------------------------------------------------------------------            
 V1.0   07-08-2016      Claude            Created
 V1.1	10-08-2016		Claude			  Re-compiled after Devmain refresh
 *******************************************************************************************/
public without sharing abstract class Fo_cls_AbstractFormController {
	
	/**
	 * Default timezone for UAE
	 */
	public static final String GST_TIMEZONE = 'GST';
	
	/**
	 * Default error message
	 */
	protected String defaultTypeErrorMsg = 'The selected type of request is either inactive or does not exist. Please contact your administrator to create and/or activate this type, then try again.';
	
	/**
	 * Set of types for Fit-out/Event forms
	 */
	public enum FormTypes 							{ FITOUT,EVENT }
	
	/**
     * This will hold the contractor details
     */
    public Service_Request__c srObj 				{ get; set; }
    
    /**
     * This will store the Id of the new request
     */
    public String srId 								{ get; private set; }
    
    /**
     * Flag to check if an error occurred
     */
    public String errMsg 							{ get; private set; }
    
    /**
     * Checks if the SR has a record Type
     */
    public Boolean hasRecordType					{ get; private set; }
    
    /**
     * Stores the page title
     */
    public transient String pageTitle				{ get; private set; }
    
    /**
     * Map of record types based
     * by name
     */
    protected Map<String,List<RecordType>> recordTypeMap;
    
    /**
	 * Stores the selected type
	 */
	protected FormTypes formType;
    
    /**
     * Determines if the current user is an admin
     */
    protected Boolean isSystemAdministrator;
    
    /**
     * Determins if the current user is a portal user
     */
    protected Boolean isPortalUser;
    
    /**
     * The page reference whenever a 
     * transaction is cancelled 
     */
    protected PageReference retUrl;
    
    /**
     * Stores the class name that extends 
     * this class
     */
    private String currentClass;
    
    /**
     * This will store the Record type
     * of the request
     */
    private String srRecordTypeName;
    
	/**
	 * Executes preliminary actions
	 */
	protected abstract void init();
	
	/**
	 * Initializes the class members 
	 */
	protected abstract void initialize();
	
	/**
	 * Runs all required actions when the 
	 * class is loaded
	 */
	protected abstract void executeOnLoad();
	
	/**
     * Gets the URL Parameter value
     * @param       param           the name of the parameter
     * @return      urlParam        the url Parameter value
     */
    protected Object getUrlParameter(String param){

        Object urlParam = ApexPages.CurrentPage().GetParameters().Get(param);
        return urlParam;
    }
    
    /**
     * Resets the error messages
     */
    protected void clearMessages(){
    	errMsg = '';
    }
	
	/**
     * Sets the error message
     */
	@testVisible protected void setErrorMessage(String errMsg){
		this.errMsg = errMsg;
	}
	
	/**
     * Checks if an error has been detected
     * @return      hasError    tells whether an error has occurred or not
     */
    protected Boolean hasError(){
        return String.isNotBlank(errMsg);
    }
    
    /**
     * Sets the record type of the Service Request
     * @params		srRecordTypeName		the name of the record type
     */
    protected void setSrRecordType(String srRecordTypeName){
    	
    	/* Assign the record type name for the search */
    	this.srRecordTypeName = srRecordTypeName;
    	
    	/* Query the Record Type */
    	List<RecordType> srRecordType = [SELECT Id FROM RecordType WHERE (Name = :srRecordTypeName OR DeveloperName = :srRecordTypeName) AND SobjectType = 'Service_Request__c' LIMIT 1];
    
    	if(!srRecordType.isEmpty()){
    		
    		/* Get the record type ID via ID */
        	srObj.recordTypeId = srRecordType[0].Id;
        	hasRecordType = true;	
        } else {
        	setErrorMessage(defaultTypeErrorMsg);
        	displayErrorMessage('setSrRecordType');
        }
    	
    }
    
    /**
     * Sets the error message
     */
    @testVisible protected void displayErrorMessage(String methodName){
        
        String internalErrorMsg = 'The error occurred at '+currentClass+'.'+methodName+' >>' + errMsg;
        
        System.debug(internalErrorMsg);
        
        /* NOTE: 
         * Default Message => 
         * 'An error occurred while processing your request. 
         *	Our technical team has been notified to resolve 
         * this issue. Please try again after a few minutes.' 
         */
        Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL,errMsg));
    }
    
    /**
     * Parent Class Initialization
     */
    {
    	/*
    	 * Initializes any variable or collection
    	 * in the class
    	 */
    	initializeMembers();
    	
    	/*
    	 * Set the current class name
    	 * for logging
    	 */
    	setCurrentClass();
    	
    	/*
    	 * Debug the class name
    	 */
    	displayCurrentClass();
    	
    	/*
    	 * Get the Service Request ID
    	 */
    	getSrId();
    	
    	/* 
    	 * Check User's Profile 
    	 */
    	checkProfile();
    }
    
    /**
     * Initializes the class members for usage
     */
    private void initializeMembers(){
    	
    	// Create new instance of Service Request
        initializeSr();
        
        // Initialize error message
        errMsg = '';
        
        // Initialize SR Record Type
        srRecordTypeName = '';
        
        // Set the record type check to default FALSE
        hasRecordType = false;
        
        // Initialize the record type map
        recordTypeMap = new Map<String,List<RecordType>>();
    }
    
    /**
     * Initializes the main SR
     */
    protected void initializeSr(){
    	srObj = new Service_Request__c();
    }
    
    /**
     * Sets the form type
     * @params		isEvent		Checks if the form is for events or fit-out 
     */
    protected void setFormType(FormTypes formType){
    	this.formType = formType;
    }
    
    /**
     * Sets the page title of the form
     * @params		pageTitle		the title of the page
     */
    protected void setPageTitle(String pageTitle){
    	this.pageTitle = pageTitle;
    }
    
    /**
     * Sets the return URL when a request is
     * cancelled
     * @params		retUrl		The return URL page reference
     */
    protected void setRetUrl(PageReference retUrl){
    	this.retUrl = retUrl;
    }
    
    /**
     * Returns a datetime field in the specific 
     * format, with timezone set to user default
     * and timezone string set to 'GST'
     * @params		dateTimeValue		The datetime value to be formatted
     * @params		format				The display format
     * @return		formattedDateTime	The datetime value displayed in the specified format
     */
    @testVisible protected String getFormattedDateTime(Datetime dateTimeValue, String format){
    	TimeZone tz = UserInfo.getTimeZone();
    	return getFormattedDateTime(tz,dateTimeValue,format,GST_TIMEZONE);
    }
    
    /**
     * Returns a datetime field in the specific 
     * format and timezone
     * @params		tzInst				The timezone instance to be used for comparison
     * @params		dateTimeValue		The datetime value to be formatted
     * @params		format				The display format
     * @params		timeZone			The timezone string to be used during formatting
     * @return		formattedDateTime	The datetime value displayed in the specified format
     */
    @testVisible protected String getFormattedDateTime(TimeZone tzInstz, Datetime dateTimeValue, String format, String timeZone){
    	return dateTimeValue.AddSeconds(tzInstz.getOffset(dateTimeValue)/1000).format(format,timeZone);
    }
    
    /**
     * Cancels a request
     */ 
    public PageReference cancelRequest(){
        return retUrl;
    }
    
    /**
     * Displays the current class instantiated
     */
    private void displayCurrentClass(){
    	System.debug('Current Class: ' + currentClass);
    }
    
    /**
     * Gets the srId passed to the form
     */
    private void getSrId(){
    	srId = String.isNotBlank((String)getUrlParameter('srId')) ? (String)getUrlParameter('srId') : '';
    }
    
    /**
     * Gets the current class 
     */ 
    private void setCurrentClass(){
    	currentClass = String.valueOf(this).substring(0,String.valueOf(this).indexOf(':'));
    }
    
    /**
     * Checks the current user's profile
     */
    private void checkProfile(){
    	
    	String profileName = [SELECT Name FROM Profile WHERE Id = :UserInfo.getProfileId()].Name;
    	
    	isSystemAdministrator = profileName.equals('System Administrator');
    	isPortalUser = profileName.contains('Community'); // Claude - 06-09-2016 => Changed from 'Portal' to 'Community'	
    }
    
    /**
     * Returns the record type id of the 
     * selected name/developer name
     * @params		recordTypeNames		Set of record type names
     */
    @testVisible protected void setRecordTypeMap(Set<String> recordTypeNames){
    	
    	List<RecordType> recordTypesList = 
    						[SELECT Id, 
    								Name, 
    								DeveloperName, 
    								sObjectType 
    								FROM RecordType WHERE (
    								Name in :recordTypeNames OR DeveloperName in :recordTypeNames)]; 
    								
		/* Iterate through the list, then populate the map by sObject */
    	for(RecordType t : recordTypesList){
    		
    		List<RecordType> recordTypeList = 
    			recordTypeMap.containsKey(t.sObjectType) ? 
    				recordTypeMap.get(t.sObjectType) : new List<RecordType>();
    		
    		
    		recordTypeList.add(t);
    		
    		recordTypeMap.put(t.sObjectType,recordTypeList);
    	}
    }
    
    /**
     * Gets a record type Id based on the
     * sObject and name
     * @params 		name				The name of the record type
     * @params		sObjectType			The sObject type	
     * @return		recordTypeId		The recordType Id
     */
    @testVisible protected Id getRecordTypeId(String name, String sObjectType){
    	return getRecordTypeId(name,sObjectType,false);
    }
    
    /**
     * Gets a record type Id based on the
     * sObject and name
     * @params 		name				The name of the record type
     * @params		sObjectType			The sObject type	
     * @params		isDeveloperName		Checks if the filter should be based on the 
     *									developer name
     * @return		recordTypeId		The recordType Id
     */
    protected Id getRecordTypeId(String name, String sObjectType, Boolean isDeveloperName){
    	
    	Id recordTypeId = null;
    	
    	if(recordTypeMap.containsKey(sObjectType)){
    		
    		List<RecordType> recordTypeList	= recordTypeMap.get(sObjectType);
    		
    		System.debug('The list: ' + recordTypeList);
    		
    		for(RecordType r : recordTypeList){
    			
    			System.debug('The record type >> ' + r);
    			Boolean isNameMatch = isDeveloperName ? r.DeveloperName.equals(name) : r.Name.equals(name);
    			
    			if(r.SobjectType.equals(sObjectType) && isNameMatch){
    				recordTypeId = r.Id;
    				break;
    			}
    			
    		}
    	}
    	
    	return recordTypeId;
    }
    
    /**
     * Sets the error URL Reference
     * @params 		methodName			The name of the method where the error occured
     * @params		sp					The savepoint
     */
    @testVisible protected void setErrorRetUrl(String methodName, Savepoint sp){
    	
        displayErrorMessage(methodName); // Display the error message
        setRetUrl(null);
        Database.rollBack(sp);			 // Rollback any changes
        srObj = srObj.clone(false);		 // Reset the record
    }
    
    /**
     * Sets the error URL Reference
     * @params 		successReference	The success URL
     */
    @testVisible protected void setSuccessRetUrl(PageReference successReference){
    	
    	setRetUrl(successReference);
		retUrl.setRedirect(true);
    }
    
    /**
     * Returns the Return URL
     * @return		retUrl		The return URL
     */
    @testVisible protected PageReference getRetUrl(){
    	return retUrl;
    }
    
}