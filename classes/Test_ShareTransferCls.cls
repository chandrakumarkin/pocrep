/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=false)
private class Test_ShareTransferCls {
    static testMethod void myUnitTest() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        insert objAccount;
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Test Account';
        insert objAccount2;
    
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Passport_No__c = '13123423';
        objContact.Email = 'test@dmcc.com';
        insert objContact;
        
        Contact objContact2 = new Contact();
        objContact2.LastName = 'Test Contact2';
        objContact2.FirstName = 'Test Contact2';
        objContact.Passport_No__c = '13123433';
        objContact2.AccountId = objAccount.Id;
        objContact2.Email = 'test@dmcc.com';
        insert objContact2;
        
        Account_Share_Detail__c accshrlist = new Account_Share_Detail__c();
        accshrlist.Account__c =  objAccount.Id;
        accshrlist.Name = 'Class10';
        accshrlist.Status__c = 'Draft';
        accshrlist.No_of_shares_per_class__c = 50;
        accshrlist.Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_No_of_Shares_per_Class__c=50;
        insert accshrlist; 

        
        Share_Transfer_History__c shareTransfer = new Share_Transfer_History__c();
        shareTransfer.Account_Share_Detail__c = accshrlist.ID;
        shareTransfer.No_of_Shares__c = 5;
        
        Shareholder_Detail__c shareHolderDetail = new Shareholder_Detail__c();
        shareHolderDetail.No_of_Shares__c = 5;
        shareHolderDetail.Sys_Proposed_No_of_Shares__c = 5;
        shareHolderDetail.Account_Share__c = accshrlist.ID;
        
        
        
        
        Relationship__c objShdRelInd = new Relationship__c();
        objShdRelInd.Relationship_Type__c = 'Is Shareholder Of';
        objShdRelInd.Active__c = true;
        objShdRelInd.Start_Date__c = system.today().addMonths(-10);
        objShdRelInd.End_Date__c = system.today().addMonths(10);
        objShdRelInd.Subject_Account__c = objAccount.Id;
        objShdRelInd.Object_Contact__c = objContact.Id;
        insert objShdRelInd;
        
        Relationship__c objShdRelBC = new Relationship__c();
        objShdRelBC.Relationship_Type__c = 'Is Shareholder Of';
        objShdRelBC.Active__c = true;
        objShdRelBC.Start_Date__c = system.today().addMonths(-10);
        objShdRelBC.End_Date__c = system.today().addMonths(10);
        objShdRelBC.Subject_Account__c = objAccount.Id;
        objShdRelBC.Object_Account__c = objAccount2.Id;
        insert objShdRelBC;
        
        Shareholder_Detail__c shareDet =  new Shareholder_Detail__c(Relationship__c=objShdRelInd.Id,Account__c=objAccount.Id,Account_Share__c=accshrlist.id,Sys_Proposed_No_of_Shares__c=2,No_of_Shares__c=2,Status__c='Active');
        insert shareDet;
        
        Shareholder_Detail__c shareDetBC =  new Shareholder_Detail__c(Relationship__c=objShdRelBC.Id,Account__c=objAccount.Id,Account_Share__c=accshrlist.id,Sys_Proposed_No_of_Shares__c=2,No_of_Shares__c=2,Status__c='Active');
        insert shareDetBC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        string RecTypeId = '';
        for(RecordType objRec:[select id from RecordType where DeveloperName='Sale_or_Transfer_of_Shares_or_Membership_Interest' and sObjectType='Service_Request__c']){
            RecTypeId = objRec.Id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.legal_structures__c = 'RLLP';
        objSR.Foreign_Registration_Name__c = 'Test FR Name';
        objSR.Foreign_Registration_Date__c = system.today();
        objSR.Foreign_Registration_Place__c = 'United Arab Emirates';
        objSR.Foreign_Registered_Building__c = 'Building1';
        objSR.Foreign_Registered_Street__c = 'Street1';
        objSR.Foreign_Registered_PO_Box__c = '13124';
        if(RecTypeId!=null && RecTypeId!='')
            objSR.RecordTypeId = RecTypeId;
        objSR.Foreign_Registered_Post__c = '124214';
        objSR.Foreign_registered_office_no__c = '124124';
        objSR.Foreign_Registered_Level__c = '12';
        objSR.Foreign_Registered_Country__c = 'United Arab Emirates';
        objSR.SR_Template__c = objTemplate.Id;
        objSR.Entity_Name__c='Test';
        objSR.Proposed_Trading_Name_1__c='Test';
        objSR.Incorporated_Organization_Name__c='Test';
        objSR.Nature_of_business__c='Test';
        objSR.Nature_of_Business_Arabic__c='Test';
        objSR.Name_identical_to_another_company__c=true;
        objSR.Business_Name__c='Test';
        objSR.Identical_Business_Domicile__c='Test';
        objSR.Pro_Entity_Name_Arabic__c='Test';
        objSR.Pro_Trade_Name_1_in_Arab__c ='Test';
        objSR.Legal_Structures__c='Test';
        objSR.Registration_Type__c='Test';
        objSR.AOA_Format__c='Test';
        objSR.Standard_Charter_Format__c='Test';
        objSR.Current_Registered_Name__c='Test';
        objSR.Domicile_list__c='Test';
        objSR.Current_Registered_Office_No__c='Test';
        objSR.Current_Registered_Building__c='Test';
        objSR.Current_Registered_Level__c='Test';
        objSR.Current_Registered_Street__c='Test';
        objSR.Current_Registered_PO_Box__c='Test';
        objSR.Current_Address_Country__c='Test';
        objSR.Current_Registered_Postcode__c='Test';
        objSR.Principal_Business_Activity__c='Test';
        objSR.Reasons_to_transfer_Foreign_Entity__c='Test';
        objSR.DFSA_Approval__c=true;
        objSR.DFSA_In_principle_Approval_Date__c=system.today();
        insert objSR;
        
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Sale_or_Transfer_of_Shares_or_Membership_Interest';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Sale_or_Transfer_of_Shares_or_Membership_Interest';
        insert objPageFlow;
        
        Page__c objPage = new Page__c();
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        objPage.VF_Page_API_Name__c = 'Change_SH_Members';
        objPage.Is_Custom_Component__c = true;
        objPage.Render_By_Default__c = true;
        insert objPage;
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Navigation_Directions__c = 'Forward';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('PageId',objPage.Id);
        Apexpages.currentPage().getParameters().put('Id',objSR.Id);
        
        ShareTransferCls ST = new ShareTransferCls();
        ShareTransferCls.Total_Transfer_History wrap = new ShareTransferCls.Total_Transfer_History();
        ST.FetchExistingRelations();
        test.startTest(); 
        ST.AddIndAmendment();
        ST.SaveAmnd();
        ST.PrepareClasses();
        ST.createRow();
        ST.AddBCAmendment();
        ST.SaveAmnd();
        ST.Calculate_Shares();
       
        ST.getDyncPgMainPB();
        ST.ReCalculateTotalNominalValue();
        ST.AmndRowIndex = 0;
        ST.strAmndType = 'Individual';
        ST.EditAmnd();
        ST.strAmndType = 'Body Corporate';
        ST.EditAmnd();
        ShareTransferCls.PrepareExceptionMsg('System.DmlException: Insert failed. First exception on row 0 with id a0N11000003OQ4eEAG; first error: INVALID_FIELD_FOR_INSERT_UPDATE, cannot specify Id in an insert call: [Id]');
        
        Amendment__c amndRemBC = new Amendment__c(Status__c='Active',Sys_Create__c=true,Shareholder_Company__c=objAccount2.Id,Company_Name__c='Rem Comp',ServiceRequest__c=objSR.id,Relationship_Type__c='Shareholder',Amendment_Type__c='Individual');
        insert amndRemBC;
        
        Amendment__c amndBodyCorporate = new Amendment__c(Status__c='Active',Sys_Create__c=true,
                        Shareholder_Company__c=objAccount2.Id,Company_Name__c='Rem Comp',ServiceRequest__c=objSR.id,Relationship_Type__c='Shareholder',
                        Amendment_Type__c='Body Corporate');
        
        insert amndBodyCorporate;
        
        Amendment__c amndUBO = new Amendment__c(Status__c='Active',Sys_Create__c=true,
                        Shareholder_Company__c=objAccount2.Id,Company_Name__c='Rem Comp',ServiceRequest__c=objSR.id,
                        Amendment_Type__c='Body Corporate' ,related__c = amndBodyCorporate.ID, Relationship_Type__c = 'UBO');
                        
        insert amndUBO;
        
        shareTransfer.Transfer_From__c = amndRemBC.ID;
        shareTransfer.Transfer_To__c = amndBodyCorporate.ID;
        insert shareTransfer;
        
        
        amndRemBC.Customer__c = objAccount.ID;
        amndRemBC.Class__c = accshrlist.ID;
        update amndRemBC;
        
        shareHolderDetail.Amendment__c = amndRemBC.ID;
        shareHolderDetail.account__c = objAccount.ID;
        insert shareHolderDetail;
        
        ST.CurrentAmnd = amndRemBC;
        ST.strAmndId = amndRemBC.Id;
        ST.createRow();
        //ST.Add_Amendment_UBO();
        ST.Save_UBO_Amendment_Draft();
        
        ST.Save_UBO_Amendment(ST.strAmndId);
        ST.ValidateAmendmentsUBO( ST.CurrentAmnd);
        ST.DelAmendId_Index = 0;
        ST.strUBOAmndID = amndUBO.ID;
        ST.updateSelectedUbo();
        ST.lstAmendUBODetails.add(amndUBO);     
        system.debug('$$$$$$$$$' +ST.lstAmendUBODetails.size());
        ST.Delete_UBOamendment();
        
        
        ST.DelBodyCorporate_Index = 0;
        ST.Individual_Index = 0;
        ST.lstAmendBCDetails.add(amndBodyCorporate);
        ST.DelAmnd_Individual();
        ST.DelAmnd();
        ST.CurrentAmnd.Amendment_Type__c = 'Body Corporate';
        ST.CancelAmnd();
        ST.cancelUbo();
        ST.strNavigatePageId = objPage.Id;
        ST.goTopage();
        ST.strActionId = objSD.Id;
        ST.DynamicButtonAction();
        
        test.stopTest();
    }
   
    static testMethod void myUnitTest_1() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        insert objAccount;
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Test Account';
        insert objAccount2;
    
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Passport_No__c = '13123423';
        objContact.Email = 'test@dmcc.com';
        insert objContact;
        
        Contact objContact2 = new Contact();
        objContact2.LastName = 'Test Contact2';
        objContact2.FirstName = 'Test Contact2';
        objContact.Passport_No__c = '13123433';
        objContact2.AccountId = objAccount.Id;
        objContact2.Email = 'test@dmcc.com';
        insert objContact2;
        
        Account_Share_Detail__c accshrlist = new Account_Share_Detail__c();
        accshrlist.Account__c =  objAccount.Id;
        accshrlist.Name = 'Class10';
        accshrlist.Status__c = 'Draft';
        accshrlist.No_of_shares_per_class__c = 50;
        accshrlist.Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_No_of_Shares_per_Class__c=50;
        insert accshrlist; 

        
        Share_Transfer_History__c shareTransfer = new Share_Transfer_History__c();
        shareTransfer.Account_Share_Detail__c = accshrlist.ID;
        shareTransfer.No_of_Shares__c = 5;
        
        Shareholder_Detail__c shareHolderDetail = new Shareholder_Detail__c();
        shareHolderDetail.No_of_Shares__c = 5;
        shareHolderDetail.Sys_Proposed_No_of_Shares__c = 5;
        shareHolderDetail.Account_Share__c = accshrlist.ID;
        
        
        
        
        Relationship__c objShdRelInd = new Relationship__c();
        objShdRelInd.Relationship_Type__c = 'Is Shareholder Of';
        objShdRelInd.Active__c = true;
        objShdRelInd.Start_Date__c = system.today().addMonths(-10);
        objShdRelInd.End_Date__c = system.today().addMonths(10);
        objShdRelInd.Subject_Account__c = objAccount.Id;
        objShdRelInd.Object_Contact__c = objContact.Id;
        insert objShdRelInd;
        
        Relationship__c objShdRelBC = new Relationship__c();
        objShdRelBC.Relationship_Type__c = 'Is Shareholder Of';
        objShdRelBC.Active__c = true;
        objShdRelBC.Start_Date__c = system.today().addMonths(-10);
        objShdRelBC.End_Date__c = system.today().addMonths(10);
        objShdRelBC.Subject_Account__c = objAccount.Id;
        objShdRelBC.Object_Account__c = objAccount2.Id;
        insert objShdRelBC;
        
        Shareholder_Detail__c shareDet =  new Shareholder_Detail__c(Relationship__c=objShdRelInd.Id,Account__c=objAccount.Id,Account_Share__c=accshrlist.id,Sys_Proposed_No_of_Shares__c=2,No_of_Shares__c=2,Status__c='Active');
        insert shareDet;
        
        Shareholder_Detail__c shareDetBC =  new Shareholder_Detail__c(Relationship__c=objShdRelBC.Id,Account__c=objAccount.Id,Account_Share__c=accshrlist.id,Sys_Proposed_No_of_Shares__c=2,No_of_Shares__c=2,Status__c='Active');
        insert shareDetBC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        string RecTypeId = '';
        for(RecordType objRec:[select id from RecordType where DeveloperName='Sale_or_Transfer_of_Shares_or_Membership_Interest' and sObjectType='Service_Request__c']){
            RecTypeId = objRec.Id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.legal_structures__c = 'RLLP';
        objSR.Foreign_Registration_Name__c = 'Test FR Name';
        objSR.Foreign_Registration_Date__c = system.today();
        objSR.Foreign_Registration_Place__c = 'United Arab Emirates';
        objSR.Foreign_Registered_Building__c = 'Building1';
        objSR.Foreign_Registered_Street__c = 'Street1';
        objSR.Foreign_Registered_PO_Box__c = '13124';
        if(RecTypeId!=null && RecTypeId!='')
            objSR.RecordTypeId = RecTypeId;
        objSR.Foreign_Registered_Post__c = '124214';
        objSR.Foreign_registered_office_no__c = '124124';
        objSR.Foreign_Registered_Level__c = '12';
        objSR.Foreign_Registered_Country__c = 'United Arab Emirates';
        objSR.SR_Template__c = objTemplate.Id;
        objSR.Entity_Name__c='Test';
        objSR.Proposed_Trading_Name_1__c='Test';
        objSR.Incorporated_Organization_Name__c='Test';
        objSR.Nature_of_business__c='Test';
        objSR.Nature_of_Business_Arabic__c='Test';
        objSR.Name_identical_to_another_company__c=true;
        objSR.Business_Name__c='Test';
        objSR.Identical_Business_Domicile__c='Test';
        objSR.Pro_Entity_Name_Arabic__c='Test';
        objSR.Pro_Trade_Name_1_in_Arab__c ='Test';
        objSR.Legal_Structures__c='Test';
        objSR.Registration_Type__c='Test';
        objSR.AOA_Format__c='Test';
        objSR.Standard_Charter_Format__c='Test';
        objSR.Current_Registered_Name__c='Test';
        objSR.Domicile_list__c='Test';
        objSR.Current_Registered_Office_No__c='Test';
        objSR.Current_Registered_Building__c='Test';
        objSR.Current_Registered_Level__c='Test';
        objSR.Current_Registered_Street__c='Test';
        objSR.Current_Registered_PO_Box__c='Test';
        objSR.Current_Address_Country__c='Test';
        objSR.Current_Registered_Postcode__c='Test';
        objSR.Principal_Business_Activity__c='Test';
        objSR.Reasons_to_transfer_Foreign_Entity__c='Test';
        objSR.DFSA_Approval__c=true;
        objSR.DFSA_In_principle_Approval_Date__c=system.today();
        insert objSR;
        
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Sale_or_Transfer_of_Shares_or_Membership_Interest';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Sale_or_Transfer_of_Shares_or_Membership_Interest';
        insert objPageFlow;
        
        Page__c objPage = new Page__c();
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        objPage.VF_Page_API_Name__c = 'Change_SH_Members';
        objPage.Is_Custom_Component__c = true;
        objPage.Render_By_Default__c = true;
        insert objPage;
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Navigation_Directions__c = 'Forward';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('PageId',objPage.Id);
        Apexpages.currentPage().getParameters().put('Id',objSR.Id);
        
        ShareTransferCls ST = new ShareTransferCls();
        ShareTransferCls.Total_Transfer_History wrap = new ShareTransferCls.Total_Transfer_History();
        ST.FetchExistingRelations();
        
        ST.AddIndAmendment();
        ST.SaveAmnd();
        ST.PrepareClasses();
        ST.createRow();
        ST.AddBCAmendment();
        ST.SaveAmnd();
        ST.Calculate_Shares();
        test.startTest();
        ST.getDyncPgMainPB();
        ST.ReCalculateTotalNominalValue();
        ST.AmndRowIndex = 0;
        ST.strAmndType = 'Individual';
        ST.EditAmnd();
        ST.strAmndType = 'Body Corporate';
        ST.EditAmnd();
        ShareTransferCls.PrepareExceptionMsg('System.DmlException: Insert failed. First exception on row 0 with id a0N11000003OQ4eEAG; first error: INVALID_FIELD_FOR_INSERT_UPDATE, cannot specify Id in an insert call: [Id]');
        
        Amendment__c amndRemBC = new Amendment__c(Status__c='Active',Sys_Create__c=true,Shareholder_Company__c=objAccount2.Id,Company_Name__c='Rem Comp',ServiceRequest__c=objSR.id,Relationship_Type__c='Shareholder',Amendment_Type__c='Individual');
        insert amndRemBC;
        
        Amendment__c amndBodyCorporate = new Amendment__c(Status__c='Active',Sys_Create__c=true,
                        Shareholder_Company__c=objAccount2.Id,Company_Name__c='Rem Comp',ServiceRequest__c=objSR.id,Relationship_Type__c='Shareholder',
                        Amendment_Type__c='Body Corporate');
        
        insert amndBodyCorporate;
        
        Amendment__c amndUBO = new Amendment__c(Status__c='Active',Sys_Create__c=true,
                        Shareholder_Company__c=objAccount2.Id,Company_Name__c='Rem Comp',ServiceRequest__c=objSR.id,
                        Amendment_Type__c='Body Corporate' ,related__c = amndBodyCorporate.ID, Relationship_Type__c = 'UBO');
                        
        insert amndUBO;
        
        shareTransfer.Transfer_From__c = amndRemBC.ID;
        shareTransfer.Transfer_To__c = amndBodyCorporate.ID;
        insert shareTransfer;
        
        //test.startTest();
        amndRemBC.Customer__c = objAccount.ID;
        amndRemBC.Class__c = accshrlist.ID;
        update amndRemBC;
        
        shareHolderDetail.Amendment__c = amndRemBC.ID;
        shareHolderDetail.account__c = objAccount.ID;
        insert shareHolderDetail;
              
        ST.CurrentAmnd = amndRemBC;
        ST.strAmndId = amndRemBC.Id;
        ST.createRow();
        //ST.Add_Amendment_UBO();
        ST.Save_UBO_Amendment_Draft();
        
        ST.Save_UBO_Amendment(ST.strAmndId);
        ST.ValidateAmendmentsUBO( ST.CurrentAmnd);
        ST.DelAmendId_Index = 0;
        ST.strUBOAmndID = amndUBO.ID;
        ST.updateSelectedUbo();
        ST.lstAmendUBODetails.add(amndUBO);     
        system.debug('$$$$$$$$$' +ST.lstAmendUBODetails.size());
        ST.Delete_UBOamendment();
        
        
        
        ST.AccShareRowIndex = 0;
        ST.delRow();
        ST.AddNewTransferHistoryLine();
        ST.TransferRowIndex = 0;
        ST.ManageTransferRow = 0;
        ST.SaveNewTransferHistoryLine();
        ST.CancelNewTransferHistoryLine();
        ST.AddNewTransferHistoryLine();
        ST.deleteTranferShare();
        ST.AddNewTransferHistoryLine();
        test.stopTest();
    }
   


    
    
     static testMethod void myUnitTest1() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        insert objAccount;
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Test Account';
        insert objAccount2;
    
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Passport_No__c = '13123423';
        objContact.Email = 'test@dmcc.com';
        insert objContact;
        
        Contact objContact2 = new Contact();
        objContact2.LastName = 'Test Contact2';
        objContact2.FirstName = 'Test Contact2';
        objContact.Passport_No__c = '13123433';
        objContact2.AccountId = objAccount.Id;
        objContact2.Email = 'test@dmcc.com';
        insert objContact2;
        
        Account_Share_Detail__c accshrlist = new Account_Share_Detail__c();
        accshrlist.Account__c =  objAccount.Id;
        accshrlist.Name = 'Class10';
        accshrlist.Status__c = 'Draft';
        accshrlist.No_of_shares_per_class__c = 50;
        accshrlist.Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_No_of_Shares_per_Class__c=50;
        insert accshrlist; 

        
        Share_Transfer_History__c shareTransfer = new Share_Transfer_History__c();
        shareTransfer.Account_Share_Detail__c = accshrlist.ID;
        shareTransfer.No_of_Shares__c = 5;
        
        Shareholder_Detail__c shareHolderDetail = new Shareholder_Detail__c();
        shareHolderDetail.No_of_Shares__c = 5;
        shareHolderDetail.Sys_Proposed_No_of_Shares__c = 5;
        shareHolderDetail.Account_Share__c = accshrlist.ID;
        
        
        
        
        Relationship__c objShdRelInd = new Relationship__c();
        objShdRelInd.Relationship_Type__c = 'Is Shareholder Of';
        objShdRelInd.Active__c = true;
        objShdRelInd.Start_Date__c = system.today().addMonths(-10);
        objShdRelInd.End_Date__c = system.today().addMonths(10);
        objShdRelInd.Subject_Account__c = objAccount.Id;
        objShdRelInd.Object_Contact__c = objContact.Id;
        insert objShdRelInd;
        
        Relationship__c objShdRelBC = new Relationship__c();
        objShdRelBC.Relationship_Type__c = 'Is Shareholder Of';
        objShdRelBC.Active__c = true;
        objShdRelBC.Start_Date__c = system.today().addMonths(-10);
        objShdRelBC.End_Date__c = system.today().addMonths(10);
        objShdRelBC.Subject_Account__c = objAccount.Id;
        objShdRelBC.Object_Account__c = objAccount2.Id;
        insert objShdRelBC;
        
        Shareholder_Detail__c shareDet =  new Shareholder_Detail__c(Relationship__c=objShdRelInd.Id,Account__c=objAccount.Id,Account_Share__c=accshrlist.id,Sys_Proposed_No_of_Shares__c=2,No_of_Shares__c=2,Status__c='Active');
        insert shareDet;
        
        Shareholder_Detail__c shareDetBC =  new Shareholder_Detail__c(Relationship__c=objShdRelBC.Id,Account__c=objAccount.Id,Account_Share__c=accshrlist.id,Sys_Proposed_No_of_Shares__c=2,No_of_Shares__c=2,Status__c='Active');
        insert shareDetBC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        string RecTypeId = '';
        for(RecordType objRec:[select id from RecordType where DeveloperName='Sale_or_Transfer_of_Shares_or_Membership_Interest' and sObjectType='Service_Request__c']){
            RecTypeId = objRec.Id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.legal_structures__c = 'RLLP';
        objSR.Foreign_Registration_Name__c = 'Test FR Name';
        objSR.Foreign_Registration_Date__c = system.today();
        objSR.Foreign_Registration_Place__c = 'United Arab Emirates';
        objSR.Foreign_Registered_Building__c = 'Building1';
        objSR.Foreign_Registered_Street__c = 'Street1';
        objSR.Foreign_Registered_PO_Box__c = '13124';
        if(RecTypeId!=null && RecTypeId!='')
            objSR.RecordTypeId = RecTypeId;
        objSR.Foreign_Registered_Post__c = '124214';
        objSR.Foreign_registered_office_no__c = '124124';
        objSR.Foreign_Registered_Level__c = '12';
        objSR.Foreign_Registered_Country__c = 'United Arab Emirates';
        objSR.SR_Template__c = objTemplate.Id;
        objSR.Entity_Name__c='Test';
        objSR.Proposed_Trading_Name_1__c='Test';
        objSR.Incorporated_Organization_Name__c='Test';
        objSR.Nature_of_business__c='Test';
        objSR.Nature_of_Business_Arabic__c='Test';
        objSR.Name_identical_to_another_company__c=true;
        objSR.Business_Name__c='Test';
        objSR.Identical_Business_Domicile__c='Test';
        objSR.Pro_Entity_Name_Arabic__c='Test';
        objSR.Pro_Trade_Name_1_in_Arab__c ='Test';
        objSR.Legal_Structures__c='Test';
        objSR.Registration_Type__c='Test';
        objSR.AOA_Format__c='Test';
        objSR.Standard_Charter_Format__c='Test';
        objSR.Current_Registered_Name__c='Test';
        objSR.Domicile_list__c='Test';
        objSR.Current_Registered_Office_No__c='Test';
        objSR.Current_Registered_Building__c='Test';
        objSR.Current_Registered_Level__c='Test';
        objSR.Current_Registered_Street__c='Test';
        objSR.Current_Registered_PO_Box__c='Test';
        objSR.Current_Address_Country__c='Test';
        objSR.Current_Registered_Postcode__c='Test';
        objSR.Principal_Business_Activity__c='Test';
        objSR.Reasons_to_transfer_Foreign_Entity__c='Test';
        objSR.DFSA_Approval__c=true;
        objSR.DFSA_In_principle_Approval_Date__c=system.today();
        insert objSR;
        
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Sale_or_Transfer_of_Shares_or_Membership_Interest';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Sale_or_Transfer_of_Shares_or_Membership_Interest';
        insert objPageFlow;
        
        Page__c objPage = new Page__c();
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        objPage.VF_Page_API_Name__c = 'Change_SH_Members';
        objPage.Is_Custom_Component__c = true;
        objPage.Render_By_Default__c = true;
        insert objPage;
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Navigation_Directions__c = 'Forward';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('PageId',objPage.Id);
        Apexpages.currentPage().getParameters().put('Id',objSR.Id);
        
        ShareTransferCls ST = new ShareTransferCls();
        ShareTransferCls.Total_Transfer_History wrap = new ShareTransferCls.Total_Transfer_History();
        ST.FetchExistingRelations();
        
        ST.AddIndAmendment();
        ST.SaveAmnd();
        ST.PrepareClasses();
        ST.createRow();
        ST.AddBCAmendment();
        ST.SaveAmnd();
        ST.Calculate_Shares();
       
        ST.getDyncPgMainPB();
        ST.ReCalculateTotalNominalValue();
        ST.AmndRowIndex = 0;
        ST.strAmndType = 'Individual';
        ST.EditAmnd();
        ST.strAmndType = 'Body Corporate';
        ST.EditAmnd();
        
        
        test.startTest();
        ShareTransferCls.PrepareExceptionMsg('System.DmlException: Insert failed. First exception on row 0 with id a0N11000003OQ4eEAG; first error: INVALID_FIELD_FOR_INSERT_UPDATE, cannot specify Id in an insert call: [Id]');
        
        Amendment__c amndRemBC = new Amendment__c(Status__c='Active',Sys_Create__c=false,Shareholder_Company__c=objAccount2.Id,Company_Name__c='Rem Comp',ServiceRequest__c=objSR.id,Relationship_Type__c='Shareholder',Amendment_Type__c='Individual');
        insert amndRemBC;
        
        Amendment__c amndBodyCorporate = new Amendment__c(Status__c='Active',Sys_Create__c=false,
                        Shareholder_Company__c=objAccount2.Id,Company_Name__c='Rem Comp',ServiceRequest__c=objSR.id,Relationship_Type__c='Shareholder',
                        Amendment_Type__c='Body Corporate' );
        
        insert amndBodyCorporate;
        
        Amendment__c amndUBO = new Amendment__c(Status__c='Active',Sys_Create__c=false,
                        Shareholder_Company__c=objAccount2.Id,Company_Name__c='Rem Comp',ServiceRequest__c=objSR.id,
                        Amendment_Type__c='Body Corporate' ,related__c = amndBodyCorporate.ID, Relationship_Type__c = 'UBO');
                        
        insert amndUBO;
        
        shareTransfer.Transfer_From__c = amndRemBC.ID;
        shareTransfer.Transfer_To__c = amndBodyCorporate.ID;
        insert shareTransfer;
        
        
        amndRemBC.Customer__c = objAccount.ID;
        amndRemBC.Class__c = accshrlist.ID;
        update amndRemBC;
        
        shareHolderDetail.Amendment__c = amndRemBC.ID;
        shareHolderDetail.account__c = objAccount.ID;
        insert shareHolderDetail;
              
        ST.CurrentAmnd = amndRemBC;
        ST.strAmndId = amndRemBC.Id;
        ST.createRow();
        ST.Add_Amendment_UBO();
        ST.Save_UBO_Amendment_Draft();
        
        ST.Save_UBO_Amendment(ST.strAmndId);
        ST.ValidateAmendmentsUBO( ST.CurrentAmnd);
        ST.DelAmendId_Index = 0;
        ST.strUBOAmndID = amndUBO.ID;
        amndUBO.Sys_Create__c = false;
        ST.updateSelectedUbo();
        ST.lstAmendUBODetails.add(amndUBO);     
        system.debug('$$$$$$$$$' +ST.lstAmendUBODetails.size());
        ST.Delete_UBOamendment();
        ST.DelBodyCorporate_Index = 1;
        ST.Individual_Index = 1;
        amndBodyCorporate.Sys_Create__c = false;
        ST.lstAmendBCDetails.add(amndBodyCorporate);
        ST.DelAmnd_Individual();
        
        ST.DelAmnd();
        ST.CurrentAmnd.Amendment_Type__c = 'Body Corporate';
        ST.CancelAmnd();
        ST.cancelUbo();
        ST.strNavigatePageId = objPage.Id;
        ST.goTopage();
        ST.strActionId = objSD.Id;
        ST.DynamicButtonAction();
        
        ST.AccShareRowIndex = 0;
        ST.delRow();
        ST.AddNewTransferHistoryLine();
        ST.TransferRowIndex = 0;
        ST.ManageTransferRow = 0;
        ST.SaveNewTransferHistoryLine();
        ST.CancelNewTransferHistoryLine();
        ST.AddNewTransferHistoryLine();
        ST.deleteTranferShare();
        ST.AddNewTransferHistoryLine();
        
        test.stopTest();
    }
   
    
   static testMethod void myUnitTest2() {
        
        Lookup__c objlkp = new Lookup__c();
        objlkp.Name = 'India';
        objlkp.Type__c = 'Nationality';
        insert objlkp;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Legal_Type_of_Entity__c = 'LLC';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.legal_structures__c = 'RLLP';
        objSR.Foreign_Registration_Name__c = 'Test FR Name';
        objSR.Foreign_Registration_Date__c = system.today();
        objSR.Foreign_Registration_Place__c = 'United Arab Emirates';
        objSR.Foreign_Registered_Building__c = 'Building1';
        objSR.Foreign_Registered_Street__c = 'Street1';
        objSR.Foreign_Registered_PO_Box__c = '13124';
        insert objSR;
        
        Apexpages.currentPage().getParameters().put('Id',objSR.Id);
        ShareTransferCls ST = new ShareTransferCls();
    }
}