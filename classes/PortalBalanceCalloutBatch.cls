/*
    *Author   : Sravan
    *Company  : NSI DMCC 
    *Date     : 18-Oct-2016
    *Description :This class is used get the latest portal balance from SAP
    
    Modification History :
======================================================================
V1.1    28/12/2016  Ravi    Modified the logic to update the Portal balance
V1.2    01/04/2017  Sravan  Modified for Tkt # 2588 changes
V1.3    23/06/2019  Azfer   Modified for Tkt # 6819 changes
    
*/
Global class PortalBalanceCalloutBatch implements Database.Batchable < sobject > , Database.AllowsCallouts, Schedulable {

    String QueryString;
    List < string > accountStatus;
    public Static string chronExp;
    Global PortalBalanceCalloutBatch() {
        accountStatus = new List < string > {
            'Active',
            'Not Renewed',
            'Under Formation',
            'Pending Dissolution',
            'Dissolved'
        };
        QueryString = 'select id,name,BP_No__c,Hosting_Company__c,Index_Card_Status__c,ROC_Status__c,Balance_Updated_On__c,PSA_Deposit_Claculated__c  from Account where BP_No__c !=null and ROC_Status__c in : accountStatus';
    }

    Global List < Account > start(Database.BatchableContext bc) {
        List < Account > accListToUpdate = new List < Account > ();
        integer balanceUpdatedInterval;
        for (Account acc: Database.Query(QueryString)) {

            if (acc.ROC_Status__c == 'Pending Dissolution' || acc.ROC_Status__c == 'Dissolved') {
                if (acc.Balance_Updated_On__c != null)
                    balanceUpdatedInterval = system.Today().daysBetween(acc.Balance_Updated_On__c.Date());
                if ((balanceUpdatedInterval != null && balanceUpdatedInterval >= 20) || acc.Balance_Updated_On__c == null) accListToUpdate.add(acc);

            } else {
                accListToUpdate.add(acc);
            }

        }
        return accListToUpdate;

    }

    global Void execute(Database.BatchableContext bc, List < Account > accountsList) {

        PortalBalanceCalloutBatch.CalculateAccountBalance(accountsList);
    }

    global void finish(Database.batchableContext BC) {

    }


    Global void Execute(SchedulableContext sc) {
        Database.ExecuteBatch(new PortalBalanceCalloutBatch(), 5);
    }

    Global static void startScheduler() {
        if (chronExp != null)
            System.Schedule('PortalBalanceCallOutBatch', chronExp, new PortalBalanceCalloutBatchScheduler());
    }



    public static void CalculateAccountBalance(List < Account > accountsList) {
        string decryptedSAPPassWrd;
        list < AccountBalenseService.ZSF_S_ACC_BALANCE > items = new list < AccountBalenseService.ZSF_S_ACC_BALANCE > ();
        set < id > accountIDs = new set < id > ();
        Map < id, id > accountWithHostingCompany = new map < id, id > ();
        List < Account > accountsToUpdate = new List < Account > ();
        decryptedSAPPassWrd = test.isrunningTest() == false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) : WebService_Details__c.getAll().get('Credentials').Password__c;

        for (Account acc: accountsList) {
            AccountBalenseService.ZSF_S_ACC_BALANCE item = new AccountBalenseService.ZSF_S_ACC_BALANCE();
            item.KUNNR = acc.BP_No__c;
            item.BUKRS = System.Label.CompanyCode5000;
            items.add(item);

            accountIDs.add(acc.id);
            accountWithHostingCompany.put(acc.id, acc.Hosting_Company__c);
        }

        AccountBalenseService.ZSF_TT_ACC_BALANCE objTTActBal = new AccountBalenseService.ZSF_TT_ACC_BALANCE();
        objTTActBal.item = items;

        AccountBalenseService.account objActBal = new AccountBalenseService.account();
        objActBal.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
        objActBal.inputHttpHeaders_x = new Map < String, String > ();

        Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c + ':' + decryptedSAPPassWrd);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        objActBal.inputHttpHeaders_x.put('Authorization', authorizationHeader);

        AccountBalenseService.Z_SF_ACCOUNT_BALANCEResponse_element objAccountBalResponse = objActBal.Z_SF_ACCOUNT_BALANCE(objTTActBal);

        map < string, decimal[] > mapAccountBalInfo = new map < string, decimal[] > ();

        system.debug('objAccountBalResponse.EX_SF_RS_ACC $$$ ' + objAccountBalResponse.EX_SF_RS_ACC);
        system.debug('objAccountBalResponse.EX_SF_RS_ACC.item $$$ ' + objAccountBalResponse.EX_SF_RS_ACC.item);

        if (objAccountBalResponse != null && objAccountBalResponse.EX_SF_RS_ACC.item != null) {
            for (AccountBalenseService.ZSF_S_ACC_BAL objAccountBalInfo: objAccountBalResponse.EX_SF_RS_ACC.item) {
                system.debug('objAccountBalInfo $$$ ' + objAccountBalInfo);
                if (objAccountBalInfo.KUNNR != null) {
                    decimal[] dActBals;
                    if (mapAccountBalInfo.containsKey(objAccountBalInfo.KUNNR))
                        dActBals = mapAccountBalInfo.get(objAccountBalInfo.KUNNR);
                    else
                        dActBals = new decimal[] {
                            0,
                            0,
                            0
                        };

                    if (objAccountBalInfo.UMSKZ == 'D')
                        dActBals[0] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '') ? decimal.valueOf(objAccountBalInfo.WRBTR) : 0;
                    if (objAccountBalInfo.UMSKZ == 'H')
                        dActBals[1] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '') ? decimal.valueOf(objAccountBalInfo.WRBTR) : 0;
                    if (objAccountBalInfo.UMSKZ == '9')
                        dActBals[2] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '') ? decimal.valueOf(objAccountBalInfo.WRBTR) : 0;

                    mapAccountBalInfo.put(objAccountBalInfo.KUNNR, dActBals);
                    system.debug('mapAccountBalInfo **' + mapAccountBalInfo);
                }
            } //end of for 
        }
        accountIDS.addAll(accountWithHostingCompany.Values());
        Map < id, decimal[] > AccountQuotaDetails = new Map < id, decimal[] > (); //GSHelperCls.QuotaBreakdownForAccounts(accountIDs); Commented for Latest Changes for Visa Quota
        decimal[] VisaInfo;
        if (mapAccountBalInfo != null && mapAccountBalInfo.size() > 0) {
            for (Account accobj: accountsList) {
                VisaInfo = GsHelperCls.CalculateVisa(accobj.Id);
                accobj.Portal_Balance__c = mapAccountBalInfo.ContainsKey(accobj.BP_No__c) ? mapAccountBalInfo.get(accobj.BP_No__c)[0] : 0;
                accobj.PSA_Actual__c = mapAccountBalInfo.ContainsKey(accobj.BP_No__c) ? mapAccountBalInfo.get(accobj.BP_No__c)[1] : 0;
                accobj.PSA_Guarantee__c = mapAccountBalInfo.ContainsKey(accobj.BP_No__c) ? mapAccountBalInfo.get(accobj.BP_No__c)[2] : 0;
                accobj.Balance_Updated_On__c = system.now();
                /*if(AccountQuotaDetails.ContainsKey(accobj.id) && accObj.Hosting_Company__c !=null  && AccountQuotaDetails.ContainsKey(accobj.Hosting_Company__c))                  
                    accobj.Calculated_Visa_Quota__c = AccountQuotaDetails.get(accobj.id)[0] + AccountQuotaDetails.get(accobj.Hosting_Company__c)[0];
                else if(AccountQuotaDetails.ContainsKey(accobj.id) && accObj.Hosting_Company__c ==null)
                    accobj.Calculated_Visa_Quota__c = AccountQuotaDetails.get(accobj.id)[0];
                accobj.Utilize_Visa_Quota__c = AccountQuotaDetails.ContainsKey(accobj.id) ? (AccountQuotaDetails.get(accobj.id)[1]+AccountQuotaDetails.get(accobj.id)[3]) : 0;
                */
                //v1.1
                accobj.Calculated_Visa_Quota__c = VisaInfo[0];
                accobj.Utilize_Visa_Quota__c = VisaInfo[1] + VisaInfo[3];
                accobj.SAP_Update__c = true;
                accobj.Updated_from_SAP__c = system.now();
                system.debug('Quota Details **' + AccountQuotaDetails);
                boolean hasEstablishmentCard = String.isNotBlank(accobj.Index_Card_Status__c) && !accobj.Index_Card_Status__c.equals('Not Available') && !accobj.Index_Card_Status__c.equals('Cancelled');
                if (mapAccountBalInfo.ContainsKey(accobj.BP_No__c)) {
                    decimal[] dPSA;
                    dPSA = AccountBalanceInfoCls.PSACalculation(accobj.id, (mapAccountBalInfo.get(accobj.BP_No__c)[1] + mapAccountBalInfo.get(accobj.BP_No__c)[2]), hasEstablishmentCard);
                    accobj.PSA_Deposit_Claculated__c = (dPSA != null && dPSA.size() > 0) ? dPSA[0] : accobj.PSA_Deposit_Claculated__c;
                    //v1.3 - Added the below line
                    accobj.PSA_Available__c = dPSA[1];

                }
                accountsToUpdate.add(accobj);
            }
        }

        try {
            if (accountsToUpdate != null && accountsToUpdate.size() > 0)
                update accountsToUpdate;
        } catch (Exception e) {
            insert LogDetails.CreateLog(null, 'PortalBalanceCalloutBatch', 'Line # ' + e.getLineNumber() + '\n' + e.getMessage());
        }
    }
}