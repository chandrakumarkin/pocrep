@isTest
private class CaseReopenControllerTest {
	static testmethod void caseReOpen(){
        Case objCase = new Case();
        objCase.Subject = 'test Subject';
        objCase.Origin = 'Email';
        objCase.GN_Department__c = 'Business Centre';
        insert objCase;
        PageReference pageRef = Page.CaseReopen;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(objCase.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
        CaseReopenController testCntrl = new CaseReopenController(sc);
        testCntrl.ReopenAction();
        testCntrl.CancelAction();
    }
}