/******************************************************************************************
 *  Author      : Selva
 *  Date        : 2019-07-02    
 *  Purpose     : test class for SendNotificationLiquidators batch
*******************************************************************************************/
@isTest(SeeAllData=false)
private class ScheduleLiquidators_test{
    
    static TestMethod void penaltyCalculatorTest(){
        
        Test.StartTest();
        Lookup__c lk =new Lookup__c();
        lk.Name ='Test'; 
        lk.Email__c='test@outlook.com';
        lk.Type__c = 'Liquidator';
        lk.Recognized__c = true;
        insert lk;
        
        ScheduleLiquidators  obj = new ScheduleLiquidators();      
        String schTime = '0 0 23 * * ?';
        system.schedule('Test', schTime, obj);
        
        SendNotificationLiquidators b = new SendNotificationLiquidators ();
        DateTime sysTime = System.now();
        sysTime = sysTime.addMinutes(2);
        String chronJobId = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        //schedule a job
        System.schedule('Batch Contact Update Auto'+chronJobId, chronJobId, b);
        
        Test.stopTest();
     }
}