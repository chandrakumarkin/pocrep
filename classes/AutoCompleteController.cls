/*
 * Controller to implement the auto-complete feature on the Visualforce page
 */
global with sharing class AutoCompleteController {
    public static List<User> selectedPortalUsers { get; set; }
    public static List<Service_Request__c> finePaymentRequests { get; set; }
    public static Decimal fineSrAmount  { get; set; }
    public  Boolean isFine  { get; set; }
    public  Boolean isPortalUser  { get; set; }
    public void GetPROList() {

        system.debug(accountList + 'accountList');

        if (accountList.size() > 0 && accountList.size() <= 1) {



            for (Relationship__c rel: [select id, Object_Contact__r.name, Object_Contact__r.id from Relationship__c where Relationship_Type__c = 'Is public Relationship officer for'
                    and Subject_Account__c in: accountList and Active__c = true
                ]) {


                RelationshipMap.put(rel.Object_Contact__r.id, rel);

            }
        } else if (accountList.size() > 1) {

            Map < id, Relationship__c > RelationshipMapDup = new Map < id, Relationship__c > ();

            List < Relationship__c > RelList = [select id, Object_Contact__r.name, Object_Contact__r.id from Relationship__c where Relationship_Type__c = 'Is public Relationship officer for'
                and Subject_Account__c in: accountList and Active__c = true
            ];

            set < id > ContactIds = new set < id > ();

            for (Relationship__c rel: RelList) {
                if (!ContactIds.add(rel.Object_Contact__r.id)) {

                    RelationshipMap.put(rel.Object_Contact__r.id, rel);
                }
            }


        }

        system.debug(RelationshipMap + 'RelationshipMap');
    }

    public boolean isGSUser { get; set; }
    public boolean isRORPUser { get; set; }

    public PageReference GetCollectedDocs() {


        if (selectedSteps.size() > 0) {
            string Contentbody = '';
            string body = '';
            String ApplicantFName = '';
            String ApplicantLName = '';

            for (step__c step: selectedSteps) {


                String Courier = (step.sr__r.Would_you_like_to_opt_our_free_couri__c == null || step.sr__r.Would_you_like_to_opt_our_free_couri__c == 'null') ? '' : step.sr__r.Would_you_like_to_opt_our_free_couri__c;
                ApplicantFName = (step.SR__r.First_Name__c == null || step.SR__r.First_Name__c == 'null') ? '' : step.SR__r.First_Name__c;
                ApplicantLName = (step.SR__r.Last_Name__c == null || step.SR__r.Last_Name__c == 'null') ? '' : step.SR__r.Last_Name__c;

                body += 'SR Number      ' + ' ' + ':' + '  ' + step.sr__r.name + ' ' + '\n';
                body += 'Service Type   ' + ' ' + ':' + '  ' + step.Sr__r.Service_Type__c + ' ' + '\n';
                body += 'Step Number    ' + ' ' + ':' + '  ' + step.name + ' ' + '\n';
                body += 'Applicant Name ' + ' ' + ':' + '  ' + ApplicantFName + '  ' + ApplicantLName + ' ' + '\n';
                body += 'Customer Name  ' + ' ' + ':' + '  ' + step.SR__r.Client_Name__c + ' ' + '\n';
                body += 'Step Name      ' + ' ' + ':' + '  ' + step.Step_Name__c + ' ' + '\n';
                body += 'SR Status      ' + ' ' + ':' + '  ' + step.SR_Status__c + ' ' + '\n';
                body += 'Opt for Courier' + ' ' + ':' + '  ' + Courier + ' ' + '\n\n\n\n';
                Contentbody = body;
            }



            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            // ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.textSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List < ConnectApi.MessageSegmentInput > ();

            //mentionSegmentInput.id = '0F90J000000To7x';

            for (CollaborationGroupMember usersingroup: [SELECT Id, MemberId FROM CollaborationGroupMember where CollaborationGroupId = '0F90J000000To7x']) {
                ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
                mentionSegmentInput.id = usersingroup.MemberId; messageBodyInput.messageSegments.add(mentionSegmentInput);
            }


            //  messageBodyInput.messageSegments.add(mentionSegmentInput);

            textSegmentInput.text = Contentbody;
            messageBodyInput.messageSegments.add(textSegmentInput);

            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = '0F90J000000To7x';

            if (!Test.isRunningTest()) ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.confirm, 'Email Has been sent.'));

        } else {


            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No records found in selected List'));


        }

        return null;
    }


    integer attCount = Integer.valueof(system.label.No_Of_Attachments);
    public List < WrapperCollectedSteps > listWrapper { get; set; }
    public String searchTerm { get; set; }
    public String selectedMovie { get; set; }
    Map < id, step__c > SelectedstepMap = new Map < id, step__c > ();
    public List < step__c > selectedSteps { get; set; }
    List < step__c > steps = new List < step__c > ();
    public string SRid { get; set; }
    public string selectedStep { get; set; }


    public PageReference cancel() { return new PageReference('/home/home.jsp');}
    public class WrapperCollectedSteps {
        public Boolean checkBool { get; set; }
        public step__c step { get; set; }
        public WrapperCollectedSteps(step__c step) {
            this.step = step;
        }
    }


    public string selectedPRO { get; set; }

    public List < Selectoption > getAllPROs() {

        List < Selectoption > lstProname = new List < selectoption > ();

        lstProname.add(new selectOption('', '- None -'));


        for (Relationship__c Rel: RelationshipMap.values()) {

            lstProname.add(new selectoption(rel.Object_Contact__r.name, rel.Object_Contact__r.name));
        }

        return lstProname;
    }

    set < ID > accountList = new set < Id > ();
    List < string > ProNames = new List < string > ();

    Map < id, Relationship__c > RelationshipMap = new Map < id, Relationship__c > ();

    public void getStepstrue() {
       
        listWrapper = new List < WrapperCollectedSteps > ();
        for (step__c step: [select id, name, Customer__c, Applicant_Name__c, Step_Name__c, sr__r.name, sr__r.Fine_Amount_AED__c, sr__r.Building__r.Name, sr__r.Unit__r.Name, Sr__r.Service_Type__c, SR__r.First_Name__c, SR__r.Last_Name__c, SR__r.Client_Name__c, sr__r.Customer__c, sr__r.Would_you_like_to_opt_our_free_couri__c, SR_Status__c from step__c where isCollectedStep__c = true and sr__c =: SRid]) {

            steps.add(step);
            system.debug('steps --- ' + step.sr__r.Unit__r.Name);
            listWrapper.add(new WrapperCollectedSteps(step));
            accountList.add(step.sr__r.Customer__c);
            fineSrAmount = fineSrAmount == null && step.sr__r.Fine_Amount_AED__c != null ? step.sr__r.Fine_Amount_AED__c : 0.00;
        }
        
        if(fineSrAmount != 0.00){
        
          finePaymentRequests = [SELECT Id,Name,External_SR_Status__r.Name FROM Service_Request__c WHERE Linked_SR__c =: SRid AND Service_Type__c ='Government Fine Payment' ];
        }
         selectedPortalUsers = [SELECT Id,Name FROM User WHERE AccountId IN :accountList AND Community_User_Role__c INCLUDES('Employee Services')];
        
        
        /* if(accountList.size()>0){
            
            for( Relationship__c  rel : [select id,Object_Contact__r.name,Object_Contact__r.id from Relationship__c where Relationship_Type__c = 'Is public Relationship officer for' and Subject_Account__c in: accountList and Active__c = true]){
                
                //ProNames.add(rel.Object_Contact__r.name);
                
                RelationshipMap.put(rel.Object_Contact__r.id,rel);
                
            }
            
            
        }*/

    }
    public void Add() {
        if (SRid == null || SRid == '') {

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide Service Request in Search Box'));

        } else {

            system.debug(listWrapper.size() + '===========size======');
            selectedSteps = new List < step__c > ();
            //1.1 Below code is added for fine checking
            for (WrapperCollectedSteps Wrapperstep: listWrapper) {
                if (Wrapperstep.checkBool && Wrapperstep.Step.Sr__r.Fine_Amount_AED__c != null && Wrapperstep.Step.Sr__r.Fine_Amount_AED__c > 0 &&
                    GSUtilityClass.CheckServiceRequestForFine(Wrapperstep.Step.SR__c, Wrapperstep.Step.Sr__r.Fine_Amount_AED__c)) {

                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The government fine related to this request is not paid'));
                    return;

                }
            }
            //1.1 End
            for (WrapperCollectedSteps Wrapperstep: listWrapper) {
                if (Wrapperstep.checkBool == true) {
                    SelectedstepMap.put(Wrapperstep.step.id, Wrapperstep.step);
                    for (step__c step: SelectedstepMap.values()) {

                        if (!(selectedSteps.contains(step))) {

                            map < Id, Step__c > mapFineSteps = new map < Id, Step__c > ([select Id, Fine_Amount__c from Step__c where Fine_Amount__c != null AND Fine_Amount__c != 0 AND SR__c =: step.SR__c AND SAP_Seq__c != null]);
                            system.debug(mapFineSteps);
                            // if ((mapFineSteps != null && !mapFineSteps.isEmpty())) {
                            //     Integer iC = 0;
                            //     for (Service_Request__c objFineSR: [select Id, External_Status_Name__c from Service_Request__c where Record_Type_Name__c = 'Over_Stay_Fine'
                            //             AND Linked_SR__c =: step.SR__c AND SAP_Unique_No__c != null AND Id != null
                            //         ]) {
                            //         system.debug(objFineSR);
                            //         iC++;
                            //     }

                            //     if (mapFineSteps.size() != iC) {

                            //         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please approve the government fines request related to this request before closing this step'));
                            //     } else {
                            //         selectedSteps.add(step);
                            //     }
                            // } else {
                                selectedSteps.add(step);
                            // }
                        }
                    }
                }
                if (selectedSteps.size() <= 0) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select aleast one check box'));
                }
            }
        }
    }

    @RemoteAction
    global static List < step__c > searchsteps(String searchTerm) {
        
        /* ID  dhaSRNo, SRNo;
        List<Step__c> visaSteps;
        List<Step__c> dhaOREidStep = Database.query('select id, SR__c from Step__c where ((Step_Name__c = \'Medical Fitness Test Completed\' and Application_Reference__c like \'%'+ string.escapeSingleQuotes(searchTerm) + '%\' ) OR (Step_Name__c = \'Emirates ID Registration is Completed\' and SAP_IDNUM__c like \'%'+ string.escapeSingleQuotes(searchTerm) + '%\') OR (SR__r.Emirates_Id_Number__c like \'%'+ string.escapeSingleQuotes(searchTerm) + '%\' OR SR__r.Contact__r.BP_No__c like \'%' + string.escapeSingleQuotes(searchTerm) + '%\' OR SR__r.Passport_Number__c like \'%' + string.escapeSingleQuotes(searchTerm) + '%\' OR SR__r.SAP_PR_No__c like \'%' + string.escapeSingleQuotes(searchTerm) + '%\'  OR SR__r.name like \'%'+ String.escapeSingleQuotes(searchTerm) +'%\' OR SR__r.Current_Visa_No__c like \'%'+ String.escapeSingleQuotes(searchTerm) +'%\' ) )');
        SRNo = dhaOREidStep.size() > 0 ? dhaOREidStep[0].SR__c : null;
        if(dhaOREidStep.size() > 0 ){
            dhaSRNo = dhaOREidStep[0].SR__c;
            visaSteps = Database.query('Select id, SR__C from step__c where ( Step_Name__c = \'Visa copy uploaded\' AND Is_Closed__c = true AND SR__c =: dhaSRNo ) ');
            
        } else{
            visaSteps = Database.query('Select id, SR__C from step__c where  (Step_Name__c = \'Visa copy uploaded\' AND Is_Closed__c = true AND (SR__r.Emirates_Id_Number__c like \'%'+ string.escapeSingleQuotes(searchTerm) + '%\' OR SR__r.Contact__r.BP_No__c like \'%' + string.escapeSingleQuotes(searchTerm) + '%\' OR SR__r.Passport_Number__c like \'%' + string.escapeSingleQuotes(searchTerm) + '%\' OR SR__r.SAP_PR_No__c like \'%' + string.escapeSingleQuotes(searchTerm) + '%\'  OR SR__r.name like \'%'+ String.escapeSingleQuotes(searchTerm) +'%\' OR SR__r.Current_Visa_No__c like \'%'+ String.escapeSingleQuotes(searchTerm) +'%\' ))');
        } 
        SRNo = visaSteps.size() > 0 ? visaSteps[0].SR__c : null;  */
        List<Step__c> SRSteps = Database.query ('select id, SR__c from Step__c where ((Step_Name__c = \'Front Desk Review\' and DNRD_Receipt_No__c like \'%'+ string.escapeSingleQuotes(searchTerm) + '%\' ) OR (Step_Name__c = \'Emirates ID Registration is Completed\' and SAP_IDNUM__c like \'%'+ string.escapeSingleQuotes(searchTerm) + '%\') OR (SR__r.Contact__r.BP_No__c like \'%' + string.escapeSingleQuotes(searchTerm) + '%\' OR SR__r.Passport_Number__c like \'%' + string.escapeSingleQuotes(searchTerm) + '%\' OR SR__r.SAP_PR_No__c like \'%' + string.escapeSingleQuotes(searchTerm) + '%\'  OR SR__r.name like \'%'+ String.escapeSingleQuotes(searchTerm) +'%\' OR SR__r.Current_Visa_No__c like \'%'+ String.escapeSingleQuotes(searchTerm) +'%\' ) )');             
        set<ID> SRIDs = new set<ID>();
        Set<Id> accountId = new Set<Id>();
        for(Step__c s : SRSteps){
            SRIDs.add(s.SR__c);
          //  accountId.add(s.client__c);
        }
        system.debug('***************deployed');
        
        
        
        // ID SRNo = SRSteps.size() > 0 ? SRSteps[0].SR__c : null;  
        List<Step__c> steps = Database.query('Select Id,Name,SR__c,sr__r.name,SAP_PR_No__c from step__c where ( isCollectedStep__c = true and SR__c in: SRIDs )');                 
        return steps;
    }


    public List < Attachment > allFileList { get; set; }
    public transient string client_Signature = '';

    public boolean isDisable { get; set; }
    public integer count = 0;

    public AutoCompleteController() {
        
        isDisable = false;
        selectedSteps = new List < step__c > ();
        profile profileName = [select id, name from profile where id =: userInfo.getProfileId() limit 1];
        isGSUser = (profileName.name.contains('GS') == true) ? true : false;
        isRORPUser = (profileName.name.contains('RORP') == true) ? true : false;
        finePaymentRequests = new List<Service_Request__c>();
        selectedPortalUsers = new List<User>();
        allFileList = new List < Attachment > ();
        allFileList.clear();
        for (Integer i = 1; i < = attCount; i++)
            allFileList.add(new Attachment());

    }

    public void setClient_Signature(String n){client_Signature = n;}
    public String getClient_Signature() {return client_Signature;}

    public Transient Attachment attachment; //V1.13
    public Attachment getattachment() {attachment = new Attachment(); return attachment;}

    public void SelectedValue() {selectedPRO = selectedPRO;}

    public Pagereference saveSignatureOfClient() {
        isDisable = true;

        if (SRid == null || SRid == '') {

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide signature'));

        } else {


            List < attachment > attachments = new List < attachment > ();
            for (step__c step: selectedSteps) {
                Attachment att = new Attachment();
                att.ParentId = step.id;
                att.name = step.name + '_Signature.png';
                att.ContentType = 'image/png';
                att.Body = EncodingUtil.base64Decode(Client_Signature);
                attachments.add(att);
            }
            //insert attachments;  

            Database.SaveResult[] AttachList = database.insert(attachments);
            if (AttachList != null) {
                for (Database.SaveResult result: AttachList) {
                    if (result.isSuccess()) {

                        count++;
                    }
                }
            }
            PageReference congratsPage;
            boolean b = SaveAttachments();
            if (b == true) {
                congratsPage = Page.AutoComplete;
                congratsPage.setRedirect(true);
                return congratsPage;
            }
        }

        return null;
    }
    public boolean SaveAttachments() {
        List < Attachment > listToInsert = new List < Attachment > ();

        integer FileCount = 0;
        integer stpCount = 0;

        List < string > EidSteps = new List < string > ();

        for (Attachment a: allFileList) {
            //FileCount++;
            for (step__c step: selectedSteps) {
                if (a.name != '' && a.name != '' && a.body != null) {
                    listToInsert.add(new Attachment(parentId = step.id, name = a.name, body = a.body));
                    FileCount++;
                }
            }
            a.body = null;
        }
        try {
            //Inserting attachments

            if (FileCount <= 0 && (selectedPRO == null || selectedPRO == '')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please uplaod at least one document'));
                return false;
            } else {
                Database.SaveResult[] AttachList = database.insert(listToInsert);
                if (AttachList != null) {
                    for (Database.SaveResult result: AttachList) {
                        if (result.isSuccess()) {

                            count++;
                        }
                    }
                }
                if (count > 0) {
                    Status__c stpStatus = [select id from Status__c where code__c = 'CLOSED'];
                    List < step__c > steps = new List < step__c > ();
                    for (step__c stp: selectedSteps) {
                        step__c step = new step__c();
                        step.id = stp.id;
                        step.status__c = stpStatus.id;
                        step.ownerid = userInfo.getUserId();
                        step.Collected_PRO__c = selectedPRO;
                        steps.add(step);
                        if (stp.Step_Name__c == 'Documents Collected for EID Biometrics' || stp.Step_Name__c == 'Collected' || stp.Step_Name__c == 'Visa transfer form is collected') {
                            EidSteps.add(stp.id);
                        }


                    }

                    Database.SaveResult[] AttachList1 = database.update(steps);
                    if (AttachList1 != null) {
                        for (Database.SaveResult result: AttachList1) {
                            if (result.isSuccess()) {

                                stpCount++;
                            }
                        }
                    }

                    if (EidSteps.size() > 0 && stpCount > 0) {
                        User objUser = [select Id, SAP_User_Id__c from User where Id =: UserInfo.getUserId() limit 1];
                        GsSapWebServiceDetails.CurrentUserId = objUser.SAP_User_Id__c;
                        GsSapWebServiceDetails.IsClosed = true;
                        GsSapWebServiceDetails.PushActivityToSAP(EidSteps);
                    }

                    map < id, string > SRMapStatus = new Map < id, string > ();
                    List < string > SRIds = new List < string > ();
                    List < service_request__c > listSRs = new List < service_request__c > ();
                    for (Step__c obj: [select Id, SAP_Seq__c, Step_Template__r.SR_Status__c, sr__c from Step__c where Id In: EidSteps AND SAP_Seq__c != null AND Step_Status__c = 'Closed'
                            AND SAP_Seq__c != '0010'
                        ]) {
                        SRIds.add(obj.sr__c);
                        SRMapStatus.put(obj.Sr__c, obj.Step_Template__r.SR_Status__c);
                    }

                    for (string str: SRIds) {
                        Service_request__c SR = new Service_request__c();
                        SR.id = str;
                        SR.External_SR_Status__c = SRMapStatus.get(str);
                        SR.Internal_SR_Status__c = SRMapStatus.get(str);
                        listSRs.add(SR);
                    }

                    update listSRs;

                    return true;
                }

                //ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.INFO, listToInsert.size() + ' file(s) are uploaded successfully'));        
            }
        } catch (Exception e) {} 
        return null;    }    }