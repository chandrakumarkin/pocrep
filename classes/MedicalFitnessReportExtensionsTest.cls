@IsTest(SeeAllData=false)
public class MedicalFitnessReportExtensionsTest {

    static testMethod void MedicalFitnessTestWithAppointment() {
    
        SR_Template__c SRTemplateObj = new SR_Template__c(Name='Permit to Work', Menu__c='Other Services', 
                        Menutext__c='new', Sub_menu__c='new', 
                        SR_RecordType_API_Name__c='Permit_to_Work', Active__c=true, Available_for_menu__c=true,
                        Available_For_Inactive_user__c='Available to both',SR_Description__c='  test-description',Is_through_Flow__c=false);
        insert SRTemplateObj;

        
        Service_Request__c ServiceRequestObj = new Service_Request__c();
        ServiceRequestObj.SR_Template__c = SRTemplateObj.Id;
        ServiceRequestObj.SR_Group__c = 'GS';
        ServiceRequestObj.Email__c = 'testsr@difc.com';    
        ServiceRequestObj.Express_Service__c = false;
          ServiceRequestObj.Would_you_like_to_opt_our_free_couri__c = 'No';
        insert ServiceRequestObj;
            
        Step_Template__c StepTemplateObj = new Step_Template__c();
        StepTemplateObj.Name = 'Medical Fitness Test scheduled';
        StepTemplateObj.Code__c = 'Medical Fitness Test scheduled';
        StepTemplateObj.Step_RecordType_API_Name__c = 'General';
        StepTemplateObj.Summary__c = 'Medical Fitness Test scheduled';
        insert StepTemplateObj;

        Step_Template__c StepTemplateObj2 = new Step_Template__c();
        StepTemplateObj2.Name = Label.DHA_Step_Name_Entry_No;
        StepTemplateObj2.Code__c = Label.DHA_Step_Name_Entry_No;
        StepTemplateObj2.Step_RecordType_API_Name__c = 'General';
        StepTemplateObj2.Summary__c = Label.DHA_Step_Name_Entry_No;
        insert StepTemplateObj2;
        
        Step__c StepObj = new Step__c();
        StepObj.SR__c = ServiceRequestObj.Id;
        StepObj.No_PR__c = false;
        StepObj.Step_Template__c = StepTemplateObj.id ;
        insert StepObj;

        Step__c StepObj2 = new Step__c();
        StepObj2.SR__c = ServiceRequestObj.Id;
        StepObj2.No_PR__c = false;
        StepObj2.Step_Template__c = StepTemplateObj2.id ;
        insert StepObj2;

        SR_Doc__c SRDocObj = new SR_Doc__c();
        SRDocObj.Name = 'Passport';
        SRDocObj.Service_Request__c =ServiceRequestObj.id;
        insert SRDocObj;
            
        Attachment ObjAttachment = new Attachment();
        ObjAttachment.name ='test';
        ObjAttachment.Body = Blob.valueOf('test body');
        ObjAttachment.ParentId = SRDocObj.id;
        insert ObjAttachment;  

        Attachment ObjAttachment2 = new Attachment();
        ObjAttachment2.name ='test';
        ObjAttachment2.Body = Blob.valueOf('test body');
        ObjAttachment2.ParentId = SRDocObj.id;
        insert ObjAttachment2;    

        Appointment__c AppointmentObj  = new Appointment__c();
        AppointmentObj.Applicantion_Type__c = 'Normal';
        AppointmentObj.Service_Request__c = ServiceRequestObj.Id;
        AppointmentObj.Step__c = StepObj.Id;
        AppointmentObj.Appointment_StartDate_Time__c = System.now();
            
        insert AppointmentObj;
        
        status__c stepStatus = new Status__c();//Code__c='Closed',Type__c='End',Name='Closed'
        stepStatus.Type__c='End';
        stepStatus.Name='Closed';
        stepStatus.Code__c='Closed';
        insert stepStatus;
            
        Test.StartTest(); 

        PageReference pageRef = Page.MedicalFitnessReport;
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(StepObj);
        
        ApexPages.currentPage().getParameters().put('Id',StepObj.id);
        ApexPages.currentPage().getParameters().put('appointmentId',AppointmentObj.id);
        
        MedicalFitnessReportExtensions MedicalFitnessReportExtensionsObj = new MedicalFitnessReportExtensions(sc);        
        
        MedicalFitnessReportExtensionsObj.Step.Is_blood_test_done__c = true;
        MedicalFitnessReportExtensionsObj.Step.Is_x_ray_done__c = true; 
        
        MedicalFitnessReportExtensionsObj.UpdateStep();
        
        MedicalFitnessReportExtensionsObj.Step.Is_blood_test_done__c = false;
        MedicalFitnessReportExtensionsObj.Step.Is_x_ray_done__c = false; 

        MedicalFitnessReportExtensionsObj.ProcessCompleted();
        
        MedicalFitnessReportExtensionsObj.CancelAndNavigateToListView();
        MedicalFitnessReportExtensionsObj.UpdateApplicantStatus();

        MedicalFitnessReportExtensions.getAttachmentDetails( ObjAttachment.Id );
        
        Test.StopTest();       
    }

    static testMethod void MedicalFitnessTestWithAppointment2() {
    
        SR_Template__c SRTemplateObj = new SR_Template__c(Name='Permit to Work', Menu__c='Other Services', 
                        Menutext__c='renewal', Sub_menu__c='renewal', 
                        SR_RecordType_API_Name__c='Permit_to_Work', Active__c=true, Available_for_menu__c=true,
                        Available_For_Inactive_user__c='Available to both',SR_Description__c='  test-description',Is_through_Flow__c=false);
        insert SRTemplateObj;

        
        Service_Request__c ServiceRequestObj = new Service_Request__c();
        ServiceRequestObj.SR_Template__c = SRTemplateObj.Id;
        ServiceRequestObj.SR_Group__c = 'GS';
        ServiceRequestObj.Email__c = 'testsr@difc.com';    
        ServiceRequestObj.Express_Service__c = false;
        ServiceRequestObj.Type_of_Request__c = 'Applicant Outside UAE';
        ServiceRequestObj.Would_you_like_to_opt_our_free_couri__c = 'No';
        insert ServiceRequestObj;
            
        Step_Template__c StepTemplateObj = new Step_Template__c();
        StepTemplateObj.Name = 'Medical Fitness Test scheduled';
        StepTemplateObj.Code__c = 'Medical Fitness Test scheduled';
        StepTemplateObj.Step_RecordType_API_Name__c = 'General';
        StepTemplateObj.Summary__c = 'Medical Fitness Test scheduled';
        insert StepTemplateObj;

        Step__c StepObj = new Step__c();
        StepObj.SR__c = ServiceRequestObj.Id;
        StepObj.No_PR__c = false;
        StepObj.Step_Template__c = StepTemplateObj.id ;
        insert StepObj;

        Step_Template__c StepTemplateObj2 = new Step_Template__c();
        StepTemplateObj2.Name = Label.DHA_Step_Name;
        StepTemplateObj2.Code__c = Label.DHA_Step_Name;
        StepTemplateObj2.Step_RecordType_API_Name__c = 'General';
        StepTemplateObj2.Summary__c = Label.DHA_Step_Name;
        insert StepTemplateObj2;

        Step__c StepObj2 = new Step__c();
        StepObj2.SR__c = ServiceRequestObj.Id;
        StepObj2.No_PR__c = false;
        StepObj2.Step_Template__c = StepTemplateObj2.id ;
        insert StepObj2;

        SR_Doc__c SRDocObj = new SR_Doc__c();
        SRDocObj.Name = 'Passport';
        SRDocObj.Service_Request__c =ServiceRequestObj.id;
        insert SRDocObj;
            
        Attachment ObjAttachment = new Attachment();
        ObjAttachment.name ='test';
        ObjAttachment.Body = Blob.valueOf('test body');
        ObjAttachment.ParentId = SRDocObj.id;
        insert ObjAttachment;  

        Attachment ObjAttachment2 = new Attachment();
        ObjAttachment2.name ='test';
        ObjAttachment2.Body = Blob.valueOf('test body');
        ObjAttachment2.ParentId = SRDocObj.id;
        insert ObjAttachment2;    

        Appointment__c AppointmentObj  = new Appointment__c();
        AppointmentObj.Applicantion_Type__c = 'Normal';
        AppointmentObj.Service_Request__c = ServiceRequestObj.Id;
        AppointmentObj.Step__c = StepObj.Id;
        AppointmentObj.Appointment_StartDate_Time__c = System.now();
            
        insert AppointmentObj;
        
        status__c stepStatus = new Status__c();//Code__c='Closed',Type__c='End',Name='Closed'
        stepStatus.Type__c='End';
        stepStatus.Name='Closed';
        stepStatus.Code__c='Closed';
        insert stepStatus;
            
        Test.StartTest(); 

        PageReference pageRef = Page.MedicalFitnessReport;
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(StepObj);
        
        ApexPages.currentPage().getParameters().put('Id',StepObj.id);
        ApexPages.currentPage().getParameters().put('appointmentId',AppointmentObj.id);
        
        MedicalFitnessReportExtensions MedicalFitnessReportExtensionsObj = new MedicalFitnessReportExtensions(sc);        
        
        MedicalFitnessReportExtensionsObj.Step.Is_blood_test_done__c = true;
        MedicalFitnessReportExtensionsObj.Step.Is_x_ray_done__c = true; 
        
        MedicalFitnessReportExtensionsObj.UpdateStep();
        
        MedicalFitnessReportExtensionsObj.Step.Is_blood_test_done__c = false;
        MedicalFitnessReportExtensionsObj.Step.Is_x_ray_done__c = false; 

        MedicalFitnessReportExtensionsObj.ProcessCompleted();
        
        MedicalFitnessReportExtensionsObj.CancelAndNavigateToListView();
        MedicalFitnessReportExtensionsObj.UpdateApplicantStatus();

        MedicalFitnessReportExtensions.getAttachmentDetails( ObjAttachment.Id );
        
        Test.StopTest();       
    }

    static testMethod void MedicalFitnessTestWithOutAppointment() {
    
        Service_Request__c ServiceRequestObj = new Service_Request__c();
        ServiceRequestObj.SR_Group__c = 'GS';
        ServiceRequestObj.Email__c = 'testsr@difc.com';    
        ServiceRequestObj.Express_Service__c = false;
        ServiceRequestObj.Would_you_like_to_opt_our_free_couri__c = 'No';
        insert ServiceRequestObj;
            
        step__c StepObj = new Step__c();
        StepObj.SR__c = ServiceRequestObj.Id;
        StepObj.No_PR__c = false;
        insert StepObj;

        SR_Doc__c SRDocObj = new SR_Doc__c();
        SRDocObj.Name = 'Passport';
        SRDocObj.Service_Request__c =ServiceRequestObj.id;
        insert SRDocObj;
            
        Attachment ObjAttachment = new Attachment();
        ObjAttachment.name ='test';
        ObjAttachment.Body = Blob.valueOf('test body');
        ObjAttachment.ParentId = SRDocObj.id;
        insert ObjAttachment;
        
        status__c stepStatus = new Status__c();//Code__c='Closed',Type__c='End',Name='Closed'
        stepStatus.Type__c='End';
        stepStatus.Name='Closed';
        stepStatus.Code__c='Closed';
        insert stepStatus;    
        
        Test.StartTest(); 

        PageReference pageRef = Page.MedicalFitnessReport;
        
        Test.setCurrentPage(pageRef);
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(StepObj);
        
        ApexPages.currentPage().getParameters().put('Id',StepObj.id);
       
        MedicalFitnessReportExtensions MedicalFitnessReportExtensionsObj = new MedicalFitnessReportExtensions(sc);        
        MedicalFitnessReportExtensionsObj.AppointmentDetails();
        MedicalFitnessReportExtensionsObj.SecondMedicalDone();
        MedicalFitnessReportExtensionsObj.ThirdMedicalDone();
        MedicalFitnessReportExtensionsObj.MarkUnfit();
        Test.StopTest();       
    }
}