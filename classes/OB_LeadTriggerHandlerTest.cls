/*
Modification History
Shikha 12/02/2020 - Modified class for test coverage
*/
@isTest
public class OB_LeadTriggerHandlerTest {
    
    @testSetup
    static  void createTestData() {
        Test.startTest();
        // create account
        List<Account> lstNewAccount = new List<Account>();
        lstNewAccount =  OB_TestDataFactory.createAccounts(1);
        insert lstNewAccount;
        
        // create contact
        List<Contact> lstContacts = new List<Contact>();
        lstContacts = OB_TestDataFactory.createContacts(1,lstNewAccount);
        insert lstContacts;
        
        
        
        Opportunity opp = new Opportunity();
        opp.AccountId = lstNewAccount[0].id;
        opp.Name = 'test';
        opp.StageName='Prospecting';
        opp.CloseDate=System.today().addMonths(1);
        insert opp;
        
        Lead leadObj = new Lead();
        leadObj.lastname =  'TestLead';
        leadObj.company= 'TestLeadComp';
        leadObj.email= 'testemail'+'@test.com';
        leadObj.Status= 'Interested';
        leadObj.Geographic_Region__c= 'Africa';
        leadObj.Country__c= 'Algeria';
        leadObj.Priority_to_DIFC__c= 'T2: Global or high FTE';
        leadObj.Rating= 'Cold';
        leadObj.LOI_Date__c = System.today().addMonths(1);
        leadObj.LeadSource = 'Referral';
        leadObj.Send_Portal_Login_Link__c = 'Yes';
        leadObj.Is_Commercial_Permission__c = 'Yes';
        leadObj.OB_Type_of_commercial_permission__c = 'Gate Avenue Pocket Shops';
        leadObj.Company_Type__c = 'Non - financial';
        leadObj.BD_Sector_Classification__c = 'Capital Markets';
        //Shikha - correcting the test class error received
        leadObj.Phone = '+971512512512';
        leadObj.MobilePhone = '+971512512512';
        insert leadObj;
        
        Lead leadObj2 = new Lead();
        leadObj2.lastname =  'TestLead';
        leadObj2.company= 'TestLeadComp';
        leadObj2.email= 'testemaill'+'@test.com';
        leadObj2.Status= 'Interested';
        leadObj2.Geographic_Region__c= 'Africa';
        leadObj2.Country__c= 'Algeria';
        leadObj2.Priority_to_DIFC__c= 'T2: Global or high FTE';
        leadObj2.Rating= 'Cold';
        leadObj2.LOI_Date__c = System.today().addMonths(1);
        leadObj2.LeadSource = 'Referral';
        leadObj2.Send_Portal_Login_Link__c = 'Yes';
        leadObj2.Is_Commercial_Permission__c = 'Yes';
        leadObj2.OB_Type_of_commercial_permission__c = 'Gate Avenue Pocket Shops';
        leadObj2.Company_Type__c = 'Non - financial';
        leadObj2.BD_Sector_Classification__c = 'Capital Markets';
        //Shikha - added code for test coverage
        leadObj2.Phone = '+971546787778';
        leadObj2.MobilePhone = '+971546787778';
        
        insert leadObj2;
        
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
        
        
        User user = new User(alias = 'test123', email='testemaill'+'@test.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = lstContacts[0].Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        
        Test.stopTest();
    }
    
    static testMethod void CreateDraftSRTest() {
        
        List<Contact> lstContacts = [select id,AccountId,Account.Phone from Contact];
        List<account> lstNewAccount = [select id,Is_Commercial_Permission__c from account];
        
        list<lead> ldList = [select id,IsConverted,Email,ConvertedContactId,ConvertedAccountId from lead where email='testemail@test.com'];
        list<lead> ldList2 = [select id,IsConverted,Email,ConvertedContactId,ConvertedAccountId from lead where email='testemaill@test.com'];
        
        
        database.leadConvert lc = new database.leadConvert();
        lc.setLeadId(ldList[0].id);
        
        leadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
        lc.setConvertedStatus(convertStatus.MasterLabel);
        lc.setAccountId(lstNewAccount[0].id);
        lc.setContactId(lstContacts[0].id);
        Database.LeadConvertResult lcr = Database.convertLead(lc);
        System.debug('ZZZTC-->Lead Con Res-->is Success??-->'+lcr.isSuccess());
        System.debug('ZZZTC-->Lead Con Res-->accId-->'+JSON.serialize(lcr.getAccountId()));
        
        ldList = [select id,IsConverted,ConvertedContactId,ConvertedAccountId,Email,Send_Portal_Login_Link__c from lead where Id = :ldList[0].id];
        
        
        Datetime tomorrow  = Datetime.now().addDays(1);
        Test.setCreatedDate(lcr.getAccountId(), tomorrow);
        
        Test.startTest();
        OB_LeadTriggerHandler.Execute_AU(ldList, new Map<Id,Lead>{ ldList[0].Id => ldList[0]});
        OB_LeadTriggerHandler.createCP(null,null);
        OB_LeadTriggerHandler.createSrsForExistingContact(ldList);
        OB_LeadTriggerHandler.createNonFinancialSRForExstConHelper(new map<id,account>{lstContacts[0].id => lstNewAccount[0]});
        
        try{
            database.leadConvert lc2 = new database.leadConvert();
            lc2.setLeadId(ldList2[0].id);
            
            leadStatus convertStatus2 = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc2.setConvertedStatus(convertStatus2.MasterLabel);
            lc2.setAccountId(lstNewAccount[0].id);
            lc2.setContactId(lstContacts[0].id);
            Database.LeadConvertResult lcr2 = Database.convertLead(lc2);
            OB_LeadTriggerHandler.Execute_AU(ldList2, new Map<Id,Lead>{ ldList2[0].Id => ldList2[0]});
        }catch(exception e){
            
        }
        
        
        
        
        test.stopTest();
    }
}