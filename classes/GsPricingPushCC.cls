/*
 --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    23-08-2017  Sravan          Added code to handle the barcode creation on the CoverPage Tkt # 4038
    
*/    
public without sharing class GsPricingPushCC {
    
    public static string ReverseSRPriceItems(Step__c objStep){
        list<SR_Price_Item__c> lstItems = new list<SR_Price_Item__c>();
        for(SR_Price_Item__c objItem : [select Id,Status__c from SR_Price_Item__c where ServiceRequest__c =: objStep.SR__c]){
            objItem.Status__c = 'Refunded';
            lstItems.add(objItem);
        }
        if(!lstItems.isEmpty())
            update lstItems;
        return 'Success';
    }
    
    
    public static string ReverseSRPriceVisaPrice(Step__c objStep){
        list<SR_Price_Item__c> lstItems = new list<SR_Price_Item__c>();
        for(SR_Price_Item__c objItem : [select Id,Status__c from SR_Price_Item__c where ServiceRequest__c =: objStep.SR__c and SRPriceLine_Text__c = 'Visa Exception - 3 Years' ]){
            objItem.Status__c = 'Refunded';
            lstItems.add(objItem);
        }
        if(!lstItems.isEmpty())
            update lstItems;
        
         GsPricingPushCC.PushToSAP(objStep.SR__c,objStep.Id);
        
        return 'Success';
    }
    
    
    
    
    
    public static string SendServiceToSAP(Step__c objStep){
        list<SR_Price_Item__c> lstItems = new list<SR_Price_Item__c>();
        for(SR_Price_Item__c objItem : [select Id,Status__c from SR_Price_Item__c where Status__c != 'Invoiced' AND Status__c != 'Utilized' AND ServiceRequest__c=:objStep.SR__c AND Price__c > 0]){
            lstItems.add(new SR_Price_Item__c(Id=objItem.Id,Status__c = 'Consumed'));
        }
        if(!lstItems.isEmpty())
            update lstItems;
        
        GsPricingPushCC.PushToSAP(objStep.SR__c,objStep.Id);
        // pushing Service Request to SAP
        //SAPWebServiceDetails.ECCServiceCall(new list<string>{SRId},true);
        return 'Success';
    }
    
    @future(callout = true)
    public static void PushToSAP(String SRId,string StepId){
        PushToSAPNormal(SRId, StepId);
    }
    
    //added by Ravi on 24th May, New Establishment card wont have any COntact BP to push the price to SAP
    public static GsSapWebServiceDetails.ServiceResponseResult PushPSAToSAP(String SRId){
        list<SR_Price_Item__c> lstSRItems = new list<SR_Price_Item__c>();
        list<Log__c> lstLogs = new list<Log__c>();
        list<SAPECCWebService.ZSF_S_RECE_SERV> lstPSADs = new list<SAPECCWebService.ZSF_S_RECE_SERV>();
        for(SR_Price_Item__c objSRItem : [select Id,Company_Code__c,ServiceRequest__r.Customer__r.BP_No__c,VAT_Amount__c,VAT_Description__c,Pricing_Line__r.TAX_CODE__c,Tax_Rate__c,Price__c,Name,Pricing_Line__r.Product__r.Name from SR_Price_Item__c where ServiceRequest__c=:SRId AND Pricing_Line__r.Product__r.Name = 'PSA' AND (Status__c='Blocked' OR Status__c='Consumed')]){
            lstPSADs.add(GsSapWebServiceDetails.PreparePSALine(new Step__c(),objSRItem,objSRItem.ServiceRequest__r.Customer__r.BP_No__c));
        }
        GsSapWebServiceDetails.ServiceResponseResult objPSAResult = new GsSapWebServiceDetails.ServiceResponseResult();
        if(!lstPSADs.isEmpty()){
            objPSAResult = GsSapWebServiceDetails.PushPSA(lstPSADs);
        }
        return objPSAResult;
    }
    
    public static void PushToSAPNormal(String SRId,string StepId){
        
        list<SR_Price_Item__c> lstSRItems = new list<SR_Price_Item__c>();
        list<Service_Request__c> lstSRs = new list<Service_Request__c>();
        list<Account> lstAccounts = new list<Account>();
        list<Step__c> lstSteps = new list<Step__c>();
        list<Log__c> lstLogs = new list<Log__c>();
        
        map<string,string> mapBPs = new map<string,string>();
        for(Service_Request__c objSR : [select Id,Contact__c,Contact__r.BP_No__c from Service_Request__c where Id=:SRId AND Contact__c != null])
            mapBPs.put(objSR.Id,objSR.Contact__r.BP_No__c);
        
        GsSapWebServiceDetails.StepId = StepId;
        for(Step__c objStep : [select Id,Verified_By__r.SAP_User_Id__c from Step__c where Id=:StepId ]){
            GsSapWebServiceDetails.VerifiedById = objStep.Verified_By__r.SAP_User_Id__c;
        }
        GsSapWebServiceDetails.ServiceResponseResult objServiceResponseResult = GsSapWebServiceDetails.PushServiceToSAP(SRId, mapBPs);
            
        if(objServiceResponseResult.PriceItems != null && !objServiceResponseResult.PriceItems.isEmpty())
            lstSRItems.addAll(objServiceResponseResult.PriceItems);
        if(objServiceResponseResult.Logs != null && !objServiceResponseResult.Logs.isEmpty())
            lstLogs.addAll(objServiceResponseResult.Logs);
        if(objServiceResponseResult.ServiceRequests != null && !objServiceResponseResult.ServiceRequests.isEmpty())
            lstSRs.addAll(objServiceResponseResult.ServiceRequests);
        if(objServiceResponseResult.Accounts != null && !objServiceResponseResult.Accounts.isEmpty())
            lstAccounts.addAll(objServiceResponseResult.Accounts);
        if(objServiceResponseResult.Steps != null && !objServiceResponseResult.Steps.isEmpty())
            lstSteps.addAll(objServiceResponseResult.Steps);
        
        if(SRId != null && SRId != ''){
            Boolean IsNewIndexCard = false;
            for(Service_Request__c objSR : [select Id from Service_Request__c where Record_Type_Name__c = 'New_Index_Card' AND Id=:SRId]){
                IsNewIndexCard = true;
            }
            if(IsNewIndexCard){
                GsSapWebServiceDetails.ServiceResponseResult objPSAResult = PushPSAToSAP(SRId);
                if(objPSAResult.PriceItems != null && !objPSAResult.PriceItems.isEmpty())
                    lstSRItems.addAll(objPSAResult.PriceItems);
                if(objPSAResult.Logs != null && !objPSAResult.Logs.isEmpty())
                    lstLogs.addAll(objPSAResult.Logs);
            }
        }
        Savepoint sp = Database.setSavepoint();         
        try{
            if(!lstSRItems.isEmpty())  update lstSRItems;
            if(!lstLogs.isEmpty())     insert lstLogs;
            if(!lstSRs.isEmpty())      update lstSRs;
            //if(!lstAccounts.isEmpty()) update lstAccounts;
            if(objServiceResponseResult.StepsToUpdate != null && !objServiceResponseResult.StepsToUpdate.isEmpty())    update objServiceResponseResult.StepsToUpdate;
            if(!lstSteps.isEmpty())   upsert lstSteps Step_Id__c; //insert lstSteps;
            if(objServiceResponseResult.SRDoc != null){
                upsert objServiceResponseResult.SRDoc Unique_SR_Doc__c;//insert objServiceResponseResult.SRDoc;
                if(objServiceResponseResult.BarCodeImage != null){
                    objServiceResponseResult.BarCodeImage.ParentId = objServiceResponseResult.SRDoc.Id;
                    insert objServiceResponseResult.BarCodeImage;
                    if(objServiceResponseResult.BarCodeImage.id !=null)  // V1.0
                        CC_GSCodeCls.CopyBarcodeDetails(objServiceResponseResult.SRDoc.Id,objServiceResponseResult.BarCodeImage.id);
                }
            }
        }catch(Exception ex){
            system.debug('Exception is : $$$ '+ex.getMessage());
            Log__c objLog = new Log__c();
            objLog.Type__c = 'GS Service Push in SFDC to SAP. SR Id : '+SRId;
            objLog.Description__c = ex.getMessage()+' Line # '+ex.getLineNumber();
            insert objLog;
        }
    }
    
}