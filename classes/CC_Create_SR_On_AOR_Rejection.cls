/*
    Author      : Durga Prasad
    Date        : 17-Nov-2019
    Description : Custom code to create In_Principle / AOR SR on rejection on AOR
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_Create_SR_On_AOR_Rejection implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        try{
            if(!System.IsBatch() && !System.isFuture()){
                if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c!=null){
                    OB_CustomCodeHelper.CreateSR(stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c,'In_Principle');
                    HexaBPM__Service_Request__c objInPrincipleSR = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c);
                    for(HexaBPM__SR_Status__c SRStatus:[Select Id from HexaBPM__SR_Status__c where HexaBPM__Code__c='AOR_REJECTED' limit 1]){
                        objInPrincipleSR.HexaBPM__Internal_SR_Status__c = SRStatus.Id;
                        objInPrincipleSR.HexaBPM__External_SR_Status__c = SRStatus.Id;
                    }
                    if(objInPrincipleSR.HexaBPM__Internal_SR_Status__c!=null && objInPrincipleSR.HexaBPM__External_SR_Status__c!=null){
                        update objInPrincipleSR;
                    }
                }else if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c==null){
                    OB_CustomCodeHelper.CreateSR(stp.HexaBPM__SR__c,stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c);
                }
            }
        }catch(Exception e){
            strResult = e.getMessage()+'';
        }
        return strResult;
    }
}