/*********************************************************************************************************************
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    30-Dec-2019 Arun        Created base class for VF Renewal_Dependent_multiple_Visa and New_Dependent_multiple_Visa_Request
 v1.2    13-Feb-2020 selva       Updated with detail page edit method - Renewal_Dependent_multiple_Visa_Detail
**********************************************************************************************************************/


public  class Cls_New_Dependent_multiple_Visa_Request {

    string RecordTypeId;
    @testVisible
    string RecordTypeName;
    map<string,string> mapParameters;
    public Account ObjAccount{get;set;}
    public Service_Request__c SRData{get;set;}

    public Boolean ReadOnly{get;set;}
    
    // public Attachment CopyOfSponsorVisa{get;set;}
    // public Attachment SponsorPassport{get;set;}
    // public Attachment DEWABill{get;set;}
     //public Attachment TenancyContract{get;set;}
     
     public string ModeType{get;set;}
     
    // public Attachment ImmigrationSticker{get;set;}
   
    public SR_Template__c SR_Template{get;set;}

//SponsorId_hidden__c
    public Cls_New_Dependent_multiple_Visa_Request(ApexPages.StandardController controller) 
    {
     
        ReadOnly=false;
        ObjAccount=new account();
        ModeType='Multiple';
    // CopyOfSponsorVisa=new Attachment();
     //SponsorPassport=new Attachment();
     //DEWABill=new Attachment();
     //TenancyContract=new Attachment();
     //ImmigrationSticker=new Attachment();
      
        List<String> fields = new List<String> {'External_Status_Name__c','RecordType.Name'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
      
        SRData=(Service_Request__c )controller.getRecord();
        
       
        mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();              
       if(mapParameters.get('RecordType')!=null) 
       {
        
            RecordTypeId= mapParameters.get('RecordType');    
            RecordtypeName= [SELECT Id,DeveloperName  FROM RecordType   WHERE Id=:RecordTypeId AND sObjectType = 'Service_Request__c' limit 1].DeveloperName;
            
        
                   for(SR_Template__c ObjTemp:[select SR_Document_Description_Text__c,Menutext__c,SR_Description__c from SR_Template__c where SR_RecordType_API_Name__c=:RecordTypeName and Active__c=true])
                    SR_Template=ObjTemp;
       }
       
   
       if(mapParameters.get('type')!=null) 

        RecordTypeName= mapParameters.get('type');

System.debug('==SR_Template=====>'+SR_Template);
   System.debug('==RecordtypeName=====>'+RecordtypeName);
   
   
    Service_Request__c eachRequest = new Service_Request__c();
        if(SRData.id !=null){
            
            eachRequest = [SELECT Id,Customer__c,RecordType.Name,Customer__r.Exempted_from_UBO_Regulations__c,Customer__r.Qualifying_Purpose_Type__c,Customer__r.Qualifying_Type__c FROM Service_Request__c where Id=:SRData.id];
            
             ObjAccount= eachRequest.Customer__r;
             //RemovedSr();//Removed the Linked SR from the list if submitted sapratly 
             ReadOnly=!getDependentList().isEmpty();
             RecordTypeName=eachRequest.RecordType.Name;
        }
        
   
   
   
            for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                             Contact.Account.Registration_License_No__c,Contact.Account.Index_Card__r.Name,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                             Contact.Account.Qualifying_Type__c,Contact.Account.Qualifying_Purpose_Type__c,Contact.Account.Contact_Details_Provided__c,
                             Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  
                             from User where Id=:userinfo.getUserId()])
            {
              
             if(SRData.id==null)
             {
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                SRData.Expiry_Date__c=objUsr.Contact.Account.Next_Renewal_Date__c;
                ObjAccount= objUsr.Contact.Account;
                
                SRData.License_Number__c=objUsr.Contact.Account.Registration_License_No__c;
                SRData.Establishment_Card_No__c=objUsr.Contact.Account.Index_Card__r.Name;
                
              }
              
             
          
                    
            
                    
            }
           
    }
     public PageReference NextClick() 
     {
         System.debug('==============Start NextClick===========================');
         PageReference pageRef;
         if(ModeType=='Multiple')
         {
            Contact ObjContact=[select id,Salutation,FirstName, LastName,Middle_Names__c,Visa_Number__c,Gender__c,Passport_Expiry_Date__c,Passport_No__c,Birthdate,First_Name_Arabic__c,Last_Name_Arabic__c,
            Middle_Name_Arabic__c,Visa_Expiry_Date__c,Nationality__c,Mother_Full_Name__c,Emirate__c,City__c,Area__c,
            PO_Box_HC__c,Phone,ADDRESS_LINE1__c,ADDRESS_LINE2__c,Country__c,Domicile__c,CITY1__c,OtherPhone,Address_outside_UAE__c,MobilePhone,Work_Phone__c from Contact where id=:SRData.Sponsor__c];

            SRData.Sponsor__c =ObjContact.id;
            
            SRData.Title_Sponsor__c=ObjContact.Salutation;
            SRData.Sponsor_First_Name__c=ObjContact.FirstName;
            SRData.Sponsor_Middle_Name__c=ObjContact.Middle_Names__c;
            SRData.Sponsor_Last_Name__c=ObjContact.LastName;
            
            SRData.Emirate__c=ObjContact.Emirate__c;
            SRData.City__c=ObjContact.City__c;
            SRData.Area__c=ObjContact.Area__c;
            SRData.PO_BOX__c=ObjContact.PO_Box_HC__c;
            SRData.Residence_Phone_No__c=ObjContact.Phone;
            SRData.Building_Name__c=ObjContact.ADDRESS_LINE1__c;
            SRData.Street_Address__c=ObjContact.ADDRESS_LINE2__c;
            SRData.Mobile_Number__c=ObjContact.MobilePhone;
            SRData.Work_Phone__c=ObjContact.Work_Phone__c;
            
            
            SRData.Identical_Business_Domicile__c=ObjContact.Domicile__c;
            SRData.City_Town__c=ObjContact.CITY1__c;
            SRData.Phone_No_Outside_UAE__c=ObjContact.OtherPhone;
            SRData.Address_Details__c=ObjContact.Address_outside_UAE__c;

            SRData.First_Name_Arabic_Sponsor__c=ObjContact.First_Name_Arabic__c;
            SRData.Middle_Name_Arabic_Sponsor__c=ObjContact.Middle_Name_Arabic__c;
            SRData.Last_Name_Arabic_Sponsor__c=ObjContact.Last_Name_Arabic__c;

            SRData.Sponsor_Visa_No__c=ObjContact.Visa_Number__c;
            SRData.Sponsor_Passport_Expiry__c=ObjContact.Passport_Expiry_Date__c;
            SRData.Sponsor_Gender__c=ObjContact.Gender__c;
            SRData.Sponsor_Nationality__c=ObjContact.Nationality__c;
            //SRData.IBAN_Number__c=ObjContact.Title_Sponsor__c;
            SRData.Sponsor_Visa_Expiry_Date__c=ObjContact.Visa_Expiry_Date__c;
            SRData.Sponsor_Passport_No__c=ObjContact.Passport_No__c;
            SRData.Sponsor_Mother_Full_Name__c=ObjContact.Mother_Full_Name__c;
            SRData.Sponsor_Date_of_Birth__c=ObjContact.Birthdate;
 
         }
         else
         {
             /*
            pageRef=new  PageReference('/a1I/e?autofill=autofill&nooverride=1&RecordType=0122000000034tCAAQ&retURL=%2Fcustomers%2Fhome&service_request_title=New+Dependent+Visa+Package&SponsorId='+SRData.Sponsor__c+'&type=Non_DIFC_Sponsorship_Visa_New');
            pageRef.setRedirect(true);
             */
         }
            
        System.debug('==============End NextClick===========================');
         return pageRef;
     }
     public PageReference SavedSR() 
     { 
     
      try
     {
     // Add the account to the database. 
        upsert SRData;
        
             
            //if files updated please show detail page or show file upload page 
            if(uploadFiles())
            {
                // Send the user to the detail page for the new account.
                PageReference acctPage = new ApexPages.StandardController(SRData).view();
                acctPage.setRedirect(true);
                return acctPage;
                
            }
            else
            {
                //PageReference pageRef =  Page.DocumentViewer;
                //pageRef.getParameters().put('id',SRData.id);
                //pageRef.getParameters().put('retURLval','apex/New_Dependent_multiple_Visa_Request?id='+SRData.id);
                //pageRef.setRedirect(true);
                return MoveToUploadDocuments();
                
            }
        
        
           }
        catch(Exception e) {
        
        ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getMessage() );
        ApexPages.addMessage(msg);
        
        

    //        String S1 = e.getMessage();
//            S1 = S1.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ' , ': [');
  //          Trigger.New[0].adderror(S1);
            
   // System.debug('The following exception has occurred: ' + e.getMessage());
            }
                return null;
         
     }


     public PageReference submitSR() 
     { 
      try
     {
        
    //Check is documents Updates ?
                if(DocumentUploadCheck())
                {
                    // Add the account to the database. 
                        upsert SRData;
                    //Copy SR's Prices  
                        priceAggregator();
                        
                        // Send the user to the detail page for the new account.
                       PageReference pageRef =  Page.SRSubmissionPage;
                       // pageRef.getParameters().put('id',SRData.id);
                        pageRef.getParameters().put('id',SRData.id);
                        pageRef.setRedirect(true);
                        return pageRef;
                        
                        
                    
                }
             
     
           }
        catch(Exception e) {
        
        ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
        ApexPages.addMessage(msg);

            blankFiles();
            
            }
         
           return null;
     }


     public PageReference AddNewDependent() 
     { 
      try
     {

         //if(DocumentUploadCheck())
          {
           // Add the account to the database. 
            upsert SRData;
            // Send the user to the detail page for the new account.

              
              //if files provide show screen to add dependent or show file upload page and then Add dependent page 
              
                if(!uploadFiles())
                {
                   return MoveToUploadDocuments_openDep();
                   
                }
                else
                {
                    
                    return  MoveToAddDependent();
                }
            
            }
        
           }
        catch(Exception e) {
        
        ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
        ApexPages.addMessage(msg);
                blankFiles();
            }
          return null;
         
     }
     
     public PageReference MoveToAddDependent() 
     {
         
      System.debug('=============MoveToAddDependent================');
                    
                    PageReference pageRef;
                   // pageRef.getParameters().put('id',SRData.id);
                    RecordTypeName=RecordTypeName.toUpperCase();
                    
             System.debug('==RecordTypeName===>'+RecordTypeName);
             
                    
                    
                     if(RecordTypeName.containsIgnoreCase('RENEWAL'))
                    {
                     pageRef = Page.Add_Renewal_New_Dependent_Multiple_Visa;
                     Id idRecordId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Non_DIFC_Sponsorship_Visa_Renewal').getRecordTypeId();
                    pageRef.getParameters().put('retURL','/apex/Renewal_Dependent_multiple_Visa?id='+SRData.id);
                    pageRef.getParameters().put('RecordType',idRecordId);
                    pageRef.getParameters().put('type','Non_DIFC_Sponsorship_Visa_Renewal');
                        
                    }
                    else if(RecordTypeName.containsIgnoreCase('CANCELLATION'))
                    {
                     pageRef = Page.Add_Cancellation_Dependent_Multiple_Visa;
                    Id idRecordId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Non_DIFC_Sponsorship_Visa_Cancellation').getRecordTypeId();
                    pageRef.getParameters().put('retURL','/apex/Cancellation_Dependent_multiple_Visa?id='+SRData.id);
                    pageRef.getParameters().put('RecordType',idRecordId);
                    pageRef.getParameters().put('type','Non_DIFC_Sponsorship_Visa_Cancellation');
                    
                    }
                    else if(RecordTypeName.containsIgnoreCase('NEW'))
                    {
                     pageRef = Page.Add_New_Dependent_Multiple_Visa;
                    Id idRecordId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Non_DIFC_Sponsorship_Visa_New').getRecordTypeId();
                    pageRef.getParameters().put('retURL','/apex/New_Dependent_multiple_Visa_Request?id='+SRData.id);
                    pageRef.getParameters().put('RecordType',idRecordId);
                    pageRef.getParameters().put('type','Non_DIFC_Sponsorship_Visa_New');
                    
                    }
                     
                    
                    pageRef.getParameters().put('related_requestId',SRData.id);
                    pageRef.setRedirect(true);
                   return pageRef;
         
     }
      public PageReference MoveToUploadDocuments() 
     {
           PageReference pageRef =  Page.DocumentViewer;
                    pageRef.getParameters().put('id',SRData.id);
                      if(RecordTypeName.containsIgnoreCase('RENEWAL'))
                    {

                        pageRef.getParameters().put('retURLval','apex/Renewal_Dependent_multiple_Visa?id='+SRData.id);
                    }
                    else if(RecordTypeName.containsIgnoreCase('CANCELLATION'))
                    {
                         pageRef.getParameters().put('retURLval','apex/Cancellation_Dependent_multiple_Visa?id='+SRData.id);
                    }
                    else if(RecordTypeName.containsIgnoreCase('NEW'))
                    {
                        pageRef.getParameters().put('retURLval','apex/New_Dependent_multiple_Visa_Request?id='+SRData.id);
                        
                        
                    }
                     
                    pageRef.setRedirect(true);
                    return pageRef;
         
     }
      public PageReference MoveToUploadDocuments_openDep() 
     {
                    PageReference pageRef =  Page.DocumentViewer;
                    pageRef.getParameters().put('id',SRData.id);
                      if(RecordTypeName.containsIgnoreCase('RENEWAL'))
                    {
                        Id idRecordId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Non_DIFC_Sponsorship_Visa_Renewal').getRecordTypeId();
                        pageRef.getParameters().put('retURLval','apex/Add_Renewal_New_Dependent_Multiple_Visa?related_requestId='+SRData.id+'&RecordType='+idRecordId+'&type=Non_DIFC_Sponsorship_Visa_Renewal&retURL=/apex/Renewal_Dependent_multiple_Visa?id='+SRData.id);
                    }
                    else if(RecordTypeName.containsIgnoreCase('CANCELLATION'))
                    {
                        
                        Id idRecordId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Non_DIFC_Sponsorship_Visa_Cancellation').getRecordTypeId();
                        pageRef.getParameters().put('retURLval','apex/Add_Cancellation_Dependent_Multiple_Visa?related_requestId='+SRData.id+'&RecordType='+idRecordId+'&type=Non_DIFC_Sponsorship_Visa_Cancellation&retURL=/apex/Cancellation_Dependent_multiple_Visa?id='+SRData.id);
                        
                    }
                    else if(RecordTypeName.containsIgnoreCase('NEW'))
                    {
                        Id idRecordId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Non_DIFC_Sponsorship_Visa_New').getRecordTypeId();
                        
                        pageRef.getParameters().put('retURLval','apex/Add_New_Dependent_Multiple_Visa?related_requestId='+SRData.id+'&RecordType='+idRecordId+'&type=Non_DIFC_Sponsorship_Visa_New&retURL=/apex/New_Dependent_multiple_Visa_Request?id='+SRData.id);
                    }
                     
                    pageRef.setRedirect(true);
                    return pageRef;
         
     }
     
    
     public boolean uploadFiles()
     {
        
        boolean isFileUpdated=true;
             /*
            List<SR_Doc__c> ListDocumts=new List<SR_Doc__c>(); 
                List<Attachment > Attachments=new List<Attachment >(); 
                for(SR_Doc__c ObjDoc:[select id,Document_Master__r.Code__c from SR_Doc__c where Service_Request__c =:SRData.id])
                {
                    if(ObjDoc.Document_Master__r.Code__c=='DEWA Bill' && DEWABill.Name!=null)
                    {
                        
                        DEWABill.Parentid=ObjDoc.id;
                        Attachments.add(DEWABill);
                        ObjDoc.Status__c = 'Uploaded';
                        ListDocumts.add(ObjDoc);
                    }
                    
                    else if(ObjDoc.Document_Master__r.Code__c=='Tenancy Contract' && TenancyContract.Name!=null)
                    {
                        
                        TenancyContract.Parentid=ObjDoc.id;
                        Attachments.add(TenancyContract);
                        ObjDoc.Status__c = 'Uploaded';
                        ListDocumts.add(ObjDoc);
                    }
                    
                    else if(ObjDoc.Document_Master__r.Code__c=='Sponsor Passport' && SponsorPassport.Name!=null)
                    {
                        
                        SponsorPassport.Parentid=ObjDoc.id;
                        Attachments.add(SponsorPassport);
                        ObjDoc.Status__c = 'Uploaded';
                        ListDocumts.add(ObjDoc);
                    }
                   
                    else if(CopyOfSponsorVisa.Name!=null)
                    {
                              CopyOfSponsorVisa.Parentid=ObjDoc.id;
                        Attachments.add(CopyOfSponsorVisa);
                        ObjDoc.Status__c = 'Uploaded';
                        ListDocumts.add(ObjDoc);
                    }
                    
                   
                    
                }
                
                if(!ListDocumts.isEmpty())
                    upsert ListDocumts;
                if(!Attachments.IsEmpty())
                    upsert Attachments;
                
                */
                
                 if(![select id,Document_Master__r.Code__c from SR_Doc__c where Service_Request__c=:SRData.id and Doc_ID__c=null].isempty())
                {
                   // blankFiles();
                    ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, 'Please upload Required files.');
                    ApexPages.addMessage(msg);
                    isFileUpdated=false;
                }
            
            return isFileUpdated;
                
     }
     public boolean DocumentUploadCheck()
     {
         
             list<SR_Doc__c> LstSRDoc = [select Service_Request_Name__c,Service_Request__r.Name,id,name from SR_Doc__c where Service_Request__r.Linked_SR__c=:SRData.id and Is_Not_Required__c=false and Received_Physically__c=false and Doc_ID__c=null];
             
             if(!LstSRDoc.isEmpty())
             {
                 for(SR_Doc__c ObjDoc:LstSRDoc)
                 {
                    ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, 'Please upload '+ObjDoc.Service_Request__r.Name+' '+ObjDoc.Name);
                    ApexPages.addMessage(msg);   
                 }
                 
        
             }
             else
                  return true;
             
         return false;
         
     }
     
     public void priceAggregator()
     {
         Delete [select id,Price__c,ServiceRequest__c,Product__c,Status__c,Pricing_Line__c from SR_Price_Item__c where Status__c='Added' and  ServiceRequest__c=:SRData.id];
         
         List<SR_Price_Item__c> ListLinkedPrices=new List<SR_Price_Item__c>();
         for(SR_Price_Item__c ObjPrice:[select id,Price__c,ServiceRequest__c,Product__c,Status__c,Pricing_Line__c from SR_Price_Item__c where Status__c='Added' and  ServiceRequest__r.Linked_SR__c=:SRData.id])
         {
                SR_Price_Item__c New_Price_Item = ObjPrice.clone();
                New_Price_Item.ServiceRequest__c =SRData.id;
                New_Price_Item.Linked_SR_Price_Item__c =ObjPrice.id;
                ListLinkedPrices.add(New_Price_Item);
         }
         insert ListLinkedPrices;
         
     }
     public void RemovedSr()
     {
         /*
         if(SRData.id!=null)
         {
             List<Service_Request__c> ListSer=new List<Service_Request__c>();
             
             for(Service_Request__c ObSr:[select id,Name,First_Name__c,Last_Name__c,Relation__c,External_Status_Name__c from Service_Request__c where Linked_SR__c!=null and  Linked_SR__c=:SRData.id])
             {
                 if(SRData.External_Status_Name__c!=ObSr.External_Status_Name__c)
                 {
                     ObSr.Linked_SR__c=null;
                     ListSer.add(ObSr);
                 }
                 
             }
             //if(!ListSer.isEmpty())
               //  update ListSer;
             
         }
         */
         
     }
  
     public void blankFiles ()
     {
     //CopyOfSponsorVisa=new Attachment();
     //SponsorPassport=new Attachment();
     //DEWABill=new Attachment();
     //TenancyContract=new Attachment();
     
     }
     public List<Service_Request__c> getDependentList()
     {
          
         return [select id,Name,First_Name__c,Last_Name__c,Relation__c,External_Status_Name__c from Service_Request__c where Linked_SR__c!=null and  Linked_SR__c=:SRData.id];
         
     }
     public List<SelectOption> getSponsorList()
     {
        
         List<SelectOption> options = new List<SelectOption>();
          options.add(new SelectOption('','--None--'));
        //string sQuery = 'select Id,Name,Is_Violator__c,Is_Absconder__c,Visa_Expiry_Date__c,Active_Employee__c from Contact where AccountId=\''+ObjAccount.id+'\' and Active_Employee__c=true and RecordType.DeveloperName = \'GS_Contact\' order by Name ';
        
        for(Relationship__c contSpn : [select id,Object_Contact__c,Relationship_Name__c from Relationship__c where Subject_Account__c=:ObjAccount.id and Object_Contact__c!=null and  (Relationship_Type__c ='Has DIFC Sponsored Employee' OR  Relationship_Type__c = 'HAS DIFC Seconded Sponsored Employee') and Active__c=true])
        {
            string Status='';
            /*
            if(contSpn.Is_Violator__c)
               Status = 'Violator';
            else if(contSpn.Visa_Expiry_Date__c!=null)
                Status= contSpn.Active_Employee__c?'Active':'Expired'; 
            else 
               Status = 'Not Available';
            */
             options.add(new SelectOption(contSpn.Object_Contact__c,contSpn.Relationship_Name__c));

           
            
        }
        
         return options;

         
    }
    
       public PageReference DeleteSR()
     {
         PageReference pageRef ;
        //Cancel Child Record before cancel this request .
        
        if(getDependentList().IsEmpty())
        {
            //delete getDependentList();
            delete SRData;
            pageRef = new PageReference('/');
            pageRef.setRedirect(true);
        }
        else
        {
            for(Service_Request__c ObjSr: getDependentList())
            {
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, 'Please Cancel '+ObjSr.Name+' before cancelling this request.');
                    ApexPages.addMessage(msg); 
                    
            }
            
        }
         
        
         return pageRef;


     }
    // Added  v1.2
    public PageReference editSR(){
         PageReference pageRef ;
         pageRef = new PageReference('/apex/Renewal_Dependent_multiple_Visa?id='+SRData.id+'&RecordType=0123N0000003LwCQAU&RecordTypeName=Multiple_Renewal_of_Dependent_Visa');
         pageRef.setRedirect(true);
         return pageRef;
    }

}