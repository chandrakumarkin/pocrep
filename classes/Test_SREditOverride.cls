@isTest
private class Test_SREditOverride {
    
    private static user testuser;
    
    @testSetup static void setTestData() {
        
        Test_FitoutServiceRequestUtils.init();
        Test_FitoutServiceRequestUtils.prepareSetup();
    }

    static testMethod void testPortalUser() {
        // TO DO: implement unit test
        
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.prepareTestUser(true);
        
        User objUser = Test_FitoutServiceRequestUtils.getTestUser();
        
        System.runAs(objUser){
            
            Test.startTest();
            
            SR_Template__c testFitoutTemplate = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Fit - Out Service Request','Fit_Out_Service_Request','Fit-Out & Events');
            
            insert testFitoutTemplate;
            
            Service_Request__c objSR1 = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
            
            objSr1.SR_Template__c = testFitoutTemplate.Id;
            objSR1.Type_of_Request__c = 'Type B';
            objSR1.customer__c = objUser.AccountId;
            objSR1.SR_Group__c = 'Fit-Out & Events';
            objSR1.contractor__c= objUser.AccountId;
            objSR1.Building__c = null;
            objSr1.Summary__c = 'Fit_Out_Induction_Request';
            objSr1.Pre_GoLive__c = false;
            
            Test_CC_FitOutCustomCode_DataFactory.setToDraft(objSR1);
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(objSR1,'Fit_Out_Service_Request'); //Set the record type to 'Request Event Contractor Access;
            
            insert objSR1;
            
            Test.setCurrentPage(Page.SREditOverride);
            
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR1);
            
            SREditOverride editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // send the first request
            
            Test_CC_FitOutCustomCode_DataFactory.setToSubmitted(objSR1);
            
            update objSR1;
            
            /* Simulate a submitted view */
            
            Test.setCurrentPage(Page.SREditOverride);
            
            con = new Apexpages.Standardcontroller(objSR1);
            
            editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // simulate an edit with a submitted SR
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestStatus(objSR1,'Project Completed',true);
            
            update objSR1;
            
            /* Simulate a completed view */
            
            con = new Apexpages.Standardcontroller(objSR1);
            
            editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // simulate an edit with a submitted SR
            
            objSr1.Project_On_hold__c = true;
            
            update objSR1;
            
            /* SImulate a project on hold view */
            
            con = new Apexpages.Standardcontroller(objSR1);
            
            editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // simulate an edit with a submitted SR
            
            /* Simulate a project on hold due to cancellation view */
            
            objSr1.Project_On_hold__c = false;
            objSr1.Project_On_hold_due_to_cancellation__c = true;
            
            update objSR1;
            
            con = new Apexpages.Standardcontroller(objSR1);
            
            editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // simulate an edit with a submitted SR
            
            /* Change record type to Fit_Out_Induction_Request */
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(objSR1,'Fit_Out_Induction_Request');
            objSr1.Project_On_hold__c = false;
            objSr1.Project_On_hold_due_to_cancellation__c = false;
            
            update objSR1;
            
            con = new Apexpages.Standardcontroller(objSR1);
            
            editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // simulate an edit with a submitted SR
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(objSR1,'GS_Comfort_Letters');
            objSr1.Project_On_hold__c = false;
            objSr1.Project_On_hold_due_to_cancellation__c = false;
            
            update objSR1;
            
            con = new Apexpages.Standardcontroller(objSR1);
            
            editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // simulate an edit with a submitted SR
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(objSR1,'Consulate_or_Embassy_Letter');
            objSr1.Project_On_hold__c = false;
            objSr1.Project_On_hold_due_to_cancellation__c = false;
            
            update objSR1;
            
            con = new Apexpages.Standardcontroller(objSR1);
            
            editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // simulate an edit with a submitted SR
            
            System.debug('The SR: ' + editCls.ServiceRequest);
            System.debug('BlockUser? : ' + editCls.BlockUser);
            System.debug('Completed? : ' + editCls.foSRCompleted);
            System.debug('On Hold? : ' + editCls.foOnHold);
            System.debug('RetUrl: ' + editCls.ReturnURL);
            System.debug('Message: ' + editCls.errorMsg);
            
            //objSR1.recordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Service_Request__c' AND DeveloperName = 'GS_Comfort_Letters'].Id;
            objSR1.Submitted_Date__c = System.Today();
            
            update objSR1;
            /*
            con = new Apexpages.Standardcontroller(objSR1);
            editCls = new SREditOverride(con);
            editCls.VerifyRequest();
            
            objSR1.SR_Group__c = 'GS';
            objSR1.Would_you_like_to_opt_our_free_couri__c = 'false';
            update objSR1;
            
            con = new Apexpages.Standardcontroller(objSR1);
            editCls = new SREditOverride(con);
            //editCls.VerifyRequest();
            */
            Test.stopTest();
            
        }
    }
    
    static testmethod void testDifcFitOut(){
        
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.setInternalUser(true);
        Test_FitoutServiceRequestUtils.setFitOutInternalUser();
        Test_FitoutServiceRequestUtils.prepareTestUser(false);
        
        testuser = Test_FitoutServiceRequestUtils.getTestUser();
        
        Test.startTest();
            try{
            createTestSR();
            }catch(Exception ex){}
        Test.stopTest();
        
    }
    
    @future
    private static void createTestSR(){
        
        
        User objUsr = testuser;
        
        SR_Template__c testFitoutTemplate = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Request Clarification Form','Request_for_Technical_Clarification_Form','Fit-Out & Events');
            
        insert testFitoutTemplate;
        
        Service_Request__c objSR1 = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        
        objSr1.SR_Template__c = testFitoutTemplate.Id;
        objSR1.SR_Group__c = 'Fit-Out & Events';
        objSR1.Building__c = null;
        objSr1.Summary__c = 'Test';
        objSr1.Pre_GoLive__c = false;
        
        Test_CC_FitOutCustomCode_DataFactory.setToDraft(objSR1);
        
        Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(objSR1,'Request_for_Technical_Clarification_Form'); //Set the record type to 'Request Event Contractor Access;
        
        insert objSR1;
        
        Test_CC_FitOutCustomCode_DataFactory.setToSubmitted(objSR1);
        
        update objSR1;
        
        objSr1.Trade_License_No__c = '12345678';
        objSr1.Name_of_the_Authority__c = 'Government of Dubai';
        
        System.runAs(objUsr){
            
            Test.setCurrentPage(Page.SREditOverride);
            
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR1);
            
            SREditOverride editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // send the first request
            
        }
        
        /* Create another user */
        
        /* Create the new contractor */
        Account newAccount = CC_cls_FitOutandEventCustomCode.createContractorAccount(objSR1,false);
        /* Create a new Contact */
        Contact newContractorContact = CC_cls_FitOutandEventCustomCode.createContractorContact(objSR1,newAccount.id,false);
        
        /* Create a new user */
        User contractorUser = new User();
        
        contractorUser.Username = CC_cls_FitOutandEventCustomCode.createUserName(objSR1.Email_Address__c);
        contractorUser.ContactId = newContractorContact.Id;
        contractorUser.CompanyName = newAccount.id;
        contractorUser.Profileid = Label.Contractor_Profile_Id; 
        contractorUser.Emailencodingkey='UTF-8';
        contractorUser.Languagelocalekey='en_US';
        contractorUser.Localesidkey='en_GB';
        contractorUser.Timezonesidkey='Asia/Dubai';
        contractorUser.Community_User_Role__c = 'Fit-Out Services';
        contractorUser.IsClient__c = false;
        contractorUser.Email=objSR1.Email_Address__c;

        /* Set the conditions */

        /* Set the varying values */
        String firstName = objSR1.First_Name__c;
        String lastName = objSR1.Last_Name__c;
        String phoheNumber = objSR1.Mobile_Number__c;
        String communityNickName = lastName + string.valueof(Math.random()).substring(4,9);
        String alias = lastName.substring(0,1) + string.valueof(Math.random()).substring(4,9);            
        String title = objSR1.Middle_Name__c;

        contractorUser.Firstname = firstName;
        contractorUser.Lastname = lastName;
            
        contractorUser.Phone = phoheNumber;
        contractorUser.CommunityNickname = communityNickName;
        contractorUser.Alias = alias;
        contractorUser.Title = title;
        
        //Mail alert Coding with temporary password
        insert contractorUser; 
        
        System.runAs(contractorUser){
            
            Test.setCurrentPage(Page.SREditOverride);
            
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR1);
            
            SREditOverride editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // send the first request
            
        }
        
        objSR1.Project_on_hold__c = true;
        update objSR1;
        
        System.runAs(contractorUser){
            
            Test.setCurrentPage(Page.SREditOverride);
            
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR1);
            
            SREditOverride editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // send the first request
            
        }
        
        objSR1.Project_on_hold__c = false;
        
        Sr_status__c approvedStatus = [SELECT Id, Type__c from sr_status__c where Name = 'Approved'];
        approvedSTatus.Type__c = 'End';
        
        update approvedSTatus;
        
        Test_CC_FitOutCustomCode_DataFactory.setServiceRequestStatus(objSR1,'Approved',false);
        
        update objSR1;
        
        System.assertEquals('End',approvedSTatus.Type__c);
        
        System.runAs(contractorUser){
            
            Test.setCurrentPage(Page.SREditOverride);
            
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR1);
            
            SREditOverride editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // send the first request
            
        }
        
        objSR1.Project_On_Hold_due_to_Cancellation__c = true;
        update objSR1;
        
        System.runAs(contractorUser){
            
            Test.setCurrentPage(Page.SREditOverride);
            
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR1);
            
            SREditOverride editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // send the first request
            
        }
        
    }
    
    static testMethod void RORPTest() {
        
       Test.startTest();

        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration','Mortgage_Registration','Discharge_Mortgage','Mortgage_Variation','RORP_Account','RORP_Contact','Mortgage_Unit','Individual_Mortgager','Company_Mortgager')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'RORP Customer';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        objAccount.RecordTypeId = mapRecordType.get('RORP_Account');
        objAccount.Issuing_Authority__c = 'DMCC';
        objAccount.RORP_License_No__c = '1234567';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.RecordTypeId = mapRecordType.get('Mortgage_Variation');
        objSR.Customer__c = objAccount.Id;
        insert objSR;
        
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            
        SREditOverride editCls = new SREditOverride(con);
        
        editCls.VerifyRequest();
        Test.stopTest();   
    }
    
    static testMethod void testPortalUser12() {
        // TO DO: implement unit test
        
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.prepareTestUser(true);
        
        User objUser = Test_FitoutServiceRequestUtils.getTestUser();
        
        System.runAs(objUser){
            
            Test.startTest();
            
            SR_Template__c testFitoutTemplate = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Fit - Out Service Request','Fit_Out_Service_Request','Fit-Out & Events');
            
            insert testFitoutTemplate;
            
            Service_Request__c objSR1 = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
            
            objSr1.SR_Template__c = testFitoutTemplate.Id;
            objSR1.Type_of_Request__c = 'Type B';
            objSR1.customer__c = objUser.AccountId;
            objSR1.SR_Group__c = 'Fit-Out & Events';
            objSR1.contractor__c= objUser.AccountId;
            objSR1.Building__c = null;
            objSr1.Summary__c = 'Fit_Out_Induction_Request';
            objSr1.Pre_GoLive__c = false;
            
            Test_CC_FitOutCustomCode_DataFactory.setToDraft(objSR1);
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(objSR1,'GS_Comfort_Letters'); //Set the record type to 'Request Event Contractor Access;
            
            insert objSR1;
            
            Test.setCurrentPage(Page.SREditOverride);
            
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR1);
            
            SREditOverride editCls = new SREditOverride(con);
         
            editCls = new SREditOverride(con);
            editCls.VerifyRequest();
            
            
            
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(objSR1,'Request_Contractor_Access');
            //objSR1.SR_Group__c = 'GS';
            objSR1.Would_you_like_to_opt_our_free_couri__c = 'false';
            update objSR1;
            
            con = new Apexpages.Standardcontroller(objSR1);
            
            editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // simulate an edit with a submitted SR
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(objSR1,'Mortgage_Registration');
            //objSR1.SR_Group__c = 'GS';
            objSR1.Would_you_like_to_opt_our_free_couri__c = 'false';
            update objSR1;
            
            con = new Apexpages.Standardcontroller(objSR1);
            
            editCls = new SREditOverride(con);
            
            editCls.VerifyRequest(); // simulate an edit with a submitted SR
            
           
            
           
            /*
            con = new Apexpages.Standardcontroller(objSR1);
            editCls = new SREditOverride(con);
            editCls.VerifyRequest();
            */
            Test.stopTest();
        }
    }
    
}