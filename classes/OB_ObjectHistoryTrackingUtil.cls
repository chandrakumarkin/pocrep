/******************************************************************************************
 *  Name        : OB_ObjectHistoryTrackingUtil
 *  Author      : Durga Prasad
 *  Description : Object History Tracking Utility Class  
*******************************************************************************************/
public without sharing class OB_ObjectHistoryTrackingUtil{
    public static sObject CreateObjectHistory(string sObjectType,string NewValue,string OldValue,string ParentId,string FieldLabel,string FieldName){
        sObject objHistory = Schema.getGlobalDescribe().get(sObjectType).newSObject();
        objHistory.put('Parent__c',ParentId);
        objHistory.put('Last_Modified_By__c',userinfo.getuserid());
        objHistory.put('Last_Modified_Date__c',system.now());
        objHistory.put('Modified_Field_Label__c',FieldLabel);
        objHistory.put('Modified_Field_Name__c',FieldName);
        objHistory.put('Old_Value__c',OldValue);
        objHistory.put('New_Value__c',NewValue);
        return objHistory;
    }
}