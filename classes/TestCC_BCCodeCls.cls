/*
    Created By  : Shabbir - DIFC on 12 Sep,2015
    Description : Test class for CC_BCCodeCls 
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 ----------------------------------------------------------------------------------------
*/
@isTest(seealldata=false)
private class TestCC_BCCodeCls {

    public static Account objAccount;
    public static Service_Request__c objSR;
    public static Step__c objStep;
    public static Amendment__c amndRemCon;
    public static Unit__c objUnit;
    public static Lease__c objLease;
    public static Tenancy__c objTen;
    public static Building__c objBuilding;
    public static Amendment__c objAmend;
    
    static void init(){
        objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;      
        
        string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Lease_Application_Request' and sObjectType='Service_Request__c']){
            strRecType = rectyp.Id;
        }    
        
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        if(strRecType!='')
            objSR.RecordTypeId = strRecType;
        insert objSR;
        
        objBuilding = new Building__c();
        objBuilding.Name = 'Gate Building';
        objBuilding.Building_No__c = '1234';
        objBuilding.Company_Code__c = '5300';
        insert objBuilding;
        
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'DIFC Short Term Business Lease';
        insert objUnit;
        
        objAmend = new Amendment__c();
        objAmend.Lease_Unit__c = objUnit.Id;
        objAmend.ServiceRequest__c = objSR.Id;
        insert objAmend; 
        
        objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.End_Date__c = system.today().addYears(1);
        objLease.Type__c = 'Leased';
        objLease.Status__c = 'Active';
        objLease.Lease_Types__c = 'Business Centre Lease';
        insert objLease;  
        
        objTen = new Tenancy__c();
        objTen.Lease__c = objLease.Id;
        objTen.End_Date__c = system.today().addYears(1);
        objTen.Unit__c = objUnit.Id;
        insert objTen;   
                        
    }

    /** Test with signage and lease email **/
    static testMethod void testSAPAttr() {

        init();
        system.Test.startTest();
        
        Step__c stp  = new Step__c();
        stp.SR__c = objSR.Id;
        insert stp;        
        CC_BCCodeCls.CreateSAPAttr(stp);
        CC_BCCodeCls.validateUnits(objSR.Id);
        //CC_BCCodeCls.UpdateSAPDate(stp);
        
        Amendment__c objAmend2 = new Amendment__c();
        objAmend2.Lease_Unit__c = objUnit.Id;
        objAmend2.ServiceRequest__c = objSR.Id;
        insert objAmend2; 
        
        Amendment__c objAmend3 = new Amendment__c();
        objAmend3.Lease_Unit__c = objUnit.Id;
        objAmend3.ServiceRequest__c = objSR.Id;
        insert objAmend3; 
        
        Amendment__c objAmend4 = new Amendment__c();
        objAmend4.Lease_Unit__c = objUnit.Id;
        objAmend4.ServiceRequest__c = objSR.Id;
        insert objAmend4; 
        
        Amendment__c objAmend5 = new Amendment__c();
        objAmend5.Lease_Unit__c = objUnit.Id;
        objAmend5.ServiceRequest__c = objSR.Id;
        objAmend5.Status__c = 'Add';
        objAmend5.Full_Name__c = 'John smith';
        insert objAmend5;         

        CC_BCCodeCls.CreateSAPAttr(stp);
        CC_BCCodeCls.createPTARel(stp);
        
        contact c = new contact();
        c.AccountId = objAccount.Id;
        c.LastName = 'abc';
        c.email = 'alsadeek@difc.ae';
        insert c;
        CC_BCCodeCls.sendSecurityEmailITForm(stp);
        CC_BCCodeCls.UpdateSAPDate(stp);
        system.Test.stopTest();
        
    }  
    
    static testMethod void testException() {

        //init();
        system.Test.startTest();

        objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;      
        
        string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Lease_Application_Request' and sObjectType='Service_Request__c']){
            strRecType = rectyp.Id;
        }    
        
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        if(strRecType!='')
            objSR.RecordTypeId = strRecType;
        insert objSR;
    
        Step__c stp  = new Step__c();
        stp.SR__c = objSR.Id;
        insert stp;        
        CC_BCCodeCls.CreateSAPAttr(stp);
        CC_BCCodeCls.UpdateSAPDate(stp);
        CC_BCCodeCls.createPTARel(stp);
        CC_BCCodeCls.validateUnits(objSR.Id);
        system.Test.stopTest();
        
    }    
    
    
}