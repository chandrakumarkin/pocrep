@isTest
public with sharing class OB_RelatedListContollerTest {
    
    public static testmethod void relatedListTest(){
            
           
    
            // create account
            List<Account> insertNewAccounts = new List<Account>();
            insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
            insert insertNewAccounts;
    
            //create contact
            Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
            insert con; 

             /* create test data here */ 
            Receipt__c ReceiptObj = new Receipt__c(Customer__c = insertNewAccounts[0].Id);
            
            insert ReceiptObj;

            
    
            test.startTest();
            
            
            Id profileToTestWith = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
            
            
            User user = new User(alias = 'test123', email='test123@noemail.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profileToTestWith, country='United States',IsActive =true,
                                 ContactId = con.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user;
            system.runAs(user){
                
                OB_RelatedListContoller.RequestWrap reqWrapper = new OB_RelatedListContoller.RequestWrap();
                OB_RelatedListContoller.RespondWrap repWrapper = new OB_RelatedListContoller.RespondWrap();
                
                reqWrapper.parentRelationFieldname = 'Customer__c';
                reqWrapper.fieldsToQuery = 'Name,Amount__c,Receipt_Type__c,Status__c,Transaction_Date__c';
                reqWrapper.parentRecordId = insertNewAccounts[0].Id;
                reqWrapper.objectToQueryFrom = 'Receipt__c' ;
                reqWrapper.additionalFilters = '';
                String requestString = JSON.serialize(reqWrapper);

                repWrapper = OB_RelatedListContoller.fetchRelatedRecords(requestString);
                
                
            }
            
            
            
        }
}