/*
Manully run in developer conlose 
NewBDKPIs batchSch=new NewBDKPIs ();
String sch='0 0 23 * * ?';
System.schedule('Batch Schedule', sch , batchSch);
Modified : 10-7-2018 added event query #5818
*/


global class NewBDKPIs implements Schedulable 
{
   global void execute(SchedulableContext sc) 
   {
     // UpdateBDKPIs b = new UpdateBDKPIs('select id,(select id,Object_Account__c from Primary_Account__r where Relationship_Type__c=\'Has Affiliates with Same Management\' and Subject_Account__r.Hosting_Company__c!=null  and Active__c=true),Total_Meeting_Count__c,Total_Interaction_Count__c,(select Formula_Activity_Name_Eng__c from License_Activities__r where End_Date__c=null), License_Activity_Details__c,Corporate_Service_Provider__c,(Select Id, RecordType.Name,AccountId From Tasks),(Select id,RecordType.Name,AccountId  From Events) from account where ROC_Status__c=\'Active\' '); 
      UpdateBDKPIs b = new UpdateBDKPIs('select id,(select id,Object_Account__c from Primary_Account__r where Relationship_Type__c=\'Has Affiliates with Same Management\' and Object_Account__c!=null  and Active__c=true),Total_Meeting_Count__c,Total_Interaction_Count__c,(select Formula_Activity_Name_Eng__c from License_Activities__r where End_Date__c=null),Corporate_Service_Provider__c, License_Activity_Details__c from account where ROC_Status__c=\'Active\' or ROC_Status__c=\'Not Renewed\' ',false); 
      database.executebatch(b,1);
   }
}