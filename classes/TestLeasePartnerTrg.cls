/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestLeasePartnerTrg {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        list<Building__c> lstBuilding = new list<Building__c>();
		
		Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP On Plan';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.SAP_Building_No__c = '123456';
        lstBuilding.add(objBuilding);
        insert lstBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c =  lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Residential';
        lstUnits.add(objUnit);
        
        insert lstUnits;
		
		map<string,string> mapRecordType = new map<string,string>();
		for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('')]){
			mapRecordType.put(objRT.DeveloperName,objRT.Id);
		}
		
		Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        list<Lease__c> lstLeases = new list<Lease__c>();
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Future Lease';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.Lease_Types__c = 'Retail Lease';
        lstLeases.add( objLease );
        
        objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.Lease_Types__c = 'Retail Lease';
        lstLeases.add( objLease );
        insert lstLeases;
        
        list<Lease_Partner__c> lstLPs = new list<Lease_Partner__c>();
        
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = lstLeases[0].Id;
        objLP.Account__c = objAccount.Id;
        objLP.Unit__c = lstUnits[0].Id;
        objLP.SAP_PARTNER__c = '12345';
        objLP.Start_date__c = system.today();
        objLP.End_Date__c = system.today().addYears(3);
        objLP.Square_Feet__c = '123456';
        objLP.Lease_Types__c = 'Retail Lease';
        objLP.Industry__c = 'Kiosik';
        objLP.Type_of_Lease__c = 'Leased';
        lstLPs.add(objLP);
        
        objLP = new Lease_Partner__c();
        objLP.Lease__c = lstLeases[1].Id;
        //objLP.Account__c = objAccount.Id;
        objLP.Unit__c = lstUnits[0].Id;
        objLP.SAP_PARTNER__c = '12345';
        objLP.Is_6_Series__c = true;
        objLP.Start_date__c = system.today();
        objLP.End_Date__c = system.today().addYears(3);
        objLP.Square_Feet__c = '123456';
        objLP.Lease_Types__c = 'Retail Lease';
        objLP.Industry__c = 'Kiosik';
        objLP.Type_of_Lease__c = 'Leased';
        lstLPs.add(objLP);
        
        insert lstLPs;
        
        lstLPs[0].Status__c = 'Future Lease';
        update lstLPs[0];
        
        lstLPs[0].Status__c = 'Active';
        update lstLPs[0];
        
    }
}