public class WebServiceCdrAccountBatch implements Database.Batchable<sObject> , Database.Stateful, Database.AllowsCallouts{ 
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator('SELECT id, Unified_License_Number__c, BP_No__c, Active_License__r.name, Active_License__r.License_Issue_Date__c, Active_License__r.License_Type__c, Active_License__r.License_Expiry_Date__c, Active_License__r.License_Terminated_Date__c,'+
                                        'Active_License__r.ROC_Status_List__c, Name, Arabic_Name__c, Authorized_Share_Capital__c, Home_Country__c, Company_Type__c, Trade_Name__c, Trading_Name_Arabic__c, Sectors__c, Issued_Share_Capital_AccShare__c, BD_Sector__c, '+
                                        ' (Select Id, License_Type__c from Licenses__r), (Select Id, Activity__r.Name, Activity__r.Activity_Code__c FROM License_Activities__r)'+
                                        'from Account '+
                                        'Where Last_PR_Push__c >= Last_N_Days:7 ');
    }
    
    
    public void execute(Database.BatchableContext BC, List<Account> scope){
        List<Log__c> ErrorObjLogList = new List<Log__c>();
        JSONGenerator jsonObj = JSON.createGenerator(true);
        jsonObj.writeStartArray();  
        for(Account Acc : scope){   
            jsonObj.writeStartObject();
            jsonObj.writeFieldName('licenseMaster');
            jsonObj.writeStartArray();
            jsonObj.writeStartObject();
            jsonObj.writeFieldName('activityMaster');
            jsonObj.writeStartArray(); 
            for(License_Activity__c ActivityObj :Acc.License_Activities__r){
                jsonObj.writeStartObject();
                if(ActivityObj != null)
                    if(!String.isBlank(ActivityObj.Activity__r.Activity_Code__c))
                    jsonObj.writeStringField('activityCode', ActivityObj.Activity__r.Activity_Code__c);
                if(!String.isBlank(ActivityObj.Activity__r.Name))
                    jsonObj.writeStringField('activityName', ActivityObj.Activity__r.Name);
                jsonObj.writeEndObject();     
            }
            jsonObj.writeEndArray();
            if(!String.isBlank(Acc.Unified_License_Number__c) && Acc.Unified_License_Number__c != null)
                jsonObj.writeStringField('unifiedNumber',Acc.Unified_License_Number__c);
            jsonObj.writeStringField('zoneName', 'DIFC');
            if(Acc.Active_License__r != null && !String.isBlank(Acc.Active_License__r.name))
                jsonObj.writeStringField('licenseNumber', Acc.Active_License__r.name);
            //if(Acc.Active_License__r != null && !String.isBlank(Acc.Active_License__r.License_Type__c))
            jsonObj.writeStringField('licenseType', 'DIFC');
            if(Acc.Active_License__r != null && (Acc.Active_License__r.License_Issue_Date__c != null))
                jsonObj.writeDateTimeField('issuedOn', Acc.Active_License__r.License_Issue_Date__c);
            if(Acc.Active_License__r != null && (Acc.Active_License__r.License_Expiry_Date__c != null))
                jsonObj.writeDateTimeField('expiresOn', Acc.Active_License__r.License_Expiry_Date__c);
            if(Acc.Active_License__r != null && (Acc.Active_License__r.License_Terminated_Date__c != null))
                jsonObj.writeDateTimeField('cancelledOn', Acc.Active_License__r.License_Terminated_Date__c);
            /*else{
jsonObj.writeDateTimeField('cancelledOn', date.valueof('null'));
}*/
            if(Acc.Active_License__r != null && !String.isBlank(Acc.Active_License__r.ROC_Status_List__c))
                jsonObj.writeStringField('licenseStatus', Acc.Active_License__r.ROC_Status_List__c);
            jsonObj.writeEndObject();
            jsonObj.writeEndArray(); 
            if(!String.isBlank(Acc.BP_No__c))
                jsonObj.writeStringField('accountNumber', Acc.BP_No__c);
            if(!String.isBlank(Acc.Name))
                jsonObj.writeStringField('nameEn', Acc.Name);
            if(!String.isBlank(Acc.Arabic_Name__c))
                jsonObj.writeStringField('nameAr', Acc.Arabic_Name__c);
            if(Acc.Active_License__r != null && Acc.Active_License__r.License_Issue_Date__c != null)
                jsonObj.writeDateTimeField('establishedOn', Acc.Active_License__r.License_Issue_Date__c);
            //if(Acc.Authorized_Share_Capital__c != null)
            if(Acc.Issued_Share_Capital_AccShare__c != null)
                jsonObj.writeStringField('capital', String.valueOf(Acc.Issued_Share_Capital_AccShare__c));
            if(!String.isBlank(Acc.Home_Country__c))
                jsonObj.writeStringField('originCountry', Acc.Home_Country__c);
            else{
                jsonObj.writeStringField('originCountry', 'United Arab Emirates');
            }
            jsonObj.writeStringField('zoneName', 'DIFC');
            if(!String.isBlank(Acc.Company_Type__c))
                jsonObj.writeStringField('companyType', Acc.Company_Type__c);
            if(!String.isBlank(Acc.Trade_Name__c))
                jsonObj.writeStringField('tradeNameEn', Acc.Trade_Name__c);
            if(!String.isBlank(Acc.Trading_Name_Arabic__c))
                jsonObj.writeStringField('tradeNameAr', Acc.Trading_Name_Arabic__c);
            jsonObj.writeFieldName('sectors');
            jsonObj.writeStartArray();
            if(!String.isBlank(Acc.BD_Sector__c))
                jsonObj.writeString(Acc.BD_Sector__c);
            else{
                jsonObj.writeString('NA');
            }
            jsonObj.writeEndArray();
            jsonObj.writeEndObject();      
        }      
        jsonObj.writeEndArray(); 
        System.debug('jsonObj:-'+jsonObj.getAsString());
        //Get authrization token 
        String Token = WebServiceCDRGenerateToken.cdrAuthToken();
        //WebService to Create the Account
        HttpRequest request = new HttpRequest();
        request.setEndPoint(System.label.Cdr_Post_Account_Url);
        request.setBody(jsonObj.getAsString());
        request.setTimeout(120000);
        request.setHeader('Accept', 'text/plain');
        request.setHeader('Content-Type', 'application/json-patch+json');
        request.setHeader('Authorization','Bearer'+' '+Token);
        request.setMethod('POST');
        HttpResponse response = new HTTP().send(request);
        System.debug('responseStringresponse:'+response.toString());
        System.debug('STATUS:'+response.getStatus());
        System.debug('STATUS_CODE:'+response.getStatusCode());
        System.debug('strJSONresponse:'+response.getBody());
        request.setTimeout(110000);
        if (response.getStatusCode() == 200)
        {           
            Log__c ErrorobjLog = new Log__c();
            ErrorobjLog.Type__c = 'CDR Account Upload';
            ErrorobjLog.Description__c = 'Request: '+ jsonObj.getAsString() +'Response: '+ response.getBody() ;
            //ErrorobjLog.Account__c = AccId;
            ErrorObjLogList.add(ErrorobjLog);
            
        }
        else
        {
            Log__c ErrorobjLog = new Log__c();
            ErrorobjLog.Type__c = 'CDR Account Upload Error';
            ErrorobjLog.Description__c = 'Request: '+ jsonObj.getAsString() +'Response: '+ response.getBody() ;
            //ErrorobjLog.Account__c = AccId;
            ErrorObjLogList.add(ErrorobjLog);      
        }
        
        Insert ErrorObjLogList;
    }
    
    public void finish(Database.BatchableContext BC){
        
    }
    
}