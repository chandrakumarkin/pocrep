public class BuyCarParkingHoursSubmitController {

    public class RequestWrap {
        @AuraEnabled public string flowID { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public string srId { get; set; }
        public RequestWrap() {
        }
    }
    
    public class RespondWrap {
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public List<HexaBPM__Section_Detail__c> ButtonSection { get; set; }
        @AuraEnabled public Boolean ShowDecAndButton { get; set; }
        @AuraEnabled public string showButton { get; set; }
        @AuraEnabled public boolean isBackendUser { get; set; }
        @AuraEnabled public decimal amountToBePaid { get; set; }
    }
    public class ButtonResponseWrapper {
        @AuraEnabled public String pageActionName { get; set; }
        @AuraEnabled public string communityName { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public string CommunityPageName { get; set; }
        @AuraEnabled public string sitePageName { get; set; }
        @AuraEnabled public string strBaseUrl { get; set; }
        @AuraEnabled public string srId { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public boolean isPublicSite { get; set; }

    }
    @AuraEnabled
    public static RespondWrap fetchSRObjFields(string requestWrapParam) {
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap = new RespondWrap();
        //list<secDetailWrap> secwrpList = new list<secDetailWrap>();
        map<string, boolean> lockingMap = new map<string, boolean>();
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        string ServiceRequestId = reqWrap.srId;
        if(ServiceRequestId != null) {
            for(User usr :[select Contact.accountID, ContactId FROM User Where Id = :userInfo.getUserId()]) {
                respWrap.isBackendUser = (usr.ContactId != null) ? false : true;
            }
            HexaBPM__Service_Request__c objSR = [Select id, Name, HexaBPM__External_Status_Name__c,
                                                  HexaBPM__Record_Type_Name__c, HexaBPM__Customer__c, 
                                                  RecordTypeId, Parking_Hours__c, HexaBPM__SR_Template__c,
                                                  HexaBPM__Internal_Status_Name__c, 
                                                 (Select id, Total_Price_AED__c from HexaBPM__SR_Price_Items1__r limit 1)
                                                  From HexaBPM__Service_Request__c 
                                                  where Id=:ServiceRequestId];
            respWrap.ShowDecAndButton = OB_QueryUtilityClass.reviewPageButtonShowCheck(objSR);
            if(objSR.HexaBPM__SR_Price_Items1__r.size()>0){
                respWrap.amountToBePaid = objSR.HexaBPM__SR_Price_Items1__r[0].Total_Price_AED__c;
            }
            /*
            List<HexaBPM__SR_Template_Item__c> templItemList = [Select id, Name, HexaBPM__SR_Template__c, 
                                                                HexaBPM__Product__c
                                                                from HexaBPM__SR_Template_Item__c where 
                                                               HexaBPM__SR_Template__c = :objSR.HexaBPM__SR_Template__c];
            Set<Id> productIdSet = new Set<Id>();
            if(templItemList.size()>0){
                for(HexaBPM__SR_Template_Item__c tempItem :templItemList){
                    productIdSet.add(tempItem.HexaBPM__Product__c);
                }
                if(productIdSet.size()>0){
                    //respWrap.prodLineItemCond = new HexaBPM__Condition__c();
                    List<HexaBPM__Condition__c> lineitemConditionList = [Select id, HexaBPM__Pricing_Line__c, 
                                                         HexaBPM__Pricing_Line__r.HexaBPM__Product__c
                                                         from HexaBPM__Condition__c where 
                                                         HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c' and 
                                                         HexaBPM__Field_Name__c = 'Parking_Hours__c' and 
                                                         HexaBPM__Value__c =: objSR.Parking_Hours__c and 
                                                         HexaBPM__Pricing_Line__r.HexaBPM__Product__c in :productIdSet limit 1];
                    //respWrap.prodLineItemCond = lineitemCondition!=null? lineitemCondition : new HexaBPM__Condition__c();
                    if(lineitemConditionList.size()>0){
                        List<HexaBPM__Dated_Pricing__c> datedPricList= [Select id, Name, HexaBPM__Pricing_Line__c, 
                                                                        HexaBPM__Pricing_Line__r.HexaBPM__Product__c, 
                                                                        HexaBPM__Unit_Price__c from HexaBPM__Dated_Pricing__c 
                                                                        where HexaBPM__Pricing_Line__r.HexaBPM__Product__c 
                                                                        in:productIdSet and HexaBPM__Pricing_Line__c =: 
                                                                        lineitemConditionList[0].HexaBPM__Pricing_Line__c];
                        respWrap.amountToBePaid = datedPricList.size()>0? String.valueOf(datedPricList[0].HexaBPM__Unit_Price__c) :'';
                    }
                }
            }
			
            List<HexaBPM__Dated_Pricing__c> datedPricList= [Select id, Name, HexaBPM__Pricing_Line__c, 
                                                            HexaBPM__Pricing_Line__r.HexaBPM__Product__c 
                                                            from HexaBPM__Dated_Pricing__c where 
                                                            HexaBPM__Pricing_Line__r.HexaBPM__Product__c in:productIdSet 
                                                           and HexaBPM__Unit_Price__c =: objSR.Parking_Hours__c];*/
            
            respWrap.ButtonSection = new List<HexaBPM__Section_Detail__c>();
			List<Sobject> sobjectListRec= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(reqWrap.srId),'');
            List<HexaBPM__Service_Request__c> srListRec= (List<HexaBPM__Service_Request__c>) sobjectListRec;
            respWrap.srObj = (srListRec != null && srListRec.size()>0) ? srListRec[0] : new HexaBPM__Service_Request__c();
            
            for (HexaBPM__Section_Detail__c secDeatil :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c 
                WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
                AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId]) {
               // respWrap.ButtonSection = secDeatil;
                respWrap.ButtonSection.add(secDeatil);
            }
        }
        return respWrap;
    }
    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string PageId) {

        system.debug('=====PageId========'+PageId);
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
        PageFlowControllerHelper.objSR = objRequest;
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();

        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;

        system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
        return respWrap;
    }
    
    @AuraEnabled
    public static ButtonResponseWrapper1 getButtonAction1(HexaBPM__Service_Request__c srObj, string SRID, string pageId, string flowId) {
       ButtonResponseWrapper1 respWrap = new ButtonResponseWrapper1();

       try {
        ValidateWrapper validateSRL = new ValidateWrapper();
        validateSRL.isValid = true;
        HexaBPM__Service_Request__c objFullSR = OB_QueryUtilityClass.QueryFullSR(SRID);
        list<string> allMsgs = new list<string>();
        map<string, string> MapHiddenPageElemIds = PageFlowControllerHelper.getHiddenPageIdsMap(flowId, objFullSR);
        system.debug('=====MapHiddenPageElemIds======' + MapHiddenPageElemIds);
        map<string, string> MapDisplayPageElemIds = new Map<string, string>();
        respWrap.debugger2 = MapHiddenPageElemIds;
        for(HexaBPM__Page__c pg :[select id, Name, HexaBPM__Render_by_Default__c from HexaBPM__Page__c where ID Not IN :MapHiddenPageElemIds.keyset() AND HexaBPM__Page_Flow__c = :flowId AND HexaBPM__Is_Custom_Component__c = false AND Skip_Review_Validation__c = false]) {
            MapDisplayPageElemIds.put(pg.Id, pg.Name);
        }
        //respWrap.debugger = MapDisplayPageElemIds;
        system.debug('=====objFullSR======' + objFullSR.Page_Tracker__c);
        system.debug('=====MapDisplayPageElemIds======' + MapDisplayPageElemIds);
        if(MapDisplayPageElemIds != null && MapDisplayPageElemIds.size() != 0) {
            for(string ss :MapDisplayPageElemIds.keyset()) {
                if(objFullSR!=null && objFullSR.Page_Tracker__c!=null && !objFullSR.Page_Tracker__c.contains(ss)) {//v1.3
                    validateSRL.isValid = false;
                    allMsgs.add('Please complete "' + MapDisplayPageElemIds.get(ss) + '" \n');
                }
            }
        }
        //respWrap.debugger = allMsgs;
        system.debug(validateSRL.isValid + '========allMsgs==check===' + allMsgs);
        boolean RequiredFieldsMissing = false;
        if(!validateSRL.isValid) {
            validateSRL.validMsg = string.join(allMsgs, '');
            RequiredFieldsMissing = true;
        }
        
        OB_ReviewAndConfirmController.ValidateWrapper CustomPageValidations = OB_ReviewAndConfirmUtils.validateSRLines(SRID);
        system.debug('CustomPageValidations==>' + CustomPageValidations);
        respWrap.debugger = CustomPageValidations.debugger;
        if(CustomPageValidations != null && !CustomPageValidations.isValid) {
            validateSRL.isValid = false;
            if(RequiredFieldsMissing) {
                validateSRL.validMsg = validateSRL.validMsg + '' + CustomPageValidations.validMsg;
            } else {
                validateSRL.validMsg = CustomPageValidations.validMsg;
            }
        }
        system.debug(validateSRL);
        //ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        if(validateSRL.isValid ) {
        
            update srObj;

            //set atm act
            if(objFullSR.HexaBPM__Record_Type_Name__c != null && objFullSR.HexaBPM__Record_Type_Name__c=='Commercial_Permission'
            && objFullSR.Type_of_commercial_permission__c != null && 
            objFullSR.Type_of_commercial_permission__c == 'ATM'){
                OB_ReviewAndConfirmUtils.setATMActivity(objFullSR);
            }

            //v.1.6
            //set prescribe act
            if(objFullSR.HexaBPM__Record_Type_Name__c != null && objFullSR.HexaBPM__Record_Type_Name__c=='In_Principle'
            && objFullSR.Select_Qualified_Purpose__c != null ){
                OB_ReviewAndConfirmUtils.setPrescribedActivity(objFullSR);
            }
            
            //v1.4
            list<HexaBPM__SR_Price_Item__c> srPriceItems = new list<HexaBPM__SR_Price_Item__c>();
            boolean hasPriceItems = false;
            decimal PaymentPrice=0;
            for(HexaBPM__SR_Price_Item__c SRP:[SELECT Id,HexaBPM__Price__c,HexaBPM__Pricing_Line__r.Non_Deductible__c,Pricing_Line_Name__c,HexaBPM__Status__c FROM HexaBPM__SR_Price_Item__c WHERE HexaBPM__ServiceRequest__c = :SRID AND HexaBPM__Status__c = 'Added' and HexaBPM__Price__c>=0]){
              if(!SRP.HexaBPM__Pricing_Line__r.Non_Deductible__c && SRP.Pricing_Line_Name__c!='Reservation of Name (Retail/Non-Retail)' && SRP.HexaBPM__Price__c>=0){
                hasPriceItems = true;
                if(SRP.HexaBPM__Price__c>0){
                  PaymentPrice = PaymentPrice + SRP.HexaBPM__Price__c;
                }else{
                  SRP.HexaBPM__Status__c = 'Blocked';
                  srPriceItems.add(SRP);
                }
              }else{
                SRP.HexaBPM__Status__c = 'Cancelled';
                srPriceItems.add(SRP);
              }
            }
            if(PaymentPrice>0)
              hasPriceItems = true;
            else
              hasPriceItems = false;
              
            respWrap.haserror = false;
            if(hasPriceItems){
                /* respWrap.pageActionName = '/digitalOnboarding/s/topupbalance?pageId=' + pageId + '&flowId=' + flowId + '&srId=' + SRID; */
                respWrap.pageActionName = '/' + System.Label.OB_Site_Prefix + '/s/topupbalance?pageId=' + pageId + '&flowId=' + flowId + '&srId=' + SRID;
            } else {
              if(srPriceItems.size()>0)
                update srPriceItems;
                string strSubmitResult = SubmitSR(SRID);
                if(strSubmitResult != 'Success') {
                    respWrap.haserror = true;
                    respWrap.errormessage = strSubmitResult;
                }
                /* respWrap.pageActionName = '/digitalOnboarding/s/ob-srindex'; */
                respWrap.pageActionName = '/' + System.Label.OB_Site_Prefix + '/s/';
            }
        } else {
            //respWrap.debugger = CustomPageValidations;
            respWrap.haserror = true;
            respWrap.errormessage = validateSRL.validMsg;
        }
    } catch(DMLException e) {
        string DMLError = e.getdmlMessage(0) + '';
        if(DMLError == null) {
          DMLError = e.getMessage() + '';
                }
                
        respWrap.haserror = true;
        respWrap.errorMessage = DMLError;
    }

        return respWrap;
    }
    public static string SubmitSR(string SRID) {
        string strResult = 'Success';
        HexaBPM__Service_Request__c objRequest = new HexaBPM__Service_Request__c(Id = SRID);
        boolean IsAllowedToSubmit = false;
        for(HexaBPM__Service_Request__c SR :[Select Id from HexaBPM__Service_Request__c where Id = :SRID and HexaBPM__Internal_SR_Status__r.Name = 'Draft']) {
            IsAllowedToSubmit = true;
        }
        if(IsAllowedToSubmit) {
            objRequest.HexaBPM__finalizeAmendmentFlg__c = true;
            objRequest.HexaBPM__Submitted_Date__c = system.today();
            objRequest.HexaBPM__Submitted_DateTime__c = system.now();
            for(HexaBPM__SR_Status__c status :[select id, Name from HexaBPM__SR_Status__c where Name = 'Submitted' or Name = 'submitted' limit 1]) {
                objRequest.HexaBPM__Internal_SR_Status__c = status.id;
                objRequest.HexaBPM__External_SR_Status__c = status.id;
            }
            try{
                update objRequest;
            } catch(DMLException e) {
                //v1.6
                string DMLError = e.getdmlMessage(0) + '';
                if(DMLError == null) {
                    DMLError = e.getMessage() + '';
                }
                strResult = DMLError;
            }
        }
        return strResult;
    }
    @AuraEnabled
    public static Boolean sendDataToKTC(String srId){
        Boolean result;
        OB_ServiceRequestTriggerHelper.CreateCompanyWrapper reqWrapObj = new OB_ServiceRequestTriggerHelper.CreateCompanyWrapper();
        Id BuyCarParkHours_RecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('Buy_Car_Parking_Hours_Request').getRecordTypeId();
        HexaBPM__Service_Request__c application = [Select id, Parking_Hours__c, HexaBPM__External_SR_Status__c,
                                                   RecordTypeId, HexaBPM__Customer__c, HexaBPM__External_Status_Name__c                                                   ,
                                                   Discount_Allocation_Id__c, HexaBPM__Customer__r.BP_No__c
                                                   from HexaBPM__Service_Request__c where id=:srId];
        if(application.RecordTypeId == BuyCarParkHours_RecId 
           && application.HexaBPM__External_Status_Name__c == 'Approved'
           && application.Parking_Hours__c != null && application.HexaBPM__Customer__c != null
           && (application.Discount_Allocation_Id__c == null || application.Discount_Allocation_Id__c == '')
           && application.HexaBPM__Customer__r.BP_No__c != null)
        {
            reqWrapObj = new OB_ServiceRequestTriggerHelper.CreateCompanyWrapper();
            reqWrapObj.ID = Integer.valueOf(application.HexaBPM__Customer__r.BP_No__c);
            reqWrapObj.Name = application.HexaBPM__Customer__r.Name;
            reqWrapObj.MultipleDiscount = true;
            String jsonReq = JSON.serialize(reqWrapObj);
            result = OB_ServiceRequestTriggerHelper.sendDataToKTC(jsonReq, JSON.serialize(application.HexaBPM__Customer__c), JSON.serialize(application));
        }
        return result;
    }
    public class ButtonResponseWrapper1 {
        @AuraEnabled public string pageActionName { get; set; }
        @AuraEnabled public boolean haserror { get; set; }
        @AuraEnabled public string errormessage { get; set; }
        @AuraEnabled public object debugger { get; set; }
        @AuraEnabled public object debugger2 { get; set; }
    }
    public class ValidateWrapper {
        @AuraEnabled public Boolean isValid { get; set; }
        @AuraEnabled public string validMsg { get; set; }
        @AuraEnabled public object debugger { get; set; }
        public ValidateWrapper() {

        }
    }
}