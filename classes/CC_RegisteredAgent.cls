/*****************************************************************************************************************************
    Author      :   Sai Kalyan
    Date        :   02-June-2020
    Description :   
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    02-June-2020   Sai        Intial Version
*****************************************************************************************************************************/

public with sharing class CC_RegisteredAgent {
    
    public static void createRegisteredAgent(Id stepID){
    	
    	List<Relationship__c> lstRelationship = new List<Relationship__c>();
    	
    	Step__c eachStep = new Step__c();
    	eachStep = [SELECT SR__c,SR__r.Customer__c FROM Step__c where ID=:stepID];
    	Set<Id> setExistingId = new Set<Id>();
    	String AccountID;
    	system.debug('!!@@!!@#'+eachStep);
    	for(Amendment__c eachAmendment:[SELECT Id,Customer__c,Related__c,Shareholder_Company__c,Status__c,ServiceRequest__r.Customer__c FROM Amendment__c where ServiceRequest__c=:eachStep.SR__c AND 
    									(Status__c='Active' OR Status__c='Remove')]){
    		
    		AccountID = eachAmendment.ServiceRequest__r.Customer__c;
    		if(eachAmendment.Status__c == 'Active'){
	    		Relationship__c eachRelation = new Relationship__c();
	    		eachRelation.Subject_Account__c = eachStep.SR__r.Customer__c; 
	    		eachRelation.Start_Date__c = system.today();
	    		eachRelation.Active__c = true;
	    		eachRelation.Object_Account__c = eachAmendment.Shareholder_Company__c;
	    		eachRelation.Relationship_Group__c = 'ROC';
	    		eachRelation.Relationship_Type__c = 'Registered agent';
	    		eachRelation.Sys_Amendment_Id__c = eachAmendment.Id;
	    		lstRelationship.add(eachRelation);
    		}
    		if(eachAmendment.Status__c == 'Remove'){
    			setExistingId.add(eachAmendment.Related__c);
    		}
    	}
    	system.debug('@@##'+setExistingId);
    	for(Relationship__c eachRelation:[SELECT Id,Active__c,End_Date__c,Sys_Amendment_Id__c FROM Relationship__c where Sys_Amendment_Id__c IN:setExistingId AND Subject_Account__c=:AccountID]){
    		eachRelation.Active__c = false;
    		eachRelation.End_Date__c = system.today();
    		lstRelationship.add(eachRelation);
    	}
    	system.debug('!!@@##$$'+lstRelationship);
    	if(lstRelationship.size()>0){
    		database.upsert(lstRelationship); 
    	}
    }
    
    
    public static void UpdateOperatingLocation(String stepID){
    	
    	List<Amendment__c> lstAmendments = new List<Amendment__c>();
    	
    	Step__c eachStep = new Step__c();
    	eachStep = [SELECT SR__c,SR__r.Customer__c FROM Step__c where ID=:stepID];
    	lstAmendments = [SELECT Shareholder_Company__c,Shareholder_Company__r.Sys_Office__c,Shareholder_Company__r.Office__c,
    					 Shareholder_Company__r.Building_Name__c,Shareholder_Company__r.PO_Box__c,Shareholder_Company__r.Street__c,
    					 Shareholder_Company__r.City__c,Shareholder_Company__r.Emirate__c,Customer__c FROM Amendment__c where 
    					 ServiceRequest__c=:eachStep.SR__c AND Status__c='Active'];
    	
    	String UnitID;
    	String LeasePartner;
    	String HostingCompany;
    	String Lease;
    	system.debug('##@@##$$'+lstAmendments);
    	for(Operating_Location__c eachLocation:[SELECT Account__c,Hosting_Company__c,Unit__c,Lease_Partner__c,Lease__c FROM Operating_Location__c where IsRegistered__c=true AND Status__c='Active' AND Account__c=:lstAmendments[0].Shareholder_Company__c]){
    		UnitID = eachLocation.Unit__c;
    		LeasePartner = eachLocation.Lease_Partner__c;
    		HostingCompany = eachLocation.Hosting_Company__c;
    		Lease = eachLocation.Lease__c;
    	}
    	
    	List<Operating_Location__c> lstCompany = new List<Operating_Location__c>();
    	lstCompany = [SELECT Hosting_Company__c,Unit__c,Lease_Partner__c,Lease__c FROM Operating_Location__c where IsRegistered__c=true AND Status__c='Active'  AND Account__c=:eachStep.SR__r.Customer__c];
    	system.debug('##@@##$$'+lstCompany);
    	if(lstCompany !=null && lstCompany.size()>0){
	    	lstCompany[0].Hosting_Company__c = HostingCompany;
	    	lstCompany[0].Unit__c = UnitID;
	    	lstCompany[0].Lease_Partner__c = LeasePartner;
	    	lstCompany[0].Lease__c = Lease;
	    	UPDATE lstCompany;
    	}
    	List<Account> lstAccountUpdate = new List<Account>();
    	system.debug('!!@####'+lstAmendments[0].Shareholder_Company__c);
    	for(Account eachAccount: [SELECT Sys_Office__c,Office__c,Building_Name__c,PO_Box__c,Street__c,City__c,Emirate__c FROM Account where ID=:eachStep.SR__r.Customer__c]){
    		eachAccount.Sys_Office__c = lstAmendments[0].Shareholder_Company__r.Sys_Office__c;
    		eachAccount.Office__c = lstAmendments[0].Shareholder_Company__r.Office__c;
    		eachAccount.Building_Name__c = lstAmendments[0].Shareholder_Company__r.Building_Name__c;
    		eachAccount.PO_Box__c = lstAmendments[0].Shareholder_Company__r.PO_Box__c;
    		eachAccount.Street__c = lstAmendments[0].Shareholder_Company__r.Street__c;
    		eachAccount.City__c = lstAmendments[0].Shareholder_Company__r.City__c;
    		eachAccount.Emirate__c = lstAmendments[0].Shareholder_Company__r.Emirate__c;
    		lstAccountUpdate.add(eachAccount);
    	}
    	system.debug('!!@####'+lstAccountUpdate);
    	if(lstAccountUpdate.size()>0){
    		UPDATE lstAccountUpdate;
    	}
    }
}