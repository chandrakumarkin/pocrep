/******************************************************************************************
 *  Author   : Veera
 *  Company  : 
 *  Date     : 14/08/2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    14/08/2021  Veera    Created
********/
 global class GSCCDelivery_Queable implements Queueable,Database.AllowsCallouts{
 
    string stepId ;
 
    global  GSCCDelivery_Queable(string stpID){
      stepId = stpID;
    }
    
     global void execute(QueueableContext context){
      
         
           CC_CourierCls.CreateShipmentNormal(stepId);
      }
 }