public without sharing class DPFSideBarComponentController{
    public list<Page__c> lstPages{get;set;}
    public list<PageWrap> lstPageDetails{get;set;}
    public String strPageId {get;set;}
    //public DynamicPageFlowCls parentClass {get;set;}
    
    public DPFSideBarComponentController(){
        strPageId = '';
    }
    
    public list<PageWrap> getLstPageWrap(){
    	map<string,string> mapURLParams = new map<String,string>();
    	if(apexpages.currentPage().getparameters()!=null)
    		mapURLParams = apexpages.currentPage().getparameters();
    	if(apexpages.currentPage().getparameters().get('PageId')!=null){
        	strPageId = apexpages.currentPage().getparameters().get('PageId');
        }
        string pageflowId = apexpages.currentPage().getparameters().get('FlowId');
        system.debug('strPageId==>'+strPageId);
        lstPageDetails = new list<PageWrap>();
        if(pageflowId!=null && pageflowId!=''){
        	PageWrap objWrap;
        	map<string,string> mapFilledPageIds = new map<string,string>();
        	if(mapURLParams.get('Id')!=null){
	        	for(Service_Request__c objSR:[Select id,Filled_Page_Ids__c from Service_Request__c where Id=:mapURLParams.get('Id')]){
	        		list<string> lstPageIds = new list<string>();
	        		if(objSR.Filled_Page_Ids__c!=null && objSR.Filled_Page_Ids__c!=''){
		        		if(objSR.Filled_Page_Ids__c.indexOf(';')>-1){
		        			lstPageIds = objSR.Filled_Page_Ids__c.split(';');
		        		}else{
		        			lstPageIds.add(objSR.Filled_Page_Ids__c);
		        		}
		        		for(string strPageId:lstPageIds){
		        			mapFilledPageIds.put(strPageId.trim(),strPageId.trim());
		        		}
	        		}
	        		
	        	}
        	}
        	for(Page__c objPg:[select id,No_Quick_navigation__c,Page_Flow__c,Name,Page_Order__c,Page_Flow__r.Name,Render_By_Default__c,(select id,Section_Detail__c,Object_Name__c,Field_Name__c,Operator__c,Value__c from Page_Flow_Conditions__r) from Page__c where Page_Flow__c =: pageflowId order by Page_Order__c]){
        		objWrap = new PageWrap();
        		objWrap.pageId = objPg.Id;
        		if(mapFilledPageIds.get(objPg.Id)!=null)
        			objWrap.isPageFilled = true;
        		objWrap.pageName = objPg.Name;
        		objWrap.pageOrder = objPg.Page_Order__c;
        		objWrap.pageFlowId = objPg.Page_Flow__c;
        		objWrap.PageFlowName = objPg.Page_Flow__r.Name;
        		objWrap.NoQuickNavigation = objPg.No_Quick_navigation__c;
        		lstPageDetails.add(objWrap);
        	}
        }
        return lstPageDetails;
    }
    public class PageWrap{
    	public string pageName{get;set;}
    	public string pageId{get;set;}
    	public decimal pageOrder{get;set;}
    	public string pageFlowId{get;set;}
    	public string PageFlowName{get;set;}
    	public boolean NoQuickNavigation{get;set;}
    	public boolean isPageFilled{get;set;}
    	public PageWrap(){
    		isPageFilled = false;
    	}
    }
}