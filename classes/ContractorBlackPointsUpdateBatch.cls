/*
Author        :   Mudasir
Description   :   This class is used to update the contractor black point calculation 
Functionality :   Once the Violation has passed a year we need to pick all those and need to update the contractor black points
--------------------------------------------------------------------------------------------------------------------------
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date                Updated By      Description
--------------------------------------------------------------------------------------------------------------------------             
V1.0    07-October-2018     Mudasir         Created  
V1.2    26-July-2021        Vinod           Optimized the code and Fixed  #11087
*/
global class ContractorBlackPointsUpdateBatch implements Database.Batchable<sObject>, Schedulable{
    
    global string queryString;
    public Set<Id> contractorIds = new Set<Id>();
    public Integer days = System.today().addYears(-1).daysBetween(System.Today());
    public List<Account> lstContractors = new List<Account>();
    public Id contractorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Contractor_Account').getRecordTypeId();
    public Integer Batch_Size = Integer.valueOf(system.label.Batch_Size);
    
    public ContractorBlackPointsUpdateBatch(){
        queryString = 'Select Id,Name,Blacklist_Point_3_months__c,Blacklist_Point_6_months__c,Blacklist_Point_1_year__c,';
        queryString += '(Select Id, Contractor__c,Service_Request__c, Fine_Amount_Formula__c, CreatedDate, Related_Request__c, Violation_Date__c, Waive_Fine_Amount__c, Waive_Black_Points__c, Blacklist_Point__c FROM Violations__r ORDER BY Contractor__r.Name, CreatedDate DESC) ';
        queryString += 'FROM Account WHERE RecordTypeId=:contractorRecordTypeId';
        system.debug('queryString***'+queryString);
    }
    
    public ContractorBlackPointsUpdateBatch(Set<Id> contractorIds){
        this.contractorIds = contractorIds;
        queryString = 'Select Id,Name,Blacklist_Point_3_months__c,Blacklist_Point_6_months__c,Blacklist_Point_1_year__c,';
        queryString += '(Select Id, Contractor__c,Service_Request__c, Fine_Amount_Formula__c, CreatedDate, Related_Request__c, Violation_Date__c, Waive_Fine_Amount__c, Waive_Black_Points__c, Blacklist_Point__c FROM Violations__r ORDER BY Contractor__r.Name, CreatedDate DESC) ';
        queryString += 'FROM Account WHERE RecordTypeId=:contractorRecordTypeId AND Id In:contractorIds';
        system.debug('queryString***'+queryString);
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator(queryString);
    }
    global void execute(Database.BatchableContext bc, List<Account> contractorAccounts){
        try{ 
            if(!contractorAccounts.isEmpty()){
                for(Account contractorRecord : contractorAccounts){
                    Integer blackListPointsThreeMonths = 0;
                    Integer blackListPointsSixMonths = 0;
                    Integer blackListPointsOneYear = 0;
                    if(contractorRecord.Violations__r.size()>0){
                        for(Violation__c violationRec : contractorRecord.Violations__r){
                            Integer vDays = violationRec.CreatedDate.date().daysBetween(System.Today());
                            if(vDays <= 90) blackListPointsThreeMonths += Integer.valueOf(violationRec.Blacklist_Point__c); // Loop: 3 months 
                            if(vDays <= 180) blackListPointsSixMonths += Integer.valueOf(violationRec.Blacklist_Point__c); // Loop: 6 months 
                            if(vDays <= days) blackListPointsOneYear += Integer.valueOf(violationRec.Blacklist_Point__c);  // Loop: 1 year 
                        }
                        contractorRecord.Blacklist_Point_3_months__c = blackListPointsThreeMonths;
                        contractorRecord.Blacklist_Point_6_months__c = blackListPointsSixMonths;
                        contractorRecord.Blacklist_Point_1_year__c = blackListPointsOneYear;    
                        lstContractors.add(contractorRecord);
                    }
                }
            }
            if(!lstContractors.isEmpty()){
                update lstContractors;
            }
        }catch(Exception ex){ Log__c objLog = new Log__c(); objLog.Type__c = 'Schedule Batch fot the contractor black point updated'; objLog.Description__c = 'Exception is : '+ex.getMessage()+'\nLine # '+ex.getLineNumber(); insert objLog;}
    }
    
    global void finish(Database.BatchableContext bc){   
        
    }
    
    global void execute(SchedulableContext context){
        database.executeBatch(new ContractorBlackPointsUpdateBatch(), Batch_Size);
    }
}