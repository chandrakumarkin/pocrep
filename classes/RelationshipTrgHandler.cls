public with sharing class RelationshipTrgHandler implements  TriggerFactoryInterface{
	
	public void executeBeforeInsertTrigger(list<sObject> lstNewRecords){

	}
	
	public void executeBeforeUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
		
	}
	
	public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
		
	}
	
	public void executeAfterInsertTrigger(list<sObject> lstNewRecords){
		updateNewVisaSR(lstNewRecords);
	}
	
	public void executeAfterUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
		
	}
	
	public void executeAfterInsertUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
		
	}
	
	public void updateNewVisaSR(list<Relationship__c> lstRelations){
		map<Id,Service_Request__c> mapSRs = new map<Id,Service_Request__c>();
		list<Id> lstContactIds = new list<Id>();
		Service_Request__c objSR;
		
		for(Relationship__c objRel : lstRelations){
			if(objRel.Active__c && objRel.Relationship_Type__c == 'Has DIFC Sponsored Employee' && objRel.Subject_Account__c != null && objRel.Object_Contact__c != null){
				lstContactIds.add(objRel.Object_Contact__c);
			}
		}
		
		if(!lstContactIds.isEmpty()){
			for(Contact objCon : [select Id,SR__c from Contact where Id IN : lstContactIds AND AccountId != null AND SR__c != null]){
				objSR = new Service_Request__c(Id=objCon.SR__c);
				objSR.Confirm_Change__c = false;
				mapSRs.put(objSR.Id,objSR);
			}
			
			if(!mapSRs.isEmpty()){
				update mapSRs.values();
			}
		}
	}
}