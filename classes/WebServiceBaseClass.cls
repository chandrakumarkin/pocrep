global with sharing class WebServiceBaseClass
{

global class WebServiceConnectorRequest
{
    @InvocableVariable(required=true)
    global ID WebConId;
    @InvocableVariable(required=true)
    global ID RecordId;
    
 }
 
    
  @InvocableMethod(label='Web Service Connector' description='Call a web service from Web Service Connector')
    public static void WebServiceConnector(list<WebServiceConnectorRequest> requests)
    {
   // System.debug('==listLeadIds='+listLeadIds);
   // System.debug('==listLeadIds='+listLeadIds[0]);
    //System.debug('==listLeadIds='+listLeadIds[1]);
    
    System.debug('==requests='+requests);
    for (WebServiceConnectorRequest request : requests)
    {
        CreateSAPAttr(request.RecordId,request.WebConId);
    
    }
        //OperateIntegrationCls.pushSFLeadtoOperate(listLeadIds);
       // OperateIntegrationCls.pushSFLeadtoOperate(listLeadIds[0],WebConId);
    }   
 
public class ResponseAuth
{
    public string access_token{get;set;}
    public string token_type{get;set;}
}

    
public static ResponseAuth ConnectorAuth(id WebConId)
{
     //string ReturnVal='Success';
    
ResponseAuth ObjResponseAuth=new ResponseAuth();
    Web_Service_Connector__c ObjWebSite=[select id,Auth_URL__c,Debug_Mode__c,Test_Record_Id__c,
    (select Connector_Field__c,Is_Valid__c,Type__c,Value__c from Connector_Auth__r) from Web_Service_Connector__c where id=:WebConId];


    Http http = new Http();
    HttpRequest AuthRequest = new HttpRequest();
    AuthRequest.setEndpoint(ObjWebSite.Auth_URL__c);
    AuthRequest.setMethod('POST');

    Map<string,string> BodyData=new Map<string,string>();

    string Body='';
    for(Connector_Auth__c ObjAuth:ObjWebSite.Connector_Auth__r)
    {
        if( ObjAuth.Type__c=='Header')
        AuthRequest.setHeader(ObjAuth.Connector_Field__c,ObjAuth.Value__c);
        else
        Body+=ObjAuth.Connector_Field__c+'='+ObjAuth.Value__c+'&';
     }

    AuthRequest.setBody(Body.substringBeforeLast('&'));
    system.debug('AuthRequest ' +AuthRequest);
    system.debug('getBody' +AuthRequest.getBody());
    
      if(!Test.isRunningTest())
      {
        System.HttpResponse response = new System.Http().send(AuthRequest); system.debug('Response ' +response.getBody());  map<String, String> values = (map<String, String>) JSON.deserialize(response.getBody(), map<String, String>.class);
        ObjResponseAuth.access_token = values.get('access_token');  ObjResponseAuth.token_type=values.get('token_type');
      }
      else
      {
          ObjResponseAuth.access_token='arun';
          ObjResponseAuth.token_type='Demo';
      }
      
      
      
      return ObjResponseAuth;
      
}
  
@Future(callout=true)   
webservice static void CreateSAPAttr(id RecordId,id WebConId)
{
    string ResponseTxt='';
    string ReplyTxt='';
    
     ResponseAuth ObjResponseAuth=ConnectorAuth(WebConId);
   //Get authrization code 
    string access_token= ObjResponseAuth.access_token;
    string token_type=ObjResponseAuth.token_type;


//,(select Connector_Field__c, Data_Type__c, Field_Type__c,Field_Value__c, Is_Valid__c from Connector_Mapping__r)
        
//Push Object Data 

set<string> MasterObjectFields=new set<string>();
string MasterObjectName='';

List<Attachment> ListAttachment=new list<Attachment>();

Map<string,set<string>> ChildObjectFields=new Map<string,set<string>>();
Map<string,string> ChildObjectNameMap=new Map<string,string>();
Map<string,string> ChildObjectFilters=new Map<string,string>();

List<Connector_Request__c> ListRequests=new List<Connector_Request__c>();

for(Connector_Request__c ObjReq:[select id,Debug_Mode__c,(select Connector_Field__c, Data_Type__c, Field_Type__c,Field_Value__c,Date_Format__c, Is_Valid__c,Type__c from Connector_Mapping__r),(select Data_Type__c,Field_Type__c,Response_Field__c,SF_Field__c from Connector_output_Mapping__r), Name,Data_URL__c,Request_Type__c,Filter_Condition__c,Object_API_Name__c,Order_No__c,Type__c,Web_Service_Connector__c from Connector_Request__c where Web_Service_Connector__c=:WebConId order by Order_No__c])
{
    ListRequests.add(ObjReq);
    
            if(ObjReq.Type__c=='Master')
            {
                MasterObjectName=ObjReq.Object_API_Name__c;
            }
            if(ObjReq.Type__c=='Child')
            {
                ChildObjectNameMap.put(ObjReq.Name,ObjReq.Object_API_Name__c);
                ChildObjectFilters.put(ObjReq.Name,ObjReq.Filter_Condition__c);
                
            }
    
        for(Connector_Mapping__c ObjMap:ObjReq.Connector_Mapping__r)
        {
            if(ObjReq.Type__c=='Master')
            {
                if(ObjMap.Field_Type__c=='SF Field')
                {
                    MasterObjectFields.add(ObjMap.Field_Value__c);
                }
                if(ObjMap.Field_Type__c=='Merge Field')
                {
                        List<string> MasterFields=ObjMap.Field_Value__c.split(';');
                        for(string objString:MasterFields)
                        {
                            string FieldName=objString.split('=')[1];
                            MasterObjectFields.add(FieldName);
                        ///MasterObjectFields.add(objString.split('='));    
                        }
                        
                }
                //Field in Post URL
                 if(ObjReq.Data_URL__c.containsAny('|'))
                MasterObjectFields.add(ObjReq.Data_URL__c.substringBetween('|'));
     
            }
            else if(ObjReq.Type__c=='Child')
            {
                
                set<string> chFields=new set<string>();
                if(ChildObjectFields.ContainsKey(ObjReq.Name))
                {
                        chFields=ChildObjectFields.get(ObjReq.Name);
                }
                
                if(ObjMap.Field_Type__c=='SF Field')
                {
                    
                    chFields.add(ObjMap.Field_Value__c);
                    
                    
                }
                if(ObjMap.Field_Type__c=='Merge Field')
                {
                        List<string> MasterFields=ObjMap.Field_Value__c.split(';');
                        for(string objString:MasterFields)
                        {
                            string FieldName=objString.split('=')[1];   chFields.add(FieldName);
                        }
                        
                }
                
                if(ObjReq.Data_URL__c.containsAny('|'))  chFields.add(ObjReq.Data_URL__c.substringBetween('|'));
                 
                ChildObjectFields.put(ObjReq.Name,chFields);    
                
                
            }
            
        }
  
  //get Master fields from Child Requests     
        if(ObjReq.Type__c=='Child')
        {
            if(ObjReq.Filter_Condition__c!=null)
            {
                if(ObjReq.Filter_Condition__c.containsAny('|'))
                {
                    MasterObjectFields.add(ObjReq.Filter_Condition__c.substringBetween('|'));
                }
                
            }
        }
                    
    
}

    
    
           
    

System.debug('MasterObjectFields==>'+MasterObjectFields);
System.debug('ChildObjectFields==>'+ChildObjectFields);

//get master Request Data from salesforce 

string MasterSQL='select ';
for(string MastField : MasterObjectFields)
{
    MasterSQL+=MastField+',';
}

MasterSQL=MasterSQL +' isdeleted from '+MasterObjectName +' where id=\''+RecordId+'\'';
System.debug('MasterSQL==>'+MasterSQL);
sObject  MasterData=Database.query(MasterSQL);
System.debug('MasterData==>'+MasterData);

//get Child Request data from SF 
Map<String,List<sObject>> ChildData=new Map<String,List<sObject>>();

for(string ChildObjectName:ChildObjectFields.keyset())
{
    string ChildSQL='select ';
    for(string ChildField : ChildObjectFields.get(ChildObjectName))
    {
        ChildSQL+=ChildField+',';
    }
    
    string Filter='';
    if(ChildObjectFilters.get(ChildObjectName)!=null)
    {
        Filter=ChildObjectFilters.get(ChildObjectName);
        
                if(Filter.containsAny('|'))
                {
                    String MainObjectField=Filter.substringBetween('|');
                    Filter=Filter.replace('|'+MainObjectField+'|',string.valueof(MasterData.get(MainObjectField)));
                    
                }
                
        
    }
    
    if(Filter=='')
    Filter=' id=\''+RecordId+'\'';
    
    ChildSQL=ChildSQL +' isdeleted from '+ChildObjectNameMap.get(ChildObjectName) +' where '+Filter;
    System.debug('ChildSQL==>'+ChildSQL);
    ChildData.put(ChildObjectName,Database.query(ChildSQL));

}



//Map<string,String> RequestMasterResponse=new Map<String,String>(); 

map<String, Object> RequestMasterResponse; 
Map<string,Map<string,Object>> RequestAllResponse=new Map<string,Map<string,Object>>(); 
List<sObject> ListSobject=new List<sObject>();//Update SF object from Web service response 

for(Connector_Request__c ObjReq:ListRequests)
{
    boolean Is_valid=true;
    System.debug('=ChildData===>'+ChildData); 
    System.debug('=ObjReq.Name===>'+ObjReq.Name);       
    sObject  SFData;
    
    if(ObjReq.Type__c=='Master')
        SFData=MasterData;
    else
    {
        if(ChildData.get(ObjReq.Name)!=null)
        {
            if(!ChildData.get(ObjReq.Name).isEmpty())
            SFData=ChildData.get(ObjReq.Name)[0];       
        }
    
    else
        Is_valid=false;
    }
        
    
    System.debug('=SFData===>'+SFData);
    
        if(Is_valid && SFData!=null)
        {
            HttpRequest Datarequest = new HttpRequest();
            
            string RequestURL=ObjReq.Data_URL__c;
            if(ObjReq.Data_URL__c.containsAny('|'))
            {
                string FieldValinURL=string.valueof(SFData.get(ObjReq.Data_URL__c.substringBetween('|')));
                RequestURL=RequestURL.replace(ObjReq.Data_URL__c.substringBetween('|'),FieldValinURL);
                RequestURL=RequestURL.replace('|','');
            }
            system.debug('RequestURL==>'+RequestURL);
            Datarequest.setEndpoint(RequestURL);
            
            
            Datarequest.setMethod(ObjReq.Request_Type__c);
            Datarequest.setHeader('Authorization',token_type+' '+access_token);

        for(Connector_Mapping__c ObjMap:ObjReq.Connector_Mapping__r)
        {
            if(ObjMap.Type__c=='Header')
            Datarequest.setHeader(ObjMap.Connector_Field__c,ObjMap.Field_Value__c);
            
        }
        
        JSONGenerator jsn = JSON.createGenerator(true);
        jsn.writeStartObject();
                    
        for(Connector_Mapping__c ObjMap:ObjReq.Connector_Mapping__r)
        {
            if(ObjMap.Type__c=='Body')
            {
                 AddNewNode(jsn,ObjMap.Connector_Field__c,ObjMap.Field_Value__c,SFData,ObjMap.Field_Type__c,ObjMap.Data_Type__c,RequestMasterResponse,ObjReq.Type__c,ObjMap.Date_Format__c);
            }   
            
            
        }
        jsn.writeEndObject();
        string data =jsn.getAsString();
        System.debug('data==>'+data);
        Datarequest.setBody(data);
        
        
        
        string responseBody = '';

          ResponseTxt+='\n Method=>'+Datarequest.getMethod(); 
          ResponseTxt+='  \n \n End Point=>'+Datarequest.getEndpoint();   
          ResponseTxt+=' \n \n  Body=>'+Datarequest.getBody();
                  
      if(!Test.isRunningTest())
      {
         // ResponseTxt+='Header=>'+Datarequest.getHeader();

          
          
          System.HttpResponse DataResponse = new System.Http().send(Datarequest); 
            responseBody = DataResponse.getBody();
            
            ReplyTxt=DataResponse.getBody();
      }
      else
      {
         responseBody ='[{"tags":[],"details":{},"calculatedStatus":"inactive","calculatedStage":"","_id":"5eadba84310113012427ff24","name":"Daman Real Estate Capital Partners Limited","url":"www.difc.ae","description":"aa","organization":"5d0ddb6f18d6cb0113e4af5a","email":"sf@difc.ae","properties":{"BPNumber":"0000415923","AccountIdSF":"0012000001H3yr5AAB"},"office":"5de8fb16e6aca0056595c298","twitterInfo":{"description":"","imageUrl":null},"image":null,"createdAt":"2020-05-02T18:23:00.149Z","modifiedAt":"2020-05-02T18:23:00.149Z","createdBy":"5ea13840b8fd3900696119dc","modifiedBy":"5ea13840b8fd3900696119dc","billing":[],"isTeam":true}]'; 
      }
         
         if(ObjReq.Debug_Mode__c==true)
         {
             Attachment ObjAttachment=new Attachment();
            ObjAttachment.ContentType='text';
            ObjAttachment.Name='Log-File.txt';
            ObjAttachment.ParentId=WebConId;
            ObjAttachment.Body=Blob.valueof('\n \n Request : \n\n '+ResponseTxt+'     \n\n  ======================   \n\n  Response :   \n\n    '+ReplyTxt);
            ResponseTxt='';
            ReplyTxt='';
            ListAttachment.add(ObjAttachment);
             
         }
        
          
      
        
        System.debug(ObjReq.Name+'==>responseBody==>'+responseBody); 
        
            if(responseBody.startsWith('['))
            {
            responseBody=responseBody.removeStart('[');
            responseBody=responseBody.removeEnd(']');
            }
            System.debug('Final==responseBody==>'+responseBody); 

            
        if(ObjReq.Type__c=='Master')
        {
           RequestMasterResponse=(Map<String, Object>) JSON.deserializeUntyped(responseBody);
           System.debug('RequestMasterResponse==>'+RequestMasterResponse); 
        }
        
        Map<String, Object> TempMap=(Map<String, Object>) JSON.deserializeUntyped(responseBody);
        
        RequestAllResponse.put(ObjReq.Name,TempMap);
    
    //Update Object based on responce 
    
        for(Output_Mapping__c ObjoutMap:ObjReq.Connector_output_Mapping__r)
        {
        
            UpdateResponseData(SFData,ObjoutMap.SF_Field__c,ObjoutMap.Response_Field__c,ObjoutMap.Data_Type__c,ObjoutMap.Field_Type__c,RequestAllResponse.get(ObjReq.Name));
            ListSobject.add(SFData);
        }
              
        
}

   
                   
        }
      
      
                    
    if(!ListSobject.isEmpty())
    update ListSobject;    
    if(!ListAttachment.isEmpty())
    insert ListAttachment;
     // return ReturnVal;


}
public static void UpdateResponseData(sObject SFData,string FieldName,string FieldVal,string DataType,string FieldType,map<String, object> RequestResponse)
{
    System.debug('============UpdateResponseData=========================');
    System.debug('==SFData=>'+SFData);
    System.debug('==FieldName==>'+FieldName);
    System.debug('==FieldVal==>'+FieldVal);
    System.debug('==DataType==>'+DataType);
    System.debug('==FieldType==>'+FieldType);
    System.debug('==RequestResponse==>'+RequestResponse);
    
    if(FieldType=='Static')
    {
        if(DataType=='String') SFData.put(FieldName,FieldVal);
        else if(DataType=='Boolean')    SFData.put(FieldName,Boolean.valueof(FieldVal));
        else if(DataType=='Blob')    SFData.put(FieldName,Blob.valueof(string.valueof(FieldVal)));
        else if(DataType=='Date')    SFData.put(FieldName,Date.valueof(FieldVal));
        else if(DataType=='Date Time')       SFData.put(FieldName,Date.valueof(FieldVal));
                      
    }
    else if(RequestResponse.containsKey(FieldVal) && FieldType=='Response Attribute')
    {
                    if(DataType=='String') SFData.put(FieldName,string.valueof(RequestResponse.get(FieldVal)));
                  else if(DataType=='Boolean') SFData.put(FieldName,Boolean.valueof(RequestResponse.get(FieldVal)));
                     else if(DataType=='Blob')  SFData.put(FieldName,Blob.valueof(string.valueof(RequestResponse.get(FieldVal))));
                     else if(DataType=='Date')  SFData.put(FieldName,Date.valueof(RequestResponse.get(FieldVal)));
                      else if(DataType=='Date Time')  SFData.put(FieldName,Date.valueof(RequestResponse.get(FieldVal)));
        
    }
    
            
                     
    
}

public static void AddNewNode(JSONGenerator jsn,string FieldName,string FieldValue,sObject SFData,string FieldType,string DataType,map<String, object> RequestMasterResponse,string MainRequestType,string FormateValue)
{
    System.debug('========AddNewNode========================');
    System.debug('=======FieldName=>'+FieldName);   
    System.debug('=======FieldValue=>'+FieldValue); 
    System.debug('=======SFData=>'+SFData); 
    System.debug('=======FieldType=>'+FieldType);   
    System.debug('=======DataType=>'+DataType); 
    System.debug('=======FormateValue=>'+FormateValue);
    
            if(FieldType=='Static')
            {
                if(DataType=='String')
                     jsn.writeStringField(FieldName,string.valueof(FieldValue));
                 else if(DataType=='Boolean')
                     jsn.writeBooleanField(FieldName,Boolean.valueof(FieldValue));
                     
                     /*
                 else if(DataType=='Blob')
                     jsn.writeBlobField(FieldName,Blob.valueof(FieldValue));
                 else if(DataType=='Date')
                     jsn.writeDateField(FieldName,Date.valueof(FieldValue));
                  else if(DataType=='Date Time')
                     jsn.writeDateTimeField(FieldName,Date.valueof(FieldValue));
                 */
                     
                     
            }
            
                 else if(FieldType=='SF Field')
                 {
                     
                     Object  FieldVal;
                     if(SFData.get(FieldValue)==null)
                         FieldVal='';
                     else
                         FieldVal=SFData.get(FieldValue);
                     System.debug('FieldValue=='+FieldValue+'===FieldVal===>'+FieldVal);
                      
                    
                    if(DataType=='String')
                         jsn.writeStringField(FieldName,string.valueof(FieldVal));
                     else if(DataType=='Boolean')
                         jsn.writeBooleanField(FieldName,Boolean.valueof(FieldVal));
                     
                       else if(DataType=='Number')
                         jsn.writeNumberField(FieldName,Decimal.valueof(FieldVal+''));
                     
                     
                      else if(DataType=='String With Datetime Format')
                      {
                         Datetime temp =datetime.valueof(FieldVal);
                         string DateNEw=temp.format(FormateValue);
                         jsn.writeStringField(FieldName,DateNEw);     
                      }
                      else if(DataType=='String With Date Format')
                      {
                         Datetime temp =date.valueof(FieldVal);
                         string DateNEw=temp.format(FormateValue);
                         jsn.writeStringField(FieldName,DateNEw);     
                      }
                     
                     
                         
                         /*
                     else if(DataType=='Blob')
                         jsn.writeBlobField(FieldName,Blob.valueof(string.valueof(FieldVal)));
                     else if(DataType=='Date')
                         jsn.writeDateField(FieldName,Date.valueof(FieldVal));
                      else if(DataType=='Date Time')
                         jsn.writeDateTimeField(FieldName,Date.valueof(FieldVal));
                           */  
                    
                 }
                   
               
               else if(FieldType=='Merge Field')
                {
                    /*
                    
                    "properties": {
                            "BPNumber": "0000413300",
                            "AccountIdSF": "0012000001H5A5NAAV"
                          }
                          
                    */
                      jsn.writeFieldName(FieldName);
                      
                        jsn.writeStartObject();
                            List<string> MasterFields=FieldValue.split(';');
                            for(string objString:MasterFields)
                            {
                                List<string> tempFieldValue=objString.split('=');
                                
                                //jsn.writeStringField(tempFieldValue[0],SFData.get(tempFieldValue[1]));
                                
                                    if(DataType=='String')
                                     jsn.writeStringField(tempFieldValue[0],string.valueof(SFData.get(tempFieldValue[1])));
                                 else if(DataType=='Boolean')
                                     jsn.writeBooleanField(tempFieldValue[0],Boolean.valueof(SFData.get(tempFieldValue[1])));
                                 
                                  else if(DataType=='Number')
                                     jsn.writeNumberField(tempFieldValue[0],Decimal.valueof(SFData.get(tempFieldValue[1])+''));
                                 
                                 
                                 
                                 
                                     
                                     /*
                                 else if(DataType=='Blob')
                                     jsn.writeBlobField(tempFieldValue[0],Blob.valueof(string.valueof(SFData.get(tempFieldValue[1]))));
                                 else if(DataType=='Date')
                                     jsn.writeDateField(tempFieldValue[0],Date.valueof(SFData.get(tempFieldValue[1])));
                                  else if(DataType=='Date Time')
                                     jsn.writeDateTimeField(tempFieldValue[0],Date.valueof(SFData.get(tempFieldValue[1])));
                                       */      
                                
                                
                                
                            }
                        jsn.writeEndObject();
                }
                else if(FieldType=='Merge Field List')
                {
                    /*
                    "properties": [{
                            "BPNumber": "0000413300",
                            "AccountIdSF": "0012000001H5A5NAAV"
                          }]
                    */
                      jsn.writeFieldName(FieldName);
                      
                       jsn.writeStartArray();
                            jsn.writeStartObject();
                                List<string> MasterFields=FieldValue.split(';');
                                for(string objString:MasterFields)
                                {
                                    List<string> tempFieldValue=objString.split('=');
                                    
                                    //jsn.writeStringField(tempFieldValue[0],SFData.get(tempFieldValue[1]));
                                    
                                        if(DataType=='String')    jsn.writeStringField(tempFieldValue[0],string.valueof(SFData.get(tempFieldValue[1])));
                                     else if(DataType=='Boolean')    jsn.writeBooleanField(tempFieldValue[0],Boolean.valueof(SFData.get(tempFieldValue[1])));
                                     else if(DataType=='Number')      jsn.writeNumberField(tempFieldValue[0],Decimal.valueof(SFData.get(tempFieldValue[1])+''));
                                     
                                     
                                     
                                     
                                 
                                         
                                   /*  else if(DataType=='Blob')
                                         jsn.writeBlobField(tempFieldValue[0],Blob.valueof(string.valueof(SFData.get(tempFieldValue[1]))));
                                     else if(DataType=='Date')
                                         jsn.writeDateField(tempFieldValue[0],Date.valueof(SFData.get(tempFieldValue[1])));
                                      else if(DataType=='Date Time')
                                         jsn.writeDateTimeField(tempFieldValue[0],Date.valueof(SFData.get(tempFieldValue[1])));
                                     */            
                                    
                                    
                                    
                                }
                            jsn.writeEndObject();
                         jsn.writeEndArray();
                             
                }
                
                
                
                else if(FieldType=='Master Response Attribute')
                {
                    System.debug('RequestMasterResponse==>'+RequestMasterResponse);
                    
                    if(RequestMasterResponse.ContainsKey(FieldValue))
                    {
                        jsn.writeStringField(FieldName,string.valueof(RequestMasterResponse.get(FieldValue)));  
                    }
                     
                }
                
    
}


}