/**
 * @description       : 
 * @author            : Mudasir
 * @group             : 
 * @last modified on  : 12-14-2020
 * @last modified by  : Mudasir Wani
 * Modifications Log 
 * Ver   Date         Author    Modification
 * 1.0   11-23-2020   Mudasir   Initial Version
 * #DIFC2-10561 - Fitout Portal Requirements: Milestone Penalties
**/
global with sharing class DIFCFitoutMilestonePenaltyCreationBatch implements Database.Batchable < sObject > , Schedulable{
    String query = '';
    String unitsWithoutProjectsSOQL =''; 
    public List<Log__c> logList;
    global Database.QueryLocator start(Database.BatchableContext bc) {  
        logList = new List<Log__c>();
        query = 'SELECT id,Building__c,Batch_Updated_Record__c,Floor__c,Unit__c,Unit_Numbers__c,Account__c,Type_of_Retail__c, Additional_Tenant_email__c,Lease__c, Service_Request__c, Conceptual_Design_Milestone_Achieved__c, Conceptual_Milestone_Start_Date__c, Detail_Design_Milestone_Achieved__c, Detail_Milestone_Start_Date__c, First_Inspection_Milestone_Achieved__c, First_Milestone_Start_Date__c, Final_Inspection_Milestone_Achieved__c, Final_Milestone_Start_Date__c, Next_Conceptual_Penalty_Date__c, Next_Detail_Design_Penalty_Date__c, Next_First_Inspection_Penalty_Date__c, Next_Final_Inspection_Penalty_Date__c, Is_Closed__c, Name FROM Fitout_Milestone_Penalty__c where (Conceptual_Design_Milestone_Achieved__c = false AND Next_Conceptual_Penalty_Date__c =today) OR (Detail_Design_Milestone_Achieved__c = false AND Next_Detail_Design_Penalty_Date__c = today) OR (First_Inspection_Milestone_Achieved__c = false AND Next_First_Inspection_Penalty_Date__c =today) OR (Final_Inspection_Milestone_Achieved__c = false AND Next_Final_Inspection_Penalty_Date__c = today)';
        system.debug('query---'+query);
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List < Fitout_Milestone_Penalty__c > scope) {
        system.debug('Mudasir batch started');
        List<Service_Request__c> milestoneServiceRequests = new List<Service_Request__c>();
        List<Fitout_Milestone_Penalty__c> milestonePenaltiesToUpdate = new List<Fitout_Milestone_Penalty__c>();
        for(Fitout_Milestone_Penalty__c fitoutMilestoneRec : (List<Fitout_Milestone_Penalty__c>) scope){
           if(fitoutMilestoneRec.Next_Conceptual_Penalty_Date__c == Date.today() && fitoutMilestoneRec.Conceptual_Design_Milestone_Achieved__c ==false)
           {
                milestoneServiceRequests.add(createServiceRequest(fitoutMilestoneRec, 'Conceptual Design Submission'));
                fitoutMilestoneRec.Next_Conceptual_Penalty_Date__c = fitoutMilestoneRec.Next_Conceptual_Penalty_Date__c.addDays(5);
           }
           if(fitoutMilestoneRec.Next_Detail_Design_Penalty_Date__c == Date.today()&& fitoutMilestoneRec.Detail_Design_Milestone_Achieved__c ==false)
           {
                milestoneServiceRequests.add(createServiceRequest(fitoutMilestoneRec , 'Detail Design Submission'));
                fitoutMilestoneRec.Next_Detail_Design_Penalty_Date__c = fitoutMilestoneRec.Next_Detail_Design_Penalty_Date__c.addDays(5);
           }
           if(fitoutMilestoneRec.Next_First_Inspection_Penalty_Date__c == Date.today() && fitoutMilestoneRec.First_Inspection_Milestone_Achieved__c ==false)
           {
                milestoneServiceRequests.add(createServiceRequest(fitoutMilestoneRec, 'First Fix Inspection'));
                fitoutMilestoneRec.Next_First_Inspection_Penalty_Date__c = fitoutMilestoneRec.Next_First_Inspection_Penalty_Date__c.addDays(5);
           }
           if(fitoutMilestoneRec.Next_Final_Inspection_Penalty_Date__c == Date.today() && fitoutMilestoneRec.Final_Inspection_Milestone_Achieved__c ==false)
           {
                milestoneServiceRequests.add(createServiceRequest(fitoutMilestoneRec , 'Final Fix Inspection'));
                fitoutMilestoneRec.Next_Final_Inspection_Penalty_Date__c = fitoutMilestoneRec.Next_Final_Inspection_Penalty_Date__c.addDays(5);
           }
		   fitoutMilestoneRec.Batch_Updated_Record__c = true;
           milestonePenaltiesToUpdate.add(fitoutMilestoneRec);         
        }
        system.debug('milestoneServiceRequests---'+milestoneServiceRequests);
        Database.SaveResult[] srList =  Database.insert(milestoneServiceRequests , false);
        List<Service_Request__c> submitServiceRequest = new List<Service_Request__c>();
        submitServiceRequest = errorHandlingMethod(srList);

        //Now submitted the requests
        Database.SaveResult[] srSubmittedList =  Database.update(submitServiceRequest , false);
        errorHandlingMethod(srList);
        
        Database.update(milestonePenaltiesToUpdate , false);
    }
    global void finish(Database.BatchableContext BC) {
        if(logList != null && logList.size() > 0){
            Database.insert(logList);
        }
    }
    global void execute(SchedulableContext ctx) {
        DIFCFitoutMilestonePenaltyCreationBatch bachable = new DIFCFitoutMilestonePenaltyCreationBatch();
        database.executeBatch(bachable);
    }
    public List<Service_Request__c> errorHandlingMethod(Database.SaveResult[] servliceReqListSaveResult){
        List<Service_Request__c> serviceRequestList = new List<Service_Request__c>();
        for (Database.SaveResult submittedServReqSaveResultRec : servliceReqListSaveResult) {
            if (submittedServReqSaveResultRec.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted Service Request ' + submittedServReqSaveResultRec.getId());
                serviceRequestList.add(new Service_Request__c(id=submittedServReqSaveResultRec.getId() , 
                                                                Internal_SR_Status__c   = System.Label.SR_Submitted_Status_Id , 
                                                                External_SR_Status__c   = System.Label.SR_Submitted_Status_Id));
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : submittedServReqSaveResultRec.getErrors()) {
                    Log__c objLog               =   new Log__c();
                    objLog.Description__c       =   'Message==>'+err.getMessage() +'---Field resulting in error ---'+ err.getFields();
                    objLog.Type__c              =   'Hoarding Service request';
                    objLog.sObject_Record_Id__c =   submittedServReqSaveResultRec.getId();
                    system.debug('objLog------'+objLog);
                    logList.add(objLog);
                }
            }
        }
        return serviceRequestList;
    }
    public Service_Request__c createServiceRequest(Fitout_Milestone_Penalty__c fitoutMilestoneRec , String justification){
        system.debug('Mudasir createServiceRequest method started');
        Service_Request__c servRequestRec   		= 	new Service_Request__c();
        servRequestRec.Lease__c             		=   fitoutMilestoneRec.Lease__c;
        servRequestRec.Customer__c          		=   fitoutMilestoneRec.Account__c;
        servRequestRec.Linked_SR__c         		=   fitoutMilestoneRec.Service_Request__c;
        servRequestRec.SR_Template__c       		=   System.Label.Fitout_Milestone_Penalty_Template_Id;
        servRequestRec.SR_Group__c          		=   'Other';
        servRequestRec.Undertaking__c       		=   'I hereby confirm that I have verified the details of this request';
        servRequestRec.Submitted_Date__c    		=   System.today();
        servRequestRec.Justification__c     		=   'Fine issue for because of the missing milestone #'+justification;
        servRequestRec.Fitout_Milestone_Penalty__c 	= 	fitoutMilestoneRec.id;
        servRequestRec.Additional_Email__c			=	fitoutMilestoneRec.Additional_Tenant_email__c;
        servRequestRec.Property_Type__c				=	fitoutMilestoneRec.Type_of_Retail__c;
        servRequestRec.Location__c					=	fitoutMilestoneRec.Unit_Numbers__c;
        servRequestRec.Summary__c					=	justification+' Milestone';
        servRequestRec.Folio_Number__c				=	justification+' Milestone';
        servRequestRec.Building_Name__c				=	fitoutMilestoneRec.Building__c;
        servRequestRec.Floor__c						=	fitoutMilestoneRec.Floor__c;
        servRequestRec.Location_of_DIFC_Unit__c		=	fitoutMilestoneRec.Unit__c;
        servRequestRec.Usage_Type__c				=	fitoutMilestoneRec.Unit__c;
        
        return servRequestRec;
    }
}