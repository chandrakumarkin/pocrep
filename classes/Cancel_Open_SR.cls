/*
    Author		:	Durga Prasad
    Class Name  :	Cancel_Open_SR
    Description :	This will check whether the SR is in Submitted status or not. 
                 	If it's not submitted then SR will be deleted.
	
	Modification History :
		
		* By Ravi on Mar 30, 2015 - When User click's on Cancel SR and it is GS related the we should stop the process in Salesforce as well as in SAP.
	
v1.1	20/12/2016	Ravi	Added the logic to ask the customer for Reason of Cancellation as per #2872


 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date                 Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    30-OMarch-2015       Ravi          When User click's on Cancel SR and it is GS related the we should stop the process in Salesforce as well as in SAP 
 V1.2    26-February -2018    Mudasir       isPortalLogin Method added in order to handle the rediret differently when we are cancelling the service request 
*/
global without sharing class Cancel_Open_SR {
     webservice static string Cancel_SR(String SRID,string SRMenuType){
        string strResult = 'Success';
        if(SRID!=null && SRID!=''){
            Boolean isDraft = false;
            ///addded by Ravi on March 20, 2015
            Boolean isGsService = false;
            
            Service_Request__c objCancelSR = new Service_Request__c(); 
            //for(Service_Request__c sr:[select Id,SR_Template__r.Menu__c from Service_Request__c where id=:SRID and Internal_Status_Name__c='Draft' limit 1])
            for(Service_Request__c sr : [select Id,Internal_Status_Name__c,SR_Template__r.Menu__c from Service_Request__c where id=:SRID limit 1]){
                if(sr.Internal_Status_Name__c == 'Draft'){
	                isDraft = true;
	                objCancelSR = sr;
                }else if(sr.SR_Template__r.Menu__c == 'Employee Services'){
                	isGsService = true;
                }
            }
            
            if(isGsService == false){ // codition added by Ravi
	            if(isDraft==false){
	                strResult = 'Request is already submitted.';
	                return strResult;
	            }else{
	            	try{
	            		delete objCancelSR;
	            	}catch(Exception e){
	            		return e.getMessage();
	            	}
	            }
            }else{
            	//return GsUpgradeDetails.CancelService(SRID);
            	//v1.1 return GsServiceCancellation.ProcessCancellation(SRID);
            	//v1.1 
            	return 'Cancellation submitted successfully';
            }
     	}
     	return strResult;
     }
    //isPortalLogin Method added by Mudasir in order to handle the rediret differently when we are cancelling the service request
  	webservice static boolean  isPortalLogin(String SRID,string SRMenuType){
  		boolean isPortalLogin = false;
	    Service_Request__c serReq = [Select id,Customer__c ,Linked_SR__c, RecordType.DeveloperName, SR_Template__r.Check_Open_SR__c from Service_Request__c where id =: SRID];
	    if(serReq.RecordType.DeveloperName =='Request_for_security_deposit_encashment' || serReq.RecordType.DeveloperName =='Site_Visit_Report' || serReq.RecordType.DeveloperName =='Look_Feel_Approval_Request'){
	    	isPortalLogin = true;
	    }
	    return isPortalLogin;
    }
}