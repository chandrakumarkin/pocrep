@isTest
global class OB_ReserveTradeMock implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        OB_INF_ReserveTradeName_WS.ReserveTradeNameResponse_element res = new OB_INF_ReserveTradeName_WS.ReserveTradeNameResponse_element();
        
        res.OP_TransactionNumber = '12345';
        res.OP_TradeNameNumber= '34567';
        res.OP_ArabicTradeName= 'abc';
        res.OP_EnglishTradeName = 'abc';
        res.OP_ArabicMessage = 'ert';
        res.OP_EnglishMessage = 'abc';
        res.OP_LogID = '12';
        res.OP_Code = '567';
        
        response.put('response_x', res); 
   }
}