/****************************************************************
* Apex Class: CheckTRCaseStatusSchedule
* Description: Use this class to get the TR case status every 5 mins
* Modified  :
24-02-2020    selva     updated the batch size to 1
*****************************************************************/
global class CheckTRCaseStatusSchedule implements Schedulable {
    
    global List<Thompson_Reuters_Check__c> trcCaselst;
    
    global CheckTRCaseStatusSchedule (){
    
        } 
    global void execute(SchedulableContext SC) {
    
        //String schTime= '0 15 17 * * ?';
       // CheckTRCaseStatusSchedule obj = new CheckTRCaseStatusSchedule();
       // Id jobId = System.schedule('TR Check scheduler',schTime,obj);
       
       Database.executeBatch(new TRCaseStatusbatch (), 1);
       
       /**********
       try{
            trcCaselst =  new List<Thompson_Reuters_Check__c>();

            trcCaselst = [select id,TRCase_No__c,TR_Case_Status__c from Thompson_Reuters_Check__c where TRCase_No__c!=null and TR_Case_Status__c!='CloseCase' limit 40];

            for(Thompson_Reuters_Check__c itr: trcCaselst){
            ThompsonReutersValidation.checkTRCaseStatus(itr.TRCase_No__c);
            }
       }
       catch(Exception e){
           
       }
      ***************/
    }
}