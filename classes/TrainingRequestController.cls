public without sharing class TrainingRequestController {
    public Training_Request__c trainingRequest {get;set;}
    public string selectedValue {get;set;}
    public list<SelectOption> lstTrainingDates {get;set;}
    public boolean isThank {get;set;}
    public boolean isSubmitted {get;set;}
    public string strCategory {get;set;}
    public string trainingRequestName {get;set;}
    public map<id,Training_Master__c> mapTrainingMaster {get;set;}
    public Training_Master__c trainingMaster {get;set;}
    public boolean boolAccountRequestLimitHit {get;set;}
    public Id contact;
    public Id account;
    public TrainingRequestController(){
        //trainingMaster = new Training_Master__c();
        trainingRequest = new Training_Request__c();
        isThank = false;
        isSubmitted = false;
        strCategory = ApexPages.currentPage().getParameters().get('category');
        for(User user : [select contactId,contact.AccountId from User where id = :Userinfo.getUserId()]){
            contact = user.contactId;
            account = user.contact.AccountId;
        }
        populateTrainingDate();
        
        
    }
    public void populateTrainingDate(){
        mapTrainingMaster = new map<id,Training_Master__c>();
        lstTrainingDates = new List<SelectOption>{new SelectOption('','-Select-')};
    	for(Training_Master__c TM : [select id,Name, Start_DateTime__c,To_DateTime__c,Description__c from Training_Master__c
                                     where Category__c = :strCategory and Start_DateTime__c > TODAY
                                     and Status__c = 'Approved by HOD' and Remaining_Seats__c > 0]){
               if(TM.Start_DateTime__c != null && TM.To_DateTime__c != null)
               	lstTrainingDates.add(new SelectOption(String.valueOf(TM.id),TM.Name + ' - ' + String.valueOf(TM.Start_DateTime__c.format('dd/MM/YYY HH:mm'))+' - '+String.valueOf(TM.To_DateTime__c.format('HH:mm'))));
        
               mapTrainingMaster.put(TM.id,TM);
        }
    }
    public PageReference save(){
        boolean proceedSubmission = false;
        validateRequestLimit();
        if(boolAccountRequestLimitHit)
            return null;
        //Check the seat is still available before submission.
        for(Training_Master__c TM : [select id,Name, Start_DateTime__c,To_DateTime__c,Description__c from Training_Master__c
                                     where id = :selectedValue  and Status__c = 'Approved by HOD' and Remaining_Seats__c > 0]){
        	proceedSubmission = true;
        }
        if(proceedSubmission && boolAccountRequestLimitHit == false){
            trainingRequest.TrainingMaster__c = selectedValue;
            trainingRequest.Contact__c = contact;
            trainingRequest.Account__c = account;
            try{
                insert trainingRequest;
                for(Training_Request__c  tr: [select Name from Training_Request__c where id = :trainingRequest.id]){
                    trainingRequestName = tr.Name;
                }
                isThank = true;
            }
            catch(Exception ex){
                ApexPages.addMessages(ex);
            }
        }
        else{
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'The requested training slot are filled. Please select a different slot when available.'));         
        	populateTrainingDate();
        }
        return null;
    }
    public PageReference doCancel()
      {
        PageReference returnPage = null;
       /* if(strCategory != null && strCategory == 'Employee Services')
        	returnPage = new PageReference('/EmployeeServices');  
        else if(strCategory != null && strCategory == 'Property Services')
            returnPage = new PageReference('/PropertyRequests');  
         else if(strCategory != null && strCategory == 'Company Services')
            returnPage = new PageReference('/CompanyServices');  */
         returnPage = new PageReference('/a3a');   
        return returnPage;
      }
    public void populateDetail(){
        if(mapTrainingMaster.get(selectedvalue) != null)
        	trainingMaster = mapTrainingMaster.get(selectedvalue);
    }
    public void validateRequestLimit(){
        boolAccountRequestLimitHit = false;
        integer seatLimitPerAccount;
        string labelSeatLimit = System.Label.Training_Request_Limit_Per_Account;
        if(String.isNotBlank(labelSeatLimit)){
            seatLimitPerAccount = integer.valueOf(labelSeatLimit);
            if(mapTrainingMaster.get(selectedvalue) != null){
                trainingMaster = mapTrainingMaster.get(selectedvalue);
                Integer seatCount = [select count() from Training_Request__c where TrainingMaster__c = :trainingMaster.id 
                                     and Account__c = :account];
                if(seatCount >= seatLimitPerAccount){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Sorry, you have exceeded the total Training Request ('+seatLimitPerAccount+') per Company. '));         
                    boolAccountRequestLimitHit = true;
                }
                
            }
        }
    }
}