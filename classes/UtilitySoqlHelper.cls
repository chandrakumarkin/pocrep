/******************************************************************************************
 *  Author   : Mariappan Gomathiraj
 *  Company  : 
 *  Date     : 21/03/2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    21/03/2021  Mari    Created

**********************************************************************************************************************/

public class UtilitySoqlHelper{

    
    /**
     * Helper method to fetch the all fields based on Object Id
     * @param  Record Id,Parent fields 
     * @return List of Sobject.          
     */
    
    Public static List<sObject> fetchSobjectRecordBasedOnId(Id rId,string parentFields){

        DescribeSObjectResult describeResult = rId.getSObjectType().getDescribe();      
        List<String> fieldNames = new List<String>( describeResult.fields.getMap().keySet() );
        String soqlQuery = ' SELECT ';
        if(parentFields != '')soqlQuery +=parentFields+',';
        soqlQuery += string.join (fieldNames, ',') + ' FROM ' + describeResult.getName() + ' Where Id =: rId limit 1';
        List<sObject> recordList = Database.query(soqlQuery);
        system.debug('=======recordList ======================='+recordList);
        return recordList;
    }
    
    /**
     * Helper method to fetch the all SOQL fields without parent fields
     * @param  Sobject Name
     * @return string.          
     */
    
    public static String AllFields(String ObjectName) {
        List<String> fields = new List<String>(Schema.getGlobalDescribe().get(ObjectName).getDescribe().fields.getMap().keySet());
        String query  = 'SELECT '+String.join(fields, ',')+' FROM '+ObjectName;
        return query;
    }
    
    /**
     * Helper method to fetch the all SOQL fields with parent fields
     * @param  Sobject Name,Parent fields
     * @return string.          
     */
    
    public static String allFieldswithParentFields(String ObjectName,string parentFields) {
        List<String> fields = new List<String>(Schema.getGlobalDescribe().get(ObjectName).getDescribe().fields.getMap().keySet());
        String query  = ' SELECT ';
        if(parentFields != '')query  +=parentFields+',';
        query  += String.join(fields, ',')+' FROM '+ObjectName;
        return query;
    }
}