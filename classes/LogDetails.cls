/*
    Created By  : Ravi on 2nd May,2016
    Description : This class is a Helper class to create the logs
     
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date			Updated By		Description
 ----------------------------------------------------------------------------------------              
 V1.0	21/07/2015		Ravi			Created
 
 ----------------------------------------------------------------------------------------
*/
public without sharing class LogDetails {
	
	public static list<Log__c> CreateLog(list<Log__c> lstLogs,string sType,string sDescription){
		lstLogs = (lstLogs == null) ? new list<Log__c>() : lstLogs;
		lstLogs.add(new Log__c(
			Type__c = sType,
			Description__c = sDescription
		));
		return lstLogs;
	}
	
}