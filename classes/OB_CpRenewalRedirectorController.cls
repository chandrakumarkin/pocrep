public with sharing class OB_CpRenewalRedirectorController {
    
    @AuraEnabled
    public static OB_AgentEntityUtils.RespondWrap redirectToPage(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        
        string contactID = '';
        for(User usr:[SELECT id,contactID FROM User  WHERE id =:Userinfo.getUserId()]){
            contactID = usr.contactID;
        }

        if(contactID != ''){
            OB_AgentEntityUtils.RespondWrap respWrap = OB_AgentEntityUtils.createNewCpRenewalSr(contactID,'cp-renewal');
            return respWrap;

            
            
        }else{
            return null;
        }
    }
        
    
    // ------------ Wrapper List ----------- //
        
    public class RequestWrap{
    
        @AuraEnabled public String srId; 
    }
        
   
}