/****************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : Tech Carrot
 *  Date     : 31 March 2020  
 *  Description : this class is used to create shipment for courier collections if shipment is not created               
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date                Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.0   31 March 2020         Shoaib               Created
****************************************************************************************/

global class CreateAutomaticShipments implements Database.Batchable<sObject>,Schedulable{
 
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        Set<String> stepNames            = new Set<String>{'Courier Collection','Courier Delivery'};
        Set<Id> stepIds                  = new Set<Id>();
        Set<Id> stepIdsWithShipment      = new Set<Id>();
        Set<Id> stepIdsWithoutShipment   = new Set<Id>();
     
        for(Step__c thisStep : [SELECT Id,
                                        Step_Name__c 
                                FROM Step__c
                                WHERE Step_Name__c IN: stepNames
                                AND CreatedDate =  LAST_N_DAYS:0 
                                AND (Status_Code__c = 'PENDING_COLLECTION'
                                OR Status_Code__c = 'PENDING_DELIVERY')]){
          
           stepIds.add(thisStep.Id);
        }
        
        for(Shipment__c thisShipment : [SELECT Id,
                                               Step__c 
                                         FROM Shipment__c
                                         WHERE Step__c IN:stepIds]){

           stepIdsWithShipment.add(thisShipment.Step__c);
        }
        
        for(Id thisIds : stepIds){
            if(!stepIdsWithShipment.contains(thisIds)){
                stepIdsWithoutShipment.add(thisIds);
            }
        }
       return Database.getQueryLocator(
            'SELECT Id FROM Step__c WHERE Id IN: stepIdsWithoutShipment LIMIT 1'
        );
       
    }
    
    global void execute(Database.BatchableContext bc, List<Step__c> records){
        
        List<Log__c> logsList = new List<Log__c>();
        for(Step__c thisStep : records){
            try{
                if(!Test.isRunningTest()) GS_POST_TO_SAP.createAnAirwayBill(thisStep.Id);
            }
            Catch(Exception ex){
                 Log__c objLog = new Log__c();
                 objLog.Type__c = 'Shipment creation failed on step '+thisStep.Id;
                 objLog.Description__c = 'Error '+ex.getMessage();
                 logsList.add(objLog);      
            }
        }
      if(!logsList.isEmpty()) insert logsList;
    }  
    
    global void finish(Database.BatchableContext bc){
       
        AsyncApexJob job = [SELECT Id, 
                                   Status, 
                                   NumberOfErrors, 
                                   JobItemsProcessed
                            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()];
        
         System.debug(job + ' records processed.!');
        
    } 
    
    global void execute(SchedulableContext sc) {
        CreateAutomaticShipments b = new CreateAutomaticShipments ();
        database.executebatch(b,200);
   }

}