public class IntermmediateCRS_Ctrl{
    
     public Id recordId {get;set;}
    CRS_Form__c crs;
    
    public IntermmediateCRS_Ctrl(ApexPages.standardController cnt){
        recordId = cnt.getRecord().Id;
       
        
        
    }   
    
    public PageReference  Redirect(){
    
         CRS_Form__c crs = [Select id , recordtype.developerName, recordtypeId,Redirect_Descision_f__c from 
                            CRS_Form__c  where Id =: recordId];
         
         User userObj = [Select Id, Contactid from user where Id =: UserInfo.getUserID()];
         
         
         string URL = (string.isBlank(userObj.Contactid)) ? Label.Salesforce_URL +'/apex' : Label.Community_URL;
        
        if(crs.Redirect_Descision_f__c)
        {
            
            PageReference pageRef = new PageReference(URL+'/CRS_Submission_Form_mass?id='+recordId);
            return pageRef;
        }
            PageReference pageRef = new PageReference(URL +'/CRS_Submission_Form?id='+recordId);
            return pageRef;
        
       
    
    }               

}