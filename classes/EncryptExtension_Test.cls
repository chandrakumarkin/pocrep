/**************************************************
* Class Name :EncryptExtension_Test
* Description : Test class for Apex class -EncryptExtension
* Created Date:  14/08/2018 Assembla #5623    
***************************************************/
@isTest
public class EncryptExtension_Test
{
   static TestMethod void encrytDataTest(){
     test.startTest(); 
      Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
    
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='RORP_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
      
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        //objSR.Contact__c = objContact.Id;
        objSR.Legal_Structures__c ='LTD';
        insert objSR;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Step_No__c = 1.0;
        
        insert objStep; 
        
        Country_Codes__c cc= new Country_Codes__c();
        cc.Name = '971';
        cc.Code__c = 'AE';
        cc.Nationality__c='United Arab Emirates';
        insert cc;
        
        map<string,string> mapRecordType = new map<string,string>();
for(RecordType objRT : [select Id,DeveloperName from RecordType where SObjectType = 'Amendment__c' and DeveloperName IN ('UBO')]){
    mapRecordType.put(objRT.DeveloperName,objRT.Id);
}        
 Amendment__c amnd1 = new Amendment__c (RecordTypeId=mapRecordType.get('UBO'),
 ServiceRequest__c=objSR.id ,
 Amendment_Type__c = 'Individual' ,
 Type_of_Landlord__c = 'Individual', 
 Given_Name__c = 'shab' ,
 Family_Name__c = 'ahmed',
 Passport_No__c ='123490' , 
 Nationality_list__c = 'United Arab Emirates',
 Registration_No__c = '234353',
 BC_UBO_Type__c ='Regulated',
 Name_of_the_regulator__c ='UAE',
 Director_Appointed_Date__c = system.today().addMonths(2),
 //Related_Amendment_Populated__c ='',
 //Shareholder_Name__c ='',
 Title_new__c ='Mr',
 Gender__c ='Male',
 Date_of_Birth__c =system.today().addyears(-20),
 Place_of_Birth__c ='Chennai',
 Passport_Expiry_Date__c =system.today().addMonths(13),
 //Phone__c  ='971556751111',
 Contact_number_of_Caterer__c='+971543345345',
 Contact_Number_After_Office_Hours__c='+971543345345',
 Apt_or_Villa_No__c ='423432',
 Building_Name__c ='ABC',
 Street__c ='karama',
 PO_Box__c ='345435',
 Permanent_Native_City__c ='Dubai',
 Permanent_Native_Country__c ='United Arab Emirates',
 Emirate_State__c ='Dubai',
 Post_Code__c ='43543543',
 Person_Email__c ='ss@gmail.com',
 Company_Name__c ='ADC Corporation',
 Company_Type__c ='Financial - related',
 Relationship_Type__c ='UBO',
 Shareholder_Company__c =objAccount.Id,
 Shareholder_enjoy_direct_benefits__c =true,
 Mobile__c ='+971543434343',
 IsUBOSecured__c =false,
 Full_Name__c = 'Mohammed'
 );
 
 insert amnd1;
 
  
map<string,string> mapRecordType1 = new map<string,string>();
for(RecordType objRT : [select Id,DeveloperName from RecordType where SObjectType = 'UBO_Data__c' and DeveloperName IN ('Individual')]){
    mapRecordType1.put(objRT.DeveloperName,objRT.Id);
}        
UBO_Data__c uboData = new UBO_Data__c(
Nationality_New__c='India',
Date_of_Cessation__c= system.today(),
Status__c='Draft',
Passport_Number_New__c='+971543343234',
Email_New__c='ss@gmail.com',
Last_Name_New__c='MOhammed',
First_Name_New__c = 'Ashik',
RecordTypeid= mapRecordType1.get('Individual'),
Type__c='Direct',
Date_of_Birth__c=system.today().addyears(-20),
Gender__c='Male',
Passport_Expiry_Date__c=system.today().addyears(3),
Passport_Number__c='FGHFJ3242',
Email__c='ss@gmail.com',
Place_of_Birth__c='Chennai',
Title__c='Mr.',
Date_Of_Becoming_a_UBO__c=system.today(),
First_Name__c='Poonkuzhali',
Last_Name__c='Senthamil Selvi',
Passport_Data_of_Issuance__c=system.today().addyears(-1),
Entity_Name__c='ABC Corporation',
Date_Of_Registration__c=system.today().addyears(-1),
Name_of_Regulator_Exchange_Govt__c='UAE',
Select_Regulator_Exchange_Govt__c='Dubai',
Reason_for_exemption__c='Regulated by a recognised financial service provider other than DFSA',
Entity_Type__c='The individual has the right to appoint or remove the majority of the Directors of the Company',
Phone_Number__c='+971543343234',
Apartment_or_Villa_Number__c='3223',
Emirate_State_Province__c='Dubai',
Nationality__c='United Arab Emirates',
Building_Name__c='Raga',
Street_Address__c='business bay',
City_Town__c='Dubai',
Country__c='India',
PO_Box__c='23432432',
Post_Code__c='234243',
Service_Request__c = objSR.id
);

insert uboData;
        
    test.stopTest();     
        
                
      EncryptExtension.cryptoKey = Blob.valueOf('443ffdf'); 
      EncryptExtension.generateURLEncryptValue('ABC');
      string encrydata = EncryptExtension.generateEncryptValue('ABC');
      EncryptExtension.generateDecryptValue(encrydata);
      EncryptExtension.generateURLDecryptValue('ABC');
      //step__c stepRec = new Step__c();
      EncryptExtension.executeEncyrptionLogic(objStep.Id);
      EncryptExtension.executeUBODATAEncyrptionLogic(objStep.Id);
    }

}