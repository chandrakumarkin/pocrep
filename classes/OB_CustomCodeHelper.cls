/*
    Author      : Durga Prasad
    Date        : 17-Nov-2019
    Description : Custom code to create Lead or Community User on approval
    --------------------------------------------------------------------------------------
    v1.0  Durga  18-Feb-2020    Logic to copy SR Documents while deep cloning
    v1.1  Durga  26-Mar-2020    Logic to update the HexaBPM Doc's Sys Doc Id and status.
    v1.2  Durga  04-Apr-2020    Logic to copy Activities from Parent SR to Child SR.
    v1.3  Durga  08-Apr-2020    Logic to copy multiple sr documents from amendment
    v1.4  Prateek  05-May-2020   clearing out a field
*/
public without sharing class OB_CustomCodeHelper {
    
    /*
        Method Name :   CreateSR
        Description :   Future method to create SR as Draft based on input params
    */
    @future
    public static void CreateSR(string ParentSRId,string RecordTypeName){
        if(ParentSRId!=null && RecordTypeName!=null){
            string Result = OB_Create_Application(ParentSRId,RecordTypeName);
        }
    }
    
    public static string OB_Create_Application(string ParentSRId,string RecordTypeName){
        string strResult = 'Success';
      
        try{
            string OBGovernmentEntityApprovalRequired = '';
            map<string,string> mapGovEntityApprovalChecks = new map<string,string>();
            list<License_Activity__c> lstActivitiesTBU = new list<License_Activity__c>();
            for(HexaBPM__Service_Request__c objSR:[Select Id,(select Id,Name,Activity__r.OB_Government_Entity_Approval_Required__c from Activities__r where Activity__r.OB_Government_Entity_Approval_Required__c!=null) from HexaBPM__Service_Request__c where Id=:ParentSRId and RecordType.DeveloperName='In_Principle']){
                if(objSR.Activities__r!=null && objSR.Activities__r.size()>0){
                	//lstActivitiesTBU.addAll(objSR.Activities__r);
                    for(License_Activity__c LA:objSR.Activities__r){
                        mapGovEntityApprovalChecks.put(LA.Activity__r.OB_Government_Entity_Approval_Required__c,LA.Activity__r.OB_Government_Entity_Approval_Required__c);
                    }
                }
            }
            
            map<string,list<HexaBPM__SR_Doc__c>> MapSRDocContentIds = new map<string,list<HexaBPM__SR_Doc__c>>();//v1.3 changed type from map<string,HexaBPM__SR_Doc__c> to map<string,list<HexaBPM__SR_Doc__c>>
            map<string,HexaBPM__SR_Doc__c> MapInprincipleDocuments = new map<string,HexaBPM__SR_Doc__c>();
            for(HexaBPM__SR_Doc__c doc:[Select Id,Name,HexaBPM__Document_Master__c,
            HexaBPM__SR_Template_Doc__c,File_Name__c,HexaBPM_Amendment__c,
            HexaBPM__Customer__c,HexaBPM__Document_Description_External__c,
            HexaBPM__Status__c,HexaBPM__Doc_ID__c,HexaBPM__From_Finalize__c 
            from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:ParentSRId 
            and HexaBPM__Sys_IsGenerated_Doc__c=false and HexaBPM__Doc_ID__c!=null]){
               
                if(doc.HexaBPM_Amendment__c!=null){
                	list<HexaBPM__SR_Doc__c> lstSRDocs = new list<HexaBPM__SR_Doc__c>();//v1.3
                	if(MapSRDocContentIds.get(doc.HexaBPM_Amendment__c)!=null)
                		lstSRDocs = MapSRDocContentIds.get(doc.HexaBPM_Amendment__c);
                	lstSRDocs.add(doc);
                    MapSRDocContentIds.put(doc.HexaBPM_Amendment__c,lstSRDocs);//v1.3
                }else{
                    MapInprincipleDocuments.put(doc.HexaBPM__Document_Master__c,doc);
                }
            }
            system.debug('$$$$$$$$$ mapGovEntityApprovalChecks '+mapGovEntityApprovalChecks);
            if(mapGovEntityApprovalChecks.size()>0){
                OBGovernmentEntityApprovalRequired = string.join(mapGovEntityApprovalChecks.values(),';');
            }
            map<String, Schema.SObjectType>  m = Schema.getGlobalDescribe();
            SObjectType objtype;
            DescribeSObjectResult objDef1;
            map<string, sObjectField> fieldmap;
            if(m.get('HexaBPM__Service_Request__c')!= null){
                objtype = m.get('HexaBPM__Service_Request__c');
                objDef1 =  objtype.getDescribe();
                fieldmap =  objDef1.fields.getmap();
            }
            set<string> setFields = new set<string>();
            if(fieldmap!=null){
                for(Schema.SObjectField strFld:fieldmap.values()){
                    Schema.DescribeFieldResult fd = strFld.getDescribe();
                    if(fd.isCustom())
                        setFields.add(string.valueOf(strFld).toLowerCase());
                }
            }
            string SRqry =  'Select Name';
            if(setFields.size()>0){
                for(string srField : setFields){
                    SRqry += ','+srField;
                }
            }
            SRqry += ' From HexaBPM__Service_Request__c where Id=:ParentSRId';
            HexaBPM__Service_Request__c objDraftSR = new HexaBPM__Service_Request__c();
            string SRRecordTypeId;
            for(RecordType rt:[Select Id from RecordType where sobjectType='HexaBPM__Service_Request__c' and DeveloperName=:RecordTypeName and IsActive=true]){
                SRRecordTypeId = rt.Id;
            }
            system.debug('$$$$$$$$$ SRRecordTypeId '+SRRecordTypeId );
            for(HexaBPM__Service_Request__c objSR:database.query(SRqry)){
                objDraftSR = objSR.Clone();
                objDraftSR.HexaBPM__Parent_SR__c = ParentSRId;
                if(SRRecordTypeId!=null)
                    objDraftSR.recordTypeId = SRRecordTypeId;
                if(OBGovernmentEntityApprovalRequired!='')
                    objDraftSR.Government_Entity_Approval_Required__c = OBGovernmentEntityApprovalRequired;
                objDraftSR.HexaBPM__SR_Template__c = null;
                objDraftSR.HexaBPM__Internal_SR_Status__c = null;
                objDraftSR.HexaBPM__External_SR_Status__c = null;
                objDraftSR.I_Agree__c = false;
                objDraftSR.HexaBPM__Submitted_Date__c = null;
                objDraftSR.HexaBPM__Submitted_DateTime__c = null;
                objDraftSR.HexaBPM__Required_Docs_not_Uploaded__c = false;
                objDraftSR.HexaBPM__FinalizeAmendmentFlg__c = false;
                objDraftSR.Id = null;
                objDraftSR.Page_Tracker__c = '';
                //v1.4
                objDraftSR.HexaBPM__Closed_Date_Time__c = null;
            }
            if(String.ISNotBlank(RecordTypeName) && (RecordTypeName.equalsIgnorecase('AOR_NF_R') || RecordTypeName.equalsIgnorecase('AOR_F'))){
              	Boolean isSharingCompleted = false;
            	for(HexaBPM__SR_Doc__c docu: [Select Id,HexaBPM__Document_Master__c
                            From HexaBPM__SR_Doc__c
                            Where HexaBPM__Service_Request__c =: ParentSRId 
                            AND HexaBPM__Document_Master__r.HexaBPM__Code__c='NOC_SHARING'
                            Limit 1]){
            	isSharingCompleted = true;
            	}
	            if(!isSharingCompleted){
	              objDraftSR.Sharing_Space_with_Affiliated_Entity__c = '';
	              objDraftSR.Name_of_the_Hosting_Affiliate__c = null;
	            }
            }
            insert objDraftSR;
            
            //v1.2
            /*for(License_Activity__c LA:lstActivitiesTBU){
            	LA.Application__c = objDraftSR.Id;
            }
            update lstActivitiesTBU;*/
            
            String AccountId     = [SELECT Id,HexaBPM__Customer__c FROM HexaBPM__Service_Request__c WHERE Id =: ParentSRId].HexaBPM__Customer__c;
            Set<String> rejectedSrdId = new Set<String>(); 
            
            for(HexaBPM__Service_Request__c thisreq : [SELECT Id FROM HexaBPM__Service_Request__c WHERE HexaBPM__Customer__c =: AccountId AND RecordType.DeveloperName='In_Principle' AND (HexaBPM__External_Status_Code__c ='AOR_REJECTED' OR HexaBPM__External_Status_Code__c ='Rejected')]){
                rejectedSrdId.add(thisreq.Id);
            }
                                                       
            for(License_Activity__c LA:[select Id,Application__c from License_Activity__c where Application__c IN: rejectedSrdId OR Application__c =: ParentSRId]){
                LA.Application__c = objDraftSR.Id;
                lstActivitiesTBU.add(LA);
            }   
            
            system.debug('$$$$$$$$$$lstLA '+lstActivitiesTBU );
            if(lstActivitiesTBU.size()>0)
             update lstActivitiesTBU;
            
            system.debug('$$$$$$$$$ objDraftSR '+objDraftSR);
            if(m.get('HexaBPM_Amendment__c')!= null){
                objtype = m.get('HexaBPM_Amendment__c');
                objDef1 =  objtype.getDescribe();
                fieldmap =  objDef1.fields.getmap();
            }
            setFields = new set<string>();
            if(fieldmap!=null){
                for(Schema.SObjectField strFld:fieldmap.values()){
                    Schema.DescribeFieldResult fd = strFld.getDescribe();
                    if(fd.isCustom())
                        setFields.add(string.valueOf(strFld).toLowerCase());
                }
                setFields.add('recordtypeid');
            }
            string Amndqry =  'Select Id,Name';
            if(setFields.size()>0){
                for(string AmndField : setFields){
                    Amndqry += ','+AmndField;
                }
            }
            Amndqry += ' From HexaBPM_Amendment__c where ServiceRequest__c=:ParentSRId';
            list<HexaBPM_Amendment__c> lstAmendments = new list<HexaBPM_Amendment__c>();
            for(HexaBPM_Amendment__c Amnd:database.query(Amndqry)){
                HexaBPM_Amendment__c newAmnd = Amnd.Clone();
                newAmnd.Copied_from_Amendment__c = Amnd.Id;
                system.debug('############newAmnd '+newAmnd);
                newAmnd.ServiceRequest__c = objDraftSR.Id;
                lstAmendments.add(newAmnd);
            }
            
            if(m.get('Company_Name__c')!= null){
                objtype = m.get('Company_Name__c');
                objDef1 =  objtype.getDescribe();
                fieldmap =  objDef1.fields.getmap();
            }
            setFields = new set<string>();
            if(fieldmap!=null){
                for(Schema.SObjectField strFld:fieldmap.values()){
                    Schema.DescribeFieldResult fd = strFld.getDescribe();
                    if(fd.isCustom())
                        setFields.add(string.valueOf(strFld).toLowerCase());
                }
            }
            string CompanyNameQry =  'Select Name';
            if(setFields.size()>0){
                for(string AmndField : setFields){
                    CompanyNameQry += ','+AmndField;
                }
            }
            CompanyNameQry += ' From Company_Name__c where Application__c=:ParentSRId';
            
            list<Company_Name__c> lstCompanyNames = new list<Company_Name__c>();
            for(Company_Name__c CName:database.query(CompanyNameQry)){
                Company_Name__c CN = CName.Clone();
                CN.Application__c = objDraftSR.Id;
                lstCompanyNames.add(CN);
            }
            if(lstCompanyNames.size()>0)
                insert lstCompanyNames;
                
                
            system.debug('$$$$$$$$$ objDraftSR '+lstAmendments.size());
            map<string,string> MapOldAmendmentId = new map<string,string>();
            list<HexaBPM__SR_Doc__c> lstSRDocTBU = new list<HexaBPM__SR_Doc__c>();
            if(lstAmendments.size()>0){
                insert lstAmendments;
                for(HexaBPM_Amendment__c amnd:lstAmendments){
                    if(amnd.Copied_from_Amendment__c!=null && MapSRDocContentIds.get(amnd.Copied_from_Amendment__c)!=null){
                    	for(HexaBPM__SR_Doc__c objOldDoc:MapSRDocContentIds.get(amnd.Copied_from_Amendment__c)){//v1.3
	                        HexaBPM__SR_Doc__c objSRDocClone = objOldDoc.clone();
	                        objSRDocClone.HexaBPM_Amendment__c = amnd.Id;
	                        objSRDocClone.HexaBPM__Service_Request__c = objDraftSR.Id;
	                        objSRDocClone.HexaBPM__From_Finalize__c = true;
	                        objSRDocClone.In_Principle_Document__c = objOldDoc.Id;
	                        objSRDocClone.HexaBPM__Doc_ID__c = objOldDoc.HexaBPM__Doc_ID__c;
	                        objSRDocClone.File_Name__c = objOldDoc.File_Name__c;
	                        objSRDocClone.HexaBPM__Status__c = objOldDoc.HexaBPM__Status__c;
	                        lstSRDocTBU.add(objSRDocClone);
                    	}
                    }
                }
            }
            for(HexaBPM__SR_Doc__c doc:MapInprincipleDocuments.values()){
                HexaBPM__SR_Doc__c objSRDocClone = doc.clone();
                objSRDocClone.HexaBPM__From_Finalize__c = true;
                objSRDocClone.HexaBPM__Service_Request__c = objDraftSR.Id;
                objSRDocClone.In_Principle_Document__c = doc.Id;
                //objSRDocClone.HexaBPM__Doc_ID__c = MapSRDocContentIds.get(amnd.Copied_from_Amendment__c).HexaBPM__Doc_ID__c;
                //objSRDocClone.File_Name__c = MapSRDocContentIds.get(amnd.Copied_from_Amendment__c).File_Name__c;
                //objSRDocClone.HexaBPM__Status__c = MapSRDocContentIds.get(amnd.Copied_from_Amendment__c).HexaBPM__Status__c;
                lstSRDocTBU.add(objSRDocClone);
            }
            
            
            /*
            for(HexaBPM__SR_Doc__c doc:[Select Id,HexaBPM__Doc_ID__c,HexaBPM__Document_Master__c,File_Name__c,HexaBPM__Status__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:objDraftSR.Id and HexaBPM__Sys_IsGenerated_Doc__c=false]){
                if(MapSRDocContentIds.get(doc.HexaBPM__Document_Master__c)!=null){
                    doc.HexaBPM__Doc_ID__c = MapSRDocContentIds.get(doc.HexaBPM__Document_Master__c).HexaBPM__Doc_ID__c;
                    doc.File_Name__c = MapSRDocContentIds.get(doc.HexaBPM__Document_Master__c).File_Name__c;
                    doc.HexaBPM__Status__c = MapSRDocContentIds.get(doc.HexaBPM__Document_Master__c).HexaBPM__Status__c;
                    lstSRDocTBU.add(doc);
                }
            }
            */
            list<ContentDocumentLink> lstCDL = new list<ContentDocumentLink>();
            if(lstSRDocTBU.size()>0){
                upsert lstSRDocTBU;
                for(HexaBPM__SR_Doc__c srdocObj : lstSRDocTBU){
                    if(srdocObj.HexaBPM__Doc_ID__c != null){
                        ContentDocumentLink contDocLnkCloned = new ContentDocumentLink();
                        contDocLnkCloned.LinkedEntityId = srdocObj.id;
                        contDocLnkCloned.contentdocumentid = srdocObj.HexaBPM__Doc_ID__c;
                        contDocLnkCloned.ShareType = 'V';
                        lstCDL.add(contDocLnkCloned);
                    }                
                }
                system.debug('lstCDL' + lstCDL);
                if(lstCDL.size()>0){
                    insert lstCDL;
                }
            }
            
        }catch(Exception e){
            strResult = e.getMessage();
            system.debug('$$$$$$$$$$ '+e.getMessage() );
            system.debug('$$$$$$$$$$ '+e.getLineNumber());
            Log__c objLog = new Log__c();
            objLog.Description__c = 'Exception on OB_CustomCodeHelper.CreateSR() - Line:129 :'+e.getMessage();
            objLog.Type__c = 'Custom Code to create SR on Rejection';
            insert objLog;
        }
        return strResult;
    }
    
    /*
        Method Name :   SRDeepClone
        Description :   Method to Clone the SR on Rejection
    */
    public static string SRDeepClone(string SRId,string OpportunityId){
        string strResult = 'Success';
        if(SRId!=null){
            try{
                string NewOppId;
                if(OpportunityId!=null){
                    Opportunity objOpp = new Opportunity();
                    for(Opportunity opp:[Select Id,Name,AccountId,CloseDate,Type,Sector_Classification__c,OwnerId,RecordTypeId,Variation__c,Lead_ID__c from Opportunity where Id=:OpportunityId]){
                        objOpp = opp.clone();
                    
                    }
                    objOpp.StageName = 'Created';
                    insert objOpp;
                    NewOppId = objOpp.Id;
                }
                map<String, Schema.SObjectType>  m = Schema.getGlobalDescribe();
                SObjectType objtype;
                DescribeSObjectResult objDef1;
                map<String, SObjectField> fieldmap;
                if(m.get('HexaBPM__Service_Request__c')!= null){
                    objtype = m.get('HexaBPM__Service_Request__c');
                    objDef1 =  objtype.getDescribe();
                    fieldmap =  objDef1.fields.getmap();
                }
                set<string> setFields = new set<string>();
                if(fieldmap!=null){
                    for(Schema.SObjectField strFld:fieldmap.values()){
                        Schema.DescribeFieldResult fd = strFld.getDescribe();
                        if(fd.isCustom())
                            setFields.add(string.valueOf(strFld).toLowerCase());
                    }
                }
                setFields.add('id');
                setFields.add('recordtypeid');
                string SRqry =  'Select name';
                if(setFields.size()>0){
                    for(string srField : setFields){
                        SRqry += ','+srField;
                    }
                }
                SRqry += ' From HexaBPM__Service_Request__c where Id=:SRId';
                
                HexaBPM__Service_Request__c objDraftSR = new HexaBPM__Service_Request__c();
                for(HexaBPM__Service_Request__c objSR:database.query(SRqry)){
                    objDraftSR = objSR.Clone();
                    objDraftSR.HexaBPM__Parent_SR__c = SRId;
                    objDraftSR.HexaBPM__SR_Template__c = null;
                    objDraftSR.HexaBPM__Internal_SR_Status__c = null;
                    objDraftSR.HexaBPM__External_SR_Status__c = null;
                    objDraftSR.I_Agree__c = false;
                    objDraftSR.HexaBPM__Submitted_Date__c = null;
                    objDraftSR.HexaBPM__Submitted_DateTime__c = null;
                    objDraftSR.HexaBPM__Required_Docs_not_Uploaded__c = false;
                    objDraftSR.HexaBPM__FinalizeAmendmentFlg__c = false;
                }
                if(NewOppId!=null)
                    objDraftSR.Opportunity__c = NewOppId;
                insert objDraftSR;
                
                map<string,HexaBPM__SR_Doc__c> MapNewSRDocs = new map<string,HexaBPM__SR_Doc__c>();//v1.1
                for(HexaBPM__SR_Doc__c SRDoc:[Select Id,HexaBPM__Document_Master__c,File_Name__c,HexaBPM__Status__c,HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRId and HexaBPM__Sys_IsGenerated_Doc__c=false]){
                	MapNewSRDocs.put(SRDoc.HexaBPM__Document_Master__c,SRDoc);//v1.1
                }
                
                if(m.get('HexaBPM_Amendment__c')!= null){
                    objtype = m.get('HexaBPM_Amendment__c');
                    objDef1 =  objtype.getDescribe();
                    fieldmap =  objDef1.fields.getmap();
                }
                setFields = new set<string>();
                if(fieldmap!=null){
                    for(Schema.SObjectField strFld:fieldmap.values()){
                        Schema.DescribeFieldResult fd = strFld.getDescribe();
                        if(fd.isCustom())
                            setFields.add(string.valueOf(strFld).toLowerCase());
                    }
                }
                string Amndqry =  'Select Id,Name,recordTypeId';
                if(setFields.size()>0){
                    for(string AmndField : setFields){
                        Amndqry += ','+AmndField;
                    }
                }
                Amndqry += ' From HexaBPM_Amendment__c where ServiceRequest__c=:SRId';
                
                list<HexaBPM_Amendment__c> lstAmendments = new list<HexaBPM_Amendment__c>();
                for(HexaBPM_Amendment__c Amnd:database.query(Amndqry)){
                    HexaBPM_Amendment__c newAmnd = Amnd.Clone();
                    newAmnd.Copied_from_Amendment__c = Amnd.Id;
                    newAmnd.ServiceRequest__c = objDraftSR.Id;
                    lstAmendments.add(newAmnd);
                }
                //v1.0
                map<string,list<HexaBPM__SR_Doc__c>> MapSRDocContentIds = new map<string,list<HexaBPM__SR_Doc__c>>();//v1.3
                map<string,HexaBPM__SR_Doc__c> MapInprincipleDocuments = new map<string,HexaBPM__SR_Doc__c>();
                for(HexaBPM__SR_Doc__c doc:[Select Id,Name,HexaBPM__Document_Master__c,HexaBPM__SR_Template_Doc__c,File_Name__c,HexaBPM_Amendment__c,HexaBPM__Customer__c,HexaBPM__Document_Description_External__c,HexaBPM__Status__c,HexaBPM__Doc_ID__c,HexaBPM__From_Finalize__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRId and HexaBPM__Sys_IsGenerated_Doc__c=false and HexaBPM__Doc_ID__c!=null]){
                    list<HexaBPM__SR_Doc__c> lstSRDocs = new list<HexaBPM__SR_Doc__c>();
                    if(doc.HexaBPM_Amendment__c!=null){
                    	if(MapSRDocContentIds.get(doc.HexaBPM_Amendment__c)!=null)//v1.3
                    		lstSRDocs = MapSRDocContentIds.get(doc.HexaBPM_Amendment__c);
                    	lstSRDocs.add(doc);
                        MapSRDocContentIds.put(doc.HexaBPM_Amendment__c,lstSRDocs);//v1.3
                    }else{
                        MapInprincipleDocuments.put(doc.HexaBPM__Document_Master__c,doc);
                    }
                }
                list<HexaBPM__SR_Doc__c> lstSRDocTBU = new list<HexaBPM__SR_Doc__c>();
                if(lstAmendments.size()>0){//v1.0
                    insert lstAmendments;
                  for(HexaBPM_Amendment__c amnd:lstAmendments){//v1.0
                        if(amnd.Copied_from_Amendment__c!=null && MapSRDocContentIds.get(amnd.Copied_from_Amendment__c)!=null){
                        	for(HexaBPM__SR_Doc__c objOldDoc:MapSRDocContentIds.get(amnd.Copied_from_Amendment__c)){//v1.3
	                            HexaBPM__SR_Doc__c objSRDocClone = objOldDoc.clone();
	                            objSRDocClone.HexaBPM_Amendment__c = amnd.Id;
	                            objSRDocClone.HexaBPM__Service_Request__c = objDraftSR.Id;
	                            objSRDocClone.HexaBPM__From_Finalize__c = true;
	                            objSRDocClone.In_Principle_Document__c = objOldDoc.Id;
	                            objSRDocClone.HexaBPM__Doc_ID__c = objOldDoc.HexaBPM__Doc_ID__c;
	                            objSRDocClone.File_Name__c = objOldDoc.File_Name__c;
	                            objSRDocClone.HexaBPM__Status__c = objOldDoc.HexaBPM__Status__c;
	                            lstSRDocTBU.add(objSRDocClone);
                        	}
                        }
                    }
                }
                for(HexaBPM__SR_Doc__c doc:MapNewSRDocs.values()){//v1.1
                	if(MapInprincipleDocuments.get(doc.HexaBPM__Document_Master__c)!=null){
                		doc.File_Name__c = MapInprincipleDocuments.get(doc.HexaBPM__Document_Master__c).File_Name__c;
                		doc.HexaBPM__Status__c = MapInprincipleDocuments.get(doc.HexaBPM__Document_Master__c).HexaBPM__Status__c;
                		doc.HexaBPM__Doc_ID__c = MapInprincipleDocuments.get(doc.HexaBPM__Document_Master__c).HexaBPM__Doc_ID__c;
                		lstSRDocTBU.add(doc);
                	}
                }
                /*
                for(HexaBPM__SR_Doc__c doc:MapInprincipleDocuments.values()){//v1.0
                    HexaBPM__SR_Doc__c objSRDocClone = doc.clone();
                    objSRDocClone.HexaBPM__From_Finalize__c = true;
                    objSRDocClone.HexaBPM__Service_Request__c = objDraftSR.Id;
                    objSRDocClone.In_Principle_Document__c = doc.Id;
                    lstSRDocTBU.add(objSRDocClone);
                }
                */
                 if(m.get('Company_Name__c')!= null){
                    objtype = m.get('Company_Name__c');
                    objDef1 =  objtype.getDescribe();
                    fieldmap =  objDef1.fields.getmap();
                }
                setFields = new set<string>();
                if(fieldmap!=null){
                    for(Schema.SObjectField strFld:fieldmap.values()){
                        Schema.DescribeFieldResult fd = strFld.getDescribe();
                        if(fd.isCustom())
                            setFields.add(string.valueOf(strFld).toLowerCase());
                    }
                }
                string CompanyNameQry =  'Select Name';
                if(setFields.size()>0){
                    for(string AmndField : setFields){
                        CompanyNameQry += ','+AmndField;
                    }
                }
                CompanyNameQry += ' From Company_Name__c where Application__c=:SRId';
                
                list<Company_Name__c> lstCompanyNames = new list<Company_Name__c>();
                for(Company_Name__c CName:database.query(CompanyNameQry)){
                    Company_Name__c CN = CName.Clone();
                    CN.Application__c = objDraftSR.Id;
                    lstCompanyNames.add(CN);
                }
                system.debug('company nanems ' +lstCompanyNames );
                if(lstCompanyNames.size()>0)
                    insert lstCompanyNames;
                
                
                list<License_Activity__c> lstLA = new list<License_Activity__c>();
                for(License_Activity__c LA:[select Id,Application__c from License_Activity__c where Application__c=:SRId ]){
                    LA.Application__c = objDraftSR.Id;
                    lstLA.add(LA);
                }
                 system.debug('$$$$$$$$$$lstLA '+lstLA );
                if(lstLA.size()>0)
                    update lstLA;
                
                 list<ContentDocumentLink> lstCDL = new list<ContentDocumentLink>();
                if(lstSRDocTBU.size()>0){
                    upsert lstSRDocTBU;//v1.0
                    for(HexaBPM__SR_Doc__c srdocObj : lstSRDocTBU){
                        if(srdocObj.HexaBPM__Doc_ID__c != null){
                            ContentDocumentLink contDocLnkCloned = new ContentDocumentLink();
                            contDocLnkCloned.LinkedEntityId = srdocObj.id;
                            contDocLnkCloned.contentdocumentid = srdocObj.HexaBPM__Doc_ID__c;
                            contDocLnkCloned.ShareType = 'V';
                            lstCDL.add(contDocLnkCloned);
                        }                
                    }
                    system.debug('lstCDL' + lstCDL);
                    if(lstCDL.size()>0){
                        insert lstCDL;
                    }
                }

                
            }catch(Exception e){
                strResult = e.getMessage()+'';
                system.debug('$$$$$$$$$$ '+e.getMessage() );
                system.debug('$$$$$$$$$$ '+e.getLineNumber());
                Log__c objLog = new Log__c();
                objLog.Description__c = 'Exception on OB_CustomCodeHelper.CreateSR() - Line:129 :'+e.getMessage();
                objLog.Type__c = 'Custom Code to create SR on Rejection Rejection';
                insert objLog;
            }
        }
        system.debug('strResult===>'+strResult);
        return strResult;
    }
}