@isTest
public class OB_NamingConventionTest {
    public static testmethod void test1(){
        Account acc = OB_PaymentModeController.getBpNumberFromAccount();
        String rectypeId = OB_PaymentModeController.getRecordTypeId();
        List<String> countries = OB_PaymentModeController.getCountries();
         Map<String,String> countryvals = OB_PaymentModeController.getCountryValues();
        Id recId = OB_PaymentModeController.getRecTypeId(); 
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Rejected','Draft','Submitted'}, new List<string> {'REJECTED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle', 'Name_Check'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        insert insertNewSRs;
        insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs[0].id;
        update insertNewSRs;
        
        List<Company_Name__c> companies = OB_TestDataFactory.createCompanyName(new List<String>{'Com1','Com2'});
        companies[0].Application__c = insertNewSRs[0].id;
        companies[0].Status__c = 'Approved';
        companies[0].OB_Is_Trade_Name_Allowed__c = 'yes';
        insert companies;
            
        test.startTest();
            OB_NamingConvention.checkName();
            OB_NamingConvention.getEntityDetails(insertNewSRs[0].id);
            OB_NamingConvention.getNameCheckDetail(insertNewSRs[0].id);
            OB_NamingConvention.validateName('Ignore difc',insertNewSRs[0].id);
            OB_NamingConvention.returnRecordsCompanyName(insertNewSRs[0].id);
            OB_NamingConvention.returnCompanyDetail(insertNewSRs[0].id,companies[0].id);
            OB_NamingConvention.deleteCompanyName(companies[1].id);
            OB_NamingConvention.updateSR(insertNewSRs[0], 500.00);
            
            Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
                Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
                insert con;  
                          
                User user = new User(alias = 'test123', email='test123@noemail.com',
                        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                        localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                        ContactId = con.Id,
                        timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
               
                insert user;
            system.runAs(user){
                OB_NamingConvention.getAccountId(user.id);
                OB_NamingConvention.retrieveCompanyWrapper(insertNewSRs[0].id,listPage[0].id,companies[0].id);
                OB_NamingConvention.createNameReservationSR(insertNewSRs[0].id);
                OB_NamingConvention.translateToArabic('test');
                OB_NamingConvention.commitNameApex(insertNewSRs[0].id,listPage[0].id,companies[0],new Map<string,string>());
                OB_NamingConvention.viewSRDocs(insertNewSRs[0].id,companies[0].id);
                OB_NamingConvention.createReceiptRecord(500.88,'Card');
                OB_NamingConvention.updateTradingName('test',companies[0].id,'warning',insertNewSRs[0].id);
                OB_NamingConvention.getselectOptions(new Account(), 'shippingcountry');
                OB_NamingConvention.getButtonAction(insertNewSRs[0].id,listPage[0].id,null,insertNewSRs[0]);
                //OB_NamingConvention.addEntityName('test',companies[0].id,' String ', insertNewSRs[0].id, 'string ', null, 'relationText', new map<string,string>());
                
                //OB_AccountOverViewController.RequestWrap reqwrap= new OB_AccountOverViewController.RequestWrap();
                //reqwrap.userId = user.id;
                
                //OB_AccountOverViewController.getConDetails();
            }
        test.stopTest();
    }
    
     public static testmethod void test2(){
        Account acc = OB_PaymentModeController.getBpNumberFromAccount();
        String rectypeId = OB_PaymentModeController.getRecordTypeId();
        List<String> countries = OB_PaymentModeController.getCountries();
         Map<String,String> countryvals = OB_PaymentModeController.getCountryValues();
        Id recId = OB_PaymentModeController.getRecTypeId(); 
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Rejected','Draft','Submitted'}, new List<string> {'REJECTED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle', 'Name_Check'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        insert insertNewSRs;
        insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs[0].id;
        update insertNewSRs;
        
        List<Company_Name__c> companies = OB_TestDataFactory.createCompanyName(new List<String>{'Com1','Com2'});
        companies[0].Application__c = insertNewSRs[0].id;
        companies[0].Status__c = 'Approved';
        companies[0].OB_Is_Trade_Name_Allowed__c = 'yes';
        insert companies;
            
        test.startTest();
            OB_NamingConvention.checkName();
            OB_NamingConvention.getEntityDetails(insertNewSRs[0].id);
            OB_NamingConvention.getNameCheckDetail(insertNewSRs[0].id);
            OB_NamingConvention.validateName('Ignore difc',insertNewSRs[0].id);
            OB_NamingConvention.returnRecordsCompanyName(insertNewSRs[0].id);
            OB_NamingConvention.returnCompanyDetail(insertNewSRs[0].id,companies[0].id);
            OB_NamingConvention.deleteCompanyName(companies[1].id);
            OB_NamingConvention.updateSR(insertNewSRs[0], 500.00);
            
            Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
                Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
                insert con;  
                          
                User user = new User(alias = 'test123', email='test123@noemail.com',
                        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                        localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                        ContactId = con.Id,
                        timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
               
                insert user;
            system.runAs(user){
            
                OB_NamingConvention.addEntityName('test',companies[0].id,' String ', insertNewSRs[0].id, 'string ', null, 'relationText', new map<string,string>());
                
                //OB_AccountOverViewController.RequestWrap reqwrap= new OB_AccountOverViewController.RequestWrap();
                //reqwrap.userId = user.id;
                
                //OB_AccountOverViewController.getConDetails();
            }
        test.stopTest();
    }
}