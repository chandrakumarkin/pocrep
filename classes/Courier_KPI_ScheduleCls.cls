/*************************************************************************************************
 *  Name        : Courier_KPI_ScheduleCls
 *  Author      : Veera 
 *  Company     : VRK IT
 *  Date        : 21-Jun-2021    
 *  Purpose     : This class is to calculate Aramex durations on step
**************************************************************************************************
Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------             
 
 ----------------------------------------------------------------------------------------*/
global without sharing class Courier_KPI_ScheduleCls implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new Courier_KPI_BatchCls(),100);
        
    }
  
}