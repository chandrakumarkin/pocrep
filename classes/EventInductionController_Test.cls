@isTest
public class EventInductionController_Test {
	
     static testmethod void TestPortalFitoutServicesEvents(){
        
        List<SR_Status__c> testSrStatusList = new List<SR_Status__c>();
            
        testSrStatusList.add(new SR_Status__c(Name = 'Approved',Code__c = 'APPROVED'));
        testSrStatusList.add(new SR_Status__c(Name = 'Induction Approved',Code__c = 'Induction_Approved'));
        
        insert testSrStatusList;
        
        insert new Status__c(Name='Approved',Code__c='APPROVED');
        
        /* Create a new account */
        Account newPersonAccount = new Account(Name='xyz', ROC_Status__c='Active', Legal_Type_of_Entity__c='All;LLP;LTD;');
        insert newPersonAccount;
        
        Account newContractor = new Account(Name='wxyz', ROC_Status__c='Active', Legal_Type_of_Entity__c='All;LLP;LTD;');
        insert newContractor;
        
        /* Create a new Contact */
        Contact cont = new Contact(FirstName='test1',LastName='test2',Email='test.1@example.com',Role__c = 'Event Services',Contractor__c = newContractor.Id);
        cont.AccountId = newPersonAccount.Id;
        cont.Contractor__c = newContractor.ID;
        
        insert new Lookup__c(DNRD_Code__c = '101',Name='United Arab Emirates',Type__c='Nationality',Code__c='AE');
        insert new CountryCodes__c(Name='971');
        
        insert cont;
        
        Profile testProfile = [SELECT Id 
                               FROM profile
                               WHERE Name = 'DIFC Customer Community User Custom' 
                               LIMIT 1];
        
        User testUser;
        
        System.runAs(new User(Id = UserInfo.getUserId())){
            
            testUser = new User(LastName = 'test2', 
                                 Username = 'test.user.1fo@example.com', 
                                 Email = 'test.1fo@example.com', 
                                 Alias = 'testu1fo', 
                                 TimeZoneSidKey = 'GMT', 
                                 LocaleSidKey = 'en_GB', 
                                 EmailEncodingKey = 'ISO-8859-1', 
                                 ProfileId = testProfile.Id, 
                                 ContactId = cont.Id,
                                 Community_User_Role__c='IT Services;Marketing Services;Company Services;Fit-Out Services;Property Services',
                                 LanguageLocaleKey = 'en_US');     
            insert testUser;
            
        }
        
        /* Create a test building */
        Building__c testBuilding = Test_CC_FitOutCustomCode_DataFactory.getTestBuilding();
        
        /* Create a test building */
        insert testBuilding; 
        
         SR_Template__c service_6 = new SR_Template__c(Name='Request for Contractor Wallet', Menu__c='Property Services', SR_Group__c='Fit-Out & Events',
                                    Menutext__c='Request for Contractor Wallet', Sub_menu__c='Company Registration', 
                                    SR_RecordType_API_Name__c='Contractor_Registration', Active__c=true, Available_for_menu__c=true,
                                    Available_For_Inactive_user__c='Available to both');
         insert service_6;
        /* Create an event request for contractor access */
        Service_Request__c srObj = new Service_Request__c();
        
        srObj.Additional_Details__c = 'Test Additional Details';
        srObj.Approving_Authority__c = 'Government of Dubai';
        srObj.SR_Template__c = service_6.id;
        //srObj.Building__c = testBuilding.Id;
        srObj.Clean_up_End_Time__c = System.Now() + 2;
        srObj.Courier_Cell_Phone__c = '+971 12345678';
        srObj.Current_Address_Country__c = 'United Arab Emirates';
        srObj.Event_Brief__c = 'Test Brief';
        srObj.Event_End_Date__c = System.Today() + 2;
        srObj.Event_Name__c = 'Test Event';
        srObj.Event_Start_Date__c = System.Today() + 1;
        srObj.Expiry_Date__c = System.Today() + Test_CC_FitOutCustomCode_DataFactory.FUTURE_DAYS_SIZE;
        srObj.Location__c =  'Other Test Location';
        srObj.Name_of_Contact_Person__c = 'Test Contact Person';
        srObj.Number_of_Participants__c = 2;
        srObj.Number_of_Spectators__c = 2;
        srObj.Set_up_Begin_Time__c = System.Now() + 1;
        srObj.Customer__c = newPersonAccount.Id;
        
        Test_CC_FitOutCustomCode_DataFactory.setServiceRequestStatus(srObj,'Approved',false);
        
        system.runAs(testUser){
            
            Test.startTest();
        
            insert srObj;
            
            srObj = [SELECT ID FROM Service_Request__c WHERE ID =:srObj.ID];
            
            runFutureTestPortalFitoutServicesEvents(srObj.Id,newPersonAccount.Id);
            
            Test.stopTest();
        }
        
    }
    
    @future
    public static void runFutureTestPortalFitoutServicesEvents(String srObjId, String accountId){
        
        /* Get a sample service request */
        Service_Request__c testSR = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        testSr.Customer__c = accountId;
        
        insert testSr;
        
        Map<String,Id> srTemplateMap = new Map<String,Id>();
            
        SR_Template__c service_1 = new SR_Template__c(Name='Fit Out Service Request', Menu__c='Property Services', SR_Group__c='Fit-Out & Events',
                                    Menutext__c='Application of Registration', Sub_menu__c='Company Registration', 
                                    SR_RecordType_API_Name__c='Fit_Out_Service_Request', Active__c=true, Available_for_menu__c=true,
                                    Available_For_Inactive_user__c='Available to both');
                                    
        SR_Template__c service_2 = new SR_Template__c(Name='Event_Induction_Request ', Menu__c='Property Services', SR_Group__c='Fit-Out & Events',
                                    Menutext__c='Application of Registration', Sub_menu__c='Company Registration', 
                                    SR_RecordType_API_Name__c='Event_Induction_Request ', Active__c=true, Available_for_menu__c=true,
                                    Available_For_Inactive_user__c='Available to both');
                
        SR_Template__c service_3 = new SR_Template__c(Name='License Renewal', Menu__c='Property Services', SR_Group__c='Fit-Out & Events',
                                    Menutext__c='License Renewal', Sub_menu__c='Corporate Actions', 
                                    SR_RecordType_API_Name__c='License_Renewal', Active__c=true, Available_for_menu__c=true,
                                    Available_For_Inactive_user__c='Available to active user');    
                                    
        SR_Template__C service_4 = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Fit - Out Induction Request','Fit_Out_Induction_Request','Fit-Out & Events');
        
        SR_Template__c service_5 = new SR_Template__c(Name='Event_Service_Request ', Menu__c='Property Services', SR_Group__c='Fit-Out & Events',
                                    Menutext__c='Application of Registration', Sub_menu__c='Company Registration', 
                                    SR_RecordType_API_Name__c='Event_Service_Request ', Active__c=true, Available_for_menu__c=true,
                                    Available_For_Inactive_user__c='Available to both'); 
        SR_Template__c service_6 = new SR_Template__c(Name='Request for Contractor Wallet', Menu__c='Property Services', SR_Group__c='Fit-Out & Events',
                                    Menutext__c='Request for Contractor Wallet', Sub_menu__c='Company Registration', 
                                    SR_RecordType_API_Name__c='Contractor_Registration', Active__c=true, Available_for_menu__c=true,
                                    Available_For_Inactive_user__c='Available to both');
                                    
        List<SR_Template__c>services = new List<SR_Template__c>();
        
        services.add(service_1);
        services.add(service_2);
        services.add(service_3);
        services.add(service_4);
        
        insert services;
        
        for(SR_Template__c service : services){
            srTemplateMap.put(service.SR_RecordType_API_Name__c,service.Id);
        }
    
        Service_Request__c testSr3 = testSr.clone(false,true,false,false);
        
        testSr3.linked_sr__c = srObjId;
        testSr3.SR_Template__c = srTemplateMap.get('Event_Induction_Request');
        testSr3.Building__c = null;
        
        Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(testSr3,'Event_Induction_Request');
        ApexPages.currentPage().getParameters().put('RecordType',Test_CC_FitOutCustomCode_DataFactory.getRecordTypeId('Event_Service_Request','Service_Request__c'));
        Test_CC_FitOutCustomCode_DataFactory.setServiceRequestStatus(testSr3,'Induction Approved',false);
        
        insert testSr3;
        
        Service_Request__c testSr4 = testSr3.clone(false,true,false,false);
        
        testSr4.SR_Template__c = srTemplateMap.get('Event_Service_Request');
        testSr4.Linked_Sr__c = testSr3.Id;
        
        Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(testSr4,'Event_Service_Request');
        
        insert testSr4;
        EventInductionController inducionCtrl = new EventInductionController();
        inducionCtrl.saveSRrequest();
        inducionCtrl.submitSRrequest();
        inducionCtrl.cancelRequest();    
    }
    
}