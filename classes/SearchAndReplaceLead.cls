global class SearchAndReplaceLead implements Database.Batchable < sObject > {

    global final String Query;
    global List<sObject> passedRecords= new List<sObject>();


    global SearchAndReplaceLead(String q,List<sObject> listofRecords) {
        passedRecords = listofRecords;
        Query = q;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(query != '')return Database.getQueryLocator(query);
        else return DataBase.getQueryLocator([Select id,Email,MobilePhone,Phone,Alternative_Phone_No__c from Lead WHERE Id IN: passedRecords and Id != null]);
    }

    global void execute(Database.BatchableContext BC, List < Lead > scope) {
        
        for (Lead leadRec: scope) {

            leadRec.Email ='test@difc.ae.invalid';
            leadRec.Phone = leadRec.Phone != NULl ? '+971569803616': leadRec.Phone;
            leadRec.MobilePhone = leadRec.MobilePhone != NULl ? '+971569803616': leadRec.MobilePhone;
            leadRec.Alternative_Phone_No__c = leadRec.Alternative_Phone_No__c != NULl ? '+971569803616': leadRec.Alternative_Phone_No__c;
        }
        system.debug('scope---'+scope);
        Database.SaveResult[] srList = Database.update(scope, false);
        List<Log__c> logRecords = new List<Log__c>();
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                for(Database.Error err : sr.getErrors()) {
                    string errorresult = err.getMessage();
                    logRecords.add(new Log__c(
                      Type__c = 'SearchAndReplaceLead',
                      Description__c = 'Exception is : '+errorresult+'=Lead Id==>'+sr.getId()
                    ));
                    System.debug('The following error has occurred.');                    
                }
            }
        }
        if(logRecords.size()>0)insert logRecords;
    }

    global void finish(Database.BatchableContext BC) {}
}