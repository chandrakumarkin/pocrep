/******************************************************************************************
 *  Author      : Durga Prasad
 *  Company     : HexaBPM
 *  Date        : 18-Dec-2019
 *  Description : Component controller to show the Risk Score of Application
 ********************************************************************************************
 **Version History**
 *  Author      Version         Description
    Rajil       V1.1            Updates the SR Risk Rating if there is a difference in the Risk Score
*******************************************************************************************/
public without sharing class OB_RiskMatrixController {
    @AuraEnabled   
    public static RiskMatrix CalculateRisk(string SRID){
        RiskMatrix objRiskMatrix = new RiskMatrix();
        if(SRID!=null){
            objRiskMatrix.hasDNFPB = OB_RiskMatrixHelper.hasDNFPB(SRID);
            objRiskMatrix.hasIndPEP = OB_RiskMatrixHelper.hasIndividualPEP(SRID);
            objRiskMatrix.hasDIFCRegisteredBodyCorporate = OB_RiskMatrixHelper.hasDIFCRegisteredBodyCorporate(SRID);
            objRiskMatrix.IsRecgExchange_FinServRegulator = OB_RiskMatrixHelper.hasRecExch_FinSecRegulator(SRID);
            
            boolean checkAdditionalConditions = true;
            if(objRiskMatrix.hasDNFPB || objRiskMatrix.hasIndPEP || objRiskMatrix.hasDIFCRegisteredBodyCorporate || objRiskMatrix.IsRecgExchange_FinServRegulator) {
                checkAdditionalConditions = false;
                if(objRiskMatrix.hasDNFPB){
                    objRiskMatrix.FinalRisk = 'Not Applicable';
                    objRiskMatrix.TotalRating = 0;
                }else{
                    if(objRiskMatrix.hasIndPEP) {
                        objRiskMatrix.FinalRisk = 'High';
                        objRiskMatrix.TotalRating = 20;
                    }else {
                        objRiskMatrix.FinalRisk = 'Low';
                        objRiskMatrix.TotalRating = 5;
                    }
                }
            }
            if(checkAdditionalConditions){
            	objRiskMatrix.checkOtherConditions = true;
            	
                decimal TotalPercentage = 0;

                objRiskMatrix.LicenseHighestRiskRate = OB_RiskMatrixHelper.LicenseHighestRiskRate(SRID);

                system.debug('LicenseHighestRating==>' + objRiskMatrix.LicenseHighestRiskRate);

                objRiskMatrix.AmendmentHighestRiskRate = OB_RiskMatrixHelper.AmendmentHighestRiskRate(SRID);

                system.debug('AmendmentHighestRiskRate==>' + objRiskMatrix.AmendmentHighestRiskRate);

                objRiskMatrix.AmendmentIncomeSourceHighestRiskRate = OB_RiskMatrixHelper.AmendmentIncomeSourceHighestRiskRate(SRID);

                system.debug('AmendmentIncomeSourceHighestRiskRate==>' + objRiskMatrix.AmendmentIncomeSourceHighestRiskRate);
                
                objRiskMatrix.HighestCS_ParBA_DistChannelCountryScoreRate = OB_RiskMatrixHelper.OtherRiskRateTotal(SRID);

                system.debug('HighestCS_ParBA_DistChannelCountryScoreRate==>' + objRiskMatrix.HighestCS_ParBA_DistChannelCountryScoreRate);

                TotalPercentage = objRiskMatrix.LicenseHighestRiskRate + objRiskMatrix.AmendmentHighestRiskRate + objRiskMatrix.HighestCS_ParBA_DistChannelCountryScoreRate + objRiskMatrix.AmendmentIncomeSourceHighestRiskRate;

                system.debug('TotalPercentage==>' + TotalPercentage);
				objRiskMatrix.TotalRating = TotalPercentage;
                if(TotalPercentage <= 30)
                    objRiskMatrix.FinalRisk = 'Low';
                else if(TotalPercentage > 30 && TotalPercentage <= 70)
                    objRiskMatrix.FinalRisk = 'Medium';
                else 
                    objRiskMatrix.FinalRisk = 'High';
            }
                    System.debug('srRecord: '+objRiskMatrix.FinalRisk);
            //V1.1: Updates the SR Risk Rating if there is a difference in the Risk Score
            for(HexaBPM__Service_Request__c srRecord : [select Risk_Percentage__c from HexaBPM__Service_Request__c where id = :SRID]){
                if(srRecord.Risk_Percentage__c != null && srRecord.Risk_Percentage__c != objRiskMatrix.TotalRating){
                    srRecord.Risk_Percentage__c = objRiskMatrix.TotalRating;
                    srRecord.Risk__c = objRiskMatrix.FinalRisk;
                    update srRecord;
                }  
            }
        }
        return objRiskMatrix;
    }
    
    public class RiskMatrix{
        @AuraEnabled public boolean hasDNFPB{get;set;}
        @AuraEnabled public boolean hasIndPEP{get;set;}
        @AuraEnabled public boolean hasDIFCRegisteredBodyCorporate{get;set;}
        @AuraEnabled public boolean IsRecgExchange_FinServRegulator{get;set;}
        
        @AuraEnabled public boolean checkOtherConditions{get;set;}
        
        @AuraEnabled public decimal LicenseHighestRiskRate{get;set;}
        @AuraEnabled public decimal AmendmentHighestRiskRate{get;set;}
        @AuraEnabled public decimal AmendmentIncomeSourceHighestRiskRate{get;set;}
        @AuraEnabled public decimal HighestCS_ParBA_DistChannelCountryScoreRate{get;set;}
        
        @AuraEnabled public decimal TotalRating{get;set;}
        @AuraEnabled public string FinalRisk{get;set;}
        
        public RiskMatrix(){
            hasDNFPB = false;
            hasIndPEP = false;
            hasDIFCRegisteredBodyCorporate = false;
            IsRecgExchange_FinServRegulator = false;
            checkOtherConditions = false;
            
            LicenseHighestRiskRate = 0;
            AmendmentHighestRiskRate = 0;
            AmendmentIncomeSourceHighestRiskRate = 0;
            HighestCS_ParBA_DistChannelCountryScoreRate = 0;
            TotalRating = 0;
        }
    }
}