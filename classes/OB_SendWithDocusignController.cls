/**
 * Description : Controller for OB_SendWithDocusign component
 *
 * ****************************************************************************************
 * History :
 * [06.FEB.2020] Prateek Kadkol - Code Creation
 */
global  without sharing class OB_SendWithDocusignController {

	public OB_SendWithDocusignController() {
	
	}
	
	@AuraEnabled
	public static RespondWrap checkRender(String requestWrapParam)  {


	
			//declaration of wrapper
			RequestWrap reqWrap = new RequestWrap();
			RespondWrap respWrap =  new RespondWrap();
			
			//deseriliaze.
			reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
	
			respWrap.renderButton = false;

			
			
			for(HexaBPM__Step__c stepOBj : [SELECT ID FROM HexaBPM__Step__c WHERE Step_Template_Code__c = 'ENTITY_SIGNING' 
			AND HexaBPM__Status_Type__c != 'END' AND HexaBPM__SR__c =:reqWrap.srId  LIMIT 1]){
				respWrap.relActionIteam = stepOBj;
				respWrap.renderButton = true;
			}
			return respWrap;
	}
			
	
	@AuraEnabled
	public static RespondWrap fetchDocusignData(String requestWrapParam) {
	
		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();
	
		//declaration of variables
		list<HexaBPM__SR_Doc__c> relatedSrDocList = new list<HexaBPM__SR_Doc__c>();
		list<HexaBPM_Amendment__c> relAmendList = new list< HexaBPM_Amendment__c>();
	
		//deseriliaze the request wrapper
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
	
	
		for(HexaBPM__Step__c stepOBj : [SELECT ID FROM HexaBPM__Step__c WHERE Step_Template_Code__c = 'ENTITY_SIGNING' 
			AND HexaBPM__Status_Type__c != 'END' AND HexaBPM__SR__c =:reqWrap.srId  LIMIT 1]){
				respWrap.relActionIteam = stepOBj;
				
			}
	
		for (HexaBPM__SR_Doc__c docObj :[SELECT id, HexaBPM__Document_Name__c,Amendment_Name__c,(select ContentDocument.Title,ContentDocumentId,LinkedEntityId,ContentDocument.LatestPublishedVersionId from ContentDocumentLinks ORDER BY SystemModstamp DESC limit 1 ),HexaBPM_Amendment__c FROM HexaBPM__SR_Doc__c WHERE HexaBPM__Service_Request__c = :reqWrap.srId]) {
	
			relatedSrDocList.add(docObj);
		}
	
		string OperatingRecordId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Operating Location').getRecordTypeId();
	
		for (HexaBPM_Amendment__c amendObj :[SELECT id,Will_Individual_Sign_AoA__c ,DI_First_Name__c ,DI_Last_Name__c ,DI_Email__c, Name__c,Email__c,Individual_Corporate_Name__c FROM HexaBPM_Amendment__c WHERE ServiceRequest__c = :reqWrap.srId AND RecordTypeId != :OperatingRecordId ]) {
			relAmendList.add(amendObj);
		}
	
		respWrap.relDocList = relatedSrDocList;
		respWrap.relAmendList = relAmendList;
	
		return respWrap;
	
	}
	
	
	@AuraEnabled
	public static RespondWrap generateSendEnvelopeLink(String requestWrapParam) {
	
		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();
	
		//declaration of variables
		List < dfsle.Recipient > recipientList = new List < dfsle.Recipient >();
		set < id > docIdSet = new set < id >();
	
		//deseriliaze the request wrapper
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
	
	
		for(HexaBPM_Amendment__c amendObj : reqWrap.amendList ){
	
			if(amendObj.Will_Individual_Sign_AoA__c  == 'No'){
				dfsle.Recipient myRecipient = dfsle.Recipient.fromSource(
				amendObj.DI_First_Name__c + ' ' + amendObj.DI_Last_Name__c ,
				amendObj.DI_Email__c,
			null,
			'Signer 1',
			new dfsle.Entity(amendObj.Id));
			recipientList.add(myRecipient);
			}else{
				dfsle.Recipient myRecipient = dfsle.Recipient.fromSource(
				amendObj.Individual_Corporate_Name__c,
				amendObj.Email__c,
			null,
			'Signer 1',
			new dfsle.Entity(amendObj.Id));
			recipientList.add(myRecipient);
			}
			
			
		}
	
		for(string Docid : reqWrap.docIds ){
			docIdSet.add(Docid);
	
		}
	
	
        if(!Test.isRunningTest()){
           dfsle.Envelope envelope = dfsle.EnvelopeService.getEmptyEnvelope(
			new dfsle.Entity(reqWrap.srId))
											 .withDocuments(dfsle.DocumentService.getLinkedDocuments(
								ContentVersion.getSObjectType(),
								docIdSet,
								false))
											 .withRecipients(recipientList)
											 /* .withEmail('Test text','Test text') */;
	
	
											 
										
		envelope = dfsle.EnvelopeService.sendEnvelope(
			envelope,
			false);
	
			
	
			respWrap.createdEnvilope = envelope; 
        }
		
	
		return respWrap;
	
	}
	
	
	
	@AuraEnabled
	
	public static RespondWrap sendEnvelopeTagging(String myEnvelopeID, String stepId, string srId) {
	
		RespondWrap respWrap = new RespondWrap();
		string responseUrl;
	
		//dfsle.UUID myTemplateId = dfsle.UUID.parse(myEnvelopeID);
	
		dfsle.UUID myTemplateId = dfsle.UUID.parse(myEnvelopeID);
	
	
		string returnUrl = system.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/OB_DocuSignCompletionPage';
	
		responseUrl = dfsle.EnvelopeService.getSenderViewUrl(myTemplateId, new Url(returnUrl + '?envilopeId=' + myTemplateId + '&stepId='+stepId+ '&srId='+srId )).toExternalForm();
	
		respWrap.docuSignTaggingURLString = responseUrl;
	
	
		respWrap.docuSignEnvilopeID = myEnvelopeID;
		return respWrap;
	
	}
	
	@AuraEnabled
	public static RespondWrap  updateEnvelopeSentDate(String requestWrapParam){
	
				//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();
		//deseriliaze the request wrapper
			reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
	
				if(reqWrap.envelopeID != null){
					dfsle__Envelope__c envelopeToUpdate = new dfsle__Envelope__c();
				for(dfsle__Envelope__c envObj : [select id from dfsle__Envelope__c where 	dfsle__DocuSignId__c = : reqWrap.envelopeID]){
					envelopeToUpdate = envObj;
				}
				if(envelopeToUpdate != null){
					envelopeToUpdate.Step__c = reqWrap.stepId;
					envelopeToUpdate.dfsle__Sent__c =  system.now();
				}
				upsert envelopeToUpdate;
				respWrap.debugger = envelopeToUpdate;
			}
				return respWrap;
				//createStatus(reqWrap.srId);
	
					
			
		}

		@AuraEnabled
		public static RespondWrap createEnvelopeStatus(String requestWrapParam) {

			//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();
		//deseriliaze the request wrapper
			reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

				List<dfsle.Envelope.Status> myStatus = dfsle.StatusService.getStatus(
					new Set<Id> { 
						reqWrap.srId
					},
					10000);
					respWrap.debugger = myStatus;
					return respWrap;	
		
			}
		
	
		/* @future(callout=true)
		public static void createStatus(string srId) {
			List<dfsle.Envelope.Status> myStatus = dfsle.StatusService.getStatus(
				new Set<Id> { 
					srId
				},
				10000);
				System.debug(myStatus);
					
	
		} */

	
	
	
	// ------------ Wrapper List ----------- //
	
	public class RequestWrap {
	
	@AuraEnabled public list<string> docIds;
	@AuraEnabled public list<HexaBPM_Amendment__c> amendList;
	@AuraEnabled public String srId;
	@AuraEnabled public String stepId;
	@AuraEnabled public String envelopeID;
	
	
	public RequestWrap() {
	}
	}
	
	public class RespondWrap {
	
	@AuraEnabled public string docuSignEnvilopeID;
	
	@AuraEnabled public list<HexaBPM__SR_Doc__c> relDocList;
	
	@AuraEnabled public list<HexaBPM_Amendment__c> relAmendList;
	
	@AuraEnabled public Object createdEnvilope;
	
	@AuraEnabled public Object DocumentList;
	@AuraEnabled public Object debugger;

	
	@AuraEnabled public HexaBPM__Step__c relActionIteam;
	
	@AuraEnabled public boolean renderButton ;
	
	@AuraEnabled public string docuSignTaggingURLString;
	
	@AuraEnabled public string docuSignTaggingURL;
	
	public RespondWrap() {
	}
	}

}