@isTest
public class OB_QueryUtilityClassTest 
{
   
    private static testmethod void methodTest()
    {
        Id p = [select id from profile where name='DIFC Customer Community User Custom'].id;
        
       // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'GENERAL'});
        insert listDocMaster;
        // create SR Template doc
        
         //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle','AOR_Financial'});
        insert createSRTemplateList;
        
        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        listSRTemplateDoc[0].HexaBPM__Added_through_Code__c = true;
        insert listSRTemplateDoc;
        
        
         ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;
        
        User user = new User(alias = 'te123', email='test123666@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = insertNewContacts[0].Id,
                timezonesidkey='America/Los_Angeles', username='tester1213@noemail.com');
       
        insert user;

      
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Identify ultimate beneficial owners';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;

        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Non Profit Incorporated Organisation (NPIO)','Services'}, 
                                                   new List<string>{'Non Profit Incorporated Organisation (NPIO)','Company'}, 
                                                   new List<string>{'NPIO','Recognized Company'});
       
        //insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        //insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        //insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        //insertNewSRs[0].Entity_Type__c = 'Financial - related';
        //insertNewSRs[1].Entity_Type__c = 'Financial - related';
        //insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        //insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        
        List<Company_Name__c> companies = OB_TestDataFactory.createCompanyName(new List<String>{'Com1','Com2'});
        companies[0].Application__c = insertNewSRs[0].id;
        companies[0].Status__c = 'Approved';
        companies[0].OB_Is_Trade_Name_Allowed__c = 'Yes';
        insert companies;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule.HexaBPM__Section_Detail__c = sec.id;
        objPgNavRule.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule.HexaBPM__Section__c=section.id;
        objPgNavRule.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule;
        
        HexaBPM__Page_Flow_Condition__c ObjPgFlowCond = new HexaBPM__Page_Flow_Condition__c();
        ObjPgFlowCond.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        ObjPgFlowCond.HexaBPM__Field_Name__c = 'Id';
        ObjPgFlowCond.HexaBPM__Operator__c = '!='; 
        ObjPgFlowCond.HexaBPM__Value__c = 'null';
        ObjPgFlowCond.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id; 
        insert ObjPgFlowCond;
        
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id;
        insert ObjPgFlowAction;
        
        HexaBPM__Document_Master__c doc= new HexaBPM__Document_Master__c();
        doc.HexaBPM__Available_to_client__c = true;
        doc.HexaBPM__Code__c = 'UBO_NPIO';
        doc.Download_URL__c = 'test';
        insert doc;
        
        String indRecTypId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        system.debug('@@@@@@@@@@ indRecTypId '+indRecTypId );

        
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = indRecTypId;
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Permanent_Native_Country__c = 'United Arab Emirates';
        objAmendment.Role__c = 'Director';
        objAmendment.Passport_No__c = '1234';
        objAmendment.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment);

        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = indRecTypId;
        objAmendment1.Nationality_list__c = 'India';
        objAmendment1.Passport_No__c = '1234';
        objAmendment1.Role__c = 'Director';
        objAmendment1.Permanent_Native_Country__c = 'United Arab Emirates';
        objAmendment1.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment1.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment1);
        insert listAmendment;
        system.debug('@@@@@@@@@@ listAmendment test data '+listAmendment );

        test.startTest();
           OB_QueryUtilityClass.checkSRStatusDraft(insertNewSRs[0]);
           OB_QueryUtilityClass.minRecValidationCheck(insertNewSRs[0].Id, 'Director');
           OB_QueryUtilityClass.checkSRStatusDraftById(insertNewSRs[0].Id);
           OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
           OB_QueryUtilityClass.setPageTracker(insertNewSRs[0], insertNewSRs[0].Id, listPage[0].Id);
           OB_QueryUtilityClass.QueryFullSR(insertNewSRs[0].Id);
           OB_QueryUtilityClass.getAccountId(user.Id);
           OB_QueryUtilityClass.reviewPageButtonShowCheck(insertNewSRs[0]);
           OB_QueryUtilityClass.treatNullAsZero(8);
           OB_QueryUtilityClass.createCompanySRDoc(insertNewSRs[0].Id, companies[0].Id, new map<string, string>{'GENERAL'=>documents[0].Id});
           OB_QueryUtilityClass.createSRDoc(insertNewSRs[0].Id, listAmendment[0].Id, 'GENERAL');
           OB_QueryUtilityClass.getAccountRec(user.Id);
            OB_QueryUtilityClass.shareholderPageValidationCheck(insertNewSRs[0]);
        /*
        OB_EntityDirectorDetailController.RequestWrapper reqWrapper1 = new OB_EntityDirectorDetailController.RequestWrapper();
        reqWrapper1.flowId = listPageFlow[0].Id;
        reqWrapper1.pageId = listPage[0].Id;
        reqWrapper1.srId = insertNewSRs[0].Id;
        //reqWrapper.amendmentID = listAmendment[0].Id;
        reqWrapper1.amedWrap = new OB_EntityDirectorDetailController.AmendmentWrapper();
        reqWrapper1.amedWrap.amedObj = listAmendment[1];
        reqWrapper1.amedWrap.amedObj.Passport_No__c = '1234';
        reqWrapper1.amedWrap.amedObj.Nationality_list__c = 'India';
        reqWrapper1.amedWrap.amedObj.Role__c = 'Director';
        reqWrapper1.amedWrap.amedObj.ServiceRequest__c = insertNewSRs[0].Id;
        OB_EntityDirectorDetailController.onAmedSaveDB(JSON.serialize(reqWrapper1));
        */
       
      /*
        String requestString = JSON.serialize(reqWrapper);
        OB_EntityDirectorContainerController.ResponseWrapper respWrap = OB_EntityDirectorContainerController.getExistingAmendment(requestString);
        OB_EntityDirectorContainerController.getButtonAction(insertNewSRs[0].Id,sec.Id,listPage[0].Id);
        
      
            system.debug('=get existing==srwrao=='+respWrap.srWrap.srObj);
            reqWrapper.typeOfInformation ='Foundation or its Council' ;
            reqWrapper.srWrap = respWrap.srWrap;
    
            reqWrapper.recordtypename= 'Individual';
            OB_EntityUBOContainerController.ResponseWrapper respWrap1 = OB_EntityUBOContainerController.initAmendmentDB(JSON.serialize(reqWrapper));
            
            system.debug('====test=amend=='+respWrap1.amedWrap.amedObj);
            respWrap1.amedWrap.amedObj.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council' ;
            respWrap1.amedWrap.amedObj.Passport_No__c = '1234';
            respWrap1.amedWrap.amedObj.Nationality_list__c = 'India';
            reqWrapper.amedWrap = respWrap1.amedWrap;
            OB_EntityUBOContainerController.getButtonAction(insertNewSRs[0].Id,sec.Id,listPage[0].Id,respWrap.srWrap.srObj);
            
            OB_EntityUBOContainerController.ResponseWrapper finalRes = OB_EntityUBOContainerController.getExistingAmendment(requestString);
            reqWrapper.typeOfInformation ='Foundation or its Council' ;
            reqWrapper.srWrap = finalRes.srWrap;
    
            reqWrapper.recordtypename= 'Individual';
            OB_EntityUBOContainerController.ResponseWrapper finalRes1 = OB_EntityUBOContainerController.initAmendmentDB(JSON.serialize(reqWrapper));
            system.debug('====test=amend=='+respWrap1.amedWrap.amedObj);
            finalRes1.amedWrap.amedObj.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council' ;
            finalRes1.amedWrap.amedObj.Passport_No__c = '1234';
            finalRes1.amedWrap.amedObj.Nationality_list__c = 'India';
            reqWrapper.amedWrap = finalRes1.amedWrap;
            finalRes1 = OB_EntityUBOContainerController.onAmedSaveDB(JSON.serialize(reqWrapper));
        */

        test.stopTest();
        
    }
    
    
  

}