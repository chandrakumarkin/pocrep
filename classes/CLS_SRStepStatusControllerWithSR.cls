/*********************************************************************************************************************
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------    
 Class to use to validate customer visa status : https://www.difc.ae/visastatus           

**********************************************************************************************************************/
@RestResource(urlMapping = '/DIFC_SRPI_NewSR/*')
global class CLS_SRStepStatusControllerWithSR 
{
 
 @HttpPost
 global static void jsonRes(string SRNumber) 
 {
       list<Step_Status_Website__c> listStepStausWebsite = new list<Step_Status_Website__c>();
    JSONGenerator jsn = JSON.createGenerator(true);
     jsn.writeStartObject();
     jsn.writeStringField('sr', SRNumber);
     jsn.writeStringField('date',string.valueof(Datetime.now()));
      
     
list<Service_Request__c> lstSR = new list<Service_Request__c>();
string statusOnWesbite = '';
string colorOnTheWebsite = '';
 
lstSR = [Select Id, Name,Is_Cancelled__c,SAP_Unique_No__c,Submitted_DateTime__c,Internal_Status_Name__c,External_Status_Name__c,External_SR_Status__c,External_SR_Status__r.Status_on_Visa_Checker_Website__c, External_SR_Status__r.Status_Color_on_Visa_Checker_Website__c,
(Select Name,Step_Name__c,Step_Status__c,SAP_MATNR__c,SAP_RMATN__c,SAP_Seq__c,Step_Template__r.Summary__c,Step_Template__r.SR_Status__c,Step_Template__c
 From Steps_SR__r where SAP_Seq__c != null AND ((Step_Status__c = 'Closed' OR Step_Status__c = 'Cancelled') OR Step_Name__c = 'Original Passport,EID Form,Visa Received After Biometrics') order by SAP_Seq__c desc limit 1) From Service_Request__c where Name =: SRNumber];
 
 
if(!lstSR.isEmpty())
{
     list<step__c> lstStep = new list<step__c>();
        lstStep = lstSR[0].Steps_SR__r;     
    jsn.writeStringField('status',lstSR[0].External_Status_Name__c);
    
    
 
    // if Service Request status is Application is pending or Awaiting Re-upload , return for information 
    if(lstSR[0].External_Status_Name__c == 'Application is pending' || lstSR[0].External_Status_Name__c == 'Awaiting Re-upload' || lstSR[0].External_Status_Name__c == 'Return for More Information'
    ||lstSR[0].External_Status_Name__c == 'Awaiting Originals'||
    lstSR[0].External_Status_Name__c == 'Rejected' || lstSR[0].External_Status_Name__c == 'The Visa application has been rejected by GDRFA' ||
    lstSR[0].External_Status_Name__c == 'The applicant is medically unfit')
    {
                statusOnWesbite = lstSR[0].External_SR_Status__r.Status_on_Visa_Checker_Website__c;
               
                colorOnTheWebsite = lstSR[0].External_SR_Status__r.Status_Color_on_Visa_Checker_Website__c;
                   
    }
    else if(
    (lstSR[0].External_Status_Name__c == 'Cancelled'|| lstSR[0].External_Status_Name__c == 'Cancellation Pending' || 
    lstSR[0].External_Status_Name__c == 'Refund Processed'|| lstSR[0].External_Status_Name__c == 'Cancellation Approved') && lstSR[0].Is_Cancelled__c ==true)
    {
                statusOnWesbite = lstSR[0].External_SR_Status__r.Status_on_Visa_Checker_Website__c;// The Service Request is cancelled.
                colorOnTheWebsite = lstSR[0].External_SR_Status__r.Status_Color_on_Visa_Checker_Website__c;
                 system.debug(statusOnWesbite +'statusOnWesbite');
    }
    
    else{ // if any of above is false
                
         // copy the step list from Service Request into new list 
        
        if(lstStep.isEmpty()==false) 
        { // check if step list is not empty
        
           if(lstStep[0].SAP_MATNR__c!=null)
            {
                
                listStepStausWebsite = [Select Id, Status_on_Visa_Checker_Website__c,Status_Color_on_Visa_Checker_Website__c from Step_Status_Website__c where Step_Template__c =: lstStep[0].Step_Template__c AND SAP_Material_No__c =: lstStep[0].SAP_MATNR__c ]; // get the status based on material and step name
                system.debug(' listStepStausWebsite ' + listStepStausWebsite);
                
                if(lstStep[0].Step_Status__c=='Cancelled')
                {
                    statusOnWesbite = Label.Site_Step_Status_Text;//'We regret to inform you that the visa application has been rejected by the General Directorate of Residency and Foreigners Affairs.';
                    colorOnTheWebsite =  'Orange';
                }
                else if(!listStepStausWebsite.isEmpty())
                {
                      statusOnWesbite = listStepStausWebsite[0].Status_on_Visa_Checker_Website__c;
                      colorOnTheWebsite =  listStepStausWebsite[0].Status_Color_on_Visa_Checker_Website__c;
                            
                }
            
            }
                    
                    
        }
    }
        if(statusOnWesbite!=null && colorOnTheWebsite!=null ) 
        {
            jsn.writeStringField('VisaStatus', statusOnWesbite);
            jsn.writeStringField('VisaStatusColor', colorOnTheWebsite);
        
        }       
    
    jsn.writeEndObject();
                            
   String jsonString = jsn.getAsString();
   System.Debug('jsonString:'+jsonString);    
   //jsonString = jsonString.replace('"RecordCount" : "' + recordsToReturn + '"','"RecordCount" : "' + actualCnt + '"');
   //jsonString = jsonString.replace('RecordCount','RecordCount2');
   

   //resp = jsonString;
       if(!Test.isRunningTest())
       {
           RestContext.response.addHeader('Content-Type', 'application/json');
           RestContext.response.responseBody = Blob.valueOf(jsonString);
       }                                    
}


    
 }
 
 
    
 
 }