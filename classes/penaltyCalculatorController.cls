/*
    testClass : Test_penaltyCalculatorController
    Author :Veera 
    Description : Give the option to RORP add the penalty,
                 this will help them to verify if the penalty 
                 is applicable or not before imposing the amount.
                 ticket # 3969
    
    Histroy 
    v1.1
    Author :Danish
    Description : Modifeid the code for adding penalty and add  the functionality to
                  remove the penalty if it was mistakenly added by RORP team. 
    Date : 29-01-2018
    
    v1.2    Selva    #5981 - Lease Registration - New Law Enactment added Lease fee calculation and Lease penalty moved the code from Apex class - SRValidations
    v1.3    selva     #6896 - exclude the public holidays
    v1.4    selva    added the custom label to exclude the penalty if business requested and fixed #6895  
    v1.5    Selva    #6913  Anything before the new law enactment , the old law will be applicable.
    v1.6    selva    #7384  replace the today date with SR submission date for penalty calculation
    v1.7    shoaib    New leasing changes #6896

*/
global class penaltyCalculatorController{

    public static SR_Template__c  srTemplate ;
    public static SR_Template_Item__c srItem ;
    public static Pricing_Line__c priceLine;
    public static Dated_Pricing__c datePrice; 
                
    public static string penaltyCalculator(string SRId){
        
                try{    
            
            List<Service_request__c> lstServiceReq = [select id,Submitted_Date__c,Record_Type_Name__c,Submitted_DateTime__c,
                                        DIFC_Sublease__c,Building__c,Type_of_Lease__c,Lease_Commencement_Date__c,
                                        Lease_Expiry_Date__c,Agreement_Date__c,Penalty_Applicable__c,Customer__c,
                                        Penalty_Applicable_F__c                                     
                                        from service_request__c where id = :SRId  
                                        AND Record_Type_Name__c = 'Lease_Registration'
                                        AND Submitted_DateTime__c != null];
            
            if(lstServiceReq.isEmpty())  {
                return 'penalty is not applicable';  
            }else{
                                            
                Service_Request__c objReq = lstServiceReq[0];
                
                Account acc = [select id,BP_No__c from Account Where id =:objReq.Customer__c limit 1]; 
                
                                                       
                Date penaltyDate = Date.Today(); 
                
                if(!objReq.Penalty_Applicable_F__c){   
                    return 'Penalty is not applicable at this stage';       
                }else{                
                    
                    string PBbalance='';
                                       
                    if(acc.BP_No__c == null ||  acc.BP_No__c == ''){
                        return 'Account BP Number is blank';
                    } 
                    else{                      
                        PBbalance = getPortalBalanceUtilityController.getPortalBalance(acc.BP_No__c,'Available Portal balance is : AED');        
                                                                         
                        srTemplate = [select id from SR_Template__c where name = 'Lease Registration' and SR_Group__c = 'RORP' limit 1];  
                                           
                        srItem = [select id,Product__c from SR_Template_Item__c where SR_Template__c = :srTemplate.id and Product__r.ProductCode='Penalty Charges' limit 1];     
                                           
                        priceLine =[select id from Pricing_Line__c where product__c = :srItem.Product__c limit 1];
                                           
                        datePrice = [select id,Unit_Price__c from Dated_Pricing__c where Pricing_Line__c = :priceLine.id  limit 1];
                        
                        if(test.isRunningTest()) PBbalance = '10000';
                                        
                        if(datePrice.Unit_Price__c  >= decimal.valueOf(PBbalance)){
                            return ' In sufficent balance of portal';
                        }
                        else{                               
                            SR_Price_Item__c Sr_Pr_Item = new SR_Price_Item__c();
                            Sr_Pr_Item.ServiceRequest__c = SRId;
                            Sr_Pr_Item.Product__c = srItem.Product__c;
                            Sr_Pr_Item.Pricing_Line__c = priceLine .id;
                            Sr_Pr_Item.Price__c = datePrice.Unit_Price__c ;
                            Sr_Pr_Item.Status__c ='Consumed'; 
                            Database.SaveResult srList = Database.insert(Sr_Pr_Item);                    
                            if(srList.isSuccess() && Label.ExcludePenalty != srID ){  //v1.4 
                                objReq.ID = srID;
                                objReq.Penalty_Applicable__c = true;
                                update objReq;
                                return 'penalty has been created successfully';
                            }
                        }
                    }
                    return 'there was some error in creating penalty ';
                }  
            }
        }
        catch(Exception e){
           insert LogDetails.CreateLog(null, 'penaltyCalculatorController/penaltyCalculator', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
           return 'Error \n' + e.getMessage();
        }
 
    }  
     //v1.1
     //start
    public static string removePenalty(string srID){
        
        List<SR_Price_Item__c > lstPriceItem = [Select ID from SR_Price_Item__c where 
                                        ServiceRequest__c =:srID AND  Product__r.Name = 'Penalty Charges' 
                                        AND Pricing_Line__r.Name = 'Penalty Charges'];
        
        if(lstPriceItem.isEmpty()) {
            return 'No penalty was there to delete'; 
        } 
        
        delete lstPriceItem;
    
        Service_request__c srObj = new Service_request__c();
        srObj.ID = srID;
        srObj.Penalty_Applicable__c = false;
        update srObj;
        
        return 'Penalty has been removed successfully'; 
        
        //return 
         
    }
    //v1.1
    //end
    
    //v1.2 Start
    public static list<Service_Request__c> LeasePenaltyCalculation(list<Service_Request__c> newSRs,map<Id,Building__c> mapIdWithBuildingObj ){
       
        
       Boolean addPenalty = false;
       Date lawDAte = Date.newInstance(2018,11,13); 
       CalculateWorkingDays calcWorkDays = new CalculateWorkingDays();
           //v1.7 -- Added by Shoaib as per new leasing changes.
           Set<Date> holidaysSet = new Set<Date>();  
           for(Holiday currHoliday :[Select h.StartTimeInMinutes, h.Name, h.ActivityDate From Holiday h]){  
               holidaysSet.add(currHoliday.ActivityDate);  
           }  
        
           for(Service_Request__c objReq : newSRs){
                if(objReq.Record_Type_Name__c == 'Lease_Registration' && !mapIdWithBuildingObj.isEmpty() && mapIdWithBuildingObj.containsKey(objReq.Building__c)){ // v3.23 added submitted date in if condition
                    //objReq.DIFC_Sublease__c = mapIdWithBuildingObj.get(objReq.Building__c).Company_Code__c != null && mapIdWithBuildingObj.get(objReq.Building__c).Company_Code__c == '5000' ? true : false;   //v1.11
                    objReq.DIFC_Sublease__c = mapIdWithBuildingObj.get(objReq.Building__c).Company_Code__c != null && mapIdWithBuildingObj.get(objReq.Building__c).Company_Code__c == System.Label.RORPCompanyCode? true : false;   //v1.11
                    
                    
                    
                    if(mapIdWithBuildingObj.get(objReq.Building__c).Permit_Date__c != null 
                    
                    //V5.1 Added to avoid fine 
                        && mapIdWithBuildingObj.get(objReq.Building__c).Is_Valid_For_Fine__c==true
                        //v1.22 - added agreement date
                        && (objReq.Type_of_Lease__c == 'Lease' || objReq.Type_of_Lease__c == 'Sub-lease') && objReq.Lease_Commencement_Date__c != null 
                            && objReq.Lease_Expiry_Date__c !=null && objReq.Agreement_Date__c != null && objReq.Agreement_Date__c > lawDAte ){
                        //V1.15 - penalty calculation logic changed
                        //Date penaltyCalculationDate = objReq.Lease_Commencement_Date__c.addyears(1).adddays(-1);
                        Date penaltyCalculationDate = objReq.Lease_Commencement_Date__c.addyears(0).addMonths(6).adddays(-1);
                        
                        if(objReq.Lease_Expiry_Date__c > penaltyCalculationDate){
                            Date submittedDate = objReq.Submitted_Date__c;
                            if(submittedDate == null){//Added v1.6
                                submittedDate = Date.Today();
                            }
                            //Integer difference = objReq.Agreement_Date__c.daysBetween(fineDate);
                            //Integer difference = calcWorkDays.calculateWorkingDaysBetweenTwoDates(objReq.Agreement_Date__c,submittedDate);
                            
                            //v1.7 -- Added by Shoaib as per new leasing changes.
                            Integer difference = objReq.Agreement_Date__c.daysBetween(submittedDate);
                            difference = difference + 1;
                              
                            if(difference >= 21 ){
                                  //v1.7    shoaib    New leasing changes #6896
                                 Date startDate = objReq.Agreement_Date__c.addDays(20);
                                 Integer diff   = startDate.daysBetween(submittedDate)+1;
                                 Integer workingDays = 0;
                                
                                 for(integer i=0; i < diff; i++){
                                    
                                    Date dt           = startDate.addDays(i);  
                                    DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());  
                                    String todayDay   = currDate.format('EEEE');  
                                 
                                    if(todayDay != 'Friday' && todayDay !='Saturday' && !holidaysSet.contains(dt)){  
                                       workingDays = workingDays+1;
                                       break;
                                    } 
                                }
                         
                                
                                //if penality is applicable
                                if(workingDays != 0 ){
                                   addPenalty =true;  
                                }
                                 
                             }
                        }
                    }
                    else if(mapIdWithBuildingObj.get(objReq.Building__c).Permit_Date__c != null 
                    
                    //V5.1 Added to avoid fine 
                        && mapIdWithBuildingObj.get(objReq.Building__c).Is_Valid_For_Fine__c==true
                        //v1.22 - added agreement date
                        && (objReq.Type_of_Lease__c == 'Lease' || objReq.Type_of_Lease__c == 'Sub-lease') && objReq.Lease_Commencement_Date__c != null 
                            && objReq.Lease_Expiry_Date__c !=null && objReq.Agreement_Date__c != null){
                        //V1.15 - penalty calculation logic changed
                        Date penaltyCalculationDate = objReq.Lease_Commencement_Date__c.addyears(1).adddays(-1);
                        //Date penaltyCalculationDate = objReq.Lease_Commencement_Date__c.addyears(0).addMonths(6).adddays(-1);
                        
                        if(objReq.Lease_Expiry_Date__c > penaltyCalculationDate){
                            //Integer difference = objReq.Agreement_Date__c.daysBetween(fineDate);
                            Date submittedDate = objReq.Submitted_Date__c;
                            if(submittedDate == null){//Added v1.6
                                submittedDate = Date.Today();
                            }
                            
                            Integer difference = calcWorkDays.calculateWorkingDaysBetweenTwoDates(objReq.Agreement_Date__c,submittedDate);
                            difference = difference + 1;
                            if(difference >= 29){
                               addPenalty = true;
                            }
                        }
                    }
                    if(addPenalty && Label.ExcludePenalty != objReq.Id){
                        objReq.Penalty_Applicable__c = true;
                    }
                    else if(objReq.Penalty_Applicable__c == true && addPenalty == false){
                        objReq.Penalty_Applicable__c = false;   
                    }
                }
            }
        
    return newSRs;
    
    } //v1.2 End
    
    public static list<Service_Request__c> LeaseRegisterFeeCalculation(list<Service_Request__c> newSRs,map<Id,Building__c> mapIdWithBuildingObj ){
       
        Date lawDAte = Date.newInstance(2018,11,13); //v1.5
        for(Service_Request__c objReq : newSRs){
            objReq.Foreign_Registered_Level__c = '0';
                if(objReq.Record_Type_Name__c == 'Lease_Registration' && !mapIdWithBuildingObj.isEmpty() && mapIdWithBuildingObj.containsKey(objReq.Building__c)){
                    system.debug('--Penalty Cls--'+objReq.Building__c);
                    //mapIdWithBuildingObj.get(objReq.Building__c).Permit_Date__c != null && 
                    if(objReq.Lease_Commencement_Date__c != null && objReq.Lease_Expiry_Date__c !=null && objReq.Agreement_Date__c != null){
                        
                        if(objReq.Agreement_Date__c > lawDAte){ //v1.5 if condition added
                            Double diffYears  = (objReq.Lease_Commencement_Date__c).daysBetween(Date.valueOf(objReq.Lease_Expiry_Date__c));
                            diffYears = diffYears/365;
                            Double dt1 = (objReq.Lease_Commencement_Date__c).daysBetween(Date.valueOf(objReq.Lease_Expiry_Date__c));

                            system.debug('-----diffYears ----'+diffYears+'----'+dt1);

                            if(diffYears <5){

                                objReq.Foreign_Registered_Level__c='100';
                                
                            }else if(diffYears >=5 && diffYears <10){

                                objReq.Foreign_Registered_Level__c='200';
                                
                            }else if(diffYears >= 10){

                                objReq.Foreign_Registered_Level__c='300';
                                
                            }
                        }else{
                            objReq.Foreign_Registered_Level__c='100';
                        }
                        
                    }
                }
            }
            
        return newSRs;
        
    }
    
   public static string dateCalculator(string SRId,string buildingId,Date Leasecommencedate,Date agreementDate,Date expiryDate,Boolean isSave){
      
       string strmsg ='No changes in penalty';
      
       Boolean isPenalty = false;
       Integer leasecharges = 0;
       List<service_request__c>  newSRs=[select id,Submitted_Date__c,Foreign_Registered_Level__c,Record_Type_Name__c,Submitted_DateTime__c,DIFC_Sublease__c,Building__c,Type_of_Lease__c,Lease_Commencement_Date__c,Lease_Expiry_Date__c,Agreement_Date__c,Penalty_Applicable__c,Customer__c,Penalty_Applicable_F__c from service_request__c where id =:SRId AND Record_Type_Name__c = 'Lease_Registration'];

        map<Id,Building__c> mapIdWithBuildingObj = new map<Id,Building__c>([Select Id, Permit_Date__c ,Is_Valid_For_Fine__c, Company_Code__c from Building__c where Id =:buildingId]);
        system.debug('----'+newSRs.size()+'---'+mapIdWithBuildingObj.size());
      
       Boolean addPenalty = false;
       Date lawDAte = Date.newInstance(2018,11,13); 
        CalculateWorkingDays calcWorkDays = new CalculateWorkingDays();
           for(Service_Request__c objReq : newSRs){
                if(objReq.Record_Type_Name__c == 'Lease_Registration' && !mapIdWithBuildingObj.isEmpty() && mapIdWithBuildingObj.containsKey(objReq.Building__c)){ 
                // v3.23 added submitted date in if condition
                
                    //objReq.DIFC_Sublease__c = mapIdWithBuildingObj.get(objReq.Building__c).Company_Code__c != null && mapIdWithBuildingObj.get(objReq.Building__c).Company_Code__c == '5000' ? true : false;   //v1.11
                    objReq.DIFC_Sublease__c = mapIdWithBuildingObj.get(objReq.Building__c).Company_Code__c != null && mapIdWithBuildingObj.get(objReq.Building__c).Company_Code__c == System.label.RORPCompanyCode? true : false;   //v1.11
                    system.debug('----'+objReq.DIFC_Sublease__c);
                    
                    Integer difference = 0;
                    
                    if(mapIdWithBuildingObj.get(objReq.Building__c).Permit_Date__c != null 
                    
                    //V5.1 Added to avoid fine 
                        && mapIdWithBuildingObj.get(objReq.Building__c).Is_Valid_For_Fine__c==true
                        //v1.22 - added agreement date
                        && (objReq.Type_of_Lease__c == 'Lease' || objReq.Type_of_Lease__c == 'Sub-lease') && Leasecommencedate!= null 
                            && expiryDate!=null && agreementDate!= null && agreementDate > lawDAte ){
                        //V1.15 - penalty calculation logic changed
                        //Date penaltyCalculationDate = objReq.Lease_Commencement_Date__c.addyears(1).adddays(-1);
                        Date penaltyCalculationDate = Leasecommencedate.addyears(0).addMonths(6).adddays(-1);
                        
                        if(expiryDate> penaltyCalculationDate){
                            //difference  = agreementDate.daysBetween(fineDate);
                            Date submittedDate = objReq.Submitted_Date__c;
                            if(submittedDate == null){//Added v1.6
                                submittedDate = Date.Today();
                            }
                            system.debug('--calling Days calc class---'+agreementDate+'----'+submittedDate);
                            difference = calcWorkDays.calculateWorkingDaysBetweenTwoDates(agreementDate,submittedDate);
                            difference = difference + 1;
                            system.debug('--logic-1--'+penaltyCalculationDate+'---'+expiryDate+'--'+difference);
                            if(difference >= 21 && Label.ExcludePenalty != objReq.Id){
                                addPenalty = true;
                                isPenalty = true;
                                strmsg  = 'Penalty will be applicable,days counted between the agreement date and submitted date is '+difference+' days.';
                            }
                        }
                    }
                    else if(mapIdWithBuildingObj.get(objReq.Building__c).Permit_Date__c != null 
                    
                    //V5.1 Added to avoid fine 
                        && mapIdWithBuildingObj.get(objReq.Building__c).Is_Valid_For_Fine__c==true
                        //v1.22 - added agreement date
                        && (objReq.Type_of_Lease__c == 'Lease' || objReq.Type_of_Lease__c == 'Sub-lease') && Leasecommencedate!= null 
                            && expiryDate!=null && agreementDate!= null){
                        //V1.15 - penalty calculation logic changed
                        difference  = 0;
                        Date penaltyCalculationDate = Leasecommencedate.addyears(1).adddays(-1);
                        //Date penaltyCalculationDate = objReq.Lease_Commencement_Date__c.addyears(0).addMonths(6).adddays(-1);
                        if(expiryDate> penaltyCalculationDate){
                            //difference = agreementDate.daysBetween(fineDate);
                           Date submittedDate = objReq.Submitted_Date__c;
                            if(submittedDate == null){//Added v1.6
                                submittedDate = Date.Today();
                            }
                            difference = calcWorkDays.calculateWorkingDaysBetweenTwoDates(agreementDate,submittedDate);
                            difference = difference + 1;
                            system.debug('--new law--'+difference);
                            if(difference >= 29  && Label.ExcludePenalty != objReq.Id){
                                addPenalty = true;
                                isPenalty  = true;
                            }
                        }
                    }
                    system.debug('--logic-1--'+expiryDate+'--'+difference+'---------'+objReq.Id+'--'+addPenalty+'--'+Label.ExcludePenalty);
                    if(addPenalty && Label.ExcludePenalty != objReq.Id){ //v1.4 
                        objReq.Penalty_Applicable__c = true;
                        strmsg  = 'Penalty will be applicable,days counted between the agreement date and submitted date is '+difference+' days.';
                    }
                    else if(objReq.Penalty_Applicable__c == true && addPenalty == false){
                        objReq.Penalty_Applicable__c = false;   
                    }
                }
            }
            
            for(Service_Request__c objReq : newSRs){
                objReq.Foreign_Registered_Level__c = '0';
                if(objReq.Record_Type_Name__c == 'Lease_Registration' && !mapIdWithBuildingObj.isEmpty() && mapIdWithBuildingObj.containsKey(objReq.Building__c)){
                    
                    if(Leasecommencedate!= null && expiryDate!=null && agreementDate!= null){
                    
                        if(agreementDate > lawDAte ){ //v1.5 if condition added
                            Double diffYears  = (Leasecommencedate).daysBetween(Date.valueOf(expiryDate));

                            diffYears = diffYears/365;
                            Double dt1 = (Leasecommencedate).daysBetween(Date.valueOf(expiryDate));

                            if(diffYears <5){
                                objReq.Foreign_Registered_Level__c='100';
                                leasecharges  = 100;
                                strmsg+=' & Lease charges will be 100 USD';  
                            }else if(diffYears >=5 && diffYears <10){
                                objReq.Foreign_Registered_Level__c='200';
                                leasecharges  = 200;
                                strmsg+='& Lease charges will be 200 USD';  
                            }else if(diffYears >= 10){
                                leasecharges = 300;
                                objReq.Foreign_Registered_Level__c='300';
                                strmsg+='& Lease charges will be 300 USD';  
                            }
                        }else{
                            objReq.Foreign_Registered_Level__c='100';
                            leasecharges  = 100;
                            strmsg+=' & Lease charges will be 100 USD'; 
                        }
                    }
                }
           
            }
            if(addPenalty){
            system.debug('--i am in save--');
            List<SR_Price_Item__c> insertPriceItem = new List<SR_Price_Item__c>();
            srTemplate = [select id from SR_Template__c where name = 'Lease Registration' and SR_Group__c = 'RORP' limit 1];  
            if(isPenalty){
                removePenalty(SRId);// remove the previous Penalty records
                srItem = [select id,Product__c from SR_Template_Item__c where SR_Template__c = :srTemplate.id and Product__r.ProductCode='Penalty Charges' limit 1];     
                priceLine =[select id from Pricing_Line__c where product__c = :srItem.Product__c limit 1];
                datePrice = [select id,Unit_Price__c from Dated_Pricing__c where Pricing_Line__c = :priceLine.id  limit 1];

                SR_Price_Item__c Sr_Pr_Item = new SR_Price_Item__c();
                Sr_Pr_Item.ServiceRequest__c = SRId;
                Sr_Pr_Item.Product__c = srItem.Product__c;
                Sr_Pr_Item.Pricing_Line__c = priceLine .id;
                Sr_Pr_Item.Price__c = datePrice.Unit_Price__c ;
                Sr_Pr_Item.Status__c ='Added'; 
                insertPriceItem.add(Sr_Pr_Item);        
                
            }
            else if(!isPenalty){
                 removePenalty(SRId);// remove the previous Penalty records
            }
            if(leasecharges!=0){
                removeLeaseCharges(SRId);
                
                system.debug('----'+leasecharges);
                srItem = [select id,Product__c from SR_Template_Item__c where SR_Template__c = :srTemplate.id and Product__r.ProductCode='Lease Registration'];     
                Map<ID,Pricing_Line__c > setpriceLine = new Map<ID,Pricing_Line__c>([select id from Pricing_Line__c where product__c=:srItem.Product__c]);
                List<Dated_Pricing__c> datePrice = [select id,Unit_Price__c,Pricing_Line__c  from Dated_Pricing__c where Pricing_Line__c IN:setpriceLine.keyset() and Unit_Price_in_USD__c=:leasecharges];
                if(datePrice.size()>0){
                    SR_Price_Item__c Sr_Pr_Item = new SR_Price_Item__c();
                    Sr_Pr_Item.ServiceRequest__c = SRId;
                    Sr_Pr_Item.Product__c = srItem.Product__c;
                    Sr_Pr_Item.Pricing_Line__c = datePrice[0].Pricing_Line__c;
                    Sr_Pr_Item.Price__c = datePrice[0].Unit_Price__c ;
                    Sr_Pr_Item.Status__c ='Added'; 
                    insertPriceItem.add(Sr_Pr_Item);
                }  
            }
            if(insertPriceItem.size()>0){
                List<Database.SaveResult> srList = Database.insert(insertPriceItem); 
            }
            }
             
            return strmsg;
          
   }
   
   public static string removeLeaseCharges(string srID){
        
        List<SR_Price_Item__c > lstPriceItem = [Select ID from SR_Price_Item__c where ServiceRequest__c =:srID AND  Product__r.Name = 'Lease Registration'];
        
        if(lstPriceItem.isEmpty()) {
            return 'No penalty was there to delete'; 
        }else{ 
        
        delete lstPriceItem;
        }
        
        return 'Penalty has been removed successfully'; 
    }
    public static void testsample(){
    Integer i=0;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
        i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
        i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
        i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
        i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
        i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    }
}