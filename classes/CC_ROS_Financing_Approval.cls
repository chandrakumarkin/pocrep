global without sharing class CC_ROS_Financing_Approval implements HexaBPM.iCustomCodeExecutable {

    Private static final String  FINANCING_STATEMENT         = 'FINANCING_STATEMENT';
    Private static final String  FINANCING_STATEMENT_AMENDMENT = 'FINANCING_STATEMENT_AMENDMENT';
     
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        
        string strResult = 'success';
        
        if(stp!=null && stp.HexaBPM__SR__c!=null){

            HexaBPM__Service_Request__c thisRequest = [ SELECT Id,HexaBPM__Customer__c ,RecordTypeId,Setting_Up__c FROM HexaBPM__Service_Request__c WHERE ID =: stp.HexaBPM__SR__c];
            String generalRecTyp = OB_QueryUtilityClass.getRecordtypeID('Debtor_Secured_Party__c','General');
            String historyRecTyp = OB_QueryUtilityClass.getRecordtypeID('Debtor_Secured_Party__c','History');

           
            if(thisRequest.RecordTypeId == getRecordTypeId(FINANCING_STATEMENT)){
                List<Approved_Form__c> approvedFormList = [select Id,Number_Of_Pages__c,Description__c,Name__c,Phone__c,Address__c,
                    SR_Statement_File_No__c,
                    (   select Id,Approved_Form__c,City__c,Is_Existing__c,Shareholder_Type__c,Jurisdiction_Of_Organization__c,
                        Country__c,First_Name__c,Service_Type__c,Last_Name__c,Mailing_Address__c,Middle_Name__c,Organizational_ID__c,
                        Organization_Name__c,Type_Of_Organization__c,RecordType.DeveloperName,Suffix__c,Status__c
                        from Debtor_Secured_Partys__r
                        where Status__c ='Draft' and RecordTypeId =:generalRecTyp
                    )
                    from Approved_Form__c 
                    where Service_Request__c=:stp.HexaBPM__SR__c
                ];   
                List<Debtor_Secured_Party__c>debtList = new List<Debtor_Secured_Party__c>();
                if(approvedFormList.size() >0 && approvedFormList[0].Debtor_Secured_Partys__r != null){
                    for(Debtor_Secured_Party__c de : approvedFormList[0].Debtor_Secured_Partys__r){
                        de.Status__c ='Active';
                        debtList.add(de);
                    }
                    if(debtList.size() >0)update debtList;
                    system.debug('======debtList=============='+debtList);
                }
            }else if(thisRequest.RecordTypeId == getRecordTypeId(FINANCING_STATEMENT_AMENDMENT)){
                
                List<Debtor_Secured_Party__c>finalUpdateList = new List<Debtor_Secured_Party__c>();
                Approved_Form__c currentApprovedForm = new Approved_Form__c();
                
                List<Approved_Form__c> currentApprovedFormList = [
                    select Id,SR_Statement_File_No__c,
                    (   select Id,Approved_Form__c,City__c,Is_Existing__c,Shareholder_Type__c,
                        Country__c,First_Name__c,Service_Type__c,Last_Name__c,Mailing_Address__c,Middle_Name__c,Organizational_ID__c,Jurisdiction_Of_Organization__c,
                        Organization_Name__c,Type_Of_Organization__c,RecordType.DeveloperName,Suffix__c,Status__c
                        from Debtor_Secured_Partys__r
                        where Status__c ='Draft' and RecordTypeId =:generalRecTyp
                    )
                    from Approved_Form__c where Service_Request__c=:stp.HexaBPM__SR__c order by CreatedDate desc limit 1
                ];
                
                if(currentApprovedFormList.size() >0){
                    currentApprovedForm = currentApprovedFormList[0];
                    
                    if(currentApprovedForm.Debtor_Secured_Partys__r.size() >0){
                        for(Debtor_Secured_Party__c deb : currentApprovedForm.Debtor_Secured_Partys__r){
                            deb.Is_Existing__c = false;
                            if(deb.Status__c != 'Removed'){
                                deb.Status__c ='Active';
                                finalUpdateList.add(deb);
                            }
                        }
                    }
                    
                    List<Approved_Form__c> oldApprovedFormList = [select Id,Number_Of_Pages__c,Description__c,Name__c,Phone__c,Address__c,
                        (select Id,Approved_Form__c,City__c,Is_Existing__c,Shareholder_Type__c,RecordTypeId,
                            Country__c,First_Name__c,Service_Type__c,Last_Name__c,Mailing_Address__c,Middle_Name__c,Organizational_ID__c,
                            Organization_Name__c,Type_Of_Organization__c,RecordType.DeveloperName,Suffix__c,Status__c,Jurisdiction_Of_Organization__c
                            from Debtor_Secured_Partys__r
                            where Status__c ='Active' and RecordTypeId =:generalRecTyp
                        )
                        from Approved_Form__c 
                        where Service_Request__r.Name=:currentApprovedForm.SR_Statement_File_No__c order by CreatedDate desc limit 1
                    ];  

                    if(oldApprovedFormList.size() >0){
                        for(Debtor_Secured_Party__c deb : oldApprovedFormList[0].Debtor_Secured_Partys__r){
                            deb.RecordTypeId = historyRecTyp;
                            finalUpdateList.add(deb);
                        }
                    }
                    
                    if(finalUpdateList.size() >0) update finalUpdateList;
                }
            }
        
        }
        return strResult;
    }
    /**
     * Return record type ID
      */ 
    private Id getRecordTypeId(String recordTypeName){
        return Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();
    } 
    
  
}