public class RORP_QR_Verifycontroller {

    public static final String TITLE_DEED= 'Title deed';
    public static final String TITLE_DEED_1004= 'Title deed';
    public static final String CAVEAT_REGISTRATION= 'Caveat Registration';
    public static final String OFFICIAL_SEARCH= 'Official Search Certificate';
    public static final String PROPERTY_REG= 'Property Registration Information Certificate';
    public static final String LEASE= 'Lease';
    Public static final Map<String, String> TRANSACTIONMAP = new Map<String, String>{
        '1001' => RORP_QR_Verifycontroller.TITLE_DEED, 
        '6000' => RORP_QR_Verifycontroller.CAVEAT_REGISTRATION, 
        '3000' => RORP_QR_Verifycontroller.OFFICIAL_SEARCH, 
        '1111' => RORP_QR_Verifycontroller.PROPERTY_REG, 
        '5000' => RORP_QR_Verifycontroller.LEASE,
        '1004' => RORP_QR_Verifycontroller.TITLE_DEED_1004
    };
    
    Public static final Map<String, String> statusMap= new Map<String, String>{
        'A' => 'Active', 
        'P' => 'Pending', 
        'C' => 'Cancelled', 
        'R' => 'Reversal', 
        'D' => 'Draft', 
        'W' => 'Withdrwan', 
        'L' => 'Laps', 
        'T' => 'Termination Pursuant to Court Order', 
        'X' => 'Discharge', 
        'U' => 'Un-registered',
        'E' => 'Expired',
        'F' => 'Future Lease',
        'S' => 'Mutual Termination',
        'Z' => ' TD Replacement'
    };

    private string privateKey ='GL2zpm2woB2YirDiJr782mAAh5Jt8pvFiG/2jDMmqcg=';
    private string vectorKey ='AAAAAAAAAAAAAAAAAAAAAA==';
    
    public string ObjLeaseStatus{get;set;}   
    public string refNumber{get;set;}
    public string headerOfLease{get;set;}
    public string titleDescription{get;set;}
    
    public RORP_QR_Verifycontroller(){

        ObjLeaseStatus='';
        headerOfLease ='';
        try {

            string ttypeEncryption =  (apexpages.currentPage().getparameters().get('TTYPE') != null) ? apexpages.currentPage().getparameters().get('TTYPE') :'';
            string refNumberEncryption =  (apexpages.currentPage().getparameters().get('ref') != null) ? apexpages.currentPage().getparameters().get('ref') :'';
            boolean isNormalRef = (refNumberEncryption != '' && refNumberEncryption.isNumeric()) ? true : false;
           
            string tType = (ttypeEncryption != '' && !isNormalRef) ? RORP_QR_Verifycontroller.AES256Decryption(ttypeEncryption,privateKey,vectorKey) : ttypeEncryption;
            refNumber = (refNumberEncryption != '' && !isNormalRef) ? RORP_QR_Verifycontroller.AES256Decryption(refNumberEncryption,privateKey,vectorKey) : refNumberEncryption;
            system.debug('===============refNumber==========='+refNumber);
            system.debug('===============tType ==========='+tType );

            List<Lease__c> listLease=[select id,name, Status__c,Lease_Types__c, Start_date__c, End_Date__c from Lease__c where Name =:refNumber limit 1];
            List<Caveat_Registration__c> cavSearch = [select Id,Status__c,Transaction_Type__c from Caveat_Registration__c where CVEAT__c=:refNumber limit 1];
            List<Official_Search__c> offSearch = [select Id,Status__c from Official_Search__c where RLTNO__c=:refNumber limit 1];

            system.debug('--listLease----'+listLease);
            titleDescription = (tType != '' && TRANSACTIONMAP.containskey(tType) && TRANSACTIONMAP.get(tType) != null) ? TRANSACTIONMAP.get(tType) : '';//Title Deed
            system.debug('--titleDescription ----'+titleDescription );
            system.debug('--tType ----'+tType );

            if(refNumber!=null && refNumber!='' && refNumber.length()<30){
                system.debug('--refNumber----'+refNumber);

                if(tType != ''){
                    if(titleDescription  != '' && (titleDescription == CAVEAT_REGISTRATION || titleDescription == OFFICIAL_SEARCH || (titleDescription == TITLE_DEED_1004 && tType =='1004'))){

                        string statusRec = (
                            (titleDescription == CAVEAT_REGISTRATION || titleDescription == TITLE_DEED_1004) && cavSearch.size()>0  && cavSearch[0].Status__c != null
                        ) ? cavSearch[0].Status__c : (titleDescription == OFFICIAL_SEARCH && offSearch.size()>0  && offSearch[0].Status__c != null) ? offSearch[0].Status__c :'';
                        system.debug('=============statusRec ============'+statusRec );
                        ObjLeaseStatus=(statusMap.containskey(statusRec) && statusMap.get(statusRec) != null) ? statusMap.get(statusRec) :'';
                        system.debug('=============ObjLeaseStatus============'+ObjLeaseStatus);
                        headerOfLease =titleDescription+' Status  ';
                    }else{
                        if(titleDescription  != ''){
                            headerOfLease = (listLease.size()==0)?'':titleDescription +' Status ';
                            ObjLeaseStatus =(listLease.size()==0)? 'Document is invalid' : listLease[0].Status__c;
                        }else{
                            headerOfLease = '';
                            ObjLeaseStatus ='Document is invalid';
                        }
                    }
                    if(headerOfLease !=''){
                        if(ObjLeaseStatus  == 'Active')ObjLeaseStatus  ='The '+titleDescription+' #'+refNumber+' is valid as of today as per our records.';
                        else ObjLeaseStatus  ='The given '+titleDescription+' is not valid as of today as per our records.';
                    }
                }else{
                    
                    headerOfLease = (listLease.size()==0) ? '' : TITLE_DEED +' Status  ';
                    ObjLeaseStatus =(listLease.size()==0)? 'Document is invalid' : listLease[0].Status__c;
                    if(listLease.size()>0){
                        if(ObjLeaseStatus  == 'Active')ObjLeaseStatus  ='The '+titleDescription+' #'+refNumber+' is valid as of today as per our records.';
                        else ObjLeaseStatus  ='The given '+titleDescription+' is not valid as of today as per our records.';
                    }
                    
                    if(listLease.size()==0 || (listLease.size() >0 && listLease[0].Status__c == null && listLease[0].Start_date__c == null && listLease[0].End_Date__c == null) ){
                                    
                        if(cavSearch.size() >0 && cavSearch[0].Status__c != null && cavSearch[0].Transaction_Type__c == '1004'){
                    
                            string statusRec = (cavSearch[0].Status__c != null ) ? cavSearch[0].Status__c : '';  
                            if(statusMap.containskey(statusRec) && statusMap.get(statusRec) != null && statusMap.get(statusRec) == 'Active')ObjLeaseStatus  ='The '+TITLE_DEED+' #'+refNumber+' is valid as of today as per our records.';                      
                            else ObjLeaseStatus  ='The given '+TITLE_DEED+' is not valid as of today as per our records.';
                            headerOfLease = (statusMap.containskey(statusRec) && statusMap.get(statusRec) != null) ? TITLE_DEED +' Status  ' :'';
                        }
                    }
                }
            }
        }catch (exception e) {
            System.debug('----------message:=========== ' + e.getMessage());
            headerOfLease = '';
            ObjLeaseStatus ='Document is invalid';
        }
        system.debug('--ObjLeaseStatus ----'+ObjLeaseStatus );
    } 

    public static string AES256Decryption(string encrypted,string privateKey,string vectorKey){

        Blob exampleIv = EncodingUtil.base64Decode(vectorKey);
        Blob decrypted = Crypto.decrypt('AES256', EncodingUtil.base64Decode(privateKey), exampleIv, EncodingUtil.base64Decode(encrypted));
        String decryptedString = decrypted.toString();
        system.debug('--------decryptedString------- :-'+decryptedString);
        return decryptedString;   
    }
}