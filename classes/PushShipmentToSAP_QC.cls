/******************************************************************************************
 *  Author   : Veera
 *  Company  : 
 *  Date     : 29/07/2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    29/07/2021  Veera    Created
********/
 global class PushShipmentToSAP_QC implements Queueable,Database.AllowsCallouts{
     
     
     
    
     
      CC_CourierCls.ShipmentInfo  ShipInfo  = new CC_CourierCls.ShipmentInfo();
      string Act;
      
      
      global PushShipmentToSAP_QC(string ShiInfo,string Action){
    
        ShipInfo = (CC_CourierCls.ShipmentInfo)JSON.deserialize(ShiInfo,CC_CourierCls.ShipmentInfo.class);
        Act =  Action ;
       
    
    }
    
      global void execute(QueueableContext context){
      
         
         CC_CourierCls.PushShipmentToSAP(ShipInfo,Act);
      }
    
    
    }