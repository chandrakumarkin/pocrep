public without sharing class Cls_FieldElementId_Util {
    public static string getFieldElementId(string strObjectName,string Fieldlabel){
        system.debug('strObjectName===>'+strObjectName);
        system.debug('Fieldlabel===>'+Fieldlabel);
        string objectKeyPrefix = getObjectPrefix(strObjectName);
        PageReference p = new PageReference('/' +objectKeyPrefix+ '/e?nooverride=1');
        
        String html = '';            
        if (Test.IsRunningTest()){
            html = '<label for="CF00N11000000qRuU"><span class="requiredMark">*</span>Client</label>';
        }
        else
          html = p.getContent().toString();        
        
       Map<String, String> labelToId = new Map<String, String>();
       Matcher m = Pattern.compile('<label for="(.*?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
      
        while (m.find()) {
            String label = m.group(3);
            String id = m.group(1);
            labelToId.put(label, id);
        }
        system.debug('labelToId====>'+labelToId);
        system.debug('labelToId====>'+labelToId.get('Contact'));
        if(labelToId!=null && labelToId.get(Fieldlabel)!=null){
            return labelToId.get(Fieldlabel);
        }else{
            return null;
        }
    }
    
    public static map<string,string> getFieldElementMap(string strObjectName,string Fieldlabel){
        system.debug('strObjectName===>'+strObjectName);
        system.debug('Fieldlabel===>'+Fieldlabel);
        string objectKeyPrefix = getObjectPrefix(strObjectName);
        PageReference p = new PageReference('/' +objectKeyPrefix+ '/e?nooverride=1');
        
        String html = '';            
        if (Test.IsRunningTest()){
            html = '<label for="CF00N11000000qRuU"><span class="requiredMark">*</span>Client</label>';
        }
        else
          html = p.getContent().toString();        
        
       Map<String, String> labelToId = new Map<String, String>();
       Matcher m = Pattern.compile('<label for="(.*?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
      
        while (m.find()) {
            String label = m.group(3);
            String id = m.group(1);
            labelToId.put(label, id);
        }
        system.debug('labelToId====>'+labelToId);
        system.debug('labelToId====>'+labelToId.get('Contact'));
        return labelToId;
    }

    public static string getObjectPrefix(string strObjectName){
        Schema.SObjectType ObjectType = Schema.getGlobalDescribe().get(strObjectName);//From the Object Api name retrieving the SObject
        Sobject Object_name = ObjectType.newSObject();
        Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
        Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
        string objectKeyPrefix = sobject_describe.getKeyPrefix();
        return objectKeyPrefix;
    }    
}