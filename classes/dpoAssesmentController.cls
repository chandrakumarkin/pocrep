/*
 * Lightninh Component: 
 * 
 ***/
public without sharing class dpoAssesmentController {

    public static dpoAssesmentController.RequestWrapper reqWrap;
    public static dpoAssesmentController.ResponseWrapper respWrap;
    public static dpoAssesmentController.RequestWrapper1 reqWrap1;
  
    @AuraEnabled
    public static dpoAssesmentController.ResponseWrapper getDataController(String reqWrapPram) {

        reqWrap = (dpoAssesmentController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, dpoAssesmentController.RequestWrapper.class);
        respWrap = new dpoAssesmentController.ResponseWrapper();

        respWrap = getDataControllerRecord(reqWrap.srId, respWrap);

        for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c 
            WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c =:GlobalConstants.COMMAND_BUTTON_SECTION
            AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
            respWrap.ButtonSection = sectionObj;
        }
        return respWrap;
    }


    /*
     * get service request and data controller amendment record
     *
     ****/
    public static dpoAssesmentController.ResponseWrapper getDataControllerRecord(String srId, ResponseWrapper respWrap) {

        String flowId = reqWrap.flowId;
       
        dpoAssesmentController.SRWrapper srWrap = new dpoAssesmentController.SRWrapper();

        if(String.IsNotBlank(srId)) {
        
            List<Sobject> sobjectList= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(srId),'');
            List<HexaBPM__Service_Request__c> srList= (List<HexaBPM__Service_Request__c>) sobjectList;
            system.debug('===========srList==============='+srList);
            
            string fieldNames = UtilitySoqlHelper.AllFields('Annual_Assessment__c');
            string soqlQuery = fieldNames + ' Where Service_Request__c =: srId order by createddate desc limit 1 ';
            List<Annual_Assessment__c> assesMentList = Database.query(soqlQuery);
            
            if(assesMentList.size() >0){
            
                //List<Sobject> sobjectList= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(srWrap.srObj.Annual_Assessment__r[0].Id),'');
                //List<Annual_Assessment__c> srList= (List<Annual_Assessment__c>) sobjectList;
                respWrap.annualAssesment = assesMentList[0];
                
                string othersCheck = GlobalConstants.MULITISELECT_OTHER;
                respWrap.howLongKeepOthers = (respWrap.annualAssesment.How_long_will_you_keep_such_data__c != null && respWrap.annualAssesment.How_long_will_you_keep_such_data__c.contains(othersCheck))? true : false;
                respWrap.howOftenSuch = (respWrap.annualAssesment.How_often_is_such_data_collected__c != null && respWrap.annualAssesment.How_often_is_such_data_collected__c.contains(othersCheck))? true : false;
                respWrap.natureOfrelationsShipOthers = (respWrap.annualAssesment.Nature_of_your_relationship_with_the_Ind__c != null && respWrap.annualAssesment.Nature_of_your_relationship_with_the_Ind__c.contains(othersCheck))? true : false;
                respWrap.technicalOrganiZationalOthers = (respWrap.annualAssesment.Technical_and_organizational_measures__c != null && respWrap.annualAssesment.Technical_and_organizational_measures__c.contains(othersCheck))? true : false;
                respWrap.currentStateEmergingOthers = (respWrap.annualAssesment.Current_state_of_emerging_or_advanced__c != null && respWrap.annualAssesment.Current_state_of_emerging_or_advanced__c.contains(othersCheck))? true : false;
                respWrap.typeOfOthersNone= (respWrap.annualAssesment.Types_of_processing_in_your_operations__c!= null && respWrap.annualAssesment.Types_of_processing_in_your_operations__c.contains('None of the above'))? true : false;

                
            }else respWrap.annualAssesment = new Annual_Assessment__c(Service_Request__c = srId);
            
        } else {
            respWrap.errorMessage = GlobalConstants.SR_IS_BLANK;
        }
        return respWrap;
    }
   
    
    @AuraEnabled
    public static dpoAssesmentController.ResponseWrapper onAmedSaveDB1(String reqWrapPram) {
        respWrap = new dpoAssesmentController.ResponseWrapper();
        try{
            reqWrap1 = (dpoAssesmentController.RequestWrapper1) JSON.deserializeStrict(reqWrapPram, dpoAssesmentController.RequestWrapper1.class);
            Annual_Assessment__c annualRec = reqWrap1.annualAssesment;
            upsert annualRec;
            Map<String, Object> fieldsToValue = annualRec.getPopulatedFieldsAsMap();
           
            delete [select Id from Annual_Assessment_Risk_and_Matrix__c where Annual_Assessment__c =: annualRec.Id];
           
            List<Annual_Assessment_PrePopulation_Matrix__mdt> assesMentList =  [select Id,Api_Field__c, Api_Value__c,Risk_Type__c,
                Likelihood_of_harm__c,Overall_risk__c,Risk_Assessment__c,Severity_of_harm__c,Risk_Description__c,Order__c,
                Mitigation_measures_to_reduce_or_elimina__c,Effect_on_risk__c,Residual_risk_help_text__c,UI_Label__c,Replace_Response__c
                from Annual_Assessment_PrePopulation_Matrix__mdt
            ];
            Map<string,Annual_Assessment_PrePopulation_Matrix__mdt>matrixMap = new Map<string,Annual_Assessment_PrePopulation_Matrix__mdt>();
            List<Annual_Assessment_Risk_and_Matrix__c>annualMatrixList = new List<Annual_Assessment_Risk_and_Matrix__c>();
            List<Annual_Assessment_PrePopulation_Matrix__mdt>assessmentMetaRecords = new List<Annual_Assessment_PrePopulation_Matrix__mdt>();
            
            for(Annual_Assessment_PrePopulation_Matrix__mdt am : assesMentList){
                //string keyValue = am.Api_Field__c+'-'+am.Api_Value__c+'-'+am.Risk_Type__c;
                string keyValue = am.Api_Field__c+'-'+am.Api_Value__c;
                matrixMap.put(keyValue,am);
            }
            system.debug('------matrixMap-----------'+matrixMap);
            SObjectType r = ((SObject)(Type.forName('Schema.'+'Annual_Assessment__c').newInstance())).getSObjectType();
            DescribeSObjectResult d = r.getDescribe();
        
            for(String fieldName : fieldsToValue.keySet()){
                string fieldType = string.valueOf(d.fields.getMap().get(fieldName).getDescribe().getType());
                Set<string> fieldValues = new Set<string>();

                if(fieldsToValue.get(fieldName) != null){
                    fieldValues.addAll(fieldType == GlobalConstants.MULITISELECT_TYPE ? string.valueOf(fieldsToValue.get(fieldName)).split(';') : new List<String>{string.valueOf(fieldsToValue.get(fieldName))});
                }

                for(string fn : fieldValues){
                    //string section5Rec =fieldName+'-'+fn+'-'+GlobalConstants.IDENTIFY_AND_ASSESS_RISK;
                    //string section6Rec =fieldName+'-'+fn+'-'+GlobalConstants.IDENTIFY_MEASURES_AND_REDUCE_RISK;
                    string section5Rec =fieldName+'-'+fn;
                    //string section6Rec =fieldName+'-'+fn+'-'+GlobalConstants.IDENTIFY_MEASURES_AND_REDUCE_RISK;
                    //Set<String> sectionRec = new Set<String>{section5Rec,section6Rec};
                    Set<String> sectionRec = new Set<String>{section5Rec};

                    for(string sec : sectionRec){
                         if(matrixMap.containskey(sec) && matrixMap.get(sec) != null){
                            Annual_Assessment_PrePopulation_Matrix__mdt ass = matrixMap.get(sec);
                            ass.Risk_Assessment__c = (ass.Replace_Response__c && ass.Risk_Assessment__c != null) ?  ass.Risk_Assessment__c.replace(GlobalConstants.REPLACE_RESPONSE_ASSESMENT, fn) : ass.Risk_Assessment__c;
                            assessmentMetaRecords.add(ass);
                        }
                    }
                }
            }
            
            if(assessmentMetaRecords.size() >0){
                for(Annual_Assessment_PrePopulation_Matrix__mdt amRisk : assessmentMetaRecords){
                    Annual_Assessment_Risk_and_Matrix__c am5 = new Annual_Assessment_Risk_and_Matrix__c(
                        Annual_Assessment__c = annualRec.Id,
                        Risk_Label__c = amRisk.Api_Value__c,
                       // Risk_Type__c = amRisk.Risk_Type__c,
                        UI_Label__c = amRisk.UI_Label__c
                    );
                    
                    if(amRisk.Risk_Description__c!= null)am5.Risk_Description__c= amRisk.Risk_Description__c;
                    if(amRisk.Likelihood_of_harm__c != null)am5.Likelihood_of_harm__c = amRisk.Likelihood_of_harm__c;
                    if(amRisk.Overall_risk__c != null)am5.Overall_risk__c = amRisk.Overall_risk__c;
                    if(amRisk.Severity_of_harm__c != null)am5.Severity_of_harm__c = amRisk.Severity_of_harm__c;
                    if(amRisk.Risk_Assessment__c != null)am5.Risk_Assessment__c = amRisk.Risk_Assessment__c;
                    if(amRisk.Mitigation_measures_to_reduce_or_elimina__c != null)am5.Mitigation_measures_to_reduce_or_elimina__c = amRisk.Mitigation_measures_to_reduce_or_elimina__c;
                    if(amRisk.Effect_on_risk__c != null)am5.Effect_on_risk__c = amRisk.Effect_on_risk__c;
                    if(amRisk.Residual_risk_help_text__c != null)am5.Residual_risk_help_text__c = amRisk.Residual_risk_help_text__c;
                    if(amRisk.Order__c != null)am5.Order_Of_Matrix__c= amRisk.Order__c;
                    annualMatrixList.add(am5);
                }
            }
            system.debug('=====111===annualMatrixList =================='+annualMatrixList); 
            if(annualMatrixList.size()>0)insert annualMatrixList;
            system.debug('==2222======annualMatrixList =================='+annualMatrixList); 
            
            HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(annualRec.Service_Request__c);
            PageFlowControllerHelper.objSR = objRequest;

            objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, reqWrap1.srId, reqWrap1.pageID);
            system.debug('=====objRequest========'+objRequest);
            upsert objRequest;
               
            // pageActionName 
            PageFlowControllerHelper objPB = new PageFlowControllerHelper();
            PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(reqWrap1.buttonId);
            system.debug('========responseNextPage =================='+responseNextPage.pg ); 
            respWrap.pageActionName = '';
            respWrap.pageActionName = responseNextPage.pg;

        } catch(dmlException e) { 
            respWrap.errorMessage = e.getMessage();
           system.debug('========dml exception=================='+e); 
           Log__c objLog = new Log__c();objLog.Description__c = e.getMessage();objLog.Type__c = 'dpoAssesmentController onAmedSaveDB1 Method';insert objLog;    
        }
        return respWrap;
    }

    
    @AuraEnabled
    public static dpoAssesmentController.ResponseWrapper onAmedSaveDB2(String reqWrapPram) {
        try{
            dpoAssesmentController.RequestWrapper2 reqWrap2 = (dpoAssesmentController.RequestWrapper2) JSON.deserializeStrict(reqWrapPram, dpoAssesmentController.RequestWrapper2.class);
           // List<Annual_Assessment_Risk_and_Matrix__c> amL = reqWrap2.annualAssesment;
            //List<Annual_Assessment_Risk_and_Matrix__c> amL1 = reqWrap2.annualMatrixAssesment;
            //if(amL1 != null && amL1.size() >0)update amL1;
            dpoAssesmentController.ResponseWrapper respWrap= new dpoAssesmentController.ResponseWrapper();

            Annual_Assessment__c amds = reqWrap2.amRec;
            if(amds != null)update amds;

            if(reqWrap2.srId != null){
                HexaBPM__Service_Request__c objRequest = new HexaBPM__Service_Request__c(id = reqWrap2.srId );
                boolean IsAllowedToSubmit = false;
                for(HexaBPM__Service_Request__c SR :[Select Id from HexaBPM__Service_Request__c 
                    where Id = :reqWrap2.srId and HexaBPM__Internal_SR_Status__r.Name =:GlobalConstants.STAT_DRAFT ]) {
                    IsAllowedToSubmit = true;
                }
                system.debug('====1111====objRequest=================='+objRequest);   
                system.debug('========IsAllowedToSubmit=================='+IsAllowedToSubmit);   

                if(IsAllowedToSubmit){
                    objRequest.HexaBPM__finalizeAmendmentFlg__c = true;
                    objRequest.HexaBPM__Submitted_Date__c = system.today();
                    objRequest.HexaBPM__Submitted_DateTime__c = system.now();
                    for(HexaBPM__SR_Status__c status :[select id, Name from HexaBPM__SR_Status__c 
                        where(Name =:GlobalConstants.STAT_SUBMITTED or Name = 'submitted') limit 1]) {
                        objRequest.HexaBPM__Internal_SR_Status__c = status.id;
                        objRequest.HexaBPM__External_SR_Status__c = status.id;
                    }
                    update objRequest;
                    system.debug('======222==objRequest=================='+objRequest);   
                }
            }
        } catch(dmlException e) {
            system.debug('========eexception=================='+e);   
            respWrap.errorMessage = e.getMessage();
            Log__c objLog = new Log__c();objLog.Description__c = e.getMessage();objLog.Type__c = 'dpoAssesmentController onAmedSaveDB2 Method';insert objLog;   
        }
        return respWrap;
    }

    @AuraEnabled
    public static dpoAssesmentController.ButtonResponseWrapper onAmendReviewPage(String reqWrapPram) {
        dpoAssesmentController.ButtonResponseWrapper respWrap= new dpoAssesmentController.ButtonResponseWrapper();

        try{
            dpoAssesmentController.RequestWrapper2 reqWrap2 = (dpoAssesmentController.RequestWrapper2) JSON.deserializeStrict(reqWrapPram, dpoAssesmentController.RequestWrapper2.class);
            system.debug('========reqWrap2=================='+reqWrap2);   
            if(reqWrap2.buttonId != null){
                List<HexaBPM__Section_Detail__c> buttonSec = [select Id,HexaBPM__Component_Label__c from HexaBPM__Section_Detail__c
                    where Id=:reqWrap2.buttonId
                    limit 1
                ];
                system.debug('========buttonSec=================='+buttonSec);   

                if(buttonSec != null && buttonSec.size() >0 && buttonSec[0].HexaBPM__Component_Label__c == 'Next'){
                    List<Annual_Assessment_Risk_and_Matrix__c> amL = reqWrap2.annualAssesment;
                    List<Annual_Assessment_Risk_and_Matrix__c> amL1 = reqWrap2.annualMatrixAssesment;
                    if(amL != null && amL.size() >0)update amL; 
                    if(amL1 != null && amL1.size() >0)update amL1; 
                    system.debug('========amL1=================='+amL1);   
                    system.debug('========amL=================='+amL); 
                    if(reqWrap2.srId != null){
                        HexaBPM__Service_Request__c objRequest = new HexaBPM__Service_Request__c(id = reqWrap2.srId );
                        update objRequest;
                    }
                }
            }
            respWrap = getButtonAction(reqWrap2.srId, reqWrap2.buttonId, reqWrap2.pageID);
        } catch(dmlException e) {
            respWrap.errorMessage = e.getMessage();
            system.debug('========eexception=================='+e);   
            Log__c objLog = new Log__c();objLog.Description__c = e.getMessage();objLog.Type__c = 'dpoAssesmentController onAmendReviewPage Method';insert objLog;   
        }
        return respWrap;
    }



    /////////////////////////////////Wrapper  classes//////////////////////////////////////
    /*
     * data will come from client side
     */
    public class RequestWrapper{
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public string pageID { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public SRWrapper srWrap { get; set; }

        public RequestWrapper() { 
        }
    }
    
     public class RequestWrapper1{
        //@AuraEnabled public Id srId { get; set; }
        @AuraEnabled public Annual_Assessment__c annualAssesment { get; set; }
        @AuraEnabled public string buttonId{ get; set; }
        @AuraEnabled public string srId{ get; set; }
        @AuraEnabled public string pageID { get; set; }
        @AuraEnabled public string flowId { get; set; }
        
        public RequestWrapper1() { 
        }
    }

     public class RequestWrapper2{
        //@AuraEnabled public Id srId { get; set; }
        @AuraEnabled public List<Annual_Assessment_Risk_and_Matrix__c> annualAssesment { get; set; }
        @AuraEnabled public List<Annual_Assessment_Risk_and_Matrix__c> annualMatrixAssesment { get; set; }
        @AuraEnabled public Annual_Assessment__c amRec{ get; set; }
        @AuraEnabled public string srId{ get; set; }
        @AuraEnabled public string pageID { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string buttonId{ get; set; }
        @AuraEnabled public string labelName{ get; set; }

        public RequestWrapper2() { 
        }
    }


    public class SRWrapper{
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public Boolean isDraft { get; set; }
        public SRWrapper() {
        }
    }
    
    /*
     * Result will send to client side
     */
    public class ResponseWrapper{
        @AuraEnabled public SRWrapper srWrap { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled
        public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        @AuraEnabled public Annual_Assessment__c annualAssesment { get; set; }
        @AuraEnabled public String pageActionName { get; set; }
        
        @AuraEnabled public boolean howLongKeepOthers { get; set; }
        @AuraEnabled public boolean howOftenSuch { get; set; }
        @AuraEnabled public boolean natureOfrelationsShipOthers { get; set; }
        @AuraEnabled public boolean technicalOrganiZationalOthers { get; set; }
        @AuraEnabled public boolean currentStateEmergingOthers { get; set; }
        @AuraEnabled public boolean typeOfOthersNone{ get; set; }
                
        public ResponseWrapper() { 
        }
    }

    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string PageId) {

        system.debug('=====PageId========'+PageId);
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
        PageFlowControllerHelper.objSR = objRequest;
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();
        
        if(ButtonId != null){
            List<HexaBPM__Section_Detail__c> buttonSec = [select Id,HexaBPM__Component_Label__c from HexaBPM__Section_Detail__c
                where Id=:ButtonId
                limit 1
            ];
            system.debug('========buttonSec=================='+buttonSec);   

            if(buttonSec != null && buttonSec.size() >0 && buttonSec[0].HexaBPM__Component_Label__c == 'Next'){
                objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
                system.debug('=====objRequest========'+objRequest);
                upsert objRequest;
            }
        }
        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;

        system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
        return respWrap;
    }
    public class ButtonResponseWrapper {
        @AuraEnabled public String pageActionName { get; set; }
        @AuraEnabled public string communityName { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public string CommunityPageName { get; set; }
        @AuraEnabled public string sitePageName { get; set; }
        @AuraEnabled public string strBaseUrl { get; set; }
        @AuraEnabled public string srId { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public boolean isPublicSite { get; set; }

    }
    
    /* ---------------------------------------------------------------------
     * Method name : fetchSRObjFields
     * description : used to get fetch the initial wrapper
     * --------------------------------------------------------------------*/
    @AuraEnabled
    public static RespondWrap fetchSRObjFields(string requestWrapParam) {
        
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap = new RespondWrap();
        list<secDetailWrap> secwrpList = new list<secDetailWrap>();
        map<string, boolean> lockingMap = new map<string, boolean>();
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        string ServiceRequestId = reqWrap.srId;
        
        if(ServiceRequestId != null) {
            
            for(User usr :[select Contact.accountID, ContactId FROM User Where Id = :userInfo.getUserId()]) {
                respWrap.isBackendUser = (usr.ContactId != null) ? false : true;
            }
            
            map<string, string> srObjCaseSensitiveFieldAPIName = getCaseSensitiveAPIFieldname('HexaBPM__Service_Request__c');
            HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c();
            map<string, Schema.SObjectType> m = Schema.getGlobalDescribe();
            SObjectType  objtype;
            DescribeSObjectResult objDef1;
            map<string, SObjectField> fieldmap;
            if(m.get('HexaBPM__Service_Request__c') != null)
                objtype = m.get('HexaBPM__Service_Request__c');
            if(objtype != null)
                objDef1 = objtype.getDescribe();
            if(objDef1 != null)
                fieldmap = objDef1.fields.getmap();
            set<string> setSRFields = new set<string>();
            if(fieldmap != null) {
                for(Schema.SObjectField strFld :fieldmap.values()) {
                    Schema.DescribeFieldResult fd = strFld.getDescribe();
                    if(fd.isCustom()) {
                        setSRFields.add(string.valueOf(strFld).toLowerCase());
                    }
                }
            }
            setSRFields.add('select_who_will_be_signing_the_psa__r.individual_corporate_name__c');

            string SRqry = 'Select Id,Name';
            if(setSRFields.size() > 0) {
                for(string srField :setSRFields) {
                    SRqry += ',' + srField;
                }
            }
            SRqry += ' From HexaBPM__Service_Request__c where Id=:ServiceRequestId';
            PageFlowControllerHelper pghelper = new PageFlowControllerHelper();
            PageFlowControllerHelper.PageFlowContent ReviewContent = new PageFlowControllerHelper.PageFlowContent();
            for(HexaBPM__Service_Request__c srRequest :database.query(SRqry)) {
                objSR = srRequest;
                pghelper.objRequest = objSR;
            }
            respWrap.ShowDecAndButton = OB_QueryUtilityClass.reviewPageButtonShowCheck(objSR);
            system.debug('pghelper.objRequest==>' + pghelper.objRequest);
            ReviewContent = pghelper.getReviewConfirmContent(reqWrap.flowID, objSR.HexaBPM__Record_Type_Name__c);

            //query for section details
            for(HexaBPM__Section__c secObj :ReviewContent.lstSections) {
                secDetailWrap secwrp = new secDetailWrap();
                secwrp.secName = secObj.name;
                secwrp.pageName = secObj.HexaBPM__Page__r.name;
                if(ReviewContent.MapSectionDetails != null && ReviewContent.MapSectionDetails.get(secObj.Id) != null) {
                    system.debug('**MapSectionDetails**' + ReviewContent.MapSectionDetails);
                    secwrp.srSecDetailObj = ReviewContent.MapSectionDetails.get(secObj.Id);
                    for(HexaBPM__Section_Detail__c secDetailObj :ReviewContent.MapSectionDetails.get(secObj.Id)) {
                        if(srObjCaseSensitiveFieldAPIName.containsKey((secDetailObj.HexaBPM__Field_API_Name__c).toLowerCase())) {
                            fieldNameAndLableWrap fieldWrap = new fieldNameAndLableWrap();
                            fieldWrap.fieldAPIName = srObjCaseSensitiveFieldAPIName.get((secDetailObj.HexaBPM__Field_API_Name__c).toLowerCase());
                            fieldWrap.fieldValue = objSR.get(fieldWrap.fieldAPIName);
                            fieldWrap.FieldLable = secDetailObj.HexaBPM__Component_Label__c;
                            secwrp.fieldNameAndLableWrapList.add(fieldWrap);
                        }
                    }
                    secwrpList.add(secwrp);
                }
            }
            
       
            List<Sobject> sobjectListRec= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(reqWrap.srId),'');
            List<HexaBPM__Service_Request__c> srListRec= (List<HexaBPM__Service_Request__c>) sobjectListRec;
            respWrap.srObj = (srListRec != null && srListRec.size()>0) ? srListRec[0] : new HexaBPM__Service_Request__c();
            set<string> docCodeToInclude = new set<string>();
                     
            //query for document record
            for (HexaBPM__SR_Doc__c docObj :[SELECT id, HexaBPM__Document_Name__c, HexaBPM__Doc_ID__c, 
            Download_URL__c, HexaBPM__Document_Description__c, HexaBPM__Status__c, HexaBPM_Amendment__c, 
            HexaBPM__Document_Master__r.HexaBPM__Code__c,HexaBPM_Amendment__r.cessation_date__c,
            HexaBPM__Service_Request__r.SR_Number__c, Amendment_Name__c FROM HexaBPM__SR_Doc__c WHERE HexaBPM__Service_Request__c = :reqWrap.srId]) {
                respWrap.docWrap.add(docObj);
            }
            system.debug('@@@@@@@ error at line 107 srid ---->  ' + reqWrap.srId);

            respWrap.secDetailWrap = secwrpList;
            
           respWrap.ButtonSection = new List<HexaBPM__Section_Detail__c>();

            for (HexaBPM__Section_Detail__c secDeatil :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c 
                WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
                AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId
                order by HexaBPM__Order__c desc
                ]) {
               // respWrap.ButtonSection = secDeatil;
                respWrap.ButtonSection.add(secDeatil);
            }
           
            system.debug('---------respWrap.ButtonSection-----------' + respWrap.ButtonSection);

           
            string querySoql = UtilitySoqlHelper.AllFields('Annual_Assessment__c');
            querySoql += ' Where Service_Request__c =: ServiceRequestId order by Createddate desc limit 1';
            List<Annual_Assessment__c> alList = Database.query(querySoql);
           
          
           Annual_Assessment__c amObj = (alList.size() >0 && alList!= null) ? alList[0] : new Annual_Assessment__c();
            respWrap.amList= new List<Annual_Assessment_Risk_and_Matrix__c>() ;
            respWrap.amMeasureList = new List<Annual_Assessment_Risk_and_Matrix__c>();
            respWrap.amRec = amObj;
            if(amObj != null && amObj.Id != null){
                string assesmentId = amObj.Id;
                string identifyRisk= GlobalConstants.IDENTIFY_AND_ASSESS_RISK;
                string identifyMeasures= GlobalConstants.IDENTIFY_MEASURES_AND_REDUCE_RISK;
                
                string amQuery = UtilitySoqlHelper.AllFields('Annual_Assessment_Risk_and_Matrix__c');
                string querySoqlAA = amQuery +' Where Annual_Assessment__c =: assesmentId  order by Order_Of_Matrix__c asc NULLS LAST'; //and Risk_Type__c=:identifyRisk order by Createddate asc';
                List<Annual_Assessment_Risk_and_Matrix__c> amList = Database.query(querySoqlAA);
                
               // string querySoqlAAR = amQuery +' Where Annual_Assessment__c =: assesmentId  and Risk_Type__c=:identifyMeasures order by Createddate asc';
                //List<Annual_Assessment_Risk_and_Matrix__c> amMeasList = Database.query(querySoqlAAR);
               
                respWrap.amList= amList ;
                //respWrap.amMeasureList =amMeasList ;
            }
        }

        return respWrap;
    }
    
    /* ---------------------------
     Wrapper list
     --------------------------*/
    public class RequestWrap {
        @AuraEnabled public string flowID { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public string srId { get; set; }
        public RequestWrap() {
        }
    }
    public class RespondWrap {
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public list<secDetailWrap> secDetailWrap { get; set; }
        @AuraEnabled public list<HexaBPM__SR_Doc__c> docWrap { get; set; }
        
        @AuraEnabled public List<HexaBPM__Section_Detail__c> ButtonSection { get; set; }
        @AuraEnabled public Boolean ShowDecAndButton { get; set; }
        @AuraEnabled public string showButton { get; set; }
        @AuraEnabled public qualifyPurposeFieldWrap qualPurpsFieldWrap { get; set; }
        @AuraEnabled public operatinglocationFieldWrap operatinglocFieldWrap { get; set; }
        @AuraEnabled public dataProtectionFieldWrap dataProtecFieldWrap { get; set; }
        @AuraEnabled public list<Account_Share_Detail__c> shareAllocationScreenDetails { get; set; }
        @AuraEnabled public list<HexaBPM_Amendment__c> shareholderAmendmentList { get; set; }
        @AuraEnabled public list<HexaBPM_Amendment__c> MultistructureShareholderAmendmentListInd { get; set; }
        @AuraEnabled public list<HexaBPM_Amendment__c> MultistructureShareholderAmendmentListBc { get; set; }
        @AuraEnabled public list<HexaBPM_Amendment__c> cessatedAmendments { get; set; }
        //@AuraEnabled public map<string,string> entitynameMap { get; set; }

        @AuraEnabled public list<Annual_Assessment_Risk_and_Matrix__c> amList{ get; set; }
        @AuraEnabled public list<Annual_Assessment_Risk_and_Matrix__c> amMeasureList{ get; set; }
        @AuraEnabled public Annual_Assessment__c amRec{ get; set; }


        @AuraEnabled public boolean isBackendUser { get; set; }
        @AuraEnabled public companyNameWrap companyNameWrap { get; set; }
        public RespondWrap() {
            
            docWrap = new list<HexaBPM__SR_Doc__c>();
            companyNameWrap = new companyNameWrap();
            qualPurpsFieldWrap = new qualifyPurposeFieldWrap();
            operatinglocFieldWrap = new operatinglocationFieldWrap();
            shareAllocationScreenDetails  = new list<Account_Share_Detail__c>();
            shareholderAmendmentList = new list<HexaBPM_Amendment__c>();
            dataProtecFieldWrap = new dataProtectionFieldWrap();
            MultistructureShareholderAmendmentListInd = new list<HexaBPM_Amendment__c>();
            MultistructureShareholderAmendmentListBc = new list<HexaBPM_Amendment__c>();
            cessatedAmendments = new list<HexaBPM_Amendment__c>();
            amList= new list<Annual_Assessment_Risk_and_Matrix__c>();
            amMeasureList = new list<Annual_Assessment_Risk_and_Matrix__c>();
            amRec = new Annual_Assessment__c();
        }
    }
 
    public class fieldNameAndLableWrap {
        @AuraEnabled public string fieldAPIName { get; set; }
        @AuraEnabled public string fieldLable { get; set; }
        @AuraEnabled public object fieldValue { get; set; }
        @AuraEnabled public string fieldType { get; set; }

        public fieldNameAndLableWrap() {
            fieldAPIName = '';
            fieldLable = '';
        }
    }
    public class companyNameWrap {
        @AuraEnabled public list<Company_Name__c> compObjList { get; set; }
        @AuraEnabled public list<fieldNameAndLableWrap> fieldNameAndLableWrapList { get; set; }
        public companyNameWrap() {
            compObjList = new list<Company_Name__c>();
            fieldNameAndLableWrapList = new list<fieldNameAndLableWrap>();
        }
    }
    public class operatinglocationFieldWrap {
        @AuraEnabled public list<fieldNameAndLableWrap> fieldNameAndLableWrapList { get; set; }
        public operatinglocationFieldWrap() {
            fieldNameAndLableWrapList = new list<fieldNameAndLableWrap>();
        }
    }
    
    public class dataProtectionFieldWrap {
        @AuraEnabled public list<fieldNameAndLableWrap> fieldNameAndLableWrapList { get; set; }
        public dataProtectionFieldWrap() {
            fieldNameAndLableWrapList = new list<fieldNameAndLableWrap>();
        }
    }


    public class qualifyPurposeFieldWrap {
        @AuraEnabled public list<fieldNameAndLableWrap> fieldNameAndLableWrapList { get; set; }
        public qualifyPurposeFieldWrap() {
            fieldNameAndLableWrapList = new list<fieldNameAndLableWrap>();
        }
    }
    public class secDetailWrap {
        @AuraEnabled public string secName { get; set; }
        @AuraEnabled public string pageName { get; set; }
        @AuraEnabled public list<HexaBPM__Section_Detail__c> srSecDetailObj { get; set; }
        @AuraEnabled public list<string> srAPIFieldNames { get; set; }
        @AuraEnabled public string lable { get; set; }
        @AuraEnabled public list<fieldNameAndLableWrap> fieldNameAndLableWrapList { get; set; }
        public secDetailWrap() {
            srAPIFieldNames = new list<string>();
            fieldNameAndLableWrapList = new list<fieldNameAndLableWrap>();
        }
    }
  
    public class CompanyWrapper {
        @AuraEnabled public HexaBPM__Section_Detail__c ButtonSection { get; set; }
    }
 
    
    /* ---------------------------------------------------------------------
     * Method name : getCaseSensitiveAPIFieldname
     * description : used to get exact case sensitive api field names
     *               for furthur evaluation.
     * --------------------------------------------------------------------*/
    public static map<string, string> getCaseSensitiveAPIFieldname(string ObjectName) {
        Map<string, string> caseSensitiveAPINameMap = new Map<string, string>();
        Map<string, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<string, Schema.SObjectField> fieldMap = schemaMap.get(ObjectName).getDescribe().fields.getMap();
        for(string fiedName :fieldMap.keySet()) {
            caseSensitiveAPINameMap.put(fieldMap.get(fiedName).getDescribe().getName().toLowerCase(), fieldMap.get(fiedName).getDescribe().getName());
        }
        return caseSensitiveAPINameMap;
    }
    
   
    public class assesmentRequest {

        @AuraEnabled public String contactId;
    }
    
    @AuraEnabled
    public static NewSRUtilityController.menuWrapperResponse createAssessmentSRs(string recType)  {
        
        NewSRUtilityController.menuWrapperResponse respWrap =  NewSRUtilityController.createHexaBPMSRs(recType);
        return respWrap;
    }
}