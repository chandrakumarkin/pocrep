/***********************************************************************************************
*  Author    : Suchita  
*  Company   : DIFC
*  Date      : 3rd March 2021
*  Purpose   : This class is used to Schedule Voluntary Strike off Batch
* Test Class : VoluntaryStrikeOffBatchTest
-----------------------------------------------------------------------------------------------
Modification History 
-----------------------------------------------------------------------------------------------
V.No    Date               Updated By      Description
-----------------------------------------------------------------------------------------------              
V1.0   3rd Mar 2021     Suchita Sharma     intial Version
**********************************************************************************************/
global class ScheduleVoluntaryStrikeOffBatch implements Schedulable {
  	global void execute(SchedulableContext sc){
        VoluntaryStrikeOffBatch Batchobj = new VoluntaryStrikeOffBatch();
        database.executeBatch(Batchobj);   
    }
}