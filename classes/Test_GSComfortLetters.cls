/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_GSComfortLetters {

    static testMethod void myUnitTest1() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Can_request_original_Letter__c = true;
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        String objRectype;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='GS_Comfort_Letters' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.name = 'Draft';
        objStatus.Code__c = 'Draft';
        insert objStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objStatus.id;
        objSR.External_SR_Status__c = objStatus.id;
        objSR.Type_of_Service__c = 'Employment Confirmation Letter; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Boat Registration; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Driving License; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Marine Driving License; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Passport Lost; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Temporary Work Permit;';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Company Vehicle Registration; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Personal Vehicle Registration; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC to Set up a Company; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Non-Objection for Hajj; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Non-Objection for Umrah; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Salary Certificate to Immigration; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Salary Certificate (Liquor Permit); ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for port entry pass; ';
        objSR.Emirate__c = 'Test';
        objSR.Monthly_Salary_AED__c = 20000;
        objSR.Passport_Number__c = 'Test';
        objSR.Name_of_Addressee__c = 'Test';
        objSR.Date_of_DIFC_Visa_Issuance__c = system.today();
        objSR.Name_of_Police_Station__c = 'Test';
        objSR.Service_Category__c = 'Test';
        objSR.Others_Please_Specify__c = 'Test';
        objSR.Is_Applicant_Salary_Above_AED_20_000__c = 'Yes';
        objSR.car_registration_number__c = 'Test';
        objSR.entity_name__c = 'Test';
        objSR.company_name__c = 'Test';
        objSR.business_name__c = 'Test';
        objSR.Post_Code__c = 'Test';
        objSR.Position__c = 'Test';
        objSR.Sponsor_Street__c = 'Test';
        objSR.Sponsor_First_Name__c = 'Test';
        objSR.Sponsor_Last_Name__c = 'Test';
        objSR.Sponsor_Middle_Name__c = 'Test';
        objSR.Sponsor_Building__c = 'Test';
        objSR.Sponsor_Mother_Full_Name__c = 'Test';
        objSR.Sponsor_P_O_Box__c = 'Test';
        objSR.Sponsor_Passport_No__c = 'Test';
        objSR.Monthly_Accommodation__c = 10000;
        objSR.Mortgage_Amount__c = 10000;
        objSR.Mother_Full_Name__c = 'Test';
        objSR.Building_Name__c = 'Test';
        objSR.Middle_Name__c = 'Test';
        objSR.First_Name__c = 'Test';
        objSR.Last_Name__c = 'Test';
        objSR.UID_Number__c = 'Test';
        objSR.Cage_No__c = 'Test';
        objSR.Other_Monthly_Allowances__c = 10000;
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.City_Town__c = 'Test';
        objSR.Commercial_Activity__c = 'Test';
        objSR.DIFC_store_specifications__c = 'Test';
        objSR.Usage_Type__c = 'Test';
        objSR.Emirate_State__c = 'Test';
        objSR.Emirates_Id_Number__c = 'Test';
        objSR.Event_Name__c = 'Test';
        objSR.FAX__c = 'Test';
        objSR.Floor__c = 'Test';
        objSR.Folio_Number__c = 'Test';
        objSR.Health_Card_Number__c = 'Test';
        objSR.Kindly_Note__c = 'Test';
        objSR.Last_Name_Arabic__c = 'Test';
        objSR.Declaration_Signatory_Capacity__c = 'Test';
        objSR.Client_s_Representative_in_Charge__c = 'Test';
        objSR.Street_Address__c = 'Test';
        objSR.Current_Registered_Building__c = 'Test';
        objSR.Commercial_Activity_Previous_Sponsor__c = 'Test';
        objSR.Tenant_Type_RORP__c = 'Test';
        objSR.Type_of_Unit__c = 'Test';
        objSR.Foreign_Registered_Level__c = 'Test';
        insert objSR; 
        
        Product2 objProd = new Product2();
        objProd.Name = 'GS Letters';
        objProd.ProductCode = 'GS Letters';
        objProd.IsActive = true;
        insert objProd;     
        
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = objProd.Id, UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'Comfort Letter';
        objPL.Product__c = objProd.Id;
        objPL.Priority__c = 1;
        objPL.Material_Code__c = 'GO-00112';
        lstPLs.add(objPL);
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        objDP = new Dated_Pricing__c();
        objDP.Pricing_Line__c = lstPLs[0].Id;
        objDP.Unit_Price__c = 5000;
        objDP.Date_From__c = system.today().addMonths(-1);
        lstDP.add(objDP);
        insert lstDP;
        
        System.runAs(objUser){
            Test.setCurrentPage(Page.GSComfortLetters);
            Apexpages.currentPage().getParameters().put('isDetail','true');
            Apexpages.currentPage().getParameters().put('id',objSR.id);
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            cls_GSComfortLetters clsObj1 = new cls_GSComfortLetters(con);
            cls_GSComfortLetters clsObj2 = new cls_GSComfortLetters();
            clsObj1.recordTypeName = 'GS_Comfort_Letters';
            clsObj2.recordTypeName = 'GS_Comfort_Letters';
            clsObj1.saveRequest();
            clsObj1.cancelRequest();
            clsObj1.editRequest();
            clsObj1.submitRequest();
            clsObj1.makeSectionVisible();
            clsObj1.confirmUpgrade();
            cls_SRGenericClassForMethods genMhd = new cls_SRGenericClassForMethods();
            genMhd.CancelServiceRequest(true,objSR,true);
            genMhd.upgradeSR(objSR,'GS_Comfort_Letters');
        }
    }
    
    static testMethod void myUnitTest2() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Can_request_original_Letter__c = true;
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        String objRectype;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='GS_Comfort_Letters' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.name = 'Submitted';
        objStatus.Code__c = 'Submitted';
        insert objStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objStatus.id;
        objSR.External_SR_Status__c = objStatus.id;
        objSR.Type_of_Service__c = 'Employment Confirmation Letter; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Boat Registration; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Driving License; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Marine Driving License; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Passport Lost; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Temporary Work Permit;';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Company Vehicle Registration; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for Personal Vehicle Registration; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC to Set up a Company; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Non-Objection for Hajj; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Non-Objection for Umrah; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Salary Certificate to Immigration; ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'Salary Certificate (Liquor Permit); ';
        objSR.Type_of_Service__c = objSR.Type_of_Service__c + 'NOC for port entry pass; ';
        objSR.Emirate__c = 'Test';
        objSR.Monthly_Salary_AED__c = 20000;
        objSR.Passport_Number__c = 'Test';
        objSR.Name_of_Addressee__c = 'Test';
        objSR.Date_of_DIFC_Visa_Issuance__c = system.today();
        objSR.Name_of_Police_Station__c = 'Test';
        objSR.Service_Category__c = 'Test';
        objSR.Others_Please_Specify__c = 'Test';
        objSR.Is_Applicant_Salary_Above_AED_20_000__c = 'Yes';
        objSR.car_registration_number__c = 'Test';
        objSR.entity_name__c = 'Test';
        objSR.company_name__c = 'Test';
        objSR.business_name__c = 'Test';
        objSR.Post_Code__c = 'Test';
        objSR.Position__c = 'Test';
        objSR.Sponsor_Street__c = 'Test';
        objSR.Sponsor_First_Name__c = 'Test';
        objSR.Sponsor_Last_Name__c = 'Test';
        objSR.Sponsor_Middle_Name__c = 'Test';
        objSR.Sponsor_Building__c = 'Test';
        objSR.Sponsor_Mother_Full_Name__c = 'Test';
        objSR.Sponsor_P_O_Box__c = 'Test';
        objSR.Sponsor_Passport_No__c = 'Test';
        objSR.Monthly_Accommodation__c = 10000;
        objSR.Mortgage_Amount__c = 10000;
        objSR.Mother_Full_Name__c = 'Test';
        objSR.Building_Name__c = 'Test';
        objSR.Middle_Name__c = 'Test';
        objSR.First_Name__c = 'Test';
        objSR.Last_Name__c = 'Test';
        objSR.UID_Number__c = 'Test';
        objSR.Cage_No__c = 'Test';
        objSR.Other_Monthly_Allowances__c = 10000;
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.City_Town__c = 'Test';
        objSR.Commercial_Activity__c = 'Test';
        objSR.DIFC_store_specifications__c = 'Test';
        objSR.Usage_Type__c = 'Test';
        objSR.Emirate_State__c = 'Test';
        objSR.Emirates_Id_Number__c = 'Test';
        objSR.Event_Name__c = 'Test';
        objSR.FAX__c = 'Test';
        objSR.Floor__c = 'Test';
        objSR.Folio_Number__c = 'Test';
        objSR.Health_Card_Number__c = 'Test';
        objSR.Kindly_Note__c = 'Test';
        objSR.Last_Name_Arabic__c = 'Test';
        objSR.Declaration_Signatory_Capacity__c = 'Test';
        objSR.Client_s_Representative_in_Charge__c = 'Test';
        objSR.Street_Address__c = 'Test';
        objSR.Current_Registered_Building__c = 'Test';
        objSR.Commercial_Activity_Previous_Sponsor__c = 'Test';
        objSR.Tenant_Type_RORP__c = 'Test';
        objSR.Type_of_Unit__c = 'Test';
        objSR.Foreign_Registered_Level__c = 'Test';
        insert objSR; 
        
        Product2 objProd = new Product2();
        objProd.Name = 'GS Letters';
        objProd.ProductCode = 'GS Letters';
        objProd.IsActive = true;
        insert objProd;     
        
        Id pricebookId = Test.getStandardPricebookId();
        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = pricebookId, Product2Id = objProd.Id, UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'Comfort Letter';
        objPL.Product__c = objProd.Id;
        objPL.Priority__c = 1;
        objPL.Material_Code__c = 'GO-VIP112';
        lstPLs.add(objPL);
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        objDP = new Dated_Pricing__c();
        objDP.Pricing_Line__c = lstPLs[0].Id;
        objDP.Unit_Price__c = 5000;
        objDP.Date_From__c = system.today().addMonths(-1);
        lstDP.add(objDP);
        insert lstDP;
        
        System.runAs(objUser){
            Test.setCurrentPage(Page.GSComfortLetters);
            Apexpages.currentPage().getParameters().put('id',objSR.id);
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            cls_GSComfortLetters clsObj1 = new cls_GSComfortLetters(con);
            cls_GSComfortLetters clsObj2 = new cls_GSComfortLetters();
            clsObj1.recordTypeName = 'GS_Comfort_Letters';
            clsObj2.recordTypeName = 'GS_Comfort_Letters';
            clsObj1.saveRequest();
            clsObj1.cancelRequest();
            clsObj1.editRequest();
            clsObj1.submitRequest();
            clsObj1.makeSectionVisible();
            clsObj1.confirmUpgrade();
            cls_SRGenericClassForMethods genMhd = new cls_SRGenericClassForMethods();
            genMhd.CancelServiceRequest(true,objSR,false);
        }
    }
    
    static testMethod void myUnitTest3() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Can_request_original_Letter__c = true;
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='GS_Comfort_Letters' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.name = 'Draft';
        objStatus.Code__c = 'Draft';
        insert objStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Country__c = 'India; ';
        objSR.Country__c = objSR.Country__c + 'Afghanistan; ';
        objSR.Country__c = objSR.Country__c + 'Pakistan; ';
        objSR.Emirate__c = 'Test';
        objSR.Monthly_Salary_AED__c = 20000;
        objSR.Passport_Number__c = 'Test';
        objSR.Name_of_Addressee__c = 'Test';
        objSR.Date_of_DIFC_Visa_Issuance__c = system.today();
        objSR.Name_of_Police_Station__c = 'Test';
        objSR.Service_Category__c = 'Test';
        objSR.Others_Please_Specify__c = 'Test';
        objSR.Is_Applicant_Salary_Above_AED_20_000__c = 'Yes';
        objSR.car_registration_number__c = 'Test';
        objSR.entity_name__c = 'Test';
        objSR.company_name__c = 'Test';
        objSR.business_name__c = 'Test';
        objSR.Post_Code__c = 'Test';
        objSR.Position__c = 'Test';
        objSR.Sponsor_Street__c = 'Test';
        objSR.Sponsor_First_Name__c = 'Test';
        objSR.Sponsor_Last_Name__c = 'Test';
        objSR.Sponsor_Middle_Name__c = 'Test';
        objSR.Sponsor_Building__c = 'Test';
        objSR.Sponsor_Mother_Full_Name__c = 'Test';
        objSR.Sponsor_P_O_Box__c = 'Test';
        objSR.Sponsor_Passport_No__c = 'Test';
        objSR.Monthly_Accommodation__c = 10000;
        objSR.Mortgage_Amount__c = 10000;
        objSR.Mother_Full_Name__c = 'Test';
        objSR.Building_Name__c = 'Test';
        objSR.Middle_Name__c = 'Test';
        objSR.First_Name__c = 'Test';
        objSR.Last_Name__c = 'Test';
        objSR.UID_Number__c = 'Test';
        objSR.Cage_No__c = 'Test';
        objSR.Other_Monthly_Allowances__c = 10000;
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.City_Town__c = 'Test';
        objSR.Commercial_Activity__c = 'Test';
        objSR.DIFC_store_specifications__c = 'Test';
        objSR.Usage_Type__c = 'Test';
        objSR.Emirate_State__c = 'Test';
        objSR.Emirates_Id_Number__c = 'Test';
        objSR.Event_Name__c = 'Test';
        objSR.FAX__c = 'Test';
        objSR.Floor__c = 'Test';
        objSR.Folio_Number__c = 'Test';
        objSR.Health_Card_Number__c = 'Test';
        objSR.Kindly_Note__c = 'Test';
        objSR.Last_Name_Arabic__c = 'Test';
        objSR.Declaration_Signatory_Capacity__c = 'Test';
        objSR.Client_s_Representative_in_Charge__c = 'Test';
        objSR.Salary_Confidential__c = '20000';
        insert objSR; 
        
        Test.setCurrentPage(Page.GSComfortLetters);
        cls_GSComfortLetters clsObj2 = new cls_GSComfortLetters();
        clsObj2.recordTypeName = 'GS_Comfort_Letters';
        clsObj2.saveRequest();
        clsObj2.confirmUpgrade();
        clsObj2.cancelRequest();
    }
    
    
    static testMethod void myUnitTest4() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Can_request_original_Letter__c = true;
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        String objRectype;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='GS_Comfort_Letters' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.name = 'Submitted';
        objStatus.Code__c = 'Submitted';
        insert objStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objStatus.id;
        objSR.External_SR_Status__c = objStatus.id;
        objSR.Type_of_Service__c = 'Employment Confirmation Letter; ';
        insert objSR; 
        
        System.runAs(objUser){
            Test.setCurrentPage(Page.GSComfortLetters);
            Apexpages.currentPage().getParameters().put('id',objSR.id);
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            cls_GSComfortLetters clsObj1 = new cls_GSComfortLetters(con);
            cls_GSComfortLetters clsObj2 = new cls_GSComfortLetters();
            cls_SRWrapperClassForFields cls_Wrap = new cls_SRWrapperClassForFields('GS_Comfort_Letters');
            clsObj1.recordTypeName = 'GS_Comfort_Letters';
            clsObj2.recordTypeName = 'GS_Comfort_Letters';
            cls_Wrap.originalOrSoftCopy = 'Original';
            cls_Wrap.confirmation = true;
             cls_Wrap.NOCboat = true;
            cls_Wrap.NOCdriving = true;
            cls_Wrap.NOCmarine = true;
            cls_Wrap.NOCpassport = true;
            cls_Wrap.NOCtemporary = true;
            cls_Wrap.NOCCompanyVehicle = true;
            cls_Wrap.NOCcompany = true;
            cls_Wrap.NOChajj = true;
            cls_Wrap.NOCumrah = true;
            cls_Wrap.NOCPortEntry = true;
            cls_Wrap.NOCMagrothercomp = true;
            clsObj1.selectedCompanyId =  objAccount.id;      
            clsObj1.saveRequest();
            clsObj1.setPrimaryTenantDetails();
            clsObj1.onlyOriginalAllowedMethod();
            cls_Wrap.initializeValuesForComfortLetters();
            
        }
    }
}