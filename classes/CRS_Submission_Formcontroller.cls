public class CRS_Submission_Formcontroller
{

private final CRS_Form__c CRs;

public Id SeletedId{ get; set; }

public boolean IsReadOnly{get;set;}

    public CRS_Submission_Formcontroller(ApexPages.StandardController stdController)
    {
    if (!Test.isRunningTest()) 
        stdController.addFields(new List<String>{'Status__c'});
        
         this.CRs= (CRS_Form__c)stdController.getRecord();
         CompaniesRecords=new List<CRSMainWrapper>();
         PersonRecords=new List<CRSMainWrapper>();
         existingRecords();
         
         if(this.CRs.Status__c=='Draft')
         IsReadOnly=false;
        else
                 IsReadOnly=true;

    }

    Map<id,List<CRS_Payment__c>>  ControllingPayments;
    public void existingRecords()
    {
        ControllingPayments=new Map<id,List<CRS_Payment__c>>();
        
        for(CRS_Client__c ObjClint:[select id,Name,Address__c,City__c,Client_Type__c,Controlling_Person_Type__c ,Country__c,CRS_Form__c,Date_of_Birthday__c,First_Name__c,IN__c,Last_Name__c,Master_Client__c,TIN__c,
        (select id,Client__c,Payment_Amount__c,Payment_Type__c from CRS_Payments__r) from CRS_Client__c where CRS_Form__c=:CRs.id and Client_Type__c='Controlling'])
        {
            
            ControllingPayments.put(ObjClint.id,ObjClint.CRS_Payments__r);
            
        }
        System.debug('==ControllingPayments==>'+ControllingPayments);
        for(CRS_Client__c ObjClint:[select id,Name,Address__c,City__c,Client_Type__c,Country__c,CRS_Form__c,Date_of_Birthday__c,First_Name__c,IN__c,Last_Name__c,Master_Client__c,TIN__c,
        (select id,Name,Address__c,City__c,Controlling_Person_Type__c ,Client_Type__c,Country__c,CRS_Form__c,Date_of_Birthday__c,First_Name__c,IN__c,Last_Name__c,Master_Client__c,TIN__c from Clients__r),(select id,Client__c,Payment_Amount__c,Payment_Type__c from CRS_Payments__r) from CRS_Client__c where CRS_Form__c=:CRs.id])
        {
            if(ObjClint.Client_Type__c=='Company')
            {
                    
                     CompaniesRecords.add(addClint(ObjClint,CRs));
            }
            else if(ObjClint.Client_Type__c=='Person')
            {
                    PersonRecords.add(addClint(ObjClint,CRs));
            }
            
        }
        
        
        
    }
    
    
    public CRSMainWrapper addClint(CRS_Client__c ObjClint,CRS_Form__c tempCRs)
    {
        System.debug('==ObjClint====>'+ObjClint);
        CRSMainWrapper TempCRSMainWrapper=new CRSMainWrapper();
        
        TempCRSMainWrapper.MainRecord=ObjClint;
        TempCRSMainWrapper.Forms=tempCRs;
        TempCRSMainWrapper.ListCRSPayment=ObjClint.CRS_Payments__r;
        
        if(ObjClint.Clients__r!=null)
        {
            List<CRSControllingPersonWrapper> ListCP=new List<CRSControllingPersonWrapper>();
            
            for(CRS_Client__c tmpCl:ObjClint.Clients__r)
            {
                
                CRSControllingPersonWrapper ObjCP=new CRSControllingPersonWrapper();
                ObjCP.ControllingPersonRecord=tmpCl;
               // ObjCP.ListCRSPayment=ControllingPayments.get(tmpCl.id);
                
                if(ControllingPayments.get(tmpCl.id)!=null)
                    ObjCP.ListCRSPayment=ControllingPayments.get(tmpCl.id);
                else
                     ObjCP.ListCRSPayment=new List<CRS_Payment__c>();
                 
                 
                ListCP.add(ObjCP);
                
            }
            
            TempCRSMainWrapper.ControllingPerson=ListCP;
        }
        
        
        
        return TempCRSMainWrapper;
        
    }
    
        public List<CRSMainWrapper> CompaniesRecords{get;set;}
        public List<CRSMainWrapper> PersonRecords{get;set;}
    
   
      
      
      public pageReference AddControllingPerson()
     {
        System.debug('===SeletedId==>'+SeletedId);
         if(SeletedId!=null)
         {
            for(CRSMainWrapper ObjPer:PersonRecords)
            {
                if(ObjPer.MainRecord.id==SeletedId)
                {
                    CRSControllingPersonWrapper ObjCRS_Payment=new CRSControllingPersonWrapper();
                    CRS_Client__c ObjClient=new CRS_Client__c();
                    ObjClient.Master_Client__c=SeletedId;
                    ObjClient.CRS_Form__c=ObjPer.MainRecord.CRS_Form__c;
                     ObjClient.Client_Type__c='Controlling';
                    
                    ObjCRS_Payment.ControllingPersonRecord=ObjClient;
                    
                     ObjPer.ControllingPerson.add(ObjCRS_Payment);
                    
                }
            }
            for(CRSMainWrapper ObjPer:CompaniesRecords)
            {
                if(ObjPer.MainRecord.id==SeletedId)
                {
                    CRSControllingPersonWrapper ObjCRS_Payment=new CRSControllingPersonWrapper();
                    CRS_Client__c ObjClient=new CRS_Client__c();
                    ObjClient.Master_Client__c=SeletedId;
                    ObjClient.CRS_Form__c=ObjPer.MainRecord.CRS_Form__c;
                    ObjClient.Client_Type__c='Controlling';
                    
                    ObjCRS_Payment.ControllingPersonRecord=ObjClient;
                    
                     ObjPer.ControllingPerson.add(ObjCRS_Payment);
                    
                }
                
            }
          
         }
          
          return null;

     }
      public pageReference AddMainPayment()
     {
         System.debug('===SeletedId==>'+SeletedId);
         List<CRS_Payment__c> ListPayments=new List<CRS_Payment__c>();
         
         if(SeletedId!=null)
         {
            for(CRSMainWrapper ObjPer:PersonRecords)
            {
                if(ObjPer.MainRecord.id==SeletedId)
                {
                    if(ObjPer.ListCRSPayment.isEmpty())
                    {
                        for(  Integer i=0;i<4;i++)
                        {
                            CRS_Payment__c ObjCRS_Payment=new CRS_Payment__c();
                            ObjCRS_Payment.Client__c=SeletedId;
                            ObjPer.ListCRSPayment.add(ObjCRS_Payment);
                            ListPayments.add(ObjCRS_Payment);
                        }
                    }
                    else
                    {
                        CRS_Payment__c ObjCRS_Payment=new CRS_Payment__c();
                        ObjCRS_Payment.Client__c=SeletedId;
                        ObjPer.ListCRSPayment.add(ObjCRS_Payment);
                        ListPayments.add(ObjCRS_Payment);
                        
                    }
                        
                    
                    
                }
            }
            for(CRSMainWrapper ObjPer:CompaniesRecords)
            {
                if(ObjPer.MainRecord.id==SeletedId)
                {
                  if(ObjPer.ListCRSPayment.isEmpty())
                    {
                        for(  Integer i=0;i<4;i++)
                        {
                            CRS_Payment__c ObjCRS_Payment=new CRS_Payment__c();
                            ObjCRS_Payment.Client__c=SeletedId;
                            ObjPer.ListCRSPayment.add(ObjCRS_Payment);
                            ListPayments.add(ObjCRS_Payment);
                        }
                    }
                    else
                    {
                        CRS_Payment__c ObjCRS_Payment=new CRS_Payment__c();
                        ObjCRS_Payment.Client__c=SeletedId;
                        ObjPer.ListCRSPayment.add(ObjCRS_Payment);
                        ListPayments.add(ObjCRS_Payment);
                        
                    }
                    
                    
                    
                }
                
            }
          
         }
          
          upsert ListPayments;
          return null;

     }
    
    
    public void AddControllingPersonPayment()
    {
        List<CRS_Payment__c> ListPayments=new List<CRS_Payment__c>();
        
           System.debug('===SeletedId==>'+SeletedId);
             if(SeletedId!=null)
             {
                for(CRSMainWrapper ObjPer:PersonRecords)
                {
                    for(CRSControllingPersonWrapper ObjCRSCP:ObjPer.ControllingPerson)
                    {
                        
                        if(ObjCRSCP.ControllingPersonRecord.id==SeletedId)
                        {
                            if(ObjCRSCP.ListCRSPayment==null)
                                ObjCRSCP.ListCRSPayment=new list<CRS_Payment__c>();
                            
                               
                                 
                                 
                                  if(ObjCRSCP.ListCRSPayment.isEmpty())
                                    {
                                        for(  Integer i=0;i<4;i++)
                                        {
                                            CRS_Payment__c ObjCRS_Payment=new CRS_Payment__c();
                                            ObjCRS_Payment.Client__c=SeletedId;
                                            ObjCRSCP.ListCRSPayment.add(ObjCRS_Payment);
                                            ListPayments.add(ObjCRS_Payment);
                                        }
                                    }
                                    else
                                    {
                                        CRS_Payment__c ObjCRS_Payment=new CRS_Payment__c();
                                        ObjCRS_Payment.Client__c=SeletedId;
                                        ObjCRSCP.ListCRSPayment.add(ObjCRS_Payment);
                                        ListPayments.add(ObjCRS_Payment);
                                        
                                    }
                    
                    
                        }
                        
                    }
                    
                    
                }
                for(CRSMainWrapper ObjPer:CompaniesRecords)
                {
                    for(CRSControllingPersonWrapper ObjCRSCP:ObjPer.ControllingPerson)
                    {
                        
                        if(ObjCRSCP.ControllingPersonRecord.id==SeletedId)
                        {
                               
                                 
                                   if(ObjCRSCP.ListCRSPayment.isEmpty())
                                    {
                                        for(  Integer i=0;i<4;i++)
                                        {
                                            CRS_Payment__c ObjCRS_Payment=new CRS_Payment__c();
                                            ObjCRS_Payment.Client__c=SeletedId;
                                            ObjCRSCP.ListCRSPayment.add(ObjCRS_Payment);
                                            ListPayments.add(ObjCRS_Payment);
                                        }
                                    }
                                    else
                                    {
                                        CRS_Payment__c ObjCRS_Payment=new CRS_Payment__c();
                                        ObjCRS_Payment.Client__c=SeletedId;
                                        ObjCRSCP.ListCRSPayment.add(ObjCRS_Payment);
                                        ListPayments.add(ObjCRS_Payment);
                                        
                                    }
                                    
                                    
                        }
                        
                    }
                }
              
             }
             
             upsert ListPayments;
    }
    

     
     public void SaveAllMain()
    {
         List<CRS_Client__c> ListSave=new List<CRS_Client__c>();
         List<CRS_Payment__c> ListPayment=new List<CRS_Payment__c>();
         
         for(CRSMainWrapper ObjInd:PersonRecords)
         {
             
             ListSave.add(ObjInd.MainRecord);
             ListPayment.addAll(ObjInd.ListCRSPayment);
             if(ObjInd.ControllingPerson!=null)
             {
                 if(!ObjInd.ControllingPerson.isEmpty())
                 {
                     for(CRSControllingPersonWrapper ObjCp :ObjInd.ControllingPerson)
                     {
                         System.debug('==ObjCp==>'+ObjCp);
                        ListSave.add(ObjCp.ControllingPersonRecord);
                        ListPayment.addAll(ObjCp.ListCRSPayment);
                                
                     }
                     
                 }
                 
             }
             
             
         }
         
           for(CRSMainWrapper ObjInd:CompaniesRecords)
         {
             
             ListSave.add(ObjInd.MainRecord);
             ListPayment.addAll(ObjInd.ListCRSPayment);
             if(ObjInd.ControllingPerson!=null)
             {
                 if(!ObjInd.ControllingPerson.isEmpty())
                 {
                     for(CRSControllingPersonWrapper ObjCp :ObjInd.ControllingPerson)
                     {
                         System.debug('==ObjCp==>'+ObjCp);
                        ListSave.add(ObjCp.ControllingPersonRecord);
                        if(ObjCp.ListCRSPayment!=null)
                        ListPayment.addAll(ObjCp.ListCRSPayment);
                                
                     }
                     
                 }
                 
             }
             
             
         }
         
         
         if(!ListSave.isEmpty())
             upsert ListSave;
         
         if(!ListPayment.isEmpty())
             upsert ListPayment;
         
          
    }
    public void DeleteControllingPersonPayments()
    {
         List<CRS_Payment__c> ListDelete=new List<CRS_Payment__c>();
         System.debug('===SeletedId==>'+SeletedId);
         //SeletedId :Payment id
             if(SeletedId!=null)
             {
                for(CRSMainWrapper ObjPer:PersonRecords)
                {
                    for(CRSControllingPersonWrapper ObjCRSCP:ObjPer.ControllingPerson)
                    {
                        Integer i=0;
                        for(CRS_Payment__c ObjPay:ObjCRSCP.ListCRSPayment)
                        {
                            if(ObjPay.id==SeletedId)
                            {
                                ObjCRSCP.ListCRSPayment.Remove(i);
                                ListDelete.add(ObjPay);
                                 break;
                            }
                        }
                        
                    }
                    
                    
                }
                for(CRSMainWrapper ObjPer:CompaniesRecords)
                {
                    
                         for(CRSControllingPersonWrapper ObjCRSCP:ObjPer.ControllingPerson)
                        {
                            Integer i=0;
                            for(CRS_Payment__c ObjPay:ObjCRSCP.ListCRSPayment)
                            {
                                if(ObjPay.id==SeletedId)
                                {
                                    ObjCRSCP.ListCRSPayment.Remove(i);
                                    ListDelete.add(ObjPay);
                                     break;
                                }
                            }
                            
                        }
                        
                        
                    
                }
              
             }
        
    }
    
    public void DeleteControllingPersonMain()
    {
        boolean IsDeleted=false;
        
         List<CRS_Client__c> ListDelete=new List<CRS_Client__c>();
          System.debug('===SeletedId==>'+SeletedId);
             if(SeletedId!=null)
             {
                for(CRSMainWrapper ObjPer:PersonRecords)
                {
                    Integer i=0;
                    for(CRSControllingPersonWrapper ObjCRSCP:ObjPer.ControllingPerson)
                    {
                        
                        if(ObjCRSCP.ControllingPersonRecord.id==SeletedId && IsDeleted==false )
                        {
                            ObjPer.ControllingPerson.Remove(i);
                            ListDelete.add(ObjCRSCP.ControllingPersonRecord);
                            IsDeleted=true;
                             break;
                        }
                        i++;
                        
                    }
                    
                    
                }
                for(CRSMainWrapper ObjPer:CompaniesRecords)
                {
                    Integer i=0;
                    for(CRSControllingPersonWrapper ObjCRSCP:ObjPer.ControllingPerson)
                    {
                        
                        if(ObjCRSCP.ControllingPersonRecord.id==SeletedId  && IsDeleted==false)
                        {
                              ObjPer.ControllingPerson.Remove(i);
                               ListDelete.add(ObjCRSCP.ControllingPersonRecord);
                               IsDeleted=true;
                                break;
                                    
                        }
                        i++;
                        
                    }
                }
              
             }
             
              System.debug('==ListDelete==>'+ListDelete);
               if(!ListDelete.isEmpty())
               {
                   delete ListDelete;
               }
           

    }
    
    public void DeletePaymentMain()
    {
        System.debug('===SeletedId==>'+SeletedId);
      List<CRS_Payment__c> ListDelete=new List<CRS_Payment__c>();   
         
         if(SeletedId!=null)
         {
            for(CRSMainWrapper ObjPer:PersonRecords)
            {
                Integer i=0;
                for(CRS_Payment__c ObjCRS_Payment:ObjPer.ListCRSPayment)
                {
                   if(ObjCRS_Payment.Id==SeletedId)
                   {
                       ObjPer.ListCRSPayment.Remove(i);
                       ListDelete.add(ObjCRS_Payment);
                        break;
                       
                   }
                i++;  
                       
                }
            }
            for(CRSMainWrapper ObjPer:CompaniesRecords)
            {
                Integer i=0;
                for(CRS_Payment__c ObjCRS_Payment:ObjPer.ListCRSPayment)
                {
                   if(ObjCRS_Payment.Id==SeletedId)
                   {
                       ObjPer.ListCRSPayment.Remove(i);
                       ListDelete.add(ObjCRS_Payment);
                        break;
                       
                   }
                i++;  
                       
                }
                
            }
          
            delete ListDelete; 
         }
    }
    
    public void DeleteMain()
    {
         List<CRS_Client__c> ListDelete=new List<CRS_Client__c>();
         if(SeletedId!=null)
           {
                    Integer i=0;
                     for(CRSMainWrapper Obj: PersonRecords)
                     {
                         System.debug('==PersonRecords==>'+PersonRecords);
                         
                         if(Obj.MainRecord.Id==SeletedId)
                         {
                             PersonRecords.Remove(i);
                             ListDelete.add(Obj.MainRecord);
                             break;
                         }
                         i++;
                         
                     }
                      for(CRSMainWrapper Obj: CompaniesRecords)
                     {
                         System.debug('==CompaniesRecords==>'+CompaniesRecords);
                         
                         if(Obj.MainRecord.Id==SeletedId)
                         {
                             CompaniesRecords.Remove(i);
                             ListDelete.add(Obj.MainRecord);
                             break;
                         }
                         i++;
                         
                     }
               
           }
           
           
           
           System.debug('==ListDelete==>'+ListDelete);
           if(!ListDelete.isEmpty())
           {
               delete ListDelete;
           }
    }
    
    
      public void AddPerson()
      {
          CRSMainWrapper ObjCRSMainWrapper=new CRSMainWrapper();
          ObjCRSMainWrapper.MainRecord=new CRS_Client__c();
          ObjCRSMainWrapper.MainRecord.CRS_Form__c=CRs.id;
          ObjCRSMainWrapper.MainRecord.Client_Type__c='Person';
          
          ObjCRSMainWrapper.ListCRSPayment=new List<CRS_Payment__c>();
          ObjCRSMainWrapper.ControllingPerson=new List<CRSControllingPersonWrapper>();
          
          
          PersonRecords.add(ObjCRSMainWrapper);
          

      }
      
         public pageReference AddCompany()
      {
         CRSMainWrapper ObjCRSMainWrapper=new CRSMainWrapper();
          ObjCRSMainWrapper.MainRecord=new CRS_Client__c();
          ObjCRSMainWrapper.MainRecord.CRS_Form__c=CRs.id;
          ObjCRSMainWrapper.MainRecord.Client_Type__c='Company';
          
          ObjCRSMainWrapper.ListCRSPayment=new List<CRS_Payment__c>();
          ObjCRSMainWrapper.ControllingPerson=new List<CRSControllingPersonWrapper>();
          CompaniesRecords.add(ObjCRSMainWrapper);
          
          return null;
    }
    
    
       public void SaveMain()
      {
           List<CRS_Client__c> ListSave=new List<CRS_Client__c>();
          
         for(CRSMainWrapper Obj: PersonRecords)
         {
             System.debug('==PersonRecords==>'+PersonRecords);
             
             if(Obj.MainRecord.Id==null)
             {
                 Obj.MainRecord.Name=Obj.MainRecord.First_Name__c+' '+Obj.MainRecord.Last_Name__c;
                 ListSave.add(Obj.MainRecord);
             }
             
         }
         for(CRSMainWrapper Obj: CompaniesRecords)
         {
             System.debug('==PersonRecords==>'+CompaniesRecords);
             
             if(Obj.MainRecord.Id==null)
             {
         
                 ListSave.add(Obj.MainRecord);
             }
             
         }
         
         if(!ListSave.isEmpty())
         upsert ListSave;
     
      }
    
    
      public void CancelMain()
      {
         
          Integer i=0;
         for(CRSMainWrapper Obj: PersonRecords)
         {
             
             if(Obj.MainRecord.id==null)
                 {
                     PersonRecords.Remove(i);
                     break;
                     
                 }
             
            i++;
         }
         
           Integer j=0;
         for(CRSMainWrapper Obj: CompaniesRecords)
         {
             
             if(Obj.MainRecord.id==null)
                 {
                     CompaniesRecords.Remove(j);
                     break;
                     
                 }
             
            j++;
         }
         
          
      }
      
    
       public void SaveControllingperson()
      {
          List<CRS_Client__c> ListSave=new List<CRS_Client__c>();
           System.debug('===SeletedId==>'+SeletedId);
           if(SeletedId!=null)
           {
                    
          
                 for(CRSMainWrapper Obj: PersonRecords)
                 {
                     System.debug('==PersonRecords==>'+PersonRecords);
                     
                     if(Obj.MainRecord.Id==SeletedId)
                     {
                         for( CRSControllingPersonWrapper ObjCP:Obj.ControllingPerson)
                         {
                             if(ObjCP.ControllingPersonRecord.id==null)
                             {
                                 ObjCP.ControllingPersonRecord.Name=ObjCP.ControllingPersonRecord.First_Name__c+' '+ObjCP.ControllingPersonRecord.Last_Name__c;
                                ListSave.add(ObjCP.ControllingPersonRecord);         
                             }
                            
                         }
                        
                     }
                        
                 }
                 
                  for(CRSMainWrapper Obj: CompaniesRecords)
                 {
                     System.debug('==PersonRecords==>'+PersonRecords);
                     
                     if(Obj.MainRecord.Id==SeletedId)
                     {
                         for( CRSControllingPersonWrapper ObjCP:Obj.ControllingPerson)
                         {
                             if(ObjCP.ControllingPersonRecord.id==null)
                             {
                                 ObjCP.ControllingPersonRecord.Name=ObjCP.ControllingPersonRecord.First_Name__c+' '+ObjCP.ControllingPersonRecord.Last_Name__c;
                                ListSave.add(ObjCP.ControllingPersonRecord);         
                             }
                            
                         }
                        
                     }
                        
                 }
                 
                 
                 
                 
                 if(!ListSave.isEmpty())
                 upsert ListSave;
     
               
           }
     

      }
      
      
      public void CancelControllingperson()
      {
      System.debug('==CancelControllingperson:=SeletedId==>'+SeletedId);
      
        
         for(CRSMainWrapper Obj: PersonRecords)
         {
             
             if(Obj.MainRecord.id==SeletedId)
                 {
                      Integer i=0;
                      for( CRSControllingPersonWrapper ObjCP:Obj.ControllingPerson)
                         {
                             if(ObjCP.ControllingPersonRecord.id==null)
                             {
                                 Obj.ControllingPerson.Remove(i);
                                 break;
                             }
                             i++;
                         }
                 }
             
            
         }
         
        
         for(CRSMainWrapper Obj: CompaniesRecords)
         {
             
             if(Obj.MainRecord.id==SeletedId)
                 {
                      Integer j=0;
                      for( CRSControllingPersonWrapper ObjCP:Obj.ControllingPerson)
                         {
                             if(ObjCP.ControllingPersonRecord.id==null)
                             {
                                 Obj.ControllingPerson.Remove(j);
                                 break;
                             }
                            j++;
                         }
                 }
             
           
         }
         
      }
      
   
      
     


    //Main node records 
    public class CRSMainWrapper
    {
    
      public CRS_Client__c MainRecord{get; set;}
      public CRS_Form__c Forms{get;set;}
      public  List<CRS_Payment__c> ListCRSPayment{get; set;}
       public List<CRSControllingPersonWrapper> ControllingPerson{get; set;}
    
    
    }
     
     //2nd level records
     public class CRSControllingPersonWrapper
    {
    
      public  CRS_Client__c ControllingPersonRecord{get; set;}
       public List<CRS_Payment__c> ListCRSPayment{get; set;}
    
    }
    
    
    
  
    
    
    
}