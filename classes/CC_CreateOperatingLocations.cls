/**
* Description: Create Operating location of customer.
* 
* v1 Abbas 15th Dec, 2020
**/
global without sharing class CC_CreateOperatingLocations implements HexaBPM.iCustomCodeExecutable
{
     global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, 
                                      HexaBPM__Step__c stp)
     {
        string strResult = 'Success';
        if(stp!= null && stp.HexaBPM__SR__c != null && stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null){
            if(stp.HexaBPM__SR__r.Type_of_Entity__c!=null)
            {
                Account acc = new Account(Id=stp.HexaBPM__SR__r.HexaBPM__Customer__c);
                acc.Legal_Structure_Full__c = stp.HexaBPM__SR__r.Type_of_Entity__c;
                update acc;
            }
            try
            {
                String srId = stp.HexaBPM__SR__c;

              
                //For Hosting case
                if(stp.HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c != NULL ){
                    createOperatingLocations(SR,stp,stp.HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c,false);
                } 
                //for Corporate_Service_Provider_Name__c
                else if(stp.HexaBPM__SR__r.Corporate_Service_Provider_Name__c != NULL ){
                    createOperatingLocations(SR,stp,stp.HexaBPM__SR__r.Corporate_Service_Provider_Name__c,false);
                }
                // for Registered_Agent__c
                else if(stp.HexaBPM__SR__r.Registered_Agent__c != NULL ){
                    createOperatingLocations(SR,stp,stp.HexaBPM__SR__r.Registered_Agent__c,false);
                }
                // for Fund_Administrator_Name__c
                else if(stp.HexaBPM__SR__r.Fund_Administrator_Name__c != NULL ){
                    createOperatingLocations(SR,stp,stp.HexaBPM__SR__r.Fund_Administrator_Name__c,false);
                }
                // for Fund_Manager__c
                else if(stp.HexaBPM__SR__r.Fund_Manager__c != NULL ){
                   createOperatingLocations(SR,stp,stp.HexaBPM__SR__r.Fund_Manager__c,false);
                }
                // Activate own location
                else if( stp.HexaBPM__SR__r.HexaBPM__Customer__c != NULL)
                {
                    system.debug('$$$$$$ stp.HexaBPM__SR__r.HexaBPM__Customer__c '+stp.HexaBPM__SR__r.HexaBPM__Customer__c);
                    createOperatingLocations(SR,stp,stp.HexaBPM__SR__r.HexaBPM__Customer__c,true);
                }
                
            }
            catch(DMLException e)
            {
               strResult = OB_QueryUtilityClass.getMessageFromDMLException(e);
            }
            
        }
        return strResult;
    }
    // Creating OL based on the Hosting/Share Account  
    public static void createOperatingLocations (HexaBPM__Service_Request__c SR, 
                                                 HexaBPM__Step__c stp,
                                                 String hostingAccId,
                                                 Boolean isOwned)
    {
        system.debug('$$$$$$ isOwned '+isOwned);        
        //Getting Hosting Acc Details.
        //Account hostingCopany = getAccount(hostingAccId);
        string customerId = stp.HexaBPM__SR__r.HexaBPM__Customer__c;
        List<HexaBPM_Amendment__c> amedLst = getExistingAmendment(stp.HexaBPM__SR__c,'Operating Location');
        List<Operating_Location__c> lstOprLoc = new List<Operating_Location__c>();
        //List<Account> lstAcc = new List<Account>();
        map<Id,Account> mapAccToUpdate = new map<Id,Account>();
        String allUnits = '';
        
        
        // if Customer/Hosting company is having amed record
        if( amedLst != NULL && amedLst.size() > 0)
        {
                for(HexaBPM_Amendment__c objAmd  : amedLst )
                {
                    //if owend, update it with registered address else create new OL.
                    String oprId = (isOwned ? objAmd.Operating_Location__c : NULL);   
                    Operating_Location__c objOL = new Operating_Location__c(id = oprId );
                    if(oprId == NULL)
                        objOL.Account__c = customerId;
                    objOL.Hosting_Company__c = ( isOwned ? NULL : hostingAccId );
                    objOL.IsRegistered__c = objAmd.IsRegistered__c;
                    objOL.Unit__c =  objAmd.Unit__c;
                    //objOL.Type__c = objAmd.Operating_Type__c;....sharing hard code it.
                    objOL.Type__c =  ( isOwned ? objAmd.Operating_Type__c : 'Sharing' );
                    //objOL.Status__c = objAmd.Operating_Location_Status__c;
                    //v1
                    objOL.Status__c = (String.isBlank(objAmd.Operating_Location_Status__c) ? objAmd.Status__c : objAmd.Operating_Location_Status__c);
                    lstOprLoc.add(objOL);
                    
                }
            
                if(lstOprLoc.size() > 0 )
                {
                    upsert lstOprLoc;                    
                }
                system.debug('$$$$$$ getting amed  lstOprLoc '+lstOprLoc); 
        }
        else if(isOwned)
        { 
             
             // get customer owns operating location
             lstOprLoc = getOwnOpertingLoaction(hostingAccId);
            
        }
        system.debug('$$$$$$ lstOprLoc '+lstOprLoc); 
        
        List<Operating_Location__c> reQueryOprLst = [SELECT id, 
                                                            Account__c, 
                                                            Account_Office__c, 
                                                            Account_Unit_Unique__c, 
                                                            Area__c, 
                                                            Area_WF__c, 
                                                            Building_Name__c, 
                                                            Floor__c, 
                                                            Hosting_Company__c, 
                                                            Is_License_to_Occupy__c, 
                                                            Lease__c, 
                                                            Lease_Partner__c, 
                                                            Lease_Types__c, 
                                                            IsRegistered__c, 
                                                            Status__c, 
                                                            Type__c, 
                                                            Unique_Id__c, 
                                                            Unit__c, 
                                                            Unit__r.Name, 
                                                            Unit_Usage_Type__c,
                                                            Unit__r.Building__r.Name,
                                                            Unit__r.Building__r.Arabic_Name_of_Building__c
                                                       FROM Operating_Location__c
                                                      WHERE id IN :lstOprLoc
                                                      ORDER BY createdDate DESC];
        
         
        //dumping on account.
        for(Operating_Location__c objOL : reQueryOprLst )
        {
            system.debug('$$$$$$ objOL '+objOL); 
            system.debug('$$$$$$ objOL.IsRegistered__c '+objOL.IsRegistered__c); 
            system.debug('$$$$$$ gobjOL.Account__c '+objOL.Account__c); 
           
            
            if(objOL.IsRegistered__c 
                        && objOL.Account__c != NULL)
            {
                
                Account acc;
                if( mapAccToUpdate.containsKey(objOL.Account__c) )
                {
                    
                    acc = mapAccToUpdate.get(objOL.Account__c);
                }
                else 
                {
                    acc = new Account(id = objOL.Account__c);
                }
                
                if( objOL.Unit__c != NULL )
                {
                    // ( String.isNotBlank(allUnits) ? ',' : '' ) +
                    allUnits = allUnits + ( String.isNotBlank(allUnits) ? ',' : '' )  + objOL.Unit__r.Name;

                    acc.Sys_Office__c = 'Unit '+allUnits;
                    system.debug('$$$$$$ acc.Sys_Office__c '+acc.Sys_Office__c); 
                    acc.Sys_Office__c = acc.Sys_Office__c +'\n Level '+objOL.Floor__c;
                    system.debug('$$$$$$ acc.Sys_Office__c '+acc.Sys_Office__c); 

                    acc.Office__c = 'Unit '+allUnits;
                    system.debug('$$$$$$ initial acc.Office__c '+acc.Office__c); 
                    acc.Office__c = acc.Office__c +', Level '+objOL.Floor__c;
                    system.debug('$$$$$$ acc.Office__c '+acc.Office__c); 
                }
                
               
                acc.Building_Name__c = objOL.Unit__r.Building__r.Name;
                acc.Street__c = 'Dubai International Financial Centre';
                acc.city__c = 'Dubai';
                acc.country__c = 'United Arab Emirates';
                //acc.Arabic_Office__c = ( String.isNotBlank(acc.Office__c) ? TranslatiorClass.translatorMethod(acc.Office__c) : '');
                //acc.Arabic_Building_Name__c = ( String.isNotBlank(acc.Building_Name__c) ? TranslatiorClass.translatorMethod(acc.Building_Name__c) : '');
                
                //lstAcc.add(acc);
                mapAccToUpdate.put(acc.Id,acc);

            }
        }
        //system.debug('$$$$$$ lstAcc '+lstAcc); 
        system.debug('$$$$$$ reQueryOprLst '+reQueryOprLst); 
        
        
        
        //if(lstAcc.size()>0)
    if( mapAccToUpdate.size() > 0 )
        {
            convertArabicAddress( 
                                    JSON.serialize( mapAccToUpdate.values()),
                                    JSON.serialize(reQueryOprLst)
                                );
            //update lstAcc;
            update mapAccToUpdate.values();

            
        }

        if((stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Commercial_Permission' 
            || stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Commercial_Permission_Renewal'
            ) && stp.HexaBPM__SR__r.HexaBPM__Customer__c != null){
                
                for(Operating_Location__c orlc : [select id,Lease__c from Operating_Location__c WHERE IsRegistered__c = true 
                AND Account__c =:stp.HexaBPM__SR__r.HexaBPM__Customer__c  AND Lease__c != null LIMIT 1]){
                   
                    Account accToUPdate = new account(id = stp.HexaBPM__SR__r.HexaBPM__Customer__c);
                    accToUPdate.Registered_Active_Lease__c = orlc.Lease__c;
                    update accToUPdate;
                    
                }
            }
        
    }
    
   // @future(callout=true)
    public static void convertArabicAddress(String lstAccParam,
                                            String reQueryOprLst)
    {   
        system.debug(' ######## convertArabicAddress ');
        List<Account> lstAccountToUpdate = new List<Account>(); 
        List<Account> accounts = (List<Account>) JSON.deserialize(lstAccParam, List<Account>.class);
        List<Operating_Location__c> lstOprLoc = (List<Operating_Location__c>) JSON.deserialize(reQueryOprLst, List<Operating_Location__c>.class);
        system.debug('######### In Future '+accounts);
        system.debug('$$$$$$ inside accounts  '+accounts); 
        system.debug('$$$$$$ inside futr lstOprLoc '+lstOprLoc); 
        
        String allUnits = ''; 
        for(Account acc : accounts)
        {
                  for(Operating_Location__c oprLoc : lstOprLoc)
                  {
                          allUnits = allUnits + ( String.isNotBlank(allUnits) ? ',' : '' )  + oprLoc.Unit__r.Name;
                          system.debug('@@@@@@@@@@@ outside allUnits '+allUnits);
                          if( oprLoc.Account__c != NULL 
                                    && acc.Id != NULL
                                        && oprLoc.Account__c == acc.Id )
                          {
                             system.debug('@@@@@@@@@@@ allUnits '+allUnits);
                             acc.Arabic_Building_Name__c = oprLoc.Unit__r.Building__r.Arabic_Name_of_Building__c;
                             acc.Arabic_Office__c = 'وحدة '+TranslatiorClass.translatorMethod(allUnits)+'\n';
                             acc.Arabic_Office__c = acc.Arabic_Office__c +'مستوى '+oprLoc.Floor__c;   
                           }
                      
                  }
                  
                  system.debug('@@@@@@@@@@@ acc '+acc);
                  lstAccountToUpdate.add(acc);
            
              
        }
        
        try
        {
            if(lstAccountToUpdate.size()>0)
            {
                update lstAccountToUpdate;
            }  
        }
        catch(Exception e)
        {
                Log__c objLog = new Log__c();
                objLog.Description__c = 'Exception on CC_CreateOperatingLocations.convertArabicAddress() - Line:171 :'+e.getMessage();
                objLog.Type__c = 'Custom Code to convert the arabic address.';
                insert objLog;
        }
        
        
    }

    
    
    public static List<HexaBPM_Amendment__c> getExistingAmendment(String srId,String amedType)
    {
        return [SELECT id, 
                       IsRegistered__c , 
                       Unit__c , 
                       Status__c , //v1
                       Hosting_Company__c , 
                       Operating_Type__c , 
                       Operating_Location__c , 
                       Operating_Location_Status__c, 
                       Unit__r.Name, 
                       Unit__r.Floor__c, 
                       Unit__r.Building__r.Name,
                       Unit__r.Building__c
                 FROM HexaBPM_Amendment__c
                WHERE ServiceRequest__c = :srId
                  AND Amendment_Type__c = :amedType 
                  AND Status__c != 'Inactive'];
                  
                  // 'Operating Location'
        
    }
    public static List<Operating_Location__c> getOwnOpertingLoaction(String accId) {
        List<Operating_Location__c> ownOprLoactionLst = new List<Operating_Location__c>();
        system.debug('######## getOpertingLoactionBasedOnAccId(accId) ' + getOpertingLoactionBasedOnAccId(accId));
        for(Operating_Location__c oprLoc :getOpertingLoactionBasedOnAccId(accId)) {
            system.debug('######## oprLoc ' + oprLoc);
            if(oprLoc.Status__c == 'Active' && (oprLoc.Type__c == 'Purchased' || oprLoc.Type__c == 'Leased')) {
                ownOprLoactionLst.add(oprLoc);
            }

        }
        return ownOprLoactionLst;

    }
    public static List<Operating_Location__c> getOpertingLoactionBasedOnAccId(String accId){
        return [SELECT id, 
                        Account__c, 
                        Account_Office__c, 
                        Account_Unit_Unique__c, 
                        Area__c, 
                        Area_WF__c, 
                        Building_Name__c, 
                        Floor__c, 
                        Hosting_Company__c, 
                        Is_License_to_Occupy__c, 
                        Lease__c, 
                        Lease_Partner__c, 
                        Lease_Types__c, 
                        IsRegistered__c, 
                        Status__c, 
                        Type__c, 
                        Unique_Id__c, 
                        Unit__c, 
                        Unit__r.Name, 
                        Unit_Usage_Type__c,
                        Unit__r.Building__r.Name
                FROM Operating_Location__c
                WHERE Account__c = :accId];
    }
}