/**********************************************************************************************************************
* Name               : OB_DeactivateCPSRBatch
* Description        : This batch class is used to deactivate the Commercial Permisssion SRs for Pocket Shops if the SR status is Draft for more than 6 months
* Created Date       : 30-11-2020
* Created By         : PWC Digital Middle East
* --------------------------------------------------------------------------------------------------------------------*
* Version       Author                  Date             Comment
* 1.0           salma.rasheed@pwc.com   10-12-2020       Initial Draft
**********************************************************************************************************************/
global class OB_DeactivateCPSRBatch implements Database.Batchable<sObject>,Schedulable  {

    /**********************************************************************************************
    * @Description  : Scheduler execute method to schedule code execution
    * @Params       : SchedulableContext        scheduling details
    * @Return       : none
    **********************************************************************************************/ 
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this,10);
    }

    /**********************************************************************************************
    * @Description  : Batch start method to define the context
    * @Params       : BatchableContext        batch context
    * @Return       : Query locator
    **********************************************************************************************/ 
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        // Get all the Draft CP SR's
        String queryString = 'Select id,CreatedDate from HexaBPM__Service_Request__c where HexaBPM__Internal_SR_Status__r.Name=\'Draft\' AND HexaBPM__Record_Type_Name__c=\'Commercial_Permission\' AND Type_of_commercial_permission__c=\'Gate Avenue Pocket Shops\'';
        system.debug(queryString);
        return Database.getQueryLocator(queryString);
    }

    /**********************************************************************************************
    * @Description  : Batch execute method to process the records in current scope
    * @Params       : scope     records in current scope
    * @Return       : none
    **********************************************************************************************/ 
    global void execute(Database.BatchableContext bc, List<HexaBPM__Service_Request__c> scope){
        try{
            List<HexaBPM__Service_Request__c> SRList = new List<HexaBPM__Service_Request__c>();
           // List<HexaBPM__SR_Status__c> srStatus = [Select id,HexaBPM__Code__c,CreatedDate from HexaBPM__SR_Status__c where HexaBPM__Code__c='EXPIRED' limit 1];
            List<HexaBPM__SR_Status__c> srStatus = [Select id,HexaBPM__Code__c,CreatedDate from HexaBPM__SR_Status__c where HexaBPM__Code__c='EXPIRED' limit 1];

            for(HexaBPM__Service_Request__c objSR : scope){
                Date createdDate = date.newinstance(objSR.CreatedDate.year(), objSR.CreatedDate.month(), objSR.CreatedDate.day());
                //Integer validityInMonths = Integer.valueOf(Label.OB_SR_Validity_In_Months);
                Integer validityInMonths = Integer.valueOf(Label.OB_Months_12);
                if(createdDate<=system.today().addMonths(-validityInMonths)){  
                    SRList.add(new HexaBPM__Service_Request__c(
                        Id=objSR.Id,
                        HexaBPM__External_SR_Status__c = srStatus[0].Id,
                        HexaBPM__Internal_SR_Status__c = srStatus[0].Id,
                        Cancellation_Reason__c = 'Expired for more than 12 months'));
                }
            }
            system.debug('SRList size===>'+SRList.size());
            system.debug('SRList ===>'+SRList);
            if(!SRList.isEmpty()) update SRList;   
        }catch (Exception e){  Log__c objLog = new Log__c(Description__c = 'Exception on OB_DeactivateCPSRBatch.execute() :'+e.getStackTraceString(),Type__c = 'Deactivate SR');    insert objLog;}
    }

    /**********************************************************************************************
    * @Description  : Batch finish method
    * @Params       : BatchableContext  batch contect details
    * @Return       : none
    **********************************************************************************************/ 
    global void finish(Database.BatchableContext bc){}    
}