@isTest
global class OB_IsNameAllowedMock implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        OB_INF_IsTradeNameAllowed_WS.ISTradeNameAllowedResponse_element res = new OB_INF_IsTradeNameAllowed_WS.ISTradeNameAllowedResponse_element();
        
        res.P_OP_Message = 'success';
        res.P_OP_ArabicMessage= 'test';
        res.P_OP_EnglishMessage= 'abc';
        res.P_OP_LogID = '123';
        res.P_OP_Code = 'ert';
        
        response.put('response_x', res); 
   }
}