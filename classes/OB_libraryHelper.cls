public without sharing class OB_libraryHelper {


    public static string isLibActive = System.Label.OB_Is_Libraray_Active;

    //* v1.0 Merul  26 march 2020  Libary implementation
   public static List<HexaBPM__Service_Request__c> getSR( String srId,Map<Id,AccountContactRelation> mapRelatedAccForAmed)
   {

    List<HexaBPM__Service_Request__c> lstSerReq = new List<HexaBPM__Service_Request__c>();

    if( mapRelatedAccForAmed != NULL && mapRelatedAccForAmed.size() > 0 && OB_libraryHelper.isLibActive == 'TRUE'){
       
        //Ticket 59 added OB_Is_Multi_Structure_Application__c,HexaBPM__Record_Type_Name__c field to Query
         lstSerReq = [SELECT id, 
                            Entity_Type__c, 
                            Type_of_Entity__c, 
                            legal_structures__c ,
                            HexaBPM__External_SR_Status__c, 
                            HexaBPM__External_SR_Status__r.Name, 
                            HexaBPM__Internal_SR_Status__c, 
                            HexaBPM__Internal_Status_Name__c, 
                            Business_Sector__c,
                            Setting_Up__c,HexaBPM__Record_Type_Name__c,
                            HexaBPM__Customer__c,OB_Is_Multi_Structure_Application__c,
                            License_Application__c,
                            (
                                SELECT id, 
                                        Role__c, 
                                        Director_Employee_of_the_CSP__c, 
                                        Certified_passport_copy__c, 
                                        ServiceRequest__c, 
                                        Title__c, 
                                        Name__c,
                                        First_Name__c, 
                                        Middle_Name__c, 
                                        Last_Name__c, 
                                        Former_Name__c, 
                                        Passport_No__c, 
                                        Nationality_list__c, 
                                        Date_of_Birth__c, 
                                        Gender__c, 
                                        Place_of_Birth__c, 
                                        Passport_Issue_Date__c, 
                                        Passport_Expiry_Date__c, 
                                        Place_of_Issue__c, 
                                        Are_you_a_resident_in_the_U_A_E__c, 
                                        U_I_D_No__c, 
                                        Mobile__c, 
                                        Email__c, 
                                        Is_this_individual_a_PEP__c, 
                                        Address__c, 
                                        Apartment_or_Villa_Number_c__c, 
                                        Permanent_Native_Country__c, 
                                        Permanent_Native_City__c, 
                                        Emirate_State_Province__c, 
                                        PO_Box__c, 
                                        RecordType.DeveloperName, 
                                        ServiceRequest__r.Id, 
                                        Is_this_Entity_registered_with_DIFC__c, 
                                        Company_Name__c, 
                                        Registered_with_DIFC__c,
                                        Entity_Name__c, 
                                        Registration_No__c, 
                                        Date_of_Registration__c, 
                                        Registration_Date__c,
                                        Place_of_Registration__c, 
                                        Country_of_Registration__c, 
                                        Telephone_No__c, 
                                        Entity_Name__r.Name, 
                                        Individual_Corporate_Name__c,
                                        Registered_No__c,
                                        Relationship_ID__c,
                                        recordTypeId,

                                        Is_this_member_a_designated_Member__c,
                                        DI_Permanent_Native_Country__c,
                                        DI_Email__c,
                                        DI_Emirate_State_Province__c,
                                        DI_Address__c,
                                        DI_Apartment_or_Villa_Number_c__c,
                                        DI_Building_Name__c,
                                        DI_Permanent_Native_City__c,
                                        DI_First_Name__c,
                                        DI_Last_Name__c,
                                        DI_PO_Box__c,
                                        DI_Post_Code__c,
                                        DI_Mobile__c,
                                        Type_of_Contribution__c,
                                        E_I_D_no__c,
                                        Relationship_Type__c,
                                        Date_Of_Becoming_a_UBO__c,
                                        Please_select_if_applicable__c,
                                        Type_of_Ownership_or_Control__c,
                                        Country_of_source_of_income__c,
                                        Will_Individual_Sign_AoA__c,
                                        Source_and_origin_of_funds_for_entity__c,
                                        Amount_of_Contribution__c,
                                        Source_of_income_wealth__c,
                                        Partner_Type__c,
                                        Select_the_Qualified_Applicant_Type__c,
                                        Job_Title_Capacity__c,
                                        cessation_date__c,
                                        I_agree_Authorized_Represntative__c,
                                        Type_of_Authority__c,Percentage_of_shares_owned__c,
                                        Govt_Entity_Name__c,
                                        
                                        Copied_from_Amendment__c




                                    FROM Amendments__r
                                    WHERE  (RecordType.DeveloperName = 'Body_Corporate' OR RecordType.DeveloperName = 'Individual')
                                     AND Role__c != null
                                ORDER BY CreatedDate DESC
                            )
                    FROM HexaBPM__Service_Request__c
                    WHERE HexaBPM__Customer__c IN: mapRelatedAccForAmed.keySet()
                    OR id =: srId  
                    ];

    }else{

        lstSerReq = [SELECT id, 
        Entity_Type__c, 
        Type_of_Entity__c, 
        legal_structures__c ,
        HexaBPM__External_SR_Status__c, 
        HexaBPM__External_SR_Status__r.Name, 
        HexaBPM__Internal_SR_Status__c, 
        HexaBPM__Internal_Status_Name__c, 
        Business_Sector__c,
        Setting_Up__c,
        HexaBPM__Customer__c,
        License_Application__c,
        (
            SELECT id, 
                    Role__c, 
                    Director_Employee_of_the_CSP__c, 
                    Certified_passport_copy__c, 
                    ServiceRequest__c, 
                    Title__c, 
                    Name__c,
                    First_Name__c, 
                    Middle_Name__c, 
                    Last_Name__c, 
                    Former_Name__c, 
                    Passport_No__c, 
                    Nationality_list__c, 
                    Date_of_Birth__c, 
                    Gender__c, 
                    Place_of_Birth__c, 
                    Passport_Issue_Date__c, 
                    Passport_Expiry_Date__c, 
                    Place_of_Issue__c, 
                    Are_you_a_resident_in_the_U_A_E__c, 
                    U_I_D_No__c, 
                    Mobile__c, 
                    Email__c, 
                    Is_this_individual_a_PEP__c, 
                    Address__c, 
                    Apartment_or_Villa_Number_c__c, 
                    Permanent_Native_Country__c, 
                    Permanent_Native_City__c, 
                    Emirate_State_Province__c, 
                    PO_Box__c, 
                    RecordType.DeveloperName, 
                    ServiceRequest__r.Id, 
                    Is_this_Entity_registered_with_DIFC__c, 
                    Registered_with_DIFC__c,
                    Company_Name__c, 
                    Entity_Name__c, 
                    Registration_No__c, 
                    Date_of_Registration__c, 
                    Registration_Date__c,
                    Place_of_Registration__c, 
                    Country_of_Registration__c, 
                    Telephone_No__c, 
                    Entity_Name__r.Name, 
                    Individual_Corporate_Name__c,
                    Registered_No__c,
                    Relationship_ID__c,
                    recordTypeId,

                    Is_this_member_a_designated_Member__c,
                    DI_Permanent_Native_Country__c,
                    DI_Email__c,
                    DI_Emirate_State_Province__c,
                    DI_Address__c,
                    DI_Apartment_or_Villa_Number_c__c,
                    DI_Building_Name__c,
                    DI_Permanent_Native_City__c,
                    DI_First_Name__c,
                    DI_Last_Name__c,
                    DI_PO_Box__c,
                    DI_Post_Code__c,
                    DI_Mobile__c,
                    Type_of_Contribution__c,
                    E_I_D_no__c,
                    Relationship_Type__c,
                    Date_Of_Becoming_a_UBO__c,
                    Please_select_if_applicable__c,
                    Type_of_Ownership_or_Control__c,
                    Country_of_source_of_income__c,
                    Will_Individual_Sign_AoA__c,
                    Source_and_origin_of_funds_for_entity__c,
                    Amount_of_Contribution__c,
                    Source_of_income_wealth__c,
                    Partner_Type__c,Percentage_of_shares_owned__c,
                    Select_the_Qualified_Applicant_Type__c,
                    Job_Title_Capacity__c,
                    cessation_date__c,
                    I_agree_Authorized_Represntative__c,
                    Type_of_Authority__c,
                    Govt_Entity_Name__c,
                    Copied_from_Amendment__c

                FROM Amendments__r
                WHERE  (RecordType.DeveloperName = 'Body_Corporate' OR RecordType.DeveloperName = 'Individual')
                 AND Role__c != null
            ORDER BY CreatedDate DESC
        )
        FROM HexaBPM__Service_Request__c
        WHERE 
        id =: srId  
        ];
       }
       
       return lstSerReq;


   }
   
   
   
   
    //* v1.0 Merul  26 march 2020  Libary implementation
   public static List<Relationship__c> getRelationShip( Map<Id,AccountContactRelation> mapRelatedAccForRelshp )
   {
       
       List<Relationship__c> relLst = new  List<Relationship__c>();
       set<string> allowedType = new set<string>{'Approved Person','Company Secretary','Guardian','Authorized Signatory','Senior Management','General Communication','Shareholder','Director',
       'General Partner','Limited Partner','founding member','founder','Council Member'};

       if( mapRelatedAccForRelshp != NULL && mapRelatedAccForRelshp.size() > 0 && OB_libraryHelper.isLibActive == 'TRUE'){
           relLst = [SELECT id,
                               Object_Contact__c,
                               Object_Account__c,
                               Subject_Account__c,
                               Relationship_Type__c,
                               Sys_Amendment_Id__c,
                               Amendment_ID__c,
                               
                               // Contact
                               Object_Contact__r.Salutation,
                               Object_Contact__r.LastName,
                               Object_Contact__r.FirstName,
                               Object_Contact__r.Birthdate,
                               Object_Contact__r.Place_of_Birth__c,
                               Object_Contact__r.Passport_No__c,
                               Object_Contact__r.Nationality__c,
                               Object_Contact__r.Phone,
                               Object_Contact__r.Email,
                               Object_Contact__r.Office_Unit_Number__c,
                               Object_Contact__r.ADDRESS_LINE1__c,
                               Object_Contact__r.PO_Box_HC__c,
                               Object_Contact__r.CITY1__c,
                               Object_Contact__r.Emirate_State_Province__c,
                               Object_Contact__r.Country__c,
                               Object_Contact__r.Passport_Expiry_Date__c,
                               Object_Contact__r.Gender__c,
                               Object_Contact__r.Issued_Country__c,
                               Object_Contact__r.Place_of_Issue__c,
                               Object_Contact__r.MobilePhone,
                               Object_Contact__r.Are_you_a_resident_in_the_U_A_E__c,
                               Object_Contact__r.Is_this_individual_a_PEP__c,
                               Object_Contact__r.Country_Income_Source__c,
                               Object_Contact__r.UID_Number__c,
                               Object_Contact__r.Emirate__c,
                               Object_Contact__r.Details_on_source_of_income__c,
                               Object_Contact__r.source_of_income_wealth__c,
                               Object_Contact__r.Type_of_Authority__c,
                               Object_Contact__r.Type_of_Authority_Other__c,
                               Object_Contact__r.Date_of_Issue__c,

                               
                               //Account
                               Object_Account__r.Is_Designated_Member__c,
                               Object_Account__r.Name,
                               Object_Account__r.Former_Name__c,
                               Object_Account__r.Phone,
                               Object_Account__r.Place_of_Registration_Text__c,
                               Object_Account__r.Place_of_Registration__c,
                               Object_Account__r.Office__c,
                               Object_Account__r.Building_Name__c,
                               Object_Account__r.PO_Box__c,
                               Object_Account__r.City__c,
                               Object_Account__r.Emirate_State__c,
                               Object_Account__r.Country__c,
                               Object_Account__r.Source_of_Income_and_Origin_of_Wealth__c,
                               Object_Account__r.Email_Address__c,
                               Object_Account__r.Emirate__c,
                               Object_Account__r.Jurisdiction__c,
                               Object_Account__r.Mobile_Number__c,
                               Object_Account__r.Nationality__c,
                               Object_Account__r.Source_of_income_wealth__c,
                               Object_Account__r.Please_state_the_source_of_income_weal__c,
                               Object_Account__r.Trade_Name__c,
                               Object_Account__r.Qualifying_Type__c,
                               Object_Account__r.ROC_reg_incorp_Date__c,
                               Object_Account__r.Name_of_Regulator_Exchange_Govt__c,
                               Object_Account__r.Registration_No__c,
                               Object_Account__r.Registration_License_No__c,
                               Object_Account__r.ROC_Status__c
                               


                        FROM  Relationship__c
                       WHERE Subject_Account__c IN: mapRelatedAccForRelshp.keySet() 
                         AND  Status__c= 'Active'  
                         AND Relationship_Type_formula__c IN : allowedType
                        
                   ];
       }
       
       return relLst;
   } 


    //* v1.0 Merul  26 march 2020  Libary implementation
    public static HexaBPM_Amendment__c  setAmedFromObjAccount(  
        HexaBPM_Amendment__c amedObj, 
        Relationship__c relShip
    )
    {
            
            amedObj.Company_Name__c = relShip.Object_Account__r.Name;
            amedObj.Is_this_member_a_designated_Member__c =( relShip.Object_Account__r.Is_Designated_Member__c ? 'Yes' : 'No');
            amedObj.Former_Name__c = relShip.Object_Account__r.Former_Name__c;
            amedObj.Telephone_No__c = relShip.Object_Account__r.Phone;
            amedObj.Place_of_Registration__c = relShip.Object_Account__r.Place_of_Registration_Text__c; 
            amedObj.Country_of_Registration__c = relShip.Object_Account__r.Place_of_Registration__c; 
            amedObj.Apartment_or_Villa_Number_c__c = relShip.Object_Account__r.Office__c; 
            amedObj.Address__c = relShip.Object_Account__r.Building_Name__c; 
            amedObj.PO_Box__c = relShip.Object_Account__r.PO_Box__c; 
            amedObj.Permanent_Native_City__c = relShip.Object_Account__r.City__c; 
            amedObj.Emirate_State_Province__c = relShip.Object_Account__r.Emirate_State__c; 
            amedObj.Permanent_Native_Country__c = relShip.Object_Account__r.Country__c;  
            amedObj.Country_of_source_of_income__c = relShip.Object_Account__r.Source_of_Income_and_Origin_of_Wealth__c; 
            amedObj.Email__c = relShip.Object_Account__r.Email_Address__c;    
            amedObj.Emirate__c = relShip.Object_Account__r.Emirate__c;   
            amedObj.Jurisdiction__c = relShip.Object_Account__r.Jurisdiction__c;  
            amedObj.Mobile__c = relShip.Object_Account__r.Mobile_Number__c;  
            amedObj.Nationality_list__c = relShip.Object_Account__r.Nationality__c;  
            amedObj.Source_and_origin_of_funds_for_entity__c = relShip.Object_Account__r.Source_of_income_wealth__c;  
            amedObj.Source_of_income_wealth__c = relShip.Object_Account__r.Please_state_the_source_of_income_weal__c;  
            amedObj.Trading_Name__c = relShip.Object_Account__r.Trade_Name__c;  
            amedObj.Select_the_Qualified_Applicant_Type__c = relShip.Object_Account__r.Qualifying_Type__c;  
            amedObj.Financial_Service_Regulator__c = relShip.Object_Account__r.Name_of_Regulator_Exchange_Govt__c;  
            amedObj.Registration_Date__c = relShip.Object_Account__r.ROC_reg_incorp_Date__c; 

            if(relShip.Object_Account__r.ROC_Status__c == null){
                amedObj.Is_this_Entity_registered_with_DIFC__c = 'No';
            }
            if(relShip.Object_Account__r.Registration_No__c != null){
                amedObj.Registration_No__c = relShip.Object_Account__r.Registration_No__c;
            }else if(relShip.Object_Account__r.Registration_License_No__c != null){
                amedObj.Registration_No__c = relShip.Object_Account__r.Registration_License_No__c;
            }
            
            
            return amedObj;
    }


    //* v1.0 Merul  26 march 2020  Libary implementation
    public static HexaBPM_Amendment__c setAmedFromObjContact( 
        HexaBPM_Amendment__c amedObj, Relationship__c relShip )
    {
        
            amedObj.Title__c = relShip.Object_Contact__r.Salutation;
            amedObj.Last_Name__c = relShip.Object_Contact__r.LastName;
            amedObj.First_Name__c = relShip.Object_Contact__r.FirstName;
            amedObj.Date_of_Birth__c = relShip.Object_Contact__r.Birthdate;
            amedObj.Place_of_Birth__c = relShip.Object_Contact__r.Place_of_Birth__c ;
            amedObj.Passport_No__c = relShip.Object_Contact__r.Passport_No__c ;
            amedObj.Nationality_list__c = relShip.Object_Contact__r.Nationality__c ;
            amedObj.Mobile__c = relShip.Object_Contact__r.Phone ;
            amedObj.Email__c = relShip.Object_Contact__r.Email ;
            amedObj.Apartment_or_Villa_Number_c__c = relShip.Object_Contact__r.Office_Unit_Number__c;
            amedObj.Address__c = relShip.Object_Contact__r.ADDRESS_LINE1__c;
            amedObj.PO_Box__c = relShip.Object_Contact__r.PO_Box_HC__c;
            amedObj.Permanent_Native_City__c = relShip.Object_Contact__r.CITY1__c;
            amedObj.Emirate_State_Province__c = relShip.Object_Contact__r.Emirate_State_Province__c;
            amedObj.Permanent_Native_Country__c = relShip.Object_Contact__r.Country__c;
            amedObj.Passport_Expiry_Date__c = relShip.Object_Contact__r.Passport_Expiry_Date__c;
            amedObj.Gender__c = relShip.Object_Contact__r.Gender__c;
            amedObj.Country__c = relShip.Object_Contact__r.Issued_Country__c;
            amedObj.Place_of_Issue__c = relShip.Object_Contact__r.Place_of_Issue__c;
            amedObj.Mobile__c = relShip.Object_Contact__r.MobilePhone;
            amedObj.Passport_Issue_Date__c = relShip.Object_Contact__r.Date_of_Issue__c;
            amedObj.Are_you_a_resident_in_the_U_A_E__c = relShip.Object_Contact__r.Are_you_a_resident_in_the_U_A_E__c;
            amedObj.Is_this_individual_a_PEP__c = relShip.Object_Contact__r.Is_this_individual_a_PEP__c;
            amedObj.Country_of_source_of_income__c = relShip.Object_Contact__r.Country_Income_Source__c;
            amedObj.U_I_D_No__c = relShip.Object_Contact__r.UID_Number__c;
            amedObj.Emirate__c = relShip.Object_Contact__r.Emirate__c;
            amedObj.Source_and_origin_of_funds_for_entity__c = relShip.Object_Contact__r.Details_on_source_of_income__c;
            amedObj.Source_of_income_wealth__c = relShip.Object_Contact__r.source_of_income_wealth__c;
            amedObj.Type_of_Authority__c = relShip.Object_Contact__r.Type_of_Authority__c;
            amedObj.Please_provide_the_type_of_authority__c = relShip.Object_Contact__r.Type_of_Authority_Other__c;
       
            return amedObj;
    }
}