/******************************************************************************************************
*  Author   : Kumar Utkarsh
*  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
V1.1    22-Apr-2021  Utkarsh         Modified
v1.3    25-May-2021  Veera           Modified
* This is a Queueable interface class which contains logic to do Application Status Check in GDRFA.
*******************************************************************************************************/
public class webServiceGdrfaAppStatusQueueable implements Queueable,  Database.AllowsCallouts {
    List<Step__c> stepLists;
    public webServiceGdrfaAppStatusQueueable(List<Step__c> stepLists) {
        this.stepLists = stepLists;
    }
    public void execute(QueueableContext context){
        
        List<Step__c> rejectedStepList = new List<Step__c>();
        List<Log__c> errorObjLogList = new List<Log__c>();
        List<Step__c> dnrdStepList = new List<Step__c>();
        map<id, Step__c> dnrdStepMap = new map<id, Step__c>();
        map<id, Step__c> approvedStepMap = new map<id, Step__c>();
        //List<Service_Request__c> approvedServiceRequests = new List<Service_Request__c>();
        List<Step__c> printVisaDnrdStepList = new List<Step__c>();
        List<Attachment> listAttchmentPdf = new List<Attachment>();
        List<SR_Doc__c> srDocList = new List<SR_Doc__c>();
        map<id,SR_Doc__c> srDocUploadedMap = new map<id,SR_Doc__c>();
        map<String, String> gdrfaMdData = new map<String, String>();
        map<String, String> gdrfaIdSatusName = new map<String, String>();
        Integer currentYear = System.Today().year();
        for(SR_Status__c SrStatus : [SELECT Id, Name FROM SR_Status__c WHERE Name = 'Entry Permit is issued']){
            gdrfaIdSatusName.put(SrStatus.Name, SrStatus.Id);
        }
        Set<String> statusValues = new Set<String>{'APPROVED','PICKUPCANCELLED','PICKUPUNASSIGED','PICKUPENROUTE','PICKUPDISPATCHED','DROPOFFUNASSIGNED','DROPOFFEXCEPTION','COUNTERPRINTINGBYCUSTOMER','DROPOFFENROUTE','PICKUPCOMPLETED','DROPOFFCANCELLED','DELIVEREDTOCUSTOMER','DROPOFFREADY','PICKUPFAILED','DROPOFFDISPATCHED','PICKUPREADY','WARRANTYREFUNDED','WARRANTYREFUNDFAILED','PICKEDBYCUSTOMER','TOBEDELIVEREDPRINTED','CUSTOMERENGAGED','REFUNDED','REFUNDINITIATED','DELIVEREDTOCOURIER','FAILEDTODELIVER','FAILEDTOCOLLECT','WARRANTYREFUNDINITIATED','REFUNDFAILED'}; 
            for(GDRFA_Master_Data__c gdrfaMasterData : [SELECT id, Look_Up_Id__c, Name_English__c, GDRFA_Name__c FROM GDRFA_Master_Data__c WHERE Lookup_Type__c = 'Country']){
                gdrfaMdData.put((gdrfaMasterData.GDRFA_Name__c).toUpperCase(), gdrfaMasterData.Look_Up_Id__c);
            }
        String fileTypeId;
        System.debug(stepLists.size()+'stepLists:'+stepLists);
        map<String, Status__c> mapStatusCode = new map<String, Status__c>();
        Group GSBackOffice = [select Id from Group where Type = 'Queue' AND Name = 'GS Back Office'];
        for(Status__c stts :[SELECT Id, Code__c FROM Status__c WHERE Code__c IN('DNRD_GS_Review','GDRFA_Returned', 'PENDING', 'CLOSED')]){
            mapStatusCode.put(stts.Code__c, stts); 
        }
        
        map<Id, SR_Doc__c> mapIdSrDoc = new map<Id, SR_Doc__c>();
        map<Id, map<String, Step__c>> mapSrStep =  new map<Id, map<String, Step__c>>();
        map<String, SR_Doc__c>  mapsrDocIdDocId =  new map<String, SR_Doc__c>();
        map<Id,Id> mapStepStepTemplate = new map<Id,Id>();
        //mapping between Status check step vs Data push step
        map<String, String> mapissueTypedStep = new map<String, String>();
        map<String, String> mapIssueStepIdSRId = new map<String, String>();
        map<String, Service_Request__c> mapStepIdSRObj = new map<String, Service_Request__c>();
        mapissueTypedStep.put('Entry Permit Form is Typed', 'Entry Permit is Issued');
        mapissueTypedStep.put('Online Entry Permit is Typed', 'Entry Permit is Issued');
        mapissueTypedStep.put('Visa Stamping Form is Typed', 'Completed');
        mapissueTypedStep.put('Cancellation Form Typed', 'Completed');
        mapissueTypedStep.put('Renewal Form is Typed', 'Passport is Ready for Collection');
        for(SR_Steps__c obj: [select id, Step_Template__c from SR_Steps__c where Step_Template_Code__c IN('Online entry permit is typed', 'Entry permit form is typed', 'Renewal form is typed', 'Cancellation Form Typed', 'Entry Permit is Issued', 'Completed', 'Passport is Ready for Collection', 'Visa Stamping Form is Typed') AND SR_Template__r.SR_Template_Code__c IN ('DIFC_Sponsorship_Visa_New','DIFC_Sponsorship_Visa_Renewal', 'DIFC_Sponsorship_Visa_Cancellation') ]){
            mapStepStepTemplate.put(obj.Step_Template__c,obj.Id);                                                                        
        }
        
        /*for(Step__c stpObj: [SELECT id, SR__c FROM Step__c WHERE Id IN:stepLists AND Step_Name__c = 'Entry Permit Is Issued']){
mapIssueStepIdSRId.put(stpObj.Id, stpObj.SR__c);
}*/
        
        /*for(Service_Request__c SR : [SELECT Id, Internal_Status_Name__c, External_Status_Name__c FROM Service_Request__c WHERE ID IN :mapIssueStepIdSRId.Values()]){
mapStepIdSRObj.put(SR.Id, SR);
}*/
        //Fetching the Status Check steps
        WebServGDRFAAuthFeePaymentClass.ResponseBasicAuth ObjResponseAuth = WebServGDRFAAuthFeePaymentClass.GDRFAAuth();
        //Get authrization code 
        string access_token = ObjResponseAuth.access_token;
        string token_type = ObjResponseAuth.token_type;
        // process each batch of records
        // Fetching the Status Check steps
        for (Step__c SrStep : stepLists) {
            map<String, Step__c> mapNameStep = new map<String, Step__c>();
            mapNameStep.put(SrStep.Step_Name__c, SrStep);
            mapSrStep.put(SrStep.SR__c, mapNameStep);
        }
        
        for (SR_Doc__c srDoc : [SELECT Id, Service_Request__c, GDRFA_Parent_Document_Id__c, Name FROM SR_Doc__c WHERE Service_Request__c IN:mapSrStep.keySet() AND GDRFA_Parent_Document_Id__c != null]){
            mapsrDocIdDocId.put(srDoc.GDRFA_Parent_Document_Id__c+''+srDoc.Service_Request__c, srDoc);
        }
        for (SR_Doc__c srDoc : [SELECT Id, Service_Request__c FROM SR_Doc__c WHERE Document_Master__r.code__c IN('Entry Permit', 'Cancellation Confirmation', 'Approved Residence Visa') AND Service_Request__c IN:mapSrStep.keySet()]){
            mapIdSrDoc.put(srDoc.Service_Request__c, srDoc);
        }
        
        for (Step__c step : [SELECT Id, Name, Step_Name__c, DNRD_Receipt_No__c, Transaction_Number__c, Payment_Date__c, SR__c, SR__r.First_Name__c, SR__r.Date_of_Birth__c, SR__r.Nationality_list__c, Step_Template__c
                             FROM Step__c WHERE Step_Name__c IN :mapissueTypedStep.keySet()
                             AND Status_Code__c = 'CLOSED' AND SR__c IN:mapSrStep.keySet() AND Transaction_Number__c != null AND DNRD_Receipt_No__c != null AND Payment_Date__c != null])
        {
            if(mapSrStep.get(step.SR__c).containsKey(mapissueTypedStep.get(step.Step_Name__c)))
            {
                
                Step__c epIssuedStep = mapSrStep.get(step.SR__c).get(mapissueTypedStep.get(step.Step_Name__c));
                if(mapStepStepTemplate.containsKey(epIssuedStep.Step_Template__c) && epIssuedStep.SR_Step__c == null){
                    epIssuedStep.SR_Step__c = mapStepStepTemplate.get(epIssuedStep.Step_Template__c);
                }
                system.debug('step:'+step);
                JSONGenerator jsonAppStatusObj = JSON.createGenerator(true);
                jsonAppStatusObj.writeStartObject();
                
                if(step.DNRD_Receipt_No__c != null){
                    jsonAppStatusObj.writeStringField('applicationNo', step.DNRD_Receipt_No__c);
                }
                
                if(step.Transaction_Number__c != null){
                    jsonAppStatusObj.writeStringField('transactionNo', step.Transaction_Number__c);
                }
                
                if(step.Payment_Date__c != null){
                    jsonAppStatusObj.writeDateField('paymentDate', step.Payment_Date__c);  
                }
                
                jsonAppStatusObj.writeEndObject();
                //WebService to Push the Submit Residence Form
                HttpRequest request = new HttpRequest();
                request.setEndPoint(System.label.GDRFA_Application_Status_Check_Endpoint);
                request.setBody(jsonAppStatusObj.getAsString());
                request.setTimeout(120000);
                request.setHeader('apikey', System.label.GDRFA_API_Key);
                request.setHeader('Content-Type', 'application/json');
                request.setHeader('AcceptLanguage', 'en-US');
                request.setMethod('POST');
                request.setHeader('Authorization',token_type+' '+access_token);
                HttpResponse response = new HTTP().send(request);
                System.debug('STATUS_CODE:'+response.getStatusCode());
                System.debug('strJSONresponseAppStatus:'+response.getBody());
                request.setTimeout(101000);
                if (response.getStatusCode() == 200)
                {
                    WebServGDRFAAuthFeePaymentClass.AppStatusCheck appStatusObj = (WebServGDRFAAuthFeePaymentClass.AppStatusCheck) System.JSON.deserialize(response.getBody(), WebServGDRFAAuthFeePaymentClass.AppStatusCheck.class);
                    //if(appStatusObj.StatesCode == 'APPROVED' || appStatusObj.StatesCode == 'CUSTOMERENGAGED')
                    if(statusValues.contains(appStatusObj.StatesCode)){  
                        JSONGenerator jsonPrintVisaObj = JSON.createGenerator(true);
                        jsonPrintVisaObj.writeStartObject();
                        if(step.DNRD_Receipt_No__c != null){
                            jsonPrintVisaObj.writeStringField('applicationNumber', step.DNRD_Receipt_No__c);
                        }
                        if(appStatusObj.FileType != null){
                            if(appStatusObj.FileType == 1){
                                fileTypeId = 'ZA901';
                            }
                            if(appStatusObj.FileType == 2){
                                fileTypeId = 'ZA903';
                            }
                        }
                        jsonPrintVisaObj.writeEndObject();
                        HttpRequest requestPrintVisa = new HttpRequest();
                        requestPrintVisa.setEndPoint(System.label.GDRFA_Print_VISA);
                        requestPrintVisa.setBody(jsonPrintVisaObj.getAsString());
                        requestPrintVisa.setTimeout(120000);
                        requestPrintVisa.setHeader('apikey', System.label.GDRFA_API_Key);
                        requestPrintVisa.setHeader('Content-Type', 'application/json');
                        requestPrintVisa.setMethod('POST');
                        requestPrintVisa.setHeader('Authorization',token_type+' '+access_token); 
                        HttpResponse responsePrintVisa = new HTTP().send(requestPrintVisa);
                        System.debug('STATUS_CODE:'+responsePrintVisa.getStatusCode());
                        System.debug('strJSONresponsePrintVisa:'+responsePrintVisa.getBody());
                        requestPrintVisa.setTimeout(101000);
                        if (responsePrintVisa.getStatusCode() == 200)
                        {
                            WebServGDRFAAuthFeePaymentClass.PrintVisa printVisasObj = (WebServGDRFAAuthFeePaymentClass.PrintVisa) System.JSON.deserialize(responsePrintVisa.getBody(), WebServGDRFAAuthFeePaymentClass.PrintVisa.class);
                            Attachment attachmentPdf = new Attachment();
                            
                            if(step.Step_Name__c == 'Entry Permit Form is Typed' || step.Step_Name__c == 'Online Entry Permit Form is Typed' || step.Step_Name__c == 'Online Entry Permit is Typed'){
                                attachmentPdf.name = 'Entry Permit is issued.'+printVisasObj.fileType;
                            }
                            else if(step.Step_Name__c == 'Cancellation Form typed'){
                                attachmentPdf.name = 'Cancellation confirmation Document is issued.'+printVisasObj.fileType;
                            }
                            else if(step.Step_Name__c == 'Renewal Form is Typed'){
                                attachmentPdf.name = 'VISA Renewal Confirmation Document is issued.'+printVisasObj.fileType;
                            }
                            else if(step.Step_Name__c == 'Visa Stamping Form is Typed'){
                                attachmentPdf.name = 'Visa Approval Document is issued.'+printVisasObj.fileType;
                            }
                            SR_Doc__c srDocUploaded;
                            if(mapIdSrDoc.get(step.SR__c) != null){
                                srDocUploaded = mapIdSrDoc.get(step.SR__c);
                            }
                            //System.debug('srDocUploaded:'+srDocUploaded.id);
                            if(srDocUploaded.Id != null){
                                attachmentPdf.parentId = srDocUploaded.Id;
                            }
                            //system.debug('printVisasObj.reportContentAsBase64String:'+printVisasObj.reportContentAsBase64String);
                            if(printVisasObj.reportContentAsBase64String != null){
                                attachmentPdf.body = EncodingUtil.base64Decode(printVisasObj.reportContentAsBase64String);
                            }
                            system.debug('printVisasObj.fileType:'+printVisasObj.fileType);
                            //if(printVisasObj.fileType != null){
                            attachmentPdf.ContentType = 'application/pdf';
                            //}
                            srDocUploaded.Status__c = 'Uploaded';
                            JSONGenerator jsonFileStatusObj = JSON.createGenerator(true);
                            jsonFileStatusObj.writeStartObject();
                            if(step.SR__r.First_Name__c != null){
                                jsonFileStatusObj.writeStringField('firstName', step.SR__r.First_Name__c);
                            }
                            if(appStatusObj.FileNo != null){
                                jsonFileStatusObj.writeStringField('fileNumber', appStatusObj.FileNo);
                            }
                            
                            if(gdrfaMdData.get(step.SR__r.Nationality_list__c.toUpperCase()) != null){
                                jsonFileStatusObj.writeStringField('nationalityId', gdrfaMdData.get(step.SR__r.Nationality_list__c.toUpperCase()));
                            }
                            if(step.SR__r.Date_of_Birth__c != null){
                                jsonFileStatusObj.writeDateField('birthDate', step.SR__r.Date_of_Birth__c);
                            }
                            HttpRequest requestFileStatus = new HttpRequest();
                            requestFileStatus.setEndPoint(System.label.GDRFA_File_Status_API);
                            requestFileStatus.setBody(jsonFileStatusObj.getAsString());
                            requestFileStatus.setTimeout(120000);
                            requestFileStatus.setHeader('apikey', System.label.GDRFA_API_Key);
                            requestFileStatus.setHeader('Content-Type', 'application/json');
                            requestFileStatus.setMethod('POST');
                            requestFileStatus.setHeader('Authorization',token_type+' '+access_token); 
                            requestFileStatus.setHeader('AcceptLanguage','en-US');
                            HttpResponse responseFileStatus = new HTTP().send(requestFileStatus);
                            System.debug('STATUS_CODE:'+responseFileStatus.getStatusCode());
                            System.debug('strJSONresponseFileStatus:'+responseFileStatus.getBody());
                            requestFileStatus.setTimeout(101000);
                            if (responseFileStatus.getStatusCode() == 200)
                            {
                                WebServGDRFAAuthFeePaymentClass.FileStatus fileStatusObj = (WebServGDRFAAuthFeePaymentClass.FileStatus) System.JSON.deserialize(responseFileStatus.getBody(), WebServGDRFAAuthFeePaymentClass.FileStatus.class);
                                if(fileStatusObj.errorCode == null){
                                    if(fileStatusObj.IssueDate != null){
                                        epIssuedStep.ID_Start_Date__c =  fileStatusObj.IssueDate;
                                    }
                                    if(fileStatusObj.expiryDate != null){
                                        epIssuedStep.ID_End_Date__c =  fileStatusObj.expiryDate;
                                    }
                                    String FileNo = appStatusObj.FileNo;
                                    srDocUploadedMap.put(srDocUploaded.id, srDocUploaded);
                                    listAttchmentPdf.add(attachmentPdf);
                                    //As discussed with SAP team these values are prepopulated from SAP system to Salesforce
                                    /*if(fileTypeId == 'ZA901'){
epIssuedStep.SAP_IDTYP__c = fileTypeId;  
}*/
                                    //Added the VISA renewal and VISA Cancellation condition to assign SAP_IDNUM__c- 22/04/2021
                                    epIssuedStep.DNRD_ID_Number__c = FileNo;
                                    if(appStatusObj.FileType == 1 && epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_New' && epIssuedStep.Step_Name__c == 'Entry Permit is Issued'){
                                        epIssuedStep.SAP_IDNUM__c = FileNo.left(3) + '/' + currentYear + '/' +FileNo.substring(4,5)+ FileNo.right(6);
                                        
                                        if(mapStatusCode.get('CLOSED').Id != null){
                                            if(epIssuedStep.Step_Name__c != 'Completed' && epIssuedStep.Step_Name__c != 'Passport is Ready for Collection' && epIssuedStep.SR__r.Record_Type_Name__c != 'DIFC_Sponsorship_Visa_Cancellation')
                                                epIssuedStep.Status__c = mapStatusCode.get('CLOSED').Id;
                                        }
                                    }
                                    if(appStatusObj.FileType == 2 && epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_New' && epIssuedStep.Step_Name__c == 'Completed'){
                                        epIssuedStep.SAP_IDNUM__c = FileNo.left(3) + '/' + currentYear + '/' + FileNo.right(7);
                                    }
                                    if(appStatusObj.FileType != null && (epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Cancellation' || epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Renewal')){
                                        epIssuedStep.SAP_IDNUM__c = FileNo.left(3) + '/' +FileNo.substring(3,7)+ '/' + FileNo.right(7);
                                    }
                                    epIssuedStep.Step_Notes__c = '';
                                    epIssuedStep.OwnerId = UserInfo.getUserId();
                                    //String SrId = mapIssueStepIdSRId.get(epIssuedStep.Id);
                                    //approvedServiceRequests.add(mapStepIdSRObj.get(SrId));
                                    approvedStepMap.put(epIssuedStep.id, epIssuedStep);
                                }else if(fileStatusObj.errorCode != null){
                                    String FileNo = appStatusObj.FileNo;
                                    WebServGDRFAAuthFeePaymentClass.ErrorClass FileStatusErrorLog = (WebServGDRFAAuthFeePaymentClass.ErrorClass) System.JSON.deserialize(responseFileStatus.getBody(), WebServGDRFAAuthFeePaymentClass.ErrorClass.class);
                                    Log__c objLog = new Log__c();objLog.Type__c = 'File Status Error';objLog.Description__c = 'Request: '+jsonFileStatusObj.getAsString() +'Response: '+ responseFileStatus.getBody();objLog.SR_Step__c = epIssuedStep.Id;
                                    errorObjLogList.add(objLog); 
                                    if(FileStatusErrorLog.errorMsg[0].messageEn != null){
                                        epIssuedStep.Step_Notes__c = FileStatusErrorLog.errorMsg[0].messageEn;
                                    }
                                    //epIssuedStep.SAP_IDTYP__c = fileTypeId;
                                    epIssuedStep.DNRD_ID_Number__c = FileNo;
                                    if(appStatusObj.FileType == 1 && epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_New' && epIssuedStep.Step_Name__c == 'Entry Permit is Issued'){
                                        epIssuedStep.SAP_IDNUM__c = FileNo.left(3) + '/' + currentYear + '/' +FileNo.substring(4,5)+ FileNo.right(6);
                                    }
                                    if(appStatusObj.FileType == 2 && epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_New' && epIssuedStep.Step_Name__c == 'Completed'){
                                        epIssuedStep.SAP_IDNUM__c = FileNo.left(3) + '/' + currentYear + '/' + FileNo.right(7);
                                    }
                                    if(appStatusObj.FileType != null && (epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Cancellation' || epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Renewal')){
                                        epIssuedStep.SAP_IDNUM__c = FileNo.left(3) + '/' +FileNo.substring(3,7)+ '/' + FileNo.right(7);
                                    }
                                    //As discussed with Moosa Step status should not be changed if any error occurred during file status or docuemnt upload
                                    /* if(mapStatusCode.get('DNRD_GS_Review').Id != null){
epIssuedStep.Status__c = mapStatusCode.get('DNRD_GS_Review').Id;
}*/
                                    dnrdStepMap.put(epIssuedStep.id, epIssuedStep);
                                    dnrdStepList.add(epIssuedStep);
                                }    
                            }
                            else{
                                WebServGDRFAAuthFeePaymentClass.ErrorClass FileStatusErrorLog = (WebServGDRFAAuthFeePaymentClass.ErrorClass) System.JSON.deserialize(responseFileStatus.getBody(), WebServGDRFAAuthFeePaymentClass.ErrorClass.class);
                                String FileNo = appStatusObj.FileNo;
                                Log__c objLog = new Log__c();objLog.Type__c = 'File Status Error';objLog.Description__c = 'Request: '+jsonFileStatusObj.getAsString() +'Response: '+ responseFileStatus.getBody();objLog.SR_Step__c = epIssuedStep.Id;errorObjLogList.add(objLog); 
                                if(FileStatusErrorLog.errorMsg[0].messageEn != null){
                                    epIssuedStep.Step_Notes__c = FileStatusErrorLog.errorMsg[0].messageEn;
                                }
                                //epIssuedStep.SAP_IDTYP__c = fileTypeId;
                                epIssuedStep.DNRD_ID_Number__c = FileNo;
                                if(appStatusObj.FileType == 1 && epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_New' && epIssuedStep.Step_Name__c == 'Entry Permit is Issued'){
                                    epIssuedStep.SAP_IDNUM__c = FileNo.left(3) + '/' + currentYear + '/' +FileNo.substring(4,5)+ FileNo.right(6);
                                }
                                if(appStatusObj.FileType == 2 && epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_New' && epIssuedStep.Step_Name__c == 'Completed'){
                                    epIssuedStep.SAP_IDNUM__c = FileNo.left(3) + '/' + currentYear + '/' + FileNo.right(7);
                                }
                                if(appStatusObj.FileType != null && (epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Cancellation' || epIssuedStep.SR__r.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Renewal')){
                                    epIssuedStep.SAP_IDNUM__c = FileNo.left(3) + '/' +FileNo.substring(3,7)+ '/' + FileNo.right(7);
                                }
                                /*if(mapStatusCode.get('DNRD_GS_Review').Id != null){
epIssuedStep.Status__c = mapStatusCode.get('DNRD_GS_Review').Id;
}*/
                                dnrdStepMap.put(epIssuedStep.id, epIssuedStep);
                                dnrdStepList.add(epIssuedStep);
                            }      
                        }
                        else{
                            WebServGDRFAAuthFeePaymentClass.ErrorClass ErrorPrintVisaLog = (WebServGDRFAAuthFeePaymentClass.ErrorClass) System.JSON.deserialize(responsePrintVisa.getBody(), WebServGDRFAAuthFeePaymentClass.ErrorClass.class);
                            Log__c objLog = new Log__c();objLog.Type__c = 'Print VISA Error'; objLog.Description__c = 'Request: '+jsonPrintVisaObj.getAsString() +'Response: '+ responsePrintVisa.getBody();objLog.SR_Step__c = epIssuedStep.Id;errorObjLogList.add(objLog); 
                            if(ErrorPrintVisaLog.errorMsg[0].messageEn != null){
                                epIssuedStep.Step_Notes__c = ErrorPrintVisaLog.errorMsg[0].messageEn;
                            }
                            /*if(mapStatusCode.get('DNRD_GS_Review').Id != null){
epIssuedStep.Status__c = mapStatusCode.get('DNRD_GS_Review').Id;
}*/
                            //epIssuedStep.OwnerId = UserInfo.getUserId();
                            printVisaDnrdStepList.add(epIssuedStep);
                        }
                    }
                    else if(appStatusObj.StatesCode == 'REJECTED' || appStatusObj.StatesCode == 'INPROCESSATMOFA')
                    {
                        Log__c objLog = new Log__c();objLog.Type__c = 'Application not Approved';objLog.Description__c = 'Status of Application: '+appStatusObj.StatesCode+' Request: '+jsonAppStatusObj.getAsString() +' Response: '+ response.getBody();objLog.SR_Step__c = epIssuedStep.Id;errorObjLogList.add(objLog);
                        epIssuedStep.Step_Notes__c = appStatusObj.CurrentStatus;
                        if(mapStatusCode.get('DNRD_GS_Review').Id != null){
                            epIssuedStep.Status__c = mapStatusCode.get('DNRD_GS_Review').Id;
                        }
                        //epIssuedStep.OwnerId = UserInfo.getUserId();
                        rejectedStepList.add(epIssuedStep);
                    }else if(appStatusObj.StatesCode == 'ADDITIONALDOCREQUIRED'){
                        Log__c objLog = new Log__c();objLog.Type__c = 'Application not Approved';objLog.Description__c = 'Status of Application: '+appStatusObj.StatesCode+' Request: '+jsonAppStatusObj.getAsString() +' Response: '+ response.getBody();objLog.SR_Step__c = epIssuedStep.Id;
                        errorObjLogList.add(objLog);
                        epIssuedStep.Step_Notes__c = appStatusObj.CurrentStatus;
                        epIssuedStep.Dependent_On__c = step.Id;
                        if(mapStatusCode.get('GDRFA_Returned').Id != null){
                            epIssuedStep.Status__c = mapStatusCode.get('GDRFA_Returned').Id;
                        }
                        //epIssuedStep.OwnerId = UserInfo.getUserId();
                        rejectedStepList.add(epIssuedStep);
                        HttpRequest requestGetReqDocuments = new HttpRequest();
                        String getRequiredDocuments = System.label.GDRFA_Get_Required_Documents+'/'+step.DNRD_Receipt_No__c+'/documents';
                        requestGetReqDocuments.setEndPoint(getRequiredDocuments);
                        //requestGetReqDocuments.setBody(jsonAppStatusObj.getAsString());
                        requestGetReqDocuments.setTimeout(120000);
                        requestGetReqDocuments.setHeader('apikey', System.label.GDRFA_API_Key);
                        requestGetReqDocuments.setHeader('Content-Type', 'application/json');
                        requestGetReqDocuments.setHeader('Locale', 'en-US');
                        requestGetReqDocuments.setMethod('GET');
                        requestGetReqDocuments.setHeader('Authorization',token_type+' '+access_token);
                        HttpResponse responseGetReqDocuments = new HTTP().send(requestGetReqDocuments);
                        System.debug('STATUS_CODE:'+responseGetReqDocuments.getStatusCode());
                        System.debug('strJSONresponseSubmitRenew:'+responseGetReqDocuments.getBody());
                        requestGetReqDocuments.setTimeout(101000);
                        if (responseGetReqDocuments.getStatusCode() == 200)
                        {
                            WebServGDRFAAuthFeePaymentClass.getRequiredDoc requiredDocLog = (WebServGDRFAAuthFeePaymentClass.getRequiredDoc) System.JSON.deserialize(responseGetReqDocuments.getBody(), WebServGDRFAAuthFeePaymentClass.getRequiredDoc.class);
                            if(requiredDocLog.DocumentsList != null){
                                for(WebServGDRFAAuthFeePaymentClass.DocumentsList docDetails : requiredDocLog.DocumentsList){
                                    if(docDetails.IsNew == true)  {
                                        SR_Doc__c srDoc = new SR_Doc__c();
                                        srDoc.Name = docDetails.documentNameEn;
                                        srDoc.Service_Request__c = step.SR__c;
                                        srDoc.GDRFA_New_Document_Id__c = docDetails.documentTypeId;
                                        srDoc.Status__c = 'Pending Upload';
                                        srDoc.Is_Not_Required__c = true; //V1.3
                                        if(docDetails.Reason != null){
                                            srDoc.Document_Description_External__c = 'GDRFA New Upload: '+docDetails.Reason;
                                        }
                                        srDocList.add(srDoc);
                                    }
                                    if(docDetails.IsToBeReplaced == true)  {
                                        SR_Doc__c srDoc;
                                        if(mapsrDocIdDocId.get(docDetails.documentTypeId+''+step.SR__c) != null){
                                            srDoc = mapsrDocIdDocId.get(docDetails.documentTypeId+''+step.SR__c);
                                        }
                                        if(srDoc != null){
                                            if(docDetails.documentTypeId != null){
                                                srDoc.GDRFA_New_Document_Id__c = docDetails.documentTypeId;
                                            }
                                            srDoc.Status__c = 'Re-upload';
                                            if(docDetails.Reason != null){
                                                srDoc.Document_Description_External__c = 'GDRFA Re-Upload: '+docDetails.Reason;
                                            }
                                            srDocList.add(srDoc);
                                        }
                                    }
                                }
                            }
                        }else{
                            WebServGDRFAAuthFeePaymentClass.ErrorClass ErrorDocLog = (WebServGDRFAAuthFeePaymentClass.ErrorClass) System.JSON.deserialize(responseGetReqDocuments.getBody(), WebServGDRFAAuthFeePaymentClass.ErrorClass.class);
                            if(ErrorDocLog != null){
                                epIssuedStep.Step_Notes__c = ErrorDocLog.errorMsg[0].messageEn;
                            }
                            Log__c objLogObj = new Log__c();objLogObj.Type__c = 'Doc upload Error';objLogObj.Description__c = 'Request: '+getRequiredDocuments +'Response: '+ responseGetReqDocuments.getBody();objLogObj.SR_Step__c = epIssuedStep.Id;errorObjLogList.add(objLogObj); 
                            if(mapStatusCode.get('DNRD_GS_Review').Id != null){
                                epIssuedStep.Status__c = mapStatusCode.get('DNRD_GS_Review').Id;
                            }
                            dnrdStepMap.put(epIssuedStep.id, epIssuedStep);
                            dnrdStepList.add(epIssuedStep);
                        }
                    }
                    else{
                        //do nothing
                        System.debug('do nothing');
                    }
                }
                else{
                    WebServGDRFAAuthFeePaymentClass.ErrorClass ErrorAppStatusLog = (WebServGDRFAAuthFeePaymentClass.ErrorClass) System.JSON.deserialize(response.getBody(), WebServGDRFAAuthFeePaymentClass.ErrorClass.class);
                    if(ErrorAppStatusLog.errorMsg[0] != null){
                        epIssuedStep.Step_Notes__c = ErrorAppStatusLog.errorMsg[0].messageEn;
                    }
                    Log__c objLog = new Log__c();objLog.Type__c = 'Application Status Error';objLog.Description__c = 'Request: '+jsonAppStatusObj.getAsString() +'Response: '+ response.getBody();objLog.SR_Step__c = epIssuedStep.Id;
                    errorObjLogList.add(objLog); 
                    if(mapStatusCode.get('DNRD_GS_Review').Id != null){
                        epIssuedStep.Status__c = mapStatusCode.get('DNRD_GS_Review').Id;
                    }
                    dnrdStepMap.put(epIssuedStep.id, epIssuedStep);
                    dnrdStepList.add(epIssuedStep);
                }
            }
            //end if statement
        } 
        
        system.debug('srDocUploadedMap:'+srDocUploadedMap);
        system.debug('approvedStepMap:'+approvedStepMap);
        system.debug('rejectedStepList:'+rejectedStepList);
        system.debug('dnrdStepMap:'+dnrdStepMap);
        system.debug('errorObjLogList:'+errorObjLogList);
        system.debug('listAttchmentPdf:'+listAttchmentPdf);
        system.debug('printVisaDnrdStepList:'+printVisaDnrdStepList);
        System.debug('srDocList:'+srDocList);
        if(approvedStepMap.size() >0){ 
            List<String> StepIdList = new List<String>();
            for (String StepIds: approvedStepMap.keySet()){
                StepIdList.add(StepIds);
            }
            
            try{
                update approvedStepMap.values();
                string Result;
                for(Step__c stp: approvedStepMap.values()){
                    GsSapWebServiceDetails.IsClosed = true;
                    GsSapWebServiceDetails.PushActivityToSAP(StepIdList);
                }
                   
            for(Step__c stpObj: [SELECT id, SR__c FROM Step__c WHERE Id IN:approvedStepMap.keySet() AND Step_Name__c = 'Entry Permit Is Issued']){
                mapIssueStepIdSRId.put(stpObj.Id, stpObj.SR__c);
            }
            
            for(Service_Request__c SR : [SELECT Id, Internal_Status_Name__c, Internal_SR_Status__c, External_SR_Status__c, External_Status_Name__c FROM Service_Request__c WHERE ID IN :mapIssueStepIdSRId.Values()]){
                SR.Internal_SR_Status__c = gdrfaIdSatusName.get('Entry Permit is issued');
                SR.External_SR_Status__c = gdrfaIdSatusName.get('Entry Permit is issued');
                //approvedServiceRequests.add(SR);
                mapStepIdSRObj.put(SR.Id, SR);
                }
                if(mapStepIdSRObj.Size() > 0){
                Update mapStepIdSRObj.Values();
                } 
            }
            catch(DmlException e) {
                System.debug('approvedStepList exception: ' + e.getMessage());
            }         
        }
        
        if(listAttchmentPdf.size() >0){
            try{
                Insert listAttchmentPdf;
            }
            catch(DmlException e) {
                System.debug('listAttchmentPdf exception: ' + e.getMessage());
            }
        }
        
        if(srDocList.size() >0){
            try{
                Upsert srDocList;
            }
            catch(DmlException e) {
                System.debug('srDocList exception: ' + e.getMessage());
            }
        }
        
        if(rejectedStepList.size() >0){    
            try{
                update rejectedStepList;
            }
            catch(DmlException e) {
                System.debug('rejectedStepList exception: ' + e.getMessage());
            }  
        }
        if(dnrdStepMap.size() >0){
            try{
                update dnrdStepMap.values();
            }
            catch(DmlException e) {
                System.debug('dnrdStepList exception: ' + e.getMessage());
            }
        }
        if(errorObjLogList.size() >0){
            try{
                Insert errorObjLogList;
            }
            catch(DmlException e) {
                System.debug('errorObjLogList exception: ' + e.getMessage());
            }
        }
        if(printVisaDnrdStepList.size() >0){
            try{
                Upsert printVisaDnrdStepList;
            }
            catch(DmlException e) {
                System.debug('printVisaDnrdStepList exception: ' + e.getMessage());
            }
        }
        if(srDocUploadedMap.size() >0){
            try{
                Update srDocUploadedMap.values();
            }
            catch(DmlException e) {
                System.debug('srDocUploadedMap exception: ' + e.getMessage());
            }
        }
    } 
}