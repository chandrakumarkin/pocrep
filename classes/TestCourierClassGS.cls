/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = false)
global class TestCourierClassGS implements WebServiceMock {

    global void doInvoke(
        Object stub,
        Object request,
        Map < String, Object > response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {

        if (endpoint != null && endpoint.contains('tracking')) {
            WS_CourierTracking.TrackingResult TrackingResult = new WS_CourierTracking.TrackingResult();
            TrackingResult.WaybillNumber = '123556';
            TrackingResult.UpdateCode = '1212';
            TrackingResult.UpdateDescription = 'Delivered';
            TrackingResult.UpdateLocation = 'Test location';
            TrackingResult.Comments = 'Test Comments';
            TrackingResult.ProblemCode = 'No Problem';
            TrackingResult.UpdateDateTime = system.now();

            list < WS_CourierTracking.TrackingResult > lstTrackingElem = new list < WS_CourierTracking.TrackingResult > ();
            lstTrackingElem.add(TrackingResult);
            WS_CourierTracking.ArrayOfTrackingResult arrayoftrackingElem = new WS_CourierTracking.ArrayOfTrackingResult();
            arrayoftrackingElem.TrackingResult = lstTrackingElem;
            WS_CourierTracking.KeyValueOfstringArrayOfTrackingResultmFAkxlpY_element keyElement = new WS_CourierTracking.KeyValueOfstringArrayOfTrackingResultmFAkxlpY_element();
            keyElement.Key = '123456';
            keyElement.Value = arrayoftrackingElem;
            list < WS_CourierTracking.KeyValueOfstringArrayOfTrackingResultmFAkxlpY_element > arrayofKeyElem = new list < WS_CourierTracking.KeyValueOfstringArrayOfTrackingResultmFAkxlpY_element > ();
            arrayofKeyElem.add(keyElement);
            WS_CourierTracking.ArrayOfKeyValueOfstringArrayOfTrackingResultmFAkxlpY arrayTrackingElem = new WS_CourierTracking.ArrayOfKeyValueOfstringArrayOfTrackingResultmFAkxlpY();
            arrayTrackingElem.KeyValueOfstringArrayOfTrackingResultmFAkxlpY = arrayofKeyElem;

            WS_CourierTracking.ShipmentTrackingResponse_element Resp = new WS_CourierTracking.ShipmentTrackingResponse_element();
            Resp.hasErrors = false;
            Resp.Transaction_x = null;
            Resp.TrackingResults = arrayTrackingElem;
            response.put('response_x', Resp);

        } else if (endpoint != null && endpoint.contains('shipping')) {
            WS_Courier.ProcessedShipment processedShipment = new WS_Courier.ProcessedShipment();
            processedShipment.id = 'ABC00011';
            processedShipment.ForeignHAWB = 'shp-000111';
            processedShipment.HasErrors = false;
            List < WS_Courier.ProcessedShipment > lstProcessedShp = new List < WS_Courier.ProcessedShipment > ();
            lstProcessedShp.add(processedShipment);

            WS_Courier.ArrayOfProcessedShipment arrayofShp = new WS_Courier.ArrayOfProcessedShipment();
            arrayofShp.ProcessedShipment = lstProcessedShp;

            WS_Courier.ShipmentCreationResponse_element Resp = new WS_Courier.ShipmentCreationResponse_element();
            Resp.Shipments = arrayofShp;
            Resp.Transaction_x = null;
            Resp.hasErrors = false;
            response.put('response_x', Resp);
        }


    }
    
    @TestSetup
   static void setup()
   {
       
        Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;

        Status__c sts = new Status__c();
        sts.Name = 'Delivered';
        sts.Code__c = 'DELIVERED';
        sts.Type__c = 'End';
        insert sts;

        SR_Status__c SRsts = new SR_Status__c();
        SRsts.Name = 'Delivered';
        SRsts.Code__c = 'Delivered';
        SRsts.Type__c = 'End';
        insert SRsts;

        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c = 'Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;

        map < string, string > mapRecordType = new map < string, string > ();
        for (RecordType objRT: [select Id, DeveloperName from RecordType where DeveloperName IN('Application_of_Registration', 'Change_Shareholder_Details', 'Sale_or_Transfer_of_Shares_or_Membership_Interest', 'Allotment_of_Shares_Membership_Interest')]) {
            mapRecordType.put(objRT.DeveloperName, objRT.Id);
        }

        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.legal_structures__c = 'LTD SPC';
        objSR.currency_list__c = 'US Dollar';
        objSR.Share_Capital_Membership_Interest__c = 90;
        objSR.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR.Consignee_FName__c = 'Test';
        objSR.Consignee_LName__c = 'Test';
        //objSR.Send_SMS_To_Mobile__c = '+97112345670';
        objSR.Use_Registered_Address__c = false;
        objSR.Apt_or_Villa_No__c = 'testing123';
        objSR.Email__c = 'test@test.com';
        objSR.Avail_Courier_Services__c = 'Yes';
        objSR.Courier_Cell_Phone__c = '+97112345670';
        objSR.Courier_Mobile_Number__c = '+97112345670';
        objSR.SR_Group__c = 'GS';
        insert objSR;

        Step__c objStp = new Step__c();
        objStp.SR__c = objSR.Id;
        objStp.SR_Group__c = 'GS';
        insert objStp;

        Shipment__c shp1 = new Shipment__c();
        shp1.status__c = 'Ready';
        shp1.Courier_Type__c = 'Delivery';
        shp1.Airway_Bill_No__c = '123456';
        shp1.Step__c = objStp.id;
        insert shp1;

   }
    static testMethod void myUnitTest1() {

     //   Step__c stp1 = [select id, Name, SR__c, SR_Group__c, SR__r.Courier_Cell_Phone__c, SR__r.Courier_Mobile_Number__c, SR__r.Consignee_FName__c, SR__r.Consignee_LName__c, SR__r.Avail_Courier_Services__c, SR__r.Send_SMS_To_Mobile__c, SR__r.Apt_or_Villa_No__c, SR__r.Customer__r.Office__c, SR__r.Customer__r.Building_Name__c, SR__r.Customer__r.Name, SR__r.Email__c, SR__r.Customer__c, SR__r.Use_Registered_Address__c, SR__r.Express_Service__c, Step_Template__r.Courier_Type__c from Step__c where SR__r.Courier_Cell_Phone__c = '+9711234567'];
        test.startTest();
        Test.setMock(WebServiceMock.class, new Test_CC_CourierCls());
        database.executeBatch(new CourierTracking_Cls(), 1);
        test.stopTest();

    }

    static testmethod void myUnitTest4() {

        WS_Courier.Attachment attch = new WS_Courier.Attachment();
        WS_Courier.ArrayOfAttachment arayofAttach = new WS_Courier.ArrayOfAttachment();
        WS_Courier.ShipmentLabel shpLabel = new WS_Courier.ShipmentLabel();
        WS_Courier.ArrayOfNotification arayofNot = new WS_Courier.ArrayOfNotification();
        WS_Courier.ArrayOfDimensions arayOfDimen = new WS_Courier.ArrayOfDimensions();
        WS_Courier.ShipmentItem shp = new WS_Courier.ShipmentItem();
        WS_Courier.Money mon = new WS_Courier.Money();
        WS_Courier.Volume vol = new WS_Courier.Volume();
        WS_Courier.ArrayOfProcessedShipmentAttachment arayofProc = new WS_Courier.ArrayOfProcessedShipmentAttachment();
        WS_Courier.ScheduledDelivery schd = new WS_Courier.ScheduledDelivery();
        WS_Courier.ProcessedShipmentAttachment procShip = new WS_Courier.ProcessedShipmentAttachment();


    }

}