/*
    Author      :   Shoaib Tariq
    Description :   
    --------------------------------------------------------------------------------------------------------------------------
  Modification History
   --------------------------------------------------------------------------------------------------------------------------
  V.No  Date    Updated By      Description
  --------------------------------------------------------------------------------------------------------------------------             
   V1.0    17-02-2020  shoaib      Created
*/
@isTest
public class PriceItemTriggerHandler_Test {
    
    static testMethod void triggerTestCase(){
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Submitted_Date__c = Date.today();
        objSR2.SR_Group__c      = 'Fit-Out & Events';
        objSR2.Pre_GoLive__c    = false; 
        objSR2.SAP_Unique_No__c = '33232432432444'; 
        objSR2.RecordTypeId     = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Miscellaneous_Services').getRecordTypeId();
        insert objSR2;
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR2.Id;
        objSRItem.Status__c = 'Invoiced';
        objSRItem.Price__c = 1000;
        insert objSRItem;
        
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'PSA';
        objProd.ProductCode = 'PSA';
        lstProducts.add(objProd);
        insert lstProducts;
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'PSA';
        objPL.DEV_Id__c = 'PSA';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'PSA';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            objDP.Unit_Price__c = 2500;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
        
        
        SR_Price_Item__c objSRItem1 = new SR_Price_Item__c();
        objSRItem1.ServiceRequest__c = objSR2.Id;
        objSRItem1.Status__c = 'Invoiced';
        objSRItem1.Product__c = lstProducts[0].id;
        objSRItem1.Pricing_Line__c = lstPLs[0].id;
        insert objSRItem1;
        
        List<SR_Price_Item__c> SRPriceList = new List<SR_Price_Item__c>();
        
        SRPriceList.add(objSRItem1); 
    
        
        Test.startTest();
          objSRItem.Transaction_Number__c = '123456789012345';
          
          Update objSRItem;
          PriceItemTriggerHandler.UpdatePipelinePSAonAccount(SRPriceList);
        Test.stopTest();
        
     
        
    }
}