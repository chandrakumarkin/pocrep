/******************************************************************************************
 *  Class Name  : TestStepHelper
 *  Author      : Sai kalyan Sanisetty 
 *  Company     : DIFC
 *  Date        : 14 April 2019        
 *  Description : Test Class for Step Helper                  
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1   14 April 2019         Sai                Initial Version  
*******************************************************************************************/

@isTest
public class TestStepHelper{
	
	public static Step__c stepRecord;
	public static User portalUser;
	public static List<Account> lstAccount;
	public static List<Amendment__c> lstAmendments;
	public static List<Contact> lstContact;
	
	public static void testData(){
		
		List<Account> lstAccount = new List<Account>();
		lstContact = new List<Contact>();
		List<Service_Request__c> lstServiceRequestData = new List<Service_Request__c>();
		lstAmendments = new List<Amendment__c>();
		
		lstAccount = TestUtilityClass.accountDataInsert(1,false);
		lstAccount[0].ROC_Status__c = 'Under Formation';
		
		if(lstAccount.size()>0){
			INSERT lstAccount;
		}
		
		lstContact = TestUtilityClass.contactDataInsert(1,true,lstAccount[0].ID);
		portalUser = new User();
        portalUser = TestUtilityClass.insertPortalUser(lstContact[0].ID); 
        
        system.runas(portalUser){
			lstServiceRequestData = TestUtilityClass.createServiceRequestData(1,false);
			lstServiceRequestData[0].Customer__c =lstAccount[0].ID;
			
			if(lstServiceRequestData.size()>0){
				INSERT  lstServiceRequestData;
			}
			
			stepRecord = new Step__c();
	        stepRecord.SR__c = lstServiceRequestData[0].Id;
	        stepRecord.SR_Record_Type_Text__c = 'Application_of_Registration';
	        insert stepRecord;
	        
			lstAmendments = TestUtilityClass.amendmentDataInsert(5,true,lstServiceRequestData[0].ID);
        }
	}
	static testmethod void positiveTest1(){
		
		try{
		testData();
		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new StepHelperMockTest());
			StepHealper.stepHealperMethod(stepRecord.Id);
		Test.stopTest();
		}
		catch(Exception ex){
			
		}
	}
	
	static testmethod void positiveTest2(){
		
		try{
		testData();
		lstContact[0].Previous_Nationality__c  = 'Russian Federation';
		lstContact[0].Nationality__c = 'Russian Federation';
		UPDATE lstContact;
		
		lstAmendments[0].Nationality_list__c  =  'Russian Federation';
		UPDATE lstAmendments;
		Test.startTest();
			Test.setMock(HttpCalloutMock.class, new StepHelperMockTest());
			StepHealper.stepHealperMethod(stepRecord.Id);
		Test.stopTest();
		}
		catch(Exception ex){
			
		}
	}
	
	static testmethod void positiveTest3(){
		
		testData();
		lstAmendments[0].Amendment_Type__c = 'Body Corporate';
		UPDATE lstAmendments;
		
		Test.startTest();
			StepHealper.stepHealperMethod(stepRecord.Id);
		Test.stopTest();
		
	}
	
	static testmethod void positiveTest4(){
		
		testData();
		
		lstAmendments[0].Amendment_Type__c = 'Body Corporate';
		lstAmendments[0].Relationship_Type__c = 'Director';
		UPDATE lstAmendments;
		
		Test.startTest();
			StepHealper.stepHealperMethod(stepRecord.Id);
		Test.stopTest();
		
	}
	static testmethod void positiveTest5(){
		
		testData();
		lstAmendments[0].Nationality_list__c = 'Russian Federation'	;
		lstAmendments[0].Relationship_Type__c = 'Director';
		UPDATE lstAmendments;
		
		Test.startTest();
			StepHealper.stepHealperMethod(stepRecord.Id);
		Test.stopTest();
		
	}
}