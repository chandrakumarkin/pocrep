/*
    Author      : Durga Prasad
    Date        : 23-Dec-2019
    Description : Custom code to create Establishment Card SR on AOR Approval
    
     --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    30-Nov-2020  Arun          Added code to create permnit 
    
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_CreateEstablishmentCard implements HexaBPM.iCustomCodeExecutable {
  
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        
        system.debug('---create establisment----');
        
        //V1.1 
        if(stp.HexaBPM__SR__c!=null)
        RocCreatePermits.DOBCreateNewPermits(stp.HexaBPM__SR__c);
        
        if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null && stp.HexaBPM__SR__r.Apply_for_Establishment_Card__c=='Yes'){
          system.debug('-12--create establisment--card--');
          try{
            Map<string,string> srStatusMap = new map<string,string>(); // Map with status code as Key and Id as Value ** SR Statuses **
            for(SR_Status__c srStatus: [select id,code__c from SR_Status__c where Code__c IN  ('SUBMITTED','DRAFT')]){
                srStatusMap.put(srStatus.code__c.tolowercase(),srStatus.id);
             }
           
            Account objAcc = [Select Id,Name,Active_License__c,Active_License__r.Name,Index_Card__r.Name,Index_Card__r.Valid_To__c,Active_License__r.License_Expiry_Date__c from Account where Id=:stp.HexaBPM__SR__r.HexaBPM__Customer__c];
            Service_Request__c serviceRequest = new Service_Request__c(SR_group__c='GS');
            for(SR_Template__c objTemplate:[select Id from SR_Template__c where SR_RecordType_API_Name__c='New_Index_Card']){
              serviceRequest.SR_Template__c =  objTemplate.Id;
            }
            
            serviceRequest.Customer__c = objAcc.Id;
            serviceRequest.License_Number__c = objAcc.Active_License__r.Name;
            serviceRequest.Current_License_Expiry_Date__c = objAcc.Active_License__r.License_Expiry_Date__c;
            serviceRequest.Email__c = stp.HexaBPM__SR__r.HexaBPM__Email__c;
            serviceRequest.Send_SMS_To_Mobile__c = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_To_Mobile__c;
            
            if(stp.HexaBPM__SR__r.Select_who_will_be_signing_the_PSA__c!=null){
              for(HexaBPM_Amendment__c objAmnd:[Select First_Name__c,Last_Name__c,Mobile__c from HexaBPM_Amendment__c where Id=:stp.HexaBPM__SR__r.Select_who_will_be_signing_the_PSA__c]){
                serviceRequest.Consignee_FName__c = objAmnd.First_Name__c;
                serviceRequest.Consignee_LName__c = objAmnd.Last_Name__c;
                serviceRequest.Courier_Mobile_Number__c = objAmnd.Mobile__c;
                //serviceRequest.Express_Service__c = SR.Establishment_Card_Express__c=='Yes'? true : false;
                //serviceRequest.Avail_Courier_Services__c = sr.Avail_Courier_Services__c;
                //serviceRequest.Would_you_like_to_opt_our_free_couri__c = (sr.Avail_Courier_Services__c == 'No'||sr.Avail_Courier_Services__c =='' ||sr.Avail_Courier_Services__c ==null)? 'No':'Yes';// v1.2
                //serviceRequest.Use_Registered_Address__c = SR.Use_Registered_Address__c;
                //serviceRequest.Courier_Cell_Phone__c = SR.Courier_Cell_Phone__c;
                //serviceRequest.Apt_or_Villa_No__c  = SR.Apt_or_Villa_No__c;
              }
            }
            //serviceRequest.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get(srRecordTypeNameTobecreated).getRecordTypeId();
            serviceRequest.Use_Registered_Address__c = true;
            serviceRequest.internal_SR_status__c = srStatusMap.get('draft');
            serviceRequest.External_SR_Status__c = srStatusMap.get('draft');
            //serviceRequest.Is_Applicant_Salary_Above_AED_20_000__c = 'Yes';
            serviceRequest.Establishment_Card_No__c = objAcc.Index_Card__r.Name;
            serviceRequest.Establishment_Card_Expiry_Date__c = objAcc.Index_Card__r.Valid_To__c;
            serviceRequest.Current_License_Expiry_Date__c = objAcc.Active_License__r.License_Expiry_Date__c;
            //serviceRequest.Linked_SR__c = stp.HexaBPM__SR__c;
            serviceRequest.OB_Application__c = stp.HexaBPM__SR__c;
            serviceRequest.Quantity__c = 0;
            serviceRequest.Mobile_Number__c = stp.HexaBPM__SR__r.Point_of_Contact__c;
            if(stp.HexaBPM__SR__r.Type_of_Services_Establishment_Card__c=='Express')
                serviceRequest.Express_Service__c = true;
            
            insert serviceRequest;
            
            submitSR(serviceRequest.Id);

            // update serviceRequest;
            
            system.debug('-12--create establisment--card--'+serviceRequest.Id);
          }catch(Exception e){
                system.debug('--------XXXX------>'+e.getMessage());
            Log__c objLog = new Log__c();
                objLog.Description__c = 'Exception on CC_CreateEstablishmentCard - Line:52 :'+e.getMessage();
                objLog.Type__c = 'Custom Code to create Establishment Card SR on AOR Approval';
                insert objLog;
          }
        }
        return strResult;
    }

    @future(Callout = true)
    
    public static void submitSR(String srId){

      String remoteURL = 'callout:Full_Access_Org/services/data/v36.0/sobjects/Service_Request__c/'+srId+'?_HttpMethod=PATCH';

      Map<string,string> srStatusMap                  = new map<string,string>();   

      for(SR_Status__c srStatus: [select id,code__c from SR_Status__c where Code__c IN  ('SUBMITTED','DRAFT')]){
            srStatusMap.put(srStatus.code__c.tolowercase(),srStatus.id);
      } 
      map<string, object> mapToSerialize             = new map<string, object>();
      mapToSerialize.put('internal_SR_status__c',srStatusMap.get('submitted')); 
      mapToSerialize.put('External_SR_Status__c',srStatusMap.get('submitted'));   
      
      String body                                             = JSON.serialize(mapToSerialize);
      HTTPRequest httpRequest                                 = new HTTPRequest();
      httpRequest.setMethod('POST');
      httpRequest.setHeader('Content-Type', 'application/json');
      httpRequest.setBody(body);
      httpRequest.setEndpoint(remoteURL);
      system.debug('@@@@@@@@@@@@@ httpRequest '+httpRequest);
      if(!test.isRunningTest()){
        HTTPResponse res                                = new Http().send(httpRequest);
      }
    }
}