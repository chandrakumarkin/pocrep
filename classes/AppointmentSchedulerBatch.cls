/**********************************************************
Class Name: AppointmentSchedulerBatch to execute the AppointmentCreationUtilityCls
Description: re developed the AppointmentCreationHandler logic
Scheduler Class : AppointmentSchedulerBatchScheduler 
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By       Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    18-09-2019  Selva    #7365  
********************************************************/
// Batch Job for Processing the Records
global class AppointmentSchedulerBatch implements Database.Batchable<sobject> {
   global Database.Querylocator start(Database.BatchableContext BC) {
      return Database.getQueryLocator('select id,Step_Code__c, sr__c,createdDate,Biometrics_Required__c,AppointmentCreation__c from step__c where AppointmentCreation__c=\''+Label.GSAppointmentCreateStatus+'\'order by createdDate ASC'); 
   }
   global void execute (Database.BatchableContext BC, List<Step__c> scope) {
        system.debug('-scope--'+scope.size());
        
        List<Step__c> stepLst = new List<Step__c>();
        for(Step__c itr:scope){
            string str= AppointmentCreationUtilityCls.MedicalAppointmentCreate(itr);
            if(str=='Processed'){
                itr.AppointmentCreation__c  = label.GSAppointmentCompleted;
                stepLst.add(itr);
            }else{
            } 
        }
        update stepLst;
   }
   global void finish(Database.BatchableContext BC) {
       
        /********** execute in developer console to schulde the class
            Integer x = 3;
            for(Integer i=3;i<22;i++){
                string ss='0 '+x+' * * * ?';
                x=x+3;
                System.schedule('Scheduled Job-'+Datetime.now()+'-'+i+x, ss, new AppointmentSchedulerBatchScheduler());
            }
         **********/
   }
}