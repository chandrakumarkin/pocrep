/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAlldata=false)
private class TestGsBatchFailedWebServices {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        objTemplate.SR_Group__c = 'GS';
        insert objTemplate;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstSRStatus.add(new SR_Status__c(Name='Closed',Code__c='Closed'));
        lstSRStatus.add(new SR_Status__c(Name='Verified',Code__c='VERIFIED'));
        insert lstSRStatus;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
       // lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        SR_Status__c underProcstatus = new SR_Status__c();
        underProcstatus.name = 'Under Verification';
       underProcstatus.code__C = 'Under_Verification';
        insert underProcstatus; 
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'DIFC New Visa';
        lstProducts.add(objProd);
        
        Product2 objPSAProd = new Product2();
        objPSAProd.Name = 'PSA';
        lstProducts.add(objPSAProd);
        
        objPSAProd = new Product2();
        objPSAProd.Name = 'DNRD';
        objPSAProd.Is_Guarantee_Product__c = true;
        lstProducts.add(objPSAProd);
        insert lstProducts;
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC New Sponsorship Visa New';
        objPL.DEV_Id__c = 'GO-00001';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00001';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'PSA';
        objPL.DEV_Id__c = 'GO-PSA';
        objPL.Product__c = lstProducts[1].Id;
        objPL.Material_Code__c = 'GO-PSA';
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'DNRD';
        objPL.DEV_Id__c = 'GO-DNRD';
        objPL.Product__c = lstProducts[2].Id;
        objPL.Material_Code__c = 'GO-DNRD';
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            objDP.Unit_Price__c = 1234;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_New');
        objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        insert objSR;
        
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = underProcstatus.Id;
        objSR.External_SR_Status__c = underProcstatus.Id;
        update objSR;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 3210;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        lstPriceItems.add(objItem);
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 2500;
        objItem.Pricing_Line__c = lstPLs[1].Id;
        objItem.Product__c = lstProducts[1].Id;
        lstPriceItems.add(objItem);
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 1500;
        objItem.Pricing_Line__c = lstPLs[2].Id;
        objItem.Product__c = lstProducts[2].Id;
        lstPriceItems.add(objItem);
        
        insert lstPriceItems;
        
        Step_Template__c stepTemplate = new Step_Template__c();
        stepTemplate.name ='Front Desk Review';
        stepTemplate.code__C ='FRONT_DESK_REVIEW';
        stepTemplate.Step_RecordType_API_Name__c ='Front_Desk_Review';
        insert stepTemplate;
                
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Status__c = lstStatus[0].Id;
        objStep.Step_Template__c = stepTemplate.id;
        objStep.Closed_Date_Time__c = system.now();
        objStep.Closed_Date__c = system.today();
        insert objStep;
        
        Service_Request__c SR = [select Id,Internal_Status_Name__c,SR_Template__r.Menu__c,Contact__c,Sponsor__c,SAP_Unique_No__c,External_Status_Name__c,Is_Rejected__c,isClosedStatus__c from Service_Request__c where id =: objSR.id];
        
        step__C stp =[select id,step_name__C,Closed_Date_Time__c,Closed_Date__c,Status__c,Step_Status__c from step__C where id =: objStep.id ];
        
        String sQuery = Apex_Utilcls.getAllFieldsSOQL('Service_Request__c');
        sQuery += ' where Id=\''+objSR.Id+'\'';
        
        Service_Request__c objSRTemp = Database.query(sQuery);
        
        stp.SR__r = SR ;
        
        
        system.debug('closed Date / Time '+ stp.Closed_Date_Time__c);
        system.debug('Step in test class' +stp );
        system.debug('SR in the test class'+SR);
                 
        Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
        Test.setMock(HttpCalloutMock.class, new TestGsCRMServiceMock());
        
        //Test Data Preparation for Partner Creatation
        list<GsSAPCRMService.ZSF_IN_LOG> lstCRMResponse = new list<GsSAPCRMService.ZSF_IN_LOG>();
        GsSAPCRMService.ZSF_IN_LOG objOut = new GsSAPCRMService.ZSF_IN_LOG();
        objOut.ACTION = 'P';
        objOut.SFREFNO = string.valueOf(objSRTemp.Id).substring(0,15);
        objOut.SFGUID = '0031100000j2ri6';
        objOut.PARTNER1 = '001234';
        objOut.PARTNER2 = '001239';
        objOut.MSG = 'Partner Created Successfully';
        objOut.STATUS = 'P';
        lstCRMResponse.add(objOut);
        objOut = new GsSAPCRMService.ZSF_IN_LOG();
        objOut.ACTION = 'P';
        objOut.SFREFNO = string.valueOf(objSRTemp.Id).substring(0,15);
        objOut.SFGUID = '0031100000j2ri6';
        objOut.PARTNER1 = '001234';
        objOut.PARTNER2 = '';
        objOut.STATUS = 'N';
        objOut.MSG = 'Partner Created Successfully';
        lstCRMResponse.add(objOut);
        
        
        // Test Data Preparation for PSA Items
        list<SAPECCWebService.ZSF_S_RECE_SERV_OP> lstPSA = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = lstPriceItems[1].Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        objTemp.UNQNO = '123456750002015';
        lstPSA.add(objTemp);
        objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'N';
        objTemp.SGUID = lstPriceItems[1].Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        objTemp.UNQNO = '123456750002015';
        lstPSA.add(objTemp);
        
        //Test Data Preparation for GS Service Posting
        string SRItemId;
        for(SR_Price_Item__c objsrItem : [select Id from SR_Price_Item__c where Id=:lstPriceItems[0].Id]){
            SRItemId = objsrItem.Id;
        }
        list<SAPGSWebServices.ZSF_S_GS_SERV_OP> lstGSItems = new list<SAPGSWebServices.ZSF_S_GS_SERV_OP>();
        SAPGSWebServices.ZSF_S_GS_SERV_OP objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'N';
        objGSItem.SOPRP = 'N';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        objGSItem.SGUID = SRItemId;
        lstGSItems.add(objGSItem);
        objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'P';
        objGSItem.SOPRP = 'N';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        objGSItem.SGUID = SRItemId;
        lstGSItems.add(objGSItem);
        objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'P';
        objGSItem.SOPRP = 'P';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        objGSItem.SGUID = SRItemId;
        lstGSItems.add(objGSItem);
        
        //Test Data Preparation for GS Account Balance
        list<SAPGSWebServices.ZSF_S_ACC_BAL> lstAccountBal = new list<SAPGSWebServices.ZSF_S_ACC_BAL>();
        SAPGSWebServices.ZSF_S_ACC_BAL objActBal = new SAPGSWebServices.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        lstAccountBal.add(objActBal);
        objActBal = new SAPGSWebServices.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        lstAccountBal.add(objActBal);
        objActBal = new SAPGSWebServices.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        lstAccountBal.add(objActBal);
        objActBal = new SAPGSWebServices.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'N';
        objActBal.WRBTR = '1234';
        lstAccountBal.add(objActBal);
        objActBal = new SAPGSWebServices.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'N';
        objActBal.WRBTR = 'Test Error';
        lstAccountBal.add(objActBal);
        
        //Test Data Preparation for Next Activity
        list<SAPGSWebServices.ZSF_S_GS_NEXT_ACT> lstNextActivity = new list<SAPGSWebServices.ZSF_S_GS_NEXT_ACT>();
        SAPGSWebServices.ZSF_S_GS_NEXT_ACT objNextAct = new SAPGSWebServices.ZSF_S_GS_NEXT_ACT();
        objNextAct.ACCTP = 'P';
        objNextAct.ACRES = 'BO';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.IDSDT = '2016-01-01';
        objNextAct.IDEDT = '2016-01-01';
        objNextAct.BANFN = '50001234';
        objNextAct.MATNR = 'GO-00001';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.SEQNR = '0010';
        objNextAct.ACTVT = 'Under Process';
        lstNextActivity.add(objNextAct);
        objNextAct = new SAPGSWebServices.ZSF_S_GS_NEXT_ACT();
        objNextAct.ACCTP = 'P';
        objNextAct.ACRES = 'BO';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.IDSDT = '2016-01-01';
        objNextAct.IDEDT = '2016-01-01';
        objNextAct.BANFN = '50001234';
        objNextAct.MATNR = 'GO-00001';
        objNextAct.UNQNO = '12345675000215';
        objNextAct.SEQNR = '0020';
        objNextAct.ACTVT = 'Online Entry permit typed';
        lstNextActivity.add(objNextAct);
        
        TestGsCRMServiceMock.lstCRMResponse = lstCRMResponse;
        TestGsCRMServiceMock.lstPSAItems = lstPSA;
        TestGsCRMServiceMock.lstGSPriceItems = lstGSItems;
        TestGsCRMServiceMock.lstAccountBal = lstAccountBal;
        TestGsCRMServiceMock.lstNextActivity = lstNextActivity;
        
        test.startTest();
            GsScheduleFailedWebServices objSchedule = new GsScheduleFailedWebServices();
            Datetime dt = system.now().addMinutes(5);
            //Database.executeBatch(new GsBatchFailedWebServices(),1);
            string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();
            for(CronTrigger obj : [select Id from CronTrigger where CronJobDetailId IN (SELECT Id FROM CronJobDetail where Name = 'Gs ScheduleFailedWebServices')])
                system.abortJob(obj.Id);
            system.schedule('Gs ScheduleFailedWebServices', sCornExp, objSchedule);
        test.stopTest();
    }
}