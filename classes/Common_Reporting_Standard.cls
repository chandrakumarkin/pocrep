/**
* @description       : Common Reporting Standard (CRS) and FATCA user information
* @author            : ChangeMeIn@UserSettingsUnder.SFDoc
* @group             : 
* @last modified on  : 20/04/2021
* @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
* Modifications Log 
* Test Class : Common_Reporting_Standard_Test
* Ver   Date         Author                               Modification
* 1.0   20/04/2021   Suchita                              Initial Version
**/
public without sharing class Common_Reporting_Standard{
    
    
    public string RecordTypeId;
    public map<string,string> mapParameters;
    public String CustomerId;
    public Account ObjAccount{get;set;}
    public Service_Request__c SRData{get;set;}
    public boolean ValidTosubmit;
    public integer rowNumberToEdit{get;set;}
    public String Type{get;set;}
    public String Summary{get;set;}
    Public boolean addnewExistingBool{get;set;}
    Public boolean addnewBool{get;set;}
    //When user click on Add new Amd    
    public Amendment__c TempObjAnd{get;set;}
    public List<Amendment__c> ListAmend {get;set;}
    public List<Contact> existingContactList {get;set;}
    public String selectedVal{get;set;}  // This will hold the selected value, the id in here
    Public integer MakerCounter=0;
    Public integer ChekerCounter=0;
    public Common_Reporting_Standard(ApexPages.StandardController controller) {
        List<String> fields = new List<String> {'Customer__c','Legal_Structures__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        
        SRData=(Service_Request__c)controller.getRecord();
        ValidTosubmit=true;
        ListAmend=new List<Amendment__c>();
        
        mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
            mapParameters = apexpages.currentPage().getParameters();              
        if(mapParameters.get('RecordType')!=null) 
            RecordTypeId= mapParameters.get('RecordType');    
        else
            RecordTypeId=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Common_Reporting_Standard').getRecordTypeId();
        
        
        for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Registration_License_No__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                         Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                         Contact.Account.Qualifying_Type__c,Contact.Account.Restrict_certificate_of_good_standing__c,Contact.Account.Contact_Details_Provided__c,
                         Contact.Account.Index_Card__c,Contact.Account.Index_Card_Status__c,Contact.Account.Is_Foundation_Activity__c,  
                         Contact.Name,Contact.Occupation__c,Contact.Account.GIIN__c from User where Id=:userinfo.getUserId()]){
            CustomerId = objUsr.Contact.AccountId;
            ObjAccount= objUsr.Contact.Account;
            
            if(SRData.id==null) {
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                SRData.Sponsor_Mother_Full_Name__c=SRData.Legal_Structures__c;
                SRData.Declaration_Signatory_Name__c = objUsr.Contact.Name;
                SRData.Declaration_Signatory_Capacity__c = objUsr.Contact.Occupation__c;
                if(objUsr.Contact.Account.GIIN__c !=null){ SRData.Summary__c=objUsr.Contact.Account.GIIN__c;}
               
            }
        }
        ExistingRecords();
        
    }
    
    public  void ExistingRecords(){
        if(SRData.id!=null){
            ListAmend=[select id,Contact__c,Permanent_Native_Country__c,Apt_or_Villa_No__c,Permanent_Native_City__c,Building_Name__c,Street__c,Emirate_State__c,PO_Box__c,Title_new__c,Occupation__c,Family_Name__c,Given_Name__c,Job_Title__c,Passport_No__c,Person_Email__c,Nationality_list__c,Mobile__c,Phone__c,Amendment_Type__c,UserName__c from Amendment__c where ServiceRequest__c=:SRData.id ];
        }
    }
    
    public  List<Amendment__c> getListofNewAmend(){
        if(SRData.id!=null) //Existing SR retrun all existing Amd 
            return [select id,Contact__c,Permanent_Native_Country__c,Apt_or_Villa_No__c,Permanent_Native_City__c,Building_Name__c,Street__c,Emirate_State__c,PO_Box__c,Title_new__c,Occupation__c,Family_Name__c,Given_Name__c,Job_Title__c,Passport_No__c,Person_Email__c,Nationality_list__c,Mobile__c,Phone__c,Amendment_Type__c,UserName__c from Amendment__c where ServiceRequest__c=:SRData.id and (Status__c='Active' or Status__c='Draft') order by createddate];
        else
            return ListAmend;
        
    }
    
    
    public  void AddAndRecord(){    
        TempObjAnd=new Amendment__c();
        TempObjAnd.RecordTypeId=Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
        TempObjAnd.Status__c='Draft';
        TempObjAnd.ServiceRequest__c=SRData.id;
        TempObjAnd.Amendment_Type__c=Type;
        TempObjAnd.Relationship_Type__c='Common Reporting Standard';
       
    }
    public  void AddMaker(){
        Type='Maker';
    }
    public  void AddChecker(){
        Type='Checker';
    }
    public void addNewExisting(){
        if(Summary=='AddExisting'){
            addnewExistingBool=true; addnewBool=false;TempObjAnd=null;
        }else if(Summary=='AddNew'){
            addnewBool=true; addnewExistingBool=false; TempObjAnd=null;
            AddAndRecord();
        }
    }
    
    public  void fillContactDetails(){
        for(Contact c:[select id,Salutation,FirstName,LastName,Email,MobilePhone from Contact where Id=:selectedVal]){
            TempObjAnd=null;
            TempObjAnd=new Amendment__c();
            TempObjAnd.Contact__c=c.id;
            TempObjAnd.Title_new__c=c.Salutation; TempObjAnd.Family_Name__c=c.LastName;
            TempObjAnd.Given_Name__c=c.FirstName; TempObjAnd.Person_Email__c=c.Email;
            TempObjAnd.Mobile__c=c.MobilePhone;
            TempObjAnd.Amendment_Type__c=Type;
            TempObjAnd.Status__c='Draft';
            TempObjAnd.Relationship_Type__c='Common Reporting Standard';
            TempObjAnd.RecordTypeId=Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
            TempObjAnd.ServiceRequest__c=SRData.id;
            TempObjAnd.Customer__c=SRData.Customer__c;

        }                
        
    }
    
    
    public List<SelectOption> ContactOptions{
        get{
            List<contact> ListContact=new   List<contact>(); 
            List<SelectOption> optns = new List<Selectoption>();
            for(AccountContactRelation cont : [select ContactId,Contact.id,Contact.Email,Contact.Name,AccountId from AccountContactRelation where  AccountId=:CustomerId  and Contact.RecordType.DeveloperName='Portal_User'  and IsActive=true and Roles INCLUDES ('Company Services') order by createdDate]){
                ListContact.add(cont.Contact); 
            } 
            optns.add(new selectOption('None', 'Please Select'));
            
            for(Contact obj :ListContact ){
                optns.add(new selectOption(obj.Id, obj.Name));
            }            
            
            return optns;
        }set;
    }
    
    //When user click on Edit Amd
    public  void EditUBORecord(){
        TempObjAnd=new Amendment__c();
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        
        if(getListofNewAmend().size()>=rowNumberToEdit){
            TempObjAnd=getListofNewAmend()[rowNumberToEdit];
        }        
    }
    //When user Click Cancel 
    public  void CancelSaveRow(){
        TempObjAnd=null;
    }
    public  void SaveRecordAndAdd(){
        SaveAmdRow();
        AddAndRecord();
    }
    
    public  void SaveAmdRow(){
        try{ 
            
            upsert SRData;
            //ListAmend
            if(TempObjAnd.id != NULL && ListAmend.size() > rowNumberToEdit){
                ListAmend[rowNumberToEdit] =  TempObjAnd;
            }
            else{
                ListAmend.add(TempObjAnd);
            }
            
        MakerCounter=0;
        MakerCounter=0;
              
            for(Amendment__c ObjAnd:ListAmend){
                ObjAnd.ServiceRequest__c=SRData.id;
                String TypeString;
                
                integer Counter=0;
                if(ObjAnd.Amendment_Type__c=='Maker'){
                    TypeString = 'M';
                   MakerCounter++;
                }else if(ObjAnd.Amendment_Type__c=='Checker'){
                    TypeString = 'C';
                    ChekerCounter++;
                }
                
                
                if(ObjAnd.UserName__c==null & ObjAnd.Amendment_Type__c=='Maker')
                    ObjAnd.UserName__c=ObjAnd.Given_Name__c+ObjAccount.Registration_License_No__c+TypeString+MakerCounter;
                else   if(ObjAnd.UserName__c==null & ObjAnd.Amendment_Type__c=='Checker')
                    ObjAnd.UserName__c=ObjAnd.Given_Name__c+ObjAccount.Registration_License_No__c+TypeString+ChekerCounter;
            }
            upsert ListAmend;
            addnewBool=false;
            addnewExistingBool=false;
            Type=null;
            Summary=null;
			selectedVal=null;
            CancelSaveRow();
        }catch (Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
            ApexPages.addMessage(myMsg);    
        }
    }
    
    public void removingUBORow()
    {
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        if(ListAmend.size()>=param && ListAmend.size() > 1){
            Amendment__c TempObjPro=ListAmend[rowNumberToEdit];
            
            ListAmend.remove(rowNumberToEdit);
            /*(TempObjPro.Contact__c!=null)
                TempObjPro.Status__c='Removed';
            else */if(TempObjPro.id!=null)
                delete TempObjPro;
            
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'At least 1 Maker/Checker is required');         
            ApexPages.addMessage(myMsg);   
        }
    }
    public string  validationRule(){
        
        string Errormessage='';
        if(SRData!=null && SRData.Type_of_Request__c!='CRS' && SRData.Summary__c==null ){
            Errormessage='Please Add Global Intermediary identification Number (GIIN).';
        }else{
           Errormessage = CheckAtleastOneCM();
        }
        return Errormessage;
        
    }
    public string  CheckAtleastOneCM(){
        list<Amendment__c> makerList = new List<Amendment__c>();
       	makerList=[select id,Amendment_Type__c from Amendment__c where ServiceRequest__c=:SRData.id and Amendment_Type__c='Maker'];
        list<Amendment__c> CheckerList = new List<Amendment__c>();
        CheckerList= [select id,Amendment_Type__c from Amendment__c where ServiceRequest__c=:SRData.id and Amendment_Type__c='Checker'];  
        string Errormessage='';
        if(ListAmend.size()<1 || makerList.size()<1 || CheckerList.size()<1) {
            Errormessage='Please add one Maker and Checker';
        }
        return Errormessage;
        
    } 
    public  PageReference SaveRecord(){
        
        string Errormessage=validationRule();
        boolean isvalid=String.isBlank(Errormessage);
        if(isvalid) {       
            
            try {upsert SRData;
                PageReference acctPage = new ApexPages.StandardController(SRData).view();        
                acctPage.setRedirect(true);        
                return acctPage;     
            }catch (Exception e) {ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
                ApexPages.addMessage(myMsg);    
            }  
        }
        else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Errormessage);         
            ApexPages.addMessage(myMsg); 
        }
        
        return null;
        
        
    }
    
}