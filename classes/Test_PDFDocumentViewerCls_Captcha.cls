/*********************************************************************************************************************
 *  Name        : Test_PDFDocumentViewerCls_Captcha
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2016-10-12     
 *  Purpose     : Test Class for PDFDocumentViewerCls_Captcha Class
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    12-Oct-2016 Claude		   Created
**********************************************************************************************************************/
@isTest(seeAllData = false)
private class Test_PDFDocumentViewerCls_Captcha {
	
	@testSetup static void setTestData() {
    	
    	/**
    	 * NOTE: The listed keys are test keys,
    	 *		 and are not supposed to be used in
    	 *		 Production. They are not used in 
    	 *		 related process
    	 */
    	Google_Captcha_Keys__c testKeys = new Google_Captcha_Keys__c();
		
		testKeys.Name = 'Letter Authentication';
		testKeys.Private_Key__c = 'orba+MreweNaDFmqtkTYoz8XrsGCDK3lnKylk8WCTeI=';
		testKeys.Public_Key__c = 'orba+MreweNaDFmqtkTYoz8XrsGCDK3lnKylk8WCTeI=';
		
		insert testKeys;
	
	}

    static testMethod void myUnitTest() {
    	
    	/* Prepare the required SR Data */
    	List<CountryCodes__c> countryCodesTestList = Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
    	List<Lookup__c> testLookups = Test_CC_FitOutCustomCode_DataFactory.getTestLookupRecords();
    	
    	insert countryCodesTestList;
    	insert testLookups;
    	
    	/* Create a test SR */
    	Service_Request__c testRequest = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
    	
    	insert testRequest;
        
        /* Create a test SR Doc */
        SR_Doc__c testSampleDoc = Test_CC_FitOutCustomCode_DataFactory.getTestSrDoc('Sample Document',testRequest.Id);
        
        testSampleDoc.Sys_IsGenerated_Doc__c = false;
        insert testSampleDoc;
        
        /* Create an attachment, and add it to the SR Doc */
        Attachment testAttachment = Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Test Name','Test Body');
        testAttachment.ParentId = testRequest.Id;
        
        insert testAttachment;
        
        testSampleDoc.Doc_ID__c = testAttachment.Id;
        
        update testSampleDoc;
        
        Test.startTest();
        
        /* Get the DOC Number needed for the search */
        String testDocNum = [SELECT Id, Document_No_Id__c FROM SR_Doc__c WHERE Id = :testSampleDoc.Id].Document_No_Id__c;
        
        /* Set the test page */
        Test.setCurrentPage(Page.PDFDocumentViewer_Captcha);
        
        /* Set the DOC Number in the GET Parameters */
        apexpages.currentPage().getparameters().put('ref',testDocNum);
        
        /* Create the class instance */
        PDFDocumentViewerCls_Captcha testClsInst = new PDFDocumentViewerCls_Captcha();
        
        /* Display the token */
        System.debug(testClsInst.publicKey);
        System.debug(testClsInst.captchaError);
        System.debug(testClsInst.challenge);
               
        /* Simulate a recaptcha challenge */
        testClsInst.validateCaptcha();
        
        /* Laod the document */
        testClsInst.loadDocument(testDocNum);
        
        System.debug(testClsInst.response);
        System.debug(testClsInst.verified);
        System.debug(testClsInst.success);
        
        testClsInst.viewDocument(); // View the document
        
        /* Display the remaining variables */
        System.debug(testClsInst.searchDoc);
        System.debug(testClsInst.AttachValBase64);
        System.debug(testClsInst.AttachBlobVal);
        System.debug(testClsInst.AtchContentType);
        System.debug(testClsInst.blobError);
        System.debug(testClsInst.isCaptchaEnabled);
        System.debug(testClsInst.debugMessage);
        
        Test.stopTest();
        
    }
}