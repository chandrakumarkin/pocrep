/*
    Author      : Merul Shah
    Date        : 10-March-2020
    Description : This updates the contact OB_Block_Homepage__c

    v1.1          Abbas     13-02-2021     update valid gender value on Contact from SR (#13204 & #13205)
    --------------------------------------------------------------------------------------
*/


global without sharing class CC_UpdateContactOnApporval implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp)
    {
        system.debug('@@@@@@@@@ entered into CC_UpdateContactOnApporvalxxxxx');
        string strResult = 'Success';
        system.debug('@@@@@@@@@222222 entered into CC_UpdateContactOnApporval');
        if(stp!=null && stp.HexaBPM__SR__c!=null){
            system.debug('@@@@@@@@@ddddd entered into CC_UpdateContactOnApporval');
            try{
              
                if(stp.HexaBPM__SR__r.HexaBPM__Contact__c!=null){
                  
                    
                    Contact con = new Contact( id = stp.HexaBPM__SR__r.HexaBPM__Contact__c);
                    con.OB_Block_Homepage__c = false;
                    
                    con.Does_the_individual_reside_in_the_UAE__c     = stp.HexaBPM__SR__r.Are_You_Resident_Of_UAE__c;
                    con.Does_the_individual_have_a_valid_UAE_res__c  = stp.HexaBPM__SR__r.Valid_UAE_residence_visa__c;
                    con.Address_Line_1_HC__c                         = stp.HexaBPM__SR__r.Address_Line_1__c;
                    con.Address_Line_2_HC__c                         = stp.HexaBPM__SR__r.Address_Line_2__c;
                    con.CITY1__c                     = stp.HexaBPM__SR__r.City_Town__c;
                    con.Emirate_State_Province__c    = stp.HexaBPM__SR__r.State_Province_Region__c;
                    con.COUNTRY_ENGLISH2__c          = stp.HexaBPM__SR__r.Country__c;
                    con.PO_Box__c                    = stp.HexaBPM__SR__r.Po_Box_Postal_Code__c;

                    //v1.1
                    String gender = ( String.isNotBlank(stp.HexaBPM__SR__r.Gender__c) && stp.HexaBPM__SR__r.Gender__c == 'M' ? 'Male' : String.isNotBlank(stp.HexaBPM__SR__r.Gender__c) && stp.HexaBPM__SR__r.Gender__c == 'F' ? 'Female' : '');
                    System.debug('ZZZ CC_UpdateContactOnApporval.cls->EvaluateCustomCode_M-->gender-->'+gender);
                    con.Gender__c  = gender; 

                    update con;
                }

               

            }catch(DMLException e){
                /*
                    string DMLError = e.getdmlMessage(0)+'';
                    if(DMLError==null){
                        DMLError = e.getMessage()+'';
                    }
                    strResult = DMLError;
             	*/
				strResult =  e.getdmlMessage(0)+'';   
            }
        }
       
        return strResult;
    }   
}