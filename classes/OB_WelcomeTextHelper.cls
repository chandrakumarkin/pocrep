/*
    Author      : Durga Prasad
    Date        : 09-Oct-2019
    Description : Class to get the user details and populate in community welcome email
    --------------------------------------------------------------------------------------
*/
public without sharing class OB_WelcomeTextHelper{
    public string UID{get;set;}
    public OB_WelcomeTextHelper(){}
    public string getwelcomeText(){
        string EmailBodyString = '';
        system.debug('@@@@@@@ UId '+UId);
        
        list<User> usr = [Select OB_Registration_Source__c,Username,Contact_Owner_Phone__c,Contact_Owner_Email__c FROM User where Id=:UId and ContactId!=null];
        system.debug('@@@@@@@ usr '+usr);
        
        if(usr!=null && usr.size()>0 && usr[0].OB_Registration_Source__c!=null)
        {
             string UserSource = usr[0].OB_Registration_Source__c;
             string EmailContent = '';
             for(OB_Welcome_Email_Text__mdt mdt:[Select Email_Content__c from OB_Welcome_Email_Text__mdt where User_Registration_Type__c=:UserSource]){
                 EmailContent = mdt.Email_Content__c;
             }
             EmailContent = EmailContent.replace('{!recipient.Username}',(String.isNotBlank(usr[0].UserName)? usr[0].UserName : ''));
             EmailContent = EmailContent.replace('{!recipient.Contact_Owner_Phone__c}',(String.isNotBlank(usr[0].Contact_Owner_Phone__c) ? usr[0].Contact_Owner_Phone__c : ''));
             EmailContent = EmailContent.replace('{!recipient.Contact_Owner_Email__c}',(String.isNotBlank(usr[0].Contact_Owner_Email__c)? usr[0].Contact_Owner_Email__c : ''));
             
            
             EmailBodyString = EmailContent;
        }
        return EmailBodyString;
    }
}