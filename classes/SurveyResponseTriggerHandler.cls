public without sharing class SurveyResponseTriggerHandler{
    /*
        Method Name :   assigntToSurvey
        Description :   Method to relate the survey response to the related survey
    */
    public static void assigntToSurvey(List<Survey_Response__c> surveyResponselst){
        Set<String> sessionids = new Set<String>();
        for(Survey_Response__c objSurRes : surveyResponselst){
            if(objSurRes.Session_ID__c != null && objSurRes.Session_ID__c != ''){
                sessionids.add(objSurRes.Session_ID__c);
            }
        }
        
        Map<String,Id> surveySessionMap = new Map<String,Id>();
        for(Survey__c objSurvey : [SELECT Id, Session_ID__c FROM Survey__c WHERE Session_ID__c IN :sessionids]){
            surveySessionMap.put(objSurvey.Session_ID__c,objSurvey.Id);
        }
        
        for(Survey_Response__c objSurRes : surveyResponselst){
            if(objSurRes.Session_ID__c != null && objSurRes.Session_ID__c != ''){
                if(surveySessionMap.containskey(objSurRes.Session_ID__c)){
                    objSurRes.Survey__c = surveySessionMap.get(objSurRes.Session_ID__c);
                }
            }
        }
    }
    /*
        Method Name :   CreateCase
        Description :   Method to create the case related to the survey response
    */
    public static void CreateCase(List<Survey_Response__c> surveyResponselst){
        List<Case> cases= new List<Case>();
        for(Survey_Response__c res: surveyResponselst){
            if (res.Comments__c != null && res.Comments__c != ''){
                //res.Response__c=='1 - Poor'||res.Response__c=='2 - Below Average'
                Case objCase = new Case();
                objCase.Origin='Mystery Shopping';
                objCase.GN_Type_of_Feedback__c='Complaint';
                objCase.Type='Parking';
                objCase.Description = res.Comments__c;//'Question: '+ res.Question__c + '\r\n' + res.Sub_Question__c + '\r\nResponse: ' + res.Response__c;
                objCase.Subject = res.Question__c;
                objCase.Survey_Response__c=res.id;
                cases.add(objCase);
            }
        }
        if(!cases.isEmpty()){
            insert cases;
        }
    }
}