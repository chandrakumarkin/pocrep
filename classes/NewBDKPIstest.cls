@istest
class NewBDKPIstest {

   static testmethod void test()
    {
   Test.startTest();

      
        List<UserRole> r = [select id,Name from UserRole where DeveloperName='Business_Development'];
        Id roleId = r[0].Id;
        
        User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'BD Team'].Id,
            LastName = 'last',
            Email = 'puser000@test.com',
            Username = 'puser000@test.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'Asia/Dubai',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            UserRoleId = roleId
        );
        
        insert objUser;
        
        System.runAs(objUser) {
           
            
            Account a = new Account();
            
            a.Name = 'testScheduledApexFromTestMethod';
            a.ROC_Status__c= 'Active';
            a.Total_Meeting_Count__c=0;
            a.Total_Interaction_Count__c=0;
            a.License_Activity_Details__c ='text';
            a.Hosting_Company__c=null;
            
            insert a;
            
            Account a1 = new Account();
            
            a1.Name = 'testScheduledApexFromTestMethod';
            //a1.ROC_Status__c= 'Active';
            a1.Total_Meeting_Count__c=0;
            a1.Total_Interaction_Count__c=0;
            a1.License_Activity_Details__c ='text';
            a1.Same_Management__c = a.id;
            a1.Hosting_Company__c=a.id;
            
            insert a1;
            
            contact con = new contact();
            con.accountId= a.id;
            con.firstName = 'test';
            con.LastName='faruk';
            
            insert con;
            
            contact con1 = new contact();
            con1.accountId= a1.id;
            con1.firstName = 'test';
            con1.LastName='faruk';
            
            //insert con1;
            
            License_Activity_Master__c objLAM = new License_Activity_Master__c();
             objLAM.Name = 'Test DIFC Activity';
             objLAM.Sector_Classification__c = 'Authorised Market Institution';
             objLAM.Type__c = 'License Activity';
             objLAM.Sys_Code__c = '048';
             objLAM.Enabled__c = true;
             insert objLAM;
            
            License_Activity__c objLA = new License_Activity__c();
            objLA.Account__c = a.Id;
            objLA.Activity__c = objLAM.Id;
            objLA.Sector_Classification__c = 'Authorised Market Institution';        
            insert objLA;
            
            Relationship__c rel = new Relationship__c();
            rel.Relationship_Type__c ='Has Affiliates with Same Management';
            rel.Subject_Account__c= a.id;
            rel.Object_Account__c = a1.id;
            //rel.Object_Contact__c= con1.id;
            rel.Active__c = true;
            insert rel;
        
            List<Task> tskLIst = new List<Task>();
            
            Task t = new Task();
            t.OwnerId = UserInfo.getUserId();
            t.Subject='Donni';
            t.Status='Not Started';
            t.Priority='Normal';
            t.Whatid=a.id;
            t.Date_Time_From__c =system.now(); 
            t.Date_Time_To__c = system.now();
            t.recordTypeID = Schema.sObjectType.task.getRecordTypeInfosByName().get('Meeting').getRecordTypeId();
            
            tskLIst.add(t);
            
            
            
            Task t1 = new Task();
            t1.OwnerId = UserInfo.getUserId();
            t1.Subject='Donni';
            t1.Status='Not Started';
            t1.Priority='Normal';
            t1.Whatid=a.id;
            t1.Date_Time_From__c =system.now(); 
            t1.Date_Time_To__c = system.now();
            t1.recordTypeID = Schema.sObjectType.task.getRecordTypeInfosByName().get('Calls').getRecordTypeId();
            
            tskLIst.add(t1);
            
            insert tskLIst;
            
            List<Event> eLst = new List<Event>();
            Event e =  new Event();
            e.Whatid=a.id;
            e.whoid=con.id;
            e.ActivityDate=date.today();
            e.ActivityDateTime = system.now();
            e.Date_Time_From__c=system.now();
            e.Date_Time_To__c=system.now();
            e.subject='Event';
             e.OwnerId = UserInfo.getUserId();
            e.DurationInMinutes=30;
            eLst.add(e);
            
            Event e1 =  new Event();
            e1.Whatid=a.id;
            e1.whoid=con.id;
            e1.ActivityDate=date.today();
            e1.ActivityDateTime = system.now();
            e1.Date_Time_From__c=system.now();
            e1.Date_Time_To__c=system.now();
            e1.subject='Event';
            e1.DurationInMinutes=30;
            e1.OwnerId = UserInfo.getUserId();
            eLst.add(e1);
            
            insert eLst;  
        }
      
   
//Same_Management__c
      NewBDKPIs batchSch=new NewBDKPIs ();
String sch='0 5 2 * * ?';
//System.schedule(String jobName, String cronExp, APEX_OBJECT schedulable);
System.schedule('Batch Schedule', sch , batchSch);



UpdateBDKPIs.ListofTask([select id,Accountid,ActivityDate,Date_Time_From__c,Date_Time_To__c,subject,Recordtype.Name from task where   CreatedDate=this_Year]);
UpdateBDKPIs.ListofEvents([select id,Accountid,ActivityDate,Date_Time_From__c,Date_Time_To__c,subject,Recordtype.Name from Event where  CreatedDate=this_Year]);
    
      

   Test.stopTest();


   }
}