/*****************************************************************************************************************************
Author      :   Saima Hidayat
Date        :   23-02-2015
Description :   This class contains the Custom codes for the Government Services processes. PO Box service code.
--------------------------------------------------------------------------------------------------------------------------
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date            Updated By      Description
--------------------------------------------------------------------------------------------------------------------------             
V1.1    27-04-2015      Kaavya          Adding custom code for updating Sponsor Open family file
V1.2    17-10-2015      Shabbir         Custom code to cancel PO Box
V1.3    22-12-2015      Kaavya          Modifying logic for PO Box according to Moosa ticket 1900
V1.4    07-01-2016      Shabbir         Added logic for GSO Authroized Signatory
V1.5    06-03-2016      Ravi            Added the logic to update end date on PO renewal for customer PO Box record as per # 2388
V1.6    03-04-2016      Swati           'Upgrade to express after medical fitness Test' only if the related SR step 'Medical Test Completed' is closed. #2724
V1.6    09-05-2016      Ravi            While querying the Steps for Parent SR, in where condition "and SR__r.linked_sr__c!=null"" was mentioned, For Parent SR there wont be any Linked SR      
V1.7    12-05-2016      Ravi            Added logic to update the PO Box Status based on the Type of PO Box Type as per #2919
v1.8    17-05-2016      Shabbir         Added logic for contact matching with nationality and contact update for GSO Auth Signatory 
v1.9    06-09-2016      Swati           generate documents for embassy letter  
v1.10   05-12-2016      Swati           as per ticket 851      
V1.11   13-03-2017      Claude          Added method to transfer PO BOX to new customer (3022)  
V1.12   10-07-2017      Sravan          Added a method to update the contact arabic fields for secupass integration  Tkt # 4089
V1.13   16-07-2017      Sravan          Added a method to copy Barcode to Coverpage on SR as per Tkt # 4038
V1.14   12-Dec-2018     Mudasir         Updated the barcode url and moved the same to Label-Domain enabled forced us to change the hardcode URL
V1.15   01-05-2019      Azfer           Updated the logic for the duration calculation based on the duration selected in the duration field.
V2.0	26-11-2019		Mudasir			#7089 - Added new method updateMobileNumber which is used to update the mobile number from Step to Service request
V2.1	26-11-2019		Mudasir			#7089 - Added new method checkIfShipmentToBeCreated which is used to creat the shipment if required
V2.2	10-02-2020		Mudasir			Added an exception code for the renewal of the PO BOX where i have introduced excemptPOBOXCompanyId label to provide the exception for a given company 
*****************************************************************************************************************************/
public without sharing class CC_GSCodeCls {

    /**
     * V1.11
     * Transfers the PO Box
     * to the new customer
     * @params    stp      The step record
     * @return    strResult  The result of the transaction
     */
    public static String transferPOBox(Step__c stp) {

        try {

            if (stp != null && stp.Id != null && stp.SR__c != null) {

                list < Service_Request__c > lstSR = getServiceRequest(stp.SR__c);

                list < Customer_PO_Box__c > custPObox = getCustomerPoBox(lstSR[0].PO_Box_Master__c, lstSR[0].Customer__c);

                if (!custPObox.isEmpty() && !lstSr.isEmpty()) {

                    Customer_PO_Box__c custpob = new Customer_PO_Box__c(id = custPObox[0].id);

                    custpob.Client__c = lstSR[0].Company_Name__c;

                    PO_Box_Master__c pob = new PO_Box_Master__c(id = lstSR[0].PO_Box_Master__c);

                    pob.Current_Owner__c = lstSR[0].Company_Name__c;

                    update custpob;

                    update pob;

                }

            }

            return 'Success';

        } catch (Exception e) { return 'Error at line: ' + e.getLineNumber() + '. Message: ' + e.getMessage(); }


    }

    private static list < Service_Request__c > getServiceRequest(String srId) {

        return [select id,
            Company_Name__c, // V1.11 - Transfers will be using contractor field to store the account ID of the new account
            finalizeAmendmentFlg__c,
            PO_Box_Master__c,
            Internal_Status_Name__c,
            Customer__c from Service_Request__c
            where Id =: srId
        ];
    }

    private static list < Customer_Po_Box__c > getCustomerPoBox(String poBoxMaster, String clientId) {

        return [Select id,
            Status__c,
            Client__c,
            Start_Date__c,
            End_Date__c,
            PO_Box_Master__c
            from Customer_PO_Box__c
            where PO_Box_Master__c =: poBoxMaster and
            Client__c =: clientId
        ];

    }

    public static string Assign_POBox(Step__c stp) {
        string strResult = 'Success';
        system.debug('stp===>' + stp);
        if (stp != null && stp.Id != null && stp.SR__c != null) {
            try {
                list < Service_Request__c > lstSR = [select id, Duration__c, finalizeAmendmentFlg__c, PO_Box_Master__c, Internal_Status_Name__c, Customer__c from Service_Request__c where Id =: stp.SR__c];
                Customer_PO_Box__c custpob = new Customer_PO_Box__c();
                custpob.Client__c = lstSR[0].Customer__c;
                custpob.Status__c = 'Booked';
                custpob.Start_Date__c = system.today();
                /**Start v1.3  Added by Kaavya *****/
                //custpob.End_Date__c = date.newInstance(system.today().year(),12,31);//system.today().addYears(1);

                //if (system.today().month() != 12)
                //    custpob.End_Date__c = date.newInstance(system.today().year(), 12, 31);
                //else
                //    custpob.End_Date__c = date.newInstance(system.today().year() + 1, 12, 31);
                /**End v1.3  *****/

                //v1.15 Code Start here
                if( lstSR[0].Duration__c != null && lstSR[0].Duration__c != '' ){
                    Integer Year = integer.valueof(lstSR[0].Duration__c.left(1));
                    Year = Year - 1 ;
                    if (system.today().month() != 12 && Year == 0) { custpob.End_Date__c = date.newInstance(system.today().year(), 12, 31); }
                    else if( Year != 0 ){ custpob.End_Date__c = date.newInstance(system.today().year(), 12, 31).addYears(Year); }
                }else{
                    return 'Duration not specified for the PO Box service';
                }
                //v1.15 Ends

                custpob.PO_Box_Master__c = lstSR[0].PO_Box_Master__c;

                PO_Box_Master__c pob = new PO_Box_Master__c(id = lstSR[0].PO_Box_Master__c);
                pob.Status__c = 'Utilized';
                pob.Current_Owner__c = lstSR[0].Customer__c;

                insert custpob;
                update pob;

            } catch (DMLException e) {
                string DMLError = e.getdmlMessage(0) + '';
                if (DMLError == null) { DMLError = e.getMessage() + ''; }
                strResult = DMLError;
                return strResult;

            }
        }
        return 'Success';
    }

    public static string Renew_POBox(Step__c stp) {
        string strResult = 'Success';
        system.debug('stp===>' + stp);
        if (stp != null && stp.Id != null && stp.SR__c != null) {
            try {
                list < Service_Request__c > lstSR = [select id, Duration__c, finalizeAmendmentFlg__c, PO_Box_Master__c, Internal_Status_Name__c, Customer__c from Service_Request__c where Id =: stp.SR__c];
                list < Customer_PO_Box__c > custPObox = [Select id, Status__c, Client__c, Start_Date__c, End_Date__c, PO_Box_Master__c from Customer_PO_Box__c where PO_Box_Master__c =: lstSR[0].PO_Box_Master__c and Client__c =: lstSR[0].Customer__c and Status__c = 'Booked'];
                if (custPObox.size() > 0 && custPObox != null) {
                    system.debug('Inside 1st if===>');
                    Customer_PO_Box__c custpob = new Customer_PO_Box__c(id = custPObox[0].id);
                    custpob.Client__c = lstSR[0].Customer__c;
                    //custpob.Status__c = 'Booked';
                    /**Start v1.3  Added by Kaavya ****
                    custpob.Start_Date__c = system.today();//custPObox[0].Start_Date__c.addYears(1);
                    custpob.End_Date__c = date.newInstance(system.today().year(),12,31);//custpob.Start_Date__c.addYears(1);
                    */
                    //system.debug('Today===>' + system.today().month());
                    //if (system.today().month() != 12) {
                    //    system.debug('Inside 2nd if===>');
                    //    if (custPObox[0].End_Date__c != null) {
                    //        if (custPObox[0].End_Date__c.year() >= system.today().year())
                    //            return 'Renewal cannot be approved until Dec 1st of this year';
                    //        else // V1.5 - added
                    //            custpob.End_Date__c = date.newInstance(system.today().year(), 12, 31);
                    //    } else
                    //        custpob.End_Date__c = date.newInstance(system.today().year(), 12, 31);
                    //} else {
                    //    system.debug('Inside else if===>');
                    //    if (custPObox[0].End_Date__c != null) {
                    //        if (custPObox[0].End_Date__c.year() > system.today().year())
                    //            return 'Renewal cannot be approved until Dec 1st of ' + string.valueof(custPObox[0].End_Date__c.year());
                    //        else if (custPObox[0].End_Date__c.year() == system.today().year())
                    //            custpob.End_Date__c = date.newInstance(system.today().year() + 1, 12, 31);
                    //        else
                    //            custpob.End_Date__c = custPObox[0].End_Date__c.addYears(1);
                    //    } else
                    //        custpob.End_Date__c = custPObox[0].End_Date__c.addYears(1);
                    //}
                    ///**End v1.3  *****/

                    
                    //v1.15 Code Start here
                    if( lstSR[0].Duration__c != null && lstSR[0].Duration__c != '' ){
                        Integer Year = integer.valueof(lstSR[0].Duration__c.left(1));
                        Year = Year - 1 ;

                        if (system.today().month() != 12 && Year == 0) {
                            if (custPObox[0].End_Date__c != null) {
                            	//V2.2 - Added code to have an excepion for the approval of the renewal of the PO BOX
                                if (custPObox[0].End_Date__c.year() >= system.today().year() && system.label.excemptPOBOXCompanyId !=custPObox[0].Client__c){
                                    return 'Renewal cannot be approved until Dec 1st of this year';
                                }
                                else{
                                    // V1.5 - added
                                    custpob.End_Date__c = date.newInstance(system.today().year(), 12, 31);
                                } 
                            } else{
                                custpob.End_Date__c = date.newInstance(system.today().year(), 12, 31);
                            }
                        } else if( Year != 0 ){
                            if (custPObox[0].End_Date__c != null) {
                                if (custPObox[0].End_Date__c.year() > system.today().year()){
                                    return 'Renewal cannot be approved until Dec 1st of ' + string.valueof(custPObox[0].End_Date__c.year());
                                }
                                //else if (custPObox[0].End_Date__c.year() == system.today().year())
                                //custpob.End_Date__c = date.newInstance(system.today().year(), 12, 31).addYears( Year );
                                else{
                                    //custpob.End_Date__c = custPObox[0].End_Date__c.addYears( Year );
                                    custpob.End_Date__c = date.newInstance(system.today().year(), 12, 31).addYears( Year );
                                }
                            } else{
                                //custpob.End_Date__c = custPObox[0].End_Date__c.addYears( Year );
                                custpob.End_Date__c = date.newInstance(system.today().year(), 12, 31).addYears( Year );   
                            }
                        }
                    
                    }else{
                        return 'Duration not specified for the PO Box service';
                    }
                    //v1.15 Ends

                    update custpob;
                }
            } catch (DMLException e) { string DMLError = e.getdmlMessage(0) + '';
                if (DMLError == null) { DMLError = e.getMessage() + ''; }
                strResult = DMLError;
                return strResult;
            }
        }
        return 'Success';
    }

    // V1.1 Adding custom code for updating Sponsor Open family file
    public static string Sponsor_OpenFamilyFile(Step__c stp) {
        string strResult = 'Success';
        system.debug('stp===>' + stp);
        if (stp != null && stp.Id != null && stp.SR__c != null) {
            try {

                list < Service_Request__c > lstSR = [select id, Sponsor__c, finalizeAmendmentFlg__c, PO_Box_Master__c, Internal_Status_Name__c, Customer__c from Service_Request__c where Id =: stp.SR__c];

                List < Contact > Sponsorlist = [select id, Paid_Family_File__c from Contact where id =: lstSR[0].Sponsor__c];
                if (!Sponsorlist.isEmpty() && Sponsorlist.size() > 0)
                    Sponsorlist[0].Paid_Family_File__c = true;
                update Sponsorlist;


            } catch (DMLException e) { string DMLError = e.getdmlMessage(0) + '';
                if (DMLError == null) { DMLError = e.getMessage() + ''; }
                strResult = DMLError;
                return strResult;
            }
        }
        return 'Success';
    }

    //v1.2
    public static string Cancel_POBox(Step__c stp) {
        string strResult = 'Success';
        system.debug('stp===>' + stp);
        if (stp != null && stp.Id != null && stp.SR__c != null) {
            try {
                list < Service_Request__c > lstSR = [select id, finalizeAmendmentFlg__c, PO_Box_Master__c, Internal_Status_Name__c, Customer__c from Service_Request__c where Id =: stp.SR__c];
                list < Customer_PO_Box__c > custPObox = [Select id, Status__c, Client__c, Start_Date__c, End_Date__c, PO_Box_Master__c from Customer_PO_Box__c where PO_Box_Master__c =: lstSR[0].PO_Box_Master__c and Client__c =: lstSR[0].Customer__c and(Status__c = 'Registered'
                    OR Status__c = 'Booked')];
                if (lstSR != null) {
                    PO_Box_Master__c pob = new PO_Box_Master__c(id = lstSR[0].PO_Box_Master__c);
                    pob.Status__c = 'Available';
                    pob.Current_Owner__c = null;
                    update pob;
                }
                if (custPObox != null && custPObox.size() > 0) {
                    Customer_PO_Box__c custpob = new Customer_PO_Box__c(id = custPObox[0].id);
                    custpob.End_Date__c = system.today();
                    update custpob;
                }
            } catch (DMLException e) { string DMLError = e.getdmlMessage(0) + '';
                if (DMLError == null) { DMLError = e.getMessage() + ''; }
                strResult = DMLError;
                return strResult;
            }
        }
        return 'Success';
    }
    //v1.4
    public static string CreateGSOAuthorizedSignatory(Step__c stp) {

        string strResult = 'Success';
        try {
            // Local Varaiables Initilization
            set < string > setPassportNum = new set < string > ();
            set < string > setNationality = new set < string > ();
            string contactRecordTypeId;
            string relationshipType = 'Is Authorized Signatory for';
            string relationshipGroup = 'GS';
            list < Relationship__c > lstInsRel = new list < Relationship__c > ();
            list < Relationship__c > lstUpdateRel = new list < Relationship__c > ();
            list < Contact > lstConInsert = new list < Contact > ();
            list < Contact > lstAllContact = new list < Contact > ();
            map < string, Contact > mapUniqueKeyWithContactObj = new map < string, Contact > ();
            map < string, Relationship__c > mapContactWithRelationship = new map < string, Relationship__c > ();
            map < string, string > mapPassportNumWithAuthorizedSignStatus = new map < string, string > ();

            list < Service_Request__c > listSR = [Select Id, Customer__c from Service_Request__c where Id =: stp.SR__c];
            if (listSR != null && !listSR.isEmpty()) {

                // get GS contact record type
                for (RecordType objRT: [select Id, Name from RecordType where DeveloperName = 'GS_Contact'
                        AND SobjectType = 'Contact'
                        AND IsActive = true
                    ])
                    contactRecordTypeId = objRT.Id;

                // get passport numbers of GSO authorized signatory from amendment
                list < Amendment__c > lstAmnd = [select id, Passport_No__c, Nationality_list__c, Status__c from Amendment__c where ServiceRequest__c =: stp.SR__c];
                for (Amendment__c amnd: lstAmnd) {
                    if (amnd.Passport_No__c != null && amnd.Nationality_list__c != null)
                        setPassportNum.add(amnd.Passport_No__c);
                    setNationality.add(amnd.Nationality_list__c);
                    mapPassportNumWithAuthorizedSignStatus.put(amnd.Passport_No__c.toLowerCase() + amnd.Nationality_list__c.toLowerCase(), amnd.Status__c);
                }

                // match passport number with existing GS type of contacts
                // v1.8 - Added nationality in SOQL
                if (setPassportNum != null && setNationality != null && setPassportNum.size() > 0 && setNationality.size() > 0) {
                    for (Contact objCon: [select id, Passport_No__c, Nationality__c, Sys_Relationship_Type__c from Contact where Passport_No__c IN: setPassportNum and Nationality__c IN: setNationality and RecordType.DeveloperName = 'GS_Contact']) {
                        mapUniqueKeyWithContactObj.put(objCon.Passport_No__c.toLowerCase() + objCon.Nationality__c.toLowerCase(), objCon); // v1.8 - changed to include nationality 
                    }
                }
                // Check existing relationship
                for (Relationship__c rel: [Select Id, Object_Contact__c, Subject_Account__c, Relationship_Type__c, Relationship_Passport__c, Active__c, Relationship_Nationality__c
                        from Relationship__c where Relationship_Type__c =: relationshipType
                        and Relationship_Passport__c IN: setPassportNum and Relationship_Nationality__c IN: setNationality
                        and Subject_Account__c =: listSR[0].Customer__c and Relationship_Group__c =: relationshipGroup
                    ]) {
                    mapContactWithRelationship.put(rel.Relationship_Passport__c.toLowerCase() + rel.Relationship_Nationality__c.toLowerCase(), rel); // v1.8 - changed to include nationality                             
                }

                // GS Contact creation
                for (Amendment__c objAmnd: [select id, Relationship_Type__c, Sys_Secondary__c, Status__c, Contact__c, Family_Name__c, Passport_No__c, Nationality_list__c, Given_Name__c,
                        Job_Title__c, Data_Center_Access_ID__c, Phone__c, Person_Email__c, Mobile__c from Amendment__c where
                        ServiceRequest__c =: stp.SR__c
                    ]) {
                    if (objAmnd.Family_Name__c != null && objAmnd.Given_Name__c != '' && objAmnd.Passport_No__c != null && objAmnd.Nationality_list__c != null) {

                        if (!mapUniqueKeyWithContactObj.containsKey(objAmnd.Passport_No__c.toLowerCase() + objAmnd.Nationality_list__c.toLowerCase())) { //v1.8
                            Contact objCon = new Contact();
                            objCon.Passport_No__c = objAmnd.Passport_No__c;
                            objCon.Nationality__c = objAmnd.Nationality_list__c;
                            objCon.Country__c = 'United Arab Emirates';
                            objCon.RecordTypeId = contactRecordTypeId;
                            if (objAmnd.Family_Name__c != null)
                                objCon.FirstName = objAmnd.Family_Name__c;
                            if (objAmnd.Given_Name__c != null)
                                objCon.LastName = objAmnd.Given_Name__c;
                            if (objAmnd.Job_Title__c != null)
                                objCon.Occupation__c = objAmnd.Job_Title__c;
                            if (objAmnd.Phone__c != null)
                                objCon.Work_Phone__c = objAmnd.Phone__c;
                            if (objAmnd.Mobile__c != null)
                                objCon.MobilePhone = objAmnd.Mobile__c;
                            if (objAmnd.Person_Email__c != null)
                                objCon.Email = objAmnd.Person_Email__c;
                            objCon.Use_ZGS1_Auth_Group__c = true;

                            lstConInsert.add(objCon);
                        } else {
                            if (!mapUniqueKeyWithContactObj.isEmpty())
                                lstAllContact.add(mapUniqueKeyWithContactObj.get(objAmnd.Passport_No__c.toLowerCase() + objAmnd.Nationality_list__c.toLowerCase()));
                        }

                    }
                }

                if (!lstConInsert.isEmpty()) {
                    insert lstConInsert; // v1.8
                }
                system.debug('lstConUpsert ' + lstConInsert);

                for (Contact contact: lstConInsert) {
                    lstAllContact.add(contact);
                }

                // Relationship Creation
                for (Contact contact: lstAllContact) {
                    string uniqueKey = contact.Passport_No__c.toLowerCase() + contact.Nationality__c.toLowerCase();

                    if (mapContactWithRelationship.containsKey(uniqueKey)) {
                        Relationship__c RelGSUser = new Relationship__c(Id = mapContactWithRelationship.get(uniqueKey).Id);
                        if (mapPassportNumWithAuthorizedSignStatus.containsKey(uniqueKey) && mapPassportNumWithAuthorizedSignStatus.get(uniqueKey) == 'Remove') {
                            RelGSUser.Active__c = false;
                            RelGSUser.End_Date__c = Date.today();
                            lstUpdateRel.add(RelGSUser);
                        }

                    } else {
                        Relationship__c RelGSUser = new Relationship__c();
                        RelGSUser.Object_Contact__c = contact.Id;
                        RelGSUser.Subject_Account__c = listSR[0].Customer__c;
                        RelGSUser.Relationship_Type__c = relationshipType;
                        RelGSUser.Start_Date__c = Date.today();
                        RelGSUser.Relationship_Group__c = relationshipGroup;
                        RelGSUser.Use_ZGS1_Auth_Group_New__c = true; // v1.8 
                        if (mapPassportNumWithAuthorizedSignStatus.containsKey(uniqueKey) && mapPassportNumWithAuthorizedSignStatus.get(uniqueKey) == 'Remove') {
                            RelGSUser.Active__c = false;
                            RelGSUser.End_Date__c = Date.today();
                        } else {
                            RelGSUser.Active__c = true;
                        }

                        lstInsRel.add(RelGSUser);

                    }

                }

                if (!lstInsRel.isEmpty()) {
                    insert lstInsRel;
                }
                system.debug('lstInsRel ' + lstInsRel);

                if (!lstUpdateRel.isEmpty()) { update lstUpdateRel; }
                system.debug('lstInsRel ' + lstInsRel);

            }

        } catch (Exception e) {
            strResult = e.getMessage();
            insert LogDetails.CreateLog(null, 'GS - Auth Signatory : Custom Code', 'Line Number : ' + e.getLineNumber() + '\nException is : ' + strResult);
            return strResult;
        }

        return strResult;
    }


    //v1.6
    public static string allowExpressMedicalTest(Step__c stp) {
        string strResult = 'Success';
        if (stp != null) {
            Medical_Fitness_Step_Name__c settings = Medical_Fitness_Step_Name__c.getValues('Medical Fitness Test Completed');
            String stepName = settings.Step_Name__c;
            String stepStatus = settings.Step_Status__c;
            list < Step__c > listofStep = [Select Id from Step__c where SR__c =: stp.SR__r.linked_sr__c and step_name__c =: stepName and Status_Code__c =: stepStatus and id != null and sr__c != null]; //V1.6 a. removed the "and SR__r.linked_sr__c!=null" from the query, Linked SR will be blank for all the time       
            if (listofStep != null && !listofStep.isEmpty()) {
                strResult = 'Success';
            } else {
                strResult = 'Medical Fitness Test not Completed for related SR, Express service cannot be submitted.';
            }
        }
        return strResult;
    }
    //End

    //V1.7
    public static string UpdatePOBoxStatus(Step__c objStep) {
        string res = 'Success';
        if (objStep != null) {
            try {
                Service_Request__c objSR = objStep.SR__r;
                if (objSR.Service_Category__c == 'New' && ((objStep.Step_Name__c == 'Front Desk Review' && objSR.Submitted_Date__c == system.today()) || (objStep.Step_Status__c == 'Cancellation Approved' || objStep.Step_Status__c == 'Rejected'))) {
                    PO_Box_Master__c objPOBox = new PO_Box_Master__c(Id = objSR.PO_Box_Master__c);
                    //If SR is Submitted then Status will be changed to 'Blocked' and when FO rejected the then system will change the status back to 'Available'
                    objPOBox.Status__c = (objStep.Step_Status__c == 'Cancellation Approved' || objStep.Step_Status__c == 'Rejected') ? 'Available' : 'Blocked';
                    update objPOBox;
                }
            } catch (Exception ex) {
                insert LogDetails.CreateLog(null, 'PO Box Status Update', 'Line # ' + ex.getLineNumber() + '\n' + ex.getMessage());
            }
        } else {
            res = 'Please contact system administrator';
        }
        return res;
    }

    //v1.9
    public static string generateEmbassyLetter(step__c objStep) {
        string res = 'Success';
        try {
            list < service_request__c > tempList = [select id, Country__c, Contact__r.gender__c, (select id, Letter_Format__c, Permanent_Native_Country__c, Letter_To_Consulate_Embassy__c,
                    Purpose_of_Visit__c, Type_of_Visit_Visa__c, emirate__c from Amendments__r)
                from service_request__c
                where id =: objStep.SR__c
                and record_type_name__c =: 'Consulate_or_Embassy_Letter'
            ];
            list < SR_Doc__c > listToInsert = new list < SR_Doc__c > ();
            if (tempList != null && !tempList.isEmpty()) {
                service_request__c obj = new service_request__c();
                obj = tempList[0];
                if (string.isNotBlank(obj.Country__c)) {
                    map < string, string > mapNationalitys = new map < string, string > ();
                    for (Lookup__c objLp: [select Id, Name from Lookup__c where Type__c = 'Nationality'
                            AND Name IN: obj.Country__c.split(';')
                        ]) {
                        mapNationalitys.put(objLp.Name, objLp.Id);
                    }
                    system.debug('obj is : ' + obj);
                    map < string, SR_Template_Docs__c > templateDocs = new map < string, SR_Template_Docs__c > ();
                    for (SR_Template_Docs__c temp: [select id, Document_Master__r.LetterTemplate__c, Document_Master__c, Group_No__c, Optional__c,
                            Generate_Document__c, Document_Description_External__c, Document_Description__c,
                            name, DMS_Document_Index__c, Document_Master_Code__c
                            from SR_Template_Docs__c
                            where SR_Template__r.SR_Template_Code__c =: 'Consulate_or_Embassy_Letter'
                        ]) {
                        templateDocs.put(temp.Document_Master_Code__c, temp);
                    }

                    for (Amendment__c obj1: obj.Amendments__r) {
                        SR_Doc__c objSRDoc = new SR_Doc__c();
                        SR_Template_Docs__c objXX = new SR_Template_Docs__c();
                        objSRDoc.country__c = obj1.Permanent_Native_Country__c;
                        if (mapNationalitys != null && mapNationalitys.size() > 0) {
                            objSRDoc.Country_Lookup__c = mapNationalitys.get(objSRDoc.country__c);
                        }
                        objSRDoc.service_request__c = obj.id;
                        objSRDoc.Status__c = 'Generated';

                        if (templateDocs != null && templateDocs.size() > 0) {
                            if (obj1.Purpose_of_Visit__c == 'Business' && obj1.Letter_Format__c == 'Arabic' && obj.Contact__r.gender__c == 'Female') { objXX = templateDocs.get('AR Consulate/Embassy Letter - Business - Female'); } 
                            else if (obj1.Purpose_of_Visit__c == 'Business' && obj1.Letter_Format__c == 'Arabic' && obj.Contact__r.gender__c == 'Male') { objXX = templateDocs.get('AR Consulate/Embassy Letter - Business - Male'); } 
                            else if (obj1.Purpose_of_Visit__c == 'Personal' && obj1.Letter_Format__c == 'Arabic' && obj.Contact__r.gender__c == 'Female') { objXX = templateDocs.get('AR Consulate/Embassy Letter - Personal - Female'); } 
                            else if (obj1.Purpose_of_Visit__c == 'Personal' && obj1.Letter_Format__c == 'Arabic' && obj.Contact__r.gender__c == 'Male') { objXX = templateDocs.get('AR Consulate/Embassy Letter - Personal - Male'); } 
                            else if (obj1.Letter_Format__c == 'English') { objXX = templateDocs.get('EN Consulate/Embassy Letter'); }
                        }
                        objSRDoc.SR_Template_Doc__c = objXX.id;
                        objSRDoc.Letter_Template__c = objXX.Document_Master__r.LetterTemplate__c;
                        objSRDoc.Document_Master__c = objXX.Document_Master__c;
                        objSRDoc.Name = objXX.Document_Master_Code__c + '-' + objSRDoc.country__c;

                        objSRDoc.DMS_Document_Index__c = objXX.DMS_Document_Index__c;
                        objSRDoc.Group_No__c = objXX.Group_No__c;
                        objSRDoc.Is_Not_Required__c = objXX.Optional__c;
                        objSRDoc.Generate_Document__c = objXX.Generate_Document__c;
                        objSRDoc.Document_Description_External__c = objXX.Document_Description_External__c;
                        objSRDoc.Amendment__c = obj1.id;
                        if (objSRDoc.Document_Description_External__c == null) {
                            objSRDoc.Document_Description_External__c = objXX.Document_Description__c;
                        }
                        objSRDoc.Sys_IsGenerated_Doc__c = objXX.Generate_Document__c;
                        objSRDoc.Step__c = objStep.id;
                        listToInsert.add(objSRDoc);
                    }
                    if (listToInsert != null && listToInsert.size() > 0) {
                        insert listToInsert;
                    }

                }
            }
        } catch (exception ex) { system.debug('----msg---' + ex.getMessage()); system.debug('----lineNumber---' + ex.getLineNumber()); res = ex.getMessage(); return res; }
        return res;
    }

    //v1.10
    public static string cancelComfortLetter(step__c objStep) {
        string res = 'Success';
        try {
            list < SR_Price_Item__c > tempListToRejectPrice = new list < SR_Price_Item__c > ();
            tempListToRejectPrice = [select id, Status__c from SR_Price_Item__c where ServiceRequest__c =: objStep.SR__c];
            if (tempListToRejectPrice != null && tempListToRejectPrice.size() > 0) {
                for (SR_Price_Item__c obj: tempListToRejectPrice) {
                    obj.Status__c = 'Rejected';
                }
                update tempListToRejectPrice;
            }
        } catch (exception ex) { system.debug('----msg---' + ex.getMessage()); system.debug('----lineNumber---' + ex.getLineNumber()); res = ex.getMessage(); return res; }
        return res;
    }

    //V1.12 start
    public static string updateArabfieldsonGSContact(step__c objStp) {
        string res = 'Success';
        try {
            map < id, map < string, string >> srContactMap = new map < id, map < string, string >> ();
            if (objStp.SR__c != null && objStp.Update_Arabic_Names__c == true && objStp.closed_Date__c != null)
                srContactMap.put(objStp.SR__c, null);
            map < id, id > srContactIDMap;
            Contact conToUpdate;
            if (srContactMap != null && srContactMap.size() > 0) {
                srContactIDMap = new map < id, id > ();

                for (service_Request__c sr: [select id, contact__c, contact__r.First_Name_Arabic__c, contact__r.Last_Name_Arabic__c from service_Request__c where id in: srContactMap.keySet() and contact__c != null]) {
                    srContactIDMap.put(sr.id, sr.contact__c);
                    srContactMap.put(sr.id, new map < string, string > ());
                    srContactMap.get(sr.id).put('firstname', sr.contact__r.First_Name_Arabic__c);
                    srContactMap.get(sr.id).put('lastname', sr.contact__r.last_Name_Arabic__c);
                }
            }

            if (srContactMap.containsKey(objStp.sr__c) && srContactMap.get(objStp.sr__c) != null && (objStp.First_Name_Arabic__c != srContactMap.get(objStp.sr__c).get('firstname') || objStp.Last_Name_Arabic__c != srContactMap.get(objStp.sr__c).get('lastname'))) {
                conToUpdate = new contact(id = srContactIDMap.get(objStp.sr__c), First_Name_Arabic__c = objStp.First_Name_Arabic__c, Last_Name_Arabic__c = objStp.Last_Name_Arabic__c, Arabic_fields_updated_on__c = system.Now());

                if (conToUpdate != null){ update conToUpdate; }
            }

        } catch (exception e) { res = e.getMessage(); }

        return res;

    } // end V1.12

    // V1.13
    Public static void CopyBarcodeDetails(id srDocID, id attachID) {
        string servicereq;
        Savepoint sp;
        string AttachmentbodyID;
        string salesforceInstance;
        if (URL.getSalesforceBaseURL().toexternalForm().indexOf('://') > 0 && URL.getSalesforceBaseURL().toexternalForm().split('://').size() > 0) {
            if (URL.getSalesforceBaseURL().toexternalForm().split('://')[1].split('\\.').size() > 0)
                salesforceInstance = URL.getSalesforceBaseURL().toexternalForm().split('://')[1].split('\\.')[0];
        }


        try {
            if (srDocID != null && attachID != null) {
                List < SR_Doc__c > srDocListToInsert = new list < SR_Doc__c > ();

                for (SR_Doc__c srd: [select id, Barcode__c, service_request__r.Name from SR_Doc__c where Name Like '%Cover Letter%'
                        and Show_Barcode__c = true and service_request__c =: [select service_request__c from SR_Doc__c where id =: srDocID].service_request__c
                    ]) {
                    //V1.14 Mudasir updated the custom url code below and moved the same to Label.
                    srd.Barcode__c = '<pre><img alt=\"User-added image" src=\"' + System.label.fileDownloadURL + attachID + '\"></img></pre>';
                    srd.Generate_Document__c = true;
                    srDocListToInsert.add(srd);
                    servicereq = srd.service_request__r.Name;
                }
                sp = database.setSavePoint();
                if (srDocListToInsert != null && srDocListToInsert.size() > 0)
                    update srDocListToInsert;
            }

        } catch (Exception e) { 
            Database.RollBack(sp); 
            insert new Log__c(Type__c = 'GS Barcode update failed for SR # ' + servicereq, Description__c = e.getMessage() + ' Line # ' + e.getLineNumber() );
        }
    }
    // V.13
    /*
    //V2.0 - Start
    public static string updateMobileNumber(step__c objStp) {
        string res = 'Success';
        try {
        	List<Service_request__c> servList = new List<Service_request__c>();
			for(Service_request__c servReq : [Select id,Mobile_number__c from service_request__c where id=:objStp.SR__c]){
				servReq.Mobile_number__c = objStp.mobile_No__c != Null ? objStp.mobile_No__c : Null;
				servList.add(servReq);
			}
			update servList;
			
        } catch (exception e) { res = e.getMessage(); }

        return res;

    } 
    //V2.0 - End
    //V2.1 - Start
     public static string checkIfShipmentToBeCreated(step__c objStp) {
        string res = 'Success';
        try {
        	if([Select count() from Service_request__c where id=:objStp.SR__c and Mobile_number__c != Null and Would_you_like_to_opt_our_free_couri__c ='Yes' and Type_of_Request__c='Applicant Outside UAE'] > 0){
        		CC_CourierCls.CreateShipment(objStp.id);
        	}
			
        } catch (exception e) { res = e.getMessage(); }

        return res;

    } 
    //V2.1 - Start
    //V2.1 - Start
     public static string checkIfMobileUpdateStepIsClosed(step__c objStp) {
     	system.assertEquals(null,objStp);
        string res = 'Success';
        try {
        	if([Select count() from Step__c where SR__c=:objStp.SR__c and Step_Name__c ='Mobile Number Update' and Status__r.Name ='Closed'] == 0){
        		res = 'Online Entry Permit is Typed cannot be closed before step Mobile Number Update is closed';
        		system.assertEquals(null,res);
        	}
			
        } catch (exception e) { res = e.getMessage(); }

        return res;

    } 
    //V2.1 - END
    */
}