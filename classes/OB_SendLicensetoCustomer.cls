/**
* Description:
* Send generated license to customer as an attachment
**/
global without sharing class OB_SendLicensetoCustomer implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        string AccountId = stp.HexaBPM__SR__r.HexaBPM__Customer__c;
        system.debug('=====CC_SendLicensetoCustomer=======');
         if(stp.HexaBPM__SR__c!=null && AccountId != null){
        
            string strTemplateId;
            Set<String> documentMasterCodeList = new Set<String>();
             if((stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c=='AOR_Financial' || stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c=='AOR_NF_R') ){
                // store documente master code of AOR doc that we go as an attachment in an email
                String documentMasterAORLabel = Label.OB_DocumentMasterAORLabel;
                
                if(String.IsNotBlank(documentMasterAORLabel)){
                    for(String ss: documentMasterAORLabel.split(',')){
                        documentMasterCodeList.add(ss);
                    }
                } 
                //documentMasterCodeList.add('LICENSE');
                //documentMasterCodeList.add('NPIO_LICENSE');
                //documentMasterCodeList.add('CERTIFICATE_OF_REGISTRATION_GENERIC_GENERATED');
                //documentMasterCodeList.add('Certificate_of_Incorporation_Generic_Generated');
                //documentMasterCodeList.add('Certificate_of_Incorporation_Continuation_Generated');
                //documentMasterCodeList.add('CERTIFICATE_OF_REGISTRATION_FOUNDATION_GENERATED');
                //documentMasterCodeList.add('Certificate_of_Incorporation_NPIO_Generated');
                // Label store the email template Name
                for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName=:Label.OB_EmailTemplateNameAOR]){
                    strTemplateId = objET.Id;
                }
            
             }else if(stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c=='In_Principle' ){
                
                // store documente master code of AOR doc that we go as an attachment in an email
                String documentMasterAORLabel = Label.OB_DocumentMasterIn_PrincipleLabel;
                if(String.IsNotBlank(documentMasterAORLabel)){
                    for(String ss: documentMasterAORLabel.split(',')){
                        documentMasterCodeList.add(ss);
                    }
                }
                //documentMasterCodeList.add('CERTIFICATE_OF_STATUS');
                // Label store the email template Name
                for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName=: Label.OB_EmailTemplateNameInPrinciple]){
                    strTemplateId = objET.Id;
                }
             }
            
            if(string.IsNotBlank(strTemplateId) && documentMasterCodeList != null && documentMasterCodeList.size() != 0){
                
                list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
                list<Messaging.Emailfileattachment> lstAttach = new list<Messaging.Emailfileattachment>();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                Messaging.Emailfileattachment efa;
                
                list<string> lstIds = new list<string>();
                
                OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
                for(HexaBPM__SR_Doc__c objSRDoc : [Select Id,HexaBPM__Document_Master__c,HexaBPM__Doc_ID__c,
                                                    HexaBPM__Document_Master__r.HexaBPM__Code__c 
                                                    From HexaBPM__SR_Doc__c 
                                                    Where HexaBPM__Service_Request__c=:stp.HexaBPM__SR__c 
                                                    AND HexaBPM__Doc_ID__c != null
                                                    AND HexaBPM__Document_Master__r.HexaBPM__Code__c IN: documentMasterCodeList]){
                    lstIds.add(objSRDoc.HexaBPM__Doc_ID__c);
                }  
                
                for(ContentVersion cv : [Select Id,PathOnClient,VersionData,Title,Application_Document__c from ContentVersion 
                                            where ContentDocumentId IN : lstIds order by CreatedDate desc]){
                    efa = new Messaging.Emailfileattachment();
                    efa.setFileName(cv.PathOnClient);
                    efa.setBody(cv.VersionData);
                    lstAttach.add(efa);
                }
                system.debug('=====lstAttach======='+lstAttach);
                if(lstAttach != null && lstAttach.size() != 0 && String.IsNotBlank(AccountId)){
                    map<string,string> mapContactEmails = new map<string,string>();       
                    // temporay contact creation for sending an email
                    Contact tempContact = new Contact(email = stp.HexaBPM__SR__r.HexaBPM__Email__c, firstName = 'test', lastName = 'email',accountID =AccountId );
                    insert tempContact;
                    mapContactEmails.put(tempContact.Id,tempContact.Email);
                    
                    //account owner in cc record
                    list<string> ccEmailsList = new list<string>();
                    for(Account acc:[Select ID,Owner.Email From Account Where Id =: AccountId limit 1]){
                        ccEmailsList.add(acc.Owner.Email);
                    }
                    
                    //Email :  stp.HexaBPM__SR__r.HexaBPM__Email__c
                    system.debug(mapContactEmails+'==email==='+stp.HexaBPM__SR__r.HexaBPM__Email__c);
                    
                    //ccEmailsList.add('rachita.agarwal@pwc.com');
                     system.debug('==lstEmails===='+ccEmailsList);
                    list<string> setToAddressesList = new list<string>();
                    setToAddressesList.add(stp.HexaBPM__SR__r.HexaBPM__Email__c);
                    
                    for(string ContactId : mapContactEmails.keySet()){
                    
                        mail = new Messaging.SingleEmailMessage();
                        mail.setCCAddresses(ccEmailsList);
                        mail.setToAddresses(setToAddressesList);
                        //mail.setReplyTo('portal@difc.ae');
                        mail.setWhatId(stp.HexaBPM__SR__c);
                        mail.setTemplateId(strTemplateId);
                        //mail.setTreatTargetObjectAsRecipient(false);
                        mail.setSaveAsActivity(false);
                        if(lstOrgEA!=null && lstOrgEA.size()>0)
                            mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
                        if(!lstAttach.isEmpty())
                            mail.setFileAttachments(lstAttach);
                        mail.setTargetObjectId(ContactId);
                        mails.add(mail);
                    }
                    //system.debug('=====mails======='+mails);
                    if(mails != null && !mails.isEmpty()){
                        if(!test.isRunningTest()){
                            Messaging.sendEmail(mails);
                        }  
                        delete tempContact;              
                    }
                }
            }
        }           
        return strResult;
    }
}