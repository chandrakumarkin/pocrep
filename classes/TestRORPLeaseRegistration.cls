/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=false)
private class TestRORPLeaseRegistration {
    
    @testSetup static void prepareData(){
        insert Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
    }

    static testMethod void testRorpCompanyLease() {
        testRorp(false,false,false,false,false,false);
    }
    
    static testMethod void testRorpCompanySubLease() {
        testRorp(true,false,false,false,false,false);
    }
    
    static testMethod void testRorpCompanyActiveLease(){
        testRorp(false,true,true,false,false,false);
    }
    
    static testMethod void testRorpCompanyFutureLease(){
        testRorp(false,false,true,true,false,false);
    }
    
    static testMethod void testRorpCompanyActiveSubLease(){
        testRorp(true,true,true,true,false,false);
    }
    
    static testMethod void testRorpCompanyWithSecondary(){
        testRorp(false,true,true,true,true,false);
    }
    
    static testMethod void testRorpIndividual(){
        testRorp(true,true,true,true,true,true);
    }
    
    static testMethod void testNegativeScenarioLease(){
        testNegativeScenarios(false,false,false);
    }
    
    static testMethod void testNegativeScenarioLeaseIndividual(){
        testNegativeScenarios(false,false,true);
    }
    
    static testMethod void testNegativeScenarioSubLease1(){
        testNegativeScenarios(true,false,false);
    }
    
    static testMethod void testNegativeScenarioSubLease2(){
        testNegativeScenarios(true,true,false);
    }
    
    static testMethod void testNegativeScenarioSubLeaseIndividual1(){
        testNegativeScenarios(true,false,true);
    }
    
    static testMethod void testNegativeScenarioSubLeaseIndividual2(){
        testNegativeScenarios(true,true,true);
    }
    
    static testMethod void testSearchAccounts(){
        
        Cls_SearchAccountsExtension testInst = new Cls_SearchAccountsExtension();
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Registration_License_No__c = '832018';
        objAccount.Company_Code__c = 'test';
        objAccount.Sub_Legal_Type_of_Entity__c='Incorporated Cell Company';
        
        objAccount.ROC_Status__c = 'Under Formation';
        
        insert objAccount;
        
        testInst.HostingCompanyName = 'Test Customer';
        testInst.searchWithSubLegalStructure = true;
         
        testInst.SearchHostingCompanies();
        testInst.clearList();
        Cls_SearchAccountsExtension.HostingWrapper hostingwrap = new Cls_SearchAccountsExtension.HostingWrapper();
        
    }
    
    private static void testRorp(Boolean isSubLease, Boolean isActive, Boolean isLease, Boolean hasPrimary, Boolean hasSecondary, Boolean isIndividual){
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        objBuilding.SAP_Building_No__c = '123456';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        objUnit.Parent_Unit__c  = null;
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Residential';
        objUnit.Parent_Unit__c  = null;
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration','Body_Corporate_Tenant','Landlord','Lease_Unit')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        
        objAccount.ROC_Status__c = 'Under Formation';
        insert objAccount;
        
        list<Lease__c> lstLeases = new list<Lease__c>();
        list<Tenancy__c> lstTens = new list<Tenancy__c>();
        
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addMonths(25);
        objLease.Status__c = isActive ? 'Active' : 'Future Lease';
        objLease.Type__c = 'Leased';
        lstLeases.add(objLease);
        insert lstLeases;
        
        Tenancy__c objTen = new Tenancy__c();
        objTen.Lease__c = lstLeases[0].Id;
        objTen.Unit__c = lstUnits[0].Id;
        objTen.Start_Date__c = system.today().addMonths(13);
        objTen.End_Date__c = system.today().addMonths(25);
        lstTens.add(objTen);
        insert lstTens;
        
        test.startTest();
       
            Apexpages.currentPage().getParameters().put('RecordType',mapRecordType.get('Lease_Registration'));
            Apexpages.currentPage().getParameters().put('isDetail','false');
            
            Apexpages.Standardcontroller objSCon = new Apexpages.Standardcontroller(new Service_Request__c());
            
            RORPLeaseRegistration objLR = new RORPLeaseRegistration(objSCon);
            
            objLR.SearchUnits();
            objLR.ServiceRequest.Customer__c = objAccount.Id;
            objLR.ServiceRequest.Building__c = objBuilding.Id;
            objLR.ServiceRequest.Service_Category__c = 'New';
            objLR.ServiceRequest.Type_of_Lease__c = isSubLease ? 'Sub-lease' : 'Lease';
            objLR.ServiceRequest.Lease_Commencement_Date__c = system.today().addYears(-1).addDays(-10);
            objLR.ServiceRequest.Lease_Expiry_Date__c = system.today().addDays(-10);
            objLR.ServiceRequest.Agreement_Date__c =  system.today().addYears(-1).addMonths(-6);
            objLR.ServiceRequest.Submitted_DateTime__c = system.today().addYears(-1).addMonths(-6).addDays(10);
           // objLR.ServiceRequest.Lease_Expiry_Date__c = system.today().addMonths(24);
            objLR.ServiceRequest.Required_Docs_not_Uploaded__c = false;
            objLR.SearchString = '123';
            objLR.BuildingId = objBuilding.Id;
            objLR.SearchUnits();
            
            objLR.ChangeLeaseTypeType();
            
            objLr.TenantType = isIndividual ? 'Individual' : 'Company';
            objLr.UsageType = isIndividual ? 'Residential' : 'Commercial';
            ObjLr.ServiceRequest.Sponsor_Middle_Name__c='Same';
            objLR.ChangeTenantType();
            objLR.ChangeOwnerType();
            
            for(RORPLeaseRegistration.UnitDetails obj : objLR.AllUnits){
                obj.isChecked = true;
            }
            
            objLR.AddUnits();
            
            for(RORPLeaseRegistration.UnitDetails obj : objLR.AllUnits){
                obj.RentalAmount = 123456;
            }
            
            objLR.AddUnits();
            objLR.SaveSR();
            objLR.EditRequest();
            objLR.AddLandlord();
            objLR.AddBCTenant();
            objLR.AddIndTenant();
            //objLR.setPrimaryTenantDetails();
            objLR.penaltycalculator();
            objLR.penaltyRemove();
            objLR.CancelUnits();
            
            objLR.RowIndex = 0;
            objLR.RemoveUnits();
            objLR.CancelRequest();
            
            objLR.ChangeTenantType();
            objLR.ChangeOwnerType();
            objLR.ChangeLeaseTypeType();
            System.debug('*** objLr.TenantType'+objLr.TenantType);
            objLR.resetvalues();
            upsert objLR.ServiceRequest;
            
            system.debug('-----SR ID--'+objLR.ServiceRequest.Id);
            performRorpSecondTest(mapRecordType,objAccount.Id,objLR.ServiceRequest.Id,objBuilding.Id,lstUnits[0].Id,isLease,isSubLease,hasPrimary,hasSecondary,isIndividual);
            
        test.stopTest();
    }
    
    @future 
    private static void performRorpSecondTest(map<string,string> mapRecordType, 
                                                                    String accountId, 
                                                                    String srId, 
                                                                    String buildingId, 
                                                                    String unitId, 
                                                                    Boolean isLease, 
                                                                    Boolean isSubLease, 
                                                                    Boolean hasPrimary, 
                                                                    Boolean hasSecondary,
                                                                    Boolean isIndividual){
        
        list<Amendment__c> lstAmdendments = new list<Amendment__c>();
            
        Amendment__c objTenant = new Amendment__c();
        
        objTenant.ServiceRequest__c = srId;
        objTenant.RecordTypeId = mapRecordType.get('Body_Corporate_Tenant');
        objTenant.Amendment_Type__c = 'Tenant';
        
        if(hasPrimary){
            objTenant.Is_Primary__c = true;
            objTenant.Manager__c = true;
        }
        
        objTenant.Company_Name__c = 'Test';
        objTenant.Shareholder_Company__c = accountId;
        objTenant.IssuingAuthority__c = 'DIFC - Under Formation';
        objTenant.Office_Unit_Number__c = '123';
        objTenant.Given_Name__c = 'RB';
        
        objTenant.Passport_Issue_Date__c = System.Today().addMonths(-7);
        objTenant.Passport_Expiry_Date__c = System.Today().addMonths(4);
        objTenant.Date_of_Birth__c = System.Today().addYears(-25);
        objTenant.Place_of_Birth__c = 'United Arab Emirates';
        objTenant.Emirates_ID_Number__c = '1234567890';
        objTenant.Gender__c = 'Male';
        objTenant.Office_Unit_Number__c = '1';
        objTenant.Office_Phone_Number__c = '+971 12345678';
        objTenant.Building_Name__c = 'test';
        objTenant.Title_new__c = 'Mr.';
        objTenant.Phone__c = '+971 12345678';
        objTenant.Street__c = 'test st.';
        objTenant.Permanent_Native_Country__c = 'United Arab Emirates';
        objTenant.Person_Email__c = 'test.user-tester@difc.test.ae';
        objTenant.Permanent_Native_City__c = 'Dubai';
        objTenant.Residence_Phone_Number__c = '+971 12345678';
        objTenant.PO_Box__c = '123456';
        objTenant.Country_of_Issuance__c = 'United Arab Emirates';
        objTenant.Family_Name__c = 'DIFC';
        objTenant.Passport_No__c = '123456789';
        objTenant.Nationality_list__c = 'India';
        objTenant.Company_Name__c = 'Test Company One';
        objTenant.Name_of_Declaration_Signatory__c = 'Test';
        objTenant.Capacity__c = 'Test';
       // objTenant.CL_Certificate_No__c = 'ABCD-1234';
        objTenant.Fax__c = '+971 12345678';
        
        lstAmdendments.add(objTenant);
        
        objTenant = new Amendment__c();
        objTenant.ServiceRequest__c = srId;
        objTenant.RecordTypeId = mapRecordType.get('Landlord');
        objTenant.Amendment_Type__c = 'Landlord';
        objTenant.Company_Name__c = 'Test Company One';
        objTenant.Type_of_Landlord__c = 'Individual';
        objTenant.Given_Name__c = 'RB';
        objTenant.Family_Name__c = 'DIFC';
        objTenant.Passport_No__c = '123456780';
        objTenant.Nationality_list__c = 'India';
        objTenant.CL_Certificate_No__c = 'ABCD-4567';
        lstAmdendments.add(objTenant);
        
        objTenant = new Amendment__c();
        objTenant.ServiceRequest__c = srId;
        objTenant.Amendment_Type__c = 'Occupant';
        objTenant.Given_Name__c = 'RB';
        objTenant.Family_Name__c = 'DIFC';
        objTenant.Passport_No__c = '123456780';
        objTenant.Company_Name__c = 'Test Company Two';
        objTenant.Nationality_list__c = 'India';
        objTenant.Unit__c = unitId;
        objTenant.CL_Certificate_No__c = 'ABCD-8910';
        
        lstAmdendments.add(objTenant);
        
        objTenant = new Amendment__c (Company_Name__c = 'Test Company Two', RecordTypeId=mapRecordType.get('Lease_Unit'),ServiceRequest__c=srId ,  Amendment_Type__c = 'Unit', Status__c = 'Active' );
        
        lstAmdendments.add(objTenant);
        
        insert lstAmdendments;
        
        Apexpages.currentPage().getParameters().put('RecordType',mapRecordType.get('Lease_Registration'));
        Apexpages.currentPage().getParameters().put('isDetail','true');
        Apexpages.currentPage().getParameters().put('id',srId);

        Apexpages.Standardcontroller objSCon = new Apexpages.Standardcontroller(new Service_Request__c());

        RORPLeaseRegistration objLR = new RORPLeaseRegistration(objSCon);
        
        list<RORPLeaseRegistration.UnitDetails> lstUD = new list<RORPLeaseRegistration.UnitDetails>();
        
        RORPLeaseRegistration.UnitDetails objUD = new RORPLeaseRegistration.UnitDetails();
        
        objLr.TenantType = isIndividual ? 'Individual' : 'Company';
        
        objLr.ContextObj.IsDetail = false;
        
        objLR.RowIndex = 0;
        objLR.RemoveUnits();
        
        objLR.SearchString = '123';
        objLR.BuildingId = buildingId;
        objLR.SearchUnits();
        
        objLR.getTenantLandlords();
        
        for(RORPLeaseRegistration.UnitDetails obj : objLR.AllUnits){
            obj.isChecked = true;
            obj.RentalAmount = 123456;
        }
        
        objLr.usageType = 'Residential';
        
        objLR.AddUnits();
        
        if(hasPrimary){
            objLr.PrimaryTenant.Is_Primary__c = true;   
            objLr.PrimaryTenant = lstAmdendments[0];
        }
        
        if(hasSecondary){
            objLr.SecondaryTenant = lstAmdendments[0];
        }
        
        objLR.ServiceRequest.Type_of_Lease__c = isSubLease ? 'Sub-lease' : 'Lease';
        
        objLr.TenantType = isIndividual ? 'Individual' : 'Company';
        
        objLR.AddBCTenant();
        objLR.AddIndTenant();
        objLR.AddOccupants();
        
        objLR.ContextObj.MenuText = 'Test';
        objLR.ContextObj.Description = 'Test';
        
        lstUD = new list<RORPLeaseRegistration.UnitDetails>();
        
        objUD = new RORPLeaseRegistration.UnitDetails();
        
        objUD.UnitId = unitId;
        objUD.Description = 'Residential';
        objUD.isLease = isLease;
        
        lstUD.add(objUD);
        
        objLR.UnitInfo = lstUD;
        objLR.EditAmendment();
        
        lstUD[0].isEdit = false;
        
        objLR.UnitInfo = lstUD;
        objLR.EditAmendment();
        
        objLR.SaveAmendment();
        lstUD[0].RentalAmount = 1234;
        objLR.UnitInfo = lstUD;
        
        objLR.SaveAmendment();
        
        objUD.UnitId = unitId;
        objUD.Description = 'Residential';
        objUD.isLease = isIndividual ? false : isLease;
        
        objLR.UnitInfo = lstUD;
        
        objLr.ContextObj.IsDetail = false;
        objLR.setPrimaryTenantDetails();
        objLR.SaveSR();
        
        objLR.SubmitRequest();
        
        objLR.RowIndex = 0;
        
        Amendment__c objAmd = new Amendment__c(ServiceRequest__c=srId);
        insert objAmd;
        
        lstUD[0].Amendment = new Amendment__c(id=objAmd.Id);
        
        objLR.UnitInfo = lstUD;
        objLR.SaveAmendment();
        
        lstUD[0].Amendment = new Amendment__c(id=objAmd.Id);
        lstUD[0].RentalAmount = 12345;
        
        objLR.UnitInfo = lstUD;
        objLR.CancelAmendment();
        
        lstUD[0].Amendment = new Amendment__c();
        
        objLR.UnitInfo = lstUD;
        objLR.CancelAmendment();
        
        objLR.ContextObj.IsDetail = true;
        
        objLr.CancelRequest();
        
        CC_RORPCodeCls.updateNoOfUnits(objLR.ServiceRequest);
        
    }
    
    private static void testNegativeScenarios(Boolean isSubLease, Boolean isLeaseSubLease, Boolean isIndividual){
        
        profile p =[select id,name from profile where name='RORP Officer with Case' limit 1];
        
         User u = new User(Alias = 'standt', Email='abc@difc.com',       
          EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',firstName ='firstName',lastname='LastName',
          LocaleSidKey='en_US', ProfileId = p.Id,title='Mr.',phone='971507301993',isActive=true,
          TimeZoneSidKey='America/Los_Angeles', UserName='RORPuser123@testorg.com');
          insert u;
        
        // TO DO: implement unit test
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        objBuilding.SAP_Building_No__c = '123456';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        objUnit.Parent_Unit__c  = null;
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Residential';
        objUnit.Parent_Unit__c  = null;
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
      
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration','Body_Corporate_Tenant','Landlord')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        //objAccount.Registration_License_No__c = '0659';
        objAccount.ROC_Status__c = 'Under Formation';
        insert objAccount;
        
        list<Lease__c> lstLeases = new list<Lease__c>();
        list<Tenancy__c> lstTens = new list<Tenancy__c>();
        
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        //objLease.Start_date__c = system.today().addMonths(13);
        //objLease.End_Date__c = system.today().addMonths(25);
        
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today() + 1;
        
        objLease.Status__c = 'Active';
        objLease.Type__c = 'Leased';
        
        if(isSubLease) objLease.Is_Sub_Lease__c = isLeaseSubLease;
        
        lstLeases.add(objLease);
        insert lstLeases;
        
        Tenancy__c objTen = new Tenancy__c();
        objTen.Lease__c = lstLeases[0].Id;
        objTen.Unit__c = lstUnits[0].Id;
        
        objTen.Start_Date__c = system.today().addMonths(1);
        objTen.End_Date__c = system.today().addMonths(12);
        
        lstTens.add(objTen);
        
        objTen = new Tenancy__c();
        objTen.Lease__c = lstLeases[0].Id;
        objTen.Unit__c = lstUnits[1].Id;
        objTen.Start_Date__c = system.today().addMonths(1);
        objTen.End_Date__c = system.today().addMonths(12);
        
        lstTens.add(objTen);
        
        insert lstTens;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordType.get('Lease_Registration');
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Service_Category__c = 'New';
        objSR.Lease_Commencement_Date__c = system.today();
        objSR.Lease_Expiry_Date__c = system.today().addMonths(13);
        objSR.Agreement_Date__c = system.today();
        objSR.Sponsor_Middle_Name__c='Decreased';
        objSR.Building__c = objBuilding.Id;
        insert objSR;
        
        test.startTest();
       
            Apexpages.currentPage().getParameters().put('id',objSR.Id);
        
            Apexpages.currentPage().getParameters().put('RecordType',mapRecordType.get('Lease_Registration'));
            Apexpages.currentPage().getParameters().put('isDetail','false');
            
            Apexpages.Standardcontroller objSCon = new Apexpages.Standardcontroller(new Service_Request__c());
            
            RORPLeaseRegistration objLR = new RORPLeaseRegistration(objSCon);
            
            objLR.ServiceRequest.Lease_Commencement_Date__c = system.today();
            
            objLR.BuildingId = objBuilding.Id;
            
            objLR.SearchUnits();
            
            objLR.ServiceRequest.Lease_Expiry_Date__c = system.today().addMonths(12);
            
            objLR.SearchUnits();
            objLR.ServiceRequest.Customer__c = objAccount.Id;
            objLR.ServiceRequest.Building__c = objBuilding.Id;
            objLR.ServiceRequest.Service_Category__c = 'New';
            objLR.ServiceRequest.Type_of_Lease__c = isSubLease ? 'Sub-lease' : 'Lease';
            objLR.ServiceRequest.Lease_Commencement_Date__c = system.today();
            objLR.ServiceRequest.Lease_Expiry_Date__c = system.today() + 1;
            objLR.ServiceRequest.Required_Docs_not_Uploaded__c = false;
            
            objLR.SearchString = '123';
            objLR.BuildingId = objBuilding.Id;
            objLR.SearchUnits();
            
            objLR.ChangeLeaseTypeType();
            
            //objLr.TenantType = 'Company';
            
            objLr.TenantType = isIndividual ? 'Individual' : 'Company';
            
            objLr.UsageType = isIndividual ? 'Residential' : 'Commercial';
            
            objLR.ChangeTenantType();
            objLR.ChangeOwnerType();
            objLR.datecalculator();
            
            for(RORPLeaseRegistration.UnitDetails obj : objLR.AllUnits){
                obj.isChecked = true;
                obj.RentalAmount = 123456;
            }
            
            //objLr.UsageType = 'Residential';
            
            objLR.AddUnits();
            objLR.SaveSR();
            objLR.resetvalues();
            list<Amendment__c> lstAmdendments = new list<Amendment__c>();
            
            Amendment__c objTenant = new Amendment__c();
            objTenant.ServiceRequest__c = objLR.ServiceRequest.Id;
            objTenant.RecordTypeId = mapRecordType.get('Body_Corporate_Tenant');
            objTenant.Amendment_Type__c = 'Tenant';
            objTenant.Company_Name__c = 'Test';
            objTenant.Shareholder_Company__c = objAccount.id;
            objTenant.IssuingAuthority__c = 'DIFC - Under Formation';
            objTenant.Office_Unit_Number__c = '123';
            objTenant.Given_Name__c = 'RB';
            
            objTenant.Passport_Issue_Date__c = System.Today().addMonths(-7);
            objTenant.Passport_Expiry_Date__c = System.Today().addMonths(4);
            objTenant.Date_of_Birth__c = System.Today().addYears(-25);
            objTenant.Place_of_Birth__c = 'United Arab Emirates';
            objTenant.Emirates_ID_Number__c = '1234567890';
            objTenant.Gender__c = 'Male';
            objTenant.Office_Unit_Number__c = '1';
            objTenant.Office_Phone_Number__c = '+971 12345678';
            objTenant.Building_Name__c = 'test';
            objTenant.Title_new__c = 'Mr.';
            objTenant.Phone__c = '+971 12345678';
            objTenant.Street__c = 'test st.';
            objTenant.Permanent_Native_Country__c = 'United Arab Emirates';
            objTenant.Person_Email__c = 'test.user-tester@difc.test.ae';
            objTenant.Permanent_Native_City__c = 'Dubai';
            objTenant.Residence_Phone_Number__c = '+971 12345678';
            objTenant.PO_Box__c = '123456';
            objTenant.Country_of_Issuance__c = 'United Arab Emirates';
            objTenant.Family_Name__c = 'DIFC';
            objTenant.Passport_No__c = '123456789';
            objTenant.Nationality_list__c = 'India';
            objTenant.Company_Name__c = 'Test Company One';
            objTenant.Name_of_Declaration_Signatory__c = 'Test';
            objTenant.Capacity__c = 'Test';
          //  objTenant.CL_Certificate_No__c = 'ABCD-1234';
            objTenant.Fax__c = '+971 12345678';
        
            lstAmdendments.add(objTenant);
            
            objTenant = new Amendment__c();
            objTenant.ServiceRequest__c = objLR.ServiceRequest.Id;
            objTenant.RecordTypeId = mapRecordType.get('Landlord');
            objTenant.Amendment_Type__c = 'Landlord';
            objTenant.Company_Name__c = 'Test Company One';
            objTenant.Type_of_Landlord__c = 'Individual';
            objTenant.Given_Name__c = 'RB';
            objTenant.Name_of_Declaration_Signatory__c = 'Test';
            objTenant.Capacity__c = 'Test';
            objTenant.IssuingAuthority__c = 'Others';
            objTenant.Family_Name__c = 'DIFC';
            objTenant.middle_name__c = 'Test Middle name';
            objTenant.Passport_No__c = '123456780';
            objTenant.Nationality_list__c = 'India';
            objTenant.CL_Certificate_No__c = 'ABCD-4567';
            lstAmdendments.add(objTenant);
            
            objTenant = new Amendment__c();
            objTenant.ServiceRequest__c = objLR.ServiceRequest.Id;
            objTenant.Amendment_Type__c = 'Occupant';
            objTenant.Given_Name__c = 'RB';
            objTenant.Family_Name__c = 'DIFC';
            objTenant.Passport_No__c = '123456780';
            objTenant.Company_Name__c = 'Test Company Two';
            objTenant.Nationality_list__c = 'India';
            objTenant.Unit__c = lstUnits[0].Id;
            objTenant.CL_Certificate_No__c = 'ABCD-8910';
            lstAmdendments.add(objTenant);
            
            insert lstAmdendments;
            
            Apexpages.currentPage().getParameters().put('isDetail','true');
            Apexpages.currentPage().getParameters().put('id',objLR.ServiceRequest.Id);
            
            objSCon = new Apexpages.Standardcontroller(objLR.ServiceRequest);

            objLR = new RORPLeaseRegistration(objSCon);
            
            objLR.SearchString = '123';
            
            list<RORPLeaseRegistration.UnitDetails> lstUD = new list<RORPLeaseRegistration.UnitDetails>();
            
            RORPLeaseRegistration.UnitDetails objUD = new RORPLeaseRegistration.UnitDetails();
            objUD.UnitId = lstUnits[0].Id;
            objUD.Description = isIndividual ? 'Residential' : 'Commercial';
            objUD.isLease = false;
            
            lstUD.add(objUD);
            objLR.UnitInfo = lstUD;
            
            objLr.ContextObj.IsDetail = false;
            
            objLR.SearchUnits();
            
            objLR.getTenantLandlords();
            
            if(isIndividual && isSubLease){
                objLr.UsageType = 'Residential';
            }
            
            if(isSubLease){
                objLr.OwnerType = isIndividual ? 'Individual' : 'Landlord';
            }
            
            objLr.doValidations();
            
            objLR.SubmitRequest();
            objLR.enableSecurityDeposit();
            objLR.checksecurityDeposit();
            
            CC_RORPCodeCls.updateNoOfUnits(objLR.ServiceRequest);
            
            
                system.currentPageReference().getParameters().put('id',objLR.ServiceRequest.Id);
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        clsLeaseRegistration_Security_BP obj = new clsLeaseRegistration_Security_BP();
        obj.Pageload();
        
        
            
        test.stopTest();
        
    }
    
}