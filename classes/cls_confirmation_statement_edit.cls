/*********************************************************************************************************************
--------------------------------------------------------------------------------------------------------------------------
Modification History 
----------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
----------------------------------------------------------------------------------------              
V1.1    26-Nov-2019 Arun        Prescribed Company change 
V1.2    15-dec-2019 Sai         #7917
v1.3    19-Aug-2020 Arun         #10679
v1.4    27-aug-2020 arun         #9756   
CR      14-oct-2020 Shikha
v1.5    17-Nov-2020 Abbas       Show subsector picklist when cutomertype is finance and subsector is blank and it has only mapped sector value (check metadata)
                                (Note: we are storing selected subsector values in "Company_Profile__c" field And Other subsector in "Current_Registered_Building__c" of Service_Request__c object which will be later used to update subsector on Account)
v1.6    12-Jan-2021 Suchita     #13034
v1.7    20-Jun-2021 Suchita     #Arun
V1.8    29-Jun-2021 Arun        #17056 added for Financial data
v1.9 	03-Aug-2021 Shikha		#17717 UBO exemption to be applicable for onboarding accounts
**********************************************************************************************************************/


public with sharing class cls_confirmation_statement_edit {
    
    public Service_Request__c SRData{get;set;}
    
    public string RecordTypeId;
    public map<string,string> mapParameters;
    public boolean isISPV{get;set;}
    public boolean isUBOExempted {get;set;}
    public Account ObjAccount{get;set;}
    public boolean IsFEY_Provided{get;set;}
    
    //V1.8  company submitted  Financial data  SR this year ?
    public boolean IsFileAccounts{get;set;}
    
    public boolean structuredFinancing{get;set;}
    //   public Company_Business_Hours__c businessHours{get;set;} 
    //  public string accQualifyingEntityType {get;set;} 
    // public string accQualifyingPurposeType {get;set;} 
    // public String prescribedCompany{get;set;}   //This Variable added for PrescribedCompany
    // public Boolean isPrescribedCompany{get;set;} //This Variable added for PrescribedCompany
    
    //Shikha - started
    Set<String> picklistValuesFinancial = new Set<String> (); //{'New York','California','Texas','Los Angeles','Chicago','San Francisco', 'Washington'};
    Set<String> picklistValuesInvestment = new Set<String>();
    Public List<String> leftSelected {get;set;}
    Public List<String> rightSelected {get;set;}
    public Set<String> leftValues = new Set<String>();
    public Set<String> rightValues = new Set<String>();
    public Service_Request__c eachRequest1{get; set;}
    public boolean checkOtherFlag{get; set;} 
    public boolean showSubSectorSection { get; set; }
    public String saveValue;
    //Shikha - end
    public String CustomerId;
    public List<Relationship__c> ListRel{get;set;}
    public Map<String,Subsector_Mapping__mdt> sectorsHavingMappedSubsectorMap { get; set;}
    public String ErrorMessage{get;set;}
    
    //public Boolean skipRequiredCheckInitiallyForMultiSelectPckLst { get; set; }
   
    //Shikha 
    /*public Boolean getShowOther (){
        return this.checkOtherFlag;
    }
    
    public void setShowOther (Boolean s){
        system.debug('### show Other '+s);
        this.checkOtherFlag = s;    
    }*/
    //Null or blank check before all this method   1.4v
    public string FEYValue(Date UserSelectedDate)
    {
        Map<Integer,String> monthNameMap=new Map<Integer, String>{1 =>'January', 2=>'February', 3=>'March', 4=>'April', 5=>'May',
            6=>'June', 7=>'July', 8=>'August', 9=>'September',10=>'October',
            11=>'November', 12=>'December'};
                
                return UserSelectedDate.day()+getDayOfMonthSuffix(UserSelectedDate.day())+' '+monthNameMap.get(UserSelectedDate.month());                                                  
    }  
    
    public String getDayOfMonthSuffix(Integer n) 
    {
        if (n == null) {
            return '';
        }
        
        if (n >= 11 && n <= 13) {
            return 'th';
        }
        
        Integer modResult = Math.mod(n, 10);        
        if (modResult == 1) { 
            return 'st'; 
        } else if (modResult == 2) { 
            return 'nd'; 
        } else if (modResult == 3) { 
            return 'rd'; 
        } else { 
            return 'th';
        }
    }
    
    public   PageReference AnnualTurnover()
    {
        return null;
    } 
    public cls_confirmation_statement_edit(ApexPages.StandardController controller)
    {
        IsFEY_Provided=false;
        isISPV = FALSE; 
        
        //V1.8
        IsFileAccounts=false;
        // isPrescribedCompany = FALSE;
        // prescribedCompany = '';
        ObjAccount=new account();
        sectorsHavingMappedSubsectorMap = new Map<String,Subsector_Mapping__mdt>();
        structuredFinancing = false;
         List<String> fields = new List<String> {'Financial_Year_End_mm_dd__c','Resolution_Date__c','Agreement_Date__c','Declaration_Laws__c','DIFC_Law_No__c','Monthly_Accommodation__c','Bank_Name__c','Security_Time__c','Apt_or_Villa_No__c','Current_Registered_Building__c','Authority_Name__c','Legal_Structures_Full__c'};
        
        
            if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        SRData=(Service_Request__c )controller.getRecord();
        Service_Request__c eachRequest = new Service_Request__c();
        if(SRData.id !=null)
        {
         //v1.9 - added BP no field in query  
            eachRequest = [SELECT Id,DIFC_Law_No__c,Agreement_Date__c,Legal_Structures_Full__c,Declaration_Laws__c,Customer__r.Name,Customer__r.License_Activity__c ,Is_the_Initiator_or_a_transaction_party__c,Customer__c,Customer__r.Exempted_from_UBO_Regulations__c,
                           Customer__r.Qualifying_Purpose_Type__c,                           
                           Customer__r.Company_UBOs__c,
                           Customer__r.Count_Body_Corporation_UBO__c,
                           Customer__r.Was_any_UBO__c,Customer__r.BP_No__c,
                           Customer__r.Contact_Details_Provided__c,
                           Customer__r.Qualifying_Type__c, Customer__r.License_Activity_Details__c, Customer__r.Company_Type__c, Customer__r.OB_Sector_Classification__c,Customer__r.Subsector__c,Customer__r.Other_SubSector__c,
                           Company_Profile__c,Current_Registered_Building__c FROM Service_Request__c where Id=:SRData.id];
            
            /*
ObjAccount.Exempted_from_UBO_Regulations__c=eachRequest.Customer__r.Exempted_from_UBO_Regulations__c;
ObjAccount.Qualifying_Purpose_Type__c=eachRequest.Customer__r.Qualifying_Purpose_Type__c;
ObjAccount.Qualifying_Type__c=eachRequest.Customer__r.Qualifying_Type__c;
ObjAccount.License_Activity__c = eachRequest.Customer__r.License_Activity__c;   //V1.2
ObjAccount.Name = eachRequest.Customer__r.Name;
*/
            
            ObjAccount=eachRequest.Customer__r;
            System.debug('ZZZ objAcc-->'+JSON.serialize(ObjAccount));
            //Shikha - added
            eachRequest1 = eachRequest;
        }
        // if(SRData.Financial_Year_End_mm_dd__c!=null)
        // ObjAccount.Financial_Year_End__c=SRData.Financial_Year_End_mm_dd__c;
        
        
        
        mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
            mapParameters = apexpages.currentPage().getParameters();              
        
        if(mapParameters.get('RecordType')!=null) 
            RecordTypeId= mapParameters.get('RecordType');    
        else
            RecordTypeId=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Confirmation_Statement').getRecordTypeId();
        
        
        //v1.9 - added BP no field in query
        for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                         Contact.Account.Company_UBOs__c,
                         Contact.Account.Count_Body_Corporation_UBO__c,
                         Contact.Account.Was_any_UBO__c,
                         
                           Contact.Account.Annual_Turnover_less_than_USD_5_million__c,
                         Contact.Account.Sub_Legal_Type_of_Entity__c,
                         
                         Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,Contact.Account.ROC_reg_incorp_Date__c,
                         Contact.Account.Qualifying_Type__c,Contact.Account.Qualifying_Purpose_Type__c,Contact.Account.Contact_Details_Provided__c,Contact.Account.BP_No__c,
                         Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c,Contact.Account.OB_Sector_Classification__c,Contact.Account.Subsector__c,Contact.Account.Other_SubSector__c 
                         from User where Id=:userinfo.getUserId()])
        {
            CustomerId = objUsr.Contact.AccountId;
            if(SRData.id==null){
                
                
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                SRData.Expiry_Date__c=objUsr.Contact.Account.Next_Renewal_Date__c;
                SRData.CreatedDate = system.today();
                ObjAccount= objUsr.Contact.Account;
                SRData.Financial_Year_End_mm_dd__c=objUsr.Contact.Account.Financial_Year_End__c;
                Formula.recalculateFormulas(new List<Service_Request__c >{SRData});                
                SRData.Company_Profile__c = '';
                SRData.Current_Registered_Building__c = '';
                
                SRData.Resolution_Date__c=objUsr.Contact.Account.ROC_reg_incorp_Date__c;
                //SRData.Year__c = string.valueOf(system.today().Year()-2);
                
                SRData.Year__c = string.valueOf(System.today().Year()-2);
                SRData.Apt_or_Villa_No__c=objUsr.Contact.Account.Sub_Legal_Type_of_Entity__c;
                
            }
            
            //V1.8
           List<Service_Request__c > ListSR=[select id from Service_Request__c where Recordtypeid='0126M000000Mhal' and Customer__c=:SRData.Customer__c and Submitted_Date__c=LAST_N_DAYS:300 and  Submitted_Date__c!=null ];

            if(SRData.Resolution_Date__c!=null)
            {
                Integer dt1 = SRData.Resolution_Date__c.daysBetween(system.today());
                
                if(!ListSR.IsEmpty() || dt1 <600)
                   IsFileAccounts=true;
                   
                  // if(dt1 <600)
                  // IsFileAccounts=true;//Do not collect Financial data  if compnay is new and not  older then 2 years .
                   
            }
            
            //Below Logic added by Sai, For 6368  
            if(objUsr.Contact.Account.Sector_Classification__c !=null && objUsr.Contact.Account.Sector_Classification__c ==Label.Intermediate_special_purpose_vehicle){
                isISPV = TRUE;           }
            if(objUsr.Contact.Account.Financial_Year_End__c!=null)
                IsFEY_Provided=true;
            //SRData.Customer__r = objUsr.Contact.Account;
            
            //system.debug('!!@@@@'+eachAccount.Qualifying_Type__c);
            
        }
        
        for(License_Activity__c eachActivity:[SELECT Activity__r.Name FROM License_Activity__c where Account__c=:CustomerId]){
            if(eachActivity.Activity__r.Name.equalsIgnoreCase('Qualifying Purpose of Structured Financing')){
                structuredFinancing = true;
            }
        }
        //start 13034
        /*ListRel=new List<Relationship__c>();
        ListRel=AccountsRelationshipMissingDetails.RelationshipMissingDetails(CustomerId);
        if(!ListRel.isEmpty())
        ErrorMessage=AccountsRelationshipMissingDetails.createMissingDetailsBody(CustomerId,ListRel);*/
        //End 13034
        
        /*
Account eachAccount = new Account();
system.debug('!!@@@@'+eachRequest.Customer__c);
if(eachRequest.Customer__c !=null){
eachAccount = [SELECT Exempted_from_UBO_Regulations__c,Sub_Legal_Type_of_Entity__c,Qualifying_Type__c
FROM Account where ID=:eachRequest.Customer__c];
if(eachAccount.Exempted_from_UBO_Regulations__c !=null){
isUBOExempted = eachAccount.Exempted_from_UBO_Regulations__c;
}

}
*/
        
        
       System.debug('ZZZ Cons()-->accObject-->'+JSON.serialize(ObjAccount));
        //v1.5
        for(Subsector_Mapping__mdt pickValues : [SELECT Id,DeveloperName,MasterLabel, Subsector_Values__c 
                                                 FROM Subsector_Mapping__mdt 
                                                ]){
            sectorsHavingMappedSubsectorMap.put(pickValues.MasterLabel, pickValues);                                          
        }
        
        System.debug('ZZZ Cons()-->sectorsHavingMappedSubsectorMap SIZE -->'+sectorsHavingMappedSubsectorMap.size());
        //System.debug('ZZZ Cons()-->showSubSectorSection-->'+showSubSectorSection);
        
        //checking if Company is "Financial - related", having "blank subsector" and it's sector has stored subsector mapping in metadata
        //if above all is true than only show subsector multipicklist
        if( ObjAccount != null && 
            String.isNotBlank(ObjAccount.Company_Type__c) &&
            ObjAccount.Company_Type__c == 'Financial - related' &&
            //String.isBlank(ObjAccount.Subsector__c) && 
            sectorsHavingMappedSubsectorMap.size() > 0 
            && sectorsHavingMappedSubsectorMap.keySet().contains(ObjAccount.OB_Sector_Classification__c)
          ){
            
            showSubSectorSection = true;
            //skipRequiredCheckInitiallyForMultiSelectPckLst = false;

            leftSelected = new List<String>();
            rightSelected = new List<String>();
             
            //Fetching all "active" Account's subsector picklist values
            leftSelected = getPickListValuesByObjectAndFieldName('Account','Subsector__c',true)?.values();
            leftValues.addAll(leftSelected); 
              
            //checkOtherFlag
            //Company_Profile__c
            //Current_Registered_Building__c
              if(SRData.id !=null && eachRequest1 != null && eachRequest1.Company_Profile__c != null){
                  if(String.isNotBlank(eachRequest1.Company_Profile__c)){
                      
                      String[] storedValues = eachRequest1.Company_Profile__c.split(';');
                      rightValues = new Set<String>(storedValues);
                      for(String str : storedValues){
                          leftValues.remove(str);
                      }
                      checkOtherFlag = !storedValues.isEmpty() && storedValues.contains('Other');
                      
                  }else{
                        
                        checkOtherFlag = false;   
                    
                     
                  }
              }else{
                  //If SR doesn't have subsector and Account already has subsector
                  if(SRData.id == null  && String.isNotBlank(ObjAccount.Subsector__c)){
                      SRData.Company_Profile__c = ObjAccount.Subsector__c;
                      String[] storedValues = SRData.Company_Profile__c.split(';');
                      rightValues = new Set<String>(storedValues);
                      for(String str : storedValues){
                          leftValues.remove(str);
                      }
                      checkOtherFlag = !storedValues.isEmpty() && storedValues.contains('Other');
                      
                      if(checkOtherFlag){
                          if(String.isNotBlank(ObjAccount.Other_SubSector__c)){
                              SRData.Current_Registered_Building__c = ObjAccount.Other_SubSector__c;  
                          }
                      }
                  }
              }
          }else{
              showSubSectorSection = false;
          }
        
        
        System.debug('ZZZ Cons()-->showSubSectorSection-->'+showSubSectorSection);
        /*
        //Shikha
        Map<String,Set<String>> picklistValues = new Map<String,Set<String>>();
        for(SR_Picklist_Value__mdt pickValues : [SELECT Id,DeveloperName,MasterLabel, Picklist_Values__c FROM SR_Picklist_Value__mdt WHERE DeveloperName 
                                                 IN ('Subsector_Financial_Company_Type','Subsector_Investment_Fund_Type')]){ 
                                                     String[] optionValues = pickValues.Picklist_Values__c.split(';');
                                                     Set<String> optionSet = new Set<String> (optionValues);                                                     
                                                     picklistValues.put(pickValues.DeveloperName,optionSet);                                                     
                                                 }
        
        leftSelected = new List<String>();
        rightSelected = new List<String>();
        String licenseActivityDetails = (eachRequest1 != null && eachRequest1.Customer__r != null) ? eachRequest1.Customer__r.License_Activity_Details__c : '';
        String companyType =  (ObjAccount != null && ObjAccount.Company_Type__c != null) ? ObjAccount.Company_Type__c : '';
        if(String.isNotBlank(licenseActivityDetails) && licenseActivityDetails.contains('Investment Fund')){
            leftValues.addAll(picklistValues.get('Subsector_Investment_Fund_Type'));
        }
        if(String.isNotBlank(companyType) && companyType == 'Financial - related'){
            leftValues.addAll(picklistValues.get('Subsector_Financial_Company_Type'));   
        }        
        if(eachRequest1 != null && eachRequest1.Company_Profile__c != null){
            String[] storedValues = eachRequest1.Company_Profile__c.split(';');
            rightValues = new Set<String>(storedValues);
            for(String str : storedValues){
                leftValues.remove(str);
            }
            checkOtherFlag = !storedValues.isEmpty() && storedValues.contains('Others');            
        }

        */  
        

        
    }
    
    //Shikha -
    public PageReference gSelect(){
        rightSelected.clear();
        for(String s : leftSelected){
            leftValues.remove(s);
            rightValues.add(s);
        }

        
        if(leftValues.isEmpty() && !rightValues.isEmpty() && rightValues.contains('Other')){
            checkOtherFlag = true; //If all left values are selected then right side contains "Other"
        }else{
            
            checkOtherFlag = !leftValues.isEmpty() && !leftValues.contains('Other') && !rightValues.isEmpty() && rightValues.contains('Other');
        }

        return null;
    }
    
    public PageReference gDeselect(){    
        leftSelected.clear();
        for(String s : rightSelected){
            rightValues.remove(s);
            leftValues.add(s);
        }
        
        checkOtherFlag = !rightValues.isEmpty() && !rightValues.contains('Others') && !leftValues.isEmpty() && !leftValues.contains('Other');
        
        return null;
    }
    
    public List<SelectOption> getDeselectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<String> objList = new List<String>();
        //Shikha
        /*if(eachRequest1.Company_Profile__c != null){
            for(String str : eachRequest1.Company_Profile__c.split(';')){
                if(leftValues.contains(str))
                    leftValues.remove(str);
            }
        }*/
        objList.addAll(leftValues);
        objList.sort();
        Set<String> sortedPklstValSet = new Set<String>(objList);
        Boolean picklstHasOtherValue = (!sortedPklstValSet.isEmpty() && sortedPklstValSet.contains('Other') ? true : false);
        if(picklstHasOtherValue){ //For adding 'Other' value in last place in picklist option
            sortedPklstValSet.remove('Other'); 
            sortedPklstValSet.add('Other'); 
        }
        
        
        for(String s : sortedPklstValSet){
            options.add(new SelectOption(s,s));
        }
        return options;
    }
    
    public List<SelectOption> getSelectedValues(){
         
        List<SelectOption> options = new List<SelectOption>();
        List<String> objList = new List<String>();//eachRequest1.Company_Profile__c.split(';');
        objList.addAll(rightvalues);
        objList.sort();
        for(String s : objList){
            options.add(new SelectOption(s,s));
        }
        //SRData.Company_Profile__c = String.join(objList, ';');
        saveValue = String.join(objList, ';');
        return options;
    }



    
  public  PageReference SaveConfirmation()
    {
        
        
        boolean isvalid=true;
        
        System.debug('ZZZ save_M-->showSubSectorSection-->'+showSubSectorSection);
        
        
        Boolean hasOpenCompliances = ![SELECT Id FROM Compliance__c WHERE Status__c = 'Created' AND Start_Date__c > TODAY AND Account__c = :SRData.Customer__c AND Name = 'Confirmation Statement'].isEmpty();
        
        if(hasOpenCompliances) {ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You can only submit the Confirmation Statement one month before expiry.'));isvalid=false;                }       

        
        if(SRData.Entity_Name_checkbox__c==false)//I hereby confirm that the company profile and officer details available on the DIFC Client Portal are accurate and up to date as of
        {
            isvalid=false;
            SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
            
        }
        
        
        // Start Below Logic added for PrescribedCompany /v 1.1
        if(ObjAccount.Qualifying_Type__c!=null && ObjAccount.Qualifying_Type__c !='Qualifying Purpose' && SRData.Processing_Personal_Data__c ==false) 
        {
            isvalid=false;
            SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
            
        }
        
        //V1.2
        if(ObjAccount.License_Activity__c !=null && ObjAccount.License_Activity__c.contains('316') && SRData.Use_Registered_Address__c ==false) 
        {
            isvalid=false;
            SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
            
        }
        
        
       
        //if(ObjAccount.Qualifying_Purpose_Type__c!=null && SRData.On_behalf_of_identical_name__c ==false && ObjAccount.Qualifying_Purpose_Type__c=='Structured Financing') 
        //  {
        //  isvalid=false;
        //  SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
        //}
        
        // End
        
        //System.debug('ZZZZ ObjAccount.Contact_Details_Provided__c-->'+ObjAccount.Contact_Details_Provided__c);
        //System.debug('ZZZZ SRData.id-->'+SRData.id);
        if(ObjAccount.Contact_Details_Provided__c!=true && SRData.id==null)//I hereby confirm that the company profile and officer details available on the DIFC Client Portal are accurate and up to date as of
        {ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You must submit Update details, add or remove management details before proceeding with this request.'));          isvalid=false;}
        
        //Below Logic updated by Sai for #6333
        if(ObjAccount.Exempted_from_UBO_Regulations__c != true && SRData.Registered_Office_Address__c==false)// I hereby confirm that there has been no changes to the Ultimate Beneficial Owners register since the last filing with the DIFC Registrar of Companies.
        {
            isvalid=false;
            SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
            // ApexPages.addMessage(myMsg);
        } 
        
        if(isISPV && SRData.Change_Activity__c == FALSE){
            isvalid=false;
            SRData.addError('Required information- ISPV Checkbox');
        }
        
        if(SRData.Registered_Phone_Number__c==false && SRData.Confirm_Change_of_Trading_Name__c=='Yes' && SRData.Legal_Structures__c=='LTD')// has an annual turnover of US$5 million or less, calculated on a consolidated basis including all subsidiaries. 
        {
            isvalid=false;
            SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
        }
        
        system.debug('##@@#$$'+SRData.Is_the_Initiator_or_a_transaction_party__c);
        if(ObjAccount.Qualifying_Type__c =='Qualifying Purpose' && ObjAccount.Qualifying_Purpose_Type__c =='Structured Financing' && SRData.Is_the_Initiator_or_a_transaction_party__c =='No' && String.isBlank(SRData.Bank_Address__c))
        {
            isvalid=false;
            SRData.addError('Please explain why the entity is no longer serving its qualifying purpose.');
        }
        if(String.isBlank(SRData.Confirm_Change_of_Trading_Name__c) && SRData.Legal_Structures__c=='LTD')// has an annual turnover of US$5 million or less, calculated on a consolidated basis including all subsidiaries. 
        {
            isvalid=false;
            SRData.addError('Required information- Annual Turnover');
        }
        
        if(SRData.Business_Activity__c==false)//I hereby confirm that all information required to be filed by the entity pursuant to DIFC Companies Law, Law No. 5 of 2018 and Companies Regulations in 
        {
            isvalid=false;
            SRData.addError('Please confirm all checkboxes in the Confirmation and Undertaking section');
        }
        
        /*
if(!isvalid)
{
ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Review all error messages below to correct your data.');
ApexPages.addMessage(myMsg);

}
*/
        
        
        //5.    If BC shareholder/founding member etc. exist without UBO, they cannot submit confirmation statement. Yes
        
        
        //9181 1.4 V
        //v1.9 started
        List<UBO_Secure__c> uboSecureRecords = DIFC_SharingBypass.fetchUBOSecureRecords(ObjAccount.BP_No__c);
        system.debug('uboSecureRecords '+uboSecureRecords.size());
        system.debug('uboSecureRecords bp no'+ObjAccount.BP_No__c);
        system.debug(' ObjAccount.Exempted_from_UBO_Regulations__c '+ObjAccount.Exempted_from_UBO_Regulations__c);
        system.debug(' ObjAccount.Company_UBOs__c '+ObjAccount.Company_UBOs__c);
        system.debug(' ObjAccount.Count_Body_Corporation_UBO__c '+ObjAccount.Count_Body_Corporation_UBO__c);
        system.debug(' ObjAccount.Was_any_UBO__c '+ObjAccount.Was_any_UBO__c);
        if(ObjAccount.Exempted_from_UBO_Regulations__c!=true && ObjAccount.Company_UBOs__c==null &&
           ObjAccount.Count_Body_Corporation_UBO__c==null && ObjAccount.Was_any_UBO__c==null && uboSecureRecords.size() == 0)
        {
            SRData.addError('We have no records of any filing of Ultimate Beneficial Owner (UBO) with the Registrar of Companies. Please proceed with submitting the request \'Update details, add or remove UBO \' or the \' Request for Exemption from DIFC UBO Regulations \' as applicable before submitting the Confirmation statement.');
            isvalid=false;
        }
        //v1.9 End
        /*
Changed 19-08-2020  v1.3
//List<Relationship__c> ListBCshareholders=[select Subject_Account__c,id from Relationship__c where Subject_Account__c=:SRData.Customer__c and Relationship_Type__c='Is Shareholder Of' and Object_Account__c!=null and Active__c=true];

if(!ListBCshareholders.isempty())
{
List<Relationship__c> ListBCOnwer=[select Subject_Account__c,id from Relationship__c where Subject_Account__c=:SRData.Customer__c and Relationship_Type__c='Beneficiary Owner' and Active__c=true];
if(ListBCOnwer.isempty()) {ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please Update your Shareholder details and provide Beneficiary Owner information.'); ApexPages.addMessage(myMsg); }

}
*/
        if(showSubSectorSection && String.isBlank(saveValue)){
            isvalid=false;
            SRData.addError('Please select at least one sub sector.');    
        }
        
        if(showSubSectorSection && 
           String.isNotBlank(saveValue) &&
           saveValue.contains('Other') && 
           (SRData.Current_Registered_Building__c == null || SRData.Current_Registered_Building__c == '')){
            isvalid=false;
            SRData.addError('Please specify other sub sector.');      
        }

        
        
        
        if(isvalid) {       
            if(!IsFEY_Provided && SRData.Agreement_Date__c!=null)
                //SRData.Financial_Year_End_mm_dd__c=ObjAccount.Financial_Year_End__c;    1.4 commnsted 
                SRData.Financial_Year_End_mm_dd__c=FEYValue(SRData.Agreement_Date__c);      
            try     
            {  
                
                
                if(showSubSectorSection){
                    
                    //System.debug('ZZZ subsector val-->'+ObjAccount.Subsector__c); 
                    //saveValue = ObjAccount?.Subsector__c;
                    
                    SRData.Company_Profile__c = saveValue;
                    //SRData.Current_Registered_Building__c = ObjAccount?.Other_SubSector__c;
                    System.debug('ZZZ SRDAta Commercial(Subsector)-->'+SRData.Company_Profile__c); 
                    
                    
                    //System.debug('ZZZ SRDAta Commercial-->'+ObjAccount.Subsector__c); 
                    
                    if(String.isBlank(saveValue) || (String.isNotBlank(saveValue) && !saveValue.contains('Other'))){
                        SRData.Current_Registered_Building__c = '';
                    }
                    
                    /*
                    //start v1.5 selva
                    List<String> financeValList = new List<string>();
                    for(Subsector_Mapping__mdt pickValues : [SELECT Id,DeveloperName,MasterLabel, Subsector_Values__c FROM Subsector_Mapping__mdt WHERE DeveloperName IN ('Banking')]){ 
                        financeValList = pickValues.Subsector_Values__c.split(';');                                                   
                    }
                    List<String> selectedValues= saveValue.split(';');
                    Boolean financeListAdded = false;
                    for(string itr:selectedValues ){
                        if(financeValList.contains(itr)){
                            financeListAdded  = true;
                        }
                    }
                    */
                    
                    
                    //System.debug('ZZZ save_M-->showSubSectorSection-->'+showSubSectorSection);
                    
                    //System.debug('ZZZ save_M-->ObjAccount.OB_Sector_Classification__c-->'+ObjAccount.OB_Sector_Classification__c);
                    
                    
                    //start v1.5 
                    Set<String> storedSubSectorMappingSet = new Set<String>();
                    
                    
                    storedSubSectorMappingSet.addAll(sectorsHavingMappedSubsectorMap != null && 
                                                     sectorsHavingMappedSubsectorMap.containsKey(ObjAccount.OB_Sector_Classification__c) ? 
                                                     sectorsHavingMappedSubsectorMap.get(ObjAccount.OB_Sector_Classification__c).Subsector_Values__c.split(';') :
                                                     new List<String>()
                                                    );
                    
                    System.debug('ZZZZ onSave_M-->storedSubSectorMappingSet-->'+JSON.serialize(storedSubSectorMappingSet));
                    
                    
                    List<String> selectedValues= saveValue != null ? saveValue.split(';') : new List<String>();
                    if(!selectedValues.isEMpty())
                        selectedValues.sort();
                    System.debug('ZZZZ onSave_M-->selectedValues-->'+JSON.serialize(selectedValues));
                    
                    //checking if atleast one mapped subsector value is selected against sector.
                    Boolean hasAtleastOneMatchingSubsectorValAgainstSector = false;
                    for(string itr:selectedValues ){
                        if(showSubSectorSection &&
                           storedSubSectorMappingSet != null && 
                           storedSubSectorMappingSet.contains(itr)
                          ){
                              
                              hasAtleastOneMatchingSubsectorValAgainstSector  = true;
                              
                          }
                    }
                    
                    //does all selected values are there is stored mapping (NOT REQUIRED NOW)
                    //hasAtleastOneMatchingSubsectorValAgainstSector = storedSubSectorMappingSet.containsAll(selectedValues);
                    
                    
                    System.debug('ZZZZ onSave_M-->hasAtleastOneMatchingSubsectorValAgainstSector-->'+hasAtleastOneMatchingSubsectorValAgainstSector);
                    
                    if(String.isNotBlank(saveValue) &&  ObjAccount.Company_Type__c== 'Financial - related' && !hasAtleastOneMatchingSubsectorValAgainstSector ){
                        List<String> secErrorList = sectorsHavingMappedSubsectorMap.get(ObjAccount.OB_Sector_Classification__c).Subsector_Values__c.split(';');
                        secErrorList.sort();
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select atleast one valid subsector values from following ( '+String.join(secErrorList,', ')+' )');         
                        ApexPages.addMessage(myMsg);
                        return null;                    
                    } 
                    //end v1.5 
                }
                
                System.debug('ZZZZ onSave_M-->SRData-->'+SRData);
                upsert SRData;
                PageReference acctPage = new ApexPages.StandardController(SRData).view();        
                acctPage.setRedirect(true);        
                return acctPage;
                
                    
            }catch (Exception e)     
            {
                System.debug('ZZZ Exception!! at line->'+e.getLineNumber()+'-->Msg-->'+e.getMessage());
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
                ApexPages.addMessage(myMsg);    
            }  
        }
        return null;
    }




    //v1.5
    public static Map<String,String> getPickListValuesByObjectAndFieldName(String objectName, String fieldApiName, Boolean allActiveValues){
        Map<String,String> finalPckLstValuesMap = new Map<String,String>();
        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName);
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldApiName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        for( Schema.PicklistEntry pickListVal : ple){
            if ( allActiveValues && pickListVal.isActive() ) {
                finalPckLstValuesMap.put(pickListVal.getLabel(),pickListVal.getValue());   
            }else{
                finalPckLstValuesMap.put(pickListVal.getLabel(),pickListVal.getValue());   
            }
        }
        return finalPckLstValuesMap;
    }
    

}