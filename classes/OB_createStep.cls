public without sharing class OB_createStep {
    
    @InvocableMethod
	public static void CreateRetriggerStep(list<account> lstAccount){

        set<string> accountIds = new set<string>();
        set<string> srIds = new set<string>();
        list<HexaBPM__Step__c> retriggerStepToBeInserted = new list<HexaBPM__Step__c>();

        for(account accObj : lstAccount){
            accountIds.add(accObj.id);
        }

        for(HexaBPM__Service_Request__c srObj : [select id,(select id from HexaBPM__Steps_SR__r WHERE HexaBPM__SR_Step__r.HexaBPM__Step_Template_Code__c = 'RETRIGGER_SECURITY' AND 
        HexaBPM__Status_Type__c != 'END' LIMIT 1 ) 
        from HexaBPM__Service_Request__c where (HexaBPM__Record_Type_Name__c = 'AOR_Financial' OR HexaBPM__Record_Type_Name__c = 'AOR_NF_R')
         AND HexaBPM__Customer__c IN:accountIds
        AND   HexaBPM__IsClosedStatus__c = false AND HexaBPM__Is_Rejected__c = false AND HexaBPM__IsCancelled__c = false]){
           /*  for(HexaBPM__SR_Steps__c step : srObj.HexaBPM__Steps_SR__r){
                srIds.add(step.id);  
            } */
            if( srObj.HexaBPM__Steps_SR__r.size() == 0){
                srIds.add(srObj.id);  
            }

        }

        
        HexaBPM__SR_Steps__c srStep = [select id,HexaBPM__Step_Template__c,HexaBPM__Start_Status__c,OwnerId,
        HexaBPM__Step_Template__r.OwnerId,HexaBPM__Do_not_use_owner__c,HexaBPM__Summary__c,HexaBPM__Step_No__c,
        HexaBPM__Estimated_Hours__c
         from HexaBPM__SR_Steps__c where HexaBPM__Step_Template_Code__c = 'RETRIGGER_SECURITY' LIMIT 1];

        for(string srId : srIds){
            /* HexaBPM__Step__c retriggerStep = new HexaBPM__Step__c();
            retriggerStep.HexaBPM__SR_Step__c = srStep.id; 
            retriggerStepToBeInserted.add(retriggerStep); */

            HexaBPM__Step__c insStp = new HexaBPM__Step__c();
            insStp.HexaBPM__SR__c = srId;
            insStp.HexaBPM__Step_Template__c = srStep.HexaBPM__Step_Template__c;
            insStp.HexaBPM__Status__c = srStep.HexaBPM__Start_Status__c;
            insStp.HexaBPM__SR_Step__c = srStep.Id;
            if (srStep.HexaBPM__Do_not_use_owner__c == false) {
                insStp.OwnerId = srStep.OwnerId;
            }else if (srStep.HexaBPM__Step_Template__c != null) {
                insStp.OwnerId = srStep.HexaBPM__Step_Template__r.OwnerId;
            }
            insStp.HexaBPM__Start_Date__c = system.today();
            insStp.HexaBPM__Summary__c = srStep.HexaBPM__Summary__c;
            insStp.HexaBPM__Step_No__c = srStep.HexaBPM__Step_No__c;
            insStp.HexaBPM__Sys_Step_Loop_No__c = string.valueOf(srStep.HexaBPM__Step_No__c) + '_1';
            if(srStep.HexaBPM__Estimated_Hours__c != null && label.HexaBPM.Business_Hours_Id!=null){
                ID BHId = Id.valueOf(label.HexaBPM.Business_Hours_Id);
                Long sla = srStep.HexaBPM__Estimated_Hours__c.longvalue();
                sla = sla * 60 * 60 * 1000L;
                datetime CreatedTime = system.now();
                insStp.HexaBPM__Due_Date__c = BusinessHours.add(BHId, CreatedTime, sla);
            }

            retriggerStepToBeInserted.add(insStp);
        }

        if(retriggerStepToBeInserted.size() > 0){
            insert retriggerStepToBeInserted;
        }

    }
}