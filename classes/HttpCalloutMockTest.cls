/*----------------------------------------------------------------------------------------------------------------------
Modification History     
-----------------------------------------------------------------------------------------------------------------------
 V.No    Date        	Updated By      	Description
 V0.0    26-12-2018  	Azfer          		This class is a mockup for HTTP callout which will be used in test classes to test the callouts
----------------------------------------------------------------------------------------------------------------------*/

@isTest
public class HttpCalloutMockTest implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    public HttpCalloutMockTest(Integer code, String status, String body, Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {

        HttpResponse res = new HttpResponse();
        for (String key : this.responseHeaders.keySet()) {
            res.setHeader(key, this.responseHeaders.get(key));
        }
        res.setBody(this.body);
        res.setStatusCode(this.code);
        res.setStatus(this.status);
        return res;
    }

}