@isTest
private class TestPortalSupportController {

    static testMethod void TestPortalSupport(){
        DIFCPortalSupportController supportCont = new DIFCPortalSupportController(); 
        
        ApexPages.currentPage().getParameters().put('recaptcha_challenge_field', '12345');
        ApexPages.currentPage().getParameters().put('recaptcha_response_field', '12345'); 
        
        supportCont.verify();        
        supportCont.addNewSupportTicket();        
		DIFCPortalSupportController.makeRequest('http://www.google.com', '12345');
    }
}