/******************************************************************************************
 *  Author   : Ravindra Babu Nagaboina
 *  Company  : NSI JLT
 *  Date     : 26th Oct 2014   
 *  Description : This class will be used to process the Change of Business Activity Information                       
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.2  03/Sep/2015      Shabbir             Added the validation to make "Provide justification for the change of business activity" field requried
 V1.3  18/Nov/2015      Shabbir             Added nature of business for exceptional customer
 V1.4  21/Dec/2015      Shabbir             Added company type logic change on the basis of selected activities
 V1.5  28/Jan/2016      Shabbir             Fixed the issue of existing activity removal and adding again
 V1.6  12/Mar/2016    Swati        Ticket - 2634, Cessation date
 V1.7  14/02/2017       Sravan              As per TKT # 3590 
 V1.8  08/04/2018     aRUN                 As per Ticket #4835 attach Activity Number with SR  
 V1.9  27-Nov-2019      Arun            Ticket-7716 Code improved and disabled Account calls multipal time 
 v2.1  23/3/2021		Shikha				#10035 - Show Registrar step only when Non Financial or Retail activity has been added (not company type)
 v2.2  23/3/2021		Shikha				#14177 - Show DNFBP Document only when DNFBP activity is added 
 v2.3  30/3/2021		Shikha				#10035 - Need to show DFSA Approval document only when Financial activity is added
 v2.4  30/3/2021		Shikha				#10035 - Removed validation of 'No Activity Added or Removed' and moved on Finalize 
*******************************************************************************************/

public without sharing class RocChangeOfBACls {
    
    /* Start of Properties for Dynamic Flow */
    public string strPageId{get;set;}
    public string strActionId{get;set;}
    public map<string,string> mapParameters;
    public Service_Request__c objSR{get;set;}
    public string strHiddenPageIds{get;set;}
    public string strNavigatePageId{get;set;}
    String SRId= '';
    public string pageTitle {get;set;}
    public string pageDescription {get;set;}
    /* End of Properties for Dynamic Flow */
    
    /* Start - Properties of Page */
        public Amendment__c Amendment {get;set;}
        public Boolean isEdit {get;set;}
        public list<LicenseAcctivityWrapper> LicenseAcctivities {get;set;}
        public list<LicenseAcctivityWrapper> SectorClassifications {get;set;}
        public Integer RowIndex {get;set;}
        public list<SelectOption> LicenseMasters {get;set;}
        string BusinessActivity = '';       
    /* End - Properties of Page */
    
    public string natureOfBusiness{get;set;} // v1.3
    public boolean exceptionalCustomer{get;set;} // v1.3
  public string companyType{get;set;} // v1.4
  public string sectorClassification{get;set;} // v1.4
  
  public Amendment__c objCessDate{get;set;} //v1.6
  public string amendRemoved{get;set;} //v1.6
  public string amendAdded{get;set;} //v1.6
  public date objEndDate{get;set;} //v1.6
    public string ActivitiesSelectedCode{get;set;} // V1.8 
      
     public Account objAccount;
    /*
        Default Contructor
    */
    public RocChangeOfBACls(){
        //moved all the Dynamic component code to the below method
        InitDynamicFlow();
        init();
        isEdit = true;  //V1.2 - change this field to true
    }
    
    /* Start of Dynamic Component methods */
    public void InitDynamicFlow(){
        /* Start of Properties Initialization for Dynamic Flow */
        mapParameters = new map<string,string>();
        if(apexpages.currentPage().getParameters()!=null)
            mapParameters = apexpages.currentPage().getParameters();
        
        SRId = mapParameters.get('Id');
        system.debug('mapParameters==>'+mapParameters);
        objSR = new Service_Request__c();
        objSR.Id = SRId;
        if(objSR.Id!=null){
             strPageId = mapParameters.get('PageId');
             if(strPageId!=null && strPageId!=''){
                for(Page__c page:[select id,Name,Page_Description__c from Page__c where Id=:strPageId]){
                    pageTitle = page.Name;
                    pageDescription = page.Page_Description__c;
                }
             }
             if(mapParameters!=null && mapParameters.get('FlowId')!=null){
                set<string> SetstrFields = Cls_Evaluate_Conditions.FetchObjectFields(mapParameters.get('FlowId'),'Service_Request__c');
                string strQuery = 'select Id,Customer__c';
                if(SetstrFields!=null && SetstrFields.size()>0){
                    for(String strFld:SetstrFields){
                        if(strFld.toLowerCase()!='id' && strFld.toLowerCase()!='customer__c')
                            strQuery += ','+strFld.toLowerCase();
                    }
                }
                    
                strQuery = strQuery+' from Service_Request__c where Id=:SRId';
                system.debug('strQuery===>'+strQuery);
                for(Service_Request__c SR:database.query(strQuery))
                    objSR = SR;
                strHiddenPageIds = PreparePageBlockUtil.getHiddenPageIds(mapParameters.get('FlowId'),objSR);
             }
        }
        /* End of Properties Initialization for Dynamic Flow */
    }
    
    public pagereference goTopage(){
        if(strNavigatePageId!=null && strNavigatePageId!=''){
            PreparePageBlockUtil objSidebarRef = new PreparePageBlockUtil();
            PreparePageBlockUtil.strSideBarPageId = strNavigatePageId;
            PreparePageBlockUtil.objSR = objSR;
            return objSidebarRef.getSideBarReference();
        }
        return null;
    }
    
    public Component.Apex.PageBlock getDyncPgMainPB(){ 
        PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = objSR;
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        return objPB.getDyncPgMainPB();
    }
    public pagereference DynamicButtonAction(){
        //perform the validations
        Boolean isNext = false;        
        for(Section_Detail__c objSec:[select id,Name,Component_Label__c,Navigation_Direction__c from Section_Detail__c where Id=:strActionId]){
            if(objSec.Navigation_Direction__c=='Forward')
                isNext = true;
        }        
        
        if(isNext){
            if(Amendment.Company_Type__c == null || Amendment.Company_Type__c == ''){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please select the Company Type.'));
                return null;
            }
            if(Amendment.Sector_Classification__c == null || Amendment.Sector_Classification__c == ''){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please select the Sector Classification.'));
                return null;
            }
            if(LicenseAcctivities.isEmpty() && Amendment.Company_Type__c == 'Financial - related'){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please select at least one License Activty.'));
                return null;
            }
            if(SectorClassifications.isEmpty() && Amendment.Company_Type__c != 'Financial - related'){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please select at least one Business Activity.'));
                return null;
            }
            //v1.2 - Start
            if(Amendment.Data_Processing_Procedure__c == null || Amendment.Data_Processing_Procedure__c == ''){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please provide justification for the change of business activity.'));
                return null;
            }//v1.2 - End 
            //V1.6
            if(amendRemoved == amendAdded){
              system.debug('--amendRemoved---'+amendRemoved);
              if(amendRemoved == amendAdded && (amendRemoved == '' || amendRemoved == null)){
                  //v2.4 - Removed validation
                //Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'No Activity added or removed.'));
                  //return null;
              }
              else{
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Activity added is the same as the one removed, cannot proceed with submission'));
                  return null;
              }
            }              
        }
        
        return NextPage();
    }
    
    public pagereference NextPage()
    {
        system.debug('strActionId==>'+strActionId);
        PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = objSR;
        PreparePageBlockUtil.ActionId = strActionId;
        
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        pagereference pg = objPB.getButtonAction();
        system.debug('pg==>'+pg);
        return pg;
        
        
    }
    /* End of Dynamic Component methods */
    
    public void init(){
        Amendment = new Amendment__c();
        objCessDate = new Amendment__c();
        Amendment.ServiceRequest__c = SRId;
        Amendment.Amendment_Type__c = 'Change of Business Activity';
        for(Amendment__c objAmd : [select Id,Name,ServiceRequest__c,Amendment_Type__c,Company_Type__c,Company_Type_Changed__c,Sector_Classification__c,Sector_Classification_Changed__c,License_Activity_Changed__c,Data_Processing_Procedure__c from Amendment__c where ServiceRequest__c=:SRId AND Amendment_Type__c='Change of Business Activity']){
            Amendment = objAmd;
            BusinessActivity = objAmd.Sector_Classification__c;
        }
        System.debug('!!@@@!!'+Amendment);
        LicenseAcctivities = new list<LicenseAcctivityWrapper>();
        SectorClassifications = new list<LicenseAcctivityWrapper>();
        for(Amendment__c objAmd : [select Id,Name,Capacity__c,ServiceRequest__c,Amendment_Type__c,End_Date__c,Registration_Date__c,License_Activity_Master__c,License_Activity_Master__r.Name,License_Activity_Master__r.Sector_Classification__c,License_Activity_Master__r.Type__c from Amendment__c where ServiceRequest__c=:SRId AND (Amendment_Type__c='License Activity' OR Amendment_Type__c='Business Activity') AND (End_Date__c = null)]){   // V1.7  Removed OR End_Date__c >=:system.today()         
            LicenseAcctivityWrapper obj = new LicenseAcctivityWrapper();
            obj.LicenseActivityName =  objAmd.License_Activity_Master__r.Name;
            obj.ActivityId = objAmd.Id;
            obj.LicenseActivityType =  objAmd.License_Activity_Master__r.Type__c;
            obj.BusinessActivity = objAmd.License_Activity_Master__r.Sector_Classification__c;
            obj.MasterId = objAmd.License_Activity_Master__c;
            obj.LAId = objAmd.Capacity__c;
            if(Amendment.Company_Type__c=='Financial - related'){
                obj.Index = LicenseAcctivities.size();
                LicenseAcctivities.add(obj);
            }else if(Amendment.Company_Type__c=='Non - financial' || Amendment.Company_Type__c=='Retail'){
                obj.Index = SectorClassifications.size();
                SectorClassifications.add(obj);
            }   
        }
        //v1.3, v1.4
        if(objSR != null){
            system.debug('!!!@@@'+objSR.Customer__c);
          
        objAccount=[select Id,Nature_of_business__c,Qualifying_Type__c,Exceptional_Customer__c,Company_Type__c,Sector_Classification__c,(select Id,Activity__c,End_Date__c,Start_Date__c,Activity__r.Name,Activity__r.Type__c,Activity__r.Sector_Classification__c from License_Activities__r where End_Date__c = null OR End_Date__c >=:system.today() order by Activity__r.Name) from Account where Id =:objSR.Customer__c limit 1]; //1.18 V
        
        natureOfBusiness = objAccount.Nature_of_business__c;
        exceptionalCustomer = objAccount.Exceptional_Customer__c;      
        companyType = objAccount.Company_Type__c;
        sectorClassification = objAccount.Sector_Classification__c;  
          //}          
        }

        
    }   
    
    public void pageLoad(){
        if(Amendment.Id == null){
            list<Amendment__c> lstAmendments = new list<Amendment__c>();
            Amendment__c objAmendment;
            system.debug('objSR is : '+objSR);
            system.debug('objSR is : '+objSR.Customer__c);
         //   for(Account objAccount : [select Id,Company_Type__c,Sector_Classification__c, from Account where Id =:objSR.Customer__c])
            //{
                objAmendment = new Amendment__c();
                objAmendment.Company_Type__c = objAccount.Company_Type__c;
                objAmendment.Sector_Classification__c = objAccount.Sector_Classification__c;
                objAmendment.ServiceRequest__c = SRId;
                objAmendment.Amendment_Type__c = 'Change of Business Activity';
                lstAmendments.add(objAmendment);
                for(License_Activity__c objLA : objAccount.License_Activities__r){
                    objAmendment = new Amendment__c();
                    objAmendment.ServiceRequest__c = SRId;
                    objAmendment.License_Activity_Master__c  = objLA.Activity__c;
                    if(objAccount.Company_Type__c=='Financial - related'){
                        objAmendment.Amendment_Type__c = 'License Activity';
                    }else if(objAccount.Company_Type__c=='Non - financial' || objAccount.Company_Type__c=='Retail'){
                        objAmendment.Amendment_Type__c = 'Business Activity';
                    }
                    objAmendment.Existing_Contact_Company__c = true;
                    objAmendment.End_Date__c = objLA.End_Date__c;
                    objAmendment.Registration_Date__c = objLA.Start_Date__c;
                    objAmendment.Capacity__c = objLA.Id;
                    lstAmendments.add(objAmendment);
                }
           // }
            system.debug('!!@@!!!'+lstAmendments);
            if(!lstAmendments.isEmpty())
                insert lstAmendments;
            init();
        }
    }
    
    public void EditActivity(){
        isEdit = true;
    }
    
    public void ChangeCompanyType(){
        Boolean isError = false;
        if(Amendment.Company_Type__c != 'Financial - related' && LicenseAcctivities.isEmpty() == false){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please remove existing License Activities'));
            isError = true;
        }else if((Amendment.Company_Type__c != 'Non - financial' && Amendment.Company_Type__c != 'Retail' )&& SectorClassifications.isEmpty() == false){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please remove existing Business Activities.'));
            isError = true;
        }
        if(BusinessActivity != Amendment.Sector_Classification__c){
            if(!LicenseAcctivities.isEmpty()){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please remove existing License Activities.'));
                isError = true;
            }
            if(!SectorClassifications.isEmpty()){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please remove existing Business Activities.'));
                isError = true;
            }
        }
        if(isError){
            for(Amendment__c objAmd : [select Id,Name,ServiceRequest__c,Amendment_Type__c,Company_Type__c,Company_Type_Changed__c,Sector_Classification__c,Sector_Classification_Changed__c,License_Activity_Changed__c,Data_Processing_Procedure__c from Amendment__c where ServiceRequest__c=:SRId AND Amendment_Type__c='Change of Business Activity']){
                Amendment = objAmd;
            }
        }
    }
        
    public void RemoveLicenseActivity(){
        amendRemoved = '';
        if(Amendment.Company_Type__c == 'Financial - related'){
            if(RowIndex != null && LicenseAcctivities != null && RowIndex < LicenseAcctivities.size()){
                if(LicenseAcctivities[RowIndex].ActivityId != null){
                    Amendment__c objLA = new Amendment__c(Id=LicenseAcctivities[RowIndex].ActivityId);
                    if(LicenseAcctivities[RowIndex].LAId != null && LicenseAcctivities[RowIndex].LAId != ''){
                          //v1.6 - copying correct end date here but on approval end date is system.today()
                          // needs to change that and copy this date once business confirms
                          objLA.End_Date__c = objEndDate;
                          objLA.Date_of_Cessation__c = objEndDate;
                            objLA.Confirmation_of_Cessation_Date__c = true;
                        update objLA;
                        amendRemoved = objLA.id;
                    }else{
                      amendRemoved = objLA.id;
                        delete objLA;
                    }
                }
                LicenseAcctivities.remove(RowIndex);
                Integer i=0;
                for(LicenseAcctivityWrapper obj : LicenseAcctivities){
                    obj.Index = i;
                    i++;
                }
            }   
        }else if(Amendment.Company_Type__c == 'Non - financial' || Amendment.Company_Type__c=='Retail'){
            if(RowIndex != null && SectorClassifications != null && RowIndex < SectorClassifications.size()){
                if(SectorClassifications[RowIndex].ActivityId != null){
                    Amendment__c objLA = new Amendment__c(Id=SectorClassifications[RowIndex].ActivityId);
                    if(SectorClassifications[RowIndex].LAId != null && SectorClassifications[RowIndex].LAId != ''){
                          //v1.6
                          objLA.End_Date__c = objEndDate;
                          objLA.Date_of_Cessation__c = objEndDate;
                            objLA.Confirmation_of_Cessation_Date__c = true;
                        update objLA;
                        amendRemoved = objLA.id;
                    }else{
                      amendRemoved = objLA.id;
                        delete objLA;
                    }   
                }
                SectorClassifications.remove(RowIndex);
                Integer i=0;
                for(LicenseAcctivityWrapper obj : SectorClassifications){
                    obj.Index = i;
                    i++;
                }
            }
        }
    }
    
    public void AddNewRow(){
        Boolean isError = false;
        if(Amendment.Company_Type__c != 'Financial - related' && LicenseAcctivities.isEmpty() == false){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please remove existing License Activities'));
            isError = true;
        }else if((Amendment.Company_Type__c !='Non - financial' && Amendment.Company_Type__c != 'Retail') && SectorClassifications.isEmpty() == false){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please remove existing Business Activities.'));
            isError = true;
        }
        /************Changed by Kaavya on Jan 6th, 2015
        if(Amendment.Company_Type__c !='Financial - related' && SectorClassifications.size() >= 1){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'You cannot add more that one Business Activity.'));
            isError = true;
        }*/
        /* v1.4
        if(Amendment.Sector_Classification__c == null || Amendment.Sector_Classification__c == ''){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please select the Sector Classification.'));
            isError = true;
        }*/
        if(!isError){
            LicenseAcctivityWrapper obj = new LicenseAcctivityWrapper();
            //obj.BusinessActivity = Amendment.Sector_Classification__c;
            if(Amendment.Company_Type__c=='Financial - related'){
                obj.LicenseActivityType =  'License Activity';
                obj.Index = LicenseAcctivities.size();
                LicenseAcctivities.add(obj);
            }else if(Amendment.Company_Type__c=='Non - financial' || Amendment.Company_Type__c=='Retail'){
                obj.LicenseActivityType =  'Business Activity';
                obj.Index = SectorClassifications.size();
                SectorClassifications.add(obj);
            }
            LicenseMasters = new list<SelectOption>();
            LicenseMasters.add(new SelectOption('','--None--'));
            String Type = (Amendment.Company_Type__c=='Financial - related')?'License Activity':'Business Activity';
           //v1.2 - Commented the old code - DIFC Shabbir Ahmed - 20/Sep/2015 - Start
           /*
           for(License_Activity_Master__c objLAM : [select Id,Name from License_Activity_Master__c where Type__c =:Type AND Sector_Classification__c=:Amendment.Sector_Classification__c AND Enabled__c = true order by Name]){
                LicenseMasters.add(new SelectOption(objLAM.Id,objLAM.Name));
           }
           */
           //v1.2 - End
           
           
            for(License_Activity_Master__c objLAM : [select Id,Name,Qualifying_Type__c,OB_Sector_Classification__c,Sector_Classification__c from
                                                      License_Activity_Master__c where Enabled__c = true order by Name]){
                //V1.9 
                if(objAccount.Qualifying_Type__c!=null)
                {                    
                    if(objAccount.Qualifying_Type__c==objLAM.Qualifying_Type__c)
                    LicenseMasters.add(new SelectOption(objLAM.Id,objLAM.Name));                          
                }
                
                else
                LicenseMasters.add(new SelectOption(objLAM.Id,objLAM.Name));
            }                 
        }
    }
    
    public void SaveLicenseActivity(){
        try{
            amendAdded = '';
            if(Amendment.Company_Type__c=='Financial - related'){
                if(RowIndex != null && LicenseAcctivities != null && RowIndex < LicenseAcctivities.size()){
                    if(LicenseAcctivities[RowIndex].MasterId != null){
                        for(LicenseAcctivityWrapper objLicenseAcctivityWrapper : LicenseAcctivities){
                            if(objLicenseAcctivityWrapper.ActivityId != null && objLicenseAcctivityWrapper.MasterId == LicenseAcctivities[RowIndex].MasterId){
                                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Selected License Master already in added.'));
                                return;
                            }
                        }
                        //SaveHeader();
                        //v1.5 - start
                        boolean isUpdate = false;
                        Amendment__c objAmendment = new Amendment__c();
            for(Amendment__c objAmd : [select Id,Name,Capacity__c,License_Activity_Master__c from Amendment__c 
              where ServiceRequest__c=:SRId AND (Amendment_Type__c='License Activity') AND (End_Date__c != null)]){
              if(LicenseAcctivities[RowIndex].MasterId == objAmd.License_Activity_Master__c){
                objAmendment.Id = objAmd.Id;
                isUpdate = true;
              }
            }
                        system.debug('isUpdate: ' + isUpdate);                       
                        if(isUpdate){
                      objAmendment.End_Date__c = null;
                        update objAmendment;
                                                   
                        }else{ //v1.5 - end
                          objAmendment.License_Activity_Master__c = LicenseAcctivities[RowIndex].MasterId;
                          objAmendment.ServiceRequest__c = SRID;
                          objAmendment.Amendment_Type__c = LicenseAcctivities[RowIndex].LicenseActivityType;
                          insert objAmendment;
                        }                        
                        SaveHeader(); // v1.4
                        for(Amendment__c objAmd : [select Id,Name,ServiceRequest__c,Amendment_Type__c,End_Date__c,Registration_Date__c,License_Activity_Master__c,License_Activity_Master__r.Name,License_Activity_Master__r.Type__c from Amendment__c where id=:objAmendment.Id]){
                            amendAdded = objAmendment.id;
                            LicenseAcctivities[RowIndex].LicenseActivityName =  objAmd.License_Activity_Master__r.Name;
                            LicenseAcctivities[RowIndex].ActivityId = objAmd.Id;
                            LicenseAcctivities[RowIndex].LicenseActivityType =  objAmd.License_Activity_Master__r.Type__c;
                        }
                    }
                }       
            }else if(Amendment.Company_Type__c=='Non - financial' || Amendment.Company_Type__c=='Retail'){
                if(RowIndex != null && SectorClassifications != null && RowIndex < SectorClassifications.size()){
                    if(SectorClassifications[RowIndex].MasterId != null){
                        for(LicenseAcctivityWrapper objLicenseAcctivityWrapper : SectorClassifications){
                            system.debug('objLicenseAcctivityWrapper.ActivityId '+objLicenseAcctivityWrapper.ActivityId);
                            system.debug('objLicenseAcctivityWrapper.MasterId '+objLicenseAcctivityWrapper.MasterId);
                            system.debug('SectorClassifications[RowIndex].MasterId '+SectorClassifications[RowIndex].MasterId);
                            if(objLicenseAcctivityWrapper.ActivityId != null && objLicenseAcctivityWrapper.MasterId == SectorClassifications[RowIndex].MasterId){
                                
                                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Selected License Master already in added.'));
                                return;
                            }
                        }
                        system.debug('SectorClassifications: ' + SectorClassifications);
                        
                        //SaveHeader();
                        //v1.5 - start
                        boolean isUpdate = false;
                        Amendment__c objAmendment = new Amendment__c();
            for(Amendment__c objAmd : [select Id,Name,Capacity__c,License_Activity_Master__c from Amendment__c 
              where ServiceRequest__c=:SRId AND (Amendment_Type__c='Business Activity') AND (End_Date__c != null)]){
              if(SectorClassifications[RowIndex].MasterId == objAmd.License_Activity_Master__c){
                objAmendment.Id = objAmd.Id;
                isUpdate = true;
              }
            }
                        system.debug('isUpdate: ' + isUpdate);                       
                        if(isUpdate){
                      objAmendment.End_Date__c = null;
                        update objAmendment;
                                                   
                        }else{ //v1.5 - end
                          objAmendment.License_Activity_Master__c = SectorClassifications[RowIndex].MasterId;
                          objAmendment.ServiceRequest__c = SRID;
                          objAmendment.Amendment_Type__c = SectorClassifications[RowIndex].LicenseActivityType;
                          insert objAmendment;  
                        }                     
                        SaveHeader(); // v1.4
                        for(Amendment__c objAmd : [select Id,Name,ServiceRequest__c,Amendment_Type__c,End_Date__c,Registration_Date__c,License_Activity_Master__c,License_Activity_Master__r.Name,License_Activity_Master__r.Type__c from Amendment__c where id=:objAmendment.Id]){
                            amendAdded = objAmendment.id;
                            SectorClassifications[RowIndex].LicenseActivityName =  objAmd.License_Activity_Master__r.Name;
                            SectorClassifications[RowIndex].ActivityId = objAmd.Id;
                            SectorClassifications[RowIndex].LicenseActivityType =  objAmd.License_Activity_Master__r.Type__c;
                        }
                    }
                }
            }
            system.debug('-amendAdded-----'+amendAdded);
            init();
        }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please select the Company Type.'));
        }
    }
    
    public void SaveHeader(){
      
       // for(Account objAccount : [select Id,Company_Type__c,Sector_Classification__c from Account where Id =:objSR.Customer__c]){
            /*
            if(Amendment.Company_Type__c != objAccount.Company_Type__c){
                Amendment.Company_Type_Changed__c = true;
            }
            if(Amendment.Sector_Classification__c != objAccount.Sector_Classification__c){
                Amendment.Sector_Classification_Changed__c = true;
            }*/
            
        // V1.4
        set<string> setAllActivities = new set<string>();
        string financialActivity = '';
        string nonFinancialActivity = '';
        string retailActivity = '';
        //v2.1 Started
        Boolean checkRegistrarStep = false;
        Boolean checkDNFBPDocGen = false;//v2.2 Started
        Boolean checkDNFBPDeRegGen = false;Boolean checkDFSAApproval = false;//v2.3
        
        //V1.8 Code Start
        Set<string> FinalActivite=new set<string>();
            ActivitiesSelectedCode='';
          for(Amendment__c objAmd : [select Id, Amendment_Type__c,capacity__c,End_Date__c,License_Activity_Master__r.Sys_Code__c,License_Activity_Master__r.Sector_Classification__c,License_Activity_Master__r.Type__c,
                                     License_Activity_Master__r.Sys_Activity_Type__c, License_Activity_Master__r.OB_DNFBP__c, Existing_Contact_Company__c
          from Amendment__c where ServiceRequest__c=:SRId  AND (Amendment_Type__c='License Activity' OR Amendment_Type__c='Business Activity')  
          order by createddate])
          {
              if(objAmd.License_Activity_Master__r.Sys_Code__c!=null)
              {
                   if(objAmd.End_Date__c==null)
                      {
                          //v2.1
                          if(objAmd.License_Activity_Master__r.Sys_Activity_Type__c != 'Financial - related') {checkRegistrarStep = true;
                          }
                          if(objAmd.Existing_Contact_Company__c == false && objAmd.License_Activity_Master__r.OB_DNFBP__c == 'Yes'){ checkDNFBPDocGen = true;
                          }
                          FinalActivite.add(objAmd.License_Activity_Master__r.Sys_Code__c);
                      }
                      else if(objAmd.End_Date__c >system.today()){                        
                          FinalActivite.add(objAmd.License_Activity_Master__r.Sys_Code__c);
                      }
                      else{                        
                          FinalActivite.remove(objAmd.License_Activity_Master__r.Sys_Code__c);
                      }
                  //v2.1
                  if(objAmd.License_Activity_Master__r.OB_DNFBP__c == 'Yes' && (objAmd.End_Date__c != Null || objAmd.End_Date__c > System.today())){checkDNFBPDeRegGen = true;
                  }
                  //v2.3
                  if(objAmd.License_Activity_Master__r.Sys_Activity_Type__c == 'Financial - related' && (objAmd.Capacity__c == '' || objAmd.Capacity__c == NULL)){checkDFSAApproval = true;
                  }
              }
             
          }            
          System.debug('===FinalActivite=='+FinalActivite);
          for(string code:FinalActivite)
          {
             
                  //Only work when added Activity not on removed activite becase filter is End_Date__c = null OR End_Date__c >=:system.today()
                //in case of remove End_Date__c != null  
                ActivitiesSelectedCode+=code+',';//V1.8
          }
        //V1.8 Code End
        
        for(Amendment__c objAmd : [select Id, Amendment_Type__c,License_Activity_Master__r.Sector_Classification__c,License_Activity_Master__r.Type__c,License_Activity_Master__r.Sys_Activity_Type__c 
          from Amendment__c where ServiceRequest__c=:SRId AND (Amendment_Type__c='License Activity' OR Amendment_Type__c='Business Activity') AND (End_Date__c = null OR End_Date__c >=:system.today()) 
          order by createddate]){
      if(objAmd.License_Activity_Master__r.Type__c =='License Activity'){
        setAllActivities.add('Financial - related');
        if(financialActivity == '')
          financialActivity = objAmd.License_Activity_Master__r.Sector_Classification__c;
      }else if(objAmd.License_Activity_Master__r.Type__c == 'Business Activity'){
        setAllActivities.add(objAmd.License_Activity_Master__r.Sys_Activity_Type__c);
         if(objAmd.License_Activity_Master__r.Sys_Activity_Type__c == 'Non - financial' && nonFinancialActivity == '')
           nonFinancialActivity = objAmd.License_Activity_Master__r.Sector_Classification__c;
         else if(objAmd.License_Activity_Master__r.Sys_Activity_Type__c == 'Retail' && retailActivity == '')
           retailActivity = objAmd.License_Activity_Master__r.Sector_Classification__c;           
      }      
        }
        
        system.debug('financialActivity' + financialActivity);
        system.debug('nonFinancialActivity' + nonFinancialActivity);
        system.debug('retailActivity' + retailActivity);
        system.debug('setAllActivities' + setAllActivities);
                
        if(objAccount.Company_Type__c == 'Financial - related'){
          if(!setAllActivities.contains(objAccount.Company_Type__c) && !setAllActivities.contains('Retial')){
            Amendment.Company_Type_Changed__c = true;
            Amendment.Company_Type__c = 'Non - financial';
            Amendment.Sector_Classification__c = nonFinancialActivity;
            Amendment.Sector_Classification_Changed__c = true;
          }
          else if(!setAllActivities.contains('Non - financial') && setAllActivities.contains('Retial')){
            Amendment.Company_Type_Changed__c = true;
            Amendment.Company_Type__c = 'Retail';
            Amendment.Sector_Classification__c = retailActivity;
            Amendment.Sector_Classification_Changed__c = true;            
          }          
        } else if(objAccount.Company_Type__c == 'Non - financial'){
          if(setAllActivities.contains('Financial - related')){
            Amendment.Company_Type_Changed__c = true;
            Amendment.Company_Type__c = 'Financial - related';
            Amendment.Sector_Classification__c = financialActivity;
            Amendment.Sector_Classification_Changed__c = true;            
          }
          else if(!setAllActivities.contains(objAccount.Company_Type__c) && setAllActivities.contains('Retail')){
            Amendment.Company_Type_Changed__c = true;
            Amendment.Company_Type__c = 'Retail';
            Amendment.Sector_Classification__c = retailActivity;
            Amendment.Sector_Classification_Changed__c = true;              
          }          
        }
    else if(objAccount.Company_Type__c == 'Retail'){
          if(setAllActivities.contains('Financial - related')){
            Amendment.Company_Type_Changed__c = true;
            Amendment.Company_Type__c = 'Financial - related';
            Amendment.Sector_Classification__c = financialActivity;
            Amendment.Sector_Classification_Changed__c = true;            
          }
          else if(!setAllActivities.contains(objAccount.Company_Type__c) && setAllActivities.contains('Retail')){
            Amendment.Company_Type_Changed__c = true;
            Amendment.Company_Type__c = 'Retail';
            Amendment.Sector_Classification__c = retailActivity;
            Amendment.Sector_Classification_Changed__c = true;              
          }          
        }              
            
       // }
        objSR.Commercial_Activity__c=ActivitiesSelectedCode;//V1.8
        objSR.Activity_NOC_Required__c = checkRegistrarStep; //v2.1
        objSR.Adhering_to_Safe_Horbour__c = checkDNFBPDocGen; //v2.2
        objSR.Court_Order__c = checkDNFBPDeRegGen;objSR.DFSA_Approval__c = checkDFSAApproval; //v2.3
        update objSR;//V1.8
        update Amendment;
    }
    
    public void SaveActivity(){
        try{
            SaveHeader();
            if(Amendment.Company_Type__c == null || Amendment.Company_Type__c == ''){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please select the Company Type.'));
                return;
            }
            if(Amendment.Sector_Classification__c == null || Amendment.Sector_Classification__c == ''){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please select the Business Activity.'));
                return;
            }
            //v1.2 - start
            if(Amendment.Data_Processing_Procedure__c == null || Amendment.Data_Processing_Procedure__c == ''){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please provide justification for the change of business activity.'));
                return;
            }             
            //isEdit = false;
            //v1.2 - end
            init();
        }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,ex.getMessage()));
        }
    }
    public void CancelActivity(){
        isEdit = false;
        init();
    }
        
    public class LicenseAcctivityWrapper {
        public string ActivityId {get;set;} 
        public string MasterId {get;set;} 
        public string LicenseActivityName {get;set;}
        public string LicenseActivityType {get;set;}
        public string BusinessActivity {get;set;}
        public Integer Index {get;set;}
        public string LAId {get;set;}
    }
       
}