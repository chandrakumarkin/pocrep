public class GSMedicalStatusUpdate {

 @InvocableMethod(label='Check Medical Fitness Step is completed with Doc' description='Check Medical Fitness Step is completed with Doc')
 
  public static void createLineManagerForRejection(List<id> ids) {
        GSMedicalStatusUpdate.HealthInsuranceUploadCheck(ids);        
    }
    
   @future(Callout=true)
   public static void HealthInsuranceUploadCheck(List<id> ids){
   
   system.debug(ids +'ids');
   
   List<step__c> steps =[select id,SR__r.id from step__c where id =:ids[0] limit 1];
   
   if(steps.size()>0){
     
    List<sr_doc__c> srDoc = [select id,Service_Request__c,Service_Request__r.External_SR_Status__c from SR_Doc__c where name ='Health Insurance Certificate' and Status__c = 'Pending Upload' and Service_Request__r.id = :steps[0].SR__r.id limit 1];
    
    if(srDoc.size()>0){
    
    system.debug('srDoc.inside');
       SR_doc__c srDocUpd = new SR_doc__c();
       srDocUpd.id = srDoc[0].id;
       srDocUpd.SR_Status__c = srDoc[0].Service_Request__r.External_SR_Status__c;
       update srDocUpd; 
       system.debug('srDoc.update'+srDocUpd.SR_Status__c);
        
         SR_Status__c SrStatus = [select id from SR_Status__c where Code__c ='AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED' limit 1];
     
         Service_request__c sr = new Service_request__c();
     
         sr.id = srDoc[0].Service_request__c;
         sr.External_SR_Status__c = SrStatus.id;
         sr.internal_sr_status__c = SrStatus.id;  
     
         update sr;  
    }else{
    
    
        //List<sr_doc__c> srDoc1 = [select id,Service_Request__c,Service_Request__r.External_SR_Status__c from SR_Doc__c where name ='Health Insurance Certificate' and Status__c = 'Uploaded' and Service_Request__r.id = :steps[0].SR__r.id limit 1];
    
       //if(srDoc1.size()>0){
        
         SR_Status__c SrStatus = [select id from SR_Status__c where Code__c ='Medical fitness certificate received from DHA' limit 1];
     
         Service_request__c sr = new Service_request__c();
     
         //sr.id = srDoc1[0].Service_request__c;
         sr.id = steps[0].sr__c;
         sr.External_SR_Status__c = SrStatus.id;
         sr.internal_sr_status__c = SrStatus.id;  
     
         update sr;  
      //  }

    
    }
    
   }
    
   }

}