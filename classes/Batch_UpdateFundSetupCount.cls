/*********************************************************************************************************************
*  Name      : Batch_UpdateFundSetupCount 
*  Author    : Shikha Khanuja
*  Purpose   : Batch class to update fund setup count 
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
v1.0		 	Shikha				08/03/2021				 Ticket #9563 - to update fund setup count 
************************************************************************************************************************/
public class Batch_UpdateFundSetupCount implements Schedulable,Database.Batchable<sObject>{
    public Database.QueryLocator start (Database.BatchableContext bc){   
        String sectorClassification = 'Investment Fund';       
        String rocStatusActive = 'Active';
        String rocStatusRenewal = 'Not Renewed';
     	String q = 'SELECT Id, Hosting_Company__c, FundSetupCount__c, OB_Sector_Classification__c FROM Account WHERE ( ROC_Status__c = '+'\''+rocStatusActive+'\''+' OR ROC_Status__c = '+'\''+rocStatusRenewal+'\''+') AND ( ' +'OB_Sector_Classification__c = '+'\''+sectorClassification+'\''+' OR Sector_Classification__c = '+'\''+sectorClassification+'\''+' )';
        system.debug('### q '+q);
        return Database.getQueryLocator(q);
    }    
    
    public void execute(Database.BatchableContext bc, List<Account> accList){
        //Set<Id> hostingCompanyIds = new Set<Id>();
        Map<Id,List<Account>> mapOfHostingVsListAccounts = new Map<Id,List<Account>>();
        for(Account a : accList){
            //hostingCompanyIds.add(a.Id);
            if(mapOfHostingVsListAccounts.containsKey(a.Hosting_Company__c)){
                mapOfHostingVsListAccounts.get(a.Hosting_Company__c).add(a);
            }
            else{
                mapOfHostingVsListAccounts.put(a.Hosting_Company__c,new List<Account> {a});
            }
        }
        system.debug('### mapOfHostingVsListAccounts '+mapOfHostingVsListAccounts);
        List<Account> updatedAccounts = new List<Account>();
        Map<Id,Account> mapOfAccountIds = new Map<Id,Account>();
        for(Account a : accList){
            if(a.Hosting_Company__c != null && mapOfHostingVsListAccounts.containsKey(a.Hosting_Company__c)){
                Account acc = new Account();
                acc.Id = a.Hosting_Company__c;
                acc.FundSetupCount__c = mapOfHostingVsListAccounts.get(a.Hosting_Company__c).size();
                //updatedAccounts.add(acc);
                mapOfAccountIds.put(acc.Id,acc);
            }
        }
        system.debug('### mapOfAccountIds '+mapOfAccountIds);
        if(!mapOfAccountIds.isEmpty()){
            /*List<Account> updatedAccountList = new List<Account>();
            for(Account acc : updatedAccounts){
                mapOfAccountIds.put(acc.Id,acc);
            }*/
            system.debug('### mapOfAccountIds '+mapOfAccountIds);
            update mapOfAccountIds.values();
        }
    }
    
    public void finish (Database.BatchableContext bc){
        system.debug('### account field is updated');
    }
    
    public void execute(SchedulableContext ctx){
        
    }
}