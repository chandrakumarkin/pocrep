@isTest(SeeAllData=true)
private class CRSTest {

    static testMethod void insertCaseTest()
    {
  
  
      CRS_Master__c ObjYear=new CRS_Master__c();
    ObjYear.Name='1985';
  insert  ObjYear;
        
  system.debug(CRS_generate_XML.generateXml('Demo'));
     //system.debug(CRS_generate_XML.generateMassXml('Demo'));  
     
    
    
  
  
user u=[select id,Accountid from user where profileid='00e200000012hZD' and isActive=true limit 1];
System.runAs(u) 
{
    CRS_Form__c ObjFormd=new CRS_Form__c ();
 
    ObjFormd.CRS_Year__c=ObjYear.id;
    ObjFormd.FI_ID__c='Deom';
    ObjFormd.Official_FI_Legal_Name__c='Deom';
    ObjFormd.Holder_Type__c='CRS101';
    ObjFormd.Account_Balance__c=11000;
    ObjFormd.Tax_Residence_Country_new__c='India';
    ObjFormd.Account_Number__c='DEEEE';
    
    ObjFormd.Status__c='Submitted';
    insert ObjFormd;
    
      CRS_Client__c ClintP=new CRS_Client__c(); 
    ClintP.Address__c='';
    ClintP.City__c='';
    ClintP.Client_Type__c='Person';//Company//Controlling=Controlling_Person_Type__c
    ClintP.Controlling_Person_Type__c='CRS801';
    ClintP.Country__c='United Kingdom';
    ClintP.CRS_Form__c=ObjFormd.id;
    ClintP.Date_of_Birthday__c=Date.today();
    ClintP.First_Name__c='United Kingdom';
    ClintP.IN__c='2222';
    ClintP.Last_Name__c='United Kingdom';
    ClintP.TIN__c='sdasdasd';
    //ObjClintCP.Master_Client__c=ObjClint.id;
    insert ClintP;
    
    List<CRS_Payment__c> ListPayment=new List<CRS_Payment__c>();
    CRS_Payment__c payment=new CRS_Payment__c();
    payment.Payment_Amount__c=1;
    payment.Client__c=ClintP.id;
    payment.Payment_Type__c='CRS501';
    ListPayment.add(payment);
        
    CRS_Payment__c payment1=new CRS_Payment__c();
    payment1.Payment_Amount__c=236;
    payment1.Client__c=ClintP.id;
    payment1.Payment_Type__c='CRS501';
    
    ListPayment.add(payment1);
    
    
    CRS_Client__c ClintCP=new CRS_Client__c(); 
    ClintCP.Address__c='';
    ClintCP.City__c='';
    ClintCP.Client_Type__c='Controlling';//Company//Controlling=Controlling_Person_Type__c
    ClintCP.Controlling_Person_Type__c='CRS801';
    ClintCP.Country__c='United Kingdom';
    ClintCP.CRS_Form__c=ObjFormd.id;
    ClintCP.Date_of_Birthday__c=Date.today();
    ClintCP.First_Name__c='United Kingdom';
    ClintCP.IN__c='2222';
    ClintCP.Last_Name__c='United Kingdom';
    ClintCP.TIN__c='sdasdasd';
    ClintCP.Master_Client__c=ClintP.id;
    insert ClintCP;
    
    CRS_Payment__c payment2=new CRS_Payment__c();
    payment2.Payment_Amount__c=236;
    payment2.Client__c=ClintCP.id;
    payment2.Payment_Type__c='CRS501';
    
    ListPayment.add(payment2);
    
    //Company 
    
    
      CRS_Client__c ClintC=new CRS_Client__c(); 
    ClintC.Address__c='';
    ClintC.City__c='';
    ClintC.Client_Type__c='Company';//Company//Controlling=Controlling_Person_Type__c
    ClintC.Controlling_Person_Type__c='CRS801';
    ClintC.Country__c='United Kingdom';
    ClintC.CRS_Form__c=ObjFormd.id;
    ClintC.Date_of_Birthday__c=Date.today();
    ClintC.First_Name__c='United Kingdom';
    ClintC.IN__c='2222';
    ClintC.Last_Name__c='United Kingdom';
    ClintC.TIN__c='sdasdasd';
    //ObjClintCP.Master_Client__c=ObjClint.id;
    insert ClintC;
    
    CRS_Payment__c paymentC=new CRS_Payment__c();
    paymentC.Payment_Amount__c=1;
    paymentC.Client__c=ClintC.id;
    paymentC.Payment_Type__c='CRS501';
    ListPayment.add(paymentC);
        
    CRS_Payment__c paymentC1=new CRS_Payment__c();
    paymentC1.Payment_Amount__c=236;
    paymentC1.Client__c=ClintC.id;
    paymentC1.Payment_Type__c='CRS501';
    ListPayment.add(paymentC1);
    
    
    CRS_Client__c ClintCCP=new CRS_Client__c(); 
    ClintCCP.Address__c='';
    ClintCCP.City__c='';
    ClintCCP.Client_Type__c='Controlling';//Company//Controlling=Controlling_Person_Type__c
    ClintCCP.Controlling_Person_Type__c='CRS801';
    ClintCCP.Country__c='United Kingdom';
    ClintCCP.CRS_Form__c=ObjFormd.id;
    ClintCCP.Date_of_Birthday__c=Date.today();
    ClintCCP.First_Name__c='United Kingdom';
    ClintCCP.IN__c='2222';
    ClintCCP.Last_Name__c='United Kingdom';
    ClintCCP.TIN__c='sdasdasd';
    ClintCCP.Master_Client__c=ClintC.id;
    insert ClintCCP;
    
    CRS_Payment__c paymentCCp2=new CRS_Payment__c();
    paymentCCp2.Payment_Amount__c=236;
    paymentCCp2.Client__c=ClintCCP.id;
    paymentCCp2.Payment_Type__c='CRS501';
    
    ListPayment.add(paymentCCp2);
    insert ListPayment;
    //Company EndOfInputException
    
    CRS_generate_XML.generateXml('Demo');
    
     ApexPages.StandardController scd = new ApexPages.StandardController(ObjFormd);
    CRS_Submission_Formcontroller  CRSSubD=new CRS_Submission_Formcontroller (scd);
     CRSSubD.existingRecords();
     
    
     List<CRS_Submission_Formcontroller.CRSMainWrapper> CompaniesRecordsD=CRSSubD.CompaniesRecords;
     List<CRS_Submission_Formcontroller.CRSMainWrapper> PersonRecordsD=CRSSubD.PersonRecords;
     
        
     //Company 
       CRSSubD.SeletedId=paymentCCp2.id;
     CRSSubD.DeleteControllingPersonPayments();
     
     CRSSubD.SeletedId=ClintC.id;
     CRSSubD.AddControllingPersonPayment();
     
        //CRSSubD.SeletedId=ClintCCP.id;
        //CRSSubD.DeleteControllingPersonMain();
    
     //person
      CRSSubD.SeletedId=payment2.id;
     CRSSubD.DeleteControllingPersonPayments();
     
     CRSSubD.SeletedId=ClintP.id;
     CRSSubD.AddControllingPersonPayment();
     
      CRSSubD.SeletedId=ClintCP.id;
      CRSSubD.DeleteControllingPersonMain();
      
    
     CRSSubD.SaveMain();
    //CRSSubD.SaveAllMain();
    
 //********************************************************************************************************************************************************************
  
  CRS_Form__c ObjForm=new CRS_Form__c ();
    ObjForm.Tax_Residence_Country_new__c='United Kingdom';
    ObjForm.CRS_Year__c=ObjYear.id;
    

    
    ApexPages.StandardController sc = new ApexPages.StandardController(ObjForm);
    CRS_Create_Formcontroller  ObjCRSCreate=new CRS_Create_Formcontroller (sc);
    
   // ObjCRSCreate.crsform=new CRS_Form__c ();
    //crsform.Tax_Residence_Country_new__c='United Kingdom';
    ObjCRSCreate.submit();
    ObjCRSCreate.submitNull();
     
    
    ApexPages.StandardController scform = new ApexPages.StandardController(ObjForm);
    
    CRS_Submission_Formcontroller CRSSub=new CRS_Submission_Formcontroller (scform );
    CRSSub.existingRecords();
    
    CRS_Client__c ObjClint=new CRS_Client__c(); 
    ObjClint.Address__c='';
    ObjClint.City__c='';
    ObjClint.Client_Type__c='Person';//Company//Controlling=Controlling_Person_Type__c
    
    ObjClint.Controlling_Person_Type__c='CRS801';
    ObjClint.Country__c='United Kingdom';
    ObjClint.CRS_Form__c=ObjForm.id;
    ObjClint.Date_of_Birthday__c=Date.today();
    ObjClint.First_Name__c='United Kingdom';
    ObjClint.IN__c='2222';
    ObjClint.Last_Name__c='United Kingdom';
    ObjClint.TIN__c='sdasdasd';
    
    //ObjClint.Master_Client__c=ObjYear.id;

    
    //CRSSub.addClint(ObjClint,ObjForm);
    
    
   
    
     CRS_Client__c ObjClintCP=new CRS_Client__c(); 
    ObjClintCP.Address__c='';
    ObjClintCP.City__c='';
    ObjClintCP.Client_Type__c='Company';//Company//Controlling=Controlling_Person_Type__c
    ObjClintCP.Controlling_Person_Type__c='CRS801';
    ObjClintCP.Country__c='United Kingdom';
    ObjClintCP.CRS_Form__c=ObjForm.id;
    ObjClintCP.Date_of_Birthday__c=Date.today();
    ObjClintCP.First_Name__c='United Kingdom';
    ObjClintCP.IN__c='2222';
    ObjClintCP.Last_Name__c='United Kingdom';
    ObjClintCP.TIN__c='sdasdasd';
    //ObjClintCP.Master_Client__c=ObjClint.id;
    insert ObjClintCP;
     //CRSSub.addClint(ObjClintCP,ObjForm);
    
    CRS_Payment__c ObjCRS_Payment=new CRS_Payment__c();
    ObjCRS_Payment.Client__c=ObjClintCP.ID;
    insert ObjCRS_Payment;
                                            
                                            
    CRSSub.AddPerson();
    CRSSub.CancelMain();
    CRSSub.AddCompany();
    CRSSub.CancelMain();
    
    CRSSub.AddPerson();
    
    
    List<CRS_Submission_Formcontroller.CRSMainWrapper> PersonRecords=new List<CRS_Submission_Formcontroller.CRSMainWrapper>();
    PersonRecords.add(CRSSub.addClint(ObjClint,ObjForm));
    CRSSub.PersonRecords=PersonRecords;
    
    List<CRS_Submission_Formcontroller.CRSMainWrapper> CompaniesRecords=new List<CRS_Submission_Formcontroller.CRSMainWrapper>();
    CompaniesRecords.add(CRSSub.addClint(ObjClintCP,ObjForm));
    CRSSub.CompaniesRecords=CompaniesRecords;
    
    
    System.debug('==PersonRecords=='+PersonRecords);
    
    
    CRSSub.SaveMain();
    CRSSub.SaveAllMain();
    
    //cOMPNAY 
    CRSSub.SeletedId=ObjClintCP.id;
     CRSSub.AddMainPayment();
     CRSSub.AddControllingPerson();
     CRSSub.SaveMain();
     CRSSub.AddControllingPersonPayment();
     CRSSub.CancelControllingperson();
     
     //PERSON
     CRSSub.SeletedId=ObjClint.id;
     CRSSub.AddMainPayment();
    CRSSub.AddControllingPerson(); 
    CRSSub.SaveMain();
    CRSSub.AddControllingPersonPayment();
    CRSSub.CancelControllingperson();
    CRSSub.SaveControllingperson();
     
     CRSSub.SeletedId=ObjCRS_Payment.id;
    CRSSub.DeletePaymentMain();
    
     CRSSub.SeletedId=ObjClint.id;
    CRSSub.DeleteMain();
    
     CRSSub.SeletedId=ObjClintCP.id;
    CRSSub.DeleteMain();
    
    
     ApexPages.StandardController scYear = new ApexPages.StandardController(ObjYear);
    CRSGenerateXmlextensions GenerateXMl=new CRSGenerateXmlextensions(scYear );
    GenerateXMl.submitXML();
    
  //  CRSGenerateXmlextensions.submitMassXML(ObjYear.id,ObjYear.Name);
    

     
    
  
  
    // CRSSub.DeleteControllingPersonMain();
/*  
DeletePaymentMain
  CRSSub.DeleteControllingPersonPayments();
   
    CRSSub.DeletePaymentMain();
    CRSSub.DeleteMain();
    CRSSub.SaveControllingperson();
*/  

    
  system.debug(CRS_generate_XML.generateXml('Demo'));

     
    

    
    
    
    
    
    }

    
    }
    
    
    }