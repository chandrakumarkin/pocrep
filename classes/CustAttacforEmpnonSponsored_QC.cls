/******************************************************************************************
 *  Author   : Veera
 *  Company  : 
 *  Date     : 24/05/2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    24/05/2021  Veera    Created
********/

global class CustAttacforEmpnonSponsored_QC implements Queueable,Database.AllowsCallouts{

    string ServID ='';
    map<id,id> validAttachmentIDs             = new map<id,id>();
    map<id,List<string>> docDetailsMap        = new map<id,List<string>>();
    List<Log__c> logList                      = new List<Log__c>();

    global CustAttacforEmpnonSponsored_QC (string SRID){
       ServID = SRID;
    }
    
    global void execute(QueueableContext context){
    
        for(sr_Doc__c srdoc : [select id,Doc_ID__c,Name,Service_Request__c,Service_Request__r.customer__r.Name,Service_Request__r.customer__c,Service_Request__r.Name from SR_Doc__c where Service_Request__c = :ServID  and Name ='Coloured Photo' and Doc_ID__c !=null and status__c = 'Uploaded' and Sent_Picture_to_Secupass__c = 0]){
        docDetailsMap.put(srdoc.id,new list<string>{srdoc.Service_Request__c,srdoc.Name,srdoc.Service_Request__r.Name,srdoc.Service_Request__r.customer__c,srdoc.Service_Request__r.customer__r.Name});          
        validAttachmentIDs.put(srdoc.id,srdoc.Doc_ID__c);
        
         List<Custom_Attachment__c> cAttachToUpdate = new List<Custom_Attachment__c>();
         Custom_Attachment__c ca;
         long noOfloops;
         integer maxSizeOfText = 131072;
         string attachmentBodybase64;
     if(docDetailsMap !=null && docDetailsMap.size()>0){
      for(Attachment att :[select id,Name,Body,parentID from attachment where parentID in :docDetailsMap.keySet() and id in:validAttachmentIDs.values()]){
        attachmentBodybase64 = string.valueOf(EncodingUtil.base64Encode(att.Body));        
        ca = new Custom_Attachment__c(SR_Doc__c =att.parentID ,Service_Request__c =docDetailsMap.get(att.ParentID)[0],SRDoc_EXT__c =att.parentID);
        ca.Picture_Length__c =  attachmentBodybase64.length();
        noOfloops = (ca.Picture_Length__c/maxSizeOfText).round(System.RoundingMode.CEILING);
        if(noOfloops <= 12){ 
          for(integer i = 1;i <=12;i++){
            if(i < noOfloops)    
              ca.put('Picture_in_Text'+i+'__c',attachmentBodybase64.substring(maxSizeOfText*(i-1), maxSizeOfText*i));
            else if(i == noOfloops)  
              ca.put('Picture_in_Text'+i+'__c',attachmentBodybase64.substring(maxSizeOfText*(i-1),attachmentBodybase64.length()));
            else if(i>noOfloops)
              ca.put('Picture_in_Text'+i+'__c','');
              
          }
            cAttachToUpdate.add(ca);
        }else{
            Log__c binaryattLog = new Log__c();
            binaryattLog.Account__c = docDetailsMap.get(att.ParentID)[3];
            binaryattLog.Description__c =  'Unable to convert picture to binary format.Reached maximum limit SR No - '+docDetailsMap.get(att.ParentID)[2]+' SR Doc -'+docDetailsMap.get(att.ParentID)[1];
            binaryattLog.Type__c   = 'Failure - Converting Attachment  to Binary format';
            logList.add(binaryattLog); 
        }
      }
    }  
          if(cAttachToUpdate !=null && !cAttachToUpdate.isEmpty()) Upsert cAttachToUpdate SRDoc_EXT__c;
      }
   }
}