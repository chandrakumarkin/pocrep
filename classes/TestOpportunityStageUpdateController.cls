@isTest
private class TestOpportunityStageUpdateController {

 public static Opportunity opportunityRecord;
 public static Opportunity_Stage_History__c eachHistory;
 public static Attachment attach;
	public static void myTestData(){
		
		Account eachAccount = new Account();
		eachAccount.name ='Test Account';
		INSERT eachAccount;
		
		opportunityRecord = new Opportunity();
		opportunityRecord.name ='Test Opportunity';
		opportunityRecord.StageName = Label.Letter_of_Intent_LOI_Received;
		opportunityRecord.CloseDate = system.today();
		opportunityRecord.RecordtypeID = '0120J000000VFLRQA4';
		INSERT opportunityRecord;
		
		eachHistory = new Opportunity_Stage_History__c();
		eachHistory.Stage__c = opportunityRecord.StageName;
		eachHistory.Related_Opportunity__c = opportunityRecord.Id;
		eachHistory.Created_Date_Time_History__c =system.today()-2;
		INSERT eachHistory;
		
		
        attach=new Attachment();   	
    	attach.Name='Unit Test Attachment';
    	Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
    	attach.body=bodyBlob;
        attach.parentId=opportunityRecord.id;
        insert attach;
        
        
		
	}
   static testmethod void positiveTest1(){
    	
    	myTestData();
    	ApexPages.currentPage().getParameters().put('id', opportunityRecord.ID);
    	OpportunityStageUpdateController opportunityStages = new OpportunityStageUpdateController();
    	OpportunityStageUpdateController.OpportunityHistoryWrapper wrapper = new OpportunityStageUpdateController.OpportunityHistoryWrapper(Label.Letter_of_Intent_LOI_Received,eachHistory,attach);
    	opportunityStages.getOpportunityWrapper();
    	opportunityStages.saveStageHistory();
    	opportunityStages.cancel();
    	opportunityStages.pageRedirect('Id');
    	List <Opportunity_Stage_History__c> lstHistory= new List<Opportunity_Stage_History__c>();
    	
    }
      static testmethod void positiveTest2(){
    	
    	myTestData();
    	opportunityRecord.RecordtypeID = '0120J00000079II';
    	UPDATE opportunityRecord;
    	ApexPages.currentPage().getParameters().put('id', opportunityRecord.ID);
    	OpportunityStageUpdateController opportunityStages = new OpportunityStageUpdateController();
    	OpportunityStageUpdateController.OpportunityHistoryWrapper wrapper = new OpportunityStageUpdateController.OpportunityHistoryWrapper(Label.Letter_of_Intent_LOI_Received,eachHistory,attach);
    	opportunityStages.getOpportunityWrapper();
    	opportunityStages.saveStageHistory();
    	opportunityStages.cancel();
    	opportunityStages.pageRedirect('Id');
    	List <Opportunity_Stage_History__c> lstHistory= new List<Opportunity_Stage_History__c>();
    	
    }
     static testmethod void positiveTest3(){
    	
    	myTestData();
    	opportunityRecord.RecordtypeID = '0120J00000079IN';
    	Update opportunityRecord;
    	eachHistory.Stage__c = 'Entity Name Check';
    	UPDATE eachHistory;
    	ApexPages.currentPage().getParameters().put('id', opportunityRecord.ID);
    	OpportunityStageUpdateController opportunityStages = new OpportunityStageUpdateController();
    	OpportunityStageUpdateController.OpportunityHistoryWrapper wrapper = new OpportunityStageUpdateController.OpportunityHistoryWrapper('Entity Name Check',eachHistory,attach);
    	opportunityStages.getOpportunityWrapper();
    	opportunityStages.saveStageHistory();
    	opportunityStages.cancel();
    	opportunityStages.pageRedirect('Id');
    	List <Opportunity_Stage_History__c> lstHistory= new List<Opportunity_Stage_History__c>();
    	
    }
    
    static testmethod void positiveTest4(){
    	
    	myTestData();
    	opportunityRecord.RecordtypeID = '0120J00000079IN';
    	Update opportunityRecord;
    	eachHistory.Stage__c = 'Entity Name Check';
    	eachHistory.Created_Date_Time_History__c = system.today()+2;
    	UPDATE eachHistory;
    	ApexPages.currentPage().getParameters().put('id', opportunityRecord.ID);
    	OpportunityStageUpdateController opportunityStages = new OpportunityStageUpdateController();
    	OpportunityStageUpdateController.OpportunityHistoryWrapper wrapper = new OpportunityStageUpdateController.OpportunityHistoryWrapper('Entity Name Check',eachHistory,attach);
    	opportunityStages.getOpportunityWrapper();
    	opportunityStages.saveStageHistory();
    	opportunityStages.cancel();
    	opportunityStages.pageRedirect('Id');
    	List <Opportunity_Stage_History__c> lstHistory= new List<Opportunity_Stage_History__c>();
    	
    }
    
      static testmethod void positiveTest5(){
    	
    	myTestData();
    	opportunityRecord.RecordtypeID = '0120J00000079IN';
    	Update opportunityRecord;
    	eachHistory.Stage__c = 'ROC Registered';
    	eachHistory.Created_Date_Time_History__c = system.today()-2;
    	UPDATE eachHistory;
    	ApexPages.currentPage().getParameters().put('id', opportunityRecord.ID);
    	OpportunityStageUpdateController opportunityStages = new OpportunityStageUpdateController(); 
    	OpportunityStageUpdateController.dummyTestMethod();
    	OpportunityStageUpdateController.OpportunityHistoryWrapper wrapper = new OpportunityStageUpdateController.OpportunityHistoryWrapper('ROC Registered',eachHistory,attach);
    	opportunityStages.getOpportunityWrapper();
    	opportunityStages.saveStageHistory();
    	opportunityStages.cancel();
    	opportunityStages.pageRedirect('Id');
    	List <Opportunity_Stage_History__c> lstHistory= new List<Opportunity_Stage_History__c>();
    	
    }
}