global without sharing  class UpdateMissedDataMaskingRecords{

    
    public static void UpdateContactMasking(){
        
        String Query = 'Select id,AssistantPhone,HomePhone,MobilePhone,OtherPhone,Phone,CONTACT_PHONE__c,Residence_Phone__c,Work_Phone__c,Email,Account_Principle_User_Email__c,Additional_Email__c from Contact order by createddate desc';
        SearchAndReplaceContact batchable = new SearchAndReplaceContact(query,new List < Contact >());
        Database.executeBatch(batchable);
    }
    
    public static void UpdateMissedContactMasking(){
        
        List<Contact> missedContacts =[
            select Id,Email,Account_Principle_User_Email__c,Additional_Email__c from 
            contact 
            where (
                (Email != null OR Account_Principle_User_Email__c != null OR Additional_Email__c != null) 
                and 
                (
                    (Email != null and (NOT (Email like '%invalid%'))) OR 
                    (Account_Principle_User_Email__c != null and (NOT (Account_Principle_User_Email__c like '%invalid%'))) OR
                    (Additional_Email__c != null and (NOT (Additional_Email__c like '%invalid%')))
                )
            )
        ];
        Database.executeBatch(new SearchAndReplaceContact('',missedContacts),1);
    }
    
    public static void UpdateAccountMasking(){
        String Query = 'Select id,Phone,Mobile_Number__c,Previous_Registered_Phone__c,Residence_Phone__c,Work_Phone__c,E_mail__c,Email_Address__c,Email_Address_to_send_Portal_Link__c,Fintech_Email__c,Initial_Contact_Email__c from Account order by createddate desc';
        SearchAndReplaceAccounts batchable = new SearchAndReplaceAccounts(query,new List < Account>());
        Database.executeBatch(batchable);
    }
    
     public static void UpdateMissedAccountMasking(){
        
        List<Account> missedContacts =[
            Select id,Phone,Mobile_Number__c,Previous_Registered_Phone__c,Residence_Phone__c,Work_Phone__c,E_mail__c,Email_Address__c,Email_Address_to_send_Portal_Link__c,Fintech_Email__c,Initial_Contact_Email__c from 
            Account 
            where (
                (E_mail__c  != null OR Email_Address__c  != null OR Email_Address_to_send_Portal_Link__c  != null
                    OR Fintech_Email__c != null OR Initial_Contact_Email__c != null
                ) 
                and 
                (
                    (E_mail__c != null and (NOT (E_mail__c like '%invalid%'))) OR 
                    (Email_Address__c != null and (NOT (Email_Address__c like '%invalid%'))) OR
                    (Email_Address_to_send_Portal_Link__c != null and (NOT (Email_Address_to_send_Portal_Link__c like '%invalid%'))) OR
                    (Fintech_Email__c != null and (NOT (Fintech_Email__c like '%invalid%'))) OR 
                    (Initial_Contact_Email__c != null and (NOT (Initial_Contact_Email__c like '%invalid%')))                    
                )
            )
        ];
        Database.executeBatch(new SearchAndReplaceAccounts('',missedContacts),1);
    }
    
    public static void UpdateSRMasking(){
        String Query ='Select id,Contact_Number_After_Office_Hours__c,Contact_Number_During_Office_Hours__c,Current_Registered_Phone__c,Foreign_Registered_Phone__c,Send_SMS_To_Mobile__c,Mobile_Number__c,Courier_Mobile_Number__c,Phone_No_Outside_UAE__c,Courier_Cell_Phone__c,Residence_Phone_No__c,Mobile_No_Previous_Sponsor__c,Sponsor_Mobile_No__c,Office_Telephone_Previous_Sponsor__c,Sponsor_Office_Telephone__c,P_O_Box_Previous_Sponsor__c,Residence_Telephone_Previous_Sponsor__c,Sponsor_Residence_Telephone__c,Work_Phone__c,Email__c,Email_Address__c ,Additional_Email__c from Service_request__c order by createdDate desc';
        SearchAndReplaceServiceRequest batchable = new SearchAndReplaceServiceRequest(Query,new List < Account>());
        Database.executeBatch(batchable);
    }
    
    public static void UpdateSRMissedMasking(){
         List<Service_request__c> missedContacts =[
            Select id,Contact_Number_After_Office_Hours__c,Contact_Number_During_Office_Hours__c,Current_Registered_Phone__c,Foreign_Registered_Phone__c,Send_SMS_To_Mobile__c,Mobile_Number__c,Courier_Mobile_Number__c,Phone_No_Outside_UAE__c,Courier_Cell_Phone__c,Residence_Phone_No__c,Mobile_No_Previous_Sponsor__c,Sponsor_Mobile_No__c,Office_Telephone_Previous_Sponsor__c,Sponsor_Office_Telephone__c,P_O_Box_Previous_Sponsor__c,Residence_Telephone_Previous_Sponsor__c,Sponsor_Residence_Telephone__c,Work_Phone__c,Email__c,Email_Address__c ,Additional_Email__c from Service_request__c 
            where (
                (Email__c != null OR Email_Address__c  != null) 
                and 
                (
                    (Email__c  != null and (NOT (Email__c like '%invalid%'))) OR 
                    (Email_Address__c != null and (NOT (Email_Address__c like '%invalid%')))                                  
                )
            )
        ];
        Database.executeBatch(new SearchAndReplaceServiceRequest('',missedContacts),1);
    }
    
    public static void UpdateLeadMasking(){
     
        String Query ='Select id,Email,MobilePhone,Phone,Alternative_Phone_No__c from Lead order by createddate desc';
        SearchAndReplaceLead batchable = new SearchAndReplaceLead (Query,new List < Lead>());
        Database.executeBatch(batchable);
    }
    
    public static void UpdateLeadMissedMasking(){
        List<Lead> missedContacts =[
            Select id,Email,MobilePhone,Phone,Alternative_Phone_No__c from Lead            
            where Email != null and (NOT (Email like '%invalid%'))
        ];
        Database.executeBatch(new SearchAndReplaceLead('',missedContacts),200);
    }
    
     public static void UpdateHexaSRMasking(){
         
        String Query = 'Select id,HexaBPM__Email__c,Authorised_individual_email__c,Authorised_individual_telephone_no__c,Foreign_entity_telephone_number__c,HexaBPM__Send_SMS_to_Mobile__c,Point_of_Contact__c,Telephone_No__c from HexaBPM__Service_Request__c order by createddate desc';
        SearchAndReplaceHexaBPMSerReq batchable = new SearchAndReplaceHexaBPMSerReq(Query,new List <HexaBPM__Service_Request__c>());
        Database.executeBatch(batchable,50);
    }
    
    public static void UpdateMissedHexaSRMasking(){
        List<HexaBPM__Service_Request__c> missedContacts =[
            Select id,HexaBPM__Email__c,Authorised_individual_email__c,Authorised_individual_telephone_no__c,
            Foreign_entity_telephone_number__c,HexaBPM__Send_SMS_to_Mobile__c,Point_of_Contact__c,Telephone_No__c
            from HexaBPM__Service_Request__c
            where (
                (HexaBPM__Email__c  != null OR Authorised_individual_email__c   != null) 
                and 
                (
                    (HexaBPM__Email__c != null and (NOT (HexaBPM__Email__c like '%invalid%'))) OR 
                    (Authorised_individual_email__c != null and (NOT (Authorised_individual_email__c like '%invalid%')))                                  
                )
            )
        ];
        Database.executeBatch(new SearchAndReplaceHexaBPMSerReq('',missedContacts),1);
    }
    
    public static void UpdateHexaSRStepsMasking(){
        String Query = 'Select id,Email__c from HexaBPM__Step__c order by createddate desc';
        SearchAndReplaceHexaBPMSteps batchable = new SearchAndReplaceHexaBPMSteps(Query,new List < HexaBPM__Step__c >());
        Database.executeBatch(batchable);
    }
    
     public static void UpdateMissedHexaSRStepsMasking(){
       
        List<HexaBPM__Step__c> missedContacts =[
                Select id,Email__c from HexaBPM__Step__c
                where Email__c != null and (NOT (Email__c like '%invalid%'))
                    
            ];
            Database.executeBatch(new SearchAndReplaceHexaBPMSteps('',missedContacts),1);
      }
      
    public static void UpdateSRStepsMasking(){
        String Query = 'Select id,Sys_Additional_Email__c,SAP_EMAIL__c,Contractor_email_for_fit_out__c,Client_Email_for_fit_out__c,Awaiting_Originals_Email_Text__c ,Applicant_Email__c ,Applicant_Mobile__c,Mobile_No__c,Telephone_Number__c from Step__c order by createddate desc';
        SearchAndReplaceSteps batchable = new SearchAndReplaceSteps(Query,new List < Step__c >());
        Database.executeBatch(batchable);
    }
    
     public static void UpdateMissedSRStepsMasking(){
        List<Step__c> missedContacts =[
           Select id,Sys_Additional_Email__c,SAP_EMAIL__c,Contractor_email_for_fit_out__c,Client_Email_for_fit_out__c,Awaiting_Originals_Email_Text__c ,
           Applicant_Email__c ,Applicant_Mobile__c,Mobile_No__c,Telephone_Number__c from Step__c
            where (
                (
                    Sys_Additional_Email__c != null OR Contractor_email_for_fit_out__c != null OR
                    Applicant_Email__c  != null OR Client_Email_for_fit_out__c   != null
                ) 
                and 
                (
                    (Sys_Additional_Email__c != null and (NOT (Sys_Additional_Email__c like '%invalid%'))) OR 
                    (Applicant_Email__c != null and (NOT (Applicant_Email__c like '%invalid%'))) OR
                    (Client_Email_for_fit_out__c != null and (NOT (Client_Email_for_fit_out__c like '%invalid%'))) OR
                    (Contractor_email_for_fit_out__c != null and (NOT (Contractor_email_for_fit_out__c like '%invalid%')))                                  
                )
            )
        ];
        Database.executeBatch(new SearchAndReplaceSteps('',missedContacts),1);
    }
    
    public static void UpdateRelationshipsMasking(){
        String Query = 'Select id,Contact_Email__c ,Contact_Phone__c ,Contact_Fax__c from Relationship__c where (Contact_Email__c != null OR Contact_Phone__c != null OR Contact_Fax__c != NULL) order by createddate desc';
        SearchAndReplaceRelationship batchable = new SearchAndReplaceRelationship(Query,new List <Relationship__c>());
        Database.executeBatch(batchable,50);
    }
    
    public static void UpdateMissedRelationshipsMasking(){
        List<Relationship__c> missedContacts =[
          Select id,Contact_Email__c ,Contact_Phone__c ,Contact_Fax__c from Relationship__c
            where Contact_Email__c != null and (NOT (Contact_Email__c like '%invalid%'))
        ];
        Database.executeBatch(new SearchAndReplaceRelationship('',missedContacts),1);
    }
    
    public static void UpdateHexaAmendmentMasking(){
        String Query = 'Select id,Email__c,DI_Email__c,DI_Mobile__c,Mobile__c from HexaBPM_Amendment__c order by createddate desc';
        SearchAndReplaceHexaBPMAmendment batchable = new SearchAndReplaceHexaBPMAmendment(Query,new List <HexaBPM_Amendment__c >());
        Database.executeBatch(batchable,50);
    }
    
    public static void UpdateMissedHexaAmendmentMasking(){
        List<HexaBPM_Amendment__c> missedContacts =[
          Select id,Email__c,DI_Email__c,DI_Mobile__c,Mobile__c from HexaBPM_Amendment__c
             where (
                (
                    Email__c != null OR DI_Email__c != null 
                ) 
                and 
                (
                    (Email__c  != null and (NOT (Email__c like '%invalid%'))) OR 
                    (DI_Email__c != null and (NOT (DI_Email__c like '%invalid%')))                               
                )
            )
        ];
        Database.executeBatch(new SearchAndReplaceHexaBPMAmendment('',missedContacts),200);
    }
    
}