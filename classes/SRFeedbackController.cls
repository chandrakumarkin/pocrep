public without sharing class SRFeedbackController {
    public string comments {get;set;}
    public string rating {get;set;}
    public boolean isThank {get;set;}
    public boolean isSubmitted {get;set;}
    public string recordId;
    public SRFeedbackController(){
        recordId = ApexPages.currentPage().getParameters().get('Id');
    	isThank = false;
        isSubmitted = false;
    }
    public pageReference doSubmit(){
        try{
            Case_Feedback__c feedbackObj = new Case_Feedback__c();
            feedbackObj.RecordTypeId=  Schema.SObjectType.Case_Feedback__c.getRecordTypeInfosByName().get('Service Request').getRecordTypeId();
            feedbackObj.ServiceRequest__c = recordId;
            feedbackObj.Rating__c = rating;
            feedbackObj.Comment__c = comments;
            insert feedbackObj;
            isThank = true;
            return null;
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            return null;
        }
    }
}