/******************************************************************************************
 *  Author      : Rajil Ravindran
 *  Date        : 19-Jan-2020
 *  Description : Controller to create Profile Complete and InPrinciple SR - invoked from
                  process builder : Create draft SR for lead conversion and contact creation
    
    V 1.0   Merul populating the contact on SR if contact(OB_Block_Homepage__c) is checked
    v 1.1   Durga	Code to populate Opportunity on Creation of the Application  
 ********************************************************************************************/
public without sharing class OB_CreateNonFinancialSR {
    //Method to create Profile Complete and InPrinciple SR
    @InvocableMethod
    public static void createNonFinancialSR(List<Contact> lstContact){
        list<HexaBPM__Service_Request__c> lstSR = new list<HexaBPM__Service_Request__c>();
        list<string> lstContactId = new list<string>();
        string lookupValue;
        Contact contactRecord;
        list<string> lstLookUpField = new list<string>();
        map<string,Contact> mapContact = new map<string,Contact>();
        map<string,boolean> mapCommercialPermission = new map<string,boolean>();
        String commaSepratedFields = '';
        string soql;
        for(Contact contObj : lstContact)
            lstContactId.add(contObj.id);

		set<string> setQueryFields = new set<string>();
		for(OB_User_Registration_SR_Map__c settingObj : OB_User_Registration_SR_Map__c.getAll().values()) {
            if(settingObj.SourceObjectField__c!=null)
            	setQueryFields.add(settingObj.SourceObjectField__c.toLowerCase());
        }
        setQueryFields.add('account.is_commercial_permission__c');
        for(string strField : setQueryFields) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = strField;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + strField;
            }
        }
        set<string> setAccountId = new set<string>();
        soql = 'select ' +commaSepratedFields+',OB_Block_Homepage__c from Contact where id in :lstContactId';
        for(Contact contactObj : Database.query(soql)){
            mapContact.put(contactObj.id,contactObj);
            if(contactObj.account.is_commercial_permission__c=='Yes')
            	mapCommercialPermission.put(contactObj.Id,true);
            else
            	mapCommercialPermission.put(contactObj.Id,false);
            	
            if(contactObj.AccountId!=null)
            	setAccountId.add(contactObj.AccountId);
        }
		
		//Code to check SR Exists or not
        map<string,string> MapAccountUserRegApplication = new map<string,string>();
        map<string,string> MapAccountInPrincpApplication = new map<string,string>();
        map<string,string> MapOpportunities = new map<string,string>();//v1.1
        set<string> setRecordTypeIds = new set<string>{Label.OB_USER_REG_RECORDTYPEID,Label.OB_IN_PRINCIPLE_ID};
        if(setAccountId.size()>0){
        	for(Opportunity Opp:[Select Id,AccountId from Opportunity where AccountId IN:setAccountId order by CreatedDate]){//v1.1
        		MapOpportunities.put(Opp.AccountId,Opp.Id);
        	}
	        for(HexaBPM__Service_Request__c objSR:[Select Id,HexaBPM__Customer__c,HexaBPM__Record_Type_Name__c from HexaBPM__Service_Request__c where HexaBPM__Customer__c IN:setAccountId and RecordTypeId IN:setRecordTypeIds and HexaBPM__IsCancelled__c=false and HexaBPM__Is_Rejected__c=false]){
	        	if(objSR.HexaBPM__Record_Type_Name__c=='Converted_User_Registration')
	        		MapAccountUserRegApplication.put(objSR.HexaBPM__Customer__c,objSR.Id);
	        	else if(objSR.HexaBPM__Record_Type_Name__c=='In_Principle')
	        		MapAccountInPrincpApplication.put(objSR.HexaBPM__Customer__c,objSR.Id);
	        }
        }
		
        for(Contact contactObj : lstContact){
        	if(contactObj.AccountId!=null && MapAccountUserRegApplication.get(contactObj.AccountId)==null && !mapCommercialPermission.get(contactObj.Id)){
	            HexaBPM__Service_Request__c userRegistrationSR = new HexaBPM__Service_Request__c();
	            userRegistrationSR.recordTypeId = Label.OB_USER_REG_RECORDTYPEID;
                contactRecord = mapContact.get(contactObj.Id);
                if(MapOpportunities.get(contactObj.AccountId)!=null)//v1.1
	            	userRegistrationSR.Opportunity__c = MapOpportunities.get(contactObj.AccountId);//v1.1
                
                for(OB_User_Registration_SR_Map__c settingObj : OB_User_Registration_SR_Map__c.getAll().values()) 
                {
	                lookupValue = '';
	                if(settingObj.SourceObjectField__c != null && settingObj.SourceObjectField__c.indexOf('.') > -1)
	                {
	                    lstLookUpField = settingObj.SourceObjectField__c.split('\\.');
	                    if(lstLookUpField != null && lstLookUpField.size() > 0 && lstLookUpField.size() < 3){
	                        if(contactRecord.getSobject(lstLookUpField[0]) != null)
	                            lookupValue = (String) contactRecord.getSobject(lstLookUpField[0]).get(lstLookUpField[1]);
	                        if(String.isNotBlank(lookupValue))
	                            userRegistrationSR.put(settingObj.TargetObjectField__c,lookupValue);
	                    }
	                }   
	                else 
                        userRegistrationSR.put(settingObj.TargetObjectField__c,contactRecord.get(settingObj.SourceObjectField__c));
                   
                }
                
                 //  V 1.0   Merul populating the contact on SR if contact(OB_Block_Homepage__c) is checked
                 if( contactRecord.OB_Block_Homepage__c )
                 {
                    userRegistrationSR.put('HexaBPM__Contact__c',contactRecord.id);  
                 }  

	            lstSR.add(userRegistrationSR);
        	}
        } 
        if(lstSR.size() > 0)
            insert lstSR;

        //v1.0 updating contact.
        /*
            if( lstContactToUpdate.size() > 0 )
            {
                update lstContactToUpdate;    
            }  
        */
        
        map<string,string> mapLinkedSR = new map<string,string>();
        for(HexaBPM__Service_Request__c sr : lstSR)
            mapLinkedSR.put(sr.HexaBPM__Contact__c,sr.id);

        //Creating Inprinciple Application
        commaSepratedFields = '';
        mapContact = new map<string,Contact>();
        for(OB_InPrinciple_SR_Map__c settingObj : OB_InPrinciple_SR_Map__c.getAll().values()) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = settingObj.SourceObjectField__c;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + settingObj.SourceObjectField__c;
            }
        }
        
        soql = 'select ' +commaSepratedFields+',OB_Block_Homepage__c from Contact where id in :lstContactId';
        for(Contact contactObj : Database.query(soql)){
            mapContact.put(contactObj.id,contactObj);
        }
        list<HexaBPM__Service_Request__c> lstInPrincipleSR = new list<HexaBPM__Service_Request__c>();
        for(Contact contactObj : lstContact){
        	if(contactObj.AccountId!=null && MapAccountInPrincpApplication.get(contactObj.AccountId)==null && !mapCommercialPermission.get(contactObj.Id)){
	            HexaBPM__Service_Request__c inprincipleSR = new HexaBPM__Service_Request__c();
	            inprincipleSR.recordTypeId = Label.OB_IN_PRINCIPLE_ID;
	            contactRecord = mapContact.get(contactObj.Id);
				if(MapOpportunities.get(contactObj.AccountId)!=null)//v1.1
	            	inprincipleSR.Opportunity__c = MapOpportunities.get(contactObj.AccountId);//v1.1
	            for(OB_InPrinciple_SR_Map__c settingObj : OB_InPrinciple_SR_Map__c.getAll().values()) {
	                if(settingObj.SourceObjectField__c != null && settingObj.SourceObjectField__c.indexOf('.') > -1){
	                    lstLookUpField = settingObj.SourceObjectField__c.split('\\.');
	                    if(lstLookUpField != null && lstLookUpField.size() > 0 && lstLookUpField.size() < 3){
	                        if(contactRecord.getSobject(lstLookUpField[0]) != null)
	                            lookupValue = (String) contactRecord.getSobject(lstLookUpField[0]).get(lstLookUpField[1]);
	                        if(String.isNotBlank(lookupValue))
	                            inprincipleSR.put(settingObj.TargetObjectField__c,lookupValue);
	                    }
	                }   
	                else 
	                    inprincipleSR.put(settingObj.TargetObjectField__c,contactRecord.get(settingObj.SourceObjectField__c));
	                
	                //Populating the Linked SR Field
	                if(mapLinkedSR.get(contactObj.id) != null)
	                    inprincipleSR.put('HexaBPM__Linked_SR__c',mapLinkedSR.get(contactObj.id));
                }
                
                //  V 1.0   Merul populating the contact on SR if contact(OB_Block_Homepage__c) is checked
                if( contactRecord.OB_Block_Homepage__c )
                    inprincipleSR.put('HexaBPM__Contact__c',contactRecord.id);  

	            lstInPrincipleSR.add(inprincipleSR);
        	}
        } 
        if(lstInPrincipleSR != null && lstInPrincipleSR.size() > 0)
            insert lstInPrincipleSR;

    }

    public static void createNonFinancialSRForExstCon(Map<Id,account> mapConAccId){

        list<contact> lstContact = new list<contact>();
        list<HexaBPM__Service_Request__c> lstSR = new list<HexaBPM__Service_Request__c>();
        list<string> lstContactId = new list<string>();
        string lookupValue;
        Contact contactRecord;
        list<string> lstLookUpField = new list<string>();
        map<string,Contact> mapContact = new map<string,Contact>();
        map<string,boolean> mapCommercialPermission = new map<string,boolean>();
        String commaSepratedFields = '';
        string soql;

        /* for(Contact contObj : lstContact)
            lstContactId.add(contObj.id); */
        for(id contId : mapConAccId.keySet()){
            lstContactId.add(contId);
        }   

		set<string> setQueryFields = new set<string>();
		for(OB_User_Registration_SR_Map__c settingObj : OB_User_Registration_SR_Map__c.getAll().values()) {
            if(settingObj.SourceObjectField__c!=null)
            	setQueryFields.add(settingObj.SourceObjectField__c.toLowerCase());
        }
        setQueryFields.add('account.is_commercial_permission__c');
        for(string strField : setQueryFields) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = strField;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + strField;
            }
        }
        set<string> setAccountId = new set<string>();
        soql = 'select ' +commaSepratedFields+',OB_Block_Homepage__c from Contact where id in :lstContactId';
        for(Contact contactObj : Database.query(soql)){
            mapContact.put(contactObj.id,contactObj);
           /*  if(contactObj.account.is_commercial_permission__c=='Yes')
            	mapCommercialPermission.put(contactObj.Id,true);
            else
            	mapCommercialPermission.put(contactObj.Id,false);
            	
            if(contactObj.AccountId!=null)
                setAccountId.add(contactObj.AccountId); */

            lstContact.add(contactObj);

            if(mapConAccId.get(contactObj.id) != null && mapConAccId.get(contactObj.id).is_commercial_permission__c=='Yes')
            	mapCommercialPermission.put(contactObj.Id,true);
            else
            	mapCommercialPermission.put(contactObj.Id,false);
            	
            if(mapConAccId.get(contactObj.id).id !=null)
                setAccountId.add(mapConAccId.get(contactObj.id).id);
            
        }
		
		//Code to check SR Exists or not
        map<string,string> MapAccountUserRegApplication = new map<string,string>();
        map<string,string> MapAccountInPrincpApplication = new map<string,string>();
        map<string,string> MapOpportunities = new map<string,string>();//v1.1
        set<string> setRecordTypeIds = new set<string>{Label.OB_USER_REG_RECORDTYPEID,Label.OB_IN_PRINCIPLE_ID};
        if(setAccountId.size()>0){
        	for(Opportunity Opp:[Select Id,AccountId from Opportunity where AccountId IN:setAccountId order by CreatedDate]){//v1.1
        		MapOpportunities.put(Opp.AccountId,Opp.Id);
        	}
	        for(HexaBPM__Service_Request__c objSR:[Select Id,HexaBPM__Customer__c,HexaBPM__Record_Type_Name__c from HexaBPM__Service_Request__c where HexaBPM__Customer__c IN:setAccountId and RecordTypeId IN:setRecordTypeIds and HexaBPM__IsCancelled__c=false and HexaBPM__Is_Rejected__c=false]){
	        	if(objSR.HexaBPM__Record_Type_Name__c=='Converted_User_Registration')
	        		MapAccountUserRegApplication.put(objSR.HexaBPM__Customer__c,objSR.Id);
	        	else if(objSR.HexaBPM__Record_Type_Name__c=='In_Principle')
	        		MapAccountInPrincpApplication.put(objSR.HexaBPM__Customer__c,objSR.Id);
	        }
        }
		
        for(Contact contactObj : lstContact){
        	if(contactObj.AccountId!=null && MapAccountUserRegApplication.get(contactObj.AccountId)==null && !mapCommercialPermission.get(contactObj.Id)){
	            HexaBPM__Service_Request__c userRegistrationSR = new HexaBPM__Service_Request__c();
	            userRegistrationSR.recordTypeId = Label.OB_USER_REG_RECORDTYPEID;
                //contactRecord = mapContact.get(contactObj.Id);
                account accountObj = mapConAccId.get(contactObj.id);
                if(MapOpportunities.get(accountObj.Id)!=null)//v1.1
	            	userRegistrationSR.Opportunity__c = MapOpportunities.get(accountObj.Id);//v1.1
                
                /* for(OB_User_Registration_SR_Map__c settingObj : OB_User_Registration_SR_Map__c.getAll().values()) 
                {
	                lookupValue = '';
	                if(settingObj.SourceObjectField__c != null && settingObj.SourceObjectField__c.indexOf('.') > -1)
	                {
	                    lstLookUpField = settingObj.SourceObjectField__c.split('\\.');
	                    if(lstLookUpField != null && lstLookUpField.size() > 0 && lstLookUpField.size() < 3){
	                        if(contactRecord.getSobject(lstLookUpField[0]) != null)
	                            lookupValue = (String) contactRecord.getSobject(lstLookUpField[0]).get(lstLookUpField[1]);
	                        if(String.isNotBlank(lookupValue))
	                            userRegistrationSR.put(settingObj.TargetObjectField__c,lookupValue);
	                    }
	                }   
	                else 
                        userRegistrationSR.put(settingObj.TargetObjectField__c,contactRecord.get(settingObj.SourceObjectField__c));
                   
                } */

                userRegistrationSR.put('Building__c',contactObj.OB_Building__c);
                userRegistrationSR.put('Business_Sector__c',accountObj.OB_Sector_Classification__c);
                userRegistrationSR.put('HexaBPM__Contact__c',contactObj.id);
                userRegistrationSR.put('HexaBPM__Customer__c',accountObj.id);
                userRegistrationSR.put('HexaBPM__Email__c',contactObj.Email);
                userRegistrationSR.put('Entity_Name__c',accountObj.Name);
                userRegistrationSR.put('Entity_Type__c',accountObj.Company_Type__c);
                userRegistrationSR.put('first_name__c',contactObj.FirstName);
                userRegistrationSR.put('OB_Is_Multi_Structure_Application__c',accountObj.OB_Is_Multi_Structure_Application__c);
                userRegistrationSR.put('last_name__c',contactObj.LastName);
                userRegistrationSR.put('Licensing_Period__c',contactObj.OB_Licensing_Period__c);
                userRegistrationSR.put('HexaBPM__Send_SMS_to_Mobile__c',accountObj.Phone);
                userRegistrationSR.put('Restricted_license__c',contactObj.OB_Restricted_license__c);
                userRegistrationSR.put('Retail_license__c',contactObj.OB_Retail_license__c);
                userRegistrationSR.put('Type_of_Property__c',contactObj.OB_Type_of_Property__c);
                

                
                 //  V 1.0   Merul populating the contact on SR if contact(OB_Block_Homepage__c) is checked
                 if( contactObj.OB_Block_Homepage__c )
                 {
                    userRegistrationSR.put('HexaBPM__Contact__c',contactObj.id);  
                 }  

	            lstSR.add(userRegistrationSR);
        	}
        } 
        if(lstSR.size() > 0)
            insert lstSR;

        //v1.0 updating contact.
        /*
            if( lstContactToUpdate.size() > 0 )
            {
                update lstContactToUpdate;    
            }  
        */
        
        map<string,string> mapLinkedSR = new map<string,string>();
        for(HexaBPM__Service_Request__c sr : lstSR)
            mapLinkedSR.put(sr.HexaBPM__Contact__c,sr.id);

        //Creating Inprinciple Application
        commaSepratedFields = '';
        mapContact = new map<string,Contact>();
        for(OB_InPrinciple_SR_Map__c settingObj : OB_InPrinciple_SR_Map__c.getAll().values()) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = settingObj.SourceObjectField__c;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + settingObj.SourceObjectField__c;
            }
        }
        
        soql = 'select ' +commaSepratedFields+',OB_Block_Homepage__c from Contact where id in :lstContactId';
        for(Contact contactObj : Database.query(soql)){
            mapContact.put(contactObj.id,contactObj);
        }
        list<HexaBPM__Service_Request__c> lstInPrincipleSR = new list<HexaBPM__Service_Request__c>();
        for(Contact contactObj : lstContact){
        	if(contactObj.AccountId!=null && MapAccountInPrincpApplication.get(contactObj.AccountId)==null && !mapCommercialPermission.get(contactObj.Id)){
	            HexaBPM__Service_Request__c inprincipleSR = new HexaBPM__Service_Request__c();
	            inprincipleSR.recordTypeId = Label.OB_IN_PRINCIPLE_ID;
                contactRecord = mapContact.get(contactObj.Id);
                account accountObj = mapConAccId.get(contactObj.id);

				if(MapOpportunities.get(accountObj.Id)!=null)//v1.1
                    inprincipleSR.Opportunity__c = MapOpportunities.get(accountObj.Id);//v1.1
                    
	            /* for(OB_InPrinciple_SR_Map__c settingObj : OB_InPrinciple_SR_Map__c.getAll().values()) {
	                if(settingObj.SourceObjectField__c != null && settingObj.SourceObjectField__c.indexOf('.') > -1){
	                    lstLookUpField = settingObj.SourceObjectField__c.split('\\.');
	                    if(lstLookUpField != null && lstLookUpField.size() > 0 && lstLookUpField.size() < 3){
	                        if(contactRecord.getSobject(lstLookUpField[0]) != null)
	                            lookupValue = (String) contactRecord.getSobject(lstLookUpField[0]).get(lstLookUpField[1]);
	                        if(String.isNotBlank(lookupValue))
	                            inprincipleSR.put(settingObj.TargetObjectField__c,lookupValue);
	                    }
	                }   
	                else 
	                    inprincipleSR.put(settingObj.TargetObjectField__c,contactRecord.get(settingObj.SourceObjectField__c));
	                
	                //Populating the Linked SR Field
	                if(mapLinkedSR.get(contactObj.id) != null)
	                    inprincipleSR.put('HexaBPM__Linked_SR__c',mapLinkedSR.get(contactObj.id));
                } */

                InPrincipleSR.put('Building__c',contactRecord.OB_Building__c);
                InPrincipleSR.put('Business_Sector__c',accountObj.OB_Sector_Classification__c);
                InPrincipleSR.put('HexaBPM__Contact__c',contactRecord.id);
                InPrincipleSR.put('HexaBPM__Customer__c',accountObj.id);
                InPrincipleSR.put('HexaBPM__Email__c',contactRecord.Email);
                InPrincipleSR.put('Entity_Name__c',accountObj.Name);
                InPrincipleSR.put('Entity_Type__c',accountObj.Company_Type__c);
                InPrincipleSR.put('first_name__c',contactRecord.FirstName);
                InPrincipleSR.put('OB_Is_Multi_Structure_Application__c',accountObj.OB_Is_Multi_Structure_Application__c);
                InPrincipleSR.put('last_name__c',contactRecord.LastName);
                InPrincipleSR.put('Licensing_Period__c',contactRecord.OB_Licensing_Period__c);
                InPrincipleSR.put('HexaBPM__Send_SMS_to_Mobile__c',accountObj.Phone);
                InPrincipleSR.put('Restricted_license__c',contactRecord.OB_Restricted_license__c);
                InPrincipleSR.put('Retail_license__c',contactRecord.OB_Retail_license__c);
                InPrincipleSR.put('Type_of_Property__c',contactRecord.OB_Type_of_Property__c);

                //Populating the Linked SR Field
                if(mapLinkedSR.get(contactObj.id) != null)
                inprincipleSR.put('HexaBPM__Linked_SR__c',mapLinkedSR.get(contactObj.id));
                
                //  V 1.0   Merul populating the contact on SR if contact(OB_Block_Homepage__c) is checked
                if( contactRecord.OB_Block_Homepage__c )
                    inprincipleSR.put('HexaBPM__Contact__c',contactRecord.id);  

	            lstInPrincipleSR.add(inprincipleSR);
        	}
        } 
        if(lstInPrincipleSR != null && lstInPrincipleSR.size() > 0)
            insert lstInPrincipleSR;

    }
    
    public static void createNonFinancialSRForExstConHelper(Map<Id,account> mapConAccId){

        list<contact> lstContact = new list<contact>();
        list<HexaBPM__Service_Request__c> lstSR = new list<HexaBPM__Service_Request__c>();
        list<string> lstContactId = new list<string>();
        string lookupValue;
        Contact contactRecord;
        list<string> lstLookUpField = new list<string>();
        map<string,Contact> mapContact = new map<string,Contact>();
        map<string,boolean> mapCommercialPermission = new map<string,boolean>();
        String commaSepratedFields = '';
        string soql;

        /* for(Contact contObj : lstContact)
            lstContactId.add(contObj.id); */
        for(id contId : mapConAccId.keySet()){
            lstContactId.add(contId);
        }   

		set<string> setQueryFields = new set<string>();
		for(OB_User_Registration_SR_Map__c settingObj : OB_User_Registration_SR_Map__c.getAll().values()) {
            if(settingObj.SourceObjectField__c!=null)
            	setQueryFields.add(settingObj.SourceObjectField__c.toLowerCase());
        }
        setQueryFields.add('account.is_commercial_permission__c');
        for(string strField : setQueryFields) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = strField;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + strField;
            }
        }
        set<string> setAccountId = new set<string>();
        soql = 'select ' +commaSepratedFields+',OB_Block_Homepage__c from Contact where id in :lstContactId';
        for(Contact contactObj : Database.query(soql)){
            mapContact.put(contactObj.id,contactObj);
           /*  if(contactObj.account.is_commercial_permission__c=='Yes')
            	mapCommercialPermission.put(contactObj.Id,true);
            else
            	mapCommercialPermission.put(contactObj.Id,false);
            	
            if(contactObj.AccountId!=null)
                setAccountId.add(contactObj.AccountId); */

            lstContact.add(contactObj);

            if(mapConAccId.get(contactObj.id) != null && mapConAccId.get(contactObj.id).is_commercial_permission__c=='Yes')
            	mapCommercialPermission.put(contactObj.Id,true);
            else
            	mapCommercialPermission.put(contactObj.Id,false);
            	
            if(mapConAccId.get(contactObj.id).id !=null)
                setAccountId.add(mapConAccId.get(contactObj.id).id);
            
        }
		
		//Code to check SR Exists or not
        map<string,string> MapAccountUserRegApplication = new map<string,string>();
        map<string,string> MapAccountInPrincpApplication = new map<string,string>();
        map<string,string> MapOpportunities = new map<string,string>();//v1.1
        set<string> setRecordTypeIds = new set<string>{Label.OB_USER_REG_RECORDTYPEID,Label.OB_IN_PRINCIPLE_ID};
        if(setAccountId.size()>0){
        	for(Opportunity Opp:[Select Id,AccountId from Opportunity where AccountId IN:setAccountId order by CreatedDate]){//v1.1
        		MapOpportunities.put(Opp.AccountId,Opp.Id);
        	}
	        for(HexaBPM__Service_Request__c objSR:[Select Id,HexaBPM__Customer__c,HexaBPM__Record_Type_Name__c from HexaBPM__Service_Request__c where HexaBPM__Customer__c IN:setAccountId and RecordTypeId IN:setRecordTypeIds and HexaBPM__IsCancelled__c=false and HexaBPM__Is_Rejected__c=false]){
	        	if(objSR.HexaBPM__Record_Type_Name__c=='Converted_User_Registration')
	        		MapAccountUserRegApplication.put(objSR.HexaBPM__Customer__c,objSR.Id);
	        	else if(objSR.HexaBPM__Record_Type_Name__c=='In_Principle')
	        		MapAccountInPrincpApplication.put(objSR.HexaBPM__Customer__c,objSR.Id);
	        }
        }
		
        for(Contact contactObj : lstContact){
            if(contactObj.AccountId!=null && MapAccountUserRegApplication.get(contactObj.AccountId)==null && !mapCommercialPermission.get(contactObj.Id)){
                
            }
        }
    }
}