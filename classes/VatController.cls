public class VatController{
    
    public string AccountTax{get;set;}
    public string AccountTaxPopup{get;set;}
    public Account acc{get;set;}
    public string id{get;set;}
    public string selectedOption {get;set;}
    public string OldTaxNumber {get;set;}
    
    string CustomerId;
    
    public VatController(){
    
        Init();
    
    }
    public PageReference UpdateTax()
    {
        ACC.VAT_Option__c= selectedOption; 
        ACC.VAT_Info_Updated_DateTime__c = DateTime.Now();
        
        if(selectedOption == 'Is Vat')
            ACC.We_undertake_to_update_VAT__c = false;
         
        
        if(OldTaxNumber != acc.Tax_Registration_Number_TRN__c )
            acc.VAT_Info_Updated__c = true;
          
        if(selectedOption == 'Is Not Vat')
            ACC.Tax_Registration_Number_TRN__c = 'Not Applicable';
        
        if(!test.isRunningTest()) 
        {
                update ACC;         
                
                
                 ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Your Tax Registration Number (TRN) information has been saved successfully .');
                ApexPages.addMessage(myMsg);
            
                
        }           
         
        
      /* PageReference pageRef = new PageReference('/apex/Home');
       pageRef.setRedirect(true);
       return pageRef;*/
       return null;
    }
    
    void Init(){
    
         User objUser = [select Id,Tax_Registration_Number_TRN__c,AccountId,Contact.Account.RecordType.DeveloperName,Account_ID__c,Contact.Account.ROC_Status__c,Community_User_Role__c from User where Id=:Userinfo.getUserId()];
         AccountTax = objUser.Tax_Registration_Number_TRN__c;
         CustomerId = objUser.AccountId;
         
         system.debug('### ' +CustomerID);
         
         
     
             acc = [Select id,VAT_Option__c,
                     We_undertake_to_update_VAT__c,We_confirm_our_obligation_to_pay__c,VAT_Info_Updated__c ,
                     Tax_Registration_Number_TRN__c from Account where ID =: CustomerId  limit 1];
             id = acc.ID;
             selectedOption = acc.VAT_Option__c;
             OldTaxNumber = acc.Tax_Registration_Number_TRN__c ;
        
            
         
    
    }
    
}
    
    //End VAT