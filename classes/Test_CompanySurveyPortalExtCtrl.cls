@isTest(SeeAllData=false)
public class Test_CompanySurveyPortalExtCtrl {
 static testMethod void businessfeedbackMethod() {     
     
     Account acc = new Account();
     acc.Name = 'feedback account';
     insert acc;
     
     Contact objContact= new Contact();
     objContact.FirstName = 'feedback';
     objContact.LastName = 'Form';
     objContact.AccountId = acc.id;
     insert objContact;     
      Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community Plus User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
         system.runAs(objUser)
    {
     Company_Survey__c ff = new Company_Survey__c();
      ff.Company__c= objContact.AccountId;    
     ff.Contact__c = objContact.id;
       ff.X11_No_of_Employees__c= 12.12;
      
     insert ff;
          
     Test.startTest();
    
     
        PageReference pageRef = Page.Company_Survey; // Add your VF page Name here
     Apexpages.currentPage().getParameters().put('ValParam', 'Email_Address__c');
      Apexpages.currentPage().getParameters().put('myParam', 'aru@sd.com');
    

        ApexPages.StandardController controller1 = new ApexPages.StandardController(ff);
     CompanySurveyPortalExtCtrl  feedctr2 = new CompanySurveyPortalExtCtrl (controller1);
      
      feedctr2.RowValueChange();
      
     
     feedctr2.pageLoad();
     feedctr2.SaveComapnySurvey();
        feedctr2.doAttachmentnew();
     feedctr2.SaveComapnySurveyNew();
     Test.stopTest();
}

     
 }     
      
}