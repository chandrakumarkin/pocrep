@isTest
public class OB_CreateDraftSRTest {
    @testSetup
    static  void createTestData() {
        // create account
        List<Account> lstNewAccount = new List<Account>();
        lstNewAccount =  OB_TestDataFactory.createAccounts(1);
        insert lstNewAccount;

        // create contact
        List<Contact> lstContacts = new List<Contact>();
        lstContacts = OB_TestDataFactory.createContacts(1,lstNewAccount);
        insert lstContacts;
        
        Opportunity opp = new Opportunity();
		opp.AccountId = lstNewAccount[0].id;
        opp.Name = 'test';
        opp.StageName='Prospecting';
        opp.CloseDate=System.today().addMonths(1);
        insert opp;

        //Create Custom Setting Records
        OB_Complete_Profile_Map__c customSetMap = new OB_Complete_Profile_Map__c();
        customSetMap.Name = 'AccountId';
        customSetMap.SourceObjectField__c = 'AccountId';
        customSetMap.TargetObjectField__c = 'HexaBPM__Customer__c';
        insert customSetMap;

        customSetMap = new OB_Complete_Profile_Map__c();
        customSetMap.Name = 'Mobile';
        customSetMap.SourceObjectField__c = 'Account.Phone';
        customSetMap.TargetObjectField__c = 'HexaBPM__Send_SMS_to_Mobile__c';
        insert customSetMap;

        OB_AOR_SR_Map__c customAORSetMap = new OB_AOR_SR_Map__c();
        customAORSetMap.Name = 'AccountId';
        customAORSetMap.SourceObjectField__c = 'AccountId';
        customAORSetMap.TargetObjectField__c = 'HexaBPM__Customer__c';
        insert customAORSetMap;

        customAORSetMap = new OB_AOR_SR_Map__c();
        customAORSetMap.Name = 'Mobile';
        customAORSetMap.SourceObjectField__c = 'Account.Phone';
        customAORSetMap.TargetObjectField__c = 'HexaBPM__Send_SMS_to_Mobile__c';
        insert customAORSetMap;
    }
    static testMethod void CreateDraftSRTest() {
        List<Contact> lstContacts = [select id,AccountId,Account.Phone from Contact];
         List<account> lstNewAccount = [select id,Is_Commercial_Permission__c from account];
        test.startTest();
            OB_CreateDraftSR.createSR(lstContacts);
        OB_CreateDraftSR.createSRForExstCon(new map<id,account>{lstContacts[0].id => lstNewAccount[0]});
         OB_CreateDraftSR.createSR(lstContacts);
        lstNewAccount[0].is_commercial_permission__c = 'yes';
        update lstNewAccount[0];
         OB_CreateDraftSR.createSRForExstCon(new map<id,account>{lstContacts[0].id => lstNewAccount[0]});
        test.stopTest();
    }

}