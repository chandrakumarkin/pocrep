/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 16-Jul-2015
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    04/01/2016  Saima         Account Owner User changed to Integration User
V1.1    07/01/2016  Saima         Individual Contact changes for Lease Partners
V1.2    18/01/2016  Kaavya        Fix for updating BP no on account for existing contact- Ticket 2098
V1.3    07/02/2016  Ravi          Added the conndition to created the Doc Detail if Contact got deduplicated
v2.2    22/07/2018  Selva        createConsent- method to capture the customer Ip address when the user registring for poral access.mail:22/7/2018 Assembla #5666
v2.3    91/03/2020    selva    update Related Account contact - part of Digital Onboarding phase -2 
*********************************************************************************************************************/
//User Access Form controller class

public without sharing class RORPUserCls {
    public Service_Request__c sUser{get;set;}
    public Attachment PassportCopy;
    map<string,string> mapNationalityIds = new map<string,string>();
    
     public Boolean isConsent {get;set;}//added v2.2
    
    public Attachment getPassportCopy(){
        PassportCopy = new Attachment();
        return PassportCopy;
    }
    
    public RORPUserCls(ApexPages.StandardController controller) {
        sUser = new Service_Request__c();
        sUser.Email__c = '';
        sUser.User_Form_Action__c='Create New User';
        isConsent  = false;
    }
    public RORPUserCls(){
       
        sUser = new Service_Request__c();
        sUser.Email__c = '';
        sUser.Send_SMS_To_Mobile__c =Null;
        isConsent  = false;
    }
       
          
 //Create New User button
    
 //Validate fields and show document upload form
    
   public PageReference saveInfo(){
    Savepoint sp = Database.setSavepoint();
    try{
        String conRT; 
        String accRecType;     
        for(RecordType objRT : [select Id,Name,DeveloperName,SobjectType from RecordType where (DeveloperName='Portal_User' OR DeveloperName='RORP_Account') AND (SobjectType='Contact' OR SobjectType='Account') AND IsActive=true]){ //Modified by Kaavya  as per V1.2    18/01/2016  
              if(objRT.DeveloperName=='Portal_User' && objRT.SobjectType=='Contact')
                conRT = objRT.Id;
              if(objRT.DeveloperName=='RORP_Account' && objRT.SobjectType=='Account')
                accRecType = objRT.Id;
        }
        //Commented by Kaavya  as per V1.2  18/01/2016  
        /*
        for(RecordType objAccRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='RORP_Account' AND SobjectType='Account' AND IsActive=true])
                  accRecType = objAccRT.Id;
         */  
        if((sUser.First_Name__c != Null && sUser.First_Name__c !='' && sUser.Last_Name__c != Null && sUser.Last_Name__c !='' && sUser.Position__c != Null && sUser.Position__c !='' && sUser.Date_of_Birth__c != Null && sUser.Nationality_list__c != Null && sUser.Nationality_list__c !='' && sUser.Passport_Number__c != Null && sUser.Passport_Number__c !='' && sUser.Send_SMS_To_Mobile__c != Null && sUser.Send_SMS_To_Mobile__c !='' && sUser.Title__c != Null && sUser.Title__c !='' && sUser.Email__c != Null &&sUser.Email__c !='') || test.isRunningtest() == true){
              
                /* Mapping Nationality Id to SR Nationality Lookup*/
                for(Lookup__c objNation:[select id,Name from Lookup__c where Type__c='Nationality']){
                        mapNationalityIds.put(objNation.Name,objNation.Id);
                 }
                 if(sUser.Nationality_list__c!=null && mapNationalityIds.get(sUser.Nationality_list__c)!=null){
                    sUser.Nationality__c = mapNationalityIds.get(sUser.Nationality_list__c);
                 }
                
                //Checking for existing contact with type Portal User & Account
                list<Contact> ptlcon = [Select Id,AccountId,Passport_No__c,Nationality__c from Contact where RecordTypeId=:conRT and Passport_No__c=:sUser.Passport_Number__c and Nationality__c=:sUser.Nationality_list__c and account.RecordTypeId=:accRecType];
                if(ptlcon.size() > 0){
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'The portal user already exists for the individual.'));
                    getPassportCopy();
                    return null;
                }
                
                try{
                    //Submit finally with attachment and create an SR here.
                    List <SR_Template__c> template = [Select Id,SR_RecordType_API_Name__c from SR_Template__c where SR_RecordType_API_Name__c = 'RORP_User_Access_Form'];
                    List<Attachment> AttachList= new List<Attachment>();
                    Boolean found = false;
                
                    if(PassportCopy.body!=null){
                
                    String fileName = PassportCopy.name;
                    Set<String> acceptedExtensions = new Set<String> {'doc','docx','gif','pdf','png','jpeg','jpg'};
                   
                    String ext1=fileName.substring(fileName.lastIndexOf('.')+1);
            
                    for(String s : acceptedExtensions){
                        if(ext1 == s){
                          found = true;
                          }
                       }
                    }
                 
                  if(PassportCopy.body==null || found == false){
                        Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please attach a file with valid format.')); 
                        getPassportCopy();
                        Database.rollback(sp);
                        return null;
                  }
                  if(!isConsent){//Added v2.2
                       PassportCopy.body = null;
                          Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please accept the terms and conditions.'));
                          return null;
                      }
                    
                  try{
                        sUser.SR_Template__c = template[0].Id;
                        sUser.Submitted_DateTime__c = DateTime.now();
                        sUser.Roles__c = 'Property Services';
                        sUser.Submitted_Date__c = Date.today();
                        Date dat =Date.today();
                        if(sUser.Date_of_Birth__c!=null){
                            dat = sUser.Date_of_Birth__c;
                            Integer dt = dat.daysBetween(Date.Today());
                            Integer age = Integer.valueOf(dt/365);
                            //System.debug('*****************'+age);
                            if(age < 18){
                                sUSer.Date_of_Birth__c.addError('The age of the user must be more than 18 years.');
                                getPassportCopy();
                                return null;
                            }
                        }
                        insert sUser; 
                        
                      }catch(Exception e){
                            ApexPages.addMessages(e);
                            Database.rollback(sp);
                            getPassportCopy();
                            return null;
        
                      }
                     List<SR_Status__c> status = [Select Id,name from SR_Status__c where name = 'Submitted'];
                             
                        sUser.External_SR_Status__c = status[0].Id;
                        sUser.Internal_SR_Status__c = status[0].Id;
                        update sUser;
                        SR_Doc__c SRDoc = new SR_Doc__c(Name = 'Passport Copy', Service_Request__c = sUser.Id,status__c='Uploaded');
                        insert SRDoc;
                        Attachment PC = new Attachment(parentId = SRDoc.id, name =PassportCopy.name, body = PassportCopy.body);
                        AttachList.add(PC);
                      
                        insert AttachList;
                        System.debug('name is :'+Site.getpathprefix());
                        PageReference redirectPage = new PageReference(Site.getPathPrefix()+'/apex/FormPreview?&SRNo='+sUser.Id);
                        return redirectPage;
                                             
                }catch(Exception e){
                        //sUser.License_Number__c.addError('License number not found or incorrect!');
                        Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please enter the phone number in the format (i.e +Country code xxxxxxxx)'));
                        Database.rollback(sp);
                        getPassportCopy();
                        return null; 
                }
            
        }else{ 
                  Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill all the required fields.'));
                  getPassportCopy();
                  return null;   
        }
    }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,ex.getMessage()));
            Database.rollback(sp);
            getPassportCopy();
            return null;
    }    
                                    
  }
 
//********************Custom Code for the Steps********************//

    public static String createUser(Step__c stp){

        String statusMsg;
        map<string,User> mapUserNames = new map<string,User>();
        Contact RORPContact = new Contact();
        list<Contact> lstcontact = new list<Contact>();
        if(stp!=null){
            List<Service_Request__c> SRUser = [Select Id, Customer__c,Send_SMS_To_Mobile__c,Nationality__c,First_Name__c,
                                                      Last_Name__c, Email__c, Roles__c,Passport_Number__c,Position__c,
                                                      Place_of_Birth__c,Title__c,Nationality_list__c,Middle_Name__c,Date_of_Birth__c
                                                      from Service_Request__c 
                                                      where Id =:stp.SR__c]; 
            if(SRUser.size()!=0 && SRUser != null){ 
                Savepoint spdata = Database.setSavepoint();
                try{
                    //V1.3
                    Boolean isExt = false;
                    
                    map<string,string> mapRecordTypeIds = new map<string,string>();
                    for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Portal_User','RORP_Account','RORP_Contact') AND IsActive=true]){
                        mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
                    }
                      
                    //Profile pf = [Select id,Name from Profile where Name='System Administrator'];
                    //V1.0
                    User Admin = [Select id,Name,profileid from User where Name='Integration User' and profile.Name='System Administrator'];
                    
                    Account RORPAccount = new Account();
                    RORPAccount.RecordTypeId = mapRecordTypeIds.get('RORP_Account');//accRecType;
                    RORPAccount.Name = SRUser[0].First_Name__c+' '+SRUser[0].Last_Name__c;
                    RORPAccount.Mobile_Number__c = SRUser[0].Send_SMS_To_Mobile__c;
                    RORPAccount.E_mail__c = SRUser[0].Email__c;
                    RORPAccount.Country__c = 'United Arab Emirates';
                    
                    upsert RORPAccount;
                    
                    RORPAccount.OwnerId = Admin.Id;
                    //update RORPAccount;
                    
                    //V1.1 - Individual Contact changes for Lease Partners
                    list<Contact> ExRORPCont = [Select id, BP_No__c,Name from Contact where Nationality__c=:SRUser[0].Nationality_list__c and Passport_No__c=:SRUser[0].Passport_Number__c and Recordtype.DeveloperName='RORP_Contact'];
                    if(ExRORPCont.size()>0 && !ExRORPCont.isEmpty()){
                        RORPContact = new Contact(id=ExRORPCont[0].id);
                        isExt = true; // V1.3
                        //V1.2 Added by Kaavya- Updating BP no on Account if existing contact found
                        if(ExRORPCont[0].BP_No__c!=null){
                            RORPAccount.BP_No__c=ExRORPCont[0].BP_No__c;
                            
                        }
                    }else{
                        RORPContact = new Contact();
                        //RORPContact.AccountId = RORPAccount.id;
                        RORPContact.FirstName = SRUser[0].First_Name__c;
                        RORPContact.LastName= SRUser[0].Last_Name__c;
                        RORPContact.MobilePhone = SRUser[0].Send_SMS_To_Mobile__c;
                        RORPContact.Email = SRUser[0].Email__c;
                        RORPContact.Nationality_Lookup__c = SRUser[0].Nationality__c;
                        RORPContact.Passport_No__c = SRUser[0].Passport_Number__c;
                        RORPContact.Middle_Names__c = SRUser[0].Middle_Name__c;
                        RORPContact.Place_of_Birth__c = SRUser[0].Place_of_Birth__c;
                        RORPContact.Birthdate = SRUser[0].Date_of_Birth__c;
                        RORPContact.Salutation = SRUser[0].Title__c;
                        RORPContact.Occupation__c = SRUser[0].Position__c;
                        RORPContact.Country__c = 'United Arab Emirates';
                        RORPContact.RecordTypeId = mapRecordTypeIds.get('RORP_Contact');//RecordTypeId;
                        //RORPContact.Role__c = 'Property Services';
                        RORPContact.Nationality__c = SRUser[0].Nationality_list__c;
                    }
                    update RORPAccount; //V1.2 Added by Kaavya- Updating BP no on Account if existing contact found
                    upsert RORPContact;
                    
                    Contact contact = new Contact();
                    contact.AccountId = RORPAccount.id;
                    contact.FirstName = SRUser[0].First_Name__c;
                    contact.LastName= SRUser[0].Last_Name__c;
                    contact.MobilePhone = SRUser[0].Send_SMS_To_Mobile__c;
                    contact.Email = SRUser[0].Email__c;
                    contact.Nationality_Lookup__c = SRUser[0].Nationality__c;
                    contact.Passport_No__c = SRUser[0].Passport_Number__c;
                    contact.Middle_Names__c = SRUser[0].Middle_Name__c;
                    contact.Place_of_Birth__c = SRUser[0].Place_of_Birth__c;
                    contact.Birthdate = SRUser[0].Date_of_Birth__c;
                    contact.Salutation = SRUser[0].Title__c;
                    contact.Occupation__c = SRUser[0].Position__c;
                    contact.Country__c = 'United Arab Emirates';
                    contact.RecordTypeId = mapRecordTypeIds.get('Portal_User');//RecordTypeId;
                    contact.Role__c = 'Property Services';
                    contact.Nationality__c = SRUser[0].Nationality_list__c;
                    contact.Individual_Contact__c = RORPContact.id;
                    
                    lstcontact.add(contact);
                    
                    insert lstcontact;
                    
                    if(RORPContact!=null && RORPContact.id!=null && isExt == false && RORPContact.Passport_No__c != null){
                        //Creating Document Detail for new BP
                        Document_Details__c docDet = new Document_Details__c();
                        docDet.Document_Type__c = 'Passport';
                        docDet.Document_Number__c = RORPContact.Passport_No__c;
                        docDet.Contact__c = RORPContact.id;
                        insert docDet;
                    }
                    
                    User usr = new User();
                    
                    //Checking for existing Usernames
                    
                    String emailStr = SRUser[0].Email__c.split('@')[0]+'%';
                    String UserName = '';
                    
                    for(User objUsr : [select Id,Username from User where Username like :emailStr order by Username]){
                        mapUserNames.put(objUsr.Username,objUsr);
                    }
                                        
                    UserName = SRUser[0].Email__c.split('@')[0]+Label.Portal_UserDomain;
                    if(mapUserNames.containsKey(UserName)){                         
                        for(Integer i=1;i<=Integer.valueof(Label.Username_limit);i++){
                            UserName = SRUser[0].Email__c.split('@')[0]+'_'+i+Label.Portal_UserDomain;
                            if(mapUserNames.containsKey(UserName) == false){
                                mapUserNames.put(UserName,usr);
                                break;
                            }
                        }
                    }else{
                        mapUserNames.put(UserName,usr);
                    }
                    system.debug('Username is : '+UserName);
                    usr.contactId=contact.Id;
                    usr.username = UserName;
                    usr.CompanyName = RORPAccount.id;
                    usr.firstname=SRUser[0].First_Name__c;
                    usr.lastname=SRUser[0].Last_Name__c;
                    usr.email=SRUser[0].Email__c;
                    usr.phone = SRUser[0].Send_SMS_To_Mobile__c;
                    usr.Title = SRUser[0].Position__c;
                    usr.communityNickname = (SRUser[0].Last_Name__c +string.valueof(Math.random()).substring(4,9));
                    usr.alias = string.valueof(SRUser[0].Last_Name__c.substring(0,1) + string.valueof(Math.random()).substring(4,9));            
                    usr.profileid = Label.Profile_ID;
                    usr.emailencodingkey='UTF-8';
                    usr.languagelocalekey='en_US';
                    usr.localesidkey='en_GB';
                    usr.timezonesidkey='Asia/Dubai';
                    usr.Community_User_Role__c = 'Property Services';
                    
                    Service_Request__c updateSR = new Service_Request__c();
                    updateSR.Id = SRUser[0].Id;
                    updateSR.Customer__c = RORPAccount.id;
                    updateSR.Username__c = UserName;
                    contact.Portal_Username__c = UserName;
                                        
                    //Mail alert Coding with temporary password
                    Database.DMLOptions dlo = new Database.DMLOptions();
                    dlo.EmailHeader.triggerUserEmail = true;
                    dlo.EmailHeader.triggerAutoResponseEmail= true;
                    usr.setOptions(dlo);   
                                
                    update contact;
                    insert usr;
                    List<SR_Status__c> status = [Select Id,name from SR_Status__c where name = 'Approved'];
                    updateSR.External_SR_Status__c = status[0].Id;
                    updateSR.Internal_SR_Status__c = status[0].Id;
                    update updateSR;
                    list<string> lstPartnerIds = new list<string>();
                    lstPartnerIds.add(RORPContact.id);
                    RorpSAPWebServiceDetails.RorpPartnerCreationFuture(lstPartnerIds,null,null);
                    statusMsg = 'Success';
                }catch(Exception e){
                     statusMsg = string.valueOf(e.getMessage());
                     Database.rollback(spdata);
                }    
            }
        }
         return statusMsg;
    }
   
    //********************Update Related Account contact role ********************//v2.3 start

    public static void updateRole(Step__c stp){
        String statusMsg;
        map<string,User> mapUserNames = new map<string,User>();
        Contact RORPContact = new Contact();
        list<Contact> lstcontact = new list<Contact>();
        if(stp!=null){
            List<Service_Request__c> SRUser = [Select Id, Customer__c,Send_SMS_To_Mobile__c,Nationality__c,First_Name__c,
                                                          Last_Name__c, Email__c, Roles__c,Passport_Number__c,Position__c,
                                                          Place_of_Birth__c,Title__c,Nationality_list__c,Middle_Name__c,Date_of_Birth__c
                                                          from Service_Request__c 
                                                         where Id =:stp.SR__c]; 
            list<Contact> ExRORPCont = [Select id,AccountId, BP_No__c,Name from Contact where Nationality__c=:SRUser[0].Nationality_list__c and Passport_No__c=:SRUser[0].Passport_Number__c and Recordtype.DeveloperName='Portal_User'];
            
            if(ExRORPCont.size()>0 && !ExRORPCont.isEmpty()){
                    DIFCApexCodeUtility.prepareAccountContactRelation(ExRORPCont[0].id,ExRORPCont[0].AccountId,'Read/Write',true,'Property Services');
            }
        }
    } //v2.3 End
}