/**
 * Description : Controller for OB_CPRegisteredAddress component
 *
 * ****************************************************************************************
 * History :
 * [04.APRL.2020] Prateek Kadkol - Code Creation
 * v1.2 [05.MAP.2020] Prateek Kadkol - added new fields to query for ticket fix
 * V1.3 [14.DEC.2020] salma.rasheed@pwc.com - Added condition for Commercial Permission Pocket Shops to display the operating locations in the community
 */
public without sharing class OB_CPRegisteredAddressController 
{

@AuraEnabled
public static ResponseWrapper getCPRegisterAddress(String reqWrapPram) 
{


    //reqest wrpper
    RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);

        //response wrpper
    ResponseWrapper respWrap = new ResponseWrapper();

    Map<Id,HexaBPM__Service_Request__c> mapAccIdSROL = new Map<Id,HexaBPM__Service_Request__c>();
    Map<Id,HexaBPM__Service_Request__c> mapAccIdSRVenue = new Map<Id,HexaBPM__Service_Request__c>();

    respWrap.lstOprLocWrap = new List<OperationLocationWrapper>();
    respWrap.lstVenueWrap = new List<VenueWrapper>();
    
    String srId = reqWrap.srId;
    respWrap.srWrap = new SRWrapper();
    respWrap.srWrap.amedWrapLst = new List<AmendmentWrapper>();

    Set<String> selectedAmedOptLoc = new Set<String>();

    
        //For existing opertating Location Amendment
        for(HexaBPM_Amendment__c amedObj : getExistingAmendment(srId,'Operating Location')) 
        {
            AmendmentWrapper amedWrap = new AmendmentWrapper();
            amedWrap.amedObj = amedObj;
            selectedAmedOptLoc.add(amedObj.Operating_Location__c);
            respWrap.srWrap.amedWrapLst.add(amedWrap);
        }

    //Quering SR
    String typeOfComPermission;
    for( HexaBPM__Service_Request__c srReq   :   [
                                                    SELECT id,
                                                            HexaBPM__Customer__c,
                                                            Type_of_commercial_permission__c,
                                                            Name_of_the_Hosting_Affiliate__c 
                                                        FROM HexaBPM__Service_Request__c
                                                        WHERE id =: srId
                                                        LIMIT 1
                                                    ]
        )
    {
                typeOfComPermission = srReq.Type_of_commercial_permission__c;
            if( 
                typeOfComPermission == 'ATM' 
                        || typeOfComPermission == 'Vending Machines' 
                                    //|| typeOfComPermission == 'Dual license of DED licensed firms with an affiliate in DIFC' 
                                        || typeOfComPermission == 'Others as authorised by Registrar' || typeOfComPermission == 'Gate Avenue Pocket Shops' //V1.3 Added condition for Type of Commercial Permission as Pocket Shops
            )
            {
                mapAccIdSROL.put(srReq.HexaBPM__Customer__c,srReq);
            }

            if( typeOfComPermission == 'Dual license of DED licensed firms with an affiliate in DIFC' )
            {
                mapAccIdSROL.put(srReq.Name_of_the_Hosting_Affiliate__c,srReq);
            }



            if(
                typeOfComPermission == 'Training or educational services offered by non-DIFC registered entities' 
                    || typeOfComPermission =='Events' 
                        || typeOfComPermission == 'Others as authorised by Registrar'
                            
            )
            {
                mapAccIdSRVenue.put(srReq.HexaBPM__Customer__c,srReq);
            }
            respWrap.srWrap.srObj = srReq;
			system.debug('###### respWrap.srWrap.srObj '+respWrap.srWrap.srObj);
    }

    system.debug('###### mapAccIdSROL '+mapAccIdSROL);
    
    if( mapAccIdSROL.size() > 0 )
    {
        //v1.2
        for(Operating_Location__c operLoc : [
                                                SELECT id,
                                                        Unit__c,
                                                        Unit__r.Name,
                                                        Floor__c,
                                                        Building_Name__c	,
                                                        IsRegistered__c ,
                                                        Type__c,
                                                        Status__c
                                                    FROM Operating_Location__c
                                                WHERE Account__c IN: mapAccIdSROL.keySet()
                                                    /* AND IsRegistered__c = true */
                                                    AND Status__c = 'Active'
                                            ]
            )
        {
            if( !selectedAmedOptLoc.contains(operLoc.Id ) )
            {
                /* OperationLocationWrapper oprWrap = new OperationLocationWrapper();
                oprWrap.oprLocObj = operLoc;
                respWrap.lstOprLocWrap.add( oprWrap );  */

                if(typeOfComPermission != null && (typeOfComPermission == 'ATM' 
                || typeOfComPermission == 'Vending Machines' 
                || typeOfComPermission == 'Gate Avenue Pocket Shops' )){//V1.3 Added condition for Type of Commercial Permission as Pocket Shops

                    
                        OperationLocationWrapper oprWrap = new OperationLocationWrapper();
                        oprWrap.oprLocObj = operLoc;
                        respWrap.lstOprLocWrap.add( oprWrap ); 
                    
                }else{
                    if(operLoc.IsRegistered__c == true){
                    OperationLocationWrapper oprWrap = new OperationLocationWrapper();
                    oprWrap.oprLocObj = operLoc;
                    respWrap.lstOprLocWrap.add( oprWrap ); 
                    }
                }
                
            }
            
        }
        system.debug('###### respWrap.lstOprLocWrap '+respWrap.lstOprLocWrap);

    }

    if( mapAccIdSRVenue.size() > 0 )
    {
        for(
            OB_Event_Contract__c evntContract : [
                                                    SELECT id,
                                                            Account__c,
                                                            Venue__c,
                                                            Venue__r.Building__c,
                                                            Venue__r.Level__c,
                                                            Venue__r.Unit__c
                                                            
                                                        FROM OB_Event_Contract__c
                                                        WHERE Account__c IN: mapAccIdSRVenue.keySet() 
                                                        AND Valid_To__c > TODAY
                                                ]

            )
        {
            VenueWrapper venuWrap = new VenueWrapper();
            venuWrap.venueObj = new OB_Venue__c();
            venuWrap.venueObj.Building__c = evntContract.Venue__r.Building__c; 
            venuWrap.venueObj.Level__c = evntContract.Venue__r.Level__c;
            venuWrap.venueObj.Unit__c = evntContract.Venue__r.Unit__c;

            respWrap.lstVenueWrap.add( venuWrap ); 
        }
    }

    for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
                                                AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
        respWrap.ButtonSection = sectionObj;
    }
    
    return respWrap;
}



@AuraEnabled
public static ResponseWrapper onAmedSaveToDB(String reqWrapPram) 
{
    system.debug('########## reqWrapPram '+reqWrapPram);
    //reqest wrpper
    RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);

    //response wrpper
    ResponseWrapper respWrap = new ResponseWrapper();

    List<RecordType> lstRecTyp = new List<RecordType>();
    lstRecTyp = [SELECT id, 
                            DeveloperName, 
                            SobjectType
                    FROM RecordType
                    WHERE DeveloperName = 'Operating_Location'
                    AND SobjectType = 'HexaBPM_Amendment__c'];


    Operating_Location__c  objOL = reqWrap.oprLocWrap.oprLocObj;
    HexaBPM__Service_Request__c srObj = reqWrap.srWrap.srObj;

    
    if(objOL != NULL) 
    {
        HexaBPM_Amendment__c objAmd = new HexaBPM_Amendment__c();
        objAmd.Customer__c = objOL.Account__c;
        objAmd.ServiceRequest__c = srObj.Id;
        objAmd.Amendment_Type__c = 'Operating Location';
        //objAmd.IsRegistered__c = objOL.IsRegistered__c;
        objAmd.IsRegistered__c = true;
        objAmd.Unit__c = objOL.Unit__c;
        objAmd.Hosting_Company__c = objOL.Hosting_Company__c;
        objAmd.Operating_Type__c = objOL.Type__c;
        objAmd.Operating_Location__c = objOL.Id;
        objAmd.Operating_Location_Status__c = objOL.Status__c;
        //objAmd.Building__c = objOL.Building__c;
        objAmd.Status__c = 'Active';
        objAmd.recordTypeId = (lstRecTyp != NULL ? lstRecTyp [0].Id :NULL);

        if(objAmd.Role__c == NULL) {
            objAmd.Role__c = 'Operating Location';
        } 
        else if(objAmd.Role__c.indexOf('Operating Location') ==-1) {
            objAmd.Role__c = objAmd.Role__c + ';Operating Location';
        }
        upsert objAmd;
    }
    return respWrap;

}


@AuraEnabled
public static ResponseWrapper removeOprLocAmed(String reqWrapPram) 
{
    //reqest wrpper
    RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);

    //response wrpper
    ResponseWrapper respWrap = new ResponseWrapper();

    try{
        AmendmentWrapper amedWrap = reqWrap.amedWrap;
        amedWrap.amedObj.Status__c = 'Inactive';
        update amedWrap.amedObj;
    } 
    catch(Exception e) 
    {
        
        system.debug('#########exception Msg ' + e.getMessage());
        respWrap.errorMessage = '' + e.getMessage();
    }

    return respWrap;


}

public static List<HexaBPM_Amendment__c> getExistingAmendment(String srId,String amedType)
{
    return [SELECT id, 
                    IsRegistered__c , 
                    Unit__c , 
                    Hosting_Company__c , 
                    Operating_Type__c , 
                    Operating_Location__c , 
                    Operating_Location_Status__c, 
                    Unit__r.Name, 
                    Unit__r.Floor__c, 
                    Unit__r.Building__r.Name
                FROM HexaBPM_Amendment__c
            WHERE ServiceRequest__c = :srId
                AND Amendment_Type__c = :amedType 
                AND Status__c != 'Inactive'];
                // 'Operating Location'
    
}

public class SRWrapper 
{
    @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
    @AuraEnabled public List<AmendmentWrapper> amedWrapLst { get; set; }

    public SRWrapper() {
    }
}

public class AmendmentWrapper 
{
    @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
    @AuraEnabled public Id amedID { get; set; }

    //lookupLabel

    public AmendmentWrapper() {
    }
}

public class OperationLocationWrapper 
{
    @AuraEnabled public Operating_Location__c oprLocObj { get; set; }

    //lookupLabel

    public OperationLocationWrapper() {
    }
}

public class VenueWrapper 
{
    @AuraEnabled public OB_Venue__c venueObj { get; set; }

    //lookupLabel
    public VenueWrapper() 
    {
    }
}

// dynamic button section
@AuraEnabled
public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId) {


    ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
    HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
    PageFlowControllerHelper.objSR = objRequest;
    PageFlowControllerHelper objPB = new PageFlowControllerHelper();

    
    objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
    upsert objRequest;

    OB_RiskMatrixHelper.CalculateRisk(SRID);

    PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
    system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
    respWrap.pageActionName = responseNextPage.pg;
    respWrap.communityName = responseNextPage.communityName;
    respWrap.CommunityPageName = responseNextPage.CommunityPageName;
    respWrap.sitePageName = responseNextPage.sitePageName;
    respWrap.strBaseUrl = responseNextPage.strBaseUrl;
    respWrap.srId = objRequest.Id;
    respWrap.flowId = responseNextPage.flowId;
    respWrap.pageId = responseNextPage.pageId;
    respWrap.isPublicSite = responseNextPage.isPublicSite;

    system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
    return respWrap;
}
public class ButtonResponseWrapper {
    @AuraEnabled public String pageActionName { get; set; }
    @AuraEnabled public string communityName { get; set; }
    @AuraEnabled public String errorMessage { get; set; }
    @AuraEnabled public string CommunityPageName { get; set; }
    @AuraEnabled public string sitePageName { get; set; }
    @AuraEnabled public string strBaseUrl { get; set; }
    @AuraEnabled public string srId { get; set; }
    @AuraEnabled public string flowId { get; set; }
    @AuraEnabled public string pageId { get; set; }
    @AuraEnabled public boolean isPublicSite { get; set; }
    @AuraEnabled public boolean tester { get; set; }

}



public class RequestWrapper 
{
    @AuraEnabled public Id flowId { get; set; }
    @AuraEnabled public Id pageId { get; set; }
    @AuraEnabled public Id srId { get; set; }
    @AuraEnabled public SRWrapper srWrap { get; set; }
    @AuraEnabled public OperationLocationWrapper oprLocWrap { get; set; }
    @AuraEnabled public AmendmentWrapper amedWrap { get; set; }


    public RequestWrapper() {
    }
}

public class ResponseWrapper 
{
    @AuraEnabled public List<OperationLocationWrapper> lstOprLocWrap { get; set; }
    @AuraEnabled public List<VenueWrapper> lstVenueWrap { get; set; }
    //@AuraEnabled public SRWrapper srWrap { get; set; }
    @AuraEnabled public String errorMessage { get; set; }
    @AuraEnabled public HexaBPM__Section_Detail__c ButtonSection { get; set; }
    @AuraEnabled public SRWrapper srWrap { get; set; }
    

    public ResponseWrapper() {
    }
}


}