@ isTest
public class OB_ReviewAndConfirmUtilsTest {

    public static testmethod void doTestConstructor() {
        
         
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts = OB_TestDataFactory.createAccounts(2);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string>{ 'Non - financial' }, new List<string>{ 'License Activity' });

        insert listLicenseActivityMaster;

        //create lead activity
        // List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        //listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        //insert listLeadActivity;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string>{ 'AOR_Financial', 'In_Principle' });
        insert createSRTemplateList;

        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>();
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;

        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>();
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        insert listPage;

        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c = false;
        section.HexaBPM__Section_Type__c = 'PageBlockSection';
        section.HexaBPM__Section_Description__c = 'PageBlockSection';
        section.HexaBPM__layout__c = '2';
        section.HexaBPM__Page__c = listPage [0].id;
        insert section;
        section.HexaBPM__Default_Rendering__c = true;
        update section;


        HexaBPM__Section_Detail__c sec = new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c = section.id;
        sec.HexaBPM__Component_Type__c = 'Input Field';
        sec.HexaBPM__Field_API_Name__c = 'first_name__c';
        sec.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c = '0';
        sec.HexaBPM__Mark_it_as_Required__c = true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c = false;
        insert sec;

        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c = false;
        section1.HexaBPM__Section_Type__c = 'CommandButtonSection';
        section1.HexaBPM__Section_Description__c = 'PageBlockSection';
        section1.HexaBPM__layout__c = '2';
        section1.HexaBPM__Page__c = listPage [0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c = true;
        update section1;

        HexaBPM__Section_Detail__c sec1 = new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c = section1.id;
        sec1.HexaBPM__Component_Type__c = 'Input Field';
        sec1.HexaBPM__Field_API_Name__c = 'first_name__c';
        sec1.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c = '0';
        sec1.HexaBPM__Mark_it_as_Required__c = true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c = false;
        insert sec1;

        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>();
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string>{ 'Closed', 'Draft', 'Submitted' }, new List<string>{ 'CLOSED', 'DRAFT', 'SUBMITTED' }, new List<string>{ 'End', '', '' });
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string>{ 'AOR_Financial', 'In_Principle' }, insertNewAccounts, 
                                                   new List<string>{ 'Non - financial', 'Retail' }, 
                                                   new List<string>{ 'Foundation', 'Services' }, 
                                                   new List<string>{ 'Foundation', 'Company' }, 
                                                   new List<string>{ 'Foundation', 'Recognized Company' });
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads [0].id).substring(0, 15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads [1].id).substring(0, 15);
        insertNewSRs[1].Setting_Up__c = 'Branch';
        insertNewSRs[0].Setting_Up__c = 'Branch';
        insertNewSRs[1].Page_Tracker__c = listPage [0].id;
        insertNewSRs[0].Page_Tracker__c = listPage [0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus [1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus [1].id;
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].OB_Is_Multi_Structure_Application__c = 'yes';
        insertNewSRs[1].OB_Is_Multi_Structure_Application__c = 'yes';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;

        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1, new List<String>{ 'Low' }, new List<Integer>{ 1 }, new List<String>{ 'Low' });
        insert listCS;
        //create amendments
        list<HexaBPM_Amendment__c> listAmendments = new list<HexaBPM_Amendment__c>();
        listAmendments = OB_TestDataFactory.createApplicationAmendment(1, listCS, insertNewSRs);
        insert listAmendments;

        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string>{ 'RM_Assignment' }, new List<String>{ 'RM_Assignment' }
                                                                 , new List<String>{ 'General' }, new List<String>{ 'RM_Assignment' });
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;

        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
        insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insert insertNewSteps;
        //---------------------------------------------------------------------------------------------------
        PageReference pageRef = Page.OB_ReviewAndConfirmVF;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(insertNewSRs [0].Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(insertNewSRs [0]);
        OB_ReviewAndConfirmController testAccPlan = new OB_ReviewAndConfirmController(sc);

        OB_ReviewAndConfirmController.RequestWrap requestWrapperObj = new OB_ReviewAndConfirmController.RequestWrap();
        requestWrapperObj.flowID = listPageFlow [0].Id;
        requestWrapperObj.pageId = listPage [0].Id;
        requestWrapperObj.srId = insertNewSRs [0].Id;
        String reqParam = JSON.serialize(requestWrapperObj);

        OB_ReviewAndConfirmController.RespondWrap respObj = new OB_ReviewAndConfirmController.RespondWrap();
        respObj = OB_ReviewAndConfirmController.fetchSRObjFields(reqParam);
        OB_ReviewAndConfirmController.updateSRObj(reqParam);
        OB_ReviewAndConfirmController.ValidateWrapper wrapperObj = new OB_ReviewAndConfirmController.ValidateWrapper();
        // wrapperObj = OB_ReviewAndConfirmController.validateSRLines(insertNewSRs[0].Id);
        // wrapperObj = OB_ReviewAndConfirmController.validateSRLines(insertNewSRs[1].Id);

        List<HexaBPM__Service_Request__c> details = [Select Id, HexaBPM__Internal_SR_Status__r.Name from HexaBPM__Service_Request__c where HexaBPM__Internal_SR_Status__r.Name = 'Draft'];
        system.debug(details);
        system.debug(details [0].HexaBPM__Internal_SR_Status__r.Name);

        OB_ReviewAndConfirmController.CompanyWrapper wrapObj = new OB_ReviewAndConfirmController.CompanyWrapper();
        wrapObj = OB_ReviewAndConfirmController.retrieveCompanyWrapper(listPage [0].Id);

        OB_ReviewAndConfirmController.ButtonResponseWrapper btnWrap = new OB_ReviewAndConfirmController.ButtonResponseWrapper();
        btnWrap = OB_ReviewAndConfirmController.getButtonAction(insertNewSRs [0], insertNewSRs [0].Id, listPage [0].Id, listPageFlow [0].Id);

        String sridsub = OB_ReviewAndConfirmController.SubmitSR(insertNewSRs [1].Id);
        OB_ReviewAndConfirmController.autoCloseCustomerStep(insertNewSRs [1].Id);

        //listLicenseActivityMaster[0].OB_Trade_Precious_Metal__c = 'Yes';
        //update  listLicenseActivityMaster[0];


        list<OB_ReviewAndConfirmController.fieldNameAndLableWrap> testFieldWrapper = new list<OB_ReviewAndConfirmController.fieldNameAndLableWrap>();

        insertNewSRs[1].Location_of_records_and_registers__c = 'Other Location';
        update insertNewSRs [1];
        testFieldWrapper = OB_ReviewAndConfirmUtils.getOperatingLocField(insertNewSRs [1]);
        insertNewSRs[1].Type_of_Entity__c = 'Public';
        insertNewSRs[1].Setting_Up__c = 'New';
        update insertNewSRs [1];
        testFieldWrapper = OB_ReviewAndConfirmUtils.getOperatingLocField(insertNewSRs [1]);
        insertNewSRs[1].Setting_Up__c = '';
        update insertNewSRs [1];
        testFieldWrapper = OB_ReviewAndConfirmUtils.getOperatingLocField(insertNewSRs [1]);
        insertNewSRs[1].Setting_Up__c = 'Branch';
        insertNewSRs[1].Type_of_Entity__c = '';
        update insertNewSRs [1];


        //create license
        License__c licenseObj = new License__c();
        licenseObj.Account__c = insertNewAccounts [0].Id;
        insert licenseObj;

        License_Activity__c testAct = new License_Activity__c();
        testAct.Activity__c = listLicenseActivityMaster [0].id;
        testAct.Account__c = insertNewAccounts [1].id;
        testAct.License__c = licenseObj.Id;
        insert testAct;

        list<License_Activity__c> testActList = new list<License_Activity__c>();
        testActList.add(testAct);



        testFieldWrapper = OB_ReviewAndConfirmUtils.getLicActScreenFields(insertNewSRs [1], testActList);
        insertNewSRs[1].Business_Sector__c = 'Single Family Office';
        update insertNewSRs [1];


        //testAct.Activity__r.OB_DNFBP__c = 'yes';
        //update testAct;
        listLicenseActivityMaster[0].OB_Sell_or_Buy_Real_Estate__c = 'yes';
        update listLicenseActivityMaster [0];
        testFieldWrapper = OB_ReviewAndConfirmUtils.getLicActScreenFields(insertNewSRs [1], testActList);
        insertNewSRs[1].Buying_Selling_Real_Estate_Property__c = 'yes';
        insertNewSRs[1].Parent_or_Entity_Business_Activities__c = 'Precious metals';
        insertNewSRs[1].Nature_of_Business__c = 'test';
        insertNewSRs [1].Setting_Up__c ='New';
        insertNewSRs [1].Business_Sector__c = 'Non Profit Incorporated Organisation (NPIO)';
        insertNewSRs [1].Type_of_Entity__c  ='Public ';
        update insertNewSRs [1];
        testFieldWrapper = OB_ReviewAndConfirmUtils.getLicActScreenFields(insertNewSRs [1], testActList);



        testFieldWrapper  = OB_ReviewAndConfirmUtils.getNamingScreenFields(insertNewSRs [1]);
        insertNewSRs[1].Fall_within_existing_entities__c = 'Yes';
        update  insertNewSRs [1];
        testFieldWrapper  = OB_ReviewAndConfirmUtils.getNamingScreenFields(insertNewSRs [1]);

        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Role__c = 'Shareholder';
        objAmendment.Type_of_Ownership_or_Control__c = 'The individual members of the Council who are deemed to be a UBO';
        objAmendment.Passport_No__c = '12345';
        objAmendment.ServiceRequest__c = insertNewSRs [1].Id;
        listAmendment.add(objAmendment);

        HexaBPM_Amendment__c objAmendment4 = new HexaBPM_Amendment__c();
        objAmendment4.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment4.Nationality_list__c = 'India';
        objAmendment4.Role__c = 'Director';
        objAmendment4.Type_of_Ownership_or_Control__c = 'The individual members of the Council who are deemed to be a UBO';
        objAmendment4.Passport_No__c = '12345';
        objAmendment4.ServiceRequest__c = insertNewSRs [1].Id;
        listAmendment.add(objAmendment4);

        HexaBPM_Amendment__c objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment3.Nationality_list__c = 'India';
        objAmendment3.Role__c = 'Guardian';
        objAmendment3.Passport_No__c = '12345';
        objAmendment3.ServiceRequest__c = insertNewSRs [1].Id;
        listAmendment.add(objAmendment3);

        HexaBPM_Amendment__c objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment2.Nationality_list__c = 'India';
        objAmendment2.Role__c = 'Authorised Signatory';
        objAmendment2.Passport_No__c = '12345';
        objAmendment2.Type_of_Authority__c = 'Jointly';
        objAmendment2.ServiceRequest__c = insertNewSRs [1].Id;
        listAmendment.add(objAmendment2);


        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        objAmendment1.Nationality_list__c = 'India';
        objAmendment1.Registration_No__c = '1234';
        objAmendment1.Role__c = 'Qualified Applicant';
        objAmendment1.ServiceRequest__c = insertNewSRs [1].Id;

        listAmendment.add(objAmendment1);
        insert listAmendment;
        
        Test.startTest();

        testFieldWrapper  = OB_ReviewAndConfirmUtils.getQualifiedPurposeFields(insertNewSRs [1]);
        insertNewSRs[1].Qualified_Applicant_Purpose__c = 'Qualified Applicant';
        update insertNewSRs [1];
        testFieldWrapper  = OB_ReviewAndConfirmUtils.getQualifiedPurposeFields(insertNewSRs [1]);
        insertNewSRs[1].Qualified_Applicant_Purpose__c = 'Qualified Purpose';
        update insertNewSRs [1];
        testFieldWrapper  = OB_ReviewAndConfirmUtils.getQualifiedPurposeFields(insertNewSRs [1]);

        insertNewSRs[1].Select_Qualified_Purpose__c = 'A family structure';
        update insertNewSRs [1];
        testFieldWrapper  = OB_ReviewAndConfirmUtils.getQualifiedPurposeFields(insertNewSRs [1]);
        insertNewSRs[1].Select_Qualified_Purpose__c = 'A structured financing';
        update insertNewSRs [1];
        testFieldWrapper  = OB_ReviewAndConfirmUtils.getQualifiedPurposeFields(insertNewSRs [1]);
        testFieldWrapper  = OB_ReviewAndConfirmUtils.getQualifiedPurposeScreenFields(insertNewSRs [1]);

        OB_ReviewAndConfirmController.ValidateWrapper testValidation = new OB_ReviewAndConfirmController.ValidateWrapper();

        Company_Name__c compName = new Company_Name__c();
        compName.Trading_Name__c = 'tester';
        compName.Status__c = 'Draft';
        compName.Application__c = insertNewSRs [1].id;
        insert compName;

        insertNewSRs[1].Qualified_Applicant_Purpose__c = 'Qualified Applicant';
        insertNewSRs[1].Type_of_Entity__c = 'recognized company';
        insertNewSRs[1].Business_Sector__c = 'Non Profit Incorporated Organisation (NPIO)';
        insertNewSRs [1].Type_of_Entity__c  ='Public ';
        update insertNewSRs [1];
        testValidation = OB_ReviewAndConfirmUtils.validateSRLines(insertNewSRs [1].id);
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('In_Principle').getRecordTypeId();
        update insertNewSRs [1];
        //listAmendments[0].role__c = 'Director';
        //update listAmendments[0];
        testValidation = OB_ReviewAndConfirmUtils.validateSRLines(insertNewSRs [1].id);

        for (HexaBPM_Amendment__c amendObj :listAmendment) {
            amendObj.ServiceRequest__c = insertNewSRs [0].id;
        }
        update listAmendment;
        insertNewSRs[0].Qualified_Applicant_Purpose__c = 'Qualified Applicant';
        insertNewSRs[0].Type_of_Entity__c = 'public';
        update insertNewSRs [0];
        testValidation = OB_ReviewAndConfirmUtils.validateSRLines(insertNewSRs [0].id);
        insertNewSRs[0].OB_Is_Multi_Structure_Application__c = 'yes';
        insertNewSRs[0].Type_of_Entity__c = 'public';
        insertNewSRs[0].No_Accordance_with_Article_27__c = 'Test123';
        update insertNewSRs [0];
        testValidation = OB_ReviewAndConfirmUtils.validateSRLines(insertNewSRs [0].id);
        testFieldWrapper  = OB_ReviewAndConfirmUtils.getDataProtectionField(insertNewSRs [0]);
        testFieldWrapper  = OB_ReviewAndConfirmUtils.getDataProtectionScreenField(insertNewSRs [0]);
        
        
        Test.stopTest();        
        
    }
    
    public static testMethod void CC_SendCP_Email() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        insert con; 
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'Commercial_Permission';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New Commercial Permission').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
        HexaBPM__Document_Master__c docmaster = new HexaBPM__Document_Master__c ();
        docmaster.HexaBPM__Code__c ='Commercial_Permission';
        insert docmaster;
        
         HexaBPM__Document_Master__c docmaster2 = new HexaBPM__Document_Master__c ();
        docmaster2.HexaBPM__Code__c ='Commercial_Permission_Dual_License';
        insert docmaster2;
        
        HexaBPM__SR_Template_Docs__c objSRTempdoc = new HexaBPM__SR_Template_Docs__c();
        objSRTempdoc.Visible_to_GS__c = true;
        objSRTempdoc.HexaBPM__Document_Master__c = docmaster.id;
        insert objSRTempdoc;
        
       
        
        
        Test.startTest();
        
         Id SRRecI2d = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New Commercial Permission').getRecordTypeId();

        
         HexaBPM__Service_Request__c objHexaSR2 = new HexaBPM__Service_Request__c();
        objHexaSR2.Entity_Name__c = 'test';
        objHexaSR2.RecordtypeId = SRRecI2d;
        objHexaSR2.Type_of_commercial_permission__c = 'ATM';
        objHexaSR2.HexaBPM__Customer__c = acc.id;
         objHexaSR2.Select_Qualified_Purpose__c = 'An Aviation Structure';
        //objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR2.HexaBPM__Email__c = 'test@test.com';
        objHexaSR2.HexaBPM__customer__c=acc.id;
        
         
        
        
        insert objHexaSR2;
        
         List<License_Activity_Master__c> listLicenseActivityMaster2 = new List<License_Activity_Master__c>();
        listLicenseActivityMaster2 = OB_TestDataFactory.createLicenseActivityMaster(2, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        listLicenseActivityMaster2[0].Name = 'tester';
        listLicenseActivityMaster2[0].Enabled__c = true;
        listLicenseActivityMaster2[0].OB_Type_of_commercial_permission__c = 'ATM';
        
        listLicenseActivityMaster2[1].Name = 'Qualifying Purpose of Aviation';
        listLicenseActivityMaster2[1].Enabled__c = true;
        insert listLicenseActivityMaster2;
        
        //create license
        License__c licenseObj = new License__c();
        licenseObj.Account__c = acc.id;
        insert licenseObj;
        
        License_Activity__c testAct = new License_Activity__c();
        testAct.Activity__c = listLicenseActivityMaster2 [0].id;
        testAct.Account__c = acc.id;
        testAct.License__c = licenseObj.Id;
        testAct.Application__c = objHexaSR2.id;
        //insert testAct;
        
        
       //  test.startTest();
         Id p = [select id from profile where name='DIFC Contractor Community Plus User Custom'].id;      
        
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user){
             OB_ReviewAndConfirmUtils.setATMActivity(objHexaSR2);
            //insert testAct;
            OB_ReviewAndConfirmUtils.setPrescribedActivity(objHexaSR2);
        OB_ReviewAndConfirmUtils.setATMActivity(objHexaSR2);
        }
       // test.stopTest();
        
        
        OB_ReviewAndConfirmUtils.validateSRLines(objHexaSR2.id);
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Role__c = 'Authorised Signatory';
        objAmendment.Passport_No__c = '1234';
        objAmendment.Type_of_Authority__c='Jointly';
        objAmendment.ServiceRequest__c = objHexaSR2.Id;
        insert objAmendment;
        
         OB_ReviewAndConfirmUtils.validateSRLines(objHexaSR2.id);
        
         objHexaSR2.Type_of_commercial_permission__c = 'Vending Machines';
        update objHexaSR2;
        OB_ReviewAndConfirmUtils.validateSRLines(objHexaSR2.id);
        
         objHexaSR2.Type_of_commercial_permission__c = 'Dual license of DED licensed firms with an affiliate in DIFC';
        update objHexaSR2;
        OB_ReviewAndConfirmUtils.validateSRLines(objHexaSR2.id);
        
         OB_ReviewAndConfirmUtils.validateSRLines(objAmendment.id);
                Test.stopTest();
       
    }
}