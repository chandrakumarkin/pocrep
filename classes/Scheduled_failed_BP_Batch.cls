global with sharing class Scheduled_failed_BP_Batch implements Schedulable,Database.AllowsCallouts
{
    /*
    #8208 - Create BP records if existing web service fail to puch data 
    * Delveloper Arun singh 
        */
    global void execute(SchedulableContext ctx) 
    {
        StartFindAndSetBPNumbers();
    }


    public static void StartFindAndSetBPNumbers()
    {
    
        FindAndSetBPNumbers([select id,Relationship_Group__c ,Object_Account__c,Relationship_ID_Contact_OR_Account__c,Object_Contact__c ,
        Subject_Account__c,Partner_1__c from Relationship__c where Partner_2__c=null and Subject_Account__c!=null and Relationship_ID_Contact_OR_Account__c!=null and Relationship_Group__c='ROC' and Relationship_Type__c!=null and  Company_ROC_Status__c='Active' order by Createddate DESC limit 20]);
        
    }

    public static void FindAndSetBPNumbers(List <Relationship__c> scope) 
    {
        list<string> lstRelationShipIds ;
        map<string,string> mapContactIds;
        
        for (Relationship__c Rel: scope) 
        { 
        lstRelationShipIds = new list<string>();
        mapContactIds = new map<string,string>();
        if(Rel.Relationship_Group__c=='ROC')
        {
            lstRelationShipIds.add(Rel.id);
            
            if(Rel.Object_Account__c!=null)
            {
            
            mapContactIds.put(Rel.Object_Account__c,Rel.Partner_1__c);
            SAPWebServiceDetails.CRMPartnerCall(lstRelationShipIds,null,mapContactIds);

            }
            else if(Rel.Object_Contact__c!=null)
            {
                mapContactIds.put(Rel.Object_Contact__c,Rel.Partner_1__c);
                SAPWebServiceDetails.CRMPartnerCall(lstRelationShipIds,mapContactIds,null);
    
            }
        
        mapContactIds.clear();
        lstRelationShipIds.clear();
            
        }
        /*
            else if(Rel.Relationship_Group__c=='RORP' && Rel.Object_Contact__c!=null)
            {
                
                lstRelationShipIds.add(Rel.Object_Contact__c);
                RorpSAPWebServiceDetails.RorpPartnerCreationFuture(lstRelationShipIds,null,null);

            }*/
                    
                        
           
        }
    
    }
    
}