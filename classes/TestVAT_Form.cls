@isTest(SeeAllData=false)
public class TestVAT_Form{

      static testMethod void myUnitTest() {
             
            Id p = [select id from profile where name='DIFC Customer Community User Custom'].id;
            Account ac = new Account(name ='Grazitti') ;
            insert ac;
            
            Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
            
            con.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
            insert con;  
            
              User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
             
             
             user.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services';
       
            insert user;
            
           system.runAs(user) {
            // statements to be executed by this test user.
            
             VatController vatCtrl = new VatController();
            
             vatCtrl.UpdateTax();
         
        }
            
             
          
      }
}