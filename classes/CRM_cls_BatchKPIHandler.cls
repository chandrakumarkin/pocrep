/******************************************************************************************
 *  Name        : CRM_cls_BatchKPIHandler 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2016-11-2
 *  Description : Batchable Class for KPI records
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   02-11-2016   Claude        Created
 V1.1   13-12-2016   Claude        Modified logic for relationships for affiliates (#3746)
*******************************************************************************************/
public without sharing class CRM_cls_BatchKPIHandler implements Schedulable, Database.Batchable<sObject>, Database.Stateful {
        
    public List<KPI__c> kpiRecords;
    public Set<String> kpiIdsBatched;
    
    public CRM_cls_BatchKPIHandler(){
        this.kpiRecords = CRM_cls_KPI_Utils.getKPIRecords();
    }
        
    public CRM_cls_BatchKPIHandler(List<KPI__c> kpiRecords){
        
        initializeKpiIdList();
        this.kpiRecords = kpiRecords;
    }
    
    private void initializeKpiIdList(){
    	this.kpiIdsBatched = new Set<String>();
    }
        
    public list<KPI__c> start(Database.BatchableContext BC ){
        return kpiRecords;
    }
        
    public void execute(Database.BatchableContext BC, list<KPI__c> kpiRecords){
        
        /* NOTE: All future bulk operations must be wrapped inside a method */
        this.kpiIdsBatched = CRM_cls_KPI_Utils.updateKpiRecords(kpiRecords);
    }
    
    public void execute(SchedulableContext ctx) {
        database.executeBatch(this);
    }
    
    public void finish(Database.BatchableContext BC){
        
        /* NOTE: All future bulk operations must be wrapped inside a method */
        CRM_cls_KPI_Utils.updateAffiliateKPI(this.kpiIdsBatched);
    }
    
}