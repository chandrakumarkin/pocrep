/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestPortalBalanceNew {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'AccountBal';
        objEP.URL__c = 'http://accbal.com/sampledata';
        insert objEP;
        
        Block_Trigger__c objCS = new Block_Trigger__c();
        objCS.Block_Web_Services__c = false;
        objCS.Name = 'Block';
        insert objCS;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Test.setMock(WebServiceMock.class, new TestAccountBalanceServiceCls());
        test.startTest();
        list<AccountBalenseService.ZSF_S_ACC_BAL> resActBals = new list<AccountBalenseService.ZSF_S_ACC_BAL>();
        AccountBalenseService.ZSF_S_ACC_BAL objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        
        TestAccountBalanceServiceCls.resActBals = resActBals;
        
        PortalBalanceNew.getAccountBalanceFuture(new list<string>{objAccount.Id}, '5100');
        PortalBalanceNew.getPortalBalance('001234');
        test.stopTest();
    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'AccountBal';
        objEP.URL__c = 'http://accbal.com/sampledata';
        insert objEP;
        
        Block_Trigger__c objCS = new Block_Trigger__c();
        objCS.Block_Web_Services__c = false;
        objCS.Name = 'Block';
        insert objCS;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Test.setMock(WebServiceMock.class, new TestAccountBalanceServiceCls());
        test.startTest();
        list<AccountBalenseService.ZSF_S_ACC_BAL> resActBals = new list<AccountBalenseService.ZSF_S_ACC_BAL>();
        AccountBalenseService.ZSF_S_ACC_BAL objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        
        TestAccountBalanceServiceCls.resActBals = resActBals;
		
        PortalBalanceNew.getPortalBalance('001234');
        test.stopTest();
    }
    static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'AccountBal';
        objEP.URL__c = 'http://accbal.com/sampledata';
        insert objEP;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Test.setMock(WebServiceMock.class, new TestAccountBalanceServiceCls());
        test.startTest();
        list<AccountBalenseService.ZSF_S_ACC_BAL> resActBals = new list<AccountBalenseService.ZSF_S_ACC_BAL>();
        AccountBalenseService.ZSF_S_ACC_BAL objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        
        TestAccountBalanceServiceCls.resActBals = resActBals;
        
        PortalBalanceNew.getAccountBalanceFuture(new list<string>{objAccount.Id}, '5100');
        PortalBalanceNew.getPortalBalance('001234');
        PortalBalanceNew.testHack();
        test.stopTest();
    }
}