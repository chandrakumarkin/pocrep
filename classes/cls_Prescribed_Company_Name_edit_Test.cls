/******************************************************************************************
 *  Author   : Mudasir Wani
 *  Date     : 30th October 2019
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        		Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    30th October 2019  	Mudasir          Created

*/
@isTest
private class cls_Prescribed_Company_Name_edit_Test {

    static testMethod void myUnitTest() {
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Arabic_Name__c = 'Test';
        objAccount.Previous_Trading_Name_1__c = 'Test1';
        objAccount.Pre_Trade_Name_1_in_Arab__c = 'Test2';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.Customer__c = objAccount.Id;
        SR1.Previous_Entity_Name__c = 'pre1';   
        SR1.Pre_Entity_Name_Arabic__c = 'pre2';   
        SR1.Previous_Trading_Name_1__c = 'pre3'; 
        SR1.Pre_Trade_Name_1_in_Arab__c ='pre4';      
        //insert SR1;
        ApexPages.StandardController sc = new ApexPages.StandardController(SR1);
       	//Add contact 
       	Contact con = new contact(lastname='lastnameDifc',Accountid=objAccount.id);
       	insert con;
       	// Setup test data
        // Create a unique UserName
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User u = new User(Alias = 'standt', Email='standarduser@difc.ae',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
        contactid=con.id,
        UserName=uniqueUserName);

        System.runAs(u) {
        		PageReference pageRef = Page.Prescribed_Company_Name;
              	//pageRef.getParameters().put('id', String.valueOf(objStep.Id));
        		Test.setCurrentPage(pageRef);
              	cls_Prescribed_Company_Name_edit tst = new cls_Prescribed_Company_Name_edit(sc);
              	tst.ObjAccount = objAccount;
              	tst.SaveRequest();
         }
    }
}