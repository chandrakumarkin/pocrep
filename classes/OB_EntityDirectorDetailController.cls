/**
*Author : Merul Shah
*Description : This class takes the form/Details of Ind/BC directors and save them to SF as amendments..

* v1.0 Merul  26 march 2020  Libary implementation
**/ 
public without sharing class OB_EntityDirectorDetailController
{
    
    
    @AuraEnabled   
    public static ResponseWrapper  onAmedSaveDB(String reqWrapPram)
    {
        //reqest wrpper
        OB_EntityDirectorDetailController.RequestWrapper reqWrap = (OB_EntityDirectorDetailController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_EntityDirectorDetailController.RequestWrapper.class);
        
        //response wrpper
        OB_EntityDirectorDetailController.ResponseWrapper respWrap = new OB_EntityDirectorDetailController.ResponseWrapper();
        HexaBPM_Amendment__c amedObj = reqWrap.amedWrap.amedObj;
        //amedObj.Is_Director__c  = true;
        Boolean isDuplicate = false;
        //String srId = amedObj.ServiceRequest__c;
        // v1.0 Merul  26 march 2020  Libary implementation
        Boolean enforceDirectorRole =  false;

        String srId = reqWrap.srId;
        system.debug('@@@@@@@@@@ amedObj.ServiceRequest__c '+amedObj.ServiceRequest__c);
        // v1.0 Merul  26 march 2020  Libary implementation
        amedObj.Id = ( amedObj.ServiceRequest__c != srId ? NULL : amedObj.Id);
        enforceDirectorRole = amedObj.ServiceRequest__c != srId;
        system.debug('$$$$$$$$$$$ enforceDirectorRole '+enforceDirectorRole);
        amedObj.ServiceRequest__c = srId;


        String amendID = amedObj.Id;
        string legalStruct= '';
        for(HexaBPM__Service_Request__c serObj :[SELECT id, 
                                                        Entity_Type__c, 
                                                        Type_of_Entity__c, 
                                                        legal_structures__c 
                                                   FROM HexaBPM__Service_Request__c
                                                  WHERE Id =: srId])
         {
            
            legalStruct =  serObj.legal_structures__c;                              
         }
                                                                  
                Map<String,String> docMasterContentDocMap = reqWrap.docMasterContentDocMap;
                system.debug(reqWrap.amedWrap.amedObj.Nationality_list__c+'@@@@@  docMasterContentDocMap '+reqWrap.amedWrap.amedObj.Passport_No__c);
                system.debug('@@@@@  recordTypeId '+amedObj.recordTypeId);
                String RecordTypename;
                if( amedObj.recordTypeId != NULL)
                {
                     RecordTypename = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosById().get(amedObj.recordTypeId).getDeveloperName();
                    system.debug('@@@@@  RecordTypename  '+RecordTypename);
                }
                
        
                //String RecordTypename = amedObj.RecordType.DeveloperName;
                system.debug(legalStruct+'@@@@@  docMasterContentDocMap '+RecordTypename);
                if(String.IsNotBlank(RecordTypename) 
                             && RecordTypename.equalsIgnoreCase('Individual') 
                                     && reqWrap.amedWrap.amedObj.Passport_No__c != NULL 
                                         && reqWrap.amedWrap.amedObj.Nationality_list__c != NULL
                   )
                 {
                        String passport = String.isNotBlank(reqWrap.amedWrap.amedObj.Passport_No__c)?reqWrap.amedWrap.amedObj.Passport_No__c:'';
                        String nationality = String.IsNotBlank(reqWrap.amedWrap.amedObj.Nationality_list__c)?reqWrap.amedWrap.amedObj.Nationality_list__c.toLowerCase():'';
                        String passportNationlity = (String.isNotBlank(passport)?passport+'_':'')+(String.IsNotBlank(nationality)?nationality:'');
                        system.debug('===passportNationlity=='+passportNationlity);
                       
                        system.debug('===amendID=='+amendID);
                        
                        for(HexaBPM_Amendment__c amd : [Select ID,First_Name__c,Passport_No__c,Nationality_list__c,
                                                               recordtype.developername,Role__c 
                                                          FROM HexaBPM_Amendment__c
                                                          WHERE ServiceRequest__c =: srId
                                                            AND (recordtype.developername = 'Individual' 
                                                                AND Id !=: amendID)
                                                            AND Passport_No__c =: passport
                                                            AND Nationality_list__c =: nationality
                                                            Limit 1 ])
                        {
                            system.debug('===duplicate==');
                            Boolean isGuardian = String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Guardian') ? true :false;
                            amedObj.Id = amd.Id;
                            // for gurdian and Legal str dup check.
                            if(isGuardian && String.IsNotBlank(legalStruct)
                                    && legalStruct.equalsIgnorecase('Foundation')) 
                            {
                                system.debug('@@@@@@ dup for gurgian and legal structure.');
                                isDuplicate = true;
                                break;
                            }
                            // For director dup check.
                            if(String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Director'))
                            {
                                 system.debug('@@@@@@ dup for Director.');
                                 isDuplicate = true;
                                 break;
                            }
                            
                            // v1.0 Merul  26 march 2020  Libary implementation
                            if( enforceDirectorRole )
                            {
                                amedObj.Role__c = 'Director';   
                            }
                            else 
                            {
                                if( amd.Role__c != NULL
                                        && !amd.Role__c.containsIgnorecase('Director') )
                                {
                                    amedObj.Role__c = amd.Role__c+';Director';
                                }
                                else if(amd.Role__c == NULL)
                                {
                                    amedObj.Role__c = 'Director';
                                }     
                            }
                                                    
                            
                            
                        }
                   }
                   else if(String.IsNotBlank(RecordTypename) 
                                   && RecordTypename.equalsIgnoreCase('Body_Corporate')
                                        && reqWrap.amedWrap.amedObj.Registration_No__c != NULL
                          )
                   {
                        String registrationNo = String.isNotBlank(reqWrap.amedWrap.amedObj.Registration_No__c)?reqWrap.amedWrap.amedObj.Registration_No__c:'';
                        system.debug(amendID+'====registrationNo====='+registrationNo);
                        for(HexaBPM_Amendment__c amd : [Select ID,Registration_No__c,
                                                                recordtype.developername,Role__c 
                                                            From HexaBPM_Amendment__c
                                                            Where ServiceRequest__c =: srId
                                                            AND (recordtype.developername = 'Body_Corporate' 
                                                            AND Id !=: amendID)
                                                            AND Registration_No__c =: registrationNo
                                                            Limit 1 ])
                        {
                            system.debug('==registrationNo=duplicate==');
                            //isDuplicate = true;
                            //break;
                            amedObj.Id = amd.Id;
                            
                            
                            // For director dup check.
                            if(String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Director'))
                            {
                                 system.debug('@@@@@@ dup for Director.');
                                 isDuplicate = true;
                                 break;
                            }

                            // v1.0 Merul  26 march 2020  Libary implementation 
                            if( enforceDirectorRole )
                            {
                                amedObj.Role__c = 'Director';   
                            }
                            else 
                            {
                                if( amd.Role__c != NULL
                                        && !amd.Role__c.containsIgnorecase('Director') )
                                {
                                    amedObj.Role__c = amd.Role__c+';Director';
                                }
                                else if(amd.Role__c == NULL)
                                {
                                    amedObj.Role__c = 'Director';
                                }   
                            }


                            
                        }
                    }
               
              system.debug(isDuplicate+'====isDuplicate=amendID===='+amendID);
              // duplicate record throw an error 
              
              if(isDuplicate)
              {
                respWrap.errorMessage = 'This person/corporate already exist';
              }
              else
              {
                    // v1.0 Merul  26 march 2020  Libary implementation 
                    if( enforceDirectorRole )
                    {
                        amedObj.Role__c = 'Director';   
                    }
                    else 
                    {
                        if(amedObj.Role__c == NULL )
                        {
                            amedObj.Role__c = 'Director';
                        }
                        else if(amedObj.Role__c.indexOf('Director') == -1)
                        {
                            amedObj.Role__c = amedObj.Role__c +';Director';
                        }
                    }
                   
                    
                    try
                    {
                        upsert amedObj;
                        OB_RiskMatrixHelper.CalculateRisk(srId);
                        reqWrap.amedWrap.amedObj = amedObj;
                        respWrap.amedWrap = reqWrap.amedWrap;
                         system.debug('@@@@@@@@@ calling reparenting  ');
                        //reparenting logic comes here....
                        OB_AmendmentSRDocHelper.RequestWrapper srDocReq = new OB_AmendmentSRDocHelper.RequestWrapper(); 
                        OB_AmendmentSRDocHelper.ResponseWrapper srDocResp = new OB_AmendmentSRDocHelper.ResponseWrapper(); 
                        srDocReq.amedId = amedObj.Id;
                        srDocReq.srId = srId;
                        srDocReq.docMasterContentDocMap = docMasterContentDocMap;
                        
                        srDocResp = OB_AmendmentSRDocHelper.reparentSRDocsToAmendment(srDocReq);
                        //respWrap.debugger = srDocResp;
                        
                    }
                    catch( DMLException e )
                    {
                        system.debug('@@@@@@@@@@ exception '+e); 
                        respWrap.errorMessage = OB_QueryUtilityClass.getMessageFromDMLException(e);
                    }
              }
              
              
              return respWrap;
    }
    
   
   
    

    public class AmendmentWrapper 
    {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public Id amedID { get; set; }
        @AuraEnabled public Boolean isIndividual { get; set; }
        @AuraEnabled public Boolean isBodyCorporate { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        @AuraEnabled public String displayName { get; set; }
        @AuraEnabled public String relatedAmendID { get; set; }
        
        //lookupLabel
        
        public AmendmentWrapper()
        {}
    }
    
    
    public class RequestWrapper 
    {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        @AuraEnabled public String fileId { get; set; }
        @AuraEnabled public Map<String,String> docMasterContentDocMap { get; set; }
        
        public RequestWrapper()
        {}
    }

    public class ResponseWrapper 
    {
        //@AuraEnabled public SRWrapper  srWrap{get;set;}
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        @AuraEnabled public String errorMessage {get;set;}
        //@AuraEnabled public object debugger {get;set;}
        
        // For OCR
        //@AuraEnabled public  Map<String,Object> mapFieldApiObject {get;set;}
        
        public ResponseWrapper()
        {}
    }

}