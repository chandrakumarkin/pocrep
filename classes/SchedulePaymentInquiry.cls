/******************************************************************************************
 *  Author      : Kaavya Raghuram
 *  Company     : NSI JLT
 *  Date        : 2014-12-09     
 *  Purpose     : Class for scheduling the payment inquiry to payment gateway          
*******************************************************************************************/

global class SchedulePaymentInquiry implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        CLS_BatchPaymentReconcile cls = new  CLS_BatchPaymentReconcile();
        database.executebatch(cls,1);
    }
}