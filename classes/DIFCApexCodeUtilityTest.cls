@isTest
public class DIFCApexCodeUtilityTest {
	 static testmethod void hasExpiredEmployeeTest(){
	 	Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
	    insert objAccount;
	    string conRT='';
	    for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
	        conRT = objRT.Id;
	        
	    Contact objContact = Test_CC_FitOutCustomCode_DataFactory.getTestContact(objAccount.id);
	    objContact.RecordTypeId = conRT;
	    insert objContact;
	    
	    Relationship__c rel = new Relationship__c();
	    rel.Relationship_Type__c ='Has DIFC Sponsored Employee';
	    rel.Subject_Account__c= objAccount.id;
	    rel.Object_Contact__c= objContact.id;
	    rel.End_Date__c =System.today().addDays(-31);
	    rel.Active__c = true;
	    insert rel;
	    
	    Contact objContactTwo = Test_CC_FitOutCustomCode_DataFactory.getTestContact(objAccount.id);
	    objContactTwo.RecordTypeId = conRT;
	    insert objContactTwo;
	    
	    Relationship__c relTwo = new Relationship__c();
	    relTwo.Relationship_Type__c ='Has DIFC Sponsored Employee';
	    relTwo.Subject_Account__c= objAccount.id;
	    relTwo.Object_Contact__c= objContactTwo.id;
	    relTwo.Active__c = true;
	    relTwo.End_Date__c =System.today().addDays(-31); 
	    insert relTwo;
	    
	    SR_Template__c objTemplate1 = new SR_Template__c();
	    objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
	    objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
	    objTemplate1.Active__c = true;
	    objTemplate1.SR_Group__c='GS';
	    insert objTemplate1;
	    
	    Product2 objProduct = new Product2();
	    objProduct.Name = 'DIFC Product';
	    objProduct.IsActive = true;
	    insert objProduct;
	    
	    Pricebook2 objPricebook = new Pricebook2();
	    objPricebook.Name = 'Test Price Book';
	    objPricebook.IsActive = true;
	    objPricebook.Description = 'Test Description';
	    insert objPricebook;
	    
	    if(objPricebook.IsActive == false){
	        objPricebook.IsActive = true;
	        update objPricebook;
	    }
	    
	    
	    SR_Template_Item__c objSRTempItem = new SR_Template_Item__c();
	    objSRTempItem.Product__c = objProduct.Id;
	    objSRTempItem.SR_Template__c = objTemplate1.Id;
	    objSRTempItem.On_Submit__c = true;
	    insert objSRTempItem;
	    
	   List<SR_Status__c>  srsList = new List<SR_Status__c>();
	   
	   SR_Status__c srs1  = new SR_Status__c();       
	   srs1.name ='Pending';
	   srs1.code__c = 'Pending';
	   srsList.add(srs1);
	   
	   SR_Status__c srs2  = new SR_Status__c();       
	   srs2.name ='AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
	   srs2.code__c = 'AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
	   srsList.add(srs2);
	   
	   
	   SR_Status__c srs3  = new SR_Status__c();       
	   srs3.name ='Medical fitness certificate received from DHA';
	   srs3.code__c = 'Medical fitness certificate received from DHA';
	   srsList.add(srs3);
	   insert srsList;
	   
	   map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
	   for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
	       mapSRRecordTypes.put(objType.DeveloperName,objType);
	   }
	    
	   Document_Details__c doc = new Document_Details__c();
	   doc.Contact__c=objContact.id; 
	   doc.Document_Type__c ='Employment Visa';
	   doc.Document_Number__c='1111';
	   insert doc;
	    
	   Lookup__c lk= new Lookup__c();
	   lk.Type__c='Nationality';
	   lk.name='India';
	   insert lk;
	    
	   List<Service_Request__c>  servList = new List<Service_Request__c>();
	    Service_Request__c objSR1 = new Service_Request__c();
		objSR1.Customer__c = objAccount.Id;
		objSR1.RecordTypeId = mapSRRecordTypes.get('DIFC_Sponsorship_Visa_Cancellation').Id;
		objSR1.Entity_Name__c = 'Test Entity Name';
		objSR1.Contact__c=objContact.id;
		objSR1.Pre_Entity_Name_Arabic__c='abc';
		objSR1.Document_Detail__c=doc.id;
		objSR1.ApplicantId_hidden__c=objContact.id;
		//objSR1.SponsorId_hidden__c=objContact.id;
		objSR1.Docdetail_hidden__c=doc.id;
		objSR1.Quantity__c=2;
		objSR1.Nationality_list__c='India';
		objSR1.External_SR_Status__c=srsList[0].id;
		objSR1.Visa_Expiry_Date__c=system.today()+20;
		objSR1.Residence_Visa_No__c='201/2015/1234567';
		objSR1.Statement_of_Undertaking__c = true;
		objSR1.Would_you_like_to_opt_our_free_couri__c ='No';        
		objSR1.Send_SMS_To_Mobile__c ='+971589602235';
		//insert objSR1;
		servList.add(objSR1);
		
		Service_Request__c objSR2 = new Service_Request__c();
		objSR2.Customer__c = objAccount.Id;
		objSR2.RecordTypeId = mapSRRecordTypes.get('DIFC_Sponsorship_Visa_Cancellation').Id;
		objSR2.Entity_Name__c = 'Test Entity Name';
		objSR2.Contact__c=objContactTwo.id;
		objSR2.Pre_Entity_Name_Arabic__c='abc';
		objSR2.Document_Detail__c=doc.id;
		objSR2.ApplicantId_hidden__c=objContact.id;
		//objSR2.SponsorId_hidden__c=objContact.id;
		objSR2.Docdetail_hidden__c=doc.id;
		objSR2.Quantity__c=2;
		objSR2.Nationality_list__c='India';
		objSR2.External_SR_Status__c=srsList[0].id;
		objSR2.Visa_Expiry_Date__c=system.today()+20;
		objSR2.Residence_Visa_No__c='201/2015/1234567';
		objSR2.Statement_of_Undertaking__c = true;
		objSR2.Would_you_like_to_opt_our_free_couri__c ='No';        
		objSR2.Send_SMS_To_Mobile__c ='+971589602235';
		servList.add(objSR2);
		insert servList;
		Step__c stepRec = Test_CC_FitOutCustomCode_DataFactory.getTestStep(servList[0].id, srsList[0].id, objAccount.Name);
	 	List <id> stepIds = new List <id>();
	 	stepIds.add(stepRec.id);
	 	
		Step_Template__c  stpTemp = new Step_Template__c ();
		stpTemp.Name ='Original passport submission';
		stpTemp.CODE__C  ='Original passport submission';
		stpTemp.Step_RecordType_API_Name__c ='General';
		insert stpTemp;
		  
		SR_Steps__c   stpTemplateCode = new SR_Steps__c   ();
		stpTemplateCode.Step_Template__c= stpTemp.ID;
		insert stpTemplateCode;
		  
		Step__c stepNew = new Step__c();
		stepNew.SR__c = servList[0].id;
		stepNew.SR_Step__c = stpTemplateCode.ID;
		stepNew.Step_Template__c  = stpTemp.ID;
		insert stepNew;
      
	 	Test.startTest();
			DIFCApexCodeUtility.hasExpiredEmployee(objAccount.id);
			DIFCApexCodeUtility.checkIfSRStatusToUpdate(stepIds);
			DIFCApexCodeUtility.getActivePortalUsers(objAccount.id, 'Id,name','');
			DIFCApexCodeUtility.CheckIfMasterDataUpdateIsApplicable(servList[0]);
			Step__c fetchStepRecord = [Select id,SR__r.External_SR_Status__c,SR__r.External_SR_Status__r.Do_Not_Change_SR_Status__c from Step__c where id=:stepNew.id];
			DIFCApexCodeUtility.serviceRequestStatusUpdateOnStep(fetchStepRecord);
			DIFCApexCodeUtility.prepareStep(true,null,'Pending','Original passport submission',servList[0].id);
		Test.stopTest();
	 }
	 
	 static testMethod void  AccountCOntactRelationshipTest(){
	 	Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        List<contact> ListobjContact = new List<contact>();
        
        ID BDrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId(); //Added as per V1.2
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.AccountId = objAccount.Id; 
        objContact.Passport_No__c = 'ABC12345';
        objContact.RecordTypeId = BDrectypeid ;
        objContact.important__C= true;
        objContact.Is_Active__c= true;
        ListobjContact.add (objContact );
        insert ListobjContact;
        
        AccountContactRelation acccon = new AccountContactRelation();
        acccon.AccountId = objAccount.id;
        acccon.ContactId = ListobjContact[0].id;
        acccon.IsActive = true;
        acccon.Roles = 'Community User';
        //insert acccon;
        
        Test.startTest();
        	DIFCApexCodeUtility.prepareAccountContactRelation(ListobjContact[0].id,objAccount.Id,'Read/Write',true,'Property Services');
        Test.StopTest();
	 }
}