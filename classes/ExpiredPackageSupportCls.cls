public class ExpiredPackageSupportCls
{

@InvocableMethod(label='Package Expired' description='assign if any active package' category='CompanyPackages')
  public static void AssignActivePackage(List<ID> ids) 
  {
  
     List<account> Listaccount=new List<account>();
   for(account Temp:[select Id,(select id from Company_packages__r where Status__c='Default' or Status__c='Approved' limit 1) from account where id in :ids])
   {
       if(!Temp.Company_packages__r.IsEmpty())
       {
           Temp.Company_Package__c=Temp.Company_packages__r[0].Id;
           Listaccount.add(Temp);
       }
   }
   
   if(!Listaccount.isEmpty())
       update Listaccount;
       
  }
  
  
}