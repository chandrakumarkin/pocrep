@isTest
private class TestTranslatiorClass {

    static testMethod void myUnitTest() {
		Test.setMock(HttpCalloutMock.class, new TranslatiorClassMOCKClass());
		Test.startTest();
			TranslatiorClass.translatorMethod('DIFC'); 
			HTTPResponse res = new HTTPResponse();
			String contentType = res.getHeader('Content-Type');
	        String actualValue = res.getBody();
	        String expectedValue = '{"DIFC":"DIFC"}';
		Test.stopTest();    
    }
}