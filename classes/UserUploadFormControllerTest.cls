/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 06-09-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   06-09-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public class UserUploadFormControllerTest {

 static testMethod void myUnitTest(){

     Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        insert objContact;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'New Lease';
        objTemplate.SR_RecordType_API_Name__c = 'Client_Information';
        objTemplate.Active__c = true;
        objTemplate.Available_for_menu__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Test Doc Master';
        insert objDocMaster;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.SR_Template__c = objTemplate.Id;
        insert objSR;
      	
      	Status__c objStatus = new Status__c();
	    objStatus.Name = 'Closed';
	    objStatus.Type__c = 'Closed';
	    objStatus.Code__c = 'Closed';
	    insert objStatus;
    
      	Step_Template__c  stpTemp = new Step_Template__c ();
		stpTemp.Name ='Entry Permit With Airport Entry Stamp Upload';
		stpTemp.CODE__C  ='Entry Permit With Airport Entry Stamp Upload';
		stpTemp.Step_RecordType_API_Name__c ='General';
		insert stpTemp;
      
        Step__c stp = new Step__c();
        stp.SR__c = objSR.Id;
        stp.Step_Notes__c = 'Sample step Note';
        stp.step_template__c = stpTemp.Id;
        insert stp;
      
        stp.Step_Notes__c = 'Durga Added new comments';
        update stp;
      
        Test.startTest();
      
        SR_Doc__c objSRDoc = new SR_Doc__c();
        objSRDoc.Service_Request__c = objSR.Id;
        objSRDoc.Is_Not_Required__c = false;
        objSRDoc.Group_No__c = 1;
        insert objSRDoc;

        UserUploadFormController.SaveFile(objSR.Id,'Testfile','xyzpdg','pdf');

 }

}