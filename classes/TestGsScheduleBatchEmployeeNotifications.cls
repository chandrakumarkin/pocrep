/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestGsScheduleBatchEmployeeNotifications {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        //objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Index_Card_Status__c = 'Active';
        
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Last RB';
        objContact.Email = 'test@difcportal.com';
        objContact.AccountId = objAccount.Id;
        objContact.FirstName = 'Nagaboina';
        insert objContact;
         
        Contact objGSContact = new Contact();
        objGSContact.LastName = 'Last RB';
        objGSContact.Email = 'test@difcportal.com';
        objGSContact.AccountId = objAccount.Id;
        objGSContact.FirstName = 'Nagaboina';
        objGSContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objGSContact;
        
         Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Contractor Community User Custom' limit 1];  //Customer Community Login User Custom
       
        User u                         = new User(Alias = 'standt', Email='tesclasst@difc.com', 
        EmailEncodingKey               ='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey                   ='en_US', ProfileId = p.Id, 
        TimeZoneSidKey                 ='America/Los_Angeles', UserName='tesclasstuat@difc.com');
        u.Community_User_Role__c       = 'Admin;Company Services Approver;Employee Services';    
        u.IsActive                     = true;
        u.contactID                    = objContact.id; 
        insert u;
        
        
        AccountContactRelation acctcr1 = new AccountContactRelation(Id= [SELECT Id FROM AccountContactRelation WHERE AccountId =: objAccount.Id LIMIT 1].Id,AccountId = objAccount.id, ContactId = objContact.id, Roles = 'Employee Services');
        update acctcr1;
       
        
        Document_Details__c objDD = new Document_Details__c();
        objDD.Contact__c = objGSContact.Id;
        objDD.EXPIRY_DATE__c = system.today();
        objDD.Document_Type__c = 'Employment Visa';
        insert objDD;
        
		Identification__c objId = new Identification__c();
        objId.Id_No__c = '2/6/1234567';
        objId.Valid_From__c = system.today().addYears(-1);
        objId.Valid_To__c = system.today().addYears(1);
        objId.Identification_Type__c = 'Access Card';
        objId.Account__c = objAccount.Id;
        objId.Valid_To__c = system.today();
        insert objId;
		
        test.startTest();
        
	        Datetime dt = system.now().addMinutes(1);
	    	string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();    	
	        system.schedule('TestGsScheduleBatchEmployeeNotifications', sCornExp, new GsScheduleBatchEmployeeNotifications());
        
        test.stopTest();
        
    }
    
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        //objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Index_Card_Status__c = 'Active';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Last RB';
        objContact.Email = 'test@difcportalds.com';
        objContact.AccountId = objAccount.Id;
        objContact.FirstName = 'Nagaboina';
        objContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        insert objContact;
         
        Contact objGSContact = new Contact();
        objGSContact.LastName = 'Last RB';
        objGSContact.Email = 'test@difcportal.com';
        objGSContact.AccountId = objAccount.Id;
        objGSContact.FirstName = 'Nagaboina';
        objGSContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objGSContact;
        
        Relationship__c thisRel = new Relationship__c();
        thisRel.Relationship_Type__c = 'Has DIFC Sponsored Employee';
        thisRel.Subject_Account__c   = objAccount.Id;
        thisRel.Object_Contact__c    = objGSContact.Id;
        insert thisRel;
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Contractor Community User Custom' limit 1];  //Customer Community Login User Custom
       
        User u                         = new User(Alias = 'standt', Email='tesclasst@difc.com', 
        EmailEncodingKey               ='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey                   ='en_US', ProfileId = p.Id, 
        TimeZoneSidKey                 ='America/Los_Angeles', UserName='tesclasstuat@difc.com');
        u.Community_User_Role__c       = 'Admin;Company Services Approver;Employee Services';    
        u.IsActive                     = true;
        u.contactID                    = objContact.id; 
        insert u;
        
        
        AccountContactRelation acctcr1 = new AccountContactRelation(Id= [SELECT Id FROM AccountContactRelation WHERE AccountId =: objAccount.Id LIMIT 1].Id,AccountId = objAccount.id, ContactId = objContact.id, Roles = 'Employee Services');
        update acctcr1;
       
        Document_Details__c objDD = new Document_Details__c();
        objDD.Contact__c = objGSContact.Id;
        objDD.EXPIRY_DATE__c = system.today();
        objDD.Document_Type__c = 'Employment Visa';
        insert objDD;
        
        Identification__c objId = new Identification__c();
        objId.Id_No__c = '2/6/1234567';
        objId.Valid_From__c = system.today().addYears(-1);
        objId.Valid_To__c = system.today().addYears(1);
        objId.Identification_Type__c = 'Access Card';
        objId.Account__c = objAccount.Id;
        objId.Valid_To__c = system.today();
        insert objId;
        
        test.startTest();
        
	        Database.executeBatch(new GsScheduleBatchEmployeeNotifications(), 1);
        
        test.stopTest();
        
    }
}