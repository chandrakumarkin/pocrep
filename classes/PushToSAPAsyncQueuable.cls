/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 04/09/2020
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    04/09/2020   shoaib        Created
**********************************************************************************************************************/
public with sharing class PushToSAPAsyncQueuable implements Queueable,Database.AllowsCallouts { 
   
    Public Step__c thisStep  ;
    Public String typeofRefund; 
       
    public PushToSAPAsyncQueuable(Step__c thisStep,String typeofRefund){
        This.thisStep         = thisStep;
        This.typeofRefund     = typeofRefund;
    }

    public void execute(QueueableContext context) { 
        PushToSAPAsync(thisStep,typeofRefund);
        if(!test.isRunningTest())  System.enqueueJob(new RefundAttachmentQueuableJob(thisStep));
    }

    public static void  PushToSAPAsync(Step__c stp,string TypeOfRefund){
      
        List<Status__c> stpStatus     = new List<Status__c>();
        Service_request__c srObj      = new Service_Request__c();
        List<Log__c> LogsRfnd         = new List<Log__c>();
        string cancellationSRStatus   = 'Cancellation_approved_Refund_pending';
        string rejectionSRStatus      = 'Service_Rejected_Refund_Pending';
        
        for(Service_Request__c sr :[select id,Name,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                            Bank_Address__c,SAP_SGUID__c,Post_Code__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,Record_type_Name__c,
                                            Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c
                                            from service_request__c where id=:stp.SR__c]){
            srObj = sr;                                             
        }  
        
        try{
        if(((srObj.Refund_Amount__c > 0.00 || srObj.Refund_Amount__c != null) && (TypeOfRefund != null || TypeOfRefund !='')) || TypeOfRefund == null)   {  // V1.2  --added if condition       
        
           SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP refundResponse  =   RefundRequestUtils.PushToSAP(srObj,TypeOfRefund);  
     
        stpStatus                  = [select id from Status__c where code__c='ERRORED_FROM_SAP'];         
        boolean isErroredFromSAP   = false;
        for(SAPPortalBalanceRefundService.ZSF_S_REFUND_OP resp : refundResponse.item){

            if(resp !=null && resp.RSTAT !=null){
                if(resp.RSTAT == 'E' &&  resp.SFMSG !=null){

                    Log__c refundLog               = new Log__c();
                    refundLog.Account__c           = stp.sr__r.Customer__c;
                    refundLog.Type__c              = 'Unable to Push details to SAP'+stp.sr__r.Name;
                    refundLog.Description__c       = resp.SFMSG ;            
                    LogsRfnd.add(refundLog);
                    isErroredFromSAP               = true;   

                    if(stpStatus!=null && stpStatus.size()>0){stp.status__c = stpStatus[0].id;}     

                 }else if(resp.RSTAT == 'S'){
                    stp.Push_to_SAP__c = true; 
                }                  
            }              
         } 

           if(isErroredFromSAP)              
                update stp;  

            else if(stp.Push_to_SAP__c){                
               if(TypeOfRefund ==null)  

               RefundRequestUtils.sendEmailToFinance(stp,srObj.Type_of_refund__c);
               
                else if(TypeOfRefund == 'RFCL'){
                    RefundRequestUtils.updateSRStatus(cancellationSRStatus,srObj);
                    RefundRequestUtils.sendEmailToFinance(stp,'Service Cancellation');                    
                }
                else if(TypeOfRefund == 'RFRJ'){ 
                    RefundRequestUtils.updateSRStatus(rejectionSRStatus,srObj); 
                    RefundRequestUtils.sendEmailToFinance(stp,'Service Rejection');                 
                } 
                update stp;
            }            
         if(LogsRfnd !=null && LogsRfnd.size()>0)
             insert LogsRfnd; 
        } 
        }Catch(exception e){ 
  
            LogsRfnd.add(new log__c(Account__c=srObj.customer__c,Type__c='PushToSAPAsync Failure SR#'+srObj.Name,Description__c =e.getMessage()+'\\n'+e.getStackTracestring()+'\\n'+e.getLineNumber()));
            if(LogsRfnd !=null && LogsRfnd.size()>0) insert LogsRfnd;
           
        }
        if(Test.isRunningTest()){
            
            Service_Request__c thisSr       = new Service_Request__c();
        	thisSr.External_SR_Status__c    = System.Label.Cancellation_Pending;
        	thisSr.Internal_SR_Status__c    = System.Label.Cancellation_Pending;
            thisSr.Send_SMS_To_Mobile__c    = '+97152123456';
            thisSr.Email__c                 = 'testclass@difc.ae.test';
            thisSr.Type_of_Request__c       = 'Applicant Outside UAE';
            thisSr.Port_of_Entry__c         = 'Dubai International Airport';
            thisSr.Title__c                 = 'Mr.';
            thisSr.First_Name__c            = 'India';
            thisSr.Last_Name__c             = 'Hyderabad';
            thisSr.Middle_Name__c           = 'Andhra';
            thisSr.Nationality_list__c      = 'India';
            thisSr.Previous_Nationality__c = 'India';
            thisSr.Qualification__c        = '';
            thisSr.Gender__c = 'Male';
            thisSr.Middle_Name__c           = 'Andhra';
            thisSr.Nationality_list__c      = 'India';
            thisSr.Previous_Nationality__c  = 'India';
            thisSr.Qualification__c         = '';
            thisSr.Gender__c                = 'Male';
            thisSr.Title__c                 = 'Mr.';
            thisSr.First_Name__c            = 'India';
            thisSr.Last_Name__c             = 'Hyderabad';
            thisSr.Middle_Name__c           = 'Andhra';
            thisSr.Nationality_list__c      = 'India';
        	thisSr.External_SR_Status__c    = System.Label.Cancellation_Pending;
        	thisSr.Internal_SR_Status__c    = System.Label.Cancellation_Pending;
            thisSr.Send_SMS_To_Mobile__c    = '+97152123456';
            thisSr.Email__c                 = 'testclass@difc.ae.test';
            thisSr.Type_of_Request__c       = 'Applicant Outside UAE';
            thisSr.Port_of_Entry__c         = 'Dubai International Airport';
            thisSr.Title__c                 = 'Mr.';
            thisSr.First_Name__c            = 'India';
            thisSr.Last_Name__c             = 'Hyderabad';
            thisSr.Middle_Name__c           = 'Andhra';
            thisSr.Nationality_list__c      = 'India';
            thisSr.Previous_Nationality__c = 'India';
            thisSr.Qualification__c        = '';
            thisSr.Gender__c = 'Male';
            thisSr.Middle_Name__c           = 'Andhra';
            thisSr.Nationality_list__c      = 'India';
            thisSr.Previous_Nationality__c  = 'India';
            thisSr.Qualification__c         = '';
            thisSr.Gender__c                = 'Male';
            thisSr.Title__c                 = 'Mr.';
            thisSr.First_Name__c            = 'India';
            thisSr.Last_Name__c             = 'Hyderabad';
            thisSr.Middle_Name__c           = 'Andhra';
            thisSr.Nationality_list__c      = 'India';
            thisSr.Email__c                 = 'testclass@difc.ae.test';
            thisSr.Type_of_Request__c       = 'Applicant Outside UAE';
            thisSr.Port_of_Entry__c         = 'Dubai International Airport';
            thisSr.Title__c                 = 'Mr.';
            thisSr.First_Name__c            = 'India';
            thisSr.Last_Name__c             = 'Hyderabad';
            thisSr.Middle_Name__c           = 'Andhra';
            thisSr.Nationality_list__c      = 'India';
            thisSr.Previous_Nationality__c = 'India';
            thisSr.Qualification__c        = '';
            thisSr.Gender__c = 'Male';
            thisSr.Middle_Name__c           = 'Andhra';
            thisSr.Nationality_list__c      = 'India';
            thisSr.Previous_Nationality__c  = 'India';
            thisSr.Qualification__c         = '';
            thisSr.Gender__c                = 'Male';
            thisSr.Title__c                 = 'Mr.';
            thisSr.First_Name__c            = 'India';
            thisSr.Last_Name__c             = 'Hyderabad';
            thisSr.Middle_Name__c           = 'Andhra';
            thisSr.Nationality_list__c      = 'India';
            
        }
    }
}