/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 03-16-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   03-16-2021   Mari                                Initial Version
**/

public without sharing  class DIFCEmployeeCancellationController{
  
    public string recordTypeName {get; set;}   
    string recordTypeId;
    public boolean isPortal {get; set;}
    public boolean readOnly{get;set;}
    public boolean isEdit {get;set;} 
    public Account objAccount{get;set;}
    public boolean isSubmitted{get;set;}
    public SR_Template__c srTemplate{get;set;}
    public boolean isDetailPage {get; set;}
    public string selectedSponsor{get;set;}
    public Service_Request__c srRequest{get;set;}
    public List<SelectOption> sponsorList{get; set;}
    string srTempId;
    public string typeOfCanRequest{get;set;}

    public boolean isOnPageLoad{get;set;}
 
    public DIFCEmployeeCancellationController(ApexPages.StandardController controller){
     
        try{
            readOnly=true;
            srTempId ='';
            selectedSponsor ='';
            isSubmitted = false;
            objAccount=new account();  
            isPortal = (Userinfo.getUserType() != 'Standard') ? true :false;
            string isEditVal = Apexpages.currentPage().getParameters().get('isEdit');
            string isDetail = Apexpages.currentPage().getParameters().get('isDetail');
            isDetailPage = (string.isNotBlank(isDetail) && isDetail == 'true') ? true : false;
            isEdit = (isEditVal != null && isEditVal =='true') ? true : false; 
            string recId = (Apexpages.currentPage().getParameters().get('id') != null) ? Apexpages.currentPage().getParameters().get('id') : '';
            //List<String> fields = new List<String> {'Contact__c','External_Status_Name__c','RecordType.Name','Identical_Business_Domicile__c'};
            //controller.addFields(fields); // still covered
            srRequest = new Service_Request__c();
            isOnPageLoad = true;
            if(recId != ''){
                //system.debug('===========recId==============='+recId);
                List<Sobject> sobjectList= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(recId),'RecordType.DeveloperName,Customer__r.Name,Contact__r.Name');
                List<Service_Request__c> srList= (List<Service_Request__c>) sobjectList;
                //system.debug('======222=====srList==============='+srList);
                srRequest = (srList.size() >0 && srList != null) ? srList[0] : new  Service_Request__c();
               // system.debug('====%%%%%%====srRequest=========='+srRequest);

                isSubmitted =(srRequest.Submitted_Date__c != null) ? true : false;
                recordTypeName=srRequest.RecordType.DeveloperName;
            }      

            if((isEdit || srRequest.id == null) && !isDetailPage)readOnly= false;

            Map<string,string>  mapParameters = (apexpages.currentPage().getParameters()!=null) ? apexpages.currentPage().getParameters() : new Map<string,string>();       
            recordTypeId = (mapParameters.get('RecordType')!=null)  ? mapParameters.get('RecordType') :'';
            recordTypeName = (mapParameters.get('RecordTypeName')!=null)  ? mapParameters.get('RecordTypeName') :'';
              
            if(recordTypeId != ''){
                
                List<RecordType> recTypeList = [SELECT Id,DeveloperName FROM RecordType  WHERE Id=:RecordTypeId AND sObjectType = 'Service_Request__c' limit 1];
               // system.debug('===========recTypeList ==============='+recTypeList );

                if(recTypeList.size() >0) recordTypeName =recTypeList[0].DeveloperName;
             //   system.debug('===========recordTypeName  ==============='+recordTypeName);

                List<SR_Template__c>srTempList = [select Id,SR_Document_Description_Text__c,Menutext__c,SR_Description__c,SR_RecordType_API_Name__c from SR_Template__c where SR_RecordType_API_Name__c=:recordTypeName and Active__c=true limit 1];
                srTemplate= (srTempList.size() >0) ? srTempList[0] : new SR_Template__c();
                srTempId = (srTempList.size() >0) ? srTempList[0].Id : '';
            }
            
            //System.debug('==srTemplate=====>'+srTemplate);
           // System.debug('==recordTypeName=====>'+recordTypeName);
            
            if(isSubmitted || isDetailPage)readOnly= true;
    
            for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                Contact.Account.Registration_License_No__c,Contact.Account.Index_Card__r.Name,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                Contact.Account.Qualifying_Type__c,Contact.Account.Qualifying_Purpose_Type__c,Contact.Account.Contact_Details_Provided__c,
                Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  
                from User where Id=:userinfo.getUserId()]){
                 
                if(srRequest.id==null){
                    srRequest.Customer__c = objUsr.Contact.AccountId;
                    srRequest.RecordTypeId=recordTypeId;
                    srRequest.Email__c = objUsr.Email;
                    srRequest.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                    srRequest.Send_SMS_To_Mobile__c = objUsr.Phone;
                    srRequest.Entity_Name__c=objUsr.Contact.Account.Name;
                    srRequest.Expiry_Date__c=objUsr.Contact.Account.Next_Renewal_Date__c;
                    objAccount= objUsr.Contact.Account;
                    srRequest.License_Number__c=objUsr.Contact.Account.Registration_License_No__c;
                    srRequest.Establishment_Card_No__c=objUsr.Contact.Account.Index_Card__r.Name;
                }
            }

            if(srRequest.Customer__c != null){
                List<Sobject> sobjectList= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(srRequest.Customer__c),'');
                List<Account> accList= (List<Account>) sobjectList;
                objAccount= (accList.size() >0) ? accList[0] : new Account();
            }
            
            
            
        }catch (Exception e) {
           // system.debug('====%%%%%%===e.getMessage()========'+e.getMessage());
            apexpages.addMessage(new ApexPages.message(Apexpages.Severity.Error,e.getMessage()));
        } 
    }
    
    public void typeOfRequest(){
    
      this.typeOfCanRequest = typeOfCanRequest;
      system.debug(typeOfCanRequest +'typeOfCanRequest');
    }
    
    /**
        * Helper method to populate the list of Sponsored DIFC and seconded employees
            Populate Contact details to SR fields
        * @param  . 
        * @return void.          
    */
    
    public void populateOnLoad(){
        sponsorList = fetchEmployeeList();
       // system.debug('===========selectedSponsor====================='+selectedSponsor);
        if(srRequest != null && srRequest.Id != null && srRequest.Contact__c != null){
            selectedSponsor = srRequest.Contact__c ;
        }
        populateContactDetails();
    }
    
    public void populateContactDetails(){
        
        //system.debug('===========selectedSponsor====================='+selectedSponsor);
        
        List<Contact> contactList = new List<Contact>();
        Contact objContact = new Contact();
        
        
        if(selectedSponsor != '' && selectedSponsor != null){
            List<Sobject> sobjectList= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(selectedSponsor),'');
            List<Contact> objContactList= (List<Contact>) sobjectList;
          //  system.debug('======222=====objContactList==============='+objContactList);
            objContact = (objContactList.size() >0 && objContactList != null) ? objContactList[0] : new  Contact();
            srRequest.Contact__c =(objContact != null) ? objContact.id : null;
        }else srRequest.Contact__c = null;
       // system.debug('=========objContact===================='+objContact);

        srRequest.Title__c=(objContact.Salutation != null) ? objContact.Salutation : null;
        srRequest.First_Name__c=(objContact.FirstName != null) ? objContact.FirstName :null;
        srRequest.Middle_Name__c=(objContact.Middle_Names__c != null) ? objContact.Middle_Names__c :null; 
        srRequest.Last_Name__c=(objContact.LastName != null) ? objContact.LastName :null; 
        srRequest.First_Name_Arabic__c=(objContact.First_Name_Arabic__c != null) ? objContact.First_Name_Arabic__c :null;  
        srRequest.Middle_Name_Arabic__c=(objContact.Middle_Name_Arabic__c != null) ? objContact.Middle_Name_Arabic__c :null;  
        srRequest.Last_Name_Arabic__c=(objContact.Last_Name_Arabic__c != null) ? objContact.Last_Name_Arabic__c :null;  
        srRequest.Gender__c =(objContact.Gender__c != null) ? objContact.Gender__c :null;  
        srRequest.Place_of_Birth__c =(objContact.Place_of_Birth__c != null) ? objContact.Place_of_Birth__c :null; 
        srRequest.Country_of_Birth__c = (objContact.Country_of_Birth__c != null) ? objContact.Country_of_Birth__c :null; 
        srRequest.Qualification__c = (objContact.Qualification__c != null) ? objContact.Qualification__c :null; 
        srRequest.Religion__c = (objContact.RELIGION__c != null) ? objContact.RELIGION__c :null; 
        srRequest.Emirate__c=(objContact.Emirate__c != null) ? objContact.Emirate__c :null;  
        srRequest.City__c=(objContact.City__c != null) ? objContact.City__c :null;  
        srRequest.Area__c=(objContact.Area__c != null) ? objContact.Area__c :null;  
        srRequest.First_Language__c=(objContact.Spoken_Language1__c != null) ? objContact.Spoken_Language1__c :null; 
        srRequest.Second_Language__c=(objContact.Spoken_Language2__c != null) ? objContact.Spoken_Language2__c :null;  
        srRequest.Marital_Status__c=(objContact.Marital_Status__c != null) ? objContact.Marital_Status__c :null;  
        srRequest.Passport_Date_of_Issue__c=(objContact.Date_of_Issue__c != null) ? objContact.Date_of_Issue__c :null;      
        srRequest.Passport_Place_of_Issue__c=(objContact.Place_of_Issue__c != null) ? objContact.Place_of_Issue__c :null;   
        srRequest.Passport_Country_of_Issue__c=(objContact.Issued_Country__c != null) ? objContact.Issued_Country__c :null;  
        srRequest.Email_Address__c=(objContact.Email != null) ? objContact.Email :null; 
        srRequest.PO_BOX__c=(objContact.PO_Box_HC__c != null) ? objContact.PO_Box_HC__c :null; 
        srRequest.Residence_Phone_No__c=(objContact.Phone != null) ? objContact.Phone :null;  
        srRequest.Building_Name__c=(objContact.ADDRESS_LINE1__c != null) ? objContact.ADDRESS_LINE1__c :null;  
        srRequest.Street_Address__c=(objContact.ADDRESS_LINE2__c != null) ? objContact.ADDRESS_LINE2__c :null;  
        srRequest.Mobile_Number__c=(objContact.MobilePhone != null) ? objContact.MobilePhone :null;  
        srRequest.Work_Phone__c=(objContact.Work_Phone__c != null) ? objContact.Work_Phone__c :null;  

        srRequest.Identical_Business_Domicile__c=(objContact.Domicile__c != null) ? objContact.Domicile__c :null;  
        srRequest.City_Town__c=(objContact.CITY1__c != null) ? objContact.CITY1__c :null;
        // srRequest.Phone_No_Outside_UAE__c=(objContact.OtherPhone != null) ? objContact.OtherPhone :null;  
        srRequest.Address_Details__c=(objContact.Address_outside_UAE__c != null) ? objContact.Address_outside_UAE__c :null;  
        srRequest.Sponsor_Visa_No__c=(objContact.Visa_Number__c != null) ? objContact.Visa_Number__c :null; 
        srRequest.Passport_Date_of_Expiry__c=(objContact.Passport_Expiry_Date__c != null) ? objContact.Passport_Expiry_Date__c :null;  
        srRequest.Gender__c=(objContact.Gender__c != null) ? objContact.Gender__c :null;  
        srRequest.Nationality_list__c=(objContact.Nationality__c != null) ? objContact.Nationality__c :null; 
        srRequest.Passport_Number__c=(objContact.Passport_No__c != null) ? objContact.Passport_No__c :null;  
        srRequest.Mother_Full_Name__c=(objContact.Mother_Full_Name__c != null) ? objContact.Mother_Full_Name__c :null; 
        srRequest.Date_of_Birth__c=(objContact.Birthdate != null) ? objContact.Birthdate :null;  
        srRequest.Residence_Visa_No__c=(objContact.Visa_Number__c != null) ? objContact.Visa_Number__c :null;  
        srRequest.Visa_Expiry_Date__c =(objContact.Visa_Expiry_Date__c != null) ? objContact.Visa_Expiry_Date__c :null;  

      //  system.debug('=========srRequest===================='+srRequest);
        isOnPageLoad = false;
    }
    
     //Tkt # DIFC2-13814
    public static string CheckOpenSRVisaCancellation(Service_Request__c objSr,string SRTemplateId,string strRecordTypeName){
       // system.debug('=========SRTemplateId===================='+SRTemplateId);
       // system.debug('=========objSr.Customer__c===================='+objSr.Customer__c);
     //   system.debug('=========objSr.Contact__c===================='+objSr.Contact__c);

        if(SRTemplateId!=null && objSr.Customer__c!=null && objSr.Contact__c != null){
            List<Service_Request__c> srList = [ SELECT id,name,SR_Template__c,SR_Template__r.Name,Customer__c,Record_Type_Name__c,isClosedStatus__c,Is_Rejected__c,IsCancelled__c,Contact__c
                FROM Service_Request__c
                WHERE SR_Template__c=:SRTemplateId and Customer__c=:objSr.Customer__c and isClosedStatus__c=false AND Is_Rejected__c=false AND IsCancelled__c=false
                and Id!=:objSr.Id and Contact__c=:objSr.Contact__c limit 1
            ];
           // system.debug('========srList ==============='+srList );

            string SRName = (srList.size() >0) ? srList[0].name :'';
            if(SRName !='')return 'There is a similar request (SR '+SRName+') raised through the portal. Please check under Service Requests.';
        }
        return 'Success';
    }

    /************************************************************************************************************
    Method Name: saveRequest
    Functionality: save or update SR Record and redirect to detail page of SR
    **************************************************************************************************************/
    public pageReference saveRequest(){
        pageReference redirectPage = null;
        try{ 
            string message = 'Success';
            if(selectedSponsor =='' || selectedSponsor ==null){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Kindly select the Employee for Cancellation request'));
                return null;
            }
            if(srRequest.Statement_of_Undertaking__c == false){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Kindly select the I agree to the above statement'));
                return null;
            }
            //system.debug('===1111====srRequest================'+srRequest);
           // system.debug('===srTemplate.Id=============='+srTemplate);
           // system.debug('==srTemplate.SR_RecordType_API_Name__c=============='+srTemplate.SR_RecordType_API_Name__c);
            //public static string CheckOpenSRVisaCancellation(Service_Request__c objSr,string SRTemplateId,string strRecordTypeName){
            System.debug('=^^^^=srRequest===SR_Template__c==>'+srTempId );  
            
            string opensrchk= DIFCEmployeeCancellationController.CheckOpenSRVisaCancellation(srRequest,srTempId,recordTypeName);    
            if(opensrchk!='Success'){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,opensrchk));
                return null;
            } 
        
           // system.debug('===1111====srRequest================'+srRequest);
            upsert srRequest;   
           // system.debug('==22222=====srRequest================'+srRequest);
            redirectPage = new PageReference('/apex/'+GlobalConstants.DIFC_EMPLOYEE_CANCELLATION_VF_NAME);
            redirectPage.getParameters().put('RecordType',srRequest.recordTypeId);
            redirectPage.getParameters().put('RecordTypeName',recordTypeName);
            redirectPage.getParameters().put('id',srRequest.id);
            redirectPage.getParameters().put('isDetail','true');
               
            if(redirectPage!=null){
                redirectPage.setRedirect(true); 
                return redirectPage;
            }
        }catch(DmlException e) {
            Integer numErrors = e.getNumDml();
            for(Integer i=0;i<numErrors;i++) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(i)));
            }
        }catch(exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        return redirectPage;
    }
    
     /************************************************************************************************************
    Method Name: submitSR
    Functionality: submit the SR Record and redirect to Submission page of SR
    **************************************************************************************************************/

    public pageReference submitSR(){
        pageReference redirectPage = null;
        
        try{

            if(srRequest.Required_Docs_not_Uploaded__c == true){

                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,GlobalConstants.DOCUMENT_UPLOAD_MANDATORY));
                return null;
            }

            redirectPage = new PageReference('/apex/SRSubmissionPage');
            redirectPage.getParameters().put('id',srRequest.id);
            redirectPage.setRedirect(true); 
            return redirectPage;          
        }catch(exception ex){
            system.debug('----msg---'+ex.getMessage()); 
            system.debug('----lineNumber---'+ex.getLineNumber());   
        }
        return redirectPage;
    }
 
     public List<SelectOption> fetchEmployeeList(){
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
       // system.debug('========objAccount====================='+objAccount.Id);

        for(Relationship__c contSpn : [select id,Object_Contact__c,Relationship_Name__c from Relationship__c 
            where Subject_Account__c=:objAccount.id and Object_Contact__c!=null and  
            (
                //Relationship_Type__c ='Has DIFC Sponsored Employee' OR  Relationship_Type__c = 'HAS DIFC Seconded Sponsored Employee'
                Relationship_Type__c IN: GlobalConstants.RT_HAS_DIFC_SPONSORED_EMPLOYEE
            ) 
            and Active__c=true])
        {
            options.add(new SelectOption(contSpn.Object_Contact__c,contSpn.Relationship_Name__c));

        }
      //  system.debug('========options====================='+options);
        return options;
    }
    
     /************************************************************************************************************
    Method Name: editRequest
    Functionality: Edit the SR Record and redirect to detail page after Save
    **************************************************************************************************************/


    public pageReference editRequest(){
        pageReference redirectPage = null;


        try{
            redirectPage = new PageReference('/apex/'+GlobalConstants.DIFC_EMPLOYEE_CANCELLATION_VF_NAME);
            redirectPage.getParameters().put('RecordTypeId',srRequest.recordTypeId);
            redirectPage.getParameters().put('isEdit','true');
            redirectPage.getParameters().put('RecordTypeName',recordTypeName);
            redirectPage.getParameters().put('id',srRequest.id);
            redirectPage.setRedirect(true); 
          //  system.debug('----edit redirectPage---'+redirectPage);   
            return redirectPage;          
        }catch(exception ex){
            system.debug('----msg---'+ex.getMessage()); 
            system.debug('----lineNumber---'+ex.getLineNumber());   
        } 
        return redirectPage;
    }
    
     /************************************************************************************************************
    Method Name: cancelRequest
    Functionality: Cancel the SR Record before approval of SR
    **************************************************************************************************************/


    public pageReference cancelRequest(){//v1.1
        pageReference objRef = null;
        try{
            if(IsDetailPage == true){ //Detail Page Button
               //system.debug('===========cancelRequest====================');
                if(srRequest.Submitted_Date__c != null && srRequest.External_Status_Name__c!=GlobalConstants.SR_STAT_APPROVED){
                   // system.debug('===========cancelRequest======submitted==============');

                    string result = GsServiceCancellation.ProcessCancellation(srRequest.id);    
                    if(result!='Cancellation submitted successfully'){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,result));   
                    }else{
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,result));
                    }
                    if(result=='Cancellation submitted successfully'){
                        objRef = new Pagereference('/'+srRequest.Id);   
                    }
                }else{
                   // system.debug('======deail not submitted=====cancelRequest======submitted==============');
                    if(srRequest.External_Status_Name__c == GlobalConstants.SR_STAT_DRAFT)delete srRequest;
                    objRef = (isPortal) ? new Pagereference('/apex/home') : new Pagereference('/home/home.jsp');
                }
            }else if(IsDetailPage == false){ //Edit Page Button
                //system.debug('======dEditeail not submitted=====cancelRequest======submitted==============');
                if(srRequest.Id != null)
                    objRef = new Pagereference('/'+srRequest.Id);
                else{
                    objRef = (isPortal) ? new Pagereference('/apex/home') : new Pagereference('/home/home.jsp');
                }
            }
            //system.debug('======objRef===================='+objRef);

            if(objRef != null)objRef.setRedirect(true);
            return objRef;
        }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please contact support.'));
        }
        return objRef;
    }
    

}