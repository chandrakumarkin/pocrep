/*
-------------------------------------------------------------------------
    **Version History**
     * @Author: Anil Valluri 
     * @version: 2.0
     * @Date: 08-Aug-2021
     * @Description: 
        1. Case management enhancements- Remove 'Type' field whereever referred.
*/

public without sharing class CreateLeadController{

    public string caseId{get;set;}
    
    public Case objCase{get;set;}

    public CreateLeadController(ApexPages.StandardController controller){
        if(apexpages.currentpage().getParameters().get('Id')!=null)
            caseId = String.escapeSingleQuotes(apexpages.currentpage().getParameters().get('Id'));
        objCase = new Case();
        Case[] cases = [SELECT Id, GN_Area_of_Business__c, GN_First_Name__c, Company_Name__c, Email__c, 
                    GN_Last_Name__c, Origin, GN_Are_you_sure_you_want_to_create_lead__c, Full_Name__c, 
                    AccountId, Account.Name, GN_Account_Name_Non_DIFC__c, GN_Related_Lead__c, 
                    Status, GN_Type_of_Feedback__c, GN_Department__c, Type 
                    FROM Case WHERE Id = :caseId];
        if(cases.size() > 0){
            objCase = cases[0];
            objCase.GN_Last_Name__c = objCase.Full_Name__c;
            if(objCase.AccountId != null){
                objCase.Company_Name__c = objCase.Account.Name;
            } else{
                objCase.Company_Name__c = objCase.GN_Account_Name_Non_DIFC__c;
            }
            objCase.GN_Are_you_sure_you_want_to_create_lead__c = 'Yes';
        }
    }
    
    public pagereference CancelAction(){
        Pagereference pg = new Pagereference('/'+caseId);
        pg.setredirect(true);
        return pg;
    }
    
    public pagereference createLead(){  
        if(objCase.GN_Related_Lead__c == null){
            try{
                
                //CR- Set the case status to closed - 05152018
                /* ----START------- */
                objCase.GN_Type_of_Feedback__c = 'Enquiry';
                //objCase.Type = 'Setting Up in the DIFC'; // PwC: Case management enhancements.
                objCase.GN_Department__c = 'Business Development';
                objCase.Status = 'Closed - Resolved';
                /* ----END------- */
                
                update objCase;
                
                Pagereference pg = new Pagereference('/'+caseId);
                pg.setredirect(true);
                return pg;
                
            } catch(DMLException e){
                string DMLError = e.getdmlMessage(0)+'';
                if(DMLError==null){
                    DMLError = e.getMessage()+'';
                }
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,DMLError);
                ApexPages.addMessage(myMsg);
                return null;
            }
        } else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'A lead has already been created for this case record.');
            ApexPages.addMessage(myMsg);
            return null;
        }
    }
}