/******************************************************************************************
*  Author         :   Durga Prasad
*  Company        :   PwC
*  Date           :   26-Nov-2019
*  Description    :   Apex Controller for Step Transition Window in Lightning
*  Version        :   1.0
********************************************************************************************/
public without sharing class OB_QuickStepTransitionController{
    
    public HexaBPM__Step__c step{get;set;}
    public string SRID{get;set;}
    public string StepID{get;set;}
    public boolean hasAccess{get;set;}
    User objCurrentUser;
    public map<string,string> MapDelegatedUsers;
    public string userType{get;set;}
    public list<OB_TransitionWrapper> lstTrnsWrap{get;set;}
    public Integer iListSize{get;set;}
    public map<id,HexaBPM__Step_Transition__c> mapStepTransition{get;set;}
    public OB_QuickStepTransitionController(string recordId,string stepId){
        lstTrnsWrap = new list<OB_TransitionWrapper>();
        iListSize = 0;
        if(recordId!=null){
            SRID = recordId;
        }
        step = new HexaBPM__Step__c();
        if(stepId!=null){
            StepID = stepId;
            for(HexaBPM__Step__c stp:[select Id,Name,HexaBPM__Summary__c,RecordTypeId,HexaBPM__Step_Status__c,RecordType.DeveloperName,OwnerId,Owner.Name,RecordType.Name,HexaBPM__Parent_Step__c,HexaBPM__SR__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,HexaBPM__Status__c,HexaBPM__SR_Step__c,HexaBPM__SR_Step__r.HexaBPM__Step_Instructions__c,HexaBPM__Rejection_Reason__c,HexaBPM__Status__r.Name from HexaBPM__Step__c where Id=:StepID and HexaBPM__SR__c!=null and HexaBPM__SR__r.HexaBPM__SR_Template__c!=null and IsDeleted=false]){
                step = stp;
            }
        }
        hasAccess = false;
        objCurrentUser = new User();
        for(User curUser:[select Id,ContactId,ProfileId,Profile.UserLicenseId,Profile.UserLicense.Name,Profile.UserLicense.LicenseDefinitionKey,Profile.Name from User where Id=:userInfo.getUserId() and IsActive=true]){
            objCurrentUser = curUser;
            if(curUser.ContactId==null){
                userType = 'salesforce';
            }else{
                userType = 'Community';
            }
        }
    }
    /**
     * Method Name : Check_Permissions
     * Desription : Checks if the current user has access to make the required transition selection or not.
     **/
    public void Check_Permissions(){
        MapDelegatedUsers = new map<string,string>();
        MapDelegatedUsers = OB_SetupObjectDataHelper.GetDelegatedUsers(objCurrentUser.Id);
        if(!system.test.isRunningTest() && (step.OwnerId==userinfo.getUserId() || (MapDelegatedUsers!=null && MapDelegatedUsers.get(step.OwnerId)!=null) || (objCurrentUser!=null && string.valueof(objCurrentUser.Profile.Name).indexof('System Administrator')>-1))){
            hasAccess = true;
        }else if(!system.test.isRunningTest() && userType!=null && userType=='Community'){
            hasAccess = true;
        }else{
            if(string.valueOf(step.OwnerId).substring(0,3)=='00G' || system.test.isRunningTest()){
                for(OB_SetupObjectDataHelper.GroupDetails GD:OB_SetupObjectDataHelper.getGroupData(step.OwnerId)){
                    if(GD.GroupOrUserId==userinfo.getUserId()){
                        hasAccess = true;
                        break;
                    }
                }
            }
        }
        Prepare_Transitions();
    }
    
    /**
     * Method Name : Prepare_Transitions
     * Description : Queries all transitions from the current step and sets the valid possible transitions and maps it to a map value
     **/
    public void Prepare_Transitions(){
        set<id> setValidSteps = new set<id>();
        mapStepTransition = new map<id,HexaBPM__Step_Transition__c>();
        system.debug('Start Status===>'+step.HexaBPM__Status__r.Name);
        if(userType=='salesforce'){
            for(HexaBPM__Step_Transition__c trans:[select HexaBPM__From__c,HexaBPM__To__c,HexaBPM__Transition__c,HexaBPM__Transition__r.HexaBPM__To__c,HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Code__c,HexaBPM__SR_Step__c,HexaBPM__SR_Status_External__c,HexaBPM__SR_Status_External__r.HexaBPM__Type__c,HexaBPM__SR_Status_Internal__c,HexaBPM__SR_Status_Internal__r.HexaBPM__Type__c from HexaBPM__Step_Transition__c where HexaBPM__Transition__c!=null and HexaBPM__From__c=:step.HexaBPM__Status__r.Name and HexaBPM__SR_Step__c=:step.HexaBPM__SR_Step__c and IsDeleted=false]){
                setValidSteps.add(trans.HexaBPM__Transition__r.HexaBPM__To__c);
                mapStepTransition.put(trans.HexaBPM__Transition__r.HexaBPM__To__c,trans);
            }
        }else{ 
            for(HexaBPM__Step_Transition__c trans:[select HexaBPM__From__c,HexaBPM__To__c,HexaBPM__Transition__c,HexaBPM__Transition__r.HexaBPM__To__c,HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Code__c,HexaBPM__SR_Step__c,HexaBPM__SR_Status_External__c,HexaBPM__SR_Status_External__r.HexaBPM__Type__c,HexaBPM__SR_Status_Internal__c,HexaBPM__SR_Status_Internal__r.HexaBPM__Type__c from HexaBPM__Step_Transition__c where HexaBPM__Transition__c!=null and HexaBPM__From__c=:step.HexaBPM__Status__r.Name and HexaBPM__SR_Step__c=:step.HexaBPM__SR_Step__c and HexaBPM__Display_on_Portal__c=true and IsDeleted=false]){
                setValidSteps.add(trans.HexaBPM__Transition__r.HexaBPM__To__c);
                mapStepTransition.put(trans.HexaBPM__Transition__r.HexaBPM__To__c,trans);
            }
        }
        if(setValidSteps!=null && setValidSteps.size()>0){
            OB_TransitionWrapper objWrap;
            for(HexaBPM__Status__c objstat:[Select Id,Name,HexaBPM__Type__c,HexaBPM__Rejection__c,HexaBPM__SR_Closed_Status__c,HexaBPM__Code__c from HexaBPM__Status__c where ID IN:setValidSteps and IsDeleted=false]){
                objWrap = new OB_TransitionWrapper();
                objWrap.objStatus = objstat;
                objWrap.objSRStepTrans = new HexaBPM__Step_Transition__c();
                if(mapStepTransition.get(objstat.id)!=null)
                    objWrap.objSRStepTrans = mapStepTransition.get(objstat.id);
                lstTrnsWrap.add(objWrap);
            }
            iListSize = lstTrnsWrap.size();
        }
        system.debug('mapStepTransition===>'+mapStepTransition);
    }
    
    /**
     * Method Name : SaveChanges
     * Desciption  : Method Invoked on click of the Proceed button. Passes the selected transition, rejected reason, step notes, SR id, Step object, map of possible step transitions and updates the step's status to the
     *               valid step value selected by the user
     **/
    @AuraEnabled
    public static OB_LtngQuickStepTransitionWrapper SaveChanges(string selTransition, string RejReason, string StepNotes, string SRID, HexaBPM__Step__c step1, map<id, HexaBPM__Step_Transition__c> mapStepTransition){
        pagereference pg;
        wrapperObj = new OB_LtngQuickStepTransitionWrapper();
        wrapperObj.flsErrorMessage='Success';
        wrapperObj.flsErrorCheck = false;
        list<HexaBPM__Step__c> stepWrapper = new list<HexaBPM__Step__c>();
        integer flag=0;
        system.debug('selTransition===>'+selTransition);
        system.debug('selTransition Map===>'+mapStepTransition.get(selTransition));
        if(selTransition!=null && mapStepTransition.get(selTransition)!=null ){
            //REUPLOAD_DOCUMENT
            System.debug(mapStepTransition.get(selTransition).HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Code__c);
            if(mapStepTransition.get(selTransition).HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Code__c=='REUPLOAD_DOCUMENT'){
                boolean hasReUploadDoc = false;
                for(HexaBPM__SR_Doc__c srdoc:[select Id from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRID and HexaBPM__Status__c='Re-upload' limit 1]){
                    hasReUploadDoc = true;
                }
                if(!hasReUploadDoc){
                    wrapperObj.flsErrorMessage = 'Atleast one document should be marked as re-upload';
                    wrapperObj.flsErrorCheck = true;
                    return wrapperObj;
                }
            }
            Savepoint Stat_svpoint = Database.setSavepoint();
            try{
                pg = new PageReference('/'+SRID);
                pg.setRedirect(true);
                
                boolean isRejected = false;
                HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id=SRID);
                if(StepNotes!=null && StepNotes!='')
                    objSR.Step_Notes_Long__c = StepNotes;
                if(RejReason!=null && RejReason!=''){
                    objSR.Step_Notes_Long__c = RejReason;
                    objSR.Rejection_Reason_Long__c = RejReason;
                }
                if(mapStepTransition.get(selTransition).HexaBPM__SR_Status_Internal__c!=null || mapStepTransition.get(selTransition).HexaBPM__SR_Status_External__c!=null){
                	if(mapStepTransition.get(selTransition).HexaBPM__SR_Status_Internal__c!=null){
                    	objSR.HexaBPM__Internal_SR_Status__c = mapStepTransition.get(selTransition).HexaBPM__SR_Status_Internal__c;
                    	if(mapStepTransition.get(selTransition).HexaBPM__SR_Status_Internal__r.HexaBPM__Type__c=='Rejected')
                    		isRejected = true;
                	}
                    if(mapStepTransition.get(selTransition).HexaBPM__SR_Status_External__c!=null){
                    	objSR.HexaBPM__External_SR_Status__c = mapStepTransition.get(selTransition).HexaBPM__SR_Status_External__c;
                    	if(mapStepTransition.get(selTransition).HexaBPM__SR_Status_External__r.HexaBPM__Type__c=='Rejected')
                    		isRejected = true;
                    }
                }
                update objSR;
                
                if(isRejected)
                	OB_StepTriggerHelper.CloseOpenStepsonRejection(objSR.Id,step1.Id);
                
                HexaBPM__Step__c objStepToUpdate = new HexaBPM__Step__c(Id=step1.Id);
                objStepToUpdate.HexaBPM__Status__c = mapStepTransition.get(selTransition).HexaBPM__Transition__r.HexaBPM__To__c;
                objStepToUpdate.HexaBPM__Step_Notes__c = StepNotes;
                objStepToUpdate.Internal_Comments__c = StepNotes;
                objStepToUpdate.HexaBPM__Rejection_Reason__c = RejReason;
                stepWrapper.add(objStepToUpdate);
                update stepWrapper;
                system.debug('updated');
            }catch(DMLException e){
                string DMLError = e.getdmlMessage(0)+'';
                if(DMLError==null){
                    DMLError = e.getMessage()+'';
                }
                selTransition = null;
                Database.rollback(Stat_svpoint);
                wrapperObj.flsErrorMessage = DMLError;
                wrapperObj.flsErrorCheck = true;
            }
        }else{
            wrapperObj.flsErrorCheck = true;
            wrapperObj.flsErrorMessage='Please select a status to Proceed';
        }
        return wrapperObj;
    }
    
    @AuraEnabled
    public static boolean acceptAction(string stepId, boolean isStepOwnedByQueue){
        if(isStepOwnedByQueue == true){
            //Accept Action
            HexaBPM__Step__c step = new HexaBPM__Step__c(Id = stepId);
            step.OwnerId = Userinfo.getUserId();
            update step;
            isStepOwnedByQueue = false;
        }else{
            //Release Action
            list<HexaBPM__Step__c> stepSelected = [SELECT Id, HexaBPM__SR_Step__r.OwnerId FROM HexaBPM__Step__c WHERE Id = :stepId];
            HexaBPM__Step__c step = new HexaBPM__Step__c(Id =stepId);
            if(stepSelected != null && stepSelected.size() > 0){
                step.OwnerId = stepSelected[0].HexaBPM__SR_Step__r.OwnerId;
                update step;
            }
            isStepOwnedByQueue = true;
        } 
        return isStepOwnedByQueue;
    }
    @AuraEnabled    
    public static OB_LtngQuickStepTransitionWrapper wrapperObj {get; set;}
   /**
     * Method Name : getLstTransWrapper
     * Description : Returns a Wrapper Class Object with the values to initialize the lightning component
     *               Also executes check permissions method and sets the hasAccess variable.
     **/
    @AuraEnabled
    public static OB_LtngQuickStepTransitionWrapper getLstTransWrapper(ID recordId, ID stepId){
        OB_QuickStepTransitionController newObj = new OB_QuickStepTransitionController(recordId, stepId);
        newObj.Check_Permissions();
        system.debug(newObj);
        string ownerId;
        wrapperObj = new OB_LtngQuickStepTransitionWrapper();
        HexaBPM__Service_Request__c temp =[select Id, Name, HexaBPM__SR_template__c, HexaBPM__SR_template__r.Name from HexaBPM__Service_Request__c where id=:newObj.SRID];
        wrapperObj.SRRequestType = temp.HexaBPM__SR_Template__r.Name;
        wrapperObj.SRNumber = temp.Name;
        wrapperObj.ltngLstTrnsWrap = newObj.lstTrnsWrap;
        wrapperObj.hasAccess = newObj.hasAccess;
        wrapperObj.userType = newObj.userType;
        wrapperObj.listSize = newObj.iListSize;
        wrapperObj.step_ltng = newObj.step;
        if(newObj.step != null && newObj.step.Owner != null){
            ownerId = newObj.step.Owner.Id;
            if(ownerId.subString(0,3) == '00G')
                wrapperObj.isStepOwnedByQueue = true;
        }
        wrapperObj.ltngMapStepTransition = newObj.mapStepTransition;
        wrapperObj.SRId = newObj.SRID;
        system.debug(wrapperObj);
        return wrapperObj;
    }
    
    /**
     * Method Name : CancelAction
     * Description : Method invoked on click of the cancel button. Redirects the page back to the SR Detail page
     **/
    @AuraEnabled
    public static void CancelAction(string SRID){
        System.debug('CancelAction');
        Pagereference pg = new Pagereference('/'+SRID);
        system.debug(pg);
        pg.setRedirect(true);
        if(!system.test.isRunningTest())
            aura.redirect(pg);
    }
    /**
     * Method Name : ViewStep
     * Description : Method invoked on click of the View Step button. Redirects the page to the Step Detail page
     **/
    @AuraEnabled
    public static void ViewStep(string StepID){
        system.debug('ViewStep');
        Pagereference pg = new Pagereference('/'+StepID);
        system.debug(pg);
        pg.setRedirect(true);
        if(!system.test.isRunningTest())
            aura.redirect(pg);
    }
    
   
}