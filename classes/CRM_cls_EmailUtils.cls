/******************************************************************************************
*  Name        : CRM_cls_EmailUtils 
*  Author      : Claude Manahan
*  Company     : NSI JLT
*  Date        : 2016-12-14
*  Description : Email Utils for CRM
----------------------------------------------------------------------------------------                               
Modification History 
----------------------------------------------------------------------------------------
V.No    Date            Updated By      Description
----------------------------------------------------------------------------------------              
V1.0    14-12-2016      Claude          Created (#3749)
v1.1    26-12-2016      Kaavya          Added log for errors 
v1.2    29-01-2016      Claude          Added emailbody to error logs
V1.3    11-07-2017      Claude          Added mail merge feature
V1.4    09-08-2017      Claude          Moved email message custom settings to class
                                        Added CC feature
V1.5    10-08-2017      Claude          Added method to get email messages and their attachments
v1.6    18-03-2018      Arun            Ticket#4220 added filter for non-financial companies
V1.7    25-03-2018      Arun            Ticket #4823 Added code to handle DNFBP 
V1.8    04-04-2018      Danish          return the list of point person on the basis of company record type tkt # 4473
v1.9    18-02-2019      Azfer           Added a method which will be used to CC account RM. 
v1.10   18-02-2019      Azfer           Updated OppAccountDFSAEmail for DNFBP dates and updates.
v1.11   17-03-2019      Azfer           Updated Method to get code coverage.
V1.12   21-APR-2020     Sai             Updating ROC Status
*******************************************************************************************/
global without sharing class CRM_cls_EmailUtils {

    /**
     * V1.4
     * Returns the primary contact ID of the account
     * @params      accountId       The account's record ID
     * @return      id              The contact's record Id
     */
    webservice static String getClientContacts(String accountId) {

        /*Map<String,String> mapOfContacts = new Map<String,String>();
        
        for(Contact c : [SELECT Id, Name FROM Contact WHERE AccountId = :accountId]){
            
            mapOfContacts.put(c.Id,'{ Name : "'+c.Name+'", Id : "'+c.Id+'" }');
            
        }
        
        return JSON.serialize(mapOfContacts);*/

        return [SELECT Id FROM Contact WHERE AccountId =: accountId].Id;
    }

    /**
     * V1.4 , 1.6
     * Returns a list of point persons for sending emails
     * v 1.6 Add parameter to get Point of origin on the basis of Account company type
     * @params      accountId       The account's record ID
     * @return          pointPersonsString          List of points persons formatted in JSON
     */
    webservice static String getPointPersons(string aAccountID) {

        string AccountCompanyType = [Select Company_type__c from Account where ID =: aAccountID limit 1].Company_type__c;
        Map < String, String > emailUrlMapByType = new Map < String, String > ();
        Map < String, String > emailUrlMapAll = new Map < String, String > ();
        List < CRM_Email_Template_Code__mdt > lstCRMEmailTemp = getAllTemplateConfigurations();

        for (CRM_Email_Template_Code__mdt c: lstCRMEmailTemp) {

            if (String.isNotBlank(c.Point_of_Origin__c) && (Test.isRunningTest() || (!Test.isRunningTest() && !c.Point_of_Origin__c.contains('Test')))) {

                if (c.Account_Company_Type__c != NULL && c.Account_Company_Type__c.contains(AccountCompanyType))
                    emailUrlMapByType.put(c.Point_of_Origin__c, c.Point_of_Origin__c);
                else
                    emailUrlMapAll.put(c.Point_of_Origin__c, c.Point_of_Origin__c);

            }

        }
        if (!emailUrlMapByType.isEmpty()) return JSON.serialize(emailUrlMapByType);
        return JSON.serialize(emailUrlMapAll);


    }

    /**
     * V1.9
     * Returns a customized 'Send Email' URL; used
     * in 'Send an Email' custom button for tasks 
     * @params      relatedToId             The related record's id
     * @params      accountId               The account's record Id
     * @params      stageName               The stage of the opportunity
     * @params      RM Email Address        The email address of the Account RM
     * @params      stateStep               The stage step of the opportunity
     * @params      pointOfOrigin           The selected point of origin
     * @params      emailTemplateCode       The email template code
     * @return      emailLink               The customized 'Send Email' URL
     */

    webservice static String createEmailLinkWithRM(String relatedToId, String accountId, String stageName, String paramRMEmailAddress, String stageStep, String pointOfOrigin, String emailTemplateCode) {
        //Method: createEmailLink return the link with the p4 i.e. the cc email as the last paramemter and checking if that param exsist update the value else add the param.
        String LinkToReturn = createEmailLink(relatedToId, accountId, stageName, stageStep, pointOfOrigin, emailTemplateCode);

        if (LinkToReturn.containsIgnoreCase('EmailAuthor') && paramRMEmailAddress != null && paramRMEmailAddress != '') {
            if (LinkToReturn.containsIgnoreCase('&p4=')) {
                LinkToReturn += ';' + paramRMEmailAddress;
            } else {
                LinkToReturn += '&p4=' + paramRMEmailAddress;
            }
        }

        return LinkToReturn;
    }

    /**
     * V1.4
     * Returns a customized 'Send Email' URL; used
     * in 'Send an Email' custom button for tasks 
     * @params      relatedToId             The related record's id
     * @params      accountId               The account's record Id
     * @params      stageName               The stage of the opportunity
     * @params      stateStep               The stage step of the opportunity
     * @params      pointOfOrigin           The selected point of origin
     * @params      emailTemplateCode       The email template code
     * @return      emailLink               The customized 'Send Email' URL
     */
    webservice static String createEmailLink(String relatedToId, String accountId, String stageName, String stageStep, String pointOfOrigin, String emailTemplateCode) {

        boolean isGate = false;
        List < Account > lstAccount = [Select ID FROM Account where ID =: accountId
            AND Property_solutions__c IN('Retail - Gate Avenue', 'Retail - Gate Avenue M2')
        ];

        if (lstAccount.isEmpty()) {
            List < Lead > lstLead = [Select ID from Lead where ConvertedAccountId =: accountId
                AND Property_solutions__c IN('Retail - Gate Avenue', 'Retail - Gate Avenue M2')
            ];

            if (lstLead.isEmpty())
                isGate = false;
            else
                isGate = true;
        } else
            isGate = true;

        if (pointOfOrigin == 'RRC Others' && isGate) {

            return 'You cannot select RRC Others for Retail Gate Avenue or Retail Gate Avenue M2';

        }

        if (pointOfOrigin == 'RRC Gate' && !isGate) {

            return '"Property Solution” field is blank, please check';

        }
        List < CRM_Email_Template_Code__mdt > referencedTemplates = String.isBlank(emailTemplateCode) ? getEmailTemplateByStage(stageName, stageStep, pointOfOrigin) : getEmailTemplateByTemplateCode(stageName, stageStep, emailTemplateCode);

        if (referencedTemplates.isEmpty() && !Test.isRunningTest()) {

            return 'You are not allowed to send to ' + pointOfOrigin + ' at this particular stage.';

        }

        //contactId = String.isBlank(contactId) ? referencedTemplate[0].Approver_ID__c : contactId;

        String contactId = pointOfOrigin.equals('Client') ? getClientContacts(accountId) : referencedTemplates[0].Approver_ID__c;

        String templateId = referencedTemplates[0].Template_ID__c;

        String copyToEmails = referencedTemplates[0].CC__c;

        return URL.getSalesforceBaseUrl().toExternalForm() + '/_ui/core/email/author/EmailAuthor?p2_lkid=' +
            contactId + '&&rtype=003&p3_lkid=' + relatedToId +
            '&retURL=%2F' + relatedToId +
            '&p26=portal@difc.ae&template_id=' + templateId +
            (String.isNotBlank(copyToEmails) ? '&p4=' + copyToEmails : ''); // V1.4 - Claude - Added CC field

    }

    /** 
     * V1.4 
     * Returns a list of all CRM Template code records
     * @return      referencedTemplates     The CRM Template code records that matches the criteria
     */
    public static List < CRM_Email_Template_Code__mdt > getAllTemplateConfigurations() {

        return [SELECT MasterLabel, CC__c, Stage_Steps__c, Approver_ID__c,
            Security_Check__c, Email_Template_Code__c, Point_of_Origin__c,
            Opportunity_Stage__c, Approver_Name__c, DeveloperName, Approver_Email__c,
            Client__c, Account_Company_Type__c FROM CRM_Email_Template_Code__mdt
        ];

    }

    /** 
     * V1.5
     * Returns a list of all CRM Template code records 
     * based on the opportunity's stage and stage step, and
     * point of origin of the email
     * @params      stageName               The stage of the opportunity
     * @params      stateStep               The stage step of the opportunity
     * @params      pointOfOrigin           The selected point of origin
     * @return      referencedTemplates     The CRM Template code records that matches the criteria
     */
    private static List < CRM_Email_Template_Code__mdt > getEmailTemplateByTemplateCode(String stageName, String stageStep, String emailTemplateCode) {

        return [SELECT Id, Approver_ID__c, CC__c, Template_ID__c FROM CRM_Email_Template_Code__mdt WHERE Opportunity_Stage__c =: stageName AND Stage_Steps__c =: stageStep AND Email_Template_Code__c =: emailTemplateCode];

    }

    /** 
     * V1.4 
     * Returns a list of all CRM Template code records 
     * based on the opportunity's stage and stage step, and
     * point of origin of the email
     * @params      stageName               The stage of the opportunity
     * @params      stateStep               The stage step of the opportunity
     * @params      pointOfOrigin           The selected point of origin
     * @return      referencedTemplates     The CRM Template code records that matches the criteria
     */
    private static List < CRM_Email_Template_Code__mdt > getEmailTemplateByStage(String stageName, String stageStep, String pointOfOrigin) {

        return [SELECT Id, Approver_ID__c, CC__c, Template_ID__c FROM CRM_Email_Template_Code__mdt WHERE Opportunity_Stage__c =: stageName AND Stage_Steps__c =: stageStep AND Point_of_Origin__c =: pointOfOrigin];

    }

    /**
     * V1.5 
     * Returns a list of attachments from
     * the most recent email message based
     * on the recipent and the parent record
     * @params      relatedToId         The parent record Id
     * @params      contactId           The recipient of the email
     * @return      attachments         The list of email attachments
     */
    public static List < Attachment > getEmailAttachmentsByRecipient(String relatedToId, String contactId) {

        Task associatedTask = new Task();

        List < Attachment > attachments = new List < Attachment > ();
        
        //v1.11 - Commented the below line.
        //contactId = Test.isRunningTest() ? Test_cls_Crm_Utils.getTestContactId() : contactId;

        List < Task > relatedTasks = [SELECT Id, WhoId, WhatId FROM Task WHERE WhatId =: relatedToId AND WhoId =: contactId ORDER BY CreatedDate DESC];

        for (Task t: relatedTasks) {

            if (String.isNotBlank(t.whoId) &&
                String.isNotBlank(t.WhatId) &&
                t.whoId.equals(contactId) &&
                t.whatId.equals(relatedToId)) {

                associatedTask = t;
                break;
            }

        }

        if (String.isNotBlank(associatedTask.Id)) {

            Set < String > attachmentIdSet = new Set < String > ();

            for (EmailMessage emailMessage: Test.isRunningTest() ?  [SELECT Id, ActivityId, RelatedToId, Subject, FromAddress, FromName, HasAttachment, CcAddress,
                                                                        (SELECT Id FROM Attachments)
                                                                     FROM EmailMessage 
                                                                     WHERE RelatedToId =: relatedToId
                                                                     AND Id =: Test_cls_Crm_Utils.getTestEmailMessageId()
                                                                     ORDER BY CreatedDate DESC ] :
                                                                    [SELECT Id, ActivityId, RelatedToId, Subject, FromAddress, FromName, HasAttachment, CcAddress,
                                                                        (SELECT Id FROM Attachments)
                                                                     FROM EmailMessage 
                                                                     WHERE RelatedToId =: relatedToId
                                                                     AND ActivityId =: associatedTask.Id
                                                                     ORDER BY CreatedDate DESC ]) 
            {

                if (String.isNotBlank(emailMessage.ActivityId) && !emailMessage.Attachments.isEmpty()) {

                    for (Attachment a: emailMessage.Attachments) attachmentIdSet.add(a.Id);

                }
            }

            if (!attachmentIdSet.isEmpty()) {

                for (Attachment a: [SELECT Id, Name, Body, ContentType FROM Attachment WHERE Id in: attachmentIdSet]) {
                    attachments.add( new Attachment( ContentType = a.ContentType, Name = a.Name, Body = a.Body ) );
                }
            }
        }
        return attachments;
    }

    /**
     * v1.3
     * Gets all the mass email activities 
     * created during a mail merge 
     * @return      massMailActivities      List of mail merge activities
     */
    public static List < Task > getMassEmailActivities() {

        return [SELECT Id, WhoId, Subject, Description FROM Task WHERE CreatedDate = TODAY AND RecordType.DeveloperName = 'Email'
            AND Subject LIKE '%Mass Email%'
        ];
    }

    /**
     * v1.3
     * Sets the body of the activities
     * based on the email template they 
     * are referencing
     * @params      leadTasks       The mail merge activities
     */
    public static void setLeadEmailContent(List < Task > leadTasks) {

        List < Task > tasksToUpdate = new List < Task > ();

        for (Task t: leadTasks) {

            if (t.whoId.getSobjectType() == Schema.Lead.SObjectType &&
                String.isNotBlank(t.Subject) && String.isBlank(t.Description)) {

                tasksToUpdate.add(t);

            }

        }

        if (!tasksToUpdate.isEmpty()) {

            Set < String > emailTemplateDescSet = new Set < String > ();

            for (Task t: tasksToUpdate) {

                emailTemplateDescSet.add(t.Subject.remove('Mass Email: '));

            }

            Map < String, String > emailTemplateMap = new Map < String, String > ();

            System.debug('emailTemplateDescSet===>' + emailTemplateDescSet);
            // Query the email tempaltes 
            List < EmailTemplate > templateId = [select Id, Description from EmailTemplate where Description IN: emailTemplateDescSet];

            System.debug('templateId===>' + templateId);
            // Create a map of the email templates 
            for (EmailTemplate emailTemplate: templateId) {
                emailTemplateMap.put(emailTemplate.Description, emailTemplate.Id);
            }

            for (Task t: tasksToUpdate) {

                if (emailTemplateMap.containsKey(t.Subject.remove('Mass Email: '))) {

                    t.Description = Messaging.renderStoredEmailTemplate(emailTemplateMap.get(t.Subject.remove('Mass Email: ')),
                        t.whoId,
                        null).getPlainTextBody();

                }
            }

            update tasksToUpdate;
            System.debug('tasksToUpdate===>' + tasksToUpdate);
        }

    }

    //V1.7
    //Created to Set Account and opp values 

    public static void OppAccountDFSAEmail(Opportunity relatedOpps, Account relatedAcc, string accountStatus, date authorizationDate) {
		system.debug('========company Type======================'+relatedAcc.Company_Type__c);
		system.debug('====accountStatus================='+accountStatus);

        if (relatedAcc.Company_Type__c == 'Financial - related') {
            if (accountStatus.tolowercase().contains('application in-principle')) {
				system.debug('====11111=========princi========');

                relatedOpps.StageName = 'In Principle Issued by DFSA';
                relatedOpps.CloseDate = authorizationDate;
                relatedAcc.DFSA_License_Status__c = 'Pending';
                relatedAcc.DFSA_Lic_InPrAppDate__c = authorizationDate;
                relatedAcc.ROC_Status__c = 'Under Formation';  // V1.12

            } else if (accountStatus.tolowercase().contains('authorised') || accountStatus.tolowercase().contains('registered')) {
                //   
				system.debug('====11111=========authorised========');
                relatedOpps.StageName = 'DFSA License Issued';
                relatedOpps.CloseDate = authorizationDate;
                relatedAcc.DFSA_Lic_Appr_Date__c = authorizationDate;
                relatedAcc.DFSA_License_Status__c = 'Active';
                relatedAcc.DFSA_Approval__c = 'Yes';
                relatedAcc.DFSA_Withdrawn_Date__c = null;

            } else if (accountStatus.tolowercase().contains('voluntary withdrawal') || accountStatus.tolowercase().contains('involuntary withdrawal')) {
				system.debug('====11111=========voluntary withdrawal========');
                relatedOpps.StageName = 'DFSA License Withdrawn';
                relatedOpps.CloseDate = authorizationDate;
                relatedAcc.DFSA_Withdrawn_Date__c = authorizationDate;
                relatedAcc.DFSA_License_Status__c = 'Withdrawn';
                //relatedAcc.DFSA_Approval__c = 'No';

                if (relatedAcc.Company_Type__c == 'Financial - related') //v1.6 
                    relatedAcc.ROC_Status__c = 'Inactive - DFSA Lic. Wdn';

            } else if (accountStatus.tolowercase().contains('application withdrawn')) {
				system.debug('====11111=========application withdrawn=====');
                relatedOpps.StageName = 'Application Withdrawn';
                relatedOpps.CloseDate = authorizationDate;

            //v1.10 added this code to update the opp status and date,
            } else if (accountStatus.tolowercase().contains('application pending')) {
				system.debug('====11111=======application pending====');
                relatedOpps.StageName = 'Application Accepted by DFSA ';
                relatedOpps.CloseDate = authorizationDate;

            }

        } else {
			system.debug('===relatedAcc.DNFBP__c==='+relatedAcc.DNFBP__c);
            if(relatedAcc.DNFBP__c == 'Yes' ){
                if ( accountStatus.tolowercase().contains('voluntary withdrawal') || accountStatus.tolowercase().contains('involuntary withdrawal') ) {
                    //v1.10-2 added the below code for DFSA DNFBP
                    relatedOpps.StageName = 'DFSA DNFBP Withdrawn';
                    relatedOpps.CloseDate = authorizationDate;
                    //v1.10-2 Ends

                    relatedAcc.DFSA_Withdrawn_Date__c = authorizationDate;
                    relatedAcc.DNFBP__c = 'Withdrawn';
                    
                }else if ( accountStatus.tolowercase().contains('application in-principle') ) {
                    //v1.10 added the below code for DFSA DNFBP
                    relatedOpps.StageName = 'DFSA DNFBP In-Principle Received';
                    relatedOpps.CloseDate = authorizationDate;
                    //v1.10 Ends

                    relatedAcc.DFSA_Lic_InPrAppDate__c = authorizationDate;

                } else if ( accountStatus.tolowercase().contains('registered') ) {
                    //v1.10 added the below code for DFSA DNFBP
                    relatedOpps.StageName = 'DFSA DNFBP Registered';
                    relatedOpps.CloseDate = authorizationDate;
                    //v1.10 added
                    relatedAcc.DFSA_Approval__c = 'Yes';
                    relatedAcc.DFSA_Lic_Appr_Date__c = authorizationDate;
                }

            }else{
                if (accountStatus.tolowercase().contains('application in-principle')) {

                    // relatedOpps.StageName = 'In Principle Issued by DFSA';
                    //  relatedOpps.CloseDate = authorizationDate;
                    //relatedAcc.DFSA_License_Status__c = 'Pending';
                    relatedAcc.DFSA_Lic_InPrAppDate__c = authorizationDate;

                } else if (accountStatus.tolowercase().contains('authorised') || accountStatus.tolowercase().contains('registered')) {

                    //relatedOpps.StageName = 'DFSA License Issued';
                    //relatedOpps.CloseDate = authorizationDate;

                    relatedAcc.DFSA_Lic_Appr_Date__c = authorizationDate;
                    //relatedAcc.DFSA_License_Status__c = 'Active';
                    relatedAcc.DFSA_Approval__c = 'Yes';
                    //relatedAcc.DNFBP__c = 'Yes';

                } else if (accountStatus.tolowercase().contains('voluntary withdrawal') || accountStatus.tolowercase().contains('involuntary withdrawal')) {

                    // relatedOpps.StageName = 'DFSA License Withdrawn';
                    //relatedOpps.CloseDate = authorizationDate;
                    relatedAcc.DFSA_Withdrawn_Date__c = authorizationDate;
                    // relatedAcc.DFSA_License_Status__c = 'Withdrawn';
                    relatedAcc.DNFBP__c = 'Withdrawn';
                    //relatedAcc.DFSA_Approval__c = 'No';
                    //relatedAcc.DNFBP__c = 'No';
                }
            }
        }
		system.debug('===relatedAcc=='+relatedAcc);
		system.debug('===relatedOpps==='+relatedOpps);
        update relatedAcc;
        update relatedOpps;

    }


    /**
     * Parses incoming emails from DFSA
     * @params        email        The email
     * @params        envelope     The email envelope meta
     */
    public static void processDFSAEmail(Messaging.InboundEmail email, Messaging.Inboundenvelope envelope) {

        Map < String, Email_Regex__c > mapOfEmailRegex = Email_Regex__c.getAll();

        CRM_cls_Utils crmUtils = new CRM_cls_Utils();

        crmUtils.setLogType('DFSA Email Automation Error');
        system.debug('*******************email.plainTextBody**************'+email.plainTextBody);
        system.debug('*******************email.Subject**************'+email.subject);
        //if (!mapOfEmailRegex.isEmpty()) {

            //String emailBody = email.plainTextBody.normalizeSpace();
			String emailBody = email.subject.normalizeSpace();
			 
            try {

                if (String.isNotBlank(emailBody)) {

                    System.debug('Body : ' + emailBody);

                    // V1.0 - Claude - FOR DFSA CRM Emails

                    String accountName = '', accountStatus = '';
                    //v1.10 if the email doesn't have the date then use today's date
                    //priori to v1.10 the authorizationDate was null
                    Date authorizationDate = Date.TODAY();
					
					if (String.isNotBlank(email.plainTextBody.normalizeSpace())) {
						for (String k: mapOfEmailRegex.keySet()) {
							
							if (mapOfEmailRegex.get(k).REGEX_Group__c.equals('DFSA CRM')) {

								Matcher m = Pattern.compile(String.valueOf(mapOfEmailRegex.get(k).Regex_Expression__c)).matcher(email.plainTextBody.normalizeSpace());

								System.debug('THE REGEX >> ' + String.valueOf(mapOfEmailRegex.get(k).Regex_Expression__c));

								if (m.matches()) {

									if (k.equals('DFSA Account Name')) {
										accountName = String.valueOf(m.group(1));
									}

									if (k.equals('DFSA Date')) {
										authorizationDate = Date.parse(String.valueOf(m.group(1)));
									}

									if (k.equals('DFSA Status')) {
										accountStatus = String.valueOf(m.group(1));
									}

								} else {
									System.debug('Not a match.');
								}
							}

						}
					}
					
					List<String> splitOfSubjectList = emailbody.split('status has changed to');

					accountStatus = (splitOfSubjectList.size() >1) ? splitOfSubjectList[1]: '';
					string accountNameandBPNo = (splitOfSubjectList.size() >0) ? splitOfSubjectList[0]: '';
					system.debug('========accountNameandBPNo==========='+ accountNameandBPNo);
					
					List<String> splitOfNameList = accountNameandBPNo.split(' ');
					system.debug('========splitOfNameList==========='+ splitOfNameList);
					string bpNo = (splitOfSubjectList.size() >0) ? splitOfSubjectList[0]: '';

					accountNameandBPNo = accountNameandBPNo.removeEnd('(AF)');
					system.debug('======accountNameandBPNo========'+accountNameandBPNo);
					accountNameandBPNo = accountNameandBPNo.replace('(AF)', '');
					accountNameandBPNo = accountNameandBPNo.trim();
					accountName = '%' + accountNameandBPNo.subString(8).removeStart(' ').removeEnd(' ') + '%';

					system.debug('======accountName========'+accountName);
					system.debug('======authorizationDate========'+authorizationDate);

					
                    Boolean isValid = authorizationDate != null && String.isNotBlank(accountStatus) && String.isNotBlank(accountName);

                    if (isValid) {

                        /* Split the name and the contact's passport number */
                        List < String > accountNameSplit = accountName.split(' ');

                       // String passportNum = accountNameSplit[1];

                       // accountName = '%' + accountName.subString(8).removeStart(' ').removeEnd(' ') + '%';

                        //v1.10 added a new field in SOQL : DNFBP__c
                        List < Account > relatedAcc = [SELECT Id, Company_Type__c, DNFBP__c  FROM Account WHERE ROC_Status__c != null AND Name LIKE: accountName]; //v1.6 

                        System.debug('Account Name: ' + accountName);

                        if (!relatedAcc.isEmpty()) {

                            List < Opportunity > relatedOpps = [SELECT Id FROM Opportunity WHERE AccountId =: relatedAcc[0].Id order by createdDate ASC limit 1];

                            if (!relatedOpps.isEmpty()) {
                                //v1.7 Update Account and Opportunity values 
                                OppAccountDFSAEmail(relatedOpps[0], relatedAcc[0], accountStatus, authorizationDate);

                            } else {
                                crmUtils.setErrorMessage('No existing opportunity found for Account-' + accountName + 'with Id -' + relatedAcc[0].Id); // v1.2 - Claude
                            }
                        } else {
                            crmUtils.setErrorMessage('No existing account found with the name -' + accountName); // v1.2 - Claude
                        }

                    } else {
                        crmUtils.setErrorMessage('The email is using an invalid format.'); // v1.2 - Claude
                    }
                    // V1.0 - Claude - End
                }

            } catch (Exception e) {
                crmUtils.setErrorMessage('Error occurred at line: ' + e.getLineNumber() + ' Message: ' + e.getMessage());
            }

            // v1.2 - Claude - Start
            if (crmUtils.hasError()) {
                crmUtils.setErrorMessage(crmUtils.getErrorMessage() + '\n\n' + 'The email body: \n\n' + emailBody);
                crmUtils.logError();
            }
            // v1.2 - Claude - End
        //}
    }

}