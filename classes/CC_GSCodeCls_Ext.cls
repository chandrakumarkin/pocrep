/**********************************************************************************************************************
    test Class Name  :   test_CC_GSCodeCls_Ext
    Description :   This is an utility class for GS department 
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    02-01-2020  Arun          Update Related Status SR's Status on submition 
    V1.2    04-04-2021  Veera         Added queable method to get the refund amount on the HOD review Step
    
 ********************************************************************************************************/

public without sharing class CC_GSCodeCls_Ext {
    public static string CC_GSCodeCls_ExtMain(Step__c stp, string MethodName) {

        switch on MethodName {
            When 'MULTIPLE_RELATED_SR_SUBMIT' {
                return MultipleRelatedSRsubmit(stp);
            }
            When 'PSA_Termination_Objection_approved' {
                return PSA_Termination_Objection(stp);
            }
            When 'HodRejectionAction' {
                return HodRejectionAction(stp);
            }
            When 'HodPreCheckRefund' {
                return HodPreCheckRefund(stp);
            }
            when 'HodPreCheckRefundQueable' { //V1.2  
                return HodPreCheckRefundQueable(stp);
            }
           
            when 'GSNonSponCustomAttac'{
             
                return GSNonSponCustomAttacMethod(stp); 
            }
        }
        return 'Success';

    }


    /*
        Method to populate actual service if there is any difference
    */
    public static string HodPreCheckRefund(Step__c objStep) {
        try { if(!Test.IsRunningTest()) { RefundHelperCls.HodPreCheckRefundHelper(objStep.SR__c, objStep.Id, objStep.Step_Name__c); }} catch (Exception ex) { return ex.getMessage(); }
        return 'Success';
    }
    
    
    public static string GSNonSponCustomAttacMethod(Step__c objStep){
      
      try { if(!Test.IsRunningTest()) { 
     
          System.enqueueJob(new CustAttacforEmpnonSponsored_QC(objStep.SR__c));
         }
     } 
     catch (Exception ex) { return ex.getMessage(); 
     }
        return 'Success';
    }


    /*
        Method to populate actual service if there is any difference
    */
    public static string HodRejectionAction(Step__c objStep) {
        try {
            RefundHelperCls.HodRejectionAction(objStep.SR__c);
        } catch (Exception ex) {
            return ex.getMessage();
        }
        return 'Success';
    }
    
     /*
        V1.2  Method to populate actual service if there is any difference
    */
    
    public static string HodPreCheckRefundQueable(step__C objStep){
    
     try { if(!Test.IsRunningTest()) { 
     
          System.enqueueJob(new HodPreCheckRefund_QC (objStep.SR__c, objStep.Id, objStep.Step_Name__c));
      }
     } 
     catch (Exception ex) { return ex.getMessage(); 
     }
        return 'Success';
    
    }
    
    
      
        
    public static string PSA_Termination_Objection(Step__c stp) {
        map < string, id > mapOfStatus = new map < string, id > ();
        for (SR_Status__c obj: [select id, name from SR_Status__c where code__c != null and name = 'Cancelled']) mapOfStatus.put(obj.name, obj.id);

        for (Service_Request__c ObSr: [select id, Name, Linked_SR__c, First_Name__c, Last_Name__c, Relation__c, External_Status_Name__c, External_SR_Status__c, Internal_SR_Status__c from Service_Request__c where id =: stp.SR__c]) {
            service_Request__c LinkedSR = new service_Request__c(id = ObSr.Linked_SR__c);
            LinkedSR.Internal_SR_Status__c = mapOfStatus.get('Cancelled');
            LinkedSR.External_SR_Status__c = mapOfStatus.get('Cancelled');
            system.debug('##@@@##' + LinkedSR);
            update LinkedSR;
        }

        return 'Success';

    }


    //Change Main SR Status to Process Completed or 
    public static void MultipleSRProcessCompleted(id ChildSRId) {

        /*
            map<string,id> mapOfStatus = new map<string,id>();
            
               for(SR_Status__c obj : [select id,name from SR_Status__c where code__c!=null and name='Process Completed']) mapOfStatus.put(obj.name, obj.id);
               
            //Child SR Steps ID, get SR and and then Upload Parent Id
            Service_Request__c StepSR=[select Linked_SR__c,Completed_Date_Time__c,External_SR_Status__c,Internal_SR_Status__c,id from Service_Request__c where id=:ChildSRId];
            if(StepSR.Linked_SR__c!=null)
            {
                List<Service_Request__c> ListChildsOpenSRs=[select id from Service_Request__c where Linked_SR__c=:StepSR.Linked_SR__c and Completed_Date_Time__c=null];
                if(ListChildsOpenSRs.isEmpty())
                {
                    
                 Service_Request__c MainSr=new Service_Request__c(Id=StepSR.Linked_SR__c);MainSr.External_SR_Status__c= mapOfStatus.get('Process Completed');MainSr.Internal_SR_Status__c= mapOfStatus.get('Process Completed');MainSr.Completed_Date_Time__c=Datetime.Now();
                 Update MainSr;
                
                }
                
                
            }
            
            */
    }
    //SR__c
    public static string MultipleRelatedSRsubmit(Step__c stp) {

        map < string, id > mapOfStatus = new map < string, id > ();
        for (SR_Status__c obj: [select id, name from SR_Status__c where code__c != null and name = 'Submitted']) mapOfStatus.put(obj.name, obj.id);

        // Service_Request__c SRData =[select id,External_Status_Name__c,Submitted_Date__c,External_SR_Status__c,Internal_SR_Status__c,Submitted_DateTime__c from Service_Request__c where id=:stp.SR__c];
        //Change Linked SR status to Submitted from Draft 
        List < Service_Request__c > ListSer = new List < Service_Request__c > ();

        for (Service_Request__c ObSr: [select id, Name, Linked_SR__c, First_Name__c, Last_Name__c, Relation__c, External_Status_Name__c, External_SR_Status__c, Internal_SR_Status__c from Service_Request__c where Linked_SR__c != null and Linked_SR__c =: stp.SR__c]) {
            //System.debug('SRData.External_Status_Name__c==>'+SRData.External_Status_Name__c);
            //System.debug('ObSr.External_Status_Name__c==>'+ObSr.External_Status_Name__c);
            // system.assertequals(null,ObSr.Id+'==ObjSR linke'+ObSr.Linked_SR__c+'..'+SRData.External_SR_Status__c+'=='+ObSr.External_SR_Status__c);

            // if(SRData.External_Status_Name__c=='Submitted' && ObSr.External_Status_Name__c=='Draft')
            if (ObSr.External_Status_Name__c == 'Draft') {
                ObSr.Internal_SR_Status__c = mapOfStatus.get('Submitted');
                ObSr.External_SR_Status__c = mapOfStatus.get('Submitted');
                ObSr.Submitted_Date__c = date.today();
                ObSr.Submitted_DateTime__c = datetime.now();
                ListSer.add(ObSr);

            }

        }
        System.debug('======ListSer===>' + ListSer);
        if (!ListSer.isEmpty() && Test.isRunningTest() == false) {
            update ListSer;
            MultipleRelatedSRsubmitPrices(stp.SR__c);


        }




        return 'Success';
    }
    public static void MultipleRelatedSRsubmitPrices(Id MainSRId) {

        //Change Change price line iteam price status to cancelled and Blocked for related SR;s 
        List < id > ListLinkedPrices = new List < id > ();
        List < SR_Price_Item__c > ListUpdatePrice = new List < SR_Price_Item__c > ();

        for (SR_Price_Item__c ObjPrice: [select Linked_SR_Price_Item__c, id from SR_Price_Item__c where ServiceRequest__c =: MainSRId and Status__c = 'Blocked']) {
            ObjPrice.Status__c = 'Cancelled';
            ListLinkedPrices.add(ObjPrice.Linked_SR_Price_Item__c);
            ListUpdatePrice.add(ObjPrice);
        }


        for (SR_Price_Item__c ObjPrice: [select id, Price__c, ServiceRequest__c, Product__c, Status__c, Pricing_Line__c from SR_Price_Item__c where Status__c = 'Added'
                and ServiceRequest__r.Linked_SR__c =: MainSRId and id in: ListLinkedPrices
            ]) {
            ObjPrice.Status__c = 'Blocked';
            ListUpdatePrice.add(ObjPrice);

        }
        if (!ListUpdatePrice.isEmpty()) update ListUpdatePrice;
        // system.assertequals(ListUpdatePrice.size(),100);
        // System.debug('======ListUpdatePrice===>'+ListUpdatePrice);

    }


}