@isTest(seealldata=false)
private class EconomySubstanceClassTest {

    public static Service_Request__c objSR;
    public static Economy_Substance__c ecoSubStance;
    public static List <Economy_Activities__c> ecActivitiesList;
    public static Account objAccount;
    static void prepareData(){
        objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Registration_License_No__c = '832018';
        objAccount.Company_Code__c = 'test';
        objAccount.ROC_Status__c = 'Active';
        objAccount.FinTech_Classification__c='FinTech';
        objAccount.Legal_Type_of_Entity__c ='LLP';
        objAccount.ROC_reg_incorp_Date__c =system.today().addmonths(-4);
        objAccount.Financial_Year_End__c ='31st December';
        insert objAccount;
        
        Account eachAccount2 =  new Account();
        eachAccount2 = new Account();
        eachAccount2.Name = 'Body Corporate';
        eachAccount2.Trade_Name__c = 'Test Trade';
        eachAccount2.RORP_License_No__c = '8877709';
        eachAccount2.Registered_Address__c = 'Dubai';
        eachAccount2.Building_Name__c = 'DIFC';
        eachAccount2.Hide_the_Company__c = false;
        eachAccount2.ROC_Status__c = 'Active';
        eachAccount2.Registration_License_No__c = '23349';
        eachAccount2.Company_Type__c ='Retail';
        eachAccount2.Company_Type__c ='Retail';
        eachAccount2.Description ='test description';
        eachAccount2.Sector_Classification__c ='Business Services';
        eachAccount2.Street__c = 'test';
        eachAccount2.City__c  ='test City';
        eachAccount2.Country__c = 'United Arab Emirates';
        eachAccount2.Postal_Code__c = '1235';
        eachAccount2.Phone = '+971512345678';
        eachAccount2.Website = 'www.assdd.com';
        eachAccount2.Company_Type__c ='Retail';
        eachAccount2.Sub_Legal_Type_of_Entity__c=null;
        insert eachAccount2;
        
        Relationship__c relBus = new Relationship__c();
        relBus.Object_Account__c = eachAccount2.id;
        relBus.Subject_Account__c =  objAccount.Id;
        //relBus.Relationship_Type__c = 'Beneficiary Owner';
        relBus.Relationship_Type__c ='Is Shareholder Of';
        relBus.Active__c= true;
        insert relBus;
        
        set<string> LAM= new set<string>{'Investment in Real Estate','Real Estate Services','Auditing of Accounts','Accepting Deposits','Managing Office','Software House','Holding Company','Effecting Contracts Of Insurance','Managing a collective Investment Fund'};
        List<License_Activity_Master__c> lstLAM = new List<License_Activity_Master__c>();
        for(string s:LAM){
            License_Activity_Master__c master = new License_Activity_Master__c();
            master.Name = 'ActivityMaster';
            master.Type__c = 'License Activity';
            lstLAM.add(master);
        }
        
        INSERT lstLAM;
        
        List<License_Activity__c> liceActLst = new List<License_Activity__c>();
        for(License_Activity_Master__c itr:lstLAM){
            License_Activity__c license = new License_Activity__c();
            license.Account__c = objAccount.ID;
            license.End_Date__c = null;
            license.Activity__c = itr.ID;
            liceActLst.add(license);
        }
        INSERT liceActLst;
        
        ID SRrectypeid = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('EconomySubstance').getRecordTypeId();
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.First_Name__c='Test';
        objSR.Last_Name__c='ss';
        objSR.Mobile_Number__c = '+919999999999';
        objSR.Nationality_list__c ='India';
        objSR.Email_Address__c = 'ss@gmail.com';
        objSR.RecordTypeId = SRrectypeid;
        insert objSR;
        
        ecoSubStance = new Economy_Substance__c();
        ecoSubStance.ShareCapitalOfEntity__c='Yes';
        ecoSubStance.GovernmentEntity__c='DFSA';
        ecoSubStance.DecreeNumberEstablishingTheEntity__c ='4';
        ecoSubStance.FinancialYearEnd__c='December 03';
        ecoSubStance.StartDate__c =system.today();
        ecoSubStance.EndDate__c =system.today().addmonths(-2);
        ecoSubStance.EntityConduct__c='Yes';
        ecoSubStance.ReconfirmHoldingCompany__c='Yes';
        ecoSubStance.IncomeDerivedSolelyFormDividends__c='Yes';
        ecoSubStance.ReconfirmHeadquartersBusiness__c='Yes';
        ecoSubStance.ReconfirmIntellectualProperty__c='Yes';
        ecoSubStance.BankingBusiness__c='Yes';
        ecoSubStance.InsuranceBusiness__c='Yes';
        ecoSubStance.ResearchandDevelopment__c='Yes';
        ecoSubStance.InvestmentFundBusiness__c='Yes';
        ecoSubStance.LeaseBusiness__c='Yes';
        ecoSubStance.EntitycreatetheIntellectualProperty__c='Yes';
        ecoSubStance.EntityAcquireTheIntellectualProperty__c='Yes';
        ecoSubStance.EntitylicensetheIP__c='Yes';
        ecoSubStance.Ultimateparentcompany__c='Yes';
        ecoSubStance.UltimateParentEntityName__c='DFSA';
        ecoSubStance.UltimateEntityRegistration__c='2322';
        ecoSubStance.UltimateParentJurisdiction__c='United Arab Emirates';
        ecoSubStance.ParentEntity__c='Body Corporate';
        ecoSubStance.IsParentEntity__c='Yes';
        ecoSubStance.ForeignEntityRegisteredinDIFC__c='Yes';
        ecoSubStance.ParentEntityName__c='test';
        ecoSubStance.Registration__c='32432432';
        ecoSubStance.ParentEntityJurisdiction__c='United Arab Emirates';
        ecoSubStance.Do_you_have_an_ultimate_beneficial_owner__c='Yes';
        ecoSubStance.EntityTaxResidentOutside__c='Yes';
        ecoSubStance.Areyouwhollyownedbymore__c='Yes';
        ecoSubStance.Declaration__c=true;
        ecoSubStance.Service_Request__c=objSR.Id;
        insert ecoSubStance;
        
        List<Economy_Substance_Activities__c> ecSetList = new List<Economy_Substance_Activities__c>();
        Economy_Substance_Activities__c ecCusSet1  = new Economy_Substance_Activities__c();
        ecCusSet1.Relevant_Activities__c = 'Banking Business';
        ecCusSet1.Name = 'Banking';
        ecSetList.add(ecCusSet1);
        Economy_Substance_Activities__c ecCusSet2  = new Economy_Substance_Activities__c();
        ecCusSet2.Relevant_Activities__c = 'Insurance Business';
        ecCusSet2.Name = 'Insurance';
        ecSetList.add(ecCusSet2);
        Economy_Substance_Activities__c ecCusSet3  = new Economy_Substance_Activities__c();
        ecCusSet3.Relevant_Activities__c = 'Fund Management Business';
        ecCusSet3.Name = 'Fund';
        ecSetList.add(ecCusSet3);
        Economy_Substance_Activities__c ecCusSet4  = new Economy_Substance_Activities__c();
        ecCusSet4.Relevant_Activities__c = 'Lease Finance Business';
        ecCusSet4.Name = 'Lease';
        ecSetList.add(ecCusSet4);
        Economy_Substance_Activities__c ecCusSet5  = new Economy_Substance_Activities__c();
        ecCusSet5.Relevant_Activities__c = 'Holding Company Business';
        ecCusSet5.Name = 'Holding';
        ecSetList.add(ecCusSet5);
        Economy_Substance_Activities__c ecCusSet6  = new Economy_Substance_Activities__c();
        ecCusSet6.Relevant_Activities__c = 'Headquarters Business';
        ecCusSet6.Name = 'Headquarters';
        ecSetList.add(ecCusSet6);
        Economy_Substance_Activities__c ecCusSet7  = new Economy_Substance_Activities__c();
        ecCusSet7.Relevant_Activities__c = 'Distribution and Service Centre Business';
        ecCusSet7.Name = 'Distribution';
        ecSetList.add(ecCusSet7);
        Economy_Substance_Activities__c ecCusSet8  = new Economy_Substance_Activities__c();
        ecCusSet8.Relevant_Activities__c = 'Intellectual Property Business';
        ecCusSet8.Name = 'Intellectual Property Business';
        ecSetList.add(ecCusSet8);
        
        insert ecSetList;
        
         ecActivitiesList = new List <Economy_Activities__c>();
         for(Economy_Substance_Activities__c itr:ecSetList){
            Economy_Activities__c eA= new Economy_Activities__c();
            eA.is_Relevant__c = true;
            eA.Economy_Substance__c =ecoSubStance.Id;
            eA.Is_tax_payable__c= 'No';
            eA.Relevant_Activities__c = itr.Relevant_Activities__c;
            eA.Service_Request__c=objSR.Id;
            //eA.Was_income_earned_in_UAE__c
            //eA.where_tax_is_payable__c
            ecActivitiesList.add(eA);
         }
        insert ecActivitiesList;
        
        UBO_Data__c uboData = new UBO_Data__c();
        uboData.Entity_Name__c  = 'Test';
        uboData.Status__c = 'Draft';
        uboData.Service_Request__c = objSR.Id;
      //  uboData.Relationship__c = relBus.Id;
        uboData.RecordtypeID = Schema.SObjectType.UBO_Data__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        uboData.BC_UBO__c = objAccount.Id;
        uboData.Country__c = 'India';
        uboData.Post_Code__c ='500081';
        uboData.City_Town__c = 'Hyderabad';
        uboData.PO_Box__c = '0000';
        uboData.Street_Address__c = 'Hitechcity';
        uboData.Building_Name__c = 'Sai';
        uboData.Phone_Number__c = '+9719988776655';
        INSERT uboData;
        
    }
    static testMethod void testmethod1(){
        prepareData();
        PageReference pageRef = Page.EconomySubstance;
        Test.setCurrentPage(pageRef);
        system.currentPageReference().getParameters().put('Id',null);
        Apexpages.currentPage().getParameters().put('isDeclaration','false');
        Apexpages.currentPage().getParameters().put('isdetail','false');
        EconomySubstanceClass obj = new EconomySubstanceClass();
        obj.callAllDefaultVal();
        obj.editConfirmation();
        obj.addEcoSub(objSR.Id);
        obj.changeEntityType();
        obj.activitesUpdate();
        obj.outSideUAE();
        obj.closePopup();
        obj.checkHighRiskIP();
        obj.Ultimateparentcompany();
        obj.isParentEntityCheck();
        EconomySubstanceClass.StringToDateFormat('12/06/2019');
        obj.EntityConduct();
        obj.checkRlevantActivityDetails(); 
        obj.calculateFinancialYearEnd();
        obj.SaveConfirmation();
        obj.SaveDeclaration();
        obj.AddUBORecord();
        Apexpages.currentPage().getParameters().put('index','0');
        //obj.removingUBORow();
        obj.SaveUBORow();
        obj.DynamicButtonAction();
        obj.CancelSaveRow();
        obj.CancelEco();
        //getbcNameList();
    }
   
    static testMethod void testmethod2(){
        prepareData();
        
        ecoSubStance.ShareCapitalOfEntity__c=null;
        ecoSubStance.GovernmentEntity__c=null;
        ecoSubStance.DecreeNumberEstablishingTheEntity__c=null;
        ecoSubStance.FinancialYearEnd__c=null;
        ecoSubStance.StartDate__c=null;
        ecoSubStance.EndDate__c =null;
        ecoSubStance.EntityConduct__c='No';
        ecoSubStance.ReconfirmHoldingCompany__c=null;
        ecoSubStance.IncomeDerivedSolelyFormDividends__c=null;
        ecoSubStance.ReconfirmHeadquartersBusiness__c=null;
        ecoSubStance.ReconfirmIntellectualProperty__c=null;
        ecoSubStance.BankingBusiness__c=null;
        ecoSubStance.InsuranceBusiness__c=null;
        ecoSubStance.ResearchandDevelopment__c=null;
        ecoSubStance.InvestmentFundBusiness__c=null;
        ecoSubStance.LeaseBusiness__c=null;
        ecoSubStance.EntitycreatetheIntellectualProperty__c=null;
        ecoSubStance.EntityAcquireTheIntellectualProperty__c='No';
        ecoSubStance.EntitylicensetheIP__c='No';
        ecoSubStance.Ultimateparentcompany__c='No';
        ecoSubStance.UltimateParentEntityName__c=null;
        ecoSubStance.UltimateEntityRegistration__c=null;
        ecoSubStance.UltimateParentJurisdiction__c=null;
        ecoSubStance.ParentEntity__c=null;
        ecoSubStance.IsParentEntity__c=null;
        ecoSubStance.ForeignEntityRegisteredinDIFC__c='No';
        ecoSubStance.ParentEntityName__c=null;
        ecoSubStance.Registration__c=null;
        ecoSubStance.ParentEntityJurisdiction__c=null;
        ecoSubStance.Do_you_have_an_ultimate_beneficial_owner__c=null;
        ecoSubStance.EntityTaxResidentOutside__c=null;
        ecoSubStance.Areyouwhollyownedbymore__c=null;
        ecoSubStance.Declaration__c=false;
        update ecoSubStance;
        
        objAccount.ROC_reg_incorp_Date__c =Date.newInstance(2019,05,20);
        objAccount.Legal_Type_of_Entity__c='LLP';
        update objAccount;
        
        PageReference pageRef = Page.EconomySubstance;
        Test.setCurrentPage(pageRef);
        system.currentPageReference().getParameters().put('Id',objSR.Id);
        //system.assertEquals(null,objSR.Id);
        pageRef.getParameters().put('isdetail','false');
        pageRef.getParameters().put('isDeclaration','false');
        EconomySubstanceClass obj = new EconomySubstanceClass();
        obj.srId = objSR.Id;
        ecoSubStance.EntitylicensetheIP__c = 'No';
        obj.callAllDefaultVal();
        //obj.editConfirmation();
        obj.addEcoSub(objSR.Id);
        obj.changeEntityType();
        obj.activitesUpdate();
        obj.outSideUAE();
        obj.closePopup();
        obj.checkHighRiskIP();
        obj.Ultimateparentcompany();
        obj.isParentEntityCheck();
        obj.ResearchAndDevelopment();
        obj.IPtoConnectedPerson();
        EconomySubstanceClass.StringToDateFormat('12/06/2019');
        obj.EntityConduct();
        obj.checkRlevantActivityDetails(); 
        obj.calculateFinancialYearEnd();
        obj.AddUBORecord();
        //obj.removingUBORow();
        Apexpages.currentPage().getParameters().put('index','true');
        obj.SaveUBORow();
        obj.SaveConfirmation();
        obj.SaveDeclaration();
        obj.DynamicButtonAction();
        obj.CancelSaveRow();
         obj.CancelEco();
        //getbcNameList();
        
        //EconomySubstanceClass.dummytest();
    }
  
  static testMethod void testmethod3(){
        prepareData();
        
        ecoSubStance.ShareCapitalOfEntity__c=null;
        ecoSubStance.GovernmentEntity__c=null;
        ecoSubStance.DecreeNumberEstablishingTheEntity__c=null;
        ecoSubStance.FinancialYearEnd__c=null;
        ecoSubStance.StartDate__c=null;
        ecoSubStance.EndDate__c =null;
        ecoSubStance.EntityConduct__c='Yes';
        ecoSubStance.ReconfirmHoldingCompany__c=null;
        ecoSubStance.IncomeDerivedSolelyFormDividends__c=null;
        ecoSubStance.ReconfirmHeadquartersBusiness__c=null;
        ecoSubStance.ReconfirmIntellectualProperty__c=null;
        ecoSubStance.BankingBusiness__c=null;
        ecoSubStance.InsuranceBusiness__c=null;
        ecoSubStance.ResearchandDevelopment__c=null;
        ecoSubStance.InvestmentFundBusiness__c=null;
        ecoSubStance.LeaseBusiness__c=null;
        ecoSubStance.EntitycreatetheIntellectualProperty__c=null;
        ecoSubStance.EntityAcquireTheIntellectualProperty__c='No';
        ecoSubStance.EntitylicensetheIP__c='No';
        ecoSubStance.Ultimateparentcompany__c='No';
        ecoSubStance.UltimateParentEntityName__c=null;
        ecoSubStance.UltimateEntityRegistration__c=null;
        ecoSubStance.UltimateParentJurisdiction__c=null;
        ecoSubStance.ParentEntity__c=null;
        ecoSubStance.IsParentEntity__c=null;
        ecoSubStance.ForeignEntityRegisteredinDIFC__c='No';
        ecoSubStance.ParentEntityName__c=null;
        ecoSubStance.Registration__c=null;
        ecoSubStance.ParentEntityJurisdiction__c=null;
        ecoSubStance.Do_you_have_an_ultimate_beneficial_owner__c=null;
        ecoSubStance.EntityTaxResidentOutside__c='No';
        ecoSubStance.Areyouwhollyownedbymore__c=null;
        ecoSubStance.Declaration__c=false;
        update ecoSubStance;
        
        List<Economy_Substance_Activities__c> escAct = [select id,Relevant_Activities__c from Economy_Substance_Activities__c ];
         for(Economy_Activities__c ea:ecActivitiesList){
            eA.is_Relevant__c = false;
         }
        update ecActivitiesList;
        
        objAccount.ROC_reg_incorp_Date__c =Date.newInstance(2018,11,27);
        update objAccount;
        
        PageReference pageRef = Page.EconomySubstance;
        Test.setCurrentPage(pageRef);
        system.currentPageReference().getParameters().put('Id',objSR.Id);
        //system.assertEquals(null,objSR.Id);
        pageRef.getParameters().put('isdetail','false');
        pageRef.getParameters().put('isDeclaration','false');
        EconomySubstanceClass obj = new EconomySubstanceClass();
        obj.srId = objSR.Id;
        ecoSubStance.EntitylicensetheIP__c = 'No';
        obj.callAllDefaultVal();
        //obj.editConfirmation();
        obj.addEcoSub(objSR.Id);
        obj.changeEntityType();
        obj.activitesUpdate();
        obj.outSideUAE();
        obj.closePopup();
        obj.checkHighRiskIP();
        obj.Ultimateparentcompany();
        obj.isParentEntityCheck();
        obj.ResearchAndDevelopment();
        ecoSubStance.EntityAcquireTheIntellectualProperty__c='No';
        obj.IPtoConnectedPerson();
        obj.foreignentityregistered();
        EconomySubstanceClass.StringToDateFormat('12/06/2019');
        ecoSubStance.EntitylicensetheIP__c='Yes';
        ecoSubStance.ForeignEntityRegisteredinDIFC__c='No';
        ecoSubStance.Ultimateparentcompany__c='No';
        obj.checkRlevantActivityDetails(); 
        obj.calculateFinancialYearEnd();
        obj.AddUBORecord();
        //obj.removingUBORow();
        Apexpages.currentPage().getParameters().put('index','true');
         obj.EntityConduct();
        obj.SaveUBORow();
        obj.SaveConfirmation();
        obj.SaveDeclaration();
        obj.DynamicButtonAction();
        obj.CancelSaveRow();
         obj.CancelEco();
        //getbcNameList();
        
        //EconomySubstanceClass.dummytest();
    }
    static testMethod void testmethod4(){
        prepareData();
        
        ecoSubStance.ShareCapitalOfEntity__c=null;
        ecoSubStance.GovernmentEntity__c=null;
        ecoSubStance.DecreeNumberEstablishingTheEntity__c=null;
        ecoSubStance.FinancialYearEnd__c=null;
        ecoSubStance.StartDate__c=null;
        ecoSubStance.EndDate__c =null;
        ecoSubStance.EntityConduct__c='Yes';
        ecoSubStance.ReconfirmHoldingCompany__c=null;
        ecoSubStance.IncomeDerivedSolelyFormDividends__c=null;
        ecoSubStance.ReconfirmHeadquartersBusiness__c='Yes';
        ecoSubStance.ReconfirmIntellectualProperty__c='Yes';
        ecoSubStance.BankingBusiness__c='Yes';
        ecoSubStance.InsuranceBusiness__c='Yes';
        ecoSubStance.ResearchandDevelopment__c='Yes';
        ecoSubStance.InvestmentFundBusiness__c='Yes';
        ecoSubStance.LeaseBusiness__c='Yes';
        ecoSubStance.EntitycreatetheIntellectualProperty__c='No';
        ecoSubStance.EntityAcquireTheIntellectualProperty__c='Yes';
        ecoSubStance.EntitylicensetheIP__c='Yes';
        ecoSubStance.Ultimateparentcompany__c='Yes';
        ecoSubStance.UltimateParentEntityName__c=null;
        ecoSubStance.UltimateEntityRegistration__c=null;
        ecoSubStance.UltimateParentJurisdiction__c=null;
        ecoSubStance.ParentEntity__c=null;
        ecoSubStance.IsParentEntity__c=null;
        ecoSubStance.ForeignEntityRegisteredinDIFC__c='Yes';
        ecoSubStance.ParentEntityName__c=null;
        ecoSubStance.Registration__c=null;
        ecoSubStance.ParentEntityJurisdiction__c=null;
        ecoSubStance.Do_you_have_an_ultimate_beneficial_owner__c=null;
        ecoSubStance.EntityTaxResidentOutside__c='Yes';
        ecoSubStance.Areyouwhollyownedbymore__c=null;
        ecoSubStance.Declaration__c=true;
        update ecoSubStance;
        
        List<Economy_Substance_Activities__c> escAct = [select id,Relevant_Activities__c from Economy_Substance_Activities__c ];
         for(Economy_Activities__c ea:ecActivitiesList){
            eA.is_Relevant__c = true;
         }
        update ecActivitiesList;
        
        objAccount.ROC_reg_incorp_Date__c =Date.newInstance(2019,10,27);
        objAccount.Legal_Type_of_Entity__c='RLP';
        update objAccount;
        
        PageReference pageRef = Page.EconomySubstance;
        Test.setCurrentPage(pageRef);
        system.currentPageReference().getParameters().put('Id',objSR.Id);
        //system.assertEquals(null,objSR.Id);
        pageRef.getParameters().put('isdetail','false');
        pageRef.getParameters().put('isDeclaration','false');
        EconomySubstanceClass obj = new EconomySubstanceClass();
        obj.srId = objSR.Id;
        obj.SRData = objSR;
        obj.ecoSubStance = ecoSubStance;
        obj.callAllDefaultVal();
        //obj.editConfirmation();
        obj.addEcoSub(objSR.Id);
        obj.changeEntityType();
        obj.activitesUpdate();
        obj.outSideUAE();
        obj.closePopup();
        obj.checkHighRiskIP();
        obj.Ultimateparentcompany();
        obj.isParentEntityCheck();
        obj.ResearchAndDevelopment();
        ecoSubStance.EntityAcquireTheIntellectualProperty__c='No';
        obj.IPtoConnectedPerson();
        obj.foreignentityregistered();
        EconomySubstanceClass.StringToDateFormat('12/06/2019');
        obj.EntityConduct();
        ecoSubStance.EntitylicensetheIP__c='Yes';
        ecoSubStance.ForeignEntityRegisteredinDIFC__c='No';
        ecoSubStance.Ultimateparentcompany__c='No';
        obj.checkRlevantActivityDetails(); 
        obj.calculateFinancialYearEnd();
        obj.AddUBORecord();
        Apexpages.currentPage().getParameters().put('index','1');
        obj.SaveUBORow();
        obj.SaveConfirmation();
        obj.SaveDeclaration();
        obj.removingUBORow();
        obj.DynamicButtonAction();
        obj.CancelSaveRow();
        obj.CancelEco();
        obj.getbcNameList();
        //obj.isNext() = true;
        obj.DynamicButtonAction();
        
        //EconomySubstanceClass.dummytest();
    }
    static testMethod void testmethod5(){
        prepareData();

        List<Economy_Substance_Activities__c> escAct = [select id,Relevant_Activities__c from Economy_Substance_Activities__c ];
         for(Economy_Activities__c ea:ecActivitiesList){
            eA.is_Relevant__c = false;
         }
        update ecActivitiesList;
        
        objAccount.ROC_reg_incorp_Date__c =Date.newInstance(2019,10,27);
        objAccount.Legal_Type_of_Entity__c='RLP';
        update objAccount;
        
        PageReference pageRef = Page.EconomySubstance;
        Test.setCurrentPage(pageRef);
        system.currentPageReference().getParameters().put('Id',objSR.Id);
        //system.assertEquals(null,objSR.Id);
        pageRef.getParameters().put('isdetail','false');
        pageRef.getParameters().put('isDeclaration','false');
        EconomySubstanceClass obj = new EconomySubstanceClass();
        obj.srId = objSR.Id;
        obj.SRData = objSR;
        obj.ecoSubStance = ecoSubStance;
        obj.callAllDefaultVal();
        //obj.editConfirmation();
        obj.addEcoSub(objSR.Id);
        obj.changeEntityType();
        obj.activitesUpdate();
        obj.outSideUAE();
        obj.closePopup();
        obj.checkHighRiskIP();
        obj.Ultimateparentcompany();
        obj.isParentEntityCheck();
        obj.ResearchAndDevelopment();
        ecoSubStance.EntityAcquireTheIntellectualProperty__c='No';
        obj.IPtoConnectedPerson();
        obj.foreignentityregistered();
        EconomySubstanceClass.StringToDateFormat('12/06/2019');
        obj.EntityConduct();
        ecoSubStance.EntitylicensetheIP__c='Yes';
        ecoSubStance.ForeignEntityRegisteredinDIFC__c='No';
        ecoSubStance.Ultimateparentcompany__c='No';
        obj.checkRlevantActivityDetails(); 
        obj.calculateFinancialYearEnd();
        obj.AddUBORecord();
        Apexpages.currentPage().getParameters().put('index','1');
        obj.SaveUBORow();
        obj.SaveConfirmation();
        obj.SaveDeclaration();
        obj.removingUBORow();
        obj.DynamicButtonAction();
        obj.CancelSaveRow();
        obj.CancelEco();
        obj.getbcNameList();
        //obj.isNext() = true;
        obj.DynamicButtonAction();
        
        //EconomySubstanceClass.dummytest();
    }
    static testMethod void testmethod6(){
        prepareData();
        
        objAccount.ROC_reg_incorp_Date__c =Date.newInstance(2019,10,27);
        objAccount.Legal_Type_of_Entity__c='RLP';
        update objAccount;
        
        ecoSubStance.ShareCapitalOfEntity__c='Yes';
        ecoSubStance.GovernmentEntity__c='DFSA';
        ecoSubStance.DecreeNumberEstablishingTheEntity__c='433';
        ecoSubStance.FinancialYearEnd__c='31st January';
        ecoSubStance.StartDate__c=Date.newInstance(2019,10,27);
        ecoSubStance.EndDate__c =Date.newInstance(2020,01,31);
        ecoSubStance.EntityConduct__c='No';
        ecoSubStance.ReconfirmHoldingCompany__c='No';
        ecoSubStance.IncomeDerivedSolelyFormDividends__c='No';
        ecoSubStance.ReconfirmHeadquartersBusiness__c='No';
        ecoSubStance.ReconfirmIntellectualProperty__c='No';
        ecoSubStance.BankingBusiness__c='No';
        ecoSubStance.InsuranceBusiness__c='No';
        ecoSubStance.ResearchandDevelopment__c='No';
        ecoSubStance.InvestmentFundBusiness__c='No';
        ecoSubStance.LeaseBusiness__c='No';
        ecoSubStance.EntitycreatetheIntellectualProperty__c='No';
        ecoSubStance.EntityAcquireTheIntellectualProperty__c='No';
        ecoSubStance.EntitylicensetheIP__c='No';
        //ecoSubStance.Ultimateparentcompany__c='No';
        //ecoSubStance.UltimateParentEntityName__c='No';
        //ecoSubStance.UltimateEntityRegistration__c='No';
        //ecoSubStance.UltimateParentJurisdiction__c='No';
        ecoSubStance.ParentEntity__c=null;
        ecoSubStance.IsParentEntity__c=null;
        ecoSubStance.ForeignEntityRegisteredinDIFC__c='No';
        ecoSubStance.ParentEntityName__c=null;
        ecoSubStance.Registration__c=null;
        //ecoSubStance.ParentEntityJurisdiction__c=null;
        ecoSubStance.Do_you_have_an_ultimate_beneficial_owner__c=null;
        ecoSubStance.EntityTaxResidentOutside__c='No';
        ecoSubStance.Areyouwhollyownedbymore__c='No';
        ecoSubStance.Declaration__c=true;
        update ecoSubStance;
        
        List<Economy_Substance_Activities__c> escAct = [select id,Relevant_Activities__c from Economy_Substance_Activities__c ];
         for(Economy_Activities__c ea:ecActivitiesList){
            eA.is_Relevant__c = false;
         }
        update ecActivitiesList;
        
        
        
        PageReference pageRef = Page.EconomySubstance;
        Test.setCurrentPage(pageRef);
        system.currentPageReference().getParameters().put('Id',objSR.Id);
        //system.assertEquals(null,objSR.Id);
        pageRef.getParameters().put('isdetail','false');
        pageRef.getParameters().put('isDeclaration','false');
        EconomySubstanceClass obj = new EconomySubstanceClass();
        obj.SaveConfirmation();
        obj.SaveDeclaration();
        
    }
}