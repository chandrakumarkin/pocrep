@isTest
public class Common_Reporting_Standard_Test {

    public static testMethod void myTest()
    {
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.GIIN__c = 'Test Account';
        insert objAccount;
        
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.RF_Valid_To__c = system.today().addDays(10);
        objLease.RF_Valid_From__c = system.today();
        objLease.Lease_Types__c = 'Retail Lease';
        insert objLease;

        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.Menutext__c = 'New Employment Visa Package';
        testSrTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.SR_Group__c = 'GS';
        testSrTemplate.Active__c = true;
        insert testSrTemplate;
        
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = objAccount.Id);
        insert con; 

        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
               
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        //insert user;
        Map<String,Lease__c> leaseMap = new Map<String,Lease__c>();
        Lease__c leaseRec = [Select id,name,Account__c,Account__R.BP_No__c,Status__c,Start_date__c,End_Date__c,Type__c,RF_Valid_To__c,RF_Valid_From__c,Lease_Types__c,Consider_For_Monthly_Sales_Submission__c from Lease__c where id=:objLease.id];
        leaseMap.put(leaseRec.Name,leaseRec);
      
        String base64Data ='12345';
        
        Service_Request__c SR1 = new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('DIFC_Sponsorship_Visa_New');
        SR1.Customer__c = objAccount.Id;
        SR1.Mobile_Number__c ='+971500000000';
        SR1.Year__c = '2020';
        SR1.Month__c = 'AUGUST';
        SR1.Transaction_Amount__c =1000;
        SR1.Lease__c = leaseRec.id;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        
        insert objContact;        
        
            Test.startTest();
              
                Apexpages.currentpage().getParameters().put('index','0');
                ApexPages.StandardController SRStandardController = new ApexPages.StandardController(SR1);
                Common_Reporting_Standard commonReportingStandard = new Common_Reporting_Standard(SRStandardController);
        		commonReportingStandard.Summary='AddExisting';
        		commonReportingStandard.selectedVal=objContact.id;
                commonReportingStandard.getListofNewAmend();
                commonReportingStandard.AddAndRecord();
                commonReportingStandard.SaveRecordAndAdd();
                commonReportingStandard.validationRule();
                commonReportingStandard.SaveRecord();
                commonReportingStandard.CancelSaveRow();
                commonReportingStandard.EditUBORecord();
        		commonReportingStandard.AddMaker();
        		commonReportingStandard.AddChecker();
        		commonReportingStandard.addNewExisting();
        		commonReportingStandard.fillContactDetails();
        		String check=commonReportingStandard.CheckAtleastOneCM();
				List<SelectOption> tempOptions = commonReportingStandard.ContactOptions;
            Test.stopTest();
        
    }
    
    public static testMethod void myTestNew()
    {
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.RF_Valid_To__c = system.today().addDays(10);
        objLease.RF_Valid_From__c = system.today();
        objLease.Lease_Types__c = 'Retail Lease';
        insert objLease;

        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.Menutext__c = 'New Employment Visa Package';
        testSrTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.SR_Group__c = 'GS';
        testSrTemplate.Active__c = true;
        insert testSrTemplate;
        
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = objAccount.Id);
        insert con; 

        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
               
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        //insert user;
        Map<String,Lease__c> leaseMap = new Map<String,Lease__c>();
        Lease__c leaseRec = [Select id,name,Account__c,Account__R.BP_No__c,Status__c,Start_date__c,End_Date__c,Type__c,RF_Valid_To__c,RF_Valid_From__c,Lease_Types__c,Consider_For_Monthly_Sales_Submission__c from Lease__c where id=:objLease.id];
        leaseMap.put(leaseRec.Name,leaseRec);
        system.debug('LeaseMap---'+leaseMap);
        String base64Data ='12345';
        
        Service_Request__c SR1 = new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('DIFC_Sponsorship_Visa_New');
        SR1.Customer__c = objAccount.Id;
        SR1.Mobile_Number__c ='+971500000000';
        SR1.Year__c = '2020';
        SR1.Month__c = 'AUGUST';
        SR1.Transaction_Amount__c =1000;
        SR1.Lease__c = leaseRec.id;
        insert SR1;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        
        insert objContact;
        
        
        Test.startTest();
           
            Apexpages.currentpage().getParameters().put('index','0');
            ApexPages.StandardController SRStandardController = new ApexPages.StandardController(SR1);
            Common_Reporting_Standard commonReportingStandard = new Common_Reporting_Standard(SRStandardController);
        	commonReportingStandard.Summary='AddNew';
        	commonReportingStandard.selectedVal=objContact.id;
            commonReportingStandard.getListofNewAmend();
            commonReportingStandard.AddAndRecord();
            commonReportingStandard.SaveRecordAndAdd();
            commonReportingStandard.validationRule();
            commonReportingStandard.SaveRecord();
            commonReportingStandard.CancelSaveRow();
            commonReportingStandard.removingUBORow();
        Test.stopTest();
    }
}