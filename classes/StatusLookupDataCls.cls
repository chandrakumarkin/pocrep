/*
    Author      :   Durga
    Description :   This class will display the Transitions for the current step status and initiate to update the status on SR.
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date            Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    25-01-2015      Ravi            Added the logic to check the Protal Balance for Customer if the SR type is AOR or DP Amendments
    V1.2    29-03-2016      Claude          Added logic to change visiblity of rejection reason
    V1.3    17/05/2016      Ravi            Added the logic to check whether the RORP Data got deduplicated or not. as per # 2092
    v1.4    23-11-2016      Claude              Added exemption for Look and Feel, and Revision of Fit-out (#3688)
    v1.5    19-Feb-2018     Veera           Added loigc to popup message if any dependent visas alrady in awaiting original status for the same sponsor tkt # 4401.
    v1.6    29-mar-2018     Veera           Added logic as part backoffice automation project.
    v1.7    16-Jun-2019     Selva           Added check the TR review completion #6752
    v1.8    4th-Feb-2019    Mudasir         #8429 - Pop message appears at the time of change the status to “Awaiting Originals” 
    V1.9    29th-Mar-2021   Arun            #14174 Added to GDRFA allow use BPM setting yo control status  

*/
public without sharing class StatusLookupDataCls {
    public string strStepId{get;set;}
    public list<Status__c> lstStatus{get;set;}
    public list<TransitionWrapper> lstTrnsWrap{get;set;}
    public Step__c step;
    public boolean isApprovalStep {get;set;}
    public integer iListSize{get;set;}
    public boolean isFitOut {get; set;}
    
    //V1.2
    public Boolean hasRejectionReason               { get; set; }
    public String hasRejectionReasonVar             { get; set; }
    public string awaitingReupload {get;set;} //v1.5
    public string ReScheduled {get;set;}  // v1.6
    
    //added by Ravi on 18/03/2015 
    public Boolean isGsStep {get;set;}
    
    //V1.3
    public String AlertData {get;set;}
    
    //V1.4 - Claude
    private Boolean isReasonAvailable;
    public Boolean isTRCheck;
   
    
    public StatusLookupDataCls(){
      
      isReasonAvailable = true;
      
        strStepId = ApexPages.CurrentPage().getParameters().get('Id');
        lstTrnsWrap = new list<TransitionWrapper>();
        step = new Step__c();
        iListSize = 0;
        set<id> setValidSteps = new set<id>();
        boolean isPortalUser = false;
        isApprovalStep = false;
        isFitOut = false;
        for(User curUser:[select id,ProfileId,contact.role__c,Profile.UserLicenseId,Profile.UserLicense.Name,Profile.UserLicense.LicenseDefinitionKey from User where Id=:userInfo.getUserId()]){
            system.debug('curUser====>'+curUser.Profile.UserLicense.Name);
            if(curUser.Profile.UserLicense.Name=='Salesforce'){
                isPortalUser = false;
            }else{
                isPortalUser = true;
            }
            if(curUser.contact.role__c!=null && (curUser.contact.role__c == 'Fit-Out Services' || curUser.contact.role__c == 'Event Services')){
                isFitOut = true;    
            }
        }
        
        boolean SanctionStep = false;
        isTRCheck = false;
        list<Step__c> lstStep = [select id,SR_Record_Type_Text__c,SR__c,Step_Name__c,SR__r.SR_Template__c,Status__c,SR__r.Customer__c,SR__r.Record_Type_Name__c,SR__r.Summary__c,SR_Step__c,Rejection_Reason__c,Status__r.Name,Status__r.Code__c,SR__r.SR_Template__r.Menu__c,SAP_Seq__c,SR_Group__c from Step__c where Id=:strStepId and SR_Step__c!=null and SR__c!=null];
        system.debug('--lstStep --'+lstStep.size());
        //v1.7 start
        Map<string,TR_Check_Status__c>  trCheckStatus = TR_Check_Status__c.getAll();
        Boolean recordtypeMatched = false;
        Boolean istypetwo = false;
        
        if(lstStep!=null && lstStep.size()>0){
           
            if(lstStep[0].SR__r.SR_Template__c!=null || lstStep[0].Status__r.Code__c=='AWAITING_SANCTION_APPROVAL'){
                step = lstStep[0];
            }
            if(lstStep[0].Status__c!=null && lstStep[0].Status__r.Code__c=='AWAITING_SANCTION_APPROVAL'){
                SanctionStep = true;
            }
            if(lstStep[0].Step_Name__c == 'Issue Letter to Contractor' && lstStep[0].SR__r.Record_Type_Name__c=='Fit_Out_Service_Request'){
                isApprovalStep = true;
            }
            //V1.4 - Claude - Start
            isReasonAvailable = !lstStep[0].SR__r.Record_Type_Name__c.equals('Look_Feel_Approval_Request') && 
                  !lstStep[0].SR__r.Record_Type_Name__c.equals('Revision_of_Fit_Out_Request');
            //V1.4 - Claude - End
        
            for(TR_Check_Status__c tcs:trCheckStatus.values()){
                if(tcs.SR_Record_Type__c == lstStep[0].SR_Record_Type_Text__c && tcs.Type__c !='2'){
                    recordtypeMatched = true;
                }
                if(tcs.SR_Record_Type__c == lstStep[0].SR_Record_Type_Text__c && tcs.Type__c =='2'){
                    istypetwo = true; // #9733
                }
            }
        }
        if(lstStep.size()>0){
            List<Thompson_Reuters_Check__c> trclst = [Select t.Id,t.Step__c,t.Contact__c,TRCase_No__c,TR_Case_Status__c From Thompson_Reuters_Check__c t Where ServiceRequest__c =: lstStep[0].SR__r.Id];
             //Exclude TR check for the below SR on Update request
             Set<String> excludeTRSRRecordType = new Set<String>{'Allotment_of_Shares_Membership_Interest','Shareholder_holding_shares_on_trust','Sale_or_Transfer_of_Shares_or_Membership_Interest'};
             
            
            if(recordtypeMatched && !istypetwo){
                if(lstStep[0].SR__r.Record_Type_Name__c!='Change_Add_or_Remove_Beneficial_or_Ultimate_Owner' && !CC_CreateConditions.Check_IS_Removed_SR_Amendment(lstStep[0].SR__r.Id)){

                    if(trclst.size()<=0){
                        if(((CC_CreateConditions.Check_IS_Add_in_SR_Amendment(lstStep[0].SR__r.Id) && lstStep[0].Step_Name__c!='Verification of Application' ) || ( !excludeTRSRRecordType.contains(lstStep[0].SR__r.Record_Type_Name__c) && !CC_CreateConditions.Check_IS_Add_in_SR_Amendment(lstStep[0].SR__r.Id) && CC_CreateConditions.CheckChangedAmendment(lstStep[0].SR__r.Id))) && (lstStep[0].Step_Name__c=='Quick Review by Employee' || lstStep[0].Step_Name__c=='Verification of Application') && recordtypeMatched && !CC_CreateConditions.Check_IS_Removed_SR_Amendment(lstStep[0].SR__r.Id)){ //&& tcs.Step_Name__c == lstStep[0].Step_Name__c
                            AlertData = 'World Check is required to change the status to verified';
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                            isTRCheck = true;
                            //return;
                        }
                        else if(lstStep[0].Step_Name__c=='Verification of Application' && lstStep[0].SR__r.Record_Type_Name__c.equals('Shareholder_holding_shares_on_trust')){
                            AlertData = 'World Check is required to change the status to verified';
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                            isTRCheck = true;
                        }
                       /* else if(lstStep[0].Step_Name__c=='Verification of Application' && lstStep[0].SR__r.Record_Type_Name__c.equals('Shareholder_holding_shares_on_trust')){
                            AlertData = 'World Check is required to change the status to verified';
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                            isTRCheck = true;
                        }*/
                        // added if SR is remove request and added new member
                        else if(lstStep[0].Step_Name__c=='Quick Review by Employee' && CC_CreateConditions.Check_IS_Add_in_SR_Amendment(lstStep[0].SR__r.Id) && CC_CreateConditions.Check_IS_Removed_SR_Amendment(lstStep[0].SR__r.Id)){
                            AlertData = 'World Check is required to change the status to verified';
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                            isTRCheck = true;
                        }
                    }
                    if(trclst.size()>0){
                        for(Thompson_Reuters_Check__c trc:trclst){
                            if(lstStep[0].Step_Name__c=='AML Approval' && trc.TRCase_No__c!=null && trc.TR_Case_Status__c!= system.label.TR_Case_Status){
                                AlertData = 'Verified option will be available once TR check is completed';
                                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                                isTRCheck = true;
                                //return;
                            }
                        }
                    }
                                
                }
                if(lstStep[0].SR__r.Record_Type_Name__c.equals('Change_Add_or_Remove_Beneficial_or_Ultimate_Owner')){
                    if(trclst.size()<=0){//9445 update
                        if(CC_CreateConditions.CheckUBODataAdded(lstStep[0].SR__r.Id)){
                            if(lstStep[0].Step_Name__c=='Quick Review by Employee'){
                                AlertData = 'World Check is required to change the status to verified';
                                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                                isTRCheck = true;
                            }
                        }
                        if(!CC_CreateConditions.CheckUBODataAdded(lstStep[0].SR__r.Id)){
                            if(CreateConditionExtension.checkUBONationality(lstStep[0].SR__r.Id) || CreateConditionExtension.checkUBOFieldsUpdated(lstStep[0].SR__r.Id)){
                                if(lstStep[0].Step_Name__c=='Verification of Application'){
                                    AlertData = 'World Check is required to change the status to verified';
                                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                                    isTRCheck = true;
                                }
                            }
                        }
                    }
                    if(trclst.size()>0){
                        for(Thompson_Reuters_Check__c trc:trclst){
                            if(lstStep[0].Step_Name__c=='AML Approval' && trc.TRCase_No__c!=null && trc.TR_Case_Status__c!= system.label.TR_Case_Status){
                                AlertData = 'Verified option will be available once TR check is completed';
                                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                                isTRCheck = true;
                                //return;
                            }
                        }
                    }
                }
            }// 9733 start
            if(!recordtypeMatched && istypetwo){
                if((trclst.size()<=0) && !CC_CreateConditions.Check_IS_Removed_SR_Amendment(lstStep[0].SR__r.Id)){
                    if(CC_CreateConditions.Check_IS_Add_in_SR_Amendment(lstStep[0].SR__r.Id) && lstStep[0].Step_Name__c=='Verification of Application' ){
                        AlertData = 'World Check is required to change the status to verified';
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                        isTRCheck = true;
                    }
                    if(!CC_CreateConditions.Check_IS_Add_in_SR_Amendment(lstStep[0].SR__r.Id) && CC_CreateConditions.CheckChangedAmendment(lstStep[0].SR__r.Id) && lstStep[0].Step_Name__c=='Verification of Application' ){
                        AlertData = 'World Check is required to change the status to verified';
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                        isTRCheck = true;
                    }
                }
                if(trclst.size()>0){
                    for(Thompson_Reuters_Check__c trc:trclst){
                        if(lstStep[0].Step_Name__c=='AML Approval' && trc.TRCase_No__c!=null && trc.TR_Case_Status__c!= system.label.TR_Case_Status){
                            AlertData = 'Verified option will be available once TR check is completed';
                            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, AlertData);ApexPages.addMessage(msg);
                            isTRCheck = true;
                            //return;
                        }
                    }
                }
            }// 9733 end
        }
        //v1.7 end
        map<id,Step_Transition__c> mapStepTransition = new map<id,Step_Transition__c>();
        
       // v1.9 commented this code 
//        if(step!=null && step.Status__c!=null && ((step.SAP_Seq__c == null || step.SAP_Seq__c == '') && step.SAP_Seq__c != '0010') ){
        //V1.9 Added this condiation 
         if(step!=null && step.Status__c!=null && step.SR_Step__c!=null )
             {
        
            if(isPortalUser==false){
                system.debug('&&&&'+step.Status__r.Name + '!!@!!!'+step.SR_Step__c);
                for(Step_Transition__c trans:[select From__c,To__c,Transition__c,Evaluate_Refund__c,Transition__r.To__c,SR_Step__c,Parent_SR_Status__c,Parent_Step_Status__c,SR_Status_External__c,SR_Status_Internal__c from Step_Transition__c where Transition__c!=null and From__c=:step.Status__r.Name and SR_Step__c=:step.SR_Step__c]){
                    setValidSteps.add(trans.Transition__r.To__c);
                    mapStepTransition.put(trans.Transition__r.To__c,trans);
                }
            }else{
                for(Step_Transition__c trans:[select From__c,To__c,Transition__c,Evaluate_Refund__c,Transition__r.To__c,SR_Step__c,Parent_SR_Status__c,Parent_Step_Status__c,SR_Status_External__c,SR_Status_Internal__c from Step_Transition__c where Transition__c!=null and From__c=:step.Status__r.Name and SR_Step__c=:step.SR_Step__c and Display_on_Portal__c=true]){
                    setValidSteps.add(trans.Transition__r.To__c);
                    mapStepTransition.put(trans.Transition__r.To__c,trans);
                }
            }
        }
        if(setValidSteps!=null && setValidSteps.size()>0){
            TransitionWrapper objWrap;
            system.debug('----isTRCheck ---'+isTRCheck+'----'+setValidSteps);
            
            for(Status__c objstat:[Select id,Name,Type__c,Rejection__c,SR_Closed_Status__c,Code__c from Status__c where ID IN:setValidSteps]){
                system.debug('--objstat.Name--'+objstat.Name);
                if(isTRCheck && (objstat.Name!='Verified' && objstat.Name!='Approved')){
                    objWrap = new TransitionWrapper();
                    objWrap.objStatus = objstat;
                    objWrap.objSRStepTrans = new Step_Transition__c();
                    if(mapStepTransition.get(objstat.id)!=null){
                        objWrap.objSRStepTrans = mapStepTransition.get(objstat.id);
                    }
                    lstTrnsWrap.add(objWrap);
                }
                else if(!isTRCheck){
                    objWrap = new TransitionWrapper();
                    objWrap.objStatus = objstat;
                    objWrap.objSRStepTrans = new Step_Transition__c();
                    if(mapStepTransition.get(objstat.id)!=null){
                        objWrap.objSRStepTrans = mapStepTransition.get(objstat.id);
                    }
                    lstTrnsWrap.add(objWrap);
                }
            }
            iListSize = lstTrnsWrap.size();
            //lstStatus = [Select id,Name,Type__c,Rejection__c,SR_Closed_Status__c from Status__c where ID IN:setValidSteps];
        }
        isGsStep = false;
        if(lstTrnsWrap.isEmpty())
            GsStepProcess();
        system.debug('lstTrnsWrap===>'+lstTrnsWrap);
        
        //V1.3
        AlertData = '';
        if(step != null && step.SR_Group__c == 'RORP' && (step.SR__r.Record_Type_Name__c == 'Freehold_Transfer_Registration' || step.SR__r.Record_Type_Name__c == 'Lease_Registration' ) && step.Step_Name__c == 'Verification of Application'){
            DupCheckRORPData();
        }
        
        
    }
    
    public void GsStepProcess(){
        if(strStepId != null && strStepId != ''){
            isGsStep = false;
            string srStatus;
            for(Step__c obj : [select Id,SAP_Seq__c,Step_Template__r.SR_Status__c from Step__c where Id=:strStepId AND SAP_Seq__c != null AND Step_Status__c != 'Closed' AND Step_Status__c != 'Cancelled' AND Step_Status__c != 'Rejected' AND SAP_Seq__c != '0010']){
                isGsStep = true;
                srStatus = obj.Step_Template__r.SR_Status__c;
            }
            
            
            if(isGsStep){
                lstTrnsWrap = new list<TransitionWrapper>();
                //v1.6
                 list<Step__c> lstStep1 = [select id,SR__c,Step_Name__c,Step_Status__c,SR__r.SR_Template__c,Status__c,SR__r.Customer__c,SR__r.Record_Type_Name__c,SR__r.Summary__c,SR_Step__c,Rejection_Reason__c,Status__r.Name,Status__r.Code__c,SR__r.SR_Template__r.Menu__c,SAP_Seq__c,SR_Group__c from Step__c where Id=:strStepId ];
                if(lstStep1[0].Step_Name__c == 'Medical Fitness Test Scheduled'){
                  //veera
                   if(lstStep1[0].Step_Status__c == 'scheduled' || lstStep1[0].Step_Status__c == 'Re-Scheduled'){
                   for(Status__c objstat:[Select id,Name,Type__c,Rejection__c,SR_Closed_Status__c,Code__c from Status__c where Name =:'Re-Scheduled']){
                    TransitionWrapper objWrap = new TransitionWrapper();
                    objWrap.objStatus = objstat;
                    objWrap.objSRStepTrans = new Step_Transition__c(SR_Status_External__c=srStatus,SR_Status_Internal__c=srStatus);
                    lstTrnsWrap.add(objWrap);
                   }
                  }else {                  
                   for(Status__c objstat:[Select id,Name,Type__c,Rejection__c,SR_Closed_Status__c,Code__c from Status__c where Name =:'scheduled']){
                    TransitionWrapper objWrap = new TransitionWrapper();
                    objWrap.objStatus = objstat;
                    objWrap.objSRStepTrans = new Step_Transition__c(SR_Status_External__c=srStatus,SR_Status_Internal__c=srStatus);
                    lstTrnsWrap.add(objWrap);
                   }
                     
                  }     
                    
                }else{ 
                for(Status__c objstat:[Select id,Name,Type__c,Rejection__c,SR_Closed_Status__c,Code__c from Status__c where Name ='Closed']){
                    TransitionWrapper objWrap = new TransitionWrapper();
                    objWrap.objStatus = objstat;
                    objWrap.objSRStepTrans = new Step_Transition__c(SR_Status_External__c=srStatus,SR_Status_Internal__c=srStatus);
                    lstTrnsWrap.add(objWrap);
                }
             }  
            }//v1.6
        }
    }
    
    //V1.3
    public void DupCheckRORPData(){
        AlertData = '';
        list<Amendment__c> lstAmendments = new list<Amendment__c>();
        
        map<string,string> mapPassports = new map<string,string>();
        map<string,string> mapNations = new map<string,string>();
        map<string,string> mapIssuingAut = new map<string,string>();
        map<string,string> mapCLs = new map<string,string>();
        map<string,string> mapTypes = new map<string,string>();
        
        string sQry = 'select Id,Name,Amendment_Type__c,Record_Type_Name__c,Related__r.IssuingAuthority__c,Contact__r.BP_No__c,Contact__c,RecordType.Name,Company_Name__c,Issuing_Authority_Other__c,Passport_No__c,Nationality_list__c,CL_Certificate_No__c,IssuingAuthority__c,Shareholder_Company__c,Shareholder_Company__r.BP_No__c,Related__r.Record_Type_Name__c,Contact__r.Name,Shareholder_Company__r.Name  from Amendment__c where ServiceRequest__c=\''+step.SR__c+'\' AND ';
        if(step.SR__r.Record_Type_Name__c == 'Lease_Registration')
            sQry = sQry + ' Amendment_Type__c!=\'Landlord\' ';
        else if(step.SR__r.Record_Type_Name__c == 'Freehold_Transfer_Registration')
            sQry = sQry + ' (Amendment_Type__c=\'Buyer\' OR Related__r.Amendment_Type__c=\'Buyer\') ';
        
            
        for(Amendment__c objAmnd : database.query(sQry)){
            //checking for contacts
            if(objAmnd.Record_Type_Name__c=='Individual_Tenant'|| objAmnd.Record_Type_Name__c=='Individual_Buyer'|| 
                ((objAmnd.Record_Type_Name__c =='RORP_Authorized_Signatory'||objAmnd.Record_Type_Name__c =='RORP_Shareholder') && 
                objAmnd.Related__r.Record_Type_Name__c =='Company_Buyer' && objAmnd.Related__r.IssuingAuthority__c=='Others')  ){ 
                if(objAmnd.Passport_No__c!=null && objAmnd.Nationality_list__c!=null){
                    if( objAmnd.Contact__c != null && objAmnd.Contact__r.BP_No__c != null){
                        AlertData += ''+objAmnd.Contact__r.Name+'('+objAmnd.Contact__r.BP_No__c+') got de-duplicated for '+objAmnd.RecordType.Name+'\n\n';
                    }else{
                        string skey = objAmnd.Passport_No__c.toLowerCase()+''+objAmnd.Nationality_list__c.toLowerCase();
                        mapPassports.put(skey,objAmnd.Passport_No__c);
                        mapNations.put(skey,objAmnd.Nationality_list__c);
                        mapTypes.put(skey,objAmnd.RecordType.Name);
                    }
                }
            }
            
            //checking for Accounts
            if(objAmnd.Record_Type_Name__c=='Body_Corporate_Tenant' || objAmnd.Record_Type_Name__c=='DIFCI_Tenant'|| objAmnd.Record_Type_Name__c=='Company_Buyer'||
                (objAmnd.Record_Type_Name__c =='RORP_Shareholder_Company' && objAmnd.Related__r.Record_Type_Name__c =='Company_Buyer' && objAmnd.Related__r.IssuingAuthority__c=='Others')){
                
                if(objAmnd.Company_Name__c!=null && objAmnd.Issuing_Authority_Other__c!=null && objAmnd.CL_Certificate_No__c!=null){
                    if(objAmnd.Shareholder_Company__c != null && objAmnd.Shareholder_Company__r.BP_No__c != null){
                        AlertData += objAmnd.Shareholder_Company__r.Name+'('+objAmnd.Shareholder_Company__r.BP_No__c+') got de-duplicated for '+objAmnd.RecordType.Name+'\n\n';
                    }else{
                        string skey = objAmnd.Issuing_Authority_Other__c.toLowerCase()+''+objAmnd.CL_Certificate_No__c.toLowerCase();
                        mapIssuingAut.put(skey,objAmnd.Issuing_Authority_Other__c);
                        mapCLs.put(skey,objAmnd.CL_Certificate_No__c);
                        mapTypes.put(skey,objAmnd.RecordType.Name);
                    }
                }
                //checking for Point of Contact for Body Corporates
                if((objAmnd.Record_Type_Name__c=='Body_Corporate_Tenant' || objAmnd.Record_Type_Name__c=='Company_Buyer') && objAmnd.IssuingAuthority__c=='Others'){
                    if(objAmnd.Passport_No__c!=null && objAmnd.Nationality_list__c!=null){
                        string skey = objAmnd.Passport_No__c.toLowerCase()+''+objAmnd.Nationality_list__c.toLowerCase();
                        mapPassports.put(skey,objAmnd.Passport_No__c);
                        mapNations.put(skey,objAmnd.Nationality_list__c);
                        if(mapTypes.containsKey(skey) == false)
                            mapTypes.put(skey,'Point of Contact Details '+objAmnd.RecordType.Name);
                    }
                }
            }
        }   
        
        //check for Contacts
        if(!mapPassports.isEmpty() && !mapNations.isEmpty()){
            for(Contact objCon : [select Id,Name,BP_No__c,Nationality__c,Passport_No__c from Contact where Passport_No__c != null AND Nationality__c != null AND Passport_No__c IN : mapPassports.values() AND Nationality__c IN : mapNations.values() AND BP_No__c != null AND RecordType.DeveloperName='RORP_Contact']){
                string skey = objCon.Passport_No__c.toLowerCase()+''+objCon.Nationality__c.toLowerCase();
                if(mapPassports.containsKey(skey)){
                    AlertData += objCon.Name+'('+objCon.BP_No__c+') got de-duplicated for '+mapTypes.get(skey)+'\n\n';
                }
            }
        }
        //check for Accounts
        if(!mapIssuingAut.isEmpty() && !mapCLs.isEmpty()){
            for(Account objAcc : [select Id,Name,BP_No__c,Issuing_Authority__c,RORP_License_No__c from Account where RORP_License_No__c != null AND Issuing_Authority__c != null AND Issuing_Authority__c IN : mapIssuingAut.values() AND RORP_License_No__c IN : mapCLs.values() AND BP_No__c != null ]){
                string skey = objAcc.Issuing_Authority__c.toLowerCase()+''+objAcc.RORP_License_No__c.toLowerCase();
                if(mapIssuingAut.containsKey(skey)){
                    AlertData += objAcc.Name+'('+objAcc.BP_No__c+') got de-duplicated for '+mapTypes.get(skey)+'\n\n';
                }
            }
        }
        if(AlertData != '')
          AlertData = 'The BP details are not pushed completely to SAP. Kindly verify the BP detail in SAP and match it with the SF \n\n'+AlertData;
    }
    
    public void AccountBalance(){
        if(step.SR__r.Record_Type_Name__c == 'Application_of_Registration' || step.SR__r.Record_Type_Name__c == 'Notification_of_Personal_Data_Operations')
            AccountBalanceInfoCls.getAccountBalance(new list<string>{step.SR__r.Customer__c}, '5000');
    }
    
    public class TransitionWrapper{
        public Status__c objStatus{get;set;}
        public Step_Transition__c objSRStepTrans{get;set;}
    }
    
    /**
     * V1.2
     * Changes the visibility of the rejection reason
     */
    public void changeReasonVisibility(){
      // v1.4 - Claude - Replaced by additional conditions
        //hasRejectionReason = hasRejectionReasonVar.equals('Rejected') || hasRejectionReasonVar.equals('Declined');
        hasRejectionReason = ( hasRejectionReasonVar.equals('Rejected') || hasRejectionReasonVar.equals('Declined') ) && isReasonAvailable;
    }
    
    /**
     Check the any dependent visa's in awaiting originals  v1.5
    */
    
    public void checkAwaitingOrginalDepntSRS(){
           
        List<String> depVisas = new List<string>();  
        
        for(Dependent_Visa_packages__mdt depVisa :[select MasterLabel from Dependent_Visa_packages__mdt where is_active__c = true] ){
          depVisas.add(depVisa.MasterLabel );
        
        }  
        //v1.8 - Start   
          step__c stp = [select id,SR__r.SponsorId_hidden__c from step__c where id =:strStepId limit 1];
          if(stp.SR__r.SponsorId_hidden__c != NULL){
            List<step__c> step = [select id,SR__r.name,SR__r.SponsorId_hidden__c from step__c where SR__r.SponsorId_hidden__c=:stp.SR__r.SponsorId_hidden__c and  Step_Status__c =:awaitingReupload and SR__r.Record_Type_Name__c in : depVisas];
            if(step.size()>0)              
               AlertData = 'there is a SR in process for the same sponsor ' +step[0].SR__r.name;   
          }
              
    }//v1.5
    public static void fakeMethod(){
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
    }
   
}