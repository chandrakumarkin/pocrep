/*
    Author      : Durga Prasad
    Date        : 30-Oct-2019
    Description : Custom code to Assign the Ownership of the Account,Lead & Opportunity to the step owner
    -----------------------------------------------------------------------------------------------------
*/
global without sharing class CC_LM_Assignment implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        system.debug('stp===>'+stp);
        try{
            if(stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null){
            	
            	
            	
            	if(stp.Relationship_Manager__c!=null){
	                Account objacc = new Account();
	                list<Contact> lstContacts = new list<Contact>();
	                list<Opportunity> lstOpps = new list<Opportunity>();
	                for(Account acc:[Select Id,(Select Id,OwnerId from Contacts),(Select Id,OwnerId from Opportunities) from Account where Id=:stp.HexaBPM__SR__r.HexaBPM__Customer__c]){
	                    objacc.Id = acc.Id;
	                    objacc.OwnerId = stp.Relationship_Manager__c;
	                    
	                    for(Contact con:acc.Contacts){
	                        con.OwnerId = stp.Relationship_Manager__c;
	                        lstContacts.add(con);
	                    }
	                    
	                    for(Opportunity opp:acc.Opportunities){
	                        opp.OwnerId = stp.Relationship_Manager__c;
	                        lstOpps.add(opp);
	                    }
	                }
	                update objacc;
	                
	                if(lstContacts.size()>0)
	                    update lstContacts;
	                    
	                if(lstOpps.size()>0)
	                    update lstOpps;
            	}
            }
        }catch(Exception e){
            strResult = e.getMessage()+'';
        }
        return strResult;
    }
    
}