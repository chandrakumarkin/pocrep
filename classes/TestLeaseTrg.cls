/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest(seeAllData=false)
private class TestLeaseTrg {
    static testMethod void myUnitTest() {
        list<Building__c> lstBuilding = new list<Building__c>();
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP On Plan';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.SAP_Building_No__c = '123456';
        lstBuilding.add(objBuilding);
        insert lstBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c =  lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Store';
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        list<Lease__c> lstLeases = new list<Lease__c>();
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.Lease_Types__c = 'Retail Lease';
        insert objLease;
        
        List<Lease_Partner__c> ListLP=new List<Lease_Partner__c>();
        
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = objLease.Id;
        objLP.Account__c = objAccount.Id;
        objLP.Unit__c = lstUnits[0].Id;
        objLP.status__c = 'Draft';
        objLP.Type_of_Lease__c = 'Leased';
        objLP.Is_License_to_Occupy__c = true;
        objLP.Is_6_Series__c = false;
        ListLP.add(objLP);
        
        Lease_Partner__c objLP1 = new Lease_Partner__c();
        objLP1.Lease__c = objLease.Id;
        objLP1.Account__c = objAccount.Id;
        objLP1.Unit__c = lstUnits[1].Id;
        objLP1.status__c = 'Draft';
        objLP1.Type_of_Lease__c = 'Leased';
        objLP1.Is_License_to_Occupy__c = true;
        objLP1.Is_6_Series__c = false;
        ListLP.add(objLP1);
        
        insert ListLP;
        
        ListLP[0].status__c = 'Active';
        ListLP[1].status__c = 'Active';
        
        update ListLP;
        
        map<Id,Lease_Partner__c> mapOldLeasePartnerRecords=new map<Id,Lease_Partner__c>();
        mapOldLeasePartnerRecords.put(ListLP[0].id,ListLP[0]);
        
        list<Operating_Location__c> lstOLs = new list<Operating_Location__c>();
        
        Operating_Location__c objOL = new Operating_Location__c();
        objOL.Account__c = objAccount.Id;
        objOL.Lease__c = objLease.Id;
        objOL.Status__c = 'Active';
        lstOLs.add(objOL);
        
        objOL = new Operating_Location__c();
        objOL.Account__c = objAccount.Id;
        objOL.Lease_Partner__c = objLP.Id;
        objOL.Status__c = 'Active';
        objOL.Unit__c = objLP.Unit__c;
        lstOLs.add(objOL);
        
        
        insert lstOLs;
        
        objLease.Status__c = 'Expired';
        update objLease;
        
        
        
        LeaseTriggerHandler objLeaseTriggerHandler = new LeaseTriggerHandler();
        
        objLeaseTriggerHandler.executeBeforeInsertTrigger(null);
        objLeaseTriggerHandler.executeBeforeUpdateTrigger(null,null);
        objLeaseTriggerHandler.executeBeforeInsertUpdateTrigger(null,null);
        
        objLeaseTriggerHandler.executeAfterInsertUpdateTrigger(ListLP,mapOldLeasePartnerRecords);
        
        
        
    }
    
    @isTest
    static void test_UpdateOperatingLocationsFromLease_M(){
        Test.startTest();
        list<Building__c> lstBuilding = new list<Building__c>();
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP On Plan';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.SAP_Building_No__c = '123456';
        lstBuilding.add(objBuilding);
        insert lstBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        
                Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        list<Lease__c> lstLeases = new list<Lease__c>();
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Expired';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.Lease_Types__c = 'Retail Lease';
        objLease.Is_License_to_Occupy__c = true;
        insert objLease;
        map<Id,Lease__c> mapOldLeaseRecords = new map<Id,Lease__c>();
        mapOldLeaseRecords.put(objLease.Id,objLease);
        
        objLease.Status__c = 'Active';
        update objLease;

        list<Operating_Location__c> lstOLs = new list<Operating_Location__c>();
        Operating_Location__c objOL = new Operating_Location__c();
        objOL.Account__c = objAccount.Id;
        objOL.Lease__c = objLease.Id;
        objOL.Status__c = 'Active';
        lstOLs.add(objOL);
        
        Operating_Location__c objOL2 = new Operating_Location__c();
        objOL2.Account__c = objAccount.Id;
        objOL2.Lease__c = objLease.Id;
        objOL2.Status__c = 'Inactive';
        lstOLs.add(objOL2);
        
        insert lstOLs;
        
        LeaseTriggerHandler objLeaseTriggerHandler = new LeaseTriggerHandler();
        objLeaseTriggerHandler.executeAfterUpdateTrigger(new List<sobject>{objLease},mapOldLeaseRecords);
        
        Test.stopTest();
    }
    
    
    @isTest
    static void test_activateOperatingLocation_M(){
        Test.startTest();
        list<Building__c> lstBuilding = new list<Building__c>();
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP On Plan';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.SAP_Building_No__c = '123456';
        lstBuilding.add(objBuilding);
        insert lstBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        list<Lease__c> lstLeases = new list<Lease__c>();
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Created';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.Lease_Types__c = 'Retail Lease';
        objLease.Is_License_to_Occupy__c = true;
        insert objLease;
        map<Id,Lease__c> mapOldLeaseRecords = new map<Id,Lease__c>();
        mapOldLeaseRecords.put(objLease.Id,objLease);
        

        list<Operating_Location__c> lstOLs = new list<Operating_Location__c>();
        Operating_Location__c objOL = new Operating_Location__c();
        objOL.Account__c = objAccount.Id;
        objOL.Lease__c = objLease.Id;
        objOL.Status__c = 'Inactive';
        lstOLs.add(objOL);
        
        Operating_Location__c objOL2 = new Operating_Location__c();
        objOL2.Account__c = objAccount.Id;
        objOL2.Lease__c = objLease.Id;
        objOL2.Status__c = 'Inactive';
        lstOLs.add(objOL2);
        
        insert lstOLs;
        
        objLease.Status__c = 'Active';
        update objLease;
        
        LeaseTriggerHandler objLeaseTriggerHandler = new LeaseTriggerHandler();
        objLeaseTriggerHandler.executeAfterUpdateTrigger(new List<sobject>{objLease},mapOldLeaseRecords);
        
        Test.stopTest();
    }
}