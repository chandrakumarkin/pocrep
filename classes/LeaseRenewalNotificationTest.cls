@isTest
public class LeaseRenewalNotificationTest{
    
    static testmethod void LeaseTest(){
        
        Account ac = new Account(name ='Grazitti', ROC_Status__c = 'Active') ;       
        insert ac;
        
        User u = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'DIFC Customer Community User Custom'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Contact; 
     Map<String,Schema.RecordTypeInfo> ContactRecordTypeInfo
                   = cfrSchema.getRecordTypeInfosByName(); 
        
       ID rt = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
       Contact con = new Contact(LastName ='testCon',AccountId = ac.Id ,Email = 'abc@hotmail.com', recordtypeid=rt);
        insert con;                 
        
        u.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services';
        u.ContactID = con.id;
        u.IsActive= true;
        insert  u;
        
        
        Lease__c lease= new Lease__c ();
        lease.Account__c = ac.ID;
        //lease.Account__c = acct.ID;
        lease.status__c = 'Active';
        lease.End_Date__c = Date.Today().AddDays(5);
        lease.Square_Feet__c = '58';
        lease.Type__c = 'Leased Property';
        lease.Is_License_to_Occupy__c  = true;
        lease.No_of_Desks__c = 2;
        lease.Lease_Types__c ='Leased Property';
        
        insert lease;
        
        
        Building__c b = new Building__c();
        b.Company_Code__c = '2300';
        insert b;
        
        Unit__c unit = new Unit__c();
        unit.Building__c = b.ID;
        unit.Unit_Usage_Type__c='retail';
        
        insert unit;
        
        Tenancy__c ten = new Tenancy__c ();
        ten.End_Date__c = Date.Today().AddDays(5);
        ten.Unit__c = unit.ID;
        ten.lease__c = lease.ID;
        ten.LeaseRenewalEmailSent__c=false;
        insert ten;
        system.debug(ten.ID);
        
        
        Tenancy__c tenSoql = [Select ID ,Days_left_in_Renewal__c, 
                              lease__r.Visa_Factor__c,Third_Party__c,Lease__r.Account__r.Name,
                              Lease__r.Account__c FROM Tenancy__c LIMIT 1];
        
        system.debug(tenSoql);
        system.debug(tenSoql.lease__r.Visa_Factor__c);
        
        
        Test.startTest();
        LeaseRenewalNotificationSchd  obj = new LeaseRenewalNotificationSchd();
        
        LeaseRenewalNotification sh1 = new LeaseRenewalNotification();
        String sch = '0 0 23 * * ?'; 
        String jobId = system.schedule('A job', sch, obj); 
        Test.stopTest();
        
        
        
    }
    
    
    static void testEmailUtility()
    {
        Test.StartTest();
        
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        
        System.assertEquals(1, invocations, 'An email has not been sent');
    }
    
    
}