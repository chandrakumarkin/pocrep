/*
    Author      : Durga Prasad
    Date        : 13-Mar-2020
    Description : Custom code executes on Approval of Create Your Profile when Agent adds new Entity from Manage Entity Screen
    
    V1.1         Sai           DIFC2-10163
    V1.2         Sai           
    ---------------------------------------------------------------------------------------------------------------------------
*/
global without sharing class CC_CreateYourProfile implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        try{
            if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Contact__c!=null && stp.HexaBPM__SR__r.Entity_Type__c!=null && stp.HexaBPM__SR__r.Entity_Name__c!=null){
                string OppId;
                string AccountId;
                system.debug('Entity Type==>'+stp.HexaBPM__SR__r.Entity_Type__c);
                system.debug('Business Sector==>'+stp.HexaBPM__SR__r.Business_Sector__c);
                
                if((stp.HexaBPM__SR__r.Entity_Type__c=='Non - financial' || stp.HexaBPM__SR__r.Entity_Type__c=='Retail' || stp.HexaBPM__SR__r.Entity_Type__c=='Financial - related') && stp.HexaBPM__SR__r.Business_Sector__c!='Investment Fund'){
                    Id AccRecId = OB_QueryUtilityClass.getRecordtypeID ('Account','Business_Development');      
                    Account acc = new Account();
                    acc.Name = stp.HexaBPM__SR__r.Entity_Name__c;
                    acc.Company_Type__c = stp.HexaBPM__SR__r.Entity_Type__c;
                    acc.OB_Sector_Classification__c = stp.HexaBPM__SR__r.Business_Sector__c;
                    if(!Test.isRunningTest())
                            acc.OwnerId = label.Portal_User_Default_Owner;
                    acc.ROC_Status__c = 'Account Created';
                    acc.Priority_to_DIFC__c = 'Standard';
                    acc.Mobile_Number__c = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;
                    acc.RecordTypeId = AccRecId;
                    // V1.1
                    if(stp.HexaBPM__SR__r.Created_by_Agent__c == true){
                        acc.Created_by_Agent__c = 'Yes';
                    }
                     // V1.1 end
                    string OpportunityRecordTypeName;
                    
                    if(stp.HexaBPM__SR__r.Entity_Type__c!='Financial - related' && stp.HexaBPM__SR__r.Business_Sector__c!='Investment Fund'){
                        if(stp.HexaBPM__SR__r.Entity_Type__c=='Non - financial'){
                            acc.BD_Sector__c = 'Corporates and Service Provider';
                            OpportunityRecordTypeName = 'BD_Non_Financial';
                        }else if(stp.HexaBPM__SR__r.Entity_Type__c=='Retail'){
                            OpportunityRecordTypeName = 'BD_Retail';
                            acc.BD_Sector__c = 'Retail (RET)';
                        }
                    }else{
                        acc.BD_Sector__c = 'Banks';
                        OpportunityRecordTypeName = 'BD_Financial';
                    }
                    
                    insert acc;
                    AccountId = acc.Id;
                    
                    /*
                    HexaBPM__Service_Request__c objSRTBU = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__c);
                    objSRTBU.HexaBPM__Customer__c = AccountId;
                    update objSRTBU;
                    */
                    
                    Opportunity objOpp = new Opportunity();
                    for(RecordType rctyp:[Select Id from RecordType where sobjectType='Opportunity' and DeveloperName=:OpportunityRecordTypeName]){
                        objOpp.RecordTypeId = rctyp.Id;
                    }
                    objOpp.AccountId = AccountId;
                    objOpp.CloseDate = system.today();
                    objOpp.Name = stp.HexaBPM__SR__r.entity_name__c+'-Opportunity';
                    objOpp.StageName = 'Created';
                    objOpp.Type = stp.HexaBPM__SR__r.Setting_Up__c;
                    objOpp.Sector_Classification__c = stp.HexaBPM__SR__r.Business_Sector__c;
                    if(!Test.isRunningTest()){
                        objOpp.OwnerId = label.Portal_User_Default_Owner;
                    }
                    Insert objOpp;
                    OppId = objOpp.Id;
                }else{
                    string CommunityUserAccountId;
                    for(User usr:[Select Id,ContactId,Contact.AccountId from user where Id=:userinfo.getuserid() and ContactId!=null]){
                        CommunityUserAccountId = usr.Contact.AccountId;
                    }
                    FundAccountData objFundAccResp = new FundAccountData();
                    if(CommunityUserAccountId!=null || system.test.isRunningTest()){
                        if(stp.HexaBPM__SR__r.Fund_Type__c!=null){
                            objFundAccResp = CreateFundAccount(CommunityUserAccountId,stp.HexaBPM__SR__r.Fund_Type__c);
                            if(objFundAccResp.AccountId!=null){
                                AccountId = objFundAccResp.AccountId;
                                for(Opportunity opp:[Select Id from Opportunity where AccountId=:AccountId]){
                                    OppId = opp.Id;
                                }
                            }else{
                                strResult = 'No Fund Account Created.';
                                if(objFundAccResp.ErrorMessage!=null && objFundAccResp.ErrorMessage!='')
                                    strResult = objFundAccResp.ErrorMessage;
                                return strResult;
                            }
                        }else{
                            strResult = 'Fund Type on Application is blank.';
                            return strResult;
                        }
                    }else{
                        strResult = 'No Entity Found to create Fund Account.';
                        return strResult;
                    }
                }
                
                HexaBPM__Service_Request__c objSRTBU = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__c);
                objSRTBU.HexaBPM__Customer__c = AccountId;
                update objSRTBU;
                    
                Contact objcon = new Contact(Id=stp.HexaBPM__SR__r.HexaBPM__Contact__c);
                if(stp.HexaBPM__SR__r.First_Name__c!=null)
                    objcon.FirstName = stp.HexaBPM__SR__r.First_Name__c;
                if(stp.HexaBPM__SR__r.Last_Name__c!=null)
                    objcon.LastName = stp.HexaBPM__SR__r.Last_Name__c;
                if(stp.HexaBPM__SR__r.hexabpm__email__c!=null)
                    objcon.Email = stp.HexaBPM__SR__r.hexabpm__email__c;
                if(stp.HexaBPM__SR__r.hexabpm__send_sms_to_mobile__c!=null){
                    objcon.MobilePhone = stp.HexaBPM__SR__r.hexabpm__send_sms_to_mobile__c;
                    objcon.Phone = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;    
                }
                if(stp.HexaBPM__SR__r.Passport_No__c!=null)
                    objcon.Passport_No__c = stp.HexaBPM__SR__r.Passport_No__c;
                if(stp.HexaBPM__SR__r.Nationality__c!=null)
                    objcon.Nationality__c = stp.HexaBPM__SR__r.Nationality__c;
                if(stp.HexaBPM__SR__r.Date_of_Birth__c!=null)
                    objcon.Birthdate = stp.HexaBPM__SR__r.Date_of_Birth__c;
                if(stp.HexaBPM__SR__r.Gender__c!=null)
                    objcon.Gender__c = stp.HexaBPM__SR__r.Gender__c;
                if(stp.HexaBPM__SR__r.Place_of_Birth__c!=null)
                    objcon.Place_of_Birth__c = stp.HexaBPM__SR__r.Place_of_Birth__c;
              //  if(stp.HexaBPM__SR__r.Country_of_Birth__c !=null)
                   // objcon.Country_of_Birth__c = stp.HexaBPM__SR__r.Country_of_Birth__c;    // V1.2  
                if(stp.HexaBPM__SR__r.Date_of_Issue__c!=null)
                    objcon.Date_of_Issue__c = stp.HexaBPM__SR__r.Date_of_Issue__c;
                if(stp.HexaBPM__SR__r.Date_of_Expiry__c!=null)
                    objcon.Passport_Expiry_Date__c = stp.HexaBPM__SR__r.Date_of_Expiry__c;
                if(stp.HexaBPM__SR__r.Place_of_Issuance__c!=null)
                    objcon.Place_of_Issue__c = stp.HexaBPM__SR__r.Place_of_Issuance__c;
                if(stp.HexaBPM__SR__r.Nationality__c!=null)
                    objcon.Issued_Country__c = stp.HexaBPM__SR__r.Nationality__c;
                if(stp.HexaBPM__SR__r.Nationality__c!=null)
                    objcon.Country_of_Birth__c = stp.HexaBPM__SR__r.Nationality__c;
                update objcon;

                OB_AgentEntityUtils.setPassportFileUnderContact(objcon,stp.HexaBPM__SR__c);
                
                AccountContactRelation objAccContactRel = new AccountContactRelation();
                objAccContactRel.AccountId = AccountId;
                objAccContactRel.ContactId = objcon.Id;
                objAccContactRel.Relationship_with_entity__c = stp.HexaBPM__SR__r.Relationship_with_entity__c;
                objAccContactRel.IsActive = true;
                objAccContactRel.Access_Level__c = 'Read/Write';
                objAccContactRel.Roles = 'Super User;Company Services;Property Services';
                insert objAccContactRel;
                
                objcon.AccountId = AccountId;
                update objcon;
                
                if(stp.HexaBPM__SR__r.Entity_Type__c=='Retail' || (stp.HexaBPM__SR__r.Entity_Type__c=='Non - financial' && stp.HexaBPM__SR__r.Business_Sector__c!='Investment Fund')){
                    if(AccountId!=null && OppId!=null){
                        CC_CreatePortalUser.CreateInPrincipleSR(stp.HexaBPM__SR__c,OppId,AccountId);
                    }else{
                        strResult = 'No Fund Account Created.';
                    }
                }else{
                    strResult = OB_CustomCodeHelper.OB_Create_Application(stp.HexaBPM__SR__c,'AOR_Financial');
                }
                UpdateUserDetails(stp.HexaBPM__SR__c,objcon.Id);
            }
        }catch(DMLException e){
            string DMLError = e.getdmlMessage(0)+'';
            if(DMLError==null){
                DMLError = e.getMessage()+'';
            }
            strResult = DMLError;
        }
        return strResult;
    }
    public FundAccountData CreateFundAccount(Id AccountId,string RelationshipType){
        FundAccountData objResp = new FundAccountData();
        if(AccountId!=null && RelationshipType!=null){
            Savepoint spdata = Database.setSavepoint();
            try {
                
                list<Account> accountRecords = [SELECT Id, Name, ROC_Status__c, (SELECT id, StageName FROM Opportunities),OwnerId,Financial_Sector_Activities__c,BD_Financial_Services_Activities__c,Geographic_Region__c from Account WHERE Id =:AccountId];
                //CRM_cls_AccountHandler.getAccountRecords(new list<Id>{AccountId});
                string fundRecordTypeId;// = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BD Fund').getRecordTypeId();
                for(RecordType rc:[Select Id from RecordType where sobjectType='Account' and Name='BD Fund' and IsActive=true]){
                    fundRecordTypeId = rc.Id;
                }
                if(!accountRecords.isEmpty()){
                    
                    CRM_cls_Utils crmUtils = new CRM_cls_Utils();
                    crmUtils.setLogType('CRM: Creating Fund Account');
                
                    Account fundAccount = new Account();
                    
                    fundAccount.Name = accountRecords[0].Name + ' - Fund';
                    fundAccount.Priority_to_DIFC__c = 'Standard';
                    fundAccount.Company_Type__c = 'Non - financial';
                    fundAccount.BD_Sector__c = 'Wealth Management (WM)';
                    
                    fundAccount.ownerId = accountRecords[0].ownerId;
                    fundAccount.Geographic_Region__c = accountRecords[0].Geographic_Region__c;
                    fundAccount.Financial_Sector_Activities__c = accountRecords[0].Financial_Sector_Activities__c;
                    fundAccount.BD_Financial_Services_Activities__c = accountRecords[0].BD_Financial_Services_Activities__c;
                    fundAccount.recordTypeId = fundRecordTypeId ;
                    fundAccount.OB_Sector_Classification__c = Label.Investment_Fund;
                    insert fundAccount;
                    
                    Relationship__c fundRelationship = new Relationship__c();
                    
                    string relType = RelationshipType;
                    
                    fundRelationship.Subject_Account__c= accountId;
                    fundRelationship.Object_Account__c = fundAccount.Id;  
                    fundRelationship.Active__c = true;
                    fundRelationship.Relationship_Type__c = relType;     
                    fundRelationship.Start_Date__c = system.today();
                    fundRelationship.Push_to_SAP__c = true;
                    insert fundRelationship;
                    
                    Map<String,String> mappedContactIds = new Map<String,String>();
                    
                    SAPWebServiceDetails.CRMPartnerCall(new List<String>{fundRelationship.Id},null,new Map<String,String>{fundAccount.Id => 'ConvertedAccount'}); // TODO: Change account param if needed
                    
                    insert new Opportunity(
                        AccountId = fundAccount.Id,
                        Name = fundAccount.Name + ' - Fund',
                        StageName = 'Documents submitted to DFSA',
                        CloseDate = System.Today() + 30,
                        RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'BD_Financial_Fund'].Id
                    );
                    objResp.AccountId = fundAccount.Id;
                }
            }catch(DMLException e){
                string DMLError = e.getdmlMessage(0)+'';
                if(DMLError==null){
                    DMLError = e.getMessage()+'';
                }
                objResp.ErrorMessage = DMLError;
                objResp.IsSuccess = false;
                Database.rollback(spdata);
            }
        }
        return objResp;
    }
    public class FundAccountData{
        public boolean IsSuccess;
        public string AccountId;
        public string ErrorMessage;
        public FundAccountData(){
            IsSuccess = true;
            ErrorMessage = '';
        }
    }
    /*
        Method Name :   UpdateUserDetails
        Description :   Future method to Update the User Details on Approval of new Entity Created by Agent
    */
    @future
    public static void UpdateUserDetails(string SRID,string ContactId){
        if(SRID!=null && ContactId!=null){
            list<User> lstusr = [Select Id,FirstName,LastName,Email,MobilePhone,Phone from User where ContactId=:ContactId and IsActive=true];
            if(lstusr!=null && lstusr.size()>0){
                for(HexaBPM__Service_Request__c objSR:[Select Id,First_Name__c,Last_Name__c,hexabpm__email__c,hexabpm__send_sms_to_mobile__c from HexaBPM__Service_Request__c where Id=:SRID]){
                    lstusr[0].FirstName = objSR.First_Name__c;
                    lstusr[0].LastName = objSR.Last_Name__c;
                    lstusr[0].Email = objSR.hexabpm__email__c;
                    lstusr[0].MobilePhone = objSR.hexabpm__send_sms_to_mobile__c;
                    lstusr[0].Phone = objSR.hexabpm__send_sms_to_mobile__c;
                }
                try{
                    update lstusr;
                }catch(Exception e){
                    Log__c objLog = new Log__c(Description__c = 'Exception on CC_CreateYourProfile.UpdateUserDetails() - Line:261 :'+e.getMessage(),Type__c = 'Custom Code to create CC_CreateYourProfile on Create Your Profile by Agent.');
                    insert objLog;
                }
            }
        }
    }
}