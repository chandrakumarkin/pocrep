@RestResource(urlMapping='/createcase/*')
global with sharing class DIFC_website_case{

  
    
    @HttpPost
    global static DFZCCreateResponce doPost() 
    {
    
        RestRequest req = RestContext.request;
        DFZCCreateResponce ObjRes=new DFZCCreateResponce();
        System.debug('===============doPost=============================');
        System.debug('=====PushedLeads=>'+req.requestBody.toString());
        string postBody=req.requestBody.toString();
        List<DIFCCase> Pushedcases = (List<DIFCCase>)JSON.deserialize(postBody,List<DIFCCase>.class);
  
          Attachment attach ;
            List<case> Listcases=new List<case>();
          //  List<Attachment> attachList = new List<Attachment>();
            for(DIFCCase Tempcase:Pushedcases)
            {
                case ObjCase=new case();
                ObjCase.Serious_harm_to_affected_people__C=Tempcase.SeriousHarmToAffectedPeople;
                ObjCase.Notice_to_notify_the_people_affected__c = Tempcase.noticeTonotifyPeopleAffected;
                ObjCase.Why_are_you_relying_on__c = Tempcase.whyAreYouRelyingOn;
                ObjCase.Number_of_people_affected__c = Tempcase.NumberOfPeopleAffected;
                ObjCase.Personal_information_involved_in_the_br__c = Tempcase.PersonalinformationInvolvedinBreach;
                ObjCase.Breach_Other_Please_Specify__c = Tempcase.BreachOtherPleaseSpecify;
                ObjCase.Type_of_breach_Other_Please_Specify__c = Tempcase.typeofbreachOtherPleaseSpecify;
                ObjCase.Type_of_breach_Other_Please_Specify__c = Tempcase.TypeofbreachOtherPleaseSpecifyAll;
                ObjCase.Tell_us_what_happened__c = Tempcase.Telluswhathappened;
                ObjCase.Do_you_know_where_the_information_has_go__c = Tempcase.doyouknowwheretheinformationhasgo;
                ObjCase.You_wish_to_share__c = Tempcase.youWishToShare;
                ObjCase.Sensitive_is_the_information__c = Tempcase.SensitiveIsTheInformation;
                ObjCase.may_obtain_the_information__c = Tempcase.mayObtaintheInformation;
                ObjCase.Discriminatory_harm__c = Tempcase.discriminatoryHarm;
                ObjCase.Emotional_harm__c = Tempcase.emotionalHarm;
                ObjCase.Employment_harm__c = Tempcase.employmentHarm;
                ObjCase.Financial_harm__c = Tempcase.financialHarm;
                ObjCase.Identity_theft__c = Tempcase.identityTheft;
                ObjCase.Loss_of_access_to_information__c = Tempcase.lossOfaccesstoInformation;
                ObjCase.Loss_of_opportunity__c = Tempcase.lossOfOpportunity;
                ObjCase.Physical_harm__c = Tempcase.physicalHarm;
                ObjCase.Reputational_harm__c = Tempcase.reputationalHarm;
                ObjCase.Threats_of_harm__c = Tempcase.threatsOfHarm;
                ObjCase.No_one__c = Tempcase.noOne;
                ObjCase.Don_t_know__c = Tempcase.dontKnow;
                ObjCase.Other_text__c = Tempcase.otherText;
                ObjCase.Other_value__c = Tempcase.otherValue;
                ObjCase.Harmed_because_of_this_breach__c = Tempcase.HarmedBecauseOfThisBreach;
                ObjCase.Reduce_the_risk_of_harm__c = Tempcase.reducetheRiskofHarm;
                ObjCase.Other_Please_Specify__c = Tempcase.RiskofharmOtherPleaseSpecify;
                ObjCase.Protect_the_information_from_being_acces__c = Tempcase.protecttheInformationfromBeingacces;
                
                ObjCase.Physical_safety_in_immediate_danger__c = Tempcase.PhysicalSafetyInImmediateDanger;
                ObjCase.Psychological_safety_at_immediate_risk__c = Tempcase.PsychologicalSafetyAtImmediateRisk;
                ObjCase.Immediate_risk_of_serious_financial_harm__c = Tempcase.ImmediateRiskOfSeriousFinancialHarm;
                ObjCase.organisations_affected_by_the_breach__c = Tempcase.organisationsAffectedByTheBreach;
                
                    
                ObjCase.Reported_to_other_authorities__c = Tempcase.ReportedToOtherAuthorities;
                ObjCase.other_affected_organisations__c = Tempcase.otherAffectedOrganisations;
                
                ObjCase.Authorities_has_the_breach__c = Tempcase.AuthoritiesHasTheBreach;
                ObjCase.Organisation_or_people_affected_by__c = Tempcase.organisationorPeopleAffectedby;
                ObjCase.Affected_by_the_breach_Other_Please_Sp__c = Tempcase.AffectedbyTheBreachOtherPleaseSp;
                ObjCase.Steps_you_have_taken_or_intend_to_take__c = Tempcase.stepsYouHaveTakenOrIntendToTake;
                ObjCase.Why_have_you_not_notified_the_people_aff__c = Tempcase.whyhaveyounotNotifiedthePeopleaff;
                ObjCase.Not_notify_the_people_affected__c = Tempcase.notNotifyThePeopleAffected;
                
               //ObjCase.GN_Type_of_Feedback__c='Complaint';
                
                 ObjCase.Origin='Web';
                ObjCase.Status='New';
                //ObjCase.GN_Sys_Case_Queue__c='00G0J000003GxIz';
                Listcases.add(ObjCase);
                
                if(Tempcase.CaseFile != null)
                {
                    attach = new Attachment();
                    attach.contentType = 'application/pdf';
                    attach.body = EncodingUtil.base64Decode(Tempcase.CaseFile);
                 //   attachList.add(attach);
                }       
            }   
      
        
       try
       {
       
       System.debug('Listcases===>'+Listcases);
       
            ObjRes.errorMessage='';
            
            Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
            List<Database.SaveResult> updateResults = Database.insert(Listcases,dmo );

            
            
            Set<Id> listOfIds = new Set<Id>();
            for (Database.SaveResult sr : updateResults) 
            {
                if (sr.isSuccess()) 
                {
                    ObjRes.isSuccess=true;
                
                    listOfIds.add(sr.getId());
                    ObjRes.caseId=sr.getId();

                      

                }
                else
                {
                    ObjRes.isSuccess=false;
                    for(Database.Error err : sr.getErrors()) 
                    {
                        ObjRes.errorMessage=err.getStatusCode() + ': ' + err.getMessage()+':'+err.getFields();

                      //  System.debug('The following error has occurred.');                    
                        //System.debug();
                       // System.debug('Account fields that affected this error: ' + err.getFields());
                    }

                }
            }
            
            if(ObjRes.caseId!=null && attach!=null)
            {
                   attach.name = 'file.pdf';
                    attach.parentId =ObjRes.caseId;
                    insert attach;
                    System.debug(attach.id);
            }
         
           
               
            /*
            List<Case> caseList =[select Id,CaseNumber from Case where Id IN:listOfIds];
            if(caseList.size() >0)
            {
                for(Attachment attach: attachList)
                {
                  
                 
                }
                if(attachList.size() >0)
            }
            */

          
       }
       catch(DmlException e)
       {
         
            ObjRes.isSuccess=false;
            ObjRes.errorMessage=e.getMessage();
           // jsonString = JSON.serialize(ObjRes);
           
       }
         
         return ObjRes;
       
       
    }
 


 global class  DFZCCreateResponce
{
    boolean isSuccess{get;set;}
    string errorMessage{get;set;}
     string caseId{get;set;}
    
    
}

global  class  DIFCCase
{
    string SeriousHarmToAffectedPeople{get;set;}
    string noticeTonotifyPeopleAffected{get;set;}
    string whyAreYouRelyingOn{get;set;}
    string NumberOfPeopleAffected{get;set;}
    string PersonalinformationInvolvedinBreach{get;set;}
    string BreachOtherPleaseSpecify{get;set;}
    string typeofbreachOtherPleaseSpecify{get;set;}
    string TypeofbreachOtherPleaseSpecifyAll{get;set;}
    string Telluswhathappened{get;set;}
    string doyouknowwheretheinformationhasgo{get;set;}
    string youWishToShare{get;set;}
    string SensitiveIsTheInformation{get;set;}
    string mayObtaintheInformation{get;set;}
    
    string discriminatoryHarm{get;set;}
    string emotionalHarm{get;set;}
    string employmentHarm{get;set;}
    string financialHarm{get;set;}
    string identityTheft{get;set;}
    string lossOfaccesstoInformation{get;set;}
    string lossOfOpportunity{get;set;}
    string physicalHarm{get;set;}
    string reputationalHarm{get;set;}
    string threatsOfHarm{get;set;}
    string noOne{get;set;}
    string dontKnow{get;set;}
    string otherText{get;set;}
    string otherValue{get;set;}
    string HarmedBecauseOfThisBreach{get;set;}
    string reducetheRiskofHarm{get;set;}
    string RiskofharmOtherPleaseSpecify{get;set;}
    string protecttheInformationfromBeingacces{get;set;}
    string PhysicalSafetyInImmediateDanger{get;set;}
    string PsychologicalSafetyAtImmediateRisk{get;set;}
    string ImmediateRiskOfSeriousFinancialHarm{get;set;}
    string organisationsAffectedByTheBreach{get;set;}   
    string ReportedToOtherAuthorities{get;set;} 
    string otherAffectedOrganisations{get;set;} 
    string AuthoritiesHasTheBreach{get;set;}    
    string organisationorPeopleAffectedby{get;set;} 
    string AffectedbyTheBreachOtherPleaseSp{get;set;}   
    string stepsYouHaveTakenOrIntendToTake{get;set;}    
    string whyhaveyounotNotifiedthePeopleaff{get;set;}  
    string notNotifyThePeopleAffected{get;set;}     
    
    String CaseFile{get;set;}
    /*
    string firstName{get;set;}
    string lastName{get;set;}
    string phoneNumber{get;set;}
    string email{get;set;}
    List<string> areaOfInterest{get;set;}
    string subZoneName{get;set;}
    string ipAddress{get;set;}
    string contactPreference{get;set;}
    string companyName{get;set;}
    string additionalComments{get;set;}
    Datetime submittedOn{get;set;}
    
    boolean gdprConsent{get;set;} 
    
    //Only use in case of get Lead details
    string status{get;set;}
    Datetime updatedOn{get;set;}
*/
    
    
}
    
}