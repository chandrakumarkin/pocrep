/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 28/03/2022
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    28/03/2022   shoaib        Created
**********************************************************************************************************************/
public class CommercialPermissionGSController {
   
    Private static final String RECORD_TYPE_ID   =   Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Commercial_Permission_Card').getRecordTypeId();
    
    @AuraEnabled
    public static wrapperClass getserviceRequest(String recordId){
     
      Service_Request__c thisServiceRequest = new  Service_Request__c();
      Account thisAccount                   = new Account();
      User  objUser =  [SELECT Id,
                        		Name,ContactId,AccountId,Phone,Email,Contact.Account.Name,Contact.Account.Active_License__c
                           FROM User
                           WHERE Id=: userinfo.getUserId()]; 
        
        if(String.isBlank(recordId)){
            thisServiceRequest.Customer__c              = objUser.AccountId;
            thisServiceRequest.License__c               = objUser.Contact.Account.Active_License__c;
            thisServiceRequest.Email__c                 = objUser.Email;
            thisServiceRequest.Send_SMS_To_Mobile__c         = objUser.Phone;
        }
        else{
          thisServiceRequest = [SELECT Id,Emirate__c,Duration_in_months__c,Reason_for_Request__c,UID_Number__c,Monthly_Salary__c,Sponsor_First_Name__c,Trade_License_No__c,
                                        Title__c,Nationality_list__c,Express_Service__c,Occupation__c,First_Name__c,First_Name_Arabic__c,Passport_Number__c,Middle_Name__c,Middle_Name_Arabic__c,
                                		    Customer__c,Passport_Date_of_Issue__c,Last_Name__c,Last_Name_Arabic__c,Passport_Date_of_Expiry__c,Current_Visa_Status__c,
                                        Date_of_Birth__c,Residence_Visa_No__c,Residence_Visa_Expiry_Date__c,Would_you_like_to_opt_our_free_couri__c,
                                        Use_Registered_Address__c,Consignee_FName__c,Courier_Mobile_Number__c,Courier_Cell_Phone__c,Consignee_LName__c,
                                        Apt_or_Villa_No__c,Contract_Start_Date__c,Statement_of_Undertaking__c,Duration__c,Occupation_GS__c,Send_SMS_To_Mobile__c,Email__c,Issuing_Authority_Names__c
                                      FROM Service_Request__c 
                                      WHERE Id =: recordId limit 1 ];  
        }
        thisAccount = [SELECT Id,
                       			  Name,
                       				Active_License__r.Name,
                       				Index_Card__r.Name 
                       FROM Account 
                       WHERE Id=:objUser.AccountId 
                       LIMIT 1];
        
      return new wrapperClass(thisAccount,thisServiceRequest);
    }
    
    
    public class wrapperClass {
       @AuraEnabled
       public Account newAccount {get;set;}
       @AuraEnabled
       public Service_Request__c newSR {get;set;}
       @AuraEnabled
       public List<String> optionList {get;set;}
       @AuraEnabled
       public List<String> occuptationList {get;set;}
      
        public wrapperClass(Account newAccount,Service_Request__c newSR){
            this.newAccount = newAccount;
            this.newSR      = newSR;
            this.optionList = EquityCardHolderController.getPickListValuesIntoList();
         } 
    }
    
    @AuraEnabled
    public static string getRegistratedAccounts(String accountId){
      return [SELECT Id,Registered_Address__c FROM Account WHERE Id =: accountId].Registered_Address__c;
    }
    
    @AuraEnabled
    public static List<String> getPickListValuesIntoList(){
       List<String> pickListValuesList= new List<String>();
       Schema.DescribeFieldResult fieldResult = Service_Request__c.Nationality_list__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       for( Schema.PicklistEntry pickListVal : ple){
          pickListValuesList.add(pickListVal.getLabel());
        }     
       return pickListValuesList;
    }
     
    @AuraEnabled
    public static string saveRequest(Service_Request__c newServiceRequest){
        String occuptionId  = '';
        
        if(!test.isRunningTest()){
            occuptionId = [SELECT ID,Name FROM Lookup__c WHERE Name =: newServiceRequest.Occupation_GS__c OR Id =:newServiceRequest.Occupation_GS__c].Id;
        }
        
        String message = '';
            
        User  objUser =  [SELECT Id,
                        	 	      Name,ContactId,Contact.AccountId,AccountId,Phone,Email,Contact.Account.Name,Contact.Account.Active_License__c
                             FROM User
                             WHERE Id=: userinfo.getUserId()]; 
        
         Service_Request__c thisServiceRequest         = new  Service_Request__c();
         thisServiceRequest                            = newServiceRequest;
         thisServiceRequest.RecordTypeId               = RECORD_TYPE_ID;
         if(!test.isRunningTest())    thisServiceRequest.Occupation_GS__c           = occuptionId;
         thisServiceRequest.Email__c                   = objUser.Email;
         thisServiceRequest.Send_SMS_To_Mobile__c      = objUser.Phone;
         thisServiceRequest.Nationality_list__c        = newServiceRequest.Nationality_list__c;
         thisServiceRequest.License__c                 = objUser.Contact.Account.Active_License__c;
         thisServiceRequest.Service_Category__c        = 'New';
        try{
            if(String.isNotBlank( thisServiceRequest.Id)){
                update thisServiceRequest;
            }
            else{
                insert thisServiceRequest;
            }
        
           message =  thisServiceRequest.Id;
        }
        catch(Exception ex){
           message = 'Error' +' ' +ex.getMessage().split(',')[1];
        }
        return message;
      }
      
    @AuraEnabled
    public static List<String> getPickListValuesIntoListTest(){
       List<String> pickListValuesList= new List<String>();
       Schema.DescribeFieldResult fieldResult = Service_Request__c.Nationality_list__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       for( Schema.PicklistEntry pickListVal : ple){
          pickListValuesList.add(pickListVal.getLabel());
        }     
       return pickListValuesList;
    }

    
}