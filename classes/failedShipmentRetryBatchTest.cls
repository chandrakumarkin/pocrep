@isTest
public class failedShipmentRetryBatchTest {
    static testMethod void validateLogs() {
        test.startTest();
        Log__c thisLog = new Log__c();
        thisLog.Type__c = 'Courier : Shipment Creation Error stepIda1P0J00003pR0Ga';
        thisLog.Description__c  = 'Request Id ===>> a1I0J00000I8PwPUAV- stepId a1P0J00003pR0Ga#';
        insert thisLog;
        
        failedShipmentRetryBatch obj = new failedShipmentRetryBatch();
        DataBase.executeBatch(obj);

        test.stopTest();
    }
}