/******************************************************************************************
 *  Name        : Cls_StepBusinessHoursUtils 
 *  Author      : Claude Manahan
 *  Company     : NSI DMCC
 *  Date        : 2016-07-14
 *  Description : This class will operations regarding Business Hours for Steps (Step__c)
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   14-07-2016	 Claude		   Created
 V1.1	03-11-2016	 Claude		   Changed IDAMA to FOSP
*******************************************************************************************/
public without sharing class Cls_StepBusinessHoursUtils {
	
	/**
     * Number of days for business hours when using BusinessHours.Diff() method; 
     * NOTE: Transform this constant to negative to count backwards
     */
	public static final long BUSINESS_HOUR_MULTIPLIER = 32400000; 	
	
	/**
     * Number of milliseconds for an hour
     */
    public static final long BUSINESS_HOUR_MILLISECONDS = 3600000; 
    
    /**
     * Stores the business hour name
     */
    private static String businessHourName = '';
    
    /**
     * Stores the owner name
     */
    private static String ownerName = '';
    
    /**
     * Calculates the business hours between the creation 
     * date of the step until is closing date
     * @author		Claude Manahan
     * @date		06-04-2016
     * @params		steps				the step records to be processed
     */
    public static void calculateStepDuration(List<Step__c> steps){
    	
    	try {
    		
    		if(!steps.isEmpty()){
    			
    			Set<String> srStepIds = new Set<String>();
    			
    			/* Get all the SR Step Ids */
    			for(Step__c step : steps){
    				
    				if(String.isNotBlank(step.SR_Step__c)){
    					srStepIds.add(step.SR_Step__c);
    				}
    			}
    			
    			if(!srStepIds.isEmpty()){
    				
    				Map<Id,SR_Steps__c> relatedSrSteps = new Map<Id,SR_Steps__c>([SELECT Id, Owner__c FROM SR_Steps__c WHERE Id in :srStepIds]);
    				
    				for(Step__c step : steps){
    					
    					if(relatedSrSteps.containsKey(step.SR_Step__C)){
    						
    						String srStepOwner = relatedSrSteps.get(step.SR_Step__C).Owner__c;
    						
    						/* Check the owner name */
				    		checkOwner(srStepOwner);
				        		
				    		ID businessHoursId = getBusinessHoursId();
							ID BHId = String.isBlank(businessHoursId) ? id.valueOf(Label.Business_Hours_Id) : businessHoursId;
							
							/* To get the exact hours with floating values, 0.0 is added */
				            Decimal businessHourDecimal = BUSINESS_HOUR_MILLISECONDS + 0.0;
				            Decimal stepDurationDifference = BusinessHours.diff(BHId,step.CreatedDate,System.Now()) + 0.0;
							
							step.Business_Hours_Duration__c = (stepDurationDifference/businessHourDecimal).setScale(2);
    					}
    					
    				}
    			}
    			
    		}
    		
    	} catch (Exception ex){
    		System.debug('FIT-OUT : Custom Code' + ' Line Number : '+ex.getLineNumber()+'\nException is : '+ex);
    		insert LogDetails.CreateLog(null, 'FIT-OUT : Custom Code', 'Line Number : '+ex.getLineNumber()+'\nException is : '+ex); 
    	}
    	
    }
    
    /**
     * Checks the owner name
     * @params		ownerNameText			the name of the owner
     */
    public static void checkOwner(String ownerNameText){
    	
    	if(String.isNotBlank(ownerNameText)){
    	
	    	setOwnerName(ownerNameText); // Set the owner name for the calculation
	    			
			if (isOwnerIdama()){
				setBusinessHourName('FOSP'); // V1.1
			} else {
				setBusinessHourName('Fit-Out');
			}
		
    	}
    }
    
    /**
     * Gets the business hour ID based on the name
     * @params		businessHourName	the name of the business hour in the custom setting
     * @return 		businessHourId		the businesshours ID
     */
    public static Id getBusinessHoursId(){
    	return String.isNotBlank(businessHourName) && Business_Hours__c.getAll().containsKey(businessHourName) ?  Id.valueOf(Business_Hours__c.getInstance(businessHourName).Business_Hours_Id__c) : id.valueOf(Label.Business_Hours_Id);
    }
    
    /**
     * Sets the business hour name to be retrieved
     * @params		businessHourNameText	the name of the business hour in the custom setting
     */
    public static void setBusinessHourName(String businessHourNameText){
    	businessHourName = businessHourNameText;
    }
    
    /**
     * Sets the business hour name to be retrieved
     * @params		ownerNameText	the name of the owner
     */
    public static void setOwnerName(String ownerNameText){
    	ownerName = ownerNameText;
    }
    
    /*
     * Checks if the owner is part of IDAMA Queue
     * @return		isOwnerDifc		checks if the owner is part of IDAMA group
     */ 
    public static Boolean isOwnerIdama(){
    	return hasOwnerText('FOSP'); // V1.1
    }
    
    /*
     * Checks if the owner is part of the designated group
     * @return		isOwnerDifc		checks if the owner is part of IDAMA group
     */ 
    public static Boolean hasOwnerText(String ownerText){
    	return String.isNotBlank(ownerName) && ownerName.contains(ownerText);
    }
}