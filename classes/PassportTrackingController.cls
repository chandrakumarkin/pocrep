/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 15/12/2019
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    15/12/2019  shoaib    Created
V2.0    01/04/2021  Mari    Created

**********************************************************************************************************************/
public class PassportTrackingController {
    static String receptionId = '';
      /**
     * Helper method to fetch the passport for tracking 
     * @param  searchValue,searchBy. 
     * @return List of tasks.          
     */
    @AuraEnabled
     public static List <PassportTrackingWrapper> getwrapperList(String searchValue) {
         
         Set<String> duplicateCheck = new Set<String>();
         
         List<PassportTrackingWrapper> thisWrapperList = new List<PassportTrackingWrapper>();
         
         String queryString = 'SELECT Id,client_Name__c,Step__c,Applicant_Name__c,SRType__c,Passport_Number__c,SR_Number__c,Status__c FROM Passport_Tracking__c';
         
         if(!Test.isRunningTest())  { receptionId = [SELECT Id From Profile WHERE Name=: System.Label.Document_Tracking_Profile].Id;}
         
         queryString  =queryString+ ' WHERE Status__c != \'Customer Collected\' AND Has_Status_Updated__c=FALSE ';
       
         if(String.isNotBlank(searchValue)){
             string newValue = String.valueOf(searchValue.trim());
             if(searchValue.contains(' ')){
                queryString =queryString+ ' AND status__c = \''+newValue+ '\''; 
             }
             else{
                queryString =queryString+ ' AND SR_Number__c like \'%'+ string.escapeSingleQuotes(newValue) + '%\'';  
             }
                                    
         }
         
         
         queryString = queryString+ ' order by CreatedDate';
         
         for(Passport_Tracking__c thisTracking : Database.query(queryString)){
             thisWrapperList.add(new PassportTrackingWrapper(thisTracking));
          }
         return thisWrapperList;
     }
    
     /**
     * Helper method to update status of passport for tracking 
     * @param  searchValue,searchBy. 
     * @return List of tasks.          
     */
    @AuraEnabled
     public static string updatePassportTracking(String selectedId,String selectedValue) {
         Map<String,String> newDataMap                       = new Map<String,String>();
         List<Passport_Tracking__c> previousPassportTracking = new List<Passport_Tracking__c>();
         
         List<Object> fieldList = (List<Object>)JSON.deserializeUntyped(selectedId);
          for(Object thisObject : fieldList){ 
              Map<String,Object> dataMap = (Map<String,Object>)thisObject;
              String Ids             = (String)dataMap.get('Id');
              String srNumber        = (String)dataMap.get('SR_Number__c');
              String passportNumber  = (String)dataMap.get('Passport_Number__c');
              String clientName      = (String)dataMap.get('Client_Name__c');
              String stepname        = (String)dataMap.get('Step__c');
              String srType          =  (String)dataMap.get('SRType__c');
              String applicantName           =  (String)dataMap.get('Applicant_Name__c');
              
              
              previousPassportTracking.add(new Passport_Tracking__c(Id =Ids,
                                                                    SR_Number__c = srNumber,
                                                                    Passport_Number__c = passportNumber,
                                                                    client_Name__c = clientName,
                                                                    Step__c = stepname,
                                                                    SRType__c = srType,
                                                                    Applicant_Name__c=applicantName));
          }
         
         List<Passport_Tracking__c> newPassportTrackingList       = new List<Passport_Tracking__c>();
         List<Passport_Tracking__c> updatePassportTrackingList       = new List<Passport_Tracking__c>();
         
         try{
             for(Passport_Tracking__c previousPassport: previousPassportTracking){
                  previousPassport.Has_Status_Updated__c    = true;
                 
                  Passport_Tracking__c newPassportTracking = previousPassport.clone(false, true);
                  newPassportTracking.Status__c            = selectedValue;
                 if(selectedValue == 'Received by Office Boy')  {
                     newPassportTracking.Has_Status_Updated__c = true;
                 }else{
                     newPassportTracking.Has_Status_Updated__c = false;
                 }
                 
                  newPassportTrackingList.add(newPassportTracking); 
                  updatePassportTrackingList.add(previousPassport);
             }
             if(!updatePassportTrackingList.isEmpty()){ 
                update updatePassportTrackingList;
              }
             if(!newPassportTrackingList.isEmpty()){
                 insert newPassportTrackingList;
             }
             return 'Success : Record Status Updated!';
         }
         catch(Exception ex){
              Log__c objLog = new Log__c();
              objLog.Type__c = 'Document Tracking';
              objLog.Description__c = ex.getMessage() + ' at line ' + ex.getLineNumber() + ' of class PassportTrackingController';
              insert objLog; 
              return 'error occured' +ex.getMessage();
         }
     }
    
    public class PassportTrackingWrapper{
        @AuraEnabled
        public Passport_Tracking__c passportTrackingList {get;set;}
        @AuraEnabled
        public Boolean isChecked  {get;set;}
        @AuraEnabled
        public Boolean isReceptionId  {get;set;}
        
        public PassportTrackingWrapper(Passport_Tracking__c passportTrackingList){
           this.passportTrackingList = passportTrackingList;
           
          if(UserInfo.getProfileId() == receptionId){
             this.isReceptionId   = true;
           }
        }
    }
    
     /**
     * V2.0 changes
     * Helper method to fetch the SRs for tracking 
     * @param  SR Number
     * @return List of SR's.          
     */
    
    @AuraEnabled
    public static List <Service_Request__c> fetchSRList(String searchSRnumber) {
         
        system.debug('====searchSRnumber========'+searchSRnumber);
      
        List<Service_Request__c>srList = new List<Service_Request__c>();
        try{
            //string queryString = 'select Id,Name,External_Status_Name__c,Customer__c,Customer__r.Name,SR_Group__c,Internal_Status_Name__c,Service_Type__c from Service_Request__c ';
            string queryString = UtilitySoqlHelper.allFieldswithParentFields('Service_Request__c','Customer__r.Name');
            searchSRnumber = String.valueOf(searchSRnumber.trim());
            if(searchSRnumber != ''){ 
                queryString+=' where Name =:searchSRnumber order by CreatedDate limit 50000';
                system.debug('====queryString ========'+queryString );
                srList =Database.query(queryString);
                system.debug('====srList ========'+srList );
            }
        }catch(Exception ex) {
            throw new AuraHandledException(ex.getMessage());
        }
        return srList; 
     }
}