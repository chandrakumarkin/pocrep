/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 02/25/2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    02/25/2021  shoaib    Created
**********************************************************************************************************************/
public without sharing class HealthInsuranceUploadCheck {
    
    @InvocableMethod(label='HealthInsuranceUpdate' description='HealthInsuranceUpdate') 
    
    public static void HealthInsuranceUpdate(List<Id> srdocIds){

		Set<Id> SRid = new Set<Id>();
		
		For(SR_Doc__c thisDoc :[SELECT Id,
		                                Service_Request__c
										FROM SR_Doc__c 
										WHERE Id IN : srdocIds ]) {

			SRid.add(thisDoc.Service_Request__c);
		}

	   List<Service_request__c> thisServiceRequest = [SELECT Id,
		                                                      External_SR_Status__c,
                                                              Internal_SR_Status__c
		                                                    FROM Service_request__c 
															WHERE Id IN : SRid ];

		List<Step__c> stepList = new List<Step__c>();
        List<Step__c> OriginalPassportStep = new List<Step__c>([SELECT Id,
		                   Step_Name__c,
						   Status_Code__c 
						FROM Step__c
						WHERE SR__C IN : SRid 
						AND Step_Name__c='Original Passport Received']);
		
		stepList = [SELECT Id,
		                   Step_Name__c,
						   Status_Code__c 
						FROM Step__c
						WHERE SR__C IN : SRid 
						AND Step_Name__c='Medical Fitness Test Completed'];

	   if(!stepList.isempty() 
	   			&&  stepList[0].Status_Code__c =='CLOSED'
                && OriginalPassportStep.size() == 0){

			thisServiceRequest[0].External_SR_Status__c = system.label.Medical_fitness_certificate_received_from_DHA;
			thisServiceRequest[0].Internal_SR_Status__c = system.label.Medical_fitness_certificate_received_from_DHA;
	   }

	   update thisServiceRequest;

	}

}