/*
 Author      : Durga Prasad
 Date        : 09-Oct-2019
 Description : Helper class to calculate risk and save the details on Application
 ----------------------------------------------------------------------------------
 */
public without sharing class OB_RiskMatrixHelper {
	public class RiskResult {
		public boolean bCriteriaMet = false;
		public string RiskRating;
		public decimal RiskPercentage = 0;
	}
	public static boolean bHasDNFPB = false;
	public static string CalculateRisk(string SRID) 
    {
		string strResult;
		string LicenseId;
		string RecordType;
		bHasDNFPB = false;
		for(HexaBPM__Service_Request__c objSR :[Select Id, License_Application__c, HexaBPM__Record_Type_Name__c from HexaBPM__Service_Request__c where Id = :SRID]) {
			LicenseId = objSR.License_Application__c;
			RecordType = objSR.HexaBPM__Record_Type_Name__c;
		}
		system.debug('LicenseId==>' + LicenseId);
		system.debug('RecordType==>' + RecordType);
		if(RecordType == 'In_Principle') {
			OB_RiskMatrixHelper.RiskResult objresult = OB_RiskMatrixHelper.PrimaryChecks(SRID);
			system.debug('objresult==>' + objresult);
			HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id = SRID);
			objSR.DNFBP__c = bHasDNFPB;
			if(!objresult.bCriteriaMet) {
				decimal TotalRating = 0;
				decimal LicenseHighestRating = OB_RiskMatrixHelper.LicenseHighestRiskRate(SRID);

				system.debug('LicenseHighestRating==>' + LicenseHighestRating);

				decimal AmendmentCountryHighestRating = OB_RiskMatrixHelper.AmendmentHighestRiskRate(SRID);

				system.debug('AmendmentCountryHighestRating==>' + AmendmentCountryHighestRating);

				decimal OtherRiskRateTotal = OB_RiskMatrixHelper.OtherRiskRateTotal(SRID);

				system.debug('OtherRiskRateTotal==>' + OtherRiskRateTotal);

				decimal AmndIncomeSourceHighestRiskRate = OB_RiskMatrixHelper.AmendmentIncomeSourceHighestRiskRate(SRID);

				system.debug('AmndIncomeSourceHighestRiskRate==>' + AmndIncomeSourceHighestRiskRate);

				TotalRating = LicenseHighestRating + AmendmentCountryHighestRating + OtherRiskRateTotal + AmndIncomeSourceHighestRiskRate;

				system.debug('TotalRating==>' + TotalRating);

				objSR.Risk_Percentage__c = TotalRating;

				if(TotalRating <= 30)
					objSR.Risk__c = 'Low';
				else if(TotalRating > 30 && TotalRating <= 70)
					objSR.Risk__c = 'Medium';
				else 
					objSR.Risk__c = 'High';
			}else{
				objSR.Risk__c = objresult.RiskRating;
				objSR.Risk_Percentage__c = objresult.RiskPercentage;
			}
			system.debug('objSR.Risk__c==>' + objSR.Risk__c);
			update objSR;
			strResult = objSR.Risk__c;
		}
		return strResult;
	}
	public static RiskResult PrimaryChecks(string SRID) {
		OB_RiskMatrixHelper.RiskResult objresult = new OB_RiskMatrixHelper.RiskResult();

		boolean hasDNFPB = OB_RiskMatrixHelper.hasDNFPB(SRID);
		bHasDNFPB = hasDNFPB;
		boolean hasIndPEP = OB_RiskMatrixHelper.hasIndividualPEP(SRID);
		boolean hasDIFCRegisteredBodyCorporate = OB_RiskMatrixHelper.hasDIFCRegisteredBodyCorporate(SRID);
		boolean hasRecExch_FinSecRegulator = OB_RiskMatrixHelper.hasRecExch_FinSecRegulator(SRID);

		system.debug('hasDIFCRegisteredBodyCorporate===>' + hasDIFCRegisteredBodyCorporate);
		system.debug('hasRecExch_FinSecRegulator===>' + hasRecExch_FinSecRegulator);
		//system.debug('hasDIFCRegisteredBodyCorporate===>'+hasDIFCRegisteredBodyCorporate);
		//system.debug('hasDIFCRegisteredBodyCorporate===>'+hasRecExch_FinSecRegulator);

		if(hasDNFPB || hasIndPEP || hasDIFCRegisteredBodyCorporate || hasRecExch_FinSecRegulator) {
			objresult.bCriteriaMet = true;
			if(hasDNFPB) {
				objresult.RiskRating = 'Not Applicable';
				objresult.RiskPercentage = 0;
			} else {
				if(hasIndPEP) {
					objresult.RiskRating = 'High';
					objresult.RiskPercentage = 20;
				} else {
					objresult.RiskRating = 'Low';
					objresult.RiskPercentage = 5;
				}
			}
		}
		system.debug('hasIndPEP===>' + hasIndPEP);
		system.debug('hasDNFPB===>' + hasDNFPB);
		system.debug('hasDIFCRegisteredBodyCorporate===>' + hasDIFCRegisteredBodyCorporate);
		system.debug('hasDIFCRegisteredBodyCorporate===>' + hasRecExch_FinSecRegulator);
		return objresult;
	}
	public static boolean hasDIFCRegisteredBodyCorporate(string SRID) {
		boolean IsDIFCRegEntity = false;
		/*for(HexaBPM_Amendment__c amnd :[Select Id from HexaBPM_Amendment__c where ServiceRequest__c = :SRID and Recordtype.DeveloperName = 'Qualified_Applicant' and Is_this_Entity_registered_with_DIFC__c = 'Yes' and Status__c != 'Inactive' limit 1]) {
			IsDIFCRegEntity = true;
		}*/
        for(HexaBPM_Amendment__c amnd :[Select Id from HexaBPM_Amendment__c where ServiceRequest__c = :SRID and Role__c includes ('Qualified Applicant') and Is_this_Entity_registered_with_DIFC__c = 'Yes' and Status__c != 'Inactive' limit 1]) {
			IsDIFCRegEntity = true;
		}
        
		return IsDIFCRegEntity;
	}
	public static boolean hasIndividualPEP(string SRID) {
		boolean hasIndPEP = false;
		set<string> setCountriesExcluded = new set<string>{ 'United Arab Emirates', 'Kuwait', 'Saudi Arabia', 'Bahrain', 'Oman' };
		for(HexaBPM_Amendment__c amnd :[Select Id from HexaBPM_Amendment__c where ServiceRequest__c = :SRID and Is_this_individual_a_PEP__c = 'Yes' and Recordtype.DeveloperName = 'Individual' and Nationality_list__c NOT IN :setCountriesExcluded and Status__c != 'Inactive' limit 1]) {
			hasIndPEP = true;
		}
		return hasIndPEP;
	}
	public static boolean hasDNFPB(string SRID) {
		boolean hasDNFBP = false;
		if(SRID != null) {
			//string strLicenseId;
			for(HexaBPM__Service_Request__c objSR :[Select Id, Entity_Type__c, Business_Sector__c, License_Application__c, Buying_Selling_Real_Estate_Property__c, Dealing_in_Precious_Metals__c from HexaBPM__Service_Request__c where Id = :SRID]) {
				//strLicenseId = objSR.License_Application__c;
				if(objSR.Entity_Type__c == 'Financial - related' || (objSR.Entity_Type__c == 'Non - financial' && (objSR.Business_Sector__c == 'Investment Fund' || objSR.Business_Sector__c == 'General Partner for a Limited Partnership Fund')))
					hasDNFBP = true;
				if(objSR.Entity_Type__c == 'Retail' && objSR.Dealing_in_Precious_Metals__c == 'Yes') {
					hasDNFBP = true;
				}
				if(objSR.Entity_Type__c == 'Non - financial' && objSR.Buying_Selling_Real_Estate_Property__c == 'Yes') {
					hasDNFBP = true;
				}
			}
			if(!hasDNFBP){
				for(License_Activity__c LA :[Select Id, Activity__r.OB_DNFBP__c, Activity__r.Activity_Code__c from License_Activity__c where Application__c=:SRID and Activity__r.OB_DNFBP__c = 'Yes' limit 1]) {
					hasDNFBP = true;
				}
			}
		}
		return hasDNFBP;
	}
	public static boolean hasRecExch_FinSecRegulator(string SRID) {
		boolean hasRecExch_FinSecRegulator = false;
		for(HexaBPM_Amendment__c amnd :[Select Id from HexaBPM_Amendment__c where ServiceRequest__c = :SRID and((Reason_for_exemption_under_DIFC_UBO__c = 'Government entity' OR Reason_for_exemption_under_DIFC_UBO__c = 'Wholly owned by a government') OR(Recognised_Exchange__c != null and Recognised_Exchange__c != 'Other') OR(Financial_Service_Regulator__c != null and Financial_Service_Regulator__c != 'Other')) and Status__c != 'Inactive' limit 1]) {
			hasRecExch_FinSecRegulator = true;
		}
		return hasRecExch_FinSecRegulator;
	}
	public static decimal LicenseHighestRiskRate(string SRID) {
		decimal HighestRate = 0;
		if(SRID != null) {
			for(License_Activity__c LA :[Select Id, Activity__r.OB_Risk__c from License_Activity__c where Application__c=:SRID and Activity__r.OB_Risk__c != null order by Activity__r.OB_Risk__c desc limit 1]) {
				HighestRate = LA.Activity__r.OB_Risk__c;
			}
		}
		return HighestRate;
	}
	public static decimal AmendmentHighestRiskRate(string SRID) {
		decimal HighestRate = 0;
		if(SRID != null) {
			map<string, decimal> MapCountryRisk = new map<string, decimal>();
			for(Country_Risk_Scoring__c objCRS :[Select Id, Name,Risk_Rating__c,Risk_Score_Percentage__c, Source_of_Income_Risk__c, Source_of_Income_Risk_Rating__c from Country_Risk_Scoring__c where Risk_Rating__c != null and Risk_Score_Percentage__c != null]) {
				MapCountryRisk.put(objCRS.Name.tolowercase(), objCRS.Risk_Score_Percentage__c);
			}
			
			for(HexaBPM_Amendment__c amnd :[Select Id, Risk__c,Nationality_list__c,Country_of_Registration__c,Existing_Entity_Country__c from HexaBPM_Amendment__c where ServiceRequest__c = :SRID and (Nationality_list__c != null or Country_of_Registration__c != null or Existing_Entity_Country__c!=null) and Status__c != 'Inactive']) {
				string Country='';
				if(amnd.Country_of_Registration__c!=null)
					Country = amnd.Country_of_Registration__c.tolowercase();
				if(amnd.Nationality_list__c!=null)
					Country = amnd.Nationality_list__c.tolowercase();
				if(amnd.Existing_Entity_Country__c!=null)
					Country = amnd.Existing_Entity_Country__c.tolowercase();
					
				if(Country!='' && MapCountryRisk.get(Country)!=null && HighestRate < MapCountryRisk.get(Country))
					HighestRate = MapCountryRisk.get(Country);
					
				/*
				system.debug('amnd.Risk__c==>' + amnd.Risk__c);
				if(amnd.Risk__c != null)
					HighestRate = amnd.Risk__c / 100;
				*/
			}
		}
		return HighestRate;
	}
	public static decimal AmendmentIncomeSourceHighestRiskRate(string SRID) {
		decimal HighestRate = 0;
		if(SRID != null) {
			map<string, decimal> MapCountryRisk = new map<string, decimal>();
			for(Country_Risk_Scoring__c objCRS :[Select Id, Name,Risk_Rating__c,Risk_Score_Percentage__c, Source_of_Income_Risk__c, Source_of_Income_Risk_Rating__c from Country_Risk_Scoring__c where Risk_Rating__c != null and Risk_Score_Percentage__c != null]) {
				MapCountryRisk.put(objCRS.Name.tolowercase(), objCRS.Risk_Score_Percentage__c);
			}
			system.debug('MapCountryRisk==>'+MapCountryRisk);
			for(HexaBPM__Service_Request__c objSR:[Select Id,Country_of_Source_Funds__c from HexaBPM__Service_Request__c where Id=:SRID]){
				system.debug('objSR.Country_of_Source_Funds__c==>'+objSR.Country_of_Source_Funds__c);
				list<string> lstCountries = new list<string>();
				if(objSR.Country_of_Source_Funds__c!=null){
					if(objSR.Country_of_Source_Funds__c.indexof(';') >-1)
						lstCountries = objSR.Country_of_Source_Funds__c.split(';');
					else 
						lstCountries.add(objSR.Country_of_Source_Funds__c);
				}
				for(string country :lstCountries) {
					if(MapCountryRisk.get(country.tolowercase()) != null && MapCountryRisk.get(country.tolowercase()) > HighestRate)
						HighestRate = MapCountryRisk.get(country.tolowercase());
				}
			}
			system.debug('HighestRate==>'+HighestRate);
			/*
			for(HexaBPM_Amendment__c amnd :[Select Id, Country_of_source_of_income__c from HexaBPM_Amendment__c where ServiceRequest__c = :SRID and Country_of_source_of_income__c != null and Status__c != 'Inactive']) {
				list<string> lstCountries = new list<string>();
				if(amnd.Country_of_source_of_income__c.indexof(';') >-1)
					lstCountries = amnd.Country_of_source_of_income__c.split(';');
				else 
					lstCountries.add(amnd.Country_of_source_of_income__c);
				for(string country :lstCountries) {
					if(MapCountryRisk.get(country.tolowercase()) != null && MapCountryRisk.get(country.tolowercase()) > HighestRate)
						HighestRate = MapCountryRisk.get(country.tolowercase());
				}
			}
			*/
		}
		return HighestRate;
	}

	public static decimal OtherRiskRateTotal(string SRID) {
		decimal HighestRate = 0;
		decimal HighestCSRating = 0;
		decimal HighestParentBussActRating = 0;
		decimal HighestDist_Chnl_Country_Score = 0;
		if(SRID != null) {
			list<string> lstControlStructures = new list<string>();
			list<string> lstParentBA = new list<string>();
			list<string> lstDistChnlCountries = new list<string>();
			for(HexaBPM__Service_Request__c objSR :[Select Id, Ownership_Control_Structures_Include__c, Parent_or_Entity_Business_Activities__c, Distribution_Channel_Countries__c from HexaBPM__Service_Request__c where Id = :SRID]){
				//Ownership_Control_Structures_Include__c with the highest score (out of 10)
				if(objSR.Ownership_Control_Structures_Include__c != null) {
					if(!objSR.Ownership_Control_Structures_Include__c.contains('Not Applicable')) {
						if(objSR.Ownership_Control_Structures_Include__c.indexof(';') >-1)
							lstControlStructures = objSR.Ownership_Control_Structures_Include__c.split(';');
						else 
							lstControlStructures.add(objSR.Ownership_Control_Structures_Include__c);
					}
				}
				//highest value of  selected Parent_or_Entity_Business_Activities__c  (out of 10)
				if(objSR.Parent_or_Entity_Business_Activities__c != null) {
					if(!objSR.Parent_or_Entity_Business_Activities__c.contains('Not Applicable')) {
						if(objSR.Parent_or_Entity_Business_Activities__c.indexof(';') >-1)
							lstParentBA = objSR.Parent_or_Entity_Business_Activities__c.split(';');
						else 
							lstParentBA.add(objSR.Parent_or_Entity_Business_Activities__c);
					}
				}
				//highest risk value of  Distribution_Channel_Countries__c (out of 20)
				if(objSR.Distribution_Channel_Countries__c != null) {
					if(objSR.Distribution_Channel_Countries__c.indexof(';') >-1)
						lstDistChnlCountries = objSR.Distribution_Channel_Countries__c.split(';');
					else 
						lstDistChnlCountries.add(objSR.Distribution_Channel_Countries__c);
				}
			}
			
			//Ownership_Control_Structures_Include__c with the highest score (out of 10)
			if(lstControlStructures.size() > 0) {
				map<string, decimal> MapCSRisk = new map<string, decimal>();
				if(OB_CS_Risk_Score__c.GetAll() != null) {
					for(OB_CS_Risk_Score__c objcs :OB_CS_Risk_Score__c.GetAll().values()) {
						MapCSRisk.put(objcs.Ownership_Control_Structures__c.tolowercase(), objcs.Risk__c);
					}
				}
				for(string strCS :lstControlStructures) {
					if(MapCSRisk.get(strCS.tolowercase()) != null && MapCSRisk.get(strCS.tolowercase()) > HighestCSRating)
						HighestCSRating = MapCSRisk.get(strCS.tolowercase());
				}
			}
			//highest value of  selected Parent_or_Entity_Business_Activities__c  (out of 10)
			if(lstParentBA.size() > 0) {
				map<string, decimal> MapBARisk = new map<string, decimal>();
				if(OB_Business_Activity_Score__c.GetAll() != null) {
					for(OB_Business_Activity_Score__c objcs :OB_Business_Activity_Score__c.GetAll().values()) {
						MapBARisk.put(objcs.Parent_or_Entity_Business_Activity__c.tolowercase(), objcs.Risk__c);
					}
				}
				for(string strBA :lstParentBA) {
					if(MapBARisk.get(strBA.tolowercase()) != null && MapBARisk.get(strBA.tolowercase()) > HighestParentBussActRating)
						HighestParentBussActRating = MapBARisk.get(strBA.tolowercase());
				}
			}
			//highest risk value of  Distribution_Channel_Countries__c (out of 20)
			if(lstDistChnlCountries.size() > 0) {
				map<string, decimal> MapDistChannelRisk = new map<string, decimal>();
				if(OB_Dist_Chnl_Country_Score__c.GetAll() != null) {
					for(OB_Dist_Chnl_Country_Score__c objcs :OB_Dist_Chnl_Country_Score__c.GetAll().values()) {
						MapDistChannelRisk.put(objcs.Distribution_Channel_Country__c.tolowercase(), objcs.Risk__c);
					}
				}
				for(string strChnl :lstDistChnlCountries) {
					if(MapDistChannelRisk.get(strChnl.tolowercase()) != null && MapDistChannelRisk.get(strChnl.tolowercase()) > HighestDist_Chnl_Country_Score)
						HighestDist_Chnl_Country_Score = MapDistChannelRisk.get(strChnl.tolowercase());
				}
			}
			HighestRate = HighestCSRating + HighestParentBussActRating + HighestDist_Chnl_Country_Score;
		}
		return HighestRate;
	}


}