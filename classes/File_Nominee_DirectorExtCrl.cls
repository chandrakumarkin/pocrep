//Class created part for #6434 supports 2 SR

public with sharing class File_Nominee_DirectorExtCrl {

    public Service_Request__c SRData { get; set;}
    public String JobTitle { get; set; }
    public string RecordTypeId;
    public map < string, string > mapParameters;
    public List<Amendment__c> ListAmendment { get; set; }
    public Amendment__c AmendmentObj { get;set; }
    public Attachment ObjAttachment { get;set; }
    public boolean SRisValid { get;set; }
    public boolean ShowDetailSection { get;set; }
    public boolean ShowDetailEditButton { get;set; }
    private Map < string, id > RecordTypeIdsUBO;
    public Account ObjAccount { get;set; }

    private string Rtype; //sR Record in URL 
    private String RequiredRel; // Type of rel records required 
    public String AmendmentId {get;set;}

    public File_Nominee_DirectorExtCrl(ApexPages.StandardController controller) {
        
        AmendmentId ='';
        ShowDetailSection = false;
        ShowDetailEditButton = false;
        RecordTypeIdsUBO = new Map < string, id > ();
        for (RecordType objRT: [SELECT Id, Name, DeveloperName 
                                FROM RecordType 
                                WHERE Name = 'Individual' 
                                AND SobjectType = 'Amendment__c' ]) 
        {
            RecordTypeIdsUBO.put(objRT.DeveloperName, objRT.id);
        }

        SRisValid = true;
        SRData = (Service_Request__c) controller.getRecord();
        
        Service_Request__c SRDataObj;
        if( SRData.Id != null ){
            SRDataObj = [SELECT Record_Type_Name__c, RecordTypeId FROM Service_Request__c WHERE Id =: SRData.Id];
        }

        ObjAccount = new account();
        ObjAttachment = new Attachment();
        if (apexpages.currentPage().getParameters() != null)
            mapParameters = apexpages.currentPage().getParameters();
        if (mapParameters.get('RecordType') != null)
            RecordTypeId = mapParameters.get('RecordType');

        if (mapParameters.get('type') != null){
            Rtype = mapParameters.get('type');
        }else if( SRDataObj != null){
            Rtype = SRDataObj.Record_Type_Name__c;
            RecordTypeId = SRDataObj.RecordTypeId;
        }
            
        if (Rtype == 'Shareholder_holding_shares_on_trust')
            RequiredRel = 'Is Shareholder Of';
        else
            RequiredRel = 'Has Director';
        
        

        ListAmendment = new List<Amendment__c>();
        AmendmentObj = new Amendment__c();
        AddAmmendment();

    }

    public List < SelectOption > UserList {
        get {
            UserList = new List < SelectOption > ();
            for (Relationship__c temp: [SELECT id, Object_Contact__c, Contact_Name__c,Object_Account__r.Name
                                        FROM Relationship__c 
                                        WHERE Relationship_Type__c =: RequiredRel 
                                        AND Subject_Account__c =: SRData.Customer__c 
                                        AND Active__c = true]) 
            {
            	//Below Logic Updated by Sai: Logic is to add body corporate shareholders
                if(temp.Object_Contact__c != null){
                	UserList.add(new SelectOption(temp.Contact_Name__c, temp.Contact_Name__c));
                }
                if(RequiredRel == 'Is Shareholder Of' && temp.Object_Account__r.Name !=null){
                	UserList.add(new SelectOption(temp.Object_Account__r.Name, temp.Object_Account__r.Name));
                }
            }
            return UserList;
        }
        set;
    }

    public PageReference SaveRecord() {

        if( ListAmendment.size() > 0 ){
            try {
            
                upsert SRData;
                for( Amendment__c AmmendmentObj : ListAmendment )
                {
                    AmmendmentObj.ServiceRequest__c = SRData.id;
                }
                
                upsert ListAmendment;

                PageReference acctPage = new ApexPages.StandardController(SRData).view();
                acctPage.setRedirect(true);
                return acctPage;

            } catch (Exception e) { system.debug( e.getMessage() );  /* //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());  //ApexPages.addMessage(myMsg); */ }
        }else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Please add details before saving.');
            ApexPages.addMessage(myMsg);
        }
        return null;
    }

    public void AddAmmendmentObj(){
        AmendmentObj.Job_Title__c = JobTitle;
        AmendmentObj = AddRecod( AmendmentObj );
    }

    public void UpdateAmmendment(){
        if(AmendmentId != null && AmendmentId != ''){
        
            Amendment__c UpdateAmendmentObj = AmendmentObj;
            UpdateAmendmentObj.Id = AmendmentId;
            update UpdateAmendmentObj;

            ListAmendment = FetchAmendmentRecord();

            ShowDetailEditButton = false;
            ShowDetailSection = false;
        }
    }

    public void AddRow(){
        ShowDetailSection = true;
        AmendmentObj = new Amendment__c();
    }

    public void RemoveAmmendment(){
        system.debug( AmendmentId );
        if(AmendmentId != null && AmendmentId != ''){
            delete new Amendment__C(Id = AmendmentId);
            ListAmendment = FetchAmendmentRecord();
        }
    }

    public void CancelAmmendmentObj(){
        ShowDetailEditButton = false;
        ShowDetailSection = false;
    }

    public void EditAmmendment(){
        if( AmendmentId != null && AmendmentId != ''){
            try{
                AmendmentObj = [SELECT id, Job_Title__c, Gender__c, Customer__c,Director_Appointed_Date__c , Title_new__c, Date_of_Birth__c, Family_Name__c, Place_of_Birth__c, Middle_Name__c, Nationality_list__c,
                                                            Given_Name__c, Passport_No__c, Person_Email__c, Passport_Expiry_Date__c, Mobile__c,
                                                            Phone__c, Apt_or_Villa_No__c, Building_Name__c, PO_Box__c, Street__c, Post_Code__c, Emirate_State__c, Permanent_Native_Country__c,
                                                            Permanent_Native_City__c, Status__c, ServiceRequest__c 
                                                            FROM Amendment__c 
                                                            WHERE Id =: AmendmentId ];
            }catch (Exception e) { system.debug( e.getMessage() );  /* //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());  //ApexPages.addMessage(myMsg); */ }

            ShowDetailEditButton = true;
            ShowDetailSection = true;        
            
        }
    }

    public void AddAmmendment(){

        //Amendment__c ObjAmendment = new Amendment__c();

        for (User objUsr: [SELECT id, ContactId, Email, Contact.Account.Legal_Type_of_Entity__c, Phone, Contact.AccountId, Contact.Account.Company_Type__c, Contact.Account.Next_Renewal_Date__c,
                                Contact.Account.Name, Contact.Account.Sector_Classification__c, Contact.Account.License_Activity__c, Contact.Account.Is_Foundation_Activity__c
                            FROM User 
                            WHERE Id =: userinfo.getUserId() ]) 
        {

            if (SRData.id == null) {

                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId = RecordTypeId;
                SRData.Email__c = objUsr.Email;
                // SRData.I_am_Exempt_entities__c='Yes';
                SRData.Legal_Structures__c = objUsr.Contact.Account.Legal_Type_of_Entity__c;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;

                //  SRData.Customer__r=ObjAccount;
                SRData.Entity_Name__c = objUsr.Contact.Account.Name;
                AmendmentObj.RecordTypeId = RecordTypeIdsUBO.get('Individual');
                AmendmentObj.Amendment_Type__c = 'Individual';
                AmendmentObj.Relationship_Type__c = 'Nominee Director';

                AmendmentObj.Status__c = 'Draft';
                ObjAccount = objUsr.Contact.Account;

            } else {
                ListAmendment = FetchAmendmentRecord();   
            }
        }
    }

    public List<Amendment__c> FetchAmendmentRecord(){
        List<Amendment__c> ListToReturn = new List<Amendment__c>();
        for( Amendment__c TempAmendmentObj : [SELECT id, Job_Title__c,RecordTypeId ,Amendment_Type__c ,Name_of_incorporating_Shareholder__c ,Director_Appointed_Date__c, Gender__c, Customer__c, Title_new__c, Date_of_Birth__c, Family_Name__c, Place_of_Birth__c, Middle_Name__c, Nationality_list__c,
                                                        Given_Name__c, Passport_No__c, Person_Email__c, Passport_Expiry_Date__c, Mobile__c,
                                                        Phone__c, Apt_or_Villa_No__c, Building_Name__c, PO_Box__c, Street__c, Post_Code__c, Emirate_State__c, Permanent_Native_Country__c,
                                                        Permanent_Native_City__c, Status__c, ServiceRequest__c 
                                                        FROM Amendment__c 
                                                        WHERE ServiceRequest__c =: SRData.id ])
        {
            ListToReturn.add( TempAmendmentObj );
        }
        return ListToReturn;
    }

    public Amendment__c AddRecod( Amendment__c ParamAmendmentObj){
        try {
            if( SRData.Id == null ){
                upsert SRData;
            }
            try {
                ParamAmendmentObj.ServiceRequest__c = SRData.id;
                
                ParamAmendmentObj.Name_of_incorporating_Shareholder__c = ParamAmendmentObj.Job_Title__c;//Arun Added this code 
                ParamAmendmentObj.Amendment_Type__c = 'Individual';//arun Added this code 
                ParamAmendmentObj.RecordTypeId = RecordTypeIdsUBO.get('Individual');//arun Added this code 
                upsert ParamAmendmentObj;
                
                ListAmendment.add( ParamAmendmentObj );
                AmendmentObj = new Amendment__c();
                ShowDetailSection = false;
          
            } catch (Exception e) { system.debug( e.getMessage() );  /* //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());  //ApexPages.addMessage(myMsg); */ }

        } catch (Exception e) { system.debug( e.getMessage() ); /* //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());  //ApexPages.addMessage(myMsg); */ }
        
        return ParamAmendmentObj;
    }
}