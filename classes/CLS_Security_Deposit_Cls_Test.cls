@isTest(seealldata=false)
private class CLS_Security_Deposit_Cls_Test{

     static testMethod void myUnitTest6(){
          
            map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration','Body_Corporate_Tenant','Landlord','Lease_Unit')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        

 
    for(Service_Request__c Objlist :[select id,Unit__c from Service_Request__c where createddate=this_year and RecordTypeid=:mapRecordType.get('Lease_Registration') and External_Status_Name__c='Approved' limit 3] )
    {
        System.debug('===Objlist==>'+Objlist);
           Security_Deposit_Cls ObjD=new Security_Deposit_Cls();
               ObjD.BP_creation(Objlist);
    }
          
      }
      
         
    public static Account objAccount;
    public static Service_Request__c objSR;
    public static Step__c objStep;
    public static Amendment__c amndRemCon;
    public static Unit__c objUnit;
    public static Lease__c objLease;
    public static Tenancy__c objTen;
    public static Building__c objBuilding;
    public static Amendment__c objAmendLandlord;
    public static Amendment__c objAmendTenant;
    public static Contact objContact;
    public static Lease_Partner__c objLP;
     public static map<string,string> mapRecordType = new map<string,string>();
      
          static testMethod void RORPCreateTenants() {
          
          mapRecordType = new map<string,string>();
        
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration','Surrender_Termination_of_Lease_and_Sublease','Freehold_Transfer_Registration','Company_Buyer','Developer','Individual_Buyer','Company_Seller','RORP_Authorized_Signatory','RORP_Shareholder','RORP_Shareholder_Company','RORP_Contact')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Lookup__c objDev = new Lookup__c();
        objDev.Name = 'DAMAC Properties';
        objDev.BP_No__c = '000123456';
        objDev.Code__c = '00000sss001';
        objDev.Type__c = 'Developer';
        insert objDev;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        objBuilding.Developer__c = 'DAMAC Properties';
        objBuilding.Developer_No__c = '00000001';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);        
        insert lstUnits;
        
        objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordType.get('Freehold_Transfer_Registration');
        objSR.Building__c = objBuilding.Id;
        objSR.Unit__c = lstUnits[0].Id;
        objSR.Ownership_type__c='Joint Tenant';
        objSR.SR_group__c='RORP';
        objSR.Email__c = 'test@difc.ae';
        insert objSR;
        
        list<Amendment__c> amendlist = new list<Amendment__c> ();
        
        Amendment__c amnd = new Amendment__c();
        amnd.RecordTypeId = mapRecordType.get('Company_Buyer');
        amnd.Ownership_type__c='Joint Tenant';
        amnd.Company_name__c='Test Company';
        amnd.ServiceRequest__c=objSR.id;
        amnd.CL_Certificate_No__c='RV1234';
        amnd.Name_of_Declaration_Signatory__c='ddd';
        amnd.IssuingAuthority__c='Others';
        amnd.Issuing_Authority_Other__c='IC';
        amnd.Amendment_Type__c='Buyer';
        amnd.Place_of_Issue__c = 'Hyderabad';
        amnd.Office_Unit_Number__c = '123';
        amnd.Office_Phone_Number__c = '+97144567890';
        amnd.Street__c = 'DIFC';
        amnd.Residence_Phone_Number__c = '+9715260527894';
        amnd.Building_Name__c = 'The Gate';
        amnd.Given_Name__c = 'DIFC';
        amnd.Family_Name__c = 'Dubai';
        amnd.Passport_No__c = 'DIFC1234';
        amnd.Nationality_list__c = 'India';
        //amnd.Email__c = 'test@difc.ae';
        amnd.Ownership_Type__c = 'Sole';
        insert amnd;
        
        Amendment__c amnd1 = new Amendment__c();
        amnd1.RecordTypeId = mapRecordType.get('Developer');
        amnd1.Amendment_Type__c='Developer';
        amnd1.ServiceRequest__c=objSR.id;
        amnd1.Request_No__c = objSR.Id;
        //amnd1.Developer__c = objDev.Id;
        amendlist.add(amnd1);
        
        Amendment__c amnd2 = new Amendment__c();
        amnd2.RecordTypeId = mapRecordType.get('RORP_Shareholder_Company');
        amnd2.Related__c=amnd.id;
        amnd2.ServiceRequest__c=objSR.id;
        amnd2.Amendment_Type__c = 'Shareholder';
        amnd2.Company_name__c='DIFC Authority';
        amnd2.CL_Certificate_No__c = 'DIFC1235';
        amnd2.IssuingAuthority__c = 'Others';
        amnd2.Issuing_Authority_Other__c= 'DIFC Authority';
        amnd2.Issuance_Date__c = system.today().addYears(-1);
        amnd2.Percentage_held__c = 50;
        amendlist.add(amnd2);
        
        amnd2 = new Amendment__c();
        amnd2.RecordTypeId = mapRecordType.get('RORP_Shareholder');
        amnd2.Related__c=amnd.id;
        amnd2.ServiceRequest__c=objSR.id;
        amnd2.Amendment_Type__c = 'Shareholder';
        amnd2.Company_name__c='DIFC Authority';
        amnd2.Given_Name__c = 'DIFC';
        amnd2.Family_Name__c = 'Individual';
        amnd2.Nationality_list__c = 'India';
        amnd2.Passport_No__c = 'DIFC1227';
        amnd2.Percentage_held__c = 50;
        amnd2.Middle_Name__c = 'International';
        amnd2.Office_Unit_Number__c = 'Level - 14';
        amnd2.Building_Name__c = 'The Gate';
        amnd2.Street__c = 'DIFC';
        amnd2.Permanent_Native_City__c = 'Dubai';
        amnd2.Post_Code__c = '12345';
        amnd2.PO_Box__c = '1234578';
        amnd2.Residence_Phone_Number__c = '+971522645781';
        amnd2.Office_Phone_Number__c = '+971522645781';
        amnd2.Phone__c = '+971524578912';
        amnd2.Email_Address_2__c = 'test@difc.ae';
        amnd2.Fax__c = '+97148794587';
        amnd2.Passport_Issue_Date__c = system.today().addYears(-1);
        amnd2.Passport_Expiry_Date__c = system.today().addYears(9);
        amnd2.Emirates_ID_Number__c = '782 123 123456789';
        amnd2.Passport_No_Occupant__c = 'DIFC1202';
        amendlist.add(amnd2);
        
        
        Amendment__c amnd4 = new Amendment__c();
        amnd4.RecordTypeId = mapRecordType.get('RORP_Authorized_Signatory');
        amnd4.Given_Name__c ='DIFC ';
        amnd4.Family_Name__c = 'Authorized Sing';
        amnd4.Nationality_list__c = 'India';
        amnd4.Passport_No__c = 'DIFC1201';
        amnd4.ServiceRequest__c=objSR.id;
        amnd4.Related__c=amnd.id;
        amendlist.add(amnd4);
        
        insert amendlist;
        
        
        objStep  = new Step__c();
        objStep.SR__c = objSR.Id;
        insert objStep;
       
       
    
               
        test.startTest();
        
           Security_Deposit_Cls ObjD=new Security_Deposit_Cls();
               ObjD.BP_creation(objSR );
    
        test.stopTest();
        
    }
}