/******************************************************************************************************
 *  Author   : Kumar Utkarsh
 *  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* This class contains logic to to do the application status check in GDRFA system by doing callouts.
*******************************************************************************************************/

public class WebServiceGdrfaAppStatusEnquiryBatch implements Database.Batchable<sObject>, Database.Stateful, Database.AllowsCallouts{
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        
        return Database.getQueryLocator('SELECT Id, Name, SAP_IDNUM__c, SAP_IDTYP__c, DNRD_ID_Number__c, DNRD_ID_Value__c, SR__c, SR__r.Record_Type_Name__c, SR_Status__c, Step_Name__c, Step_Template__c, SR_Step__c, Dependent_On__c '+
                                        'FROM Step__c '+
                                        'WHERE (Step_Name__c = \'Entry Permit is Issued\' '+
                                        'OR Step_Name__c = \'Completed\' '+
                                        'OR Step_Name__c = \'Passport is Ready for Collection\') '+
                                        'AND (Status__r.code__c = \'AWAITING_REVIEW\' '+
                                        'OR Status__r.code__c = \'Repush_to_GDRFA\' '+
                                        'OR Status__r.code__c = \'Manually_handled\') '+
                                        'AND (SR_Status__c  NOT IN ( \'Process Completed\', \'Cancelled\',\'Refund Processed\',\'Cancellation Approved\',\'Draft\') ) '+
                                        'AND (NOT SR_Status__c  Like \'%The Visa application has been rejected by GDRFA%\') '+
                                        'AND (NOT SR_Status__c  Like \'%Application is pending%\') '+
                                        'AND SR__r.Completed_Date_Time__c = null '+
                                        'AND (SAP_IDNUM__c = null '+
                                        'OR ID_Start_Date__c = null '+
                                        'OR ID_End_Date__c = null )'+
                                        'AND (SR__r.Record_Type_Name__c = \'DIFC_Sponsorship_Visa_Renewal\' '+
                                        'OR SR__r.Record_Type_Name__c = \'DIFC_Sponsorship_Visa_New\' '+
                                        'OR SR__r.Record_Type_Name__c = \'DIFC_Sponsorship_Visa_Cancellation\') ');
        
    }
    
    public void execute(Database.BatchableContext bc, List<Step__c> scope){
        
         System.enqueueJob(new webServiceGdrfaAppStatusQueueable(scope));
      
    }
    
    public void finish(Database.BatchableContext bc){
        //Do nothing
    }
    
}