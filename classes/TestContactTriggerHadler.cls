@isTest(seeAllData=false)
private class TestContactTriggerHadler {

  static testMethod void myUnitTest() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        List<contact> ListobjContact = new List<contact>();
        
       ID BDrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Development').getRecordTypeId(); //Added as per V1.2
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.AccountId = objAccount.Id; 
        objContact.Passport_No__c = 'ABC12345';
        objContact.RecordTypeId = BDrectypeid ;
        objContact.important__C= true;
        objContact.Is_Active__c= true;
        //ListobjContact.add (objContact );
      
        //Contact objContact = new Contact();
        objContact.FirstName = 'Test1';
        objContact.LastName = 'Name2';
        objContact.AccountId = objAccount.Id; 
        objContact.Passport_No__c = 'ABC123451';
        objContact.RecordTypeId = BDrectypeid ;
        objContact.important__C= true;
        objContact.Is_Active__c= true;
        objContact.Send_Portal_Login_Link__c = 'Yes';
        ListobjContact.add (objContact );
      
        insert ListobjContact;
        
        ContactTriggerHadler.IsImportantCheck(ListobjContact, false);
        ContactTriggerHadler.onAfterInsert(ListobjContact);
  		//ContactTriggerHadler.onAfterUpdate(ListobjContact, false);
  }
  
  
  static testMethod void testAccountTrigger() {
  
           //Getting the record type ids of Contact
        ID ROCrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Contact').getRecordTypeId();
        ID GSrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        ID RORPrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RORP Contact').getRecordTypeId();        
        ID BDrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Development').getRecordTypeId(); //Added as per V1.2
        
        
        List<contact> ListobjContact = new List<contact>();
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.RecordTypeId = GSrectypeid ;
        objContact.important__C= false;
        ListobjContact.add (objContact );
        
        objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.RecordTypeId = ROCrectypeid ;
        objContact.important__C= false;
        ListobjContact.add (objContact );
        
        objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.RecordTypeId = BDrectypeid ;
        objContact.important__C= false;
        ListobjContact.add (objContact );
        
        objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.RecordTypeId = RORPrectypeid ;
        objContact.important__C= false;
        ListobjContact.add (objContact );
      
        objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.RecordTypeId = RORPrectypeid ;
        objContact.important__C= false;
        objContact.Send_Portal_Login_Link__c ='Yes';
        ListobjContact.add (objContact );
        insert ListobjContact;
  }

	static testMethod void myUnitTestNew() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        List<contact> ListobjContact = new List<contact>();
        
       ID BDrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Development').getRecordTypeId(); //Added as per V1.2
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.AccountId = objAccount.Id; 
        objContact.Passport_No__c = 'ABC12345';
        objContact.RecordTypeId = BDrectypeid ;
        objContact.important__C= true;
        objContact.Is_Active__c= true;
        //ListobjContact.add (objContact );
      
        //Contact objContact = new Contact();
        objContact.FirstName = 'Test1';
        objContact.LastName = 'Name2';
        objContact.AccountId = objAccount.Id; 
        objContact.Passport_No__c = 'ABC123451';
        objContact.RecordTypeId = BDrectypeid ;
        objContact.important__C= true;
        objContact.Is_Active__c= true;
        objContact.Send_Portal_Login_Link__c = 'Yes';
        ListobjContact.add (objContact );
      
        insert ListobjContact;
        Map<id , Contact > oldMap = new Map<id,Contact>();
        for(Contact conRec : ListobjContact){
            oldMap.put(conRec.id, conRec);
        }
        //ContactTriggerHadler.IsImportantCheck(ListobjContact, false);
        //ContactTriggerHadler.onAfterInsert(ListobjContact);
  		ContactTriggerHadler.onAfterUpdate(ListobjContact, oldMap,oldMap);
  }
}