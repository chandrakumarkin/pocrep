/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_InternalUserDocUploadCls {

    static testMethod void myUnitTest() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        insert objContact;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'New Lease';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Active__c = true;
        objTemplate.Available_for_menu__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.DMS_Document_Index__c = '12345';
        objDocMaster.Name = 'Test Doc Master';
        objDocMaster.Code__c = 'Test Doc Master';
        insert objDocMaster;
        
        Service_Request__c objSR = new Service_Request__c();
	    objSR.Customer__c = objAccount.Id;
	    insert objSR;
	      
	    apexpages.currentPage().getParameters().put('Id',objSR.Id);
	    InternalUserDocUploadCls objInternalUserDocUploadCls = new InternalUserDocUploadCls();
	    objInternalUserDocUploadCls.strSelDocMaster = objDocMaster.Id;
	    
	    Attachment objAttch = new Attachment();
	    objAttch.Body = blob.valueOf('Test Attachment');
	    objAttch.Name = 'Test Attachment';
	    
	    objInternalUserDocUploadCls.objAttch = objAttch;
	    objInternalUserDocUploadCls.objSRDoc.Document_Master__c = objDocMaster.Id;
	    objInternalUserDocUploadCls.SaveDocument();
	    
	    objInternalUserDocUploadCls.objAttch = new Attachment();
	    objInternalUserDocUploadCls.strSelDocMaster = null;
	    objInternalUserDocUploadCls.SaveDocument();
	    
	    
	    objInternalUserDocUploadCls.CancelDocument();
	    
    }
}