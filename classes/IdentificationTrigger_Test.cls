/**
*Test Class for IdentificationTrigger 	q
 */
@isTest
private class IdentificationTrigger_Test {

    static testMethod void insertIdentification() {
    	Account acc = new Account(name='Test');
    	insert acc;
       	Identification__c ident = new Identification__c(Name='test',Account__c=acc.id,Identification_Type__c='Index Card',Valid_To__c = system.today());
       	insert ident; 
    }
    
    static testMethod void updateIdentification() {
    	Account acc = new Account(name='Test');
    	insert acc;
       	Identification__c ident = new Identification__c(Name='test',Account__c=acc.id,Identification_Type__c='Index Card');
       	insert ident; 
       	ident.Valid_To__c = system.today();
       	update ident;
    }
    static testMethod void exceptionOnIdentification() {
    	Account acc = new Account(name='Test');
    	insert acc;
       	Identification__c ident = new Identification__c(Name='test',Account__c=acc.id,Identification_Type__c='Index Card');
       	insert ident; 
       	ident.Valid_To__c = system.today();
       	ident.Account__c = '0012000001j4j3Z';
       	update ident;
    }
}