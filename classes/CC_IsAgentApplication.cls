/*
    Author      : Durga Prasad
    Date        : 15-Mar-2020
    Description : Custom code to check the Application is created by Agent
    -----------------------------------------------------------------------
*/
global without sharing class CC_IsAgentApplication implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'False';
        if(SR!=null){
            for(HexaBPM__Service_Request__c ojbSR:[Select Id from HexaBPM__Service_Request__c where Id=:SR.Id and CreatedBy.ContactId!=null]){
               strResult = 'TRUE'; 
            }
        }else if(stp!=null && stp.HexaBPM__SR__c!=null){
            for(HexaBPM__Service_Request__c ojbSR:[Select Id from HexaBPM__Service_Request__c where Id=:stp.HexaBPM__SR__c and CreatedBY.ContactId!=null]){
               strResult = 'TRUE'; 
            }
        }
        /*
        if(SR!=null && SR.HexaBPM__Contact__c!=null)
          strResult = 'True';
        else if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Contact__c!=null)
          strResult = 'True';
        */
        return strResult;
    }
}