/******************************************************************************************
*  Author   : Shikha
*  Date     : 23/3/2020 
*  Description : Generic class to list all the utility methods for ROC group
-----------------------------------------------------------------------------------------------------
Modification History
-----------------------------------------------------------------------------------------------------
V.No   Date            Updated By          Description
---------------------------------------------------------------------------------------- 
v1.0    24/03/2021       Shikha              #14723 Added a new method to validate Incumbency questions
v1.1    30/05/2021       Suchita             #16300 Added a new method to update create new ICC relationship and update the old one.
v1.2	02/08/2021		 Shikha				 #15115 Added code to auto close Review And Approve step
*********************************************************************************************/
public class ROC_Utility {
    
    //#14100 : Method - to update total amount associated to an SR
    //@InvocableMethod(label='UpdatePricesSR' description='update price' category='Service_Request__c')
    public static void updatePricesSR (List<Service_Request__c> srList){
        //updateSumOnSR(srList[0].Id);
        try{
            Id SRId = srList[0].Id;
            String templateId = srList[0].SR_Template__c;
            Decimal sumValue = 0;
            List<SR_Price_Item__c> priceItmList = [SELECT Id,Price__c, ServiceRequest__c, Status__c, Total_Service_Amount__c 
                                                   FROM SR_Price_Item__c WHERE ServiceRequest__c = :SRId];
            for(SR_Price_Item__c priceItm : priceItmList){
                sumValue = sumValue + priceItm.Total_Service_Amount__c;
            }
            system.debug('### sumValue '+sumValue);
            //RecursiveControlCls.isUpdatedAlready = true;
            Service_Request__c obSR = new Service_Request__c(Id = SRId, Net_Payable_Amount__c = sumValue );
            if(obSR != null && obSR.Net_Payable_Amount__c != 0){
                update obSR;
            }
            system.debug('### obSR '+obSR);
            List<SR_Doc__c> srDocuments = [SELECT Id, Name FROM SR_Doc__c WHERE Service_Request__c = :SRId AND Name = 'NoticeToPay'];
            system.debug('### srDocuments '+srDocuments.size());
            if(srDocuments.isEmpty()){
                boolean bCriteriaMet = false;
                bCriteriaMet = obSR.Net_Payable_Amount__c != 0;
                CreateGeneratedSRDoc(obSR.Id, templateId, 'NOTICE_TO_PAY', bCriteriaMet);
                /*SR_Template_Docs__c SRTmpDoc = new SR_Template_Docs__c();
                List<SR_Template_Docs__c> srTempDocs = [SELECT Id,Document_Master__c,Document_Master__r.Name, SR_Template_Docs_Condition__c,Courier_collected_at__c,
                                                        DMS_Document_Index__c,Group_No__c,Optional__c,Generate_Document__c,Document_Description_External__c,Courier_delivered_at__c,
                                                        Document_Description__c,Document_Master__r.LetterTemplate__c
                                                        FROM SR_Template_Docs__c 
                                                        WHERE SR_Template__c =:templateId AND Document_Master_Code__c = 'NOTICE_TO_PAY'];                
                system.debug('##$ srTempDocs '+srTempDocs);
                if(!srTempDocs.isEmpty()){
                    SRTmpDoc = srTempDocs[0];
                }
                system.debug('### SRTmpDoc '+SRTmpDoc);
                boolean bCriteriaMet = false;
                bCriteriaMet = obSR.Net_Payable_Amount__c != 0;
                list<SR_Doc__c> lstReqSRDocsInsert_at_Insert = new list<SR_Doc__c>();
                system.debug('### bCriteriaMet '+bCriteriaMet);
                if(bCriteriaMet && SRTmpDoc != null){
                    SR_Doc__c objSRDoc = new SR_Doc__c();
                    objSRDoc.Name = SRTmpDoc.Document_Master__r.Name;
                    objSRDoc.Service_Request__c = obSR.id;
                    objSRDoc.SR_Template_Doc__c = SRTmpDoc.Id;
                    //objSRDoc.Status__c = 'Pending Upload';
                    objSRDoc.Document_Master__c = SRTmpDoc.Document_Master__c;
                    objSRDoc.DMS_Document_Index__c = SRTmpDoc.DMS_Document_Index__c;
                    objSRDoc.Group_No__c = SRTmpDoc.Group_No__c;
                    objSRDoc.Is_Not_Required__c = SRTmpDoc.Optional__c;
                    objSRDoc.Generate_Document__c = SRTmpDoc.Generate_Document__c;
                    objSRDoc.Document_Description_External__c = SRTmpDoc.Document_Description_External__c;
                    if(objSRDoc.Document_Description_External__c==null){
                        objSRDoc.Document_Description_External__c = SRTmpDoc.Document_Description__c;
                    }
                    objSRDoc.Sys_IsGenerated_Doc__c = SRTmpDoc.Generate_Document__c;
                    objSRDoc.Letter_Template__c = SRTmpDoc.Document_Master__r.LetterTemplate__c;
                    if(SRTmpDoc.Courier_collected_at__c!=null || SRTmpDoc.Courier_delivered_at__c!=null){
                        objSRDoc.Is_Not_Required__c = true;
                        objSRDoc.Courier_Docs__c = true; 
                    }
                    //ddp will generate all doc where gen__doc__c = true
                    // user will not upload these docs
                    //
                    string Gen_or_Upload = ''; 
                    if(objSRDoc.Generate_Document__c==true){
                        objSRDoc.Is_Not_Required__c = true;
                        objSRDoc.Status__c = 'Generated';
                        string tempString  = string.valueof(Crypto.getRandomInteger());
                        objSRDoc.Sys_RandomNo__c = tempString.right(4);
                        Gen_or_Upload = 'Generate';
                    }                                       
                    objSRDoc.Unique_SR_Doc__c = SRTmpDoc.Document_Master__r.Name+'_'+obSR.id+'_'+Gen_or_Upload;
                    lstReqSRDocsInsert_at_Insert.add(objSRDoc);
                }
                system.debug('### lstReqSRDocsInsert_at_Insert '+lstReqSRDocsInsert_at_Insert);
                if(lstReqSRDocsInsert_at_Insert.size() > 0){
                    insert lstReqSRDocsInsert_at_Insert;
                }*/
            }
            
            
        }
        catch(Exception ex){
            Log__c objLog = new Log__c();objLog.Description__c = ex.getMessage() +'   '+ex.getLineNumber();objLog.Type__c = 'UpdatePriceSR';insert objLog; 
        }
    }
    
    //v1.0 - Incumbency Validation
    public static Boolean Incumbency_Validate(Service_Request__c objSR){
        Boolean IncumFlag = true;
        try{
            if(objSR.Doyouwishtoaddpassportnumbers__c == NULL){
                IncumFlag = false;
            }
        }
        catch(Exception ex){
            Log__c objLog = new Log__c();objLog.Description__c = ex.getMessage() +'   '+ex.getLineNumber();objLog.Type__c = 'UpdatePriceSR';insert objLog; 
        }
        return IncumFlag;
    }
    
    public static void CreateGeneratedSRDoc(String srId, String templateId, String DocMasterCode, Boolean bCriteria){
        SR_Template_Docs__c SRTmpDoc = new SR_Template_Docs__c();
        List<SR_Template_Docs__c> srTempDocs = [SELECT Id,Document_Master__c,Document_Master__r.Name, SR_Template_Docs_Condition__c,Courier_collected_at__c,
                                                DMS_Document_Index__c,Group_No__c,Optional__c,Generate_Document__c,Document_Description_External__c,Courier_delivered_at__c,
                                                Document_Description__c,Document_Master__r.LetterTemplate__c
                                                FROM SR_Template_Docs__c 
                                                WHERE SR_Template__c =:templateId AND Document_Master_Code__c = :DocMasterCode];                
        system.debug('##$ srTempDocs '+srTempDocs);
        if(!srTempDocs.isEmpty()){
            SRTmpDoc = srTempDocs[0];
        }
        system.debug('### SRTmpDoc '+SRTmpDoc);               
        list<SR_Doc__c> lstReqSRDocsInsert_at_Insert = new list<SR_Doc__c>();
        system.debug('### bCriteriaMet '+bCriteria);
        if(bCriteria && SRTmpDoc != null){
            SR_Doc__c objSRDoc = new SR_Doc__c();
            objSRDoc.Name = SRTmpDoc.Document_Master__r.Name;
            objSRDoc.Service_Request__c = srId;
            objSRDoc.SR_Template_Doc__c = SRTmpDoc.Id;
            //objSRDoc.Status__c = 'Pending Upload';
            objSRDoc.Document_Master__c = SRTmpDoc.Document_Master__c;
            objSRDoc.DMS_Document_Index__c = SRTmpDoc.DMS_Document_Index__c;
            objSRDoc.Group_No__c = SRTmpDoc.Group_No__c;
            objSRDoc.Is_Not_Required__c = SRTmpDoc.Optional__c;
            objSRDoc.Generate_Document__c = SRTmpDoc.Generate_Document__c;
            objSRDoc.Document_Description_External__c = SRTmpDoc.Document_Description_External__c;
            if(objSRDoc.Document_Description_External__c==null){
                objSRDoc.Document_Description_External__c = SRTmpDoc.Document_Description__c;
            }
            objSRDoc.Sys_IsGenerated_Doc__c = SRTmpDoc.Generate_Document__c;
            objSRDoc.Letter_Template__c = SRTmpDoc.Document_Master__r.LetterTemplate__c;
            if(SRTmpDoc.Courier_collected_at__c!=null || SRTmpDoc.Courier_delivered_at__c!=null){
                objSRDoc.Is_Not_Required__c = true;
                objSRDoc.Courier_Docs__c = true; 
            }
            //ddp will generate all doc where gen__doc__c = true
            // user will not upload these docs
            //
            string Gen_or_Upload = ''; 
            if(objSRDoc.Generate_Document__c==true){
                objSRDoc.Is_Not_Required__c = true;
                objSRDoc.Status__c = 'Generated';
                string tempString  = string.valueof(Crypto.getRandomInteger());
                objSRDoc.Sys_RandomNo__c = tempString.right(4);
                Gen_or_Upload = 'Generate';
            }                                       
            objSRDoc.Unique_SR_Doc__c = SRTmpDoc.Document_Master__r.Name+'_'+srId+'_'+Gen_or_Upload;
            lstReqSRDocsInsert_at_Insert.add(objSRDoc);
        }
        system.debug('### lstReqSRDocsInsert_at_Insert '+lstReqSRDocsInsert_at_Insert);
        if(lstReqSRDocsInsert_at_Insert.size() > 0){
            insert lstReqSRDocsInsert_at_Insert;
        }
    }
    //Start v1.1
     public static string updateICCCompany(String stepId){
        string status = 'Success'; 
        id AccountID;
        List<Step__c> ListStep=[select id,SR__r.Customer__c,SR__r.Transfer_to_account__c 
                                from Step__c where id=:stepId limit 1];
        if(!ListStep.IsEmpty())
        {
            AccountID=ListStep[0].SR__r.Customer__c;
            list<Relationship__c> lstRelationToBeUpdated= new list<Relationship__c>();
            for(Relationship__c existingICCRel:[select id,End_Date__c,Active__c from Relationship__c where Subject_Account__c=:AccountID and Active__c=true and Relationship_Type__c = 'Incorporated Cell Company' ]){
                existingICCRel.Active__c=false;
                existingICCRel.End_Date__c=Date.today(); 
                lstRelationToBeUpdated.add(existingICCRel);
            }
            if(lstRelationToBeUpdated.size()>0){ 
                try{
                    update lstRelationToBeUpdated;
                }catch(Exception e){
                    Log__c objLog = new Log__c(Description__c = 'Exception on updateICCCompany:'+e.getMessage(),Type__c = 'Code to update existing Relationship for ICC Company');
                    insert objLog;
                }
            }
            CreateICCRelationship(AccountID,ListStep[0].SR__r.Transfer_to_account__c);
        }
        return status;
    }
    public static void CreateICCRelationship(String AccountID,String RelatedICCAccount){
        list<Relationship__c> lstRelationToBeCreated = new list<Relationship__c>();
        
            Relationship__c relaObject = new Relationship__c();
            relaObject.Object_Account__c = RelatedICCAccount;
            relaObject.Subject_Account__c = AccountID;
            relaObject.Relationship_Type__c = 'Incorporated Cell Company';
            relaObject.Start_Date__c = Date.today(); 
            relaObject.Active__c = true;
            relaObject.Relationship_Group__c = 'ROC';
            lstRelationToBeCreated.add(relaObject);
        
        if(lstRelationToBeCreated.size()>0){
            try{
                insert lstRelationToBeCreated;
            }catch(Exception e){
                Log__c objLog = new Log__c(Description__c = 'Exception on CreateICCRelationship:'+e.getMessage(),Type__c = 'Code to create Relationship for ICC Company');
                insert objLog;
            }
        }
    }
    //End v1.1
    
    //v1.2 Start
    public static void autoCloseReviewApprove (String stepId, String SRId, String status){
        Step__c stp = new Step__c(Id=stepId, status__c = id.valueof(Label.StepStatusApproved),ownerID = id.valueof(Label.IntegrationUserId));
        update stp;
        system.debug('### status '+status);
        if(status != Label.StepStatusApproved){
            Service_Request__c objSR = new Service_Request__c (Id=SRId, External_SR_Status__c = id.valueof(Label.stepstatusApp),
                                                          Internal_SR_Status__c=id.valueof(Label.stepstatusApp));
            update objSR;
        }
     }
    //v1.2 End
    
    //@InvocableMethod(label='FineSRAutomation' description='create Price Item' category='Service_Request__c')
    public static void fineSRAutomation (List<Service_Request__c> srList){
        system.debug('### in fie SR');
         List <SR_Price_Item__c> insertFine = new List <SR_Price_Item__c>();
         Set<String> srIdSet = new Set<String>();
        for(Service_Request__c sr : srList){
            srIdSet.add(sr.Id);
        }
        system.debug('### in fie SR'+srIdSet);
         List<product2> objProd = [SELECT id,(SELECT id,Material_Code__c FROM Pricing_Lines__r WHERE DEV_Id__c ='ROCP-004') 
                                   FROM product2 where ProductCode='Fine' and IsActive=true];
        system.debug('### objProd '+objProd);
          list<SR_Price_Item__c> deleteFine = [select id from SR_Price_Item__c where servicerequest__c IN: srIdSet
                                               and Material_Code__c =: 'ROCP-004' and Status__c =: 'Added'];
        system.debug('### deleteFine '+deleteFine);
          if(objProd!=null && !objProd.isEmpty()){
            for(Service_Request__c objSR : srList){
              SR_Price_Item__c objPriceItem = new SR_Price_Item__c();
                objPriceItem.product__c = objProd[0].id;
                objPriceItem.ServiceRequest__c = objSR.id;
                objPriceItem.Status__c = 'Added';
                objPriceItem.Price__c = objSR.Fine_Amount_AED__c;
                objPriceItem.Pricing_Line__c = objProd[0].Pricing_Lines__r[0].id;
                insertFine.add(objPriceItem);
            }
            if(deleteFine!=null && !deleteFine.isEmpty()){
                  deleteFine[0].Status__c = 'Cancelled';
                  update deleteFine;
              }
            if(insertFine!=null && !insertFine.isEmpty()){
                  insert  insertFine;
              }
              system.debug('## insertFine '+insertFine);
          }
    }
    
    @InvocableMethod(label='updateAccountBlockList' description='create Price Item' category='Account')
    public static void updateAccountBlockList (List<Account> accList){
        system.debug('### accList '+accList);
        for(Account acc : accList){
            acc.BlockServiceTextValue__c = acc.BlockServiceList__c;
        }
        system.debug('### BlockList '+accList);
        update accList;
    }
    
}