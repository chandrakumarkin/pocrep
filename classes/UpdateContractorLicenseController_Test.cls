@isTest
public class UpdateContractorLicenseController_Test{
    
    @testSetup static void setup(){        
        Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();        
        insert objAccount;        
        Contact objContact = Test_CC_FitOutCustomCode_DataFactory.getTestContact(objAccount.Id);        
        insert objContact;        
        System.runAs(new User(Id = Userinfo.getUserId(), ContactId = objContact.Id)){
            /* 
* NOTE: If your master/dependent data are used in validation
*       rules that uses the VLOOKUP function, please take note
*       of the number of records to be created. Records beyond 
*       20 may result to recursion errors when they are retrieved
*/
            
            /* Prepare the master data */
            
            Account objContractor = Test_CC_FitOutCustomCode_DataFactory.getTestContractor();
            Account objContractor2 = Test_CC_FitOutCustomCode_DataFactory.getTestContractor();
            
            objContractor2.Name = 'Test Empty Violations COntractor';
            objContractor2.Bp_No__c = 'BP12345678';
            
            Building__c testBuilding = Test_CC_FitOutCustomCode_DataFactory.getTestBuilding();
            
            List<Lookup__c> testLookups = Test_CC_FitOutCustomCode_DataFactory.getTestLookupRecords();
            List<Status__c> testStepStatus = Test_CC_FitOutCustomCode_DataFactory.getTestStepStatuses();
            List<SR_Status__c> testSRStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSrStatuses();
            List<CountryCodes__c> countryCodesTestList = Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
            List<Step_Template__c> testStepTemplates = Test_CC_FitOutCustomCode_DataFactory.getTestStepTemplate();
            List<Checklist_Information__c> testChecklistInfo = Test_CC_FitOutCustomCode_DataFactory.getTestChecklistInformation();
            
            //TODO: Add any other parent data declarations 
            
            /* Insert all the master data records */
            insert new List<Account>{objContractor,objContractor2};
                insert countryCodesTestList;
            insert testSRStatus;
            insert testStepStatus;
            insert testLookups;
            insert testStepTemplates;
            insert testBuilding;
            insert testChecklistInfo;
            
            /* Create other dependencies needed for the tests */
            List<Unit__c> testUnits = Test_CC_FitOutCustomCode_DataFactory.getTestUnits(testBuilding.Id);
            Lease__c testLease = Test_CC_FitOutCustomCode_DataFactory.getTestLease(objAccount.Id);
            License__c testLicense = Test_CC_FitOutCustomCode_DataFactory.getTestLicense(objAccount.Id);
            
            insert testUnits;
            insert testLicense;
            insert testLease;
            
            objAccount.Active_License__c = testLicense.Id;
            update objAccount;
            
            /* Create Documents for fit-out Manuals */
            Document fitOutManual = Test_CC_FitOutCustomCode_DataFactory.getTestDocument(UserInfo.getUserId(),'DIFC_Fit_Out_Manual');
            Document eventManual = Test_CC_FitOutCustomCode_DataFactory.getTestDocument(UserInfo.getUserId(),'DIFC_Event_Manual');
            
            insert new List<Document>{fitOutManual,eventManual};
            SR_Template__c objTemplate = new SR_Template__c();
            objTemplate.Name = 'Update_License_Details';
            objTemplate.SR_RecordType_API_Name__c = 'Update_License_Details';
            objTemplate.ORA_Process_Identification__c = 'Update_License_Details';
            objTemplate.Active__c = true;
            insert objTemplate;
        }
    }
    
    @isTest static void testMethod1() {
        List<Account> lstAccounts = [Select Id,Name from Account limit 2];
		SR_Template__c template = [Select Id,Name from SR_Template__c limit 1];
        UpdateContractorLicenseController oblCls = new UpdateContractorLicenseController();
		oblCls.getcompanyTypeList();
		oblCls.getissuingAuthority();
        oblCls.selectedCompanyType = 'Fitout Contractor';
        oblCls.selectedAuthority = 'Government of Dubai';
        oblCls.save();
        oblCls.Cancel();
        
        UpdateContractorLicenseController oblCls1 = new UpdateContractorLicenseController();
        oblCls1.save();
        
    }
}