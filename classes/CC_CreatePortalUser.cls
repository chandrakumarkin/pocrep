/*
    Author      : Durga Prasad
    Date        : 09-Oct-2019
    Description : Custom code to create Lead or Community User on approval
* -------------------------------------------------------------------------------------------------
*    Version    Author    Date           Comment
*  ------------------------------------------------------------------------------------------------
*     v1.1      Durga     18-03-2020     Code to update the role and access level for the newly created AccountContactRelation
      v1.2      Leeba     19-03-2020     Code to copy the passport copy from Service Request to Contact
      v1.3      Prateek   19-03-2020     fixed issue of contact gender
      V1.4      Sai       06-06-2020     DIFC2-10163
      
*/
      
global without sharing class CC_CreatePortalUser implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        string AccountId = stp.HexaBPM__SR__r.HexaBPM__Customer__c;
        try{
            if(stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.Last_Name__c!=null && stp.HexaBPM__SR__r.HexaBPM__Email__c!=null){
                if(stp.HexaBPM__SR__r.Entity_Type__c!=null){
                    if(stp.HexaBPM__SR__r.Entity_Type__c!='Financial - related' && stp.HexaBPM__SR__r.Business_Sector__c!='Investment Fund'){
                        if(AccountId==null){
                            Account acc = new Account();
                            acc.Name = stp.HexaBPM__SR__r.entity_name__c;
                            acc.Company_Type__c = stp.HexaBPM__SR__r.Entity_Type__c;
                            
                            if(stp.HexaBPM__SR__r.Entity_Type__c=='Non - financial')
                                acc.BD_Sector__c = 'Corporates and Service Provider';
                            else if(stp.HexaBPM__SR__r.Entity_Type__c=='Retail')
                                acc.BD_Sector__c = 'Retail (RET)';
                                
                            acc.OB_Sector_Classification__c = stp.HexaBPM__SR__r.Business_Sector__c;
                            
                            if(!Test.isRunningTest())
                                acc.OwnerId = label.Portal_User_Default_Owner;
                            
                            acc.ROC_Status__c = 'Account Created';
                            acc.Priority_to_DIFC__c = 'Standard';
                            acc.Mobile_Number__c = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;
                            
                            //V1.4
                            if(stp.HexaBPM__SR__r.Created_By_Agent__c == false){
                                acc.Created_by_Agent__c = 'No';
                            }
                            insert acc;
                            AccountId = acc.Id;
                            
                        }
                        
                        string OpportunityRecordTypeName = 'BD_Non_Financial';
                        
                        if(stp.HexaBPM__SR__r.Entity_Type__c=='Retail')
                            OpportunityRecordTypeName = 'BD_Retail';
                            
                        Opportunity objOpp = new Opportunity();
                        for(RecordType rctyp:[Select Id from RecordType where sobjectType='Opportunity' and DeveloperName=:OpportunityRecordTypeName]){
                            objOpp.RecordTypeId = rctyp.Id;
                        }
                        objOpp.AccountId = AccountId;
                        objOpp.CloseDate = system.today();
                        objOpp.Name = stp.HexaBPM__SR__r.entity_name__c+'-Opportunity';
                        objOpp.StageName = 'Created';
                        objOpp.Type = stp.HexaBPM__SR__r.Setting_Up__c;
                        objOpp.Sector_Classification__c = stp.HexaBPM__SR__r.Business_Sector__c;
                        if(!Test.isRunningTest()){
                            objOpp.OwnerId = label.Portal_User_Default_Owner;
                        }
                        insert objOpp;
                        
                        Contact objcon = new Contact();
                        if(Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Portal_User')!=null)
                            objcon.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Portal_User').getRecordTypeId();

                        objcon.AccountId = AccountId;
                        objcon.LastName = stp.HexaBPM__SR__r.Last_Name__c;
                        objcon.FirstName = stp.HexaBPM__SR__r.First_Name__c;
                        objcon.MobilePhone = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;
                        objcon.Phone = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;
                        objcon.Email = stp.HexaBPM__SR__r.HexaBPM__Email__c;
                        objcon.Contact_Source__c = 'Self Registration';
                        objcon.Send_Portal_Login_Link__c = 'Yes';
                        objcon.OB_Self_Registered_Contact__c = true;
                        
                        if(!Test.isRunningTest())
                            objcon.OwnerId = label.Portal_User_Default_Owner;
                        
                        objcon.Passport_No__c = stp.HexaBPM__SR__r.Passport_No__c;
                        objcon.Nationality__c = stp.HexaBPM__SR__r.Nationality__c;
                        objcon.Birthdate = stp.HexaBPM__SR__r.Date_of_Birth__c;

                        //objcon.Gender__c = stp.HexaBPM__SR__r.Gender__c;
                        if(stp.HexaBPM__SR__r.Gender__c == 'M'){
                            objcon.Gender__c = 'Male';
                        }else if(stp.HexaBPM__SR__r.Gender__c == 'F'){
                            objcon.Gender__c = 'Female';
                        }else{
                            objcon.Gender__c = stp.HexaBPM__SR__r.Gender__c;
                        }

                        objcon.Does_the_individual_reside_in_the_UAE__c     = stp.HexaBPM__SR__r.Are_You_Resident_Of_UAE__c;
                        objcon.Does_the_individual_have_a_valid_UAE_res__c  = stp.HexaBPM__SR__r.Valid_UAE_residence_visa__c;
                        objcon.Address_Line_1_HC__c         = stp.HexaBPM__SR__r.Address_Line_1__c;
                        objcon.Address_Line_2_HC__c                    = stp.HexaBPM__SR__r.Address_Line_2__c;
                        objcon.CITY1__c                     = stp.HexaBPM__SR__r.City_Town__c;
                        objcon.Emirate_State_Province__c    = stp.HexaBPM__SR__r.State_Province_Region__c;
                        objcon.COUNTRY_ENGLISH2__c          = stp.HexaBPM__SR__r.Country__c;
                        objcon.PO_Box__c                    = stp.HexaBPM__SR__r.Po_Box_Postal_Code__c;

                        objcon.Place_of_Birth__c = stp.HexaBPM__SR__r.Place_of_Birth__c;
                        objcon.Date_of_Issue__c = stp.HexaBPM__SR__r.Date_of_Issue__c;
                        objcon.Passport_Expiry_Date__c = stp.HexaBPM__SR__r.Date_of_Expiry__c;
                        objcon.Place_of_Issue__c = stp.HexaBPM__SR__r.Place_of_Issuance__c;
                        objcon.Issued_Country__c = stp.HexaBPM__SR__r.Nationality__c;
                        objcon.Country_of_Birth__c = stp.HexaBPM__SR__r.Nationality__c;
                        insert objcon;
                        
                        //v1.2  
                        
                        OB_AgentEntityUtils.setPassportFileUnderContact(objcon,stp.HexaBPM__SR__c);
                         						
                        list<AccountContactRelation> lstACR = new list<AccountContactRelation>();//v1.1(
                        for(AccountContactRelation ACR:[Select Id,Access_Level__c,Roles from AccountContactRelation where AccountId=:AccountId and ContactId=:objcon.Id and IsDirect=true]){
                            ACR.Access_Level__c = 'Read/Write';
                            // Start 12450
                            if(stp.HexaBPM__SR__r.Access_to_Employee_Services__c!=null && stp.HexaBPM__SR__r.Access_to_Employee_Services__c=='Yes'){
                              	ACR.Roles = 'Company Services; Employee Services; Property Services; Super User';
                            }else{
                                ACR.Roles = 'Company Services;Property Services;Super User';
                            }
                            // End 12450
                            ACR.Relationship_with_entity__c = stp.HexaBPM__SR__r.Relationship_with_entity__c;
                            lstACR.add(ACR);
                        }
                        if(lstACR.size()>0)
                            update lstACR;
                        CreateInPrincipleSR(stp.HexaBPM__SR__c,objOpp.Id,AccountId);
                        
                    }else{
                        Lead objLead = new Lead();
                        string RecordTypeName;
                        if(stp.HexaBPM__SR__r.Entity_Type__c=='Financial - related')
                            RecordTypeName = 'Financial';
                        else if(stp.HexaBPM__SR__r.Entity_Type__c=='Non - financial')
                            RecordTypeName = 'Non_Financial';
                        else if(stp.HexaBPM__SR__r.Entity_Type__c=='Retail')
                            RecordTypeName = 'Retail';
                        if(RecordTypeName!=null){
                            for(RecordType rectype:[Select Id from RecordType where sobjectType='Lead' and DeveloperName=:RecordTypeName]){
                                objLead.RecordTypeId = rectype.Id;
                            }
                        }
                        objLead.LastName = stp.HexaBPM__SR__r.Last_Name__c;
                        objLead.FirstName = stp.HexaBPM__SR__r.First_Name__c;
                        objLead.Company = stp.HexaBPM__SR__r.entity_name__c;
                        objLead.Email = stp.HexaBPM__SR__r.HexaBPM__Email__c;
                        //objLead.BD_Sector_Classification__c = stp.HexaBPM__SR__r.Business_Sector__c;
                        objLead.Sector_Classification__c = stp.HexaBPM__SR__r.Business_Sector__c;
                        objLead.LeadSource = 'Company Website';
                        objLead.Lead_Sub_Source__c = 'Self Registration';
                        objLead.MobilePhone =  stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;
                        objLead.Phone = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;
                        
                        Database.DMLOptions dmo = new Database.DMLOptions();
                        dmo.assignmentRuleHeader.useDefaultRule= true;
                        objLead.setOptions(dmo);
                        insert objLead;
                    }
                }else{
                    Contact objcon = new Contact();
                    if(Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Portal_User')!=null)
                            objcon.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Portal_User').getRecordTypeId();
                    objcon.AccountId = AccountId;
                    objcon.LastName = stp.HexaBPM__SR__r.Last_Name__c;
                    objcon.FirstName = stp.HexaBPM__SR__r.First_Name__c;
                    objcon.MobilePhone = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;
                    objcon.Phone = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;
                    objcon.Email = stp.HexaBPM__SR__r.HexaBPM__Email__c;
                    objcon.Contact_Source__c = 'Self Registration';
                    objcon.Send_Portal_Login_Link__c = 'Yes';
                    if(!Test.isRunningTest()){
                    objcon.OwnerId = label.Portal_User_Default_Owner;
                    }
                    objcon.Issued_Country__c = stp.HexaBPM__SR__r.Nationality__c;
                    objcon.Country_of_Birth__c = stp.HexaBPM__SR__r.Nationality__c;
                    insert objcon;
                }
            }
        
        
            
        }catch(Exception e){
            strResult = e.getMessage()+'';
            return strResult;
        }
        
        return strResult;
    }
    
    /*
        Method Name :   CreateInPrincipleSR
        Description :   Future method to create InPrinciple SR as Draft on Self-Registration Approval
    */
    @future
    public static void CreateInPrincipleSR(string SRId,string OpportunityId,string AccountId){
        if(SRId!=null){
            try{
                if(AccountId!=null){
                    HexaBPM__Service_Request__c objSRTBU = new HexaBPM__Service_Request__c(Id=SRId);
                    objSRTBU.HexaBPM__Customer__c = AccountId;
                    if(OpportunityId!=null)
                        objSRTBU.Opportunity__c = OpportunityId;
                    update objSRTBU;
                }
                set<string> setSRIds = new set<string>();
                setSRIds.add(SRId);
                
                map<String, Schema.SObjectType>  m = Schema.getGlobalDescribe();
                SObjectType  objtype;
                DescribeSObjectResult objDef1;
                map<String, SObjectField> fieldmap;
                if(m.get('HexaBPM__Service_Request__c')!= null)
                    objtype = m.get('HexaBPM__Service_Request__c');
                if(objtype != null)
                    objDef1 =  objtype.getDescribe();
                if(objDef1 != null)
                    fieldmap =  objDef1.fields.getmap();
                
                set<string> setSRFields = new set<string>();
                
                if(fieldmap!=null){
                    for(Schema.SObjectField strFld:fieldmap.values()){
                        Schema.DescribeFieldResult fd = strFld.getDescribe();
                            if(fd.isCustom()){
                                setSRFields.add(string.valueOf(strFld).toLowerCase());
                        }
                    }
                }
                
                string SRqry =  'Select Name';
                if(setSRFields.size()>0){
                    for(string srField : setSRFields){
                        SRqry += ','+srField;
                    }
                }
                SRqry += ' From HexaBPM__Service_Request__c where Id IN:setSRIds';
                
                list<HexaBPM__Service_Request__c> lstInPrincipleSRs = new list<HexaBPM__Service_Request__c>();
                HexaBPM__Service_Request__c olsSr =new HexaBPM__Service_Request__c();
                for(HexaBPM__Service_Request__c objSR:database.query(SRqry)){
                    olsSr = objSR;
                    HexaBPM__Service_Request__c SR = objSR.Clone();
                    if(Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('In_Principle')!=null)
                        SR.RecordTypeId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('In_Principle').getRecordTypeId();
                    SR.HexaBPM__SR_Template__c = null;
                    SR.HexaBPM__Internal_SR_Status__c = null;
                    SR.HexaBPM__External_SR_Status__c = null;
                    SR.I_Agree__c = false;
                    SR.HexaBPM__Submitted_Date__c = null;
                    SR.HexaBPM__Submitted_DateTime__c = null;
                    SR.HexaBPM__Required_Docs_not_Uploaded__c = false;
                    SR.HexaBPM__FinalizeAmendmentFlg__c = false;
                    SR.HexaBPM__Linked_SR__c = SRId;
                    SR.Page_Tracker__c = '';
                    lstInPrincipleSRs.add(SR);
                }
                Upsert lstInPrincipleSRs;
                
                string BrandProfileFileId = '';
                string BrandProfileFileName = '';
                for(HexaBPM__SR_Doc__c BrandProfile:[Select Id,HexaBPM__Doc_ID__c,File_Name__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRId and HexaBPM__Doc_ID__c!=null]){
                    BrandProfileFileId = BrandProfile.HexaBPM__Doc_ID__c;
                    BrandProfileFileName = BrandProfile.File_Name__c;
                }
                
                if(BrandProfileFileId!='' && BrandProfileFileName!=''){
                    list<HexaBPM__SR_Doc__c> lstDocsTBU = new list<HexaBPM__SR_Doc__c>();
                    for(HexaBPM__SR_Doc__c BrandProfile:[Select Id,HexaBPM__Doc_ID__c,File_Name__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c IN:lstInPrincipleSRs and HexaBPM__SR_Template_Doc__r.HexaBPM__Document_Master_Code__c='Brand_Profile']){
                        BrandProfile.HexaBPM__Doc_ID__c = BrandProfileFileId;
                        BrandProfile.File_Name__c = BrandProfileFileName;
                        BrandProfile.HexaBPM__Status__c = 'Uploaded';
                        lstDocsTBU.add(BrandProfile);
                    }
                    if(lstDocsTBU.size()>0)
                        update lstDocsTBU;

                }
            
            
               addPortalUserAsAmendment(olsSr);
            }catch(Exception e){
                Log__c objLog = new Log__c();
                objLog.Description__c = 'Exception on CC_CreatePortalUser.CreateInPrincipleSR() - Line:209 :'+e.getMessage();
                objLog.Type__c = 'Custom Code to create In_Principle on Self Reg Approval';
                insert objLog;
            }
            
        }
    }

    


    public static void addPortalUserAsAmendment(HexaBPM__Service_Request__c srObj){
                                                 
         
         map<String,HexaBPM__Service_Request__c> mapSrIdCurrSR = new map<String,HexaBPM__Service_Request__c>();
         map<String,String> mapCurrSRIdAcId = new map<String,String>();
         map<String,List<HexaBPM__Service_Request__c>> mapAccIdRelatedSR = new map<String,List<HexaBPM__Service_Request__c>>();
         map<String,List<HexaBPM__SR_Doc__c>> mapSrIdRelatedSRDocs = new map<String,List<HexaBPM__SR_Doc__c>>();
         
         List<HexaBPM_Amendment__c> lstAmedToBeCreated = new   List<HexaBPM_Amendment__c>(); 
         
         /* for(HexaBPM__Service_Request__c newSr : (List<HexaBPM__Service_Request__c>)TriggerNew)
         {
             HexaBPM__Service_Request__c oldSr = TriggerOldMap.get(newSr.Id);
             system.debug('@@@@@@@@@@@@@@@ newSr.HexaBPM__Record_Type_Name__c '+newSr.HexaBPM__Record_Type_Name__c);
             // On Submitted, Approved, Created.
            
                 // Converted_User_Registration... for Complete your profile
                 // New_User_Registration ... for self registration.
                 if( 
                     newSr.HexaBPM__Record_Type_Name__c == 'Converted_User_Registration' 
                            ||  newSr.HexaBPM__Record_Type_Name__c == 'New_User_Registration'   
                   )
                 {
                     if(newSr.HexaBPM__Customer__c != NULL) 
                     {
                         mapCurrSRIdAcId.put(newSr.Id,newSr.HexaBPM__Customer__c);
                     }
                     mapSrIdCurrSR.put(newSr.Id,newSr);
                 }
             
         
         } */
         if( srObj.HexaBPM__Record_Type_Name__c == 'New_User_Registration'   
                   )
                 {
                     if(srObj.HexaBPM__Customer__c != NULL) 
                     {
                         mapCurrSRIdAcId.put(srObj.Id,srObj.HexaBPM__Customer__c);
                     }
                     mapSrIdCurrSR.put(srObj.Id,srObj);
                 }
                 
         system.debug('@@@@@@@@ mapCurrSRIdAcId '+mapCurrSRIdAcId);
           
         // getting relate SR Dos
         Set<String> srIds = mapSrIdCurrSR.keyset();
         Set<String> srDocsIds = new Set<String>();
         List<String> fields = new List<String>(HexaBPM__SR_Doc__c.SObjectType.getDescribe().fields.getMap().keySet());
         /*String soql = ''
                        + ' SELECT ' + String.join(fields, ',')
                        + ' FROM HexaBPM__SR_Doc__c'
                        + ' WHERE HexaBPM__Service_Request__c IN:srIds';*/

        List<HexaBPM__SR_Doc__c> lstCurrSrDocs = new  List<HexaBPM__SR_Doc__c>();
        lstCurrSrDocs = [
                         SELECT Id,
                                Name,
                                HexaBPM_Amendment__c,
                                Amendment_Name__c,
                                HexaBPM__From_Finalize__c,
                                OB_CompanyName__c,
                                Copied_from_InPrinciple__c,
                                HexaBPM__Customer__c,
                                HexaBPM__Customer_Comments__c,
                                HexaBPM__Document_Description__c,
                                HexaBPM__Document_Master__c,
                                HexaBPM__Document_Name__c,
                                Document_No__c,
                                Document_No_Id__c,
                                HexaBPM__Document_No__c,
                                HexaBPM__Document_Type__c,
                                Download_URL__c,
                                Drawloop_Next__c,
                                HexaBPM__Step__c,
                                HexaBPM__Rejection_Reason__c,
                                File_Name__c,
                                HexaBPM__Group_No__c,
                                In_Principle_Document__c,
                                HexaBPM__Document_Description_External__c,
                                HexaBPM__Comments__c,
                                Legal_Statement__c,
                                Letter_Authentication_URL__c,
                                HexaBPM__Letter_Template_Id__c,
                                HexaBPM__Is_Not_Required__c,
                                //Passport_No_Formula__c,
                                HexaBPM__Preview_Download_Document__c,
                                QR_Code__c,
                                HexaBPM__Generate_Document__c,
                                HexaBPM__Service_Request__c,
                                HexaBPM__SR_Template_Doc__c,
                                HexaBPM__Status__c,
                                //HexaBPM__Sys_AutoNo__c,
                                HexaBPM__Sys_IsGenerated_Doc__c,
                                //HexaBPM__Sys_RandomNo__c,
                                Sys_AlphaNumeric_Random__c,
                                HexaBPM__Doc_ID__c,
                                //HexaBPM__Unique_SR_Doc__c,
                                HexaBPM__Document_Master__r.HexaBPM__Code__c
                           FROM HexaBPM__SR_Doc__c 
                          WHERE HexaBPM__Service_Request__c IN:srIds
                        
                    ];

         for (HexaBPM__SR_Doc__c srDocs : lstCurrSrDocs ) 
         {
              //mapSrIdRelatedSRDocs
              List<HexaBPM__SR_Doc__c> lstrelatedSRDocs = ( mapSrIdRelatedSRDocs.containsKey(srDocs.HexaBPM__Service_Request__c) ? mapSrIdRelatedSRDocs.get(srDocs.HexaBPM__Service_Request__c) : new List<HexaBPM__SR_Doc__c>());
              lstrelatedSRDocs.add(srDocs);
              mapSrIdRelatedSRDocs.put(srDocs.HexaBPM__Service_Request__c,lstrelatedSRDocs);
              srDocsIds.add(srDocs.Id);
         }
         
         // quering all other SR in Draft.
         
            for(HexaBPM__Service_Request__c srTemp : [SELECT id,
                                                            Name,
                                                            HexaBPM__External_Status_Name__c,
                                                            HexaBPM__Customer__c,
                                                            HexaBPM__Record_Type_Name__c,
                                                            HexaBPM__Linked_SR__c
                                                       FROM HexaBPM__Service_Request__c 
                                                      WHERE HexaBPM__Customer__c IN: mapCurrSRIdAcId.values()
                                                    ])
            {
                if(srTemp.HexaBPM__External_Status_Name__c == 'Draft')
                {
                    List<HexaBPM__Service_Request__c> lstrelatedSR = ( mapAccIdRelatedSR.containsKey(srTemp.HexaBPM__Customer__c) ? mapAccIdRelatedSR.get(srTemp.HexaBPM__Customer__c) : new List<HexaBPM__Service_Request__c>());
                    lstrelatedSR.add(srTemp);
                    mapAccIdRelatedSR.put(srTemp.HexaBPM__Customer__c,lstrelatedSR);
                }
            }
            system.debug('@@@@@@@@ mapAccIdRelatedSR '+mapAccIdRelatedSR);
         
         for(HexaBPM__Service_Request__c currSR : mapSrIdCurrSR.values() )
         {
             HexaBPM__Service_Request__c parentSRForUserAmed;
             List<HexaBPM__Service_Request__c> lstrelatedSR = new List<HexaBPM__Service_Request__c>();
             lstrelatedSR =  mapAccIdRelatedSR.get(currSR.HexaBPM__Customer__c);
             
            //if(Retail) => get InPrincipal
            //if(NonFin and nonInvestFund => get InPrincipal)
            system.debug('@@@@@@@@ currSR.Business_Sector__c '+currSR.Business_Sector__c);
            system.debug('@@@@@@@@ currSR.Entity_Type__c '+ currSR.Entity_Type__c);
            if(currSR.Entity_Type__c == 'Retail' || (currSR.Entity_Type__c == 'Non - financial' 
                                                            && currSR.Business_Sector__c != 'Investment fund' ) ) 
            {
                 system.debug('this is qualified non fin ');
                 parentSRForUserAmed = OB_ServiceRequestTriggerHandler.getSRBasedOnRecType(lstrelatedSR,'In_Principle',currSR);
                 
            }
             
            //if(fin) => get AOR_F
            //if(NonFin && InvFund) => get AOR_F
            if( currSR.Entity_Type__c == 'Financial - related' || 
                                                    (currSR.Entity_Type__c == 'Non - financial' 
                                                        && currSR.Business_Sector__c == 'Investment fund' ) ) 
             {
                   system.debug('this is qualified fin ');
                   parentSRForUserAmed = OB_ServiceRequestTriggerHandler.getSRBasedOnRecType(lstrelatedSR,'AOR_Financial',currSR);
             }

             system.debug('@@@@@@@@@ parentSRForUserAmed  '+parentSRForUserAmed);
             if(parentSRForUserAmed != NULL)
             {
                HexaBPM_Amendment__c userAmed = OB_ServiceRequestTriggerHandler.createUserAmedBasedOnSR(currSR);
                userAmed.ServiceRequest__c = parentSRForUserAmed.Id;
                lstAmedToBeCreated.add(userAmed);
             }
             
         }
         
         system.debug('@@@@@@@@ lstAmedToBeCreated '+lstAmedToBeCreated);
         //inserting useramed
         if(lstAmedToBeCreated != NULL 
                && lstAmedToBeCreated.size() <> 0 )
         {
             upsert lstAmedToBeCreated;
         }

         List<HexaBPM__SR_Doc__c> lstSRDocsUpdate = new  List<HexaBPM__SR_Doc__c>();
         // Cloning SRDocs (Passport Copy) to newly created Amed.
         for(HexaBPM_Amendment__c amed : lstAmedToBeCreated){
                if(amed.Parent_SR__c!=null){
                    // Getting passport copy of currSR.
                    HexaBPM__SR_Doc__c passportSRDoc = OB_ServiceRequestTriggerHandler.getSRDocBasedOnCode(amed.Parent_SR__c,mapSrIdRelatedSRDocs,'Passport_Copy');
                    if(passportSRDoc != NULL){
                        HexaBPM__SR_Doc__c passportSRDocCloned = OB_ServiceRequestTriggerHandler.cloneSRDoc( passportSRDoc, amed );
                        //passportSRDocCloned.HexaBPM_Amendment__c = amed.id;   
                        lstSRDocsUpdate.add(passportSRDocCloned);
                    }  
                }
         }

         if(lstSRDocsUpdate != NULL 
                    && lstSRDocsUpdate.size() > 0  )
         {
            update lstSRDocsUpdate;

            list<ContentDocumentLink> lstCDL = new list<ContentDocumentLink>();
                    
            
            for(HexaBPM__SR_Doc__c srdocObj : lstSRDocsUpdate){
                if(srdocObj.HexaBPM__Doc_ID__c != null){
                    ContentDocumentLink contDocLnkCloned = new ContentDocumentLink();
                    contDocLnkCloned.LinkedEntityId = srdocObj.id;
                    contDocLnkCloned.contentdocumentid = srdocObj.HexaBPM__Doc_ID__c;
                    contDocLnkCloned.ShareType = 'V';
                    lstCDL.add(contDocLnkCloned);
                }                
            }
            system.debug('lstCDL' + lstCDL);
            if(lstCDL.size()>0){
                insert lstCDL;
            }
                    
         }


          
     }
     
     public static void dummytest(){
     
         integer i =1;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
            i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
            i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
          i++;
         i++;
         i++;
         i++;
         i++;
         i++;
         i++;
     }
}