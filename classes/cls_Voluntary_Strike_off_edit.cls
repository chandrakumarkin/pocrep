/*********************************************************************************************************************
*  Name     : cls_Voluntary_Strike_off_edit
*  Author     : Arun Singh
*  Purpose   : support Voluntary_Strike_off service request

--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No        Date                Updated By         Description
v1.1      25 Feb 2021         Suchita Sharma       #DIFC2-13675 - Voluntary Strike Off
v1.2      21/06/2021          Utkarsh              #DIFC-16268
*****************************************************************/

public with sharing class cls_Voluntary_Strike_off_edit {
    
    public string RecordTypeId;
    public string RecordTypeDissolution;
    public map<string,string> mapParameters;
    public String CustomerId;
    public Account ObjAccount{get;set;}
    public Service_Request__c SRData{get;set;}
    public boolean ValidTosubmit{get;set;}
    public string ErrorMessage{get;set;}
    
    public cls_Voluntary_Strike_off_edit(ApexPages.StandardController controller) {
        System.debug('Test');
        SRData=(Service_Request__c )controller.getRecord();
        ValidTosubmit=true;
        //Start v1.1 
        set<string> indexCardStatusSet = new Set<String>{'Not Available', 'Cancelled'}; //13675 - Voluntary Strike Off - Allow only for these status otherwise error
            //End v1.1 
            mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
            mapParameters = apexpages.currentPage().getParameters();              
        if(mapParameters.get('RecordType')!=null) 
            RecordTypeId= mapParameters.get('RecordType');    
        else
            RecordTypeId=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Request_for_Voluntary_Strike_off').getRecordTypeId();
        RecordTypeDissolution=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Dissolution').getRecordTypeId();
        for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                         Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                         Contact.Account.Qualifying_Type__c,Contact.Account.Qualifying_Purpose_Type__c,Contact.Account.Contact_Details_Provided__c,
                         Contact.Account.Index_Card__c,Contact.Account.Index_Card_Status__c,Contact.Account.Is_Foundation_Activity__c  
                         from User where Id=:userinfo.getUserId()])
        {
            System.debug('Test'+objUsr);
            CustomerId = objUsr.Contact.AccountId;
            if(SRData.id==null)
            {
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                ObjAccount= objUsr.Contact.Account;
                
                //
                //Start v1.1    
                //if(objUsr.Contact.Account.Index_Card_Status__c=='Active') 
                if(!indexCardStatusSet.contains(objUsr.Contact.Account.Index_Card_Status__c)) { //13675 - Voluntary Strike Off
                    //End v1.1 
                    ValidTosubmit=false;
                    //ErrorMessage='Please Service request not available because Company establishment card still active Please cancell .';
                    
                    ErrorMessage='The entity has an active establishment card. Please cancel the establishment card to submit this request. For any queries, please contact gs.helpdesk@difc.ae';
                }
                //fetch the SR where customer is current user and rt is resolution and completeion status != null
                //Start v1.2
                List<Service_Request__c> SRList= [SELECT Id, Name from Service_Request__c WHERE Customer__c =: SRData.Customer__c AND RecordTypeId =:RecordTypeDissolution AND Submitted_Date__c != null Limit 1];       
                System.debug('Test'+SRList);
                if(!SRList.isEmpty()){
                    ErrorMessage='The entity has an active Application for Winding up '+SRList[0].Name;
                    ValidTosubmit=false;
                }
                //End v1.2
                list<PO_Box_Master__c> ListPo=[select Name,id from PO_Box_Master__c where Current_Owner__c=:SRData.Customer__c and Status__c='Utilized' limit 1];
                
                if(!ListPo.isempty())
                {
                    //ErrorMessage='Service request not available because Company PO BOX '+ListPo[0].Name+' still  active'; 
                    
                    ErrorMessage='The entity has an active DIFC P O Box '+ListPo[0].Name+' . Please cancel your DIFC P O Box subscription to submit this request. For any queries, please contact gs.helpdesk@difc.ae';
                    ValidTosubmit=false;
                }
                //Start v1.1 
                //if(objUsr.Contact.Account.Index_Card_Status__c=='Active'&& ! ListPo.isempty())
                if(!indexCardStatusSet.contains(objUsr.Contact.Account.Index_Card_Status__c) && ! ListPo.isempty()){ //13675 - Voluntary Strike Off
                    //End v1.1
                    ErrorMessage='The entity has an active establishment card and  DIFC P O Box. Please cancel your establishment card and DIFC P O Box subscription to submit this request. For any queries, please contact gs.helpdesk@difc.ae';
                    ValidTosubmit=false;
                    
                }
                
                
                //
                
                
            }
            
        }
        
        
    }
    
    public string  validationRule()
    {
        
        string Errormessage='';
        
        if(SRData.In_accordance_with_Article_10_1__c!=true ||
           SRData.In_Accordance_with_Article_11__c!=true ||
           SRData.In_Accordance_with_Article_12_1a__c!=true ||
           SRData.In_Accordance_with_Article_12_1bj__c!=true)
        {
            Errormessage='Please provide all the required information.';
            
        }
        return Errormessage;
    }
    
    public  PageReference SaveRecord()
    {
        string Errormessage=validationRule();
        
        boolean isvalid=String.isBlank(Errormessage);
        
        
        if(isvalid) 
        {       
            
            try     
            {  
                upsert SRData;
                PageReference acctPage = new ApexPages.StandardController(SRData).view();        
                acctPage.setRedirect(true);        
                return acctPage;     
            }catch (Exception e)     
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
                ApexPages.addMessage(myMsg);    
            }  
        }
        else
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Errormessage);         
            ApexPages.addMessage(myMsg); 
        }
        
        return null;
        
        
    }
    
}