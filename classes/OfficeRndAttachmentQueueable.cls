public with sharing class OfficeRndAttachmentQueueable implements Queueable,Database.AllowsCallouts { 
   
    Public Office_RND_Membership__c officeRMD ;
    Public List<Office_RND_Membership__c> officeRMDList ;
    Public String offRMDId; 
       
    public OfficeRndAttachmentQueueable(List<Office_RND_Membership__c> offRMDIdList){
        This.officeRMDList = offRMDIdList;
    }

    public void execute(QueueableContext context) { 
        
         try{
            
            List<Office_RND_Membership__c> offRmdList = [select id,Name,membershipid__c,Response_From_SAP__c from Office_RND_Membership__c where Id IN:officeRMDList];
            system.debug('^^^^invocable^^^^^offRmdList ^^^^^^^^^^^^^'+offRmdList );
    
            List<Office_RND_Membership__c> updateList = new List<Office_RND_Membership__c>();
            if(offRmdList.size()>0){
                for(Office_RND_Membership__c off : offRmdList ){
                    if(off.membershipid__c != null){
                        String response = PushAttachmentToSAP(off);
                        system.debug('^^^^^^^^^response ^^^^^^^^^^^^^'+response);
                        off.Response_From_SAP__c = response;
                        updateList.add(off);
                    }
                }
                if(updateList.size() >0)update updateList;
            }
         }catch(Exception e){
            insert LogDetails.CreateLog(null, 'OfficeRndAttachmentQueueable: execute', 'Line Number : '+e.getLineNumber()+'\nException is : '+e.getMessage());
        }
    }
    
    public static String PushAttachmentToSAP(Office_RND_Membership__c offRmd){
        String response = '';

        string decryptedSAPPassWrd = test.isrunningTest()  ==   false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) : '123456';
       
        sapComDocumentSapSoapFunctionsMcSRMD.ZRE_OFRND_ATTACHMENT   refundResponse;
     
        refundResponse                      = new sapComDocumentSapSoapFunctionsMcSRMD.ZRE_OFRND_ATTACHMENT();
        refundResponse.timeout_x            = 120000;
        refundResponse.clientCertName_x     = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
        refundResponse.inputHttpHeaders_x   = new Map < String, String > ();
                
        Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c + ':' +decryptedSAPPassWrd);
        String authorizationHeader         = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                
        refundResponse.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        PageReference pdf = Page.QRViewFintech;
        pdf.getParameters().put('id',offRmd.Id);
        
        string pdfBody = (Test.isRunningTest()) ? 'test' : EncodingUtil.Base64Encode(pdf.getContent());

        
        system.debug('^^^^^^^^^body^^^^^^^^^^^^^'+pdfBody);

        sapComDocumentSapSoapFunctionsMcSRMD.ZattResponse  item  = refundResponse.ZreOfrndAttachment('application/pdf',offRmd.membershipid__c,offRmd.membershipid__c ,pdfBody);
        response = string.valueof(item);
        return response;
    }
    
}