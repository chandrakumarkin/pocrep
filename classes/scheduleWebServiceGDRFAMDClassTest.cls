@isTest
public class scheduleWebServiceGDRFAMDClassTest {
    
    public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void testScheduledJobForMD() {
        Test.startTest();
        String jobId = System.schedule('scheduleMDTest',CRON_EXP,new scheduleWebServiceGDRFAMDClass());
        // Get the information from the CronTrigger API object
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                          NextFireTime
                          FROM CronTrigger WHERE id = :jobId];
        
        // Verify the expressions are the same
        System.assertEquals(CRON_EXP, 
                            ct.CronExpression);
        
        // Verify the job has not run
        System.assertEquals(0, ct.TimesTriggered);
        
        Test.stopTest();
    }
    
 static testMethod void webServiceGdrfaAppStatusTest(){
     GDRFAIntegrationSettings__c setting = new GDRFAIntegrationSettings__c();
     setting.GDRFAEndPointURL__c = 'Test Setting';
     setting.name = 'GDRFA';
     insert setting;
     Test.startTest();
     Test.setMock(HttpCalloutMock.class, new WebServiceGdrfaMdMock());
     WebServiceFetchMDFromGDRFAClass.GDRFAFetchLookUpDetails();
     Test.stopTest();
    }
    
     static testMethod void webServiceGdrfaAppStatusTest1(){
     GDRFAIntegrationSettings__c setting = new GDRFAIntegrationSettings__c();
     setting.GDRFAEndPointURL__c = 'Test Setting';
     setting.name = 'GDRFA';
     insert setting;
     Test.startTest();
     Test.setMock(HttpCalloutMock.class, new WebServiceMDGdrfaMock());
     WebServiceFetchMDFromGDRFAClass.GDRFAFetchLookUpDetails();
     Test.stopTest();
    }
}