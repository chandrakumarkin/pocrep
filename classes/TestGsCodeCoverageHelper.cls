/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class TestGsCodeCoverageHelper {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Service_Request__c objSR;
        for(Service_Request__c obj : [select Id,Customer__c,Customer__r.BP_No__c from Service_Request__c where Customer__c != null AND Customer__r.BP_No__c != null AND Customer__r.ROC_Status__c='Active' limit 1]){
            objSR = obj;
        }
        if(objSR != null){
            GsUpgradeSRCls objGsUpgradeSRCls = new GsUpgradeSRCls();
            objGsUpgradeSRCls.ServiceRequest = objSR;
            
            Test.setMock(WebServiceMock.class, new TestAccountBalanceServiceCls());
            test.startTest();
            list<AccountBalenseService.ZSF_S_ACC_BAL> resActBals = new list<AccountBalenseService.ZSF_S_ACC_BAL>();
            AccountBalenseService.ZSF_S_ACC_BAL objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
            objActBal.KUNNR = objSR.Customer__r.BP_No__c;
            objActBal.UMSKZ = 'D';
            objActBal.WRBTR = '1234';
            resActBals.add(objActBal);
            objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
            objActBal.KUNNR = objSR.Customer__r.BP_No__c;
            objActBal.UMSKZ = 'H';
            objActBal.WRBTR = '1234';
            resActBals.add(objActBal);
            objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
            objActBal.KUNNR = objSR.Customer__r.BP_No__c;
            objActBal.UMSKZ = '9';
            objActBal.WRBTR = '1234';
            resActBals.add(objActBal);
            
            TestAccountBalanceServiceCls.resActBals = resActBals;
                    
            objGsUpgradeSRCls.AccountBalance();
        }
    }
    
    static testMethod void myUnitTest1() {
        SR_Price_Item__c objSRPriceItem;
        for(SR_Price_Item__c obj : [select Id,ServiceRequest__c,ServiceRequest__r.Customer__c,ServiceRequest__r.Customer__r.BP_No__c from SR_Price_Item__c where ServiceRequest__r.Customer__c != null AND ServiceRequest__r.Customer__r.BP_No__c != null AND ServiceRequest__r.Customer__r.ROC_Status__c='Active' AND Transaction_Id__c != null AND ServiceRequest__r.Record_Type_Name__c != 'Application_of_Registration' order by CreatedDate desc  limit 1 ]){
            objSRPriceItem = obj;
        }
        if(objSRPriceItem != null){
            Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
            
            list<SAPGSWebServices.ZSF_S_GS_SERV_OP> lstGSItems = new list<SAPGSWebServices.ZSF_S_GS_SERV_OP>();
            SAPGSWebServices.ZSF_S_GS_SERV_OP objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
            objGSItem.ACCTP = 'P';
            objGSItem.BANFN = '50001234';
            objGSItem.VBELN = '05001234';
            objGSItem.BELNR = '1234567';
            objGSItem.WSTYP = 'SERV';
            objGSItem.MATNR = 'GO-00001';
            objGSItem.UNQNO = '12345675000215';
            objGSItem.SGUID = objSRPriceItem.Id;
            lstGSItems.add(objGSItem);
            TestGsCRMServiceMock.lstGSPriceItems = lstGSItems;
            try{
            GsUpgradeSRCls.UpgradePushToSAP(objSRPriceItem.Id);
            }catch(Exception ex){}
        }
    }
    
     static testMethod void GSUpgradeConfirmPayment() {
       SR_Price_Item__c objSRPriceItem = new SR_Price_Item__c();
        for(SR_Price_Item__c obj : [select Id,ServiceRequest__c,ServiceRequest__r.Customer__c,ServiceRequest__r.Customer__r.BP_No__c from SR_Price_Item__c where ServiceRequest__c != null AND Status__c = 'Added' AND Price__c > 0 limit 1]){
            objSRPriceItem = obj;
        }
        Service_Request__c objSR;
        for(Service_Request__c obj : [select Id,Name,Submitted_Date__c,Customer__c,External_Status_Name__c,SAP_Unique_No__c,Upgrade_Type__c,Express_Service__c,Customer__r.BP_No__c,SR_Template__c,Type_of_Request__c,SAP_MATNR__c,SAP_UMATN__c,Upgrade_Docs_Attached__c,Last_Date_of_Entry__c,Current_Visa_Status__c,Registered_EIA__c,Record_Type_Name__c,
                                          (select Id,Step_Name__c from Steps_SR__r where Step_Name__c =: Label.Entry_Permit_Step_Name AND Status_Code__c = 'CLOSED'),
                                          SR_Template__r.Is_Exit_Process__c,Portal_Service_Request_Name__c,Required_Docs_not_Uploaded__c,
                                          Avail_Courier_Services__c,Use_Registered_Address__c,Consignee_FName__c,Consignee_LName__c,Courier_Mobile_Number__c,Apt_or_Villa_No__c,Courier_Cell_Phone__c,
                                          IsGSLetter__c,SR_Group__c
                                          from Service_Request__c where Id=:objSRPriceItem.ServiceRequest__c LIMIT 1]){
                            objSR =     obj;   
                           
                                          }
        if(objSRPriceItem != null){
                Test.setMock(WebServiceMock.class, new TestServiceCreationCls());
        
                list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
                SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
                objTemp.ACCTP = 'P';
                objTemp.SGUID = objSRPriceItem.Id;
                objTemp.SFMSG = 'Test Class';
                objTemp.BELNR = '1234567';
                resItems.add(objTemp);
                
                list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
                SAPECCWebService.ZSF_S_ACC_BAL objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
                objActBal.KUNNR = '001234';
                objActBal.UMSKZ = 'D';
                objActBal.WRBTR = '1234';
                resActBals.add(objActBal);
                objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
                objActBal.KUNNR = '001234';
                objActBal.UMSKZ = 'H';
                objActBal.WRBTR = '1234';
                resActBals.add(objActBal);
                objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
                objActBal.KUNNR = '001234';
                objActBal.UMSKZ = '9';
                objActBal.WRBTR = '1234';
                resActBals.add(objActBal);
                
                TestServiceCreationCls.resItems = resItems;
                TestServiceCreationCls.resActBals = resActBals;
                
                GsUpgradeSRCls objGsUpgradeSRCls = new GsUpgradeSRCls(); 
                objGsUpgradeSRCls.AvailablePortalBalance = 1000;
                objGsUpgradeSRCls.AmountToCharge = 100;
                objGsUpgradeSRCls.ServiceRequest = objSR;
                
                objGsUpgradeSRCls.ConfirmPayment();
                
                objGsUpgradeSRCls.CancelPayment();
                
                
                
        }
    }
    
    static testMethod void GSUpgradePushCouriertoSAP() {
      /*  Service_Request__c objSR = new Service_Request__c();
       for(Service_Request__c obj : [select Id,Name,Submitted_Date__c,Customer__c,External_Status_Name__c,SAP_Unique_No__c,Upgrade_Type__c,Express_Service__c,Customer__r.BP_No__c,SR_Template__c,Type_of_Request__c,SAP_MATNR__c,SAP_UMATN__c,Upgrade_Docs_Attached__c,Last_Date_of_Entry__c,Current_Visa_Status__c,Registered_EIA__c,Record_Type_Name__c,
                                          (select Id,Step_Name__c from Steps_SR__r where Step_Name__c =: Label.Entry_Permit_Step_Name AND Status_Code__c = 'CLOSED'),
                                          SR_Template__r.Is_Exit_Process__c,Portal_Service_Request_Name__c,Required_Docs_not_Uploaded__c,
                                          Avail_Courier_Services__c,Use_Registered_Address__c,Consignee_FName__c,Consignee_LName__c,Courier_Mobile_Number__c,Apt_or_Villa_No__c,Courier_Cell_Phone__c,
                                          IsGSLetter__c,SR_Group__c
                                          from Service_Request__c where Record_Type_Name__c = 'DIFC_Sponsorship_Visa_New' AND isClosedStatus__c = false AND Pre_GoLive__c = false AND SR_Template__c != null limit 1  ]){
                objSR = obj;
                objSR.Avail_Courier_Services__c = 'Yes';
            }
            */
            
            
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
       /* list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        */
        
        list<Status__c> lstStatus = [select id,name,code__C from status__C where Name='Draft'];
        list<SR_Status__c> lstSRStatus = [select id,name,code__C from SR_Status__c where Name='Draft'];
        SR_Template__c objTemplate = [select id ,Name,SR_RecordType_API_Name__c,Menutext__c,Available_for_menu__c,Template_Sequence_No__c,Menu__c,Active__c from SR_Template__c where Name = 'DIFC Sponsorship Visa-New'];
        
        /*list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstSRStatus;*/
        
        /*CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'DIFC New Visa';
        lstProducts.add(objProd);
        
        Product2 objPSAProd = new Product2();
        objPSAProd.Name = 'PSA';
        lstProducts.add(objPSAProd);
        
        objPSAProd = new Product2();
        objPSAProd.Name = 'DNRD';
        objPSAProd.Is_Guarantee_Product__c = true;
        lstProducts.add(objPSAProd);
        
        objPSAProd = new Product2();
        objPSAProd.Name = 'Visa Exception - 3 Years';
        objPSAProd.Is_Guarantee_Product__c = false;
        lstProducts.add(objPSAProd);
        
        
        insert lstProducts;
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC New Sponsorship Visa New';
        objPL.DEV_Id__c = 'GO-00001';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00001';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'PSA';
        objPL.DEV_Id__c = 'GO-PSA';
        objPL.Product__c = lstProducts[1].Id;
        objPL.Material_Code__c = 'GO-PSA';
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'DNRD';
        objPL.DEV_Id__c = 'GO-DNRD';
        objPL.Product__c = lstProducts[2].Id;
        objPL.Material_Code__c = 'GO-DNRD';
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'GO-00239';
        objPL.DEV_Id__c = 'GO-00239';
        objPL.Product__c = lstProducts[3].Id;
        objPL.Material_Code__c = 'GO-00239';
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            objDP.Unit_Price__c = 1234;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP; */
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New','New_index_Card')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }       
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_New');
        //objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+971589602235';
        objSR.Work_Phone__c = '+971589602235';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+919790142792';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[0].Id;
        objSR.External_SR_Status__c = lstSRStatus[0].Id;
        objSR.Avail_Courier_Services__c ='Yes';
        objSR.Consignee_FName__c = 'Test';
        objSR.Consignee_LName__c = 'Test';
        //objSR1.Send_SMS_To_Mobile__c = '+97112345670';
        objSR.Use_Registered_Address__c = false;
        objSR.Apt_or_Villa_No__c = 'testing123';
        objSR.Email__c = 'test@test.com';        
        objSR.Courier_Cell_Phone__c = '+97112345670';
        objSR.Courier_Mobile_Number__c = '+97112345670';
        insert objSR;
        
         list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 3210;
        //objItem.Pricing_Line__c = lstPLs[0].Id;
        //objItem.Product__c = lstProducts[0].Id;
        lstPriceItems.add(objItem);

        insert  lstPriceItems;  
            
            
            
            
        if(objSR != null){
                GsUpgradeSRCls objGsUpgradeSRCls = new GsUpgradeSRCls();
                objGsUpgradeSRCls.ServiceRequest = objSR;
                objGsUpgradeSRCls.checkForCourierPrice(objSR);
                
                objGsUpgradeSRCls.CourierCheck();
                objGsUpgradeSRCls.CourierAddressCheck();
                
                list<string> lst = new list<string>();
                for(SR_Price_Item__c obj : [select Id,ServiceRequest__c,ServiceRequest__r.Customer__c,ServiceRequest__r.Customer__r.BP_No__c from SR_Price_Item__c where ServiceRequest__c != null AND Status__c = 'Added' AND Price__c > 0 AND Transaction_Id__c = null limit 1]){
                    lst.add(obj.Id);
                }
                                
                Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
            
                list<SAPGSWebServices.ZSF_S_GS_SERV_OP> lstGSItems = new list<SAPGSWebServices.ZSF_S_GS_SERV_OP>();
                SAPGSWebServices.ZSF_S_GS_SERV_OP objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
                objGSItem.ACCTP = 'P';
                objGSItem.BANFN = '50001234';
                objGSItem.VBELN = '05001234';
                objGSItem.BELNR = '1234567';
                objGSItem.WSTYP = 'SERV';
                objGSItem.MATNR = 'GO-00001';
                objGSItem.UNQNO = '12345675000215';
                objGSItem.SGUID = lst[0];
                objGSItem.SOPRP = 'P';
                lstGSItems.add(objGSItem);
                
                objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
                objGSItem.ACCTP = 'S';
                objGSItem.BANFN = '50001234';
                objGSItem.VBELN = '05001234';
                objGSItem.BELNR = '1234567';
                objGSItem.WSTYP = 'SERV';
                objGSItem.MATNR = 'GO-00001';
                objGSItem.UNQNO = '12345675000215';
                objGSItem.SGUID = lst[0];
                lstGSItems.add(objGSItem);
                TestGsCRMServiceMock.lstGSPriceItems = lstGSItems;
                
                GsUpgradeSRCls.PushCouriertoSAP(lst);
                GsUpgradeSRCls objGsUpgradeSRCls1 = new GsUpgradeSRCls();
                //objGsUpgradeSRCls1.PriceItemInfo();
              //  GsUpgradeSRCls objGsUpgradeSRCls = new GsUpgradeSRCls();
                objGsUpgradeSRCls.ConfirmPayment();
                objGsUpgradeSRCls.CancelPayment();
                objGsUpgradeSRCls1.UpgradeType = '';
                objGsUpgradeSRCls.ServiceRequest.Upgrade_Docs_Attached__c = false;
                objGsUpgradeSRCls.UploadDocs();
                objGsUpgradeSRCls.ConfirmUpgrade();
                objGsUpgradeSRCls.run();
        }
    }
     static testMethod void GSUpgradePushCourier2() {
        Service_Request__c objSR = new Service_Request__c();
       for(Service_Request__c obj : [select Id,Name,Submitted_Date__c,Customer__c,External_Status_Name__c,SAP_Unique_No__c,Upgrade_Type__c,Express_Service__c,Customer__r.BP_No__c,SR_Template__c,Type_of_Request__c,SAP_MATNR__c,SAP_UMATN__c,Upgrade_Docs_Attached__c,Last_Date_of_Entry__c,Current_Visa_Status__c,Registered_EIA__c,Record_Type_Name__c,
                                          (select Id,Step_Name__c from Steps_SR__r where Step_Name__c =: Label.Entry_Permit_Step_Name AND Status_Code__c = 'CLOSED'),
                                          SR_Template__r.Is_Exit_Process__c,Portal_Service_Request_Name__c,Required_Docs_not_Uploaded__c,
                                          Avail_Courier_Services__c,Use_Registered_Address__c,Consignee_FName__c,Consignee_LName__c,Courier_Mobile_Number__c,Apt_or_Villa_No__c,Courier_Cell_Phone__c,
                                          IsGSLetter__c,SR_Group__c
                                          from Service_Request__c where Record_Type_Name__c = 'DIFC_Sponsorship_Visa_New' AND isClosedStatus__c = false AND Pre_GoLive__c = false AND SR_Template__c != null limit 1  ]){
                objSR = obj;
                objSR.Avail_Courier_Services__c = 'Yes';
            }
        if(objSR != null){
            GsUpgradeSRCls thisGsUpgrade = new GsUpgradeSRCls();
            thisGsUpgrade.ServiceRequest = objSR;
            thisGsUpgrade.UpgradeType ='Cross';
            thisGsUpgrade.ServiceRequest.Upgrade_Docs_Attached__c = false;
            thisGsUpgrade.ServiceRequest.Visa_Expiry_Date__c = system.today().addDays(300);
            thisGsUpgrade.ServiceRequest.Passport_Date_of_Expiry__c = system.today().addDays(300);
            thisGsUpgrade.ServiceRequest.Last_Date_of_Entry__c = system.today().addDays(-100);
            thisGsUpgrade.ServiceRequest.Current_Visa_Status__c = 'Other';
            thisGsUpgrade.ServiceRequest.Avail_Courier_Services__c = 'No';
            thisGsUpgrade.ServiceRequest.Type_of_Request__c = 'Applicant Outside UAE';
            thisGsUpgrade.PriceItemInfo();
            /*thisGsUpgrade.CheckDocs();*/
            

        }
     }
    
}