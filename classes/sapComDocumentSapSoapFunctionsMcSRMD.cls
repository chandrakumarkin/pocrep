public class sapComDocumentSapSoapFunctionsMcSRMD {
    public class ZreOfrndAttachmentResponse_element {
        public sapComDocumentSapSoapFunctionsMcSRMD.ZattResponse ExResponse;
        private String[] ExResponse_type_info = new String[]{'ExResponse','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'ExResponse'};
    }
    public class ZreOfrndAttachment_element {
        public String ImExtension;
        public String ImFileName;
        public String ImMid;
        public String Xstr;
        private String[] ImExtension_type_info = new String[]{'ImExtension','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] ImFileName_type_info = new String[]{'ImFileName','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] ImMid_type_info = new String[]{'ImMid','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Xstr_type_info = new String[]{'Xstr','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'ImExtension','ImFileName','ImMid','Xstr'};
    }
    public class ZattResponse {
        public String Status;
        public String Msg;
        private String[] Status_type_info = new String[]{'Status','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] Msg_type_info = new String[]{'Msg','urn:sap-com:document:sap:soap:functions:mc-style',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style','false','true'};
        private String[] field_order_type_info = new String[]{'Status','Msg'};
    }
    public class ZRE_OFRND_ATTACHMENT {
        //public String endpoint_x = 'https://s4qzone.difc.ae:44300/sap/bc/srt/rfc/sap/zre_ofrnd_attachment/200/zre_ofrnd_attachment/zre_ofrnd_attachment';
        public String endpoint_x = Endpoint_URLs__c.getAll().get('Office Rnd Attachment').URL__c;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:sap-com:document:sap:soap:functions:mc-style', 'sapComDocumentSapSoapFunctionsMcSRMD', 'urn:sap-com:document:sap:rfc:functions', 'sapComDocumentSapRfcFunctionsRMD'};
        public sapComDocumentSapSoapFunctionsMcSRMD.ZattResponse ZreOfrndAttachment(String ImExtension,String ImFileName,String ImMid,String Xstr) {
            sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachment_element request_x = new sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachment_element();
            request_x.ImExtension = ImExtension;
            request_x.ImFileName = ImFileName;
            request_x.ImMid = ImMid;
            request_x.Xstr = Xstr;
            sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachmentResponse_element response_x;
            Map<String, sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachmentResponse_element> response_map_x = new Map<String, sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachmentResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:sap-com:document:sap:soap:functions:mc-style:ZRE_OFRND_ATTACHMENT:ZreOfrndAttachmentRequest',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZreOfrndAttachment',
              'urn:sap-com:document:sap:soap:functions:mc-style',
              'ZreOfrndAttachmentResponse',
              'sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachmentResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.ExResponse;
        }
    }
}