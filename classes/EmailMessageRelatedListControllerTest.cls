@isTest
private class EmailMessageRelatedListControllerTest {
    static testmethod void test1(){
        Case objCase = new Case();
        objCase.Subject = 'test subject';
        insert objCase;  
        
        EmailMessage objEm = new EmailMessage();
        objEm.Subject = 'test subject';
        objEm.ParentId = objCase.id;
        objEm.HtmlBody = 'test body';
        objEm.MessageDate = System.now();
        insert objEm;
        
        test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(objCase);
            EmailMessageRelatedListController emRelList = new EmailMessageRelatedListController(sc);
            emRelList.emailMessageId = objEm.Id;
            emRelList.deleteEMessage();
        test.stopTest();
    }
}