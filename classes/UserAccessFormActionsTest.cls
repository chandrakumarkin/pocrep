@istest
public class UserAccessFormActionsTest 
{
    static testmethod Void UserAccessFormActionstestMeth1()
    {
    
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Under Formation';
        objAccount.FinTech_Classification__c='FinTech';
        objAccount.Legal_Type_of_Entity__c ='LLP';
        objAccount.ROC_reg_incorp_Date__c =system.today().addmonths(-4);
        objAccount.Financial_Year_End__c ='31st December';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        insert objContact;
    
    
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'New Lease';
        objTemplate.SR_RecordType_API_Name__c = 'User_Access_form';
        objTemplate.Active__c = true;
        objTemplate.Available_for_menu__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Test Doc Master';
        insert objDocMaster;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.SR_Template__c = objTemplate.Id;
        insert objSR;  
        
        List<Status__c> statusList = new List<Status__c>();
        status__c stat = new Status__c();
        stat.Name = 'Submitted';
        stat.code__c = 'Submitted';
        statusList.add(stat);
        
         status__c stat1 = new Status__c();
        stat1 .Name = 'Approved';
        stat1.code__c = 'Approved';
        statusList.add(stat1);
        
        insert statusList;
        
        SR_status__c srs1 = new SR_status__c();
        srs1.name = 'Approved';
        srs1.code__c='Approved';
        insert srs1;       
        
        step__c stp = new Step__c();
        stp.sr__c = objSR.id;
        stp.status__c = statusList[0].id;
        insert stp;
        
        step__c stpObj = [select step_status__c,status__c,sr__r.customer__r.Roc_status__c from step__c where id =:stp.id limit 1];
        
        string serializedString = Json.Serialize(stpObj);
        
        UserAccessFormActions.ApproveUserstep(serializedString);

        SR_Doc__c objSRDoc = new SR_Doc__c();
        objSRDoc.Service_Request__c = objSR.Id;
        objSRDoc.Is_Not_Required__c = false;
        objSRDoc.Group_No__c = 1;
        objSRDoc.Applicant_Email__c = 'abc@difc.ae';
        objSRDoc.Service_Request__c = objSR.id;
        //objSRDoc.Doc_ID__c = attach.id;
        insert objSRDoc;   

        attachment attach = new attachment();
        attach.Body = blob.valueof('this is a test text');
        attach.Name = 'test Document';
        attach.parentID = objSRDoc.id;
        insert attach;
        
        objSRDoc.Doc_ID__c = attach.id;
        Update objSRDoc;   

        test.startTest();
        UserAccessFormActions.sendUserAccessFormEmail(new set<id>{objSRDoc.id});
        test.stopTest();

    }
     static testmethod Void UserAccessFormActionstestMethod2(){
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Under Formation';
        objAccount.FinTech_Classification__c='FinTech';
        objAccount.Legal_Type_of_Entity__c ='LLP';
        objAccount.ROC_reg_incorp_Date__c =system.today().addmonths(-4);
        objAccount.Financial_Year_End__c ='31st December';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        insert objContact;
    
    
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'New Lease';
        objTemplate.SR_RecordType_API_Name__c = 'EconomySubstance';
        objTemplate.Active__c = true;
        objTemplate.Available_for_menu__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Test Doc Master';
        insert objDocMaster;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.SR_Template__c = objTemplate.Id;
        ID SRrectypeid = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('EconomySubstance').getRecordTypeId();
        objSR = new Service_Request__c();
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.First_Name__c='Test';
        objSR.Last_Name__c='ss';
        objSR.Mobile_Number__c= '+9719803616';
        objSR.Email_Address__c = 'ss@gmail.com';
        objSR.RecordTypeId = SRrectypeid;
        insert objSR;
        
        List<Status__c> statusList = new List<Status__c>();
        status__c stat = new Status__c();
        stat.Name = 'Submitted';
        stat.code__c = 'Submitted';
        statusList.add(stat);
        
        status__c stat1 = new Status__c();
        stat1 .Name = 'Approved';
        stat1.code__c = 'Approved';
        statusList.add(stat1);
        
        insert statusList;
        
        SR_status__c srs1 = new SR_status__c();
        srs1.name = 'Approved';
        srs1.code__c='Approved';
        insert srs1;       
        
        step__c stp = new Step__c();
        stp.sr__c = objSR.id;
        stp.status__c = statusList[0].id;
        insert stp;
        
        step__c stpObj = [select step_status__c,status__c,sr__r.customer__r.Roc_status__c from step__c where id =:stp.id limit 1];
        
        string serializedString = Json.Serialize(stpObj);
        
        UserAccessFormActions.ApproveUserstep(serializedString);
       
        //UserAccessFormActions.dummytest();

     }
}