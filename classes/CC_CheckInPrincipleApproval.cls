/*
    Author      : Durga Prasad
    Date        : 24-Nov-2019
    Description : Custom code to check the open steps for InPrinciple Approval

    v1.0    Merul  19 Feb 2020 Fix for count of approval.
    v1.1    Arun   11-Feb-2021 Added to fix Duplicate steps   
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_CheckInPrincipleApproval implements HexaBPM.iCustomCodeExecutable 
{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) 
    {
    
    
        string strResult = 'true';
        List<string> stepTempateCodes = new List<string>{'CLO_REVIEW','CFO_REVIEW','REGISTRAR_REVIEW'};
        Set<string> setCreatedStep = new Set<string>();
        
        
        if(stp.HexaBPM__SR__c!=null 
                    && stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c=='In_Principle')
        {
            //integer TotalStepsCount = stepTempateCodes.size();
            integer TotalStepsCount = 0;
            integer ApprovedStepsCount = 0;
            for(HexaBPM__Step__c step:[Select Id,HexaBPM__Status__c,HexaBPM__Status_Type__c,
                                              HexaBPM__Status__r.HexaBPM__Rejection__c,
                                              HexaBPM__Status__r.Name,
                                              HexaBPM__Status__r.HexaBPM__Reupload_Document__c,
                                              HexaBPM__Status__r.Return_for_More_Information__c,
                                              Step_Template_Code__c  
                                         FROM HexaBPM__Step__c 
                                        WHERE HexaBPM__SR__c=:stp.HexaBPM__SR__c 
                                          AND Step_Template_Code__c IN:stepTempateCodes])
            {
                // counts total number of created step based of stepTempateCodes 
                //TotalStepsCount = TotalStepsCount + 1;
                setCreatedStep.add(step.Step_Template_Code__c); 
                
                if(
                    step.HexaBPM__Status_Type__c=='End' 
                        && step.HexaBPM__Status__r.Name == 'Approved'
                        && step.HexaBPM__Status__r.HexaBPM__Rejection__c==false 
                                && step.HexaBPM__Status__r.HexaBPM__Reupload_Document__c==false
                                    && step.HexaBPM__Status__r.Return_for_More_Information__c == false
                  )
                {
                    // Actual approved.
                    ApprovedStepsCount = ApprovedStepsCount + 1;
                }    
                    
            }
            system.debug('########## Final Stats');
            system.debug('########## setCreatedStep.size() '+setCreatedStep.size());
            system.debug('########## ApprovedStepsCount '+ApprovedStepsCount);
            
            if( ApprovedStepsCount == setCreatedStep.size())
            {
             // v1.1 
                if([Select Id FROM HexaBPM__Step__c        WHERE HexaBPM__SR__c=:stp.HexaBPM__SR__c 
                                          AND Step_Template_Code__c ='IN_PRINCIPLE_APPROVED'].isEmpty())                strResult = 'true';                 else                strResult = 'false';
                    
                
            }
            
            else                strResult = 'false';
        }
        
        return strResult ;
        
        //system.debug('arun Demo==>'+stp);
         //return 'true';
    }
    
}