/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestCompanyOverviewCls {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        //objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Last RB';
        objContact.Email = 'test@difcportal.com';
        objContact.AccountId = objAccount.Id;
        objContact.FirstName = 'Nagaboina';
        insert objContact;
         
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, 
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        objUser.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services';
        insert objUser;
        
        system.runAs(objUser) {
            ApexPages.StandardController controller = new ApexPages.StandardController(objAccount);
            
            CompanyOverviewCls objCompanyOverviewCls = new CompanyOverviewCls();
            objCompanyOverviewCls.AccountBalance();
            objCompanyOverviewCls.CallType = 'Page';
            objCompanyOverviewCls.AccountBalance();
        }
    }
}