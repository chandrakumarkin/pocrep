public class TrainingRequestViewController {
    
    public String SelectedValue{get;set;}
    public string strViewLink {get;set;}
    public TrainingRequestViewController(Apexpages.standardcontroller controller){
    	
    }
    
    public List<Selectoption> getCommunityRoles(){
    	
    	User eachUser = new User();
    	eachUser = [SELECT Community_User_Role__c FROM USER where Community_User_Role__c INCLUDES 
    				('Company Services','Employee Services','Property Services') AND Id=:userinfo.getuserid()];
    	
    	List<Selectoption> lstOptions = new List<Selectoption>();
    	
    	if(eachUser.Community_User_Role__c.contains('Company Services')){
    		lstOptions.add(new SelectOption('Company Services','Company Services'));
    			
    	}
    	if(eachUser.Community_User_Role__c.contains('Employee Services')){
    		lstOptions.add(new SelectOption('Employee Services','Employee Services'));
    	}
    	if(eachUser.Community_User_Role__c.contains('Property Services')){
    		lstOptions.add(new SelectOption('Property Services','Property Services'));
    	}
    	
    	return lstOptions;
    }
    
    public pagereference trainingRequestPage(){
    	
    	pagereference pref;
        boolean isAvailable = false;
        string keyPrefix = Training_Request__c.sObjectType.getDescribe().getKeyPrefix();
        for(ListView lstView : [SELECT Id FROM ListView WHERE SObjectType = 'Training_Request__c' and Name = :SelectedValue]){
        	String ListviewID = '';
        	ListviewID = String.valueOf((lstView.Id)).substring(0,15);
            strViewLink = '/customers/'+keyPrefix+'?fcf='+ListviewID;
        }
        system.debug('!!@@!!'+SelectedValue);
        
        
        for(Training_Master__c TM : [select id,Name,Start_DateTime__c,To_DateTime__c from Training_Master__c
                                     where Category__c =:SelectedValue and Start_DateTime__c > TODAY
                                     and Status__c = 'Approved by HOD' and Remaining_Seats__c > 0]){
        	isAvailable = true;
        	system.debug('!!@$$$$$@!!'+SelectedValue);
        }
        
        system.debug('!!@@###'+isAvailable);
        if(isAvailable){
        	pref = new pagereference('/trainingrequest?category='+SelectedValue);
        	pref.setredirect(true);
        }
        else if(isAvailable == false && SelectedValue == 'Company Services'){
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error,'No Trainings scheduled for Company Services. please contact roc.helpdesk@difc.ae for more details.'));
        }
        else if(isAvailable == false && SelectedValue == 'Employee Services'){
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error,'No Trainings scheduled for Employ services. please contact GS.helpdesk@difc.ae for more details.'));
        }
        else if(isAvailable == false && SelectedValue == 'Property Services'){
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error,'No Trainings scheduled for Property Services. please contact rorp.helpdesk@difc.ae for more details.'));
        }
        
        return pref;
    }
}