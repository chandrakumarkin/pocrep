/*********************************************************************************************************************
*  Name     : HexaBPM_SecurityStepStatusUpdateBatch 
*  Author   : 
*  Purpose  : This class used to get response from Dubai police portel
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
 V1.0                             12/09/2019
 V1.1         Arun Singh          31-Jan-2021               Added code to support RETRIGGER_SECURITY
**/

global class OB_SecurityStepStatusUpdateBatch implements database.batchable<sobject>,Database.AllowsCallouts, Database.Stateful{
    
    public static string testStatus ='';
    global database.querylocator start(database.batchablecontext bc){
        
        String statusRequest = Label.SECURITY_IN_PROGRESS; 
        List<String> serviceType = Label.Service_Request_Type_Security_Check.split(',');
        String stepName = Label.SECURITY_REVIEW_STEP;
        
        system.debug('----statusRequest---'+statusRequest);
        system.debug('----serviceType---'+serviceType);
        system.debug('----stepName---'+stepName);
        
        String s = 'SELECT Id,Name,HexaBPM__Step_Status__c,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__Status__c, HexaBPM__Step__c.HexaBPM__SR__r.Name FROM HexaBPM__Step__c';
        // V1.1   
        s += ' where SR_Recordtype_Name__c IN '+ serviceType +' AND Step_Template_Code__c in (\'SECURITY_REVIEW\',\'RETRIGGER_SECURITY\')  AND HexaBPM__Step_Status__c =\''+statusRequest+'\'';
        
        //s += ' where SR_Recordtype_Name__c IN '+ serviceType +' AND Step_Template_Code__c =\''+stepName+'\' AND HexaBPM__Step_Status__c =\''+statusRequest+'\'';
        system.debug('=query==test='+s);
        system.debug('=query==test='+database.query(s));
        
        return database.getquerylocator(s);
    }
    
    global void execute(database.batchablecontext bc, List<HexaBPM__Step__c> lstStepData){
        try{
            system.debug('====lstStepData======'+lstStepData);
        String USERNAME = Label.DYFC_API_User_Name;   //This Variable used to Store API user User Name
        String PASSWORD = Label.DYFC_Password;        //This Variable used to Store API user PASSWORD
        String CONSUMER_KEY = Label.DIFC_Consumer_KEY; //This Variable used to Store CONSUMER_KEY
        String CONSUMER_SECRET = Label.DIFC_Consumer_Secret; //This Variable used to Store CONSUMER_SECRET
        List<HexaBPM__Step__c> lstStepDataUpdate = new List<HexaBPM__Step__c>();
        
        Blob headerValue = Blob.valueOf(USERNAME + ':' + PASSWORD);
        String authorizationHeader = Label.Dubai_Police_Response_URL+CONSUMER_KEY+'&client_secret='+CONSUMER_SECRET+'&username='+USERNAME+'&password='+PASSWORD;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(authorizationHeader);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        HttpResponse response = http.send(request);
         system.debug('---response----'+response); 
        system.debug('---response--body--'+response.getBody()); 
        if(response.getStatusCode() == 200){    
            
            Http http1 = new Http();
            HttpRequest request1 = new HttpRequest();
            HttpResponse response1 = new HttpResponse(); 
            
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Object BearerCode1 = results.get('access_token');
            String BearerCode = String.valueOf(BearerCode1);
            BearerCode = 'Bearer '+BearerCode;
            system.debug('---BearerCode----'+BearerCode);
            Map<string,String> statuscodeIDMap = new Map<String,String>();
            List<string> statusList = new List<String>{'REJECTED','RETURN_FOR_MORE_INFORMATION','APPROVED'};
            List<Account> acctList = new List<Account>();
            for(HexaBPM__Status__c status:[Select Id,HexaBPM__Code__c From HexaBPM__Status__c
                                            Where HexaBPM__Code__c IN: statusList]){
                statuscodeIDMap.put(status.HexaBPM__Code__c,status.Id);
            } 
            
            for(HexaBPM__Step__c eachStep:lstStepData){
                Account acct = new Account();
                if(String.IsNotBlank(eachStep.HexaBPM__SR__r.HexaBPM__Customer__c)){
                     acct.Id = eachStep.HexaBPM__SR__r.HexaBPM__Customer__c;
                }
                system.debug(eachStep.HexaBPM__SR__r.Name+'=eachStep==='+eachStep.Id);
                request1 = new HttpRequest();
                request1.setHeader('Authorization',BearerCode);
                request1.setEndpoint(Label.Dubai_Police_Response_URL_2+ eachStep.HexaBPM__SR__r.Name);
                request1.setMethod('GET');
                response1 = http1.send(request1);
                system.debug('---response1.getStatusCode()-----'+response1.getStatusCode());
                system.debug('---response1.getBody()-----'+response1.getBody());
                system.debug('---statuscodeIDMap----'+statuscodeIDMap);
               
                if(response1.getStatusCode() == 200){
                    system.debug('==inside suv==');
                    DubaiPoliceJSONResponse myClass;
                    if(!Test.isRunningTest()){
                        myClass = DubaiPoliceJSONResponse.parse(response1.getBody());
                    }else{
                        myClass = new DubaiPoliceJSONResponse();
                        DubaiPoliceJSONResponse.IssuanceDto ins = new DubaiPoliceJSONResponse.IssuanceDto();
                        DubaiPoliceJSONResponse.RequestCommonData rcd = new DubaiPoliceJSONResponse.RequestCommonData();
                        rcd.policeNotes = 'Police Notes';
                        rcd.status = testStatus;
                        ins.requestCommonData = rcd;                        
                        myClass.issuanceDto = ins;
                         
                    }
                    String Status = myClass.IssuanceDto.requestCommonData.status;
                    String comments = myClass.IssuanceDto.requestCommonData.policeNotes;
                    system.debug('------Status-----'+Status);
                    //Status = 'APPROVED';
                    if(String.isNotBlank(Status) && Status =='APPROVED'){
                        eachStep.HexaBPM__Status__c = statuscodeIDMap != null && statuscodeIDMap.size() != 0 && statuscodeIDMap.containskey('APPROVED') ? statuscodeIDMap.get('APPROVED'):null;
                        eachStep.HexaBPM__Step_Notes__c= comments;
                        eachStep.HexaBPM__Closed_Date__c = system.today();
                        lstStepDataUpdate.add(eachStep);
                        //account
                        acct.Security_Review_Date__c= system.now();
                        acct.Security_Review_Status__c= 'Approved';
                        acctList.add(acct);
                    } 
                    else if(String.isNotBlank(Status) && Status =='NEED_MORE_INFO'){
                        eachStep.HexaBPM__Status__c = statuscodeIDMap != null && statuscodeIDMap.size() != 0 && statuscodeIDMap.containskey('RETURN_FOR_MORE_INFORMATION') ? statuscodeIDMap.get('RETURN_FOR_MORE_INFORMATION'):null;
                        eachStep.OwnerID= Label.ROC_Officer_Queue;
                        eachStep.HexaBPM__Step_Notes__c= comments;
                        lstStepDataUpdate.add(eachStep);
                        //account
                        acct.Security_Review_Date__c= system.now();
                        acct.Security_Review_Status__c= 'Return for More Information';
                        acctList.add(acct);
                    }
                    else if(String.isNotBlank(Status) && Status =='Rejected'){
                        eachStep.HexaBPM__Status__c = statuscodeIDMap != null && statuscodeIDMap.size() != 0 && statuscodeIDMap.containskey('REJECTED') ? statuscodeIDMap.get('REJECTED'):null;
                        eachStep.HexaBPM__Step_Notes__c= comments;
                        lstStepDataUpdate.add(eachStep);
                        //account
                        acct.Security_Review_Date__c= system.now();
                        acct.Security_Review_Status__c= 'Rejected';
                        acctList.add(acct);
                    }
                }      
            }
            system.debug('==lstStepDataUpdate=='+lstStepDataUpdate);
            if(lstStepDataUpdate!=null && lstStepDataUpdate.size()>0){
                database.update(lstStepDataUpdate);
                database.update(acctList);
                
            }   
        }
        }catch(Exception e){
            string errorresult = e.getMessage();
            insert LogDetails.CreateLog(null, 'OB_SecurityStepStatusUpdateBatch : execute', 'Line Number : '+e.getLineNumber()+'\nException is : '+errorresult);
        }
    }
    
    global void finish(database.batchablecontext bc){
    
    }
    
}