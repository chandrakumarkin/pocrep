/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_SRTemplateDeepClone {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Sample_Test_RecType';
        objTemplate.SR_RecordType_API_Name__c = 'Sample_Test_RecType';
        objTemplate.ORA_Process_Identification__c = 'Sample_Test_RecType';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Step_Template__c objStepType = new Step_Template__c();
        objStepType.Name = 'Approve Sample_Test_RecType';
        objStepType.Code__c = 'Sample_Test_RecType';
        objStepType.Step_RecordType_API_Name__c = 'Sample_Test_RecType';
        objStepType.Summary__c = 'Sample_Test_RecType';
        insert objStepType;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'AR Third Party NOC';
        insert objDocMaster;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'REJECTED';
        lstStatus.add(objStatus);
        
        insert lstStatus;
        
        /*
        Transaction_Type__c objTranType = new Transaction_Type__c();
        objTranType.Name = 'Test TranType';
        insert objTranType;
        */
        
        Product2 objProd = new Product2();
        objProd.Name = 'Sample Test';
        objProd.ProductCode = 'SampleTest';
        objProd.IsActive = true;
        //objProd.ORA_AR_Transaction_Type__c = objTranType.Id;
        insert objProd;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
	    SR_Status__c objSRStatus;
	    objSRStatus = new SR_Status__c();
	    objSRStatus.Name = 'Closed';
	    objSRStatus.Code__c = 'CLOSED';
	    objSRStatus.Type__c = 'End';
	    lstSRStatus.add(objSRStatus);
	    
	    objSRStatus = new SR_Status__c();
	    objSRStatus.Name = 'Rejected';
	    objSRStatus.Code__c = 'REJECTED';
	    objSRStatus.Type__c = 'Rejected';
	    lstSRStatus.add(objSRStatus);
	    
	    objSRStatus = new SR_Status__c();
	    objSRStatus.Name = 'Awaiting Approval';
	    objSRStatus.Code__c = 'AWAITING_APPROVAL';
	    objSRStatus.Type__c = 'Start';
	    lstSRStatus.add(objSRStatus);
	    
	    objSRStatus = new SR_Status__c();
	    objSRStatus.Name = 'Approved';
	    objSRStatus.Code__c = 'APPROVED';
	    objSRStatus.Type__c = 'Intermediate';
	    lstSRStatus.add(objSRStatus);
	    
	    objSRStatus = new SR_Status__c();
	    objSRStatus.Name = 'Submitted';
	    objSRStatus.Code__c = 'SUBMITTED';
	    lstSRStatus.add(objSRStatus);
	    
	    objSRStatus = new SR_Status__c();
	    objSRStatus.Name = 'Draft';
	    objSRStatus.Code__c = 'DRAFT';
	    lstSRStatus.add(objSRStatus);
	    
	    insert lstSRStatus;
	    
	    list<Transition__c> lstTransitions = new list<Transition__c>();
	    Transition__c objTransition = new Transition__c();
	    objTransition.From__c = lstStatus[0].Id; // awaiting approval
	    objTransition.To__c = lstStatus[1].Id; // approved
	    lstTransitions.add(objTransition);
	    
	    objTransition = new Transition__c();
	    objTransition.From__c = lstStatus[1].Id; // approved
	    objTransition.To__c = lstStatus[2].Id; // Rejected
	    lstTransitions.add(objTransition);
	    
	    insert lstTransitions;
    
    	SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = objTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Step_Template__c = objStepType.Id; // Step Type
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
    	insert objSRStep;
    
    	SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Evaluated_at__c = objSRStep.Id;
        objTempDocs.Validated_at__c = objSRStep.Id;
        objTempDocs.Original_Sighted_at__c = objSRStep.Id;
        objTempDocs.Courier_collected_at__c = objSRStep.Id;
        objTempDocs.Courier_delivered_at__c = objSRStep.Id;
        objTempDocs.Generate_Document__c = true;
        insert objTempDocs;
        
        
    	SR_Template_Item__c objTemplItem = new SR_Template_Item__c();
        objTemplItem.Consumed_at__c = objSRStep.Id;
        objTemplItem.Evaluated_at__c = objSRStep.Id;
        objTemplItem.On_Submit__c = true;
        objTemplItem.Product__c = objProd.Id;
        objTemplItem.SR_Template__c = objTemplate.Id;
        insert objTemplItem;
        
        
	    list<Step_Transition__c> lstStepTransitions = new list<Step_Transition__c>();
	    Step_Transition__c objStepTran;
	    objStepTran = new Step_Transition__c();
	    objStepTran.SR_Step__c = objSRStep.Id;
	    objStepTran.SR_Status_Internal__c = lstSRStatus[0].Id;
	    objStepTran.SR_Status_External__c = lstSRStatus[0].Id;
	    objStepTran.Transition__c = lstTransitions[0].Id;
	    lstStepTransitions.add(objStepTran);
	    
	    objStepTran = new Step_Transition__c();
	    objStepTran.SR_Step__c = objSRStep.Id;
	    objStepTran.SR_Status_Internal__c = lstSRStatus[1].Id;
	    objStepTran.SR_Status_External__c = lstSRStatus[1].Id;
	    objStepTran.Transition__c = lstTransitions[1].Id;
	    lstStepTransitions.add(objStepTran);
	        
	    Business_Rule__c objBR = new Business_Rule__c();
	    objBR.Action_Type__c = 'Custom Conditions Actions';
	    objBR.Execute_on_Insert__c = true;
	    objBR.Execute_on_Update__c = true;
	    objBR.SR_Steps__c = objSRStep.Id;
	    insert objBR;
	    
	    list<Condition__c> lstCond = new list<Condition__c>();
	    
	    Condition__c objCon = new Condition__c();
	    objCon.Object_Name__c = 'Service_Request__c';
	    objCon.Field_Name__c = 'Name';
	    objCon.Operator__c = '!=';
	    objCon.Value__c = 'null';
	    objCon.Business_Rule__c = objBR.Id;
	    lstCond.add(objCon);
	    
	    objCon = new Condition__c();
	    objCon.Object_Name__c = 'Service_Request__c';
	    objCon.Field_Name__c = 'Name';
	    objCon.Operator__c = '!=';
	    objCon.Value__c = 'null';
	    objCon.SR_Template_Docs__c = objTempDocs.Id;
	    lstCond.add(objCon);
	    
	    insert lstCond;
	    
	    list<Action__c> lstActions = new list<Action__c>();
	    Action__c objAction = new Action__c();
	    objAction.Business_Rule__c = objBR.Id;
	    objAction.Action_Type__c = 'Initiate SubSR';
	    objAction.Field_Value__c = objStepType.Id;
	    objAction.Field_Type__c = 'string';
	    lstActions.add(objAction);
	    
	    objAction = new Action__c();
	    objAction.Business_Rule__c = objBR.Id;
	    objAction.Action_Type__c = 'Account';
	    objAction.Field_Value__c = objStepType.Id;
	    objAction.Field_Type__c = 'string';
	    
	    objAction = new Action__c();
	    objAction.Business_Rule__c = objBR.Id;
	    objAction.Action_Type__c = 'Registration';
	    objAction.Field_Value__c = objStepType.Id;
	    objAction.Field_Type__c = 'string';
	    
	    objAction = new Action__c();
	    objAction.Business_Rule__c = objBR.Id;
	    objAction.Action_Type__c = 'Step';
	    objAction.Field_Value__c = objStepType.Id;
	    objAction.Field_Type__c = 'string';
	    lstActions.add(objAction);
	    
	    insert lstActions;
	    
	    SRTemplateDeepClone objDeepClone = new SRTemplateDeepClone();
        objDeepClone.strSRTemplateName = 'Test1_Template';
        objDeepClone.strSRRecTypeName = 'Test1_Template';
        ApexPages.currentPage().getParameters().put('Id',objTemplate.Id);
        objDeepClone.strOldSRTemplateId = objTemplate.Id;
        objDeepClone.CloneProcessRecords();
        objDeepClone.CancelClone();
        
        SRTemplateDeepDelete.DeepDelete(objTemplate.Id);
    
    }
}