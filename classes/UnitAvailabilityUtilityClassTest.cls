/****************************************************************
*Class name : UnitAvailabilityUtilityClassTest
* Description ;Test Class for UnitAvailabilityUtilityClass  Apex Class
****************************************************************/
@isTest
private class UnitAvailabilityUtilityClassTest{
    
    public static Account objAccount;
    static Building__c objBuilding ;
    static map<string,string> mapRecordType = new map<string,string>();
    static List<Service_Request__c> servReqList = new List<Service_Request__c>();
    static list<Unit__c> lstUnits;
    public static void prepareData(){
        
        objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        insert objBuilding;
        
        lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        objUnit.Is_official_Search__c=true;
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        objAccount.RORP_License_No__c='R123';
        objAccount.Issuing_Authority__c ='IC';
        insert objAccount;
        
        Contact c1= new Contact();
        c1.lastname='c1';
        c1.Passport_No__c='P123';
        c1.Nationality__c='india';
        c1.RecordTypeId= [select id from RecordType where DeveloperName='GS_Contact' and sObjectType='contact' LIMIT 1].Id;
        insert c1;
       
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('User_Access_Form','RORP_User_Access_Form','IT_User_Access_Form','')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        
    }
   
    static TestMethod void unit2(){
        test.starttest();
        prepareData();
        Lease__c lease = new Lease__c(Account__c=objAccount.id,Type__c='Leased',Status__c='Active',Lease_Types__c='Data Centre Lease',Lease_Termination_Date__c=system.today());
        insert lease;
        Tenancy__c ten = new Tenancy__c(Unit__c=lstUnits[0].id,Lease__c=lease.id);
        insert ten;
        UnitAvailabilityUtilityClass uc = new UnitAvailabilityUtilityClass();
        UnitAvailabilityUtilityClass.checkunitStatus(lstUnits);
        test.stopTest();
        
    }
}