/*************************************************************************************************
 *  Name        : CreateShipmentfornOnSponEmpCancellation
 *  Author    : Shoaib Tariq
 *  Company  : Tech Carrot
 *  Date        : 2020-05-01     
 *  Purpose  : This class is to call the Courier webservice 
 **************************************************************************************************/
public class CreateShipmentfornOnSponEmpCancellation {
    @InvocableMethod
    public static void createShippingStatus(list <id> objId ){
        if(!test.isRunningTest() ){ List<step__c> objStp = [SELECT Id,SAP_DOCCR__c ,Step_Name__c ,Is_Letter__c ,SAP_DOCDL__c,SR__c,Customer_Name__c,Status_Code__c,SR_Group__c,SAP_CTOTL__c,SAP_CTEXT__c,SAP_DOCCO__c,SR_Record_Type__c,SAP_DOCRC__c FROM STEP__C  WHERE ID =:objId  LIMIT 1];
                                   if([Select count() from Service_request__c where id=:objStp[0].SR__c and Would_you_like_to_opt_our_free_couri__c ='Yes'] > 0)  GsCourierDetails.CreateCourierStep(objStp);}    }
   
   public static void checkShipmentStatus(list <id> objId ){
      system.debug('Add code here');
   }
}