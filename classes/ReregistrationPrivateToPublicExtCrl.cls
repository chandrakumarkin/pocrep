/*
Created By  : Suchita - DIFC on 17 Nov 2020
Description : This Class is extension class for ReregistrationPrivateToPublic visualforce page
--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No        Date                Updated By          Description
v1.0      17 Nov 2020          Suchita Sharma,       This Class is extension class for ReregistrationPrivateToPublic visualforce page

----------------------------------------------------------------------------------------
*/
public with sharing class ReregistrationPrivateToPublicExtCrl {
    
    public Service_Request__c SRData { get; set;}
    public string RecordTypeId;
    public map < string, string > mapParameters;
    public Attachment ObjAttachment { get;set; }
    public boolean SRisValid { get;set; }
    public Account ObjAccount { get;set; }
    public Decimal IssuedShareCapitalAccShare;
    private string Rtype; //sR Record in URL 
    public List<String> SuffixList = new List<String>{'LTD', 'ltd.','Limited'};
    public boolean IsSuffixAvailable{get;set;}
    public string EntityName{get;set;}
    public List<SelectOption> suffixOption{set;}
    public String selectedSuffix ='PLC';//13167
    public boolean IsSuffixAvailableTrade{get;set;}
    public string TradeName{get;set;}
    public String selectedSuffixTrade='PLC';//13167
    public integer issuedShareCapitalReregistrationLimit = 100000;
    public string ReregistrationPrivateToPublicValidation = 'The entity must have a share capital of at least USD 100,000 to be able to submit this service request';
    //Constructor
    public ReregistrationPrivateToPublicExtCrl (ApexPages.StandardController controller) {
        
        SRisValid = false;
        SRData = (Service_Request__c) controller.getRecord();
        
        Service_Request__c SRDataObj;
        if( SRData.Id != null ){
            SRDataObj = [SELECT id,Record_Type_Name__c,RecordTypeId,Entity_Name__c,Proposed_Trading_Name_1__c FROM Service_Request__c WHERE Id =: SRData.Id];
            IsSuffixAvailable = false;
            if(SRDataObj.Entity_Name__c!=null){
                String EntityNameWithSuffix = SRDataObj.Entity_Name__c;
                EntityName= EntityNameWithSuffix;
                IsSuffixAvailable = false;
                for(String suffixString:SuffixList){
                    if(EntityNameWithSuffix.endsWithIgnoreCase(suffixString)){
                        IsSuffixAvailable = true;
                        EntityName = EntityNameWithSuffix.removeEndIgnoreCase(suffixString);
                        EntityName = EntityName+' '+selectedSuffix;//13167
                        break;
                    }
                }
            }
            IsSuffixAvailableTrade = false;
            If(SRDataObj.Proposed_Trading_Name_1__c!=null){
               String TradeNameWithSuffix = SRDataObj.Proposed_Trading_Name_1__c;
                TradeName= TradeNameWithSuffix;
                IsSuffixAvailableTrade = false;
                for(String suffixStringTrade :SuffixList){
                    if(TradeNameWithSuffix.endsWithIgnoreCase(suffixStringTrade)){
                        IsSuffixAvailableTrade = true;
                        TradeName = TradeNameWithSuffix.removeEndIgnoreCase(suffixStringTrade);
                        TradeName = TradeName +' '+selectedSuffixTrade;//13167
                        break;
                    }
                }   
            }
            
        }
        
        ObjAccount = new account();
        ObjAttachment = new Attachment();
        if (apexpages.currentPage().getParameters() != null)
            mapParameters = apexpages.currentPage().getParameters();
        if (mapParameters.get('RecordType') != null)
            RecordTypeId = mapParameters.get('RecordType');
        if (mapParameters.get('type') != null){
            Rtype = mapParameters.get('type');
        }else if( SRDataObj != null){
            Rtype = SRDataObj.Record_Type_Name__c;
            RecordTypeId = SRDataObj.RecordTypeId;
        }
        PopulateInitialData();  
        if(IssuedShareCapitalAccShare < issuedShareCapitalReregistrationLimit){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.SEVERITY.WARNING, ReregistrationPrivateToPublicValidation);
            ApexPages.addMessage(myMsg);
        }else{SRisValid = true;}
    }
    public List<SelectOption> getsuffixOption () {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('PLC','PLC'));
        options.add(new SelectOption('Plc','Plc'));
        return options;
    }
    public PageReference SaveRecord() {
        //integer issuedShareCapitalReregistrationLimit = Integer.valueof(System.Label.IssuedShareCapitalReregistration);
        Date dt = System.today();//current date
        //if(IssuedShareCapitalAccShare!=null && IssuedShareCapitalAccShare >= issuedShareCapitalReregistrationLimit){
            try {
                if(IsSuffixAvailable){
                    SRData.Entity_Name__c =  EntityName;//13167
                    SRData.Legal_entity_of_company_before_transfer__c = selectedSuffix;
                }
                if(IsSuffixAvailableTrade){
                    SRData.Proposed_Trading_Name_1__c =  TradeName;//13167
                    SRData.Legal_entity_of_company_before_transfer__c = selectedSuffixTrade;
                }
                 
                 upsert SRData;
                 PageReference acctPage = new ApexPages.StandardController(SRData).view();
                 acctPage.setRedirect(true);
                 return acctPage;
                }catch (Exception e) {
                 ApexPages.Message myMsg =new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getDmlMessage(0));
                 ApexPages.addMessage(myMsg);
                 return NULL;
                }
       // return null;
        /*}else{
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.SEVERITY.FATAL, ReregistrationPrivateToPublicValidation);
            ApexPages.addMessage(myMsg);
        }*/
        
    }
    
    public void PopulateInitialData(){
        for (User objUsr: [SELECT id, ContactId, Email, Contact.Account.Legal_Type_of_Entity__c, Phone, Contact.AccountId, 
                           Contact.Account.Name,Contact.Account.Issued_Share_Capital_AccShare__c,
                           Contact.Account.Arabic_Name__c,Contact.Account.Trade_Name__c,Contact.Account.Trading_Name_Arabic__c
                           FROM User 
                           WHERE Id =: userinfo.getUserId() ]) {
                               IssuedShareCapitalAccShare = objUsr.Contact.Account.Issued_Share_Capital_AccShare__c;
                               
                               if (SRData.id == null) {
                                   SRData.Customer__c = objUsr.Contact.AccountId;
                                   SRData.RecordTypeId = RecordTypeId;
                                   SRData.Email__c = objUsr.Email;
                                   SRData.Legal_Structures__c = objUsr.Contact.Account.Legal_Type_of_Entity__c;
                                   SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                                   SRData.Entity_Name__c = objUsr.Contact.Account.Name;
                                   IsSuffixAvailable = false;
                                   if(objUsr.Contact.Account.Name!=null){
                                      String EntityNameWithSuffix = objUsr.Contact.Account.Name;
                                      EntityName= EntityNameWithSuffix;
                                      for(String suffixString:SuffixList){
                                           if(EntityNameWithSuffix.endsWithIgnoreCase(suffixString)){
                                               IsSuffixAvailable = true;
                                               EntityName = EntityNameWithSuffix.removeEndIgnoreCase(suffixString);
                                               EntityName = EntityName+' '+selectedSuffix;//13167
                                               break;
                                            }
                                        }  
                                   }
                                   IsSuffixAvailableTrade = false;
                                   if(objUsr.Contact.Account.Trade_Name__c!=null){
                                       String TradeNameWithSuffix = objUsr.Contact.Account.Trade_Name__c;
                                       TradeName= TradeNameWithSuffix;
                                       for(String suffixStringTrade:SuffixList){
                                           if(TradeNameWithSuffix.endsWithIgnoreCase(suffixStringTrade)){
                                               IsSuffixAvailableTrade = true;
                                               TradeName = TradeNameWithSuffix.removeEndIgnoreCase(suffixStringTrade);
                                               TradeName = TradeName +' '+selectedSuffixTrade;//13167
                                               break;
                                            }
                                       }
                                   }
                                   
                                   SRData.Previous_Entity_Name__c = objUsr.Contact.Account.Name;
                                   SRData.Pre_Entity_Name_Arabic__c= objUsr.Contact.Account.Arabic_Name__c;
                                   SRData.Pro_Entity_Name_Arabic__c= objUsr.Contact.Account.Arabic_Name__c;
                                   SRData.Previous_Trading_Name_1__c = objUsr.Contact.Account.Trade_Name__c;
                                   SRData.Proposed_Trading_Name_1__c = objUsr.Contact.Account.Trade_Name__c;
                                   SRData.Pre_Trade_Name_1_in_Arab__c=objUsr.Contact.Account.Trading_Name_Arabic__c;
                                   SRData.pro_trade_name_1_in_arab__c=objUsr.Contact.Account.Trading_Name_Arabic__c;
                                   ObjAccount = objUsr.Contact.Account;
                               }
                           }
    }
}