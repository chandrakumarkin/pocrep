/*
* OB_IntegrationHelper
*/
@isTest
public with sharing class OB_IntegrationHelperTest {
	@isTest
    private static void initDubaiPolice(){  
    	// create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insertNewContacts[0].Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Previous_Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Country_of_Birth__c  = 'Russian Federation';
        insertNewContacts[0].Country__c  = 'Russian Federation';
        insertNewContacts[0].Issued_Country__c  = 'Russian Federation';
        insertNewContacts[0].Gender__c  = 'Male';
        insertNewContacts[0].UID_Number__c  = '9712';
        insertNewContacts[0].MobilePhone  = '+97123576565';
        insertNewContacts[0].Passport_No__c  = '+97123576565';
        insertNewContacts[0].recordTypeId = portalUserId;
        insert insertNewContacts;
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle','New_User_Registration'});
        insert createSRTemplateList;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'New_User_Registration', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].HexaBPM__Contact__c=insertNewContacts[0].id;
        insertNewSRs[1].HexaBPM__Contact__c=insertNewContacts[0].id;
        insertNewSRs[0].Foreign_Entity_Name__c='Test Forien';
        insertNewSRs[0].Place_of_Registration_parent_company__c='Russian Federation';
        
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Non - financial';
        insertNewSRs[1].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','In_Principle');
        insertNewSRs[1].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','AOR_Financial');
        insert insertNewSRs;
        
        HexaBPM__Status__c srStatus = new HexaBPM__Status__c();
        srStatus.Name = 'In Progress';
        srStatus.HexaBPM__Code__c = 'In Progress';
		insert srStatus;
        
        List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(1,
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'In_Principle','AOR_Financial'},
                                                        new List<String>{'Test'});

        insert stepTemplate;
        
        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
        objSRSteps.HexaBPM__Active__c = true;
       // objSRSteps.HexaBPM__Status__c = srStatus[0].Id;
        insert objSRSteps;
        
        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(2,insertNewSRs,new List<HexaBPM__SR_Steps__c>{objSRSteps});
		stepList[0].HexaBPM__Status__c = srStatus.Id;
		stepList[1].HexaBPM__Status__c = srStatus.Id;
		insert stepList;
        
        
        HexaBPM__Document_Master__c doc= new HexaBPM__Document_Master__c();
        doc.HexaBPM__Available_to_client__c = true;
        doc.HexaBPM__Code__c = 'UBO_FOUNDATION';
        doc.Download_URL__c = 'test';
        insert doc;
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Role__c = 'Shareholder';
        objAmendment.Passport_No__c = '12345';
        objAmendment.Gender__c = 'Male';
        objAmendment.Mobile__c = '+9712346572';
        objAmendment.DI_Mobile__c = '+97124567882';
        objAmendment.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment);
        HexaBPM_Amendment__c objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment3.Nationality_list__c = 'India';
        objAmendment3.Role__c = 'Shareholder';
        objAmendment3.Passport_No__c = '1r45';
        objAmendment3.Gender__c = 'Female';
        objAmendment3.Mobile__c = '+9712346572';
        objAmendment3.DI_Mobile__c = '+97124567882';
        objAmendment3.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment3);

        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment1.Nationality_list__c = 'India';
        objAmendment1.Passport_No__c = '1234';
        objAmendment1.Role__c = 'Director';
        objAmendment1.Gender__c = 'Male';
        objAmendment1.DI_Mobile__c = '+97124567882';
        objAmendment1.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment1.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment1);

        HexaBPM_Amendment__c objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        objAmendment2.Country_of_Registration__c = 'India';
        objAmendment2.Registration_No__c = '1234';
        objAmendment2.Role__c = 'Shareholder;Director';
        objAmendment2.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment2.DI_Mobile__c = '+97124567882';
        objAmendment2.Company_Name__c = '+97124567882';
        objAmendment2.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment2);
        insert listAmendment;
        
        HexaBPM__SR_Doc__c srDoc = new HexaBPM__SR_Doc__c();
        srDoc.HexaBPM_Amendment__c = listAmendment[0].Id;
        srDoc.HexaBPM__Doc_ID__c = listAmendment[0].Id;
        srDoc.Name = 'Passport';
        srDoc.HexaBPM__Service_Request__c=insertNewSRs[0].Id;
        insert srDoc;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = srDoc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        List<Product2>  prodList = OB_TestDataFactory.createProduct(new list<String>{'TEST'});
        insert prodList;
        
        HexaBPM__Pricing_Line__c lineItem = OB_TestDataFactory.createSRPriceItem();
        insert lineItem;
        
        List<HexaBPM__SR_Price_Item__c> pricList = OB_TestDataFactory.createSRPriceItem(prodList,insertNewSRs[0].Id,lineItem.Id);
        pricList[0].HexaBPM__Status__c='test';
        pricList[0].HexaBPM__Price__c=2;
        pricList[0].HexaBPM__ServiceRequest__c=insertNewSRs[0].Id;
        insert pricList;
        
        List<HexaBPM__SR_Price_Item__c> pricList1 = OB_TestDataFactory.createSRPriceItem(prodList,insertNewSRs[1].Id,lineItem.Id);
        pricList1[0].HexaBPM__Status__c='test';
        pricList1[0].HexaBPM__Price__c=2;
        pricList1[0].HexaBPM__ServiceRequest__c=insertNewSRs[1].Id;
        insert pricList1;
        WebService_Details__c wed = new WebService_Details__c();
        wed.Name ='Credentials';
        wed.Password__c ='test';
        wed.SAP_User_Id__c ='SFIUSER';
        wed.Username__c ='SFIUSER';
        insert wed;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://eccdev.com/sampledata';
        insert objEP;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        test.starttest();
        Test.setMock(WebServiceMock.class, new TestServiceCreationCls());
        
        list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = pricList[0].Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = pricList[0].Id;
        objTemp.SFMSG = 'Records are Inserted Successfully in the custom table';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        
        list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
        SAPECCWebService.ZSF_S_ACC_BAL objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        
        TestServiceCreationCls.resItems = resItems;
        TestServiceCreationCls.resActBals = resActBals;
        
        system.debug('==price='+[select id,HexaBPM__ServiceRequest__r.RecordType.DeveloperName,HexaBPM__Status__c,HexaBPM__Price__c,Transaction_Id__c,Company_Code__c From HexaBPM__SR_Price_Item__c]);
        
        list<string> lstSRIds = new list<string>{insertNewSRs[0].Id,insertNewSRs[1].Id};
        OB_IntegrationHelper.SendPriceItemsToSAP(lstSRIds,true);
        
        
        test.stoptest();
    	
    }
	/*@isTest
    private static void initSAPScheduler(){  
    	// create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insertNewContacts[0].Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Previous_Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Country_of_Birth__c  = 'Russian Federation';
        insertNewContacts[0].Country__c  = 'Russian Federation';
        insertNewContacts[0].Issued_Country__c  = 'Russian Federation';
        insertNewContacts[0].Gender__c  = 'Male';
        insertNewContacts[0].UID_Number__c  = '9712';
        insertNewContacts[0].MobilePhone  = '+97123576565';
        insertNewContacts[0].Passport_No__c  = '+97123576565';
        insertNewContacts[0].recordTypeId = portalUserId;
        insert insertNewContacts;
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle','New_User_Registration'});
        insert createSRTemplateList;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','In Progress','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'New_User_Registration', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].HexaBPM__Contact__c=insertNewContacts[0].id;
        insertNewSRs[1].HexaBPM__Contact__c=insertNewContacts[0].id;
        insertNewSRs[0].Foreign_Entity_Name__c='Test Forien';
        insertNewSRs[0].Place_of_Registration_parent_company__c='Russian Federation';
        
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__External_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Non - financial';
        insertNewSRs[1].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','In_Principle');
        insertNewSRs[1].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','AOR_Financial');
        insert insertNewSRs;
        
        HexaBPM__Status__c srStatus = new HexaBPM__Status__c();
        srStatus.Name = 'In Progress';
        srStatus.HexaBPM__Code__c = 'In Progress';
		insert srStatus;
        
        List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(1,
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'In_Principle','AOR_Financial'},
                                                        new List<String>{'Test'});

        insert stepTemplate;
        
        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
        objSRSteps.HexaBPM__Active__c = true;
       // objSRSteps.HexaBPM__Status__c = srStatus[0].Id;
        insert objSRSteps;
        
        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(2,insertNewSRs,new List<HexaBPM__SR_Steps__c>{objSRSteps});
		stepList[0].HexaBPM__Status__c = srStatus.Id;
		stepList[1].HexaBPM__Status__c = srStatus.Id;
		insert stepList;
        
        
        HexaBPM__Document_Master__c doc= new HexaBPM__Document_Master__c();
        doc.HexaBPM__Available_to_client__c = true;
        doc.HexaBPM__Code__c = 'UBO_FOUNDATION';
        doc.Download_URL__c = 'test';
        insert doc;
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Role__c = 'Shareholder';
        objAmendment.Passport_No__c = '12345';
        objAmendment.Gender__c = 'Male';
        objAmendment.Mobile__c = '+9712346572';
        objAmendment.DI_Mobile__c = '+97124567882';
        objAmendment.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment);
        HexaBPM_Amendment__c objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment3.Nationality_list__c = 'India';
        objAmendment3.Role__c = 'Shareholder';
        objAmendment3.Passport_No__c = '1r45';
        objAmendment3.Gender__c = 'Female';
        objAmendment3.Mobile__c = '+9712346572';
        objAmendment3.DI_Mobile__c = '+97124567882';
        objAmendment3.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment3);

        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment1.Nationality_list__c = 'India';
        objAmendment1.Passport_No__c = '1234';
        objAmendment1.Role__c = 'Director';
        objAmendment1.Gender__c = 'Male';
        objAmendment1.DI_Mobile__c = '+97124567882';
        objAmendment1.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment1.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment1);

        HexaBPM_Amendment__c objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        objAmendment2.Country_of_Registration__c = 'India';
        objAmendment2.Registration_No__c = '1234';
        objAmendment2.Role__c = 'Shareholder;Director';
        objAmendment2.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment2.DI_Mobile__c = '+97124567882';
        objAmendment2.Company_Name__c = '+97124567882';
        objAmendment2.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment2);
        insert listAmendment;
        
        HexaBPM__SR_Doc__c srDoc = new HexaBPM__SR_Doc__c();
        srDoc.HexaBPM_Amendment__c = listAmendment[0].Id;
        srDoc.HexaBPM__Doc_ID__c = listAmendment[0].Id;
        srDoc.Name = 'Passport';
        srDoc.HexaBPM__Service_Request__c=insertNewSRs[0].Id;
        insert srDoc;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = srDoc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        List<Product2>  prodList = OB_TestDataFactory.createProduct(new list<String>{'TEST'});
        insert prodList;
        
        HexaBPM__Pricing_Line__c lineItem = OB_TestDataFactory.createSRPriceItem();
        insert lineItem;
        
        List<HexaBPM__SR_Price_Item__c> pricList = OB_TestDataFactory.createSRPriceItem(prodList,insertNewSRs[0].Id,lineItem.Id);
        pricList[0].HexaBPM__Status__c='test';
        pricList[0].HexaBPM__Price__c=2;
        pricList[0].HexaBPM__ServiceRequest__c=insertNewSRs[0].Id;
        insert pricList;
        
        List<HexaBPM__SR_Price_Item__c> pricList1 = OB_TestDataFactory.createSRPriceItem(prodList,insertNewSRs[1].Id,lineItem.Id);
        pricList1[0].HexaBPM__Status__c='test';
        pricList1[0].HexaBPM__Price__c=2;
        pricList1[0].HexaBPM__ServiceRequest__c=insertNewSRs[1].Id;
        insert pricList1;
        WebService_Details__c wed = new WebService_Details__c();
        wed.Name ='Credentials';
        wed.Password__c ='test';
        wed.SAP_User_Id__c ='SFIUSER';
        wed.Username__c ='SFIUSER';
        insert wed;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://eccdev.com/sampledata';
        insert objEP;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        
        list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = pricList[0].Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = pricList[0].Id;
        objTemp.SFMSG = 'Records are Inserted Successfully in the custom table';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        
        list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
        SAPECCWebService.ZSF_S_ACC_BAL objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        
        TestServiceCreationCls.resItems = resItems;
        TestServiceCreationCls.resActBals = resActBals;
        
        system.debug('==price='+[select id,HexaBPM__ServiceRequest__r.RecordType.DeveloperName,HexaBPM__Status__c,HexaBPM__Price__c,Transaction_Id__c,Company_Code__c From HexaBPM__SR_Price_Item__c]);
        test.starttest();
        Test.setMock(WebServiceMock.class, new TestServiceCreationCls());
        
        OB_SendPriceItemsToSAPScheduler sh1 = new OB_SendPriceItemsToSAPScheduler();      
		 String sch = '0 0 23 * * ?';
		        system.schedule('Test check', sch, sh1);
        test.stoptest();
    	
    }*/
}