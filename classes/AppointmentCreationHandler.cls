/*
1. Appointment at lunch time needs to be corrected.
2. If appointments are scheduled for the tomorrow the lunch time of tomorrow should be taken which is not considered as of now (Consider only time of the day)
3. Code is working fine for the next day moosa reported some other issue.
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By       Description
---------------------------------------------------------------------------------------------------------------------             
V1.0                                 Created
V2.0    19-Jan-2019  Mudasir         Updated the logic for the 45 minute appointment.
v2.1    22-Apr-2019  Azfer           Updated the custom labels and other logic.

*/
public class AppointmentCreationHandler {

    public static Appointment__c Appntment = new Appointment__c();
    public static boolean isnextday = false;
    public static string Counter = system.label.Medical_Appointment_Counter_Mins;
    public static Integer LunchDuration = Integer.valueof(system.label.Lunch_Duration);
    public static Integer ExpressLunchDuration = Integer.valueof(system.label.Express_Lunch_Duration);
    public static string Lunch_Time = system.label.Lunch_Time;
    public static string Lunch_Time_Express = system.label.Lunch_Time_Express;
    public static integer Mins = Integer.valueof(Counter);
    public static integer ExpressMins = Integer.valueof(system.label.Medical_Express_Mins);
    public static integer ExpressMins10 = Integer.valueof(system.label.Express_Mins_10);
    public static string ExpressLunchHour = system.label.Express_Lunch_Hours;
    public static integer min5 = Integer.valueOF(system.label.min5);

    public static void MedicalAppointmentCreate(Step__c step) {
        /************* moved the appointment logic to AppointmentCreationUtilityCls Class********/
    if(label.EnableAppointmentOldLogic=='TRUE'){ //execute old logic
        List < service_request__c > SR = [select id, Express_Service__c, name, first_Name__c, Last_Name__c from service_request__c where id =: Step.sr__c limit 1];

        DateTime Cdt = step.createdDate.addDays(1);
        Date createdDate = Date.newinstance(cdt.year(), cdt.month(), cdt.day());
        Date ExpressDate = Date.newinstance(step.createdDate.year(), step.createdDate.month(), step.createdDate.Day());

        if (SR.size() > 0 && SR[0].Express_Service__c == false) {
            BusinessHours BH = [SELECT Id, FridayStartTime, MondayEndTime, MondayStartTime, SaturdayEndTime, SaturdayStartTime, SundayEndTime, SundayStartTime, ThursdayEndTime, ThursdayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime FROM BusinessHours where name = 'GS- Medical Appointment Normal'
                limit 1
            ];

            Datetime nextStartDate_time = BusinessHours.nextStartDate(BH.Id, createdDate);

            system.debug(nextStartDate_time + 'nextStartDate_time');

            Date nxtBusinessDate = Date.newinstance(nextStartDate_time.year(), nextStartDate_time.month(), nextStartDate_time.day());
            // order by createddate DESC limit 1
            List < Appointment_Schedule_Counter__c > ListAppointmentCounter = [select id, Appointment_Time__c, Lunch_Time__c, Lunch_Duration__c, Application_Type__c from Appointment_Schedule_Counter__c where DAY_ONLY(Appointment_Time__c) =: nxtBusinessDate and Application_Type__c = 'Normal'];

            if (ListAppointmentCounter.size() < = 0) {
                getStartTimeofDay(BH, nxtBusinessDate);

                // List < Appointment__c > appointment = [select id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c from Appointment__c where Appointment_StartDate_Time__c =: Appntment.Appointment_StartDate_Time__c and Service_Type__c = 'Normal'];
                List < Appointment__c > appointment = [select id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c from Appointment__c where Appointment_StartDate_Time__c =: Appntment.Appointment_StartDate_Time__c];

                system.debug('appointment ' + appointment);
                if (appointment.size() > 0) {
                    Appntment.Appointment_StartDate_Time__c = appointment[0].Appointment_EndDate_Time__c;
                    Appntment.Appointment_EndDate_Time__c = appointment[0].Appointment_EndDate_Time__c.addMinutes(mins);
                }
            }

            if (ListAppointmentCounter.size() > 0) {
                appointmentBookingMethod(BH, ListAppointmentCounter, false);
            }

            Appntment.Step__c = step.id;
            Appntment.Service_Request__c = SR[0].id;
            Appntment.Subject__c = SR[0].name + '-' + SR[0].first_Name__c + '' + SR[0].Last_Name__c + '-' + 'Normal';
            List < step__c > stp = [select id, Step_Code__c, Biometrics_Required__c from step__c where step_code__c = 'Emirates ID Registration is completed'
                and sr__r.id =: SR[0].id limit 1
            ];

            if (stp.size() > 0) Appntment.Bio_Metrics__c = stp[0].Biometrics_Required__c;
    
            Database.SaveResult saveResultList;

            if (Appntment.Appointment_StartDate_Time__c != null) {
                saveResultList = Database.insert(Appntment, false);
            }
    
            if (saveResultList.isSuccess()) {
                updateScheduleCounter(Appntment, isnextday, ListAppointmentCounter);
            }

        } else if (SR.size() > 0 && SR[0].Express_Service__c == true) {

            BusinessHours BH = [SELECT Id, FridayStartTime, MondayEndTime, MondayStartTime, SaturdayEndTime, SaturdayStartTime, SundayEndTime, SundayStartTime, ThursdayEndTime, ThursdayStartTime, TuesdayEndTime, TuesdayStartTime, WednesdayStartTime, WednesdayEndTime FROM BusinessHours where name = 'GS- Medical Appointment Express'
                limit 1
            ];

            Datetime nextStartDate_time = BusinessHours.nextStartDate(BH.Id,ExpressDate);
            system.debug(nextStartDate_time + 'nextStartDate_time');

            Date nxtBusinessDate = Date.newinstance(nextStartDate_time.year(), nextStartDate_time.month(), nextStartDate_time.day());
            List < Appointment_Schedule_Counter__c > ListAppointmentCounter = new List < Appointment_Schedule_Counter__c > ();

            datetime dt = step.createdDate.addMinutes(ExpressMins);
            system.debug('step.createdDate : ' + step.createdDate);
            system.debug('dt : ' + dt);
            
            Integer Dateminutes = dt.minute();
            Integer DateminutesToAdd = 0;
        
            if( ( Dateminutes > 0 && Dateminutes < 5 ) || ( Dateminutes > 10 && Dateminutes < 15 ) || ( Dateminutes > 20 && Dateminutes < 25 ) || 
                ( Dateminutes > 30 && Dateminutes < 35 ) || ( Dateminutes > 40 && Dateminutes < 45 ) || ( Dateminutes > 50 && Dateminutes < 55 ) ){
                
                Dateminutes = math.mod( Dateminutes,5 );
                DateminutesToAdd = 5 - Dateminutes;
        
            }else if( ( Dateminutes > 5 && Dateminutes < 10 ) || ( Dateminutes > 15 && Dateminutes < 20 ) || ( Dateminutes > 25 && Dateminutes < 30 ) || 
                        ( Dateminutes > 35 && Dateminutes < 40 ) || ( Dateminutes > 45 && Dateminutes < 50 ) || ( Dateminutes > 55 && Dateminutes < 60 ) ){
                Dateminutes = math.mod( Dateminutes,10 );
                DateminutesToAdd = 10 - Dateminutes;
            }
            dt = dt.addMinutes( DateminutesToAdd );

            string lunchHoursNormal = system.label.Normal_Lunch_time;
            string lunchHoursExpress = system.label.Express_Lunch_Hours;
            Integer LunchMinsExpress = Integer.valueof(system.label.Express_Lunch_time);
            Integer LunchMinsExpress1 = Integer.valueof(system.label.Express_lunch_Time1);

            Boolean isWithin= BusinessHours.isWithin(bh.id, dt);

            if (isWithin) {

                DateTime dt1;
                string sHours = string.valueOf(dt.hour());

                integer mins1 = dt.addMinutes(ExpressMins).minute();
                if (ExpressLunchHour == sHours) {
                    if (mins1 > LunchMinsExpress) {
                        dt1 = dt.addMinutes(ExpressLunchDuration);
                    } 
                } else if (lunchHoursNormal == sHours) {
                    if (mins1 < LunchMinsExpress1) {
                        integer minutes1 = LunchMinsExpress1 - mins1;
                        dt1 = dt.addMinutes(minutes1);
                    }
                } else {
                    // integer reminder = (mins1-(mins1/min5)*min5);
                    // integer roundOff = reminder > 0 ? reminder : min5 ;
                    dt1 = dt;
                }
                
                if( dt1 == null ){
                    dt1 = dt;
                }

                //  List<Appointment__c> ListAppointment =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Appointment_EndDate_Time__c,Appointment_StartTime_Mins__c,Appointment_Hours1__c, Step__c from Appointment__c where Appointment_StartDate_Time__c = : dt1 and Service_Type__c ='Express'];
                List < Appointment__c > ListAppointment = [select id, Applicantion_Type__c, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c, Appointment_StartTime_Mins__c, Appointment_Hours1__c, Step__c from Appointment__c where Appointment_StartDate_Time__c =: dt1];
                if (ListAppointment.size() > 0) {
                    Appntment.Appointment_StartDate_Time__c = ListAppointment[0].Appointment_EndDate_Time__c;
                    Appntment.Appointment_EndDate_Time__c = ListAppointment[0].Appointment_EndDate_Time__c.addMinutes(Mins);
                } else {
                    Appntment.Appointment_StartDate_Time__c = dt1;
                    Appntment.Appointment_EndDate_Time__c = dt1.addMinutes(Mins);
                }
            } else {
                Date nxtBusinessDateExpress = ExpressDate.addDays(1);
                Datetime nextStartDate_time1 = BusinessHours.nextStartDate(BH.Id, nxtBusinessDateExpress);
                Date nxtBusinessDate1 = Date.newinstance(nextStartDate_time1.year(), nextStartDate_time1.month(), nextStartDate_time1.day());
                //datetime nxtBusinessDatetime =  datetime.newInstance();
                //Boolean isWithin= BusinessHours.isWithin(bh.id, dt);

                ListAppointmentCounter = [select id, Appointment_Time__c, Lunch_Time__c, Lunch_Duration__c, Application_Type__c from Appointment_Schedule_Counter__c where DAY_ONLY(Appointment_Time__c) =: nxtBusinessDate1 and Application_Type__c = 'Express'];

                if (ListAppointmentCounter.size() < = 0) {
                    getStartTimeofDay(BH, nxtBusinessDate1);
                    // List < Appointment__c > appointment = [select id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c from Appointment__c where Appointment_StartDate_Time__c =: Appntment.Appointment_StartDate_Time__c and Service_Type__c = 'Express'];
                    List < Appointment__c > appointment = [select id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c from Appointment__c where Appointment_StartDate_Time__c =: Appntment.Appointment_StartDate_Time__c];

                    system.debug('appointment ' + appointment);
                    if (appointment.size() > 0) {

                        Appntment.Appointment_StartDate_Time__c = appointment[0].Appointment_EndDate_Time__c;
                        Appntment.Appointment_EndDate_Time__c = appointment[0].Appointment_EndDate_Time__c.addMinutes(mins);
                    }
                } else if (ListAppointmentCounter.size() > 0) {

                    appointmentBookingMethod(BH, ListAppointmentCounter, true);
                }
            }

            Appntment.Step__c = step.id;
            Appntment.Service_Request__c = SR[0].id;
            Appntment.Subject__c = SR[0].name + '-' + SR[0].first_Name__c + '' + SR[0].Last_Name__c + '-' + 'Express';
            List < step__c > stp = [select id, Step_Code__c, Biometrics_Required__c from step__c where step_code__c = 'Emirates ID Registration is completed'
                and sr__r.id =: SR[0].id limit 1
            ];
            if (stp.size() > 0) Appntment.Bio_Metrics__c = stp[0].Biometrics_Required__c;
            Database.SaveResult saveResultList;
            if (Appntment.Appointment_StartDate_Time__c != null)
                saveResultList = Database.insert(Appntment, false);

            if (saveResultList.isSuccess()) {
                updateScheduleCounterExpress(Appntment, isnextday, ListAppointmentCounter);
            }
        }
        }
    }

    public static void updateScheduleCounter(Appointment__c Appntment, boolean isnextday, List < Appointment_Schedule_Counter__c > ListAppointmentCounter) {

        Appointment_Schedule_Counter__c appSchedule = new Appointment_Schedule_Counter__c();

        if (ListAppointmentCounter.size() > 0 && isnextday == false) {
            system.debug('if');
            appSchedule.id = ListAppointmentCounter[0].id;
            appSchedule.Appointment_Time__c = Appntment.Appointment_EndDate_Time__c;

        } else if (ListAppointmentCounter.size() <= 0 && isnextday == false) {
            system.debug('else if1');
            appSchedule.Appointment_Time__c = Appntment.Appointment_EndDate_Time__c;
            appSchedule.Application_Type__c = 'Normal';
            appSchedule.Lunch_Duration__c = LunchDuration;
            Date LunchDate = date.newInstance(Appntment.Appointment_StartDate_Time__c.year(), Appntment.Appointment_StartDate_Time__c.month(), Appntment.Appointment_StartDate_Time__c.Day());
            DateTime LunchTime = dateTime.newInstance(LunchDate, Time.newInstance(Integer.valueOf(string.valueof(Lunch_Time).split(':')[0]),
                Integer.valueof(string.valueof(Lunch_Time).split(':')[1]), 0, 0));
            appSchedule.Lunch_Time__c = LunchTime;

        } else if (isnextday == true) {
            Date nextAppDate = date.newInstance(Appntment.Appointment_StartDate_Time__c.year(), Appntment.Appointment_StartDate_Time__c.month(), Appntment.Appointment_StartDate_Time__c.Day());

            List < Appointment_Schedule_Counter__c > ListAppointmentCounter1 = [select id, Appointment_Time__c, Lunch_Time__c, Lunch_Duration__c, Application_Type__c from Appointment_Schedule_Counter__c where DAY_ONLY(Appointment_Time__c) =: nextAppDate and Application_Type__c = 'Normal'];
            if (ListAppointmentCounter1.size() <= 0) {
                system.debug('else if 2 if');
                appSchedule.Appointment_Time__c = Appntment.Appointment_EndDate_Time__c;
                appSchedule.Lunch_Duration__c = LunchDuration;
                appSchedule.Application_Type__c = 'Normal';
                Date LunchDate = date.newInstance(Appntment.Appointment_StartDate_Time__c.year(), Appntment.Appointment_StartDate_Time__c.month(), Appntment.Appointment_StartDate_Time__c.Day());
                DateTime LunchTime = dateTime.newInstance(LunchDate, Time.newInstance(Integer.valueOf(string.valueof(Lunch_Time).split(':')[0]),
                    Integer.valueof(string.valueof(Lunch_Time).split(':')[1]), 0, 0));
                appSchedule.Lunch_Time__c = LunchTime;

            } else if (ListAppointmentCounter1.size() > 0) {
                system.debug('else if 3 else if');
                appSchedule.id = ListAppointmentCounter1[0].id;
                appSchedule.Appointment_Time__c = Appntment.Appointment_EndDate_Time__c;
            }
        }
        upsert appSchedule;
    }

    public static void updateScheduleCounterExpress(Appointment__c Appntment, boolean isnextday, List < Appointment_Schedule_Counter__c > ListAppointmentCounter) {

        Appointment_Schedule_Counter__c appSchedule = new Appointment_Schedule_Counter__c();

        if (ListAppointmentCounter.size() > 0 && isnextday == false) {
            system.debug('if');
            appSchedule.id = ListAppointmentCounter[0].id;
            appSchedule.Appointment_Time__c = Appntment.Appointment_EndDate_Time__c;

        } else if (ListAppointmentCounter.size() <= 0 && isnextday == false) {
            system.debug('else if1');
            appSchedule.Appointment_Time__c = Appntment.Appointment_EndDate_Time__c;
            appSchedule.Application_Type__c = 'Express';
            appSchedule.Lunch_Duration__c = ExpressLunchDuration;
            Date LunchDate = date.newInstance(Appntment.Appointment_StartDate_Time__c.year(), Appntment.Appointment_StartDate_Time__c.month(), Appntment.Appointment_StartDate_Time__c.Day());
            DateTime LunchTime = dateTime.newInstance(LunchDate, Time.newInstance(Integer.valueOf(string.valueof(Lunch_Time).split(':')[0]),
                Integer.valueof(string.valueof(Lunch_Time_Express).split(':')[1]), 0, 0));
            appSchedule.Lunch_Time__c = LunchTime;
        }
        upsert appSchedule;
    }



    public static void appointmentBookingMethod(BusinessHours BH, List < Appointment_Schedule_Counter__c > ListAppointmentCounter, boolean isExpressService) {
        dateTime NextAppTime = ListAppointmentCounter[0].Appointment_Time__c.addMinutes(Mins);
        dateTime CuurrentAppTime = ListAppointmentCounter[0].Appointment_Time__c;

        boolean findnextavilabletime = false;
        //Mudasir added the or condition as the logic was adjusted on the both fields - V2.0 
        if (CuurrentAppTime == ListAppointmentCounter[0].Lunch_Time__c || NextAppTime == ListAppointmentCounter[0].Lunch_Time__c) {
            //Mudasir commented this line for the issue of 45 minutes appointment 
            //NextAppTime = NextAppTime.addMinutes(ListAppointmentCounter[0].Lunch_Duration__c.intValue());
            CuurrentAppTime = NextAppTime.addMinutes(ListAppointmentCounter[0].Lunch_Duration__c.intValue());
            NextAppTime = CuurrentAppTime.addMinutes(Mins);
        }
        List < Appointment__c > ListAppointment = [select id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c, Appointment_StartTime_Mins__c, Appointment_Hours1__c, Step__c from Appointment__c where Appointment_EndDate_Time__c =: NextAppTime];

        if (ListAppointment.size() > 0) {
            CuurrentAppTime = NextAppTime;
            NextAppTime = CuurrentAppTime.addMinutes(Mins);
            List < Appointment__c > ListAppointment1 = [select id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c, Appointment_StartTime_Mins__c, Appointment_Hours1__c, Step__c from Appointment__c where Appointment_EndDate_Time__c =: NextAppTime];

            if (ListAppointment1.size() > 0) {
                CuurrentAppTime = NextAppTime;
                NextAppTime = CuurrentAppTime.addMinutes(Mins);
            }
        } else if (ListAppointment.size() < 0) {
            CuurrentAppTime = CuurrentAppTime;
            NextAppTime = NextAppTime;
        }

        Boolean isWithin = BusinessHours.isWithin(bh.id, NextAppTime);
        if (isWithin) {
            // if (isExpressService == true) {
            //     Appntment.Appointment_StartDate_Time__c = CuurrentAppTime.addMinutes(2);
            //     Appntment.Appointment_EndDate_Time__c = NextAppTime.addMinutes(2);
            // } else {
            Appntment.Appointment_StartDate_Time__c = CuurrentAppTime;
            Appntment.Appointment_EndDate_Time__c = NextAppTime;
            // }
        } else {
            Date NxtBusinessDay = date.newInstance(NextAppTime.year(), NextAppTime.month(), NextAppTime.Day()).addDays(1);
            appointmentBookingNxtMethod(bh, NxtBusinessDay, ListAppointmentCounter[0].Application_Type__c, isExpressService);
            isnextday = true;
        }

    }

    public static void appointmentBookingNxtMethod(BusinessHours BH, Date NxtBusinessDay, string AppointmentType, boolean isExpressService) {

        List < Appointment_Schedule_Counter__c > ListAppointmentCounter = [select id, Appointment_Time__c, Lunch_Time__c, Lunch_Duration__c, Application_Type__c from Appointment_Schedule_Counter__c where DAY_ONLY(Appointment_Time__c) =: NxtBusinessDay and Application_Type__c =: AppointmentType];

        if (ListAppointmentCounter.size() < = 0) {
            getStartTimeofDay(BH, NxtBusinessDay);
        } else {
            DateTime NextAppTime = ListAppointmentCounter[0].Appointment_Time__c.addMinutes(Mins);
            dateTime CuurrentAppTime = ListAppointmentCounter[0].Appointment_Time__c;
            boolean findnextavilabletime = false;
            //Mudasir added the or condition as the logic was adjusted on the both fields - V2.0 
            if (CuurrentAppTime == ListAppointmentCounter[0].Lunch_Time__c || NextAppTime == ListAppointmentCounter[0].Lunch_Time__c) {
                //Mudasir commented this line for the issue of 45 minutes appointment 
                //NextAppTime = NextAppTime.addMinutes(ListAppointmentCounter[0].Lunch_Duration__c.intValue());
                // CuurrentAppTime = isExpressService ? NextAppTime.addMinutes(ListAppointmentCounter[0].Lunch_Duration__c.intValue() + 2) : NextAppTime.addMinutes(ListAppointmentCounter[0].Lunch_Duration__c.intValue());
                // NextAppTime = isExpressService ? CuurrentAppTime.addMinutes(Mins + 2) : CuurrentAppTime.addMinutes(Mins);

                CuurrentAppTime = NextAppTime.addMinutes(ListAppointmentCounter[0].Lunch_Duration__c.intValue());
                NextAppTime = CuurrentAppTime.addMinutes(Mins);
            }
            List < Appointment__c > ListAppointment = [select id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c, Appointment_StartTime_Mins__c, Appointment_Hours1__c, Step__c from Appointment__c where Appointment_EndDate_Time__c =: NextAppTime];

            if (ListAppointment.size() > 0) {
                CuurrentAppTime = NextAppTime;
                NextAppTime = CuurrentAppTime.addMinutes(Mins);
            } else if (ListAppointment.size() <= 0) {
                // CuurrentAppTime = isExpressService ? CuurrentAppTime.addMinutes(2) : CuurrentAppTime;
                // NextAppTime = isExpressService ? NextAppTime.addMinutes(2) : NextAppTime;
                CuurrentAppTime = CuurrentAppTime;
                NextAppTime = NextAppTime;
            }

            Boolean isWithin = BusinessHours.isWithin(bh.id, NextAppTime);
            if (isWithin) {
                Appntment.Appointment_StartDate_Time__c = CuurrentAppTime;
                Appntment.Appointment_EndDate_Time__c = NextAppTime;
            } else {
                Date NxtBusinessDay1 = date.newInstance(NextAppTime.year(), NextAppTime.month(), NextAppTime.Day()).addDays(1);
                appointmentBookingNxtMethod(bh, NxtBusinessDay1, AppointmentType, isExpressService);
                isnextday = true;
            }
        }
    }

    public static void getStartTimeofDay(BusinessHours BH, Date CreatedDate) {

        dateTime CDt = datetime.newInstance(createdDate.year(), createdDate.month(), createdDate.day());
        string Dayofweek = CDt.format('EEEE');
        DateTime startDate_time;
        DateTime EndDate_time;

        if (dayofWeek == 'SUNDAY' || Test.isRunningTest()) {
            startDate_time = datetime.newInstance(CreatedDate, Time.newInstance(Integer.valueOf(string.valueof(BH.SundayStartTime).split(':')[0]),
                Integer.valueof(string.valueof(BH.SundayStartTime).split(':')[1]), 0, 0));
            EndDate_time = startDate_time.addMinutes(Mins);
        }

        if (dayofWeek == 'MONDAY' || Test.isRunningTest()) {
            startDate_time = datetime.newInstance(CreatedDate, Time.newInstance(Integer.valueOf(string.valueof(BH.MondayStartTime).split(':')[0]),
                Integer.valueof(string.valueof(BH.MondayStartTime).split(':')[1]), 0, 0));
            EndDate_time = startDate_time.addMinutes(Mins);
        }

        if (dayofWeek == 'TUESDAY' || Test.isRunningTest()) {
            startDate_time = datetime.newInstance(CreatedDate, Time.newInstance(Integer.valueOf(string.valueof(BH.TuesdayStartTime).split(':')[0]),
                Integer.valueof(string.valueof(BH.TuesdayStartTime).split(':')[1]), 0, 0));
            EndDate_time = startDate_time.addMinutes(Mins);
        }

        if (dayofWeek == 'Wednesday' || Test.isRunningTest()) {
            startDate_time = datetime.newInstance(CreatedDate, Time.newInstance(Integer.valueOf(string.valueof(BH.WednesdayStartTime).split(':')[0]),
                Integer.valueof(string.valueof(BH.WednesdayStartTime).split(':')[1]), 0, 0));
            EndDate_time = startDate_time.addMinutes(Mins);
        }

        if (dayofWeek == 'Thursday' || Test.isRunningTest()) {
            startDate_time = datetime.newInstance(CreatedDate, Time.newInstance(Integer.valueOf(string.valueof(BH.ThursdayStartTime).split(':')[0]),
                Integer.valueof(string.valueof(BH.ThursdayStartTime).split(':')[1]), 0, 0));
            EndDate_time = startDate_time.addMinutes(Mins);
        }
        if (dayofWeek == 'Friday' || dayofWeek == 'Saturday' || Test.isRunningTest()) {
            startDate_time = datetime.newInstance(CreatedDate, Time.newInstance(Integer.valueOf(string.valueof(BH.SundayStartTime).split(':')[0]),
                Integer.valueof(string.valueof(BH.SundayStartTime).split(':')[1]), 0, 0));
            EndDate_time = startDate_time.addMinutes(Mins);
        }

        Datetime nextStartDate_time = BusinessHours.nextStartDate(BH.Id, startDate_time);
        Datetime nextStartEnd_time = BusinessHours.nextStartDate(BH.Id, EndDate_time);

        Appntment.Appointment_StartDate_Time__c = nextStartDate_time;
        Appntment.Appointment_EndDate_Time__c = nextStartEnd_time;
    }
}