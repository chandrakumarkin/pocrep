public without sharing class GSMasterDataUpdate{ 
   list<Log__c> lstLogs = new list<Log__c>();
   Private static string  decryptedSAPPassWrd = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456' ;
   @InvocableMethod(Label='MasterDataUpdateAfterPost' description='MasterDataUpdateAfterPost')     
   
   
    public static void MasterDateUpdateMethod(List<id> ids){    
         
         
         MasterDateUpdateMethodFuture(ids);
         
    }
    
    @future(callout = true)
         
    public static void MasterDateUpdateMethodFuture(List<id> ids){    
         
          list<SAPGSWebServices.ZSF_S_GS_SERV> items = new list<SAPGSWebServices.ZSF_S_GS_SERV>();
          SAPGSWebServices.ZSF_S_GS_SERV item;
          if(ids.size()>0){
       
         for(SR_Price_Item__c objSRItem : [select Id,Pricing_Line__r.TAX_CODE__c,VAT_Amount__c,Tax_Rate__c,Price__c,Transaction_Date__c,Company_Code__c,ServiceRequest__c,ServiceRequest__r.Record_Type_Name__c,ServiceRequest__r.Customer__c,ServiceRequest__r.Customer__r.BP_No__c,ServiceRequest__r.Parent_SR__r.name,
                            Pricing_Line__c,Pricing_Line__r.Material_Code__c,ServiceRequest__r.Sponsor__c,ServiceRequest__r.Sponsor__r.BP_No__c,ServiceRequest__r.Nationality_list__c,
                            ServiceRequest__r.Domestic_Helper__c,ServiceRequest__r.Relation__c,ServiceRequest__r.SR_Template__r.Company_related__c,
                            ServiceRequest__r.Type_of_Visa_Amendment__c,ServiceRequest__r.Passport_Number__c,ServiceRequest__r.Passport_Type__c,ServiceRequest__r.Date_of_Birth__c,
                            ServiceRequest__r.Passport_Date_of_Issue__c,ServiceRequest__r.Passport_Date_of_Expiry__c,ServiceRequest__r.Passport_Country_of_Issue__c,
                            ServiceRequest__r.Title__c,ServiceRequest__r.First_Name__c,ServiceRequest__r.Last_Name__c,ServiceRequest__r.Middle_Name__c,ServiceRequest__r.Occupation_GS__r.Name,
                            Pricing_Line__r.Product__r.Name,Pricing_Line__r.Product__r.Is_Guarantee_Product__c,Pricing_Line__r.Additional_Item__c,ServiceRequest__r.Gender__c,
                            ServiceRequest__r.Name,ServiceRequest__r.Submitted_DateTime__c,ServiceRequest__r.Submitted_Date__c,Pricing_Line__r.Is_Master_Data_Change__c, ServiceRequest__r.Update_Contact__c,ServiceRequest__r.Contact__r.Date_of_Issue__c,ServiceRequest__r.Contact__r.Passport_Expiry_Date__c,ServiceRequest__r.Contact__r.Issued_Country__c,ServiceRequest__r.Quantity__c from SR_Price_Item__c where ServiceRequest__c In: ids AND Status__c = 'Invoiced' AND Pricing_Line__r.Product__r.Name != 'PSA' AND Pricing_Line__r.Material_Code__c != null AND Price__c != null AND Price__c != 0 and Pricing_Line__r.Additional_Item__c != true]){
                            
                            item = new SAPGSWebServices.ZSF_S_GS_SERV(); 
                            item.GSCHG = 'X';           
                            item.WSTYP = 'MAST';
                            //item.BUKRS = '5000';
                            item.BUKRS = System.Label.CompanyCode5000;
                            item.RENUM = objSRItem.ServiceRequest__r.Name;                          
                            item.SGUID = string.valueOf(objSRItem.id);
                            item.MATNR = objSRItem.Pricing_Line__r.Material_Code__c;
                               
                            item.IDNUMBER = objSRItem.ServiceRequest__r.Passport_Number__c;
                            item.IDTYPE = 'FS0002';
                            Date dIssue = objSRItem.ServiceRequest__r.Passport_Date_of_Issue__c != null ? objSRItem.ServiceRequest__r.Passport_Date_of_Issue__c : objSRItem.ServiceRequest__r.Contact__r.Date_of_Issue__c;
                            Date dExpiry = objSRItem.ServiceRequest__r.Passport_Date_of_Expiry__c != null ? objSRItem.ServiceRequest__r.Passport_Date_of_Expiry__c : objSRItem.ServiceRequest__r.Contact__r.Passport_Expiry_Date__c;
                            String sCountry = objSRItem.ServiceRequest__r.Passport_Country_of_Issue__c != null ? objSRItem.ServiceRequest__r.Passport_Country_of_Issue__c : objSRItem.ServiceRequest__r.Contact__r.Issued_Country__c;
                            item.ID_DATE_FROM = (dIssue != null ? GsPendingInfo.SAPDateFormat(dIssue) : '');
                            item.ID_DATE_TO = (dExpiry != null ? GsPendingInfo.SAPDateFormat(dExpiry) : '');
                            item.IDCOUNTRY = Country_Codes__c.getAll().get(sCountry) != null ? Country_Codes__c.getAll().get(sCountry).Code__c:'';             
                            item.NATIO = objSRItem.ServiceRequest__r.Nationality_list__c!=''? Country_Codes__c.getAll().get(objSRItem.ServiceRequest__r.Nationality_list__c).Code__c:'';
                            item.NAME_FIRST = objSRItem.ServiceRequest__r.First_Name__c;
                            item.NAME_LAST = objSRItem.ServiceRequest__r.Last_Name__c;
                            item.NAMEMIDDLE = objSRItem.ServiceRequest__r.Middle_Name__c;     
                            item.DESIG = objSRItem.ServiceRequest__r.Occupation_GS__r.Name; 
                            if(objSRItem.ServiceRequest__r.Title__c == 'Mr.')
                            item.TITLE = '0002';
                            else if(objSRItem.ServiceRequest__r.Title__c == 'Ms.')
                            item.TITLE = '0001';
                            else if(objSRItem.ServiceRequest__r.Title__c == 'Mrs.')
                            item.TITLE = '0005';
                            else if(objSRItem.ServiceRequest__r.Title__c == 'Mr. and Mrs.')
                            item.TITLE = '0004';                            
                            item.BIRTHDATE= GsPendingInfo.SAPDateFormat(objSRItem.ServiceRequest__r.Date_of_Birth__c);
                            if(objSRItem.ServiceRequest__r.Gender__c == 'Male')                              
                                item.SEX = '2';                            
                            else if (objSRItem.ServiceRequest__r.Gender__c == 'Female')
                                item.SEX = '1';
                              
                            items.add(item);       
                            
                            
                            }
           
        SAPGSWebServices.ZSF_TT_GS_SERV objGSData = new SAPGSWebServices.ZSF_TT_GS_SERV();
        objGSData.item = items;
            
        SAPGSWebServices.serv_actv objSFData = new SAPGSWebServices.serv_actv();        
        objSFData.timeout_x = 120000;
        objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
        objSFData.inputHttpHeaders_x= new Map<String, String>();
      //Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c); Commented for V1.7
        Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+GSMasterDataUpdate.decryptedSAPPassWrd); // Added to V1.7
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element response;
        system.debug('request is $$$ : '+items);
        
        if(items != null && !items.isEmpty())
            response = objSFData.Z_SF_GS_ACTIVITY(null, objGSData);
         system.debug('response $$$ : '+response);
       
          }   
      }
    }