@isTest
private class Test_CustAttacforEmpnonSponsored_QC {

   static testMethod void myUnitTest() {
   
     Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Last RB';
        objContact.Email = 'test@difcportal.com';
        objContact.AccountId = objAccount.Id;
        objContact.FirstName = 'Nagaboina';
        objContact.RecordTypeId = [SELECT ID FROM RecordType WHERE DeveloperName = 'GS_Contact' AND SobjectType = 'Contact'].ID;
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Employee Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
            
        
        String objRectype;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Equity_Holder_Card') AND SobjectType='Service_Request__c']){
            objRectype = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
        objSR.SR_Group__c = 'GS';
        objSR.contact__c = objContact.id;
        
        insert objSR; 
        
        Step_Template__c stepTemplate = new Step_Template__c();
        stepTemplate.name = 'Front Desk Review';
        stepTemplate.Code__c = 'Front Desk Review';
        stepTemplate.Step_RecordType_API_Name__c = 'General';
        insert stepTemplate;
        
        Status__c stepStatus = new Status__c();
        stepStatus.name = 'Awaiting Review';
        stepStatus.Code__c = 'Awaiting_Review';
        insert stepStatus;
        
        Status__c stepStatus1 = new Status__c();
        stepStatus1.name = 'Verified';
        stepStatus1.Code__c = 'Verified';
        insert stepStatus1;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Step_Template__c = stepTemplate.id;
        objStep.Status__c = stepStatus.id;
        objStep.first_Name_Arabic__c ='test';
        objStep.Last_Name_Arabic__c ='test';
        
        objStep.Government_Services__c = true;
        insert objStep;
        
        
        
         Step_Template__c  stpTemp = new Step_Template__c ();
    stpTemp.Name ='Entry Permit With Airport Entry Stamp Upload';
    stpTemp.CODE__C  ='Entry Permit With Airport Entry Stamp Upload';
    stpTemp.Step_RecordType_API_Name__c ='General';
    insert stpTemp;
      
        Step__c stp = new Step__c();
        stp.SR__c = objSR.Id;
        stp.Step_Notes__c = 'Sample step Note';
        stp.step_template__c = stpTemp.Id;
        insert stp;
      
        stp.Step_Notes__c = 'Durga Added new comments';
        update stp;
        
        
         SR_Doc__c objSRDoc = new SR_Doc__c();
         objSRDoc.Name = 'Coloured Photo';
         objSRDoc.Service_Request__c = objSR.Id;  
         objSRDoc.status__c ='Uploaded';    
        insert objSRDoc;
      
      
      Attachment objAttachment = new Attachment();
      objAttachment.Name = 'New User Access Form.pdf';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objSRDoc.Id;
      
      
        insert objAttachment;
        Test.startTest();
        
         CustAttacforEmpnonSponsored_QC Qc = new CustAttacforEmpnonSponsored_QC (objSR.id);
         System.enqueueJob(Qc);
         
          Test.stopTest();
        
        
    }
   
   
}