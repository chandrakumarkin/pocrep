@istest
private class TestAccountOverridecls{
    
    static testMethod void MyUnitTest1(){
        PortalBalance_Update_Profiles__c pu = new PortalBalance_Update_Profiles__c(name='System Administrator');
        insert pu;
        
         Account objAccountSC = new Account();
        objAccountSC.Name = 'Test Custoer SC';
        objAccountSC.E_mail__c = 'test@test.com';
        objAccountSC.BP_No__c = '001235';
        objAccountSC.ROC_Status__c = 'Under Formation';
        insert objAccountSC;
        
        apexPages.StandardController sc = new apexPages.StandardController(objAccountSC);
        AccountOverridecls ao = new AccountOverridecls(sc);
        ao.calculatePortalBalance();
    
    }

}