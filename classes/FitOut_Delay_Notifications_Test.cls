/**
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class FitOut_Delay_Notifications_Test {

    

    static testMethod void myUnitTest() {
    	 EmailTemplate  emailTemp = new EmailTemplate ();
	      emailTemp.developerName  ='Test';
	      emailTemp.TemplateType ='Text';
	      emailTemp.name ='test';
	      emailTemp.FolderId ='00l20000001DSMmAAO';
	      //insert emailTemp;
        Test.startTest();
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Fit_Out_Units','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Status__c objStatusApproved = new Status__c();
        objStatusApproved.Name = 'Approved';
        objStatusApproved.Code__c = 'APPROVED';
        insert objStatusApproved;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
           
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'Submitted';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);  
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'Approved';
		lstSRStatus.add(objSRStatus);
        
		objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Fit to Occupy Certificate Issued';
        objSRStatus.Code__c = 'Fit_to_Occupy_Certificate_Issued';        
        
        lstSRStatus.add(objSRStatus);
             
        insert lstSRStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit - Out Service Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        
        insert testSrTemplate;
        
        SR_Steps__c srstep = new SR_Steps__c();
        insert srstep;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        list<Building__c> lstBuilding = new list<Building__c>();
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP On Plan';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '2300';
        objBuilding.SAP_Building_No__c = '123456';
        lstBuilding.add(objBuilding);
        insert lstBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c =  lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'office';
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        
         list<Lease__c> lstLeases = new list<Lease__c>();
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.RF_Valid_To__c = system.today().addDays(10);
        objLease.RF_Valid_From__c = system.today();
        objLease.Lease_Types__c = 'Retail Lease';
        insert objLease;
        
        List<Lease_Partner__c> ListLP=new List<Lease_Partner__c>();
        
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = objLease.Id;
        objLP.Account__c = objAccount.Id;
        objLP.Unit__c = lstUnits[0].Id;
        objLP.status__c = 'Draft';
        objLP.Type_of_Lease__c = 'Leased';
        objLP.Is_License_to_Occupy__c = true;
        objLP.Is_6_Series__c = false;
        ListLP.add(objLP);
        
        Lease_Partner__c objLP1 = new Lease_Partner__c();
        objLP1.Lease__c = objLease.Id;
        objLP1.Account__c = objAccount.Id;
        objLP1.Unit__c = lstUnits[1].Id;
        objLP1.status__c = 'Draft';
        objLP1.Type_of_Lease__c = 'Leased';
        objLP1.Is_License_to_Occupy__c = true;
        objLP1.Is_6_Series__c = false;
        ListLP.add(objLP1);
        /*
        Lease_Partner__c objLP2 = new Lease_Partner__c();
        objLP2.Lease__c = objLease.Id;
        objLP2.Account__c = objAccount.Id;
        objLP2.Unit__c = lstUnits[1].Id;
        objLP2.status__c = 'Draft';
        objLP2.Type_of_Lease__c = 'Leased';
        objLP2.Is_License_to_Occupy__c = true;
        objLP2.Is_6_Series__c = false;
        ListLP.add(objLP2);
        
        Lease_Partner__c objLP12 = new Lease_Partner__c();
        objLP12.Lease__c = objLease.Id;
        objLP12.Account__c = objAccount.Id;
        objLP12.Unit__c = lstUnits[0].Id;
        objLP12.status__c = 'Draft';
        objLP12.Type_of_Lease__c = 'Leased';
        objLP12.Is_License_to_Occupy__c = true;
        objLP12.Is_6_Series__c = false;
        ListLP.add(objLP12);
        */
        
        insert ListLP;
        

        Service_Request__c SR111= new Service_Request__c();
        SR111.RecordTypeId = mapRecordType.get('Request_Contractor_Access');
        SR111.Customer__c = objAccount.Id;
        SR111.external_SR_Status__c = lstSRStatus[2].Id;
        SR111.Building__c = lstBuilding[0].Id;
        //SR.Service_Category__c = 'New'; 
        SR111.email__c = 'mudasir.w@difc.ae';      
        insert SR111;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        SR1.email__c = 'mudasir.w@difc.ae';
        sr1.Linked_SR__C = SR111.id;
        
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;
        sr.External_SR_Status__c = lstSRStatus[3].id;
        sr.email__c = 'mudasir.w@difc.ae';
        //SR.SR_Group__c='Fit-Out & Events';
        SR.Linked_SR__c = SR1.id;     
        SR.Type_of_Request__c = 'Type A'; 
        //sr.Service_Type__c ='Fit - Out Service Request';
        SR.Linked_SR__C = SR1.id;
        insert SR;
           //Test.startTest();
	    	FitOutOngoingPrjDelay_Notification_Batch abs= new FitOutOngoingPrjDelay_Notification_Batch();
	    	String cronExpr = '0 0 0 15 3 ? 2022';
			String jobId = System.schedule('myJobTestJobName', cronExpr, abs);
			//EmailTemplate emailTemp = new EmailTemplate(Name='test'); insert emailTemp;
			//abs.createEmailMessage(SR,new List<String>{'mudasir@difc.ae'},new List<String>{'mudasir@difc.ae'},new List<String>{'mudasir@difc.ae'},emailTemp,21);
        test.stopTest();
    }
}