/******************************************************************************************
 *  Author   : Ravindra Babu Nagaboina
 *  Company  : NSI DMCC
 *  Date     : 03rd Oct 2016   
 *  Description : This class will be used to havea all the Custom Codes related to Mortgage .                       
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.0  07/Oct/2016      Ravi                Created
*******************************************************************************************/
public without sharing class RORPMortgageCustomCode {
	
	public static string ValidateMortgageRegistration(Step__c objStep){
		string res = 'Success';
		//Savepoint sp = Database.setSavepoint();
		try{
			if(objStep != null && objStep.SR__c != null){
				Service_Request__c objSR = objStep.SR__r;
				map<Id,set<string>> mapUnitOwners = new map<Id,set<string>>();
				//map<string,string> mapLandLordTypes = new map<string,string>();
				map<string,string> mapLandLordInfo = new map<string,string>();
				
				map<string,string> mapEnteredLandlords = new map<string,string>();
				
				//to get the Unit Landlords from System
				for(Lease_Partner__c objLP : [select Id,Unit__c,Account__c,Account__r.Registration_License_No__c,Account__r.Issuing_Authority__c,Account__r.RORP_License_No__c,Contact__c,Contact__r.Passport_No__c,Contact__r.Nationality__c,Contact__r.Birthdate from Lease_Partner__c where Type__c = 'Landlord' AND Unit__c IN (select Unit__c from Amendment__c where ServiceRequest__c =: objSR.Id AND Amendment_Type__c = 'Mortgage Registration' AND Status__c = 'Active' AND Unit__c != null)]){
					string sKey = '';
					system.debug('objLP '+objLP);
					if(objLP.Account__c != null){
						if(objLP.Account__r.Registration_License_No__c != null){
							sKey = objLP.Account__r.Registration_License_No__c.toLowerCase();
							//mapLandLordTypes.put(sKey,'Account');
							mapLandLordInfo.put(sKey,objLP.Account__c);
						}else{
							if(objLP.Account__r.Issuing_Authority__c != null && objLP.Account__r.RORP_License_No__c != null){
								sKey = objLP.Account__r.Issuing_Authority__c.toLowerCase()+'_'+objLP.Account__r.RORP_License_No__c.toLowerCase();
								//mapLandLordTypes.put(sKey,'Account');
								mapLandLordInfo.put(sKey,objLP.Account__c);
							}else{
								//Issuing Authority or License Number was not maintained for Landlords
								return 'Issuing Authority or License Number was not maintained for Landlord. Please update accordingly.';
							}
						}
					}else if(objLP.Contact__c != null){
						if(objLP.Contact__r.Nationality__c != null && objLP.Contact__r.Birthdate != null){ //objLP.Contact__r.Passport_No__c
							//sKey = objLP.Contact__r.Passport_No__c.toLowerCase()+'_'+objLP.Contact__r.Nationality__c.toLowerCase();
							sKey = objLP.Contact__r.Birthdate.format()+'_'+objLP.Contact__r.Nationality__c.toLowerCase();
							//mapLandLordTypes.put(sKey,'Contact');
							mapLandLordInfo.put(sKey,objLP.Contact__c);
						}else{
							//Passport Number or Nationality was not maintained for Landlords
							return 'Nationality or Birthdate was not maintained for Landlord. Please update accordingly.';
						}
					}else if(objLP.Account__c == null && objLP.Contact__c == null){
						
					}
					if(sKey != ''){
						set<string> setLLs = mapUnitOwners.containsKey(objLP.Unit__c) ? mapUnitOwners.get(objLP.Unit__c) : new set<string>();
						setLLs.add(sKey);
						mapUnitOwners.put(objLP.Unit__c,setLLs);
					}
				}
				
				//to get the landlord details entered by banks
				for(Amendment__c objAmd : [select Id,Name,Name__c,Record_Type_Name__c,IssuingAuthority__c,Issuing_Authority_Other__c,CL_Certificate_No__c,Amendment_Type__c,Passport_No__c,Nationality_list__c,Given_Name__c,Family_Name__c,Company_Name__c,Date_of_Birth__c from Amendment__c where Amendment__c.ServiceRequest__c =: objSR.Id AND (Amendment_Type__c = 'Individual Mortgager' or Amendment_Type__c = 'Company Mortgager') order by  Amendment_Type__c]){
                	if(objAmd.Record_Type_Name__c == 'Individual_Mortgager'){
                		if(objAmd.Date_of_Birth__c != null && objAmd.Nationality_list__c != null){ //Passport_No__c
                			string sKey = objAmd.Date_of_Birth__c.format()+'_'+objAmd.Nationality_list__c.toLowerCase();
                			mapEnteredLandlords.put(sKey,sKey);
                		}
                	}else if(objAmd.Record_Type_Name__c == 'Company_Mortgager'){
                		if(objAmd.IssuingAuthority__c == 'DIFC - Registered'){
                			string sKey = objAmd.CL_Certificate_No__c.toLowerCase();
                			mapEnteredLandlords.put(sKey,sKey);
                		}else{
                			if(objAmd.CL_Certificate_No__c != null && objAmd.Issuing_Authority_Other__c != null ){
                				string sKey = objAmd.Issuing_Authority_Other__c.toLowerCase()+'_'+objAmd.CL_Certificate_No__c.toLowerCase();
                				mapEnteredLandlords.put(sKey,sKey);
                			}
                		}
                	}
				}
				
				//checking the bank entered landlord details witht the landlords in the system.
				for(string key : mapEnteredLandlords.keySet() ){
					if(mapLandLordInfo.containsKey(key)){
						mapLandLordInfo.remove(Key);
					}
				}
				// if mapLandLordInfo have any values then user did not entered all the landlords information
				if(mapLandLordInfo.isEmpty() == false){
					res = 'Landlord details are mismatched.Please check. ';
				}
			}
		}catch(Exception ex){
			res = ex.getMessage();
			//database.rollback(sp);
		}
		return res;
	}
	
	/*public static string ValidateMortgageVariation(Step__c objStep){
		string res = 'Success';
		//Savepoint sp = Database.setSavepoint();
		try{
			if(objStep != null && objStep.SR__c != null){
				Service_Request__c objSR = objStep.SR__r;
				map<Id,set<string>> mapUnitOwners = new map<Id,set<string>>();
				
				//to get the Unit Landlords from System
				for(Lease_Partner__c objLP : [select Id,Unit__c,SAP_PARTNER__c from Lease_Partner__c where Type__c = 'Landlord' AND Unit__c != null AND SAP_PARTNER__c != null AND Unit__c IN (select Unit__c from Amendment__c where ServiceRequest__c =: objSR.Id AND Amendment_Type__c = 'Mortgage Registration' AND Status__c = 'Active' AND Unit__c != null) ]){
					set<string> lstBPs = mapUnitOwners.containsKey(objLP.Unit__c) ? mapUnitOwners.get(objLP.Unit__c) : new set<string>();
					lstBPs.add(objLP.SAP_PARTNER__c);
					mapUnitOwners.put(objLP.Unit__c,lstBPs);
				}
				
				if(!mapUnitOwners.isEmpty()){
					set<String> setLBPs = new set<String>();
					Integer i = 0;
					Boolean isError = false;
					for(Id UnitId : mapUnitOwners.keySet()){
						if(i==0){
							setLBPs = mapUnitOwners.get(UnitId);
						}else{
							for(string BPNo : mapUnitOwners.get(UnitId)){
								if(setLBPs.contains(BPNo) == false){
									isError = true;
									break;
								}
							}
							if(isError)
								break;
						}
						i++;
					}
					if(isError){
						res = 'Landlord are diffrent for selected units. Please check.';
					}
				}
				
			}
		}catch(Exception ex){
			res = ex.getMessage();
			//database.rollback(sp);
		}
		return res;
	}*/
	
	public static string UpdateAmendments(Step__c objStep){
		try{
			list<string> lstIssuingAus = new list<string>();
			list<string> lstLicneseNos = new list<string>();
			list<string> lstPassportNos = new list<string>();
			list<string> lstNations = new list<string>();
			list<string> lstROCLicenseNos = new list<string>();
			list<Date> lstDOBs = new list<Date>();
			list<Id> lstUnitIds = new list<Id>();
			
			map<string,Amendment__c> mapAmends = new map<string,Amendment__c>();
			//Amendment__c.
			for(Amendment__c objAmd : [select Id,Name,Name__c,Record_Type_Name__c,Contact__c,Shareholder_Company__c,SR_Record_Type__c,IssuingAuthority__c,Issuing_Authority_Other__c,CL_Certificate_No__c,Amendment_Type__c,Passport_No__c,Nationality_list__c,Given_Name__c,Family_Name__c,Company_Name__c,Date_of_Birth__c,Unit__c,(select Id,Price__c from SR_Price_Items__r) from Amendment__c where ServiceRequest__c =: objStep.SR__c AND (Record_Type_Name__c = 'Individual_Mortgager' OR Record_Type_Name__c = 'Company_Mortgager' OR Record_Type_Name__c = 'Mortgage_Unit') order by  Amendment_Type__c]){
		    	if(objAmd.SR_Record_Type__c == 'Mortgage_Registration' && (objAmd.Record_Type_Name__c == 'Individual_Mortgager' || objAmd.Record_Type_Name__c == 'Company_Mortgager')){
			    	string sKey = '';
			    	if(objAmd.Record_Type_Name__c == 'Individual_Mortgager'){
			    		if(objAmd.Date_of_Birth__c != null && objAmd.Nationality_list__c != null){
			    			sKey = objAmd.Date_of_Birth__c.format()+'_'+objAmd.Nationality_list__c.toLowerCase();
			    			//lstPassportNos.add(objAmd.Passport_No__c);
			    			lstNations.add(objAmd.Nationality_list__c);
			    			lstDOBs.add(objAmd.Date_of_Birth__c);
			    		}
			    	}else if(objAmd.Record_Type_Name__c == 'Company_Mortgager'){
			    		if(objAmd.IssuingAuthority__c == 'DIFC - Registered'){
			    			sKey = objAmd.CL_Certificate_No__c.toLowerCase();
			    			lstROCLicenseNos.add(objAmd.CL_Certificate_No__c);
			    		}else{
			    			if(objAmd.CL_Certificate_No__c != null && objAmd.Issuing_Authority_Other__c != null ){
			    				sKey = objAmd.Issuing_Authority_Other__c.toLowerCase()+'_'+objAmd.CL_Certificate_No__c.toLowerCase();
			    				lstIssuingAus.add(objAmd.Issuing_Authority_Other__c);
			    				lstLicneseNos.add(objAmd.CL_Certificate_No__c);
			    			}
			    		}
			    	}
			    	system.debug('sKey is : '+sKey);
			    	if(sKey != '')
			    		mapAmends.put(sKey,objAmd);
		    	}else if(objAmd.Record_Type_Name__c == 'Mortgage_Unit'){
		    		if(objAmd.SR_Price_Items__r != null && objAmd.SR_Price_Items__r.isEmpty() == false ){
			    		Amendment__c objTemp = new Amendment__c(Id=objAmd.Id);
			    		objTemp.Area_of_IT_Room__c = objAmd.SR_Price_Items__r[0].Price__c;
			    		mapAmends.put(objTemp.Id,objTemp);
		    		}
		    		lstUnitIds.add(objAmd.Unit__c);
		    	}
			}
			system.debug('mapAmends is : '+mapAmends);
			if(mapAmends.isEmpty() == false){
				string sKey = '';
				if(lstDOBs.isEmpty() == false && lstNations.isEmpty() == false){
					for(Contact objCon : [select Id,Name,BP_No__c,Passport_No__c,Nationality__c,Birthdate from Contact where RecordType.DeveloperName='RORP_Contact' AND Birthdate IN : lstDOBs AND Nationality__c IN : lstNations AND Birthdate != null AND Nationality__c != null AND Id IN (select Contact__c from Lease_Partner__c where Unit__c IN : lstUnitIds AND Type__c = 'Landlord')]){
						sKey = objCon.Birthdate.format()+'_'+objCon.Nationality__c.toLowerCase();
						system.debug('sKey is : '+sKey);
						if(mapAmends.containsKey(sKey)){
							Amendment__c objTemp = mapAmends.get(sKey);
							objTemp.Contact__c = objCon.Id;
							mapAmends.put(sKey,objTemp);
						}
					}
				}
				system.debug('mapAmends is : '+mapAmends);
				//Integer i= 1/0;
				if(lstROCLicenseNos.isEmpty() == false || (lstIssuingAus.isEmpty() == false && lstLicneseNos.isEmpty() == false)){
					sKey = '';
					for(Account objAcc : [select Id,Name,RecordType.DeveloperName,Issuing_Authority__c,RORP_License_No__c,Registration_License_No__c from Account 
						where ( (RecordType.DeveloperName = 'RORP_Account' AND Issuing_Authority__c != null AND RORP_License_No__c != null AND Issuing_Authority__c IN : lstIssuingAus AND RORP_License_No__c IN : lstLicneseNos)
						OR (RecordType.DeveloperName != 'RORP_Account' AND Registration_License_No__c != null AND Registration_License_No__c IN : lstROCLicenseNos)) AND Id IN (select Account__c from Lease_Partner__c where Unit__c IN : lstUnitIds AND Type__c = 'Landlord') ]){
						
						if(objAcc.RecordType.DeveloperName != 'RORP_Account'){
							sKey = objAcc.Registration_License_No__c.toLowerCase();
						}else if(objAcc.RecordType.DeveloperName == 'RORP_Account'){
							sKey = objAcc.Issuing_Authority__c.toLowerCase()+'_'+objAcc.RORP_License_No__c.toLowerCase();
						}
						if(sKey != '' && mapAmends.containsKey(sKey)){
							Amendment__c objTemp = mapAmends.get(sKey);
							objTemp.Shareholder_Company__c = objAcc.Id;
							mapAmends.put(sKey,objTemp);
						}
					}
				}
				update mapAmends.values();
				
				//if(objStep.SR__r.Record_Type_Name__c == 'Mortgage_Registration')
					RORPMortgageCustomCode.CreateSAPAttributes(objStep.SR__c);
				//else if(objStep.SR__r.Record_Type_Name__c == 'Mortgage_Variation')
					//RORPMortgageVariation.CreateVariationSAPAttributes(objStep.SR__c);
					
				Service_Request__c objSRTemp = new Service_Request__c(Id=objStep.SR__c);
				objSRTemp.Required_Service__c = 'Push to SAP';
				objSRTemp.Confirm_Change__c = true;
				objSRTemp.Updated_from_SAP__c = system.now();
				objSRTemp.Approved_By__c = Userinfo.getUserId();
				update objSRTemp;
			}
			return 'Success';
		}catch(Exception ex){
			return ex.getMessage()+'';
		}
	}
	
	public static Boolean CreateOffPlanAmendments(String SRId){
		list<Amendment__c> lstOffPlanAmendments = new list<Amendment__c>();
		Amendment__c objOffPlanAmd;
		//Amendment__c.
		for(Amendment__c objAmd : [select Id,Name,ServiceRequest__c,Unit__c,Amendment_Type__c,Status__c,Mortgage_Degree__c,Mortgage_Type__c,Nature_of_Mortgage__c,Building2__c,Unit_Rent__c,(select Id from Amendments__r where Amendment_Type__c='Off Plan Mortgage') from Amendment__c where ServiceRequest__c=:SRId AND Amendment_Type__c = 'Mortgage Registration' AND Building2__c != null AND Building2__r.Permit_Date__c = null]){
			if(objAmd.Amendments__r.size() == 0){
				objOffPlanAmd = objAmd.clone(false);
				objOffPlanAmd.Amendment_Type__c = 'Off Plan Mortgage';
				objOffPlanAmd.Related__c = objAmd.Id;
				lstOffPlanAmendments.add(objOffPlanAmd);
			}
		}
		if(lstOffPlanAmendments.isEmpty() == false){
			insert lstOffPlanAmendments;
		}
		return !lstOffPlanAmendments.isEmpty();
	}
	
	public static string CreateSAPAttributes(String SRId){
        string res = 'Success';
        //try{
            list<SAP_Attribute__c> lstAttributes = new list<SAP_Attribute__c>();
            SAP_Attribute__c objAttribute;
            
            map<string,string> mapUnitAmendments = new map<string,string>();
            map<string,string> mapBPAmendments = new map<string,string>();
	            
            for(Amendment__c objAmd : [select Id,Name,Amendment_Type__c from Amendment__c where ServiceRequest__c=:SRId AND (Amendment_Type__c='Mortgage Registration' OR Amendment_Type__c='Off Plan Mortgage' OR Amendment_Type__c='Individual Mortgager' OR Amendment_Type__c='Company Mortgager')]){
                if(objAmd.Amendment_Type__c == 'Mortgage Registration' || objAmd.Amendment_Type__c == 'Off Plan Mortgage' ){
                    mapUnitAmendments.put(objAmd.Id,objAmd.Amendment_Type__c);
                }else if(objAmd.Amendment_Type__c == 'Individual Mortgager' || objAmd.Amendment_Type__c == 'Company Mortgager' ){
                    mapBPAmendments.put(objAmd.Id,objAmd.Amendment_Type__c);
                }
            }
            
            if(mapUnitAmendments.isEmpty() == false && mapBPAmendments.isEmpty() == false){
                for(String UnitId : mapUnitAmendments.keySet()){
                    for(String BPId : mapBPAmendments.keySet()){
                        objAttribute = new SAP_Attribute__c();
                        objAttribute.BP_Amendment__c = BPId;
                        objAttribute.Unit_Amendment__c = UnitId;
                        objAttribute.Service_Request__c = SRId;
                        objAttribute.SAP_Attribute_Id__c = SRId+'_'+BPId+'_'+UnitId;
                        lstAttributes.add(objAttribute);
                    }
                }
                if(lstAttributes.isEmpty() == false){
                    upsert lstAttributes SAP_Attribute_Id__c;
                }
            }
            
            //if no error update the Amendments with Account/Contact
			//UpdateAmendments();
            
        //}catch(Exception ex){
          //  res = ex.getMessage();
         //   system.debug('Exception is in : '+ex.getLineNumber()+' Exception is : '+ex.getMessage());
      //  }
		return res;
	}
}