/**********************************************************************************************************************
    Author      :   Sravan Booragadda
    Class Name  :   AccountOverride
    Description :   This class is used to make a realtime call to SAP for checking the Balance information
    --------------------------------------------------------------------------------------------------------------------------
    
    --------------------------------------------------------------------------------------------------------------------------
**********************************************************************************************************************/    


public  class AccountOverridecls {
    
    public  Account accDetail;
    public  user userDetail;
    public AccountOverridecls(apexPages.StandardController sc){
        userDetail = [select Calculate_Portal_Balance__c,contactID,Profile.name from User where id =:UserInfo.getUserId()];
        accDetail = [select id,name,BP_No__c,Hosting_Company__c,Index_Card_Status__c,ROC_Status__c,Balance_Updated_On__c,PSA_Deposit_Claculated__c from account where id =:sc.getID()];
    }
    
    public pagereference calculatePortalBalance(){
        pagereference pg;
        set<string> accountStatus = new set<string>{'Active','Not Renewed','Under Formation','Pending Dissolution','Dissolved'}; 
        map<string,PortalBalance_Update_Profiles__c> portalBalanceProfileCheck = PortalBalance_Update_Profiles__c.getAll();  
        if(accDetail !=null && userDetail !=null && ((portalBalanceProfileCheck !=null &&  portalBalanceProfileCheck.containsKey(userDetail.profile.Name)) || (userDetail.Calculate_Portal_Balance__c && userDetail.contactID ==null)) && accDetail.BP_No__c !=null && accountStatus.contains(accDetail.ROC_Status__c)){
            try{
                portalBalanceCalloutBatch.CalculateAccountBalance(new List<Account>{accDetail});
            }catch(exception e){}
            pg = new pagereference('/'+accDetail.id+'?nooverride=1');
        }else if(userDetail.contactID !=null)
            pg = new pagereference('/'+accDetail.id+'/apex/CompanyInfo');
        else if(userDetail.contactID ==null)        
            pg = new pagereference('/'+accDetail.id+'?nooverride=1');
        return pg;  
    }
    
    
    
    
    
}