@isTest(seeAllData=false)
public class Test_New_Dependent_multiple_Visa_Request {
    
 static testmethod void newDependentMultipleVisa1() {
        test.startTest();
        Map<string,string> mapRecordTypeIds = new Map<string,string>();
        
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
       Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        insert objContact;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Consulate_or_Embassy_Letter';
        objTemplate.SR_RecordType_API_Name__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Menutext__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Required Docs to Upload';
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        Lookup__c nat1 = new Lookup__c(Type__c='Nationality',Name='Afghanistan');
        insert nat1;
        Lookup__c nat2 = new Lookup__c(Type__c='Nationality',Name='India');
        insert nat2;
        Lookup__c nat3 = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat3;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Consulate_or_Embassy_Letter' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
       // objSR.Internal_SR_Status__c = objStatus.id;
        //objSR.External_SR_Status__c = objStatus.id;
        objSR.Country__c = 'India; ';
        objSR.Country__c = objSR.Country__c + 'Afghanistan; ';
        objSR.Country__c = objSR.Country__c + 'Pakistan; ';
        objSR.contact__c = objContact1.id;
        objSR.Sponsor__c = objContact1.Id;
        
          PageReference pageRef = Page.New_Dependent_multiple_Visa_Request;
              Test.setCurrentPage(pageRef);

            pageRef.getParameters().put('RecordType', mapRecordTypeIds.get('License_Renewal'));
            ApexPages.StandardController sc                       = new ApexPages.StandardController(objSR);
            Cls_New_Dependent_multiple_Visa_Request thisInstance = new Cls_New_Dependent_multiple_Visa_Request(sc);
            thisInstance.ModeType = 'Multiple';
            thisInstance.NextClick();
            thisInstance.SRData = objSR;
            thisInstance.SavedSR();
         test.stopTest();
     }
    
    static testmethod void newDependentMultipleVisa2() {
        test.startTest();
        Map<string,string> mapRecordTypeIds = new Map<string,string>();
        
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Non_DIFC_Sponsorship_Visa_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
       Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        insert objContact;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Consulate_or_Embassy_Letter';
        objTemplate.SR_RecordType_API_Name__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Menutext__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Required Docs to Upload';
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        Lookup__c nat1 = new Lookup__c(Type__c='Nationality',Name='Afghanistan');
        insert nat1;
        Lookup__c nat2 = new Lookup__c(Type__c='Nationality',Name='India');
        insert nat2;
        Lookup__c nat3 = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat3;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Consulate_or_Embassy_Letter' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId; 
        objSR.submitted_date__c = date.today();
       // objSR.Internal_SR_Status__c = objStatus.id;
        //objSR.External_SR_Status__c = objStatus.id;
        objSR.Country__c = 'India; ';
        objSR.Country__c = objSR.Country__c + 'Afghanistan; ';
        objSR.Country__c = objSR.Country__c + 'Pakistan; ';
        objSR.contact__c = objContact1.id;
        objSR.Sponsor__c = objContact1.Id;
        insert objSR;
        
          PageReference pageRef = Page.New_Dependent_multiple_Visa_Request;
              Test.setCurrentPage(pageRef);

            pageRef.getParameters().put('RecordType', mapRecordTypeIds.get('Non_DIFC_Sponsorship_Visa_Renewal'));
            ApexPages.StandardController sc                       = new ApexPages.StandardController(objSR);
            Cls_New_Dependent_multiple_Visa_Request thisInstance = new Cls_New_Dependent_multiple_Visa_Request(sc);

            thisInstance.RecordTypeName = 'Non_DIFC_Sponsorship_Visa_Renewal';
            thisInstance.MoveToUploadDocuments_openDep();
            thisInstance.MoveToUploadDocuments();
            thisInstance.MoveToAddDependent();
        
            thisInstance.RecordTypeName = 'CANCELLATION';
            thisInstance.MoveToUploadDocuments_openDep();
            thisInstance.MoveToUploadDocuments();
            thisInstance.MoveToAddDependent();
        
            thisInstance.RecordTypeName = 'NEW';
            thisInstance.MoveToUploadDocuments_openDep();
            thisInstance.MoveToUploadDocuments();
            thisInstance.MoveToAddDependent();
        
            thisInstance.SRData = objSR;
            thisInstance.AddNewDependent();
            thisInstance.getSponsorList();
        
            SR_Doc__c thisSr = new SR_Doc__c();
            thisSr.Service_Request__c = objSR.Id;
            thisSr.Is_Not_Required__c = false;
            insert thisSr;
            thisInstance.submitSR();
            thisInstance.DocumentUploadCheck();
        test.stopTest();
    }
  }