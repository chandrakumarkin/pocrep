/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 06-01-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   28-04-2021   Mari                                Initial Version
**/

public without sharing  class EmployeeCardNonSponsoredController{
  
    public string recordTypeName {get; set;}   
    string recordTypeId;
    public boolean isPortal {get; set;}
    public boolean readOnly{get;set;}
    public boolean isEdit {get;set;} 
    public Account objAccount{get;set;}
    public boolean isSubmitted{get;set;}
    public SR_Template__c srTemplate{get;set;}
    public boolean isDetailPage {get; set;}
    public string selectedSponsor{get;set;}
    public Service_Request__c srRequest{get;set;}
   // public List<SelectOption> sponsorList{get; set;}
    string srTempId;

 
    public EmployeeCardNonSponsoredController(ApexPages.StandardController controller){
     
        try{
            readOnly=true;
            srTempId ='';
            selectedSponsor ='';
            isSubmitted = false;
            objAccount=new account();  
            isPortal = (Userinfo.getUserType() != 'Standard') ? true :false;
            string isEditVal = Apexpages.currentPage().getParameters().get('isEdit');
            string isDetail = Apexpages.currentPage().getParameters().get('isDetail');
            isDetailPage = (string.isNotBlank(isDetail) && isDetail == 'true') ? true : false;
            isEdit = (isEditVal != null && isEditVal =='true') ? true : false; 
            string recId = (Apexpages.currentPage().getParameters().get('id') != null) ? Apexpages.currentPage().getParameters().get('id') : '';
            srRequest = new Service_Request__c();
            if(recId != ''){
                List<Sobject> sobjectList= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(recId),'RecordType.DeveloperName,Customer__r.Name,Customer__r.Registration_License_No__c,Contact__r.Name,Customer__r.Index_Card__r.Name');
                List<Service_Request__c> srList= (List<Service_Request__c>) sobjectList;
                srRequest = (srList.size() >0 && srList != null) ? srList[0] : new  Service_Request__c();
                isSubmitted =(srRequest.Submitted_Date__c != null) ? true : false;
                recordTypeName=srRequest.RecordType.DeveloperName;
            }      

            if((isEdit || srRequest.id == null) && !isDetailPage)readOnly= false;

            Map<string,string>  mapParameters = (apexpages.currentPage().getParameters()!=null) ? apexpages.currentPage().getParameters() : new Map<string,string>();       
            recordTypeId = (mapParameters.get('RecordType')!=null)  ? mapParameters.get('RecordType') :'';
            recordTypeName = (mapParameters.get('RecordTypeName')!=null)  ? mapParameters.get('RecordTypeName') :'';
              
            if(recordTypeName != ''){ 
                
                List<RecordType> recTypeList = [SELECT Id,DeveloperName FROM RecordType  WHERE Id=:RecordTypeId AND sObjectType = 'Service_Request__c' limit 1];
                if(recTypeList.size() >0) recordTypeName =recTypeList[0].DeveloperName;
                List<SR_Template__c>srTempList = [select Id,SR_Document_Description_Text__c,Menutext__c,SR_Description__c,SR_RecordType_API_Name__c from SR_Template__c where SR_RecordType_API_Name__c=:recordTypeName and Active__c=true limit 1];
                srTemplate= (srTempList.size() >0) ? srTempList[0] : new SR_Template__c();
                srTempId = (srTempList.size() >0) ? srTempList[0].Id : '';
            }
            
            if(isSubmitted || isDetailPage)readOnly= true;

            if(srRequest.Customer__c != null){
                List<Sobject> sobjectList= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(srRequest.Customer__c),'Index_Card__r.Name');
                List<Account> accList= (List<Account>) sobjectList;
                objAccount= (accList.size() >0) ? accList[0] : new Account();
            }
        }catch (Exception e) {
            apexpages.addMessage(new ApexPages.message(Apexpages.Severity.Error,e.getMessage()));
        } 
    }
    
    /**
        * Helper method to populate the list of Sponsored DIFC and seconded employees
            Populate Contact details to SR fields
        * @param  . 
        * @return void.          
    */
    
    public void populateOnLoad(){
      
    }
    
    /************************************************************************************************************
    Method Name: saveRequest
    Functionality: save or update SR Record and redirect to detail page of SR
    **************************************************************************************************************/
    public pageReference saveRequest(){
        pageReference redirectPage = null;
        try{ 
            string message = 'Success';           
            upsert srRequest;   
            /*
            redirectPage = new PageReference('/apex/'+GlobalConstants.EMPLOYEE_DL_ACCESS_VF_NAME);
            redirectPage.getParameters().put('RecordType',srRequest.recordTypeId);
            redirectPage.getParameters().put('RecordTypeName',recordTypeName);
            redirectPage.getParameters().put('id',srRequest.id);
            redirectPage.getParameters().put('isDetail','true');
            */   
            redirectPage = new PageReference('/'+srRequest.id);
            if(redirectPage!=null){
                redirectPage.setRedirect(true); 
                return redirectPage;  
            }
        }catch(DmlException e) {
            Integer numErrors = e.getNumDml();
            for(Integer i=0;i<numErrors;i++) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getDmlMessage(i)));
            }
        }catch(exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
        }
        return redirectPage;
    }
    
     /************************************************************************************************************
    Method Name: submitSR
    Functionality: submit the SR Record and redirect to Submission page of SR
    **************************************************************************************************************/

    public pageReference submitSR(){
        pageReference redirectPage = null;
        
        try{

            if(srRequest.Required_Docs_not_Uploaded__c == true){
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,GlobalConstants.DOCUMENT_UPLOAD_MANDATORY));
                return null;
            }
            redirectPage = new PageReference('/apex/SRSubmissionPage');
            redirectPage.getParameters().put('id',srRequest.id);
            redirectPage.setRedirect(true); 
            return redirectPage;          
        }catch(exception ex){
            system.debug('----msg---'+ex.getMessage()); 
            system.debug('----lineNumber---'+ex.getLineNumber());   
        }
        return redirectPage;
    }
    
    /************************************************************************************************************
    Method Name: editRequest
    Functionality: Edit the SR Record and redirect to detail page after Save
    **************************************************************************************************************/
    public pageReference editRequest(){
        pageReference redirectPage = null;


        try{
            redirectPage = new PageReference('/'+srRequest.id+'/e?retURL='+srRequest.id);
            redirectPage.setRedirect(true); 
            return redirectPage;          
        }catch(exception ex){
            system.debug('----msg---'+ex.getMessage()); 
            system.debug('----lineNumber---'+ex.getLineNumber());   
        } 
        return redirectPage;
    }
    
     /************************************************************************************************************
    Method Name: cancelRequest
    Functionality: Cancel the SR Record before approval of SR
    **************************************************************************************************************/
    public pageReference cancelRequest(){//v1.1
        pageReference objRef = null;
        try{
            if(IsDetailPage == true){ //Detail Page Button
               //system.debug('===========cancelRequest====================');
                if(srRequest.Submitted_Date__c != null && srRequest.External_Status_Name__c!=GlobalConstants.SR_STAT_APPROVED){
                    string result = GsServiceCancellation.ProcessCancellation(srRequest.id);    
                    if(result!='Cancellation submitted successfully'){
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,result));   
                    }else{
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,result));
                    }
                    if(result=='Cancellation submitted successfully'){
                        objRef = new Pagereference('/'+srRequest.Id);   
                    }
                }else{
                    if(srRequest.External_Status_Name__c == GlobalConstants.SR_STAT_DRAFT)delete srRequest;
                    objRef = (isPortal) ? new Pagereference('/apex/home') : new Pagereference('/home/home.jsp');
                }
            }else if(IsDetailPage == false){ //Edit Page Button
                if(srRequest.Id != null)
                    objRef = new Pagereference('/'+srRequest.Id);
                else{
                    objRef = (isPortal) ? new Pagereference('/apex/home') : new Pagereference('/home/home.jsp');
                }
            }
            if(objRef != null)objRef.setRedirect(true);
            return objRef;
        }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please contact support.'));
        }
        return objRef;
    }
}