@isTest
public class WebServiceCdrTest {
    
    @testSetup static void setup() {
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insertNewAccounts[0].Registration_License_No__c = '1234';
        insertNewAccounts[0].ROC_reg_incorp_Date__c = System.today().addMonths(3);
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Private' ;
        insertNewAccounts[0].Trading_Name_Arabic__c = 'ذا ستيك بار';
        insertNewAccounts[0].Trade_Name__c = 'Abc corp123';
        insertNewAccounts[0].Last_PR_Push__c = system.today();
        insert insertNewAccounts;
        
        License__c objLic = new License__c();
        objLic.License_Issue_Date__c = system.today();
        objLic.License_Expiry_Date__c = system.today().addDays(365);
        objLic.Status__c = 'Active';
        objLic.Account__c = insertNewAccounts[0].id;
        insert objLic;
        
       insertNewAccounts[0].Active_License__c = objLic.id;
        update insertNewAccounts[0];
        
        
        License_Activity_Master__c lm = new License_Activity_Master__c();
        lm.Name = 'Restaurant';
        lm.Activity_Code__c = 'RESTAURANT';
        lm.Enabled__c = true;
        lm.Sys_Code__c  = '255';
        lm.Type__c = 'Business Activity';
        insert lm;
        
        
        License_Activity__c act = new License_Activity__c();
        act.Account__c  = insertNewAccounts[0].Id;
        act.Activity__c = lm.Id;
        act.Start_Date__c = System.today();
        act.End_Date__c   = System.today() + 90;
        insert act;

    }
    
    @isTest
    static void testWebServiceCdrAccountBatch(){
        
        Account acc = [
            SELECT id, Unified_License_Number__c, BP_No__c, Active_License__r.name, Active_License__r.License_Issue_Date__c, Active_License__r.License_Type__c, 
            Active_License__r.License_Expiry_Date__c, Active_License__r.License_Terminated_Date__c
            from Account limit 1
        ];
        Test.setMock(HttpCalloutMock.class, new WebServiceCdrMockCalloutTest());

        Test.startTest();
            WebServiceCdrDeleteAccount.deleteJsonAccCdr(acc.Id);
            WebServiceCdrDeleteAccount.deleteAccountToCdr(acc.Id);
            WebServiceCdrAccountBatch obj = new WebServiceCdrAccountBatch();
            DataBase.executeBatch(obj); 
        Test.stopTest();

    }
    
    @isTest
    static void testWebServiceCdrCreateActivity(){
        
        License_Activity__c lc = [select Id from License_Activity__c limit 1];
        Test.setMock(HttpCalloutMock.class, new WebServiceCdrMockCalloutTest());
        Test.startTest();
        
           
           WebServiceCdrCreateActivity.createJsonActivityCdr(lc.Id);
           WebServiceCdrCreateActivity.createActivityCdr(lc.Id);
        Test.stopTest();
    }
    
    @isTest
    static void testWebServiceCdrCreateActivity1(){
        
        License_Activity__c lc = [select Id from License_Activity__c limit 1];
        Test.setMock(HttpCalloutMock.class, new WebServiceCdrMockCalloutTest());
        Test.startTest();
        
            WebServiceCdrDeleteActivity.deleteJsonActivityCdr(lc.Id);
            WebServiceCdrDeleteActivity.deleteActivityCdr(lc.Id);
        
        
        Test.stopTest();
    }
    
    
    @isTest
    static void testWebServiceCdrCreateLicense(){
        
        License__c lc = [select Id from License__c limit 1];
        Test.setMock(HttpCalloutMock.class, new WebServiceCdrMockCalloutTest());
        Test.startTest();
           WebServiceCdrCreateLicense.createJsonLicenseCdr(lc.Id);
           WebServiceCdrCreateLicense.PostLicenseToCdr(lc.Id);
           
            WebServiceCdrCreateLicenseBatch obj = new WebServiceCdrCreateLicenseBatch();
            DataBase.executeBatch(obj); 
        Test.stopTest();
    }
    
    @isTest
    static void testWebServiceCdrDeleteLicense(){
        
        License__c lc = [select Id from License__c limit 1];
        Test.setMock(HttpCalloutMock.class, new WebServiceCdrMockCalloutTest());
        Test.startTest();
           WebServiceCdrDeleteLicense.deleteJsonLicenseCdr(lc.Id);
           WebServiceCdrDeleteLicense.deleteLicenseToCdr(lc.Id);
           
        Test.stopTest();
    }
    
    @isTest
    static void testWebServiceCdrEditAccount(){
        
        Account acc = [
            SELECT id, Unified_License_Number__c, BP_No__c, Active_License__r.name, Active_License__r.License_Issue_Date__c, Active_License__r.License_Type__c, 
            Active_License__r.License_Expiry_Date__c, Active_License__r.License_Terminated_Date__c
            from Account limit 1
        ];
        Test.setMock(HttpCalloutMock.class, new WebServiceCdrMockCalloutTest());

        Test.startTest();
            WebServiceCdrEditAccount.editJsonAccCdr(acc.Id);
            WebServiceCdrEditAccount.editAccountToCdr(acc.Id);
            
        Test.stopTest();

    }
    
     
    @isTest
    static void testWebServiceCdrEditActivity(){
        
        License_Activity__c lc = [select Id from License_Activity__c limit 1];
        Test.setMock(HttpCalloutMock.class, new WebServiceCdrMockCalloutTest());
        Test.startTest();
        
            WebServiceCdrEditActivity.editJsonActivityCdr(lc.Id);
            WebServiceCdrEditActivity.editActivityCdr(lc.Id);
        
        
        Test.stopTest();
    }
    
     @isTest
    static void testWebServiceCdrEditLicense(){
        
        License__c lc = [select Id from License__c limit 1];
        Test.setMock(HttpCalloutMock.class, new WebServiceCdrMockCalloutTest());
        Test.startTest();
           WebServiceCdrEditLicense.editJsonLicenseCdr(lc.Id);
           WebServiceCdrEditLicense.PutLicenseToCdr(lc.Id);
           
        Test.stopTest();
    }
    
    @isTest
    static void testWebServiceCdrPostAccount(){
        
        Account acc = [
            SELECT id, Unified_License_Number__c,ROC_Status__c,Place_of_Registration__c, BP_No__c, Active_License__r.name, Active_License__r.License_Issue_Date__c, Active_License__r.License_Type__c, 
            Active_License__r.License_Expiry_Date__c, Active_License__r.License_Terminated_Date__c
            from Account limit 1
        ];
        Test.setMock(HttpCalloutMock.class, new WebServiceCdrMockCalloutTest());

        Test.startTest();
            WebServiceCdrPostAccount.createJsonAccCdr(acc.Id);
            WebServiceCdrPostAccount.PostAccountToCdr(acc.Id);
            
        Test.stopTest();

    }
       
}