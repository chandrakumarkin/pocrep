/**
 * @description       : 
 * @author            : Shoaib Tariq
 * @group             : 
 * @last modified on  : 05-06-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   05-06-2021   Shoaib Tariq   Initial Version
**/
global with sharing class CC_NocStepApproved implements HexaBPM.iCustomCodeExecutable {
  
      global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {

        HexaBPM__Service_Request__c thisRequest = [SELECT Id,
                                                        HexaBPM__Customer__c ,
                                                        RecordTypeId,Setting_Up__c 
                                                    FROM HexaBPM__Service_Request__c 
                                                    WHERE ID =: stp.HexaBPM__SR__c ];

        if([SELECT Id,Step_Template_Code__c FROM HexaBPM__Step__c
                                            WHERE HexaBPM__SR__c =: thisRequest.Id 
                                            AND Step_Template_Code__c = 'SHARING_OF_OFFICE_APPROVE' LIMIT 1 ].Size()>0){

               If([SELECT Id,Step_Template_Code__c FROM HexaBPM__Step__c
                                            WHERE HexaBPM__SR__c =: thisRequest.Id 
                                            AND HexaBPM__Status__r.Name = 'Approved'
                                            AND Step_Template_Code__c = 'SHARING_OF_OFFICE_APPROVE' LIMIT 1 ].Size()>0){
                   return 'Success';                                      
               }
               else {
                   return 'False';
               }
        }
        else{
           return 'Success';
        }
        
    }
}