@isTest
global class ThompsonReutersValidationMockTest implements HttpCalloutMock {
    
    public static string clientValID ='';
    public ThompsonReutersValidationMockTest (String clientID){
        clientValID = clientID;
    }
    global HTTPResponse respond(HTTPRequest req) {
        
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        // single request res.setBody('<sdResultSet><indexDate>2019-12-24T17:08:33.970+04:00</indexDate><sdQuery><clientID>"'+clientValID+'"</clientID><clientType>CUSTOMER</clientType><createCaseOnMatches>CREATECASE</createCaseOnMatches><types><type>INDIVIDUAL</type></types><caseNum>200</caseNum><sdResult>200</sdResult><entityId>200</entityId><category>200</category><originalScript>200</originalScript><score>200</score><secondaryGenderInfo>Male</secondaryGenderInfo><secondaryCountryInfo>200</secondaryCountryInfo><secondaryCityInfo>200</secondaryCityInfo><secondaryAddrInfo>200</secondaryAddrInfo><name>fsdf fdsf</name><nationalityName></nationalityName><ruleId>RULE151</ruleId></sdQuery><sdResults/></sdResultSet>');
        // multiple reqest
        res.setBody('<?xml version="1.0" encoding="UTF-8" standalone="yes"?><sdResultsSets><resultSets><sdResultSet><caseCode>20200603093658975312</caseCode><caseNum>202000003520</caseNum><clientCode>TRC-24006</clientCode><clientFeedSourceSystem>-</clientFeedSourceSystem><indexDate>2020-06-02T17:12:31.823+04:00</indexDate><sdQuery><clientID>TRC-24006</clientID><clientType>CUSTOMER</clientType><createCaseOnMatches>CREATECASE</createCaseOnMatches><types><type>INDIVIDUAL</type></types><indDob>1981-01-28T00:00:00+04:00</indDob><name>George Davida</name><nationalityName>United Arab Emirates</nationalityName><ruleId>RULE151</ruleId></sdQuery><sdResults><sdResult><categories><category>ec_4</category></categories><entityId>e_tr_wci_3086840</entityId><modifiedDate>2020-02-28T04:00:00+04:00</modifiedDate><name>George DAVID</name><originalScript>George DAVID</originalScript><score>93.46293210983276</score><secondaryAddrInfo>Lagos, Lagos, NIGERIA</secondaryAddrInfo><secondaryCityInfo>Lagos</secondaryCityInfo><secondaryCountryInfo>NIGERIA</secondaryCountryInfo><secondaryDateOfBirthInfo>1937-06-10</secondaryDateOfBirthInfo><secondaryGenderInfo>MALE</secondaryGenderInfo><secondaryNationalityInfo>CY/CYPRUS</secondaryNationalityInfo><secondaryRuleInfo>Name Partial Match, Secondary: -6.0</secondaryRuleInfo><secondaryScore>-6.0</secondaryScore><sources><source>b_trwc_1</source></sources><totalScore>87.46293210983276</totalScore><type>INDIVIDUAL</type></sdResult></sdResults></sdResultSet></resultSets></sdResultsSets>');
        res.setStatusCode(200);
        //{"DIFC":"<?xml version="1.0" encoding="UTF-8" standalone="yes"?> 
        return res;
    }
    
    
    
}