/******************************************************************************************
 *  Class    : AttachmentTriggerHandler
 *  Author   : Rica Ramos
 *  Company  : PWC
 *  Date     : 2018-17-Apr                           
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    17-04-2018  Rica         Created 
v1.1    07-12-2019  Durga        Method to create content document for attachments created by drawloop fo Onbaording
****************************************************************************************************************/
public without sharing class AttachmentTriggerHandler{
    public static void validateExtension(list<Attachment> TriggerNew){
        Set<String> extNotAllowed = new set<String> {'exe','bat'};
        for(Attachment objAtt : TriggerNew){
            system.debug('@@PARENT: '+objAtt.ParentId);
            if(objAtt.ParentId != null){
                String parentIdStr = objAtt.ParentId;
                String parentIdPrefix = parentIdStr.subString(0,3);
                system.debug('@@PARENTSTR: '+parentIdPrefix);
                if(parentIdPrefix == '500'){
                    String filename = objAtt.Name.toLowerCase();
                    String filenameExt = filename.SubString(filename.indexof('.')+1,filename.length());
                    system.debug('@@FILE_EXT: '+filenameExt);
                    if(filename != null && filename.indexof('.')>-1 && filename.SubString(filename.indexof('.')+1,filename.length())!=''){
                        string contyp = filename.SubString(filename.indexof('.')+1,filename.length());
                        if(extNotAllowed.contains(contyp.toLowercase())){
                            objAtt.addError('These file extensions are not alllowed: .exe .bat');
                        }
                    }
                }
            }
        }
        
    }
    /*
        Method Name :   CreateContentVersion //v1.1
        Description :   Method to create content version for the attachment under HexaBPM SR Doc
    */
    public static void CreateContentVersion(list<Attachment> TriggerNew){
        set<string> setParentId = new set<string>();
        map<string,string> MapGeneratedDocumentId = new map<string,string>(); 
        set<string> setParentIdSR = new set<string>();
        map<string,string> MapGeneratedDocumentIdSR = new map<string,string>(); 
        for(Attachment objAtt : TriggerNew){
            if(objAtt.ParentId.getSobjectType() == HexaBPM__SR_Doc__c.SobjectType){
                setParentId.add(objAtt.ParentId);
            }
            if(objAtt.ParentId.getSobjectType() == SR_Doc__c.SobjectType){
                setParentIdSR.add(objAtt.ParentId);
            }
        }
        if(setParentId.size()>0){
            for(HexaBPM__SR_Doc__c doc:[Select Id from HexaBPM__SR_Doc__c where Id IN:setParentId AND (HexaBPM__Sys_IsGenerated_Doc__c=true or HexaBPM__Status__c='Generated')]){
                MapGeneratedDocumentId.put(doc.Id,doc.Id);
            }
        }
        if(setParentIdSR.size()>0){
            for(SR_Doc__c doc:[Select Id from SR_Doc__c where Id IN:setParentIdSR AND (Sys_IsGenerated_Doc__c=true or Status__c='Generated') AND SR_Template_Doc__r.Allow_File_Creation__c=true]){
                MapGeneratedDocumentIdSR.put(doc.Id,doc.Id);
            }
        }
		map<String,String> mapCV_Parent_ID = new map<String,String>();
        list<ContentVersion> lstCV = new list<ContentVersion>();
        for(Attachment objAtt : TriggerNew){
            if((objAtt.ParentId.getSobjectType() == HexaBPM__SR_Doc__c.SobjectType && MapGeneratedDocumentId.get(objAtt.ParentId)!=null)
                || (objAtt.ParentId.getSobjectType() == SR_Doc__c.SobjectType && MapGeneratedDocumentIdSR.get(objAtt.ParentId)!=null)){
                ContentVersion conVer = new ContentVersion();
                conVer.ContentLocation = 'S';
                conVer.PathOnClient = objAtt.Name;// '.'+objAtt.ContentType
                conVer.Title = objAtt.Name;
                conVer.Origin = 'C';//Content Origin
				if(objAtt.ParentId.getSobjectType() == HexaBPM__SR_Doc__c.SobjectType){
					conVer.Application_Document__c = objAtt.ParentId;
				}else if(objAtt.ParentId.getSobjectType() == SR_Doc__c.SobjectType){
					conVer.SR_Doc__c=objAtt.ParentId;
				}
                conVer.VersionData = objAtt.Body;
                lstCV.add(conVer);
            }
        }
        if(lstCV.size()>0){
            insert lstCV;
            map<string,string> MapCV_CD_Id = new map<string,string>();
            list<ContentDocumentLink> lstCDL = new list<ContentDocumentLink>();
            for(ContentVersion cv:[select Id,ContentDocumentId,Application_Document__c,SR_Doc__c from ContentVersion where Id IN: lstCV]){
                ContentDocumentLink cDocLink = new ContentDocumentLink();				
                cDocLink.ContentDocumentId = cv.ContentDocumentId;//Add ContentDocumentId
                cDocLink.LinkedEntityId = cv.Application_Document__c!=null?cv.Application_Document__c:cv.SR_Doc__c;//Add attachment parentId
                cDocLink.ShareType = 'V';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
                cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
                lstCDL.add(cDocLink);
            }
            if(lstCDL.size()>0)
                insert lstCDL;
        }
    }
}