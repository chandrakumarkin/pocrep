/******************************************************************************************
 *  Author   : Kaavya Raghuram
 *  Company  : NSI
 *  Date     : 13 June 2016   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class Test_CC_Cls_FitOutCustomCode_Addl {

    static testMethod void mytest() {
        
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        
        insert testSrTemplate;
        
        SR_Steps__c srstep = new SR_Steps__c();
        insert srstep;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;
        //SR.SR_Group__c='Fit-Out & Events';
        SR.Linked_SR__c = SR1.id;     
        SR.Type_of_Request__c = 'Tenant Authorized Representative';   
        
        insert SR;
        
        
        
         list<Amendment__c> amendlist = new list<Amendment__c> ();
          Amendment__c amnd1 = new Amendment__c (RecordTypeId=mapRecordType.get('Fit_Out_Contact'),ServiceRequest__c=SR.id,Amendment_Type__c='Tenant Authorized Representative');
          amendlist.add(amnd1);
          /*
          Amendment__c amnd2 = new Amendment__c (RecordTypeId=mapRecordType.get('Data_Center_Requestor'),ServiceRequest__c=lstSR[1].id);
          amendlist.add(amnd2);
          Amendment__c amnd3 = new Amendment__c (RecordTypeId=mapRecordType.get('Tool'),ServiceRequest__c=lstSR[0].id);
          amendlist.add(amnd3);*/
  
          insert amendlist;
        
        Test.startTest();
        Step__c objStep = new Step__c();
        objStep.SR__c = SR.Id;
        objStep.Step_No__c = 1.0;
        objStep.step_notes__c = 'abc';
        objStep.SR_Step__c=srstep.id;
        //objStep.SR_Group__c='Fit-Out & Events';
        insert objStep;  
        
        Step__c objStepD = new Step__c();
        objStepD.SR__c = SR.Id;
        objStepD.Step_No__c = 1.0;
        objStepD.step_notes__c = 'abc';
        objStepD.SR_Step__c=srstep.id;
        insert objStepD;  
        
        SR_DOC__c srdoc= new SR_DOC__c();
        srdoc.Service_Request__c=SR.id;
        srdoc.Step__c=objStep.id; 
        srdoc.Sys_IsGenerated_Doc__c = true;
        srdoc.Customer__c =objAccount.Id;
                                                               
        
        Date endDate= system.today()+4;
        Datetime SD= system.now();
        
        
        
        Service_Request__c RevSR = new Service_Request__c();
        RevSR.RecordTypeId = mapRecordType.get('Revision_of_Fit_Out_Request');
        RevSR.Customer__c = objAccount.Id;
        RevSR.Linked_SR__c=SR.id; 
        RevSr.Type_of_Request__c = 'Major modification';
        RevSR.User_Form_Action__c='First Fix Inspection planning step';      
        insert RevSR;
        
        Step__c objStep1 = new Step__c();
        objStep1.SR__c = RevSR.Id;
        objStep1.Step_No__c = 1.0;
        objStep1.step_notes__c = 'abc';
        objStep1.SR_Step__c=srstep.id;
        objStep1.Sr_Type__c = RevSR.Id;
        insert objStep1;
        
       
        
        CC_cls_FitOutandEventCustomCode.addlookAndFeelComments(objStep);
        CC_cls_FitOutandEventCustomCode.updateEstimatedHours(objStep);
        CC_cls_FitOutandEventCustomCode.checkSRstepStatus(objStep);
        CC_cls_FitOutandEventCustomCode.DocumentUploadCheck(objStep);
        CC_cls_FitOutandEventCustomCode.DocumentVerifiedCheck(objStep);
        CC_cls_FitOutandEventCustomCode.contractorPassExist(objStep);
        CC_cls_FitOutandEventCustomCode.revisionOfFORequest(objStep1);
        CC_cls_FitOutandEventCustomCode.calculateStartDate(endDate,10);
        CC_cls_FitOutandEventCustomCode.calculateEndDate(endDate,10);
        CC_cls_FitOutandEventCustomCode.insertStep(objStep,SR,objStatus.id);
        CC_cls_FitOutandEventCustomCode.copySRDocs(objStep);
        CC_cls_FitOutandEventCustomCode.generateNocDocs(objStep);
        CC_cls_FitOutandEventCustomCode.copyDateOfInduction(objStep);
        CC_cls_FitOutandEventCustomCode.checkDewaDcdInspectionDates(objStep);
        CC_cls_FitOutandEventCustomCode.validateRenewalDates(SR1,SR);
        CC_cls_FitOutandEventCustomCode.updateContractorExpiryDate(objStep);       
        
        //CC_cls_FitOutandEventCustomCode.calculateStepDuration(SD,SD,BH.id);
        Test.stopTest();
        
    }
    static testMethod void mytest2() {
        
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;  
        SR.Type_of_Request__c = 'Update Client Details';           
        insert SR;
        
      
        
        Step__c objStep = new Step__c();
        objStep.SR__c = SR.Id;
        objStep.Step_No__c = 1.0;
        //objStep.Status__c = objStatus.Id;
        insert objStep;  
        
        Date endDate= system.today()+4;
        Datetime SD= system.now();
        
        //CC_cls_FitOutandEventCustomCode.updateContactDetails(objStepU);
        
        Service_Request__c RevSR = new Service_Request__c();
        RevSR.RecordTypeId = mapRecordType.get('Revision_of_Fit_Out_Request');
        RevSR.Customer__c = objAccount.Id;
        RevSR.Linked_SR__c=SR.id; 
        RevSr.Type_of_Request__c ='Major modification';
        RevSR.User_Form_Action__c='Final Inspection Planning step';      
        insert RevSR;
        
        Step__c objStep1 = new Step__c();
        objStep1.SR__c = RevSR.Id;
        objStep1.Step_No__c = 1.0;     
        objStep1.Sr_Type__c = RevSR.Id;   
        insert objStep1;  
        
        
        
        CC_cls_FitOutandEventCustomCode.revisionOfFORequest(objStep1);
        
        
    
    }
    static testMethod void mytest3() {
        
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        
        insert testSrTemplate;
        
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;  
        SR.Type_of_Request__c = 'Update Client Details';
                   
        insert SR;
        
       
         list<Amendment__c> amendlist = new list<Amendment__c> ();
          Amendment__c amnd1 = new Amendment__c (RecordTypeId=mapRecordType.get('Fit_Out_Contact'),ServiceRequest__c=SR.id,Amendment_Type__c='Tenant Authorized Representative');
          amendlist.add(amnd1);
          /*
          Amendment__c amnd2 = new Amendment__c (RecordTypeId=mapRecordType.get('Data_Center_Requestor'),ServiceRequest__c=lstSR[1].id);
          amendlist.add(amnd2);
          Amendment__c amnd3 = new Amendment__c (RecordTypeId=mapRecordType.get('Tool'),ServiceRequest__c=lstSR[0].id);
          amendlist.add(amnd3);*/
  
          insert amendlist;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = SR.Id;
        objStep.Step_No__c = 1.0;
        //objStep.Status__c = objStatus.Id;
        insert objStep;  
        
        Date endDate= system.today()+4;
        Datetime SD= system.now();
        
        //CC_cls_FitOutandEventCustomCode.updateContactDetails(objStepU);
        System.runAs(new User(id=userinfo.getUserId())){
        Service_Request__c RevSR = new Service_Request__c();
        RevSR.RecordTypeId = mapRecordType.get('Revision_of_Fit_Out_Request');
        RevSR.Customer__c = objAccount.Id;
        RevSR.Linked_SR__c=SR.id; 
        RevSr.Type_of_Request__c ='Major modification';
        RevSR.User_Form_Action__c='Final Inspection Planning step';      
        insert RevSR;
        
        Step__c objStep1 = new Step__c();
        objStep1.SR__c = RevSR.Id;
        objStep1.Step_No__c = 1.0;        
        insert objStep1;  
        
        Map<Id,Violation__c> srViolationMap = new Map<Id,Violation__c>();
        Violation__c v= new Violation__c();
        v.Related_Request__c=SR.id;
        insert v;
        srViolationMap.put(SR.id, v);
        
        
        SR_Price_Item__c srp = new SR_Price_Item__c();
        srp.ServiceRequest__c=SR.id;
        srp.Price__c=100;
        insert srp;
        
        CC_cls_FitOutandEventCustomCode.checkTechnicalClarificationForm(objStep);
        CC_cls_FitOutandEventCustomCode.validateDcdDewaSrDocs(objStep);
       
        CC_cls_FitOutandEventCustomCode.setProjectsOnHold(objStep);
        CC_cls_FitOutandEventCustomCode.resetSrsOnHold(objStep);
       // CC_cls_FitOutandEventCustomCode.updateSrPriceLineItem(srViolationMap);
        
        try {
            CC_cls_FitOutandEventCustomCode.hasViolationAttachments(srViolationMap,true);
        } catch(Exception e) {
            
        }
        }
        test.StartTest();
        
        insert new SR_Status__c(Name = 'Project Cancelled',Code__c = 'PROJECT_CANCELLED',Type__c = 'End');
        
        CC_cls_FitOutandEventCustomCode.setToProjectCancelled(objStep);
        //CC_cls_FitOutandEventCustomCode.createContractorRecord(objStep);
        
        test.stopTest();
    
    }
    static testMethod void mytest4() {
        
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        
        insert testSrTemplate;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        /*
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1; */
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;  
        SR.Type_of_Request__c = 'Update Client Details';           
        insert SR;
        
       
         list<Amendment__c> amendlist = new list<Amendment__c> ();
          Amendment__c amnd1 = new Amendment__c (RecordTypeId=mapRecordType.get('Fit_Out_Contact'),ServiceRequest__c=SR.id,Amendment_Type__c='Tenant Authorized Representative');
          amendlist.add(amnd1);
          /*
          Amendment__c amnd2 = new Amendment__c (RecordTypeId=mapRecordType.get('Data_Center_Requestor'),ServiceRequest__c=lstSR[1].id);
          amendlist.add(amnd2);
          Amendment__c amnd3 = new Amendment__c (RecordTypeId=mapRecordType.get('Tool'),ServiceRequest__c=lstSR[0].id);
          amendlist.add(amnd3);*/
  
          insert amendlist;
        /*
        Step__c objStep = new Step__c();
        objStep.SR__c = SR.Id;
        objStep.Step_No__c = 1.0;
        //objStep.Status__c = objStatus.Id;
        insert objStep;  */
        
        Date endDate= system.today()+4;
        Datetime SD= system.now();
        
        Service_Request__c updSR = new Service_Request__c();
        updSR.RecordTypeId = mapRecordType.get('Update_Contact_Details');
        updSR.Customer__c = objAccount.Id;
        updSR.Linked_SR__c = SR.id;     
        updSR.Type_of_Request__c = 'Tenant Authorized Representative';   
        
        insert updSR;
        
        Step__c objStepU = new Step__c();
        objStepU.SR__c = updSR.Id;
        objStepU.Step_No__c = 1.0;
        objStepU.step_notes__c = 'abc';
        //objStepU.SR_Step__c=srstep.id;
        insert objStepU; 
        CC_cls_FitOutandEventCustomCode.updateContactDetails(objStepU);
        
        Service_Request__c RevSR = new Service_Request__c();
        RevSR.RecordTypeId = mapRecordType.get('Revision_of_Fit_Out_Request');
        RevSR.Customer__c = objAccount.Id;
        RevSR.Linked_SR__c=SR.id; 
        RevSr.Type_of_Request__c = 'Major modification';
        RevSR.User_Form_Action__c='Detailed Design Approval steps';      
        insert RevSR;
        
        SR_Steps__c srstep = new SR_Steps__c();
        insert srstep;
        
        Step__c objStep1 = new Step__c();
        objStep1.SR__c = RevSR.Id;
        objStep1.Step_No__c = 1.0;
        objStep1.step_notes__c = 'abc';
        objStep1.SR_Step__c=srstep.id;
        objStep1.Sr_Type__c = RevSR.Id;
        insert objStep1;
        
        CC_cls_FitOutandEventCustomCode.revisionOfFORequest(objStep1);
    }
    static testMethod void mytest5() {
        
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        
        insert testSrTemplate;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;  
        SR.Type_of_Request__c = 'Update Client Details';           
        insert SR;
        
       
         list<Amendment__c> amendlist = new list<Amendment__c> ();
          Amendment__c amnd1 = new Amendment__c (RecordTypeId=mapRecordType.get('Fit_Out_Contact'),ServiceRequest__c=SR.id,Amendment_Type__c='Tenant Authorized Representative');
          amendlist.add(amnd1);
          /*
          Amendment__c amnd2 = new Amendment__c (RecordTypeId=mapRecordType.get('Data_Center_Requestor'),ServiceRequest__c=lstSR[1].id);
          amendlist.add(amnd2);
          Amendment__c amnd3 = new Amendment__c (RecordTypeId=mapRecordType.get('Tool'),ServiceRequest__c=lstSR[0].id);
          amendlist.add(amnd3);*/
  
          insert amendlist;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = SR.Id;
        objStep.Step_No__c = 1.0;
        //objStep.Status__c = objStatus.Id;
        insert objStep;  
        
        Date endDate= system.today()+4;
        Datetime SD= system.now();
        
        Service_Request__c updSR = new Service_Request__c();
        updSR.RecordTypeId = mapRecordType.get('Update_Contact_Details');
        updSR.Customer__c = objAccount.Id;
        updSR.Linked_SR__c = SR.id;     
        updSR.Type_of_Request__c = 'Update Contractor Details';   
        
        insert updSR;
        
        Test.startTest();
        Step__c objStepU = new Step__c();
        objStepU.SR__c = updSR.Id;
        objStepU.Step_No__c = 1.0;
        objStepU.step_notes__c = 'abc';
        //objStepU.SR_Step__c=srstep.id;
        insert objStepU; 
        CC_cls_FitOutandEventCustomCode.updateContactDetails(objStepU);
        
        Service_Request__c RevSR = new Service_Request__c();
        RevSR.RecordTypeId = mapRecordType.get('Revision_of_Fit_Out_Request');
        RevSR.Customer__c = objAccount.Id;
        RevSR.Linked_SR__c=SR.id; 
        RevSr.Type_of_Request__c = 'Major modification';
        RevSR.User_Form_Action__c='Concept Design Approval steps';      
        insert RevSR;
        
        SR_Steps__c srstep = new SR_Steps__c();
        insert srstep;
        
        Step__c objStep1 = new Step__c();
        objStep1.SR__c = RevSR.Id;
        objStep1.Step_No__c = 1.0;
        objStep1.step_notes__c = 'abc';
        objStep1.SR_Step__c=srstep.id;
        objStep1.Sr_Type__c = RevSR.Id;
        insert objStep1;
        
        CC_cls_FitOutandEventCustomCode.revisionOfFORequest(objStep1);  
        Lookup__c lk= new Lookup__c();
        lk.Name='HSE';
        lk.Type__c='Property';
        insert lk;
        Fit_Out_Inspection__c ft= new Fit_Out_Inspection__c();   
        ft.lookup__c=lk.id;
        ft.service_request__c=SR.id;
        
        insert ft;   
        //CC_cls_FitOutandEventCustomCode.checkHseFOSPStatus(objStep );
        CC_cls_FitOutandEventCustomCode.updateContractorDetails(objStepU);
        Test.stopTest();
    }
    
    static TestMethod void testDcdDewaInspection(){
        insert Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
        Test_FitoutServiceRequestUtils.prepareSetup();
        Test_FitoutServiceRequestUtils.testDcdDewaInspection();
    }
    
    static TestMethod void testValidateRenewalDates(){
        
        Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
        Account objContractor = Test_CC_FitOutCustomCode_DataFactory.getTestContractor();
        
        insert new List<Account>{objAccount,objContractor};
        insert Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
            
        Test_FitoutServiceRequestUtils.prepareSetup();
        
        Test_FitoutServiceRequestUtils.testValidateRenewalDates();
    }
     static testMethod void mytestFitOut() {
        
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Look_Feel_Approval_Request','Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Status__c objStatus1 = new Status__c();
        objStatus1.Name = 'Declined';
        objStatus1.Code__c = 'Declined';
        insert objStatus1;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Look_Feel_Approval_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        insert testSrTemplate;
        //Step_Template_Code__c='VERIFICATION_BY DIFC_FIT_OUT_TEAM'  and SR_Template__r.SR_RecordType_API_Name__c ='Look_Feel_Approval_Request'
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test scheduled';
        objStepType1.Code__c = 'VERIFICATION_BY DIFC_FIT_OUT_TEAM';
        objStepType1.Step_RecordType_API_Name__c = 'VERIFICATION_BY DIFC_FIT_OUT_TEAM';
        objStepType1.Summary__c = 'VERIFICATION_BY DIFC_FIT_OUT_TEAM';
        objStepType1.Code__c='VERIFICATION_BY DIFC_FIT_OUT_TEAM';
        insert objStepType1;
        
        SR_Steps__c srstep = new SR_Steps__c();
        srstep.Step_Template__c = objStepType1.id;
        srstep.Step_RecordType_API_Name__c = testSrTemplate.id;
        srstep.SR_Template__c =testSrTemplate.id;
        insert srstep;
        SR_Steps__c srStepCreate = [Select id,Step_RecordType_API_Name__c,Name,Step_No__c,Start_Status__c,Step_Template__c,ownerid,Step_Template_Code__c,SR_Template__r.SR_RecordType_API_Name__c from SR_Steps__c where id=:srstep.id];
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;
        //SR.SR_Group__c='Fit-Out & Events';
        SR.Linked_SR__c = SR1.id;     
        SR.Type_of_Request__c = 'Tenant Authorized Representative';   
        
        insert SR;
        
        
        
         list<Amendment__c> amendlist = new list<Amendment__c> ();
          Amendment__c amnd1 = new Amendment__c (RecordTypeId=mapRecordType.get('Fit_Out_Contact'),ServiceRequest__c=SR.id,Amendment_Type__c='Tenant Authorized Representative');
          amendlist.add(amnd1);
          /*
          Amendment__c amnd2 = new Amendment__c (RecordTypeId=mapRecordType.get('Data_Center_Requestor'),ServiceRequest__c=lstSR[1].id);
          amendlist.add(amnd2);
          Amendment__c amnd3 = new Amendment__c (RecordTypeId=mapRecordType.get('Tool'),ServiceRequest__c=lstSR[0].id);
          amendlist.add(amnd3);*/
  
          insert amendlist;
        
        Test.startTest();
        Step__c objStep = new Step__c();
        objStep.SR__c = SR.Id;
        objStep.Step_No__c = 1.0;
        objStep.step_notes__c = 'abc';
        objStep.SR_Step__c=srstep.id;
        //objStep.SR_Group__c='Fit-Out & Events';
        insert objStep;  
        
        Step__c objStepD = new Step__c();
        objStepD.SR__c = SR.Id;
        objStepD.Step_No__c = 1.0;
        objStepD.step_notes__c = 'abc';
        objStepD.SR_Step__c=srstep.id;
        insert objStepD;  
        
        SR_DOC__c srdoc= new SR_DOC__c();
        srdoc.Service_Request__c=SR.id;
        srdoc.Step__c=objStep.id; 
        srdoc.Sys_IsGenerated_Doc__c = true;
        srdoc.Customer__c =objAccount.Id;
                                                               
        
        Date endDate= system.today()+4;
        Datetime SD= system.now();
        
        
        Service_Request__c RevSR = new Service_Request__c();
        RevSR.RecordTypeId = mapRecordType.get('Revision_of_Fit_Out_Request');
        RevSR.Customer__c = objAccount.Id;
        RevSR.Linked_SR__c=SR.id; 
        RevSr.Type_of_Request__c = 'Major modification';
        RevSR.User_Form_Action__c='First Fix Inspection planning step';      
        insert RevSR;
        
        Step__c objStep1 = new Step__c();
        objStep1.SR__c = RevSR.Id;
        objStep1.Step_No__c = 1.0;
        objStep1.step_notes__c = 'abc';
        objStep1.SR_Step__c=srstep.id;
        objStep1.Sr_Type__c = RevSR.Id;
        insert objStep1; 
        
        CC_cls_FitOutandEventCustomCode.optionalDocumentUploadCheck(objStep);
        CC_cls_FitOutandEventCustomCode.createStepVerificationByDIFC_FITOUT(objStep);
        CC_cls_FitOutandEventCustomCode.declineRelaedLookAndFeelSteps(objStep);
        
        Test.stopTest();
        
    }
    
    static testMethod void DeclineRelaedLookAndFeelSteps() {
        
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Look_Feel_Approval_Request','Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Status__c objStatus1 = new Status__c();
        objStatus1.Name = 'Declined';
        objStatus1.Code__c = 'Declined';
        insert objStatus1;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Look_Feel_Approval_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        insert testSrTemplate;
        
        SR_Template__c testSrTemplate1 = new SR_Template__c();
        testSrTemplate1.Name = 'Look and Feel Approval request';
        testSrTemplate1.Menutext__c = 'Look and Feel Approval request';
        testSrTemplate1.SR_RecordType_API_Name__c = 'Look_Feel_Approval_Request';
        testSrTemplate1.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate1.Active__c = true;
        insert testSrTemplate1;
        //Step_Template_Code__c='VERIFICATION_BY DIFC_FIT_OUT_TEAM'  and SR_Template__r.SR_RecordType_API_Name__c ='Look_Feel_Approval_Request'
        //RecordType.DeveloperName='Look_Feel_Approval_Request'
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test scheduled';
        objStepType1.Code__c = 'VERIFICATION_BY DIFC_FIT_OUT_TEAM';
        objStepType1.Step_RecordType_API_Name__c = 'VERIFICATION_BY DIFC_FIT_OUT_TEAM';
        objStepType1.Summary__c = 'VERIFICATION_BY DIFC_FIT_OUT_TEAM';
        objStepType1.Code__c='VERIFICATION_BY DIFC_FIT_OUT_TEAM';
        insert objStepType1;
        
        Step_Price_Item_Configuration__c objPriceItem = new Step_Price_Item_Configuration__c();
        objPriceItem.Name='Test';
        objPriceItem.Pricing_Line__c = 'Test Pricing';
        objPriceItem.Product__c = 'Test product';
        objPriceItem.status__c = 'Test';
        insert objPriceItem;
        
        SR_Steps__c srstep = new SR_Steps__c();
        srstep.Step_Template__c = objStepType1.id;
        srstep.Step_RecordType_API_Name__c = testSrTemplate.id;
        srstep.SR_Template__c =testSrTemplate.id;
        insert srstep;
        SR_Steps__c srStepCreate = [Select id,Step_RecordType_API_Name__c,Name,Step_No__c,Start_Status__c,Step_Template__c,ownerid,Step_Template_Code__c,SR_Template__r.SR_RecordType_API_Name__c from SR_Steps__c where id=:srstep.id];
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        Contact cont = new Contact(lastName ='TestMePortal', AccountId=objAccount.id);
        insert cont;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;
        
        SR.Type_of_Request__c = 'Tenant Authorized Representative';   
        
        insert SR;
        
        SR_Status__c srStatus = new SR_Status__c(Name = 'Submitted',Code__c = 'SUBMITTED'); 
        insert srStatus;
        Service_Request__c SR2 = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Look_Feel_Approval_Request');
        SR.Customer__c = objAccount.Id;
        //SR.SR_Group__c='Fit-Out & Events';
        SR2.Linked_SR__c = SR.id; 
        SR2.External_SR_Status__c = srStatus.Id;
        SR2.Type_of_Request__c = 'Look & Feel related to a fit out project';   
        
        insert SR2; 
        //system.assertEquals(null,SR2);
        
         list<Amendment__c> amendlist = new list<Amendment__c> ();
          Amendment__c amnd1 = new Amendment__c (RecordTypeId=mapRecordType.get('Fit_Out_Contact'),ServiceRequest__c=SR.id,Amendment_Type__c='Tenant Authorized Representative');
          amendlist.add(amnd1);
          /*
          Amendment__c amnd2 = new Amendment__c (RecordTypeId=mapRecordType.get('Data_Center_Requestor'),ServiceRequest__c=lstSR[1].id);
          amendlist.add(amnd2);
          Amendment__c amnd3 = new Amendment__c (RecordTypeId=mapRecordType.get('Tool'),ServiceRequest__c=lstSR[0].id);
          amendlist.add(amnd3);*/
  
          insert amendlist;
        
        Test.startTest();
        Step__c objStep = new Step__c();
        objStep.SR__c = SR.Id;
        objStep.Step_No__c = 1.0;
        objStep.step_notes__c = 'abc';
        objStep.SR_Step__c=srstep.id;
        //objStep.SR_Group__c='Fit-Out & Events';
        insert objStep;  
        
        Step__c objStepD = new Step__c();
        objStepD.SR__c = SR.Id;
        objStepD.Step_No__c = 1.0;
        objStepD.step_notes__c = 'abc';
        objStepD.SR_Step__c=srstep.id;
        insert objStepD;  
        
        SR_DOC__c srdoc= new SR_DOC__c();
        srdoc.Service_Request__c=SR.id;
        srdoc.Step__c=objStep.id; 
        srdoc.Sys_IsGenerated_Doc__c = true;
        srdoc.Customer__c =objAccount.Id;
        
        SR_DOC__c srdocParent= new SR_DOC__c();
        srdocParent.Service_Request__c=SR1.id;
        srdocParent.Step__c=objStep.id; 
        srdocParent.Sys_IsGenerated_Doc__c = true;
        srdocParent.Customer__c =objAccount.Id;
        srdocParent.name ='Copy of Security deposit cheque';
        insert srdocParent;
                                                             
        Date endDate= system.today()+4;
        Datetime SD= system.now();
        
        
        Service_Request__c RevSR = new Service_Request__c();
        RevSR.RecordTypeId = mapRecordType.get('Revision_of_Fit_Out_Request');
        RevSR.Customer__c = objAccount.Id;
        RevSR.Linked_SR__c=SR.id; 
        RevSr.Type_of_Request__c = 'Major modification';
        RevSR.User_Form_Action__c='First Fix Inspection planning step';      
        insert RevSR;
        
        Step__c objStep1 = new Step__c();
        objStep1.SR__c = RevSR.Id;
        objStep1.Step_No__c = 1.0;
        objStep1.step_notes__c = 'abc';
        objStep1.SR_Step__c=srstep.id;
        objStep1.Sr_Type__c = RevSR.Id;
        insert objStep1; 
        
        CC_cls_FitOutandEventCustomCode.optionalDocumentUploadCheck(objStep);
        CC_cls_FitOutandEventCustomCode.declineRelaedLookAndFeelSteps(objStep);
        CC_cls_FitOutandEventCustomCode.validateChequeAndReceiptNo(objStep);
        CC_cls_FitOutandEventCustomCode.makePortalUserInactive(objStep);
        CC_cls_FitOutandEventCustomCode.updatePortalUserDetails(objStep);
        CC_cls_FitOutandEventCustomCode.lookAndFeelIsRequired(objStep);
        CC_cls_FitOutandEventCustomCode.checkIfStepAlreadyExist(objStep);
        CC_cls_FitOutandEventCustomCode.copySecurityDepositChequeDocAndReceipt(new List<id> {objStep.SR__c});
        CC_cls_FitOutandEventCustomCode.updateOpportunitySecurityApproval(objStep);
        CC_cls_FitOutandEventCustomCode.updatedParentSrStatus(objStep);
        CC_cls_FitOutandEventCustomCode.deleteDuplicateStep(objStep.id);
        CC_cls_FitOutandEventCustomCode.associateUserAndPortalUserAccesRequest(new Service_Request__c(id=objStep.SR__c) , new User(id=UserInfo.getuserId()));
        CC_cls_FitOutandEventCustomCode.testCoverage();
        CC_cls_FitOutandEventCustomCode.lookAndFeelIsRequiredAtStep(objStep);
        CC_cls_FitOutandEventCustomCode.customerFeedbackIsRequired(objStep);
        CC_cls_FitOutandEventCustomCode.provideStepComments(objStep);
       // CC_cls_FitOutandEventCustomCode.createAssociatedPriceItem(objStep);
        CC_cls_FitOutandEventCustomCode.updateMilestonePenaltyRecord(objStep);
        CC_cls_FitOutandEventCustomCode.deleteEncashmentStep(objStep);
        Test.stopTest();
    }
}