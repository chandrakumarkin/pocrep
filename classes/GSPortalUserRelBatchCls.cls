/*****************************************************************************************************************************
	--------------------------------------------------------------------------------------------------------------------------
	Modification History
	--------------------------------------------------------------------------------------------------------------------------
	V.No    Date        Updated By    Description
	--------------------------------------------------------------------------------------------------------------------------             
	V1.1    27/Feb/2016 Shabbir       Added field Use_ZGS1_Auth_Group__c in contact soql because of exception generating in test class TestRelationshipCreationTrg
	V1.2    15/Jun/2016 Kaavya        Commenting the body of the methods as the class was created for on time use
*****************************************************************************************************************************/
global class GSPortalUserRelBatchCls implements Database.Batchable<sObject>,Database.AllowsCallouts{
        
    global list<Relationship__c> start(Database.BatchableContext BC ){
        
        list<Relationship__c> lstR = new list<Relationship__c>();
        /*
        for(Relationship__c obj : [select Id,Partner_1__c,Partner_2__c,Object_Contact__c,Subject_Account__c from Relationship__c where Push_to_SAP__c = true AND Sys_Test__c = true]){
        	lstR.add(obj);
        }*/
        return lstR;
    }
    
    global void execute(Database.BatchableContext BC, list<Relationship__c> lstR){
    	/*
		list<string> lstRelationShipIds = new list<string>();   
        map<string,string> mapContactIds = new map<string,string>();
        for(Relationship__c obj : lstR){
			if(obj.Object_Contact__c != null){
				mapContactIds.put(obj.Object_Contact__c,obj.Partner_1__c);
			}
			lstRelationShipIds.add(obj.Id);
        }
        
        list<SAPCRMWebService.ZSFS_DATA> items = new list<SAPCRMWebService.ZSFS_DATA>();
		SAPCRMWebService.ZSFS_DATA item;
        //v1.1 modified soql to add Use_ZSF1_Auth_Group__c field in last
		map<Id,Contact> mapContacts = new map<Id,Contact>([select Id,FirstName,LastName,AccountId,Account.BP_No__c,Middle_Names__c,Phone,Email,Salutation,Birthdate,RELIGION__c,Gender__c,
									ADDRESS_LINE1__c,ADDRESS2_LINE2__c,CITY1__c,Emirate__c,Qualification__c,Gross_Monthly_Salary__c,Mothers_Name__c,CurrencyISOCode,
									Occupation__c,Country__c,Nationality__c,Marital_Status__c,Mother_Full_Name__c,Use_ZGS1_Auth_Group__c
									from Contact where Id IN : mapContactIds.keySet()]);
		
		String Month = string.valueOf(system.today().month()).length()==1?'0'+system.today().month():''+system.today().month();
		String Day = string.valueOf(system.today().day()).length()==1?'0'+system.today().day():''+system.today().day();
		String Hours = string.valueOf(system.now().hour()).length()==1?'0'+system.now().hour():''+system.now().hour();
		String Minitues = string.valueOf(system.now().minute()).length()==1?'0'+system.now().minute():''+system.now().minute();
		String Seconds = string.valueOf(system.now().second()).length()==1?'0'+system.now().second():''+system.now().second();
		
		map<string,string> mapNationalityCodes = new map<string,string>();
		for(string Country : Country_Codes__c.getAll().keySet()){
			if(Country_Codes__c.getAll().get(Country).Nationality__c != null){
				mapNationalityCodes.put(Country_Codes__c.getAll().get(Country).Nationality__c,Country_Codes__c.getAll().get(Country).Code__c);
			}
		}
		
		for(Contact objContact : mapContacts.values()){
			item = new SAPCRMWebService.ZSFS_DATA();
			item = SAPWebServiceDetails.ProcessContact(objContact,mapContactIds,mapNationalityCodes);
			items.add(item);
		}
		
		SAPCRMWebService.ZSFS_DATA_IN objZSFSDataIn = new SAPCRMWebService.ZSFS_DATA_IN();
		objZSFSDataIn.item = items;
		SAPCRMWebService.sf_data objSFData = new SAPCRMWebService.sf_data();
		objSFData.timeout_x = 120000;
		objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
		objSFData.inputHttpHeaders_x= new Map<String, String>();
		Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c);
		String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
		objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
		SAPCRMWebService.ZSF_IN_LOGT response;
		if(test.isRunningTest() == false)
			response = objSFData.Z_SF_INBOUND_DATA(objZSFSDataIn);
		else{
			response = new SAPCRMWebService.ZSF_IN_LOGT();
			list<SAPCRMWebService.ZSF_IN_LOG> lstItemsTemp = new list<SAPCRMWebService.ZSF_IN_LOG>();
		    SAPCRMWebService.ZSF_IN_LOG objItem =  new SAPCRMWebService.ZSF_IN_LOG();
		    objItem.ACTION = 'P';
		    objItem.SFREFNO = mapContacts.values()[0].Id;
		    objItem.SFGUID = mapContacts.values()[0].Id;
		    objItem.PARTNER1 = '001234';
		    objItem.PARTNER2 = '001239';
		    objItem.MSG = 'Partner Created Successfully RC';
		    //objItem.STATUS = 'S';
		    lstItemsTemp.add(objItem);
		    response.item = lstItemsTemp;
		}
		if(response != null && response.item != null){
			map<string,Contact> mapContactsToUpdate = new map<string,Contact>();
			map<string,string> mapRelationShipPartners = new map<string,string>();
			list<Log__c> lstLogs = new list<Log__c>();
			Contact objContact;
			for(SAPCRMWebService.ZSF_IN_LOG objRes : response.item){
				if(objRes.SFGUID != null && objRes.SFGUID.indexOf('003') == 0){
					objContact = new Contact(Id=objRes.SFGUID);
					if(objRes.PARTNER2 != null && objRes.PARTNER2 != ''){
						objContact.BP_No__c = objRes.PARTNER2;
						objContact.Is_Error__c = false;
						mapRelationShipPartners.put(objRes.SFGUID,objRes.PARTNER2);
					}
					mapContactsToUpdate.put(objContact.Id,objContact);
				}
			}
			SAPWebServiceDetails.RelationshipResult objRelationshipResult = new SAPWebServiceDetails.RelationshipResult();
			if(test.isRunningTest() == false) 
				objRelationshipResult = SAPWebServiceDetails.CreateRelationInSAP(lstRelationShipIds,mapRelationShipPartners);
			else{
				//objRelationshipResult
				list<Relationship__c> lst = new list<Relationship__c>();
				for(string i : lstRelationShipIds)
					lst.add(new Relationship__c(Id=i));
				objRelationshipResult.Relationships = lst;
				objRelationshipResult.Logs = new list<Log__c>();
				objRelationshipResult.Message = 'Test Class';
			}
			if(!mapContactsToUpdate.isEmpty())
				update mapContactsToUpdate.values();
			if(objRelationshipResult != null){
				if(objRelationshipResult.Relationships != null){
					for(Relationship__c obj : objRelationshipResult.Relationships)
						obj.Sys_Test__c = false;
					update objRelationshipResult.Relationships;
				}	
				if(objRelationshipResult.Logs != null)
					lstLogs.addAll(objRelationshipResult.Logs);//insert objRelationshipResult.Logs;
				system.debug('Exception is : '+objRelationshipResult.Message);							
				if(objRelationshipResult.Message != 'Success'){
					Log__c objLog = new Log__c();
					objLog.Description__c = objRelationshipResult.Message;
					objLog.Type__c = 'Webservice callout for CRM Relationship Creation';
					//insert objLog;
					lstLogs.add(objLog);
				}
			}
			if(!lstLogs.isEmpty())
				insert lstLogs;
		}
		*/
    }
    global void finish(Database.BatchableContext BC){}
}