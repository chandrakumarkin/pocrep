/*********************************************************************************************************************
*  Name     : HexaBPM_SecurityStepUpdateBatchschedule
*  Author   : Durga Kandula
*  Purpose  : This class used to schedule HexaBPM_SecurityStepStatusUpdateBatch class
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
 V1.0         Durga
**/

global class OB_SecurityStepUpdateBatchschedule implements schedulable{
    global void execute(schedulablecontext sc){
        OB_SecurityStepStatusUpdateBatch sc1 = new OB_SecurityStepStatusUpdateBatch(); 
        database.executebatch(sc1,1);
    }
}