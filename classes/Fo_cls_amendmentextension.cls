/******************************************************************************************
 *  Author   	: Claude Manahan
 *  Company  	: NSI DMCC 
 *  Date     	: 2016-11-07
 *  Description : This class handles any new conditions for amendments override page            
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By        Description
 ----------------------------------------------------------------------------------------            
 V1.0   11-07-2016      Claude            Created
 V1.1	11-07-2016		Claude			  Added exemption for contractor insurances (#3612)
 *******************************************************************************************/
public without sharing class Fo_cls_amendmentextension {
	
	/**
	 * Tells if a contractor insurance 
	 * can be edited
	 */
	public Boolean isContractorInsuranceEditable	{get; set;}
	
	public Fo_cls_amendmentextension(Apexpages.StandardController std){
		
		Amendment__c amnd = (Amendment__c)std.getRecord();
		
		String srId = [SELECT ServiceRequest__c FROM Amendment__c WHERE Id = :amnd.Id].ServiceRequest__c;
		
		List<Step__c> requiredSteps = [SELECT Status__r.Name, SR__R.Type_Of_Request__c FROM Step__c WHERE SR__c =:srId AND step_name__c = 'Upload Detailed Design Documents' order by createddate DESC];
		
		isContractorInsuranceEditable = amnd.Record_Type_Name__c.equals('Contractor_Insurance') && 
									(requiredSteps.isEmpty() || 
										(requiredSteps[0].Status__r.Name.equals('Pending') &&  
										( requiredSteps[0].SR__R.Type_Of_Request__c.equals('Type A') || 
										requiredSteps[0].SR__R.Type_Of_Request__c.equals('Type D Major') || 
										requiredSteps[0].SR__R.Type_Of_Request__c.equals('Type D Minor') ) ));
	}
    
}