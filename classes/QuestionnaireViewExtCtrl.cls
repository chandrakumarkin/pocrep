/*
Created By  : Suchita - DIFC on 14 Dec 2020 
Description : This Class is extension class for Questionnaire visualforce page
--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No        Date                Updated By          Description
v1.0      14 Dec 2020          Suchita Sharma,      Initial Version

----------------------------------------------------------------------------------------
*/
public class QuestionnaireViewExtCtrl {
    public Questionnaire__c QuestionnaireObj {get;set;}
    public Id parentId {get;set;}
    public boolean IsValid {get;set;}
    
    public boolean ShowOtherSectionFlag {get;set;}
    public boolean ShowOtherSectionFlag1 {get;set;}
    public List<QuestionnaireChild__c> questionnaireChildList {get; set;}
    public List<QuestionnaireChild__c> questionnaireChildList2 {get; set;}
    public List<QuestionnaireChild__c> questionnaireChildList3 {get; set;}
    public List<QuestionnaireChild__c> questionnaireChildList4 {get; set;}
    public List<QuestionnaireChild__c> questionnaireChildList5 {get; set;}
    
    
    public List<QuestionnaireChild__c> lobUAEQCList {get; set;}
    public List<QuestionnaireChild__c> lobMEQCList {get; set;}
    public List<QuestionnaireChild__c> lobGCCQCList {get; set;}
    public List<QuestionnaireChild__c> lobAfricaQCList {get; set;}
    public List<QuestionnaireChild__c> lobOTHERQCList {get; set;}

    String ContactId;
    public Boolean disabl {get;set;}
    
    
    public QuestionnaireViewExtCtrl (ApexPages.StandardController ctrl) {
        QuestionnaireObj = (Questionnaire__c) ctrl.getRecord();
        parentId = ctrl.getId();
        lobUAEQCList = new List<QuestionnaireChild__c>(); 
        lobMEQCList = new List<QuestionnaireChild__c>(); 
        lobGCCQCList = new List<QuestionnaireChild__c>(); 
        lobAfricaQCList = new List<QuestionnaireChild__c>(); 
        lobOTHERQCList = new List<QuestionnaireChild__c>();
        questionnaireChildList = new List<QuestionnaireChild__c>();
        questionnaireChildList2 = new List<QuestionnaireChild__c>();
        questionnaireChildList3 = new List<QuestionnaireChild__c>();
        questionnaireChildList4 = new List<QuestionnaireChild__c>();
        questionnaireChildList5 = new List<QuestionnaireChild__c>();
        
        for(QuestionnaireChild__c qc: [SELECT Id, Name, Questionnaire__c, Country__c, Line_of_insurance__c, Percentage_value_only_in_percentage__c,
                                             line_of_business__c, value_in_and_thousand__c, Other_business_line__c, Value__c, line_of_business_2020__c, 
                                             value_in_and_thousand_2020__c, LOB_Country__c, Jurisdiction__c, Type_Of_Operation__c, Year__c
                                             FROM QuestionnaireChild__c where Questionnaire__c=:parentId]){
             if(qc.LOB_Country__c == GlobalConstants.LOB_Country_UAE){
                lobUAEQCList.add(qc);                            
             }
             if(qc.LOB_Country__c == GlobalConstants.LOB_Country_ME_EXCLUDE_UAE){
                lobMEQCList.add(qc);                            
             }
             if(qc.LOB_Country__c == GlobalConstants.LOB_Country_GCC){
                lobGCCQCList.add(qc);                            
             }
             if(qc.LOB_Country__c == GlobalConstants.LOB_Country_AFRICA){
                lobAfricaQCList.add(qc);                            
             }
             if(qc.LOB_Country__c == GlobalConstants.LOB_Country_OTHER){
                lobOTHERQCList.add(qc);                            
             }
             if(qc.Type_Of_Operation__c == GlobalConstants.TYPEOPERATION_PREMIUMS_2019){
                questionnaireChildList2.add(qc);             
             }
             if(qc.Type_Of_Operation__c == GlobalConstants.TYPEOPERATION_OTHER_CLAIMS_PAID){
                questionnaireChildList3.add(qc);                                   
             }
             if(qc.Type_Of_Operation__c == GlobalConstants.TYPEOPERATION_OTHER_RISKS_PAID){
                questionnaireChildList5.add(qc);                                     
             }
             if(qc.Type_Of_Operation__c == GlobalConstants.TYPEOPERATION_PREMIUMS_2020){
                questionnaireChildList4.add(qc);                                     
             }
             if(qc.Country__c!=null){
                 questionnaireChildList.add(qc);                                    
             } 
        }
        
        ShowOtherSectionFlag = false;
        ShowOtherSectionFlag1 = false;
    }
    public void pageLoad() {
        IsValid = true;
        disabl = False;
        ContactId = ApexPages.currentPage().getParameters().get('cid');
        string emailId = ApexPages.currentPage().getParameters().get('eid');
        QuestionnaireObj.Email_Address__c = emailId;
        
        if (string.isblank(ContactId) == false && string.isblank(emailId) == false){
            List < Contact > ListContact = [SELECT Id, FirstName, LastName 
                                            FROM Contact 
                                            WHERE id =: ContactId];
            if (ListContact.size() < 1){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You are not a valid Contact'));
                IsValid = false;
            }else{
                QuestionnaireObj.Contact__c = ListContact[0].id;
                List <Questionnaire__c> ListQuestionnaire = [SELECT id, Name, Contact__c 
                                                             FROM Questionnaire__c 
                                                             WHERE Contact__c =: Contactid];
                if (ListQuestionnaire.size() > 0){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You have already submitted the Information'));
                    IsValid = false;
                }
            }
        }else if(string.isblank(emailId) == false){
            //Nothing
        }else{
            IsValid = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You are not a valid Contact'));
        }
    }
}