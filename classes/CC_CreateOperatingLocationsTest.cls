/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 05-04-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   05-04-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public class CC_CreateOperatingLocationsTest 
{
    public static testmethod void evaluateCustomCodeTest()
    {
       // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        Account hostingCompany = new Account(Name = 'hostingCompany');
        insert hostingCompany;
        
       
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(1, new List<string> {'In_Principle'});
        insert createSRTemplateList;
      
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
       
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(1, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].Id;
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        //insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        system.debug('######### insertNewSRs'+insertNewSRs);
        
        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1,new List<String>{'Low'},new List<Integer>{1}, new List<String>{'Low'});
        insert listCS;
        
       
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;
        
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(1, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insertNewSteps[0].HexaBPM__SR__c = insertNewSRs[0].Id;
        insert insertNewSteps;
        
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'DIFC';
        objBuilding.Building_No__c = '1234';
        objBuilding.Company_Code__c = '5200';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'DIFC Retail Outlet General';
        lstUnits.add(objUnit);
        insert lstUnits;
        
        
        Operating_Location__c objOL = new Operating_Location__c();
        objOL.Account__c = insertNewAccounts[0].Id;
        //objOL.Building_Name__c = 'Test';
        objOL.Unit__c = lstUnits[0].Id;
        objOL.Status__c = 'Active';
        objOL.Type__c = 'Leased';
        objOL.IsRegistered__c = true;
        insert objOL;
        
         //create amendments
        list<HexaBPM_Amendment__c> listAmendments = new list<HexaBPM_Amendment__c>() ;
        listAmendments = OB_TestDataFactory.createApplicationAmendment(1,listCS,insertNewSRs);
        listAmendments[0].ServiceRequest__c = insertNewSRs[0].Id;
        listAmendments[0].Amendment_Type__c = 'Operating Location';
        listAmendments[0].Status__c = 'Active';
        listAmendments[0].Unit__c =  lstUnits[0].Id; 
        insert listAmendments;
        
        
        test.startTest();
        CC_CreateOperatingLocations ccOprLoc = new CC_CreateOperatingLocations();
         List<HexaBPM__Step__c> lstStep = [SELECT id,
                                           		  HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c,
												  HexaBPM__SR__r.HexaBPM__Customer__c,
                                                  HexaBPM__SR__r.Corporate_Service_Provider_Name__c,
                                                  HexaBPM__SR__r.Registered_Agent__c,
                                            	  HexaBPM__SR__r.Fund_Administrator_Name__c,
                                                  HexaBPM__SR__r.Fund_Manager__c,
                                           		   HexaBPM__SR__r.Type_of_Entity__c
                                           ,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c
                                             FROM HexaBPM__Step__c 
                                            WHERE id=:insertNewSteps[0].Id];
        
        ccOprLoc.EvaluateCustomCode(insertNewSRs[0],lstStep[0]);
        
         CC_UpdateRegisterAddress ccOprLoc1 = new CC_UpdateRegisterAddress();
         ccOprLoc1.EvaluateCustomCode(insertNewSRs[0],lstStep[0]);
        
        
        test.stopTest();
       
    }
    
     public static testmethod void evaluateCustomCodeTest1()
    {
       // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        Account hostingCompany = new Account(Name = 'hostingCompany');
        insert hostingCompany;
        
       
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(1, new List<string> {'In_Principle'});
        insert createSRTemplateList;
      
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
       
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(1, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].Id;
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        //insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        system.debug('######### insertNewSRs'+insertNewSRs);
        
        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1,new List<String>{'Low'},new List<Integer>{1}, new List<String>{'Low'});
        insert listCS;
        
       
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;
        
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(1, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insertNewSteps[0].HexaBPM__SR__c = insertNewSRs[0].Id;
        insert insertNewSteps;
        
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'DIFC';
        objBuilding.Building_No__c = '1234';
        objBuilding.Company_Code__c = '5200';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'DIFC Retail Outlet General';
        lstUnits.add(objUnit);
        insert lstUnits;
        
        
        Operating_Location__c objOL = new Operating_Location__c();
        objOL.Account__c = insertNewAccounts[0].Id;
        //objOL.Building_Name__c = 'Test';
        objOL.Unit__c = lstUnits[0].Id;
        objOL.Status__c = 'Active';
        objOL.Type__c = 'Leased';
        objOL.IsRegistered__c = true;
        insert objOL;
        
        
        
        
        test.startTest();
        CC_CreateOperatingLocations ccOprLoc = new CC_CreateOperatingLocations();
         List<HexaBPM__Step__c> lstStep = [SELECT id,
                                           		  HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c,
												  HexaBPM__SR__r.HexaBPM__Customer__c,
                                                  HexaBPM__SR__r.Corporate_Service_Provider_Name__c,
                                                  HexaBPM__SR__r.Registered_Agent__c,
                                            	  HexaBPM__SR__r.Fund_Administrator_Name__c,
                                                  HexaBPM__SR__r.Fund_Manager__c,
                                           		  HexaBPM__SR__r.Type_of_Entity__c
                                           ,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c
                                             FROM HexaBPM__Step__c 
                                            WHERE id=:insertNewSteps[0].Id];
        
        ccOprLoc.EvaluateCustomCode(insertNewSRs[0],lstStep[0]);

        CC_UpdateRegisterAddress ccOprLoc1 = new CC_UpdateRegisterAddress();
        ccOprLoc1.EvaluateCustomCode(insertNewSRs[0],lstStep[0]);
        
        
        test.stopTest();
       
    }


}