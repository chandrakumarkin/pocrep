/******************************************************************************************************
*  Author   : Kumar Utkarsh
*  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created  * This class contains logic of Auth Fee. Payment and Resubmit Logic.
V1.1    27-Jun-2021  Veera           Pass the transaction nUmber to SAP.

*******************************************************************************************************/
public class WebServGDRFAAuthFeePaymentClass {
    
    public class ResponseBasicAuth
    {
        public string access_token{get;set;}
        public string token_type{get;set;}
    }
    
    public class GetfeesClass{
        public String applicationId;
        public List<Fees> fees;
    }
    
    public class Fees {
        public String feeTypeId;
        public Double amount;
        public String nameEn;
        public String nameAr;
        public Boolean isMandatory;
        public Boolean isParent;
        public Boolean isChild;
    }
    
    public class Payment {
        public String applicationId;
        public String batchId;
        public Date PaymentDate;
    }
    
    public class Translate{
        public String TranslatedText{get;set;}
    }
    
    public class ErrorClass {
        public List<ErrorMsg> errorMsg;
        public String errorCode;
        public String errorType;
    }
    public class ErrorMsg {
        public String messageEn;
        public String messageAr;
    }
    
    public class AppStatusCheck {
        public String ApplicationNumber;
        public String StatesCode;
        public String PaidOn;
        public String RefernceNumber;
        public String TransactionId;
        public String ApplicantName;
        public String CurrentStatus;
        public String ServiceName;
        public String FileNo;
        public Integer FileType;
        public Boolean IsSuccess;
    }
    
    public class FileStatus {
        public String applicantName;
        public String fileNumber;
        public Date expiryDate;
        public String fileType;
        public String visaTypeId;
        public String permitOrResStatusId;
        public String firstName;
        public Boolean IsSuccess;
        public Date IssueDate;
        public String errorCode;
    }
    
    public class PrintVisa { 
        public String fileName;
        public String fileType;
        public String reportContentAsBase64String;      
    }
    
    public class getRequiredDoc{
        public List<DocumentsList> DocumentsList;
    }
    
    public class DocumentsList {
        public String documentTypeId;
        public Boolean IsMandatory;
        public String MimeType;
        public String documentNameEn;
        public String documentNameAr;
        public String Reason;
        public Boolean IsNew;
        public Boolean IsToBeReplaced;
        public String document;
        public String FileName;
    }
    
    public class ResubmitDocuments {
        public String applicationId;
        public List<uploadDocuments> DocumentsUpload;
    }
    
    public class uploadDocuments {
        public String documentTypeId;
        public Boolean IsMandatory;
        public String documentNameEn;
        public String documentNameAr;
    }
    
    public class ReUploadDoc{
        public Boolean Success{get;set;}
    }
    
    public class ReSubmitDoc{
        public String isValid{get;set;}
    }
    
    public class ReceiptPayment{
        public String ReportBinary{get;set;}
    }
    
    public static ResponseBasicAuth GDRFAAuth()
    {
        ResponseBasicAuth ObjResponseAuth=new ResponseBasicAuth();
        //Http http = new Http();
        HttpRequest AuthRequest = new HttpRequest();
        AuthRequest.setEndpoint(System.label.GDRFA_Login);
        AuthRequest.setMethod('POST');
        string Body='';
        String username = System.label.GDRFA_Username;
        String password = System.label.GDRFA_Password;
        Blob headerValue = Blob.valueOf(username+':'+password);
        AuthRequest.setHeader('Content-Type','application/json');
        AuthRequest.setHeader('apikey',System.label.GDRFA_API_Key);        
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        AuthRequest.setHeader('Authorization', authorizationHeader);  
        AuthRequest.setTimeout(120000);
        JSONGenerator jsonObj = JSON.createGenerator(true);
        jsonObj.writeStartObject();
        jsonObj.writeStringField('loginType', 'establishment'); 
        jsonObj.writeStringField('username', System.label.GDRFA_Login_Username);
        jsonObj.writeStringField('password', System.label.GDRFA_Login_Password);
        jsonObj.writeStringField('grant_type', 'password');
        jsonObj.writeStringField('scope', 'webapiscope acctokenref profile email');
        jsonObj.writeEndObject();
        system.debug('AuthRequest ' +AuthRequest);
        system.debug('getBody' +AuthRequest.getBody());
        String finalJSON = jsonObj.getAsString();
        System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
        AuthRequest.setBody(jsonObj.getAsString());
        
        if(!Test.isRunningTest())
        {
            System.HttpResponse response = new System.Http().send(AuthRequest);
            system.debug('Response ' +response.getBody()); 
            map<String, String> values = (map<String, String>) JSON.deserialize(response.getBody(), map<String, String>.class);
            ObjResponseAuth.access_token = values.get('access_token');
            ObjResponseAuth.token_type=values.get('token_type');
        }
        else
        {
            ObjResponseAuth.access_token='Test';
            ObjResponseAuth.token_type='Test1';
        }
        return ObjResponseAuth;   
    }
    
    public static void getApplicationFeeAPI(String ApplicationId, String access_token, String token_type, String SrId, Step__c SrStep, String StatusReference)
    {
        String finalGetFeeJson;
        Double TotalAmount = 0;
        Double PrAmount = 0;
        //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
        Set <String> feeTypeIdSet = new Set<String>();
        Set <String> feeNameEnSet = new Set<String>();
        map<String, Double> mapMaterialPrice = new map<String, Double>();
        JSONGenerator getFeeObj = JSON.createGenerator(true);
        getFeeObj.writeStartObject();
        getFeeObj.writeStringField('applicationId', ApplicationId);
        getFeeObj.writeBooleanField('validate', false); 
        getFeeObj.writeEndObject();
        finalGetFeeJson = getFeeObj.getAsString();
        System.debug('DEBUG Full Request finalGetFeeJson : ' +(getFeeObj.getAsString()));
        HttpRequest requestGetFee = new HttpRequest();
        requestGetFee.setEndPoint(System.label.GDRFA_Get_Fee);        
        requestGetFee.setBody(getFeeObj.getAsString());
        requestGetFee.setTimeout(120000);
        requestGetFee.setHeader('apikey', System.label.GDRFA_API_Key);
        requestGetFee.setHeader('Content-Type', 'application/json');
        requestGetFee.setMethod('POST');
        requestGetFee.setHeader('Authorization',token_type+' '+access_token);
        HttpResponse responseGetFeeDoc = new HTTP().send(requestGetFee);
        System.debug('responseGetFeeDoc:'+responseGetFeeDoc.toString());
        System.debug('STATUS:'+responseGetFeeDoc.getStatus());
        System.debug('STATUS_CODE:'+responseGetFeeDoc.getStatusCode());
        System.debug('strJSONresponseGetFeeDoc:'+responseGetFeeDoc.getBody());
        if (responseGetFeeDoc.getStatusCode() == 200)
        {
            GetfeesClass feeClass = (GetfeesClass) System.JSON.deserialize(responseGetFeeDoc.getBody(), GetfeesClass.class);
            System.debug('applicationId:'+feeClass.applicationId);
            System.debug('fees:'+feeClass.fees);
            if(feeClass.fees != null){
                For(Fees feeDetails : feeClass.fees){
                    feeTypeIdSet.add(feeDetails.feeTypeId);
                    System.debug(feeDetails.amount);
                    if(feeDetails.amount != null){
                        TotalAmount = TotalAmount + feeDetails.amount;
                    }
                    feeNameEnSet.add(feeDetails.nameEn);
                }
            }
            System.debug('TotalAmount:'+TotalAmount);
            /*  if(TotalAmount != null){
getPayment(ApplicationId, access_token, token_type, SrId, SrStep);
}  */ 
        }
        else {
            ErrorClass responseGetFeeError = (ErrorClass) System.JSON.deserialize(responseGetFeeDoc.getBody(), ErrorClass.class);Log__c objLog = new Log__c();objLog.Type__c = 'Get Fee Error '; objLog.Description__c = getFeeObj.getAsString() +'Response: '+ responseGetFeeDoc.getBody(); objLog.SR_Step__c = SrStep.Id;
            insert objLog;
            if(responseGetFeeError.errorMsg[0].messageEn != null){
                SrStep.Step_Notes__c = responseGetFeeError.errorMsg[0].messageEn;
            }
            //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
            SrStep.Status__c = StatusReference;
            SrStep.DNRD_Receipt_No__c = ApplicationId;
            SrStep.Step_Notes__c = 
                SrStep.OwnerId = UserInfo.getUserId();
            try{
                Update SrStep;
            }catch(Exception ex) {
                insert LogDetails.CreateLog(null, 'On Get fee error', 'SR Id is ' + SrStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
            }
            
        } 
        if(TotalAmount != null){
            
            for(GDRFA_SAP_Pricing__mdt gdrfaPriceMdt: [Select MasterLabel, Net_Price__c, Release_Material_Description__c from GDRFA_SAP_Pricing__mdt]){
                mapMaterialPrice.put(gdrfaPriceMdt.MasterLabel+' '+gdrfaPriceMdt.Release_Material_Description__c, gdrfaPriceMdt.Net_Price__c);
            }
            for(SR_Price_Item__c plMaterialCode: [SELECT Pricing_Line__r.Material_Code__c FROM SR_Price_Item__c WHERE ServiceRequest__c=: SrId AND Pricing_Line__r.Material_Code__c != null]){
                if(mapMaterialPrice.get(plMaterialCode.Pricing_Line__r.Material_Code__c+' '+SrStep.Step_Name__c) != null){
                    PrAmount += mapMaterialPrice.get(plMaterialCode.Pricing_Line__r.Material_Code__c+' '+SrStep.Step_Name__c);
                    System.debug('PrAmount:'+PrAmount);
                }
            }
            
            if(TotalAmount <= PrAmount){
                Payment paymentClass = getPayment(ApplicationId, access_token, token_type, SrId, SrStep, StatusReference);
            }else{
                //Payment paymentClass = getPayment(ApplicationId, access_token, token_type, SrId, SrStep);
                Log__c objLog = new Log__c();objLog.Type__c = 'GDRFA Payment Mismatch ' + 'TotalAmount: '+ TotalAmount+' is not equal to Pricing Line Net Price: '+PrAmount; objLog.Description__c = getFeeObj.getAsString() +'Response: '+ responseGetFeeDoc.getBody();objLog.SR_Step__c = SrStep.Id;
                insert objLog; 
                //Status__c StatusReferenceReview = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
                //Status__c StatusReferenceClosed = [SELECT Id FROM Status__c WHERE Code__c = 'Closed'];
                SrStep.Status__c = StatusReference;
                SrStep.DNRD_Receipt_No__c = ApplicationId;
                SrStep.Step_Notes__c = 'GDRFA Payment Mismatch ' + 'TotalAmount: '+ TotalAmount+' is not equal to Pricing Line Net Price: '+PrAmount;
                /*If(paymentClass.batchId != null){
SrStep.Transaction_Number__c = paymentClass.batchId;
}
If(paymentClass.paymentDate != null){
SrStep.Payment_Date__c = paymentClass.paymentDate;
}*/
                SrStep.OwnerId = UserInfo.getUserId();
                try{
                    Update SrStep;
                }catch(Exception ex) {
                    insert LogDetails.CreateLog(null, 'On Get Fee after Payment error', 'SR Id is ' + SrStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                }
                
            }
        }     
    }
    
    public static Payment getPayment(String ApplicationId, String access_token, String token_type, String SrId, Step__c SrStep, String StatusReference)
    {
        Payment paymentClass;
        String finalGetPaymentJson;
        JSONGenerator getPaymentObj = JSON.createGenerator(true);
        getPaymentObj.writeStartObject();
        getPaymentObj.writeStringField('applicationId', ApplicationId);
        getPaymentObj.writeEndObject();
        finalGetPaymentJson = getPaymentObj.getAsString();
        System.debug('DEBUG Full Request finalGetFeeJson : ' +getPaymentObj.getAsString());
        HttpRequest requestGetPayment = new HttpRequest();
        requestGetPayment.setEndPoint(System.label.GDRFA_Application_Payment);        
        requestGetPayment.setBody(getPaymentObj.getAsString());
        requestGetPayment.setTimeout(120000);
        requestGetPayment.setHeader('apikey', System.label.GDRFA_API_Key);
        requestGetPayment.setHeader('Content-Type', 'application/json');
        requestGetPayment.setMethod('POST');
        requestGetPayment.setHeader('Authorization',token_type+' '+access_token);
        HttpResponse responseGetPayment = new HTTP().send(requestGetPayment);
        System.debug('responseGetPayment:'+responseGetPayment.toString());
        System.debug('STATUS:'+responseGetPayment.getStatus());
        System.debug('STATUS_CODE:'+responseGetPayment.getStatusCode());
        System.debug('strJSONresponseGetPayment:'+responseGetPayment.getBody());
        if (responseGetPayment.getStatusCode() == 200)
        {
            PaymentClass = (Payment) System.JSON.deserialize(responseGetPayment.getBody(), Payment.class); 
            If(paymentClass.applicationId != null){
                SrStep.DNRD_Receipt_No__c = paymentClass.applicationId;
            }
            If(paymentClass.batchId != null){
                SrStep.Transaction_Number__c = paymentClass.batchId;
            }
            If(paymentClass.paymentDate != null){
                SrStep.Payment_Date__c = paymentClass.paymentDate;
            }
            Status__c StatusReferenceClosed = [SELECT Id FROM Status__c WHERE Code__c = 'Closed'];
            SrStep.Status__c = StatusReferenceClosed.Id;
            SrStep.OwnerId = UserInfo.getUserId();
            SrStep.Step_Notes__c = '';
            //List<String> lstStepIds = new List<String>();
            //lstStepIds.add(SrStep.Id);
            GsSapWebServiceDetails.IsClosed = true;
            
            string Result;
            //Result = GsSapWebServiceDetails.PushActivityToSAPNew(new list < string > { SrStep.Id});
            
            Result = GsSapWebServiceDetailsActPushController.PushActivityToSAPNew(new list < string > { SrStep.Id},paymentClass.applicationId,paymentClass.batchId); //V1.1
                    
                    
            System.debug('Result:-'+Result);
            try{
                Update SrStep;
            }catch(Exception ex) {
                insert LogDetails.CreateLog(null, 'Payment method', 'SR Id is ' + SrStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
            } 
            SR_Status__c SrStatus = [SELECT Id, Name FROM SR_Status__c WHERE Name = 'The application is under process with GDRFA'];
            Service_Request__c SR = [SELECT Id, Internal_Status_Name__c, Internal_SR_Status__c, External_SR_Status__c, External_Status_Name__c FROM Service_Request__c WHERE ID =:SrId];
            SR.External_SR_Status__c = SrStatus.Id;
            SR.Internal_SR_Status__c = SrStatus.Id;
            Update SR;
        }
        else {
            Log__c objLog = new Log__c();objLog.Type__c = 'Payment Error '+ApplicationId; objLog.Description__c = getPaymentObj.getAsString() +'Response: '+ responseGetPayment.getBody();objLog.SR_Step__c = SrStep.Id;
            insert objLog; 
            ErrorClass ErrorPaymentLog = (ErrorClass) System.JSON.deserialize(responseGetPayment.getBody(), ErrorClass.class);
            //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
            if(ErrorPaymentLog.errorMsg[0].messageEn != null){
                SrStep.Step_Notes__c = ErrorPaymentLog.errorMsg[0].messageEn;
            }
            SrStep.Status__c = StatusReference;
            SrStep.OwnerId = UserInfo.getUserId();
            SrStep.DNRD_Receipt_No__c = ApplicationId;
            try{
                Update SrStep;
            }catch(Exception ex) {
                insert LogDetails.CreateLog(null, 'Payment error ', 'SR Id is ' + SrStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
            }
            
        }
        return PaymentClass;
    }
    
    public static String getArabicTranslate(String Name, Step__c SrStep, String access_token, String token_type)
    {   
        System.debug('TranslateName'+Name);
        //Get authrization code 
        JSONGenerator jsonObj = JSON.createGenerator(true);
        jsonObj.writeStartObject();
        jsonObj.writeStringField('TextToTranslate', Name.replaceAll('[^a-zA-Z0-9\\s+]', ''));
        jsonObj.writeBooleanField('IsEnglishToArabic', true);
        jsonObj.writeEndObject();
        HttpRequest request = new HttpRequest();
        request.setEndPoint(System.label.GDRFA_Translate);        
        request.setBody(jsonObj.getAsString());
        request.setTimeout(120000);
        request.setHeader('apikey', System.label.GDRFA_API_Key);
        request.setHeader('Content-Type', 'application/json');
        request.setMethod('POST');
        request.setHeader('Authorization',token_type+' '+access_token);
        HttpResponse response = new HTTP().send(request);
        System.debug('responseStringresponse:'+response.toString());
        System.debug('STATUS:'+response.getStatus());
        System.debug('STATUS_CODE:'+response.getStatusCode());
        System.debug('strJSONresponse:'+response.getBody());
        request.setTimeout(101000);
        Translate translateClass;
        if (response.getStatusCode() == 200)
        {
            translateClass = (Translate) System.JSON.deserialize(response.getBody(), Translate.class);
            System.debug('translateClass.TranslatedText'+ translateClass.TranslatedText);
            return translateClass.TranslatedText;
        }
        else{
            return '';
        }
    }
    //Reupload Documents in case of Rejection or MoreDocumentsRequired 
    @InvocableMethod 
    public static void invokeRePushDocData(List<Id> StepIdLists)
    {
        System.debug('List<Id>'+StepIdLists);
        rePushDocData(String.valueOf(StepIdLists[0]));
    }
    
    @future(callout=true)
    public static void rePushDocData(String stepId){
        map<String, Status__c> mapStatusCode = new map<String, Status__c>();
        map<String, String> mapNewDocAttch = new  map<String, String>();
        map<String, String> mapDocIdDocName = new map<String, String>();
        for(Status__c obj :[SELECT Id, Code__c FROM Status__c WHERE Code__c IN('DNRD_GS_Review', 'CLOSED', 'Repush_to_GDRFA')]){
            mapStatusCode.put(obj.Code__c, obj); 
        }
        List<Log__c> ErrorObjLogList = new List<Log__c>();
        //List<Step__c> stepList = new List<Step__c>();
        Boolean isAllUploadSuccess = false;
        WebServGDRFAAuthFeePaymentClass.ResponseBasicAuth ObjResponseAuth = WebServGDRFAAuthFeePaymentClass.GDRFAAuth();
        string access_token= ObjResponseAuth.access_token;
        string token_type=ObjResponseAuth.token_type;
        Step__c step =  [SELECT Id, Status__c, SR__c, OwnerId, Step_Notes__c, Parent_Step__c, Dependent_On__c FROM Step__c WHERE Id =:stepId];
        Step__c stepIsTyped = [SELECT Id, DNRD_Receipt_No__c FROM Step__c WHERE Id =:step.Dependent_On__c AND DNRD_Receipt_No__c != null];
        //Step__c stepIsIssued = [SELECT Id, Name FROM Step__c WHERE Id =:step.Parent_Step__c];
        for(SR_Doc__c srDoc : [SELECT Id, Name, GDRFA_New_Document_Id__c, GDRFA_Attchment_Doc_Id__c, GDRFA_Parent_Document_Id__c FROM SR_Doc__c WHERE Service_Request__c =:step.SR__c AND GDRFA_New_Document_Id__c != null AND GDRFA_Attchment_Doc_Id__c!= null]){
            mapNewDocAttch.put(srDoc.GDRFA_Attchment_Doc_Id__c, srDoc.GDRFA_New_Document_Id__c);
            mapDocIdDocName.put(srDoc.GDRFA_Attchment_Doc_Id__c, srDoc.Name);
        }
        
        for (Attachment objAttch: [SELECT Id, Name, ContentType, Body from Attachment where Id IN:mapNewDocAttch.keySet()]){
            JSONGenerator docJsonObj = JSON.createGenerator(true);
            docJsonObj.writeStartObject();
            docJsonObj.writeFieldName('Documents');
            docJsonObj.writeStartObject();
            docJsonObj.writeObjectField('documentTypeId', mapNewDocAttch.get(objAttch.Id));
            docJsonObj.writeBlobField('document', objAttch.Body);    
            docJsonObj.writeBooleanField('IsMandatory', true); 
            docJsonObj.writeStringField('MineType', objAttch.ContentType);
            docJsonObj.writeStringField('documentNameEn', mapDocIdDocName.get(objAttch.Id)); 
            //docJsonObj.writeStringField('documentNameAr', mapSRDocIdGDRFAArabicName.get(objAttch.Id)); 
            docJsonObj.writeStringField('FileName', objAttch.Name); 
            docJsonObj.writeEndObject();
            if(stepIsTyped.DNRD_Receipt_No__c != null){
                docJsonObj.writeStringField('applicationId', stepIsTyped.DNRD_Receipt_No__c);
            }
            docJsonObj.writeEndObject();
            //finalDocJSON = docJsonObj.getAsString();
            System.debug('DEBUG Full Request finalJSON : ' +(docJsonObj.getAsString()));
            HttpRequest requestUploadDoc = new HttpRequest();
            requestUploadDoc.setEndPoint(System.label.GDRFA_Document_Upload_Endpoint);        
            requestUploadDoc.setBody(docJsonObj.getAsString());
            requestUploadDoc.setTimeout(120000);
            requestUploadDoc.setHeader('apikey', System.label.GDRFA_API_Key);
            requestUploadDoc.setHeader('Content-Type', 'application/json');
            requestUploadDoc.setMethod('POST');
            requestUploadDoc.setHeader('Authorization',token_type+' '+access_token);
            HttpResponse responseUploadDoc = new HTTP().send(requestUploadDoc);
            System.debug('responseUploadDoc:'+responseUploadDoc.toString());
            System.debug('STATUS:'+responseUploadDoc.getStatus());
            System.debug('STATUS_CODE:'+responseUploadDoc.getStatusCode());
            System.debug('strJSONresponseUploadDoc:'+responseUploadDoc.getBody());
            requestUploadDoc.setTimeout(101000);
            if (responseUploadDoc.getStatusCode() == 200)
            {
                WebServGDRFAAuthFeePaymentClass.ReUploadDoc docUpload = (WebServGDRFAAuthFeePaymentClass.ReUploadDoc) System.JSON.deserialize(responseUploadDoc.getBody(), WebServGDRFAAuthFeePaymentClass.ReUploadDoc.class);
                System.debug('IsSuccess:'+docUpload.Success);
                if(docUpload.Success != null && docUpload.Success){
                    isAllUploadSuccess = true;   
                }else{
                    isAllUploadSuccess = false;
                }   
            }
            else{
                //ErrorClass ErrorResubmitLog = (ErrorClass) System.JSON.deserialize(responseUploadDoc.getBody(), ErrorClass.class);
                Log__c ErrorobjLog = new Log__c();ErrorobjLog.Type__c = mapDocIdDocName.get(objAttch.Id)+'-'+objAttch.Id+'-Document Upload Error';ErrorobjLog.Description__c = 'Request: '+objAttch.Name +'Response: '+ responseUploadDoc.getBody();ErrorobjLog.SR_Step__c = step.Id;ErrorObjLogList.add(ErrorobjLog); 
                isAllUploadSuccess = false;
            }
        }
        
        //Resubmit the application
        if(isAllUploadSuccess){
            HttpRequest requestResubmit = new HttpRequest();
            String resubmitDocEndPoint = System.label.GDRFA_Resubmit_Document+'/'+stepIsTyped.DNRD_Receipt_No__c+'/resubmit';
            system.debug('resubmitDocEndPoint:'+resubmitDocEndPoint);
            requestResubmit.setEndPoint(resubmitDocEndPoint);
            requestResubmit.setTimeout(120000);
            requestResubmit.setHeader('apikey', System.label.GDRFA_API_Key);
            requestResubmit.setHeader('Content-Type', 'application/json');
            requestResubmit.setMethod('POST');
            requestResubmit.setHeader('Authorization',token_type+' '+access_token);
            requestResubmit.setHeader('Content-Length', '0');
            HttpResponse responseResubmit = new HTTP().send(requestResubmit);
            System.debug('responseStringresponse:'+responseResubmit.toString());
            System.debug('STATUS:'+responseResubmit.getStatus());
            System.debug('STATUS_CODE:'+responseResubmit.getStatusCode());
            System.debug('responseResubmit:'+responseResubmit.getBody());
            requestResubmit.setTimeout(101000);
            if (responseResubmit.getStatusCode() == 200)
            {
                WebServGDRFAAuthFeePaymentClass.ReSubmitDoc reSubmit = (WebServGDRFAAuthFeePaymentClass.ReSubmitDoc) System.JSON.deserialize(responseResubmit.getBody(), WebServGDRFAAuthFeePaymentClass.ReSubmitDoc.class);
                System.debug('IsValid:'+reSubmit.IsValid);
                if(reSubmit.IsValid != null){
                    if(reSubmit.IsValid == 'True'){
                        if(mapStatusCode.get('Repush_to_GDRFA').Id != null){
                            step.Status__c = mapStatusCode.get('Repush_to_GDRFA').Id;
                        }
                        step.OwnerId = UserInfo.getUserId();
                        step.Step_Notes__c = '';
                        try{
                            Update step; 
                        }catch(Exception ex) {
                            System.Debug(ex);
                        }
                    }else if(reSubmit.IsValid == 'False'){
                        if(mapStatusCode.get('DNRD_GS_Review').Id != null){
                            step.Status__c = mapStatusCode.get('DNRD_GS_Review').Id;
                        }
                        step.OwnerId = UserInfo.getUserId();
                        step.Step_Notes__c = 'Resubmit unsuccessful in GDRFA';
                        try{
                            Update step; 
                        }catch(Exception ex) {
                            insert LogDetails.CreateLog(null, 'Resubmit update failure ', 'SR Id is ' + step.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                        }
                    }
                }
                
            }
            else{
                WebServGDRFAAuthFeePaymentClass.ErrorClass responseResubmitError = (WebServGDRFAAuthFeePaymentClass.ErrorClass) System.JSON.deserialize(responseResubmit.getBody(), WebServGDRFAAuthFeePaymentClass.ErrorClass.class);
                Log__c ErrorobjLog = new Log__c();ErrorobjLog.Type__c = 'Resubmit Error'; ErrorobjLog.Description__c = 'Request: '+resubmitDocEndPoint +'Response: '+ responseResubmit.getBody();ErrorobjLog.SR_Step__c = step.Id;
                Insert ErrorobjLog; 
                if(mapStatusCode.get('DNRD_GS_Review').Id != null){
                    step.Status__c = mapStatusCode.get('DNRD_GS_Review').Id;
                }
                step.OwnerId = UserInfo.getUserId();
                if(responseResubmitError.errorMsg[0] != null){
                    step.Step_Notes__c  = responseResubmitError.errorMsg[0].messageEn;
                }
                try{
                    Update step; 
                }catch(Exception ex) {
                    insert LogDetails.CreateLog(null, 'Resubmit update failure error ', 'SR Id is ' + step.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                }
            } 
        }
        if(!ErrorObjLogList.isEmpty() && ErrorObjLogList != null){
            try {
                insert ErrorObjLogList;
            }
            catch(Exception ex) {
                System.Debug(ex);
            }
            isAllUploadSuccess = false;
            if(mapStatusCode.get('DNRD_GS_Review').Id != null){
                step.Status__c = mapStatusCode.get('DNRD_GS_Review').Id;
            }
            step.OwnerId = UserInfo.getUserId();
            step.Step_Notes__c = 'Document Upload Error';
            try{
                Update step; 
            }catch(Exception ex) {
                insert LogDetails.CreateLog(null, 'Document update failure ', 'SR Id is ' + step.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
            }
        }
    }
    
    public static map<String,map<String,String>> masterDataMap;
    public static string fetchGDRFALookUpId(String lookUpType, String SFValue)
    {
        //map<String,map<String,String>> masterDataMap;
        //system.debug('======fetchGDRFALookUpId====================');
        
        if(masterDataMap ==null)
        {
            masterDataMap = new map<String,map<String,String>>();
            for(GDRFA_Master_Data__c gdData: [SELECT id, Look_Up_Id__c, Lookup_Type__c, GDRFA_Name__c FROM GDRFA_Master_Data__c])
            {
                map<String,String> MapMainData=new map<String,String>();
                
                if(masterDataMap.containsKey(gdData.Lookup_Type__c.toUpperCase()))
                {
                    MapMainData=masterDataMap.get(gdData.Lookup_Type__c.toUpperCase());
                }
                /*system.debug('==gdData.GDRFA_Name__c.===>'+gdData.id); 
                system.debug('==gdData.GDRFA_Name__c.===>'+gdData.GDRFA_Name__c); 
                system.debug('==gdData.GDRFA_Name__c===>'+gdData.Look_Up_Id__c); */
                
                MapMainData.put(gdData.GDRFA_Name__c.toUpperCase(),gdData.Look_Up_Id__c);
                
                masterDataMap.put(gdData.Lookup_Type__c.toUpperCase(),MapMainData);
                
            }
        }
        
        
        //system.debug('==masterDataMap===>'+masterDataMap);  
        
        string GDRFAID='';
        if(masterDataMap.containsKey(lookUpType.toUpperCase()))
            
            if(masterDataMap.get(lookUpType.toUpperCase()).containsKey(SFValue.toUpperCase()))
            GDRFAID =masterDataMap.get(lookUpType.toUpperCase()).get(SFValue.toUpperCase());  
        
        //system.debug(lookUpType+'==>'+SFValue+'==GDRFAID==>'+GDRFAID);    
        return GDRFAID;
        
    }
    public static void fakeMethod(){
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; 
    }
    
    
}