/******************************************************************************************
 *  Name        : CLS_queueableRelationship 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2016-06-19
 *  Description : This class enqueues SAP Contact Relationships when resources become available
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   2016-06-19	 Claude		   Created
*******************************************************************************************/
global class CLS_queueableRelationship implements Queueable {
	
	global List<Relationship__c> newRelationShipContacts;
	
	global List<Account> acctToUpdate;
	
	global void execute(QueueableContext SC) {
   		
		System.debug('Relationships created');
		
		insert newRelationShipContacts;
		update acctToUpdate;
   	}
   	
   	public CLS_queueableRelationship(List<Relationship__c> newRelationShipContacts, List<Account> acctToUpdate){
   		this.newRelationShipContacts = newRelationShipContacts;
   		this.acctToUpdate = acctToUpdate;
   	}
    
}