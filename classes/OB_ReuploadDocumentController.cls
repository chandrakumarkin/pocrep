/******************************************************************************************
   *  Author         :   Jayanta
   *  Company        :   HexaBPM
   *  Date           :   19-Nov-2019
   *  Description    :   Apex Controller for Uploading Documents in LEX
********************************************************************************************/
public without sharing class OB_ReuploadDocumentController {
         
    /*** Method to fetch all the SR Docs related to the Service Request ***/ 
    @AuraEnabled
    public static List<HexaBPM__SR_Doc__c> getSRDocs(ID SRId) {
        list<HexaBPM__SR_Doc__c> SRDocs = new List<HexaBPM__SR_Doc__c>();
        for(HexaBPM__SR_Doc__c doc:[select id,Name,createddate,lastmodifieddate,
                      HexaBPM__Document_Description_External__c,
                      HexaBPM__Is_Not_Required__c,
                      HexaBPM__Document_Master__r.Name,
                      HexaBPM__Document_Master__r.Document_Description__c,
                      HexaBPM__Service_Request__c,
                      HexaBPM__Service_Request__r.HexaBPM__Internal_Status_Name__c,
                      HexaBPM__Service_Request__r.HexaBPM__External_Status_Name__c,
                      HexaBPM__Doc_ID__c,
                      HexaBPM__Customer_Comments__c,
                      HexaBPM__Comments__c,
                      HexaBPM__Document_Type__c,
                      HexaBPM__Rejection_Reason__c,
                      HexaBPM__Status__c ,
                      HexaBPM__Letter_Template_Id__c
                      from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRId and HexaBPM__Sys_IsGenerated_Doc__c=false AND HexaBPM__Status__c = 'Re-upload' order by LastmodifiedDate asc]){
            SRDocs.add(doc);
        }
        for(HexaBPM__SR_Doc__c doc:[select id,Name,createddate,lastmodifieddate,
                      HexaBPM__Document_Description_External__c,
                      HexaBPM__Is_Not_Required__c,
                      HexaBPM__Document_Master__r.Name,
                      HexaBPM__Document_Master__r.Document_Description__c,
                      HexaBPM__Service_Request__c,
                      HexaBPM__Service_Request__r.HexaBPM__Internal_Status_Name__c,
                      HexaBPM__Service_Request__r.HexaBPM__External_Status_Name__c,
                      HexaBPM__Doc_ID__c,
                      HexaBPM__Customer_Comments__c,
                      HexaBPM__Comments__c,
                      HexaBPM__Document_Type__c,
                      HexaBPM__Rejection_Reason__c,
                      HexaBPM__Status__c ,
                      HexaBPM__Letter_Template_Id__c
                      from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRId and HexaBPM__Sys_IsGenerated_Doc__c=false AND HexaBPM__Status__c = 'Uploaded' order by LastmodifiedDate asc]){
            SRDocs.add(doc);
        }
        
        /*
        SRDocs = [select id,Name,createddate,lastmodifieddate,
                  HexaBPM__Document_Description_External__c,
                  HexaBPM__Is_Not_Required__c,
                  HexaBPM__Document_Master__r.Name,
                  HexaBPM__Service_Request__c,
                  HexaBPM__Service_Request__r.HexaBPM__Internal_Status_Name__c,
                  HexaBPM__Service_Request__r.HexaBPM__External_Status_Name__c,
                  HexaBPM__Doc_ID__c,
                  HexaBPM__Customer_Comments__c,
                  HexaBPM__Comments__c,
                  HexaBPM__Document_Type__c,
                  HexaBPM__Rejection_Reason__c,
                  HexaBPM__Status__c ,
                  HexaBPM__Letter_Template_Id__c
                  from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRId and HexaBPM__Sys_IsGenerated_Doc__c=false order by HexaBPM__Status__c,LastmodifiedDate asc];
                  //AND HexaBPM__Status__c = 'Re-upload' 
        */
        system.debug('SRDocs==>'+SRDocs);
        return SRDocs; 
    }   
    
    /*** Method to fetch the Attachment ID to show in the document preview screen ***/                                      
    @AuraEnabled
    public static String getAttachmentURL(ID SRID,String AttachmentID) {
        String AttachmentURL = URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.FileDownload?file=';
        if(AttachmentID==''){
            /* Getting the first attachment Id when the page is loaded */
            List<HexaBPM__SR_Doc__c> SRDocList = [Select id,Name,HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRID order by createdDate DESC];
            if(!SRDocList.isEmpty())
                AttachmentURL+='&'+SRDocList[0].HexaBPM__Doc_ID__c; 
            if(SRDocList.size()==1)//In case there is just one SR Doc attachment, disabling the Next button
                AttachmentURL+='&DisableNext';
        }
        else
           AttachmentURL+='&'+AttachmentID;
       return AttachmentURL;
    }
                                       
    /*** Method to get the next Attachment ID when Next button is clicked in the document preview section***/                                      
    @AuraEnabled
    public static String getNextAttachment(String SRID, String AttachmentID){   
        Integer index = 0;
        String AttachmentURL = URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.FileDownload?file=';
        list<HexaBPM__SR_Doc__c> SRDocList = [Select id,Name,HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRID order by createdDate DESC];
        for(HexaBPM__SR_Doc__c ObjSRDoc : SRDocList){
            if(ObjSRDoc.HexaBPM__Doc_ID__c == AttachmentID)
                break;
            index++;            
        }
        if(!SRDocList.isEmpty()){
            if(SRDocList.size()==index+2){
                AttachmentURL +='&'+SRDocList[index+1].HexaBPM__Doc_ID__c+'&'+SRDocList[index+1].Name+'&LastAttachment';
            }else if(SRDocList.size() > index)
                AttachmentURL+='&'+SRDocList[index+1].HexaBPM__Doc_ID__c+'&'+SRDocList[index+1].Name;
        }
        return AttachmentURL;
    }
    
    /*** Method to get the previous Attachment ID when Previous button is clicked in the document preview section***/ 
    @AuraEnabled
    public static String getPreviousAttachment(String SRID, String AttachmentID){
        Integer index = 0;
        String AttachmentURL = URL.getSalesforceBaseUrl().toExternalForm()+'/servlet/servlet.FileDownload?file=';
        list<HexaBPM__SR_Doc__c> SRDocList = [Select id,Name,HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRID order by createdDate ASC];
        for(HexaBPM__SR_Doc__c ObjSRDoc : SRDocList){
             system.debug('ObjSRDoc.HexaBPM__Doc_ID__c=>'+ObjSRDoc.HexaBPM__Doc_ID__c+'-'+AttachmentID);
            if(ObjSRDoc.HexaBPM__Doc_ID__c == AttachmentID)
                break;
            index++;            
        }  
        system.debug('index==>'+index);
        if(!SRDocList.isEmpty()){
            if(SRDocList.size()==index+2){
                AttachmentURL +='&'+SRDocList[index+1].HexaBPM__Doc_ID__c+'&'+SRDocList[index+1].Name+'&FirstAttachment';
            }else
                AttachmentURL+='&'+SRDocList[index-1].HexaBPM__Doc_ID__c+'&'+SRDocList[index-1].Name;
        }
        return AttachmentURL;
    }
    
    /*** Method to save the Uploaded file under the selected SR Doc ***/                                     
    @AuraEnabled
    public static Id saveTheFile(ID SRId,Id parentId, string fileName, string base64Data, String contentType,String CustomerComments) { 
        //Attachment ObjAttachment;
        try{
            base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
 
            //Insert ContentVersion
            ContentVersion cVersion = new ContentVersion();
            cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
            cVersion.PathOnClient = fileName;//attach.Name;//File name with extention
            cVersion.Origin = 'H';//C-Content Origin. H-Chatter Origin.
            //cVersion.OwnerId = parentId; // attach.OwnerId;//Owner of the file
            cVersion.Title = fileName; //attach.Name;//Name of the file
            cVersion.VersionData = EncodingUtil.base64Decode(base64Data);//File content
            Insert cVersion;
             
            //After saved the Content Verison, get the ContentDocumentId
            Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:cVersion.Id].ContentDocumentId;
             
            //Insert ContentDocumentLink
            ContentDocumentLink cDocLink = new ContentDocumentLink();
            cDocLink.ContentDocumentId = conDocument;//Add ContentDocumentId
            cDocLink.LinkedEntityId = parentId; //attach.ParentId;//Add attachment parentId
            cDocLink.ShareType = 'I';//V - Viewer permission. C - Collaborator permission. I - Inferred permission.
            cDocLink.Visibility = 'AllUsers';//AllUsers, InternalUsers, SharedUsers
            Insert cDocLink;
            
            //system.debug('ObjAttachment==>'+ObjAttachment);
            HexaBPM__SR_Doc__c ObjSRDoc = new HexaBPM__SR_Doc__c(id=parentId);
            ObjSRDoc.HexaBPM__Customer_Comments__c=CustomerComments;
            ObjSRDoc.HexaBPM__Status__c='Uploaded';
			ObjSRDoc.HexaBPM__Doc_ID__c = conDocument;
            update ObjSRDoc;
            system.debug('ObjSRDoc=>'+ObjSRDoc);
            
            AutoCloseReuploadDocStep(SRId);
            
            return cDocLink.id;
        }catch(Exception e){
            system.debug('Exception IN saveTheFile()====>'+e.getMessage());
        }
        return null;
    }
 
    /*** Method to fetch the Status picklist values ***/                                      
    @AuraEnabled
    public static List<String> getSRDocStatus(){
        List<String> options = new List<String>();
        Schema.DescribeFieldResult fieldResult = HexaBPM__SR_Doc__c.HexaBPM__Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry f: ple) {
            options.add(f.getLabel());
        } 
        system.debug('options==>'+options);
        return options;
    }
    
    /*** Method to Update the SR Docs when Save is clicked on the List of SR Docs. Also auto closed the step in case of Documents re-uploaded.***/
    @AuraEnabled
    public static void UpdateSRDocs(String SRDocList,String SRId){
    
        /*
        HexaBPM__SR_Doc__c[] lstUploadableDocs = (HexaBPM__SR_Doc__c[])JSON.deserialize(SRDocList, List<HexaBPM__SR_Doc__c>.class);
        if(lstUploadableDocs!=null && lstUploadableDocs.size()>0){
            Savepoint svpoint = Database.setSavepoint();
            upsert lstUploadableDocs;
            
            //Auto Close Document Re-upload step when the status is set to document reuploaded            
            AutoCloseReuploadDocStep(SRId);
        }   
        */
        AutoCloseReuploadDocStep(SRId);
    }  
    
    /*** Generic Method to auto close the Reupload step once the status is set to Re-upload ***/                                     
    public static void AutoCloseReuploadDocStep(string SRId){
        try{
        	if(SRId!=null){
	            //Query all steps & check if there are any Document Re-upload step open
	            list<HexaBPM__Step__c> Step = [select id,HexaBPM__SR_Step__c,HexaBPM__Status__c from HexaBPM__Step__c 
	                                             where HexaBPM__Status__r.HexaBPM__Type__c!='End' AND HexaBPM__Status__r.HexaBPM__Reupload_Document__c = true AND
	                                             (HexaBPM__SR__c=:SRId OR HexaBPM__SR__r.HexaBPM__Parent_SR__c=:SRId)];
	            if(!Step.isEmpty()){
	                //Check is there are any documents are not yet uploaded or approved
	                Integer SRDocCount = 0;//[select count() from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRId AND (HexaBPM__Status__c!='Uploaded' AND HexaBPM__Status__c!='Approved')];
	                for(HexaBPM__SR_Doc__c objDoc:[select Id from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRId AND HexaBPM__Status__c='Re-upload' limit 1]){
	                    SRDocCount = SRDocCount + 1;
	                }
	                //If there is no such document with Re-upload or pending status, then close the step
	                if(SRDocCount==0){
	                    //Map<string,string> MapStatusValues = new Map<string,string>();
	                    map<string,HexaBPM__Step_Transition__c> MapTransition = new map<string,HexaBPM__Step_Transition__c>();
	                    for(HexaBPM__Step_Transition__c ObjTransition:[Select Id,HexaBPM__Transition__c,HexaBPM__SR_Status_Internal__c,HexaBPM__SR_Status_External__c,HexaBPM__Transition__r.HexaBPM__To__c,HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Code__c from HexaBPM__Step_Transition__c 
	                                                                        where HexaBPM__SR_Step__c=:Step[0].HexaBPM__SR_Step__c]){
	                                //MapStatusValues.put(ObjTransition.HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Code__c,ObjTransition.HexaBPM__Transition__r.HexaBPM__To__c);
	                                MapTransition.put(ObjTransition.HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Code__c,ObjTransition);
	                    }
	                    /*
	                    if(MapStatusValues.containsKey('DOCUMENT_RE_UPLOADED')){
	                        Step[0].HexaBPM__Status__c = MapStatusValues.get('DOCUMENT_RE_UPLOADED');
	                    }
	                    */
	                    if(MapTransition.get('DOCUMENT_RE_UPLOADED')!=null){
	                    	if(MapTransition.get('DOCUMENT_RE_UPLOADED').HexaBPM__SR_Status_Internal__c!=null || MapTransition.get('DOCUMENT_RE_UPLOADED').HexaBPM__SR_Status_External__c!=null){
	                    		HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id=SRId);
	                    		if(MapTransition.get('DOCUMENT_RE_UPLOADED').HexaBPM__SR_Status_Internal__c!=null)
	                    			objSR.HexaBPM__Internal_SR_Status__c = MapTransition.get('DOCUMENT_RE_UPLOADED').HexaBPM__SR_Status_Internal__c;
	                    		if(MapTransition.get('DOCUMENT_RE_UPLOADED').HexaBPM__SR_Status_External__c!=null)
	                    			objSR.HexaBPM__External_SR_Status__c = MapTransition.get('DOCUMENT_RE_UPLOADED').HexaBPM__SR_Status_External__c;
	                    		update objSR;
	                    	}
	                    	Step[0].HexaBPM__Status__c = MapTransition.get('DOCUMENT_RE_UPLOADED').HexaBPM__Transition__r.HexaBPM__To__c;
	                    	update Step;
	                    }
	                }                                          
	            }
        	}
        }catch(Exception e){
            system.debug('Exception In AutoCloseReuploadDocStep()===>'+e.getMessage());
        }
    }
   
    /*** Method to Enable/Disable Next & Previous Buttons for the document preview section***/
    @AuraEnabled
    public static String EnableDisableButtons(String SRID,String AttachmentID){
        Integer index = 0;
        list<HexaBPM__SR_Doc__c> SRDocList = [Select id,Name,HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRID order by createdDate ASC];
        Integer SRDocListSize = SRDocList.size();
        for(HexaBPM__SR_Doc__c ObjSRDoc : SRDocList){
            if(ObjSRDoc.HexaBPM__Doc_ID__c == AttachmentID)
                break;
            index++;            
        }
        if(SRDocListSize-index == SRDocListSize){
            return 'disablePrevious-enableNext';    
        }else if(SRDocListSize-index == 1){
            return 'disableNext-enablePrevious';
        }else
            return 'enablePrevious-enableNext';
    }  
    
    /*** Method to check if the logged in user is a community user or not***/                            
    @AuraEnabled
    public static Boolean CheckIfCommunityUser(){
        Boolean flag = false;
        list<User> UserList = [select id,ContactId from User where Id=:userInfo.getUserId()];
        for(User ObjUser : UserList){
            if(ObjUser.ContactId!=null){
                flag = true;
            }else
               flag = false;
        }
        return flag;
    }
}