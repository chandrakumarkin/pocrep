@isTest
public class WebServGDRFAAuthFeePaymentClassTest {
    
    @testSetup static void testSetupdata(){
        
        Service_Request__c testSr = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        Id RecordTypeIdSr = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-Cancellation').getRecordTypeId();
        testSr.RecordTypeId = RecordTypeIdSr;
        testSr.Mobile_Number__c = '';
        insert testSr;
        
        // Create Status__c data
        List<Status__c> statusList = new list<Status__c>();
        Status__c status = new Status__c();
        status.Code__c = 'AWAITING_REVIEW';
        statusList.add(status);
        
        Status__c status1 = new Status__c();
        status1.Code__c = 'CLOSED';
        status1.Name = 'closed';
        statusList.add(status1);
        
        Status__c status2 = new Status__c();
        status2.Code__c = 'DNRD_GS_Review';
        statusList.add(status2);
        
        Status__c status3 = new Status__c();
        status3.Code__c = 'Repush_to_GDRFA';
        statusList.add(status3);
        insert statusList;
        
        List<Step_Template__c> stepTemplateList = new List<Step_Template__c>();
        Step_Template__c stepTemplate = new Step_Template__c();
        stepTemplate.Name = 'Completed';
        stepTemplate.Code__c = 'Completed';
        stepTemplate.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate);
        
        Step_Template__c stepTemplate1 = new Step_Template__c();
        stepTemplate1.Name = 'Entry Permit Form is Typed';
        stepTemplate1.Code__c = 'Entry Permit Form is Typed';
        stepTemplate1.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate1);
        insert stepTemplateList;
        
        // Create Step__c data
        List<Step__c> stepList = new List<Step__c>();
        Step__c step = new Step__c();
        step.Transaction_Number__c = '2609361';
        step.DNRD_Receipt_No__c = '98903801';
        step.Payment_Date__c = Date.newInstance(2019, 01, 22);
        step.Step_Template__c = stepTemplate.Id;
        step.Status__c = status.Id;
        step.SR__c  = testSr.Id;
        stepList.add(step);
        
        Step__c step1 = new Step__c();
        step1.Step_Template__c = stepTemplate1.Id;
        step1.Transaction_Number__c = '260936';
        step1.DNRD_Receipt_No__c = '9890380';
        step1.Payment_Date__c = Date.newInstance(2019, 01, 22);
        step1.Status__c = status.Id;
        step1.SR__c  = testSr.Id;
        stepList.add(step1);
        insert stepList;
        step.Dependent_On__c = step1.id;
        step.Parent_Step__c = step1.id;
        update step;
        
        SR_Doc__c srDoc = new SR_Doc__c();
        srDoc.Service_Request__c = testSr.Id;
        srDoc.GDRFA_New_Document_Id__c = '231';
        insert srDoc;
        
        Attachment attch = new Attachment();
        attch.Name = 'Test attch';
        attch.Body = Blob.valueOf('Unit Test Attachment Body');
        attch.ParentId = srDoc.Id;
        attch.ContentType = 'image/jpg';
        insert attch;
        List<SR_Status__c > ListSRStatus = new List<SR_Status__c>();
        
        
        
        SR_Status__c srStatus = new SR_Status__c();
        srStatus.name = 'The application is under process';
        srStatus.Code__c = 'The application is under process';
        ListSRStatus.add(srStatus);
        
        SR_Status__c srStatus1 = new SR_Status__c();
        srStatus1.name = 'The application is under process with GDRFA';
        srStatus1.Code__c = 'The application is under process with GDRFA';
        ListSRStatus.add(srStatus1);  
        
        
        insert ListSRStatus;
        
        GDRFA_Master_Data__c mdData = new GDRFA_Master_Data__c();
        mdData.Look_Up_Id__c = '101';
        mdData.Lookup_Type__c = 'Country';
        mdData.Name_English__c = 'India';
        mdData.GDRFA_Unique_Value__c = '101 Country';
        insert mdData;
        
    }
    static testMethod void webServGDRFAAuthPaymentTest(){
        String ApplicationId = '12345678';
        String access_token = 'eyJc0MDU1RjVFMTBDQjRFRTI0QjZDMUQiLg';
        String token_type = 'Bearer';
        Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
        Step__c SrStep = [SELECT Transaction_Number__c, DNRD_Receipt_No__c, Payment_Date__c, Status__c, SR__c FROM Step__c WHERE Transaction_Number__c ='260936'];
        Test.setMock(HttpCalloutMock.class, new webServiceGdrfaPaymentMock());
        Test.startTest();
        WebServGDRFAAuthFeePaymentClass.getPayment(ApplicationId, access_token, token_type, SrStep.SR__c, SrStep, StatusReference.Id);
        Test.stopTest();
    }
    static testMethod void fetchGDRFARepushTest(){
        String ApplicationId = '12345678';
        String access_token = 'eyJc0MDU1RjVFMTBDQjRFRTI0QjZDMUQiLg';
        String token_type = 'Bearer';
        Step__c SrStep = [SELECT Transaction_Number__c, DNRD_Receipt_No__c, Payment_Date__c, Status__c, SR__c FROM Step__c WHERE Transaction_Number__c ='2609361'];
        Test.setMock(HttpCalloutMock.class, new webServiceGdrfaAppFeeMock());
        Test.startTest();
        WebServGDRFAAuthFeePaymentClass.rePushDocData(SrStep.Id);
        Test.stopTest();
    }
     static testMethod void fetchGDRFARepushDataTest(){
        String ApplicationId = '12345678';
        String access_token = 'eyJc0MDU1RjVFMTBDQjRFRTI0QjZDMUQiLg';
        String token_type = 'Bearer';
        Step__c SrStep = [SELECT Transaction_Number__c, DNRD_Receipt_No__c, Payment_Date__c, Status__c, SR__c FROM Step__c WHERE Transaction_Number__c ='2609361'];
        Test.setMock(HttpCalloutMock.class, new webServiceGdrfaPaymentMock());
        Test.startTest();
        WebServGDRFAAuthFeePaymentClass.rePushDocData(SrStep.Id);
        Test.stopTest();
    }
        static testMethod void webServGDRFAAuthArabicTransTest(){
        String ApplicationId = '12345678';
        String access_token = 'eyJc0MDU1RjVFMTBDQjRFRTI0QjZDMUQiLg';
        String token_type = 'Bearer';
        Step__c SrStep = [SELECT Transaction_Number__c, DNRD_Receipt_No__c, Payment_Date__c, Status__c, SR__c FROM Step__c WHERE Transaction_Number__c ='260936'];
        Test.setMock(HttpCalloutMock.class, new webServiceGdrfaPaymentMock());
        Test.startTest();
        WebServGDRFAAuthFeePaymentClass.getArabicTranslate('Test',SrStep, access_token,token_type);
        Test.stopTest();
    }
    
    static testMethod void webServGDRFAAuthArabicTransTest1(){
        Test.startTest();
        WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country', 'India');
        Test.stopTest();
    }
    
      static testMethod void webServGDRFAAuthArabicTransTest2(){
        Test.startTest();
        WebServGDRFAAuthFeePaymentClass.fakeMethod();
        Test.stopTest();
    }
    
}