public class FitOut_SOQL_Queries_MDT {
    public static Map<String , FitOut_SOQL_Queries__mdt> fitOutSOQLQuerriesMap { get; set; }
    public static String FitOut_OngoingProjectsSOQL;
    public static String All_FitOut_OngoingProjectsSOQL;
    public static string AmendmentsForManagementReport;
    public static string FitOut_ContractorPortalAccessCreated;
    public static string FitOut_InductionCreated;
    public static string Lease_Partners;
    public static String RentFreeUnitsWithoutProjects;
    public static String unitsOpenedForTradingSOQL;
    public static string FitOut_ClientsWithCompletedProjects;
    public string FitOut_OngoingProjectsDelayNotification;
    public string Units_With_out_Projects;
    public FitOut_SOQL_Queries_MDT (){
        loadData();
        setData(); 
    }
    public void loadData(){
    	fitOutSOQLQuerriesMap = new Map<String ,FitOut_SOQL_Queries__mdt>();
        for(FitOut_SOQL_Queries__mdt fitout : [select SOQL__c,TestClass_SOQL__c,Filter_Applicable__c ,Order_by__c,MasterLabel,Related_Functionality__c,Display_Order__c from FitOut_SOQL_Queries__mdt where is_Used__c = true order by Display_Order__c ASC]){
           fitOutSOQLQuerriesMap.put(fitout.MasterLabel,fitout);
        }
    }
    public void setData(){
    	FitOut_OngoingProjectsSOQL = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('OngoingFitOutProjects').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('OngoingFitOutProjects').SOQL__c;
    	if(fitOutSOQLQuerriesMap.get('OngoingFitOutProjects').Filter_Applicable__c){
    		 FitOut_OngoingProjectsSOQL = FitOut_OngoingProjectsSOQL+'filters' +' '+ fitOutSOQLQuerriesMap.get('OngoingFitOutProjects').Order_by__c;
    	}else{
    		 FitOut_OngoingProjectsSOQL = FitOut_OngoingProjectsSOQL+' '+fitOutSOQLQuerriesMap.get('OngoingFitOutProjects').Order_by__c;
    	}
    	FitOut_OngoingProjectsDelayNotification = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('FitOut_OngoingProjectsDelayNotification').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('FitOut_OngoingProjectsDelayNotification').SOQL__c;
    	Units_With_out_Projects = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('Units_With_out_Projects').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('Units_With_out_Projects').SOQL__c;
    	All_FitOut_OngoingProjectsSOQL = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('AllOngoingFitOutProjects').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('AllOngoingFitOutProjects').SOQL__c;
        AmendmentsForManagementReport = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('AmendmentsForManagementReport').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('AmendmentsForManagementReport').SOQL__c;
        FitOut_ContractorPortalAccessCreated = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('ContractorPortalAccess').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('ContractorPortalAccess').SOQL__c;
        Lease_Partners = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('Lease_Partners').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('Lease_Partners').SOQL__c;
    	FitOut_InductionCreated = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('FitOutInduction').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('FitOutInduction').SOQL__c;
    	RentFreeUnitsWithoutProjects = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('RentFreeUnitsWithoutProjects').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('RentFreeUnitsWithoutProjects').SOQL__c;
    	unitsOpenedForTradingSOQL = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('unitsOpenedForTrading').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('unitsOpenedForTrading').SOQL__c;
    	FitOut_ClientsWithCompletedProjects = Test.isRunningTest() ? fitOutSOQLQuerriesMap.get('ClientsWithCompletedProjects').TestClass_SOQL__c : fitOutSOQLQuerriesMap.get('ClientsWithCompletedProjects').SOQL__c;
    	system.debug('-------------createFitOut_Management_Report----FitOut_OngoingProjectsSOQL-----'+FitOut_OngoingProjectsSOQL);
    	system.debug('-------------createFitOut_Management_Report----Lease_Partners-----'+Lease_Partners);
    	system.debug('-------------createFitOut_Management_Report---AmendmentsForManagementReport------'+AmendmentsForManagementReport);
    	system.debug('-------------createFitOut_Management_Report----All_FitOut_OngoingProjectsSOQL-----'+All_FitOut_OngoingProjectsSOQL);
    	system.debug('-------------createFitOut_Management_Report----All_FitOut_OngoingProjectsSOQL-----'+All_FitOut_OngoingProjectsSOQL);
    	system.debug('-------------createFitOut_Management_Report----Units_With_out_Projects-----'+Units_With_out_Projects);
    }
    
}