/******************************************************************************************
 *  Name        : CRM_cls_EmailUtils_lightning_Test 
 *  Author      : Ghanshyam Yadav
 *  Company     : DIFC
 *  Date        : 2018-12-19
 *  Description : Main test class for CRM Lightning functionality
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   2018-12-19   Ghanshyam Yadav        Created
*******************************************************************************************/
@isTest(seeAllData=False)
public class CRM_cls_EmailUtils_lightning_Test {       
  static testmethod void EmailTest() {
  User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US');
     insert u;
      
     Lead testCrmLead;
     testCrmLead  = new Lead();
     testCrmLead = Test_cls_Crm_Utils.getTestLead();
     testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
     testCrmLead.Activities__c = 'ATM';
     testCrmLead.Property_solutions__c = 'Retail - Gate Avenue';
     insert testCrmLead;    
      
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    acc.Property_solutions__c = '';
    insert acc;   
      
    Contact con = new Contact();
    con.lastName = 'Yadav';
    con.AccountId = acc.Id;  
    insert con;
      
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
    opp.AccountId = acc.Id;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Confirmed Unit';
    opp.Lead_Id__c = testCrmLead.Id;
    opp.Stage_Steps__c = 'In Progress';
    opp.OwnerId = u.Id;  
    insert opp; 
    
   Test.startTest();
   CRM_cls_EmailUtils_lightning.getClientContacts(acc.id); 
    try{
        CRM_cls_EmailUtils_lightning.createEmailLink(opp.id,acc.id,'Confirmed Unit','In Progress','pointOfOrigin','emailTemplateCode','OwnerId');
      }
    catch(Exception e){
          system.debug('Error Message');
      }
   CRM_cls_EmailUtils_lightning.getEmailTemplateByStage('In Progress','pointOfOrigin','pointOfOrigin');
   CRM_cls_EmailUtils_lightning.getEmailTemplateByTemplateCode('In Progress','pointOfOrigin','emailTemplateCode');
   Test.stopTest();   
    }
 static testmethod void EmailTest1() {
     User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US');
     insert u;
      
     Lead testCrmLead;
     testCrmLead  = new Lead();
     testCrmLead = Test_cls_Crm_Utils.getTestLead();
     testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
     testCrmLead.Activities__c = 'ATM';
     testCrmLead.Property_solutions__c = 'Retail - Gate Avenue';
     insert testCrmLead;    
      
     Account acc = new Account();
     acc.Name = 'DIFC';
     acc.Lead_Reference_Number__c = '123456';
     acc.Property_solutions__c = 'Retail - Gate Avenue';
    insert acc;        
    Contact con = new Contact();
    con.lastName = 'Yadav';
    con.AccountId = acc.Id;  
    insert con;
      
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
    opp.AccountId = acc.Id;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Confirmed Unit';
    opp.Lead_Id__c = testCrmLead.Id;
    opp.Stage_Steps__c = 'In Progress';
    opp.OwnerId = u.Id;  
    insert opp; 
    
List<CRM_Email_Template_Code__mdt> Mdata =  [SELECT MasterLabel, CC__c, Stage_Steps__c, Approver_ID__c,
                Security_Check__c, Email_Template_Code__c, Point_of_Origin__c, 
                Opportunity_Stage__c, Approver_Name__c, DeveloperName, Approver_Email__c ,
                Client__c,Account_Company_Type__c FROM CRM_Email_Template_Code__mdt where DeveloperName = 'Financial_Security_Approval'];     
       
   Test.startTest();
   CRM_cls_EmailUtils_lightning.getClientContacts(acc.id);   
  // CRM_cls_EmailUtils_lightning.createEmailLink(opp.id,acc.id,'Confirmed Unit','In Progress','pointOfOrigin','','OwnerId');
   try{
     CRM_cls_EmailUtils_lightning.createEmailLink(opp.id,acc.id,'Confirmed Unit','In Progress','pointOfOrigin','emailTemplateCode','OwnerId');
      }
    catch(Exception e){
      system.debug('Error Message');
     }
   CRM_cls_EmailUtils_lightning.getEmailTemplateByStage('In Progress','pointOfOrigin','pointOfOrigin');
   CRM_cls_EmailUtils_lightning.getEmailTemplateByTemplateCode('In Progress','pointOfOrigin','emailTemplateCode');
   Test.stopTest();      
    }   
 @isTest
  static void EmailTest3(){ 
     User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     LastName = 'last',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US');
     insert u;
      
    Lead testCrmLead;
    testCrmLead  = new Lead();
    testCrmLead = Test_cls_Crm_Utils.getTestLead();
    testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
    testCrmLead.Activities__c = 'ATM';
    testCrmLead.Property_solutions__c = Null;
    insert testCrmLead;    
      
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    acc.Property_solutions__c = 'Retail - Gate Avenue';
    insert acc;   
      
    Contact con = new Contact();
    con.lastName = 'Yadav';
    con.AccountId = acc.Id;  
    insert con;
      
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
    opp.AccountId = acc.Id;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Confirmed Unit';
    opp.Lead_Id__c = testCrmLead.Id;
    opp.Stage_Steps__c = 'In Progress';
    opp.OwnerId = u.Id;  
    insert opp;    
      
    Opportunity result = CRM_cls_EmailUtils_lightning.getopportunity(opp.Id);
    } 
}