/*
Author :Shikha Khanuja
Date  : 10 Dec 2020
Descrption : Car parking batch to expire parking

History 
-----------------------------
Version       Date              Change By            Description
========      =======          ===============    ===========================
*/
public class CarParking_ExpiryBatch implements Schedulable,Database.Batchable<sObject>{

    public Database.QueryLocator start (Database.BatchableContext bc){
        String recordTypeName = GlobalConstants.MEMBERSHIP;
        String expiryStatus = GlobalConstants.STAT_EXP;
        String query = 'SELECT Id, MembershipStatus__c, Contact__c,Contact__r.AccountId, Service_Request__r.Customer__c, Service_Request__c, FromDate__c, ToDate__c FROM Membership__c WHERE ToDate__c <= YESTERDAY AND RecordType.Name = '+'\''+recordTypeName+'\''+' AND MembershipStatus__c != '+'\''+expiryStatus+'\'';
        system.debug('### query '+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute (Database.BatchableContext BC, List<Membership__c> membershipList){
        system.debug('### membershipList '+membershipList);
        Set<Id> AccountIdSet = new Set<Id>();
        Set<Id> SRIdSet = new Set<Id>();
        Map<Id,List<Membership__c>> accountVsMembershipMap = new Map<Id,List<Membership__c>>();
        for(Membership__c mem : membershipList){
            mem.MembershipStatus__c = GlobalConstants.STAT_EXP;
            AccountIdSet.add(mem.Service_Request__r.Customer__c);
            SRIdSet.add(mem.Service_Request__c);
        }        
        update membershipList;
        List<SR_Detail__c> srDetailList = [SELECT Id, Status__c, Service_Request__c FROM SR_Detail__c WHERE Service_Request__c IN :SRIdSet AND Status__c NOT IN ('Expired','Cancelled','Remove')];
        for(SR_Detail__c srObj : srDetailList){
            srObj.Status__c = GlobalConstants.STAT_EXP;
        }
        if(!srDetailList.isEmpty()){
            update srDetailList;
        }
        List<Account> accountList = [SELECT Id, Paid_Membership__c FROM Account WHERE Id IN :AccountIdSet];
        List<Relationship__c> relationshipList = [SELECT Id, Object_Contact__c FROM Relationship__c 
                                                 WHERE Object_Contact__r.RecordType.DeveloperName = 'GS_Contact'
                                                 AND Subject_Account__c IN :AccountIdSet AND Active__c = true];
        //List<Contact> contactList = [SELECT Id, AccountId FROM Contact WHERE AccountId IN :AccountIdSet];
        Set<Id> contactIds = new Set<Id>();        
        for(Relationship__c con : relationshipList){
            contactIds.add(con.Object_Contact__c);
        }
        system.debug('### contactIds '+contactIds);
        List<Membership__c> accountMembershipList = [SELECT Id, MembershipStatus__c,Contact__r.AccountId, Service_Request__c, Service_Request__r.Customer__c 
                                                     FROM Membership__c WHERE Contact__c IN : contactIds
                                                     AND MembershipStatus__c = 'Active' AND RecordType.Name = 'Membership'];
        system.debug('### accountMembershipList '+accountMembershipList);
        for(Membership__c objMem : accountMembershipList){
            if(accountVsMembershipMap.containsKey(objMem.Service_Request__r.Customer__c)){
                accountVsMembershipMap.get(objMem.Service_Request__r.Customer__c).add(objMem);
            }
            else{
                accountVsMembershipMap.put(objMem.Service_Request__r.Customer__c,new List<Membership__c> {objMem});
            }
        }
        system.debug('### accountVsMembershipMap '+accountVsMembershipMap);
        for(Account accountRecord : accountList){
            accountRecord.Paid_Membership__c = (accountVsMembershipMap != null && accountVsMembershipMap.containsKey(accountRecord.Id)) ? 
                accountVsMembershipMap.get(accountRecord.Id).size() : 0; 
        }
        system.debug('### account '+accountList);
        system.debug('### account size '+accountList.size());
        update accountList;
    }
    
    public void finish (Database.BatchableContext BC){
        system.debug('### Job has been completed');
    }
    
    public void execute(SchedulableContext scon) {
      Database.executeBatch(new CarParking_ExpiryBatch());
    }
}