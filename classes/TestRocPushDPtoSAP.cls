/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestRocPushDPtoSAP {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Notification_of_Personal_Data_Operations')]){
        	mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
 		Product2 objProd = new Product2();
        objProd.Name = 'Fine';
        insert objProd;
        
 		list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'FI_1100_ADP_fin';
        objPL.DEV_Id__c = 'FI_1100_ADP_fin';
        objPL.Product__c = objProd.Id;
        objPL.Material_Code__c = 'FI_1100_ADP';
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'FI_1100_RGF_fin';
        objPL.DEV_Id__c = 'FI_1100_RGF_fin';
        objPL.Product__c = objProd.Id;
        objPL.Material_Code__c = 'FI_1100_RGF_fin';
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'FI_1100_PSO_fin';
        objPL.DEV_Id__c = 'FI_1100_PSO_fin';
        objPL.Product__c = objProd.Id;
        objPL.Material_Code__c = 'FI_1100_PSO_fin';
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'FI_1100_PSP_fin';
        objPL.DEV_Id__c = 'FI_1100_PSP_fin';
        objPL.Product__c = objProd.Id;
        objPL.Material_Code__c = 'FI_1100_PSP';
        objPL.Priority__c = 1;
        lstPLs.add(objPL);        
        insert lstPLs;
 		
 		list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        Integer i = 0;
        for(Pricing_Line__c objP : lstPLs){
        	objDP = new Dated_Pricing__c();
        	objDP.Pricing_Line__c = objP.Id;
        	objDP.Unit_Price__c = 1234;
        	objDP.Date_From__c = system.today().addMonths(-1);
        	objDP.Date_To__c = system.today().addYears(2);
            objDP.DEV_Id__c = 'FI_1100_ADP_fin111' + i;
            i++;
        	lstDP.add(objDP);
        }
        insert lstDP;
 		
 		Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Portal_Balance__c = 10000;
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.RecordTypeId = mapRecordTypeIds.get('Notification_of_Personal_Data_Operations');
        objSR.Purpose_of_Notification__c = 'Prior to or immediately upon personal data processing';
        objSR.Transferring_Personal_Data_outside_DIFC__c = true;
        objSR.Processing_any_senstive_data__c = true;
        objSR.In_accordance_with_Article_10_1__c = false;
        objSR.Personal_Data_Processing_Procedure__c = '';
        objSR.Purpose_of_Processing_Data__c = '';
        objSR.In_Accordance_with_Article_11__c = true;
        objSR.Recognized_Jurisdictions__c = 'Indai;US Dept';
        objSR.In_Accordance_with_Article_12_1bj__c = true;
        objSR.In_Accordance_with_Article_12_1a__c = true;
        insert objSR;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        
        for(Pricing_Line__c objP : lstPLs){
	        SR_Price_Item__c objPriceItem = new SR_Price_Item__c(ServiceRequest__c=objSR.Id,Product__c=objProd.Id,Price__c=1200,Unique_SR_PriceItem__c=(objSR.Id+'_'+objP.Id),Status__c='Blocked');
			objPriceItem.Pricing_Line__c = objP.Id;
			lstPriceItems.add(objPriceItem);
        }
        insert lstPriceItems;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        insert objStep;
        
        for(Step__c objStp : [select Id,SR__c,SR__r.Customer__c,SR__r.Purpose_of_Notification__c,SR__r.Permit_1__c,SR__r.Permit_2__c,SR__r.Permit_3__c,SR__r.Record_Type_Name__c from Step__c where Id=: objStep.Id]){
			RocPushDPtoSAP.PushDPPriceLinesToSAP(objStp);			
		}
		
		objSR.Permit_1__c = 'New';
        objSR.Permit_2__c = 'New';
        objSR.Permit_3__c = 'New';
        update objSR;
        
        for(Step__c objStp : [select Id,SR__c,SR__r.Customer__c,SR__r.Purpose_of_Notification__c,SR__r.Permit_1__c,SR__r.Permit_2__c,SR__r.Permit_3__c,SR__r.Record_Type_Name__c from Step__c where Id=: objStep.Id]){
			RocPushDPtoSAP.PushDPPriceLinesToSAP(objStp);			
		}		
    }
}