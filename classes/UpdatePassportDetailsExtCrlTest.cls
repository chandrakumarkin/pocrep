@isTest
public class UpdatePassportDetailsExtCrlTest {
    public static testMethod void TestMethod1(){
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;
        Account accountObj = new Account ();
        accountObj.Name='Test Public Account Limited';
        accountObj.Trade_Name__c = 'Test Public Account Limited';
        Insert accountObj;
        
        Contact con = new Contact(LastName ='testCon',AccountId = accountObj.Id);
        insert con;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = accountObj.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactId=objContact.Id, Community_User_Role__c='Company Services',
                                TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        insert user;
        
        Service_request__c servicerequestObj = new Service_request__c ();
        servicerequestObj.Declaration_Signatory_Name__c = con.id;
        servicerequestObj.RecordTypeId=Schema.SObjectType.Service_request__c.getRecordTypeInfosByDeveloperName().get('Update_Passport_Details').getRecordTypeId();
        insert servicerequestObj;
        
        step__c objStep = new Step__c();
        objStep.SR__c = servicerequestObj.Id;
        objStep.No_PR__c = false;
        insert objStep;
        
        PageReference pageRef = Page.UpdatePassportDetails;
        
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('RecordType',servicerequestObj.RecordTypeId);
        Test.Starttest();
        Apexpages.StandardController sc = new Apexpages.StandardController(servicerequestObj);
        UpdatePassportDetailsExtCrl ext = new  UpdatePassportDetailsExtCrl(sc);         
        ext.SaveRecord();
        UpdatePassportDetailsExtCrl.autoApproveSR(objStep.Id);
        UpdatePassportDetailsExtCrl.updatePassportDetailOnContact(servicerequestObj.Id);
        UpdatePassportDetailsExtCrl.createAmendmentforSR(servicerequestObj);
        System.runAs(objUser) {	
            ext.checkShareHolder();	
        }
        Test.StopTest();    
    }
    public static testMethod void TestMethod_2(){
        Service_request__C ServicerequestObj = new Service_request__C ();
        PageReference pageRef = Page.UpdatePassportDetails;
        Test.setCurrentPage(pageRef);
        Apexpages.StandardController sc = new Apexpages.StandardController(servicerequestObj);
        UpdatePassportDetailsExtCrl ext = new  UpdatePassportDetailsExtCrl(sc);        
    }
}