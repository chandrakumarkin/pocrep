global class SearchAndReplaceHexaBPMSerReq implements Database.Batchable < sObject > {
    
    global final String Query;
    global List<sObject> passedRecords= new List<sObject>();

    global SearchAndReplaceHexaBPMSerReq(String q,List<sObject> listofRecords) {
        passedRecords = listofRecords;
        Query = q;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(query != '')return Database.getQueryLocator(query);
        else return DataBase.getQueryLocator([
            Select id,HexaBPM__Email__c,Authorised_individual_email__c,Authorised_individual_telephone_no__c,Foreign_entity_telephone_number__c,HexaBPM__Send_SMS_to_Mobile__c,Point_of_Contact__c,Telephone_No__c from HexaBPM__Service_Request__c        WHERE Id IN: passedRecords and Id != null]);
  
    }
    global void execute(Database.BatchableContext BC, List < HexaBPM__Service_Request__c > scope) {
        for (HexaBPM__Service_Request__c Sr: scope) {
            Sr.HexaBPM__Email__c ='test@difc.ae.invalid';
            Sr.Authorised_individual_email__c = 'test@difc.ae.invalid';
            Sr.Authorised_individual_telephone_no__c = Sr.Authorised_individual_telephone_no__c != NULL ? '+971569803616' : NULL;
            Sr.Foreign_entity_telephone_number__c = Sr.Foreign_entity_telephone_number__c != NULL ? '+971569803616' : NULL;
            Sr.HexaBPM__Send_SMS_to_Mobile__c = Sr.HexaBPM__Send_SMS_to_Mobile__c != NULL ? '+971569803616' : NULL;
            Sr.Point_of_Contact__c = Sr.Point_of_Contact__c != NULL ? '+971569803616' : NULL;
            Sr.Telephone_No__c = Sr.Telephone_No__c != NULL ? '+971569803616' : NULL;
        }
        Database.SaveResult[] srList = Database.update(scope, false);

        List<Log__c> logRecords = new List<Log__c>();
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                for(Database.Error err : sr.getErrors()) {
                    string errorresult = err.getMessage();
                    logRecords.add(new Log__c(
                      Type__c = 'SearchAndReplaceHexaBPMSerReq',
                      Description__c = 'Exception is : '+errorresult+'=Lead Id==>'+sr.getId()
                    ));
                    System.debug('The following error has occurred.');                    
                }
            }
        }
        if(logRecords.size()>0)insert logRecords;
    }

    global void finish(Database.BatchableContext BC) {}
}