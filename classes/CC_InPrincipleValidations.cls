/*
    Author      : Durga Prasad
    Date        : 03-Dec-2019
    Description : Custom code to check the validations for In Principle SR
    ---------------------------------------------------------------------------------------
    v1.0    Durga   19-Feb-2020     Removing the owner check for BD_Review Assesments
*/
global without sharing class CC_InPrincipleValidations implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        boolean hasReviewExists = false;
        system.debug('Step Template Code====>'+stp.Step_Template_Code__c);
        if(stp!=null && stp.Step_Template_Code__c=='BD_DETAILED_REVIEW'){
            Boolean isExemptUBO = false;
            for(HexaBPM_Amendment__c amed :[SELECT id 
                                            FROM HexaBPM_Amendment__c
                                            WHERE  ServiceRequest__c =:stp.HexaBPM__SR__c
                                            AND RecordType.DeveloperName =: OB_ConstantUtility.AMENDMENT_CORPORATE_RT.developerName
                                            AND Reason_for_exemption_under_DIFC_UBO__c = 'Regulated entity'
                                            Limit 1]){
                isExemptUBO = true;
            }
            if(!isExemptUBO){// if have any ubo with "Regulated Entity" then this validation shoul be skipped.
                //v1.0
                for(Application_Review__c AR:[Select Id from Application_Review__c where Application__c=:stp.HexaBPM__SR__c and RecordType.DeveloperName='BD_Review' limit 1]){
                    hasReviewExists = true;
                }
                if(!hasReviewExists)
                    strResult = 'Please complete the RM Assesment before approval.';
            }

            HexaBPM__Service_Request__c thisRequest = [SELECT Id,
                                                            HexaBPM__Customer__c ,
                                                            RecordTypeId,Setting_Up__c 
                                                    FROM HexaBPM__Service_Request__c 
                                                    WHERE ID =: stp.HexaBPM__SR__c ];
        
            Account thisAccount = [SELECT Id,
                                    OB_Sector_Classification__c,
                                    Company_Package__c 
                                  FROM Account
                                  WHERE Id =: thisRequest.HexaBPM__Customer__c];


            if((thisAccount.OB_Sector_Classification__c =='FinTech and Innovation' || thisAccount.OB_Sector_Classification__c =='FinTech') && String.isBlank(thisAccount.Company_Package__c)){
                strResult ='Please update the Company Package on Account';
            }

        }else if(stp!=null && stp.Step_Template_Code__c=='CLO_REVIEW'){
            for(Application_Review__c AR:[Select Id from Application_Review__c where Application__c=:stp.HexaBPM__SR__c and RecordType.DeveloperName='CLO_Review' and OwnerId=:userinfo.getuserid() limit 1]){
                hasReviewExists = true;
            }
            if(!hasReviewExists)
                strResult = 'Please complete CLO Review Assesment before approval.';
        }else if(stp!=null && stp.Step_Template_Code__c=='COMPLIANCE_REVIEW'){
            for(Application_Review__c AR:[Select Id from Application_Review__c where Application__c=:stp.HexaBPM__SR__c and RecordType.DeveloperName='Compliance_Review' and OwnerId=:userinfo.getuserid() limit 1]){
                hasReviewExists = true;
            }
            if(stp.HexaBPM__SR__r.Compliance_Rating__c==null)
                hasReviewExists = false;
            if(!hasReviewExists)
                strResult = 'Please complete the Complaince Assesment & Complaince Rating before approval.';
        }else if(stp!=null && stp.Step_Template_Code__c=='CFO_REVIEW'){
            for(Application_Review__c AR:[Select Id from Application_Review__c where Application__c=:stp.HexaBPM__SR__c and RecordType.DeveloperName='Committee_Review' and OwnerId=:userinfo.getuserid() limit 1]){
                hasReviewExists = true;
            }
            if(!hasReviewExists)
                strResult = 'Please complete CFO Review Assesment before approval.';
        }else if(stp!=null && stp.Step_Template_Code__c=='REGISTRAR_REVIEW'){
            for(Application_Review__c AR:[Select Id from Application_Review__c where Application__c=:stp.HexaBPM__SR__c and RecordType.DeveloperName='Registrar_Review' and OwnerId=:userinfo.getuserid() limit 1]){
                hasReviewExists = true;
            }
            if(!hasReviewExists)
                strResult = 'Please complete Registrar Review Assesment before approval.';
        }
        
        system.debug('hasReviewExists====>'+hasReviewExists);
        system.debug('strResult====>'+strResult);
        return strResult;
    }
}