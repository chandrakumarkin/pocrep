/*
    Author      :   Durga Prasad
    Description :   Custom Code which will validate whether user uploads the document which is configured
                    to upload in an particular step.    
                    
Modification History :
V.No    Name    Date        Description
1.1     Ravi    13/072016   Added the SR condition in the query to check for pendinf SR Doc 

*/
public without sharing class CC_Validate_ReqDocument {
    public static string Document_Upload_Check(Step__c stp){
        string strResult = null;
        try{
            strResult = 'Success';
            if(stp.SR_Step__c!=null){
                string strDocuments = '';
                //v1.1 - added the SR condition in the query
                for(SR_Doc__c srdoc:[select id,Doc_ID__c,SR_Template_Doc__c,Document_Name__c from SR_Doc__c where SR_Template_Doc__r.Validate_In_Code_at_Step_No__c=:stp.SR_Step__c and Doc_ID__c=null AND Service_Request__c =:stp.SR__c]){
                    if(strDocuments=='')
                        strDocuments = SRDOC.Document_Name__c;
                    else
                        strDocuments = strDocuments +' , '+SRDOC.Document_Name__c;
                }
                if(strDocuments!=null && strDocuments!=''){
                    return 'Please upload "'+strDocuments+'" document(s) by clicking Download/Upload Doc from Service Request in order to proceed.';
                }
            }
            system.debug('---strResult----'+strResult);
            return strResult;
        }catch(Exception e){
             strResult = string.valueOf(e.getMessage());
             system.debug('----line--'+e.getLineNumber());
        } 
        return strResult;
    }
    
    public static string checkLetterSubmittedToGDFRA(Step__c stp){
        string strResult = 'Success';
        try{
            //List<Step__c> stepList =[select id,Is_Letter_Submitted_to_GDFRA__c from Step__c where Id=:stp.Id limit 1];
            //if(!(stepList.size() >0 && stepList[0].Is_Letter_Submitted_to_GDFRA__c)){
            if(!stp.Is_Letter_Submitted_to_GDFRA__c){
                return 'Kindly check the Letter submiited To GDFRA check before updating';
            }
        }catch(Exception e){
             strResult = string.valueOf(e.getMessage());
             system.debug('----line--'+e.getLineNumber());
        } 
        return strResult;
    }
    
    public static string DeactivatePortalSRServiceBasedOnCustomer(Step__c Step){
        
        string res = 'Success';
        
        /*  List<Service_Request__c> srList = [ SELECT id,name,SR_Template__c,SR_Template__r.Name,
                Customer__c,Record_Type_Name__c
                FROM Service_Request__c
                WHERE Id=:Step.SR__c and SR_Template__c != null and Customer__c != null limit 1
        ];
        
        if(srList.size() >0){
        
            List<Portal_Menu_Access_Of_Customer__c> portalMenuConfiguredSpecificList = [select Id,Customer__c,
                Active__c,SR_Template__c,SR_Template__r.SR_RecordType_API_Name__c
                from Portal_Menu_Access_Of_Customer__c
                where SR_Template__c =:srList[0].SR_Template__c and Customer__c=:srList[0].Customer__c
            ];
            
            if(portalMenuConfiguredSpecificList.size() >0){
                List<Portal_Menu_Access_Of_Customer__c> ptList = new List<Portal_Menu_Access_Of_Customer__c>();
                for(Portal_Menu_Access_Of_Customer__c pt : portalMenuConfiguredSpecificList){
                    pt.Active__c = false;
                    ptList.add(pt);
                }
                
                if(ptList.size()>0)update ptList;
            }
            
        }*/
        return res;
    } 
    
    public static string populateArabicValuesOnLetter(Step__c Step){

        return '';
    }
}