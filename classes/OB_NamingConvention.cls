/**
Description : This apex class is used for Name reservation - Creation of record in Company Name object, Submitting the record for approval,
               Checking for the words under the criterias for Name criteria evaluation check, Duplicate check for existing companies and reserved names.

    v1.0 Merul 10 March 2020  logic for display of reserve and submit button
**/

public without sharing class OB_NamingConvention {
     @AuraEnabled
    public static List<Naming_Convention__mdt> checkName(){
        System.debug('inside Naming_Convention__mdt');
        return [Select Id,Warning_Type__c,DeveloperName,Is_Active__c,Verify_Complete_Word__c,Restricted_Words__c,Message__c from Naming_Convention__mdt where Is_Active__c = true];
    }
    
    @AuraEnabled
    public static HexaBPM__Service_Request__c getEntityDetails(String srId){
        string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        HexaBPM__Service_Request__c serviceRequest = new HexaBPM__Service_Request__c();
        for(HexaBPM__Service_Request__c srObj : [Select Entity_Type__c,Business_Sector__c,
                                                        Setting_Up__c,Legal_Structures__c,
                                                        Type_of_Entity__c,HexaBPM__Customer__r.OB_Franchise__c 
                                                 from HexaBPM__Service_Request__c
                                                 where Id=:srId and HexaBPM__Customer__c = :accountId]){
            serviceRequest = srObj;
        }
        return serviceRequest;
    }
    @AuraEnabled
    public static NameCheck getNameCheckDetail(string srId){
        NameCheck nameCheckObj = new NameCheck();
        string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        for(Company_Name__c compName : [Select Id,Stage__c,Status__c from Company_Name__c where Application__c =:srId and Application__r.HexaBPM__Customer__c = :accountId]){
            if(compName.Status__c == 'Approved' || compName.Status__c == 'In Progress'){
                nameCheckObj.isCompanyReserved = true;
                break;
            }
        }
        //Check for Return for more information - verify code
        //If the sub SR for name check is 'rejected',unlock the page. Set boolean = false 
        for(HexaBPM__Service_Request__c NameCheckSR:[Select Id,HexaBPM__Customer__c from HexaBPM__Service_Request__c 
                                                    where HexaBPM__Parent_SR__c=:srId and 
                                                    HexaBPM__Customer__c = :accountId and
                                                    RecordType.DeveloperName='Name_Check' and 
                                                    HexaBPM__Internal_Status_Name__c = 'Rejected']){
            nameCheckObj.isCompanyReserved = false;
        }
        return nameCheckObj;
    }
    @AuraEnabled
    public static NameCheck validateName(string searchTerm, string srId){
        NameCheck nameCheckObj = new NameCheck();
        List<String> lstSearchTerm = new list<string>();
        list<string> lstSkipWords = new list<string>();
        list<string> lstTempSkipWord = new list<string>();
        //Validating the Naming Criteria
        searchTerm = searchTerm.trim();
        if(String.isNotBlank(searchTerm)){
            
            List<Naming_Convention__mdt> lstNamingConvention = new List<Naming_Convention__mdt>();
            lstNamingConvention = checkName();
            list<string> lstRestrictedWordsArr = new list<string>();
            list<string> lstRestrictedWords;
            string regex;
            string lowerCaseSearchTerm;
            for(Naming_Convention__mdt namingCriteria : lstNamingConvention){
                lstRestrictedWords = new list<string>();
                if(namingCriteria.DeveloperName == 'Ignore_Words_Ending_With' && String.isNotBlank(namingCriteria.Restricted_Words__c)){
                    lstTempSkipWord = namingCriteria.Restricted_Words__c.split(',');
                    for(String word : lstTempSkipWord)
                        lstSkipWords.add(word.toLowerCase().trim());
                }
                else{
                    if(namingCriteria.Restricted_Words__c != null){
                        lstRestrictedWordsArr = namingCriteria.Restricted_Words__c.split(',');
                        
                        System.debug(lstRestrictedWords.size());
                        System.debug(lstRestrictedWords);
                        for(string keyWord : lstRestrictedWordsArr){
                            lstRestrictedWords.add(keyWord.toLowerCase().trim());
                        }

                        if(namingCriteria.Verify_Complete_Word__c == true)
                        {
                            lowerCaseSearchTerm = searchTerm.toLowerCase();
                            for(string keyWord : lstRestrictedWordsArr){
                                if(searchTerm.containsIgnoreCase(keyWord)){
                                    WarningMessage warningMsg = new WarningMessage();
                                    warningMsg.ruleName = namingCriteria.DeveloperName;
                                    warningMsg.warningType = namingCriteria.Warning_Type__c;
                                    warningMsg.message = namingCriteria.Message__c;
                                    warningMsg.restrictedWord = keyWord;//lstRestrictedWords[lstRestrictedWords.indexOf(s.toLowerCase())];
                                    nameCheckObj.lstWarningMessage.add(warningMsg);
                                    nameCheckObj.lstWarningType.add(namingCriteria.Warning_Type__c);
                                    break;
                                }
                            }
                        }
                        
                        else{
                            lstSearchTerm = searchTerm.split(' ');
                            for(String s: lstSearchTerm){
                                if(String.isNotBlank(s)){
                                    if(lstRestrictedWords.indexOf(s.toLowerCase()) > -1 ){
                                        System.debug('found');
                                        WarningMessage warningMsg = new WarningMessage();
                                        warningMsg.ruleName = namingCriteria.DeveloperName;
                                        warningMsg.warningType = namingCriteria.Warning_Type__c;
                                        warningMsg.message = namingCriteria.Message__c;
                                        warningMsg.restrictedWord = s;//lstRestrictedWords[lstRestrictedWords.indexOf(s.toLowerCase())];
                                        nameCheckObj.lstWarningMessage.add(warningMsg);
                                        nameCheckObj.lstWarningType.add(namingCriteria.Warning_Type__c);
                                        break;
                                    }
                                }
                            }
                        } 
                    } //End of validating restricted words
                }
            }
            //If no restricted words = no error , then proceed selection of name listing down idential names.
            if(nameCheckObj.lstWarningType.indexOf('Error') < 0 ){
                lstSearchTerm = searchTerm.split(' ');
                String sanitizedSearchString ;//= String.escapeSingleQuotes(searchTerm);
                sanitizedSearchString = '%' + searchTerm + '%';

                //Search for CompanyNames having same name and mark the boolean.
                boolean isReserved = false;
                
                for(Company_Name__c company : [select id from Company_Name__c where Entity_Name__c = :searchTerm and 
                                                (Status__c in ( 'Approved') or (Application__c = :srId) )]){
                    nameCheckObj.isCompanyReserved = true;
                    //nameCheckObj.isCompanyNameReserved = true;
                    isReserved = true;
                    break;
                }
                
                //Check for existing account names having identical name only if Company Name ->exact name match turns out false.
                //list<string> lstSearchWord = new list<string>();
                //lstSearchWord = searchTerm.split(' ');
                if(isReserved == false){
                    System.debug('>>>>');
                    System.debug(lstSkipWords);
                    
                    Set<String> companyNamesSet = new Set<String>();
                    for(string s : lstSearchTerm){
                        System.debug(s);
                        if(String.isNotBlank(s) && lstSkipWords.indexOf(s.toLowerCase()) < 0 && s.length() > 2){ //Ignore the skip words from searching
                            s = s.replaceAll('[^a-zA-Z0-9]', ''); //Removes the special characters from the search term
                            companyNamesSet.add('%'+s+'%');
                        }
                    }
                    for(Account acc : [select Id,Name,Office__c,Building_Name__c from Account where name like :companyNamesSet 
                                        and (ROC_Status__c ='Not Renewed' OR ROC_Status__c = 'Active') limit 20]){
                        nameCheckObj.lstIdenticalName.add(acc);
                        if(acc.Name.toLowerCase() == searchTerm.toLowerCase()){
                            nameCheckObj.lstReservedName.add(acc);
                            nameCheckObj.isCompanyReserved = true;
                            //nameCheckObj.isCompanyNameReserved = true;
                            break;
                        }
                    }
                }
                if(nameCheckObj.lstIdenticalName != null && nameCheckObj.lstIdenticalName.size() > 0)
                    nameCheckObj.lstWarningType.add('Warning Identical');
            }
        }
        return nameCheckObj;
    }
    
  
    @AuraEnabled
    public static List<Company_Name__c> returnRecordsCompanyName(string srId){
        system.debug('inside returnRecordsCompanyName');
        string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        List<Company_Name__c> returnLIst =[Select Id,Entity_Name__c,Status__c,Stage__c,
                                            Trading_Name__c,Ends_With__c, Arabic_Trading_Name__c,
                                             Arabic_Entity_Name__c, Rank__c,Application__c
                                              from Company_Name__c where Application__c =:srId 
                                               and Application__r.HexaBPM__Customer__c = :accountId
                                               ORDER BY CreatedDate];
        system.debug('returnLIst is '+returnLIst);
        return returnLIst;
    }
    @AuraEnabled
    public static Company_Name__c returnCompanyDetail(string srId, string companyId){
        system.debug('inside returnCompanyDetail');
        string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        Company_Name__c companyNameOb = new Company_Name__c();
        for (Company_Name__c cn : [Select Id,Entity_Name__c,Trading_Name__c,Ends_With__c,
                                     Arabic_Trading_Name__c, Arabic_Entity_Name__c,
                                      Rank__c,Application__c,Arabic_Ends_With__c,
                                      Check_Identical_to_existing__c
                                       from Company_Name__c where id= :companyId
                                        and Application__c =:srId and Application__r.HexaBPM__Customer__c = :accountId]){
            companyNameOb = cn;
        }
        return companyNameOb;
    }
    
    @AuraEnabled
    public static String deleteCompanyName(String companyId){
        try{
            string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
            list<HexaBPM__SR_Doc__c> lstSRDocs = new list<HexaBPM__SR_Doc__c>();
            lstSRDocs = [select id from HexaBPM__SR_Doc__c 
                            where OB_CompanyName__c = :companyId
                              and HexaBPM__Service_Request__r.HexaBPM__Customer__c = :accountId];
            if(lstSRDocs.size() > 0)
               delete  lstSRDocs;

            list<Company_Name__c> lstCompanyToDelete = new list<Company_Name__c>();
            lstCompanyToDelete = [Select Id from Company_Name__c where Id=:companyId and Application__r.HexaBPM__Customer__c = :accountId];
            if(lstCompanyToDelete.size() > 0)
                delete lstCompanyToDelete;
            return 'SUCCESS';
        }
        catch(Exception e){
            return e.getMessage();
        }
    }
    
    @AuraEnabled 
    public static NamePayment updateSR(HexaBPM__Service_Request__c serviceRequest, decimal amount){
        NamePayment namePaymentObj = new NamePayment();
        string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        string EntityType;
        
        /*Shoaib -- Added as a part to check check business activity is added */
        HexaBPM__Service_Request__c SR = [Select Id,Qualified_Applicant_Purpose__c,
                                          		Entity_Type__c,
                                                legal_structures__c,
                                                RecordType.DeveloperName
                                          from HexaBPM__Service_Request__c 
                                          where Id = :serviceRequest.Id LIMIT 1 ];
        
        if((SR.RecordType.DeveloperName == 'AOR_Financial' || SR.RecordType.DeveloperName == 'In_Principle'  )
            && (SR.legal_structures__c == 'Partnership'
                || SR.legal_structures__c == 'Company')
            && SR.Qualified_Applicant_Purpose__c != 'Qualified Purpose'){
                  
           Integer count = [SELECT Count() 
                                  FROM License_Activity__c 
                                 WHERE Account__c =:AccountId 
                                 AND Application__c =:serviceRequest.id];
          
          if(count == 0 ){
             namePaymentObj.errorMessage = 'Please add business activity before submiiting.'; 
             return namePaymentObj;
            }
         }
        
        
        try{
            if(serviceRequest != null)
                update serviceRequest;
            
            namePaymentObj.srId = serviceRequest.id;
            
            if(amount == 0){
                OB_NamingConventionHelper.CreateNameApprovalSR(namePaymentObj.srId);
            }
            else{
            list<HexaBPM__SR_Price_Item__c> lstPriceItem = [select id from HexaBPM__SR_Price_Item__c 
                                                    where HexaBPM__Product__r.ProductCode = 'OB Name Reservation'
                                                    and HexaBPM__Status__c = 'Added'
                                                    and HexaBPM__ServiceRequest__c = :namePaymentObj.srId limit 1];
            if(lstPriceItem.isEmpty())
                OB_NamingConvention.createPriceItem(namePaymentObj.srId, amount, 'Added', 'OB Name Reservation');
            
            namePaymentObj.paymentpageURL = '/topupbalance?srId='+namePaymentObj.srId+'&category=namereserve'; // Take to payment confirmation page.
            }
        }

        catch(System.DmlException  ex){
            namePaymentObj.errorMessage = ex.getDmlMessage(0);
        }
        return namePaymentObj;
    }
    public static string getAccountId(string userId){
        string strAccountId;
        for(User user: [select contact.AccountId from User where id = :userId])           
            strAccountId = user.contact.AccountId;
            
        return strAccountId;
    }
    public static string getDeclaration(string recordTypeName){
        string declarationText;
        for(Declarations__mdt declObj : [select Declaration_Text__c from Declarations__mdt 
                                            where Field_API_Name__c = 'I_Agree_EntityNameCheck__c' 
                                            and Record_Type_API_Name__c = :recordTypeName]){
            declarationText = declObj.Declaration_Text__c;
        }
        return declarationText;
    }
    @AuraEnabled
    public static CompanyWrapper retrieveCompanyWrapper(string srId, string pageId, string companyNameId){
        system.debug('inside getTransCompNameDetails');//
        string docMasterCode;
        string acctId = getAccountId(UserInfo.getUserId());
        CompanyWrapper wrapObj = new CompanyWrapper();
        string typeOfEntity;
        string subTypeOfEntity;
        if(String.isNotBlank(companyNameId))
            wrapObj.company = returnCompanyDetail(srId, companyNameId);
        wrapObj.lstCompany = returnRecordsCompanyName(srId);
        
       
        list<HexaBPM__Service_Request__c> lstSR = new list<HexaBPM__Service_Request__c>();
        lstSR = [select id from HexaBPM__Service_Request__c   
                    where HexaBPM__Customer__c = :acctId
                    and HexaBPM__Internal_Status_Name__c = 'Draft' 
                    and id = :srId];
        if(lstSR != null && lstSR.size() > 0)
        {
            //If the sub SR for name check is 'rejected',unlock the page. Set boolean = false 
            for(HexaBPM__Service_Request__c NameCheckSR:[Select Id,HexaBPM__Customer__c,HexaBPM__Internal_Status_Name__c from HexaBPM__Service_Request__c 
                                                        where HexaBPM__Parent_SR__c=:srId and 
                                                        RecordType.DeveloperName='Name_Check'
                                                        and HexaBPM__Internal_Status_Name__c in ('Submitted','Approved') 
                                                        order by createddate desc limit 1 ]){
                wrapObj.isCompanyReserved = true;
            }
        }
        else
        {
            wrapObj.isCompanyReserved = true;
            for(HexaBPM__Step__c  srStep : [select id from HexaBPM__Step__c where HexaBPM__SR__c = :srId 
                                            and HexaBPM__SR__r.HexaBPM__Customer__c = :acctId
                                            and HexaBPM__SR__r.HexaBPM__Internal_Status_Name__c != 'Draft' 
                                            and HexaBPM__Status_Type__c != 'End' 
                                            and HexaBPM__SR_Step__r.Pages_to_Unlock__c includes ('ob-searchentityname')]){
                wrapObj.isCompanyReserved = false;
            }

        }

        for(HexaBPM__Section_Detail__c sectionObj: [SELECT id,HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection' 
                                                    AND HexaBPM__Section__r.HexaBPM__Page__c = :pageId LIMIT 1]) {
            wrapObj.ButtonSection =  sectionObj;                                            
        }
        

        // v1.0 Merul 10 March 2020  logic for display of reserve and submit button
        for(HexaBPM__Service_Request__c srObj : [SELECT Id, recordType.DeveloperName, DNFBP__c,Entity_Type__c,Check_Identical_to_existing__c,Fall_within_existing_entities__c,Parent_Entity_Name__c,
                                                    Former_Name__c,Date_of_Registration__c,Place_of_Registration__c,Type_of_Entity__c,Sub_Type_of_Entity__c ,Franchise__c,
                                                    Country_of_Registration__c,Registered_No__c,Family_Group__c,Registered_address__c,Setting_Up__c,Foreign_Entity_Name__c,Business_Sector__c,
                                                    Address_Line_1__c,Address_Line_2__c,Country__c,City_Town__c,
                                                    State_Province_Region__c,Po_Box_Postal_Code__c, I_Agree_EntityNameCheck__c,
                                                    ( 
                                                     SELECT id
                                                       FROM Company_Names__r
                                                      WHERE Reserved_Name__c = true
                                                        AND Status__c = 'Approved'
                                                    ) 

                                                FROM HexaBPM__Service_Request__c
                                               WHERE id =:srId 
                                                 AND HexaBPM__Customer__c = :acctId])
        {

            wrapObj.sr = srObj;
            wrapObj.declarationText = getDeclaration(srObj.recordType.DeveloperName);
            typeOfEntity = srObj.Type_of_Entity__c;
            subTypeOfEntity = srObj.Sub_Type_of_Entity__c;
            //v1.0 Merul 10 March 2020  logic for display of reserve and submit button
            wrapObj.displayResAndSubmtBtn = ( srObj.Company_Names__r != NULL && srObj.Company_Names__r.size() > 0 ? false : true );
            

            
        }
        
        List<map<string,string>> lstOptionItems = new List<map<string,string>>();
        for(Ob_EntityEndsWith_Setting__mdt mdtObj : [select Ends_With__c,OB_Ends_With_Arabic__c 
                                                    from Ob_EntityEndsWith_Setting__mdt
                                                    where OB_Type_Of_Entity__c = :typeOfEntity and 
                                                     OB_SubType_of_Entity__c = :subTypeOfEntity]){
            EndsWith endsWithOb = new EndsWith();
            endsWithOb.endsWithEnglish = mdtObj.Ends_With__c;
            endsWithOb.endsWithArabic = mdtObj.OB_Ends_With_Arabic__c;
            wrapObj.lstEndsWith.add(endsWithOb);
        }
        //Setting the price item - price for name reservation
        for(HexaBPM__Dated_Pricing__c  pricing : [select id,HexaBPM__Unit_Price__c from HexaBPM__Dated_Pricing__c where HexaBPM__Pricing_Line__r.HexaBPM__Product__r.ProductCode='OB Name Reservation']){
            wrapObj.price = pricing.HexaBPM__Unit_Price__c;
        }
        return wrapObj;
    }
  
    @AuraEnabled
    public static string addEntityName(String EntityName,String NameId, String warningMessage, String srId, string reason, String relatedTo, String relationText, map<string,string> docMap){ //, string contentDocId, string docMasterCode
       
        string companyNameId;
        
        //Editting the existing name
        if(NameId!=null && NameId!=''){
            for(Company_Name__c companyNameToBeUpdated : [Select Id,Entity_Name__c,OB_Relation__c,Trading_Name__c,OB_Reason__c, Warning_Messages__c, Stage__c, Application__c from Company_Name__c 
                                                            where  Id=:NameId]){
                companyNameToBeUpdated.Entity_Name__c=EntityName;
                companyNameToBeUpdated.OB_Reason__c = reason;
                if(String.isNotBlank(relatedTo)){
                    companyNameToBeUpdated.OB_Related_To__c = relatedTo;
                    companyNameToBeUpdated.OB_Relation__c = relationText;
                }
                else {
                    companyNameToBeUpdated.OB_Related_To__c = null;
                    companyNameToBeUpdated.OB_Relation__c = '';
                }
                //Arabic Translations for update
                if(String.isNotBlank(EntityName)){
                    companyNameToBeUpdated.Arabic_Entity_Name__c = translateToArabic(EntityName);
                    companyNameToBeUpdated.Arabic_Trading_Name__c = translateToArabic(EntityName);
                }
                update companyNameToBeUpdated;
                companyNameId = companyNameToBeUpdated.Id;
            }
        }
       
        else{
            
            Company_Name__c acc = new Company_Name__c();
            acc.OB_Reason__c = reason;
            acc.Entity_Name__c=EntityName;
            acc.Trading_Name__c=EntityName;
            acc.Date_Of_Registration__c = system.today();
            if(String.isNotBlank(EntityName)){
                acc.Arabic_Entity_Name__c = translateToArabic(EntityName);
                acc.Arabic_Trading_Name__c = translateToArabic(EntityName);
            }
            acc.Warning_Messages__c=warningMessage;
            acc.Stage__c='Draft'; //
            //acc.Status__c = 'Not Started';
            acc.Status__c = 'Draft';
            acc.Application__c = srId;
            acc.OB_Relation__c = relationText;
            system.debug('New==>'+acc);
            insert acc;
            companyNameId = acc.Id;
        }

        //Insert SR Docs
        if(docMap != null)
            OB_NamingConvention.saveSRDoc(srId, companyNameId,docMap);

        return companyNameId;
    }
    public static void saveSRDoc(string srId, string parentId, map<string,string> docMap){
        
        //delete the previous SR Docs for the same parent and documentMasterCode
        if(docMap != null && docMap.size() > 0)
            delete [select id from HexaBPM__SR_Doc__c where OB_CompanyName__c = :parentId and HexaBPM__Document_Master__r.HexaBPM__Code__c in :docMap.keySet()];
        
        OB_QueryUtilityClass.createCompanySRDoc(srId, parentId, docMap);
    }
    
    //Create Price Item
    @AuraEnabled
    public static string createPriceItem(String srId, decimal amountPaid, string status, string productCode){
        //Create Price Item 
        HexaBPM__SR_Price_Item__c priceItem = new HexaBPM__SR_Price_Item__c();
        priceItem.HexaBPM__ServiceRequest__c = srId;
        priceItem.HexaBPM__Status__c = status;//'Blocked';
        priceItem.HexaBPM__Price__c = amountPaid;
        priceItem.HexaBPM__Sys_Added_through_Code__c = true;
        //select id from Product2 where ProductCode = :productCode limit 1
        for(HexaBPM__Pricing_Line__c priceLine : [select Id,HexaBPM__Product__c from HexaBPM__Pricing_Line__c
                             where HexaBPM__Product__r.ProductCode = :productCode
                              and HexaBPM__Product__r.IsActive=true and
                               HexaBPM__Active__c = true limit 1]){ 
            priceItem.HexaBPM__Product__c = priceLine.HexaBPM__Product__c;
            priceItem.HexaBPM__Pricing_Line__c = priceLine.Id;
        }
        insert priceItem;
        return priceItem.Id;
    }
    //Create Price Item and Sub SR for Name Reservation
    @AuraEnabled
    public static void createNameReservationSR(String srId){
        //Create Sub SR for Naming Reservation
        OB_NamingConventionHelper.CreateNameApprovalSR(srId);
    }
    @AuraEnabled
    public static string translateToArabic(string text){
        try{
            return TranslatiorClass.translatorMethod(text);
        }
        catch(Exception ex){
            return ex.getMessage();
        }
    }
    //Submitting for Name check
    @AuraEnabled
    public static CompanyWrapper commitNameApex(String srId, String pageId, Company_Name__c companyName, map<string,string> docMap){
        try{
             //If the SR sector = 'representative office', Trading Name must end with the phrase “(DIFC Representative Office)” at the end of Trading Name
            if(String.isNotBlank(companyName.Trading_Name__c)){
                for(HexaBPM__Service_Request__c servObj : [select Business_Sector__c from HexaBPM__Service_Request__c
                                                            where id = :srId and Business_Sector__c = 'Representative Office']){
                    companyName.Trading_Name__c = companyName.Trading_Name__c + System.Label.Representative_Office;
                }
            }
            companyName.Status__c = 'Draft';
            companyName.Stage__c = 'Draft';
            companyName.Application__c = srId;
            if(companyName != null){
                upsert companyName;
            }

            //Insert SR Docs
            if(docMap != null)
                OB_NamingConvention.saveSRDoc(srId, companyName.Id,docMap);

            CompanyWrapper wrapObj = new CompanyWrapper();
            wrapObj = retrieveCompanyWrapper(srId, pageId, '');
            //wrapObj.lstCompany = returnRecordsCompanyName(srId);
            return wrapObj;
            
        }
        catch(Exception e){
            CompanyWrapper wrapObj = new CompanyWrapper();
            wrapObj.errorMessage = e.getMessage();
            return wrapObj;
        }
    }
    @AuraEnabled
    public static map<string,map<string,string>> viewSRDocs(string srId, string companyNameId){ 
        //Preparing the SRDocs for the amendment
        map<string,map<string,string>> mapSRDocs = new map<string,map<string,string>>();
        map<string,string> mapIdName = new map<string,string>();
        if(String.isNotBlank(companyNameId)){
            for(HexaBPM__SR_Doc__c srDoc : [select Id,File_Name__c,HexaBPM__Document_Master__r.HexaBPM__Code__c from HexaBPM__SR_Doc__c
                                            where HexaBPM__Service_Request__c = :srId and OB_CompanyName__c = :companyNameId 
                                            and HexaBPM__Document_Master__r.HexaBPM__Code__c in ('Evidence_of_relationship_and_consent','Evidence_of_relationship_to_existing_entity')]){
                                                
                                                mapIdName.put('Id',srDoc.Id);
                                                mapIdName.put('FileName',srDoc.File_Name__c);
                                                mapSRDocs.put(srDoc.HexaBPM__Document_Master__r.HexaBPM__Code__c,mapIdName); 
                                                
            }
        }
        return mapSRDocs;
    }
    //Create receipt for the payment
    public static void createReceiptRecord(decimal amount, string paymentType){
        string userAccountID;
        for(User usr : [Select contactid,Contact.AccountId,Contact.Account.Name from User where Id =: UserInfo.getUserId()]){
                userAccountID = usr.Contact.AccountID;
                
        }
        Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get(paymentType).getRecordTypeId();
        //Create a receipt record with values entered by the customer
        Receipt__c R = new Receipt__c();
        R.Customer__c=userAccountID; 
        R.Amount__c=amount; 
        R.Receipt_Type__c=paymentType;
        R.Transaction_Date__c=system.now(); 
        R.RecordTypeId=rt;
        R.Payment_Status__c='Success';
        insert R; 
        
    }
    @AuraEnabled
    public static void updateTradingName(String TradingName, String NameId, String warningMessage,String srId){
        Company_Name__c latestReservedName = [Select Id,Entity_Name__c,Trading_Name__c from Company_Name__c where Id=:NameId];
        latestReservedName.Trading_Name__c=TradingName;
        latestReservedName.Warning_Messages_Trading__c=warningMessage;
        update latestReservedName;  
    }

    
      @AuraEnabled
    public static List <String> getselectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        System.debug('objType is' + objType);
        // Describe the SObject using its object type.
        
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        System.debug('objDescribe is' + objDescribe);
        System.debug('Result is' + objObject.getSObjectType().getDescribe().fields.getMap());
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        System.debug('fieldMap is' + fieldMap);
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values = fieldMap.get(fld).getDescribe().getPickListValues();
        System.debug('values are '+values);
        // Add these values to the select option list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
     @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID,string pageId, string ButtonId, HexaBPM__Service_Request__c serviceRequest){
        
        if(serviceRequest != null)
           update serviceRequest;
        
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
        PageFlowControllerHelper.objSR = objRequest;
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();

        objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
        upsert objRequest;
        If(!test.isRunningTest()) OB_RiskMatrixHelper.CalculateRisk(SRID);
        
        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage '+responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;
        
        system.debug('@@@@@@@@2 respWrap.pageActionName '+respWrap.pageActionName);
        return respWrap;
    }
    
    public class ButtonResponseWrapper{
        @AuraEnabled public String pageActionName{get;set;}
        @AuraEnabled public string communityName{get;set;}
        @AuraEnabled public String errorMessage{get;set;}
        @AuraEnabled public string CommunityPageName{get;set;}
        @AuraEnabled public string sitePageName{get;set;}
        @AuraEnabled public string strBaseUrl{get;set;}
        @AuraEnabled public string srId{get;set;}
        @AuraEnabled public string flowId{get;set;}
        @AuraEnabled public string  pageId{get;set;}
        @AuraEnabled public boolean isPublicSite{get;set;}
        
    }
    public class NameCheck{
        @AuraEnabled
        public boolean isCompanyReserved {get; set;}
        @AuraEnabled
        public boolean isCompanyNameReserved {get;set;}
        @AuraEnabled
        public list<string> lstWarningType {get; set;}
        @AuraEnabled
        public list<WarningMessage> lstWarningMessage {get;set;}
        @AuraEnabled
        public list<Account> lstIdenticalName {get;set;}
        @AuraEnabled
        public list<Account> lstReservedName {get;set;}
        @AuraEnabled
        public map<string,map<string,string>> srDocMap {get;set;}
        public NameCheck(){
            isCompanyReserved = false;
            isCompanyNameReserved = false;
            lstWarningMessage = new list<WarningMessage>();
            lstWarningType = new list<string>();
            lstIdenticalName = new list<Account>();
            lstReservedName = new list<Account>();
            srDocMap = new map<string,map<string,string>>();
        }
    }
    public class WarningMessage{
        @AuraEnabled
        public string ruleName {get; set;}
        @AuraEnabled
        public string warningType {get; set;}
        @AuraEnabled
        public string message {get; set;}
        @AuraEnabled
        public string restrictedWord {get; set;}
    }
    public class CompanyWrapper
    {
        @AuraEnabled
        public list<Company_Name__c> lstCompany {get; set;}
        @AuraEnabled
        public Company_Name__c company {get; set;}
        @AuraEnabled
        public HexaBPM__Service_Request__c sr {get; set;}
        @AuraEnabled
        public string docId {get; set;}
        @AuraEnabled
        public string evidenceDocId {get; set;}
        @AuraEnabled
        public boolean isCompanyReserved {get; set;}
        @AuraEnabled
        public string declarationText {get; set;}
        @AuraEnabled
        public list<EndsWith> lstEndsWith {get; set;}
        @AuraEnabled 
        public HexaBPM__Section_Detail__c ButtonSection { get; set; }  
         @AuraEnabled
        public decimal price {get; set;}
        @AuraEnabled
        public string errorMessage {get; set;}
        @AuraEnabled
        public boolean displayResAndSubmtBtn {get; set;}

        public CompanyWrapper()
        {
            lstEndsWith = new list<EndsWith>();
            lstCompany = new list<Company_Name__c>();
            company = new Company_Name__c();
            isCompanyReserved = false;
            docId = '';
            evidenceDocId = '';
            ButtonSection = new HexaBPM__Section_Detail__c();
            price = 0;
            errorMessage = '';
            displayResAndSubmtBtn = true;
            
        }
    }
    public class EndsWith{
        @AuraEnabled
        public string endsWithEnglish {get; set;}
        @AuraEnabled
        public string endsWithArabic {get; set;}
        public EndsWith(){
            endsWithEnglish = '';
            endsWithArabic = '';
        }
    }
    public class NamePayment{
        @AuraEnabled
        public string srId {get; set;}
        @AuraEnabled
        public decimal amount {get; set;}
        @AuraEnabled
        public string productCode {get; set;}
        @AuraEnabled
        public decimal portalBalance {get; set;}
        @AuraEnabled
        public string paymentpageURL {get; set;}
        @AuraEnabled
        public string errorMessage {get; set;}
        public NamePayment(){
            srId = '';
            amount = 0;
            productCode = '';
            portalBalance = 0;
            paymentpageURL = '';
            errorMessage = '';
        }
    }
    
    
}