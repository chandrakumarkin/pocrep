/*
    Author      : Leeba
    Date        : 11-April-2020
    Description : Test class for CC_CreateRelCommPerSR
    --------------------------------------------------------------------------------------
	Modification History
	Shikha 12/02/2020 - Modified class for test coverage
*/

@isTest
public without sharing class CC_CreateRelCommPerSRTest{

    public static testMethod void CC_CreateRelCommPerSR() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
        
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = acc.Id);
        insert con; 
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.Date_of_Issue__c = system.today();
        objHexaSR.Duration_days__c = 12;
        
        objHexaSR.Date_of_Expiry__c = system.today()+100;
        objHexaSR.Duration_days__c = 5;
        objHexaSR.Foreign_Entity_Name__c = 'test';
        objHexaSR.Foreign_entity_registered_number__c = '+971566785645';
        objHexaSR.Foreign_entity_telephone_number__c  = '+971566785645';
         objHexaSR.Estmd_No_of_Employees_in_1st_Yr__c  = 10;
         objHexaSR.Foreign_entity_country_of_registration__c  = 'India';
         objHexaSR.Foreign_entity_date_of_registration__c  = system.today()-100;
         objHexaSR.Foreign_entity_country__c  = 'India';
         objHexaSR.Foreign_entity_city_town__c  = 'India';
         objHexaSR.Foreign_entity_state_province_region__c  = 'India';
         objHexaSR.Foreign_entity_Po_Box_Postal_Code__c  = '000000';
         objHexaSR.License_Issuing_Authority__c  = 'India';
        objHexaSR.Brief_Background__c  = 'India';
        objHexaSR.Describe_affiliation__c  = 'India';
         objHexaSR.Foreign_entity_address_line_1__c  = 'India';
         objHexaSR.Foreign_entity_address_line_2__c  = 'India';
        //Shikha - added
        objHexaSR.Type_of_commercial_permission__c = 'ATM';
        objHexaSR.Duration__c = 'One year';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
         Id SRRecId1 = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
        HexaBPM_Amendment__c objAmm = new HexaBPM_Amendment__c ();
        objAmm.First_Name__c = 'try';
        objAmm.ServiceRequest__c = objHexastep.HexaBPM__SR__c;
        objAmm.Recordtypeid = SRRecId1 ;
        objAmm.Nationality_list__c = 'India';
        objAmm.Passport_No__c = '12348';
       
        insert objAmm;
        
        
        
       
        Test.startTest();
         HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c, HexaBPM__SR__r.Duration_days__c,HexaBPM__SR__r.Duration__c,
                                  HexaBPM__SR__r.Date_of_Expiry__c,  HexaBPM__SR__r.Type_of_commercial_permission__c ,HexaBPM__SR__r.Foreign_entity_trading_name__c ,
                                  HexaBPM__SR__r.Arabic_Entity_Name__c,HexaBPM__SR__r.Foreign_Entity_Name__c,HexaBPM__SR__r.DIFC_Owned__c,HexaBPM__SR__r.Select_Qualified_Purpose__c,
                                  HexaBPM__SR__r.Address_Line_1__c,HexaBPM__SR__r.Address_Line_2__c,HexaBPM__SR__r.Type_of_entity__c,HexaBPM__SR__r.Foreign_entity_address_line_1__c,HexaBPM__SR__r.Foreign_entity_address_line_2__c, 
                                  HexaBPM__SR__r.Describe_affiliation__c, HexaBPM__SR__r.Brief_Background__c, HexaBPM__SR__r.License_Issuing_Authority__c,HexaBPM__SR__r.Start_date__c,
                                   HexaBPM__SR__r.Foreign_entity_Po_Box_Postal_Code__c, HexaBPM__SR__r.Foreign_entity_state_province_region__c, HexaBPM__SR__r.Foreign_entity_city_town__c,
                                   HexaBPM__SR__r.Foreign_entity_country__c, HexaBPM__SR__r.Foreign_entity_date_of_registration__c, HexaBPM__SR__r.Foreign_entity_country_of_registration__c,
                                   HexaBPM__SR__r.Estmd_No_of_Employees_in_1st_Yr__c, HexaBPM__SR__r.Foreign_entity_telephone_number__c, HexaBPM__SR__r.Foreign_entity_registered_number__c
                            
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_CreateRelCommPerSR CC_CreateRelCommPerSRObj = new CC_CreateRelCommPerSR();
        CC_CreateRelCommPerSRObj.EvaluateCustomCode(objHexaSR,step); 
        
        objHexaSR.Type_of_commercial_permission__c = 'Events';
        
        update objHexaSR;
         HexaBPM__Step__c step2 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c, HexaBPM__SR__r.Duration_days__c,
                                  HexaBPM__SR__r.Date_of_Expiry__c,  HexaBPM__SR__r.Type_of_commercial_permission__c ,HexaBPM__SR__r.Foreign_entity_trading_name__c ,
                                  HexaBPM__SR__r.Arabic_Entity_Name__c,HexaBPM__SR__r.Foreign_Entity_Name__c,HexaBPM__SR__r.DIFC_Owned__c,HexaBPM__SR__r.Start_Date__c,
                                  HexaBPM__SR__r.Address_Line_1__c,HexaBPM__SR__r.Address_Line_2__c,HexaBPM__SR__r.Type_of_entity__c,HexaBPM__SR__r.Foreign_entity_address_line_1__c,HexaBPM__SR__r.Foreign_entity_address_line_2__c
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
          CC_CreateRelCommPerSRObj.EvaluateCustomCode(objHexaSR,step2); 
        
        objHexaSR.Type_of_commercial_permission__c  ='Dual license of DED licensed firms with an affiliate in DIFC';
        objHexaSR.Duration__c = 'One year';
        objHexaSR.Type_of_entity__c = 'Public';
        update objHexaSR;
        objAmm.Roles__c = 'Authorized Representative';
        update objAmm;
        
        Id SRRecId12 = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Body Corporate').getRecordTypeId();
        HexaBPM_Amendment__c objAmm2 = new HexaBPM_Amendment__c ();
        objAmm2.ServiceRequest__c = objHexastep.HexaBPM__SR__c;
        objAmm2.Recordtypeid = SRRecId12 ;
        objAmm2.Roles__c = 'Authorized Representative;Approved person';
        objAmm2.Nationality_list__c = 'India';
        objAmm2.Passport_No__c = '1234';
        insert objAmm2;
        
        
        
         HexaBPM__Step__c step3 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c, HexaBPM__SR__r.Duration_days__c,
                                  HexaBPM__SR__r.Date_of_Expiry__c,  HexaBPM__SR__r.Type_of_commercial_permission__c ,HexaBPM__SR__r.Foreign_entity_trading_name__c ,
                                  HexaBPM__SR__r.Arabic_Entity_Name__c,HexaBPM__SR__r.Foreign_Entity_Name__c,HexaBPM__SR__r.DIFC_Owned__c,
                                  HexaBPM__SR__r.Address_Line_1__c,HexaBPM__SR__r.Address_Line_2__c,HexaBPM__SR__r.Type_of_entity__c,
                                   HexaBPM__SR__r.Duration__c,HexaBPM__SR__r.Select_Qualified_Purpose__c
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        
         objHexaSR.Type_of_commercial_permission__c = 'Dual license of DED licensed firms with an affiliate in DIFC';
         //objHexaSR.Duration__c = 'Event - Up to 3 days';
        //objHexaSR.Duration__c ='2';
        update objHexaSR;
         CC_CreateRelCommPerSR CC_CreateRelCommPerSRObj2 = new CC_CreateRelCommPerSR();
        HexaBPM__Step__c step4 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c, HexaBPM__SR__r.Duration_days__c,
                                  HexaBPM__SR__r.Date_of_Expiry__c,  HexaBPM__SR__r.Type_of_commercial_permission__c ,HexaBPM__SR__r.Foreign_entity_trading_name__c ,
                                  HexaBPM__SR__r.Arabic_Entity_Name__c,HexaBPM__SR__r.Foreign_Entity_Name__c,HexaBPM__SR__r.DIFC_Owned__c,HexaBPM__SR__r.Select_Qualified_Purpose__c,
                                  HexaBPM__SR__r.Address_Line_1__c,HexaBPM__SR__r.Address_Line_2__c,HexaBPM__SR__r.Type_of_entity__c
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
          CC_CreateRelCommPerSRObj2.EvaluateCustomCode(objHexaSR,step4); 
        
        try{
             CC_CreateRelCommPerSR.createRelatioship(step3);
        }catch(exception e){
            
        }
      
        
        CC_CreateRelCommPerSR.getObjectRelationship(new list<sObject>{objAmm2},objAmm2);
        objAmm2.Roles__c =  'Foreign Company;Director;Council Member;Shareholder;Authorized Representative;General Partner;Guardian;Authorized Representative;Partner;Member;Founder;Founding Member;Company Secretary;Authorized Representative;Signatory;Approved Person;Senior Management;General Classification;Exempt Entity;DPO;Qualified Applicant;Foreign Partnership;test';
        update objAmm2;
        CC_CreateRelCommPerSR.getObjectRelationship(new list<sObject>{objAmm2},objAmm2);
        
        Currency__c currencyVal = new Currency__c();
        currencyVal.Name = 'AED';
        currencyVal.Active__c = true;
        currencyVal.Currency_Code__c = 'AED';
		insert  currencyVal; 
        
         Account_Share_Detail__c shareClass= new Account_Share_Detail__c();
        shareClass.Account__c = acc.Id;
        shareClass.No_of_shares_per_class__c = 23;
        shareClass.Currency__c = currencyVal.Id; 
        shareClass.Nominal_Value__c =100; 
        insert shareClass;
        
         Shareholder_Detail__c sd = new Shareholder_Detail__c();
        sd.Account__c= acc.Id;
        sd.Account_Share__c= shareClass.Id;
        sd.OB_Amendment__c= objAmm2.Id;
        insert sd;       
        
         CC_CreateRelCommPerSR.getObjectRelationship(new list<sObject>{objAmm2},objAmm2);
        
        Relationship__c rel = new Relationship__c();
        rel.Relationship_Type__c ='Is Public Relationship officer for';
        rel.Subject_Account__c= acc.Id;
        rel.Object_Contact__c= con.id;
        rel.Active__c = true;
        insert rel;
        
         CC_CreateRelCommPerSR.getShareDetail(sd,rel);
       
        
        objAmm2.Is_this_member_a_designated_Member__c = 'yes';
        objAmm2.Company_Name__c = 'test';
        objAmm2.Former_Name__c = 'test';
        objAmm2.Telephone_No__c = '+97154757';
        objAmm2.Place_of_Registration__c = 'India';
        objAmm2.Apartment_or_Villa_Number_c__c = '12';
        objAmm2.Address__c = 'yes';
        objAmm2.PO_Box__c = 'yes';
         objAmm2.Emirate_State_Province__c = 'dubai';
         objAmm2.Permanent_Native_Country__c = 'dubai';
        objAmm2.Permanent_Native_City__c = 'dubai';
        objAmm2.Jurisdiction__c='Andorra';
         objAmm2.Mobile__c = '+97154757';
        objAmm2.Name_of_the_exchange__c = 'test';
          objAmm2.Name_of_the_Regulated_Firm__c = 'test';
         objAmm2.Please_state_the_name_of_the_Regulator__c = 'test';
         objAmm2.Recognised_Exchange__c = '	Abu Dhabi Stock Exchange';
         objAmm2.Source_and_origin_of_funds_for_entity__c = 'test';
         objAmm2.Source_of_income_wealth__c = 'Others';
         objAmm2.Trading_Name__c = 'test';
        objAmm2.Select_the_Qualified_Applicant_Type__c = 'Fund';
        objAmm2.Financial_Service_Regulator__c = 'Autorite des marches financiers (France) (AMF)';
        objAmm2.Date_of_Registration__c = system.today();
        
        objAmm2.Title__c = 'Dr.';
        objAmm2.Last_Name__c = 'India';
        objAmm2.First_Name__c = 'India';
        objAmm2.Date_of_Birth__c = system.today().addMonths(-120);
        objAmm2.Place_of_Birth__c = 'India';
        //Shikha - correcting for test coverage
        objAmm2.Passport_Expiry_Date__c = system.today().addMonths(2);
        objAmm2.Gender__c = 'Male';
        objAmm2.Country__c = 'India';
        objAmm2.Place_of_Issue__c = 'India';
        objAmm2.Passport_Issue_Date__c = system.today().addMonths(-1);
        objAmm2.Are_you_a_resident_in_the_U_A_E__c = 'Yes';
        objAmm2.Is_this_individual_a_PEP__c = 'Yes';
        objAmm2.E_I_D_no__c = '123';
        objAmm2.U_I_D_No__c = '123';
        objAmm2.Type_of_Authority__c = 'Separately';
        objAmm2.Please_provide_the_type_of_authority__c = 'yt';
        objAmm2.Is_this_Entity_registered_with_DIFC__c ='No';        
         objAmm2.Country_of_source_of_income__c = 'India';
         objAmm2.Email__c = 'yes@yes.com';
         objAmm2.Emirate__c = 'dubai';
         //objAmm2.Jurisdiction__c = 'yes';
        update objAmm2;
        
         try{
             CC_CreateRelCommPerSR.createRelatioship(step3);
        }catch(exception e){
            
        }
        
         CC_CreateRelCommPerSR.getObjectContact(con,objAmm2);
        CC_CreateRelCommPerSR.getObjectAccount(acc,objAmm2);
        CC_CreateRelCommPerSR.reasonOfExemptionMapping('');
        CC_CreateRelCommPerSR.reasonOfExemptionMapping('Government entity');
        CC_CreateRelCommPerSR.reasonOfExemptionMapping('Listed with an exchange in a recognised Jurisdiction');
        CC_CreateRelCommPerSR.reasonOfExemptionMapping('Regulated entity');
        CC_CreateRelCommPerSR.reasonOfExemptionMapping('Wholly owned by a government');
        Test.stopTest();
       
    }
    
    
 }