/*
    Author      : Rajil Ravindran
    Company     : PwC
    Date        : 02-May-2019
    Description : Gets the selected case records from the list view and update the status to invalid and update them.
    -------------------------------------------------------------------------
*/
public with sharing class CasesListViewController 
{
    private ApexPages.StandardSetController standardController;
    public CasesListViewController(ApexPages.StandardSetController standardController)
    {
        this.standardController = standardController;
    }
    public PageReference markInvalidCases()
    {       
        // Get the selected records (optional, you can use getSelected to obtain ID's and do your own SOQL)
        List<Case> selectedCases = (List<Case>) standardController.getSelected();

        // Update records       
        for(Case selectedCase : selectedCases)
        {
            selectedCase.status = 'Invalid';
        }   
        return null;        
    }
    public PageReference updateCases()
    {       
        // Call StandardSetController 'save' method to update (optional, you can use your own DML)
        try{
        	return standardController.save();   
        }
        catch(Exception ex){ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage());ApexPages.addMessage(msg);
        	return null;
        }   
    }
}