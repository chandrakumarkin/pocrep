/*--------------------------------------------------------------------------------------------------------------------------
    Modification History
--------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    V1.0    09-03-2017  Sravan          Added a variable to carry SR Reason for cancellation for TKT # 2872
    V1.1    30-09-2017  Sravan         Added method to check refund amount as per tkt # 4164
--------------------------------------------------------------------------------------------------------------------------*/
public class GsSRCancellationForm {
    public Service_Request__c ServiceRequest {get;set;}
   
    
    public GsSRCancellationForm(){
        ServiceRequest = new Service_Request__c();
    
        getSR();
    }
    
    public void getSR(){
        String SRId = ApexPages.currentPage().getParameters().get('id');
        if(SRId != null && SRId != ''){
            for(Service_Request__c obj : [select Id,Name,Is_Cancelled__c,Internal_Status_Name__c,Service_Type__c,Reason_for_Request__c  from Service_Request__c where Id=:SRId]){
                ServiceRequest = obj;
            }
        }
    }
    
    public PageReference SaveSR(){
        //Savepoint sp = Database.setSavepoint();
        List<Log__c> LogsRfnd = new List<Log__c>();
        try{
            if(ServiceRequest.Reason_for_Request__c != null && ServiceRequest.Reason_for_Request__c != ''){
                //ServiceRequest.Is_Cancelled__c = true;
                string str;
                
                if(ServiceRequest.Is_Cancelled__c != true && !(ServiceRequest.Internal_Status_Name__c =='Refund Processed'|| ServiceRequest.Internal_Status_Name__c =='Cancellation Pending'||ServiceRequest.Internal_Status_Name__c =='Cancellation Approved'||ServiceRequest.Internal_Status_Name__c =='Service rejected - Refund pending' || ServiceRequest.Internal_Status_Name__c =='Cancellation approved - Refund pending')){
                    
                GsServiceCancellation.reasonForCancellationStr = ServiceRequest.Reason_for_Request__c; //V1.0
                GsServiceCancellation.refundAmnt = 0;
                
                if(!test.isRunningTest())
                    str = GsServiceCancellation.CancellationPrecheck(ServiceRequest.Id);   // V1.1             
                else
                    str = 'Success';
                
                if(str == 'Success'){
                    str = '';
                    str = GsServiceCancellation.ProcessCancellation(ServiceRequest.Id);
                }   
                if(str == 'Cancellation submitted successfully'){
                    if(GsServiceCancellation.refundAmnt !=null && GsServiceCancellation.refundAmnt !=0) 
                        ServiceRequest.Refund_Amount__c = GsServiceCancellation.refundAmnt;  // V1.1
                        
                    ServiceRequest.Is_Cancelled__c = true;
                    update ServiceRequest;                   
                    PageReference objRef = new PageReference(Site.getpathprefix()+'/'+ServiceRequest.Id);
                    objRef.setRedirect(true);
                    return objRef;
                }else{
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, str));
                    //database.rollback(sp);
                    return null;
                }
                }else{
                    
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Request has been alrady submitted '));
                    //database.rollback(sp);
                    return null;
                    
                }
            }else{
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Please provide the Reason for Cancellation'));
                return null;
            }
            
           
            
           
        }catch(Exception ex){
            //database.rollback(sp);
            system.debug('*****'+ex.GetMessage());
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Please contact support.'));                  
            insert LogDetails.CreateLog(null, 'GS Service Cancellation Form', ex.getMessage()+'\nLine #'+ex.getLineNumber()+'\nTrace '+ex.getStackTraceString());
        }
        return null;
    }
    
    public PageReference BackToSR(){
        PageReference objRef = new PageReference(Site.getpathprefix()+'/'+ServiceRequest.Id);
        objRef.setRedirect(true);
        return objRef;
    }
}