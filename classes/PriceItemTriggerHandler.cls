/*
    Author      :   Shoaib Tariq
    Description :   This class is update SAP Unique Id for Miscellaneous_Services
    --------------------------------------------------------------------------------------------------------------------------
  Modification History
   --------------------------------------------------------------------------------------------------------------------------
  V.No  Date    Updated By      Description
  --------------------------------------------------------------------------------------------------------------------------             
   V1.0    17-02-2020  shoaib      Created
*/
public class PriceItemTriggerHandler {
    
    public static void UpdateSAPUniqueField(List<SR_Price_Item__c> priceItemList){
        
        Set<Id> srIds = new Set<Id>();
        Map<String,SR_Price_Item__c> srItemMap = new Map<String,SR_Price_Item__c>();
        
        for(SR_Price_Item__c thisItem :priceItemList){
            if(String.isNotBlank(thisItem.Transaction_Number__c)){
                  
                   srIds.add(thisItem.ServiceRequest__c);
                   srItemMap.put(thisItem.ServiceRequest__c, thisItem);
            }
        }
        if(!srIds.isEmpty()){
            List<Service_Request__c> thisRequestList = new List<Service_Request__c>(); 
            for(Service_Request__c thisRequest : [SELECT Id,
                                                            RecordTypeId,
                                                            SAP_Unique_No__c,
                                                            SAP_SO_No__c
                                                            FROM Service_Request__c 
                                                            WHERE Id IN:srIds]){
                                                                
                 if(thisRequest.RecordTypeId == Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Miscellaneous_Services').getRecordTypeId()){
                   
                     thisRequest.SAP_Unique_No__c = String.isBlank(thisRequest.SAP_Unique_No__c)
                                                    && srItemMap.containsKey(thisRequest.Id) 
                                                    ?  srItemMap.get(thisRequest.Id).Transaction_Number__c :thisRequest.SAP_Unique_No__c;
                 }  
                thisRequestList.add(thisRequest);
            }  
            update thisRequestList;
        }  
     }
     
     
   public static void UpdatePipelinePSAonAccount(List<SR_Price_Item__c> priceItemList){
   
   List<String> AddPSAItemIds = new List<String>();
   List<String> RemovePSAItemIds = new List<String>();
    List<Account> AccoutToUpdate = new List<Account> ();
     
       for(SR_Price_Item__c SRPrItem : priceItemList){
       
          if(SRPrItem.Status__c == 'Blocked'){
          
            AddPSAItemIds.add(SRPrItem.id);
          }
          
           if(SRPrItem.Status__c == 'Invoiced' || SRPrItem.Status__c == 'Cancelled'){
          
            RemovePSAItemIds.add(SRPrItem.id);
          }
       
       }
       
       
       if(AddPSAItemIds.size()>0) {
           
           
          for(SR_Price_Item__c  SRPriceAdd : [select id,ServiceRequest__r.Customer__r.id,ServiceRequest__r.Customer__r.PSA_Pipeline_App_not_posted__c from SR_Price_Item__c  where id in: AddPSAItemIds]){
      
             Account acc = new Account();
             
             Integer i = Integer.valueOf(SRPriceAdd.ServiceRequest__r.Customer__r.PSA_Pipeline_App_not_posted__c);
             system.debug('Integer i' +i);
             
             acc.id = SRPriceAdd.ServiceRequest__r.Customer__r.id;
             if(i ==  null)
             acc.PSA_Pipeline_App_not_posted__c  =  1;
             else 
             acc.PSA_Pipeline_App_not_posted__c = i + 1;
             AccoutToUpdate.add(acc);
      
           }   
           
           
       }
       
       if(RemovePSAItemIds.size()>0){
           
           for(SR_Price_Item__c  SRPriceAdd : [select id,ServiceRequest__r.Customer__r.id,ServiceRequest__r.Customer__r.PSA_Pipeline_App_not_posted__c from SR_Price_Item__c  where id in: RemovePSAItemIds]){
      
             Account acc = new Account();
             
             acc.id = SRPriceAdd.ServiceRequest__r.Customer__r.id;
             if(SRPriceAdd.ServiceRequest__r.Customer__r.PSA_Pipeline_App_not_posted__c > 0 )
             acc.PSA_Pipeline_App_not_posted__c = SRPriceAdd.ServiceRequest__r.Customer__r.PSA_Pipeline_App_not_posted__c - 1;
             
             AccoutToUpdate.add(acc);
      
           }
           
       }
     
    
    
    
    
    if(AccoutToUpdate.size()> 0) {
    
       try{
       update AccoutToUpdate;
       }catch(Exception Ex){
       
           Log__c objLog = new Log__c( Type__c = 'Account PipeLine PSA update failed ',
               Description__c = 'Exception is : ' + ex.getMessage() + '\nLine # ' + ex.getLineNumber());
              
               insert objLog;
       
       }
     }
   }
}