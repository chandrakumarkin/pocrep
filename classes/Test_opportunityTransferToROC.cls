@isTest(SeeAllData=false)
public class Test_opportunityTransferToROC {

 static testMethod void myUnitTest() {
Id RecordTypeIdopportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('BD Financial').getRecordTypeId();
UserRole r = new UserRole(DeveloperName = 'BusinessDevelopment', Name = 'BusinessDevelopment');
insert r;
User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
    FirstName = 'Shyam', 
    LastName = 'Yadav',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
);     
     system.runAs(u){  
  Account acc = new Account();
  acc.Name = 'Rex Constructor';
  acc.OwnerId = userinfo.getUserId();
  acc.Sector_Lead_lookup__c = u.Id;
  acc.Transferred_to_ROC__c = True;
  acc.Authorization_Group__c = 'Blocked by BDD Admin';
  acc.Company_Type__c = 'Financial - related';
  acc.Priority_to_DIFC__c = 'T1: Global with high FTE';
  acc.BD_Sector__c = 'Business Entities (BE)';
  acc.Activities__c = 'Antiques & Rare Collections Repaire';
  acc.Sector_Classification__c = 'Ancillary Service Provider';
  acc.ROC_Status__c = 'Under Formation';
  acc.Financial_Sector_Activities__c = 'Private Equity'; 
  acc.BD_Financial_Services_Activities__c = 'Accepting Deposits';  
  insert acc;
     
  Opportunity opps = new  Opportunity();
  opps.AccountId = acc.id;
  opps.Name = 'RFL';
  opps.StageName = 'Created';
  opps.CloseDate = system.today();
  opps.Lead_Email__c = 'gsyadav19@gmail.com';
  opps.Transferred_to_ROC__c = False;
  opps.Activities__c = 'Antiques & Rare Collections Repaire';
  opps.Sector_Classification__c  = 'Halls and exibition';
  opps.RecordTypeId =  RecordTypeIdopportunity;
  insert opps;
     
 Test.startTest();
  opportunityTransferToROC.updateOpportunity(opps.Id);
  Test.stopTest();
}
 } 
 static testMethod void myUnitTest1() {
Id RecordTypeIdopportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('BD Financial').getRecordTypeId();
 system.debug('RecordTypeIdopportunity'+RecordTypeIdopportunity);
UserRole r = new UserRole(DeveloperName = 'BusinessDevelopment', Name = 'BusinessDevelopment');
insert r;
User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
    FirstName = 'Shyam', 
    LastName = 'Yadav',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
);     
     system.runAs(u){  
  Account acc = new Account();
  acc.Name = 'Rex Constructor';
  acc.OwnerId = userinfo.getUserId();
  acc.Sector_Lead_lookup__c = u.Id;
  acc.Transferred_to_ROC__c = False;
  acc.Authorization_Group__c = 'Blocked by BDD Admin';
  acc.Company_Type__c = 'Non - financial';
  acc.Priority_to_DIFC__c = 'T1: Global with high FTE';
  acc.BD_Sector__c = 'Business Entities (BE)';
  acc.Activities__c = 'Antiques & Rare Collections Repaire';
  acc.Sector_Classification__c = 'Ancillary Service Provider';
  acc.ROC_Status__c = 'Name Reserved';
  acc.Financial_Sector_Activities__c = 'Private Equity'; 
  acc.BD_Financial_Services_Activities__c = 'Accepting Deposits';  
  insert acc;
     
  Opportunity opps = new  Opportunity();
  opps.AccountId = acc.id;
  opps.Name = 'RFL';
  opps.StageName = 'Created';
  opps.CloseDate = system.today();
  opps.Lead_Email__c = 'gsyadav19@gmail.com';
  opps.Transferred_to_ROC__c = False;
  opps.Activities__c = 'Antiques & Rare Collections Repaire';
  opps.Sector_Classification__c  = 'Halls and exibition';
  opps.RecordTypeId =  RecordTypeIdopportunity;
  insert opps;
     
 Test.startTest();
  opportunityTransferToROC.updateOpportunity(opps.Id); 
 Test.stopTest();
}
 }     
 
 static testMethod void myUnitTest2() {
Id RecordTypeIdopportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('BD Financial (Fund)').getRecordTypeId();
 system.debug('RecordTypeIdopportunity'+RecordTypeIdopportunity);
UserRole r = new UserRole(DeveloperName = 'BusinessDevelopment', Name = 'BusinessDevelopment');
insert r;
User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
    FirstName = 'Shyam', 
    LastName = 'Yadav',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
);     
     system.runAs(u){  
  Account acc = new Account();
  acc.Name = 'Rex Constructor';
  acc.OwnerId = userinfo.getUserId();
  acc.Sector_Lead_lookup__c = u.Id;
  acc.Transferred_to_ROC__c = False;
  acc.Authorization_Group__c = 'Blocked by BDD Admin';
  acc.Company_Type__c = 'Financial - related';
  acc.Priority_to_DIFC__c = 'T1: Global with high FTE';
  acc.BD_Sector__c = 'Business Entities (BE)';
  acc.Activities__c = 'Antiques & Rare Collections Repaire';
  acc.Sector_Classification__c = 'Ancillary Service Provider';
  acc.ROC_Status__c = 'Name Reserved';
  acc.Financial_Sector_Activities__c = ''; 
  acc.BD_Financial_Services_Activities__c = '';  
  insert acc;
     
  Opportunity opps = new  Opportunity();
  opps.AccountId = acc.id;
  opps.Name = 'RFL';
  opps.StageName = 'Created';
  opps.CloseDate = system.today();
  opps.Lead_Email__c = 'gsyadav19@gmail.com';
  opps.Transferred_to_ROC__c = False;
  opps.Activities__c = 'Antiques & Rare Collections Repaire';
  opps.Sector_Classification__c  = 'Halls and exibition';
  opps.RecordTypeId =  RecordTypeIdopportunity;
  insert opps;
     
 Test.startTest();
  opportunityTransferToROC.updateOpportunity(opps.Id);  
 Test.stopTest();
}
 }  
    
 static testMethod void myUnitTest3() {
Id RecordTypeIdopportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('BD Financial (Fund)').getRecordTypeId();
 system.debug('RecordTypeIdopportunity'+RecordTypeIdopportunity);
UserRole r = new UserRole(DeveloperName = 'BusinessDevelopment', Name = 'BusinessDevelopment');
insert r;
User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
    FirstName = 'Shyam', 
    LastName = 'Yadav',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
);     
     system.runAs(u){  
  Account acc = new Account();
  acc.Name = 'Rex Constructor';
  acc.OwnerId = userinfo.getUserId();
  acc.Sector_Lead_lookup__c = u.Id;
  acc.Transferred_to_ROC__c = False;
  acc.Authorization_Group__c = 'Blocked by BDD Admin';
  acc.Company_Type__c = 'Retail';
  acc.Priority_to_DIFC__c = 'T1: Global with high FTE';
  acc.BD_Sector__c = 'Business Entities (BE)';
  acc.Activities__c = 'Antiques & Rare Collections Repaire';
  acc.Sector_Classification__c = 'Ancillary Service Provider';
  acc.ROC_Status__c = 'Name Reserved';
  acc.Financial_Sector_Activities__c = ''; 
  acc.BD_Financial_Services_Activities__c = '';  
  insert acc;
     
  Opportunity opps = new  Opportunity();
  opps.AccountId = acc.id;
  opps.Name = 'RFL';
 // opps.Thompson_Reuters__c = 'In Progress';
//  opps.Security_Check__c = Null;
  opps.StageName = 'Approved';
 // opps.Stage_Steps__c = 'Approved';
  opps.CloseDate = system.today();
  opps.Lead_Email__c = 'gsyadav19@gmail.com';
  opps.Transferred_to_ROC__c = False;
  opps.Activities__c = Null;
  opps.Sector_Classification__c  = Null;
  opps.RecordTypeId =  RecordTypeIdopportunity;
  insert opps;
     
 Test.startTest();
  opportunityTransferToROC.updateOpportunity(opps.Id);  
  Test.stopTest();
}
 }
 static testMethod void myUnitTest4() {
Id RecordTypeIdopportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
 system.debug('RecordTypeIdopportunity'+RecordTypeIdopportunity);
UserRole r = new UserRole(DeveloperName = 'BusinessDevelopment', Name = 'BusinessDevelopment');
insert r;
User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
    FirstName = 'Shyam', 
    LastName = 'Yadav',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
);     
     system.runAs(u){  
  Account acc = new Account();
  acc.Name = 'Rex Constructor';
  acc.OwnerId = userinfo.getUserId();
  acc.Sector_Lead_lookup__c = u.Id;
  acc.Transferred_to_ROC__c = False;
  acc.Authorization_Group__c = 'Blocked by BDD Admin';
  acc.Company_Type__c = 'Financial - related';
  acc.Priority_to_DIFC__c = 'T1: Global with high FTE';
  acc.BD_Sector__c = 'Business Entities (BE)';
  acc.Activities__c = 'Antiques & Rare Collections Repaire';
  acc.Sector_Classification__c = 'Ancillary Service Provider';
  acc.ROC_Status__c = 'Name Reserved';
  acc.Financial_Sector_Activities__c = ''; 
  acc.BD_Financial_Services_Activities__c = '';  
  insert acc;
     
  Opportunity opps = new  Opportunity();
  opps.AccountId = acc.id;
  opps.Name = 'RFL';
  opps.Thompson_Reuters__c = 'In Progress';
  opps.Security_Check__c = Null;
  opps.StageName = 'Approved';
  opps.Stage_Steps__c = 'Approved';
  opps.CloseDate = system.today();
  opps.Lead_Email__c = 'gsyadav19@gmail.com';
  opps.Transferred_to_ROC__c = False;
  opps.Activities__c = Null;
  opps.Sector_Classification__c  = Null;
  opps.RecordTypeId =  RecordTypeIdopportunity;
 try{
  insert opps;
   }
 catch(exception e){
             
         }   
         
 Test.startTest();
  opportunityTransferToROC.updateOpportunity(Null); 
  Test.stopTest();
}
 } 

    
static testMethod void myUnitTest5() {
Id RecordTypeIdopportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('BD Financial').getRecordTypeId();
RecordType rt =   [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType = 'Opportunity' and DeveloperName = 'BD_Financial_Fund_with_Security'];
system.debug('rt----'+rt);    
    
    UserRole r = new UserRole(DeveloperName = 'BusinessDevelopment', Name = 'BusinessDevelopment');
insert r;
User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
    FirstName = 'Shyam', 
    LastName = 'Yadav',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
);     
     system.runAs(u){  
  Account acc = new Account();
  acc.Name = 'Rex Constructor';
  acc.OwnerId = u.id;
  acc.Sector_Lead_lookup__c = u.Id;
 // acc.Sector_Lead_user__c        
  acc.Transferred_to_ROC__c = False;
  acc.Authorization_Group__c = 'Blocked by BDD Admin';
  acc.Company_Type__c = 'Financial - related';
  acc.Priority_to_DIFC__c = 'T1: Global with high FTE';
  acc.BD_Sector__c = 'Business Entities (BE)';
  acc.Activities__c = 'Antiques & Rare Collections Repaire';
  acc.Sector_Classification__c = 'Ancillary Service Provider';
  acc.ROC_Status__c = 'Name Reserved';
  acc.Financial_Sector_Activities__c = ''; 
  acc.BD_Financial_Services_Activities__c = '';  
  insert acc;
     
  Opportunity opps = new  Opportunity();
  opps.AccountId = acc.Id;
  opps.Name = 'Shyam';
 // opps.Thompson_Reuters__c = 'Additional Information Required';
  opps.Security_Check__c = '';
  opps.StageName = 'Approved by RRC';
 // opps.Stage_Steps__c = 'Approved by RRC';
  opps.CloseDate = system.today();
  opps.Lead_Email__c = 'gsyadav19@gmail.com';
  opps.Transferred_to_ROC__c = False;
  system.debug('opps.Thompson_Reuters__c'+opps.Thompson_Reuters__c);
  system.debug('opps.Security_Check__c'+opps.Security_Check__c);
  system.debug('opps.Stage_Steps__c'+opps.Stage_Steps__c);
  opps.Activities__c = 'Antiques & Rare Collections Repaire';
  opps.Sector_Classification__c  = 'Halls and exibition';
  opps.RecordTypeId =  rt.id;
  insert opps;

 opportunity opp= [select id,Record_Type_Name__c,ROC_Transfarrable_for_NFS_Security__c from Opportunity where id=:opps.id];  
system.debug('opp.ROC_Transfarrable_for_NFS_Security__c'+opp.ROC_Transfarrable_for_NFS_Security__c);
  Test.startTest();
  opportunityTransferToROC.updateOpportunity(opp.Id);
 // CLS_CreateBP.createLicenseActivitiesFromOppty(opps.Id,opps.Activities__c);
         
 Test.stopTest();        
}
 }    
static testMethod void myUnitTest6() {
Id RecordTypeIdopportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('BD Financial').getRecordTypeId();
RecordType rt =   [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType = 'Opportunity' and DeveloperName = 'BD_Financial_Fund'];
system.debug('rt----'+rt);    
    
    UserRole r = new UserRole(DeveloperName = 'BusinessDevelopment', Name = 'BusinessDevelopment');
insert r;
User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
    FirstName = 'Shyam', 
    LastName = 'Yadav',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'TEST',
     Title = 'title',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US',
     UserRoleId = r.Id
);     
     system.runAs(u){  
  Account acc = new Account();
  acc.Name = 'Rex Constructor';
  acc.OwnerId = u.id;
  acc.Sector_Lead_lookup__c = u.Id;
 // acc.Sector_Lead_user__c        
  acc.Transferred_to_ROC__c = False;
  acc.Authorization_Group__c = 'Blocked by BDD Admin';
  acc.Company_Type__c = 'Retail';
  acc.Priority_to_DIFC__c = 'T1: Global with high FTE';
  acc.BD_Sector__c = 'Business Entities (BE)';
  acc.Activities__c = 'Antiques & Rare Collections Repaire';
  acc.Sector_Classification__c = 'Ancillary Service Provider';
  acc.ROC_Status__c = 'Not Renewed';
  acc.Financial_Sector_Activities__c = 'Exempt Fund'; 
  acc.BD_Financial_Services_Activities__c = 'Managing Assets';  
  insert acc;
     
  Opportunity opps = new  Opportunity();
  opps.AccountId = acc.Id;
  opps.Name = 'Shyam';
 // opps.Thompson_Reuters__c = 'Additional Information Required';
  opps.Security_Check__c = '';
  opps.StageName = 'Approved by RRC';
 // opps.Stage_Steps__c = 'Approved';
  opps.CloseDate = system.today();
  opps.Lead_Email__c = 'gsyadav19@gmail.com';
  opps.Transferred_to_ROC__c = True;
  system.debug('opps.Thompson_Reuters__c'+opps.Thompson_Reuters__c);
  system.debug('opps.Security_Check__c'+opps.Security_Check__c);
  system.debug('opps.Stage_Steps__c'+opps.Stage_Steps__c);
  opps.Activities__c = 'Antiques & Rare Collections Repaire';
  opps.Sector_Classification__c  = 'Halls and exibition';
  opps.RecordTypeId =  rt.id;

  insert opps;

 opportunity opp= [select id,Record_Type_Name__c,ROC_Transfarrable_for_NFS_Security__c from Opportunity where id=:opps.id];  
   system.debug('opp.ROC_Transfarrable_for_NFS_Security__c'+opp.ROC_Transfarrable_for_NFS_Security__c);
  Test.startTest();
  opportunityTransferToROC.updateOpportunity(opp.Id);
 // CLS_CreateBP.createLicenseActivitiesFromOppty(opps.Id,opps.Activities__c);
         
 Test.stopTest();        
}
 }    

    
}