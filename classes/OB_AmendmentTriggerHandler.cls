/******************************************************************************************
 *  Name        : OB_AmendmentTriggerHandler 
 *  Author      : Durga Kandula
 *  Company     : PwC
 *  Date        : 13-Nov-2019
 *  Description : Trigger Handler for OB_AmendmentTrigger
 ----------------------------------------------------------------------------------------                               
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   13-Nov-2019  Durga         Initial Version
 V1.1   27-Jan-2020  Rachita       populateValueFromAccount: if amendment related to existing account then populate 
                                    the Country of Registration and date of registration field
                                    on amendment record.
 v1.2   17-Mar-2020  Durga          Code to create the History Records.
 v1.3	26-Mar-2020  Maruf 			Added check to only update SR if body corp shareholer is updated
 v1.4   04/04/2020   Maruf			Added duplicate check on contentDocumentId by adding it to Set
*******************************************************************************************/
public without sharing class OB_AmendmentTriggerHandler{
    public static void OnBeforeInsert(list<HexaBPM_Amendment__c> TriggerNew){
        CalculateCountryRiskScore(TriggerNew,null);
        populateValueFromAccount(TriggerNew,null);
        OB_AmendmentTriggerHelper.PopulateMultiStructureEntityName(TriggerNew,null,true);
    }   
    public static void OnBeforeUpdate(list<HexaBPM_Amendment__c> TriggerOld,list<HexaBPM_Amendment__c> TriggerNew,map<Id,HexaBPM_Amendment__c> TriggerOldMap,map<Id,HexaBPM_Amendment__c> TriggerNewMap){
        CalculateCountryRiskScore(TriggerNew,TriggerOldMap);
        populateValueFromAccount(TriggerNew,TriggerOldMap);
        OB_AmendmentTriggerHelper.PopulateMultiStructureEntityName(TriggerNew,TriggerOldMap,false);
    }
    public static void OnBeforeDelete(list<HexaBPM_Amendment__c> TriggerOld,list<HexaBPM_Amendment__c> TriggerNew,map<Id,HexaBPM_Amendment__c> TriggerOldMap,map<Id,HexaBPM_Amendment__c> TriggerNewMap){
        deleteSRDocsAmendment(TriggerOld,TriggerOldMap);
    }
    public static void onAfterInsert(list<HexaBPM_Amendment__c> TriggerNew){
        rollUpAmedToSR(TriggerNew,null,null,null);
        OB_AmendmentTriggerHelper.CreateHistoryRecords(TriggerNew,null,true);//v1.2
    } 
    public static void OnAfterUpdate(list<HexaBPM_Amendment__c> TriggerOld,list<HexaBPM_Amendment__c> TriggerNew,map<Id,HexaBPM_Amendment__c> TriggerOldMap,map<Id,HexaBPM_Amendment__c> TriggerNewMap){
        rollUpAmedToSR(TriggerNew,TriggerNewMap,TriggerOld,TriggerOldMap);
        OB_AmendmentTriggerHelper.CreateHistoryRecords(TriggerNew,TriggerOldMap,false);//v1.2
    }
    public static void OnAfterDelete(list<HexaBPM_Amendment__c> TriggerOld,list<HexaBPM_Amendment__c> TriggerNew,map<Id,HexaBPM_Amendment__c> TriggerOldMap,map<Id,HexaBPM_Amendment__c> TriggerNewMap){
        rollUpAmedToSR(null,null,TriggerOld,TriggerOldMap);
    }
    public static void deleteSRDocsAmendment(list<HexaBPM_Amendment__c> TriggerOld,map<Id,HexaBPM_Amendment__c> TriggerOldMap){
        if(TriggerOld != NULL && TriggerOld.size() > 0){
           Map<id,HexaBPM__SR_Doc__c> mapSRDocs= new Map<id,HexaBPM__SR_Doc__c>([SELECT Id FROM HexaBPM__SR_Doc__c WHERE HexaBPM_Amendment__c =:TriggerOldMap.keySet()]);
           list<ContentDocument> conDocToDelete = new List<ContentDocument>();
           // v1.4
           Set<ContentDocument> conDocToDeleteSet = new Set<ContentDocument>();
           if( mapSRDocs != NULL && mapSRDocs.size() > 0 ){
               for(contentDocumentLink  cntDocLink : [SELECT id,
                                                             ContentDocument.Id   
                                                       FROM contentDocumentLink 
                                                      WHERE LinkedEntityId =:mapSRDocs.keySet()]){
                 // v1.4
                 conDocToDeleteSet.add(new ContentDocument(id = cntDocLink.ContentDocument.Id ) );
               }
           
           }                                       
           // v1.4
           if(conDocToDeleteSet != NULL){
               conDocToDelete.addAll(conDocToDeleteSet);
               delete conDocToDelete;
           }  
           if(mapSRDocs != NULL && mapSRDocs.size() > 0){
               delete mapSRDocs.values();
           }                                      
        }
    }
    //Merul: To set Body_Corporate_Shareholder__c  if BC amed exist 
    public static void rollUpAmedToSR(list<HexaBPM_Amendment__c> TriggerNew,
                                      map<Id,HexaBPM_Amendment__c> TriggerNewMap,
                                      list<HexaBPM_Amendment__c> TriggerOld,
                                      map<Id,HexaBPM_Amendment__c> TriggerOldMap){
        
        system.debug('$$$$$ In rollUpAmedToSR ');
        Set<Id> srIds = new Set<Id>();
        List<HexaBPM__Service_Request__c> srLstUpdate = new List<HexaBPM__Service_Request__c>();
        if(Trigger.isInsert || Trigger.isUndelete){
            for(HexaBPM_Amendment__c amed : (List<HexaBPM_Amendment__c>)Trigger.new)
            {
                srIds.add(amed.ServiceRequest__c);
            }
        }
        
        if(Trigger.isUpdate || Trigger.isdelete)
        {
            
            for(HexaBPM_Amendment__c amed : (List<HexaBPM_Amendment__c>)Trigger.old)
            {
                                
                if( Trigger.isUpdate 
                        && TriggerNewMap != NULL 
                            && TriggerOldMap != NULL
                                && TriggerOldMap.get(amed.Id).Role__c != TriggerNewMap.get(amed.Id).Role__c
                  )
                {
                    system.debug('inside change');
                    srIds.add(amed.ServiceRequest__c);
                }
                else if(Trigger.isdelete)
                {
                     srIds.add(amed.ServiceRequest__c);
                }
                
                
            }
        }
        
        
        system.debug('$$$$$ In rollUpAmedToSR srIds'+srIds);
        
        for(HexaBPM__Service_Request__c srObj : [
                                                 SELECT id,Body_Corporate_Shareholder__c,
                                                        (SELECT id,
                                                                RecordType.Id,
                                                                RecordType.DeveloperName,
                                                                Role__c 
                                                           FROM Amendments__r) 
                                                   FROM HexaBPM__Service_Request__c 
                                                  WHERE id IN:srIds
                                                ])
        {
            //v1.3 Added check to only update SR if body corp shareholer is updated
            boolean bodyCorpSh = false;
           // srObj.Body_Corporate_Shareholder__c = false;
            for( HexaBPM_Amendment__c amed : srObj.Amendments__r)
            {

                //recordtypr check remaining.
                if( amed.RecordType.DeveloperName == 'Body_Corporate'
                                  && amed.Role__c != NULL 
                                      && amed.Role__c.contains('Shareholder') 
                  )
                {
                    system.debug('$$$$$ Inside true '+amed.RecordType.DeveloperName );
                   // srObj.Body_Corporate_Shareholder__c = true;
                    bodyCorpSh = true;
                }
            }
            if(bodyCorpSh != srObj.Body_Corporate_Shareholder__c){
                srObj.Body_Corporate_Shareholder__c = bodyCorpSh;
          	    srLstUpdate.add(srObj);
            }
        }
         system.debug('$$$$$ In srLstUpdate '+srLstUpdate);
        
        if( srLstUpdate.size() > 0 )
        {
            update srLstUpdate;
        }
        
    }
    
    /*
    * if amendment related to existing account then populate the Country of Registration and date of registration field
    * on amendment record.
    **/
    
    public static void populateValueFromAccount(list<HexaBPM_Amendment__c> TriggerNew,
                                                 map<Id,HexaBPM_Amendment__c> TriggerOldMap)
    {
        Boolean isUpdate = TriggerOldMap != null && TriggerOldMap.size() != 0?true:false;
        Map<string,List<HexaBPM_Amendment__c>> accountIDAmendmentListMap = new Map<string,List<HexaBPM_Amendment__c>>();
        
        for(HexaBPM_Amendment__c itr:TriggerNew){
            if(String.IsNotBlank(itr.Entity_Name__c)){// in case of new
                if(!isUpdate)
                {
                    if( (String.isBlank(itr.Place_of_Registration__c) || itr.Registration_Date__c == null) )
                    {
                         
                         if(accountIDAmendmentListMap.containskey(itr.Entity_Name__c))
                         {
                                accountIDAmendmentListMap.get(itr.Entity_Name__c).add(itr);
                         }
                         else
                         {
                                accountIDAmendmentListMap.put(itr.Entity_Name__c,new List<HexaBPM_Amendment__c>{itr});
                         }
                    
                    }
                }
                else
                {// in case of update
                    HexaBPM_Amendment__c oldAmnd = TriggerOldMap.get(itr.Id);
                    if(oldAmnd != null && itr.Entity_Name__c != oldAmnd.Entity_Name__c){
                        if(accountIDAmendmentListMap.containskey(itr.Entity_Name__c)){
                            accountIDAmendmentListMap.get(itr.Entity_Name__c).add(itr);
                        }else{
                            accountIDAmendmentListMap.put(itr.Entity_Name__c,new List<HexaBPM_Amendment__c>{itr});
                        }
                    } 
                }
            }
        }
        
        // popultate Account: place of registration on Amendment:Place_of_Registration__c 
        // Account:Registration / Incorporation Date on Amendment: Registration_Date__c
        if(!test.isRunningTest()){
        for(Account acc:[Select Id,ROC_reg_incorp_Date__c,Place_of_Registration__c 
                         From Account Where ID IN:accountIDAmendmentListMap.keyset()]){
            for(HexaBPM_Amendment__c amnd: accountIDAmendmentListMap.get(acc.id)){
                amnd.Place_of_Registration__c = acc.Place_of_Registration__c;
                amnd.Registration_Date__c = acc.ROC_reg_incorp_Date__c;
            }
        }
        }
    }
    public static void CalculateCountryRiskScore(list<HexaBPM_Amendment__c> TriggerNew,
                                                 map<Id,HexaBPM_Amendment__c> TriggerOldMap)
    {
        set<string> setCountryNames = new set<string>();
        map<string,Country_Risk_Scoring__c> MapCountryRiskScoring = new map<string,Country_Risk_Scoring__c>();
        map<string,string> MapSRRiskRating = new map<string,string>();
        Boolean isUpdate = TriggerOldMap != null && TriggerOldMap.Size() != 0 ? true:false;
        for(HexaBPM_Amendment__c amnd:TriggerNew){
            
            if(isUpdate){
                HexaBPM_Amendment__c oldAmnd = TriggerOldMap.get(amnd.Id);
                if(oldAmnd != null && oldAmnd.Role__c != amnd.Role__c){
                    // populate roles field value
                    populateRoleValue(amnd);
                }
            }else{
                // populate roles field value
                populateRoleValue(amnd);
                
            }
            
            if(amnd.Nationality_list__c!=null){
                setCountryNames.add(amnd.Nationality_list__c);
            }
        }
        system.debug('setCountryNames===>'+setCountryNames);
        if(setCountryNames.size()>0){
            for(Country_Risk_Scoring__c CRS:[select Id,Name,Risk_Rating__c,Risk_Score_Percentage__c from Country_Risk_Scoring__c where Name IN:setCountryNames]){
                MapCountryRiskScoring.put(CRS.Name.tolowercase(),CRS);
            }
        }
        system.debug('MapCountryRiskScoring===>'+MapCountryRiskScoring);
        for(HexaBPM_Amendment__c amnd:TriggerNew){
            if(amnd.Nationality_list__c!=null && MapCountryRiskScoring.get(amnd.Nationality_list__c.toLowerCase())!=null){
                amnd.Country_Risk_Scoring__c = MapCountryRiskScoring.get(amnd.Nationality_list__c.toLowerCase()).Id;
            }else{
                amnd.Country_Risk_Scoring__c = null;
            }
        }
    }
    
    /*
    * This will populate the : Roles__c field based on Type of Entity and role field present on amendment
    */
    public static void populateRoleValue(HexaBPM_Amendment__c amnd){
            system.debug(amnd.Type_of_Entity__c+'---populateRoleValue-------'+amnd.Role__c);
            if(String.IsBlank(amnd.Role__c)){
                amnd.Roles__c = '';
            }else{
                amnd.Roles__c = amnd.Role__c; // assign Role
                if(amnd != null && String.IsNotBlank(amnd.Type_of_Entity__c)){
                    // for Private or Public
                    /*if((amnd.Type_of_Entity__c.equalsIgnorecase('Private') 
                        || amnd.Type_of_Entity__c.equalsIgnorecase('Public'))){
                            // nothing to do
                        
                    }*/
                    // for General Partnership (GP) or Limited Partnership (LP) or Limited Liability Partnership (LLP)
                    if(amnd.Type_of_Entity__c.equalsIgnorecase('General Partnership (GP)')){
                        if(amnd.Role__c.contains('Shareholder')){
                            amnd.Roles__c = amnd.Roles__c.replace('Shareholder','General Partner');
                        }   
                    }
                    if(amnd.Type_of_Entity__c.equalsIgnorecase('Limited Partnership (LP)')){
                        if(amnd.Role__c.contains('Shareholder')){
                            amnd.Roles__c = amnd.Roles__c.replace('Shareholder','Partner');
                        }
                    }
                    if(amnd.Type_of_Entity__c.equalsIgnorecase('Limited Liability Partnership (LLP)')){
                        
                        if(amnd.Role__c.contains('Shareholder')){
                            amnd.Roles__c = amnd.Roles__c.replace('Shareholder','Member');
                        }
                    }
                    //Foundation
                    if(amnd.Type_of_Entity__c.equalsIgnorecase('Foundation')){
                        if(amnd.Role__c.contains('Shareholder')){
                            amnd.Roles__c = amnd.Roles__c.replace('Shareholder','Founder');
                        }
                        if(amnd.Role__c.contains('Director')){
                            amnd.Roles__c = amnd.Roles__c.replace('Director','Council Member');
                        }
                    }
                    //NPIO
                    if(amnd.Type_of_Entity__c.equalsIgnorecase('NPIO')){
                        if(amnd.Role__c.contains('Shareholder')){
                            amnd.Roles__c = amnd.Roles__c.replace('Shareholder','Founding Member');
                        }
                        if(amnd.Role__c.contains('Company Secretary')){
                            amnd.Roles__c = amnd.Roles__c.replace('Company Secretary','Secretary ');
                        }
                    }
                    //Recognized Company
                    if(amnd.Type_of_Entity__c.equalsIgnorecase('Recognized Company')){
                        if(amnd.Role__c.contains('Shareholder')){
                            amnd.Roles__c = amnd.Roles__c.replace('Shareholder','Foreign Entity Shareholder');
                        }
                    }
                    //Recognized Limited Partnership (RLP)
                    if(amnd.Type_of_Entity__c.equalsIgnorecase('Recognized Limited Partnership (RLP)')){
                        if(amnd.Role__c.contains('Shareholder')){
                            amnd.Roles__c = amnd.Roles__c.replace('Shareholder','Foreign Limited Partnership');
                        }
                    }
                    //Recognized Limited Liability Partnership (RLLP)
                    if(amnd.Type_of_Entity__c.equalsIgnorecase('Recognized Limited Liability Partnership (RLLP)')){
                        if(amnd.Role__c.contains('Shareholder')){
                            amnd.Roles__c = amnd.Roles__c.replace('Shareholder','Foreign Limited Liability Partnership');
                        }
                    }
                    //Recognized Partnership (RP)
                    if(amnd.Type_of_Entity__c.equalsIgnorecase('Recognized Partnership (RP)')){
                        if(amnd.Role__c.contains('Shareholder')){
                            amnd.Roles__c = amnd.Roles__c.replace('Shareholder','Foreign Partnership');
                        }
                    }
                }
            }
        
        }
        
}