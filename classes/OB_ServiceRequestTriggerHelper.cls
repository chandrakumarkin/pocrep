/******************************************************************************************
 *  Author      : Durga Prasad
 *  Company     : PwC
 *  Date        : 14-Dec-2019     
 *  Description : helper class for OB_SRTriggerHandler
 *  Modification History :
 ----------------------------------------------------------------------------------------
    V.No    Date          Updated By    Description
    =====   ===========   ==========    ============
    v.1.2   03/Feb/2020   Durga         Invoking the helper method to create History Track records.
        
*******************************************************************************************/
public without sharing class OB_ServiceRequestTriggerHelper {
	public static void SubmitMultiStructureApplications(list<HexaBPM__Service_Request__c> TriggerNew){
		list<HexaBPM__Service_Request__c> lstMultiStructureApps = new list<HexaBPM__Service_Request__c>();
		for(HexaBPM__Service_Request__c ObjSR:[Select Id,HexaBPM__Parent_SR__r.HexaBPM__Internal_SR_Status__c,HexaBPM__Internal_SR_Status__c,HexaBPM__External_SR_Status__c,HexaBPM__Internal_Status_Name__c from HexaBPM__Service_Request__c where HexaBPM__Parent_SR__c IN:TriggerNew and HexaBPM__Internal_Status_Name__c='Draft' and RecordType.DeveloperName='Multi_Structure']){
			ObjSR.HexaBPM__Internal_SR_Status__c = ObjSR.HexaBPM__Parent_SR__r.HexaBPM__Internal_SR_Status__c;
			ObjSR.HexaBPM__External_SR_Status__c = ObjSR.HexaBPM__Parent_SR__r.HexaBPM__Internal_SR_Status__c;
			lstMultiStructureApps.add(ObjSR);
		}
		try{
			if(lstMultiStructureApps.size()>0)
				update lstMultiStructureApps;
		}catch(Exception e){
			insert LogDetails.CreateLog(null, 'OB_ServiceRequestTriggerHelper : SubmitMultiStructureApplications', 'Line Number : '+e.getLineNumber()+'\nException is : '+e.getMessage());
		}
	}
    public static void CreateBCAmendment(list<HexaBPM__Service_Request__c> TriggerNew,map<Id,HexaBPM__Service_Request__c> TriggerOldMap){
        list<HexaBPM_Amendment__c> listOBAmendments = new list<HexaBPM_Amendment__c>();
        set<string> setRegistrationNumber = new set<string>();
        for(HexaBPM__Service_Request__c objSR:TriggerNew){
            if(TriggerOldMap==null && objSR.Foreign_entity_registered_number__c!=null){
                setRegistrationNumber.add(objSR.Foreign_entity_registered_number__c);
            }else{
                if(TriggerOldMap!=null && objSR.Foreign_entity_registered_number__c!=TriggerOldMap.get(objSR.Id).Foreign_entity_registered_number__c && TriggerOldMap.get(objSR.Id).Foreign_entity_registered_number__c!=null){
                    setRegistrationNumber.add(objSR.Foreign_entity_registered_number__c);
                }
            }
        }
        system.debug('setRegistrationNumber==>'+setRegistrationNumber);
        if(setRegistrationNumber.size()>0){
            map<string,string> MapExistingBCShareHolders = new map<string,string>();
            for(HexaBPM_Amendment__c Amnd:[Select Id,Registration_No__c,ServiceRequest__c from HexaBPM_Amendment__c where ServiceRequest__c IN:TriggerNew and Registration_No__c IN:setRegistrationNumber and Role__c INCLUDES('Shareholder')]){
                MapExistingBCShareHolders.put(Amnd.Registration_No__c+'-'+Amnd.ServiceRequest__c,Amnd.Id);
            }
            if(TriggerOldMap!=null && TriggerOldMap.size()>0){
                for(HexaBPM__Service_Request__c objSR:TriggerNew){
                    if(objSR.Setting_Up__c=='Branch' && objSR.legal_structures__c=='Company' && objSR.Foreign_entity_registered_number__c!=null){
                        HexaBPM_Amendment__c objAmnd = new HexaBPM_Amendment__c();
                        objAmnd = MapAmendmentFields(objSR);
                        if(MapExistingBCShareHolders.get(objSR.Foreign_entity_registered_number__c+'-'+objSR.Id)!=null)
                            objAmnd.Id = MapExistingBCShareHolders.get(objSR.Foreign_entity_registered_number__c+'-'+objSR.Id);
                        else if(TriggerOldMap.get(objSR.Id).Foreign_entity_registered_number__c!=null && MapExistingBCShareHolders.get(TriggerOldMap.get(objSR.Id).Foreign_entity_registered_number__c+'-'+objSR.Id)!=null)
                            objAmnd.Id = MapExistingBCShareHolders.get(TriggerOldMap.get(objSR.Id).Foreign_entity_registered_number__c+'-'+objSR.Id);
                        
                        if(objAmnd.Id==null)
                            objAmnd.Role__c = 'Foreign Company';
                        listOBAmendments.add(objAmnd);
                    }
                }
            }else{
                for(HexaBPM__Service_Request__c objSR:TriggerNew){
                    if(objSR.Setting_Up__c=='Branch' && objSR.legal_structures__c=='Company' && objSR.Foreign_entity_registered_number__c!=null){
                        HexaBPM_Amendment__c objAmnd = new HexaBPM_Amendment__c();
                        objAmnd = MapAmendmentFields(objSR);
                        objAmnd.Role__c = 'Foreign Company';
                        listOBAmendments.add(objAmnd);
                    }
                }
            }
        }
        system.debug('listOBAmendments==>'+listOBAmendments);
        if(listOBAmendments.size()>0){
            try{
                upsert listOBAmendments;
            }catch(DMLException e){
                    
            }
        }
    }
    /*
        Method Name :   MapAmendmentFields
        Description :   Method to map the Application fields to Application Amendment
    */
    public static HexaBPM_Amendment__c MapAmendmentFields(HexaBPM__Service_Request__c objSR){
        HexaBPM_Amendment__c objAmnd = new HexaBPM_Amendment__c();
        if(Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByDeveloperName().get('Body_Corporate')!=null)
            objAmnd.RecordTypeId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByDeveloperName().get('Body_Corporate').getRecordTypeId();
        
        objAmnd.ServiceRequest__c = objSR.Id;
        objAmnd.Customer__c = objSR.HexaBPM__Customer__c;
        
        objAmnd.Registration_No__c = objSR.Foreign_entity_registered_number__c;
        objAmnd.Company_Name__c = objSR.Foreign_Entity_Name__c;
        objAmnd.Former_Name__c = objSR.Foreign_entity_former_name__c;
        objAmnd.Registration_Date__c = objSR.Foreign_entity_date_of_registration__c;
        objAmnd.Date_of_Registration__c = objSR.Foreign_entity_date_of_registration__c;
        objAmnd.Place_of_Registration__c = objSR.Foreign_entity_place_of_registration__c;
        objAmnd.Country_of_Registration__c = objSR.Foreign_entity_country_of_registration__c;
        objAmnd.Telephone_No__c = objSR.Foreign_entity_telephone_number__c;
        
        objAmnd.Address__c = objSR.Foreign_entity_address_line_1__c;
        objAmnd.Apartment_or_Villa_Number_c__c = objSR.Foreign_entity_address_line_2__c;
        objAmnd.Permanent_Native_City__c = objSR.Foreign_entity_city_town__c;
        objAmnd.Permanent_Native_Country__c = objSR.Foreign_entity_country__c;
        objAmnd.Emirate_State_Province__c = objSR.Foreign_entity_state_province_region__c;
        objAmnd.PO_Box__c = objSR.Foreign_entity_Po_Box_Postal_Code__c;
        
        objAmnd.Title__c = objSR.Authorised_Individual_Title__c;
        objAmnd.First_Name__c = objSR.Authorised_individual_first_name__c;
        objAmnd.Last_Name__c = objSR.Authorized_individual_last_name__c;
        objAmnd.Email__c = objSR.Authorised_individual_email__c;
        objAmnd.Mobile__c = objSR.Authorised_individual_telephone_no__c;
        objAmnd.Is_this_Entity_registered_with_DIFC__c = 'No';
        return objAmnd;
    }
    /*
        Method Name :   CreateHistoryRecords
        Description :   Method to create the Field History Tracking object
    */
    public static void CreateHistoryRecords(list<HexaBPM__Service_Request__c> TriggerNew,map<Id,HexaBPM__Service_Request__c> TriggerOldMap,boolean IsInsert){
        map<string,string> mapHistoryFields = new map<string,string>();
        map<string,History_Tracking__c> mapHistoryCS = new map<string,History_Tracking__c>();
        if(History_Tracking__c.getAll()!=null){
            mapHistoryCS = History_Tracking__c.getAll();//Getting the Objects from Custom Setting which has to be displayed in the screen for the users
            for(History_Tracking__c objHisCS:mapHistoryCS.values()){
                system.debug('objHisCS==>'+objHisCS);
                if(objHisCS.Object_Name__c!=null && objHisCS.Object_Name__c.tolowercase()=='hexabpm__service_request__c' && objHisCS.Field_Name__c!=null){
                    mapHistoryFields.put(objHisCS.Field_Name__c,objHisCS.Field_Label__c);
                }
            }
        }
        system.debug('mapHistoryCS==>'+mapHistoryCS);
        system.debug('mapHistoryFields==>'+mapHistoryFields);
        list<sobject> lstApplicationHistory = new list<sobject>();
        if(IsInsert){
        	/*
            for(HexaBPM__Service_Request__c app:TriggerNew){
                if(mapHistoryFields!=null && mapHistoryFields.size()>0){
                    for(string objFld:mapHistoryFields.keyset()){
                        string NewValue = '';
                        if(app.get(objFld)!=null)
                            NewValue = app.get(objFld)+'';
                        sobject objHistory = OB_ObjectHistoryTrackingUtil.CreateObjectHistory('Application_History__c',NewValue,'',app.Id,mapHistoryFields.get(objFld),objFld);
                        lstApplicationHistory.add(objHistory);
                    }
                }
            }
            */
        }else{
            system.debug('***Update Block***');
            for(HexaBPM__Service_Request__c app:TriggerNew){
            	if(app.HexaBPM__Internal_Status_Name__c!=null && app.HexaBPM__Internal_Status_Name__c.tolowercase()!='draft'){
	                if(mapHistoryFields!=null && mapHistoryFields.size()>0 && TriggerOldMap.get(app.Id)!=app){
	                    for(string objFld:mapHistoryFields.keyset()){
	                        if(app.get(objFld) != trigger.oldMap.get(app.Id).get(objFld)){
	                            string OldValue = '';
	                            string NewValue = '';
	                            system.debug('OldValue==>'+OldValue);
	                            system.debug('NewValue==>'+NewValue);
	                            if(app.get(objFld)!=null)
	                                NewValue = app.get(objFld)+'';
	                            if(trigger.oldMap.get(app.Id).get(objFld)!=null)
	                                OldValue = trigger.oldMap.get(app.Id).get(objFld)+'';
	                            sobject objHistory = OB_ObjectHistoryTrackingUtil.CreateObjectHistory('Application_History__c',NewValue,OldValue,app.Id,mapHistoryFields.get(objFld),objFld);
	                            lstApplicationHistory.add(objHistory);
	                        }
	                    }
	                }
            	}
            }
        }
        system.debug('lstApplicationHistory==>'+lstApplicationHistory);
        if(lstApplicationHistory.size()>0)
            insert lstApplicationHistory;
    }
    
    public static void buyCarParkHours(list<HexaBPM__Service_Request__c> newList, Map<Id, HexaBPM__Service_Request__c> oldMap){
        Id BuyCarParkHours_RecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('Buy_Car_Parking_Hours_Request').getRecordTypeId();
        List<HexaBPM__Service_Request__c> srRecList = new List<HexaBPM__Service_Request__c>();
        set<Id> accIdSet = new Set<Id>();
        CreateCompanyWrapper reqWrapObj = new CreateCompanyWrapper();
        for(HexaBPM__Service_Request__c srRec: newList){
            if(srRec.RecordTypeId == BuyCarParkHours_RecId && srRec.HexaBPM__External_Status_Name__c == 'Approved'
              && srRec.HexaBPM__External_SR_Status__c != oldMap.get(srRec.Id).HexaBPM__External_SR_Status__c
              && srRec.Parking_Hours__c != null && srRec.HexaBPM__Customer__c != null)
            {
                srRecList.add(srRec);
                accIdSet.add(srRec.HexaBPM__Customer__c);
            }
        }
        if(srRecList != null && srRecList.size()>0){
            Map<Id, Account> accMap = new Map<Id, Account>([Select Id, Name, BP_No__c from Account where id in :accIdSet]);
            for(HexaBPM__Service_Request__c srObj: srRecList){
                if(accMap.containsKey(srObj.HexaBPM__Customer__c) && accMap.get(srObj.HexaBPM__Customer__c).BP_No__c != null){
                    reqWrapObj = new CreateCompanyWrapper();
                    reqWrapObj.ID = Integer.valueOf(accMap.get(srObj.HexaBPM__Customer__c).BP_No__c);
                    reqWrapObj.Name = accMap.get(srObj.HexaBPM__Customer__c).Name;
                    reqWrapObj.MultipleDiscount = true;
                    String jsonReq = JSON.serialize(reqWrapObj);
                    createCompany(jsonReq, JSON.serialize(accMap.get(srObj.HexaBPM__Customer__c)), JSON.serialize(srObj));
                }
            }
        }
    }
    
    @future(callout=true)
    public static void createCompany(String reqBody, String accRec, String srRec){
        sendDataToKTC(reqBody, accRec, srRec);
    }
    
    @AuraEnabled
    public static Boolean sendDataToKTC(String reqBody, String accRec, String srRec){
        HexaBPM__Service_Request__c updateSRRec = new HexaBPM__Service_Request__c();
        HttpResponse response1;
        ResponseWrapper resObj;
        Account accountRec = (Account)JSON.deserialize(accRec, Account.Class);
        HexaBPM__Service_Request__c srObj = (HexaBPM__Service_Request__c)JSON.deserialize(srRec, HexaBPM__Service_Request__c.class);
        HttpResponse response = sendHttpRequest(reqBody, 'http://ktcsupport.dyndns.org:8800/WebDiscountAPI/api/companies');
        ResponseWrapper obj = parseResponse(response);
        if(obj.statusCode == 400){
            if(obj.message.containsIgnoreCase('already taken')){
                obj.success = true;
            }
        }
        //create customer
        if(obj.success){
            User userRec = [Select id, ContactId, Email, Name, FirstName, LastName, UserName, 
                            UserRoleId, UserRole.Name from User where id =: UserInfo.getUserId()];
			CreateCustomerWrapper customerWrapObj = new CreateCustomerWrapper();
            //customerWrapObj.CompanyID = Integer.valueOf(accountRec.BP_No__c);
			customerWrapObj.CustomerID = UserInfo.getUserId();
            customerWrapObj.CompanyID = Integer.valueOf(accountRec.BP_No__c);
            customerWrapObj.UserName = userRec.UserName;
            customerWrapObj.Role = 'Operator';//userRec.UserRole != null ? userRec.UserRole.Name : 'Operator';
            customerWrapObj.FirstName = userRec.FirstName;
            customerWrapObj.LastName = userRec.LastName;
            customerWrapObj.Email = userRec.Email;
            customerWrapObj.Comments = 'This is a customer';
            response1 = sendHttpRequest(JSON.serialize(customerWrapObj), 'http://ktcsupport.dyndns.org:8800/WebDiscountAPI/api/Customers');
            resObj = parseResponse(response1);
            System.debug('resObj: '+resObj);
            if(resObj.statusCode == 400){
                
                if(resObj.message.containsIgnoreCase('already taken')){
                    resObj.success = true;
                }
            }
            //link company and customer
            if(resObj.success){
                /*LinkCustomerWrapper custWrapObj = new LinkCustomerWrapper();
                custWrapObj.CompanyID = Integer.valueOf(accountRec.BP_No__c);
                custWrapObj.CustomerID = UserInfo.getUserId();
                System.debug('custWrapObj: '+JSON.serialize(custWrapObj));
                HttpResponse res = sendHttpRequest(JSON.serialize(custWrapObj), 'http://ktcsupport.dyndns.org:8800/WebDiscountAPI/api/CompanyCustomers');
            	ResponseWrapper linkCustRes = parseResponse(res);
                System.debug('linkCustRes: '+linkCustRes);
                if(linkCustRes.success){*/
                    DiscountAllocationWrapper discountAllObj = new DiscountAllocationWrapper();
                    discountAllObj.CompanyID = Integer.valueOf(accountRec.BP_No__c);
                    discountAllObj.DiscountItemId = 1;//always send as 1
                    discountAllObj.ExpirationTypeId = 3;//static value
                    discountAllObj.Quantity = Integer.valueOf(srObj.Parking_Hours__c);
                    discountAllObj.Duration = 999;//static value//999 days
                    discountAllObj.AddedByCustomerID = UserInfo.getUserId();
                    HttpResponse response2 = sendHttpRequest(JSON.serialize(discountAllObj, true), 'http://ktcsupport.dyndns.org:8800/WebDiscountAPI/api/DiscountAllocations');
                    ResponseWrapper finalResponse = parseResponse(response2);
                    if(finalResponse.success){
                        DiscountAllocationWrapper finalRes = (DiscountAllocationWrapper)JSON.deserialize(finalResponse.resBody, DiscountAllocationWrapper.class);
                        updateSRRec.Id = srObj.Id;
                        updateSRRec.Discount_Allocation_Id__c = String.valueOf(finalRes.DiscountAllocationId);
                        update updateSRRec;
                        return true;
                    }
                    else{
                        //send an email
                        System.debug('finalResponse.message: '+finalResponse.message);
                        sendMail(srObj.Id);
                        return false;
                    }
                /*}
                else{
                    //send an email
                    System.debug('linkCustRes.message: '+linkCustRes.message);
                }*/
            }
            else{
                //send an email
                System.debug('resObj.message: '+resObj.message);
                sendMail(srObj.Id);
                return false;
            }
        }
        else{
            //send an email
            System.debug('obj.message: '+obj.message);
            sendMail(srObj.Id);
            return false;
        }
        /*if(updateSRRec.Id != null){
            update updateSRRec;
            return true;
        }*/
    }
    
    public static HttpResponse sendHttpRequest(String reqBody, String endpointURL){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        Blob headerValue = Blob.valueOf('webservice' + ':' + '123456');        
        String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);        
        //request.setHeader('Authorization', authorizationHeader);
        request.setHeader('Content-Type', 'application/json');
        request.setEndpoint(endpointURL);
        request.setMethod('POST');
        request.setBody(reqBody);
        system.debug('Request: '+request);
        HttpResponse response = http.send(request);
        System.debug('status: '+response.getStatusCode()+', response: '+response.getBody());
        return response;
    }
    
    public static ResponseWrapper parseResponse(HttpResponse response){
        ResponseWrapper obj = new ResponseWrapper();
        if(response != null){
            obj.success = false;
            obj.resBody = response.getBody();
            obj.message = '';
            obj.statusCode = response.getStatusCode();
            if(response.getStatusCode() == 201 || response.getStatusCode() == 200){
                System.debug('response: '+response.getBody());
                obj.success = true;
            }
            else{
                System.debug('error response: '+response.getBody());
                obj.success = false;
                if(response.getBody() != null && response.getBody() != ''){
                    Map<String, Object> objMap = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
                    obj.message = (String)objMap.get('Message');
                }
                else if(response.getStatusCode() == 401){
                    obj.message = 'Unuthorized';
                }
            }
        }
        else{
            
        }
        System.debug('obj+++++++++: '+obj);
        return obj;
    }
    @AuraEnabled
    public static void sendMail(String srId){
      	String templateId = [select Id,Name,Subject,body from EmailTemplate where DeveloperName='Buy_Car_Park_Hours_API_Failure_Notification'].id;
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //Contact cont = [Select id from contact];
        String[] toAddresses = new String[] {'c-sabeeha.syed@difc.ae'}; 
        mail.setToAddresses(toAddresses);
        mail.setTargetObjectId('0030D00000PBrCF');
		mail.setTemplateId(templateId);
        mail.saveAsActivity = false;
        mail.setWhatId(srId);
		try{
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }catch(exception e){
            System.debug('Send Email failed: '+e.getMessage());
        }
    }
    
    public class ResponseWrapper{
        @AuraEnabled public Boolean success;
        @AuraEnabled public String resBody;
        @AuraEnabled public String message;
        @AuraEnabled public Integer statusCode;
    }
            
    public class CreateCompanyWrapper{
        @AuraEnabled public Integer ID;
        @AuraEnabled public String Name;
        @AuraEnabled public String Address;
        @AuraEnabled public String Email;
        @AuraEnabled public String ContactNo;
        @AuraEnabled public String Comments;
        @AuraEnabled public Boolean MultipleDiscount;
    }
    
    public class CreateCustomerWrapper{
        @AuraEnabled public String CustomerID;
        @AuraEnabled public Integer CompanyID;
        @AuraEnabled public String UserName;
        @AuraEnabled public String Role;
        @AuraEnabled public String FirstName;
        @AuraEnabled public String LastName;
        @AuraEnabled public String Email;
        @AuraEnabled public String Comments;
    }
    
    public class LinkCustomerWrapper{
        @AuraEnabled public Integer CompanyID;
        @AuraEnabled public String CustomerID;//should acceptalphanumeric
    }
    
    public class DiscountAllocationWrapper{
        @AuraEnabled public Integer CompanyID;
        @AuraEnabled public Integer DiscountItemId;
        @AuraEnabled public Integer ExpirationTypeId;
        @AuraEnabled public Integer Quantity;
        @AuraEnabled public Integer Duration;
        @AuraEnabled public String AddedByCustomerID;//should acceptalphanumeric
        @AuraEnabled public Integer DiscountAllocationId;
        @AuraEnabled public String ExpirationTime;
        @AuraEnabled public String DateAdded;
    }
}