/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 11-Dec-2014   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class TestAmendmentFieldMapping {

    public static testmethod void testAFM() {
        Cls_AmendmentFieldMappingUtil AFMCls = new Cls_AmendmentFieldMappingUtil();
        Amendment__c amend =new Amendment__c();
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.Authorized_Share_Capital__c = 500000;
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971232342345';
        objSR.legal_structures__c ='LLC';
        objSR.currency_list__c = 'US Dollar';
        objSR.Share_Capital_Membership_Interest__c = 50000;
        objSR.No_of_Authorized_Share_Membership_Int__c = 560;
        insert objSR;
        Lookup__c nat = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat;
        
        Contact cntct = new Contact();
        Account accnt = new Account();
        Cls_AmendmentFieldMappingUtil.getObjectContact(cntct,amend,'Director');
        Cls_AmendmentFieldMappingUtil.getObjectAccount(accnt,amend,'Director');
        amend.ServiceRequest__c=objSR.id;
        amend.Title_new__c = 'Mrs.';
        amend.Family_Name__c = 'Mrs.';
        amend.Given_Name__c = 'Mrs.';
        amend.Date_of_Birth__c=Date.newinstance(1980,10,17);
        amend.Place_of_Birth__c = 'Pakistan';
        amend.Passport_No__c = 'ASD122323';
        amend.Nationality__c = nat.id;
        amend.Nationality_list__c = 'Pakistan';
        amend.Phone__c = '+971562019803';
        amend.Person_Email__c = 'abc@nsigulf.com';
        amend.First_Name_Arabic__c ='AbcTest';
        amend.Occupation__c='Developer';
        amend.Apt_or_Villa_No__c='villa';
        amend.Building_Name__c='building';
        amend.Street__c='street';
        amend.PO_Box__c = '45231435';
        amend.Permanent_Native_City__c='Karachi';
        amend.Emirate_State__c='Emairate';
        amend.Post_Code__c='2322';
        amend.Emirate__c='Dubai';
        amend.Permanent_Native_Country__c = 'Pakistan';
        amend.Is_Designated_Member__c = true;
        amend.Company_Name__c = 'Company';
        amend.Former_Name__c = 'Former';
        amend.Registration_Date__c = Date.newinstance(1980,10,17);
        amend.Place_of_Registration__c = 'Registration';
        
        insert amend;
        Cls_AmendmentFieldMappingUtil.getObjectContact(cntct,amend,'Director');
        Cls_AmendmentFieldMappingUtil.getObjectAccount(accnt,amend,'Director');
        Cls_AmendmentFieldMappingUtil.getIndvAmendment(cntct,amend,'Director');
        Cls_AmendmentFieldMappingUtil.getBCAmendment(accnt,amend,'Director');
    }

}