@isTest
private class SetupObjectDataHelperTest{
    static testMethod void SetupObjectDataHelperTest1(){
    
        Group grp = new Group();
        grp.Name = 'Business Grp';
        grp.Type = 'Queue';
        insert grp;
        
        
        User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            //UserRoleId = r.Id
        );
        
        insert objUser;
        
        GroupMember grpmember = new GroupMember();
        grpmember.GroupId = grp.Id;
        grpmember.UserOrGroupId = objUser.Id;
        insert grpmember;
    
        system.runAs(objUser){

            test.startTest();
                SetupObjectDataHelper.getGroupData(grp.id);
                list<string> listGroupNames = new list<string>();
                listGroupNames.add('Business Grp');
                SetupObjectDataHelper.getGroupMembers(listGroupNames);
            test.stopTest();
        }

    }
}