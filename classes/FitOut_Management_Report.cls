/*****************************************************************************************************************************
Created By  	:	Mudasir
Description 	:	Custom class to generate Fitout dashboard which gives overall idea of the ongoing projects and their status.
Created Date	:	March 2018
--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No    	Date            Updated By          Description
v1.01    	March 2018      Mudasir      		Created this class for Fitout dashboard which gives overall idea of the ongoing projects and their status.

******************************************************************************************************************************/
public with sharing class FitOut_Management_Report {
    //Retail
    public Integer TotalRetailNoOfFitOutDrawingsReceived{get;set;}
    public Integer RetailNoOfFitOutDrawingsProcessedWithinSLA{get;set;}
    public Integer RetailNoOfFitOutDrawingsCrossedSLA{get;set;}
    public Integer RetailNoOfFitOutDrawingsReviewInProgress{get;set;}
    public Integer RetailpercentageDrawingsReviewCompletion{get;set;}
    //Office 
    public Integer TotalOfficeNoOfFitOutDrawingsReceived{get;set;}
    public Integer OfficeNoOfFitOutDrawingsProcessedWithinSLA{get;set;}
    public Integer OfficeNoOfFitOutDrawingsCrossedSLA{get;set;}
    public Integer OfficeNoOfFitOutDrawingsReviewInProgress{get;set;}
    public Integer OfficepercentageDrawingsReviewCompletion{get;set;}
    public Integer unitsOpenedForTrading{get;set;}
    public Integer officeunitsOpenedForTrading{get;set;}
    
    private Map<id,projectProgressDetails> projectProgressDetailsListMap{get;set;}
    private Map<id,projectProgressDetails> projectProgressDetailsListDelayedMap{get;set;}
    private Map<id,projectProgressDetails> officeProjectProgressDetailsListMap{get;set;}
    private Map<id,projectProgressDetails> officeProjectProgressWRFPMap{get;set;}
    private Map<id,projectProgressDetails> officeProjectProgressWRFPDelayedMap{get;set;}
    private Map<id,projectProgressDetails> officeProjectProgressDetailsListDelayedMap{get;set;}
    private Map<id,projectProgressDetails> officeProjectProgressWithinRFPMap{get;set;}
    private Map<id,projectProgressDetails> retailProjectProgressWithinRFPMap{get;set;}
    
    public List<projectProgressDetails> projectProgressDetailsList{get;set;}
    public List<projectProgressDetails> officeProjectProgressWRFPList{get;set;}
    public List<projectProgressDetails> officeProjectProgressWRFPListDelayed{get;set;}
    public List<projectProgressDetails> projectProgressDetailsListDelayed{get;set;}
    public List<projectProgressDetails> officeProjectProgressDetailsList{get;set;}
    public List<projectProgressDetails> officeProjectProgressDetailsListDelayed{get;set;}
    public List<projectProgressDetails> officeProjectProgressWithinRFP{get;set;}
    public List<projectProgressDetails> retailProjectProgressWithinRFP{get;set;}
    public List<Step__c> listOfUnitsOpenedForTrading{get;set;}
    private Set<Date> holidaysSet{get;set;}
    public integer unitsAssignedForRetail {get;set;}
    public integer retailsUnitsWithinRFPValue{get;set;}
    public integer retailsUnitsRecordedDelay {get;set;}
    public Set<id> retailsUnitsWithinRFP {get;set;}
    public integer unitsAssignedForOffice {get;set;}
    public integer officeUnitsWithinRFPValue {get;set;}
    public integer officeUnitsRecordedDelay {get;set;}
    public integer unitsRecordedPotentialDelay {get;set;}
    private Map<Id,Lease_Partner__c> officeUnitMapIds;
    private Map<Id,Lease_Partner__c> retailUnitMapIds;
    public Set<id> officeUnitsWithinRFP {get;set;}
    private List<String> portalAccessSRIds {get;set;}
    
    private Map<id,service_request__c> completedProjects{get;set;}
    private Map<id,service_request__c> cancelledProjects{get;set;}
    private Map<id,service_request__c> ongoingProjects{get;set;}
    
    public Integer officeUnitsWithFitOut{get;set;}
    public Integer retailUnitsWithFitOut{get;set;}
    private projectProgressDetails progWrap;
    private Map <id, Amendment__c> servAmndeMap;
    public integer officeProjectsCompletedOrCancelled{get;set;}
    public integer retailProjectsCompletedOrCancelled{get;set;}
    private List<String> ongoingFitOutInductionSRIDs{get;set;}
    private list<String> unitswithprojects{get;set;}
    private list<String> customerswithprojects{get;set;}
    public static boolean runClassOnce{get;set;}
    private Date dateBeforeOneYear;
    public FitOut_Management_Report(){
        initializeData();
        FitOut_SOQL_Queries_MDT queryPreparation = new FitOut_SOQL_Queries_MDT();
        if(runClassOnce ==true){
            if(!Test.isRunningTest()){
                LoadData();
                //weired coding no other option 
                finalizeDataVisualization();
            }
        }
    }
    public void initializeData(){
        TotalRetailNoOfFitOutDrawingsReceived       = 0;
        RetailNoOfFitOutDrawingsProcessedWithinSLA  = 0;
        RetailNoOfFitOutDrawingsCrossedSLA			= 0;
        RetailNoOfFitOutDrawingsReviewInProgress    = 0;
        RetailpercentageDrawingsReviewCompletion    = 0;
        TotalOfficeNoOfFitOutDrawingsReceived       = 0;
        OfficeNoOfFitOutDrawingsProcessedWithinSLA  = 0;
        OfficeNoOfFitOutDrawingsCrossedSLA  		= 0;
        OfficeNoOfFitOutDrawingsReviewInProgress    = 0;
        OfficepercentageDrawingsReviewCompletion    = 0;
        unitsOpenedForTrading                       = 0;
        unitsAssignedForRetail                      = 0;
        unitsAssignedForOffice                      = 0;
        officeUnitsWithinRFPValue                   = 0;
        unitsRecordedPotentialDelay                 = 0;
        retailsUnitsRecordedDelay                   = 0;
        officeUnitsRecordedDelay                    = 0;
        retailsUnitsWithinRFPValue                  = 0;
        officeUnitsWithFitOut                       = 0;
        retailUnitsWithFitOut                       = 0;
        officeunitsOpenedForTrading                 = 0;
        officeProjectsCompletedOrCancelled          = 0;
        retailProjectsCompletedOrCancelled          = 0;
        dateBeforeOneYear = Date.today().addDays(-365);
        officeUnitMapIds = new Map<Id,Lease_Partner__c>();
        retailUnitMapIds = new Map<Id,Lease_Partner__c>();
        portalAccessSRIds = new list<String>();
        retailsUnitsWithinRFP = new Set<id>();
        officeUnitsWithinRFP  = new Set<id>();
        projectProgressDetailsListMap  			= new Map<id,projectProgressDetails>();
        projectProgressDetailsListDelayedMap  		= new Map<id,projectProgressDetails>();
        officeProjectProgressDetailsListMap  		= new Map<id,projectProgressDetails>();
        officeProjectProgressWRFPMap				= new Map<id,projectProgressDetails>();
        officeProjectProgressWRFPDelayedMap			= new Map<id,projectProgressDetails>();
        officeProjectProgressDetailsListDelayedMap  = new Map<id,projectProgressDetails>();
        officeProjectProgressWithinRFPMap 			= new Map<id,projectProgressDetails>();
        retailProjectProgressWithinRFPMap			= new Map<id,projectProgressDetails>();
        listOfUnitsOpenedForTrading 				= new List<Step__c>();
        completedProjects = new Map<id,service_request__c>();
        cancelledProjects = new Map<id,service_request__c>();
        ongoingProjects = new Map<id,service_request__c>();
        projectProgressDetails progWrap = new projectProgressDetails();
        servAmndeMap = new Map < id, Amendment__c > ();
        ongoingFitOutInductionSRIDs = new List<String>();
        unitswithprojects = new List<String>();
        customerswithprojects = new List<String>();
        runClassOnce = true;
        Set<Date> holidaysSet = new Set<Date>();
       
        for(Holiday currHoliday : [Select ActivityDate from holiday])
        {
            holidaysSet.add(currHoliday.ActivityDate);
        }
    }
    public void LoadData(){
        String allfitoutProjectsSOQLString = FitOut_SOQL_Queries_MDT.All_FitOut_OngoingProjectsSOQL;
        system.debug('allfitoutProjectsSOQLString----------------'+allfitoutProjectsSOQLString);
        List<Service_Request__c> servReqList = Database.query(allfitoutProjectsSOQLString);
        if(!Test.isRunningTest()) createFitOut_Management_Report(servReqList);
        unitsOpenedForTrading();
    }
    
    public void unitsOpenedForTrading(){
    	//String unitsOpenedForTradingSOQL = 'Select id,name,sr__c,sr__r.Customer__r.Name,sr__r.Customer__c,sr__r.Contractor__r.Name from step__c where sr__r.RecordType.DeveloperName=\'Fit_Out_Service_Request\' and (Step_Name__c=\'Issue Completion Certificate\' OR Step_Name__c=\'Issue Fit To Occupy Certificate\') limit 10';
    	String unitsOpenedForTradingSOQL = FitOut_SOQL_Queries_MDT.unitsOpenedForTradingSOQL;
    	List<step__c> stepList = new List<Step__c>();
    	Map<id,Step__c> numberOfServiceRequests = new Map<Id,Step__c>();
    	//stepList = database.query(unitsOpenedForTradingSOQL);
    	if(!Test.isRunningTest()) for(Step__c stepRec : database.query(unitsOpenedForTradingSOQL)){numberOfServiceRequests.put(stepRec.id,stepRec);}
    	if(numberOfServiceRequests.size() >0) listOfUnitsOpenedForTrading.addAll(numberOfServiceRequests.values());
    }
   
    public void createreprojectProgressDetails(Service_request__c servReqRec1){
            progWrap = new projectProgressDetails();
            progWrap.TenantId = servReqRec1.Customer__c;
            progWrap.Tenant = servReqRec1.Customer__r.Name;
            progWrap.Location = servReqRec1.Location__c;
            progWrap.Progress = servReqRec1.Progress__c;
            progWrap.ServiceRequest = servReqRec1.Name;
            progWrap.ServiceRequestId = servReqRec1.id;
            progWrap.propertyType   = servReqRec1.Property_Type__c;
            progWrap.RecordTypeName = servReqRec1.RecordType.DeveloperName;
            progWrap.servReqRecord = servReqRec1;
            if ((servReqRec1.Linked_SR__c != Null && servReqRec1.Linked_SR__r.Linked_SR__c != Null && servAmndeMap.get(servReqRec1.Linked_SR__r.Linked_SR__c) != Null && servAmndeMap.get(servReqRec1.Linked_SR__r.Linked_SR__c).Lease_Partner__c != Null && servAmndeMap.get(servReqRec1.Linked_SR__r.Linked_SR__c).Lease_Partner__r.Lease__c != Null && servAmndeMap.get(servReqRec1.Linked_SR__r.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_From__c != null)|| Test.isRunningTest()) {
                progWrap.RFP_StartDate = servReqRec1.Linked_SR__r.Linked_SR__c != Null ? servAmndeMap.get(servReqRec1.Linked_SR__r.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_From__c : NULL;
                progWrap.RFP_EndDate = servReqRec1.Linked_SR__r.Linked_SR__c != Null ? servAmndeMap.get(servReqRec1.Linked_SR__r.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_To__c : NULL;
                progWrap.RentFreePeriod = servReqRec1.Linked_SR__r.Linked_SR__c != Null ? servAmndeMap.get(servReqRec1.Linked_SR__r.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_From__c.daysBetween(servAmndeMap.get(servReqRec1.Linked_SR__r.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_To__c) : 0;
                progWrap.LeasedDate = servReqRec1.Linked_SR__r.Linked_SR__c != Null ? servAmndeMap.get(servReqRec1.Linked_SR__r.Linked_SR__c).Lease_Partner__r.Lease__r.Start_date__c : null;
                progWrap.Duration = servReqRec1.Linked_SR__r.Linked_SR__c != Null ? servAmndeMap.get(servReqRec1.Linked_SR__r.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_From__c.daysBetween(system.today()) : 0;
                progWrap.ElapsedDays = progWrap.Duration - progWrap.RentFreePeriod;
                if (progWrap.ElapsedDays != Null && progWrap.ElapsedDays < 0) {
                    //retailsUnitsRecordedDelay = retailsUnitsRecordedDelay +1;
                    progWrap.istruevalue = true;
                    progWrap.isfalsevalue = false;
                    if(servReqRec1.Delayed_Days__c > 20){
	                    if (servReqRec1.Property_Type__c  == 'Office') officeProjectProgressDetailsListDelayedMap.put(servReqRec1.Customer__c,progWrap);
	                    else projectProgressDetailsListDelayedMap.put(servReqRec1.Customer__c,progWrap);
                     }else{
                        if (servReqRec1.Property_Type__c  == 'Office') {officeUnitsWithinRFPValue = officeUnitsWithinRFPValue +1; 
                        	officeProjectProgressWithinRFPMap.put(servReqRec1.Customer__c,progWrap);
                        	}
                        else {retailsUnitsWithinRFPValue = retailsUnitsWithinRFPValue +1;retailProjectProgressWithinRFPMap.put(servReqRec1.Customer__c,progWrap);}
                    }
                } else {
                    if (servReqRec1.Property_Type__c  == 'Office') officeProjectProgressDetailsListMap.put(servReqRec1.Customer__c,progWrap);
                    else projectProgressDetailsListMap.put(servReqRec1.Customer__c,progWrap);
                    progWrap.istruevalue = false;
                    progWrap.isfalsevalue = true;
                }
            }else{
                    if(System.Label.addToFitOutManagementReport =='true'){
                        if(servReqRec1.Delayed_Days__c > 20){
                    		officeProjectProgressWRFPDelayedMap.put(servReqRec1.Customer__c,progWrap);
                    	}else {officeProjectProgressWRFPMap.put(servReqRec1.Customer__c,progWrap);}
                        //else projectProgressDetailsListMap.put(servReqRec1.Customer__c,progWrap);
                        progWrap.istruevalue = false;
                        progWrap.isfalsevalue = true;
                    }
            }
    }
    public void createreprojectProgressDetailsInduc(Service_request__c servReqRec1){
            progWrap = new projectProgressDetails();
            progWrap.Tenant = servReqRec1.Customer__r.Name;
            progWrap.TenantId = servReqRec1.Customer__c;
            progWrap.Location = servReqRec1.Location__c;
            progWrap.Progress = servReqRec1.Progress__c;
            progWrap.ServiceRequest = servReqRec1.Name;
            progWrap.ServiceRequestId = servReqRec1.id;
            progWrap.propertyType   = servReqRec1.Property_Type__c;
            progWrap.RecordTypeName = servReqRec1.RecordType.DeveloperName;
            progWrap.servReqRecord = servReqRec1;
            if (servReqRec1.Linked_SR__c != Null && servAmndeMap.get(servReqRec1.Linked_SR__c) != Null && servAmndeMap.get(servReqRec1.Linked_SR__c).Lease_Partner__c != Null && servAmndeMap.get(servReqRec1.Linked_SR__c).Lease_Partner__r.Lease__c != Null && servAmndeMap.get(servReqRec1.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_From__c != null) {
                progWrap.RFP_StartDate  =   servReqRec1.Linked_SR__c != Null ? servAmndeMap.get(servReqRec1.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_From__c : NULL;
                progWrap.RFP_EndDate    =   servReqRec1.Linked_SR__c != Null ? servAmndeMap.get(servReqRec1.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_To__c : NULL;
                progWrap.RentFreePeriod =   servReqRec1.Linked_SR__c != Null ? servAmndeMap.get(servReqRec1.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_From__c.daysBetween(servAmndeMap.get(servReqRec1.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_To__c) : 0;
                progWrap.LeasedDate     =   servReqRec1.Linked_SR__c != Null ? servAmndeMap.get(servReqRec1.Linked_SR__c).Lease_Partner__r.Lease__r.Start_date__c : null;
                progWrap.Duration       =   servReqRec1.Linked_SR__c != Null ? servAmndeMap.get(servReqRec1.Linked_SR__c).Lease_Partner__r.Lease__r.RF_Valid_From__c.daysBetween(system.today()) : 0;
                progWrap.ElapsedDays    =   progWrap.Duration - progWrap.RentFreePeriod;
                if (progWrap.ElapsedDays != Null && progWrap.ElapsedDays < 0) {
                    //retailsUnitsRecordedDelay = retailsUnitsRecordedDelay +1;
                    progWrap.istruevalue    = true;
                    progWrap.isfalsevalue   = false;
                    if(servReqRec1.Delayed_Days__c > 20){
                    	if (servReqRec1.Property_Type__c == 'Office') officeProjectProgressDetailsListDelayedMap.put(servReqRec1.Customer__c,progWrap);
                    	else projectProgressDetailsListDelayedMap.put(servReqRec1.Customer__c,progWrap);
                    }else{
                    	if (servReqRec1.Property_Type__c  == 'Office') {officeUnitsWithinRFPValue = officeUnitsWithinRFPValue +1; 
                        	officeProjectProgressWithinRFPMap.put(servReqRec1.Customer__c,progWrap);
                        	}
                        else {retailsUnitsWithinRFPValue = retailsUnitsWithinRFPValue +1;retailProjectProgressWithinRFPMap.put(servReqRec1.Customer__c,progWrap);}
                        //if (servReqRec1.Property_Type__c  == 'Office') officeUnitsWithinRFPValue = officeUnitsWithinRFPValue +1; else retailsUnitsWithinRFPValue = retailsUnitsWithinRFPValue +1;
                    }
                } else {
                    if (servReqRec1.Property_Type__c  == 'Office') officeProjectProgressDetailsListMap.put(servReqRec1.Customer__c,progWrap);
                    else projectProgressDetailsListMap.put(servReqRec1.Customer__c,progWrap);
                    progWrap.istruevalue    = false;
                    progWrap.isfalsevalue   = true;
                }
            }else{
                if(System.Label.addToFitOutManagementReport =='true'){
                    if (servReqRec1.Property_Type__c  == 'Office') {
                    	if(servReqRec1.Delayed_Days__c > 20){
                    		officeProjectProgressWRFPDelayedMap.put(servReqRec1.Customer__c,progWrap);
                    	}else {officeProjectProgressWRFPMap.put(servReqRec1.Customer__c,progWrap);}
                    }
                    //else projectProgressDetailsListMap.put(servReqRec1.Customer__c,progWrap);
                    progWrap.istruevalue    = false;
                    progWrap.isfalsevalue   = true;
                }
            }
    }
    public void createreprojectProgressDetailsNow(id accountId,String  propertyType,String tenantName, string location,Decimal Progress,String servicerequestname,Date RFP_StartDate,Date RFP_EndDate ,Date LeasedDate){
        progWrap = new projectProgressDetails();
        progWrap.Tenant = tenantName;
        progWrap.TenantId = accountId;
        progWrap.Location = location;
        progWrap.Progress = Progress;
        progWrap.RecordTypeName = ' ';
        progWrap.ServiceRequest = servicerequestname;
        progWrap.RFP_StartDate = RFP_StartDate; 
        progWrap.RFP_EndDate = RFP_EndDate;
        progWrap.RentFreePeriod = RFP_StartDate != Null ? RFP_StartDate.daysBetween(RFP_EndDate)+1 : 0;
        progWrap.LeasedDate = LeasedDate;
        progWrap.Duration = RFP_StartDate != Null ? RFP_StartDate.daysBetween(system.today()) : 0;
        progWrap.ElapsedDays = progWrap.Duration - progWrap.RentFreePeriod;
        if (progWrap.ElapsedDays != Null && progWrap.ElapsedDays < 0) {
            //retailsUnitsRecordedDelay = retailsUnitsRecordedDelay +1;
            progWrap.istruevalue = true;
            progWrap.isfalsevalue = false;
            if (propertyType != 'Commercial') projectProgressDetailsListDelayedMap.put(accountId,progWrap);
            else officeProjectProgressDetailsListDelayedMap.put(accountId,progWrap);
        } else {
            if (propertyType != 'Commercial') projectProgressDetailsListMap.put(accountId,progWrap);
            else officeProjectProgressDetailsListMap.put(accountId,progWrap);
            progWrap.istruevalue = false;
            progWrap.isfalsevalue = true;
        }
    }
   Public void createFitOut_Management_Report(List < Service_Request__c > servReqList) {
        //Take all ongoing requests and create the details
        Set<id> serviceReqSetIDs = new Set<Id>();
        for (Service_Request__c servReq: servReqList) {
        	if(servReq.Linked_SR__c != NULL) ongoingFitOutInductionSRIDs.add('\'' + servReq.Linked_SR__c + '\'');
            if(servReq.External_Status_Name__c == 'Project Completed' || servReq.External_Status_Name__c == 'Project Cancelled'){
                if(servReq.External_Status_Name__c == 'Project Completed') completedProjects.put(servReq.Linked_SR__r.Linked_SR__c,servReq); else cancelledProjects.put(servReq.Linked_SR__r.Linked_SR__c,servReq);
                
            }
            else{
            	system.debug('Ongoing project request -------'+servReq);
                ongoingProjects.put(servReq.Linked_SR__r.Linked_SR__c,servReq);
                if(servReq.Linked_SR__r.Linked_SR__c != NULL) portalAccessSRIds.add('\'' + servReq.Linked_SR__r.Linked_SR__c + '\'');
            }
            date previousStepClosedDate = null;
                for (Step__c step: servReq.Steps_SR__R) {
                    //Calculate Number Of FitOut Drawings Received
                    if (step.sr_Step__r.owner__c == 'Contractor') {
                        previousStepClosedDate = step.Closed_Date__c;
                    }
                    //Calculate Number Of FitOut Drawings Received
                    if (step.step_name__c == 'Review by Civil/Architectural Team' || step.step_name__c == 'Conceptual Design Review by Civil/Architectural Team' || step.step_name__c == 'Detailed Design Review by Civil/Architectural Team' || step.step_name__c == 'Review by Civil/Architectural Team') {
                        if (servReq.Property_Type__c != 'Office') {
                        	system.debug('Mudasir ------'+TotalRetailNoOfFitOutDrawingsReceived);
                            TotalRetailNoOfFitOutDrawingsReceived = TotalRetailNoOfFitOutDrawingsReceived + 1;
                        } else {
                            TotalOfficeNoOfFitOutDrawingsReceived = TotalOfficeNoOfFitOutDrawingsReceived + 1;
                        }
                    }
                    integer fospSLA = Integer.valueOF(System.Label.FOSPSlaManagementReport);
                    //Calculate Number Of FitOut Drawings processed
                    if ((step.status__r.Name == 'Approved' || step.status__r.name == 'Re-upload Document') && (step.step_name__c == 'Verify & Approve by FOSP' || step.Step_Name__c == 'Detailed Design Approval by FOSP' || step.step_name__c == 'Conceptual Approval By FOSP')) {
                        if (servReq.Property_Type__c != 'Office') {
                        	if(previousStepClosedDate != Null && calculateWorkingDays(previousStepClosedDate,step.Closed_Date__c,holidaysSet) > fospSLA){
                        		RetailNoOfFitOutDrawingsCrossedSLA = RetailNoOfFitOutDrawingsCrossedSLA +1;
                        	}else{ RetailNoOfFitOutDrawingsProcessedWithinSLA = RetailNoOfFitOutDrawingsProcessedWithinSLA + 1;}
                        } else {
                            if(previousStepClosedDate != Null && calculateWorkingDays(previousStepClosedDate,step.Closed_Date__c,holidaysSet) > fospSLA)
                            {
                            	OfficeNoOfFitOutDrawingsCrossedSLA = OfficeNoOfFitOutDrawingsCrossedSLA+1; 
                            }else{ OfficeNoOfFitOutDrawingsProcessedWithinSLA = OfficeNoOfFitOutDrawingsProcessedWithinSLA + 1;}
                        }
                    } 
                    //Calculate Number Of FitOut Drawings Review In Progress
                    if ((step.status__r.name == 'Pending' || step.status__r.name == 'Pending Review') && !String.isBlank(step.sr_step__r.Submission_Type__c)) {
                        if( serviceReqSetIDs.contains(step.SR__c) ){ 
                        	continue;
                        } else if(serviceReqSetIDs.size() == 0 || (serviceReqSetIDs.size() > 0 && !serviceReqSetIDs.contains(step.SR__c))){
                        	serviceReqSetIDs.add(step.SR__c);
                        	if (servReq.Property_Type__c != 'Office') {
                        		system.debug('RetailNoOfFitOutDrawingsReviewInProgress---'+step.Step_Name__c);
                            	RetailNoOfFitOutDrawingsReviewInProgress = RetailNoOfFitOutDrawingsReviewInProgress + 1;
	                        } else {
	                        	system.debug('OfficeNoOfFitOutDrawingsReviewInProgress---'+step.Step_Name__c);
	                            OfficeNoOfFitOutDrawingsReviewInProgress = OfficeNoOfFitOutDrawingsReviewInProgress + 1;
	                        }
                        }
                    }
                }
            //system.assertEquals(null,RetailNoOfFitOutDrawingsProcessedWithinSLA*1000/TotalRetailNoOfFitOutDrawingsReceived);
            if (RetailNoOfFitOutDrawingsProcessedWithinSLA != Null && RetailNoOfFitOutDrawingsProcessedWithinSLA > 0 && TotalRetailNoOfFitOutDrawingsReceived != NULL && TotalRetailNoOfFitOutDrawingsReceived > 0) {
                Decimal valueofpercentage = RetailNoOfFitOutDrawingsProcessedWithinSLA * 1000 / TotalRetailNoOfFitOutDrawingsReceived;
                long valueok = (valueofpercentage / 10).round();
                RetailpercentageDrawingsReviewCompletion = valueok.intvalue();
            }
            if (OfficeNoOfFitOutDrawingsProcessedWithinSLA != Null && OfficeNoOfFitOutDrawingsProcessedWithinSLA > 0 && TotalOfficeNoOfFitOutDrawingsReceived != Null && TotalOfficeNoOfFitOutDrawingsReceived > 0) {
                Decimal valueofpercentageOffice = OfficeNoOfFitOutDrawingsProcessedWithinSLA * 1000 / TotalOfficeNoOfFitOutDrawingsReceived;
                long valueokOffice = (valueofpercentageOffice / 10).round();
                OfficepercentageDrawingsReviewCompletion = valueokOffice.intvalue();
            }
        }
        system.debug('TotalRetailNoOfFitOutDrawingsReceived--------'+TotalRetailNoOfFitOutDrawingsReceived);
        FitOutInductionsWithoutFitOutSR(ongoingFitOutInductionSRIDs,ongoingProjects); 
    }
    public static Integer calculateWorkingDays(Date startDate, Date endDate,Set<Date> holidaysSet)
    {        
        Integer workingDays = 0;
       
        for(integer i=0; i <= startDate.daysBetween(endDate); i++)
        {
            Date dt = startDate + i;
            DateTime currDate = DateTime.newInstance(dt.year(), dt.month(), dt.day());
            String todayDay = currDate.format('EEEE');
            if(todayDay != 'Saturday' && todayDay !='Friday' && holidaysSet!= Null && (!holidaysSet.contains(dt)))
            {
                workingDays = workingDays + 1;
            }
        }
       
        System.debug('--Working days'+workingDays);
        return workingDays;
    }
    
    private void FitOutInductionsWithoutFitOutSR(List<String> fitOutSRInductions , Map<id,service_request__c> ongoingProjects){
        List<service_request__c> InductionRequestsWithOutFitOut = New List<service_request__c>();
        String inductionSOQL = FitOut_SOQL_Queries_MDT.FitOut_InductionCreated;
        inductionSOQL = inductionSOQL.replace('filters', '  Id NOT IN (' +String.join(fitOutSRInductions,',')+')');
        system.debug('inductionSOQL---------'+inductionSOQL);
        InductionRequestsWithOutFitOut = database.query(inductionSOQL);
        //[ Select id,Progress__c,recordType.DeveloperName,service_type__c,Location__c,Customer__r.Name,Linked_SR__c,name,External_Status_Name__c,Submitted_Date__c,Type_of_Request__c,Site_Progress__c,Upgraded_Date__c,Property_Type__c from service_Request__c where recordType.DeveloperName='Fit_Out_Induction_Request' and service_type__c ='Fit-Out Induction Request'  AND  Id NOT IN : fitOutSRInductions AND External_status_Name__c ='Induction Approved']
        for(service_request__c servReqInduc : InductionRequestsWithOutFitOut){
            ongoingProjects.put(servReqInduc.Linked_SR__c,servReqInduc);
            if(servReqInduc.Linked_SR__c != NULL)portalAccessSRIds.add('\'' + servReqInduc.Linked_SR__c + '\'');
        }
        FitOutContractorPortalAccessAmendments(ongoingProjects);
    }
    public void FitOutContractorPortalAccessAmendments(Map<id,service_request__c> ongoingProjects) {
        String ongoingSOQLString = FitOut_SOQL_Queries_MDT.FitOut_ContractorPortalAccessCreated;
        ongoingSOQLString = ongoingSOQLString.replace('filters', ' ID IN (' +String.join(portalAccessSRIds,',')+')');
        system.debug('ongoingSOQLString----------'+ongoingSOQLString);
        //String ongoingSOQLString = 'Select id,name from service_request__c where recordType.Developername =\'Request_Contractor_Access\' and ID IN : ongoingProjects.keyset()';
        List < Service_Request__c > servReqListData = Database.query(ongoingSOQLString);
        //List < Service_Request__c > servReqListData = [Select id,name from service_request__c where recordType.Developername ='Request_Contractor_Access' and ID IN : ongoingProjects.keyset()];
        Map < id, id > mainFitOutSRConAccSRMap = new Map < id, id > ();
        Map < id, id > conAccSRmainFitOutSRMap = new Map < id, id > ();
        List < Amendment__c > amendUnitList = new List < Amendment__c > ();
        for (Service_Request__c servReqRec: servReqListData) { conAccSRmainFitOutSRMap.put(servReqRec.id, servReqRec.id); mainFitOutSRConAccSRMap.put(servReqRec.id, servReqRec.id);}
        //String ongoingSOQLString = SOQLValue(FitOut_SOQL_Queries_MDT.FitOut_ContractorPortalAccessCreated);
        //Get the Unit and lease details
        if (mainFitOutSRConAccSRMap.keyset().size() > 0) { amendUnitList = [Select id, ServiceRequest__c, name, Lease_Partner__c, Lease_Partner__r.Lease__c, Lease_Partner__r.Lease__r.Start_date__c, Lease_Partner__r.Lease__r.RF_Valid_From__c, Lease_Partner__r.Lease__r.RF_Valid_To__c,Lease_partner__r.Unit__c,Lease_partner__r.Account__c from Amendment__c where ServiceRequest__c in: conAccSRmainFitOutSRMap.keyset() AND RecordType.Developername = 'Fit_Out_Units' AND Lease_Partner__r.Lease__r.RF_Valid_From__c!=NULL];} 
        for (Amendment__c amend: amendUnitList) {
            unitswithprojects.add('\'' + amend.Lease_partner__r.Unit__c + '\'');
            if(amend.Lease_partner__c != Null && amend.Lease_partner__r.Account__c != Null) customerswithprojects.add('\'' + amend.Lease_partner__r.Account__c + '\'');
            servAmndeMap.put(amend.ServiceRequest__c, amend);
        }
        system.debug('servAmndeMap---------------'+servAmndeMap.size());
        /* we are creating the data at the different levels*/
        for (Id idValue: ongoingProjects.keyset()) {
        	if(system.label.checkManagementReportAssert =='true'){system.debug('idValue-------------'+idValue); system.assertEquals(null,ongoingProjects.get(idValue));}
            system.debug('idValue-------------------'+idValue+'---'+ongoingProjects.get(idValue).recordType.DeveloperName=='Fit_Out_Service_Request');
            //Under Retail Report – Type C and Type B fitout (existing tenant) not to be shown in the report
            system.debug('--------'+dateBeforeOneYear+'--------'+ongoingProjects.get(idValue).Customer__r.Active_License__r.License_Issue_Date__c);
            if(test.isRunningTest() || dateBeforeOneYear < ongoingProjects.get(idValue).Customer__r.Active_License__r.License_Issue_Date__c || ongoingProjects.get(idValue).Property_Type__c == 'Office' ||
            (dateBeforeOneYear > ongoingProjects.get(idValue).Customer__r.Active_License__r.License_Issue_Date__c && ongoingProjects.get(idValue).Type_of_Request__c !='Type C Major' && ongoingProjects.get(idValue).Type_of_Request__c !='Type C Minor' && ongoingProjects.get(idValue).Type_of_Request__c !='Type B' && ongoingProjects.get(idValue).Property_Type__c != 'Office')){
	            if(ongoingProjects.get(idValue).recordType.DeveloperName=='Fit_Out_Service_Request'){
	                if(ongoingProjects.get(idValue).Progress__c < 100 && !ongoingProjects.get(idValue).Customer__r.Exclude_From_Fitout_Management_Report__c) createreprojectProgressDetails(ongoingProjects.get(idValue));
	            }else {if(!ongoingProjects.get(idValue).Customer__r.Exclude_From_Fitout_Management_Report__c)createreprojectProgressDetailsInduc(ongoingProjects.get(idValue));}
            }
        } 
        unitsWithoutSRs();
    } 
    
    //Get All the units with rent free period where no Induction or FitOut service request is raised.
    private void unitsWithoutSRs(){
        String unitsWithoutProjectsSOQL = FitOut_SOQL_Queries_MDT.RentFreeUnitsWithoutProjects;
        unitsWithoutProjectsSOQL = unitsWithoutProjectsSOQL.replace('filters', ' AND Account__c NOT IN (' +String.join(customerswithprojects,',')+')');
        List<Lease_Partner__c> leasePartnerList = new List<Lease_Partner__c>();
        system.debug('unitsWithoutProjectsSOQL----------'+unitsWithoutProjectsSOQL);
        if(!Test.isRunningTest())leasePartnerList = Database.query(unitsWithoutProjectsSOQL);
        Set<id> unitsSet = new Set<id>();
        Set<id> clientsWithCompProj = new Set<id>();
        //Get the Clients with completed Projects 
        String clientsWithCompletedProjectsSOQL = FitOut_SOQL_Queries_MDT.FitOut_ClientsWithCompletedProjects;
        List<Service_request__c> clientsWithClosedProjects = new List<Service_request__c>();
        if(!Test.isRunningTest()){ clientsWithClosedProjects = database.query(clientsWithCompletedProjectsSOQL); for(Service_request__c servReqComp : clientsWithClosedProjects){clientsWithCompProj.add(servReqComp.customer__c);}}
        for(Lease_Partner__c leasePartnerRec : leasePartnerList){
            if(leasePartnerRec.Unit__c != Null){
                if(!unitsSet.contains(leasePartnerRec.Unit__c))
                {
                    if(leasePartnerRec.Account__c != Null && clientsWithCompProj.size() > 0 && !clientsWithCompProj.contains(leasePartnerRec.Account__c) && !leasePartnerRec.Account__r.Exclude_From_Fitout_Management_Report__c) createreprojectProgressDetailsNow(leasePartnerRec.Account__c,leasePartnerRec.Unit__r.Unit_Type_DDP__c,leasePartnerRec.Account__r.Name,+LeasePartnerRec.Unit__r.Building_Name__c+' - '+LeasePartnerRec.Unit__r.SAP_Unit_No__c,0,'Not Submitted',leasePartnerRec.Lease__r.RF_Valid_From__c,leasePartnerRec.Lease__r.RF_Valid_To__c ,Null);
                    //Add unique units to the display
                    unitsSet.add(leasePartnerRec.Unit__c);
                }
            }
        }
    } 
    class projectProgressDetails{
        public string Tenant{get;set;}
        public string TenantId{get;set;}
        public string Location{get;set;}
        public string RecordTypeName{get;set;}
        public string propertyType{get;set;}
        public Integer RentFreePeriod{get;set;}
        public Date LeasedDate{get;set;}
        public Date RFP_StartDate{get;set;}
        public Date RFP_EndDate{get;set;}
        public Integer Duration{get;set;}
        public Integer ElapsedDays{get;set;}
        public Decimal Progress{get;set;}
        public string ServiceRequest{get;set;}
        public string ServiceRequestId{get;set;}
        public boolean istruevalue{get;set;}
        public boolean isfalsevalue{get;set;}
        public Service_Request__c servReqRecord{get;set;}
        projectProgressDetails(){
            Tenant =''; Location ='';RentFreePeriod = 0; LeasedDate = Null ; Duration =0; ElapsedDays=0; Progress =0;ServiceRequestId='';propertyType=''; servReqRecord = new Service_request__c();
        }
    }
    public void finalizeDataVisualization(){
    	 projectProgressDetailsList					=	projectProgressDetailsListMap.size() > 0 ? projectProgressDetailsListMap.values() : projectProgressDetailsList;
    	 officeProjectProgressWRFPList				=	officeProjectProgressWRFPMap.size() > 0 ? officeProjectProgressWRFPMap.values() : officeProjectProgressWRFPList;
    	 officeProjectProgressWRFPListDelayed 		=	officeProjectProgressWRFPDelayedMap.size() > 0 ? officeProjectProgressWRFPDelayedMap.values() : officeProjectProgressWRFPListDelayed;
	     projectProgressDetailsListDelayed			=	projectProgressDetailsListDelayedMap.size() > 0 ?  projectProgressDetailsListDelayedMap.values() : projectProgressDetailsListDelayed;
	     officeProjectProgressDetailsList			=	officeProjectProgressDetailsListMap.size() > 0 ?  officeProjectProgressDetailsListMap.values() : officeProjectProgressDetailsList;
	     officeProjectProgressDetailsListDelayed	= 	officeProjectProgressDetailsListDelayedMap.size() > 0 ? officeProjectProgressDetailsListDelayedMap.values() : officeProjectProgressDetailsListDelayed;
	     officeProjectProgressWithinRFP 			= 	officeProjectProgressWithinRFPMap.size() > 0 ? officeProjectProgressWithinRFPMap.values() : officeProjectProgressWithinRFP;
	     retailProjectProgressWithinRFP 			=	retailProjectProgressWithinRFPMap.size() > 0 ?  retailProjectProgressWithinRFPMap.values() : retailProjectProgressWithinRFP;
	     runClassOnce = false;
    }
}