@isTest
public with sharing class CaseReassignControllerTest{

    public static Case caseobj;
    
    @isTest static void test(){
        caseObj = new Case(
        GN_Type_of_Feedback__c = 'Complaint',
        Type = 'Parking',
        Status = 'In Progress',
        Origin = 'Phone',
        Subject = 'Testing',
        Full_Name__c='TEST',
        GN_Proposed_Due_Date__c = System.today() + 5);
        
        insert caseObj;
        
        test.startTest();
            system.currentPageReference().getParameters().put('Id', caseObj.Id);
            //ApexPages.StandardSetController sc = new ApexPages.StandardSetController(caseObj);
            List<Case> cases = new List<Case>();
            cases.add([ Select Id From Case Where Id = : caseObj.Id ]);
            //CaseReassignController reassignCtrlr = new CaseReassignController(sc);
            CaseReassignController reassignCtrlr = new CaseReassignController(new ApexPages.StandardSetController( cases ));
            reassignCtrlr.CancelAction();
            reassignCtrlr.ReassignAction();
        test.stopTest();
    }
    
    @isTest static void test2(){
        caseObj = new Case(
        GN_Type_of_Feedback__c = 'Complaint',
        Type = 'Parking',
        Status = 'In Progress',
        Origin = 'Phone',
        Subject = 'Testing',
        Full_Name__c='TEST',
        GN_Proposed_Due_Date__c = System.today() + 5);
        
        insert caseObj;
        
        User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'Contact Centre'].Id,
            LastName = 'last',
            Email = 'puser000@test.com',
            Username = 'puser000@test.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'Asia/Dubai',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            //UserRoleId = r.Id
        );
        
        insert objUser;
        
        test.startTest();
            system.runAs(objUser){
                //system.currentPageReference().getParameters().put('Id', caseObj.Id);
                //ApexPages.StandardController sc = new ApexPages.StandardController(caseObj);
                // load the page       
                PageReference pageRef = Page.CaseReassign;
                //pageRef.getParameters().put('Id',caseObj.Id);
                Test.setCurrentPageReference(pageRef);
                 List<Case> cases = new List<Case>();
                cases.add([ Select Id From Case Where Id = : caseObj.Id ]);
                ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(cases);
                stdSetController.setSelected(cases);
                CaseReassignController reassignCtrlr = new CaseReassignController(stdSetController );
                reassignCtrlr.CancelAction();
                reassignCtrlr.ReassignAction();
            }
        test.stopTest();
    }
}