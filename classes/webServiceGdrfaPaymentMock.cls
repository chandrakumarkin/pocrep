@isTest
global class webServiceGdrfaPaymentMock implements HttpCalloutMock{
   //Implement http mock callout here
   // Implement this interface method
   global HTTPResponse respond(HTTPRequest request){
       // Create a fake response
       HttpResponse response = new HttpResponse();
       response.setStatusCode(200);
       response.setBody('{"applicationId":"1920100000073","batchId":"26093632","paymentDate":"2019-01-22","errorMsg": [ { "messageEn": "Fail to submit the application, please make sure that the application has been paid and all of the mandatory documents have been uploaded","messageAr": "فشل في تقديم الطلب ، يرجى التأكد من أنه قد تم دفع الطلب وتم تحميل جميع المستندات الإلزامية"}], "isValid": "false", "fees": [{"feeTypeId": "7d101c62-9b27-4792-b47e-b251999ef7b6","amount": 737983,"nameEn": "Service Fee","nameAr": "رسم الخدمة"}]}');     
       return response;
   }

}