/******************************************************************************************
*  Author   : Sravan Booragadda
*  Company  : DIFC
*  Date     : 27-Aug-2017
*  Description : This is a Contoller for GS Refund Automation Page                  
--------------------------------------------------------------------------------------------------------------------------
Modification History 
----------------------------------------------------------------------------------------
V.No   Date            Updated By          Description
----------------------------------------------------------------------------------------              
V1.0    04-10-2017      Sravan              Added vars to display request and response in logs
v1.1    12-11-2017      Veera               updated "Cheque in favour of another Entity" returns to CHTI to SAP        
V1.2    18-12-2017      Veera               Commenented the external status code in getPSA and Portal balance Methods              
v1.4    15 -March- 202   Shoaib              #8433 Enhancement Required for the service 'Request a Refund'
v1.5    18-May-2021      Utkarsh             Added the condition of Contractor Refund Project
v1.6    05-July-2021     Veera               Updated code for PSA Refund negative   
*******************************************************************************************/
public class RefundRequestUtils {       
    
    public  static SAPPortalBalanceRefundService.ZSF_TT_REFUND requestStructure; // V1.0
    Public  static SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP responseStructure; // V1.0
    
    public static void  PushToSAPAsync(string stepSerialized,string TypeOfRefund){
        step__c stp  =  (step__c)Json.deserialize(stepSerialized,step__c.class);
        
        
        System.enqueueJob(new PushToSAPAsyncQueuable(stp,TypeOfRefund));
        
    }
    
    public static void updateSRStatus(string status,service_Request__c srObj){
        Map<string,SR_Status__c> srStatus = new Map<string,SR_Status__c>();
        for(SR_Status__c srStat :[select id,Name,code__c from SR_Status__c where code__c =:status] ){
            srStatus.put(srStat.code__c,srStat);
        }    
        
        if(srStatus.containsKey(status) && srStatus.get(status)!=null){
            srObj.External_SR_status__c = srStatus.get(status).id ;
            srObj.Internal_SR_status__c = srStatus.get(status).id ;
            try{
                update srObj;
            }catch(exception e){
                log__c log = new log__c(Account__c=srObj.customer__c,Type__c='Unable to update status of SR for Refund#'+srObj.Name,Description__c =e.getMessage());
                insert log;
            }
            
        }else{
            log__c log = new log__c(Account__c=srObj.customer__c,Type__c='Unable to update status of SR for Refund#'+srObj.Name,Description__c ='SR Status Not Found');
            insert log;
        }
    }
    
    public static SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP PushToSAP(Service_Request__c  sr,string typeOfRefund){        
        Service_Request__c srObj = sr;             
        SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP refundResponse = RefundRequestUtils.PushRequestToSAP(srObj,typeOfRefund);
        system.debug('refundResponse::'+refundResponse);
        return refundResponse;
        
    }    
    
    Private static SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP PushRequestToSAP(Service_Request__c srObj,string typeOfRefund){  
        string decryptedSAPPassWrd = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456' ;        
        SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP refundResponse;
        if(srObj !=null && srObj !=null){
            SAPPortalBalanceRefundService.ZSF_S_REFUND item = prepareSAPData(srObj,typeOfRefund);
            system.debug('Refund Request'+ item);
            if(item !=null){
                List<SAPPortalBalanceRefundService.ZSF_S_REFUND> items = new List<SAPPortalBalanceRefundService.ZSF_S_REFUND>();  
                items.add(item);        
                SAPPortalBalanceRefundService.ZSF_TT_REFUND objTTRefund = new SAPPortalBalanceRefundService.ZSF_TT_REFUND();
                objTTRefund.item = items;
                SAPPortalBalanceRefundService.refund objRefund= new SAPPortalBalanceRefundService.refund();
                objRefund.timeout_x = 120000;
                objRefund.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c; 
                objRefund.inputHttpHeaders_x= new Map<String,String>();
                // Blob headerValue = Blob.valueOf('90029627:D!fc20171');
                Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+decryptedSAPPassWrd);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                objRefund.inputHttpHeaders_x.put('Authorization', authorizationHeader);
                RefundRequestUtils.requestStructure = objTTRefund; // V1.0
                refundResponse = objRefund.Z_SF_REFUND_PROCESS(objTTRefund);   
                system.debug('refundResponse!:'+refundResponse);
                RefundRequestUtils.responseStructure = refundResponse; // V1.0
                
            }
        }
        System.debug('refundResponse'+refundResponse);
        return refundResponse;        
        
    }
    
    Private static SAPPortalBalanceRefundService.ZSF_S_REFUND prepareSAPData(Service_Request__c srObj,string typeOfRefund){
        
        SAPPortalBalanceRefundService.ZSF_S_REFUND item ;
        
        if(srObj !=null && typeOfRefund == null && (srObj.Type_of_Refund__c == 'Portal Balance Refund' || srObj.Type_of_Refund__c == 'PSA Refund')){      
            item = new SAPPortalBalanceRefundService.ZSF_S_REFUND();
            item.RENUM = srObj.Name; // Salesforce Reference Number
            item.SGUID = srObj.id; // SF: Salesforce GUID
            item.RFTYP =  RefundRequestUtils.getSAPRefundType(srObj.Type_of_Refund__c);  // Refund Type
            item.RFAMT = srObj.Refund_Amount__c !=null ? srObj.Refund_Amount__c.toPlainString() : '0';  // Refund Amount
            item.RFMOD = RefundRequestUtils.getSAPRefundMode(srObj.Mode_of_Refund__c);   // Mode of Type
            item.Kunnr = srObj.Customer__r.BP_No__c;
            item.HODCOM  = [SELECT Id,Comments__c,Step_Name__c FROM step__c WHERE SR__c =:srObj.id AND Step_Name__c  ='HOD Review'  LIMIT 1].Comments__c;
            
            if(srObj.Mode_of_Refund__c == 'Wire Transfer to the account of another entity' || srObj.Mode_of_Refund__c == 'Wire Transfer to the account of another Individual' || srObj.Mode_of_Refund__c == 'Wire Transfer to the entity account'){
                item.BNAME = srObj.Bank_Name__c;
                item.BADDR = srObj.Bank_Address__c;
                item.ACNAM = srObj.SAP_SGUID__c;
                item.ACNUM = srObj.SAP_OSGUID__c;
                item.IBANN = srObj.IBAN_Number__c;
                item.SWFCD = srObj.SAP_GSTIM__c;   
                item.SORTC = srObj.Post_Code__c;                 
            }
            
            if(srObj.Mode_of_Refund__c == 'Cheque in favour of another Entity' || srObj.Mode_of_Refund__c == 'Cheque in favour of another Individual'){
                item.CCOMP = srObj.SAP_SGUID__c;
                item.CINDV = srObj.First_Name__c+' '+ srObj.Last_Name__c;            
            }    
            
        }
        
        if(srObj !=null && typeOfRefund == null && (srObj.Type_of_Refund__c == 'Portal Balance Transfer' || srObj.Type_of_Refund__c == 'PSA Transfer')){
            item = new SAPPortalBalanceRefundService.ZSF_S_REFUND();
            item.RENUM = srObj.Name; // Salesforce Reference Number
            item.SGUID = srObj.id; // SF: Salesforce GUID           
            item.RFAMT = srObj.Refund_Amount__c.toPlainString();  // Refund Amount
            item.TKUNN = srObj.Transfer_to_Account__r.BP_No__c;
            item.Kunnr = srObj.Customer__r.BP_No__c;
            item.RFTYP = RefundRequestUtils.getSAPRefundType(srObj.Type_of_Refund__c);  // Refund Type
            item.RFMOD = RefundRequestUtils.getSAPRefundMode(srObj.Type_of_Refund__c);  // Mode of Type (Will send type of refund as there are no mode of refunds config for tfr)
        }
        
        //v1.5 - Added By Utkarsh- Contractor Wallet Refund Project
        //if(srObj.getPopulatedFieldsAsMap().containsKey('Service_Type__c')){
        //if(srObj !=null && typeOfRefund != null && (srObj.Service_Type__c == 'Request for Wallet Refund')){
        if(srObj !=null && typeOfRefund != null && (srObj.Type_of_Refund__c == 'Contractor Portal Balance Refund')){
            System.debug('srObj.Type_of_Refund__c::'+srObj.Type_of_Refund__c);
            item = new SAPPortalBalanceRefundService.ZSF_S_REFUND();
            item.RENUM = srObj.Name; // Salesforce Reference Number
            item.SGUID = srObj.id; // SF: Salesforce GUID  
            if(srObj.Refund_Amount__c != null){
                item.RFAMT = srObj.Refund_Amount__c.toPlainString();  // Refund Amount
            }
            if(srObj.Transfer_to_Account__c != null){
                item.TKUNN = srObj.Transfer_to_Account__r.BP_No__c;
            }
            if(srObj.Customer__c != null){
                item.Kunnr = srObj.Customer__r.BP_No__c;
            }
            item.RFTYP = typeOfRefund;  // Refund Type
            if(srObj.Mode_of_Refund__c == 'Wire Transfer to the entity account'  ||  srObj.Mode_of_Refund__c == 'Wire Transfer to the account of another entity'){
                
                if(srObj.Mode_of_Refund__c == 'Wire Transfer to the entity account'){
                    item.RFMOD = 'WICO';  
                }if(srObj.Mode_of_Refund__c == 'Wire Transfer to the account of another entity'){item.RFMOD = 'WITC'; }if(srObj.Declaration_Signatory_Name__c != null){item.BNAME = srObj.Declaration_Signatory_Name__c;}if(srObj.Address_Details__c != null){item.BADDR = srObj.Address_Details__c;  }if(srObj.SAP_SGUID__c != null){ item.ACNAM = srObj.SAP_SGUID__c; }if(srObj.SAP_OSGUID__c != null){ item.ACNUM = srObj.SAP_OSGUID__c;  } if(srObj.SAP_GSTIM__c != null){ item.SWFCD = srObj.SAP_GSTIM__c; }
                if(srObj.SAP_GSDAT__c != null){  item.IBANN = srObj.SAP_GSDAT__c;  }     
            }
            else if(srObj.Payment_Method__c == 'Cheque'){
                item.RFMOD = 'CHCO';   // Mode of Type (Will send type of refund as there are no mode of refunds config for tfr)
                if(srObj.SAP_SGUID__c != null){
                    item.CCOMP = srObj.SAP_SGUID__c;   
                }
                if(srObj.First_Name__c != null && srObj.Last_Name__c != null){
                    item.CINDV = srObj.First_Name__c+' '+ srObj.Last_Name__c;  
                }
                
            }
            else if(srObj.Payment_Method__c == 'Cash'){
                item.RFMOD = 'CASH';   // Mode of Type (Will send type of refund as there are no mode of refunds config for tfr)   
            }
            system.debug('item.RFMOD:'+item.RFMOD);
        }
        //}
        
        //if(srObj.getPopulatedFieldsAsMap().containsKey('Service_Type__c')){
        //if(srObj !=null && typeOfRefund != null && (srObj.Service_Type__c != 'Request for Wallet Refund')){
        if(srObj !=null && typeOfRefund != null && (srObj.Type_of_Refund__c != 'Contractor Portal Balance Refund')){
            item = new SAPPortalBalanceRefundService.ZSF_S_REFUND();
            item.RENUM = srObj.Name; // Salesforce Reference Number
            item.SGUID = srObj.id; // SF: Salesforce GUID
            item.RFTYP = typeOfRefund;  // Refund Type
            item.Kunnr = srObj.Customer__r.BP_No__c;  item.UNQNO = srObj.SAP_Unique_No__c;item.APLNR = srObj.Contact__r.BP_No__c;item.MATNR = srObj.SAP_MATNR__c;item.POSNR = '000010';item.RFAMT = item.RFTYP=='RFCK' ? '0' :srObj.Refund_Amount__c.toPlainString();  // Refund Amount           
        }
        //}
        // //v1.5 -  End
        /*else if(srObj !=null && typeOfRefund != null){
            item = new SAPPortalBalanceRefundService.ZSF_S_REFUND();
            item.RENUM = srObj.Name; // Salesforce Reference Number
            item.SGUID = srObj.id; // SF: Salesforce GUID
            item.RFTYP = typeOfRefund;  // Refund Type
            item.Kunnr = srObj.Customer__r.BP_No__c;  
            item.UNQNO = srObj.SAP_Unique_No__c;
            item.APLNR = srObj.Contact__r.BP_No__c;
            item.MATNR = srObj.SAP_MATNR__c;
            item.POSNR = '000010';
            item.RFAMT = item.RFTYP=='RFCK' ? '0' :srObj.Refund_Amount__c.toPlainString();  // Refund Amount      
        } */
        if(item.RFMOD == null &&( item.RFTYP == 'RFCL' || item.RFTYP=='RFRJ' || item.RFTYP=='TRAF'))
            item.RFMOD = 'PORT';      
        if(srObj.Type_of_Refund__c=='PSA Transfer' && item.RFMOD == null)  
            item.RFMOD = 'PSAR'; 
        if(srObj.Type_of_Refund__c=='Portal Balance Transfer' && item.RFMOD == null) 
            item.RFMOD = 'PORT';       
        return item;
    }
    
    private static string getSAPRefundMode(string sfRefundType){
        if(sfRefundType == 'Credit to the portal Account' || sfRefundType == 'Portal Balance Transfer')
            return 'PORT';
        else if(sfRefundType == 'Cheque in favour of the entity')
            return 'CHCO';
        else if(sfRefundType == 'Cheque in favour of another Individual')
            return 'CHTI';
        else if(sfRefundType == 'Cheque in favour of another Entity')
            return 'CHTC';//v1.1
        else if(sfRefundType == 'Wire Transfer to the entity account')
            return 'WICO';
        else if(sfRefundType == 'Wire Transfer to the account of another entity')
            return 'WITC';
        else if(sfRefundType == 'Wire Transfer to the account of another Individual')
            return 'WITI';
        else if(sfRefundType == 'PSA Refund' || sfRefundType == 'PSA Transfer')
            return 'PSAR';
        //added by shoaib
        else if(sfRefundType == 'Refund in Cash')
            return 'CASH';
        else
            return '';             
        
    }
    
    private static string getSAPRefundType(string sfRefundType){
        if(sfRefundType == 'Portal Balance Refund')
            return 'PBAL';
        else if(sfRefundType == 'PSA Refund')
            return 'RPSA';
        else if(sfRefundType == 'Service Cancellation Refund')
            return 'RFCL';
        else if(sfRefundType == 'Service Rejection Refund')
            return 'RFRJ';
        else if(sfRefundType == 'Service Cancellation/Rejection Check')
            return 'RFCK';
        else if(sfRefundType == 'Portal Balance Transfer' || sfRefundType == 'PSA Transfer')
            return 'TRAF'; 
        else 
            return '';      
    }
    
    
    public static void sendEmailToFinance(step__c  stp,string TypeOfRefund){
        // step__c stp =  (step__c)Json.deserialize(stepSerialized,step__c.class);
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'portal@difc.ae'];//noreply.portaluat@difc.ae
        map<string,Email_Addresses__c> emailAddresses = Email_Addresses__c.getAll();
        list<string> toaddresses = new List<string>();
        List<string> ccAddresses = new List<string>();
        
        if(emailAddresses !=null && emailAddresses.size()>0 && emailAddresses.containsKey('Refund Emails To Finance')){
            if(emailAddresses.get('Refund Emails To Finance').To_Addresses__c !=null)
                toaddresses = emailAddresses.get('Refund Emails To Finance').To_Addresses__c.split(',');                        
            if(emailAddresses.get('Refund Emails To Finance').CC_Addresses__c !=null)
                ccAddresses = emailAddresses.get('Refund Emails To Finance').cc_Addresses__c.split(',');        
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List<Messaging.EmailFileAttachment> emailAttachments = RefundRequestUtils.prepareRequiredAttachments(stp,TypeOfRefund);
        List<EmailTemplate> emailTemplate = new List<EmailTemplate>();
        emailTemplate = [select id from emailtemplate where DeveloperName ='Refund_Notification_to_Finance' limit 1];            
        mail.setCCAddresses(ccAddresses); 
        mail.setReplyTo('portal@difc.ae');          
        mail.setTargetObjectID(label.Finance_Contact_for_Refund);
        mail.setWhatID(stp.SR__c);
        mail.SaveAsActivity =false;
        if(emailTemplate !=null && emailTemplate.size()>0)
            mail.SetTemplateID(emailTemplate[0].id);
        if(owea!=null && owea.size()>0)
            mail.setOrgWideEmailAddressId(owea.get(0).Id);   
        if(emailAttachments !=null && emailAttachments.size()>0)
            mail.SetFileAttachments(emailAttachments);
        
        try{                
            Messaging.SendEmail(new List<Messaging.SingleEmailMessage>{mail});  
        }catch(exception e){
            Log__c refundLog = new Log__c(Account__c = stp.sr__r.Customer__c, Type__c = 'Unable to Send Email to Finance'+stp.sr__r.Name,Description__c = e.getMessage());
           /* refundLog.Account__c = stp.sr__r.Customer__c;
            refundLog.Type__c = 'Unable to Send Email to Finance'+stp.sr__r.Name;
            refundLog.Description__c = e.getMessage();    */       
            insert refundLog;
            
        }            
        
    }
    
    
    private static List<Messaging.EmailFileAttachment> prepareRequiredAttachments(step__c stp,string TypeOfRefund){
        transient list<Messaging.EmailFileAttachment> lstMailAttachments = new list<Messaging.EmailFileAttachment>();
        Messaging.EmailFileAttachment attachment; 
        pagereference pr = new pagereference('/apex/EmailToFinance_Refund?id='+stp.SR__c+'&typ='+TypeOfRefund);    
        attachment = new Messaging.EmailFileAttachment();         
        attachment.body = !test.IsRunningTest() ?  pr.getContentAsPDF() : blob.valueOf('Test string');
        //attachment.contentType = 'pdf';
        attachment.fileName = 'Refund Details.pdf';
        lstMailAttachments.add(attachment);  
        lstMailAttachments = RefundRequestUtils.getAttachments(stp.SR__c,TypeOfRefund,lstMailAttachments);          
        return lstMailAttachments;
    }
    
    private static list<Messaging.EmailFileAttachment> getAttachments(string servicereq,string typeofRefund ,List<Messaging.EmailFileAttachment> lstMailAttachments){
        
        Map<string,Documents_Required_Refund__c> docsReqd = Documents_Required_Refund__c.getAll();
        Messaging.EmailFileAttachment attachment = new  Messaging.EmailFileAttachment();
        List<string> srDocumentNames = new List<string>();
        
        srDocumentNames = (docsReqd.containsKey(typeofRefund) && docsReqd.get(typeofRefund) !=null && docsReqd.get(typeofRefund).Document_Codes__c !=null) ?  docsReqd.get(typeofRefund).Document_Codes__c.split(';') : srDocumentNames;
        
        List<string> attachmentIDs = new List<string>();
        for(sr_Doc__c srd : [select id,Doc_ID__c from sr_doc__c where Service_Request__c =:servicereq and Name in :srDocumentNames]){
            attachmentIDs.add(srd.Doc_ID__c);
        }                      
        if(attachmentIDs !=null && attachmentIDs.size()>0){
            for(attachment att : [select id,body,Name,contentType from attachment where id in : attachmentIDs]){
                attachment = new  Messaging.EmailFileAttachment();
                attachment.body = att.body;
                attachment.contentType = att.contentType;
                attachment.fileName = att.Name;
                lstMailAttachments.add(attachment);                 
            }                   
        }
        
        return lstMailAttachments;
        
        
    }
    
    public static decimal getRefundAmount(string custId){
        decimal refundAmnt= 0;
        
        List<string> status = new List<String>{'Cancelled','Rejected','Refund Processed','Refund Processed by Receivable now pending with Payable'} ;
            List<string> typeOfRefund = new List<string>{'Portal Balance Refund','Portal Balance Transfer'} ;
                
                for(Service_Request__c srRefund :[select id,Refund_Amount__c from service_request__c where Customer__c=:custId and Record_type_Name__c ='Refund_Request' and Submitted_Date__c !=null and Refund_Amount__c !=null and /*External_SR_Status__r.Name not in :status or*/  Internal_SR_Status__r.Name not in :status and type_of_refund__c in : typeOfRefund]){//V1.2
                    refundAmnt += srRefund.Refund_Amount__c;                      
                }
        
        return refundAmnt;
        
    }
    
    public static decimal getPSAAmount(string custId){
        decimal psaAmount= 0;
        decimal PSAAmount1 =0;
        decimal PSAAmount2 =0;
        List<string> status = new List<String>{'Cancelled','Rejected','Refund Processed','Refund Processed by Receivable now pending with Payable'} ;
            List<string> typeOfRefund = new List<string>{'PSA Refund','PSA Transfer'} ;
            List<string> ModeRefundForC = new List<string> {'Credit to the portal Account','Wire Transfer to the entity account'};
            List<string> ModeRefundForP = new List<string> {'Wire Transfer to the account of another entity','Wire Transfer to the account of another Individual'};
            
            if(system.label.PSA_Refund_Logic_Check == 'false'){ //v1.6
                for(Service_Request__c srObj :[select id,Refund_Amount__c from service_request__c where customer__c =: custId and record_type_Name__c ='Refund_Request' and Submitted_date__c !=null and (External_SR_Status__r.Name not in :status  or  Internal_SR_Status__r.Name not in :status) and type_of_refund__c in : typeOfRefund]){//V1.2
                    psaAmount += srObj.Refund_Amount__c;
                } 
            }   
            //v1.6  --start
            else{   
            
            
            
            for(Service_Request__c srObj :[select id,Refund_Amount__c,Mode_of_Refund__c from service_request__c where customer__c =: custId and record_type_Name__c ='Refund_Request' and Submitted_date__c !=null and ( (Mode_of_Refund__c in : ModeRefundForC  AND type_of_refund__c = 'PSA Refund') OR  type_of_refund__c ='PSA Transfer')  AND  External_SR_Status__r.Name not in :status ]){
                    
                       psaAmount1 += srObj.Refund_Amount__c;
                     }

                for(Service_Request__c srObj :[select id,Refund_Amount__c from service_request__c where customer__c =: custId and record_type_Name__c ='Refund_Request' and Submitted_date__c !=null and Internal_SR_Status__r.Name not in :status and type_of_refund__c = 'PSA Refund' and Mode_of_Refund__c in :ModeRefundForP ]){
                     psaAmount2 += srObj.Refund_Amount__c;
                    
                }
                
                psaAmount = psaAmount1 + psaAmount2;
            }

            //v1.6  -- End          
        return psaAmount ;
        
    }
    
    @InvocableMethod(label='Create Line Manager for Service Rejection' description='Create Line Manager for Service Rejection GS')
    public static void createLineManagerForRejection(List<ID> ids) {
        RefundRequestUtils.calculateRefundAndCreateStep(ids);
        
    }
    
    
    @future(Callout=true)
    public static void calculateRefundAndCreateStep(List<id> ids){
        List<Service_Request__c> lstNewRecords = new List<Service_Request__c> ();           
        map<id,SR_Steps__c> srTemplateMap = new map<id,SR_Steps__c>();
        set<id> srTempIds = new set<id>();  
        List<step__c> stpList = new List<step__c>(); 
        integer LoopStepNo;
        integer maxLoopStepNo = 0;   
        
        for(service_Request__c sr :[select id,Name,External_SR_Status__c,SR_Template__c,External_Status_Name__c,Customer__r.Name,
                                    (select Id,Sys_Step_Loop_No__c from Steps_SR__r where Sys_Step_Loop_No__c != null order by lastModifiedDate)
                                    from service_request__c where id  =: ids[0]])
        {
            
            lstNewRecords.add(sr);
            srTempIds.add(sr.SR_Template__c);
            
            if(sr.Steps_SR__r != null){
                for(Step__c step : sr.Steps_SR__r){
                    if(step.Sys_Step_Loop_No__c!=null){
                        LoopStepNo = integer.valueOf(step.Sys_Step_Loop_No__c.substringAfter('_'));
                        if(maxLoopStepNo < LoopStepNo){
                            maxLoopStepNo = LoopStepNo;
                        }
                    }
                }
            }                               
            
        }
        
        if(lstNewRecords !=null && lstNewRecords.size()>0){  
            for(SR_Steps__c obj : [select Id,SR_Template__c,Step_Template__c,Start_Status__c,OwnerId,Step_RecordType_API_Name__c,Summary__c,Step_No__c,Estimated_Hours__c,SR_Template__r.SR_Group__c from SR_Steps__c where SR_Template__c in: srTempIds AND Step_Template__r.Name = 'HOD Review']){
                srTemplateMap.put(obj.SR_Template__c,obj);
                
            }
            
            map<string,Id> mapStepRecType = new map<string,Id>();//Key Step RecordType API Name and Value as RecordType Id
            for(RecordType RT:[select id,Name,DeveloperName,sObjectType from RecordType where sObjectType='Step__c']){
                mapStepRecType.put(RT.DeveloperName,RT.id);
            }    
            
            Step__c objStep;
            
            if(srTemplateMap !=null && srTemplateMap.size()>0){
                
                
                
                
                SR_Steps__c objSRStep;
                for(service_request__c objSR : lstNewRecords){
                    objSRStep = new SR_Steps__c();
                    if(srTemplateMap.containsKey(objSR.SR_Template__c) && srTemplateMap.get(objSR.SR_Template__c) !=null)
                        objSRStep = srTemplateMap.get(objSR.SR_Template__c);
                    objStep = new Step__c();
                    objStep.SR__c = objSR.ID;
                    objStep.Client_Name__c = objSR.Customer__r.Name;
                    objStep.Step_Template__c = objSRStep.Step_Template__c;
                    objStep.Status__c = objSRStep.Start_Status__c;
                    objStep.OwnerId = objSRStep.OwnerId;
                    if(mapStepRecType.containsKey(objSRStep.Step_RecordType_API_Name__c))
                        objStep.RecordTypeId = mapStepRecType.get(objSRStep.Step_RecordType_API_Name__c);
                    objStep.SR_Step__c = objSRStep.Id;
                    objStep.Start_Date__c = system.today();
                    objStep.Summary__c = objSRStep.Summary__c;
                    objStep.Step_No__c = objSRStep.Step_No__c;
                    //objStep.Sys_Step_Loop_No__c = string.valueOf(objStep.Step_No__c)+'_'+maxLoopStepNo;
                    
                    Id BHId = id.valueOf(Label.Business_Hours_Id);
                    //V2.0 - End 
                    if(objSRStep.Estimated_Hours__c!=null){
                        Long sla = objSRStep.Estimated_Hours__c.longvalue();
                        sla=sla*60*60*1000L;
                        datetime CreatedTime = system.now(); 
                        objStep.Due_Date__c=BusinessHours.add(BHId,CreatedTime,sla);
                    }
                    
                    stpList.add(objStep);
                }   
            } 
            // Service Call to fetch refund amount
            string str;
            service_request__c srObjToUpdate;
            if(lstNewRecords !=null && lstNewRecords.size()>0){             
                GsServiceCancellation.refundAmnt = 0;
                str = GsServiceCancellation.CancellationPrecheck(lstNewRecords[0].id);
                srObjToUpdate = lstNewRecords[0];
            }
            
            if(str =='Success' && GsServiceCancellation.refundAmnt !=null){
                
                srObjToUpdate.Refund_Amount__c = GsServiceCancellation.refundAmnt;
                srObjToUpdate.Event_Name__c = 'Rejection';
                
                if(srObjToUpdate !=null && srObjToUpdate.id !=null){
                    try{
                        SavePoint sp = Database.setSavepoint();
                        update srObjToUpdate;
                        
                        if(stpList!=null && stpList.size()>0)
                            insert stpList;                     
                    }
                    catch(exception ex){
                        // Database.RollBack(sp);
                        Log__c objLog = new Log__c();
                        objLog.Type__c = 'Service Rejected By Immigration - Refund';
                        objLog.Description__c = 'Error Updating the Refund Amount and Step creation for SR #'+srObjToUpdate.Name+'Error - '+ex.getMessage();
                        insert objLog;                     
                    }       
                }else{
                    Log__c objLog = new Log__c();
                    objLog.Type__c = 'Service Rejected By Immigration - Refund';
                    objLog.Description__c = 'Error Updating the Refund Amount and Step creation for SR #'+srObjToUpdate.Name;
                    insert objLog;
                }           
            }else if(str !='Success'){
                Log__c objLog = new Log__c();
                objLog.Type__c = 'Service Rejected By Immigration - Refund';
                objLog.Description__c = 'Unable to get Refund Amount for SR#'+lstNewRecords[0].Name+'On SR Rejection Reason -'+str;
                insert objLog;
            }     
            
        }         
    }    
}