/******************************************************************************************************
*  Author   : Kumar Utkarsh
*  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
V1.1    22-Apr-2021  Utkarsh         Modified
* This class contains logic of VISA Renewal callout.
*******************************************************************************************************/
public class WebServIntegrationGdrfaResRenewalClass {
    
    @InvocableMethod 
    public static void invokeSubmitResRenew(List<Id> SrIds)
    {
        Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
        System.debug('List<Id> SrIds:'+SrIds);
        for(Id SrId :SrIds){
        GDRFARenewResidenceCallout(String.valueOf(SrId), StatusReference.Id);
        }
    }
    
    public class MandateDocuments {
        public String applicationId;
        public List<Documents> Documents;
    }
    
    public class Documents {
        public String documentTypeId;
        public Boolean IsMandatory;
        public String documentNameEn;
        public String documentNameAr;
    } 
    
    public class UploadDoc{
        public Boolean Success{get;set;}
    }    
    //Fetched the SR's for Renewal Request and build the JSON for submission
    public static String SubmitRenewal(String ID, String accessToken, String tokenType)
    {
        Step__c visaRenewStep;
        String finalJSON;
        Service_Request__c SR = [SELECT Id, Service_Type__c, Record_Type_Name__c, Nationality__c, Previous_Nationality__c, Country_of_Birth__c, Gender__c, Marital_Status__c, Religion__c, Qualification__c, Occupation__c, Occupation_GS__r.Code__c, 
                                 First_Name__c, First_Name_Arabic__c, Middle_Name__c, Middle_Name_Arabic__c, Location__c, Last_Name__c, Last_Name_Arabic__c, Mother_Full_Name__c, Establishment_Card_No__c, Residence_Visa_No__c, Passport_Type__c,
                                 Mother_s_full_name_Arabic__c, Date_of_Birth__c, Place_of_Birth__c, Place_of_Birth_Arabic__c, First_Language__c, Passport_Number__c, Passport_Date_of_Issue__c, Passport_Date_of_Expiry__c, 
                                 Passport_Place_of_Issue__c, Building__c, Emirates_Id_Number__c, Area__c, Street_Address__c, Current_Registered_Street__c, Mobile_Number__c, Sponsor_Mobile_No__c, Address_Details__c, Visa_Duration__c,
                                 Passport_Country_of_Issue__c, Type_of_Request__c, Identical_Business_Domicile__c, City_Town__c, Emirate__c, City__c, Building_Name__c, Phone_No_Outside_UAE__c, Would_you_like_to_opt_our_free_couri__c
                                 FROM Service_Request__c WHERE Id =: ID ];
        
        String YearOfResidence;
        String DeliveryOption;
        If(SR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Renewal'){
            visaRenewStep = [SELECT Id, DNRD_Receipt_No__c FROM Step__c WHERE SR__c =: ID AND (Step_Status__c = 'Awaiting Review' OR Step_Status__c = 'Repush to GDRFA') AND Step_Name__c = 'Renewal Form is Typed'];
        } 
        
        if(SR.Visa_Duration__c != null){
            if(SR.Visa_Duration__c == '1 Year'){
                YearOfResidence = '1';
            }
            if(SR.Visa_Duration__c == '2 Years'){
                YearOfResidence = '2';
            }
            if(SR.Visa_Duration__c == '3 Years'){
                YearOfResidence = '3';
            }
        }else if(SR.Visa_Duration__c == null){
            YearOfResidence = '3';
        }
        
        if(SR.Would_you_like_to_opt_our_free_couri__c == 'Yes'){
            DeliveryOption = 'Zajel Delivery';
        }
        if(SR.Would_you_like_to_opt_our_free_couri__c == 'No'){
            DeliveryOption = 'Self Collection';
        }
        Lookup__c NationalityName;
        if(SR.Nationality__c != null){
            NationalityName = [SELECT Id, Name FROM Lookup__c WHERE Id =:SR.Nationality__c];
        }
        String isInsideUAE;
        If(SR.Type_of_Request__c == 'Applicant Inside UAE'){
            isInsideUAE = 'Yes';
        }else{
            isInsideUAE = 'No';
        }
        String GDRFAIsInsideUAELookUp = WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Inside Outside Country',isInsideUAE); 
        String GDRFACountryLookUpId = WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Identical_Business_Domicile__c); 
        system.debug('visaRenewStep:'+visaRenewStep);
        If(visaRenewStep != null){
            JSONGenerator jsonRenwObj = JSON.createGenerator(true);
            jsonRenwObj.writeStartObject();
            if(visaRenewStep.DNRD_Receipt_No__c != null && visaRenewStep.DNRD_Receipt_No__c != ''){
                jsonRenwObj.writeStringField('applicationNumber', visaRenewStep.DNRD_Receipt_No__c);
            }
            if(SR.Residence_Visa_No__c != null){
                jsonRenwObj.writeStringField('residenceNo', SR.Residence_Visa_No__c.replaceAll('/',''));
            }
            jsonRenwObj.writeFieldName('applicationData');
            jsonRenwObj.writeStartObject();
            jsonRenwObj.writeFieldName('Application');
            jsonRenwObj.writeStartObject();
            jsonRenwObj.writeFieldName('ApplicantDetails');
            jsonRenwObj.writeStartObject();
            if(SR.First_Name__c != null ) {
                jsonRenwObj.writeStringField('FirstNameE', SR.First_Name__c);
            }
            if(SR.First_Name_Arabic__c != null ) {
                jsonRenwObj.writeStringField('FirstNameA', SR.First_Name_Arabic__c);
            }else if(SR.First_Name_Arabic__c == null && SR.First_Name__c != null){
                String FirstNameArabic = WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.First_Name__c, visaRenewStep, accessToken, tokenType);
                jsonRenwObj.writeStringField('FirstNameA', FirstNameArabic);
            }
            if(SR.Middle_Name__c != null ) {
                jsonRenwObj.writeStringField('MiddleNameE', SR.Middle_Name__c);
            }
            if(SR.Middle_Name_Arabic__c != null ) {
                jsonRenwObj.writeStringField('MiddleNameA', SR.Middle_Name_Arabic__c);
            }else if(SR.Middle_Name_Arabic__c == null && SR.Middle_Name__c != null){
                String MiddleNameArabic = WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.Middle_Name__c, visaRenewStep, accessToken, tokenType);
                jsonRenwObj.writeStringField('MiddleNameA', MiddleNameArabic);
            }
            
            if(SR.Last_Name__c != null ) {
                jsonRenwObj.writeStringField('LastNameE', SR.Last_Name__c);
            }
            
            if(SR.Last_Name_Arabic__c != null ) {
                jsonRenwObj.writeStringField('LastNameA', SR.Last_Name_Arabic__c);
            }else if(SR.Last_Name_Arabic__c == null && SR.Last_Name__c != null){
                String LastNameArabic = WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.Last_Name__c, visaRenewStep, accessToken, tokenType);
                jsonRenwObj.writeStringField('LastNameA', LastNameArabic);
            }
            
            if(SR.Mother_Full_Name__c != null ) {
                jsonRenwObj.writeStringField('MotherNameE', SR.Mother_Full_Name__c);
            }
            
            if(SR.Mother_s_full_name_Arabic__c != null ) {
                jsonRenwObj.writeStringField('MotherNameA', SR.Mother_s_full_name_Arabic__c);
            }else if(SR.Mother_s_full_name_Arabic__c == null && SR.Mother_Full_Name__c != null){
                String MothersNameArabic = WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.Mother_Full_Name__c, visaRenewStep, accessToken, tokenType);
                jsonRenwObj.writeStringField('MotherNameA', MothersNameArabic);
            }
            if(NationalityName.Name != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country', NationalityName.Name) != ''){
                    jsonRenwObj.writeStringField('CurrentNationalityId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country', NationalityName.Name));
                }
            }
            
            if(SR.Previous_Nationality__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Previous_Nationality__c) != ''){
                    jsonRenwObj.writeStringField('PreviousNationality', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Previous_Nationality__c));
                }
            }
            if(SR.Date_of_Birth__c != null ) {
                jsonRenwObj.writeDateField('BirthDate', SR.Date_of_Birth__c);
            }
            
            if(SR.Country_of_Birth__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Country_of_Birth__c) != ''){
                    jsonRenwObj.writeStringField('BirthCountryId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Country_of_Birth__c));
                }
            }
            
            
            if(SR.Place_of_Birth__c != null ) {
                jsonRenwObj.writeStringField('BirthPlaceE', SR.Place_of_Birth__c);
            }
            if(SR.Place_of_Birth_Arabic__c != null ) {
                jsonRenwObj.writeStringField('BirthPlaceA', SR.Place_of_Birth_Arabic__c);
            }
            else{
                jsonRenwObj.writeStringField('BirthPlaceA', WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.Place_of_Birth__c, visaRenewStep, accessToken, tokenType));
            }
            
            jsonRenwObj.writeStringField('SexId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Gender',SR.Gender__c));
            jsonRenwObj.writeStringField('MaritalStatusId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('List of Marital Status',SR.Marital_Status__c));
            jsonRenwObj.writeStringField('ReligionId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Religions',SR.Religion__c));
            jsonRenwObj.writeStringField('Faith', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Religions',SR.Religion__c));
            if(SR.Qualification__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Education',SR.Qualification__c) != ''){
                    jsonRenwObj.writeStringField('EducationId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Education',SR.Qualification__c));
                }
            }
            if(SR.Occupation_GS__r.Code__c != null){
                jsonRenwObj.writeStringField('ProfessionId',SR.Occupation_GS__r.Code__c);
            }
            
            if(SR.First_Language__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Language',SR.First_Language__c) != ''){
                    jsonRenwObj.writeStringField('Language1', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Language',SR.First_Language__c)); 
                }
            }
            
            
            if(SR.Passport_Number__c != null ) {
                jsonRenwObj.writeStringField('PassportNo', SR.Passport_Number__c);
            }
            jsonRenwObj.writeStringField('PassportTypeId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Passport Type',SR.Passport_Type__c));
            if(SR.Passport_Date_of_Issue__c != null ) {
                jsonRenwObj.writeDateField('PassportIssueDate', SR.Passport_Date_of_Issue__c);
            }
            if(SR.Passport_Date_of_Expiry__c != null ) {
                jsonRenwObj.writeDateField('PassportExpiryDate', SR.Passport_Date_of_Expiry__c);
            }
            if(SR.Passport_Place_of_Issue__c != null ) {
                jsonRenwObj.writeStringField('PassportPlaceE', SR.Passport_Place_of_Issue__c);
            }
            if(SR.Passport_Place_of_Issue__c != null ) {
                String PassportPlaceArabic = WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.Passport_Place_of_Issue__c, visaRenewStep, accessToken, tokenType);
                jsonRenwObj.writeStringField('PassportPlaceA', PassportPlaceArabic);
            }
            if(SR.Passport_Country_of_Issue__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Passport_Country_of_Issue__c) != ''){
                    jsonRenwObj.writeStringField('PassportIssueCountryId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Passport_Country_of_Issue__c));  
                }  
            }
            if(SR.Passport_Country_of_Issue__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Passport_Country_of_Issue__c) != ''){
                    jsonRenwObj.writeStringField('PassportIssueGovId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Passport_Country_of_Issue__c)); 
                }    
            }
            
            jsonRenwObj.writeStringField('IsInsideUAE', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Inside Outside Country',isInsideUAE));
            if(SR.Identical_Business_Domicile__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Identical_Business_Domicile__c) != ''){
                    jsonRenwObj.writeStringField('AddressOutsideCountryId',WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Identical_Business_Domicile__c)); 
                }   
            }
            
            jsonRenwObj.writeStringField('AddressOutsideCity', SR.City_Town__c);  
            jsonRenwObj.writeStringField('AddressOutside1', SR.Address_Details__c); 
            jsonRenwObj.writeStringField('AddressOutside2', '');
            jsonRenwObj.writeStringField('AddressOutsideTelNo', SR.Phone_No_Outside_UAE__c);
            
            if(SR.Emirate__c != null ) {
                jsonRenwObj.writeStringField('EmirateId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Emirates',SR.Emirate__c));
                jsonRenwObj.writeStringField('CityId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Cities',SR.Emirate__c));
            }
            
            
            if(SR.Area__c != null ) {
                String GdrfaAreaId =  WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas',SR.Area__c);
                if(GdrfaAreaId != null && GdrfaAreaId != ''){
                    jsonRenwObj.writeStringField('AreaId', GdrfaAreaId);
                }else if (GdrfaAreaId == null || GdrfaAreaId != ''){
                    jsonRenwObj.writeStringField('AreaId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas', 'Zabeel 1'));
                } 
            }
            else if(SR.Area__c == null ){
                jsonRenwObj.writeStringField('AreaId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas', 'Zabeel 1'));
            }
            if(SR.Street_Address__c != null ) {
                jsonRenwObj.writeStringField('Street', SR.Street_Address__c);
            }
            
            if(SR.Building_Name__c != null ) {
                jsonRenwObj.writeStringField('Building', SR.Building_Name__c);
            }
            
            if(SR.Current_Registered_Street__c != null ) {
                jsonRenwObj.writeStringField('Landmark', 'DIFC');
            }
            jsonRenwObj.writeStringField('MakaniNo', '');
            if(SR.Mobile_Number__c != null ) {
                jsonRenwObj.writeStringField('MobileNo', SR.Mobile_Number__c);
            }
            if(YearOfResidence != null ) {
                jsonRenwObj.writeStringField('ResidenceRequestYear', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Years Of Residence',YearOfResidence));
            }
            
            jsonRenwObj.writeEndObject(); 
            jsonRenwObj.writeFieldName('SponsorDetails');
            jsonRenwObj.writeStartObject();
            jsonRenwObj.writeStringField('SponsorEmail', 'gdrfanotifications@difc.ae');
            if(SR.Sponsor_Mobile_No__c != null ) {
                jsonRenwObj.writeStringField('MobileNo', SR.Sponsor_Mobile_No__c);
            }
            if(SR.Establishment_Card_No__c != null){
                jsonRenwObj.writeStringField('EstCode', SR.Establishment_Card_No__c.replaceAll('/',''));
            }
            jsonRenwObj.writeEndObject();
            jsonRenwObj.writeFieldName('ApplicationDetails');
            jsonRenwObj.writeStartObject();
            jsonRenwObj.writeStringField('ResidencyPickup', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Delivery Method','Self Collection'));
            jsonRenwObj.writeStringField('DeliveryEmirateId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Emirates','Dubai'));
            jsonRenwObj.writeStringField('DeliveryCityId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Cities','Dubai'));
            jsonRenwObj.writeStringField('DeliveryAreaId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas','Zabeel 1'));
            jsonRenwObj.writeStringField('DeliveryStreet', 'Shaikh Zayed Road');
            jsonRenwObj.writeStringField('DeliveryBuilding', 'DIFC');
            jsonRenwObj.writeStringField('DeliveryFloorNo','14');
            jsonRenwObj.writeStringField('DeliveryFlatNo', '14');
            jsonRenwObj.writeStringField('DeliveryLandmark', 'Emirates Tower');
            jsonRenwObj.writeStringField('DeliveryMakaniNo', '');
            jsonRenwObj.writeBooleanField('IsDraft', false);
            jsonRenwObj.writeEndObject();
            jsonRenwObj.writeEndObject();
            jsonRenwObj.writeEndObject();
            jsonRenwObj.writeEndObject();
            finalJSON = jsonRenwObj.getAsString();
            System.debug('DEBUG Full Request finalJSON : ' +(jsonRenwObj.getAsString()));
        }
        WebServGDRFAAuthFeePaymentClass.masterDataMap = null;
        return finalJSON;
    }
    
    @future(callout=true)
    //Callout method for Submit VISA Renewal 
    public static void GDRFARenewResidenceCallout(String ID, String StatusReference) 
    { 
        map<Id,Id> mapStepStepTemplate = new map<Id,Id>();
        Step__c visaRenewStep;
        String visaRenewJson;
        Attachment attachment = new Attachment();
        attachment.Name = 'VISA renewal Json.txt';
        attachment.ParentId = ID;
        attachment.Body = Blob.valueOf('Error before Json Creation');
        attachment.ContentType = 'text/plain';
        //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
        try{
            String finalDocJSON;
            Boolean isAllUploadSuccess = false;
            MandateDocuments responseDocList;
            visaRenewStep = [SELECT Id, DNRD_Receipt_No__c, Step_Name__c, Step_Status__c, SR__c, Step_Template__c, SR_Step__c FROM Step__c WHERE SR__c =: ID AND (Step_Status__c = 'Awaiting Review' OR Step_Status__c = 'Repush to GDRFA') AND Step_Name__c = 'Renewal Form is Typed' ];
            for(SR_Steps__c obj: [select id, Step_Template__c from SR_Steps__c where Step_Template_Code__c IN('Online entry permit is typed', 'Entry permit form is typed', 'Renewal form is typed', 'Cancellation Form Typed', 'Entry Permit is Issued', 'Completed', 'Passport is Ready for Collection', 'Visa Stamping Form is Typed') AND SR_Template__r.SR_Template_Code__c IN ('DIFC_Sponsorship_Visa_New','DIFC_Sponsorship_Visa_Renewal', 'DIFC_Sponsorship_Visa_Cancellation') ]){
                mapStepStepTemplate.put(obj.Step_Template__c,obj.Id);                                                                        
            }
            if(mapStepStepTemplate.containsKey(visaRenewStep.Step_Template__c) && visaRenewStep.SR_Step__c == null){
                visaRenewStep.SR_Step__c = mapStepStepTemplate.get(visaRenewStep.Step_Template__c);
            }
            WebServGDRFAAuthFeePaymentClass.ResponseBasicAuth ObjResponseAuth = WebServGDRFAAuthFeePaymentClass.GDRFAAuth();
            List<Log__c> ErrorObjLogList = new List<Log__c>();
            //Get authrization code 
            string access_token= ObjResponseAuth.access_token;
            string token_type=ObjResponseAuth.token_type;
            //WebService to Push the Submit Cancel Form
            HttpRequest request = new HttpRequest();
            request.setEndPoint(System.label.GDRFA_Submit_Renew_Residence); 
            visaRenewJson = SubmitRenewal(ID, access_token, token_type);
            attachment.Body = Blob.valueOf(visaRenewJson);
            attachment.ParentId = visaRenewStep.Id;
            request.setBody(visaRenewJson);
            request.setTimeout(120000);
            request.setHeader('apikey', System.label.GDRFA_API_Key);
            request.setHeader('Content-Type', 'application/json');
            request.setMethod('POST');
            request.setHeader('Authorization',token_type+' '+access_token);
            HttpResponse response = new HTTP().send(request);
            System.debug('responseStringresponse:'+response.toString());
            System.debug('STATUS:'+response.getStatus());
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('strJSONresponseSubmitRenew:'+response.getBody());
            request.setTimeout(101000);
            
            if (response.getStatusCode() == 200)
            {
                responseDocList = (MandateDocuments) System.JSON.deserialize(response.getBody(), MandateDocuments.class);
                System.debug('applicationId:'+responseDocList.applicationId);
                System.debug('Documents:'+responseDocList.Documents);
                Boolean mandateDocsExist = false;
                Set<String> setGDRFADocId = new Set<String>();
                map<Id, String> mapSRDocIdGDRFADocId = new map<Id, String>();
                map<Id, String> mapSRDocIdGDRFAname = new map<Id, String>();
                map<Id, String> mapSRDocIdGDRFAArabicName = new map<Id, String>();
                map<String, String> mapGdrfaDocIdGDRFAname = new map<String, String>();
                map<String, String> mapGdrfaDocIdGDRFAArabicname = new map<String, String>();
                map<String, Boolean> mapGdrfaDocIdGdrfaIsMandate = new map<String, Boolean>();
                map<String, Boolean> mapSRDocIdGdrfaIsMand = new map<String, Boolean>();
                map<Id, Boolean> mapSRDocMandate = new map<Id, Boolean>();
                if(responseDocList.Documents != null){
                    For(Documents docList : responseDocList.Documents){
                        setGDRFADocId.add(docList.documentTypeId);
                        mapGdrfaDocIdGdrfaIsMandate.put(docList.documentTypeId, docList.IsMandatory);
                        mapGdrfaDocIdGDRFAname.put(docList.documentTypeId, docList.documentNameEn);
                        mapGdrfaDocIdGDRFAArabicname.put(docList.documentTypeId, docList.documentNameAr);
                    }
                }
                System.debug('mapGdrfaDocIdGdrfaIsMandate:'+mapGdrfaDocIdGdrfaIsMandate);
                for(SR_Doc__c objDoc:  [SELECT GDRFA_Attchment_Doc_Id__c, GDRFA_Parent_Document_Id__c FROM SR_Doc__c
                                        WHERE Service_Request__c =: ID AND GDRFA_Parent_Document_Id__c IN: setGDRFADocId AND GDRFA_Parent_Document_Id__c != null AND GDRFA_Attchment_Doc_Id__c != null]){
                                            mapSRDocIdGDRFADocId.put(objDoc.GDRFA_Attchment_Doc_Id__c, objDoc.GDRFA_Parent_Document_Id__c);
                                            mapSRDocIdGDRFAname.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGDRFAname.get(objDoc.GDRFA_Parent_Document_Id__c));
                                            mapSRDocIdGdrfaIsMand.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGdrfaIsMandate.get(objDoc.GDRFA_Parent_Document_Id__c));
                                            mapSRDocIdGDRFAArabicName.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGDRFAArabicname.get(objDoc.GDRFA_Parent_Document_Id__c));
                                        }
                System.debug('mapSRDocIdGDRFADocId:'+mapSRDocIdGDRFADocId);
                System.debug('mapSRDocIdGdrfaIsMand:'+mapSRDocIdGdrfaIsMand);
                for(Attachment objAttch: [SELECT Id, Name, ContentType, Body from Attachment where Id IN:mapSRDocIdGDRFADocId.keySet()]){
                    //Webservice to push Sr docs
                    JSONGenerator docJsonObj = JSON.createGenerator(true);
                    docJsonObj.writeStartObject();
                    docJsonObj.writeFieldName('Documents');
                    docJsonObj.writeStartObject();
                    System.debug('mapSRDocIds1:'+mapSRDocIdGDRFADocId.get(objAttch.Id));
                    docJsonObj.writeObjectField('documentTypeId', mapSRDocIdGDRFADocId.get(objAttch.Id));
                    docJsonObj.writeBlobField('document', objAttch.Body);    
                    docJsonObj.writeBooleanField('IsMandatory', mapSRDocIdGdrfaIsMand.get(objAttch.Id)); 
                    docJsonObj.writeStringField('MineType', objAttch.ContentType);
                    docJsonObj.writeStringField('documentNameEn', mapSRDocIdGDRFAname.get(objAttch.Id)); 
                    docJsonObj.writeStringField('documentNameAr', mapSRDocIdGDRFAArabicName.get(objAttch.Id)); 
                    docJsonObj.writeStringField('FileName', objAttch.Name); 
                    docJsonObj.writeEndObject();
                    docJsonObj.writeStringField('applicationId', responseDocList.applicationId);
                    docJsonObj.writeEndObject();
                    finalDocJSON = docJsonObj.getAsString();
                    System.debug('DEBUG Full Request finalJSON : ' +(docJsonObj.getAsString()));
                    HttpRequest requestUploadDoc = new HttpRequest();
                    requestUploadDoc.setEndPoint(System.label.GDRFA_Document_Upload_Endpoint);        
                    requestUploadDoc.setBody(docJsonObj.getAsString());
                    requestUploadDoc.setTimeout(120000);
                    requestUploadDoc.setHeader('apikey', System.label.GDRFA_API_Key);
                    requestUploadDoc.setHeader('Content-Type', 'application/json');
                    requestUploadDoc.setMethod('POST');
                    requestUploadDoc.setHeader('Authorization',token_type+' '+access_token);
                    HttpResponse responseUploadDoc = new HTTP().send(requestUploadDoc);
                    System.debug('responseUploadDoc:'+responseUploadDoc.toString());
                    System.debug('STATUS:'+responseUploadDoc.getStatus());
                    System.debug('STATUS_CODE:'+responseUploadDoc.getStatusCode());
                    System.debug('strJSONresponseUploadDoc:'+responseUploadDoc.getBody());
                    if (responseUploadDoc.getStatusCode() == 200)
                    {
                        UploadDoc docUpload = (UploadDoc) System.JSON.deserialize(responseUploadDoc.getBody(), UploadDoc.class);
                        System.debug('IsSuccess:'+docUpload.Success);
                        if(docUpload.Success){
                            isAllUploadSuccess = true;   
                        }else{
                            isAllUploadSuccess = false;
                        }   
                    }
                    else{
                        Log__c ErrorobjLog = new Log__c();
                        ErrorobjLog.Type__c = objAttch.Id+'-'+mapSRDocIdGDRFAname.get(objAttch.Id)+'-Document Upload Error';
                        ErrorobjLog.Description__c = 'Response: '+ responseUploadDoc.getBody();
                        ErrorobjLog.SR_Step__c = visaRenewStep.Id;
                        ErrorObjLogList.add(ErrorobjLog); 
                        isAllUploadSuccess = false;
                        
                    }
                }
                if(!ErrorObjLogList.isEmpty() && ErrorObjLogList != null){
                    isAllUploadSuccess = false;
                    try{
                        insert ErrorObjLogList;
                    }catch(Exception ex) {
                        System.Debug(ex);
                    }
                    //insert attachment;
                    //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
                    visaRenewStep.Status__c = StatusReference;
                    visaRenewStep.OwnerId = UserInfo.getUserId();
                    visaRenewStep.Step_Notes__c = 'Document Upload Error';
                    try{
                        Update visaRenewStep;
                    }catch(Exception ex) {
                        insert LogDetails.CreateLog(null, 'VISA Renewal Step', 'SR Id is ' + visaRenewStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                    }
                    
                }   
            }
            else {
                WebServGDRFAAuthFeePaymentClass.ErrorClass ErrorLog = (WebServGDRFAAuthFeePaymentClass.ErrorClass) System.JSON.deserialize(response.getBody(), WebServGDRFAAuthFeePaymentClass.ErrorClass.class);
                Log__c objLog = new Log__c();objLog.Type__c = 'Submit Renew Residence Error ';objLog.Description__c = visaRenewJson +'Response: '+ response.getBody();objLog.SR_Step__c = visaRenewStep.Id;insert objLog;
                //insert attachment;
                //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
                if(ErrorLog.errorMsg[0].messageEn != null){
                    visaRenewStep.Step_Notes__c = ErrorLog.errorMsg[0].messageEn;
                }
                visaRenewStep.Status__c = StatusReference;
                visaRenewStep.OwnerId = UserInfo.getUserId();
                try{
                    update visaRenewStep; 
                }catch(Exception ex) {
                    insert LogDetails.CreateLog(null, 'VISA Renewal Step on Error', 'SR Id is ' + visaRenewStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                }
                
            }
            //Webservice to get the Application Fee 
            if(isAllUploadSuccess){
                WebServGDRFAAuthFeePaymentClass.getApplicationFeeAPI(responseDocList.applicationId, access_token, token_type, ID, visaRenewStep, StatusReference);    
            }
            insert attachment;
        }catch(exception e){
            insert attachment;
            system.debug('Error:- ' + e);
            visaRenewStep.Status__c = StatusReference;
            visaRenewStep.OwnerId = UserInfo.getUserId();
            visaRenewStep.Step_Notes__c = 'Error:- ' + e;
            try{
                Update visaRenewStep; 
            }catch(Exception ex) {
                insert LogDetails.CreateLog(null, 'VISA Renewal Step on Exception', 'SR Id is ' + visaRenewStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
            }
            Log__c objLog = new Log__c();objLog.Type__c = 'VISA Renewal' + e;objLog.Description__c = 'Error:- ' + e; objLog.SR_Step__c = visaRenewStep.Id; insert objLog; 
        }
    }
    
}