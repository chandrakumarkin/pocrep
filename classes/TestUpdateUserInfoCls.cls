/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 31-May-2015   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class TestUpdateUserInfoCls {

    public static testmethod void testUser() {
        Account acc = new Account(Name = 'TestAccount',BP_No__c='0000008978');
        insert acc;
        
        string contInd;
        string conRT1;
        for(RecordType conInd : [select Id,Name,DeveloperName from RecordType where DeveloperName='Individual' AND SobjectType='Contact' AND IsActive=true])
                     contInd = conInd.Id;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true])
                  conRT1 = objRT.Id;
        list<Contact> listcon = new list<Contact>();
        Contact cont1 = new Contact(firstname='Timmy',lastname='John',Passport_No__c='ASD12322',Email='test12356@test.com',RecordTypeId=contInd);
        insert cont1;
        Contact cont = new Contact(firstname='Tim',lastname='Khan',Individual_Contact__c=cont1.id,accountId=acc.id,Email='test@test.com',RecordTypeId=conRT1,Passport_No__c='ASD12322');
        insert cont;
        
        User usr = new User();
        usr.IsActive = true;
        usr.CompanyName = acc.id;
        usr.username = 'testDIFC@difcportal.ae';
        usr.contactId=cont.Id;
        usr.firstname='Test';
        usr.lastname='Test';
        usr.email='test@difc.ae';
        //usr.phone = SR.Send_SMS_To_Mobile__c;
        usr.Title = 'Mr.';
        usr.communityNickname = (usr.firstname +string.valueof(Math.random()).substring(4,9));
        usr.alias = string.valueof(usr.firstname.substring(0,1) + string.valueof(Math.random()).substring(4,9));            
        usr.profileid = Label.Profile_ID;
        usr.emailencodingkey='UTF-8';
        usr.languagelocalekey='en_US';
        usr.localesidkey='en_GB';
        usr.timezonesidkey='Asia/Dubai';
        usr.Community_User_Role__c = 'Employee Services';
        insert usr;
        
        UpdateUserInfo.usrUpdate();
        
        
        
    
        
    }
    
}