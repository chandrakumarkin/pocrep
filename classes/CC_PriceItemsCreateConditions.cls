public without sharing class CC_PriceItemsCreateConditions {

    public static List<string> trexclusion = system.label.TR_Exclusion.split(',');
    
    public static boolean Check_Condition(string strMethodName,string strSRID){
    
       boolean bResult = false;
        system.debug('&&&&&&&&&&&&&&Check_Condition&&&&&&&&strMethodName&&&&&&&&'+strMethodName);
        system.debug('&&&&&&&&&&&&&&Check_Condition&&&&&&&&&&&strSRID&&&&&'+strSRID);
        if(strMethodName!=null && strMethodName!='' && strSRID!=null && strSRID!=''){
        
              if( strMethodName =='checkVisaProfessionChange'){
                bResult = checkVisaProfessionChange(strSRID);
            }
        }
        
        return bResult;
    }
    
     public static boolean checkVisaProfessionChange(string StrSRID){
        
         return CC_VisaAmenedmentPriceController.checkVisaProfessionChange(strSRID);
    }
    
}