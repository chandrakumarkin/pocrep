@isTest
public with sharing class FormReviewSuperAdminControllerTest1 {
    
    static testmethod Void mytest1(){
        
       Test.starttest(); 
         // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'Super User Authorisation - Generated'});
        insert listDocMaster;
        // create SR Template doc
        
         //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'Super User Authorization','Superuser_Authorization'});
        insert createSRTemplateList;
        
        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        insert listSRTemplateDoc;
        
        
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(1, new List<string> {'Superuser_Authorization', 'Superuser_Authorization'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        
        insertNewSRs[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        insert insertNewSRs;
       
         List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Role__c = 'Shareholder';
        objAmendment.Passport_No__c = '12345';
        objAmendment.Gender__c = 'Male';
        objAmendment.Mobile__c = '+9712346572';
        objAmendment.DI_Mobile__c = '+97124567882';
        objAmendment.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment);
        HexaBPM_Amendment__c objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment3.Nationality_list__c = 'India';
        objAmendment3.Role__c = 'Shareholder';
        objAmendment3.Passport_No__c = '1r45';
        objAmendment3.Gender__c = 'Female';
        objAmendment3.Mobile__c = '+9712346572';
        objAmendment3.DI_Mobile__c = '+97124567882';
        objAmendment3.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment3);
        INSERT listAmendment;
        
        // create SR Doc
        List<HexaBPM__SR_Doc__c> listSRDoc = new List<HexaBPM__SR_Doc__c>();
        listSRDoc = OB_TestDataFactory.createSRDoc(1, insertNewSRs, listDocMaster, listSRTemplateDoc);
        listSRDoc[0].HexaBPM__Doc_ID__c = listAmendment[0].Id;
        listSRDoc[0].HexaBPM__Sys_IsGenerated_Doc__c = true;
        listSRDoc[0].Name ='Super User Authorisation - Generated';
        upsert listSRDoc;
        
        Set<Id> setDocumentID = new Set<Id>();
        
        for(HexaBPM__SR_Doc__c eachDoc:listSRDoc){
            setDocumentID.add(eachDoc.Id);
        }
        
        FormReviewSuperAdminController.Emailwithoutfuture(setDocumentID);
        FormReviewSuperAdminController.dummytest();
      Test.stoptest();
    }
}