/*
    Author      :   Ravi
    Description :   This class is used to create/update the Pending Comments for a ST
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    10-03-2015  Ravi            Created   
    V1.1    02-08-2016  Ravi            For HANA migration, while storing the Date & Time fields in Salesforce format got changed. Earliar it used to be like 01-01-2016 but not it is 20160101
    V1.2    15-10-2016  Sravan          Added for Password Decryption Tkt # 3392
    v1.4    30-04-2019  Azfer           Added the call for shipment creation.
*/
public without sharing class GsPendingInfo {
    
    public Pending__c PendingDetails {get;set;}
    public Step__c Step {get;set;}
    public string SRId {get;set;}
    string CustomerName = '';
    public Boolean isChecked {get;set;}
    public ID Account{get;set;}
        
    public GsPendingInfo(Apexpages.Standardcontroller con){
        Step = new Step__c();
        if(Apexpages.currentPage().getParameters().get('SRId') != null && Apexpages.currentPage().getParameters().get('SRId') != ''){
            SRId = Apexpages.currentPage().getParameters().get('SRId');
            for(Step__c objS : [select Id,Name,SR__c,SR__r.Name,SR__r.Customer__c,SR__r.Customer__r.Name,Is_Courier_Enabled__c from Step__c where SR__c=:SRId AND SAP_Seq__c = '0010']){
                Step = objS;
                SRId = objS.SR__c;
                CustomerName = objS.SR__r.Customer__r.Name;
                Account = objS.SR__r.Customer__c;
            }
            
            //there will be always only one active pending record for SR
            PendingDetails = new Pending__c(Service_Request__c=SRId,Step__c=Step.Id,Start_Date__c=system.now());
            
            for(Pending__c objP : [select Id,Internal_Pending__c,Internal_Pending_X__c,Account__c,Name,Start_Date__c,End_Date__c,FD_Error__c,Service_Request__c,Step__c,Comments__c,Remove_Pending__c,Pending_Type__c,Is_Courier_Related__c,No_Of_Docs__c,Courier_Description__c,Type_Of_Courier__c,SAP_PSUSR__c
                                    from Pending__c where End_Date__c = null AND Service_Request__c=:SRId]){
                PendingDetails = objP;
            }
        }else if(Apexpages.currentPage().getParameters().get('id') != null && Apexpages.currentPage().getParameters().get('id') != ''){
            
            //there will be always only one active pending record for SR
            //PendingDetails = new Pending__c(Service_Request__c=SRId,Step__c=Step.Id,Start_Date__c=system.now());
            
            for(Pending__c objP : [select Id,Internal_Pending__c,Internal_Pending_X__c,Name,Account__c,Start_Date__c,End_Date__c,Service_Request__c,Step__c,Comments__c,Remove_Pending__c,Pending_Type__c,Is_Courier_Related__c,No_Of_Docs__c,Courier_Description__c,Type_Of_Courier__c,SAP_PSUSR__c,FD_Error__c 
                                    from Pending__c where id=:Apexpages.currentPage().getParameters().get('id')]){
                SRId = objP.Service_Request__c;
            }
            
            for(Step__c objS : [select Id,Name,SR__c,SR__r.Name,SR__r.Customer__r.Name,Is_Courier_Enabled__c from Step__c where SR__c=:SRId AND SAP_Seq__c = '0010']){
                Step = objS;
                SRId = objS.SR__c;
                CustomerName = objS.SR__r.Customer__r.Name;
            }
            
            //there will be always only one active pending record for SR
            PendingDetails = new Pending__c(Service_Request__c=SRId,Step__c=Step.Id,Start_Date__c=system.now());
            
            //v1.5 added the Is_Courier_Enabled__c field
            for(Pending__c objP : [select Id,Internal_Pending__c,Internal_Pending_X__c,Name,Account__c,Start_Date__c,End_Date__c,Service_Request__c,Step__c,Comments__c,Remove_Pending__c,Pending_Type__c,Is_Courier_Related__c,No_Of_Docs__c,Courier_Description__c,Type_Of_Courier__c,SAP_PSUSR__c,FD_Error__c,Is_Courier_Enabled__c
                                    from Pending__c where End_Date__c = null AND Service_Request__c=:SRId]){
                PendingDetails = objP;
            }
        }
    }
    
    public Pagereference SavePending(){
        try{
            if(PendingDetails != null){
                if(PendingDetails.Remove_Pending__c){
                    PendingDetails.End_Date__c = system.now();
                }else{
                    PendingDetails.End_Date__c = null;
                    if(PendingDetails.Internal_Pending__c == false){                    
                    PendingDetails.Account__c = Account;
                    PendingDetails.Internal_Pending_X__c = false;
                    
                    }
                }
                string result = PushPendingToSAP();
                if(result != 'Success' || Apexpages.hasMessages()){
                    return null;
                }
                
                upsert PendingDetails;

                //v1.4 Added the code below
                //v1.5 added the Is_Courier_Related__c check
                if( PendingDetails.Step__c != null && !PendingDetails.Remove_Pending__c && PendingDetails.Is_Courier_Related__c){
                    CC_CourierCls.CreateShipment( PendingDetails.Step__c );
                }

                //v1.4 ended
                //added on 15th April, to create the Customer Step for Pending
                //CreateCustomerStep(PendingDetails);
                
                Pagereference ref = new Pagereference('/'+PendingDetails.Service_Request__c+'?nooverride=1');
                ref.setRedirect(true);
                return ref;
            }
        }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, ex.getMessage()));
        }
        return null;
    }
    
    public string PushPendingToSAP(){
        SAPGSWebServices.ZSF_S_GS_ACTIVITY item;
        User CurrentUser = new User();
        
        list<Step__c> lstSptes = new list<Step__c>();
        
        for(User obj : [select Id,SAP_User_Id__c from User where Id=:Userinfo.getUserId() AND SAP_User_Id__c != null]){
            CurrentUser = obj;
            GsSapWebServiceDetails.CurrentUserId = obj.SAP_User_Id__c;
        }
        if(CurrentUser.Id == null){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'You cannot put a pending because user does not exist in SAP.'));
            return null;
        }
        for(Step__c objStep : [select Id,Name,SAP_Seq__c,Step_Status__c,OwnerId,Owner.Name,LastModifiedBy.SAP_User_Id__c,Biometrics_Required__c,
                                    SAP_AMAIL__c,SAP_BANFN__c,SAP_BIOMT__c,SAP_BNFPO__c,SAP_CMAIL__c,SAP_CRMAC__c,SAP_EACTV__c,SAP_EMAIL__c,SAP_ERDAT__c,SAP_ERNAM__c,
                                    SAP_ERTIM__c,SAP_ETIME__c,SAP_FINES__c,SAP_FMAIL__c,SAP_GSDAT__c,SAP_GSTIM__c,SAP_IDDES__c,SAP_IDNUM__c,
                                    SAP_IDTYP__c,SAP_MDATE__c,SAP_MEDIC__c,SAP_MEDRT__c,SAP_MEDST__c,SAP_MFDAT__c,SAP_MFSTM__c,SAP_NMAIL__c,SAP_POSNR__c,SAP_RESCH__c,
                                    SAP_RMATN__c,SAP_STIME__c,SAP_TMAIL__c,SAP_UNQNO__c,SAP_ACRES__c,ID_End_Date__c,ID_Start_Date__c,SAP_MATNR__c,
                                    SR__c,SR__r.SAP_Unique_No__c,SR__r.SAP_PR_No__c,SR__r.SAP_SGUID__c,SR__r.SAP_MATNR__c,SR__r.Is_Pending__c,
                                    SR__r.SAP_OSGUID__c,SR__r.SAP_UMATN__c,SR__r.Name,SAP_OSGUID__c,SAP_UMATN__c,
                                    Step_Template__c,Step_Template__r.Name,Step_Id__c,No_PR__c,SAP_DOCDL__c,SAP_DOCRC__c,SAP_DOCCO__c,SAP_DOCCR__c,SAP_CTOTL__c,SAP_CTEXT__c,
                                    SR__r.Consignee_FName__c,SR__r.Consignee_LName__c,
                                    (select Id,Name,Comments__c,Start_Date__c,End_Date__c,CreatedBy.SAP_User_Id__c,LastModifiedBy.SAP_User_Id__c from Pendings__r)
                                    from Step__c where Id =: PendingDetails.Step__c]){
            
            item = GsSapWebServiceDetails.PrepareActivity(objStep);
            item.ACTIN = 'CHG';
            
            if(PendingDetails.Remove_Pending__c == false){
                item.PESTR = 'X';
                item.PSDAT = SAPDateFormat(PendingDetails.Start_Date__c);
                item.PSTIM = SAPTimeFormat(PendingDetails.Start_Date__c);
                item.PSUSR = CurrentUser.SAP_User_Id__c;
                item.IPEND = (PendingDetails.Internal_Pending__c == true)? 'X':'';//veera added for internal pending
                PendingDetails.SAP_PSUSR__c = CurrentUser.SAP_User_Id__c;
                //item.IUSER ='KKB';
            }else if(PendingDetails.Remove_Pending__c){
                item.PEEND = 'X';
                item.PEDAT = SAPDateFormat(PendingDetails.End_Date__c);
                item.PETIM = SAPTimeFormat(PendingDetails.End_Date__c);
                item.PEUSR = CurrentUser.SAP_User_Id__c;
               // item.IPEND = '';
            }
            item.PRMRK = PendingDetails.Comments__c;
            //added for GS Courier Process
            if(PendingDetails.Is_Courier_Related__c){
                if(PendingDetails.Type_Of_Courier__c == 'Delivery'){
                    item.PCFLG = 'D';
                    //item.DOCDL = 'X';
                }else if(PendingDetails.Type_Of_Courier__c == 'Collection'){
                    item.PCFLG = 'C';
                    //item.DOCCO = 'X';
                }else if(PendingDetails.Type_Of_Courier__c == 'Delivery with Return'){
                    item.PCFLG = 'R';
                    //item.DOCRC = 'X';
                }else if(PendingDetails.Type_Of_Courier__c == 'Collection with Return'){
                    item.PCFLG = 'E';
                    //item.DOCCR = 'X';
                }   
                if(PendingDetails.Type_Of_Courier__c == 'Delivery' || PendingDetails.Type_Of_Courier__c == 'Delivery with Return'){
                    item.CNSNM = objStep.SR__r.Consignee_FName__c+' '+objStep.SR__r.Consignee_LName__c;//Consignee Name
                    item.SHPNM = 'DIFC';//Shipper Name
                }else{
                    item.CNSNM = 'DIFC';//Consignee Name
                    item.SHPNM = objStep.SR__r.Consignee_FName__c+' '+objStep.SR__r.Consignee_LName__c;//Shipper Name
                }
                /*if(objShip.Status__c == 'Ready'){
                    item.CCRDT = GsPendingInfo.SAPDateFormat(system.now());//Courier Creation Date
                    item.CCRTM = GsPendingInfo.SAPTimeFormat(system.now());//Courier Creation Time
                }*/
                item.CTOTL = PendingDetails.No_Of_Docs__c != null ? string.valueOf(PendingDetails.No_Of_Docs__c) : '';
                item.CTEXT = PendingDetails.Courier_Description__c;
                if(PendingDetails.Remove_Pending__c)
                    item.CSTAT = 'Closed';//Courier Status
                else    
                    item.CSTAT = 'Pending to Push';//Courier Status
            }
        }
        if(item != null){
            SAPGSWebServices.ZSF_TT_GS_ACTIVITY objActivityData = new SAPGSWebServices.ZSF_TT_GS_ACTIVITY();
            objActivityData.item = new list<SAPGSWebServices.ZSF_S_GS_ACTIVITY>{item};
            
            SAPGSWebServices.serv_actv objSFData = new SAPGSWebServices.serv_actv();        
            objSFData.timeout_x = 120000;
            objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
            objSFData.inputHttpHeaders_x= new Map<String, String>();
             string  decryptedSAPPassWrd = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456';  // V1.3
           // Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c); Commented for V1.3
            Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+decryptedSAPPassWrd);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
            
            system.debug('request $$$ : '+objActivityData);
            
            SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element response = objSFData.Z_SF_GS_ACTIVITY(objActivityData,null);
            system.debug('response $$$ : '+response.EX_GS_SERV_OUT.item);
            
            if(response.EX_GS_SERV_OUT != null && response.EX_GS_SERV_OUT.item != null){
                for(SAPGSWebServices.ZSF_S_GS_SERV_OP objStepRes : response.EX_GS_SERV_OUT.item){
                    if(objStepRes.ACCTP != 'P'){
                        Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, objStepRes.SFMSG));
                    }
                }
            }
        }
        
        return 'Success';
    }
    
    public static void CreateCustomerStep(Pending__c ObjPending,string CustomerName){
        Step__c objStep;
        for(Step__c objS : [Select Id,SR__c,Step_Notes__c,Client_Name__c,Step_Template__c,Status__c,OwnerId,Start_Date__c,Pending__c,Customer_Comments__c,Status_Code__c from Step__c where Pending__c =: ObjPending.Id]){
            objStep = objS; 
        }
        if(ObjPending.Is_Courier_Related__c && ObjPending.Is_Courier_Enabled__c){
            map<string,string> mapStatus = new map<string,string>();
            map<string,string> mapTemplates = new map<string,string>();
            list<Step__c> lstCourierSteps = new list<Step__c>();
            list<string> lstStatus = new list<string>{'PENDING_COLLECTION','PENDING_DELIVERY','DELIVERED','COLLECTED','CLOSED'};
            list<Shipment__c> lstShips = new list<Shipment__c>();
            for(Status__c obj : [select Id,Code__c from Status__c where Code__c IN : lstStatus ]){
                mapStatus.put(obj.Code__c,obj.Id);
            }
            for(Step_Template__c obj : [select Id,Code__c from Step_Template__c where Code__c='GS_COURIER_COLLECTION' OR Code__c='GS_COURIER_DELIVERY']){
                mapTemplates.put(obj.Code__c,obj.Id);
            }       
            /*
                DOCDL - Document - Delivery (DE)
                DOCRC - Document - Return (RE)
                DOCCO - Document - Collect (CO)
                DOCCR - Document - Collect & Return (CO&RE)
            */
            //for(Step__c objS : lstSteps){
                Step__c objCStep = objStep != null ? new Step__c(Id=objStep.Id) : new Step__c();
                if(objCStep.Id == null){
                    objCStep.SR__c = ObjPending.Service_Request__c;
                    objCStep.Client_Name__c = CustomerName;
                    String Code = (ObjPending.Type_Of_Courier__c == 'Delivery' || ObjPending.Type_Of_Courier__c == 'Delivery with Return') ? 'PENDING_DELIVERY' : 'PENDING_COLLECTION';
                    Boolean isReturn = (ObjPending.Type_Of_Courier__c == 'Delivery with Return') ? true : false;
                    if(mapStatus.containsKey(Code))
                        objCStep.Status__c = mapStatus.get(Code);
                    Code = (Code == 'PENDING_DELIVERY') ? 'GS_COURIER_DELIVERY' : 'GS_COURIER_COLLECTION';
                    if(mapTemplates.containsKey(Code))
                        objCStep.Step_Template__c = mapTemplates.get(Code);
                    objCStep.Pending__c = ObjPending.Id;
                    objCStep.Sys_Courier_Check__c = true;
                    objCStep.Parent_Step__c = ObjPending.Step__c;
                    objCStep.SR_Group__c = 'GS';
                    objCStep.SAP_CTOTL__c = string.valueOf(ObjPending.No_Of_Docs__c);
                    objCStep.SAP_CTEXT__c = ObjPending.Courier_Description__c;
                    if(ObjPending.Type_Of_Courier__c == 'Delivery'){
                        objCStep.SAP_DOCDL__c = 'X';
                    }else if(ObjPending.Type_Of_Courier__c == 'Collection'){
                        objCStep.SAP_DOCCO__c = 'X';
                    }else if(ObjPending.Type_Of_Courier__c == 'Delivery with Return'){
                        objCStep.SAP_DOCRC__c = 'X';
                    }else if(ObjPending.Type_Of_Courier__c == 'Collection with Return'){
                        objCStep.SAP_DOCCR__c = 'X';
                    }
                }else if(ObjPending.End_Date__c != null){
                    //Close the Step
                    String Code = (ObjPending.Type_Of_Courier__c == 'Delivery' || ObjPending.Type_Of_Courier__c == 'Delivery with Return') ? 'DELIVERED' : 'COLLECTED'; 
                    if(mapStatus.containsKey(Code))
                        objCStep.Status__c = mapStatus.get(Code);
                    for(Shipment__c objSh : [select Id,Status__c,Step__c from Shipment__c where Step__c =: objCStep.Id]){
                        if(objSh.Status__c != 'Delivered'){
                            objSh.Status__c = 'Closed';
                            if(objSh.Step__c == objCStep.Id){
                                objCStep.Status__c = mapStatus.get('CLOSED');
                            }
                        }
                        lstShips.add(objSh);
                    }
                    if(lstShips.isEmpty()){
                        objCStep.Status__c = mapStatus.get('CLOSED');
                    }
                }
                lstCourierSteps.add(objCStep);
            //}
            
            if(!lstCourierSteps.isEmpty()){
                upsert lstCourierSteps;
                
                 //CC_CourierCls.CreateShipment( lstCourierSteps[0].id);
                
                }
            if(!lstShips.isEmpty())
                update lstShips;
        }else{
            objStep = objStep != null ? objStep : new Step__c();
            objStep.SR__c = ObjPending.Service_Request__c;
            objStep.Step_Notes__c = ObjPending.Comments__c;
            objStep.Client_Name__c = CustomerName;
            objStep.Pending__c = ObjPending.Id;
            if(ObjPending.End_Date__c == null){
                objStep.Step_Template__c = [select Id from Step_Template__c where Code__c='REQUIRE_MORE_INFORMATION_FROM_CUSTOMER' limit 1].Id;
                objStep.Status__c = [select Id from Status__c where Code__c='PENDING_CUSTOMER_INPUTS' limit 1].Id;
                objStep.OwnerId = [select QueueId from QueueSobject where Queue.Name = 'Client Entry User' limit 1].QueueId;
                objStep.Start_Date__c = objStep.Start_Date__c != null ? objStep.Start_Date__c : system.today();
                upsert objStep;
            }else if(ObjPending.End_Date__c != null && objStep.Status_Code__c=='PENDING_CUSTOMER_INPUTS'){
                objStep.Status__c = [select Id from Status__c where Code__c='Closed' limit 1].Id;
                objStep.Customer_Comments__c = 'Not Applicable';
                objStep.OwnerId = Userinfo.getUserId();
                upsert objStep;
            }
        }
    }
    
    public void doAction(){
        if(PendingDetails != null){
            PendingDetails.Is_Courier_Related__c = isChecked;
        }
    }
    
    public static string SAPDateFormat(Datetime dt){
        String Month = string.valueOf(dt.month()).length()==1?'0'+dt.month():''+dt.month();
        String Day = string.valueOf(dt.day()).length()==1?'0'+dt.day():''+dt.day();
        return dt.year()+'-'+Month+'-'+Day;
    }
    public static string SAPTimeFormat(Datetime dt){
        String Hours = string.valueOf(dt.hour()).length()==1?'0'+dt.hour():''+dt.hour();
        String Minitues = string.valueOf(dt.minute()).length()==1?'0'+dt.minute():''+dt.minute();
        String Seconds = string.valueOf(dt.second()).length()==1?'0'+dt.second():''+dt.second();
        return Hours+':'+Minitues+':'+Seconds;
    }
    public static string SAPDateFormat(Date dt){
        String Month = string.valueOf(dt.month()).length()==1?'0'+dt.month():''+dt.month();
        String Day = string.valueOf(dt.day()).length()==1?'0'+dt.day():''+dt.day();
        return dt.year()+'-'+Month+'-'+Day;
    }
    public static Date SFDCDateFormat(string sDate){
        Date dt;
        if(sDate != null && sDate != ''){
            dt = Date.newInstance(Integer.valueOf(sDate.split('-')[0]), Integer.valueOf(sDate.split('-')[1]), Integer.valueOf(sDate.split('-')[2]));
        }
        return dt;
    }
    public static string PendingKeyFormat(Datetime dt){
        string str = ''+dt.year();
        str += string.valueOf(dt.month()).length()==1?'0'+dt.month():''+dt.month();
        str += string.valueOf(dt.day()).length()==1?'0'+dt.day():''+dt.day();
        str += '_';
        str += string.valueOf(dt.hour()).length()==1?'0'+dt.hour():''+dt.hour();
        str += string.valueOf(dt.minute()).length()==1?'0'+dt.minute():''+dt.minute();
        str += string.valueOf(dt.second()).length()==1?'0'+dt.second():''+dt.second();
        return str;
    }
    
    //V1.1
    public static String SAPHANADateFormat(String s){
        if(s != null && s != ''){
            s = s.left(4)+'-'+s.left(6).right(2)+'-'+s.right(2);
        }
        return s;
    }
    public static String SAPHANATimeFormat(String s){
        if(s != null && s != ''){
            s = s.left(2)+':'+s.left(4).right(2)+':'+s.right(2);
        }
        return s;
    }
}