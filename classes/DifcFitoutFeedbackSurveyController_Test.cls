/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 11-19-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   11-19-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
private class DifcFitoutFeedbackSurveyController_Test {
    static testMethod void myUnitTest() {
        createTestConfig();
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Fit_Out_Units','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        SR_Template__c contractorAccessTemplate = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Request for Contractor Access','Request_Contractor_Access','Fit-Out & Events');
        
        insert contractorAccessTemplate;
        
        Account objContractor = Test_CC_FitOutCustomCode_DataFactory.getTestContractor();
        objContractor.is_Registered__c = true;
        
        insert objContractor;
        
        Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
        
        insert objAccount;
        
        Building__c testBuilding = Test_CC_FitOutCustomCode_DataFactory.getTestBuilding();
        
        insert testBuilding;
        
        List<Unit__c> testUnits = Test_CC_FitOutCustomCode_DataFactory.getTestUnits(testBuilding.Id);
        Lease__c testLease = Test_CC_FitOutCustomCode_DataFactory.getTestLease(objAccount.Id);
        License__c testLicense = Test_CC_FitOutCustomCode_DataFactory.getTestLicense(objAccount.Id);
        
        //TODO: Add any dependent data declarations 
        insert testUnits;
        insert testLicense;
        insert testLease;

        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Status__c objStatusApproved = new Status__c();
        objStatusApproved.Name = 'Approved';
        objStatusApproved.Code__c = 'APPROVED';
        insert objStatusApproved;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
           
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'Submitted';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);  
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'Approved';
        lstSRStatus.add(objSRStatus);

        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Fit to Occupy Certificate Issued';
        objSRStatus.Code__c = 'Fit_to_Occupy_Certificate_Issued';        
        
        lstSRStatus.add(objSRStatus);
             
        insert lstSRStatus;

        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit - Out Service Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        
        insert testSrTemplate;
        
        SR_Steps__c srstep = new SR_Steps__c();
        insert srstep;
        
        Service_Request__c SR111= new Service_Request__c();
        SR111.RecordTypeId = mapRecordType.get('Request_Contractor_Access');
        SR111.Customer__c = objAccount.Id;
        SR111.external_SR_Status__c = lstSRStatus[2].Id;
        //SR111.Building__c = lstBuilding[0].Id;
        //SR.Service_Category__c = 'New'; 
        SR111.email__c = 'mudasir.w@difc.ae';      
        insert SR111;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        SR1.email__c = 'mudasir.w@difc.ae';
        sr1.Linked_SR__C = SR111.id;
        
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;
        sr.External_SR_Status__c = lstSRStatus[3].id;
        sr.email__c = 'mudasir.w@difc.ae';
        //SR.SR_Group__c='Fit-Out & Events';
        SR.Linked_SR__c = SR1.id;     
        SR.Type_of_Request__c = 'Type A'; 
        //sr.Service_Type__c ='Fit - Out Service Request';
        SR.Linked_SR__C = SR1.id;
        insert SR;

        Step__c testStep = Test_CC_FitOutCustomCode_DataFactory.getTestStep(SR.Id,[SELECT ID FROM Status__c where name = 'Pending'].Id,'Test Contractor'); 
    
        testStep.Step_Template__c = [SELECT Id FROM Step_Template__c LIMIT 1].Id;
        
        insert testStep;

        Survey_Question__c survQues = new Survey_Question__c(Question__c='Test',active__c=true);
        insert survQues;

        Test.startTest();
            DifcFitoutFeedbackSurveyController.getServeyFeedbackWrapperRecord(testStep.id);
        	List<Feedback_Survey__c> feedbackList = DifcFitoutFeedbackSurveyController.getRelatedQuestions(testStep.id);
        	DifcFitoutFeedbackSurveyController.insertFitoutFeedback(feedbackList);
        	DifcFitoutFeedbackSurveyController.updateStep(testStep.Id, 'remarks');
         Test.stopTest();
     }

     private static void createTestConfig(){
        
        Sr_Status__c testDraftStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Draft','DRAFT');
        Sr_Status__c testSubmittedStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Submitted','SUBMITTED');
        
        //insert new Status__c(Name='Approved',Code__c='APPROVED');
        //insert new Status__c(Name='Pending',Code__c='PENDING');
        
        insert new Step_Template__c(Code__c = 'RE_UPLOAD_DOCUMENT',Name='Re-upload Document',Step_RecordType_API_Name__c = 'General');
        insert new Step_Template__c(Code__c = 'UPLOAD_DOCUMENT',Name='Upload Detailed Design Documents',Step_RecordType_API_Name__c = 'General');
        
        //insert new List<Sr_Status__c>{testDraftStatus,testSubmittedStatus};
        
        insert Test_CC_FitOutCustomCode_DataFactory.getCountryLookupList();
        
        insert Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
        
    }
}