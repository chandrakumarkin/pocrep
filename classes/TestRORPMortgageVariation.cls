/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRORPMortgageVariation {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        list<Building__c> lstBuilding = new list<Building__c>();
		
		Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP On Plan';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.SAP_Building_No__c = '123456';
        lstBuilding.add(objBuilding);
        
        objBuilding = new Building__c();
        objBuilding.Name = 'RORP Off Plan';
        objBuilding.Building_No__c = '000002';
        objBuilding.Company_Code__c = '5300';
        objBuilding.SAP_Building_No__c = '1234567';
        //objBuilding.Permit_Date__c = system.today().addMonths(-1);
        //objBuilding.Building_Permit_Note__c = 'test class data';
        lstBuilding.add(objBuilding);
        
        insert lstBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c =  lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Residential';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP Off Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c =  lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP Off Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Residential';
        lstUnits.add(objUnit);
        insert lstUnits;
		
		map<string,string> mapRecordType = new map<string,string>();
		for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Mortgage_Registration','Discharge_Mortgage','Mortgage_Variation','RORP_Account','RORP_Contact')]){
			mapRecordType.put(objRT.DeveloperName,objRT.Id);
		}
		
		Account objAccount = new Account();
        objAccount.Name = 'RORP Customer';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        objAccount.RecordTypeId = mapRecordType.get('RORP_Account');
        objAccount.Issuing_Authority__c = 'DMCC';
        objAccount.RORP_License_No__c = '1234567';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Boina';
        objContact.Birthdate = system.today().addYears(-20);
        objContact.Nationality__c = 'India';
        objContact.RecordTypeId = mapRecordType.get('RORP_Contact');
    	insert objContact;
    	
    	Product2 objProduct = new Product2();
	    objProduct.Name = 'Mortgage Registration';
	    objProduct.IsActive = true; 
	    insert objProduct;
		
		list<Pricing_Line__c> lstPricingLines = new list<Pricing_Line__c>();
		Pricing_Line__c objPricingLine = new Pricing_Line__c();
		objPricingLine.Name = 'Caveat Registration';
	    objPricingLine.Product__c = objProduct.Id;
	    objPricingLine.Active__c = true;
	    objPricingLine.Priority__c = 12;
	    lstPricingLines.add(objPricingLine);
	    
	    objPricingLine = new Pricing_Line__c();
		objPricingLine.Name = 'Islamic Mortgage';
	    objPricingLine.Product__c = objProduct.Id;
	    objPricingLine.Active__c = true;
	    objPricingLine.Priority__c = 12;
	    lstPricingLines.add(objPricingLine);
	    
	    objPricingLine = new Pricing_Line__c();
		objPricingLine.Name = 'Conventional Registration';
	    objPricingLine.Product__c = objProduct.Id;
	    objPricingLine.Active__c = true;
	    objPricingLine.Priority__c = 12;
	    lstPricingLines.add(objPricingLine);
	    
	    insert lstPricingLines;
	    
	    list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
		for(Pricing_Line__c objPL : lstPricingLines){
			Dated_Pricing__c objDP = new Dated_Pricing__c();
		    objDP.Pricing_Line__c = objPL.Id;
		    objDP.Unit_Price__c = 1200;
		    objDP.Appllication_Type__c = 'Add';
		    objDP.Date_From__c = system.today().addYears(-2);
		    objDP.Use_List_Price__c = true;
		    lstDP.add(objDP);
		}
		insert lstDP;
    	
    	list<Lease__c> lstLeases = new list<Lease__c>();
    	Lease__c objLease = new Lease__c();
    	objLease.Name = '987654321';
    	objLease.Lease_Types__c = 'Mortgage Property Registration';
    	objLease.Status__c = 'Active';
    	objLease.Type__c = 'Mortgage';
    	lstLeases.add(objLease);
    	
    	objLease = new Lease__c();
    	objLease.Name = '987654320';
    	objLease.Lease_Types__c = 'Mortgage Property Registration';
    	objLease.Status__c = 'Active';
    	objLease.Type__c = 'Purchased';
    	lstLeases.add(objLease);
    	
    	objLease = new Lease__c();
    	objLease.Name = '987654320';
    	objLease.Lease_Types__c = 'Mortgage Property Caveat';
    	objLease.Status__c = 'Active';
    	objLease.Type__c = 'Purchased';
    	lstLeases.add(objLease);
    	
    	insert lstLeases;
    	
    	list<Lease_Partner__c> lstLPs = new list<Lease_Partner__c>();
    	
    	Lease_Partner__c objLP = new Lease_Partner__c();
    	objLP.Name = lstUnits[0].Name;
    	objLP.Lease__c = lstLeases[0].Id;
    	objLP.Unit__c = lstUnits[0].Id;
    	objLP.Status__c = 'Active';
    	objLP.Contact__c = objContact.Id;
    	objLP.SAP_PARTNER__c = '001234';
    	objLP.Unit__c = lstUnits[0].Id;
    	objLP.Mortgage_Degree__c = 'Primary';
    	objLP.Lease_Types__c = 'Mortgage Property Registration';
    	lstLPs.add(objLP);
    	
    	objLP = new Lease_Partner__c();
    	objLP.Name = lstUnits[1].Name;
    	objLP.Lease__c = lstLeases[0].Id;
    	objLP.Unit__c = lstUnits[1].Id;
    	objLP.Status__c = 'Active';
    	objLP.Account__c = objAccount.Id;
    	objLP.SAP_PARTNER__c = '001234';
    	objLP.Mortgage_Degree__c = 'Primary';
    	objLP.Lease_Types__c = 'Mortgage Property Registration';
    	lstLPs.add(objLP);
    	
    	objLP = new Lease_Partner__c();
    	objLP.Lease__c = lstLeases[1].Id;
    	objLP.Unit__c = lstUnits[0].Id;
    	objLP.Status__c = 'Active';
    	objLP.Contact__c = objContact.Id;
    	objLP.Mortgage_Degree__c = 'Primary';
    	objLP.Lease_Types__c = 'Mortgage Property Registration';
    	objLP.SAP_PARTNER__c = '001234';
    	lstLPs.add(objLP);
    	
    	objLP = new Lease_Partner__c();
    	objLP.Lease__c = lstLeases[1].Id;
    	objLP.Unit__c = lstUnits[0].Id;
    	objLP.Status__c = 'Active';
    	objLP.Account__c = objAccount.Id;
    	objLP.Mortgage_Degree__c = 'Primary';
    	objLP.Lease_Types__c = 'Mortgage Property Registration';
    	objLP.SAP_PARTNER__c = '001234';
    	lstLPs.add(objLP);
    	
    	objLP = new Lease_Partner__c();
    	objLP.Name = lstUnits[0].Name;
    	objLP.Lease__c = lstLeases[2].Id;
    	objLP.Unit__c = lstUnits[0].Id;
    	objLP.Status__c = 'Active';
    	objLP.Account__c = objAccount.Id;
    	objLP.SAP_PARTNER__c = '001234';
    	objLP.Mortgage_Degree__c = 'Primary';
    	objLP.Lease_Types__c = 'Mortgage Property Caveat';
    	lstLPs.add(objLP);
    	
    	objLP = new Lease_Partner__c();
    	objLP.Name = lstUnits[1].Name;
    	objLP.Lease__c = lstLeases[2].Id;
    	objLP.Unit__c = lstUnits[1].Id;
    	objLP.Status__c = 'Active';
    	objLP.Account__c = objAccount.Id;
    	objLP.SAP_PARTNER__c = '001234';
    	objLP.Mortgage_Degree__c = 'Primary';
    	objLP.Lease_Types__c = 'Mortgage Property Caveat';
    	lstLPs.add(objLP);
    	
    	insert lstLPs;
    	
    	test.startTest();
    	
    		Apexpages.currentPage().getParameters().put('RecordType',mapRecordType.get('Mortgage_Variation'));
			Apexpages.currentPage().getParameters().put('isDetail','false');
			
        	Apexpages.Standardcontroller objSCon = new Apexpages.Standardcontroller(new Service_Request__c());
			RORPMortgageVariation objVariation = new RORPMortgageVariation(objSCon);
			objVariation.ContextObj.SapBPNo = '001234';
			objVariation.AddNewUnits();
			objVariation.BuildingChange();
			objVariation.BuildingId = lstBuilding[0].Id;
			objVariation.SearchString = '12';
			objVariation.ContextObj.SapBPNo = '001234';
			objVariation.SearchUnits();
    		
    		if(objVariation.AllUnits != null && objVariation.AllUnits.size() > 0){
				objVariation.AllUnits[0].isChecked = true;
				objVariation.AddUnits();
			}
    		
    		objVariation.CancelUnits();
			objVariation.AddToMortgage();
			objVariation.ServiceRequest.Customer__c = objAccount.Id;
			
			objVariation.AddToMortgage();
    		objVariation.MortgageUnit.Amendment.Unit_Rent__c = -120000;
    		objVariation.MortgageUnit.Amendment.End_Date__c = system.today().addYears(-1);
    		objVariation.AddToMortgage();
    		objVariation.MortgageUnit.Amendment.End_Date__c = system.today().addYears(2);
    		objVariation.AddToMortgage();
    		objVariation.MortgageUnit.Amendment.Unit_Rent__c = 120000;
    		objVariation.AddToMortgage();
    		
    		objVariation.CancelMortgage();
    		objVariation.EditRequest();
			objVariation.RowIndex = 0;
			objVariation.EditAmendment();
			
    		for(RORPMortgageVariation.UnitDetails objUD : objVariation.MortgageUnits){
				objUD.Amendment.Building2__c = lstBuilding[0].Id;
			}
    		objVariation.SaveSR();
    		
    		objVariation.ContextObj.IsDetail = false;
			objVariation.CancelRequest();
			
			Apexpages.currentPage().getParameters().put('id',objVariation.ServiceRequest.Id);
			objVariation = new RORPMortgageVariation(objSCon);
			objVariation.ServiceRequest.Required_Docs_not_Uploaded__c = false;
			objVariation.SubmitRequest();
			
			objVariation.RowIndex = 0;
			objVariation.EditAmendment();
			
			objVariation.ServiceRequest.Submitted_Date__c = system.today();			
			objVariation.AddToMortgage();
			
			objVariation.RowIndex = 0;
			objVariation.RemoveUnits();
			
			RORPMortgageVariation.UnitDetails objUD = new RORPMortgageVariation.UnitDetails();
			objUD.MortgageNature = '';
			objUD.MortgageValue = 0;
			objUD.Unit = new Unit__c();
    		
    	test.stopTest();
    	
    }
}