/**
 * Apex Controller for looking up an SObject via SOSL 
 */
public without sharing class OB_LookupSObjectController {
    /**
     * Aura enabled method to search a specified SObject for a specific string
     */
    @AuraEnabled
    public static Result[] lookup(String searchString, 
                                        String sObjectAPIName, 
                                            String whereClause, 
                                                String rawSOQL,
                                                    String context,
                                                        String filterString,
                                                            String parentFieldName,
                                                                String srId) {
        // Sanitze the input
        String sanitizedSearchString = String.escapeSingleQuotes(searchString);
        String sanitizedFilterString;
        String sanitizedParentFieldName;
        if(String.isNotBlank(filterString))
            sanitizedFilterString = String.escapeSingleQuotes(filterString);
        if(String.isNotBlank(parentFieldName))
            sanitizedParentFieldName = String.escapeSingleQuotes(parentFieldName);
        String sanitizedSObjectAPIName = String.escapeSingleQuotes(sObjectAPIName);

        List<Result> results = new List<Result>();
        if(String.isBlank(rawSOQL)){
            String returningFields = '(id,name)';
            if(String.isNotBlank(parentFieldName))
                returningFields = '(id,'+sanitizedParentFieldName+') ';
            if(String.isNotBlank(whereClause)){
                returningFields =' (id,name WHERE '+whereClause+') ';
                if(String.isNotBlank(parentFieldName))
                    returningFields = '(id,'+sanitizedParentFieldName+' WHERE '+whereClause+') ';
            }

            
            
            // Build our SOSL query
            String searchQuery = 'FIND \'' + sanitizedSearchString + '*\' IN ALL FIELDS RETURNING ' + sanitizedSObjectAPIName +returningFields+' Limit 50'; 

            System.debug('-->searchQuery'+searchQuery);

            // Execute the Query
            List<List<SObject>> searchList = search.query(searchQuery);

            // Create a list of matches to return
            for (SObject so : searchList[0]) {
                if(String.isNotBlank(parentFieldName))
                    results.add(new Result(so, (String)so.get(sanitizedParentFieldName), so.Id));
                else
                    results.add(new Result(so, (String)so.get('Name'), so.Id));
            }
        }else{
            string AccountName;
            string acId;
            System.debug('-->rawSOQL'+rawSOQL);
            System.debug('---sanitizedFilterString'+sanitizedFilterString);
            rawSOQL = rawSOQL.replace('searchString', sanitizedSearchString);
            System.debug('>>rawSOQL'+rawSOQL);
            if(String.isNotBlank(sanitizedFilterString))
                rawSOQL = rawSOQL.replace('filterString', sanitizedFilterString);
            if(String.isNotBlank(srId) && String.isNotBlank(sObjectAPIName) && sObjectAPIName == 'HexaBPM_Amendment__c'){
                rawSOQL = rawSOQL.replace('lookUpSRID', srId);
            }
            System.debug('sanitized>>'+rawSOQL);
            if(String.isNotBlank(context) && String.isNotBlank(rawSOQL)){
                //run quieries specific context wise
            }else{
                for(Sobject so : Database.query(rawSOQL)){
                    if(String.isNotBlank(parentFieldName)){
                        if(parentFieldName.indexOf('.') > -1 && sObjectAPIName == 'License_Activity__c'){
                            AccountName = (string)so.getSObject('Account__r').get('Name');
                            acId = (string)so.getSObject('Account__r').get('Id');
                            results.add(new Result(so, AccountName, acId));
                        }
                        else{
                            results.add(new Result(so, (String)so.get(sanitizedParentFieldName), so.Id));
                        }
                    }
                    else
                        results.add(new Result(so, (String)so.get('Name'), so.Id)); 
                }
            }
        }
        
        return results;
    }
     /**
     * Inner class to wrap up an SObject Label and its Id
     */
    @AuraEnabled
    public static string searchLookUpName(string lookupId, String sObjectAPIName )
    {
        system.debug('@@@@@@@ calling look up ');
        string lookUpName;
        String sanitizedSearchString = '\''+ String.escapeSingleQuotes(lookupId) + '\'';
        string rawSOQL = 'select id,Name from ' + sObjectAPIName + ' where id = '+sanitizedSearchString;
        for(Sobject so : Database.query(rawSOQL)){
            lookUpName = (String)so.get('Name');
            //results.add(new Result(so, (String)so.get('Name'), so.Id));  
        }
        return lookUpName;

    }
    
    /**
     * Inner class to wrap up an SObject Label and its Id
     */
    public class Result {
        @AuraEnabled public String SObjectLabel {get; set;}
        @AuraEnabled public Id SObjectId {get; set;}
        @AuraEnabled public SObject sObj {get;set;}
        public Result(SObject sObj, String sObjectLabel, Id sObjectId) {
            this.sObj = sObj;
            this.SObjectLabel = sObjectLabel;
            this.SObjectId = sObjectId;
        }
    }
}