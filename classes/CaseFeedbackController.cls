public without sharing class CaseFeedbackController {
    public string comments {get;set;}
    public string rating {get;set;}
    public boolean isThank {get;set;}
    public boolean isSubmitted {get;set;}
    public string caseId;
    public string emailRefId;
    public CaseFeedbackController(){
        caseId = ApexPages.currentPage().getParameters().get('caseid');
        emailRefId = ApexPages.currentPage().getParameters().get('refid');
    	isThank = false;
        isSubmitted = false;
    }
    public pageReference doSubmit(){
        try{
            if(String.isNOtBlank(caseId)){
                for(Case caseObj : [select id from Case where id = :caseId]){
                    Case_Feedback__c feedbackObj = new Case_Feedback__c();
                    feedbackObj.RecordTypeId=  Schema.SObjectType.Case_Feedback__c.getRecordTypeInfosByName().get('Case').getRecordTypeId();
                    feedbackObj.Case__c = caseId;
                    feedbackObj.Rating__c = rating;
                    feedbackObj.Comment__c = comments;
                    insert feedbackObj;
                    isThank = true;
                }
            }
            return null;
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
            return null;
        }
    }
}