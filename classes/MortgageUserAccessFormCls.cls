/************************************************************************************************************************
    Author      :   Sravan Booragadda
    Class Name  :   MortgageUserAccessFormCls
    Description :   Custom Code for User Access Provision
  
************************************************************************************************************************/

global without sharing class MortgageUserAccessFormCls {
    
    Public List<SelectOption> typeofAccess{set;}    
    Public string typeofAccessSelected{get;set;}
    public string userName{get;set;}
    Public string licenseNo{get;set;}
    Public Service_Request__c UserAccessServiceRequest{get;set;}
    
    public MortgageUserAccessFormCls(){
         
        UserAccessServiceRequest = new Service_Request__c(Email__c='');
        typeofAccessSelected='';
    }
    
     
    
    public pagereference CancelRegistration()
    {
       /* UserAccessServiceRequest = new Service_Request__c(Email__c='');
        userName='';
        typeofAccessSelected='';
        licenseNo ='';
        userName =''; */
        
        pagereference pg = new pageReference('/home/home.jsp');
        return pg;
    }   
    
    public List<SelectOption> gettypeofAccess(){
        List<SelectOption> options = new List<selectoption>();
        options.add(new SelectOption('-None-','-None-'));
        options.add(new SelectOption('Create New User','Create New User'));
        options.add(new SelectOption('Update Existing User','Update Existing User'));
        options.add(new SelectOption('Remove Existing User','Remove Existing User'));
        
        return options;
        
    }
    
    public void displayExistingUserDetails(){
        List<User> existingActiveUser = new List<User>();       
        if(userName !=null && typeofAccessSelected!=null)
        {
            try{
            existingActiveUser = [select id,name,UserName,AccountID,contact.Account.Active_License__r.name,Community_User_Role__c,contact.Nationality__c,
                                         ContactID,contact.Middle_Names__c,Contact.Salutation__c,Email,Title,LastName,FirstName,Contact.title,Contact.Salutation,
                                         Contact.Passport_No__c,contact.MobilePhone,Contact.Nationality_Lookup__c from User where UserName = :userName and contactID !=null and
                                         UserName=:userName and isActive =true and  Community_User_Role__c includes('Mortgage Registration','Discharge of Mortgage','Variation of Mortgage') ];
            
            if(existingActiveUser !=null && existingActiveUser.Size() >0 )
            {
                
                    UserAccessServiceRequest.Customer__c = existingActiveUser[0].AccountID;
                    UserAccessServiceRequest.Type_of_Access__c = existingActiveUser[0].Community_User_Role__c;
                    UserAccessServiceRequest.License_Number__c = existingActiveUser[0].Contact.Account.Active_License__r.name;
                    UserAccessServiceRequest.First_Name__c = existingActiveUser[0].FirstName;
                    UserAccessServiceRequest.Last_Name__c = existingActiveUser[0].LastName;
                    UserAccessServiceRequest.Send_SMS_To_Mobile__c = existingActiveUser[0].contact.MobilePhone;
                    UserAccessServiceRequest.Email__c = existingActiveUser[0].Email;
                    UserAccessServiceRequest.Title__c = existingActiveUser[0].Contact.Salutation;
                    UserAccessServiceRequest.Nationality_List__c = existingActiveUser[0].Contact.Nationality__c;
                    UserAccessServiceRequest.Passport_Number__c = existingActiveUser[0].contact.Passport_No__c;
                    UserAccessServiceRequest.Confirm_Change__c  = existingActiveUser[0].contact.Is_Approver__c;             
                            
                
            } 
             else
                   apexpages.addMessage(new ApexPages.Message(apexPages.Severity.error,'No user exists with the provided details,please contact System Administrator'));
            
            }Catch(Exception e)
            {
                apexpages.addMessage(new ApexPages.Message(apexPages.Severity.error,e.getMessage()));   
            }            
          
        }
    }
    
    Public pagereference SaveRequest(){ 
            
            try{
                 
                 if(Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Mortgage User Access Form').getRecordTypeId() !=null)
                    UserAccessServiceRequest.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Mortgage User Access Form').getRecordTypeId();
                 else
                    apexpages.addMessage(new ApexPages.Message(apexPages.Severity.info,'Record Type not found, Please contact System Administrator'));  
                system.debug('---------------->'+UserAccessServiceRequest);    
                if(UserAccessServiceRequest !=null)
                {
                    if(typeofAccessSelected=='Create New User')
                    {
                        List<Service_Request__c> checkExistingServiceRequests = new List<Service_Request__c>();
                        checkExistingServiceRequests = [select id,Name,Customer__c,Customer__r.Name,Email__c,Passport_Number__c,Nationality_list__c from Service_Request__c 
                                                                where Email__c =:UserAccessServiceRequest.Email__c and(Nationality_list__c=:UserAccessServiceRequest.Nationality_list__c 
                                                                or Passport_Number__c =: UserAccessServiceRequest.Passport_Number__c) and Customer__c =:UserAccessServiceRequest.Customer__c and Internal_Status_Name__c='Draft' limit 1];
                    
                        if(checkExistingServiceRequests !=null && checkExistingServiceRequests.size()>0)
                        {
                            apexpages.addMessage(new ApexPages.Message(apexPages.Severity.error,'Cannot create a request ,There is an open Service Request'+checkExistingServiceRequests[0].Name+'for the same Client'));
                            return null;
                        }
                        List<User> existingUser = new List<User>([select id,email from User where Email=:UserAccessServiceRequest.Email__c and (contact.Passport_No__c =:UserAccessServiceRequest.Passport_Number__c or contact.Nationality__c=:UserAccessServiceRequest.Nationality_list__c) 
                                                                        and contact.AccountID =:UserAccessServiceRequest.Customer__c and IsActive=true and contactid !=null]);
                        if(existingUser !=null && existingUser.size()>0)
                        {
                            apexpages.addMessage(new ApexPages.Message(apexPages.Severity.error,'Cannot create a request ,There is an active user with same details Ref ID:'+existingUser[0].id));    
                            return null;
                        }
                    }
                    UserAccessServiceRequest.User_Form_Action__c = typeofAccessSelected; 
                    insert UserAccessServiceRequest;
                    pagereference serviceRequestDetailPage = new ApexPages.StandardController(UserAccessServiceRequest).view(); 
                    serviceRequestDetailPage.setRedirect(false);
                    return serviceRequestDetailPage;
                     
                }
               else 
                {               
                    
                    apexpages.addMessage(new ApexPages.Message(apexPages.Severity.error,'Some Internal Error occured,Please contact Support'));
                    return null;
                }    
                
                
                        
            }
            catch(Exception e){
                apexpages.addMessage(new ApexPages.Message(apexPages.Severity.error,e.getMessage()+e.getLineNumber()));
                return null;
            }   
    }
    
    
    
    public static string MortgageUserAccessApproval(step__c stp){         
           List<SR_Status__c> approvalStatus = new List<SR_Status__c>();
           Savepoint sp;
           if(stp !=null && stp.id !=null){                                  
                 approvalStatus = [select id,name from SR_Status__c where name='Approved'];                  
           }
           
        try{ 
             sp =  Database.SetSavepoint(); 
             contact con = new contact();
             Service_Request__c srToApprove = new Service_Request__c();
             if(approvalStatus.size()>0)
             {
                 srToApprove.id = stp.sr__c;
                 srToApprove.External_SR_Status__c = approvalStatus[0].id;
                 srToApprove.Internal_SR_Status__c = approvalStatus[0].id;               
                 update srToApprove;
             }
             else
              return 'Unable to Approve Service Request Please contact System Administrator';
             if(stp !=null && stp.SR__r.User_Form_Action__c=='Create New User'){
                
                con.Salutation= stp.SR__r.Title__c;
                con.Email = stp.SR__r.Email__c;
                con.firstName = stp.SR__r.First_Name__c;
                con.LastName = stp.SR__r.Last_name__c;
                con.Nationality__c =stp.SR__r.Nationality_list__c;
                con.Passport_No__c = stp.SR__r.Passport_Number__c ;
                con.MobilePhone = stp.SR__r.Send_SMS_To_Mobile__c;
                con.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Mortgage Portal User').getRecordTypeId();
                con.AccountID = stp.SR__r.Customer__c;
                con.Is_Approver__c = stp.SR__r.Confirm_Change__c;
                insert con;
             }
             
             MortgageUserAccessFormCls.CommitUserChanges(stp.id,con.id);
             
         }catch(Exception e){
            Database.Rollback(sp);
           // system.debug('Message'+e.getMessage());
            return e.getMessage();  
            
         }  
        return 'success';
    }
    
    @future
    public static void CommitUserChanges(id stpID,id ConID){
        
           list<step__c> stepDetails = new list<Step__c>();
           stepDetails = [select id,Name,SR__r.Service_Type__c,SR__r.Type_of_Access__c,SR__r.User_Form_Action__c,SR__r.Passport_Number__c,SR__r.Customer__c,
                                   SR__r.Nationality_list__c,SR__r.Title__c,SR__r.Last_Name__c,SR__r.First_Name__c,SR__r.Middle_Name__c,SR__r.Name,
                                   SR__r.Send_SMS_To_Mobile__c,SR__r.Email__c from Step__c where id =:stpID];
       
            if(stepDetails !=null && stepDetails.size() >0)             
                    MortgageUserAccessFormCls.ProcessUserRequest(stepDetails[0],ConID);               
                            
         
      } 
      
    
      public static void ProcessUserRequest(step__c  stepDetails,id ConID)
      {   
        
       List<User> existingActiveUser = new List<User>();
            existingActiveUser = [select id,name,UserName,AccountID,Community_User_Role__c,contact.Middle_Names__c,Contact.Salutation__c,Email,Title,LastName,FirstName,contact.Title,
                                         Contact.Passport_No__c,contact.MobilePhone,Contact.Nationality_Lookup__c from User where Email = :stepDetails.SR__r.Email__c and ContactID !=null and
                                         contact.Accountid =:stepDetails.SR__r.Customer__c and isActive =true and  Community_User_Role__c includes('Mortgage Registration','Discharge of Mortgage','Variation of Mortgage')];   
        try
        {               
                   User usr = new User();
                   Usr.firstName = stepDetails.SR__r.First_Name__c;
                   Usr.LastName  = stepDetails.SR__r.Last_name__c;
                   usr.phone = stepDetails.SR__r.Send_SMS_To_Mobile__c;
                   usr.Community_User_Role__c = stepDetails.SR__r.Type_of_Access__c;
                   usr.Email = stepDetails.SR__r.Email__c;                 
                   
               
                if(stepDetails.sr__r.User_Form_Action__c=='Create New User' && conID !=null)
                {
                       // usr.UserName = stepDetails.SR__r.Email__c.split('@')[0]+Label.Portal_UserDomain;
                        usr.UserName = MortgageUserAccessFormCls.prepareUserName(stepDetails.SR__r.Email__c);
                        usr.alias = string.valueof(stepDetails.SR__r.Last_Name__c.substring(0,1) + string.valueof(Math.random()).substring(4,9));
                        usr.Title =  stepDetails.SR__r.Title__c;
                        usr.emailencodingkey='UTF-8';
                        usr.languagelocalekey='en_US';
                        usr.localesidkey='en_GB';
                        usr.profileid = Label.Mortgage_User_Profile;
                        usr.timezonesidkey='Asia/Dubai';
                        usr.contactID = conID;
                        Database.DMLOptions dlo = new Database.DMLOptions();
                        dlo.EmailHeader.triggerUserEmail = true;
                        dlo.EmailHeader.triggerAutoResponseEmail= true;
                        usr.setOptions(dlo);     
                        insert usr;  
                        
                                  
                }                
                else if(stepDetails.sr__r.User_Form_Action__c=='Update Existing User' && existingActiveUser.size()>0)
                {
                        usr.id = existingActiveUser[0].id;
                        Update usr;        
                }
                else if(stepDetails.sr__r.User_Form_Action__c=='Remove Existing User' && existingActiveUser.size()>0){
                    usr.isActive = false;
                    usr.id = existingActiveUser[0].id;
                    update usr;
                }
                   
          
        }catch(exception e){
            system.debug('Exception is : '+e.getMessage());
            Log__c objLog = new Log__c();
            objLog.Description__c ='Line No===>'+e.getLineNumber()+'---Message==>'+e.getMessage();
            objLog.Type__c = 'Mortgage'+stepDetails.sr__r.User_Form_Action__c+'failed';            
            insert objLog;
        }
      }
      
      public static string prepareUserName(string Email)
      {
          
                String emailStr = Email.split('@')[0]+'%';
                String UserName = '';
                set<string> userNames = new set<String>();
                
                for(User objUsr : [select Id,Username from User where Username like :emailStr order by Username]){
                    userNames.add(objUsr.Username);
                }
                
                UserName = Email.split('@')[0]+Label.Portal_UserDomain;
                if(userNames.contains(UserName)){                         
                    for(Integer i=1;i<=Integer.valueof(Label.Username_limit);i++){
                        UserName = Email.split('@')[0]+'_'+i+Label.Portal_UserDomain;
                        if(!userNames.contains(UserName)){
                            userNames.add(UserName);
                            break;
                        }
                    }
                }
             return UserName ;   
      
      }
      
      public static void dmymthd()
      {
          if(test.isrunningtest())
          {
          user usr = new user();
                        
                        usr.emailencodingkey='UTF-8';
                        usr.languagelocalekey='en_US';
                        usr.localesidkey='en_GB';
                        usr.profileid = Label.Mortgage_User_Profile;
                        usr.timezonesidkey='Asia/Dubai';
                        
                        
            user usr1 = new user();
                      
                        usr1.emailencodingkey='UTF-8';
                        usr1.languagelocalekey='en_US';
                        usr1.localesidkey='en_GB';
                        usr1.profileid = Label.Mortgage_User_Profile;
                        usr1.timezonesidkey='Asia/Dubai';  
                        
                         user usr21 = new user();
                        
                        usr21.emailencodingkey='UTF-8';
                        usr21.languagelocalekey='en_US';
                        usr21.localesidkey='en_GB';
                        usr21.profileid = Label.Mortgage_User_Profile;
                        usr21.timezonesidkey='Asia/Dubai';
                        
                         user usr3 = new user();
                        
                        usr3.emailencodingkey='UTF-8';
                        usr3.languagelocalekey='en_US';
                        usr3.localesidkey='en_GB';
                        usr3.profileid = Label.Mortgage_User_Profile;
                        usr3.timezonesidkey='Asia/Dubai';
                        
                         user usr5 = new user();
                       
                        usr5.emailencodingkey='UTF-8';
                        usr5.languagelocalekey='en_US';
                        usr5.localesidkey='en_GB';
                        usr5.profileid = Label.Mortgage_User_Profile;
                        usr5.timezonesidkey='Asia/Dubai'; 
                        
                        user usr4 = new user();
                        usr4.emailencodingkey='UTF-8';
                        usr4.languagelocalekey='en_US';
                        usr4.localesidkey='en_GB';
                        usr4.profileid = Label.Mortgage_User_Profile;
                        usr4.timezonesidkey='Asia/Dubai'; 
                        
                        
                        user usr6 = new user();
                        usr6.emailencodingkey='UTF-8';
                        usr6.languagelocalekey='en_US';
                        usr6.localesidkey='en_GB';
                        usr6.profileid = Label.Mortgage_User_Profile;
                        usr6.timezonesidkey='Asia/Dubai'; 
                        
                         user usr7 = new user();
                        usr7.emailencodingkey='UTF-8';
                        usr7.languagelocalekey='en_US';
                        usr7.localesidkey='en_GB';
                        usr7.profileid = Label.Mortgage_User_Profile;
                        usr7.timezonesidkey='Asia/Dubai'; 
                        
                        user usr8 = new user();
                        usr8.emailencodingkey='UTF-8';
                        usr8.languagelocalekey='en_US';
                        usr8.localesidkey='en_GB';
                        usr8.profileid = Label.Mortgage_User_Profile;
                        usr8.timezonesidkey='Asia/Dubai'; 
                        
                        user usr9 = new user();
                        usr9.emailencodingkey='UTF-8';
                        usr9.languagelocalekey='en_US';
                        usr9.localesidkey='en_GB';
                        usr9.profileid = Label.Mortgage_User_Profile;
                        usr9.timezonesidkey='Asia/Dubai';    
               }                
      
      }
      
     
   
    
}