/****************************************************************
*Class name : consentUtilityTest
* Description ;Test Class for consentUtility Apex Class
****************************************************************/
@isTest
private class consentUtilityTest{
    
    public static Account objAccount;
    static Building__c objBuilding ;
    static map<string,string> mapRecordType = new map<string,string>();
    static List<Service_Request__c> servReqList = new List<Service_Request__c>();
    static list<Unit__c> lstUnits;
    public static void prepareData(){
        
        objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        insert objBuilding;
        
        lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        objUnit.Is_official_Search__c=true;
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        objAccount.RORP_License_No__c='R123';
        objAccount.Issuing_Authority__c ='IC';
        insert objAccount;
        
        Contact c1= new Contact();
        c1.lastname='c1';
        c1.Passport_No__c='P123';
        c1.Nationality__c='india';
        c1.RecordTypeId= [select id from RecordType where DeveloperName='GS_Contact' and sObjectType='contact' LIMIT 1].Id;
        insert c1;
       
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('User_Access_Form','RORP_User_Access_Form','IT_User_Access_Form','')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        
    }
    static TestMethod void unit1(){
        test.starttest();
        prepareData();
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordType.get('User_Access_Form');
        objSR.Building__c = objBuilding.Id;
        objSR.Unit__c = lstUnits[0].Id;
        objSR.Ownership_type__c='Joint Tenant';
        objSR.SR_group__c='RORP';
        servReqList.add(objSR);
        
        insert servReqList;
        
        consentUtility cc = new consentUtility();
        consentUtility.createConsent(servReqList);
        
        PageReference vfPage= Page.User_Access_Form;
        vfPage.getParameters().put('id',servReqList[0].Id);
        Test.setCurrentPage(vfPage); 
        test.stoptest();
    }
    
}