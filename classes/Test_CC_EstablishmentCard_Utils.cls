/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CC_EstablishmentCard_Utils {
  
   static Map<string,string> recTypeMap = new Map<string,String>();
  @testSetup
  static void prepareTestData(){
    List<Establishment_Card_Components__c> ecList = new List<Establishment_Card_Components__c>();
      Establishment_Card_Components__c ec;
      
      ec= new Establishment_Card_Components__c();
      ec.Name = 'Application_of_Registration';
      ec.Document_Master__c = 'Government Services Authorized Signatory,Signed GSO Authorized Signatory,Signed Certificate of Incorporation Registration,Signed Commercial License,Personnel Sponsorship Agreement';
      ec.Product_Master__c = 'GSO Authorized Signatory,PSA,Courier Charges-Establishment Card,Establishment Card-New';
      ec.Validate_Docs__c = 'Signed Commercial License,Signed Certificate of Incorporation Registration';
      ecList.add(ec);
      
      ec= new Establishment_Card_Components__c();
      ec.Name = 'Change_of_Entity_Name';
      ec.Document_Master__c = 'Signed Commercial License,Company Establishment Card';
      ec.Product_Master__c = 'Establishment Card-Amendment,Courier Charges-Establishment Card';
      ec.Validate_Docs__c = 'Signed Commercial License';
      ecList.add(ec);
      
      ec= new Establishment_Card_Components__c();
      ec.Name = 'License_Renewal';
      ec.Document_Master__c = 'Signed Commercial License';
      ec.Product_Master__c = 'Establishment Card-Renewal,Courier Charges-Establishment Card,Establishment Card-Combo,Index Card Fine,Over-stay fine';
      ec.Validate_Docs__c = 'Signed Commercial License';
      ecList.add(ec);
      
      insert ecList;
      
      // Record Types
     
      for(RecordType rec: [select id,DeveloperName,Name from RecordType where SobjectType = 'Service_Request__c']){
        recTypeMap.put(rec.DeveloperName,rec.id);
      }
      
      // SR Status
      List<SR_Status__c> srsList = new List<SR_Status__c>();
      SR_Status__c srs ;
      srs = new SR_Status__c();
      srs.Name = 'Draft';
      srs.Code__c = 'Draft';
      srsList.add(srs);
      srs = new SR_Status__c();
      srs.Name = 'Submitted';
      srs.Code__c = 'SUBMITTED';
      srsList.add(srs);
      
      srs = new SR_Status__c();
      srs.Name = 'Approved';
      srs.Code__c = 'Approved';
      srsList.add(srs);
      insert srsList;
      
      CountryCodes__c  cc = new CountryCodes__c(name='971');
      insert cc;
      
      
      Account objAccountSC = new Account();
        objAccountSC.Name = 'Test Custoer SC Ltd';
        objAccountSC.E_mail__c = 'test@test.com';
        objAccountSC.BP_No__c = '001235';
        insert objAccountSC;
      
      List<service_Request__c> srList = new List<service_Request__c>();
      Service_Request__c objSR = new Service_Request__c();
        
        objSR.Customer__c = objAccountSC.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.legal_structures__c ='LTD';
        objSR.currency_list__c = 'US Dollar';
        objSR.Share_Capital_Membership_Interest__c =90;
        objSR.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR.RecordTypeId = recTypeMap.get('Application_of_Registration');
        objSR.Purpose_of_Notification__c = 'Prior to or immediately upon personal data processing';
        objSR.Transferring_Personal_Data_outside_DIFC__c = true;
        objSR.In_Accordance_with_Article_11__c = true;
        objSR.Avail_Courier_Services__c = 'No';
        objSr.Personal_Data_Processing_Procedure__c = 'test';
        Objsr.RecordTypeID = recTypeMap.get('Application_of_Registration');
        Objsr.Quantity__c = 0;
        srList.add(Objsr);
        
        
        insert srList;
        
        Amendment__c amnd1 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='GSO Authorized Signatory',Amendment_Type__c ='Individual',Passport_No__c='a3',Family_Name__c='Test',Given_Name__c='User',Job_Title__c='Developer');
        insert amnd1;
        
        
        
        SR_Doc__c srDoc = new SR_Doc__c();
    srDoc.Service_Request__c = objSR.id;
    srDoc.Name = 'Test User - Passport Copy';
    srDoc.Status__c = 'Generated';    
    srDoc.Unique_SR_Doc__c = amnd1.id+'Test User - Passport Copy';
    srDoc.Amendment__c = amnd1.id;
    insert srDoc;         
       
        
        Attachment att = new Attachment();
        att.Name = 'Test User - Passport Copy.pdf';
        att.contentType = '.pdf';
        att.parentID = srDoc.id;
        att.body = blob.ValueOf('test Attach');
        insert att;
        
        step__c stp = new Step__c();
        stp.SR__c = Objsr.id;
        insert stp;     
      
      List<SR_Template__c> Templist = new List<SR_Template__c>();
      SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'New_Index_Card';
        objTemplate1.SR_RecordType_API_Name__c = 'New_Index_Card';
        objTemplate1.Menutext__c = 'New_Index_Card';
        objTemplate1.Available_for_menu__c = true;
        objTemplate1.Template_Sequence_No__c = 123;
        objTemplate1.Menu__c = 'Company';
        objTemplate1.Active__c = true;
        Templist.add(objTemplate1);
        
        insert   Templist;
        
        
        
        Document_Master__c dm = new  Document_Master__c();
        dm.Name  = ' Government Services Authorized Signatory';
        dm.code__c = ' Government Services Authorized Signatory';
        insert dm;
        
        Product2  prd = new Product2(); 
        prd.Name = 'Government Services Authorized Signatory';
        prd.ProductCode = 'Government Services Authorized Signatory';
        prd.IsActive =true;
        prd.Family = 'GS';
        insert prd;
        
        Pricing_Line__c pl = new Pricing_Line__c();
        pl.Product__c = prd.id;
        pl.Name = 'Add/Remove GSO Authorized Signatory';
        pl.Material_Code__c = 'GO-00231';
        pl.DEV_Id__c = 'GO-00231';
        pl.Priority__c = 1;
        insert pl;
        
        
        
        srDoc = new SR_Doc__c();
    srDoc.Service_Request__c = objSR.id;
    srDoc.Name = 'Government Services Authorized Signatory';
    srDoc.Status__c = 'Generated';    
    srDoc.Unique_SR_Doc__c = amnd1.id+'Government Services Authorized Signatory';
    srDoc.Amendment__c = amnd1.id;
    srDoc.Document_Master__c = dm.id;
    insert srDoc;         
       
        
        att = new Attachment();
        att.Name = 'Test User - Passport Copy.pdf';
        att.contentType = '.pdf';
        att.parentID = srDoc.id;
        att.body = blob.ValueOf('test Attach');
        insert att;
        
        
        SR_Price_Item__c spi = new SR_Price_Item__c();
        spi.ServiceRequest__c = objSR.id;
        spi.Price__c = 25;
        spi.Price_in_USD__c = 7;
        spi.status__c = 'Blocked';
        spi.Pricing_Line__c = pl.id;
        insert spi;
        
        
        
    
  }
  

    static testMethod void myUnitTest() {
      step__c stp = [select id,sr__r.Record_Type_Name__c,sr__c from step__c limit 1];   
      test.startTest();        
        CC_EstablishmentCard_Utils.insertEstablishmentCardDraftFromReq(stp);
        CC_EstablishmentCard_Utils.createEstablishmentCardFromRocRequest(stp);
        CC_EstablishmentCard_Utils.updateESTServiceRequest(new list<id>{stp.id});
      //CC_EstablishmentCard_Utils.createEstablishmentCardFromRocRequestAsync(JSON.Serialize(stp));
        CC_EstablishmentCard_Utils.testsample();
        test.stoptest();
        
        Service_Request__c srObj = new Service_Request__c(id=stp.sr__c);
        srObj.RecordTypeID = Test_CC_EstablishmentCard_Utils.recTypeMap.get('Change_of_Entity_Name');
        update srObj;
        
        stp = [select id,sr__r.Record_Type_Name__c,sr__c from step__c limit 1]; 
        CC_EstablishmentCard_Utils.insertEstablishmentCardDraftFromReq(stp);
    }
}