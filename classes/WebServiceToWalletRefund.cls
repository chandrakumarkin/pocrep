/******************************************************************************************
*  Author   : Kumar Utkarsh
*  Company  : DIFC
*  Date     : 01-June-2021
*  Description : This is a class for Fitout Wallet refund and RORP refund                 
--------------------------------------------------------------------------------------------------------------------------
Modification History 
----------------------------------------------------------------------------------------
V.No   Date            Updated By          Description
----------------------------------------------------------------------------------------              
V1.0    01-June-2021      Utkarsh             Initial Version

*******************************************************************************************/

public class WebServiceToWalletRefund {
    
    public  static SAPPortalBalanceRefundService.ZSF_TT_REFUND requestStructure; // V1.0
    Public  static SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP responseStructure; // V1.0
    public  static SAPPortalBalanceRefundService.ZSF_TT_REFUND requestStructure1; // V1.0
    Public  static SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP responseStructure1; // V1.0
    public  static SAPPortalBalanceRefundService.ZSF_TT_REFUND requestStructure2; // V1.0
    Public  static SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP responseStructure2; // V1.0
    //fit-out refund service
    public static void refundContractWallet(String SRId){
        System.debug('Calldefaultaction');
        Service_Request__c srObj = [select id,Name,Type_of_Refund__c,Service_Type__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,record_Type_Name__c,
                                    Submitted_Date__c,Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c, Post_Code__c, Transfer_to_Account__r.BP_No__c,
                                    (select id,Name,Status__c,Step_Name__c,Status__r.Name from Steps_SR__r)
                                    from service_request__c where id=:SRId AND Service_Type__c = 'Request for Wallet Refund'];
        
        String result = AccountBalanceInfoCls.getPortalBalance(srObj.Customer__r.BP_No__c);
        System.debug('Result:'+result.replaceAll('Available Portal balance is : AED ', ''));
        if(srObj.Refund_Amount__c != null){
            //if(Double.ValueOf(result.replaceAll('Available Portal balance is : AED ', '')) >= srObj.Refund_Amount__c)
            //if(Double.ValueOf('166975612.92') >= srObj.Refund_Amount__c)
            PushSrToSAP(SRId, 'FCWR');
        }
        //String response  = PushAttToSAP(srObj);
        //System.debug('response:'+response);
    }
    //fit-out refund service- future call
    @future(callout=true)
    public static void PushSrToSAP(String SRId, String RefundType){
        Service_Request__c srObj = [select id,Name,Type_of_Refund__c,Service_Type__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,record_Type_Name__c,
                                    Submitted_Date__c,Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c, First_Name__c,Last_Name__c,SAP_Unique_No__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c, Post_Code__c, Transfer_to_Account__c, Transfer_to_Account__r.BP_No__c,Customer__c, 
                                    Payment_Method__c, Declaration_Signatory_Name__c, Address_Details__c, SAP_GSDAT__c,
                                    (select id,Name,Status__c,Step_Name__c,Status__r.Name from Steps_SR__r)
                                    from service_request__c where id=:SRId AND Service_Type__c = 'Request for Wallet Refund'];
        if(!test.isrunningtest()){
            try{
                RefundRequestUtils.PushToSAP(srObj, RefundType);
            }
            Catch(Exception e){
                System.debug('Exception::'+e.getMessage());
            }
        }
        String response  = PushAttToSAP(srObj);
        System.debug('response:'+response);
    }
    
    public static String PushAttToSAP(Service_Request__c srObj) {
        String response = '';
        
        string decryptedSAPPassWrd = test.isrunningTest()  ==   false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) : '123456';
        
        sapComDocumentSapSoapFunctionsMcS.ZSF_REF_ATTACHMENT  refundResponse;
        
        List <string> attachmentIDs                =   new List < string > ();
        
        for (sr_Doc__c srd: [select id, Doc_ID__c from sr_doc__c where Service_Request__c =: srObj.Id]) {
            if(srd.Doc_ID__c != null  && srd.Doc_ID__c != ''){
            attachmentIDs.add(srd.Doc_ID__c);
                }
        }
        System.debug('attachmentIDs:-'+attachmentIDs);
        if(attachmentIDs != null && attachmentIDs.size() > 0){
        refundResponse                      = new sapComDocumentSapSoapFunctionsMcS.ZSF_REF_ATTACHMENT();
        refundResponse.timeout_x            = 120000;
        refundResponse.clientCertName_x     = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
        refundResponse.inputHttpHeaders_x   = new Map < String, String > ();
        
        Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c + ':' +decryptedSAPPassWrd);
        String authorizationHeader         = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        
        refundResponse.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        
        if (attachmentIDs != null && attachmentIDs.size() > 0) {
            for (attachment att: [select id, body, Name, contentType from attachment where id in: attachmentIDs]) {
                system.debug(srObj+''+ att.Name+''+ att.contentType+''+ EncodingUtil.Base64Encode(att.body));
                sapComDocumentSapSoapFunctionsMcS.ZattResponse  item         = refundResponse.ZsfRefAttachment(att.contentType,att.Name,srObj.Name,EncodingUtil.Base64Encode(att.body));
                response = string.valueof(item);
            }
        }
        system.debug('responseAttchment:'+response);
        return response;
            }
        return 'No Attchments';
    }
    //RORP refund service
    public static void refundWalletRorpExpiry(String SRId){
        try{
            Service_Request__c srObj = [select id,Name,Type_of_Refund__c,Type_of_Request__c,Service_Type__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,record_Type_Name__c,
                                        Submitted_Date__c,Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,Payment_Method__c,
                                        Contact__r.BP_No__c,SAP_MATNR__c, Post_Code__c, Transfer_to_Account__r.BP_No__c,Sys_Estimated_Share_Capital__c,Authority_Name__c,
                                        Sponsor_Middle_Name__c,Quantity__c,SAP_GSDAT__c,Address_Details__c,Annual_Amount_AED__c,Share_Value__c,Middle_Name_Previous_Sponsor__c,
                                        Tax_Registration_Number_TRN__c,Cage_No__c,Security_Deposit_AED__c,Building__r.Name, Building__r.SAP_Building_No__c, Building__r.Developer_Lookup__r.Code__c, Lease_Contract_No__c,Unit__r.Name,Sponsor_Last_Name__c,Sponsor_Street__c,
                                        (select Id, Floor__c, Unit__r.Name, Unit__r.Building__r.name, Unit__r.Building__r.SAP_Building_No__c, Address1__c, Unit__r.SAP_Unit_No__c, Unit__r.Building__r.Developer_Lookup__r.Code__c from Amendments__r where Amendment_Type__c = 'Unit'),
                                        (select id,Name,Status__c,Step_Name__c,Status__r.Name from Steps_SR__r)
                                        from service_request__c where id=:SRId AND Type_of_Request__c IN ('Refund of part of the Security Deposit','Release of Security Deposit')];
            System.debug('srObj:::'+srObj);
            //if(srObj.Type_of_Request__c == 'Refund of part of the Security Deposit'){
            //    String result = AccountBalanceInfoCls.getPortalBalance(srObj.Customer__r.BP_No__c);
            //if(srObj.Share_Value__c != null){
            pushRefundPartialAmountSAP(SRId, 'RSDR');
            //}
            //  }
            //  if(srObj.Type_of_Request__c == 'Release of Security Deposit'){
            //      String result = AccountBalanceInfoCls.getPortalBalance(srObj.Customer__r.BP_No__c);
            //     if(srObj.Share_Value__c != null){
            //         pushRefundPartialAmountSAP(SRId, 'RSDR');
            //   }
            //   }
        }Catch(Exception e){
            String Result;
            Result = e.getMessage();
            System.debug('Result:'+Result);
        }
        
    }
    @future(callout=true)
    public static void pushRefundPartialAmountSAP(String SrId, String RefundType){
        
        Service_Request__c srObj = [select id,Name,Type_of_Refund__c,Service_Type__c,Type_of_Request__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,record_Type_Name__c,
                                    Submitted_Date__c,Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,Payment_Method__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c, Post_Code__c, Transfer_to_Account__r.BP_No__c,Sys_Estimated_Share_Capital__c,Authority_Name__c,Unit__r.Parent_Unit__c,Unit__r.Parent_Unit__r.SAP_Unit_No__c,
                                    Sponsor_Middle_Name__c,Quantity__c,SAP_GSDAT__c,Address_Details__c,Annual_Amount_AED__c,Share_Value__c,Middle_Name_Previous_Sponsor__c,Unit__r.SAP_Unit_No__c,
                                    Tax_Registration_Number_TRN__c,Cage_No__c,Security_Deposit_AED__c,Building__r.Name,Building__r.Developer_Lookup__r.Code__c, Building__r.SAP_Building_No__c, Lease_Contract_No__c, Unit__r.Name,Sponsor_Last_Name__c,Sponsor_Street__c,
                                    (select Id, Floor__c, Unit__r.Name, Unit__r.Building__r.name, Unit__r.Building__r.SAP_Building_No__c, Address1__c,Unit__r.Building__r.Developer_Lookup__r.Code__c, Unit__r.Parent_Unit__r.SAP_Unit_No__c, Unit__r.SAP_Unit_No__c from Amendments__r where Amendment_Type__c = 'Unit' and Unit__r.Building__r.name != null),
                                    (select id,Name,Status__c,Step_Name__c,Status__r.Name from Steps_SR__r)
                                    from service_request__c where id=:SRId AND Type_of_Request__c IN ('Refund of part of the Security Deposit','Release of Security Deposit')];
        PushServToSAP(srObj, RefundType);
        String response  = PushAttToSAP(srObj);
        System.debug('responseAttchment'+response);
    }
    
    public static SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP PushServToSAP(Service_Request__c  sr,string typeOfRefund){        
        Service_Request__c srObj = sr;             
        SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP refundResponse = PushRequestToSAP(srObj,typeOfRefund);
        system.debug('refundResponse::'+refundResponse);
        return refundResponse;
        
    }
    
    Public static SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP PushRequestToSAP(Service_Request__c srObj,string typeOfRefund){  
        string decryptedSAPPassWrd = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456' ;        
        SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP refundResponse;
        SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP refundResponse1;
        SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP refundResponse2;
        SAPPortalBalanceRefundService.ZSF_S_REFUND item;
        SAPPortalBalanceRefundService.ZSF_S_REFUND item1;
        SAPPortalBalanceRefundService.ZSF_S_REFUND item2;
        List<SAPPortalBalanceRefundService.ZSF_S_REFUND> items = new List<SAPPortalBalanceRefundService.ZSF_S_REFUND>();  
        
        if(srObj !=null){
            if(srObj.Type_of_Request__c =='Refund of part of the Security Deposit' && srObj.Share_Value__c != null){
                item = prepareSAPData(srObj, typeOfRefund, 'Tenant');
                
                system.debug('Refund Request'+ item);
                if(item !=null)
                {
                    
                    items.add(item);        
                    
                }
            }
            if(srObj !=null)
            {
                if(srObj.Type_of_Request__c =='Release of Security Deposit'){
                    //Tenant
                    if(srObj.Share_Value__c != null){
                        system.debug('test1');
                        item1 = prepareSAPData(srObj,typeOfRefund, 'Tenant');
                    }
                    //Landlord
                    if(srObj.Security_Deposit_AED__c != null){
                        system.debug('test2');
                        item2 = prepareSAPData(srObj,typeOfRefund, 'Landlord');
                    }
                }
                system.debug('Refund Request 1::'+ item1);
                system.debug('Refund Request 2::'+ item2);
                if(item1 !=null){
                    items.add(item1);        
                    
                }
                if(item2 !=null){
                    // List<SAPPortalBalanceRefundService.ZSF_S_REFUND> items = new List<SAPPortalBalanceRefundService.ZSF_S_REFUND>();  
                    items.add(item2);        
                    
                }
                
                SAPPortalBalanceRefundService.ZSF_TT_REFUND objTTRefund = new SAPPortalBalanceRefundService.ZSF_TT_REFUND();
                objTTRefund.item = items;
                System.debug('items==>'+items);
                SAPPortalBalanceRefundService.refund objRefund= new SAPPortalBalanceRefundService.refund();
                objRefund.timeout_x = 120000;
                objRefund.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c; 
                objRefund.inputHttpHeaders_x= new Map<String,String>();
                // Blob headerValue = Blob.valueOf('90029627:D!fc20171');
                Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+decryptedSAPPassWrd);
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                objRefund.inputHttpHeaders_x.put('Authorization', authorizationHeader);
                WebServiceToWalletRefund.requestStructure = objTTRefund; // V1.0
                refundResponse = objRefund.Z_SF_REFUND_PROCESS(objTTRefund);   
                system.debug('refundResponse!::'+refundResponse);
                WebServiceToWalletRefund.responseStructure = refundResponse; // V1.0   
                
            }         
        }
        System.debug('refundResponse'+refundResponse);
        System.debug('refundResponse1:'+refundResponse1);
        System.debug('refundResponse2::'+refundResponse2);
        return refundResponse1; 
    }
    
    Public static SAPPortalBalanceRefundService.ZSF_S_REFUND prepareSAPData(Service_Request__c srObj,string typeOfRefund, String TypeOfReceiver){
        
        SAPPortalBalanceRefundService.ZSF_S_REFUND item ;
        System.debug('srObj::'+srObj);
        System.debug('typeOfRefund::'+typeOfRefund);
        System.debug('srObj.Share_Value__c::'+srObj.Share_Value__c);
        //Tenant
        if(srObj !=null && typeOfRefund != null && srObj.Share_Value__c != null && TypeOfReceiver == 'Tenant'){
            item = new SAPPortalBalanceRefundService.ZSF_S_REFUND();
            System.debug('itempreparedatastart'+item);
            item.RENUM = srObj.Name; // Salesforce Reference Number
            if(srObj.Type_of_Request__c =='Release of Security Deposit' && srObj.Share_Value__c != null){
                item.SGUID = '1'; // SF: Salesforce GUID 
            }else if(srObj.Type_of_Request__c =='Refund of part of the Security Deposit' && srObj.Share_Value__c != null){
                item.SGUID = srObj.id; // SF: Salesforce GUID   
            }
            if(srObj.Share_Value__c != null){
                item.RFAMT = srObj.Share_Value__c.toPlainString();  // Refund Amount
            }
            /*if(srObj.Transfer_to_Account__c != null){
item.TKUNN = srObj.Transfer_to_Account__r.BP_No__c;
}*/
            if(srObj.Sponsor_Last_Name__c != null){
                item.KUNNR = srObj.Sponsor_Last_Name__c;
            }
            item.RFTYP = typeOfRefund;  // Refund Type
            if(srObj.Service_Type__c == 'Release / Refund of Security Deposit' || srObj.Service_Type__c == 'Lease Registration'){
                for(Amendment__c amd: srObj.Amendments__r){
                    if(amd.Unit__r != null){
                        if(amd.Unit__r.SAP_Unit_No__c != null)
                            item.SMENR = amd.Unit__r.SAP_Unit_No__c;
                    }
                    if(amd.Unit__r != null){
                        if(amd.Unit__r.Building__r.SAP_Building_No__c != null){
                            item.SGENR = amd.Unit__r.Building__r.SAP_Building_No__c;
                        }
                    }
                    if(amd.Unit__r != null && amd.Unit__r.Parent_Unit__c != null){
                        item.SPRUNO = amd.Unit__r.SAP_Unit_No__c;
                    }
                    if(amd.Unit__r != null && amd.Unit__r.Building__r.Developer_Lookup__r.Code__c != null){
                        item.SWENR = amd.Unit__r.Building__r.Developer_Lookup__r.Code__c;
                    }
                }
            }
            if(srObj.Service_Type__c == 'Mutual Termination of a Lease'){
                if(srObj.Unit__r.SAP_Unit_No__c != null){
                    item.SMENR = srObj.Unit__r.SAP_Unit_No__c;}
                if(srObj.Building__r.SAP_Building_No__c != null){
                    item.SGENR = srObj.Building__r.SAP_Building_No__c;
                }
                if(srObj.Unit__r.Parent_Unit__c != null){
                    item.SPRUNO = srObj.Unit__r.SAP_Unit_No__c;
                }
                if(srObj.Unit__r != null && srObj.Building__r.Developer_Lookup__r.Code__c != null){
                    item.SWENR = srObj.Building__r.Developer_Lookup__r.Code__c;
                }
                
            }
            if(srObj.Service_Type__c == 'Mutual Termination of a Lease' || srObj.Service_Type__c == 'Release / Refund of Security Deposit'){
                if(srObj.Lease_Contract_No__c != null){
                    item.RECNNR = srObj.Lease_Contract_No__c;
                }
            }
            if(srObj.Service_Type__c == 'Lease Registration'){
                for(Amendment__c amd: srObj.Amendments__r){
                    item.RECNNR = amd.Address1__c;
                }
            }
            /*if(srObj.Security_Deposit_AED__c != null){
item.RFPAR = 'Landlord';
}*/
            if(srObj.Share_Value__c != null){
                item.RFPAR = 'Tenant';
            }
            if(srObj.Service_Type__c != null){
                item.RTYPE = srObj.Service_Type__c;
            }
            if(srObj.Sponsor_Last_Name__c != null){
                item.REFBP = srObj.Sponsor_Last_Name__c;
            }
            
            if(srObj.Payment_Method__c == 'Wire Transfer' || srObj.Service_Type__c == 'Mutual Termination of a Lease' || srObj.Service_Type__c == 'Lease Registration'){
                item.RFMOD = 'WICO';   // Mode of Type (Will send type of refund as there are no mode of refunds config for tfr)
                if(srObj.Middle_Name_Previous_Sponsor__c != null){
                    item.BNAME = srObj.Middle_Name_Previous_Sponsor__c;
                }
                if(srObj.Address_Details__c != null){
                    item.BADDR = srObj.Address_Details__c;  
                }
                if(srObj.Authority_Name__c != null){
                    item.ACNAM = srObj.Authority_Name__c; 
                }
                if(srObj.Tax_Registration_Number_TRN__c != null){
                    item.ACNUM = srObj.Tax_Registration_Number_TRN__c;  
                }
                if(srObj.Post_Code__c != null){
                    item.SWFCD = srObj.Post_Code__c;  
                }
                if(srObj.SAP_GSDAT__c != null){
                    item.IBANN = srObj.SAP_GSDAT__c;  
                }     
            }
            else if(srObj.Payment_Method__c == 'Cheque'){
                item.RFMOD = 'CHCO';   // Mode of Type (Will send type of refund as there are no mode of refunds config for tfr)
                if(srObj.Authority_Name__c != null){
                    item.CCOMP = srObj.Authority_Name__c;   
                }
                /*if(srObj.First_Name__c != null && srObj.Last_Name__c != null){
item.CINDV = srObj.First_Name__c+' '+ srObj.Last_Name__c;  
}*/
                
            }
            else if(srObj.Payment_Method__c == 'Cash'){
                item.RFMOD = 'CASH';   // Mode of Type (Will send type of refund as there are no mode of refunds config for tfr)   
            }
            system.debug('item.RFMOD:'+item.RFMOD);
        }
        //Landlord
        if(srObj !=null && typeOfRefund != null && srObj.Security_Deposit_AED__c != null && TypeOfReceiver == 'Landlord'){
            item = new SAPPortalBalanceRefundService.ZSF_S_REFUND();
            item.RENUM = srObj.Name; // Salesforce Reference Number
            item.SGUID = '2'; // SF: Salesforce GUID  
            if(srObj.Security_Deposit_AED__c != null){
                item.RFAMT = srObj.Security_Deposit_AED__c.toPlainString();  // Refund Amount
            }
            if(srObj.Transfer_to_Account__c != null){
                item.TKUNN = srObj.Transfer_to_Account__r.BP_No__c;
            }
            if(srObj.Sponsor_Last_Name__c != null){
                item.KUNNR = srObj.Sponsor_Last_Name__c;
            }
            item.RFTYP = typeOfRefund;  // Refund Type
            if(srObj.Service_Type__c == 'Release / Refund of Security Deposit'){
                for(Amendment__c amd: srObj.Amendments__r){
                    if(amd.Unit__r != null){
                        if(amd.Unit__r.Name != null)
                            item.SMENR = amd.Unit__r.SAP_Unit_No__c;
                    }
                    if(amd.Unit__r != null){
                        if(amd.Unit__r.Building__r.SAP_Building_No__c != null){
                            item.SGENR = amd.Unit__r.Building__r.SAP_Building_No__c;
                        }
                    }
                    if(amd.Unit__r != null && amd.Unit__r.Parent_Unit__c != null){
                        item.SPRUNO = amd.Unit__r.SAP_Unit_No__c;
                    }
                    if(amd.Unit__r != null && amd.Unit__r.Building__r.Developer_Lookup__r.Code__c != null){
                        item.SWENR = amd.Unit__r.Building__r.Developer_Lookup__r.Code__c;
                    }
                }
            }
            if(srObj.Service_Type__c == 'Mutual Termination of a Lease' || srObj.Service_Type__c == 'Lease Registration'){
                if(srObj.Unit__r.Name != null){
                    item.SMENR = srObj.Unit__r.SAP_Unit_No__c;}
                if(srObj.Building__r.SAP_Building_No__c != null){
                    item.SGENR = srObj.Building__r.SAP_Building_No__c;
                }
                if(srObj.Unit__r.Parent_Unit__c != null){
                    item.SPRUNO = srObj.Unit__r.SAP_Unit_No__c;
                }
                if(srObj.Unit__r != null && srObj.Building__r.Developer_Lookup__r.Code__c != null){
                    item.SWENR = srObj.Building__r.Developer_Lookup__r.Code__c;
                }
                
            }
            if(srObj.Service_Type__c == 'Mutual Termination of a Lease' || srObj.Service_Type__c == 'Release / Refund of Security Deposit'){
                if(srObj.Lease_Contract_No__c != null){
                    item.RECNNR = srObj.Lease_Contract_No__c;
                }
            }
            if(srObj.Service_Type__c == 'Lease Registration'){
                for(Amendment__c amd: srObj.Amendments__r){
                    item.RECNNR = amd.Address1__c;
                }
            }
            if(srObj.Security_Deposit_AED__c != null){
                item.RFPAR = 'Landlord';
            }
            /*if(srObj.Share_Value__c != null){
item.RFPAR = 'Tenant';
}*/
            if(srObj.Service_Type__c != null){
                item.RTYPE = srObj.Service_Type__c;
            }
            if(srObj.Sponsor_Street__c != null){
                item.REFBP = srObj.Sponsor_Street__c;
            }
            
            
            if(srObj.Payment_Method__c == 'Wire Transfer' || srObj.Service_Type__c == 'Mutual Termination of a Lease' || srObj.Service_Type__c == 'Lease Registration'){
                item.RFMOD = 'WICO';   // Mode of Type (Will send type of refund as there are no mode of refunds config for tfr)
                if(srObj.Cage_No__c != null){
                    item.BNAME = srObj.Cage_No__c;
                }
                if(srObj.Bank_Address__c != null){
                    item.BADDR = srObj.Bank_Address__c;  
                }
                if(srObj.SAP_SGUID__c != null){
                    item.ACNAM = srObj.SAP_SGUID__c; 
                }
                if(srObj.SAP_OSGUID__c != null){
                    item.ACNUM = srObj.SAP_OSGUID__c;  
                }
                if(srObj.SAP_GSTIM__c != null){
                    item.SWFCD = srObj.SAP_GSTIM__c;  
                }
                if(srObj.IBAN_Number__c != null){
                    item.IBANN = srObj.IBAN_Number__c;  
                }     
            }
            else if(srObj.Payment_Method__c == 'Cheque'){
                item.RFMOD = 'CHCO';   // Mode of Type (Will send type of refund as there are no mode of refunds config for tfr)
                if(srObj.SAP_SGUID__c != null){
                    item.CCOMP = srObj.SAP_SGUID__c;   
                }
                /*if(srObj.First_Name__c != null && srObj.Last_Name__c != null){
item.CINDV = srObj.First_Name__c+' '+ srObj.Last_Name__c;  
}*/
                
            }
            else if(srObj.Payment_Method__c == 'Cash'){
                item.RFMOD = 'CASH';   // Mode of Type (Will send type of refund as there are no mode of refunds config for tfr)   
            }
            system.debug('item.RFMOD:'+item.RFMOD);
        }
        
        
        /* if(srObj.getPopulatedFieldsAsMap().containsKey('Service_Type__c')){
if(srObj !=null && typeOfRefund != null && (srObj.Service_Type__c != 'Request for Wallet Refund')){
item = new SAPPortalBalanceRefundService.ZSF_S_REFUND();
item.RENUM = srObj.Name; // Salesforce Reference Number
item.SGUID = srObj.id; // SF: Salesforce GUID
item.RFTYP = typeOfRefund;  // Refund Type
item.Kunnr = srObj.Customer__r.BP_No__c;  
item.UNQNO = srObj.SAP_Unique_No__c;
item.APLNR = srObj.Contact__r.BP_No__c;
item.MATNR = srObj.SAP_MATNR__c;
item.POSNR = '000010';
item.RFAMT = item.RFTYP=='RFCK' ? '0' :srObj.Refund_Amount__c.toPlainString();  // Refund Amount           
}
}
// //v1.5 -  End
else if(srObj !=null && typeOfRefund != null){
item = new SAPPortalBalanceRefundService.ZSF_S_REFUND();
item.RENUM = srObj.Name; // Salesforce Reference Number
item.SGUID = srObj.id; // SF: Salesforce GUID
item.RFTYP = typeOfRefund;  // Refund Type
item.Kunnr = srObj.Customer__r.BP_No__c;  
item.UNQNO = srObj.SAP_Unique_No__c;
item.APLNR = srObj.Contact__r.BP_No__c;
item.MATNR = srObj.SAP_MATNR__c;
item.POSNR = '000010';
item.RFAMT = item.RFTYP=='RFCK' ? '0' :srObj.Refund_Amount__c.toPlainString();  // Refund Amount      
} 
if(item.RFMOD == null &&( item.RFTYP == 'RFCL' || item.RFTYP=='RFRJ' || item.RFTYP=='TRAF'))
item.RFMOD = 'PORT';      
if(srObj.Type_of_Refund__c=='PSA Transfer' && item.RFMOD == null)  
item.RFMOD = 'PSAR'; 
if(srObj.Type_of_Refund__c=='Portal Balance Transfer' && item.RFMOD == null) 
item.RFMOD = 'PORT'; */      
        return item;
    }
    
}