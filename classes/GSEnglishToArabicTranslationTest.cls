/******************************************************************************************
 *  Class Name  : GSEnglishToArabicTranslationTest
 *  Author      : Shoaib Tariq
 *  Company     : DIFC
 *  Date        : 28 jan 2020      
 *  Description :                
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1    18 NOV 2019         Shoaib                Initial Version  
*******************************************************************************************/
@isTest
public class GSEnglishToArabicTranslationTest {
  static PageReference pref;
   static testmethod void myTestMethod1() {
       Service_Request__c thisSR = new Service_Request__c();
       thisSR.First_Name__c          = 'tester';
       thisSR.Last_Name__c           = 'tester';
       thisSR.Place_of_Birth__c      = 'Kerala';
       thisSR.Mother_Full_Name__c    = 'tester';
       insert thisSR;
       ApexPages.StandardController con           = new ApexPages.StandardController(thisSR); 
       GSEnglishToArabicTranslation thisGsEnglish = new GSEnglishToArabicTranslation(con);
       thisGsEnglish.redirectToRecord();
       pref = Page.GSEnglishToArabicTranslationVf;
       pref.getParameters().put('id',thisSR.id);
       Test.setCurrentPage(pref);
     }
     static testmethod void myTestMethod2() {
       Service_Request__c thisSR = new Service_Request__c();
       thisSR.First_Name__c          = 'tester';
       thisSR.Last_Name__c           = 'tester';
       thisSR.Place_of_Birth__c      = 'Kerala';
       thisSR.Mother_Full_Name__c    = 'tester';
       
       ApexPages.StandardController con           = new ApexPages.StandardController(thisSR); 
       GSEnglishToArabicTranslation thisGsEnglish = new GSEnglishToArabicTranslation(con);
       thisGsEnglish.redirectToRecord();
       pref = Page.GSEnglishToArabicTranslationVf;
       pref.getParameters().put('id',thisSR.id);
       Test.setCurrentPage(pref);
     }
  }