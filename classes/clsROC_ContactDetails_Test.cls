@isTest

public class clsROC_ContactDetails_Test {
    private static testmethod void testContactDetails(){
       map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Client_Information','Lease_Registration','Surrender_Termination_of_Lease_and_Sublease','Freehold_Transfer_Registration','Company_buyer','Developer','Individual_Buyer','Company_Seller','RORP_Authorized_Signatory','RORP_Shareholder','RORP_Shareholder_Company')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        objUnit.Is_official_Search__c=true;
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c SRStauts = new SR_Status__c(Name='Draft',Code__c='DRAFT');
        lstSRStatus.add(SRStauts);
        
        insert lstSRStatus;
                
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordType.get('Client_Information');
        objSR.Building__c = objBuilding.Id;
        objSR.Unit__c = lstUnits[0].Id;
        objSR.Service_Category__c = 'New';
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        insert objSR;
        //Create Service request 
        List<RecordType> recordtypeIdValue = [SELECT id from RecordType where (name='Individual' or name='CEO') and sobjectType = 'Amendment__c' order by Name];
        Id recordtypeId = [SELECT id from RecordType where name='Client Information' and sobjectType = 'Service_Request__c' order by Name].id;
        //Service_Request__c SRData = new Service_Request__c(Customer__c=acc.id,recordTypeId=recordtypeId);
        //insert SRData;
        //Create amendments 
        Amendment__c CEODetails = new Amendment__c();
        CEODetails.Amendment_Type__c = 'Individual';
        CEODetails.Relationship_Type__c = 'Has CEO/Regional Head/Chairman';
        CEODetails.recordtypeId = recordtypeIdValue[0].id;
        CEODetails.Status__c = 'Draft';
        CEODetails.ServiceRequest__c = objSR.id;
        insert CEODetails;
        Amendment__c CorporateCommunication = new Amendment__c();
        CorporateCommunication = new Amendment__c();
        CorporateCommunication.Amendment_Type__c = 'Individual';
        CorporateCommunication.recordtypeId = recordtypeIdValue[1].id;
        CorporateCommunication.Relationship_Type__c = 'Has Corporate Communication';
        CorporateCommunication.Status__c = 'Draft';
        CorporateCommunication.ServiceRequest__c = objSR.id;
        insert CorporateCommunication;
        //Create User
        test.startTest();
        //ApexPages.StandardController sc = new ApexPages.StandardController(objSR);
        clsROC_ContactDetails cont = new clsROC_ContactDetails();
        cont.testServiceRequest = objSR;
        cont.populateDetails();
        cont.AddNewAmendment();
        cont.indexOfList = 0;
        cont.CorporateCommunication = CorporateCommunication;
        //cont.EditAmendment();
        cont.save();
        cont.populateDetails();
        test.stopTest();
    }
    public static testmethod void testNegative(){
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Client_Information','Lease_Registration','Surrender_Termination_of_Lease_and_Sublease','Freehold_Transfer_Registration','Company_buyer','Developer','Individual_Buyer','Company_Seller','RORP_Authorized_Signatory','RORP_Shareholder','RORP_Shareholder_Company')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        objUnit.Is_official_Search__c=true;
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c SRStauts = new SR_Status__c(Name='Draft',Code__c='DRAFT');
        lstSRStatus.add(SRStauts);
        
        insert lstSRStatus;
                
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordType.get('Client_Information');
        objSR.Building__c = objBuilding.Id;
        objSR.Unit__c = lstUnits[0].Id;
        objSR.Service_Category__c = 'New';
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        Relationship__c rel = new Relationship__c(Subject_Account__c=objAccount.id,Relationship_Type__c='Has CEO/Regional Head/Chairman');
         insert rel;
        Relationship__c rel1 = new Relationship__c(Subject_Account__c=objAccount.id,Relationship_Type__c='Has General Communication');
        insert rel1;
        test.startTest();
            //ApexPages.StandardController sc = new ApexPages.StandardController(objSR);
            clsROC_ContactDetails cont = new clsROC_ContactDetails(); 
            cont.testServiceRequest = objSR;
            cont.populateDetails();
        test.stopTest();
    } 
}