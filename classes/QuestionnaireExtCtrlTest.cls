@isTest(SeeAllData=false)
public class QuestionnaireExtCtrlTest {
    static testMethod void questionnaireMethod() {     
     
     Account acc = new Account();
     acc.Name = 'feedback account';
     insert acc;
     
     Contact con = new Contact();
     con.FirstName = 'feedback';
     con.LastName = 'Form';
     con.AccountId = acc.id;
     insert con;     
     
     Questionnaire__c ff = new Questionnaire__c();
     ff.Account__c = con.AccountId;    
     ff.Contact__c = con.id;    
     insert ff;
          
     Test.startTest();
     ApexPages.StandardController controller = new ApexPages.StandardController(ff);
     QuestionnaireExtCtrl feedctrl = new QuestionnaireExtCtrl(controller);
     PageReference pageRef = Page.Questionnaire; // Add your VF page Name here
     pageRef.getParameters().put('cid', con.Id);
     pageRef.getParameters().put('eid', 'aru@sd.com');
     Test.setCurrentPage(pageRef);     
     feedctrl.pageLoad();
     
     feedctrl.QuestionnaireObj.Do_you_underwrite_risks__c ='Yes';
     
     feedctrl.SaveQuestionnaire();
     //feedctrl.RowValueChange();
     feedctrl.showOtherSection();
     feedctrl.showOtherSection1();
     feedctrl.addLOBLineiTems();
     Test.stopTest();
    }  
}