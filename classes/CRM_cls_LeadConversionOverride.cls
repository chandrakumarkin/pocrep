/******************************************************************************************
 *  Author      : Claude Manahan
 *  Date        : 24-05-2017
 *  Description : Overrides the default lead conversion behavior
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    24-05-2017  Claude         Created
V1.1	01-06-2017	Claude		   Removed leads that are already converted
****************************************************************************************************************/
public class CRM_cls_LeadConversionOverride {
    
    /**
     * Instance of the Lead to be converted
     */
    private Lead currentLead;
    
    /**
     * Extension Controller
     */
    public CRM_cls_LeadConversionOverride(Apexpages.StandardController std){
    
        if(Apexpages.currentPage().getParameters().containsKey('Id')){
            
            if(!Test.isRunningTest()) std.addFields(new List<String>{'Date_of_notification__c','Previous_Date_of_notification__c'});
            
            currentLead = !Test.isRunningTest() ? (Lead) std.getRecord() : [SELECT Id, Date_of_notification__c FROM Lead WHERE Id =: Apexpages.currentPage().getParameters().get('Id')];
            
        } 
        
    }
    
    /**
     * Removes all pending notifications
     * from the current lead
     */
    public PageReference removePendingActivities(){
        
        if(currentLead != null && currentLead.Date_of_notification__c != null){
            
            currentLead.Previous_Date_of_notification__c = currentLead.Date_of_notification__c;
            
            currentLead.Date_of_notification__c = null;
            
            update currentLead;
        }
        
        return new PageReference('/lead/leadconvert.jsp?&id='+currentLead.Id+'&retURL='+currentLead.Id);
        
    }
    
    /**
     * Returns the unconverted leads 
     * @return      unconvertedLeads        List of unconverted leads
     */
    public static list<Lead> getUncovertedLeads(){
    	
    	return CLS_LeadUtils.getLeads(' Date_of_notification__c = NULL AND Previous_Date_of_notification__c != NULL AND isConverted = false'); //v1.1 - Claude - Added filter to exclude converted leads 
        
    }
    
    /**
     * Resets the notification dates of unconverted 
     * leads
     * @params      leadsToBeProcessed      The leads to be updated
     */
    public static void resetNotificationDates(list<Lead> leadsToBeProcessed){
    
        list<Lead> leadsToBeUpdated = new list<Lead>();
        
        for(Lead l : leadsToBeProcessed){
            
            if(l.Previous_Date_of_notification__c != null){ 
                l.Date_of_notification__c = l.Previous_Date_of_notification__c;
                l.Previous_Date_of_notification__c = null;
                
                leadsToBeUpdated.add(l);
            }
            
        }
        
        if(!leadsToBeUpdated.isEmpty()){
        
            Database.SaveResult[] updateLeadsResults = Database.update(leadsToBeProcessed,false);
        
            String errorMessage = 'The following errors has occurred: \n';
            
            Boolean hasErrors = false;
            
            for (Database.SaveResult sr : updateLeadsResults) {
                
                if(sr.isSuccess()) {
                    
                    System.debug('Successfully inserted account. Lead ID: ' + sr.getId());
                    
                } else {
                                    
                    for(Database.Error err : sr.getErrors()) errorMessage += err.getStatusCode() + ': ' + 
                    		err.getMessage() + ' >> ' + 'Lead fields that affected this error: ' + err.getFields() + '\n';
                    
                    if(!hasErrors) hasErrors = true;
                }
            }
            
            if(hasErrors){
            	
            	CRM_cls_Utils crmUtils = new CRM_cls_Utils();
            	
            	crmUtils.setLogType('CRM: Scheduled Lead Update Job');
            	crmUtils.setErrorMessage(errorMessage);
            	
	        	crmUtils.logError();	
            	
            } 
        
        }
    }
    
}