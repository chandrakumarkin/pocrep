@isTest
public class cls_Voluntary_Strike_off_editTest {
    
    public static Service_Request__c ServiceRequests{get;set;}
    
    public static void testData(){
        
        string conRT;
        for (RecordType objRT: [select Id, Name, DeveloperName from RecordType where DeveloperName = 'Request_for_Voluntary_Strike_off' AND SobjectType = 'Service_Request__c']){
           conRT = objRT.Id;
        }
            
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
               
        ServiceRequests = new Service_Request__c();
        ServiceRequests.Customer__c = objAccount.Id;
        ServiceRequests.Email__c = 'testsr123@difc.com';
        ServiceRequests.In_accordance_with_Article_10_1__c = true;
        ServiceRequests.In_Accordance_with_Article_11__c = true;
        ServiceRequests.In_Accordance_with_Article_12_1a__c = true;
        ServiceRequests.In_Accordance_with_Article_12_1bj__c = true;
        ServiceRequests.Express_Service__c = false;
        ServiceRequests.RecordtypeId = conRT;
        INSERT ServiceRequests;
    }
    
    static testmethod void positiveTest(){
        
        //testData();
        ServiceRequests = new Service_Request__c();
        ApexPages.StandardController sc = new ApexPages.StandardController(ServiceRequests);
        cls_Voluntary_Strike_off_edit voluntary_Strike = new cls_Voluntary_Strike_off_edit(sc);
        voluntary_Strike.validationRule();
        voluntary_Strike.SaveRecord();
    }
    
    static testmethod void positiveTest1(){
        
        testData();
        ApexPages.StandardController sc = new ApexPages.StandardController(ServiceRequests);
        apexpages.currentpage().getparameters().put('RecordType' , ServiceRequests.RecordtypeID);
        
        cls_Voluntary_Strike_off_edit voluntary_Strike = new cls_Voluntary_Strike_off_edit(sc);
        voluntary_Strike.validationRule();
        voluntary_Strike.SaveRecord();
    }
}