/*
    Created By  : Manpreet Singh on 18th Oct,2019
    Description : 
	Test Class :  OB_RegisterDIFCFlowControllerTest
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 v1.0   14-Dec-2019     Durga           Business asked to remove the duplicate SR check
 ----------------------------------------------------------------------------------------
*/
public without sharing class OB_RegisterWithDIFCController {
    
    @AuraEnabled
    public static wrapPageFlow getServiceEmailAddress(String srId){
        system.debug('==getServiceEmailAddress=====');
        string selfRegLabel = Label.Self_Registration_Record_Type;
        wrapPageFlow objwrap = new wrapPageFlow(null,null,null,false,false,'Success');
        system.debug('==srId====='+srId);
        
        for(HexaBPM__Service_Request__c serviceList: [Select id,HexaBPM__Email__c,entity_type__c,Business_Sector__c from HexaBPM__Service_Request__c 
                                                       where  RecordType.DeveloperName=:selfRegLabel 
                                                       AND ID =: srId
                                                     ]){
                                                                                            
            //ServiceRequestId = serviceList.Id;
             objwrap.emailAddress=  serviceList.HexaBPM__Email__c;                                          
             objwrap.entityType=  serviceList.entity_type__c;
             objwrap.businessSector=  serviceList.Business_Sector__c;                                           
             
        }
        
        system.debug('------------------objwrap----------------'+objwrap);
        return objwrap;
    }
    /*
        * Used in: OB_RegisterWithDIFC Lightning component
        * Description: will take inputs from User and insert or update based upon the email entered.
    */
    
    @AuraEnabled
    public static wrapPageFlow saveService(HexaBPM__Service_Request__c service){
        String selfRegLabel = Label.Self_Registration_Record_Type;
        String ServiceRequestId='';
        String serviceEmail =  service.HexaBPM__Email__c;
        wrapPageFlow objwrap = new wrapPageFlow();
        system.debug('------------------service----------------'+service);
        
        /*v1.0
        for(HexaBPM__Service_Request__c serviceList: [Select id,HexaBPM__Email__c from HexaBPM__Service_Request__c 
                                                       where  RecordType.DeveloperName=:selfRegLabel AND HexaBPM__Internal_Status_Name__c='Draft' 
                                                       AND HexaBPM__Email__c =: service.HexaBPM__Email__c
                                                     ]){
                                                                                            
            ServiceRequestId = serviceList.Id;
            objwrap = new wrapPageFlow(null,null,null,true,true,'Service Request Exists with this Email');
            system.debug('------------------objwrap----------------'+objwrap);
        }
        */

        boolean isDuplicate = false;
        for(user uerObj : [SELECT id FROM user where email =: serviceEmail AND ContactId != NULL 
        AND IsActive = true LIMIT 1]){
            isDuplicate = true;
        }

        if(isDuplicate == false){
            Id strRecordTypeId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(selfRegLabel).getRecordTypeId();
        
            if(ServiceRequestId==null || ServiceRequestId== '' ){
                HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c();
                objSR = service;
                objSR.RecordTypeId = strRecordTypeId;
                system.debug(objSR);
                string PageFlowId;
                string NextPageId;
                list<HexaBPM__Page__c> lstPages = new list<HexaBPM__Page__c>();
                for(HexaBPM__Page__c page : [select Id,Name,HexaBPM__Page_Flow__c,HexaBPM__Page_Order__c from HexaBPM__Page__c 
                                             where HexaBPM__Page_Flow__r.HexaBPM__Record_Type_API_Name__c =: selfRegLabel order by HexaBPM__Page_Order__c ASC limit 2]){
                    lstPages.add(page);
                    PageFlowId = page.HexaBPM__Page_Flow__c;
                    if(page.HexaBPM__Page_Order__c==2)
                        NextPageId = page.Id;
                    else
                        objSR.Page_Tracker__c = page.Id;
                }
                if(objSR!=null){
                    try{
                        insert objSR;
                        //System.debug('insertion' +objSR);
                        objwrap = new wrapPageFlow(PageFlowId,objSR.Id,NextPageId,false,false,'Success');
                    }
                    catch(DMLException e) 
                    {
                        string DMLError = e.getdmlMessage(0)+'';
                        if(DMLError==null)
                            DMLError = e.getMessage()+'';
                        
                        system.debug('DMLError==>' + DMLError);
                        objwrap.errorMessage = DMLError;
                        objwrap = new wrapPageFlow(null,null,null,false,false,DMLError);
                        return objwrap;
                        
                    }
                }
            }
            return objWrap;
        }else{
            objwrap = new wrapPageFlow(null,null,null,false,true,'Duplicate User exist');
            return objwrap;  
        }
        
       
    }
    
    public class wrapPageFlow {
       @AuraEnabled
        public string serviceID {get;set;}
        @AuraEnabled
        public string pageFlowID {get; set;}
        @AuraEnabled
        public string pageID {get; set;}
        @AuraEnabled
        public boolean IsSRExists{get;set;}
        @AuraEnabled
        public boolean isEmailExisting{get;set;}
        @AuraEnabled
        public string emailAddress {get; set;}
        
        @AuraEnabled
        public string entityType {get; set;}
        
        @AuraEnabled
        public string businessSector {get; set;}
        
         @AuraEnabled
        public string  errorMessage{get; set;}
        
        public wrapPageFlow()
        {
            
        }
        public wrapPageFlow(string pFlowID , string srID , string page, boolean SRExists, boolean emailExist,string errorMessage) {
            IsSRExists = SRExists;
            pageFlowID = pFlowID;
            serviceID = srID;
            pageID = page;
            isEmailExisting = emailExist;
            errorMessage = errorMessage;
        }
    }
}