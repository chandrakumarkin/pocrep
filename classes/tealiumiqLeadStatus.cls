global class tealiumiqLeadStatus implements Schedulable {
   global void execute(SchedulableContext sc) 
   {
      
      for(Lead ObjLead:[select Email,Phone,Milestone_ID__c,Tealium_Status__c from 
lead where Milestone_ID__c!=null and LeadSource='Company Website' and lastModifieddate=today limit 100])
    {
                Http http = new Http();
                HttpRequest AuthRequest = new HttpRequest();
                AuthRequest.setEndpoint('https://collect.tealiumiq.com/event');
                AuthRequest.setMethod('POST');
                AuthRequest.setHeader('Content-Type', 'application/json');  
                map<String, String> BodyData=new map<String, String>();
                BodyData.put('tealium_account','difc');
                BodyData.put('tealium_profile','main');
                BodyData.put('tealium_event','apisf');
                BodyData.put('email_address',ObjLead.Email);
                BodyData.put('imported_lead_status',ObjLead.Tealium_Status__c);
                BodyData.put('tealium_visitor_id',ObjLead.Milestone_ID__c);
                System.debug('BodyVal==>'+JSON.serialize(BodyData));
                AuthRequest.setBody(JSON.serialize(BodyData));
                if(!Test.isRunningTest())
                {
                System.HttpResponse response = new System.Http().send(AuthRequest);  System.debug('response==>'+response);
                }
                
        
    }

   }
}