/***********************************************************************************
 *  Author   : Ravi
 *  Company  : NSI
 *  Date     : 25/12/2015
 *  Purpose  : This class is to perform all Compliance related tasks 
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date            Updated By  Description
 ----------------------------------------------------------------------------------------              
      
 V1.0   4th May 2015    Swati       Automated Fine Process
 **********************************************************************************************/

global class BatchProcessFineSRs implements Database.Batchable<sObject>,Database.Stateful { //Database.AllowsCallouts
    
    list<Service_Request__c> FineSRsToProcess = new list<Service_Request__c>();
    list<Id> lstSRIds = new list<Id>();
    
    global BatchProcessFineSRs(list<Service_Request__c> lstSRs){
    	FineSRsToProcess = lstSRs;
    }
    
	global list<Service_Request__c> start(Database.BatchableContext BC ){
        return FineSRsToProcess;
    }
    
    global void execute(Database.BatchableContext BC, list<Service_Request__c> lstFineSR){
    	try{
    		insert lstFineSR;
    		list<Service_Request__c> lstFineSRStatusUpdate = new list<Service_Request__c>();
    		list<SR_Status__c> lstStatus = [select id,name from SR_Status__c where Name='Submitted' or Name='submitted' limit 1];
    		if(lstStatus != null && !lstStatus.isEmpty()){
	    		for(Service_Request__c objSR : lstFineSR){
	    			Service_Request__c objSRTemp = new Service_Request__c(Id=objSR.Id);
	    			objSRTemp.Internal_SR_Status__c = lstStatus[0].id;
                    objSRTemp.External_SR_Status__c = lstStatus[0].id;
                    objSRTemp.Submitted_Date__c = system.today();
                    objSRTemp.Submitted_DateTime__c = system.now();
                    objSRTemp.Date_of_Written_Notice__c = system.today();
                    objSRTemp.Date__c = system.today()+15;
                    objSRTemp.Due_Date__c = system.today()+30;
                    lstFineSRStatusUpdate.add(objSRTemp);
                    lstSRIds.add(objSR.Id);
	    		}
	    		RecursiveControlCls.isUpdatedAlready = true;
	    		if(!lstFineSRStatusUpdate.isEmpty())
	    			update lstFineSRStatusUpdate;
    		}	
    		/*if(lstFineSR != null && !lstFineSR.isEmpty()){
    			list<SR_Price_Item__c> lstSRPriceItems = new list<SR_Price_Item__c>();
    			list<Id> lstSRIds = new list<Id>();
    			
    			for(Service_Request__c objSR : lstFineSR)
					lstSRIds.add(objSR.Id);
	    		
	    		String Result = SAPWebServiceDetails.ECCServiceCall(lstSRIds,true);
	    	}*/
	    	
        }catch(Exception ex){
        	
        }
    }
    
    global void finish(Database.BatchableContext BC){
    	
    }
}