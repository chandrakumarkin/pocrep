/*
    Author      :   Ravi
    Description :   THis calss is used to send the DP Price lines to SAP whenever a step is approved
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    25-01-2015  Ravi            Created   
    V1.1    20-03-2016  Ravi            Changed the Purpose of Notification Picklist value of DP - as per # 2532
    V1.2    16-07-2017  Sravan          Added condition to exclude GS Price Items in ROC
    V1.3    30-09-2017  Srvan           Added conditions to exclude refund price items from portal Balance
    V1.4    18-07-2020  Shoaib         #10318   
*/
public without sharing class RocPushDPtoSAP {
    public static string PushDPPriceLinesToSAP(Step__c objStep){
        try{
            Account objAccount;
            for(Account objAct : [select Id,Name,Company_Type__c,Portal_Balance__c from Account where Id=:objStep.SR__r.Customer__c]){
                objAccount = objAct;
            }
            string rValue = ProcessDPLines(objAccount,objStep);
            if(rValue != 'Success')
                return rValue;
            // Push the DP Pricelines to SAP
            SAPWebServiceDetails.ECCServiceCallToPushDPFuture(new list<string>{objStep.SR__c}, false);
        }catch(Exception ex){
            return ex.getMessage();
        }
        return 'Success';
    }
    
    public static string ProcessDPLines(Account objAccount,Step__c objStep){
        map<string,string> mapPricingLineIds = new map<string,string>();
        map<string,SR_Price_Item__c> mapExistingPriceItems = new map<string,SR_Price_Item__c>();
        map<string,SR_Price_Item__c> mapItemsToDelete = new map<string,SR_Price_Item__c>();
        map<string,SR_Price_Item__c> mapItemsToInsert = new map<string,SR_Price_Item__c>();
        map<string,Pricing_Line__c> mapPriceLines = new map<string,Pricing_Line__c>();
        decimal dAmtToCharge = 0;
        list<Id> lstPriceItemIds = new list<Id>();
        if(test.isRunningTest())
            Helper();
        list<string> lstPricingLineDevIds = new list<string>{
            'FI_1100_ADP_fin','FI_1100_ADP_nfin','FI_1100_ADP_ret', // Amendments to Data Protection
            'FI_1100_RGF_fin','FI_1100_RGF_nfin','FI_1100_RGF_ret', // Notification to process personal data
            'FI_1100_PSO_fin','FI_1100_PSO_nfin','FI_1100_PSO_ret', // Permit to transfer personal data
            'FI_1100_PSP_fin','FI_1100_PSP_nfin','FI_1100_PSP_ret' // Permit to process sensitive personal data
        };
        for(Pricing_Line__c objPL : [select Id,DEV_Id__c,Name,Product__c,Product__r.Name,(select Id,Unit_Price__c from Dated_Pricing__r where Unit_Price__c != null AND ( Date_To__c = null OR Date_To__c >=: system.today() )) from Pricing_Line__c where DEV_Id__c IN : lstPricingLineDevIds]){
            mapPricingLineIds.put(objPL.DEV_Id__c,objPL.Id);
            mapPriceLines.put(objPL.DEV_Id__c,objPL);
        }
        
        for(SR_Price_Item__c objPriceItem : [select Id,Pricing_Line__c,Pricing_Line__r.DEV_Id__c,Status__c from SR_Price_Item__c where ServiceRequest__c =: objStep.SR__c AND Pricing_Line__c IN : mapPricingLineIds.values()]){
            mapExistingPriceItems.put(objPriceItem.Pricing_Line__r.DEV_Id__c,objPriceItem);
            lstPriceItemIds.add(objPriceItem.Id);
        }
        string CompanyType = objAccount.Company_Type__c=='Financial - related'?'fin': objAccount.Company_Type__c=='Non - financial'?'nfin': objAccount.Company_Type__c=='Retail'?'ret':'';
        if(objStep.SR__r.Purpose_of_Notification__c != 'To Inform the Commissioner of Data Protection that you do not Process Personal Data' && objStep.SR__r.Purpose_of_Notification__c != 'In relation to changing contact details only'){ //v1.1
            if(objStep.SR__r.Permit_1__c == 'New'){
                if(mapExistingPriceItems.containsKey('FI_1100_RGF_'+CompanyType) == false){
                    //Create New SR PRice Item for Permit 1
                    string UniqueSRPriceItem = objStep.SR__c+'_'+mapPriceLines.get('FI_1100_RGF_'+CompanyType).Product__c;
                    decimal UnitPrice = (mapPriceLines.get('FI_1100_RGF_'+CompanyType).Dated_Pricing__r != null && mapPriceLines.get('FI_1100_RGF_'+CompanyType).Dated_Pricing__r.size() > 0)?mapPriceLines.get('FI_1100_RGF_'+CompanyType).Dated_Pricing__r[0].Unit_Price__c:0;
                    SR_Price_Item__c objPriceItem = new SR_Price_Item__c(ServiceRequest__c=objStep.SR__c,Product__c=mapPriceLines.get('FI_1100_RGF_'+CompanyType).Product__c,Price__c=UnitPrice,Unique_SR_PriceItem__c=UniqueSRPriceItem,Status__c='Blocked');
                    objPriceItem.Pricing_Line__c = mapPriceLines.get('FI_1100_RGF_'+CompanyType).Id;
                    mapItemsToInsert.put('FI_1100_RGF_'+CompanyType,objPriceItem);
                    dAmtToCharge += UnitPrice;
                }
            }else if(mapExistingPriceItems.containsKey('FI_1100_RGF_'+CompanyType)){
                mapItemsToDelete.put('FI_1100_RGF_'+CompanyType,mapExistingPriceItems.get('FI_1100_RGF_'+CompanyType));
                mapExistingPriceItems.remove('FI_1100_RGF_'+CompanyType);
            }
            
            if(objStep.SR__r.Permit_2__c == 'New'){
                if(mapExistingPriceItems.containsKey('FI_1100_PSO_'+CompanyType) == false){
                    //Create New SR PRice Item for Permit 2
                    string UniqueSRPriceItem = objStep.SR__c+'_'+mapPriceLines.get('FI_1100_PSO_'+CompanyType).Product__c;
                    decimal UnitPrice = (mapPriceLines.get('FI_1100_PSO_'+CompanyType).Dated_Pricing__r != null && mapPriceLines.get('FI_1100_PSO_'+CompanyType).Dated_Pricing__r.size() > 0)?mapPriceLines.get('FI_1100_PSO_'+CompanyType).Dated_Pricing__r[0].Unit_Price__c:0;
                    SR_Price_Item__c objPriceItem = new SR_Price_Item__c(ServiceRequest__c=objStep.SR__c,Product__c=mapPriceLines.get('FI_1100_PSO_'+CompanyType).Product__c,Price__c=UnitPrice,Unique_SR_PriceItem__c=UniqueSRPriceItem,Status__c='Blocked');
                    objPriceItem.Pricing_Line__c = mapPriceLines.get('FI_1100_PSO_'+CompanyType).Id;
                    mapItemsToInsert.put('FI_1100_PSO_'+CompanyType,objPriceItem);
                    dAmtToCharge += UnitPrice;
                }
            }else if(mapExistingPriceItems.containsKey('FI_1100_PSO_'+CompanyType)){
                mapItemsToDelete.put('FI_1100_PSO_'+CompanyType,mapExistingPriceItems.get('FI_1100_PSO_'+CompanyType));
                mapExistingPriceItems.remove('FI_1100_PSO_'+CompanyType);
            }
            
            if(objStep.SR__r.Permit_3__c == 'New'){
                if(mapExistingPriceItems.containsKey('FI_1100_PSP_'+CompanyType) == false){
                    //Create New SR PRice Item for Permit 3
                    string UniqueSRPriceItem = objStep.SR__c+'_'+mapPriceLines.get('FI_1100_PSP_'+CompanyType).Product__c;
                    decimal UnitPrice = (mapPriceLines.get('FI_1100_PSP_'+CompanyType).Dated_Pricing__r != null && mapPriceLines.get('FI_1100_PSP_'+CompanyType).Dated_Pricing__r.size() > 0)?mapPriceLines.get('FI_1100_PSP_'+CompanyType).Dated_Pricing__r[0].Unit_Price__c:0;
                    SR_Price_Item__c objPriceItem = new SR_Price_Item__c(ServiceRequest__c=objStep.SR__c,Product__c=mapPriceLines.get('FI_1100_PSP_'+CompanyType).Product__c,Price__c=UnitPrice,Unique_SR_PriceItem__c=UniqueSRPriceItem,Status__c='Blocked');
                    objPriceItem.Pricing_Line__c = mapPriceLines.get('FI_1100_PSP_'+CompanyType).Id;
                    mapItemsToInsert.put('FI_1100_PSP_'+CompanyType,objPriceItem);
                    dAmtToCharge += UnitPrice;
                }
            }else if(mapExistingPriceItems.containsKey('FI_1100_PSP_'+CompanyType)){
                mapItemsToDelete.put('FI_1100_PSP_'+CompanyType,mapExistingPriceItems.get('FI_1100_PSP_'+CompanyType));
                mapExistingPriceItems.remove('FI_1100_PSP_'+CompanyType);
            }
            
            if(objStep.SR__r.Record_Type_Name__c != 'Application_of_Registration' && mapExistingPriceItems.containsKey('FI_1100_ADP_'+CompanyType) == false && objStep.SR__r.Purpose_of_Notification__c == 'In relation to any changes related to the registrable particulars notified under the previous notification'){
                //Create New SR PRice Item for Amendment
                string UniqueSRPriceItem = objStep.SR__c+'_'+mapPriceLines.get('FI_1100_ADP_'+CompanyType).Product__c;
                decimal UnitPrice = (mapPriceLines.get('FI_1100_ADP_'+CompanyType).Dated_Pricing__r != null && mapPriceLines.get('FI_1100_ADP_'+CompanyType).Dated_Pricing__r.size() > 0)?mapPriceLines.get('FI_1100_ADP_'+CompanyType).Dated_Pricing__r[0].Unit_Price__c:0;
                SR_Price_Item__c objPriceItem = new SR_Price_Item__c(ServiceRequest__c=objStep.SR__c,Product__c=mapPriceLines.get('FI_1100_ADP_'+CompanyType).Product__c,Price__c=UnitPrice,Unique_SR_PriceItem__c=UniqueSRPriceItem,Status__c='Blocked');
                objPriceItem.Pricing_Line__c = mapPriceLines.get('FI_1100_ADP_'+CompanyType).Id;
                mapItemsToInsert.put('FI_1100_ADP_'+CompanyType,objPriceItem);
                dAmtToCharge += UnitPrice;
            }
            
        }else{ // can delete all the Price Items
            mapItemsToDelete.putAll(mapExistingPriceItems);
            mapExistingPriceItems = new map<string,SR_Price_Item__c>();
        }
        decimal dPortalBalance = 0; 
        dPortalBalance = (objAccount.Portal_Balance__c != null)?objAccount.Portal_Balance__c:0;
        for(SR_Price_Item__c objSRPriceItem : [select Price__c from SR_Price_Item__c 
                                                where ServiceRequest__c!=null 
                                                AND Price__c!=null 
                                                AND ServiceRequest__r.Customer__c=:objStep.SR__r.Customer__c 
                                                AND (Status__c='Blocked' OR Status__c='Consumed')
                                                AND Id NOT IN : lstPriceItemIds and Add_in_Account_Balance__c = true   //V1.2
                                            ]){
            dPortalBalance -= objSRPriceItem.Price__c;
        }
        
        for(Receipt__c objReceipt : [select Amount__c from Receipt__c where Customer__c=:objStep.SR__r.Customer__c and Amount__c!=null and Payment_Status__c='Success' and Pushed_to_SAP__c=false]){
            dPortalBalance += objReceipt.Amount__c;
        }
        
        dPortalBalance -= RefundRequestUtils.getRefundAmount(objStep.SR__r.Customer__c); //V1.3
        
        if(dAmtToCharge > 0 && dPortalBalance < dAmtToCharge)
            return 'Customer does not have sufficeient protal balance.';
        system.debug('mapItemsToDelete is : '+mapItemsToDelete);
        if(!mapItemsToDelete.isEmpty())
            delete mapItemsToDelete.values();
        if(!mapItemsToInsert.isEmpty())
            insert mapItemsToInsert.values();       
        return 'Success';
    }
    public static void Helper(){
        map<string,string> mapPricingLineIds = new map<string,string>();
        map<string,SR_Price_Item__c> mapExistingPriceItems = new map<string,SR_Price_Item__c>();
        map<string,SR_Price_Item__c> mapItemsToDelete = new map<string,SR_Price_Item__c>();
        map<string,SR_Price_Item__c> mapItemsToInsert = new map<string,SR_Price_Item__c>();
        map<string,Pricing_Line__c> mapPriceLines = new map<string,Pricing_Line__c>();
        
        list<CCHelper> lstCCHelper = new list<CCHelper>();
        CCHelper objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
        objCCHelper = new CCHelper();
        objCCHelper.FirstName = 'First Name';
        objCCHelper.FirstName = 'Last Name';
        lstCCHelper.add(objCCHelper);
    }
    
    public class CCHelper{
        public string FirstName {get;set;}
        public string LastName {get;set;}
    }
    
}