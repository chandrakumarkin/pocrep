/********************************************************************************************
*  Author   : Suchita Sharma 
*  Purpose  : This class is to ensure clients update their passport and missing details 
----------------------------------------------------------------------------------------------
Modification History 
----------------------------------------------------------------------------------------------
V.No           Date            Updated By       Description
----------------------------------------------------------------------------------------------             
V 1.0          27 Dec 2020     Suchita Sharma  Initial Version
V 1.1          15-Aug-2021     Arun Singh       Avoid Duplicate Passport expr    
**********************************************************************************************/
Public class AccountsRelationshipMissingDetails{
    
    public static  List<Relationship__c> RelationshipMissingDetails(String AccountId){
        Set<String> relationshipTypeStringSet = new Set<String>{'Shareholder','Director','Authorized Signatory','Has Service Document Responsible for','Senior Management','Has Authorized Representative'}; 
        List<Relationship__c> missingDetailsRelList = new List<Relationship__c>();
        Date todaysdate = date.today();
        Date dateafter30days = todaysdate.addDays(30);
        
        for(Relationship__c rel : [Select id,Object_Contact__r.Name,Subject_Account__c,Subject_Account__r.Ownerid,Object_Account__c,Relationship_Name__c,Passport_No__c,Passport_Expiry_Date__c,Individual_missing_details__c,
                                   Body_Corporate_missing_details__c from Relationship__c where Status__c='Active' and System_Relationship_Type__c in:relationshipTypeStringSet and 
                                   Subject_Account__c =:AccountId and(Passport_Expiry_Date__c<=:dateafter30days or Individual_missing_details__c=true or Body_Corporate_missing_details__c=true)]){
                                       missingDetailsRelList.add(rel);
                                   }
        
        return missingDetailsRelList;          
    }
    
    public static  void AddEnactmentNotiRelMissingDetails(String AccountId,List<Relationship__c> relationshipList){
        id RMAccount;
        String SRGroup = 'ROC';
        List<Enactment_Notification__c> enactmentNotificationInsertList = new List<Enactment_Notification__c >();
        for(Relationship__c relObj: relationshipList){
                RMAccount=relObj.Subject_Account__r.Ownerid;
        }
        List<Contact> mapAccountWiseContact=CC_ROCCodeCls.CompanyServicesPortalContacts(AccountId,SRGroup);
        String HtmlBody=AccountsRelationshipMissingDetails.createMissingDetailsBody(AccountId,relationshipList);
        if(htmlBody!=''){
            String NotificationType ='Relationship Missing Details';
            Enactment_Notification__c ObjNoti=new Enactment_Notification__c();
            ObjNoti.Type__c=NotificationType;
            ObjNoti.Account__c=AccountId;
            ObjNoti.Send_Email__c=true;
            ObjNoti.Comment__c=htmlBody;
            ObjNoti.Key_Duplicate__c=AccountId+'-'+NotificationType;
            ObjNoti.User_1__c=RMAccount;
            if(mapAccountWiseContact.size() > 0) ObjNoti.Portal_User_1__c= mapAccountWiseContact[0].id;
            if(mapAccountWiseContact.size() > 1) ObjNoti.Principal_User_2__c= mapAccountWiseContact[1].id;
            if(mapAccountWiseContact.size() > 2) ObjNoti.Principal_User_3__c= mapAccountWiseContact[2].id;
            if(mapAccountWiseContact.size() > 3) ObjNoti.Principal_User_4__c= mapAccountWiseContact[3].id;
            if(mapAccountWiseContact.size() > 4) ObjNoti.Principal_User_5__c= mapAccountWiseContact[4].id;
            enactmentNotificationInsertList.add(ObjNoti);
            
        }
        
        try{
            if(!enactmentNotificationInsertList.isEmpty()){
                // Insert Enactment Notification Records
                Upsert enactmentNotificationInsertList Key_Duplicate__c;
            }
        }catch(Exception e){
            insert LogDetails.CreateLog(null, 'MissingDetailsBatch', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
        } 
    }
    
    public static String createMissingDetailsBody(String AccountId,List<Relationship__c> relationshipList){
        String HtmlBody='';
        Date todaysdate = date.today();
        Date dateafter30days = todaysdate.addDays(30);
        String passportTR = '';
        String individualTR = '';
        String bodyCorporateTR = '';
        Map<String,String> contactIndividualMap = new Map<String,String>();
        Map<String,String> contactIndividualPassportMap = new Map<String,String>();
        Map<String,String> accountBodyCorporateMap = new Map<String,String>();
        if(!relationshipList.isEmpty()){
            for(Relationship__c relObj: relationshipList){ 
                //Create Table Row Data 
                //The passports of the individuals listed below are nearing expiry or have expired as per the details recorded on the DIFC Portal.
                if(relObj.Passport_Expiry_Date__c<=dateafter30days && contactIndividualPassportMap.containsKey(relObj.Passport_No__c)==false){
                    String name = relObj.Relationship_Name__c; 
                    String passport = String.valueOf(relObj.Passport_No__c); 
                    String passportExpiryDate = String.valueOf(relObj.Passport_Expiry_Date__c); 
                    String status;
                    if(relObj.Passport_Expiry_Date__c<=todaysdate){
                        status='Expired';
                    }else{
                        status='Active';
                    }
                    passportTR += '<tr><td class="datatablerow" style="border: 1px solid #ddd; padding: 10px;">' + name + '</td><td class="datatablerow" style="border: 1px solid #ddd; padding: 10px;">' + passport + '</td><td class="datatablerow"  style="border: 1px solid #ddd; padding: 10px;">' + passportExpiryDate + '</td><td class="datatablerow"  style="border: 1px solid #ddd; padding: 10px;">'+status+'</td></tr>';
                    contactIndividualPassportMap.put(relObj.Passport_No__c,relObj.Passport_No__c);
                }
                //The individuals listed below have missing details on the DIFC Portal
                if(relObj.Object_Contact__c!=null && relObj.Individual_missing_details__c){
                    if(!contactIndividualMap.isEmpty() && contactIndividualMap.containsKey(relObj.Object_Contact__c)){
                       //Do nothing
                    }else{
                        String name = relObj.Relationship_Name__c; 
                        String detailsRequired='Please provide updated details as some details are found expired or missing';
                        individualTR += '<tr><td class="datatablerow"  style="border: 1px solid #ddd; padding: 10px;">' + name + '</td><td class="datatablerow"  style="border: 1px solid #ddd; padding: 10px;">' + detailsRequired +'</td></tr>';
                        contactIndividualMap.put(relObj.Object_Contact__c,individualTR);
                    }
                }
                //The body corporates listed below have missing details on the DIFC Portal
                if(relObj.Object_Account__c!=null && relObj.Body_Corporate_missing_details__c){
                    if(!accountBodyCorporateMap.isEmpty() && accountBodyCorporateMap.containsKey(relObj.Object_Account__c)){
                       //Do nothing
                    }else{
                        String name = relObj.Relationship_Name__c; 
                        String detailsRequired='Please provide updated details as some details are found expired or missing';
                        bodyCorporateTR += '<tr><td class="datatablerow"  style="border: 1px solid #ddd; padding: 10px;">' + name + '</td><td class="datatablerow"  style="border: 1px solid #ddd; padding: 10px;">' + detailsRequired +'</td></tr>';
                        accountBodyCorporateMap.put(relObj.Object_Account__c,bodyCorporateTR);
                    }
                }
            }
            htmlBody ='';
            if(passportTR!=''){
                htmlBody = '<b>The passports of the individuals listed below are nearing expiry or have expired as per the details recorded on the DIFC Portal.</b><br /><br /><table  style="font-family: arial, sans-serif; border-collapse:collapse; width: 80%;border: 1px solid #ddd;padding: 10px;" ><tr><th style="border: 1px solid #ddd; padding: 10px;">Name</th><th style="border: 1px solid #ddd; padding: 10px;">Passport Number</th><th style="border: 1px solid #ddd; padding: 10px;">Passport Expiry Date</th><th style="border: 1px solid #ddd; padding: 10px;">Passport Status</th></tr>'+ passportTR + '</table><br />';
            }
            if(individualTR!=''){
             htmlBody += '<b>The individuals listed below have missing details on the DIFC Portal</b><br /><br /><table  style="font-family: arial, sans-serif; border-collapse: collapse; width: 80%; border: 1px solid #ddd;padding: 10px;"><tr><th style="border: 1px solid #ddd; padding: 10px;">Name</th><th style="border: 1px solid #ddd; padding: 10px;">Details Required</th></tr>'+ individualTR + '</table><br />';
            }
            if(bodyCorporateTR!=''){
              htmlBody += '<b>The body corporates listed below have missing details on the DIFC Portal</b><br /><br /><table style="font-family: arial, sans-serif; border-collapse: collapse; width: 80%;border: 1px solid #ddd;padding: 10px;"><tr><th style="border: 1px solid #ddd; padding: 10px;" >Entity Name</th><th style="border: 1px solid #ddd; padding: 10px;">Details Required</th></tr>'+ bodyCorporateTR + '</table><br />';
            }
         }
        return HtmlBody;
    }
}