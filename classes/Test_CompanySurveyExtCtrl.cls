@isTest(SeeAllData=false)
public class Test_CompanySurveyExtCtrl {
 static testMethod void businessfeedbackMethod() {     
     
     Account acc = new Account();
     acc.Name = 'feedback account';
     insert acc;
     
     Contact con = new Contact();
     con.FirstName = 'feedback';
     con.LastName = 'Form';
     con.AccountId = acc.id;
     insert con;     
     
     Company_Survey__c ff = new Company_Survey__c();
    
     ff.Company__c= con.AccountId;    
     ff.Contact__c = con.id;
     
     ff.X11_No_of_Employees__c= 12.12;
      
     insert ff;
          
     Test.startTest();
     ApexPages.StandardController controller = new ApexPages.StandardController(ff);
     CompanySurveyExtCtrl feedctrl = new CompanySurveyExtCtrl(controller);
     PageReference pageRef = Page.Company_Survey; // Add your VF page Name here
     pageRef.getParameters().put('cid', con.Id);
      pageRef.getParameters().put('eid', 'aru@sd.com');
      Apexpages.currentPage().getParameters().put('ValParam','Email_Address__c');
      Apexpages.currentPage().getParameters().put('myParam','aru@sd.com');
      
     // pageRef.getParameters().put('ValParam', 'Email_Address__c');
    //  pageRef.getParameters().put('myParam', 'aru@sd.com');
      
      feedctrl.RowValueChange();
      
     Test.setCurrentPage(pageRef);     
     feedctrl.pageLoad();
     feedctrl.SaveComapnySurvey();
     Test.stopTest();
 }     
      
}