/*****************************************************************************
* Name             : FATCAAudit
* Created By       : Shabbir Ahmed
* Created Date     : 04/June/2014  
* Modifed Date     : 04/June/2014  
* Purpose          : A custom controller class to record the Audit for FATCA_Detail_Override, FATCA_Create_Edit_Override,FATCA_Build_Xml

/*****************************************************************************/

public class FATCAAudit{

public static void CreateAuditRecord(Id FATCAId, string description, string ipAddress){
    
    FATCA_Audit__c fAudit = new FATCA_Audit__c();
    fAudit.User__c = UserInfo.getUserId();
    fAudit.View_Datetime__c = Datetime.Now();
    fAudit.Description__c = description;
    fAudit.FATCA_Submission__c = FATCAId != NULL ? FATCAId : NULL;
    fAudit.IP_Address__c  = ipAddress; 
    insert fAudit;
}

}