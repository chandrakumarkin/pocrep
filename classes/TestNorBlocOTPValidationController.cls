@isTest
private class TestNorBlocOTPValidationController {

    static testMethod void myUnitTest() {
        
        Contact eachContact = new Contact();
        
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        List<Contact> lstContact = new List<Contact>();
        lstContact = OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert lstContact;
             
        Profile profile1 = [Select Id from Profile where name = 'DIFC Customer Community User Custom'];
        User portalAccountOwner1 = new User(ProfileId = profile1.Id,Username = System.now().millisecond() + 'test2@test.com',ContactId=lstContact[0].Id,
                                            Alias = 'TestDIFC',Email='test123@Difc.com',EmailEncodingKey='UTF-8',Firstname='Test',Lastname='DIFC',
                                            LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Chicago',isActive=true,Community_User_Role__c='Company Services');
        Database.insert(portalAccountOwner1);
        
        system.runAs(portalAccountOwner1){
            //Test.setMock(HttpCalloutMock.class, new NorBlocOTPValidationControllerMockTest());
            NorBlocOTPValidationController.getotpValidation('111444');  
          //  NorBlocOTPValidationController.dummytest();
            try{
                NorBlocOTPValidationController.sendOTPAgain();   
            }
            catch(Exception ex){
                
            }
            
            
        } 
    }
}