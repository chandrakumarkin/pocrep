public with sharing class TrainingConfirmationController {
    public Training_Master__c trainingMaster;
    public boolean submissionAllowed {get;set;}
    public TrainingConfirmationController(ApexPages.StandardController st){
        submissionAllowed = false;
        if (!Test.isRunningTest())
        	st.addFields(new List<String>{'Status__c'});
        this.trainingMaster = (Training_Master__c) st.getRecord();
        if(trainingMaster.Status__c == 'Approved by HOD')
            submissionAllowed = true;
    }
    public PageReference confirmInvitation() {
        PageReference pr ;
        Pr = new PageReference('/'+trainingMaster.id);
        List<Training_Request__c> lstTrainingRequest = new List<Training_Request__c>(); //
        for(Training_Request__c trainingReqObj : [select Status__c from Training_Request__c 
                                                  where TrainingMaster__c = :trainingMaster.Id and Status__c = 'Submitted']){
            trainingReqObj.Status__c = 'Invitation Sent';
            lstTrainingRequest.add(trainingReqObj);
        }
        try{
            if(lstTrainingRequest != null && lstTrainingRequest.size() > 0)
                update lstTrainingRequest;
            
            pr.setRedirect(true);
            return pr;
        }
        catch(Exception ex){
            ApexPages.addMessages(ex) ; 
        }
        return null;
    }
    public PageReference confirmReschedule() {
        try{
            PageReference pr ;
            Pr = new PageReference('/'+trainingMaster.id);
            List<Training_Request__c> lstTrainingRequest = new List<Training_Request__c>(); //
            for(Training_Request__c trainingReqObj : [select Status__c from Training_Request__c 
                                                      where TrainingMaster__c = :trainingMaster.Id 
                                                      and (Status__c = 'Submitted' or Status__c = 'Accepted')]){
                trainingReqObj.Status__c = 'Rescheduled';
                lstTrainingRequest.add(trainingReqObj);
            }
        
        	if(lstTrainingRequest != null && lstTrainingRequest.size() > 0)
        		update lstTrainingRequest;
            
        	pr.setRedirect(true);
            return pr;
        }
        catch(Exception ex){
            ApexPages.addMessages(ex) ; 
        }
        return null;
    }
    public PageReference confirmUpdate() {
        PageReference pr ;
        Pr = new PageReference('/'+trainingMaster.id);
        List<Training_Request__c> lstTrainingRequest = new List<Training_Request__c>(); //
        for(Training_Request__c trainingReqObj : [select Send_Update__c from Training_Request__c 
                                                  where TrainingMaster__c = :trainingMaster.Id ]){
            trainingReqObj.Send_Update__c = true;
            lstTrainingRequest.add(trainingReqObj);
        }
        try{
            if(lstTrainingRequest != null && lstTrainingRequest.size() > 0)
                update lstTrainingRequest;
            
            pr.setRedirect(true);
            return pr;
        }
        catch(Exception ex){
            ApexPages.addMessages(ex) ; 
        }
        return null;
    }
    public PageReference cancel(){
        PageReference pr ;
        Pr = new PageReference('/'+trainingMaster.id);
        pr.setRedirect(true);
        return pr;
    }
    /*
     @AuraEnabled
    public static string changeStatus(Id recordId) {
        List<Training_Request__c> lstTrainingRequest = new List<Training_Request__c>(); //
        for(Training_Request__c trainingReqObj : [select Status__c from Training_Request__c 
                                                  where TrainingMaster__c = :recordId and Status__c != 'Invalid']){
            trainingReqObj.Status__c = 'Confirmed';
            lstTrainingRequest.add(trainingReqObj);
        }
        try{
            if(lstTrainingRequest != null && lstTrainingRequest.size() > 0)
                update lstTrainingRequest;
        }
        catch(System.DmlException ex){
            return ex.getDmlMessage(0);
        }
        return 'The Training email is send successfully.';
    }
	*/
}