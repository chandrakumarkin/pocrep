@isTest
public class OB_AuthorizedSignatoryControllerTest {
    
    public static testmethod void test1(){
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        insertNewAccounts[0].ROC_Status__c = 'Active';
        update insertNewAccounts[0];
        
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 
        
       
        
         Relationship__c rel = new Relationship__c();
        rel.Relationship_Type__c ='Has Service Document Responsible for';
        rel.Subject_Account__c= insertNewAccounts[0].Id;
        rel.Object_Contact__c= con.id;
        rel.Active__c = true;
        insert rel;
        
         for( 
				 AccountContactRelation accConRel :  [
																								 SELECT Id,
																												 AccountId,
																												 ContactId,
																												 Roles,
																												 Access_Level__c,
																												 Account.ROC_Status__c
																								 FROM AccountContactRelation
																								 WHERE ContactId =:con.id
                     AND AccountId =: insertNewAccounts[0].Id
																								 
																				 
																						 ]
         ){
            AccountContactRelation newe = new AccountContactRelation(id = accConRel.id);
       
            newe.Roles = 'Company Services';
        upsert newe; 
         }
        
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Identify ultimate beneficial owners';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Rejected','Draft','Submitted'}, new List<string> {'REJECTED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
      
        
         // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'GENERAL'});
        insert listDocMaster;


//create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle','AOR_Financial'});
        insert createSRTemplateList;
        
        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        insert listSRTemplateDoc;        
        
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle', 'Name_Check'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].HexaBPM__customer__c=insertNewAccounts[0].Id;
        insertNewSRs[0].HexaBPM__customer__c=insertNewAccounts[0].Id;
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        insert insertNewSRs;
        insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs[0].id;
        update insertNewSRs;
        
        
        // create SR Doc
        List<HexaBPM__SR_Doc__c> listSRDoc = new List<HexaBPM__SR_Doc__c>();
        listSRDoc = OB_TestDataFactory.createSRDoc(1, insertNewSRs, listDocMaster, listSRTemplateDoc);
        insert listSRDoc;
        ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true
            );
            insert contentVersionInsert;
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            
            //create ContentDocumentLink  record 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = listSRDoc[0].id;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.shareType = 'V';
            insert cdl;
        
        List<Company_Name__c> companies = OB_TestDataFactory.createCompanyName(new List<String>{'Com1','Com2'});
        companies[0].Application__c = insertNewSRs[0].id;
        companies[0].Status__c = 'Approved';
        //insert companies;
        
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Role__c = 'Approved person';
        objAmendment.Passport_No__c = '1234';
        objAmendment.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment);
        
        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment1.Nationality_list__c = 'India';
        objAmendment1.Passport_No__c = '1234';
        objAmendment1.Role__c = 'UBO';
        objAmendment1.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment1.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment1);
        
        insert listAmendment;
        
        
        
        test.startTest();
        
        
        Id p = [select id from profile where name='DIFC Customer Community User Custom'].id;      
        
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user){
            
            OB_AuthorizedSignatoryController.RequestWrap reqWrapper = new OB_AuthorizedSignatoryController.RequestWrap();
            OB_AuthorizedSignatoryController.RespondWrap repWrapper = new OB_AuthorizedSignatoryController.RespondWrap();
            
            reqWrapper.pageId = listPage[0].Id;
            reqWrapper.flowId = listPageFlow[0].Id;
            reqWrapper.srId = insertNewSRs[0].Id;
            String requestString = JSON.serialize(reqWrapper);
            
            repWrapper  = OB_AuthorizedSignatoryController.fetchAmendRec(requestString);
            repWrapper  = OB_AuthorizedSignatoryController.initNewAmendment(requestString);
            repWrapper  = OB_AuthorizedSignatoryController.amendRemoveAction(requestString);
            
            try{
                OB_AuthorizedSignatoryController.updateCessationDate(requestString);
            }catch(Exception e){
                
            }
            reqWrapper.isAssigned = true;
            reqWrapper.amendObj = objAmendment;
            String requestString2 = JSON.serialize(reqWrapper);
            repWrapper  = OB_AuthorizedSignatoryController.amendRemoveAction(requestString2);
            repWrapper  =  OB_AuthorizedSignatoryController.amenSaveAction(requestString2);
            
            
            map<string,string> docmap =  new map<string,string>{'GENERAL' => documents[0].Id};
            reqWrapper.amendObj = objAmendment1;
            reqWrapper.docMap = docmap;
            String requestString3 = JSON.serialize(reqWrapper);
            repWrapper  =  OB_AuthorizedSignatoryController.amenSaveAction(requestString3);
            OB_AuthorizedSignatoryController.getButtonAction(insertNewSRs[0].Id,sec.Id,listPage[0].Id);
            
            
            
        }
        
        
        
    }

}