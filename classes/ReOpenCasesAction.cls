/*
    Author      : Kim Noceda -- PWC
    Date        : 05-May-2018
    Description : Class referenced in Re-Open Cases process flow. Has an invocable method
    --------------------------------------------------------------------------------------
*/
public without sharing class ReOpenCasesAction{

    @InvocableMethod(label='Create SLA' description='Create sla record for reopen cases.')
    public static void createSLA(List<Id> caseIds){
        string type_Origin;
        Long slaVal;
        /*
        List<Case_SLA__c> objSLAList = [SELECT Id, Parent__c, From__c, Until__c, Change_Type__c, 
                                        Owner__c, Status__c, Business_Hours_Id__c, Due_Date_Time__c 
                                        FROM Case_SLA__c WHERE Parent__c IN: caseIds ORDER BY CreatedDate DESC];
        
        Map<Id, Case_SLA__c> mapcaseSLA = new Map<Id, Case_SLA__c>();
        for(Case_SLA__c objSLA : objSLAList){
            if(!mapcaseSLA.containskey(objSLA.Parent__c)){
                mapcaseSLA.put(objSLA.Parent__c, objSLA);
            }
        }
        */
        map<string,SLA_Configurations__mdt> MapSLAMaster = new map<string,SLA_Configurations__mdt>();
        for(SLA_Configurations__mdt SLA:[select Id,Business_Hour_Id__c,Queue_Name__c,SLA_Hours__c,SLA_Minutes__c,Type__c from SLA_Configurations__mdt limit 10000])
            MapSLAMaster.put(SLA.Queue_Name__c.tolowercase()+'_'+SLA.Type__c,SLA);
        if(!caseIds.isEmpty()){
            
            List<Case_SLA__c> slaToBeInsertedlist = new List<Case_SLA__c>();
            List<Case> updateParentCase = new List<Case>();
            for(Case objCase : [SELECT Id,Status, GN_Department__c,GN_Type_of_Feedback__c,Origin,GN_Sys_Case_Queue__c from Case where id in :caseIds]){
                if(objCase.GN_Type_of_Feedback__c != null && objCase.GN_Type_of_Feedback__c != 'Invalid'){
                    type_Origin = objCase.GN_Type_of_Feedback__c;
                } else{
                    type_Origin = objCase.Origin;
                } 
                slaVal=0;
                if(String.isNotBlank(objCase.GN_Sys_Case_Queue__c) && String.isNotBlank(type_Origin) && MapSLAMaster.get(objCase.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin) != null ){
                    slaVal = MapSLAMaster.get(objCase.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).SLA_Minutes__c.longvalue();
                    slaVal = slaVal*60*1000L; 
                }
            //for(String keyStr : mapcaseSLA.keySet()){
                Case_SLA__c newObjSLA = new Case_SLA__c();//mapcaseSLA.get(keyStr).clone();
                newObjSLA.From__c = System.Now();
                if(String.isNotBlank(objCase.GN_Sys_Case_Queue__c) && String.isNotBlank(type_Origin) && MapSLAMaster.get(objCase.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin) != null ){
                    newObjSLA.Business_Hours_Id__c = MapSLAMaster.get(objCase.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).Business_Hour_Id__c;
                    //Long busHoursRemaining = BusinessHours.diff(mapcaseSLA.get(keyStr).Business_Hours_Id__c,mapcaseSLA.get(keyStr).Until__c,mapcaseSLA.get(keyStr).Due_Date_Time__c);
                    //DateTime newDueDate = BusinessHours.add(mapcaseSLA.get(keyStr).Business_Hours_Id__c,System.Now(),busHoursRemaining);
                    newObjSLA.Due_Date_Time__c = BusinessHours.add(newObjSLA.Business_Hours_Id__c,system.now(),slaVal);//newDueDate;
                }
                newObjSLA.Until__c = null;
                newObjSLA.Change_Type__c = 'Owner';
                newObjSLA.Status__c = objCase.Status;
                newObjSLA.Parent__c = objCase.id;
                newObjSLA.Owner__c = objCase.GN_Sys_Case_Queue__c;
                slaToBeInsertedlist.add(newObjSLA);
                
                Case updateCase = new Case(Id=objCase.Id);
                updateCase.GN_Due_Date_Time__c = newObjSLA.Due_Date_Time__c;
                updateParentCase.add(updateCase);
            //}
            }
            
            if(!slaToBeInsertedlist.isEmpty()){
                try{
                    insert slaToBeInsertedlist;
                    
                    update updateParentCase;
                } catch(Exception e){
                    slaToBeInsertedlist[0].addError(e.getMessage());
                    system.debug('@@ERROR: '+e.getMessage());
                }
            }
            //end for loop
        }
    }
}