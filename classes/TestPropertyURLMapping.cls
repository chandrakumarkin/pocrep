@isTest
private class TestPropertyURLMapping {
    static testMethod void myUnitTest() {
        PropertyURLMapping urlMapping = new PropertyURLMapping();
        PageReference ref = new PageReference('https://www.property.difc.com/map');
        urlMapping.mapRequestUrl(ref);
        
        ref = new PageReference('https://www.property.difc.com/map');
        urlMapping.mapRequestUrl(ref);
        
        ref = new PageReference('https://www.property.difc.com/search-results');
        urlMapping.mapRequestUrl(ref);
        
        ref = new PageReference('https://www.property.difc.com/advanced-search');
        urlMapping.mapRequestUrl(ref);
        
        ref = new PageReference('https://www.property.difc.com/for-sale/fs-123456789');
        urlMapping.mapRequestUrl(ref);
        
       
        
        
        List<PageReference> urls = new List<PageReference>();
        urls.add(new PageReference('https://www.property.difc.com/MapView'));
        urls.add(new PageReference('https://www.property.difc.com/AdvancedSearch'));
        urls.add(new PageReference('https://www.property.difc.com/SearchResults'));
        urls.add(new PageReference('https://www.property.difc.com/OtherURL'));
        urlMapping.generateUrlFor(urls);
    }
}