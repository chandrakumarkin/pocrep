@istest
public  class MortgageUserAccessFormClsTest {
    
    static testMethod Void createNewUserTest()
    {
        MortgageUserAccessFormCls usrAccessCls = new MortgageUserAccessFormCls();
        usrAccessCls.CancelRegistration();
        usrAccessCls.gettypeofAccess();
        usrAccessCls.displayExistingUserDetails();      
        usrAccessCls.typeofAccessSelected = 'Create New User';
        
        // Account Creation
        
         Account objAccount = new Account();
        objAccount.Name = 'Cancel SR Account';
        objAccount.AccountNumber = '12345';
        //objAccount.Is_Bank__c = true;
        insert objAccount;
        
        SR_Status__c srs =new SR_Status__c();
        srs.name = 'Approved';
        srs.Code__c = 'Approved';
        insert srs;
        
        usrAccessCls.UserAccessServiceRequest.User_form_Action__c='Create New User';
        usrAccessCls.UserAccessServiceRequest.customer__c = objAccount.id;
        usrAccessCls.UserAccessServiceRequest.Type_of_Access__c= 'Discharge of Mortgage';
        usrAccessCls.UserAccessServiceRequest.Title__c='Mr.';
        usrAccessCls.UserAccessServiceRequest.First_Name__c = 'firstName';
        usrAccessCls.UserAccessServiceRequest.Last_Name__c = 'LastName';
        usrAccessCls.UserAccessServiceRequest.Nationality_list__c = 'India';
        usrAccessCls.UserAccessServiceRequest.Passport_Number__c = '12fdgf';
       // usrAccessCls.UserAccessServiceRequest.Send_SMS_To_Mobile__c = '+971507301993';
        usrAccessCls.UserAccessServiceRequest.Email__c = 'abc@difc.com';
        usrAccessCls.SaveRequest(); 
        
        upsert  usrAccessCls.UserAccessServiceRequest;
        
        service_Request__c sr = new Service_request__c();
        sr = [select id from service_request__c limit 1];
        
         Step_Template__c stpTemp = new Step_Template__c();
      //  stpTemp =[select id from Step_Template__c  where code__c='END_TASK'];
        stpTemp.Code__c = 'END_TASK';
        stpTemp.Name = 'End Task';
        stpTemp.Step_RecordType_API_Name__c = 'End_Task';
        insert stpTemp;
        
        Step__c objStp = new Step__c();
        objStp.SR__c = sr.Id;
        objStp.Step_Template__c = stpTemp.id;
        insert objStp;
        
        step__c stp = new step__c();
        stp = [select id,sr__c,SR__r.Title__c,SR__r.Email__c,SR__r.First_Name__c,SR__r.Last_name__c,SR__r.User_Form_Action__c,SR__r.Nationality_list__c,SR__r.Passport_Number__c,SR__r.Send_SMS_To_Mobile__c,SR__r.Customer__c from step__c where id =:objStp.id limit 1];
        MortgageUserAccessFormCls.MortgageUserAccessApproval(stp);
       // MortgageUserAccessFormCls.CommitUserChanges(stp.id,con.id);
    }
    
    static testMethod Void updateExistingUserTest(){
        
        profile p =[select id,name from profile where name='DIFC Mortgage Community User' limit 1];
        MortgageUserAccessFormCls usrAccessCls = new MortgageUserAccessFormCls();
        Account objAccount = new Account();
        objAccount.Name = 'Cancel SR Account';
        objAccount.AccountNumber = '12345';
        insert objAccount;
        
        contact con = new contact();
        con.firstName = 'firstName';
        con.LastName = 'LastName';
        con.AccountID = objAccount.id;
        insert con;
        
        SR_Status__c srs =new SR_Status__c();
        srs.name = 'Approved';
        srs.Code__c = 'Approved';
        insert srs;
        
        
        
        User u = new User(Alias = 'standt', Email='abc@difc.com',       
          EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',firstName ='firstName',lastname='LastName',
          LocaleSidKey='en_US', ProfileId = p.Id,title='Mr.',phone='971507301993',contactID=con.id,Community_User_Role__c='Mortgage Registration',isActive=true,
          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
          insert u;
          
          usrAccessCls.typeofAccessSelected = 'Update Existing User';
          usrAccessCls.userName = 'standarduser@testorg.com';
          usrAccessCls.displayExistingUserDetails();
          usrAccessCls.SaveRequest();
          
        Step_Template__c stpTemp = new Step_Template__c();
       // stpTemp =[select id from Step_Template__c  where code__c='END_TASK'];
        stpTemp.Code__c = 'END_TASK';
        stpTemp.Name = 'End Task';
        stpTemp.Step_RecordType_API_Name__c = 'End_Task';
        insert stpTemp;
        
        Step__c objStp = new Step__c();
        objStp.SR__c = usrAccessCls.UserAccessServiceRequest.Id;
        objStp.Step_Template__c = stpTemp.id;
        insert objStp;
        
        step__c stp = new step__c();
        stp = [select id,sr__c,SR__r.Title__c,SR__r.Email__c,SR__r.First_Name__c,SR__r.Last_name__c,SR__r.User_Form_Action__c,SR__r.Nationality_list__c,SR__r.Passport_Number__c,SR__r.Send_SMS_To_Mobile__c,SR__r.Customer__c from step__c where id =:objStp.id limit 1];
        MortgageUserAccessFormCls.MortgageUserAccessApproval(stp);
        MortgageUserAccessFormCls.CommitUserChanges(stp.id,con.id);
          
          
          
          
          
          
          
          
          
          
          


        
    }
    
     static testMethod Void RemoveExistingUserTest(){
        
        profile p =[select id,name from profile where name='DIFC Mortgage Community User' limit 1];
        MortgageUserAccessFormCls usrAccessCls = new MortgageUserAccessFormCls();
        Account objAccount = new Account();
        objAccount.Name = 'Cancel SR Account';
        objAccount.AccountNumber = '12345';
        insert objAccount;
        
        contact con = new contact();
        con.firstName = 'firstName';
        con.LastName = 'LastName';
        con.AccountID = objAccount.id;
        insert con;
        
        SR_Status__c srs =new SR_Status__c();
        srs.name = 'Approved';
        srs.Code__c = 'Approved';
        insert srs;
        
        
        
        User u = new User(Alias = 'standt', Email='abc@difc.com',       
          EmailEncodingKey='UTF-8', LanguageLocaleKey='en_US',firstName ='firstName',lastname='LastName',
          LocaleSidKey='en_US', ProfileId = p.Id,title='Mr.',phone='971507301993',contactID=con.id,Community_User_Role__c='Mortgage Registration',isActive=true,
          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com');
          insert u;
          
          usrAccessCls.typeofAccessSelected = 'Remove Existing User';
          usrAccessCls.userName = 'standarduser@testorg.com';
          usrAccessCls.displayExistingUserDetails();
          usrAccessCls.SaveRequest();
          
        Step_Template__c stpTemp = new Step_Template__c();
      //  stpTemp =[select id from Step_Template__c  where code__c='END_TASK'];
        stpTemp.Code__c = 'END_TASK';
        stpTemp.Name = 'End Task';
        stpTemp.Step_RecordType_API_Name__c = 'End_Task';
        insert stpTemp;
        
        Step__c objStp = new Step__c();
        objStp.SR__c = usrAccessCls.UserAccessServiceRequest.Id;
        objStp.Step_Template__c = stpTemp.id;
        insert objStp;
        
        step__c stp = new step__c();
        stp = [select id,sr__c,SR__r.Title__c,SR__r.Email__c,SR__r.First_Name__c,SR__r.Last_name__c,SR__r.User_Form_Action__c,SR__r.Nationality_list__c,SR__r.Passport_Number__c,SR__r.Send_SMS_To_Mobile__c,SR__r.Customer__c from step__c where id =:objStp.id limit 1];
        MortgageUserAccessFormCls.MortgageUserAccessApproval(stp);
        MortgageUserAccessFormCls.CommitUserChanges(stp.id,con.id);
        MortgageUserAccessFormCls.prepareUserName('abc@difc.com');
        MortgageUserAccessFormCls.prepareUserName('standarduser@testorg.com');
        MortgageUserAccessFormCls.dmymthd();
          
          
          
          
          
          
          
          
          
          


        
    }
    
    
    
    
}