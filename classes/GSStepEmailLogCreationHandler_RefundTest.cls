@isTest(seeAllData=true)

public class GSStepEmailLogCreationHandler_RefundTest{


  static testMethod void myUnitTest1() {     
  
  
 Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        List<Contact> objContacts = new List<Contact>();
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        objContacts.add(objContact);
        
        Contact objContact1 = new Contact();
        objContact1.FirstName = 'Test Contact';
        objContact1.LastName = 'Test Contact';
        objContact1.AccountId = objAccount.Id;
        objContact1.Email = 'test1@difc2.com';
        objContact1.RecordTypeId = conRT;
        objContacts.add(objContact1);
        
        
        Contact objContact2 = new Contact();
        objContact2.FirstName = 'Test Contact';
        objContact2.LastName = 'Test Contact';
        objContact2.AccountId = objAccount.Id;
        objContact2.Email = 'test1@difc1.com';
        objContact2.RecordTypeId = conRT;
        objContacts.add(objContact2);
        
        
        Contact objContact3 = new Contact();
        objContact3.FirstName = 'Test Contact';
        objContact3.LastName = 'Test Contact';
        objContact3.AccountId = objAccount.Id;
        objContact3.Email = 'test1@difc3.com';
        objContact3.RecordTypeId = conRT;
        objContacts.add(objContact3);
        
        
        Contact objContact4 = new Contact();
        objContact4.FirstName = 'Test Contact';
        objContact4.LastName = 'Test Contact';
        objContact4.AccountId = objAccount.Id;
        objContact4.Email = 'test1@difc4.com';
        objContact4.RecordTypeId = conRT;
        objContacts.add(objContact4);
        
        insert objContacts;
        
        
        
        Document_Details__c doc = new Document_Details__c();
        doc.Contact__c=objContact.id; 
        doc.Document_Type__c ='Employment Visa';
        doc.Document_Number__c='1111';
        insert doc;
        
       /** Lookup__c lk= new Lookup__c();
        lk.Type__c='Nationality';
        lk.name='India';
        insert lk;**/
        
        Product2 objProduct = new Product2();
        objProduct.Name = 'DIFC Product';
        objProduct.IsActive = true;
        insert objProduct;
        
        Pricebook2 objPricebook = new Pricebook2();
        objPricebook.Name = 'Test Price Book';
        objPricebook.IsActive = true;
        objPricebook.Description = 'Test Description';
        insert objPricebook;
        
        if(objPricebook.IsActive == false){
            objPricebook.IsActive = true;
            update objPricebook;
        }
        
        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
        
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'Refund_Request';
        objTemplate1.SR_RecordType_API_Name__c = 'Refund_Request';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c='GS';
        insert objTemplate1;
        
        SR_Template__c objTemplate2 = new SR_Template__c();
        objTemplate2.Name = 'Access_Card';
        objTemplate2.SR_RecordType_API_Name__c = 'Access_Card';
        objTemplate2.Active__c = true;
        objTemplate2.SR_Group__c='GS';
        insert objTemplate2;
        
        
        SR_Template_Item__c objSRTempItem = new SR_Template_Item__c();
        objSRTempItem.Product__c = objProduct.Id;
        objSRTempItem.SR_Template__c = objTemplate1.Id;
        objSRTempItem.On_Submit__c = true;
        insert objSRTempItem;
        
       
        
         SR_Status__c srs1= [select id,name,code__c from SR_Status__c where Code__c='Pending' limit 1];
         
         SR_Template__c srTemplate = [select id,name,SR_RecordType_API_Name__c,SR_Group__c,Menutext__c  from SR_Template__c where name ='Request a Refund' ];
        
        
        system.Test.startTest();
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.RecordTypeId = mapSRRecordTypes.get('Refund_Request').Id;
        objSR1.Entity_Name__c = 'Test Entity Name';
        objSR1.Contact__c=objContact.id;
        objSR1.Pre_Entity_Name_Arabic__c='abc';
        objSR1.Document_Detail__c=doc.id;
        objSR1.ApplicantId_hidden__c=objContact.id;
        objSR1.Docdetail_hidden__c=doc.id;
        objSR1.Quantity__c=2;
        objSR1.Nationality_list__c='India';
        objSR1.External_SR_Status__c=srs1.id;
        objSR1.Visa_Expiry_Date__c=system.today()+20;
        objSR1.Residence_Visa_No__c='201/2015/1234567';
        objSR1.Statement_of_Undertaking__c = true;
        objSR1.Would_you_like_to_opt_our_free_couri__c ='No';
        objsR1.SR_Template__c = srTemplate.id;
        objSR1.Mobile_Number__c  ='+971504500847';
        objSR1.Type_of_Refund__c ='PSA Refund';
        objSR1.Mode_of_Refund__c ='PSA';
        objSR1.Refund_Amount__c =2500;
       insert objSR1;
        
        Step_Template__c stepTemplate = [select id,name,code__c from Step_Template__c where code__c ='FRONT_DESK_REVIEW'];
        Status__c srs =[select id,name,code__c from status__c where code__c = 'REJECTED'];
        
        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Contractor Community User Custom' limit 1];  //Customer Community Login User Custom
        List<user> users = new List<user>();
        User u = new User(Alias = 'standt', Email='tesclasst@difc.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='tesclasstuat@difc.com');
        u.Community_User_Role__c = 'Admin;Company Services Approver;Employee Services';    
        u.IsActive = true;
        u.contactID = objContacts[0].id; 
        users.add(u); 
        
        User u1 = new User(Alias = 'standt1', Email='tesclasst1@difc.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='tesclasstuat1@difc.com');
        u1.Community_User_Role__c = 'Admin;Company Services Approver;Employee Services';    
        u1.IsActive = true;
        u1.contactID = objContacts[1].id;
        users.add(u1);  
        
        User u2 = new User(Alias = 'standt3', Email='tesclasst2@difc.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='tesclasstuat2@difc.com');
        u2.Community_User_Role__c = 'Admin;Company Services Approver;Employee Services';    
        u2.IsActive = true;
        u2.contactID = objContacts[2].id; 
        users.add(u2); 
        
        User u3 = new User(Alias = 'standt3', Email='tesclasst3@difc.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='tesclasstuat3@difc.com');
        u3.Community_User_Role__c = 'Admin;Company Services Approver;Employee Services';    
        u3.IsActive = true;
        u3.contactID = objContacts[3].id;
        users.add(u3);  
        
        AccountContactRelation acctcr1 = new AccountContactRelation(Id= [SELECT Id FROM AccountContactRelation WHERE AccountId =: objAccount.Id LIMIT 1].Id,AccountId = objAccount.id, ContactId = objContacts[0].id, Roles = 'Employee Services');
        update acctcr1;
        
        AccountContactRelation acctcr2 = new AccountContactRelation(Id= [SELECT Id FROM AccountContactRelation WHERE AccountId =: objAccount.Id LIMIT 1].Id,AccountId = objAccount.id, ContactId = objContacts[1].id, Roles = 'Employee Services');
        update acctcr2;
        
        AccountContactRelation acctcr3 = new AccountContactRelation(Id= [SELECT Id FROM AccountContactRelation WHERE AccountId =: objAccount.Id LIMIT 1].Id,AccountId = objAccount.id, ContactId = objContacts[2].id, Roles = 'Employee Services');
        update acctcr3;
        
        AccountContactRelation acctcr4 = new AccountContactRelation(Id= [SELECT Id FROM AccountContactRelation WHERE AccountId =: objAccount.Id LIMIT 1].Id,AccountId = objAccount.id, ContactId = objContacts[3].id, Roles = 'Employee Services');
        update acctcr4;
        
        User u4 = new User(Alias = 'standt4', Email='tesclasst4@difc.com', 
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey='en_US', ProfileId = p.Id, 
        TimeZoneSidKey='America/Los_Angeles', UserName='tesclasstuat4@difc.com');
        u4.Community_User_Role__c = 'Admin;Company Services Approver;Employee Services';    
        u4.IsActive = true;
        u4.contactID = objContacts[4].id; 
        users.add(u4);
        
        insert users;
        
        
         
        
        Step__c objStep1 = new Step__c();
        objStep1.SR__c = objSR1.Id;
        objStep1.Step_Template__c = stepTemplate.id ;
        objStep1.status__c = srs.id;
        objstep1.Step_Notes__c ='test1';
        objStep1.Client_Name__c ='test user';
        objStep1.Rejection_Reason__c ='Test Rejection';
        insert objStep1;
        
        
        step__c objstp = [select id,Sys_SR_Group__c,Step_Name__c,Status_Code__c,SR_Template__c,SR_Menu_Text__c,Client_Name__c from step__c where id =:objStep1.id];
        
        List<GSStepandStatus__mdt> gstep = [select  MasterLabel,status__c from GSStepandStatus__mdt ];
        
       
        
        GSStepEmailLogCreationHandler_Refund.CreateEmailLogOnUpdate(objstp);
        
        
        
        system.Test.stopTest();
        
       } 

}