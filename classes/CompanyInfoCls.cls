/*********************************************************************************************************************
 *  Name        : CompanyInfoCls
 *  Author      : Ravi
 *  Company     : NSI DMCC
 *  Date        : 2014-10-18     
 *  Purpose     : This class is to display the company info on the Portal     
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------  
 V1.1    01/04/2015  Kaavya       Changes in GS Relationship display
 V1.2    13/11/2015  Rajil        Added a function to prepare the operating locations list.
 V1.3    21/12/2015  Kaavya       Fix for including expired employees in the Relationships  
 V1.4    25/01/2016  Rajil        Added other fields - index number, Visa Quota to the Company info - Mobile
 V1.5    21/03/2016  Ravi         Added logic for DP - As per #2418, if company is not processing the DP currently but processed earlier then system should the previous dp info
 V1.6    02-09-2016  Sravan       Added the logic to fetch Portal users for Account TKT #  2962
 V1.7    06-10-2016  Claude       #3438 - Changed condition from "!= 'GS'" to " == 'ROC' "
**********************************************************************************************************************/
public without sharing class CompanyInfoCls {
    list<User> lstUser;
    Id accid;
    //public Contact ContactInfo {get;set;}
    
    public Boolean MoreRows {get;set;} 
    public Boolean MoreGSRows {get;set;}
    //Added by Kaavya for Nature of business
    public String NoB {get;set;} 
    public Boolean ExcpCust {get;set;} 
    public string accessType {set;get;}
    Public list<user> portalUser{get;set;} // V1.6
    
    public decimal VisaQuota{get;set;}
    public decimal NoOfEmp{get;set;}
    public CompanyInfoCls(ApexPages.StandardController controller) {
        objAcc = new Account();
        lstCon = new list<Contact>();
        VisaQuota = 0;
        NoOfEmp = 0;
        map<String, Schema.SObjectType>  m = Schema.getGlobalDescribe();
        SObjectType  objtype;
        DescribeSObjectResult objDef1;
        map<String, SObjectField> fieldmap;
        if(m.get('Account')!= null)
            objtype = m.get('Account');
            
        //dynamic query to retreive all the account fields
        string strAccQuery = 'Select ';
        if(objtype != null)
            objDef1 =  objtype.getDescribe();
        if(objDef1 != null)
            fieldmap =  objDef1.fields.getmap();
        if(fieldmap!=null){
            for(Schema.SObjectField strFld:fieldmap.values()){
                Schema.DescribeFieldResult fd = strFld.getDescribe();
                if(fd.isAccessible() && string.valueOf(strFld).toLowerCase()!='id' && string.valueOf(strFld).toLowerCase()!='calculated_visa_quota__c' && string.valueOf(strFld).toLowerCase()!='utilize_visa_quota__c')
                    strAccQuery += strFld+',';
            }
            //checking if the loggedin user is a community user to fetch the account details
            lstUser = [select id,ContactId,Community_User_Role__c,Contact.AccountId,AccountID from user where id=:userinfo.getUserId()];
            if(lstUser!=null && lstUser.size()>0 && lstUser[0].Contact.AccountId!=null){
                string AccountId = lstUser[0].Contact.AccountId;
                strAccQuery+='Id,Nature_of_Business__c,calculated_visa_quota__c,Utilize_Visa_Quota__c,Exceptional_Customer__c,Index_Card__r.Name from Account where Id=:AccountId';
                for(Account acc:database.query(strAccQuery)){
                    objAcc = acc;
                    if(objAcc.Calculated_Visa_Quota__c!=null)
                        VisaQuota = objAcc.Calculated_Visa_Quota__c;
                    if(objAcc.Utilize_Visa_Quota__c!=null)
                        NoOfEmp = objAcc.Utilize_Visa_Quota__c;
                }
                accid=objAcc.id; //Added by Kaavya on Jan 5th,2015
                
                //Added by Kaavya on Mar 16th,2015
                NoB=objAcc.Nature_of_Business__c; 
                if(objAcc.Exceptional_Customer__c==true)
                    ExcpCust=true;
             
               //User Access Restriction Added by Saima - 29th Apr 2015
               if(lstUser[0].Community_User_Role__c!=null && lstUser[0].Community_User_Role__c!=''){
                   if(lstUser[0].Community_User_Role__c.contains('Employee Services') && lstUser[0].Community_User_Role__c.contains('Company Services')){
                        accessType='both';
                   }else if(lstUser[0].Community_User_Role__c.contains('Employee Services') && !(lstUser[0].Community_User_Role__c.contains('Company Services'))){
                        accessType='GS';
                   }else if(lstUser[0].Community_User_Role__c.contains('Company Services') && !(lstUser[0].Community_User_Role__c.contains('Employee Services'))){
                        accessType='ROC';
                   }
               }
                //Fetching only the active employees for employee related list.
                /*
                if(objAcc!=null && objAcc.Id!=null){
                    lstCon = [select id,Name,Email,Passport_No__c,Job_Title__c,Job_Title__r.Name,Employment_Status__c,DOCUMENT_TYPE__c,Nationality_Lookup__c,Nationality_Lookup__r.Name from Contact where AccountId=:objAcc.Id and Active_Employee__c=true and DOCUMENT_TYPE__c!='UserRole'];
                    
                }*/
            }
        }
        MoreRows = false;
        MoreGSRows = false;
        PrimaryRelationshipDetails();
        EmployeeRelationshipDetails();
        //ContactInfo = [select Id,Name,FirstName,LastName,Email,Country__c,BP_No__c,MobilePhone,Phone,Work_Phone__c from Contact where Id='0031100000c6s5L'];
        Prepare_Operating_Locations();
        
        //  V1.6  Get User Details
        if(lstUser !=null && lstUser.size()>0 && lstUser[0].accountID !=null)
            CompanyPortalUserDetails(lstUser[0].accountID);  
            
    }
    
    //Added by Rajil
    //Purpose: Prepares the Operating Locations for DIFC Mobile.
    public list<OperatingLocations> lstOperatingLocations{get;set;}
    
    public void Prepare_Operating_Locations()
    {
        lstOperatingLocations = new list<OperatingLocations>();
        if(lstUser[0].Community_User_Role__c.contains('Company Services')){
            for(Operating_Location__c templ:[select Name,Unit__r.Name,Unit__r.Building_Name__c,Unit__r.Floor__c,Area__c from Operating_Location__c WHERE Account__c =:accid]){
                OperatingLocations obj = new OperatingLocations();
                obj.Name = templ.Name;
                obj.AccountUnitNumber = templ.Unit__r.Name;
                obj.buildingName = templ.Unit__r.Building_Name__c;
                obj.floorNumber = templ.Unit__r.Floor__c;
                obj.occupiedArea = String.valueOf(templ.Area__c) + ' SqFt';
                lstOperatingLocations.add(obj);
            }
        }
        
    }
    public class OperatingLocations
    {
        public string Name {get;set;}
        public string AccountUnitNumber {get;set;}
        public string buildingName {get;set;}
        public string floorNumber {get;set;}
        public String occupiedArea {get;set;}
    }
    
    //Added by Kaavya on Jan 5th,2015 - To display DP details on the page
    public List<Permit__c> getpermitlist() {
        List<Permit__c> PermitList = 
            [SELECT id,Name,Permit_Type__c,(select Id,Name,Name_of_Jurisdiction__c,RecordType.Name from Data_Protections__r) FROM Permit__c WHERE  Active__c=True and Account__c=:accid order by Permit_Type__c];
        //V1.5
        if(PermitList == null || PermitList.isEmpty()){
            PermitList = new list<Permit__c>();
            Date dDate;
            for(Permit__c objP : [select Id,Name,Permit_Type__c,Date_To__c,(select Id,Name,Name_of_Jurisdiction__c,RecordType.Name from Data_Protections__r) FROM Permit__c where Account__c=:accid order by Date_To__c desc]){
                if(dDate == null || dDate == objP.Date_To__c){
                    dDate = objP.Date_To__c;
                    PermitList.add(objP);
                }else
                    break;
            }           
        }
           return PermitList ;
    }
    
    //Added by Kaavya on Mar 16th,2015 - To display Activities on the page
    public List<License_Activity__c> getActylist() {
           List<License_Activity__c> ActyList = 
              [SELECT id,Name,Activity__c,Activity__r.name,CreatedDate FROM License_Activity__c WHERE  End_Date__c=null and Account__c=:accid order by createddate];
           return ActyList ;
    }
    
    public void PrimaryRelationshipDetails(){
        //Relationship__c.Relationship_Type__c
        CompanyRelationships = new list<Relationship__c> ();
        system.debug('accesstype '+accessType);
        if(MoreRows){
            // V1.7 - Claude - Replaced by = 'ROC' Filter => CompanyRelationships = [select Id,Name,End_Date__c,Start_Date__c,Relationship_Group__c,Subject_Account__c,Object_Account__c,Object_Contact__c,Object_Account__r.Name,Object_Contact__r.Name,Active__c,Relationship_Type_formula__c from Relationship__c where Relationship_Group__c !='GS' AND Subject_Account__c=:accid order by Name];
            CompanyRelationships = [select Id,Name,End_Date__c,Start_Date__c,Relationship_Group__c,Subject_Account__c,Object_Account__c,Object_Contact__c,Object_Account__r.Name,Object_Contact__r.Name,Active__c,Relationship_Type_formula__c from Relationship__c where Relationship_Group__c ='ROC' AND Subject_Account__c=:accid order by Name];
        }else {
            // V1.7 - Claude - Replaced by = 'ROC' Filter => CompanyRelationships = [select Id,Name,End_Date__c,Start_Date__c,Relationship_Group__c,Subject_Account__c,Object_Account__c,Object_Contact__c,Object_Account__r.Name,Object_Contact__r.Name,Active__c,Relationship_Type_formula__c from Relationship__c where Relationship_Group__c !='GS' AND Subject_Account__c=:accid order by Name limit 10];
            CompanyRelationships = [select Id,Name,End_Date__c,Start_Date__c,Relationship_Group__c,Subject_Account__c,Object_Account__c,Object_Contact__c,Object_Account__r.Name,Object_Contact__r.Name,Active__c,Relationship_Type_formula__c from Relationship__c where Relationship_Group__c ='ROC' AND Subject_Account__c=:accid order by Name limit 10];
        }
        system.debug('PPPP'+CompanyRelationships);
        
    }
    public void EmployeeRelationshipDetails(){
        //Relationship__c.Relationship_Type__c
        system.debug('accesstype '+accessType);
       //accessType='GS';
        EmployeeRelationships= new list<Relationship__c> ();
        if(MoreGSRows)
            EmployeeRelationships = [select Id,Name,End_Date__c,Relationship_Group__c,Subject_Account__c,Object_Account__c,Object_Contact__c,Object_Account__r.Name,Object_Contact__r.Name,Object_Contact__r.Visa_Expiry_Date__c,Object_Contact__r.Is_Absconder__c,Object_Contact__r.Is_Violator__c,Object_Contact__r.Active_Employee__c,Active__c,Relationship_Type_formula__c from Relationship__c where (End_Date__c> TODAY OR (Active__c=TRUE AND Relationship_Type__c='Has DIFC Sponsored Employee')) AND Relationship_Group__c ='GS' AND Subject_Account__c=:accid order by Name]; //v1.3 included expired employees
        else
            EmployeeRelationships = [select Id,Name,End_Date__c,Relationship_Group__c,Subject_Account__c,Object_Account__c,Object_Contact__c,Object_Account__r.Name,Object_Contact__r.Name,Object_Contact__r.Visa_Expiry_Date__c,Object_Contact__r.Is_Absconder__c,Object_Contact__r.Is_Violator__c,Object_Contact__r.Active_Employee__c,Active__c,Relationship_Type_formula__c from Relationship__c where (End_Date__c> TODAY OR (Active__c=TRUE AND Relationship_Type__c='Has DIFC Sponsored Employee')) AND Relationship_Group__c ='GS' AND Subject_Account__c=:accid order by Name limit 10]; //v1.3 included expired employees
        
        
    }
    
   
    //  V1.6  
     
    public void CompanyPortalUserDetails(string accID)
    {
        portalUser = new list<user>();
        if(accID !=null)
                portalUser = [select id,email,userName,UserRole.Name,AccountID,Name,Community_User_Role__c from user where contactid !=null and accountID !=null and accountID =:accID and IsActive=true];
    } 
    
    transient public Account objAcc{get;set;}
    transient public list<Contact> lstCon{get;set;}
    transient public list<Service_Request__c> lstSR{get;set;}
    transient public list<Permit__c> lstPermit{get;set;}
    transient public list<License__c> lstLicense{get;set;}
    transient public list<Relationship__c> PrimaryRelationships {get;set;}
    transient public list<Relationship__c> CompanyRelationships {get;set;}
    transient public list<Relationship__c> EmployeeRelationships {get;set;}
}