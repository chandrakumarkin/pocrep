global class SearchAndReplaceHexaBPMSteps implements Database.Batchable < sObject > {
    
    global final String Query;
    global List<sObject> passedRecords= new List<sObject>();

    global SearchAndReplaceHexaBPMSteps(String q,List<sObject> listofRecords) {
        Query = q;
        passedRecords = listofRecords;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(query != '')return Database.getQueryLocator(query);
        else return DataBase.getQueryLocator([Select id,Email__c from HexaBPM__Step__c WHERE Id IN: passedRecords and Id != null]);
    }
    global void execute(Database.BatchableContext BC, List < HexaBPM__Step__c > scope) {
    
        for (HexaBPM__Step__c Sr: scope) {
            Sr.Email__c = 'test@difc.ae.invalid';
        }
        Database.SaveResult[] srList = Database.update(scope, false);

        List<Log__c> logRecords = new List<Log__c>();
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                for(Database.Error err : sr.getErrors()) {
                    string errorresult = err.getMessage();
                    logRecords.add(new Log__c(
                      Type__c = 'SearchAndReplaceHexaBPMSteps',
                      Description__c = 'Exception is : '+errorresult+'=Lead Id==>'+sr.getId()
                    ));
                    System.debug('The following error has occurred.');                    
                }
            }
        }
        if(logRecords.size()>0)insert logRecords;
    }

    global void finish(Database.BatchableContext BC) {}
}