/*********************************************************************************************************************
    Author      :   Saima Hidayat
    Description :   Shares/Membership Interest Details for the Shareholder or Member
    Date        :   21-Sep-2014
    
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.1    09-02-2015  Saima         Removed condition for currency picklist is null in calcshares func.
V1.2    04-02-2016  Saima         Added code for restrict reduction of no. of shares in existing active class.
V1.3    04-09-2016  Sravan        Added code for Ticket # 2825
V1.4    14-11-2016  Sravan        Added Status field in the Accountshare query Tkt # 3315
V1.5    23-12-2019  Zoeb          Removed validation for Total amount of share must be >= 50000 line 410-413
*********************************************************************************************************************/
public without sharing class UpdateAuthorizedShares_Ctrl{

    /* Start of Properties for Dynamic Flow */
    public boolean isChangesSaved = false;
    public boolean isProcessFlow {
        get;
        set;
    }
    public string strPageId {
        get;
        set;
    }
    public string strHiddenPageIds {
        get;
        set;
    }
    public string strActionId {
        get;
        set;
    }
    public map < string,
    string > mapParameters;
    public Service_Request__c objSRFlow {
        get;
        set;
    }

    public string pageTitle {
        get;
        set;
    }
    public string pageDescription {
        get;
        set;
    }
    public string checkFrmDetail {
        set;
        get;
    } // V1.3
    
    Set<string> SetofExistingClass{
        get;
        set;
    }
    
    
    

    /* End of Properties for Dynamic Flow */

    Public string srStatus {
        get;
        set;
    } // V1.4 

    String SRId;
    public boolean delBtn {
        get;
        set;
    }
    Public String SRtype {
        get;
        set;
    }
    Public String AutType {
        get;
        set;
    }
    Public String strType {
        get;
        set;
    }
    public Service_Request__c obSR {
        get;
        set;
    }
    public Integer RowId {
        get;
        set;
    }
    public List < AccountShareDetailsWrapper > lstAccountWrapper {
        get;
        set;
    }
    
    public decimal totalAuthorized{
        
        get;
        set;
    }
    
    public Map<ID,List<Amendment__c>> mapAccountDetails{
        
        get;
        set;
    }
    
    Decimal productShares = 0;
    Decimal sumShares = 0;
    string AccountID;
    
    string Old_ClassName;
    Decimal Old_NominalValue;
    Decimal Old_SharesValue;
    
    boolean IsError;
    
    Decimal totalshare;
    Decimal sum;
    map < string,string > mapCurrencyIds = new map < string,string > ();
    map < string,string > mapCurrencyNames = new map < string,string > ();
    map < string,decimal > mapCurrencyRates = new map < string,decimal > ();
    
    string RecordTypeID;

    public UpdateAuthorizedShares_Ctrl() {
        

        IsError = false;
        RecordTypeID = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Account Share Holder Detail').getRecordTypeId();
        SRId = ApexPages.currentPage().getParameters().get('Id');
        /* Start of Properties Initialization for Dynamic Flow */
        isProcessFlow = false;
        isChangesSaved = false;
        mapParameters = new map < string,string > ();
        
        if (apexpages.currentPage().getParameters() != null) mapParameters = apexpages.currentPage().getParameters();
        //V1.3
        if (mapParameters != null && mapParameters.containsKey('fd')) {
            checkFrmDetail = mapParameters.get('fd'); // V1.3
        }

        objSRFlow = new Service_Request__c();
        objSRFlow.Id = SRId;
        mapAccountDetails = new Map<ID,List<Amendment__c>>();

        PropertyIntilizationFlow();
        /* End of Properties Initialization for Dynamic Flow */

        ServiceRequestDetail();
        
        GetCurrenctCapital();
        
        //GetAccountShareDetails();
        
        GetCurrencyRates();

        

        

    }
    
    public void GetAmendment(){
        
        List<Amendment__c> lstAmendment = [Select ID, Class_Name__c,Is_History__c,No_of_shares_per_class__c,Currency__c,sys_create__c,
                                            Nominal_Value__c,Sys_Proposed_Nominal_Value__c,Sys_Proposed_No_of_Shares_per_Class__c,
                                            Currency__r.Name,Sys_Proposed_Nominal_Text__c,
                                            Total_Issued__c, ServiceRequest__c FROM Amendment__c WHERE ServiceRequest__c =: SRId
                                            AND Is_History__c = false AND Status__c = 'Active' ORDER BY ID];
        
        
        
        if(lstAmendment.isEmpty()){
            List<Amendment__c> lstNewAmnd = GetAccountShareDetails();
            if(lstNewAmnd != null)
                LoadAmendment(lstNewAmnd);
            else{
                lstAccountWrapper = new List<AccountShareDetailsWrapper>();
                addRow();
            }// new create if now found anything
        }
        else{
            
            LoadAmendment(lstAmendment);
        }
    }
    
    void GetRemovedClass(){
        SetofExistingClass = new Set<string>();
        for(Amendment__c iAcc :[Select Class_Name__c FROM Amendment__c WHERE ServiceRequest__c =: SRId
                                            AND Is_History__c = false AND Status__c = 'Removed']){
                
            SetofExistingClass.add(iAcc.Class_Name__c.ToUpperCase());
        }
        
    }
    

    
    void LoadAmendment(List<Amendment__c> aLstAmnd){
        
        decimal AuthPerClass = 0;
        totalAuthorized = 0;
        lstAccountWrapper = new List<AccountShareDetailsWrapper>();
        
        
        for(Amendment__c iAcc : aLstAmnd){
            
            
            if(iAcc.Is_History__c) continue;
            
            //if(iAcc.Proposed_Class_Name__c == '' || iAcc.Proposed_Class_Name__c == NULL) iAcc.Proposed_Class_Name__c = iAcc.Name;
            
            if(iAcc.Sys_Proposed_Nominal_Value__c != null && string.isBlank(iAcc.Sys_Proposed_Nominal_Text__c))
                iAcc.Sys_Proposed_Nominal_Text__c = string.valueOf(iAcc.Sys_Proposed_Nominal_Value__c);
                
            totalAuthorized += (iAcc.Sys_Proposed_No_of_Shares_per_Class__c * decimal.valueOf(iAcc.Sys_Proposed_Nominal_Text__c));
            AuthPerClass = (iAcc.Sys_Proposed_No_of_Shares_per_Class__c * decimal.valueOf(iAcc.Sys_Proposed_Nominal_Text__c));
            
            //string oldKey = iAcc.Name+'-'+iAcc.Nominal_Value__c+'-'+iAcc.No_of_shares_per_class__c;
            lstAccountWrapper.add(new AccountShareDetailsWrapper(iAcc,'',false, AuthPerClass));//NewAmnd,oldAmnd
            /*if(iAcc.status__c == 'Active')
                mapAccountDetails.put(iAcc.ID,new List<Amendment__c> { createAmendment(iAcc,true)}); //old */
            
            
            
        }
        
        obSR.Total_Proposed_Authorized_Capital__c = totalAuthorized;
        
        GetRemovedClass(); // get class name which are removed so they cannot enter again.

    }
    
    List<Amendment__c> GetAccountShareDetails(){
        
        List < Account_Share_Detail__c > lstAccountShare = [Select id,Name,Proposed_Class_Name__c, Sys_Proposed_No_of_Shares_per_Class__c, Sys_Proposed_Nominal_Value__c, Nominal_Value__c, 
                                                        No_of_shares_per_class__c, Total_Issued__c, Status__c, Total_Issues_Shares_sys_Proposed__c,Currency__c
                                                        from Account_Share_Detail__c where Account__c = :obSR.Customer__c AND Status__c = 'Active']; // V1.4 // Added Total_Issues_Shares_sys_Proposed__c for V1.3 
        
        if(!lstAccountShare.isEmpty()){
            List<Amendment__c> lstNewAmendment = new List<Amendment__c>();
            for(Account_Share_Detail__c iAcc : lstAccountShare){
                lstNewAmendment.add(createAmendment(iAcc,true));
                lstNewAmendment.add(createAmendment(iAcc,false));
            }
            
            insert lstNewAmendment;
            return lstNewAmendment;
        }
        
        return null;
        
        //addRow();
    }
    
    Amendment__c createAmendment(Account_Share_Detail__c aAccHolder , boolean aIsHistory){
        
        Amendment__c amm = new Amendment__c();
        system.debug('$$$$$' + aAccHolder.Name);
        amm.Class_Name__c = aAccHolder.Name;
        amm.Is_History__c = aIsHistory;
        amm.No_of_shares_per_class__c =  aAccHolder.No_of_shares_per_class__c;
        amm.Nominal_Value__c = aAccHolder.Nominal_Value__c;
        amm.Sys_Proposed_Nominal_Value__c = aAccHolder.Sys_Proposed_Nominal_Value__c;
        amm.Sys_Proposed_No_of_Shares_per_Class__c = aAccHolder.Sys_Proposed_No_of_Shares_per_Class__c;
        amm.Total_Issued__c = aAccHolder.Total_Issued__c;
        amm.ServiceRequest__c = SRId;
        amm.AccountShareHolderDetail__c = aAccHolder.ID;
        amm.Currency__c = aAccHolder.Currency__c;
        system.debug('@@@@@@2=== ' + aAccHolder.Currency__c);
        amm.Status__c = 'Active';
        amm.sys_create__c = true;
        amm.RecordTypeID = RecordTypeID;
        system.debug('@@@@@@2=== ' + amm.Currency__c);
        return amm;
        
        
    }
    
    
    void GetCurrenctCapital(){
        
        obSR.Share_Capital_Membership_Interest__c = [Select Authorized_Share_Capital__c 
                                                    from Account where ID =: AccountID limit 1].Authorized_Share_Capital__c;
        
    }
    
    
 
    void GetCurrencyRates(){
        
        for(Currency__c curr : [Select id,Name,Exchange_Rate__c from Currency__c where Active__c=true]){
            mapCurrencyIds.put(curr.Name,curr.Id);
            mapCurrencyNames.put(curr.Id,curr.Name);
            mapCurrencyRates.put(curr.Name,curr.Exchange_Rate__c);
        }
        
    }
    
    void ServiceRequestDetail(){
        
        Service_Request__c srDetails = [Select Share_Capital_Membership_Interest__c, Issued_Share_Capital_Membership_Interest__c, Date_of_Resolution__c, No_of_Authorized_Share_Membership_Int__c, No_of_Issued_Share_Membership_Interest__c, External_Status_Code__c, 
                                        currency_rate__r.Name,Total_Proposed_Authorized_Capital__c,Customer__c,currency_rate__c,currency_list__c,Customer__r.Min_capital_requirements__c  from Service_Request__c
                                        where id = :objSRFlow.Id];

        srStatus = srDetails.External_Status_Code__c; // V1.4
        AccountID = srDetails.Customer__c;

        obSR = new Service_Request__c(Id = SRId, legal_structures__c = objSRFlow.legal_structures__c, Customer__c = objSRFlow.Customer__c, Share_Capital_Membership_Interest__c = srDetails.Share_Capital_Membership_Interest__c, Issued_Share_Capital_Membership_Interest__c = srDetails.Issued_Share_Capital_Membership_Interest__c, No_of_Authorized_Share_Membership_Int__c = srDetails.No_of_Authorized_Share_Membership_Int__c, No_of_Issued_Share_Membership_Interest__c = srDetails.No_of_Issued_Share_Membership_Interest__c, currency_list__c = srDetails.currency_list__c , Date_of_Resolution__c = srDetails.Date_of_Resolution__c, Total_Proposed_Authorized_Capital__c = srDetails.Total_Proposed_Authorized_Capital__c,currency_rate__r = srDetails.currency_rate__r,currency_rate__c = srDetails.currency_rate__c);
      
        strType = mapParameters.get('Type');
        if (objSRFlow.record_type_name__c == 'Change_in_Authorized_Shares_or_Nominal_Value') {
            if (strType == 'amendshares' && strType != null && objSRFlow.legal_structures__c == 'LLC') {
                strType = 'Member';
            } else {
                strType = 'Shareholder';
            }
        }

        if (strType == 'Shareholder') {
            SRtype = 'Shares';
            AutType = 'Share Capital';
        }
        else {
            SRtype = 'Membership Interest';
            AutType = 'Membership Interest';
        }
    }
    
    
   
    
    void PropertyIntilizationFlow() {

        if (objSRFlow.Id != null) {
            strPageId = mapParameters.get('PageId');
            if (strPageId != null && strPageId != '') {
                for (Page__c page: [select id, Name, Page_Description__c from Page__c where Id = :strPageId]) {
                    pageTitle = page.Name;
                    pageDescription = page.Page_Description__c;
                }
            }
            if (mapParameters != null && mapParameters.get('FlowId') != null) {
                set < string > SetstrFields = Cls_Evaluate_Conditions.FetchObjectFields(mapParameters.get('FlowId'), 'Service_Request__c');
                string strQuery = 'select Id';
                SetstrFields.add('legal_structures__c');
                SetstrFields.add('customer__c');
                SetstrFields.add('finalizeamendmentflg__c');
                SetstrFields.add('share_capital_membership_interest__c');
                SetstrFields.add('record_type_name__c');
                SetstrFields.add('currency_list__c');
                SetstrFields.add('currency_rate__c');
                SetstrFields.add('External_Status_Code__c');
                SetstrFields.add('isClosedStatus__c');
                //   SetstrFields.add('Record_type_Name__c'); // V1.3
                if (SetstrFields != null && SetstrFields.size() > 0) {
                    for (String strFld: SetstrFields) {
                        if (strFld.toLowerCase() != 'id') strQuery += ',' + strFld.toLowerCase();
                    }
                }

                strQuery = strQuery + ' from Service_Request__c where Id=:SRId';
                system.debug('strQuery===>' + strQuery);
                for (Service_Request__c SR: database.query(strQuery)) {
                    objSRFlow = SR;
                }
                strHiddenPageIds = PreparePageBlockUtil.getHiddenPageIds(mapParameters.get('FlowId'), objSRFlow);
            }
        }

    }

    
  
    public Component.Apex.PageBlock getDyncPgMainPB() {
        PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = objSRFlow;
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        return objPB.getDyncPgMainPB();
    }

    public pagereference DynamicButtonAction() {
        system.debug('strActionId==>' + strActionId);
        
        if(IsError){
            
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Cannot Proceed to Next Step before Saving this Record'));
                return null;
        }
        
        if(obSR.Date_of_Resolution__c == null){
              Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Date of Resolution is Required!'));
                return null;
        }
        
         if(obSR.Date_of_Resolution__c > Date.Today()){
              Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Date of Resolution is not Greater than Today!'));
                return null;
        }
        
        /*if (obSR.legal_structures__c != 'LTD SPC' &&obSR.legal_structures__c != 'LTD IC' && totalAuthorized < 50000) {
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'The total amount of ' + SRtype + ' must be greater than or equal to $50,000!'));
                return null;
        }*/
        if (obSR.legal_structures__c == 'LTD SPC' && totalAuthorized < 100) {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'The total amount of ' + SRtype + ' must be greater than or equal to $100!'));
            return null;
        }

        if (strActionId != null && strActionId != '') {
            boolean isNext = false;
            boolean isAllowToproceed = false;
            for (Section_Detail__c objSec: [select id, Name, Component_Label__c, Navigation_Direction__c from Section_Detail__c where Id = :strActionId]) {
                if (objSec.Navigation_Direction__c == 'Forward') {
                    isNext = true;
                } else {
                    isNext = false;
                }
            }
            if (isNext == true) {
                for (Account_Share_Detail__c acc: [select id from Account_Share_Detail__c where Account__c = :obSR.Customer__c AND Status__c != 'Inactive']) {
                    isAllowToproceed = true;
                }

            } else {
                isAllowToproceed = true;
            }
            if (isAllowToproceed == true) {
                PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
                PreparePageBlockUtil.PageId = mapParameters.get('PageId');
                PreparePageBlockUtil.objSR = objSRFlow;
                PreparePageBlockUtil.ActionId = strActionId;

                PreparePageBlockUtil objPB = new PreparePageBlockUtil();
                pagereference pg = objPB.getButtonAction();
                system.debug('pg==>' + pg);
                //update SR fields
                update obSR;
                return pg;
            } else {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Please add atleast one class detail to proceed.'));
                return null;
            }
        }
        return null;
    }

    public void Save_ShareDetail() {
        
        
        IsError = false;
        SRId = ApexPages.currentPage().getParameters().get('Id');
        decimal oldShares;
        Service_Request__c serviceReq = [Select id, finalizeAmendmentFlg__c From Service_Request__c where id = :SRId limit 1];
        obSR.finalizeAmendmentFlg__c = serviceReq.finalizeAmendmentFlg__c;
        system.debug('======>>>>> ' + SRId + '  SR:   ' + serviceReq.finalizeAmendmentFlg__c);
        //if(!obSR.finalizeAmendmentFlg__c ){ // Commented for V1.3
        
        if (!obSR.finalizeAmendmentFlg__c || (obSR.finalizeAmendmentFlg__c && checkFrmDetail == '1')) { // V1.3
            /*if (obSR.Share_Capital_Membership_Interest__c == NULL || obSR.currency_list__c == null || obSR.currency_list__c == '') {
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Please fill in the required fields below!'));
                return;
            }
            else {*/

            sumShares = 0;
            oldShares = totalAuthorized;
            totalAuthorized = 0;
            List<Amendment__c> lstAccountShareDetail = new List<Amendment__c>();
            
           
           
            Map < String,Amendment__c > m1 = new Map < String,Amendment__c > ();
            for (AccountShareDetailsWrapper acclist: lstAccountWrapper) {
               
                if (!m1.containsKey(acclist.accDetail.Class_Name__c.ToUpperCase())) {
                    m1.put(acclist.accDetail.Class_Name__c.ToUpperCase(), acclist.accDetail);
                }
                else {
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Duplicate class names found!'));
                     totalAuthorized = oldShares ;
                    IsError = true;
                    return;
                }
                
                if(SetofExistingClass.contains(acclist.accDetail.Class_Name__c.ToUpperCase())) {
                    system.debug('$$$$$$$' + SetofExistingClass);
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'You cannot re-use removed class Name'));
                    totalAuthorized = oldShares ;
                    IsError = true;
                    return;
                }
            }
            
            
            for (AccountShareDetailsWrapper acclist: lstAccountWrapper) {

               /* if (accShare[i].Name == NULL || accShare[i].Sys_Proposed_Nominal_Value__c == NULL || accShare[i].Sys_Proposed_No_of_Shares_per_Class__c == NULL || accShare[i].Sys_Proposed_Nominal_Value__c == 0 || accShare[i].Sys_Proposed_No_of_Shares_per_Class__c == 0) {
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Please fill in the required fields below!'));
                    return;

                }*/
                if (acclist.accDetail.Sys_Proposed_No_of_Shares_per_Class__c < acclist.accDetail.Total_Issued__c) { //V1.2 - Code for Existing Active Class

                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'The no. of shares for Class ' + acclist.accDetail.Class_Name__c + ' cannot be less than issued shares!'));
                    totalAuthorized = oldShares ;
                    IsError = true;
                    return;

                }  
                if (checkFrmDetail == '1' && objSRFlow.Record_type_Name__c == 'Application_of_Registration' && objSRFlow.isClosedStatus__c == false && acclist.accDetail.Total_Issued__c != null && acclist.accDetail.Sys_Proposed_No_of_Shares_per_Class__c < acclist.accDetail.Total_Issued__c) { // V1.3
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'The no. of shares for Class ' + acclist.accDetail.Class_Name__c + ' cannot be less than issued shares!'));
                    totalAuthorized = oldShares ;
                    IsError = true;
                    return;

                } 
                
                accList.accDetail.Sys_Proposed_Nominal_Value__c = decimal.valueOf(acclist.accDetail.Sys_Proposed_Nominal_Text__c);
                accList.ProposedAuthorized = (acclist.accDetail.Sys_Proposed_No_of_Shares_per_Class__c * decimal.valueOf(acclist.accDetail.Sys_Proposed_Nominal_Text__c));
                //accList.ProposedAuthorized *=  mapCurrencyRates.get(obSR.currency__r.Name)
                //productShares = acclist.accDetail.Sys_Proposed_Nominal_Value__c * acclist.accDetail.Sys_Proposed_No_of_Shares_per_Class__c;
                sumShares = sumShares + acclist.accDetail.Sys_Proposed_No_of_Shares_per_Class__c;
                totalAuthorized = totalAuthorized + accList.ProposedAuthorized;
                acclist.accDetail.Currency__c = obSR.Currency_Rate__c;
                system.debug('@@@' + obSR.Currency_Rate__c);
                lstAccountShareDetail.add(acclist.accDetail);
                
                
            accList.IsEdit = false; 
                
            }

          
            
            
 
            
            obSR.No_of_Authorized_Share_Membership_Int__c = sumShares;
            obSR.Total_Proposed_Authorized_Capital__c = totalAuthorized;
            
            
            if(!lstAccountShareDetail.isEmpty()){
                
                try{
                    update obSR;
                    upsert lstAccountShareDetail;
                }
                catch(Exception ex){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,ex.getMessage()));
                    return;
                }
            }
                
            //upsert accShare;
          //  createHistory();
           
            isError = false;
            //if(RowId != NULL)   accShare.get(RowId).IsEdit = false;                
            //Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.CONFIRM,SRtype+' details are saved sucessfully!'));
            return;
        }
           
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Your request has been finalised and cannot be updated.'));
        return;
    }
    
  
    public void cancelRow(){
        
        IsError = false;
        if(lstAccountWrapper.get(RowId).accDetail.ID == NULL){
            lstAccountWrapper.remove(RowId);
            return;
        }
        lstAccountWrapper.get(RowId).IsEdit = false;
        lstAccountWrapper.get(RowId).accDetail.Class_Name__c = Old_ClassName;
        lstAccountWrapper.get(RowId).accDetail.Sys_Proposed_Nominal_Value__c = Old_NominalValue;
        lstAccountWrapper.get(RowId).accDetail.Sys_Proposed_No_of_Shares_per_Class__c = Old_SharesValue;
        
        //ResetRecord();
        
    }
    
    public void EditRow(){
        
        lstAccountWrapper.get(RowId).IsEdit = true;
        Old_ClassName = lstAccountWrapper.get(RowId).accDetail.Class_Name__c;
        Old_NominalValue = lstAccountWrapper.get(RowId).accDetail.Sys_Proposed_Nominal_Value__c;
        Old_SharesValue = lstAccountWrapper.get(RowId).accDetail.Sys_Proposed_No_of_Shares_per_Class__c;
        isError = true;
        
    }
    
    public void delRow() {
        if (obSR.finalizeAmendmentFlg__c) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Your request has been finalised and cannot be updated.'));
            return;
        }
            
        if (lstAccountWrapper.size() == 1) {
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Atleast one class must be defined!'));
            return;
        }
        
        if(lstAccountWrapper[RowId].accDetail.Id == null)    {
            IsError = false;
            lstAccountWrapper.remove(RowId);
            
            return;
        }
        
        if(!lstAccountWrapper[RowId].accDetail.sys_create__c && lstAccountWrapper[RowId].accDetail.Id != null)    {
           
            delete lstAccountWrapper.get(RowId).accDetail;
            lstAccountWrapper.remove(RowId);
            IsError = false;
            calcShares();
            return;
        }
        
        
        
        
        List < Amendment__c > acc = [Select id,Class_Name__c,AccountShareHolderDetail__c,Status__c from Amendment__c where id = :lstAccountWrapper[RowId].accDetail.id
                                                AND Total_Issued__c = 0 AND sys_create__c = true];
        
        if(acc.isEmpty()){
            
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Cannot remove this class as shares/membership interest are allotted against it. Please contact portal@difc.ae for further assistance. '));
            return;
            
        }
        
        acc[0].Status__c = 'Removed';
        if(acc[0].AccountShareHolderDetail__c != NULL && acc[0].AccountShareHolderDetail__c != ''){
            SetofExistingClass.add(acc[0].Class_Name__c.ToUpperCase());
        }
        update acc;
        IsError = false;
        lstAccountWrapper.remove(RowId);
        calcShares();
    
    }
    public void addRow() {
        
        system.debug(obSR.finalizeAmendmentFlg__c);
        if (!obSR.finalizeAmendmentFlg__c) {
            
            Amendment__c amnd = new Amendment__c();
            amnd.Total_Issued__c = 0;
            amnd.ServiceRequest__c = SRId;
            amnd.Is_History__c = false;
            amnd.Status__c = 'Active';
            amnd.sys_create__c = false;
            amnd.RecordTypeID = RecordTypeID;
            lstAccountWrapper.add(new AccountShareDetailsWrapper(amnd,'',true , 0));
            isError = true;
            
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Your request has been finalised and cannot be updated.'));
            return;
        }
    }

    public string strNavigatePageId {
        get;
        set;
    }
    public pagereference goTopage() {
        if (strNavigatePageId != null && strNavigatePageId != '') {
            PreparePageBlockUtil objSidebarRef = new PreparePageBlockUtil();
            PreparePageBlockUtil.strSideBarPageId = strNavigatePageId;
            PreparePageBlockUtil.objSR = objSRFlow;
            return objSidebarRef.getSideBarReference();
        }
        return null;
    }

    public void calcShares() {
        
        totalAuthorized = 0;
        
        
        for (AccountShareDetailsWrapper acclist: lstAccountWrapper) {

               /* if (accShare[i].Name == NULL || accShare[i].Sys_Proposed_Nominal_Value__c == NULL || accShare[i].Sys_Proposed_No_of_Shares_per_Class__c == NULL || accShare[i].Sys_Proposed_Nominal_Value__c == 0 || accShare[i].Sys_Proposed_No_of_Shares_per_Class__c == 0) {
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Please fill in the required fields below!'));
                    return;

                }*/
            
            if(acclist.accDetail.Sys_Proposed_No_of_Shares_per_Class__c != NULL && acclist.accDetail.Sys_Proposed_Nominal_Text__c != ''){            
                accList.ProposedAuthorized = (acclist.accDetail.Sys_Proposed_No_of_Shares_per_Class__c * decimal.valueOf(acclist.accDetail.Sys_Proposed_Nominal_Text__c));
                //productShares = acclist.accDetail.Sys_Proposed_Nominal_Value__c * acclist.accDetail.Sys_Proposed_No_of_Shares_per_Class__c;
                sumShares = sumShares + acclist.accDetail.Sys_Proposed_No_of_Shares_per_Class__c;
                totalAuthorized = totalAuthorized + accList.ProposedAuthorized;
                acclist.accDetail.Currency__c = obSR.Currency_Rate__c;
            }
            //lstAccountShareDetail.add(acclist.accDetail);
               
        }
        
        obSR.Total_Proposed_Authorized_Capital__c = totalAuthorized;


    }

    public pagereference BackToServiceRequest() {
        pagereference pg;
        if (obSR.id != null) pg = new pagereference('/' + obSR.id);

        return pg;
    }
    
    public class AccountShareDetailsWrapper{
        
        public Amendment__c accDetail {get;set;}
        
        public boolean IsEdit {get;set;}
        public decimal ProposedAuthorized {get;set;}
        public string oldKey {get;set;}
        public string oldData {get;set;}
        
        
        public AccountShareDetailsWrapper(Amendment__c aAccDetail,string aOldKey, boolean aIsEdit, decimal aPropAuth){
            accDetail = aAccDetail;
            IsEdit = aIsEdit;
            oldKey = aOldKey;
            ProposedAuthorized = aPropAuth;
            
        }
    }
    
  }