/******************************************************************************************
 *  Author      : Claude Manahan
 *  Company     : NSI DMCC 
 *  Date        : 2016-07-08      
 *  Description : Validation Template for Contractor Forms
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By        Description
 ----------------------------------------------------------------------------------------            
 V1.0   08-08-2016      Claude            Created
 V1.1   19-10-2016      Claude            Added new constant for limiting queries
 V1.2   24-10-2016      Claude            Added functionality to render company search panel (#3490)
 V1.3   26-10-2016      Claude            Added functionality to change display of event organizer portal user details (#3490)
 V1.4   05-12-2016      Claude            Added field for contractor
 *******************************************************************************************/
public without sharing abstract class Fo_cls_ContractorFormValidation extends Fo_cls_AbstractFormController {
    
    /**
     * Checks if the details entered 
     * by the user matches the contact
     * record of the contractor account
     */
    @TestVisible protected Boolean isContractMatch;
    
    /**
     * Default limit for number of records to be retrieved
     */
    public static final Integer FO_DEFAULT_QUERY_COUNT = 500;
    
    //V1.3 - Claude - New Variables
    /**
     * Tells if the organizer portal user details
     * will be editable
     */ 
    public Boolean isOrganizerEditable                          {get; set;}
    //V1.3 - Claude - End
    
    /**
     * This will store the accounts in a options list
     */
    public List<SelectOption> contractAccounts                  { get; private set; }
    
    /**
     * Stores the selected contractor           
     */
    public String contractorId                                  { get; set; }
    
    /**
     * Id-key map for the list of contractors
     */
    @TestVisible protected Map<Id,Account> contractorMap;
    
    /**
     * Stores the built query string
     * for organizers
     */
    @TestVisible private String portalUserQueryStr;
    
    /**
     * Checks whether the contracto account
     * has issues
     */
    @TestVisible protected Boolean hasContractorError; 
    
    /**
     * The list of contractors that matches
     * the request details
     */
    @TestVisible protected List<Account> contractorAccount;
    
    /** V1.2 - Claude
     * This will store the contactID of the selected 
     * contractor/customer 
     */
    public String contactIdSelected                     {get; set;}
    
    /**
     * Checks if any of the contractor fields are blank
     * @return      hasNoContractorDetails      flag that tells if any of the contractor fields are blank
     */
    protected abstract Boolean hasNoContractorDetails();
    
    /**
     * Checks if all the required portal fields are blank
     * @return      hasNoPortalDetails      flag that tells if any of the portal fields are blank
     */
    protected abstract Boolean hasNoPortalDetails();
    
    /**
     * Checks if any of the fields are blank
     * @return      hasMissingFields        flag that tells if any of the fields are blank
     */
    protected abstract Boolean hasMissingFields();
    
    /**
     * Executes preliminary actions
     */
    protected override abstract void init();
    
    /**
     * Initializes the class members 
     */
    protected override abstract void initialize();
    
    /**
     * Sets the contractor list
     */
    private void setContractorsList(){
        
        System.debug('Building Contractor List: ');
        
        /* Initialize Contractor Map */
        contractorMap = new Map<Id,Account>();
        
        // Query the accounts
        contractorMap = new Map<Id,Account>([SELECT ID, 
                                                      E_mail__c,
                                                      Blacklist_Point_3_months__c,
                                                      Blacklist_Point_1_year__c,
                                                      Blacklist_Point_6_months__c,
                                                      Action_1_year__c,   // V1.4 - Claude - Added fields
                                                      Action_3_Months__c, // V1.4 - Claude - Added fields
                                                      Action_6_Months__c, // V1.4 - Claude - Added fields
                                                      Phone,
                                                      Fax,
                                                      PO_Box__c,
                                                      Work_Phone__c,
                                                      Contractor_License_Expiry_Date__c,
                                                      Registered_Address__c,
                                                      Issuing_Authority__c,
                                                      RORP_License_No__c,
                                                      Name, 
                                                      (SELECT id, email, Passport_No__c, Nationality__c FROM Contacts) FROM Account 
                                                      WHERE RecordType.DeveloperName = 'Contractor_Account' AND
                                                      Is_Registered__c = true
                                                      ORDER BY Name]);
        
        // Get the values
        List<Account> contractorAccountList = contractorMap.values(); 
        
        if(!contractorAccountList.isEmpty()){
            
            /* Fill the list */
            for(Account a : contractorAccountList){
                 contractAccounts.add(new SelectOption(a.Id,a.Name));    
            }
            
            System.debug('Contractor List Complete >> ' + contractAccounts);
            
        }
    }
    
    /**
     * Sets the Contractor details in the request
     */
    public void setSelectedContractor(){
        
        System.debug('Contractor ID: ' + contractorId);
        
        Boolean hasContractorValue = String.isNotBlank(contractorId);
        
        //Account contractorRecord = hasContractorValue ? contractorMap.get(contractorId) : null;
        
        Account contractorRecord = hasContractorValue ? [SELECT ID, 
                                                      E_mail__c,
                                                      Blacklist_Point_3_months__c,
                                                      Blacklist_Point_1_year__c,
                                                      Blacklist_Point_6_months__c,
                                                      Action_1_year__c,   // V1.4 - Claude - Added fields
                                                      Action_3_Months__c, // V1.4 - Claude - Added fields
                                                      Action_6_Months__c, // V1.4 - Claude - Added fields
                                                      Phone,
                                                      Fax,
                                                      PO_Box__c,
                                                      Work_Phone__c,
                                                      Contractor_License_Expiry_Date__c,
                                                      Registered_Address__c,
                                                      Issuing_Authority__c,
                                                      RORP_License_No__c,
                                                      Name, 
                                                      (SELECT id, email, Passport_No__c, Nationality__c FROM Contacts) FROM Account 
                                                      WHERE RecordType.DeveloperName = 'Contractor_Account' AND
                                                      Is_Registered__c = true
                                                      and Id =:contractorId] : null;
        
        srObj.Name_of_the_Authority__c = hasContractorValue ? contractorRecord.Issuing_Authority__c : '';
        srObj.Mobile_Number__c = hasContractorValue ? contractorRecord.phone : '';
        srObj.PO_BOX__c = hasContractorValue ? contractorRecord.PO_Box__c : '';
        
        if(formType == FormTypes.EVENT){
            /* Get the values for Events */
            System.debug('Event Form'); 
            srObj.Registered_Address__c = hasContractorValue ? contractorRecord.Registered_Address__c : '';
            srObj.Sponsor_Establishment_No__c = hasContractorValue ? contractorRecord.Name : '';
            srObj.Sponsor_Visa_No__c = hasContractorValue ? contractorRecord.RORP_License_No__c : '';
            srObj.Sponsor_P_O_Box__c = hasContractorValue ? contractorRecord.PO_Box__c : '';
            srObj.Sponsor_Street__c = hasContractorValue ? contractorRecord.Fax : ''; 
            srObj.Approving_Authority__c = hasContractorValue ? contractorRecord.Issuing_Authority__c : '';
            srObj.Sponsor_Visa_Expiry_Date__c = hasContractorValue ? contractorRecord.Contractor_License_Expiry_Date__c : null;
            
            isContractMatch = false;
            
            /* V1.1 - Claude - Set the contact ID */
            if(!contractorMap.get(contractorId).contacts.isEmpty()){
                
                for(Contact c : contractorMap.get(contractorId).contacts){
                    
                    if(String.isNotBlank(c.email) && 
                            c.Email.equals(srObj.Email_Address__c) && 
                            String.isNotBlank(c.Nationality__c) && 
                            c.Nationality__c.equals(srObj.Sponsor_Nationality__c) &&
                            String.isNotBlank(c.Passport_No__c) && 
                            c.Passport_No__c.equals(srObj.Sponsor_Passport_No__c)){
                        isContractMatch = true;
                    }
                }
                
            } 
            
        } else {
            /* Get the values for Fit-out */
            System.debug('Fit-out Form');
            //srObj.Email_Address__c = hasContractorValue ? contractorRecord.E_mail__c : '';
            srObj.Address_Details__c = hasContractorValue ? contractorRecord.Registered_Address__c : '';
            srObj.Company_Name__c = hasContractorValue ? contractorRecord.Name : '';
            srObj.Trade_License_No__c = hasContractorValue ? contractorRecord.RORP_License_No__c : '';
            srObj.Expiry_Date__c = hasContractorValue ? contractorRecord.Contractor_License_Expiry_Date__c : null;
            
            if(contractorRecord != null && !contractorRecord.contacts.isEmpty()){
               //contactIdSelected = contractorRecord.contacts[0].Id;            
               
               /* Check the contacts if any of them are active */
               setActiveContractorId(contractorRecord.contacts);
               
            } else {
                System.debug('Clearing portal user details.');
                clearPortalUserDetails(false);
            }

        }
        
        
    }
    
    private void setActiveContractorId(List<Contact> contactRecords){
     
        Set<String> contactRecordIds = new Set<String>();
        
        for(Contact c : contactRecords){
            contactRecordIds.add(c.Id);
        }
        
        List<User> activeUsers = [SELECT Id, IsActive, ContactId FROM User WHERE ContactId in : contactRecordIds AND isActive = true];
        
        for(User u : activeUsers){
            if(u.IsActive){
                contactIdSelected = u.ContactId;
                break;
            }
        }
    }
    
    /**
     * Checks if there is any contractor match
     * @return          hasContractorMatch          Checks if there is any match
     */
    protected Boolean hasContractorMatch(){
        return isContractMatch;
    }
    
    /**
     * Clears the portal user details
     * if not found
     * @params          isOrganizer         Checks if the portal user details are coming from an organizer account
     */
    @TestVisible protected void clearPortalUserDetails(Boolean isOrganizer){
    
        if(isOrganizer){
                
            // Organizer Portal Details
            srObj.Title__c = '';
            srObj.Email__c = '';
            srObj.First_Name__c = '';
            srObj.Last_Name__c ='';
            srObj.Mobile_Number__c = '';
            srObj.Work_Phone__c = '';
            srObj.Position__c = '';                    
            srObj.Passport_Number__c = '';    
            srObj.Nationality_list__c = '';
            
        } else {
            
            // Contractor Portal Details
            srObj.Title_Sponsor__c = '';
            srObj.Email_Address__c = '';
            srObj.Sponsor_First_Name__c = '';
            srObj.Sponsor_Mobile_No__c = '';
            srObj.Sponsor_Last_Name__c = '';
            srObj.Sponsor_Office_Telephone__c = '';
            srObj.Sponsor_Middle_Name__c = '';  
            srObj.Sponsor_Passport_No__c = '';  
            srObj.Sponsor_Nationality__c = '';
        }
        
    }
    
    /**
     * Gets the related cotnractor based
     * upon the license number and name
     */
    protected void getRelatedContractor(){
        
        String contractorName = formType == FormTypes.EVENT ? srObj.Sponsor_Establishment_No__c : srObj.Company_Name__c;
        String licenseNo = formType == FormTypes.EVENT ? srObj.Sponsor_Visa_No__c : srObj.Trade_License_No__c; 
        
        contractorAccount = new List<Account>([SELECT Id,
                                                      Is_Registered__c,
                                                      Action_1_year__c,
                                                      Action_3_Months__c,
                                                      Action_6_Months__c,
                                                      Name, (SELECT Id, Nationality__c, Passport_No__c from Contacts) FROM 
                                                      Account WHERE 
                                                        RecordType.DeveloperName = 'Contractor_Account' 
                                                        AND Name =:contractorName 
                                                        AND RORP_License_No__c =: licenseNo]);
            
        hasContractorError = hasNoContractor() || hasDuplicateContractor();
    }
    
    /**
     * Tells if no contractor was
     * found based on the entered details
     */
    protected Boolean hasNoContractor(){
        return contractorAccount.isEmpty();
    }
    
    /**
     * Tells if there are more 
     * than 1 contractor found
     */
    @TestVisible protected Boolean hasDuplicateContractor(){
        return contractorAccount.size() > 1;
    }
    
    /**
     * Tells if the contractor
     * already exists
     */
    @TestVisible protected Boolean contractorExists(){
        return !contractorAccount.isEmpty() && contractorAccount[0].Is_Registered__c;
    }
    
    /**
     * Initializes the contractor list
     */
    protected void initializeContractorList(){
        
        contractAccounts = new List<SelectOption>();
        contractAccounts.add(new SelectOption('','--None--'));
    }
    
    /**
     * V1.1 - Claude
     * Sets the portal user details in the
     * request form
     * @params      isOrganizer     Checks if the portal user details are coming from an organizer account
     */
    @TestVisible protected void setPortalUserDetails(Boolean isOrganizer){
    
        if(String.isNotblank(contactIdSelected)){
        
            Set<String> criteriaSet = new Set<String>{'Community_User_Role__c = \'Fit-Out Services\' ', 
                                                        'ContactId = \'' + contactIdSelected + '\' ',
                                                        'IsActive = true'};
            
            Set<String> portalUserFields = new Set<String>{'Contact.firstname','Contact.lastname','Contact.email','Contact.Salutation',
                                                        'title','isActive','phone','Contact.Nationality__c','Contact.MobilePhone','Contact.Passport_No__c'};
            
            String addlCondition = isOrganizer ? ' isClient__c = true ' : '';
            
            /* Set the query fields */
            setPortalUserQueryStr(portalUserFields,criteriaSet);
            
            /* Query the User based on the selected contact Id */
            List<User> portalUserDetails = Database.query(portalUserQueryStr);
            
            clearPortalUserDetails(formType == FormTypes.FITOUT);
                                             
            if(!portalUSerDetails.isEmpty()){
            
                System.debug('Portal User found.' + portalUserDetails[0]);
                System.debug('Email.' + portalUserDetails[0].contact.email);
                
                if(isOrganizer){
                    
                    // Organizer Portal Details
                    srObj.Title__c = portalUserDetails[0].Contact.Salutation;
                    srObj.Email__c = portalUserDetails[0].Contact.email;
                    srObj.First_Name__c = portalUserDetails[0].Contact.firstname;
                    srObj.Last_Name__c = portalUserDetails[0].Contact.lastname;
                    srObj.Mobile_Number__c = portalUserDetails[0].Contact.MobilePhone;
                    srObj.Work_Phone__c = portalUserDetails[0].phone;
                    srObj.Position__c = portalUserDetails[0].Title;                    
                    srObj.Passport_Number__c = portalUserDetails[0].contact.Passport_No__c;    
                    srObj.Nationality_list__c = portalUSerDetails[0].Contact.Nationality__c;
                    
                } else {
                    
                    // Contractor Portal Details
                    srObj.Title_Sponsor__c = portalUserDetails[0].Contact.Salutation;
                    srObj.Email_Address__c = portalUserDetails[0].Contact.email;
                    srObj.Sponsor_First_Name__c = portalUserDetails[0].Contact.firstname;
                    srObj.Sponsor_Mobile_No__c = portalUserDetails[0].Contact.MobilePhone;
                    srObj.Sponsor_Last_Name__c = portalUserDetails[0].Contact.lastname;
                    srObj.Sponsor_Office_Telephone__c = portalUserDetails[0].phone;
                    srObj.Sponsor_Middle_Name__c = portalUserDetails[0].Title;  
                    srObj.Sponsor_Passport_No__c = portalUserDetails[0].contact.Passport_No__c;  
                    srObj.Sponsor_Nationality__c = portalUSerDetails[0].Contact.Nationality__c;
                    
                }
                
            } else {
                
                /*
                 * Allow the users to enter the details
                 */
                isOrganizerEditable = true;
                
            }
        } 
    }
    
    /**
     * Sets the query string for
     * the organizer
     * @params      organizerFields         Set of fields to be used for the query
     * @params      criteriaSet             Set of criteria for the query
     */
    protected void setPortalUserQueryStr(Set<String> organizerFields, Set<String> criteriaSet){
        
        portalUserQueryStr = 'SELECT ';
        
        for(String s : organizerFields){
            
            portalUserQueryStr += ' ' + s.replaceAll(',','') + ', ';
            
        }
        
        /* Remove the last comma */
        portalUserQueryStr = portalUserQueryStr.subString(0,portalUserQueryStr.lastIndexOf(','));
        
        portalUserQueryStr += ' FROM User ';
        
        if(criteriaSet != null && !criteriaSet.isEmpty()){
            
            portalUserQueryStr += ' WHERE '; // Start the criteria
            
            for(String s : criteriaSet){
                
                portalUserQueryStr += ' ' + s + ' AND ';
                
            }
            
            portalUserQueryStr = portalUserQueryStr.subString(0,portalUserQueryStr.lastIndexOf('AND'));
            
        }
        
    }
    
    /**
     * Class Initialization
     */
    {
        /*
         * Initialize Contractor list
         */
        initializeContractorList();
        
        /*
         * Populate the collection
         */
        setContractorsList();
        
        /* Initialize all class members */
        hasContractorError = false;
        
        isOrganizerEditable = false;
        
        portalUserQueryStr = '';
    }
    
}