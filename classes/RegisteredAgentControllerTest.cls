/*****************************************************************************************************************************
    Author      :   Sai Kalyan
    Date        :   02-June-2020
    Description :   Test class Registered Agent Controller
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    02-June-2020   Sai        Intial Version
*****************************************************************************************************************************/
@isTest

public with sharing class RegisteredAgentControllerTest {
  
    static testmethod void test1(){
        
        Account eachAccount = new Account();
        eachAccount.Name = 'Test Account';
        eachAccount.ROC_Status__c = 'Active';
        eachAccount.DFSA_Approval__c = 'Yes';
        INSERT eachAccount;
        
        Account eachAccount1 = new Account();
        eachAccount1.Name = 'Test Account1';
        eachAccount1.Apartment_Villa_Office_Number__c = 'Gate Building';
        eachAccount1.Country__c = 'India';
        eachAccount1.City__c = 'Hyderabad';
        eachAccount1.Street__c ='Test';
        eachAccount1.Building_Name__c ='DIFC';
        eachAccount1.Emirate_State__c = 'Dubai';
        eachAccount1.ROC_Status__c = 'Active';
        eachAccount1.DFSA_Approval__c = 'Yes';
        INSERT eachAccount1;
        
        License_Activity_Master__c newActivity = new License_Activity_Master__c();
        newActivity.name = 'Corporate Service Provider';
        INSERT newActivity;
        
        License_Activity__c eachActivity = new License_Activity__c();
        eachActivity.Account__c = eachAccount.Id;
        eachActivity.Activity__c = newActivity.ID;
        INSERT eachActivity;
        
        Contact eachContact = new Contact();
        eachContact.LastName = 'Test contact';
        eachContact.AccountID = eachAccount.ID;
        INSERT eachContact;
        Id p = [select id from profile where name=:Label.Portal_Profile_Name].id;
        User user = new User(alias = 'Tes', email='Testuser.123@Test.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = eachContact.Id,
                timezonesidkey='America/Los_Angeles', username='Testuser.123@Test.com');
       
        insert user;
        
        Relationship__c eachRelation = new Relationship__c();
        eachRelation.Subject_Account__c = eachAccount.ID;
        eachRelation.Relationship_Type__c = 'Registered agent';
        eachRelation.Active__c = true;
        eachRelation.Object_Account__c = eachAccount1.Id;
        INSERT eachRelation;
        RegisteredAgentController controller = new RegisteredAgentController();
        controller.selectedAgentValue = eachAccount.ID;
        system.runas(user){
            controller.existingAgents();
            controller.displayMessage();
            controller.removeAgents();
        }
        controller.SaveRecord();
    }
    
     static testmethod void test2(){
        
        Account eachAccount1 = new Account();
        eachAccount1.Name = 'Test Account1';
        eachAccount1.Apartment_Villa_Office_Number__c = 'Gate Building';
        eachAccount1.Country__c = 'India';
        eachAccount1.City__c = 'Hyderabad';
        eachAccount1.Street__c ='Test';
        eachAccount1.Building_Name__c ='DIFC';
        eachAccount1.Emirate_State__c = 'Dubai';
        INSERT eachAccount1;
        
        RegisteredAgentController controller = new RegisteredAgentController();
        controller.selectedAgentValue = eachAccount1.ID; 
        controller.SaveRecord();
     }
        
}