//Generated by wsdl2apex

public class WS_PublicRegistry {
    public class statsResponse_element {
        public String return_x;
        private String[] return_x_type_info = new String[]{'return','urn:hellowsdl',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:hellowsdl','false','false'};
        private String[] field_order_type_info = new String[]{'return_x'};
    }
    public class statsRequest_element {
        public String name;
        private String[] name_type_info = new String[]{'name','urn:hellowsdl',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:hellowsdl','false','false'};
        private String[] field_order_type_info = new String[]{'name'};
    }
    public class hellowsdlPort {
        public String endpoint_x = Label.PR_URL;
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'urn:hellowsdl', 'WS_PublicRegistry'};
        public String stats(String name) {
            WS_PublicRegistry.statsRequest_element request_x = new WS_PublicRegistry.statsRequest_element();
            request_x.name = name;
            WS_PublicRegistry.statsResponse_element response_x;
            Map<String, WS_PublicRegistry.statsResponse_element> response_map_x = new Map<String, WS_PublicRegistry.statsResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'urn:statswsdl#stats',
              'urn:hellowsdl',
              'statsRequest',
              'urn:hellowsdl',
              'statsResponse',
              'WS_PublicRegistry.statsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.return_x;
        }
    }
}