/**********************************************************************************************************************
* Name               : OB_NotifyClientsForCPOpenStepsBatch
* Description        : This batch class is used to send notifications to the client when the CP SR steps are not actioned within 3/5/10 working days
* Created Date       : 06-12-2020
* Created By         : PWC Digital Middle East
* --------------------------------------------------------------------------------------------------------------------*
* Version       Author                  Date             Comment
* 1.0           salma.rasheed@pwc.com   10-12-2020       Initial Draft
**********************************************************************************************************************/
global class OB_NotifyClientsForCPOpenStepsBatch implements Database.Batchable<sObject>,Schedulable  {

    /**********************************************************************************************
    * @Description  : Scheduler execute method to schedule code execution
    * @Params       : SchedulableContext        scheduling details
    * @Return       : none
    **********************************************************************************************/ 
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this,10);
    }

    /**********************************************************************************************
    * @Description  : Batch start method to define the context
    * @Params       : BatchableContext        batch context
    * @Return       : Query locator
    **********************************************************************************************/ 
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        // Get all the CP SR Steps assigned to client queue & still open
        String queryString = 'Select id,CreatedDate,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR_Step__r.OwnerId,HexaBPM__SR__r.HexaBPM__Contact__c from HexaBPM__Step__c where (HexaBPM__SR__r.HexaBPM__Record_Type_Name__c=\'Commercial_Permission\' OR HexaBPM__SR__r.HexaBPM__Record_Type_Name__c=\'Commercial_Permission_Renewal\') AND HexaBPM__Status_Type__c!=\'End\' AND HexaBPM__SR_Step__r.HexaBPM__Owner_Queue_Name__c=\'Client_Entry_User\'';
        system.debug('QueryString=>'+queryString);
        return Database.getQueryLocator(queryString);
    }

    /**********************************************************************************************
    * @Description  : Batch execute method to process the records in current scope
    * @Params       : scope     records in current scope
    * @Return       : none
    **********************************************************************************************/ 
    global void execute(Database.BatchableContext bc, List<HexaBPM__Step__c> scope){
        String tempContactEmail;
        Id tempAccountId;
        try{
            Map<Id,Id> queueBusinessHourMap = new Map<Id,Id>();
            for(OBBusinessHour__c objBusinessHour : [SELECT id,Business_Hour_Id__c,Queue_Id__c FROM OBBusinessHour__c]){
                queueBusinessHourMap.put(Id.valueOf(objBusinessHour.Queue_Id__c), Id.valueOf(objBusinessHour.Business_Hour_Id__c));
            }
            Map<Id,HexaBPM__Step__c> stepMap = new Map<Id,HexaBPM__Step__c>();
            for(HexaBPM__Step__c objStep : scope){
                Long ms = BusinessHours.diff(queueBusinessHourMap.get(objStep.HexaBPM__SR_Step__r.OwnerId), objStep.CreatedDate, System.Now());
                system.debug(ms);
                Decimal decMS = Decimal.valueOf(ms);
                system.debug('MS=>'+decMS);
                Decimal minutes= (((decMS)/1000)/60).setscale(1);
                system.debug('minutes=>'+minutes);
                Decimal hours = minutes/60;   
                system.debug('hours=>'+hours);
                Integer days = Integer.valueOf(hours/8);
                system.debug('days=>'+days);
                if(Test.isRunningTest())    days=3;
                if(days==Integer.valueOf(Label.OB_Days_3) || days==Integer.valueOf(Label.OB_Days_5) || days==Integer.valueOf(Label.OB_Days_7)){//Example - If current date is 10th, 3 days before is 7th
                    stepMap.put(objStep.Id,objStep);
                    tempContactEmail = objStep.HexaBPM__SR__r.HexaBPM__Email__c;
                    tempAccountId = objStep.HexaBPM__SR__r.HexaBPM__Customer__c;
                }
            }
            if(!stepMap.isEmpty()) {
                Id contactRTId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Contact').getRecordTypeId();
                Contact tempContact = new Contact(email = tempContactEmail, firstName = 'test', lastName = 'email',accountID=tempAccountId,RecordTypeId=contactRTId );
                insert tempContact;

                EmailTemplate[] emailTemplate = [Select Id from EmailTemplate where DeveloperName='OB_Open_Action_Items_Commercial_Permission' limit 1];
                OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];    
                list<Messaging.SingleEmailMessage> singleEmailMessageList = new list<Messaging.SingleEmailMessage>();
                for(Id stepId : stepMap.keyset()){  
                    String[] ToAddresses = new String[]{stepMap.get(stepId).HexaBPM__SR__r.HexaBPM__Email__c};
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setTargetObjectId(tempContact.Id);
                    mail.setToAddresses(ToAddresses);
                    mail.setSaveAsActivity(false);
                    mail.setWhatId(stepId);
                    if(lstOrgEA!=null && lstOrgEA.size()>0)
                    mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
                    if(emailTemplate!=null && emailTemplate.size()>0)
                    mail.setTemplateId(emailTemplate[0].Id);
                    singleEmailMessageList.add(mail);
                }
                system.debug('singleEmailMessageList==>'+singleEmailMessageList);
                Messaging.SendEmailResult[] results = Messaging.sendEmail(singleEmailMessageList);
                system.debug('results==>'+results);
                system.debug('Limits.getEmailInvocations()==>'+Limits.getEmailInvocations());
                delete tempContact;
            }
        }catch (Exception e){  Log__c objLog = new Log__c(Description__c = 'Exception on OB_NotifyClientsForCPOpenStepsBatch.execute() :'+e.getStackTraceString(),Type__c = 'Activate Deactivate CP');    insert objLog;}
    }

    /**********************************************************************************************
    * @Description  : Batch finish method
    * @Params       : BatchableContext  batch contect details
    * @Return       : none
    **********************************************************************************************/ 
    global void finish(Database.BatchableContext bc){}    
}