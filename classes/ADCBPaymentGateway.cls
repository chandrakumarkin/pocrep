public  class ADCBPaymentGateway
{

 public static Map<String,String> oPassedParams{get;set;}
 public static String secretKey = Label.ADCB_Portal_secretkey; 

 public void PushtoPaymentGetway(string reference_number,string amount)
    {
       
      
  oPassedParams = new Map<String,String>();
     
        
            oPassedParams.put('access_key',Label.ADCB_access_key);
            oPassedParams.put('profile_id',Label.ADCB_profile_id);
            oPassedParams.put('transaction_uuid',getUUID());
        
            oPassedParams.put('signed_field_names','access_key,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency');
        
            oPassedParams.put('unsigned_field_names','');
            oPassedParams.put('signed_date_time',getUTCDateTime());
            oPassedParams.put('locale','en');
            oPassedParams.put('transaction_type','sale');
            oPassedParams.put('reference_number',reference_number);
            oPassedParams.put('amount',amount);
            oPassedParams.put('currency','aed');
     

    }
     public String getSignedData(){
            String result = '';
            result += '\n <input type="hidden" id="signature" name="signature" value="' + sign(buildDataToSign(oPassedParams),secretKey) + '"/>';
            system.debug('-- getSignedData' + result);
            return result;
        }
        
        public String sign(String data, String secretKey) 
        {
            String result = EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(data), Blob.valueOf(secretKey)));
            return result;
        }
        
     public String getParametersValuesHidden(){
            String result = '';
            for (String oKey : oPassedParams.KeySet()){ 
                result += '\n <input type="hidden" id="' + oKey + '" name="' + oKey + '" value="' + oPassedParams.get(oKey) + '" />';
            }
            system.debug('--ParametersValuesHidden' + result);
            return result;
        }
    
      public String buildDataToSign(Map<string,string> paramsArray) 
      {
            String[] signedFieldNames = paramsArray.get('signed_field_names').Split(',');
            List<string> dataToSign = new List<string>();
    
            for (String oSignedFieldName : signedFieldNames){
                 dataToSign.Add(oSignedFieldName + '=' + paramsArray.get(oSignedFieldName));
            }
            return commaSeparate(dataToSign);
        }
          private String commaSeparate(List<string> dataToSign) {
            String result = '';
            for(String Str : dataToSign) {
                result += (result==''?'':',')+Str;
            }
            return result;                         
        }
        
    public String getUTCDateTime() 
    {
            DateTime oUTSDateTime = System.now().addHours(-4);
            String strUTCDateTime = oUTSDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
            return strUTCDateTime;
       } 
    
       public String getUUID(){
            Blob b = Crypto.generateAesKey(128);
            String h = EncodingUtil.convertToHex(b);
            String guid = h.substring(0,8) + '-' + h.substring(8,12) + '-' + h.substring(12,16) + '-' + h.substring(16,20) + '-' + h.substring(20);       
            return guid;
        }
        
}