/*
    Author      : Durga Prasad
    Date        : 18-Apr-2020
    Description : Application Invoice Page controller to show the price items
    ---------------------------------------------------------------------------------------
*/
public without sharing class OB_ApplicationInvoiceController {
    public string ApplicationId{get;set;}
    public boolean isNotValid;
    public list<HexaBPM__SR_Price_Item__c> lstSRPriceItems{get;set;}
    public OB_ApplicationInvoiceController(ApexPages.StandardController controller){
        lstSRPriceItems = new list<HexaBPM__SR_Price_Item__c>();
        if(apexpages.currentPage().getParameters().get('Id')!=null){
            ApplicationId = apexpages.currentPage().getParameters().get('Id');
            PrepareData();
        }
    }
    public void PrepareData(){
        for(HexaBPM__SR_Price_Item__c objSRP:[Select Id,Total_Price_AED__c,VAT_Amount__c,Pricing_Line_Name__c,VAT_Description__c,Difc_Margin_Amount__c,HexaBPM__Pricing_Line__r.Material_Code__c,HexaBPM__Status__c from HexaBPM__SR_Price_Item__c where HexaBPM__ServiceRequest__c=:ApplicationId and HexaBPM__Pricing_Line__r.Non_Deductible__c=false and Total_Price_AED__c>0]){
             
             if(objSRP.HexaBPM__Status__c =='Cancelled' && (objSRP.HexaBPM__Pricing_Line__r.Material_Code__c =='PSA' ||objSRP.HexaBPM__Pricing_Line__r.Material_Code__c =='PSAFintech')){
                isNotValid = true;
             }
             else{
             lstSRPriceItems.add(objSRP);
             }
        }
    }
}