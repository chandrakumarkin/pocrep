/******************************************************************************************
 *  Class Name  : NorBlocOTPValidationController
 *  Author      : Sai kalyan Sanisetty 
 *  Company     : DIFC
 *  Date        : 28 NOV 2019        
 *  Description :                   
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1    28 NOV 2019           Sai                Initial Version  
*******************************************************************************************/


public with sharing class NorBlocOTPValidationController {
    
    public static String OTPData{get;set;}
    
    
    @AuraEnabled
    public static String getotpValidation(String OTP){
    	String Status = '';  
    	
    	try{
    		
	    	User eachUser = new User();
	    	eachUser = [SELECT Id,Email,Contact.AccountID,Contact.Email FROM User where ID=:userinfo.getUserId()];
	    	Http http = new Http();
			HttpRequest userAccessToken = new HttpRequest();
			userAccessToken.setEndpoint(Label.NorBlocOTPEndPoint);
			userAccessToken.setMethod('POST');
			userAccessToken.setHeader('Authorization', Label.NorBlocAuthorization);
	        userAccessToken.setHeader('Content-Type', 'application/x-www-form-urlencoded');
	        userAccessToken.setHeader('Content-Length', '0');
	        String requestBody = 'email='+EncodingUtil.urlEncode(eachUser.Contact.Email, 'UTF-8')+'&code='+OTP+'&grant_type=authorization_code&scope=customer.manage';
	        userAccessToken.setBody(requestBody);  
	        HttpResponse authorizationresponse = http.send(userAccessToken);
	        if(authorizationresponse.getStatusCode() == 200 || authorizationresponse.getStatusCode() == 204){
	            	
	            	Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(authorizationresponse.getBody() );
	                Object BearerCode1 = results.get('access_token');
	                String BearerCode = 'Bearer '+String.valueOf(BearerCode1);
	                Http httpImportCustomer = new Http();
	                HttpRequest importCustomerRequest = new HttpRequest();
	                importCustomerRequest.setEndpoint(Label.NorBlockCreateNewLeadEndPoint);
		    		importCustomerRequest.setMethod('POST');
		    		importCustomerRequest.setHeader('Authorization',BearerCode);
		            importCustomerRequest.setHeader('Content-Type','application/json');
		            importCustomerRequest.setTimeout(120000);
		            String importCustomerData = NorBlockNewLeadController.finalJSON(eachUser.Contact.AccountID);
		            importCustomerRequest.setBody(importCustomerData);
		            HttpResponse importCustomerresponse = http.send(importCustomerRequest);
		            if(importCustomerresponse.getStatusCode() == 200 || importCustomerresponse.getStatusCode() == 204){
		            	
		            	HexaBPM__Step__c eachStep = new HexaBPM__Step__c();
		            	eachStep = [SELECT Id,HexaBPM__Status__c,HexaBPM__SR__c,HexaBPM__SR_Step__c FROM HexaBPM__Step__c where HexaBPM__SR__r.HexaBPM__Customer__c =:eachUser.Contact.AccountID AND SR_Recordtype_Name__c='Open_a_Bank_Account' limit 1];
		            	CC_AutoCloseStep closeStep = new CC_AutoCloseStep();
		            	closeStep.EvaluateCustomCode(null,eachStep);
		            	
		            }
		            Status ='Success';
		            
	        }	
	        else if(authorizationresponse.getStatusCode() == 403){
	        	Status ='error';
	        }
	       
	    }
	   catch(Exception ex){
	   		
	    	system.debug('@@####'+ex.getmessage());
	   }
	   return status;
    }
    
    @AuraEnabled
    
    public static void sendOTPAgain(){
    	
    	User eachUser = new User();
	    eachUser = [SELECT Id,Email,Contact.AccountID,Contact.Email FROM User where ID=:userinfo.getUserId()];
	    if(eachUser.Contact.Email !=null){
	    	
	    	Http httpOTP = new Http();
    		HttpRequest authorizationRequestOTP = new HttpRequest();
    		authorizationRequestOTP.setEndpoint(Label.SendOTPEndpoint);
    		authorizationRequestOTP.setMethod('POST');
    		authorizationRequestOTP.setHeader('Authorization',Label.NorBlocAuthorization);
            authorizationRequestOTP.setHeader('Content-Type', 'application/x-www-form-urlencoded');
            String requestBodyOTP = 'grant_type=client_credentials&email='+EncodingUtil.urlEncode(eachUser.Contact.Email, 'UTF-8');
            authorizationRequestOTP.setBody(requestBodyOTP); 
            authorizationRequestOTP.setTimeout(120000);
            HttpResponse authorizationresponseOTP = httpOTP.send(authorizationRequestOTP);
	    }
	    	
    }
    
    public static void dummytest(){
        integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
    
}