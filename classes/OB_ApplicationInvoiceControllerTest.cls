/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OB_ApplicationInvoiceControllerTest {
    static testMethod void myUnitTest() {
        HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c();
        objSR.First_Name__c = 'Test';
        objSR.Last_Name__c = 'SR';
        insert objSR;
        
        //HexaBPM__Pricing_Line__c PL = new HexaBPM__Pricing_Line__c();
        
        HexaBPM__SR_Price_Item__c SRP = new HexaBPM__SR_Price_Item__c();
        SRP.HexaBPM__ServiceRequest__c = objSR.Id;
        SRP.HexaBPM__Price__c = 100;
        SRP.HexaBPM__Sys_Added_through_Code__c = true;
        SRP.HexaBPM__Non_Reevaluate__c = true;
        insert SRP;
        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(objSR);
        ApexPages.currentPage().getParameters().put('Id',objSR.Id);
        OB_ApplicationInvoiceController InvCtrl = new OB_ApplicationInvoiceController(sc);
        
    }
}