/******************************************************************************************
 *  Name        : Cls_SearchAccountsExtension 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2017-7-13
 *  Description : Extension Class for searching accounts
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   13-07-2016   Claude        Created (#4078)
 V1.1   30-05-2021   Suchita       Modified(#16300)
*******************************************************************************************/
public virtual class Cls_SearchAccountsExtension {
    
    public String currentAccountId                      {get;set;}
    public String HostingCompanyName                    {get;set;}
    
    public String accountStatus                         {get;set;}
    
    public Boolean searchWithLicenseNo                  {get;set;}
    public Boolean isSameClient                         {get;set;}
    
    public list<HostingWrapper> HostingCompanies        {get;set;}
    public String accountSubLegalStructure{get;set;} //1.1
    public Boolean searchWithSubLegalStructure{get;set;} //1.1
    public Cls_SearchAccountsExtension(){
        
        searchWithLicenseNo = false;
        isSameClient = false;
        
    }
    
    public void clearList(){
        
        HostingCompanies.clear();
        
    }
    
    public virtual void SearchHostingCompanies(){
        
        HostingCompanies = new list<HostingWrapper>();
        
        HostingCompanyName = String.isNotBlank(HostingCompanyName) ? HostingCompanyName.trim() : '';
        accountSubLegalStructure =  String.isNotBlank(accountSubLegalStructure ) ? accountSubLegalStructure .trim() : '';//1.1
        
        HostingWrapper objHostingWrapper;
        
        Set<String> activeStatuses = String.isNotBlank(accountStatus) ? new Set<String>{accountStatus} : new Set<String>{'Active','Not Renewed','Under Formation'};
        
        if(activeStatuses.contains('Active')) activeStatuses.add('Not Renewed');
        
        string accountQuery = Apex_Utilcls.getAllFieldsSOQL('Account');
        
        accountQuery = accountQuery.remove('from Account') + ',Active_License__r.License_Issue_Date__c FROM Account';
        
        accountQuery = accountQuery +' where (Name like \''+HostingCompanyName+'%\' '+ 
                            ( searchWithLicenseNo ? ' OR Registration_License_No__c like \''+HostingCompanyName+'%\' )' : ')' ) +
                            ( searchWithSubLegalStructure? ' AND Sub_Legal_Type_of_Entity__c like \''+accountSubLegalStructure +'%\'' : '' ) +
                            ( isSameClient ? '' : ' AND Id!=:currentAccountId ' )+ 
                            ( activeStatuses.contains('Active') ? 'AND Registration_License_No__c != NULL ' : '' ) +
                             ' AND ROC_Status__c IN :activeStatuses LIMIT 100';
        //1.1 added condition for sub legal structure
        for(Account objAccount : Database.query(accountQuery) ){ // V1.9 - Claude - new query 
        
            objHostingWrapper = new HostingWrapper();
            objHostingWrapper.CompanyName = objAccount.Name;
            objHostingWrapper.CompanyId = objAccount.Id;
            objHostingWrapper.RegistrationNumber = objAccount.Registration_License_No__c;
            HostingCompanies.add(objHostingWrapper);
        }
    }
    
    public class HostingWrapper{
        
        public string CompanyId             {get;set;}
        public string CompanyName           {get;set;}
        public String RegistrationNumber    {get;set;}
    }
    
}