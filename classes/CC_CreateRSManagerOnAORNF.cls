/*
    Author: Merul
    Descripton: This will excetue on approval of Security review of In principal. 
                This will create RS manage step on AOR_NF_R upon successfull execution of code.
*/
global without sharing class CC_CreateRSManagerOnAORNF  implements HexaBPM.iCustomCodeExecutable 
{
    
        global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, 
                                                    HexaBPM__Step__c stp) 
        {
           
            string strResult = 'Success';
           
            // init boolean for Step status code.
            Map<String,Boolean> mapStepCodeStatusApproval = new Map<String,Boolean>{'RS_OFFICER_REVIEW'=>false,
                                                                                    'ENTITY_SIGNING'=>false,
                                                                                    'COMPLIANCE_REVIEW'=>false};

            // map of approved code.
            Map<String,String> mapStepCodeStatus = new Map<String,String>{'RS_OFFICER_REVIEW'=>'APPROVED',
                                                                          'ENTITY_SIGNING'=>'APPROVED',
                                                                          'COMPLIANCE_REVIEW'=>'AML_SUBMITTED'};


            Boolean isAllStepApproved = false;
            Boolean isRSOfficerReviewApporved = false;
            Boolean isEntitySigningApproved = false;
            Boolean isCC_ThomRutersChkValCondPass = false;
            HexaBPM__Step__c relatedAORRSOfferReviewStep;


            HexaBPM__Service_Request__c relatedAOR;
            if(stp!= null 
                    && stp.HexaBPM__SR__r.HexaBPM__Customer__c != null)
            {
                
                  //1. get related AOR_NF_R
                  //2. check all the designated step are closed.
                  //3. create RS manager Step.
                   

                // get related In Progress AOR_NF_R
                for(HexaBPM__Service_Request__c relatedAORTemp : [
                                                                    SELECT id 
                                                                      FROM HexaBPM__Service_Request__c
                                                                    WHERE HexaBPM__Customer__c =:stp.HexaBPM__SR__r.HexaBPM__Customer__c
                                                                      AND HexaBPM__Record_Type_Name__c = 'AOR_NF_R'
                                                                      AND HexaBPM__External_Status_Name__c = 'In Progress'
                                                                      AND HexaBPM__Parent_SR__c =:stp.HexaBPM__SR__c
                                                                    LIMIT 1
                                                             ]
                    )
                {
                    relatedAOR = relatedAORTemp;
                }

                if( relatedAOR == NULL)
                {
                            system.debug('No related AOR found.');
                            return 'Success';
                }

                // gettng steps of related AOR_NF_R
                for(HexaBPM__Step__c  step : [SELECT id,
                                                    HexaBPM__SR__c, 
                                                    HexaBPM__Step_Template__c,
                                                    HexaBPM__Step_Template__r.HexaBPM__Code__c,
                                                    HexaBPM__Status__r.HexaBPM__Code__c 
                                               FROM HexaBPM__Step__c
                                              WHERE HexaBPM__SR__c =:relatedAOR.Id
                                                AND HexaBPM__SR__r.HexaBPM__Customer__c =:stp.HexaBPM__SR__r.HexaBPM__Customer__c
                                                AND HexaBPM__SR__r.HexaBPM__Record_Type_Name__c = 'AOR_NF_R'
                                                AND HexaBPM__Step_Template__r.HexaBPM__Code__c IN:mapStepCodeStatusApproval.keySet()
                                                //AND HexaBPM__Status__r.HexaBPM__Code__c = 'APPROVED'
                                            ]
                    )
                    {
                            
                            if( step.HexaBPM__Step_Template__r.HexaBPM__Code__c == 'RS_OFFICER_REVIEW')
                            {
                                relatedAORRSOfferReviewStep = step;
                            }
                            
                            // gathering all designated approved step.
                            if( mapStepCodeStatusApproval.containsKey( step.HexaBPM__Step_Template__r.HexaBPM__Code__c ) 
                                        && mapStepCodeStatus.containsKey(step.HexaBPM__Step_Template__r.HexaBPM__Code__c)
                                            && mapStepCodeStatus.get(step.HexaBPM__Step_Template__r.HexaBPM__Code__c) == step.HexaBPM__Status__r.HexaBPM__Code__c
                            )
                            {
                                    mapStepCodeStatusApproval.put(step.HexaBPM__Step_Template__r.HexaBPM__Code__c,true);
                            }
                    }

                    system.debug('@@@@@@@@@@ mapStepCodeStatusApproval '+mapStepCodeStatusApproval);
                    //checking is all step approved.
                    for(String stepCode : mapStepCodeStatusApproval.keySet())
                    {
                        isAllStepApproved = (isAllStepApproved == NULL ? mapStepCodeStatusApproval.get(stepCode) : isAllStepApproved && mapStepCodeStatusApproval.get(stepCode));
                        
                        // Checkcking RS_OFFICER_REVIEW,ENTITY_SIGNING is approved
                        if( stepCode == 'RS_OFFICER_REVIEW' ) {  isRSOfficerReviewApporved = mapStepCodeStatusApproval.get(stepCode); }
                        if( stepCode == 'ENTITY_SIGNING' ) {  isEntitySigningApproved = mapStepCodeStatusApproval.get(stepCode); }
                    }
                    system.debug('######### isAllStepApproved '+isAllStepApproved);
                    
                    // Checkcking RS_OFFICER_REVIEW,ENTITY_SIGNING is approved and CC_ThomsonReutersCheckValidation != 'Please close all open TR cases before approval' 
                    //isCC_ThomRutersChkValCondPass 
                    if( relatedAORRSOfferReviewStep != NULL )
                    {
                        String thomsonReutersCheckValidationResult = new CC_ThomsonReutersCheckValidation().EvaluateCustomCode(NULL,relatedAORRSOfferReviewStep);
                        isCC_ThomRutersChkValCondPass =  (thomsonReutersCheckValidationResult != 'Please close all open TR cases before approval');
                    }
                   
                    
                    if( 
                        (isAllStepApproved != NULL && isAllStepApproved)
                        ||
                        ( isRSOfficerReviewApporved && isEntitySigningApproved && isCC_ThomRutersChkValCondPass)
                      )
                    {
                            //fetching SRStep
                            HexaBPM__SR_Steps__c CurrentSRStep;
                            for(HexaBPM__SR_Steps__c CurrentSRStepTemp : [SELECT id,
                                                                                HexaBPM__SR_Template__c,
                                                                                HexaBPM__Step_Template__c,
                                                                                HexaBPM__Start_Status__c,
                                                                                HexaBPM__Summary__c, 
                                                                                HexaBPM__Step_No__c,
                                                                                OwnerId
                                                                            FROM HexaBPM__SR_Steps__c
                                                                        WHERE HexaBPM__SR_Template__r.HexaBPM__SR_RecordType_API_Name__c = 'AOR_NF_R'
                                                                            AND HexaBPM__Step_Template__r.HexaBPM__Code__c = 'RS_MANAGER_REVIEW'
                                                                        LIMIT 1
                                                                        ] )
                            {
                                CurrentSRStep = CurrentSRStepTemp;
                            }
                            system.debug('@@@@@@@@@@ CurrentSRStep '+CurrentSRStep);
                            if(CurrentSRStep == NULL)
                            {
                                //return 'No SR Step Found.';
                                insert LogDetails.CreateLog(null, 'CC_CreateRSManagerOnAORNF ', 'No RS manager step found');  
                                // allowing other step to execute.
                                return 'Success';
                            }

                                // Create RS Manager Step.
                                HexaBPM__Step__c insStp = new HexaBPM__Step__c();
                                //insStp.HexaBPM__SR__c = stp.HexaBPM__SR__c;
                                insStp.HexaBPM__SR__c = relatedAOR.Id;
                                insStp.HexaBPM__Step_Template__c = CurrentSRStep.HexaBPM__Step_Template__c;
                                insStp.HexaBPM__Status__c = CurrentSRStep.HexaBPM__Start_Status__c;
                                insStp.HexaBPM__SR_Step__c = CurrentSRStep.id;
                                insStp.HexaBPM__Start_Date__c = system.today();
                                insStp.HexaBPM__Summary__c = CurrentSRStep.HexaBPM__Summary__c;
                                //insStp.HexaBPM__Step_Notes__c = stp.HexaBPM__Step_Notes__c;
                                insStp.HexaBPM__Step_No__c = CurrentSRStep.HexaBPM__Step_No__c;
                                insStp.OwnerId = CurrentSRStep.OwnerId;
                                //insStp.Returned__c = true;
                                //insStp.HexaBPM__Parent_Step__c = stp.Id;
                                system.debug('@@@@@@@@ insStp '+ insStp);
                            try 
                            {
                                insert insStp;
                            } 
                            catch (Exception e) 
                            {
                                strResult = 'Error: '+ e.getMessage();
                                system.debug('@@@@@@@@ exception '+ e.getMessage());
                                return strResult;
                            }
                            

                        }


            }
            
            return strResult;

        }
    
}