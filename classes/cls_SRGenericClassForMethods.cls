/*****************************************************************************************************************************
    Author      :   Swati Sehrawat
    Date        :   21st July 2016
    Description :   This class is used as a Generic class for Service Request object to use for Save, Submit, Cancel, edit operations
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
*****************************************************************************************************************************/

public without sharing class cls_SRGenericClassForMethods {

/********************************************************************************************************************************************************/
    //Constructor defined   
    public cls_SRGenericClassForMethods(){
        
    }

/**********************************************************************************************************************************************************/
    //Used for save or update service request record and return to detail page of the SR Record     
    public pageReference upsertServiceRequest(service_request__c objSR, string recordTypeName){
        pageReference redirectPage = null;
        upsert objSR;
        if(recordTypeName == 'GS_Comfort_Letters'){
            redirectPage = new PageReference('/apex/GSComfortLetters');
        }
        if(recordTypeName == 'Consulate_or_Embassy_Letter'){
            redirectPage = new PageReference('/apex/GSEmbassyLetter');
        }
        redirectPage.getParameters().put('RecordTypeId',objSR.RecordType.id);
        redirectPage.getParameters().put('RecordTypeName',objSR.RecordType.DeveloperName);
        redirectPage.getParameters().put('id',objSR.id);
        redirectPage.getParameters().put('isDetail','true');
        return redirectPage;    
    }   

/**********************************************************************************************************************************************************/
    //Used to redirect to SRSubmission page to submit the request, all validations should be handled before calling this method     
    public pageReference SubmitServiceRequest(service_request__c objSR){
        pageReference redirectPage = null;
        redirectPage = new PageReference('/apex/SRSubmissionPage');
        redirectPage.getParameters().put('id',objSR.id);
        return redirectPage;    
    }

/**********************************************************************************************************************************************************/
    //Used to redirect to edit page to edit the SR, replaced custom button used on standard edit page   
    public pageReference EditServiceRequest(service_request__c objSR){
        pageReference redirectPage = null;
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
        SREditOverride obj = new SREditOverride(con);
        redirectPage = obj.VerifyRequest();
        return redirectPage;    
    }

/**********************************************************************************************************************************************************/
    //Used to redirect to detail page or home page when request is cancelled
    public pageReference CancelServiceRequest(boolean isPortal, service_request__c objSR, boolean IsDetail){
        Pagereference objRef;
        try{
            if(IsDetail == true){ //Detail Page Button
                if(objSR.Submitted_Date__c != null){
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Request was already submitted.'));
                    return null;
                }else{
                    delete objSR;
                    if(isPortal)
                        objRef = new Pagereference('/apex/home');
                    else
                        objRef = new Pagereference('/home/home.jsp');
                }
            }else if(IsDetail == false){ //Edit Page Button
                if(objSR.Id != null)
                    objRef = new Pagereference('/'+objSR.Id);
                else{
                    if(isPortal)
                        objRef = new Pagereference('/apex/home');
                    else
                        objRef = new Pagereference('/home/home.jsp');
                }
            }
            if(objRef != null)
                objRef.setRedirect(true);
            return objRef;
        }catch(Exception ex){
            system.debug('-----');
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please contact support.'));
        }
        return null;    
    }

/**********************************************************************************************************************************************************/
    //Used for upgrading SR
    public pageReference upgradeSR(service_request__c objSR, string recordTypeName){
        pageReference redirectPage = null;
        redirectPage = new PageReference('/apex/GsUpgradeSR');
        redirectPage.getParameters().put('id',objSR.id);
        redirectPage.getParameters().put('type','vip');
        return redirectPage;    
    }   

/**********************************************************************************************************************************************************/

    //Used to get SR Template depending on the recordTypeName, used to show description and title on portal page
    public SR_template__c getSRTemplate(string recordtypename){
        list<SR_Template__c> tempListTemplate = [select id, SR_Description__c, Portal_Service_Request_Name__c, name from SR_Template__c where SR_RecordType_API_Name__c=:recordTypeName];
        if(tempListTemplate!=null && !tempListTemplate.isEmpty()){
            return tempListTemplate[0];
        }
        return null;
    }

/**********************************************************************************************************************************************************/
    //Used to get portal details and autopopulate same when logged in from portal   
    public user  getPortalDetails(){
        list<user> tempList = [select id, email, phone, contact.AccountId,contact.Account.Can_request_original_Letter__c, contact.Account.Name, contact.Account.Registration_License_No__c, contact.Account.Index_Card_Number__c from user where id=:userinfo.getuserid()];
        if(tempList!=null && !tempList.isEmpty()){
            return tempList[0];
        }
        return null;
    }

/**********************************************************************************************************************************************************/
    //Used to get SR record by passing ID as parametre.
    public service_request__c getServiceRequest(string srID){
        service_request__c objSR = new service_request__c();
        list<service_request__c> tempList = [select Customer__c,Establishment_Card_No__c,License_Number__c,Email__c,Send_SMS_To_Mobile__c,Avail_Courier_Services__c,
                                                    Use_Registered_Address__c,Consignee_FName__c,Apt_or_Villa_No__c,Consignee_LName__c,Courier_Cell_Phone__c,
                                                    Courier_Mobile_Number__c,Contact__c,Express_Service__c,Emirate__c,Monthly_Salary_AED__c,Passport_Number__c,
                                                    Name_of_Addressee__c,Date_of_DIFC_Visa_Issuance__c,Name_of_Police_Station__c,Service_Category__c,
                                                    Others_Please_Specify__c,Is_Applicant_Salary_Above_AED_20_000__c,car_registration_number__c,
                                                    entity_name__c,Post_Code__c,Position__c,Type_of_Service__c,Name,
                                                    submitted_date__c, Sponsor_Street__c, Sponsor_First_Name__c, Sponsor_Last_Name__c, Sponsor_Middle_Name__c,
                                                    Sponsor_Building__c, Sponsor_Mother_Full_Name__c, Sponsor_P_O_Box__c, Sponsor_Passport_No__c,
                                                    Monthly_Accommodation__c, Mortgage_Amount__c, Mother_Full_Name__c, Building_Name__c, Middle_Name__c, 
                                                    First_Name__c, Last_Name__c  ,External_Status_Name__c,Required_Docs_not_Uploaded__c ,UID_Number__c,
                                                    Other_Monthly_Allowances__c,Monthly_Basic_Salary__c,Usage_Type__c, Event_Name__c,   Folio_Number__c, 
                                                     Kindly_Note__c, Last_Name_Arabic__c, Client_s_Representative_in_Charge__c,Street_Address__c, Type_of_Unit__c,Tenant_Type_RORP__c,Name_of_Informer__c,
                                                    Country__c,Health_Card_Number__c,Authority_Name__c,Business_Name__c,Cage_No__c,Declaration_Signatory_Capacity__c,
                                                    City_Town__c,Commercial_Activity_Previous_Sponsor__c,
                                                    Commercial_Activity__c,Company_Name__c,Current_Registered_Building__c,Current_Registered_City__c,
                                                    Current_Registered_PO_Box__c,Current_Registered_Postcode__c,Current_Visa_No__c,DIFC_store_specifications__c,
                                                    Emirate_State__c,Emirates_Id_Number__c,FAX__c,First_Name_Arabic_Previous_Sponsor__c,Building_Previous_Sponsor__c,
                                                    First_Name_Arabic_Sponsor__c,First_Name_Arabic__c,Floor__c,Foreign_Registered_Building__c,Incorporated_Organization_Name__c,
                                                    Foreign_Registered_Level__c,Salary_Confidential__c,RecordTypeId,Place_of_Birth_Arabic__c,Mother_s_Full_Name_Arabic__c,Middle_Name_Arabic__c,Passport_No_Previous_Sponsor__c
                                             from service_request__c 
                                             where id =:srID];
        if(tempList!=null && !tempList.isEmpty()){
            objSR = tempList[0];
        }
        return objSR;   
    }

/**********************************************************************************************************************************************************/
    //Used to get recordTypeId depending on recordTypename passed
    public string getRecordTypeId(string recordTypeName){
        if(string.isNotBlank(recordTypeName)){
            Id recordTypeId = Schema.SObjectType.Service_request__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();   
            return string.valueOf(recordTypeId);
        }
        return null;
    }   
}