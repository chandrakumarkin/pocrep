/*
    Author      :   Sravan
    Description :   This class consists the actions for User Access form
    
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date            Updated By    Description
    V1.1    21-Nov-2020      Arun         Changed email send logic 12454
    
    -------------------------------------------------------------------------------------------------------------------------- 
*/

public class FormReviewSuperAdminController {
    
    
        public static void sendUserAccessFormEmail(Set<Id> setDocID){
            
            if(!system.isFuture()){
                sendUserAccessFormEmailfuture(setDocID);
            }
            else {
                Emailwithoutfuture(setDocID);
            }
        }
         public static void sendUserAccessEmail(Set<Id> setDocID)
         {
             Contact securityContact = new Contact();
              try{
                  
             
               map<id,HexaBPM__SR_Doc__c> srDocDetails = new map<id,HexaBPM__SR_Doc__c>([select id,Name,HexaBPM__Doc_ID__c,HexaBPM__Service_Request__r.HexaBPM__Email__c,HexaBPM__Service_Request__r.HexaBPM__SR_Template__r.Name,HexaBPM__Service_Request__c,HexaBPM__Service_Request__r.Name,HexaBPM__Service_Request__r.HexaBPM__Customer__c,HexaBPM__Service_Request__r.HexaBPM__Contact__r.FirstName,HexaBPM__Service_Request__r.HexaBPM__Customer__r.Name from HexaBPM__SR_Doc__c where Id IN:setDocID AND Name= 'Super User Authorisation - Generated']);
                
                 EmailTemplate et = [Select Id,Name from EmailTemplate where DeveloperName= 'DOB_User_access_form_template'];
                  id owEmailID;
            for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) {
                 owEmailID =owa.id; 
            }
                 
                for(id srDOcID : srDocDetails.KeySet())
                {
                     if(srDocDetails.get(srDocID).HexaBPM__Service_Request__r.HexaBPM__Email__c !=null)
                    {
                         Savepoint sp = Database.SetSavePoint();
                    List<messaging.SingleEmailMessage> messages = new List<messaging.SingleEmailMessage>();
                  
                    securityContact.LastName = srDocDetails.get(srDocID).HexaBPM__Service_Request__r.HexaBPM__Customer__r.Name;
                    securityContact.FirstName =  srDocDetails.get(srDocID).HexaBPM__Service_Request__r.HexaBPM__Contact__r.FirstName;
                    securityContact.email = srDocDetails.get(srDocID).HexaBPM__Service_Request__r.HexaBPM__Email__c;
                    securityContact.Accountid=srDocDetails.get(srDocID).HexaBPM__Service_Request__r.HexaBPM__Customer__c;
                    insert securityContact;
        
                 
                    
                    messaging.SingleEmailMessage message = new messaging.SingleEmailMessage();
                    message.setTemplateId(et.id);
                    message.setWhatId(srDocDetails.get(srDocID).HexaBPM__Service_Request__c);
                    
                    message.SettargetObjectId(securityContact.id);
                    message.SetOrgWideEmailAddressID(owEmailID);
                    messages.add(message);
                    //messages.setSaveAsActivity(false);
                    List<Messaging.SendEmailResult> results = Messaging.sendEmail(messages);
                    if(results!=null && results.size() >0 && !results[0].isSuccess())
                    delete securityContact;
                
                        
                    }
                }   
              
            }catch(Exception e){
            if(securityContact.Id!=null)delete securityContact;Log__c objLog = new Log__c(); objLog.Type__c = 'User Access form Actions';objLog.Description__c = e.getMessage()+' Line # '+e.getLineNumber(); insert objLog; 
            }  
            
             
         }
             
        @future(Callout=true)
        public static void sendUserAccessFormEmailfuture(Set<Id> setDocID){
           sendUserAccessEmail(setDocID);
        }
        
        public static void Emailwithoutfuture(Set<Id> setDocID){
           sendUserAccessEmail(setDocID); 
        }
        
        public static void dummytest(){
            
            Integer i = 0;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
            i++;
        
        }
}