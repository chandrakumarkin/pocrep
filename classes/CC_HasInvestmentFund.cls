/*
    Author      : Durga Prasad
    Date        : 17-Nov-2019
    Description : Custom code to check user has selected Activity of name "Investment Fund"
    ---------------------------------------------------------------------------------------
*/
global without sharing class CC_HasInvestmentFund implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'false';
        if(SR!=null && SR.License_Application__c!=null){
            for(License_Activity__c LA:[Select Id,Name from License_Activity__c where License__c=:SR.License_Application__c and Activity__r.Name='Investment Fund']){
                strResult = 'true';
            }
        }else if(SR!=null && SR.HexaBPM__Parent_SR__c!=null){
            for(License_Activity__c LA:[Select Id,Name from License_Activity__c where Application__c=:SR.HexaBPM__Parent_SR__c and Activity__r.Name='Investment Fund']){
                strResult = 'true';
            }
        }
        return strResult;
    }
}