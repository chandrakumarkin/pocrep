/**
 * @description       : 
 * @author            : Veera
 * @group             : 
 * @last modified on  : 04-08-2021
 * @last modified by  : Veera
 * Modifications Log 
 * Ver   Date          Author                               Modification
 * 1.0  04-08-2021     Veera                                 Initial Version
 **/

public without sharing class GSCloseCollectedParentStepController{
   
      @InvocableMethod(Label = 'GSCloseCollectedParentStepController' description = 'GSCloseCollectedParentStepController')
       public static void GSCloseCollectedParentStepControllerMethod(List < id > ids) {
           GsCourierDetails.CloseAwaitingOriginals(ids);
      }
 
}