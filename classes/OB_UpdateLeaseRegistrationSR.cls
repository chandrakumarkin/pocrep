/**
* Description:
* Regenerate Lease Certifcate when Commertial license is generated
**/
global without sharing class OB_UpdateLeaseRegistrationSR implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        string AccountId = stp.HexaBPM__SR__r.HexaBPM__Customer__c;
        
        if(stp.HexaBPM__SR__c!=null && AccountId != null){
            CC_RORPCodeCls.UpdateLeaseSR(AccountId);
        }
        Return strResult;
    }
}