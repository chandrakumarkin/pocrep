/**
 *Created By Mudasir for clsDraftSRDeleteBatch class
 */
@isTest
private class AccountPSAUpdate_Test {

    static testMethod void myUnitTest() {
	//SELECT ID, Name, Internal_Status_Name__c, External_Status_Name__c,SR_Template__r.Delete_Draft_Service_Requests__c FROM Service_Request__c 
	//WHERE SR_Group__c = \'Fit-Out & Events\' AND SR_Template__r.Delete_Draft_Service_Requests__c = true AND  External_Status_Name__c = \'Draft\' AND createdDate < LAST_N_DAYS:30 ORDER BY Name asc';        
     Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        objContact.Phone = '1234567890';
        insert objContact;
      
        
    	Test.startTest();
	    	AccountPSAUpdate abs= new AccountPSAUpdate();
	    	String cronExpr = '0 0 0 15 3 ? 2022';
			String jobId = System.schedule('myJobTestJobName', cronExpr, abs);
        Test.stopTest();
    }
}