@isTest
global class OB_OCRHelperMockTest implements HttpCalloutMock 
{
    global HTTPResponse respond(HTTPRequest request) 
    {
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        String jsonString = '{"Name":"OSULLIVAN,LAUREN","Gender":"F","Country":"IRL","DOB":"1988-05-04","PassportNo":"XN5004216","PassportExpiry":"2023-09-15","PersonalNo":""}';
        //String jsonString = '{"Name":"OSULLIVAN,LAUREN","Gender":"F","Country":"IRL","DOB":"1988-05-04","PassportNo":"XN5004216","PassportExpiry":"2023-09-15","PersonalNo":""}';
        //String jsonString = '{\\"Name\\":\\"OSULLIVAN,LAUREN\\",\\"Gender\\":\\"F\\",\\"Country\\":\\"IRL\\",\\"DOB\\":\\"1988-05-04\\",\\"PassportNo\\":\\"XN5004216\\",\\"PassportExpiry\\":\\"2023-09-15\\",\\"PersonalNo\\":\\"\\"}';
        response.setBody(jsonString);
        
        response.setStatusCode(200);
        return response; 
    }
}