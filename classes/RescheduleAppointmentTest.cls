@isTest
public class RescheduleAppointmentTest{
/*****************

    static testMethod void myUnitTestNormal(){

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;

        string conRT;
        
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;

        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }

        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c='GS';
        insert objTemplate1;

        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test scheduled';
        objStepType1.Code__c = 'Medical Fitness Test scheduled';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Medical Fitness Test scheduled';
        insert objStepType1;


        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Email__c = 'testsr@difc.com';    
        objSR3.Contact__c = objContact.Id;
        objSR3.Express_Service__c = false;
        objSR3.SR_Template__c = objTemplate1.Id;
        objSR3.Statement_of_Undertaking__c  = true;
        insert objSR3;

        Status__c objStatus = new Status__c();
        objStatus.Name = 'Re-Scheduled';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'RE-SCHEDULED';
        insert objStatus;

        step__c objStep = new Step__c();
        objStep.SR__c = objSR3.Id;

        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        objStep.status__c = objStatus.id;
        insert objStep;

        Appointment__c  app = new Appointment__c ();

        app.Applicantion_Type__c ='Normal';
        //app.Appointment_StartDate_Time__c =  System.now();
        app.Appointment_StartDate_Time__c =  datetime.newInstance(Date.today(), time.newInstance( 12, 00, 00, 00 ));
        app.step__c = objStep.id;
        insert app;

        CalendarDemoCtrl CalendarDemoCtrl = new CalendarDemoCtrl();

        //Appointment__c app2 =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Service_Type__c  from appointment__c where id =: app.id];  
        //app2.Appointment_StartDate_Time__c  = app2.Appointment_StartDate_Time__c.addMinutes(10); 
        //update app2;

        //Appointment__c app1 =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Service_Type__c from appointment__c where id =: app.id];
        //app1.Applicantion_Type__c ='Express';
        //app1.Appointment_StartDate_Time__c  = app1.Appointment_StartDate_Time__c.addMinutes(10); 
        //update app1;

        Appointment_Schedule_Counter__c appCounter = new Appointment_Schedule_Counter__c();
        appCounter.Appointment_Time__c = app.Appointment_StartDate_Time__c  ;
        appCounter.Application_Type__c = 'Normal';
        insert appCounter ;

        Test.startTest();
        
        RescheduleAppointmentSchedule RescheduleAppointmentScheduleObj = new RescheduleAppointmentSchedule();
        String sch = '20 30 8 10 2 ?';
        String jobID = system.schedule('Test Job', sch, RescheduleAppointmentScheduleObj);


        //RescheduleAppointmentBatch RescheduleAppointmentBatchObj = new RescheduleAppointmentBatch('');
        //database.executebatch( RescheduleAppointmentBatchObj, 10 );
        
        Test.stopTest();
    }

    static testMethod void myUnitTestExpress(){

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;

        string conRT;
        
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;

        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }

        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c='GS';
        insert objTemplate1;

        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test scheduled';
        objStepType1.Code__c = 'Medical Fitness Test scheduled';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Medical Fitness Test scheduled';
        insert objStepType1;


        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Email__c = 'testsr@difc.com';    
        objSR3.Contact__c = objContact.Id;
        objSR3.Express_Service__c = true;
        objSR3.SR_Template__c = objTemplate1.Id;
        objSR3.Statement_of_Undertaking__c  = true;
        insert objSR3;

        Status__c objStatus = new Status__c();
        objStatus.Name = 'Re-Scheduled';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'RE-SCHEDULED';
        insert objStatus;

        step__c objStep = new Step__c();
        objStep.SR__c = objSR3.Id;

        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        objStep.status__c = objStatus.id;
        insert objStep;

        Appointment__c  app = new Appointment__c ();

        app.Applicantion_Type__c ='Normal';
        app.Appointment_StartDate_Time__c =  datetime.newInstance(Date.today(), time.newInstance( 12, 00, 00, 00 ));
        app.step__c = objStep.id;
        insert app;

        CalendarDemoCtrl CalendarDemoCtrl = new CalendarDemoCtrl();

       

        Appointment_Schedule_Counter__c appCounter = new Appointment_Schedule_Counter__c();
        appCounter .Appointment_Time__c = app.Appointment_StartDate_Time__c  ;
        appCounter .Application_Type__c = 'Normal';
        insert appCounter ;

        Test.startTest();
        
        
        RescheduleAppointmentSchedule RescheduleAppointmentScheduleObj = new RescheduleAppointmentSchedule();
        String sch = '20 30 8 10 2 ?';
        String jobID = system.schedule('Test Job', sch, RescheduleAppointmentScheduleObj);

        
        Test.stopTest();
    }

    static testMethod void myUnitTestNormalWithCreateddate(){

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;

        string conRT;
        
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;

        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }

        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c='GS';
        insert objTemplate1;

        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test scheduled';
        objStepType1.Code__c = 'Medical Fitness Test scheduled';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Medical Fitness Test scheduled';
        insert objStepType1;


        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Email__c = 'testsr@difc.com';    
        objSR3.Contact__c = objContact.Id;
        objSR3.Express_Service__c = false;
        objSR3.SR_Template__c = objTemplate1.Id;
        objSR3.Statement_of_Undertaking__c  = true;
        insert objSR3;

        Status__c objStatus = new Status__c();
        objStatus.Name = 'Re-Scheduled';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'RE-SCHEDULED';
        insert objStatus;

        step__c objStep = new Step__c();
        objStep.SR__c = objSR3.Id;
        objStep.createddate = datetime.newInstance(Date.today(), time.newInstance( 12, 00, 00, 00 ));
        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        objStep.status__c = objStatus.id;
        insert objStep;

        Appointment__c  app = new Appointment__c ();

        app.Applicantion_Type__c ='Normal';
        //app.Appointment_StartDate_Time__c =  System.now();
        app.Appointment_StartDate_Time__c =  datetime.newInstance(Date.today(), time.newInstance( 12, 00, 00, 00 ));
        app.step__c = objStep.id;
        insert app;

        CalendarDemoCtrl CalendarDemoCtrl = new CalendarDemoCtrl();

        //Appointment__c app2 =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Service_Type__c  from appointment__c where id =: app.id];  
        //app2.Appointment_StartDate_Time__c  = app2.Appointment_StartDate_Time__c.addMinutes(10); 
        //update app2;

        //Appointment__c app1 =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Service_Type__c from appointment__c where id =: app.id];
        //app1.Applicantion_Type__c ='Express';
        //app1.Appointment_StartDate_Time__c  = app1.Appointment_StartDate_Time__c.addMinutes(10); 
        //update app1;

        Appointment_Schedule_Counter__c appCounter = new Appointment_Schedule_Counter__c();
        appCounter.Appointment_Time__c = app.Appointment_StartDate_Time__c  ;
        appCounter.Application_Type__c = 'Normal';
        insert appCounter ;

        Test.startTest();
        
        RescheduleAppointmentSchedule RescheduleAppointmentScheduleObj = new RescheduleAppointmentSchedule();
        String sch = '20 30 8 10 2 ?';
        String jobID = system.schedule('Test Job', sch, RescheduleAppointmentScheduleObj);


        //RescheduleAppointmentBatch RescheduleAppointmentBatchObj = new RescheduleAppointmentBatch('');
        //database.executebatch( RescheduleAppointmentBatchObj, 10 );
        
        Test.stopTest();
    }

    static testMethod void myUnitTestExpressWithCreateddate(){

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;

        string conRT;
        
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;

        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }

        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c='GS';
        insert objTemplate1;

        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test scheduled';
        objStepType1.Code__c = 'Medical Fitness Test scheduled';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Medical Fitness Test scheduled';
        insert objStepType1;


        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Email__c = 'testsr@difc.com';    
        objSR3.Contact__c = objContact.Id;
        objSR3.Express_Service__c = true;
        objSR3.SR_Template__c = objTemplate1.Id;
        objSR3.Statement_of_Undertaking__c  = true;
        insert objSR3;

        Status__c objStatus = new Status__c();
        objStatus.Name = 'Re-Scheduled';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'RE-SCHEDULED';
        insert objStatus;

        step__c objStep = new Step__c();
        objStep.SR__c = objSR3.Id;  
        objStep.createddate = datetime.newInstance(Date.today(), time.newInstance( 12, 00, 00, 00 ));
        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        objStep.status__c = objStatus.id;
        insert objStep;

        Appointment__c  app = new Appointment__c ();

        app.Applicantion_Type__c ='Normal';
        app.Appointment_StartDate_Time__c =  datetime.newInstance(Date.today(), time.newInstance( 12, 00, 00, 00 ));
        app.step__c = objStep.id;
        insert app;

        CalendarDemoCtrl CalendarDemoCtrl = new CalendarDemoCtrl();

       
        Appointment_Schedule_Counter__c appCounter = new Appointment_Schedule_Counter__c();
        appCounter .Appointment_Time__c = app.Appointment_StartDate_Time__c  ;
        appCounter .Application_Type__c = 'Normal';
        insert appCounter ;

        Test.startTest();
        
        
        RescheduleAppointmentSchedule RescheduleAppointmentScheduleObj = new RescheduleAppointmentSchedule();
        String sch = '20 30 8 10 2 ?';
        String jobID = system.schedule('Test Job', sch, RescheduleAppointmentScheduleObj);


        //RescheduleAppointmentBatch RescheduleAppointmentBatchObj = new RescheduleAppointmentBatch('');
        //database.executebatch( RescheduleAppointmentBatchObj, 10 );
        
        Test.stopTest();
    }
    ****************************************/
    @isTest static void test(){
        Test.startTest(); 
    
        RescheduleAppointmentBatch resobj= new RescheduleAppointmentBatch();
       database.executebatch(resobj);
       
       RescheduleAppointmentSchedule obj = new RescheduleAppointmentSchedule();
        String sch = '20 30 8 10 2 ?';
        String jobID = system.schedule('Test Job', sch, obj);

        Test.stopTest();
      
    }
}