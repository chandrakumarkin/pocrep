/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCC_DissolutionCls{
    
    @testSetup static void setTestData() {
        
        Test_FitoutServiceRequestUtils.init();
        Test_FitoutServiceRequestUtils.prepareSetup();
        
       
        
        
    }
    
    static testMethod void myUnitTest1() {
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Strike_Off','Change_of_Address_Details','Objection_Strike_Off')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '0012345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.Has_Permit_1__c = true;
        objAccount.ROC_Status__c = 'Active';
        objAccount.fine_not_paid__c = 'License Renewal';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.RecordTypeId = mapRecordTypeIds.get('Strike_Off');
        objSR.Statement_of_Undertaking__c = true;
        objSR.Fine_Amount_AED__c = 1000;
        insert objSR;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        insert objStep; 
        Step__c stepObj = [SELECT Id, SR__c, SR__r.RecordTypeId, SR__r.Record_Type_Name__c FROM Step__c WHERE Id =:objStep.Id];
        system.debug('### stepObj '+stepObj);
        CC_DissolutionCls.StrikeOffObjectionApproved(stepObj);
        objSR.RecordTypeId = mapRecordTypeIds.get('Objection_Strike_Off');
        update objSR;
        CC_DissolutionCls.StrikeOffObjectionApproved(stepObj);
        
     //   CC_DissolutionCls.updateFineValue(objStep);
    }

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        try{
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.setToInternalUser();
        Test_FitoutServiceRequestUtils.prepareTestUser(true);
        
        //User objUser = Test_FitoutServiceRequestUtils.getTestUser();
        
        //objUser.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services';
        
       // update objUser;
        
       // system.runAs(objUser){
        
            Account objAccount = new Account();
            objAccount.Name = 'Test Account123';
            objAccount.Place_of_Registration__c ='Pakistan';
            objAccount.ROC_Status__c = 'Active';
            insert objAccount;
            
            string strRecType = '';
            for(RecordType rectyp:[select id from RecordType where DeveloperName='De_Registration' and sObjectType='Service_Request__c']){
                strRecType = rectyp.Id;
            }   
            
            Service_Request__c objSR = new Service_Request__c();
            objSR.Customer__c = objAccount.Id;
            objSR.Date_of_Declaration__c = Date.today();
            if(strRecType!='')
                objSR.RecordTypeId = strRecType;
            insert objSR;       
                
            Step__c objStep = new Step__c();
            objStep.SR__c = objSR.Id;
            insert objStep;
            
            Lookup__c nat = new Lookup__c(Type__c='Liquidator',Name='Test Liquidator', Email__c = 'test@test.com', phone__c = '+971000000001' , fax__c = '+971000000001' , Address__c = 'test' );
            insert nat;

            String liqRecTypeId = '';
            for(RecordType objRT : [select Id,Name from RecordType where DeveloperName='Liquidator' AND SobjectType='Amendment__c' AND IsActive=true])
                liqRecTypeId = objRT.Id;
            
        
        
            Amendment__c amndRemCon = new Amendment__c(RecordTypeId=liqRecTypeId,Liquidator__c=nat.Id,ServiceRequest__c=objSR.id);
            insert amndRemCon;      
        } 
       // }
        catch(exception ex){
            
        } 
    }
    
    static testMethod void NotificationEmailsTest() {
        // TO DO: implement unit test
        SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        insert objSRStep;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Test Doc Master';
        objDocMaster.code__c='Passport & Visa Copy-Family Members';
        objDocMaster.DMS_Document_Index__c='abd';
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.On_Submit__c=true;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.Validate_In_Code_at_Step_No__c = objSRStep.Id;
        insert objTempDocs;
     //   try{
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.setToInternalUser();
        Test_FitoutServiceRequestUtils.prepareTestUser(true);
        
        User objUser = Test_FitoutServiceRequestUtils.getTestUser();
        
        objUser.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services';
        
      //  update objUser;
        
        
        
        system.runAs(objUser) {
            User userObj = [SELECT Id, ContactId,Contact.AccountId FROM user WHERE Id = :objuser.Id];
            Service_Request__c objSR = new Service_Request__c ();
            objSR.Customer__c = objUser.Contact.AccountId;
            objSR.Date_of_Declaration__c = Date.today();
            
            string strRecType = '';
            
            for(RecordType rectyp:[select id from RecordType where DeveloperName='Strike_Off' and sObjectType='Service_Request__c']){
                strRecType = rectyp.Id;
            }
            
            if(strRecType!='')
                objSR.RecordTypeId = strRecType;
            
            insert objSR;       
            //Shikha
            /*system.debug('### Account '+userObj.Contact.AccountId);
            system.debug('### Contact '+userObj.ContactId);
            AccountContactRelation accRelation = new AccountContactRelation();
            accRelation.AccountId = userObj.Contact.AccountId;
            accRelation.ContactId = userObj.ContactId;
            system.debug('### accRelation '+accRelation);
            insert accRelation;*/
            
            Step__c objStep = new Step__c();
            objStep.SR__c = objSR.Id;
            objStep.Client_Name__c = 'abc';
            insert objStep;
            
            list<SR_Doc__c> lstSR_Docs = new list<SR_Doc__c>();
            list<Attachment> lstAttachments = new list<Attachment>();
            
            SR_Doc__c objSRDoc = new SR_Doc__c();
            objSRDoc.Service_Request__c = objSR.Id;
            objSrDoc.Name = 'Dissolution Letter';
            lstSR_Docs.add( objSRDoc );
            
            objSRDoc = new SR_Doc__c();
            objSRDoc.Service_Request__c = objSR.Id;
            objSrDoc.Name = 'De-Registration Letter';
            lstSR_Docs.add( objSRDoc );
            
            SR_Doc__c objSRDoc2 = new SR_Doc__c();
            objSRDoc2.Service_Request__c = objSR.Id;
            objSrDoc2.Name = 'NOC to Transfer';
            lstSR_Docs.add( objSRDoc2);
            
            SR_Doc__c objSRDoc3 = new SR_Doc__c();
            objSRDoc3.Service_Request__c = objSR.Id;
            objSRDoc3.Name = 'Signed copy Strike Off Notice';
            lstSR_Docs.add( objSRDoc3 );
            
            SR_Doc__c objSRDoc4 = new SR_Doc__c();
            objSRDoc4.Service_Request__c = objSR.Id;
            objSRDoc4.Name = 'Signed copy Strike Off Notice';
            objSRDoc4.SR_Template_Doc__c = objTempDocs.Id;
            lstSR_Docs.add(objSRDoc4);
            
            objSRDoc4 = new SR_Doc__c();
            objSRDoc4.Service_Request__c = objSR.Id;
            objSRDoc4.Name = 'Signed copy Strike Off Notice 2';
            objSRDoc4.SR_Template_Doc__c = objTempDocs.Id;
            lstSR_Docs.add(objSRDoc4);
            
            objSRDoc4 = new SR_Doc__c();
            objSRDoc4.Service_Request__c = objSR.Id;
            objSRDoc4.Name = 'ROC Confirmation Letter';
            objSRDoc4.SR_Template_Doc__c = objTempDocs.Id;
            lstSR_Docs.add(objSRDoc4);
            
            insert lstSR_Docs;
            
            Attachment objAttachment1 = new Attachment();
            objAttachment1.Name = 'Test Document';
            objAttachment1.Body = blob.valueOf('test');
            objAttachment1.ParentId = lstSR_Docs[0].Id;
            lstAttachments.add(objAttachment1); 
            
            Attachment objAttachment2 = new Attachment();
            objAttachment2.Name = 'Test Document';
            objAttachment2.Body = blob.valueOf('test');
            objAttachment2.ParentId = lstSR_Docs[1].Id;
            lstAttachments.add( objAttachment2 ); 
            
            Attachment objAttachment3 = new Attachment();
            objAttachment3.Name = 'Test Document';
            objAttachment3.Body = blob.valueOf('test');
            objAttachment3.ParentId = lstSR_Docs[2].Id;
            lstAttachments.add( objAttachment3 );
            
            Attachment objAttachment4 = new Attachment();
            objAttachment4.Name = 'Test Document';
            objAttachment4.Body = blob.valueOf('test');
            objAttachment4.ParentId = lstSR_Docs[6].Id;
            lstAttachments.add( objAttachment4 );
            
            insert lstAttachments;
            Test.StartTest();
            
            CC_DissolutionCls.StrikeOffNotification(objStep);           
            CC_DissolutionCls.NoCForTransfer(objStep);      
            CC_DissolutionCls.NotificationOfWindingUp(objStep); 
            CC_DissolutionCls.UpdatePendingDissolutionDate(objStep);
            CC_DissolutionCls.StrikeOffObjectionApproved(objStep);
            CC_DissolutionCls.ReviewApprove(objStep);
         //   CC_DissolutionCls.updateFineValue(objStep);
            
            SR_Steps__c testSrStep = new SR_Steps__c();
            
            testSrStep.Step_No__c = 82;
    
            insert testSrStep;
            
            /* Create a Document master */
            Document_Master__c testDocumentMaster = new Document_Master__c();
            
            testDocumentMaster.Name = 'Test Document Master';
            
            insert testDocumentMaster;
            
            SR_Template__c testFitoutTemplate 
                    = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Test','Test','Fit-Out & Events');
                    
            testFitoutTemplate.Menutext__c = 'Test';
            
            insert testFitoutTemplate;
            
            /* Create an SR Template Doc */
            SR_Template_Docs__c testSRTemplateDoc = new SR_Template_Docs__c();
            
            testSRTemplateDoc.SR_Template__c = testFitoutTemplate.Id;
            testSRTemplateDoc.Document_Master__c = testDocumentMaster.Id;
            testSrTemplateDoc.Validate_In_Code_at_Step_No__c = testSrStep.Id;
            
            /* Create an SR Template Doc */     
            insert testSRTemplateDoc;
            
            objStep.SR_Step__c = testSrStep.Id; // just to checck
            
            update objStep;
            
            /* Create an SR */
            Service_Request__c testIssueFineRequest 
                = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
            
            testIssueFineRequest.Type_Of_Request__c = 'Other';
            testIssueFineRequest.Comments__c = 'Test';
            testIssueFineRequest.Fine_Amount_AED__c = 1000;
            testIssueFineRequest.Email__c = 'test.difc@test.org';
            testIssueFineRequest.Is_From_Back_Office__c = true;
                
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(testIssueFineRequest,'Issue_Fine');
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestStatus(testIssueFineRequest,'Draft',true);
            
            insert testIssueFineRequest;
    
            SR_Doc__c testSampleStepDoc = Test_CC_FitOutCustomCode_DataFactory.getTestSrDoc('Sample Step Document',testIssueFineRequest.Id);
            
            testSampleStepDoc.Step__c = objStep.Id;
            testSampleStepDoc.Sys_IsGenerated_Doc__c = false;
            testSampleSTepDoc.SR_Template_Doc__c = testSRTemplateDoc.ID;
            
            SR_Doc__c testSampleStepDoc2 = Test_CC_FitOutCustomCode_DataFactory.getTestSrDoc('Sample Step Document2',testIssueFineRequest.Id);
            
            testSampleStepDoc2.Step__c = objStep.Id;
            testSampleStepDoc2.Sys_IsGenerated_Doc__c = false;
            testSampleStepDoc2.SR_Template_Doc__c = testSRTemplateDoc.ID;
            
            insert new List<SR_Doc__c>{testSampleSTepDoc,testSampleStepDoc2};
            objStep.SR_Step__c = objSRStep.Id;
            CC_Validate_ReqDocument.Document_Upload_Check(objStep);
            
            lstAttachments = new list<Attachment>();
            objAttachment3 = new Attachment();
            objAttachment3.Name = 'Test Document';
            objAttachment3.Body = blob.valueOf('test');
            objAttachment3.ParentId = lstSR_Docs[3].Id;
            lstAttachments.add( objAttachment3 );
            objAttachment3 = new Attachment();
            objAttachment3.Name = 'Test Document';
            objAttachment3.Body = blob.valueOf('test');
            objAttachment3.ParentId = lstSR_Docs[4].Id;
            lstAttachments.add( objAttachment3 );
            insert lstAttachments;
            
            CC_Validate_ReqDocument.Document_Upload_Check(objStep);
            
            Test.stopTest();
        }
       /* }
        catch(exception ex){
        }*/
         
    }   
    
    static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        Test.startTest(); 
        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='De_Registration' and sObjectType='Service_Request__c']){
            strRecType = rectyp.Id;
        }   

        Product2 objProd = new Product2();
        objProd.Name = 'Sample Test';
        objProd.ProductCode = 'SampleTest';
        objProd.IsActive = true;
        insert objProd;     
        
        Id pricebookId = Test.getStandardPricebookId();
        
        // 1. Insert a price book entry for the standard price book.
        // Standard price book entries require the standard price book ID we got earlier.
        PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = objProd.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;

        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'Fines- Failure to file Annual Return';
        objPL.Product__c = objProd.Id;
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        objPL = new Pricing_Line__c();
        objPL.Name = 'Fines- Failure to Renew CL';
        objPL.Product__c = objProd.Id;
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        objPL = new Pricing_Line__c();
        objPL.Name = 'Fines- Failure to Appoint an Auditor';
        objPL.Product__c = objProd.Id;
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        objPL = new Pricing_Line__c();
        objPL.Name = 'Fines-Failure to file Annual Reporting';
        objPL.Product__c = objProd.Id;
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        objPL = new Pricing_Line__c();
        objPL.Name = 'Fines- Failure to renew DP Fine (financial)';
        objPL.Product__c = objProd.Id;
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        objPL = new Pricing_Line__c();
        objPL.Name = 'Fines-Failure to renew DP Fines (non financial/Retail)';
        objPL.Product__c = objProd.Id;
        objPL.Priority__c = 1;
        objPL.Priority__c = 1;
        lstPLs.add(objPL);
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            objDP.Unit_Price__c = 1234;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
        
        
        list<Compliance__c> lstCompliance = new list<Compliance__c>();
        
        Compliance__c objCompliance1 = new Compliance__c();
        objCompliance1 = new Compliance__c();
        objCompliance1 .Name = 'License Renewal';
        objCompliance1 .Account__c = objAccount.Id;
        objCompliance1 .Start_Date__c = system.today().addDays(-7).addDays(-7);
        objCompliance1 .End_Date__c = system.today().addDays(-1);
        objCompliance1 .Exception_Date__c = objCompliance1 .End_Date__c;
        objCompliance1 .Status__c = 'Defaulted';
        lstCompliance.add(objCompliance1 );
        
        
        Compliance__c objCompliance = new Compliance__c();
        objCompliance = new Compliance__c();
        objCompliance.Name = 'Annual Reporting';
        objCompliance.Account__c = objAccount.Id;
        objCompliance.Start_Date__c = system.today().addDays(-7).addDays(-7);
        objCompliance.End_Date__c = system.today().addDays(-1);
        objCompliance.Exception_Date__c = objCompliance.End_Date__c;
        objCompliance.Status__c = 'Defaulted';
        lstCompliance.add(objCompliance);
        insert lstCompliance;       
               
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Annual Reporting';
        objTemplate.SR_RecordType_API_Name__c = 'Annual_Reporting';
        objTemplate.ORA_Process_Identification__c = 'Annual_Reporting';
        objTemplate.Active__c = true;
        insert objTemplate;  
        
        
       
        SR_Template_Item__c objTemplItem = new SR_Template_Item__c();
       // objTemplItem.Consumed_at__c = objSRStep.Id;
        //objTemplItem.Evaluated_at__c = objSRStep.Id;
        objTemplItem.On_Submit__c = true;
        objTemplItem.Product__c = objProd.Id;
        objTemplItem.SR_Template__c = objTemplate.Id;
        insert objTemplItem;
               
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Date_of_Resolution__c = Date.today().addDays(20);
        if(strRecType!='')
            objSR.RecordTypeId = strRecType;
        objSR.SR_Group__c = 'ROC';
        insert objSR;       
        
        List<Step__c> stpList = new List<step__c>();   
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
       stpList.add(objStep);  
       
        status__c stepStatus = new Status__c();//Code__c='VERIFIED',Type__c='End',Name='Verified'
        stepStatus =[select id from Status__c where code__c='VERIFIED'  limit 1];
        stepStatus.Type__c='End';
        stepStatus.Name='Verified';
        upsert stepStatus;
        
        step_Template__c stpTemp = new Step_template__c(Name='Verification of Application',Step_RecordType_API_Name__c='Verification_of_Application',Code__c='VERIFICATION_OF_APPLICATION');
        insert stpTemp;
        
        step__c verfStep = new step__c();
        verfStep.sr__c = objSR.id;
        verfStep.Step_Template__c = stpTemp.id;
        verfStep.status__c = stepStatus.id;
        stpList.add(verfStep);
        insert stpList;
      
        
         Lookup__c nat = new Lookup__c(Type__c='Liquidator',Name='Test Liquidator', Email__c = 'test@test.com', phone__c = '+971000000001' , fax__c = '+971000000001' , Address__c = 'test' );
         insert nat;

            String liqRecTypeId = '';
            for(RecordType objRT : [select Id,Name from RecordType where DeveloperName='Liquidator' AND SobjectType='Amendment__c' AND IsActive=true])
                liqRecTypeId = objRT.Id;
            
        
        
            Amendment__c amndRemCon = new Amendment__c(RecordTypeId=liqRecTypeId,Liquidator__c=nat.Id,ServiceRequest__c=objSR.id);
            insert amndRemCon;           
        
        CC_DissolutionCls.CalculateClearance(objSR.Id);      
        CC_DissolutionCls.CreateLiquidators(stpList[0]);
        CC_DissolutionCls.CalculateAmount(2000, 10);
        CC_DissolutionCls.CheckOpenSR(stpList[0]);
        CC_DissolutionCls.StrikeOffNotification(stpList[0]);
        CC_DissolutionCls.NotificationOfWindingUp(stpList[0]);
        CC_DissolutionCls.NoCForTransfer(stpList[0]);
        CC_DissolutionCls.RemovePortalAccess(new list<Id>{objAccount.Id});
        CC_DissolutionCls.IssueClearanceNotification(stpList[0].Id,stpList[0].SR__c);
        Test.stopTest();
    }
    
    static testmethod void finalTest(){
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='De_Registration' and sObjectType='Service_Request__c']){
            strRecType = rectyp.Id;
        } 
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Date_of_Resolution__c = Date.today().addDays(20);
        objSR.Change_Address__c = false;
        if(strRecType!='')
            objSR.RecordTypeId = strRecType;
        objSR.SR_Group__c = 'ROC';
        insert objSR; 
        CC_DissolutionCls.isCustomCode = true;
        CC_DissolutionCls.ProcessLiquidators(objSR.Id);
    }
    
    
    static testmethod void finalTest1(){
        
         string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Dissolution' and sObjectType='Service_Request__c']){
            strRecType = rectyp.Id;
        } 
        
         Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        objAccount.fine_not_paid__c = 'Annual Return';
        objAccount.DFSA_Withdrawn_Date__c = System.today();        
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Date_of_Resolution__c = Date.today().addDays(20);
        objSR.Change_Address__c = false;
        objSR.Fine_Amount_AED__c =0;
            objSR.RecordTypeId = strRecType;
        objSR.SR_Group__c = 'ROC';
        insert objSR; 
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Liquidator')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Lookup__c nat1 = new Lookup__c(Type__c='Nationality',Name='Afghanistan',Email__c  ='test@test.com');
        insert nat1;
        
        Amendment__c amend =new Amendment__c();
       
        amend.ServiceRequest__c= objSR.id;
        amend.Title_new__c = 'Mrs.';
        amend.Family_Name__c = 'Mrs.';
        amend.Given_Name__c = 'Mrs.';
        amend.Liquidator__c = nat1.ID;
        amend.recordtypeID = '01220000000Mc0tAAC';
        insert amend;
        
        String RecordTypeId;
        for(RecordType objRT : [select Id,Name from RecordType where DeveloperName='Liquidator' AND SobjectType='Contact' AND IsActive=true]){
            RecordTypeId = objRT.Id;
    	}
        Contact eachContact = new Contact();
        eachContact.lastName = 'Test';
        eachContact.Email = 'test@test.com';
        eachContact.AccountId = objAccount.ID;
        eachContact.RecordtypeID = RecordTypeId;
        INSERT eachContact;
        
        Step__c objStep = new Step__c();
            objStep.SR__c = objSR.Id;
            objStep.Client_Name__c = 'abc';        
            insert objStep;
            
        CC_DissolutionCls.ProcessLiquidators(objSR.ID);
        Step__c stepObj = [SELECT Id, SR__c, SR__r.Customer__c FROM Step__c WHERE Id = :objStep.Id];
        CC_DissolutionCls.updateFineValue(stepObj);
        CC_DissolutionCls.updateDFSAWithdrawalDate(objAccount.Id,objStep.Id);
    }
    
    @istest
    public static void updateFineValueTest(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        objAccount.fine_not_paid__c = 'Annual Return';
        objAccount.DFSA_Withdrawn_Date__c = System.today();        
        insert objAccount;
        
        string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Dissolution' and sObjectType='Service_Request__c']){
            strRecType = rectyp.Id;
        } 
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Date_of_Resolution__c = Date.today().addDays(20);
        objSR.Change_Address__c = false;
        objSR.Fine_Amount_AED__c =0;
        objSR.RecordTypeId = strRecType;
        objSR.SR_Group__c = 'ROC';
        insert objSR; 
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Client_Name__c = 'abc';        
        insert objStep;
        Step__c stepObj = [SELECT Id, SR__c, SR__r.Customer__c,SR__r.Fine_Amount_AED__c FROM Step__c WHERE Id = :objStep.Id];
        Test.startTest();
        CC_DissolutionCls.updateFineValue(stepObj);
        
        objAccount.Fine_Not_Paid__c = '';
        update objAccount;
        objSR.Fine_Amount_AED__c = 10;
        update objSR;
        CC_DissolutionCls.updateFineValue(stepObj);
        CC_DissolutionCls.updateFineValue(objStep);
        Test.stopTest();
    }
    
    @istest
    public static void updateClosureStatus(){
        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        objAccount.fine_not_paid__c = 'Annual Return';
        objAccount.DFSA_Withdrawn_Date__c = System.today();        
        insert objAccount;
        
        string strRecType = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Transfer_from_DIFC' and sObjectType='Service_Request__c']){
            strRecType = rectyp.Id;
        } 
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Date_of_Resolution__c = Date.today().addDays(20);
        objSR.Change_Address__c = false;
        objSR.Fine_Amount_AED__c =0;
        objSR.RecordTypeId = strRecType;
        objSR.SR_Group__c = 'ROC';
        insert objSR; 
        
        step_template__c objTemp = [SELECT Id, Name FROM Step_template__c WHERE Code__c = 'COMPANY_CLOSURE_INFORMATION'];
        system.debug('### objTemp 1 '+objtemp);
         /*objtemp = new Step_Template__c();
        objtemp.Name = 'Company Closure Information';
        objtemp.Code__c = 'COMPANY_CLOSURE_INFORMATION';
        objTemp.Step_RecordType_API_Name__c = 'Company_Closure_Information';
        insert objtemp;
        system.debug('### objtemp '+objtemp);*/
        
        Status__c objStat = [SELECT Id, Name FROM Status__c WHERE Code__c = 'PENDING_CUSTOMER_INPUTS'];
            /*new Status__c();
        objStat.Name = 'Pending Customer Inputs';
        objStat.Code__c = 'PENDING_CUSTOMER_INPUTS';
        insert objStat;*/
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Step_Template__c = objtemp.Id;
        objStep.Status__c = objStat.id;         
        insert objStep;
        
        Test.startTest();
        CC_DissolutionCls.updateClosureInformationStatus(new List<Step__c> {objStep});
        Test.stopTest();
        //system.assertEquals(objStep.Status_Code__c, 'REQUIRED_INFO_UPDATED');
    }
}