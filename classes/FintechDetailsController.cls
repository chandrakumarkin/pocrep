/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 06-06-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   06-06-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public without sharing class FintechDetailsController {
   @AuraEnabled
   public static String getAccountDetails(){
    	
    	User eachUser = new User();
        eachUser  = [
					 SELECT Community_User_Role__c,
							Contact.Id,
							Contact.Account.Id,
                            Contact.Account.Type_of_Start_up__c,
                            Contact.Account.OB_Sector_Classification__c,
                            Contact.Account.Sector_Classification__c
					   FROM User 
					  WHERE ID=:userinfo.getUserId()
					];
        
        if(String.isBlank(eachUser.Contact.Account.Type_of_Start_up__c) && 
           (eachUser.Contact.Account.OB_Sector_Classification__c == 'FinTech and Innovation' 
              || eachUser.Contact.Account.OB_Sector_Classification__c == 'FinTech' 
              || eachUser.Contact.Account.OB_Sector_Classification__c == 'FinTech'
              || eachUser.Contact.Account.OB_Sector_Classification__c == 'FinTech and Innovation')){

             return eachUser.Contact.Account.Id;
        }
        else{
              return 'UPDATED';
        }
      
    }

     @AuraEnabled
   public static String saveAccountDetails(Account thisAccount){
    	
    	Account newAccount = new Account();
        newAccount         = thisAccount;
        update newAccount;

        return 'success';
    }


}