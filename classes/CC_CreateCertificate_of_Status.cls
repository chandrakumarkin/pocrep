/*
    Author      : Durga Prasad
    Date        : 16-Apr-2020
    Description : Custom code to create Application Document for Certificate of status
    -------------------------------------------------------------------------------------
*/
global without sharing class CC_CreateCertificate_of_Status implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
        	try{
	        	list<HexaBPM__SR_Doc__c> lstSRDocsTBC = new list<HexaBPM__SR_Doc__c>();
	        	HexaBPM__SR_Template_Docs__c TemplateDoc = new HexaBPM__SR_Template_Docs__c();
	        	for(HexaBPM__SR_Template_Docs__c TD:[Select Id,HexaBPM__Document_Master__c,HexaBPM__Generate_Document__c,HexaBPM__Document_Master__r.Name,HexaBPM__Document_Description_External__c,HexaBPM__Group_No__c,
	        	HexaBPM__Document_Master__r.HexaBPM__LetterTemplate__c from HexaBPM__SR_Template_Docs__c where HexaBPM__Document_Master_Code__c='CERTIFICATE_OF_STATUS' and HexaBPM__SR_Template__r.HexaBPM__Active__c=true and HexaBPM__SR_Template__r.HexaBPM__SR_RecordType_API_Name__c='Multi_Structure']){
	        		TemplateDoc = TD;
	        	}
	        	for(HexaBPM__Service_Request__c objSR:[Select Id,RecordType.DeveloperName,HexaBPM__IsClosedStatus__c from HexaBPM__Service_Request__c where HexaBPM__Parent_SR__c=:stp.HexaBPM__SR__c]){
	        		if((objSR.RecordType.DeveloperName=='Multi_Structure' && objSR.HexaBPM__IsClosedStatus__c && TemplateDoc.Id!=null) || test.isrunningtest()){
	        			lstSRDocsTBC.add(PrepareSRDoc(TemplateDoc,objSR.Id));
	        		}
	        	}
	        	if(lstSRDocsTBC.size()>0)
	        		insert lstSRDocsTBC;
        	}catch(Exception e){
        		strResult = e.getMessage()+'';
        	}
        }
		return strResult;	
    }
    public static HexaBPM__SR_Doc__c PrepareSRDoc(HexaBPM__SR_Template_Docs__c SRTmpDoc,string SRID){
    	HexaBPM__SR_Doc__c objSRDoc = new HexaBPM__SR_Doc__c();
    	objSRDoc.Name = SRTmpDoc.HexaBPM__Document_Master__r.Name;
		objSRDoc.HexaBPM__Service_Request__c = SRID;
		objSRDoc.HexaBPM__SR_Template_Doc__c = SRTmpDoc.Id;
		objSRDoc.HexaBPM__Document_Master__c = SRTmpDoc.HexaBPM__Document_Master__c;
		objSRDoc.HexaBPM__Group_No__c = SRTmpDoc.HexaBPM__Group_No__c;
		objSRDoc.HexaBPM__Generate_Document__c = true;
		objSRDoc.HexaBPM__Document_Description_External__c = SRTmpDoc.HexaBPM__Document_Description_External__c;
		objSRDoc.HexaBPM__Sys_IsGenerated_Doc__c = true;
		objSRDoc.HexaBPM__Letter_Template_Id__c = SRTmpDoc.HexaBPM__Document_Master__r.HexaBPM__LetterTemplate__c;
		objSRDoc.HexaBPM__Is_Not_Required__c = true;
		objSRDoc.HexaBPM__Status__c = 'Generated';
		string tempString  = string.valueof(Crypto.getRandomInteger());
		objSRDoc.HexaBPM__Sys_RandomNo__c = tempString.right(4);
		string Gen_or_Upload = 'Generate';
		objSRDoc.HexaBPM__Unique_SR_Doc__c = SRTmpDoc.HexaBPM__Document_Master__r.Name+'_'+SRID+'_'+Gen_or_Upload;
    	return objSRDoc;
    }
}