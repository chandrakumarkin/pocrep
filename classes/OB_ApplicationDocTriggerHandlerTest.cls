/******************************************************************************************
*  Name        : OB_ApplicationDocTriggerHandlerTest
*  Author      : Prateek Kadkol
*  Company     : PwC
*  Date        : 30-Jan-2020
*  Description : Trigger Handler Test for OB_ApplicationDocTrigger            
*******************************************************************************************/

@isTest
public class OB_ApplicationDocTriggerHandlerTest {
    
    public static testmethod void testMethod1(){
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Non Profit Incorporated Organisation (NPIO)','Services'}, 
                                                   new List<string>{'Non Profit Incorporated Organisation (NPIO)','Company'}, 
                                                   new List<string>{'NPIO','Recognized Company'});
       
        insertNewSRs[0].Setting_Up__c='Branch';
       
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
       
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[2].id;
        insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[2].id;
     
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        
        test.startTest();
        
        
        HexaBPM__SR_Doc__c srDocRec = new HexaBPM__SR_Doc__c();
        srDocRec.HexaBPM__Service_Request__c = insertNewSRs[0].id;
        insert srDocRec;
        
        
        
        test.stopTest();
        
        
    }
}