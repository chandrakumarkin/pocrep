global class closeCourierStepsBatch implements Database.Batchable<sObject>,Schedulable{
    
   global void execute(SchedulableContext ctx) {
        database.executeBatch(new closeCourierStepsBatch(), 1);
   }
    
   global List<Step__c> start(Database.BatchableContext bc) {
        
       Set<Id> stepIds = new Set<Id>();
       
       for(Shipment__c thisshipment : [SELECT Id,Step__c
                                        FROM Shipment__c 
                                        WHERE (Status__c = 'Delivered' OR Status__c = 'Closed') 
                                        AND Step__r.Status_Code__c !='DELIVERED'
                                        AND Step__r.Status_Code__c !='CLOSED'
                                        AND Step__r.Status_Code__c !='COLLECTED'
                                    ]){
                                        
          stepIds.add(thisshipment.Step__c);
           
       }
        
       return[SELECT ID,Status__c,Step_Name__c
                  FROM Step__c 
                  WHERE ID IN : stepIds]; 
    }

    global void execute(Database.BatchableContext bc, List<Step__c> scope){
        
        List<Step__c> listToUpdate = new List<Step__c>();
        
        for(Step__c thisStep : scope ){
            
            if(thisStep.Step_Name__c =='Courier Collection'){
                if(!test.isRunningTest()) thisStep.Status__c ='a1M20000001jICz';
            }
            
            else if(thisStep.Step_Name__c =='Courier Delivery'){
                 if(!test.isRunningTest()) thisStep.Status__c ='a1M20000002Ifgd';
            }
            
            else if(thisStep.Step_Name__c =='Original Passport Received'){
                 if(!test.isRunningTest()) thisStep.Status__c ='a1M20000001iL9Z'; //Closed
            }
            
            listToUpdate.add(thisStep);
        }
        
        update listToUpdate;
    }
    
    global void finish(Database.BatchableContext bc){
       closeCourierStepsBatch objSchedule = new closeCourierStepsBatch();
        Datetime dt = DateCalculation(system.now(), 50);
        string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();
        for(CronTrigger obj : [select Id from CronTrigger where CronJobDetailId IN (SELECT Id FROM CronJobDetail where Name = 'Close Courier Steps')])
         system.abortJob(obj.Id);
         system.schedule('Close Courier Steps', sCornExp, objSchedule);
    } 
    
   public static DateTime DateCalculation(DateTime dt,Integer iFrequnacy){
    Datetime temp = dt.addMinutes(iFrequnacy);
    if(temp.hour() >= 18){
      temp = temp.addDays(1);
      string sDay = temp.format('EEEE');
      system.debug('sDay is : '+sDay);
      if(sDay == 'Friday'){
        temp = temp.addDays(2);
      }else if(sDay == 'Saturday'){
        temp = temp.addDays(1);
      }
      DateTime tempDate = Datetime.newInstance(temp.year(), temp.month(), temp.day(), 7, 30, 0);
      return tempDate;
    }
    return temp;
  }

}