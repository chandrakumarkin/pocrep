/******************************************************************************************
 *  Author   : Veera
 *  Company  : 
 *  Date     : 16/06/2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    16/06/2021   Veera          Created


**********************************************************************************************************************/

public class GDRFATransDocController {

   @AuraEnabled
     public static  List<recieptWrapper>  getSRDocList (String searchValue){
     
      List<string> SRstatus = new List<string>(); // {'Process Completed','The application is under process with GDRFA'};
      List<string> StepNames = new List<string>();
      List<string> Docuname = new List<string> {'Transaction Receipt Document'};
      for(Transaction_Doc_step_Names__c t : Transaction_Doc_step_Names__c.getall().values() ){
         StepNames.add(t.name);
      }
      
      for(GDRFA_Transaction_Rec_SR_Status__c g : GDRFA_Transaction_Rec_SR_Status__c.getall().values()){
        SRstatus.add(g.SR_Status__c);
      }
      
      List<SR_Doc__c> SRDocList = new List<SR_Doc__c>();
      List<recieptWrapper> recieptWrapperList = new List<recieptWrapper>();
      string queryString = 'select id,step__r.DNRD_Receipt_No__c,step__r.Payment_Date__c,step__r.Transaction_Number__c,Service_Request__r.name,step__r.SR_Menu_Text__c,step__r.Is_It_Express_Service__c,step__r.Created_Date_Time__c,Doc_ID__c,Service_Request__r.Nationality_list__c from SR_Doc__c where Name in:Docuname and Step__r.Step_Name__c in : StepNames and ';
      queryString = queryString+ 'Service_Request__r.External_Status_Name__c in : SRstatus and Status__c = \'uploaded\' and ';
      queryString = queryString + 'Step__r.Status_Code__c = \'Closed\' and step__r.Payment_Date__c = TODAY and Remove_from_Ipad__c = false ';
      
        if(String.isNotBlank(searchValue)){
             string newValue = String.valueOf(searchValue.trim());
             if(searchValue.contains(' ')){
                queryString =queryString; 
             }
             else{
                queryString =queryString+ 'AND Service_Request__r.Passport_Number__c like \'%'+ string.escapeSingleQuotes(newValue) + '%\'';  
             }
                                    
         }
         
           queryString = queryString+ ' order by step__r.Payment_Date__c';
         
         for (SR_Doc__c SrDoc : database.query(queryString)){
         
             system.debug(SrDoc.step__r.DNRD_Receipt_No__c);
             
             SRDocList.add(SrDoc);
             
             recieptWrapperList.add(new recieptWrapper(SrDoc.id,SrDoc.step__r.DNRD_Receipt_No__c,srdoc.step__r.Payment_Date__c,srdoc.step__r.Transaction_Number__c,
                 srdoc.Service_Request__r.name,srdoc.step__r.SR_Menu_Text__c,srdoc.step__r.Is_It_Express_Service__c,srdoc.step__r.Created_Date_Time__c,srdoc.Doc_ID__c,srdoc.Service_Request__r.Nationality_list__c ));
            
              system.debug(recieptWrapperList+'recieptWrapperList');
           
           }
         
          
         
         return recieptWrapperList;
         
       
      
     }
      @AuraEnabled      
      public static void updateRecord(List<string> SRdocRecid){
      
      }
    
   public class recieptWrapper{
      @AuraEnabled
      public string SRDOCId {get;set;}
      @AuraEnabled
      public string DNRDrecnumber {get;set;}
      @AuraEnabled
      public Date PaymentDate {get;set;}
      @AuraEnabled
      public string TransactionNumber{get;set;}
      @AuraEnabled
      public string SRName{get;set;}
      @AuraEnabled
      public string SRMenuText{get;set;}
      @AuraEnabled
      public string IsItExpress_Service{get;set;}
      @AuraEnabled
      public Datetime createdDateTime{get;set;}
      @AuraEnabled
      public string Link{get;set;}
      @AuraEnabled
      public string Country{get;set;}
      
      
      public recieptWrapper(string SRDOCId, string DNRDrecnumber , Date PaymentDate ,string TransactionNumber,string SRName, string SRMenuText, string IsItExpress_Service, Datetime createdDateTime , string Link, string Country ){
        
           this.SRDOCId = SRDOCId ;
           this.DNRDrecnumber  = DNRDrecnumber ;
           this.PaymentDate = PaymentDate ;
           this.TransactionNumber = TransactionNumber;
           this.SRName = SRName;
           this.SRMenuText = SRMenuText;
           this.IsItExpress_Service = IsItExpress_Service;
           this.createdDateTime = createdDateTime;
           this.Link = Link;
           this.Country = Country;
           
           
      
      }
       
     }
     
     
     @AuraEnabled
     public static String SRDOCfromIpad(String[] lstSRDocId){
     
         string msg='';
         
         List<SR_Doc__c> SRDocs = new List<SR_Doc__c>();
         
         for (SR_Doc__c Docs : [select id from SR_Doc__c where id in: lstSRDocId]){
             
              SR_Doc__c Doc = new SR_Doc__c();
              Doc.id = Docs.id;
              Doc.Remove_from_Ipad__c = true;
              SRDocs.add(Doc);
         }
         
         try{
         update SRDocs;
         }catch(exception Ex){
            msg=ex.getMessage()+'\n'+ex.getLineNumber()+'\n'+ex.getCause();
         }
         
        return msg;
     
     }
     
    

}