/******************************************************************************************
 *  Author   	: Claude Manahan
 *  Company  	: NSI DMCC 
 *  Date     	: 2016-08-30      
 *  Description : This class handles FIT-OUT Email transactions
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By        Description
 ----------------------------------------------------------------------------------------            
 V1.0   30-08-2016      Claude            Created
 *******************************************************************************************/
public without sharing class Fo_cls_EmailServiceHandler {
	
	/**
	 * Contains the details of the email
	 */
	private EmailDetails emailAttachmentInst;
	
	@InvocableMethod(label='Send SRDoc Email' description='Sends an SR Doc once the form is generated')
	public static void sendSrDocEmail(List<SrDocEmailDetails> srDocDetails){
		
		Set<Id> srDocIds = new Set<id>();
		Set<String> emailTemplateNames = new Set<String>();
		
		Map<String,String> emailTemplateMap = new Map<String,String>();
		Map<String,String> documentMap = new Map<String,String>();
		Map<String,String> emailTemplateMapPerDoc = new Map<String,String>();
		
		/* Go through each sr doc detail */
		for(SrDocEmailDetails srDetail : srDocDetails){
			
			/* Get the SR Doc Record IDs */
			srDocIds.add(srDetail.srDocId);
			
			/* Store the email template for retrieval */
			emailTemplateNames.add(srDetail.emailTemplateName);
			emailTemplateMapPerDoc.put(srDetail.srDocId,srDetail.emailTemplateName);
			
			documentMap.put(srDetail.srDocId,srDetail.documentName);
		}
		
		/* Query the email tempaltes */
		List<EmailTemplate> templateId = [select Id, DeveloperName from EmailTemplate where DeveloperName IN :emailTemplateNames];
		
		/* Create a map of the email templates */
		for(EmailTemplate emailTemplate : templateId){
			emailTemplateMap.put(emailTemplate.DeveloperName,emailTemplate.Id);
		}
		
	    /* Get the SR Doc */
        List<SR_Doc__c> SRDoc = 
        	[SELECT Doc_ID__c, Id,
        			Preview_Document__c, (SELECT Id, Name FROM Attachments),
        			Service_Request__r.Email_Address__c,
        			Service_Request__r.Email__c,
        			Service_Request__r.Name,
        			Service_Request__c
        			FROM SR_Doc__c WHERE 
        			//Id =:srDocDetails[0].srDocId AND
        			Id IN :srDocIds AND
        			Doc_ID__c != Null];
        
        if(SRDoc != Null && !SRDoc.isEmpty()){
        	
        	for(SR_Doc__c doc : SRDoc){
	            /* Create a map of attachments */
	            Map<String,String> attachmentMap = new Map<String,String>();
	            //attachmentMap.put(SRDoc[0].Attachments[0].Id,'contractor_authorisation.pdf');
	            //attachmentMap.put(SRDoc[0].Attachments[0].Id,srDocDetails[0].documentName);
	            attachmentMap.put(doc.Attachments[0].Id,documentMap.get(doc.id));
	            
	            /* Send an Email with attachments */
	            //List<EmailTemplate> templateId = [select Id from EmailTemplate where DeveloperName='Portal_User_creation_for_contractor_wallet'];
	            //List<EmailTemplate> templateId = [select Id from EmailTemplate where DeveloperName=:srDocDetails[0].emailTemplateName];
	            
	            if(!templateId.isEmpty()){
	            	
	            	Fo_cls_EmailServiceHandler.EmailDetails emailDetails = 
	            		new Fo_cls_EmailServiceHandler.EmailDetails(attachmentMap,
	            			new List<String>{doc.Service_Request__r.Email_Address__c,doc.Service_Request__r.Email__c},
	            			emailTemplateMap.get(emailTemplateMapPerDoc.get(doc.id)),UserInfo.getUserId(),doc.Service_Request__c);
	
					System.debug('EMail: ' + doc.Service_Request__r.Email_Address__c);
					System.debug('Email Result: ' + new Fo_cls_EmailServiceHandler(emailDetails).sendEmailWithAttachments());
	            }
        	}
        }
	    
	}
	
	/**
	 * Default Constructor
	 */
	public Fo_cls_EmailServiceHandler(EmailDetails emailAttachmentInst){
		this.emailAttachmentInst = emailAttachmentInst;
	}
    
    /**
     * Sends an attachment to the user
     */
    public Messaging.SendEmailResult[] sendEmailWithAttachments(){
    	
    	/* Query the attachment separately */
        Map<Id,Attachment> contractorDocs = new Map<Id,Attachment>([SELECT ID,
        									  ContentType,
        									  Body FROM Attachment 
        									  WHERE Id in :emailAttachmentInst.attachmentMap.keyset()]);
        									  
		if(!contractorDocs.isEmpty()){
			
			System.debug('Sending Email to: ' + emailAttachmentInst.emailAddresses);
			
			/* Send an email */
            // Define the email
			Messaging.SingleEmailMessage email = 
				Messaging.renderStoredEmailTemplate(emailAttachmentInst.templateId,
													emailAttachmentInst.contactId,
													emailAttachmentInst.whatId);
			
			/* Create a list of attachments */
			Messaging.EmailFileAttachment[] attachments = new List<Messaging.EmailFileAttachment>();
			
			/* Loop through the list to create the attachments */
			for(String k : emailAttachmentInst.attachmentMap.keyset()){
				
				Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
	        	
	        	efa.setFileName(emailAttachmentInst.attachmentMap.get(k));		// populate the file name
	        	efa.setBody(contractorDocs.get(k).Body);	// get the body
	        
	        	attachments.add(efa);
			}
			
			// Sets the paramaters of the email
			//email.setTargetObjectId(emailAttachmentInst.contactId);
	        email.setToAddresses(emailAttachmentInst.emailAddresses);
	        email.setTemplateId(emailAttachmentInst.templateId);
	        //email.setWhatId(emailAttachmentInst.whatId);
	        
	        email.setSaveAsActivity(false); 
	        
	        // Add the attachments
            email.setFileAttachments(attachments);
            
            // Sends the email
        	return Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});  
		}
    	
    	return null;
    }
    
    /**
     * Wrapper classes for email attachments
     * through the forms
     */
    public class EmailDetails {
    	
    	public Map<String,String> attachmentMap 	{get; private set;}
    	
    	public String[] emailAddresses 				{get; private set;}
    	
    	public String templateId					{get; private set;}
    	
    	public String contactId						{get; private set;}
    	
    	public String whatId						{get; private set;}
    	
    	public EmailDetails(Map<String,String> attachmentMap, String[] emailAddresses, String templateId, String contactId, String whatId){
    	
    		this.attachmentMap = attachmentMap;
    		this.emailAddresses = emailAddresses;
    		this.templateId = templateId;
    		this.contactId = contactId;
    		this.whatId = whatId;
    	}
    }
    
    public class SrDocEmailDetails {

        @InvocableVariable(required=true)
        public String srDocId; 
        
        @InvocableVariable(required=true)
        public String documentName;
        
        @InvocableVariable(required=true)
        public String emailTemplateName;

    }
    
    
}