@isTest(SeeAllData=false)
private class Test_penaltyCalculatorController{
    static TestMethod void penaltyCalculatorTest(){
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration','Surrender_Termination_of_Lease_and_Sublease','Freehold_Transfer_Registration','Company_buyer','Developer','Individual_Buyer','Company_Seller','RORP_Authorized_Signatory','RORP_Shareholder','RORP_Shareholder_Company')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
         test.startTest();
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = label.RORPCompanyCode;
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c SRStauts = new SR_Status__c(Name='Draft',Code__c='DRAFT');
        lstSRStatus.add(SRStauts);
        
        insert lstSRStatus;
                
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordType.get('Lease_Registration');
        objSR.Building__c = objBuilding.Id;
        objSR.Unit__c = lstUnits[0].Id;
        objSR.Service_Category__c = 'New';
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        objSr.Submitted_DateTime__c = system.today();
        objSR.Lease_Commencement_Date__c = Date.newInstance(2019,04,25);
        objSR.Lease_Expiry_Date__c = Date.newInstance(2030,04,25);
        objSR.Agreement_Date__c = Date.newInstance(2019,04,25);
        objSR.Submitted_Date__c = Date.newInstance(2019,04,29);
        //objSr.Penalty_Applicable__c = false;
        insert objSR;
        
        service_request__c sr = [select id ,Record_Type_Name__c from service_request__c  where id =:objSR.id];
        //system.assertNotEquals(sr.Record_Type_Name__c,null); 
        
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Lease Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Lease_Registration';
        objTemplate.Menutext__c = 'Lease_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        objTemplate.SR_Group__c ='RORP';
        
        insert objTemplate; 
        
        penaltyCalculatorController.srTemplate =  objTemplate;
        /**************************
        product2 prod= new product2();
        prod.Name='Penalty Charges';
        prod.ProductCode='Penalty Charges';
        prod.isActive=true;
        prod.Family= 'RORP' ;
        insert prod;
        
        SR_Template_Item__c  srItem = new SR_Template_Item__c ();
        sritem.SR_Template__c  = objTemplate.id;
        sritem.Product__c = prod.id;       
        insert srItem;
        
        Pricing_Line__c  prLine = new Pricing_Line__c ();
        prLine.product__c= srItem.product__c;
        prLine.Priority__c =1;
        prLine.Name = 'Penalty Charges';
        insert prLine;
        
        Dated_Pricing__c  dtprice = new Dated_Pricing__c ();
        dtprice.Unit_Price__c = 1200;
        dtprice.Pricing_Line__c  = prLine.id;
        insert dtprice;
        **************************/
        List<product2> productList  = new List<product2>();
        product2 prod= new product2();
        prod.Name='Penalty Charges';
        prod.ProductCode='Penalty Charges';
        prod.isActive=true;
        prod.Family= 'RORP';
        productList.add(Prod);
        
        product2 prod1= new product2();
        prod1.Name='Lease Registration';
        prod1.ProductCode='Lease Registration';
        prod1.isActive=true;
        prod1.Family= 'RORP';
        productList.add(Prod1);
        
        insert productList;
        
        List<SR_Template_Item__c> stTemplateItem = new List<SR_Template_Item__c>();
        SR_Template_Item__c  srItem = new SR_Template_Item__c ();
        sritem.SR_Template__c  = objTemplate.id;
        sritem.Product__c = productList[0].id;       
        stTemplateItem.add(sritem);
        
        SR_Template_Item__c  srItem1 = new SR_Template_Item__c ();
        sritem1.SR_Template__c  = objTemplate.id;
        sritem1.Product__c = productList[1].id;       
        stTemplateItem.add(sritem1);
        
        insert stTemplateItem;
        penaltyCalculatorController.srItem  = stTemplateItem[0];
        
        List<Pricing_Line__c> priceLineList = new List<Pricing_Line__c>();
        Pricing_Line__c  prLine = new Pricing_Line__c ();
        prLine.product__c= stTemplateItem[0].product__c;
        prLine.Priority__c =1;
        prLine.Name = stTemplateItem[0].product__r.Name;
        priceLineList.add(prLine);
        
        Pricing_Line__c  prLine1 = new Pricing_Line__c ();
        prLine1.product__c= stTemplateItem[1].product__c;
        prLine1.Priority__c =1;
        prLine1.Name = stTemplateItem[1].product__r.Name;
        
        priceLineList.add(prLine1);
        
        insert priceLineList;
        penaltyCalculatorController.priceLine = priceLineList[0];
        
        List<Dated_Pricing__c> Listdatedprice =  new List<Dated_Pricing__c>();
        Dated_Pricing__c  dtprice = new Dated_Pricing__c ();
        dtprice.Unit_Price__c = 3675;
        dtprice.Pricing_Line__c  = priceLineList[0].id;
        //dtprice.Unit_Price_in_USD__c = 100;
        Listdatedprice.add(dtprice);
        
        Dated_Pricing__c  dtprice1 = new Dated_Pricing__c ();
        dtprice1.Unit_Price__c = 3675;
        dtprice1.Pricing_Line__c  = priceLineList[1].id;
        //dtprice1.Unit_Price_in_USD__c = 100;
        Listdatedprice.add(dtprice1);
        
        insert Listdatedprice;
        
        penaltyCalculatorController.datePrice = Listdatedprice[0];
        
        string pbbalance = getPortalBalanceUtilityController.getPortalBalance(objAccount.BP_No__c,'Available Portal balance is : AED');
        //system.assertEquals(pbbalance,'100');
        
        
        Lease__c objLease = new Lease__c();
        objLease.Start_date__c = system.today().addDays(-30);
        objLease.End_Date__c = system.today().addMonths(1);
        objLease.Status__c = 'Active';
        objLease.Type__c = 'Leased';
        objLease.SAP_Lease_Number__c = '123456';
        objLease.Name = '123456';
        insert objLease;
        
        Tenancy__c objTen = new Tenancy__c();
        objTen.Lease__c = objLease.Id;
        objTen.Unit__c = lstUnits[0].Id;
        insert objTen;
        
        Lease__c objLeaseF = new Lease__c();
        objLeaseF.Start_date__c = system.today().addMonths(1).addDays(1);
        objLeaseF.End_Date__c = objLeaseF.Start_date__c.addMonths(1);
        objLeaseF.Status__c = 'Future Lease';
        objLeaseF.Type__c = 'Leased';
        objLeaseF.SAP_Lease_Number__c = '123457';
        objLeaseF.Name = '123457';
        insert objLeaseF;
        
        Tenancy__c objTenF = new Tenancy__c();
        objTenF.Lease__c = objLeaseF.Id;
        objTenF.Unit__c = lstUnits[0].Id;
        insert objTenF;
        
        
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = objLease.Id;
        objLP.Unit__c = lstUnits[0].Id;
        objLP.Account__c = objAccount.Id;
        insert objLP;
        
         List<SR_Price_Item__c > SRPrioceItemList = new List<SR_Price_Item__c>();
        SR_Price_Item__c Sr_Pr_Item1 = new SR_Price_Item__c();
         Sr_Pr_Item1.ServiceRequest__c = objSR.id;
         Sr_Pr_Item1.Product__c = productList[0].ID;
         Sr_Pr_Item1.Pricing_Line__c = priceLineList[0].id;
         Sr_Pr_Item1.Price__c = Listdatedprice[0].Unit_Price__c ;
         Sr_Pr_Item1.Status__c ='Consumed';
         
         SRPrioceItemList.add(Sr_Pr_Item1) ;
         
         SR_Price_Item__c Sr_Pr_Item2 = new SR_Price_Item__c();
         Sr_Pr_Item2.ServiceRequest__c = objSR.id;
         Sr_Pr_Item2.Product__c = productList[1].ID;
         Sr_Pr_Item2.Pricing_Line__c = priceLineList[1].id;
         Sr_Pr_Item2.Price__c = Listdatedprice[1].Unit_Price__c ;
         Sr_Pr_Item2.Status__c ='Consumed';
         
         SRPrioceItemList.add(Sr_Pr_Item2) ;
         
         insert SRPrioceItemList;
               
        
        penaltyCalculatorController.penaltyCalculator(objSR.id);
        penaltyCalculatorController.dateCalculator(objSR.id,objBuilding.id,objSR.Lease_Commencement_Date__c ,objSR.Agreement_Date__c,objSR.Lease_Expiry_Date__c,true);
        penaltyCalculatorController.removeLeaseCharges(objSR.id);
         List<SR_Price_Item__c > SRPrioceItemList1 = new List<SR_Price_Item__c>();
        SR_Price_Item__c Sr_Pr_Item12 = new SR_Price_Item__c();
         Sr_Pr_Item12.ServiceRequest__c = objSR.id;
         Sr_Pr_Item12.Product__c = productList[0].ID;
         Sr_Pr_Item12.Pricing_Line__c = priceLineList[0].id;
         Sr_Pr_Item12.Price__c = Listdatedprice[0].Unit_Price__c ;
         Sr_Pr_Item12.Status__c ='Consumed';
         
         SRPrioceItemList1.add(Sr_Pr_Item12) ;
         
         SR_Price_Item__c Sr_Pr_Item22 = new SR_Price_Item__c();
         Sr_Pr_Item22.ServiceRequest__c = objSR.id;
         Sr_Pr_Item22.Product__c = productList[1].ID;
         Sr_Pr_Item22.Pricing_Line__c = priceLineList[1].id;
         Sr_Pr_Item22.Price__c = Listdatedprice[1].Unit_Price__c ;
         Sr_Pr_Item22.Status__c ='Consumed';
         
         SRPrioceItemList1.add(Sr_Pr_Item22) ;
         
         insert SRPrioceItemList1;
        penaltyCalculatorController.removePenalty(objSR.id);
        delete stTemplateItem;
        penaltyCalculatorController.penaltyCalculator(objSR.id);
         test.stopTest();
     }  
     
      static TestMethod void penaltyCalculatorRemoveTest(){
        map<string,string> mapRecordType = new map<string,string>();
        map<Id,Building__c> mapIdWithBuildingObj =  new Map<Id,Building__c>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
         test.startTest();
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = Label.RORPCompanyCode;
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        insert objBuilding;
        
        mapIdWithBuildingObj.put(objBuilding.Id,objBuilding);
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c SRStauts = new SR_Status__c(Name='Draft',Code__c='DRAFT');
        lstSRStatus.add(SRStauts);
        
        insert lstSRStatus;
                
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        List<Service_Request__c > lstSR = new List<Service_Request__c >();
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordType.get('Lease_Registration');
        objSR.Building__c = objBuilding.Id;
        objSR.Unit__c = lstUnits[0].Id;
        objSR.Service_Category__c = 'New';
        objSR.Type_of_Lease__c = 'Lease';
        
        objSR.Rental_Amount__c = 1234;
        
        objSR.Lease_Commencement_Date__c = system.today().addYears(-1).addDays(-10);
        objSR.Submitted_Date__c =system.today().addDays(10);
        objSR.Lease_Expiry_Date__c = system.today().addDays(400);
        objSR.Agreement_Date__c = system.today().addYears(-1).addDays(-10);
        insert objSR;
        lstSR.add(objSR);
        
        service_request__c sr = [select id ,Record_Type_Name__c from service_request__c  where id =:objSR.id];
        //system.assertNotEquals(sr.Record_Type_Name__c,null); 
        
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Lease Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Lease_Registration';
        objTemplate.Menutext__c = 'Lease_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        objTemplate.SR_Group__c ='RORP';
        insert objTemplate; 
        /***************
        product2 prod= new product2();
        prod.Name='Penalty Charges';
        prod.ProductCode='Penalty Charges';
        prod.isActive=true;
        prod.Family= 'RORP' ;
        insert prod;
        
        product2 prod1= new product2();
        prod1.Name='Lease Registration';
        prod1.ProductCode='Lease Registration';
        prod1.isActive=true;
        prod1.Family= 'RORP' ;
        insert prod1;
        
        SR_Template_Item__c  srItem = new SR_Template_Item__c ();
        sritem.SR_Template__c  = objTemplate.id;
        sritem.Product__c = prod.id;       
        insert srItem;
        
        Pricing_Line__c  prLine = new Pricing_Line__c ();
        prLine.product__c= srItem.product__c;
        prLine.Priority__c =1;
        prLine.Name = 'Penalty Charges';
        insert prLine;
        
        Dated_Pricing__c  dtprice = new Dated_Pricing__c ();
        dtprice.Unit_Price__c = 1200;
        dtprice.Pricing_Line__c  = prLine.id;
        insert dtprice;
        *********************/
        List<product2> productList  = new List<product2>();
        product2 prod= new product2();
        prod.Name='Penalty Charges';
        prod.ProductCode='Penalty Charges';
        prod.isActive=true;
        prod.Family= 'RORP';
        productList.add(Prod);
        
        product2 prod1= new product2();
        prod1.Name='Lease Registration';
        prod1.ProductCode='Lease Registration';
        prod1.isActive=true;
        prod1.Family= 'RORP';
        productList.add(Prod1);
        
        insert productList;
        
        List<SR_Template_Item__c> stTemplateItem = new List<SR_Template_Item__c>();
        SR_Template_Item__c  srItem = new SR_Template_Item__c ();
        sritem.SR_Template__c  = objTemplate.id;
        sritem.Product__c = productList[0].id;       
        stTemplateItem.add(sritem);
        
        SR_Template_Item__c  srItem1 = new SR_Template_Item__c ();
        sritem1.SR_Template__c  = objTemplate.id;
        sritem1.Product__c = productList[1].id;       
        stTemplateItem.add(sritem1);
        
        insert stTemplateItem;
        
        List<Pricing_Line__c> priceLineList = new List<Pricing_Line__c>();
        Pricing_Line__c  prLine = new Pricing_Line__c ();
        prLine.product__c= stTemplateItem[0].product__c;
        prLine.Priority__c =1;
        prLine.Name = stTemplateItem[0].product__r.Name;
        priceLineList.add(prLine);
        
        Pricing_Line__c  prLine1 = new Pricing_Line__c ();
        prLine1.product__c= stTemplateItem[1].product__c;
        prLine1.Priority__c =1;
        prLine1.Name = stTemplateItem[1].product__r.Name;
        priceLineList.add(prLine1);
        
        insert priceLineList;
        
        List<Dated_Pricing__c> Listdatedprice =  new List<Dated_Pricing__c>();
        Dated_Pricing__c  dtprice = new Dated_Pricing__c ();
        dtprice.Unit_Price__c = 1200;
        dtprice.Pricing_Line__c  = priceLineList[0].id;
        Listdatedprice.add(dtprice);
        
        Dated_Pricing__c  dtprice1 = new Dated_Pricing__c ();
        dtprice1.Unit_Price__c = 1200;
        dtprice1.Pricing_Line__c  = priceLineList[1].id;
        Listdatedprice.add(dtprice1);
        
        insert Listdatedprice;
        
        string pbbalance = getPortalBalanceUtilityController.getPortalBalance('001234','Available Portal balance is : AED');
        //system.assertEquals(pbbalance,'100');
        
        
        Lease__c objLease = new Lease__c();
        objLease.Start_date__c = system.today().addDays(-30);
        objLease.End_Date__c = system.today().addMonths(1);
        objLease.Status__c = 'Active';
        objLease.Type__c = 'Leased';
        objLease.SAP_Lease_Number__c = '123456';
        objLease.Name = '123456';
        insert objLease;
        
        Tenancy__c objTen = new Tenancy__c();
        objTen.Lease__c = objLease.Id;
        objTen.Unit__c = lstUnits[0].Id;
        insert objTen;
        
        Lease__c objLeaseF = new Lease__c();
        objLeaseF.Start_date__c = system.today().addMonths(1).addDays(1);
        objLeaseF.End_Date__c = objLeaseF.Start_date__c.addMonths(1);
        objLeaseF.Status__c = 'Future Lease';
        objLeaseF.Type__c = 'Leased';
        objLeaseF.SAP_Lease_Number__c = '123457';
        objLeaseF.Name = '123457';
        insert objLeaseF;
        
        Tenancy__c objTenF = new Tenancy__c();
        objTenF.Lease__c = objLeaseF.Id;
        objTenF.Unit__c = lstUnits[0].Id;
        insert objTenF;
        
        
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = objLease.Id;
        objLP.Unit__c = lstUnits[0].Id;
        objLP.Account__c = objAccount.Id;
        insert objLP;
        
          List<SR_Price_Item__c > SRPrioceItemList = new List<SR_Price_Item__c>();
        SR_Price_Item__c Sr_Pr_Item1 = new SR_Price_Item__c();
         Sr_Pr_Item1.ServiceRequest__c = objSR.id;
         Sr_Pr_Item1.Product__c = productList[0].ID;
         Sr_Pr_Item1.Pricing_Line__c = priceLineList[0].id;
         Sr_Pr_Item1.Price__c = Listdatedprice[0].Unit_Price__c ;
         Sr_Pr_Item1.Status__c ='Consumed';
         
         SRPrioceItemList.add(Sr_Pr_Item1) ;
         
         SR_Price_Item__c Sr_Pr_Item2 = new SR_Price_Item__c();
         Sr_Pr_Item2.ServiceRequest__c = objSR.id;
         Sr_Pr_Item2.Product__c = productList[1].ID;
         Sr_Pr_Item2.Pricing_Line__c = priceLineList[1].id;
         Sr_Pr_Item2.Price__c = Listdatedprice[1].Unit_Price__c ;
         Sr_Pr_Item2.Status__c ='Consumed';
         
         SRPrioceItemList.add(Sr_Pr_Item2) ;
         
         insert SRPrioceItemList;
               
        
        objBuilding.Company_Code__c = Label.RORPCompanyCode;
        update objBuilding;
        penaltyCalculatorController.LeaseRegisterFeeCalculation(lstSR,mapIdWithBuildingObj);
        
        objSr.Submitted_DateTime__c = system.today();
        objSr.Penalty_Applicable__c = true;
        update objSr;
        penaltyCalculatorController.removePenalty(objSR.id);
        
        
        objAccount.BP_No__c = null;
        update objAccount;
        
       
        
        penaltyCalculatorController.penaltyCalculator(objSR.id);
        
        penaltyCalculatorController.LeasePenaltyCalculation(lstSR,mapIdWithBuildingObj);
        penaltyCalculatorController.dateCalculator(objSR.id,objBuilding.id,objSR.Lease_Commencement_Date__c ,objSR.Agreement_Date__c,objSR.Lease_Expiry_Date__c,true);
        penaltyCalculatorController.removeLeaseCharges(objSR.id);
        List<Id> strId = new List<Id>();
        strId.add(objSR.id);
        sendSRDocumentInMail.callfuturetoSendEmail(strId);
        //delete priceLineList;
      //delete Sr_Pr_Item ;
       
        penaltyCalculatorController.penaltyCalculator('ABC');
        
        List<SR_Price_Item__c > SRPrioceItemList1 = new List<SR_Price_Item__c>();
        
        SR_Price_Item__c Sr_Pr_Item12 = new SR_Price_Item__c();
         Sr_Pr_Item12.ServiceRequest__c = objSR.id;
         Sr_Pr_Item12.Product__c = productList[0].ID;
         Sr_Pr_Item12.Pricing_Line__c = priceLineList[0].id;
         Sr_Pr_Item12.Price__c = Listdatedprice[0].Unit_Price__c ;
         Sr_Pr_Item12.Status__c ='Consumed';
         
         SRPrioceItemList1.add(Sr_Pr_Item12) ;
         
         SR_Price_Item__c Sr_Pr_Item22 = new SR_Price_Item__c();
         Sr_Pr_Item22.ServiceRequest__c = objSR.id;
         Sr_Pr_Item22.Product__c = productList[1].ID;
         Sr_Pr_Item22.Pricing_Line__c = priceLineList[1].id;
         Sr_Pr_Item22.Price__c = Listdatedprice[1].Unit_Price__c ;
         Sr_Pr_Item22.Status__c ='Consumed';
         
         SRPrioceItemList1.add(Sr_Pr_Item22) ;
         
         SR_Price_Item__c Sr_Pr_Item23 = new SR_Price_Item__c();
         Sr_Pr_Item23.ServiceRequest__c = objSR.id;
         Sr_Pr_Item23.Product__c = productList[1].ID;
         Sr_Pr_Item23.Pricing_Line__c = priceLineList[0].id;
         Sr_Pr_Item23.Price__c = Listdatedprice[1].Unit_Price__c ;
         Sr_Pr_Item23.Status__c ='Consumed';
         
         SRPrioceItemList1.add(Sr_Pr_Item23) ;
         
         SR_Price_Item__c Sr_Pr_Item24 = new SR_Price_Item__c();
         Sr_Pr_Item24.ServiceRequest__c = objSR.id;
         Sr_Pr_Item24.Product__c = productList[0].ID;
         Sr_Pr_Item24.Pricing_Line__c = priceLineList[1].id;
         Sr_Pr_Item24.Price__c = Listdatedprice[1].Unit_Price__c ;
         Sr_Pr_Item24.Status__c ='Consumed';
         
         SRPrioceItemList1.add(Sr_Pr_Item24) ;
         
         insert SRPrioceItemList1;
        
        penaltyCalculatorController.removeLeaseCharges(objSR.id);
        
      
         test.stopTest();
     }   
     
  static TestMethod void penaltyCalculatorRemoveTest2(){
  
        map<string,string> mapRecordType = new map<string,string>();
        map<Id,Building__c> mapIdWithBuildingObj =  new Map<Id,Building__c>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
         test.startTest();
         
        BusinessHours defaultBH = [Select Id From BusinessHours Where IsDefault = True];
        Holiday holidays=[Select h.StartTimeInMinutes, h.Name, h.ActivityDate From Holiday h limit 1];
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = Label.RORPCompanyCode;
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        insert objBuilding;
        
        mapIdWithBuildingObj.put(objBuilding.Id,objBuilding);
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c SRStauts = new SR_Status__c(Name='Draft',Code__c='DRAFT');
        lstSRStatus.add(SRStauts);
        
        insert lstSRStatus;
                
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        List<Service_Request__c > lstSR = new List<Service_Request__c >();
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordType.get('Lease_Registration');
        objSR.Building__c = objBuilding.Id;
        objSR.Unit__c = lstUnits[0].Id;
        objSR.Service_Category__c = 'New';
        objSR.Type_of_Lease__c = 'Lease';
        
        objSR.Rental_Amount__c = 1234;
        
        objSR.Lease_Commencement_Date__c = system.today();
        objSR.Lease_Expiry_Date__c = system.today().addDays(754);
        objSR.Agreement_Date__c = system.today().addDays(-31);
        objSR.Submitted_Date__c =system.today().addDays(25);
        insert objSR;
        lstSR.add(objSR);
        
        service_request__c sr = [select id ,Record_Type_Name__c from service_request__c  where id =:objSR.id];
        //system.assertNotEquals(sr.Record_Type_Name__c,null); 
        
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Lease Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Lease_Registration';
        objTemplate.Menutext__c = 'Lease_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        objTemplate.SR_Group__c ='RORP';
        insert objTemplate; 
        
        product2 prod12= new product2();
        prod12.Name='Penalty Charges';
        prod12.ProductCode='Penalty Charges';
        prod12.isActive=true;
        prod12.Family= 'RORP';
       
        insert prod12;
        
        SR_Template_Item__c  srItem12 = new SR_Template_Item__c ();
        sritem12.SR_Template__c  = objTemplate.id;
        sritem12.Product__c = prod12.id;       
        insert sritem12;
        
        Pricing_Line__c  prLine12 = new Pricing_Line__c ();
        prLine12.product__c= sritem12.product__c;
        prLine12.Priority__c =1;
        prLine12.Name ='Penalty Charges';
        insert prLine12;
        
        Dated_Pricing__c  dtprice12 = new Dated_Pricing__c ();
        dtprice12.Unit_Price__c = 3675;
        dtprice12.Pricing_Line__c  = prLine12.id;
        Insert dtprice12;
        
        SR_Price_Item__c Sr_Pr_Item22 = new SR_Price_Item__c();
        Sr_Pr_Item22.ServiceRequest__c = objSR.id;
        Sr_Pr_Item22.Product__c = prod12.ID;
        Sr_Pr_Item22.Pricing_Line__c = prLine12.id;
        Sr_Pr_Item22.Price__c = dtprice12.Unit_Price__c ;
        Sr_Pr_Item22.Status__c ='Consumed';
        
        insert Sr_Pr_Item22;
        
        penaltyCalculatorController.removePenalty(objSR.id);
        penaltyCalculatorController.testsample();
     /********************
      ApexPages.StandardController sc = new ApexPages.standardController(new Service_Request__c());
       CancelGSrequest csr = new CancelGSrequest(sc);
      //MyExtension extension = new MyExtension(controller);
      csr.autoRun();
      *************************/   
        
        CalculateWorkingDays objclass= new CalculateWorkingDays();
        objclass.calculateWorkingDaysBetweenTwoDates(System.Today().adddays(-1),System.Today().adddays(2));
        test.stopTest();
         
        
        }
        
       
    }