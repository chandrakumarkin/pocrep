@isTest
global class TestSecurityStepStatusUpdateBatch {
     global class insertDatedConversionMock implements HttpCalloutMock {
     public HTTPResponse respond(HTTPRequest req) {
       if(req.getEndpoint().contains('https://cscs.dfzc.ae/o/oauth2')) {
                HTTPResponse res = new HTTPResponse();
                res.setBody('{"DIFC":"DIFC"}');
                res.setStatusCode(200);
                return res;
            } 
            else if(req.getEndpoint().contains('https://cscs.dfzc.ae/o/dfzc-rest/trade-licence/')) {
                HTTPResponse res = new HTTPResponse();
                res.setBody('{"RequestCommonData": ["Status":"Approved"]}');
                res.setStatusCode(200);
                return res;
            } 
          return null;
           }
         
     }
    
    public static Step__c stepRecord;
    public static User portalUser;
    public static List<Account> lstAccount;
    public static List<Amendment__c> lstAmendments;
    
    @testSetup static void setup() {

        
        List<Account> lstAccount = new List<Account>();
        List<Contact> lstContact = new List<Contact>();
        List<Service_Request__c> lstServiceRequestData = new List<Service_Request__c>();
        lstAmendments = new List<Amendment__c>();
        
        lstAccount = TestUtilityClass.accountDataInsert(1,false);
        lstAccount[0].ROC_Status__c = 'Under Formation';
        
        if(lstAccount.size()>0){
            INSERT lstAccount;
        }
        
        lstContact = TestUtilityClass.contactDataInsert(1,true,lstAccount[0].ID);
        portalUser = new User();
        //portalUser = TestUtilityClass.insertPortalUser(lstContact[0].ID); 
        
        //system.runas(portalUser){
            lstServiceRequestData = TestUtilityClass.createServiceRequestData(1,false);
            lstServiceRequestData[0].Customer__c =lstAccount[0].ID;
            lstServiceRequestData[0].RecordtypeID = '01220000000MbZ4';
            lstServiceRequestData[0].SR_Template__c = 'a1C20000003S7pW';
            if(lstServiceRequestData.size()>0){
                INSERT  lstServiceRequestData;
            }
            
            Step_Template__c srTemplate = new Step_Template__c();
            srTemplate.Code__c = 'SECURITY_EMAIL_APPROVAL';
            srTemplate.Name='Security Email Approval';
            srTemplate.Step_RecordType_API_Name__c ='Security_Email_Approval';
            INSERT srTemplate; 
             
            stepRecord = new Step__c();
            stepRecord.SR__c = lstServiceRequestData[0].Id;
            stepRecord.SR_Record_Type_Text__c = 'Application_of_Registration';
            stepRecord.Status__c  = Label.Pending_Review_Status;
            stepRecord.Step_Template__c = srTemplate.ID;
            insert stepRecord;
        //}
    }
    
    static testMethod void myUnitTest() {
        
        try{
        
           
            Test.startTest();
                Test.setMock(HttpCalloutMock.class, new insertDatedConversionMock());
                SecurityStepStatusUpdateBatch securityBatch = new SecurityStepStatusUpdateBatch();
                database.executeBatch(securityBatch,1);
              
             
            Test.StopTest();
        }
        catch(Exception ex){
            
        }
    }
}