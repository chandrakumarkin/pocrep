/*  -------------------------------------------------------------------------
    **Version History**
     * @Author: Rajil Ravindran 
     * @version: 1.0
     * @Date: 02-May-2019
     * @Description: 
        1. Gets the selected case records from the list view and reassigns to the selected department.
        
    -------------------------------------------------------------------------
    **Version History**
     * @Author: Anil Valluri 
     * @version: 2.0
     * @Date: 08-Aug-2021
     * @Description: 
        1. Case management enhancements- Remove 'Type' field whereever referred.
*/
public without sharing class CaseReassignController{

    public string caseId{get;set;}
    
    public Case objCase{get;set;}
    
    public string currentTheme{get;set;}
    private ApexPages.StandardSetController standardController;
    public CaseReassignController(ApexPages.StandardSetController standardController){
        this.standardController = standardController;
        objCase = new Case();
        if(apexpages.currentpage().getParameters().get('Id')!=null){ //This is specifically for the case detail page to reassign a single case.
            caseId = String.escapeSingleQuotes(apexpages.currentpage().getParameters().get('Id'));
            objCase = [SELECT Id, Type, GN_Department__c,AccountId,GN_Proposed_Due_Date__c,GN_Account_Name_Non_DIFC__c,GN_Client_Type__c FROM Case WHERE Id = :caseId];
        }
        System.debug('##UI: '+UserInfo.getUiThemeDisplayed());
        currentTheme = UserInfo.getUiThemeDisplayed();
    }
    
    public pagereference CancelAction(){
        Pagereference pg = new Pagereference('/'+caseId);
        pg.setredirect(true);
        return standardController.cancel();
    }
    
    public pagereference ReassignAction(){  
        try{
            if(caseId != null && String.isNotBlank(caseId)) //This is specifically for the case detail page to reassign a single case.
            {
                update objCase;
            }
            else{ //This is for mass reassignment from the list view page. @version: 1.0
               // Get the selected records (optional, you can use getSelected to obtain ID's and do your own SOQL)
                List<Case> selectedCases = (List<Case>) standardController.getSelected();
                list<Case> lstCaseToUpdate = new list<Case>();
                // Update records       
                for(Case selectedCase : selectedCases)
                {
                    //selectedCase.Type = objCase.Type; // PwC: Case management enhancements.
                    selectedCase.GN_Department__c = objCase.GN_Department__c;
                    lstCaseToUpdate.add(selectedCase);
                }   
                if(lstCaseToUpdate != null && lstCaseToUpdate.size() > 0)
                    update lstCaseToUpdate;
            }//@version: 1.0 ends here.
            //return standardController.cancel();
            Pagereference pg = new Pagereference('/'+500);
            pg.setredirect(true);
            return pg;
            
        } catch(DmlException  ex){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, ex.getDmlMessage(0));
            ApexPages.addMessage(msg);
            return null;
        }
    }
}