/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-16-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-07-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public class CC_AMLManagerReviewCheckTest {
    
     static testMethod void CreateAutomaticShipments(){
         
        List<Account> insertNewAccounts              = new List<Account>();
        insertNewAccounts                            = OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insert insertNewAccounts;
        
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 
        
        License__c lic = new License__c(name='12345',Account__c=insertNewAccounts[0].id, License_Expiry_Date__c = Date.today());
        insert lic;
        
        insertNewAccounts[0].Active_License__c = lic.id;
         
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
         
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});
        insert listSRStatus;
         
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Setting_Up__c='new';
        insertNewSRs[0].recordTypeId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].Passport_No__c = 'A12345';
        insertNewSRs[0].Nationality__c = 'India';
        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
        insertNewSRs[0].Gender__c = 'Male';
        insertNewSRs[0].Place_of_Birth__c = 'Dubai';
        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
        insertNewSRs[0].Last_Name__c = 'test';
        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Business_Sector__c = 'Hotel';
        insertNewSRs[0].entity_name__c = 'Tentity';
        insertNewSRs[0].Progress_Completion__c = 10.0;
        insertNewSRs[1].License_Application__c = lic.id;
        insertNewSRs[0].HexaBPM__Contact__c = con.id;
        insert insertNewSRs;
        
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].HexaBPM__Customer__c = insertNewAccounts[0].id;
        update insertNewSRs;
         
          
        //create step template
        HexaBPM__Step_Template__c listStepTemplate = new HexaBPM__Step_Template__c();
        listStepTemplate.Name             = 'RS_OFFICER_REVIEW';
        listStepTemplate.HexaBPM__Code__c = 'RS_OFFICER_REVIEW';
        listStepTemplate.HexaBPM__Step_RecordType_API_Name__c = 'RS_OFFICER_REVIEW';
        insert listStepTemplate;
         
        //create SR Step
        HexaBPM__SR_Steps__c listSRSteps      = new HexaBPM__SR_Steps__c();
        listSRSteps.HexaBPM__SR_Template__c   = createSRTemplateList[0].id;
        listSRSteps.HexaBPM__Step_Template__c = listStepTemplate.Id;
        listSRSteps.HexaBPM__Active__c=true;
        insert listSRSteps;
        
        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Approved',HexaBPM__Code__c='APPROVED',HexaBPM__Type__c='Start');
        insert status; 
      
        HexaBPM__Step__c newStep = new HexaBPM__Step__c();
        newStep.HexaBPM__SR__c =insertNewSRs[0].Id;
        newStep.HexaBPM__Due_Date__c = System.Now().addDays(2);
        newStep.HexaBPM__Step_No__c = 10;
        newStep.HexaBPM__SR_Step__c = listSRSteps.Id;
        newStep.HexaBPM__Step_Notes__c = 'test';
        newStep.HexaBPM__Status__c = status.Id;
        insert newStep;
         
           //create step template
        HexaBPM__Step_Template__c listStepTemplate1 = new HexaBPM__Step_Template__c();
        listStepTemplate1.Name             = 'ENTITY_SIGNING';
        listStepTemplate1.HexaBPM__Code__c = 'ENTITY_SIGNING';
        listStepTemplate1.HexaBPM__Step_RecordType_API_Name__c = 'RS_OFFICER_REVIEW';
        insert listStepTemplate1;
         
        //create SR Step
        HexaBPM__SR_Steps__c listSRSteps1      = new HexaBPM__SR_Steps__c();
        listSRSteps1.HexaBPM__SR_Template__c   = createSRTemplateList[0].id;
        listSRSteps1.HexaBPM__Step_Template__c = listStepTemplate.Id;
        listSRSteps1.HexaBPM__Step_RecordType_API_Name__c = 'ENTITY_SIGNING';
        listSRSteps1.HexaBPM__Active__c=true;
        insert listSRSteps1;
        
        HexaBPM__Step__c newStep1 = new HexaBPM__Step__c();
        newStep1.HexaBPM__SR__c =insertNewSRs[0].Id;
        newStep1.HexaBPM__Due_Date__c = System.Now().addDays(2);
        newStep1.HexaBPM__Step_No__c = 10;
        newStep1.HexaBPM__SR_Step__c = listSRSteps.Id;
        newStep1.HexaBPM__Step_Notes__c = 'test';
        newStep1.HexaBPM__Status__c = status.Id;
        insert newStep1;
        
        CC_AMLManagerReviewCheck thisSR = new CC_AMLManagerReviewCheck();
        thisSR.EvaluateCustomCode(insertNewSRs[0],newStep1);
         
     }
    
     static testMethod void CreateAutomaticShipments1(){
         
        List<Account> insertNewAccounts              = new List<Account>();
        insertNewAccounts                            = OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insert insertNewAccounts;
        
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 
        
        License__c lic = new License__c(name='12345',Account__c=insertNewAccounts[0].id, License_Expiry_Date__c = Date.today());
        insert lic;
        
        insertNewAccounts[0].Active_License__c = lic.id;
         
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
         
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});
        insert listSRStatus;
         
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].recordTypeId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].Passport_No__c = 'A12345';
        insertNewSRs[0].Nationality__c = 'India';
        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
        insertNewSRs[0].Gender__c = 'Male';
        insertNewSRs[0].Place_of_Birth__c = 'Dubai';
        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
        insertNewSRs[0].Last_Name__c = 'test';
        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Business_Sector__c = 'Hotel';
        insertNewSRs[0].entity_name__c = 'Tentity';
        insertNewSRs[0].Progress_Completion__c = 10.0;
        insertNewSRs[1].License_Application__c = lic.id;
        insertNewSRs[0].HexaBPM__Contact__c = con.id;
        insert insertNewSRs;
        
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].HexaBPM__Customer__c = insertNewAccounts[0].id;
        update insertNewSRs;
         
          
        //create step template
        HexaBPM__Step_Template__c listStepTemplate = new HexaBPM__Step_Template__c();
        listStepTemplate.Name             = 'RS_OFFICER_REVIEW';
        listStepTemplate.HexaBPM__Code__c = 'RS_OFFICER_REVIEW';
        listStepTemplate.HexaBPM__Step_RecordType_API_Name__c = 'RS_OFFICER_REVIEW';
        insert listStepTemplate;
         
        //create SR Step
        HexaBPM__SR_Steps__c listSRSteps      = new HexaBPM__SR_Steps__c();
        listSRSteps.HexaBPM__SR_Template__c   = createSRTemplateList[0].id;
        listSRSteps.HexaBPM__Step_Template__c = listStepTemplate.Id;
        listSRSteps.HexaBPM__Active__c=true;
        insert listSRSteps;
        
        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Approved',HexaBPM__Code__c='APPROVED',HexaBPM__Type__c='Start');
        insert status; 
      
        HexaBPM__Step__c newStep = new HexaBPM__Step__c();
        newStep.HexaBPM__SR__c =insertNewSRs[0].Id;
        newStep.HexaBPM__Due_Date__c = System.Now().addDays(2);
        newStep.HexaBPM__Step_No__c = 10;
        newStep.HexaBPM__SR_Step__c = listSRSteps.Id;
        newStep.HexaBPM__Step_Notes__c = 'test';
        newStep.HexaBPM__Status__c = status.Id;
        insert newStep;
         
           //create step template
        HexaBPM__Step_Template__c listStepTemplate1 = new HexaBPM__Step_Template__c();
        listStepTemplate1.Name             = 'SECURITY_REVIEW';
        listStepTemplate1.HexaBPM__Code__c = 'SECURITY_REVIEW';
        listStepTemplate1.HexaBPM__Step_RecordType_API_Name__c = 'SECURITY_REVIEW';
        insert listStepTemplate1;
         
        //create SR Step
        HexaBPM__SR_Steps__c listSRSteps1      = new HexaBPM__SR_Steps__c();
        listSRSteps1.HexaBPM__SR_Template__c   = createSRTemplateList[0].id;
        listSRSteps1.HexaBPM__Step_Template__c = listStepTemplate.Id;
        listSRSteps1.HexaBPM__Step_RecordType_API_Name__c = 'SECURITY_REVIEW';
        listSRSteps1.HexaBPM__Active__c=true;
        insert listSRSteps1;
        
        HexaBPM__Step__c newStep1 = new HexaBPM__Step__c();
        newStep1.HexaBPM__SR__c =insertNewSRs[0].Id;
        newStep1.HexaBPM__Due_Date__c = System.Now().addDays(2);
        newStep1.HexaBPM__Step_No__c = 10;
        newStep1.HexaBPM__SR_Step__c = listSRSteps.Id;
        newStep1.HexaBPM__Step_Notes__c = 'test';
        newStep1.HexaBPM__Status__c = status.Id;
        insert newStep1;
        
        CC_AMLManagerReviewCheck thisSR = new CC_AMLManagerReviewCheck();
        thisSR.EvaluateCustomCode(insertNewSRs[0],newStep1);
         
     }
    
     static testMethod void CreateAutomaticShipments3(){
         
        List<Account> insertNewAccounts              = new List<Account>();
        insertNewAccounts                            = OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insert insertNewAccounts;
        
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 
        
        License__c lic = new License__c(name='12345',Account__c=insertNewAccounts[0].id, License_Expiry_Date__c = Date.today());
        insert lic;
        
        insertNewAccounts[0].Active_License__c = lic.id;
         
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
         
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});
        insert listSRStatus;
         
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].recordTypeId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].Passport_No__c = 'A12345';
        insertNewSRs[0].Nationality__c = 'India';
        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
        insertNewSRs[0].Gender__c = 'Male';
        insertNewSRs[0].Place_of_Birth__c = 'Dubai';
        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
        insertNewSRs[0].Last_Name__c = 'test';
        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Business_Sector__c = 'Hotel';
        insertNewSRs[0].entity_name__c = 'Tentity';
        insertNewSRs[0].Progress_Completion__c = 10.0;
        insertNewSRs[1].License_Application__c = lic.id;
        insertNewSRs[0].HexaBPM__Contact__c = con.id;
        insert insertNewSRs;
        
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].HexaBPM__Customer__c = insertNewAccounts[0].id;
        update insertNewSRs;
         
          
        //create step template
        HexaBPM__Step_Template__c listStepTemplate = new HexaBPM__Step_Template__c();
        listStepTemplate.Name             = 'RS_OFFICER_REVIEW';
        listStepTemplate.HexaBPM__Code__c = 'RS_OFFICER_REVIEW';
        listStepTemplate.HexaBPM__Step_RecordType_API_Name__c = 'RS_OFFICER_REVIEW';
        insert listStepTemplate;
         
        //create SR Step
        HexaBPM__SR_Steps__c listSRSteps      = new HexaBPM__SR_Steps__c();
        listSRSteps.HexaBPM__SR_Template__c   = createSRTemplateList[0].id;
        listSRSteps.HexaBPM__Step_Template__c = listStepTemplate.Id;
        listSRSteps.HexaBPM__Active__c=true;
        insert listSRSteps;
        
        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Approved',HexaBPM__Code__c='APPROVED',HexaBPM__Type__c='Start');
        insert status; 
      
        HexaBPM__Step__c newStep = new HexaBPM__Step__c();
        newStep.HexaBPM__SR__c =insertNewSRs[0].Id;
        newStep.HexaBPM__Due_Date__c = System.Now().addDays(2);
        newStep.HexaBPM__Step_No__c = 10;
        newStep.HexaBPM__SR_Step__c = listSRSteps.Id;
        newStep.HexaBPM__Step_Notes__c = 'test';
        newStep.HexaBPM__Status__c = status.Id;
        insert newStep;
         
           //create step template
        HexaBPM__Step_Template__c listStepTemplate1 = new HexaBPM__Step_Template__c();
        listStepTemplate1.Name             = 'SECURITY_REVIEW';
        listStepTemplate1.HexaBPM__Code__c = 'SECURITY_REVIEW';
        listStepTemplate1.HexaBPM__Step_RecordType_API_Name__c = 'SECURITY_REVIEW';
        insert listStepTemplate1;
         
        //create SR Step
        HexaBPM__SR_Steps__c listSRSteps1      = new HexaBPM__SR_Steps__c();
        listSRSteps1.HexaBPM__SR_Template__c   = createSRTemplateList[0].id;
        listSRSteps1.HexaBPM__Step_Template__c = listStepTemplate.Id;
        listSRSteps1.HexaBPM__Step_RecordType_API_Name__c = 'SECURITY_REVIEW';
        listSRSteps1.HexaBPM__Active__c=true;
        insert listSRSteps1;
        
        HexaBPM__Step__c newStep1 = new HexaBPM__Step__c();
        newStep1.HexaBPM__SR__c =insertNewSRs[0].Id;
        newStep1.HexaBPM__Due_Date__c = System.Now().addDays(2);
        newStep1.HexaBPM__Step_No__c = 10;
        newStep1.HexaBPM__SR_Step__c = listSRSteps.Id;
        newStep1.HexaBPM__Step_Notes__c = 'test';
        newStep1.HexaBPM__Status__c = status.Id;
        insert newStep1;
        
        CC_AMLManagerReviewCheck thisSR = new CC_AMLManagerReviewCheck();
        thisSR.EvaluateCustomCode(insertNewSRs[0],newStep1);
         
     }
}