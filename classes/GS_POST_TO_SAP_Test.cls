@istest
private class GS_POST_TO_SAP_Test {
    private static testMethod void postToSapMethod(){
        SR_Template__c testSrTemplate = new SR_Template__c();    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;        
        insert testSrTemplate;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'REJECTED';
        lstStatus.add(objStatus);
        
        insert lstStatus;
        
        Step_Template__c objStepType = new Step_Template__c();
        objStepType.Name = 'Approve Request';
        objStepType.Code__c = 'APPROVE_REQUEST';
        objStepType.Step_RecordType_API_Name__c = 'General';
        objStepType.Summary__c = 'Approve Request';
        insert objStepType;
		
		SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = testSrTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Step_Template__c = objStepType.Id; // Step Type
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
	    insert objSRStep;
	    
	    Business_Rule__c objBR = new Business_Rule__c();
	    objBR.Action_Type__c = 'Default Actions';
	    objBR.Execute_on_Insert__c = true;
	    objBR.Execute_on_Update__c = true;
	    objBR.SR_Steps__c = objSRStep.Id;
	    insert objBR;
	    
	    list<BR_Objects__c> lstCSData = new list<BR_Objects__c>();
	    lstCSData.add(new BR_Objects__c(Name='Send Service To SAP',Class_Name__c='GsPricingPushCC',Method_Name__c='SendServiceToSAP',Class_Method__c=true));
        lstCSData.add(new BR_Objects__c(Name='Process Contact Details',Class_Name__c='GsContactCreation',Method_Name__c='ProcessContactDetails',Class_Method__c=true));
	    insert lstCSData;        
            
	    list<Condition__c> lstConditions = new list<Condition__c>();	
	    Condition__c objCondition = new Condition__c();
	    objCondition.Business_Rule__c = objBR.Id;
	    objCondition.Object_Name__c = 'Step__c';
	    objCondition.Field_Name__c = 'Step_Status__c';
	    objCondition.Value__c = 'Approved';
	    objCondition.Operator__c = '=';
	    objCondition.S_No__c = 1;
	    lstConditions.add(objCondition);
        
        /*objCondition = new Condition__c();
	    objCondition.Business_Rule__c = objBR.Id;
	    objCondition.Object_Name__c = 'Step__c';
	    objCondition.Field_Name__c = 'Step_Status__c';
	    objCondition.Value__c = 'Approved';
	    objCondition.Operator__c = '!=';
	    objCondition.S_No__c = 2;
	    lstConditions.add(objCondition);
		*/
	    insert lstConditions;
        
        list<Action__c> lstActions = new list<Action__c>();
        Action__c objAction = new Action__c();
        objAction.Business_Rule__c = objBR.Id;
        objAction.Action_Type__c = 'Initiate SubSR';
        objAction.Field_Value__c = objStepType.Id;
        objAction.Field_Type__c = 'string';
        objAction.Class_Name__c = 'GsPricingPushCC';
        objAction.Custom_Method__c ='SendServiceToSAP';
        lstActions.add(objAction);
        
        /*Action__c objAction1 = new Action__c();
        objAction1.Business_Rule__c = objBR.Id;
        objAction1.Action_Type__c = 'Initiate SubSR';
        objAction1.Field_Value__c = objStepType.Id;
        objAction1.Field_Type__c = 'string';
        objAction1.Class_Name__c = 'GsPricingPushCC';
        objAction1.Custom_Method__c ='SendServiceToSAP';
        lstActions.add(objAction1);
        
        Action__c objAction2 = new Action__c();
        objAction2.Business_Rule__c = objBR.Id;
        objAction2.Action_Type__c = 'Initiate SubSR';
        objAction2.Field_Value__c = objStepType.Id;
        objAction2.Field_Type__c = 'string';
        objAction2.Class_Name__c = 'GsEntryPermitUploadHandler';
        objAction2.Custom_Method__c ='SendServiceToSAP';
        lstActions.add(objAction2);
        
        Action__c objAction3 = new Action__c();
        objAction3.Business_Rule__c = objBR.Id;
        objAction3.Action_Type__c = 'Initiate SubSR';
        objAction3.Field_Value__c = objStepType.Id;
        objAction3.Field_Type__c = 'string';
        objAction3.Class_Name__c = 'CC_GSCodeCls';
        objAction3.Custom_Method__c ='Renew_POBox';
        lstActions.add(objAction3);*/
        
        insert lstActions;
        
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        //SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Step__c objStep = new Step__c();
        objStep.SR__c = SR1.Id;
        objStep.Step_No__c = 1.0;
        objStep.step_notes__c = 'abc';
        objStep.SR_Step__c=objSRStep.id;
        insert objStep;
        test.startTest();
        	GS_POST_TO_SAP.postTheCurrentRecordToSAP(objStep.Id, objSRStep.id);
        test.stopTest();
    }
    private static testMethod void postToSapMethodOne(){
        SR_Template__c testSrTemplate = new SR_Template__c();    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;        
        insert testSrTemplate;
        
        list<Status__c> lstStatus = new list<Status__c>();
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Awaiting Approval';
        objStatus.Type__c = 'Start';
        objStatus.Code__c = 'AWAITING_APPROVAL';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Approved';
        objStatus.Type__c = 'Intermediate';
        objStatus.Code__c = 'APPROVED';
        lstStatus.add(objStatus);
        
        objStatus = new Status__c();
        objStatus.Name = 'Rejected';
        objStatus.Type__c = 'End';
        objStatus.Code__c = 'REJECTED';
        lstStatus.add(objStatus);
        
        insert lstStatus;
        
        Step_Template__c objStepType = new Step_Template__c();
        objStepType.Name = 'Approve Request';
        objStepType.Code__c = 'APPROVE_REQUEST';
        objStepType.Step_RecordType_API_Name__c = 'General';
        objStepType.Summary__c = 'Approve Request';
        insert objStepType;
		
		SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.SR_Template__c = testSrTemplate.Id;
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Step_Template__c = objStepType.Id; // Step Type
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        objSRStep.Start_Status__c = lstStatus[0].Id; // Default Step Status
	    insert objSRStep;
	    
	    Business_Rule__c objBR = new Business_Rule__c();
	    objBR.Action_Type__c = 'Default Actions';
	    objBR.Execute_on_Insert__c = true;
	    objBR.Execute_on_Update__c = true;
	    objBR.SR_Steps__c = objSRStep.Id;
	    insert objBR;
	    
	    list<BR_Objects__c> lstCSData = new list<BR_Objects__c>();
	    lstCSData.add(new BR_Objects__c(Name='Send Service To SAP',Class_Name__c='GsPricingPushCC',Method_Name__c='SendServiceToSAP',Class_Method__c=true));
        lstCSData.add(new BR_Objects__c(Name='Process Contact Details',Class_Name__c='GsContactCreation',Method_Name__c='ProcessContactDetails',Class_Method__c=true));
	    insert lstCSData;        
            
	    list<Condition__c> lstConditions = new list<Condition__c>();	
	    Condition__c objCondition = new Condition__c();
	    objCondition.Business_Rule__c = objBR.Id;
	    objCondition.Object_Name__c = 'Step__c';
	    objCondition.Field_Name__c = 'Step_Status__c';
	    objCondition.Value__c = 'Approved';
	    objCondition.Operator__c = '=';
	    objCondition.S_No__c = 1;
	    lstConditions.add(objCondition);
        
        /*objCondition = new Condition__c();
	    objCondition.Business_Rule__c = objBR.Id;
	    objCondition.Object_Name__c = 'Step__c';
	    objCondition.Field_Name__c = 'Step_Status__c';
	    objCondition.Value__c = 'Approved';
	    objCondition.Operator__c = '!=';
	    objCondition.S_No__c = 2;
	    lstConditions.add(objCondition);
		*/
	    insert lstConditions;
        
        list<Action__c> lstActions = new list<Action__c>();
        Action__c objAction = new Action__c();
        objAction.Business_Rule__c = objBR.Id;
        objAction.Action_Type__c = 'Initiate SubSR';
        objAction.Field_Value__c = objStepType.Id;
        objAction.Field_Type__c = 'string';
        objAction.Class_Name__c = 'GsContactCreation';
        objAction.Custom_Method__c ='ProcessContactDetails';
        lstActions.add(objAction);
        
        /*Action__c objAction1 = new Action__c();
        objAction1.Business_Rule__c = objBR.Id;
        objAction1.Action_Type__c = 'Initiate SubSR';
        objAction1.Field_Value__c = objStepType.Id;
        objAction1.Field_Type__c = 'string';
        objAction1.Class_Name__c = 'GsPricingPushCC';
        objAction1.Custom_Method__c ='SendServiceToSAP';
        lstActions.add(objAction1);
        
        Action__c objAction2 = new Action__c();
        objAction2.Business_Rule__c = objBR.Id;
        objAction2.Action_Type__c = 'Initiate SubSR';
        objAction2.Field_Value__c = objStepType.Id;
        objAction2.Field_Type__c = 'string';
        objAction2.Class_Name__c = 'GsEntryPermitUploadHandler';
        objAction2.Custom_Method__c ='SendServiceToSAP';
        lstActions.add(objAction2);
        
        Action__c objAction3 = new Action__c();
        objAction3.Business_Rule__c = objBR.Id;
        objAction3.Action_Type__c = 'Initiate SubSR';
        objAction3.Field_Value__c = objStepType.Id;
        objAction3.Field_Type__c = 'string';
        objAction3.Class_Name__c = 'CC_GSCodeCls';
        objAction3.Custom_Method__c ='Renew_POBox';
        lstActions.add(objAction3);*/
        
        insert lstActions;
        
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        //SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Step__c objStep = new Step__c();
        objStep.SR__c = SR1.Id;
        objStep.Step_No__c = 1.0;
        objStep.step_notes__c = 'abc';
        objStep.SR_Step__c=objSRStep.id;
        insert objStep;
        test.startTest();
        	GS_POST_TO_SAP.postTheCurrentRecordToSAP(objStep.Id, objSRStep.id);
        test.stopTest();
    }
    
    private static testMethod void createAnAirwayBillMethod(){
        SR_Template__c testSrTemplate = new SR_Template__c();    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;        
        insert testSrTemplate;
        
        SR_Steps__c srstep = new SR_Steps__c();
        insert srstep;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        //SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Step__c objStep = new Step__c();
        objStep.SR__c = SR1.Id;
        objStep.Step_No__c = 1.0;
        objStep.step_notes__c = 'abc';
        objStep.SR_Step__c=srstep.id;
        //objStep.SR_Group__c='Fit-Out & Events';
        insert objStep;  
        test.startTest();
        	string returnedStr = GS_POST_TO_SAP.createAnAirwayBill(objStep.id);
        test.stopTest();
        
    }
}