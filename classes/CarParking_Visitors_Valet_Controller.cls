public class CarParking_Visitors_Valet_Controller {
    public static SearchResults searchResObj = new SearchResults();
    public static String dueAmount = '';
    public static Integer gracePeriod = 0;
    public static Boolean hasChildNode = true;
    @AuraEnabled
    public static String getBaseURL(){
        return Site.getBaseRequestUrl();
    }
    @AuraEnabled
    public static PaymentWrapper fetchDetailsFromSparkApp(String carParkDetStr){
        Car_Parking_Details_Visitor_Valet__c carParkDet = (Car_Parking_Details_Visitor_Valet__c)JSON.deserialize(carParkDetStr, Car_Parking_Details_Visitor_Valet__c.class);
        JSONRequestWrapper jsonReqObj = new JSONRequestWrapper();
        jsonReqObj.DIFC_TID = '1';//Need to get this value for production
        jsonReqObj.Card_Number = carParkDet.Card_Number__c;
        jsonReqObj.Entity_ID = '3';//Need to get this value for production
        jsonReqObj.Location_ID = 4;//Need to get this value for production
        jsonReqObj.RDateTime = System.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss');
        String jsonReq = JSON.serialize(jsonReqObj);
        HttpResponse response = fetchDetailsFromSpark(jsonReq, 'https://sparkappae.tech:3443/DIFC_Spark_GetValetTotalAmountDue', carParkDet);
        return parseResponse(response, carParkDet);
    }
    
    public static HttpResponse fetchDetailsFromSpark(String reqBody, String endPointURL, Car_Parking_Details_Visitor_Valet__c carParkDet){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endPointURL);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json');
        request.setHeader('Token', 'lcX!beb[X{4~iNL:y0D}.0&+N)p9J*ChsVd/8}h2RYg%gcU`XjiF@w,_I0:beoE');//Need to get this value for production
        //request.setBody('{"card":"'+carParkDet.Card_Number__c+'"}');
        request.setBody(reqBody);
        system.debug('Request: '+request);
        HttpResponse response = http.send(request);
        System.debug('status: '+response.getStatusCode()+', response: '+response.getBody());        
        //return parseResponse(response, carParkDet);
        return response;
    }
    
    public static PaymentWrapper parseResponse(HttpResponse response, Car_Parking_Details_Visitor_Valet__c carParkDet){
        PaymentWrapper wrapperObj = new PaymentWrapper();
        wrapperObj.hasError = false;
        wrapperObj.errorMessage = '';
        if(response.getStatusCode() == 200 || response.getStatusCode() == 201){
            System.debug('response: '+response.getBody());
            string resBody = response.getBody();
            resBody = resBody.contains('"Currency"') ? resBody.replace('"Currency"','"Currency_Z"') : '"Currency"';
            JSONResponseWrapper obj = (JSONResponseWrapper)JSON.deserialize(response.getBody(), JSONResponseWrapper.class);
            if(obj.amount != null){
                carParkDet.Valet_Transaction_Id__c = String.valueOf(obj.SPARK_TID);
                carParkDet.Amount__c = obj.Amount;
                carParkDet.Entry_Time__c = DateTime.valueOf(obj.Arrival.replace('T', ' '));
                carParkDet.Location__c = String.valueOf(obj.Location_ID);
                carParkDet.Requested_DateTime__c = DateTime.valueOf(obj.RDateTime.replace('T', ' '));
                carParkDet.Hours__c = obj.Hours;
                //carParkDet.Exit_Time__c = DateTime.valueOf(obj.Departure.replace('T', ' '));
                wrapperObj.carParkDetObj = carParkDet;
            }
            else{
                wrapperObj.hasError = true;
                wrapperObj.errorMessage = 'Oops!! Something went wrong. Please contact support.';
                wrapperObj.carParkDetObj = carParkDet;
            }
        }
        else if(response.getStatusCode() == 400){
            wrapperObj.hasError = true;
            wrapperObj.errorMessage = 'Vehicle not found. Please contact support.';
            wrapperObj.carParkDetObj = carParkDet;
        }
        else{
            wrapperObj.hasError = true;
            wrapperObj.errorMessage = 'Oops!! Something went wrong. Please contact support.';
            wrapperObj.carParkDetObj = carParkDet;
        }
        return wrapperObj;
    }
    
    @AuraEnabled
    public static PaymentWrapper fetchCarParkDetails(String carParkDetStr){
        Car_Parking_Details_Visitor_Valet__c carParkDet = (Car_Parking_Details_Visitor_Valet__c)JSON.deserialize(carParkDetStr, Car_Parking_Details_Visitor_Valet__c.class);
        Map<String, List<Car_Parking_API_Service_fields__mdt>> servieFieldMdtMap = 
            new Map<String, List<Car_Parking_API_Service_fields__mdt>>();
        PaymentWrapper wrapperObj = new PaymentWrapper();
        try{
            List<Car_Parking_API_Service_fields__mdt> serviceFields = 
                [SELECT Request_Name__c, Sequence__c, Namespace__c, 
                 Parent_Node__c, Type__c, Xml_Label__c, Source_Object__c, 
                 Field_Name__c FROM Car_Parking_API_Service_fields__mdt WHERE 
                 Request_Name__c in ('getCardsByWildcardSearch', 'getCardInfo') ORDER BY Sequence__c];
            if(serviceFields.size()>0){
                for(Car_Parking_API_Service_fields__mdt servFieldMdt: serviceFields){
                    if(servieFieldMdtMap.containsKey(servFieldMdt.Request_Name__c))
                        servieFieldMdtMap.get(servFieldMdt.Request_Name__c).add(servFieldMdt);
                    else
                        servieFieldMdtMap.put(servFieldMdt.Request_Name__c, new  List <Car_Parking_API_Service_fields__mdt> { servFieldMdt });
                }
                Dom.XMLNode rootEle = generatexmlRequest(servieFieldMdtMap.get('getCardsByWildcardSearch'), carParkDet,'getCardsByWildcardSearch');
                if(rootEle != null){
                    iterateThroughXML(rootEle, 'getCardsByWildcardSearchResult',carParkDet.Emirates__c);
                    System.debug('searchResObj: '+searchResObj);
                    
                    if(hasChildNode && searchResObj != null && searchResObj.CardCarrierNrId != null && searchResObj.CardCarrierNrId != ''){
                        System.debug('Eneterd in if');
                        carParkDet.Entry_Time__c = DateTime.valueOf(searchResObj.timeValidFrom.replace('T',' '));
                        carParkDet.Address__c = searchResObj.name;
                        carParkDet.Ticket_Number__c = searchResObj.CardCarrierNrId;
                        Dom.XMLNode rootEle1 = generatexmlRequest(servieFieldMdtMap.get('getCardInfo'), carParkDet,'getCardInfo');
                        if(rootEle1 != null){
                            iterateThroughXML(rootEle1, 'getCardInfoResult', carParkDet.Ticket_Number__c);
                            if(hasChildNode){
                                carParkDet.Amount__c = Decimal.valueOf(dueAmount);
                                carParkDet.Grace_Period__c = gracePeriod;
                                wrapperObj.hasError = false;
                                wrapperObj.errorMessage = '';
                                wrapperObj.carParkDetObj = carParkDet;
                                //return carParkDet;
                            }
                            else{
                                wrapperObj.hasError = true;
                                wrapperObj.errorMessage = 'Please contact Car Parking support team.';
                                wrapperObj.carParkDetObj = carParkDet;
                                //return null;
                            }
                        }
                        else {
                            wrapperObj.hasError = true;
                            wrapperObj.errorMessage = 'Please contact Car Parking support team.';
                            wrapperObj.carParkDetObj = carParkDet;
                            //return null;
                        }
                    }
                    else{
                        wrapperObj.hasError = true;
                        wrapperObj.errorMessage = 'Vehicle not found. Please contact Car Parking support team.';
                        wrapperObj.carParkDetObj = carParkDet;
                        //return null;
                    }
                }
                else{
                    wrapperObj.hasError = true;
                    wrapperObj.errorMessage = 'Please contact Car Parking support team.';
                    wrapperObj.carParkDetObj = carParkDet;
                    //return null;
                }
            }
            else{
                wrapperObj.hasError = true;
                wrapperObj.errorMessage = 'Please contact Car Parking support team.';
                wrapperObj.carParkDetObj = carParkDet;
                //return null;
            }
        }
        Catch(Exception ex){
            wrapperObj.hasError = true;
            wrapperObj.errorMessage = 'Please contact Car Parking support team.';
            wrapperObj.carParkDetObj = carParkDet;
            //return null;
        }
        return wrapperObj;
    }
    
    public static Dom.XMLNode generatexmlRequest(List<Car_Parking_API_Service_fields__mdt> serviceFields, 
                                                 Car_Parking_Details_Visitor_Valet__c carParkDet, String servName)
    {
        Car_Parking_Services_Namespace__mdt servNS = Car_Parking_Services_Namespace__mdt.getInstance(servName);
        System.debug('servNS: '+servNS.End_point_URL__c);
        
        DOM.Document doc = new DOM.Document();
        String soapNS = servNS.Soap_Namespace__c;
        String xsi = servNS.XSI__c;
        String xsd = servNS.XSD__c;
        String serviceNS = servNS.Service_Namespace__c;
        dom.XmlNode envelope
            = doc.createRootElement('Envelope', soapNS, 'soap');
        envelope.setNamespace('xsi', xsi);
        envelope.setNamespace('xsd', xsd);
        dom.XmlNode body = envelope.addChildElement('Body', soapNS, null);
        Map<string, dom.xmlnode> xmlnodemap = new Map<string, dom.xmlnode>();
        xmlnodemap.put('body', body);
        for(Car_Parking_API_Service_fields__mdt field : serviceFields){
            if(field.Type__c == 'XmlNode'){
                if(field.Parent_Node__c == 'body'){
                    xmlnodemap.put(field.Xml_Label__c, xmlnodemap.get(field.parent_Node__c).
                                   addChildElement(field.Xml_Label__c, serviceNS, ''));
                }
            }
            else if(field.Type__c == 'Field'){
                String innerText = '';
                Boolean isNull = true;
                
                if(field.xml_Label__c == 'user' || field.xml_Label__c == 'UserID'){
                    DOM.XmlNode xmlField = xmlnodemap.get(field.parent_Node__c).addChildElement(field.xml_Label__c, serviceNS, null);
                    xmlField.addTextNode(EncodingUtil.base64Decode(servNS.Username__c).toString());
                }
                else if(field.xml_Label__c == 'pwd' || field.xml_Label__c == 'UserPWD'){
                    DOM.XmlNode xmlField = xmlnodemap.get(field.parent_Node__c).addChildElement(field.xml_Label__c, serviceNS, null);
                    xmlField.addTextNode(EncodingUtil.base64Decode(servNS.Password__c).toString());
                }
                else{
                    if(field.Field_Name__c != null && field.Field_Name__c != ''){
                        String fieldRequired = field.Field_Name__c;
                        sObject carParkDetRec = carParkDet;
                        Object value = carParkDetRec.get(fieldRequired);
                        if(value != null){
                            innerText = String.valueOf(value);
                            isNull = false;
                        }
                        if(!isNull){
                            if(field.Field_Name__c == 'Exit_Time__c'){
                                String datevalue='';
                                DateTime dt = carParkDetRec.get(fieldRequired)!=null? DateTime.valueOf(carParkDetRec.get(fieldRequired)) : null;
                                datevalue =  dt!=null ? dt.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss'):'';
                                DOM.XmlNode xmlField = xmlnodemap.get(field.parent_Node__c).addChildElement(field.xml_Label__c, serviceNS, null);
                                xmlField.addTextNode(datevalue); 
                            }
                            else{
                            DOM.XmlNode xmlField = xmlnodemap.get(field.parent_Node__c).addChildElement(field.xml_Label__c, serviceNS, null);
                            xmlField.addTextNode(innerText); 
                            }
                        }
                    }
                    else{
                        DOM.XmlNode xmlField = xmlnodemap.get(field.parent_Node__c).addChildElement(field.xml_Label__c, serviceNS, null);
                        xmlField.addTextNode(innerText); 
                    }
                }
            }            
            else if(field.Type__c == 'Constant'){
                DOM.XmlNode xmlField = xmlnodemap.get(field.parent_Node__c).addChildElement(field.xml_Label__c, serviceNS, null);
                xmlField.addTextNode(field.Field_Name__c); 
            }
        }
        System.debug('request: '+doc.toXmlString());
        
        httpRequest httpRequest = new httpRequest();
        httpRequest.setHeader('Content-type','text/xml');
        Http httpObject = new Http();
        HttpResponse httpResponse;
        httpRequest.setHeader('Content-length', '0');
        httpRequest.setEndpoint(servNS.End_point_URL__c);
        httpRequest.setMethod('POST');
        httpRequest.setTimeout(120000);
        httpRequest.setBodyDocument(doc);
        httpResponse = httpObject.send(httpRequest);
        Dom.XMLNode rootEle;
        if(httpResponse.getStatusCode() == 200) {
            system.debug('Response 200 - '+httpResponse.getBody());
            Dom.Document docRes = httpResponse.getBodyDocument(); 
            rootEle = docRes.getRootElement();       
            return rootEle;
        }else{
            return null;
        }        
    }
    public static void iterateThroughXML(DOM.XMLNode node, String servName, String param1){
        if (node.getNodeType() == DOM.XMLNodeType.ELEMENT){            
            if(node.getName() == servName){
                System.debug('entered here');
                if(node.getChildElements().size()>0){
                    hasChildNode = true;
                    if(servName =='getCardsByWildcardSearchResult'){
                        searchResObj.process(node, param1);
                    }
                    else{
                        if(node.getChildElement('AmountDueCurrency', 'http://www.designa.de/') != null){                    
                            dueAmount = node.getChildElement('AmountDueCurrency', 'http://www.designa.de/').getText();                        
                        }
                        if(node.getChildElement('GracePeriod', 'http://www.designa.de/') != null){
                            Integer timeToExit = Integer.valueOf(node.getChildElement('GracePeriod', 'http://www.designa.de/').getText())/60;
                            gracePeriod = timeToExit >= 1 ? timeToExit : 1;
                        }
                    }                    
                }
                else{
                    hasChildNode = false;
                }
            }            
            for (Dom.XMLNode child: node.getChildElements())
                iterateThroughXML(child, servName, param1);
        }
    }
    
    public class SearchResults{
        public String lastCountryCode;
        public String name;
        public String CardCarrierNrId;
        public String timeValidFrom;
        public void process(Dom.XmlNode inputNode, String countryCode){
            String currentNodeName;            
            for(Dom.XmlNode childNode :inputNode.getChildElements()){
                currentNodeName = childNode.getName();
                if(currentNodeName == 'CardData'){
                    if(childNode.getChildElement('LastCountryCode', 'http://www.designa.de/').getText().toLowerCase() == countryCode.toLowerCase()){
                        lastCountryCode = childNode.getChildElement('LastCountryCode', 'http://www.designa.de/').getText();
                        timeValidFrom = childNode.getChildElement('TimeValidFrom', 'http://www.designa.de/').getText();
                        name = childNode.getChildElement('Carpark', 'http://www.designa.de/').getAttributeValue('Name', '');
                        for(Dom.XmlNode subChildNode : childNode.getChildElement('CardCarriers', 'http://www.designa.de/').getChildElements()){
                            if(subChildNode.getName() == 'CardCarrierData'){
                                CardCarrierNrId = subChildNode.getChildElement('CardCarrierNrId', 'http://www.designa.de/').getText();
                                break;
                            }
                        }
                        break;
                    }
                }
            }
        }
    }
    @AuraEnabled
    public static String fetchAmount(String carParkDetStr){
        Car_Parking_Details_Visitor_Valet__c carParkDet = (Car_Parking_Details_Visitor_Valet__c)
            JSON.deserialize(carParkDetStr, Car_Parking_Details_Visitor_Valet__c.class);
        List<Car_Parking_API_Service_fields__mdt> serviceFields = 
                [SELECT Request_Name__c, Sequence__c, Namespace__c, 
                 Parent_Node__c, Type__c, Xml_Label__c, Source_Object__c, 
                 Field_Name__c FROM Car_Parking_API_Service_fields__mdt WHERE 
                 Request_Name__c = 'getCardInfo' ORDER BY Sequence__c];
        try{
            Dom.XMLNode rootEle1 = generatexmlRequest(serviceFields, carParkDet,'getCardInfo');
            if(rootEle1 != null){
                iterateThroughXML(rootEle1, 'getCardInfoResult', carParkDet.Ticket_Number__c);                
            }
            return dueAmount;
        }
        Catch(Exception ex){
            return 'Error: '+ex.getMessage();
        } 
    }
    @AuraEnabled
    public static String createRecords(String carParkDetStr){
        try{
            Car_Parking_Details_Visitor_Valet__c carParkDet = (Car_Parking_Details_Visitor_Valet__c)JSON.deserialize(carParkDetStr, Car_Parking_Details_Visitor_Valet__c.class);
            insert carParkDet;
            return carParkDet.Id;
        }
        Catch(Exception ex){
            return ex.getMessage();
        }        
    }
    
    @AuraEnabled//(cacheable=true)
    public static Car_Parking_Receipt__c getReceiptRecord(String recId){
        
        return [Select Id,Name, Payment_Status__c, 
                Payment_Gateway_Ref_No__c,Amount__c,
                Card_Type__c, Transaction_Date__c
                From Car_Parking_Receipt__c Where Id=:recId LIMIT 1];
    }
    
    public class PaymentWrapper{
        @AuraEnabled public Boolean hasError;
        @AuraEnabled public String errorMessage;
        @AuraEnabled public Car_Parking_Details_Visitor_Valet__c carParkDetObj;
    }
    
    public class JSONRequestWrapper{
        @AuraEnabled public String DIFC_TID;
        @AuraEnabled public string Card_Number;
        @AuraEnabled public String Entity_ID;
        @AuraEnabled public Integer Location_ID;
        @AuraEnabled public String RDateTime;
    }
    
    public class JSONResponseWrapper{
        @AuraEnabled public String DIFC_TID;
        @AuraEnabled public String SPARK_TID;
        @AuraEnabled public string Card_Number;
        @AuraEnabled public String Entity_ID;
        @AuraEnabled public Integer Location_ID;
        @AuraEnabled public String RDateTime;
        @AuraEnabled public Integer Amount;
        @AuraEnabled public String Currency_Z;
        @AuraEnabled public Integer Hours;
        @AuraEnabled public String Arrival;
        @AuraEnabled public String Departure;
        @AuraEnabled public Boolean validated;
        @AuraEnabled public Integer validatedBy;
        @AuraEnabled public String ValidationDateTime;
        @AuraEnabled public Integer ResponseStatus;
        @AuraEnabled public String ResponseResult;
        @AuraEnabled public Boolean Payment_Status;
    }
}