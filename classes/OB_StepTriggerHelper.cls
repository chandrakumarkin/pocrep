/******************************************************************************************
 *  Name        : OB_StepTriggerHelper
 *  Author      : Durga Kandula
 *  Company     : PwC
 *  Date        : 17-Dec-2019
 *  Description : Trigger helper for OB_StepTriggerHandler
 ----------------------------------------------------------------------------------------                               
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   30-Oct-2019  Durga         Initial Version
*******************************************************************************************/
public without sharing class OB_StepTriggerHelper{
    public static void CloseOpenStepsonRejection(string SRID,string StepId){
        list<HexaBPM__Step__c> lstStepsToClose = new list<HexaBPM__Step__c>();
        for(HexaBPM__Step__c stp:[Select Id,HexaBPM__Status__c from HexaBPM__Step__c where HexaBPM__SR__c=:SRID and Id!=:StepId and HexaBPM__Status_Type__c!='End']){
        	lstStepsToClose.add(stp);
        }
        if(lstStepsToClose.size()>0){
        	string RejectedStatus = '';
        	for(HexaBPM__Status__c stepStatus:[Select Id from HexaBPM__Status__c where HexaBPM__Code__c='REQUEST_REJECTED' and HexaBPM__Rejection__c=true]){
        		RejectedStatus = stepStatus.Id;
        	}
        	if(RejectedStatus!=''){
        		for(HexaBPM__Step__c stp:lstStepsToClose){
        			stp.HexaBPM__Status__c = RejectedStatus;
        		}
        		update lstStepsToClose;
        	}
        }
    }
}