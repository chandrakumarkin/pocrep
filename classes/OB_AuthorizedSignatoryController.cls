/**
 * Description : Controller for OB_AuthorizedSignatory component
 *
 * ****************************************************************************************
 * History :
 * [24.APRL.2020] Prateek Kadkol - CP Logic
 */
public without sharing class OB_AuthorizedSignatoryController {

	@AuraEnabled
	public static RespondWrap fetchAmendRec(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		system.debug(reqWrap);

		respWrap.recordId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
		respWrap.amendWrappList = FetchUpdatedAmendListHelper(reqWrap.srId);

		for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
												    AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
			respWrap.ButtonSection = sectionObj;
		}

	 for(HexaBPM__Service_Request__c serReq :[SELECT id,HexaBPM__Record_Type_Name__c,
		 Authorized_Signatory_Combinations__c, recordtype.developername FROM HexaBPM__Service_Request__c
		 WHERE id = :reqWrap.srId
		 LIMIT 1
		 ]) {
			respWrap.srObj =  serReq;
		 }

	/*
		 respWrap.srWrap = new SRWrapper();
		 respWrap.srWrap.srObj = serReq;

		 String flowId = reqWrap.flowId;
		 if(respWrap.srWrap.srObj != null && respWrap.srWrap.srObj.Id != null) {
		 respWrap.srWrap.isDraft = OB_QueryUtilityClass.checkSRStatusDraft(respWrap.srWrap.srObj);
		 // if SR status is not draft then redirect to ob-reviewandconfirm lightning page
		 if(!respWrap.srWrap.isDraft) {
		 system.debug('=====flowId====' + flowId);
		 respWrap.srWrap.viewSRURL = PageFlowControllerHelper.communityURLFormation(respWrap.srWrap.srObj, flowId, 'ob-reviewandconfirm');
		 system.debug('=====viewSRURL====' + respWrap.srWrap.viewSRURL);
		 }
		 } else {
		 respWrap.srWrap.isDraft = false;
		 }
		 } */



		return respWrap;

	}

	@AuraEnabled
	public static RespondWrap initNewAmendment(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

		AmendmentWrapper amedWrap = new AmendmentWrapper();
    

		string recordId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();

		respWrap.recordId = recordId;




		HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c();
		amedObj.ServiceRequest__c = reqWrap.srId;
		amedObj.RecordTypeId = recordId;
		amedObj.Role__c = 'Authorised Signatory';

		amedWrap.amedObj = amedObj;
		//respWrap.newAmendToCreate = amedObj;

		respWrap.newAmendWrappToCreate = amedWrap;

		return respWrap;

		
	}

	@AuraEnabled
	public static amendWrappList FetchUpdatedAmendListHelper(String srId) {

		amendWrappList respWrap = new amendWrappList();

		String indRecTyp = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Individual');
    String  bodyRecTyp = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Body_Corporate');

		//* v1.0 Merul  26 march 2020  Libary implementation
		Map<Id,AccountContactRelation> mapRelatedAccForAmed = new Map<Id,AccountContactRelation>();
		Map<Id,AccountContactRelation> mapRelatedAccForRelshp = new Map<Id,AccountContactRelation>();
		
		//* v1.0 Merul  26 march 2020  Libary implementation
		HexaBPM__Service_Request__c currentSR = new HexaBPM__Service_Request__c();
                
		// * v1.0 Merul  26 march 2020  Libary implementation
		Map<String,AmendmentWrapper> mapExistingAmed = new  Map<String,AmendmentWrapper>();
		Map<String,AmendmentWrapper> mapShareholder = new  Map<String,AmendmentWrapper>();
		Map<String,AmendmentWrapper> mapRelShpToAmedWrap = new  Map<String,AmendmentWrapper>();
		Map<String,AmendmentWrapper> mapShareholderCombine = new  Map<String,AmendmentWrapper>();

		list<HexaBPM_Amendment__c> libAmendmentList = new list<HexaBPM_Amendment__c>();

		 // getting all related account based on current loggedin user.
		 User loggedInUser = new User();
		 String contactId;

		 for(User UserObj : [
														 SELECT id,
																 contactid,
																 Contact.AccountId,
																 ProfileId 
														 FROM User 
														 WHERE Id = :UserInfo.getUserId() 
														 
														 LIMIT 1
												 ]
				 ) 
		 {
				 loggedInUser =  UserObj;
				 contactId = UserObj.contactid;
		 }

		 
		 // getting account contact relation ship
		 for( 
				 AccountContactRelation accConRel :  [
																								 SELECT Id,
																												 AccountId,
																												 ContactId,
																												 Roles,
																												 Access_Level__c,
																												 Account.ROC_Status__c
																								 FROM AccountContactRelation
																								 WHERE ContactId =:contactId
																								 AND Roles includes ('Company Services')
																				 
																						 ]
		 )
		 {
				 if( accConRel.Account.ROC_Status__c == 'Under Formation' 
										 || accConRel.Account.ROC_Status__c == 'Account Created' )    
										 {
												 mapRelatedAccForAmed.put( accConRel.AccountId , accConRel);
										 }
				 
				 if( accConRel.Account.ROC_Status__c == 'Active' 
										 || accConRel.Account.ROC_Status__c == 'Not Renewed' )    
										 {
												 mapRelatedAccForRelshp.put( accConRel.AccountId , accConRel);
										 }
		 }

		 // getting relationship
		 
		 set<id> addedContacts = new set<id>();
		 set<id> addedAccounts = new set<id>();

		 List<Relationship__c> lstRel = OB_libraryHelper.getRelationShip( mapRelatedAccForRelshp );
		 for(Relationship__c relShip : lstRel)                           
		 {
				 system.debug('############## relShip '+relShip);
				 AmendmentWrapper amedWrap = new AmendmentWrapper();
				 amedWrap.amedObj = new HexaBPM_Amendment__c();
				 if(relShip.Amendment_ID__c != null){
					amedWrap.relatedAmendID = relShip.Amendment_ID__c;
				}else{
					amedWrap.relatedAmendID = relShip.Sys_Amendment_Id__c;
				}
				 amedWrap.RelatedRelId = relShip.id;

				 // amedWrap.lookupLabel = (amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name :'');
				 if( relShip.Object_Contact__c != NULL )
				 {
						 if(!addedContacts.contains(relShip.Object_Contact__c)){

								addedContacts.add(relShip.Object_Contact__c);
								amedWrap.amedObj = OB_libraryHelper.setAmedFromObjContact(amedWrap.amedObj,relShip);
								amedWrap.isIndividual = true;
								amedWrap.displayName = (String.isNotBlank( amedWrap.amedObj.First_Name__c) ? amedWrap.amedObj.First_Name__c : '') +' '+  (String.isNotBlank( amedWrap.amedObj.Last_Name__c) ? amedWrap.amedObj.Last_Name__c : '');
								//amedWrap.amedObj.recordTypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Individual');
								amedWrap.amedObj.recordTypeId = indRecTyp;
								
								system.debug('@@@@@@@@@@@   amedWrap.amedObj.recordTypeId  '+ amedWrap.amedObj.recordTypeId );

								String passport = ( String.isNotBlank( amedWrap.amedObj.Passport_No__c )  ? amedWrap.amedObj.Passport_No__c : '' );
								String nationality = ( String.isNotBlank( amedWrap.amedObj.Nationality_list__c )  ? amedWrap.amedObj.Nationality_list__c : '' ); 
								String uniqKey = passport + nationality;
								if( String.isNotBlank( uniqKey  ) )
								{
										mapRelShpToAmedWrap.put( uniqKey.toLowercase() , amedWrap);
								}
						 }
						 
																	 

				 }
				 else if( relShip.Object_Account__c != NULL )
				 {
						 
					if(!addedAccounts.contains(relShip.Object_Account__c)){
						addedAccounts.add(relShip.Object_Account__c);
						amedWrap.amedObj = OB_libraryHelper.setAmedFromObjAccount(amedWrap.amedObj,relShip);
						amedWrap.isBodyCorporate = true;
						amedWrap.displayName = amedWrap.amedObj.Company_Name__c;
						//amedWrap.amedObj.recordTypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Body_Corporate');
						amedWrap.amedObj.recordTypeId = bodyRecTyp;

						if( String.isNotBlank( amedWrap.amedObj.Registration_No__c  ) )
						{
								mapRelShpToAmedWrap.put( amedWrap.amedObj.Registration_No__c.toLowercase(), amedWrap);
						}
					}
						
						 
				 }

		 }

		 set<string> dupAmendId = new set<string>();
		 List<HexaBPM__Service_Request__c> lstSerReq =   OB_libraryHelper.getSR(srId,mapRelatedAccForAmed);
                    for(HexaBPM__Service_Request__c serObj :  lstSerReq) 
                    {
                            system.debug('===inside service request===');

                            /* if( serObj.Id == srId )
                            { */
                                    
                                    for(HexaBPM_Amendment__c amed :serObj.Amendments__r) 
                                    {

																			String passport = ( String.isNotBlank( amed.Passport_No__c )  ? amed.Passport_No__c : '' );
																			String nationality = ( String.isNotBlank( amed.Nationality_list__c )  ? amed.Nationality_list__c : '' ); 
																			String uniqKey = passport + nationality;

																			if(serObj.Id != srId ){
																				dupAmendId.add(uniqKey);
																			}
																			
																				if(!(serObj.Id != srId && dupAmendId.contains(uniqKey))){

                                        system.debug('===inside amendment===');
                                        AmendmentWrapper amedWrap = new AmendmentWrapper();
																				amedWrap.amedObj = amed;
																				amedWrap.relatedAmendID = amed.Id;
																				if(serObj.Id != srId){
																						amedWrap.amedObj.Id = null;
																				}
                                        
                                        amedWrap.serviceRequest = serObj;
                                        amedWrap.lookupLabel = amed.Entity_Name__r.Name;
                                        amedWrap.isIndividual = (amed.RecordType.DeveloperName == 'Individual' ? true :false);
																				amedWrap.isBodyCorporate = (amed.RecordType.DeveloperName == 'Body_Corporate' ? true :false);
																				amedWrap.displayName = (String.isNotBlank( amedWrap.amedObj.First_Name__c) ? amedWrap.amedObj.First_Name__c : '') +' '+  (String.isNotBlank( amedWrap.amedObj.Last_Name__c) ? amedWrap.amedObj.Last_Name__c : '');
                                        //amedWrap.lookupLabel = (amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name :'');

																				if(amedWrap.amedObj.Role__c != null && serObj.Id == srId && amedWrap.isIndividual){
																					list<string> amendRole = amedWrap.amedObj.Role__c.split(';');
																					if( amendRole.contains('Authorised Signatory')) {
																						respWrap.AddedAmendList.add(amedWrap);
																					} else if(!amendRole.contains('Authorised Signatory') && amendRole.size() > 0 && !amendRole.contains('DPO') && !amendRole.contains('Operating Location')) {
																						respWrap.AmendListForPicklist.add(amedWrap);
																					}
																				}else if(amedWrap.amedObj.Role__c != null && amedWrap.isIndividual){
																					respWrap.AmendListForPicklist.add(amedWrap);
																				}
                                        
																			}
																				
                                    }
                            /* } */
                    } 

									
		 
										if( mapRelShpToAmedWrap.size() > 0 )
                    {
											respWrap.AmendListForPicklist.addAll( mapRelShpToAmedWrap.values() );
                    }


		return respWrap;


	/* 	amendListWrapp respWrap = new amendListWrapp();

		string recordId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();



		for (HexaBPM_Amendment__c amendObj :[SELECT id, Name__c, Title__c, First_Name__c, Middle_Name__c, Last_Name__c, Former_Name__c, Passport_No__c, Nationality_list__c, Date_of_Birth__c, Gender__c, Place_of_Birth__c, Passport_Issue_Date__c, Passport_Expiry_Date__c, Type_of_Authority__c, Please_provide_the_type_of_authority__c, ServiceRequest__c, 
										     Place_of_Issue__c, Telephone_No__c, Email__c, Is_this_individual_a_PEP__c, Address__c, Apartment_or_Villa_Number_c__c, Building_Name__c, I_agree_Authorized_Represntative__c, 
										     PO_Box__c, Permanent_Native_Country__c, Permanent_Native_City__c, Emirate_State_Province__c, 
										     Post_Code__c, Role__c,Relationship_ID__c,cessation_date__c FROM HexaBPM_Amendment__c WHERE ServiceRequest__c = :srId AND
										     RecordTypeId = :recordId AND Role__c != null AND cessation_date__c = null]) {
			list<string> amendRole = amendObj.Role__c.split(';');

			if(amendRole.contains('Authorised Signatory') && amendRole.size() == 1) {
				respWrap.onlyCurrentRoleAmendList.add(amendObj);
			} else if(amendRole.contains('Authorised Signatory') && amendRole.size() > 1) {
				respWrap.currentRoleAndOtherAmendList.add(amendObj);
			} else if(!amendRole.contains('Authorised Signatory') && amendRole.size() > 0 && !amendRole.contains('DPO') && !amendRole.contains('Operating Location')) {
				respWrap.otherRolesAmendList.add(amendObj);
			}
		}



		return respWrap; */

	}

	@AuraEnabled
	public static RespondWrap updateCessationDate(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		system.debug(reqWrap);

		HexaBPM_Amendment__c amendObjToUpdate = reqWrap.amendObj;

		try {
			update amendObjToUpdate;
			respWrap.amendWrappList = FetchUpdatedAmendListHelper(reqWrap.srId);
			respWrap.errorMessage = 'no error recorded';
			respWrap.updatedRec = amendObjToUpdate;
		} catch(exception e) {
			respWrap.errorMessage = e.getMessage();
		}




		return respWrap;

	}

	@AuraEnabled
	public static RespondWrap amendRemoveAction(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		system.debug(reqWrap);

		HexaBPM_Amendment__c amendObjToUpdate = reqWrap.amendObj;

		system.debug(amendObjToUpdate);
		try {
			list<string> amendRoles = amendObjToUpdate.Role__c.split(';');
				if(amendRoles.indexOf('Authorised Signatory') !=-1 && amendRoles.size() == 1) {
					delete amendObjToUpdate;
				}else if(amendRoles.indexOf('Authorised Signatory') !=-1 && amendRoles.size() > 1) {
					amendRoles.remove(amendRoles.indexOf('Authorised Signatory'));
					amendObjToUpdate.Role__c = String.join(amendRoles, ';');
				update amendObjToUpdate;
				}

			/* if(reqWrap.isAssigned) {
				list<string> amendRoles = amendObjToUpdate.Role__c.split(';');
				if(amendRoles.indexOf('Authorised Signatory') !=-1) {
					amendRoles.remove(amendRoles.indexOf('Authorised Signatory'));
				}
				amendObjToUpdate.Role__c = String.join(amendRoles, ';');
				update amendObjToUpdate;

			} else {
				delete amendObjToUpdate;
			} */
			respWrap.amendWrappList = FetchUpdatedAmendListHelper(reqWrap.srId);
			respWrap.errorMessage = 'no error recorded';
			respWrap.updatedRec = amendObjToUpdate;
		} catch(exception e) {
			respWrap.errorMessage = e.getMessage();
		}




		return respWrap;
	}

	/* public static void saveSRDoc(string srId, string parentId, map<string, string> docMap) {

	 list<string> lstDocMasterCodes = new list<string>();
	 for (string docMasterCode :docMap.keySet())
	 lstDocMasterCodes.add(docMasterCode);

	 map<string, string> mapDocMaster = new map<string, string>();
	 for(HexaBPM__Document_Master__c docMaster :[select id, HexaBPM__Code__c from HexaBPM__Document_Master__c where HexaBPM__Code__c in :lstDocMasterCodes]) {
	 mapDocMaster.put(docMaster.id, docMaster.HexaBPM__Code__c);
	 }
	 //delete the previous SR Docs for the same parent and documentMasterCode
	 if(lstDocMasterCodes != null && lstDocMasterCodes.size() > 0)
	 delete [select id from HexaBPM__SR_Doc__c where HexaBPM_Amendment__c = :parentId and HexaBPM__Document_Master__r.HexaBPM__Code__c in :lstDocMasterCodes];

	 list<HexaBPM__SR_Doc__c> lstSRDocs = new list<HexaBPM__SR_Doc__c>();
	 OB_AmendmentSRDocHelper.RequestWrapper reqWrap = new OB_AmendmentSRDocHelper.RequestWrapper();
	 reqWrap.srId = srId;
	 reqWrap.amedId = parentId;
	 reqWrap.docMasterContentDocMap = docMap;
	 OB_AmendmentSRDocHelper.ResponseWrapper repatenResp = new OB_AmendmentSRDocHelper.ResponseWrapper();
	 repatenResp = OB_AmendmentSRDocHelper.reparentSRDocsToAmendment(reqWrap);
	 //string srDocumentId = OB_QueryUtilityClass.createCompanyNameSRDoc(srId, parentId, documentMasterCode);
	 list<ContentDocumentLink> lstContentDocumentLink = new list<ContentDocumentLink>();
	 string docMasterCode;
	 for(HexaBPM__SR_Doc__c srDoc :lstSRDocs) {
	 System.debug('mapDocMaster.get(srDoc.HexaBPM__Document_Master__c)' + mapDocMaster.get(srDoc.HexaBPM__Document_Master__c));
	 docMasterCode = mapDocMaster.get(srDoc.HexaBPM__Document_Master__c);
	 if(docMasterCode != null && docMap.get(docMasterCode) != null) {
	 ContentDocumentLink cDe = new ContentDocumentLink();
	 cDe.ContentDocumentId = docMap.get(docMasterCode);
	 cDe.LinkedEntityId = srDoc.Id; // you can use objectId,GroupId etc
	 cDe.ShareType = 'I'; // Inferred permission, checkout description of ContentDocumentLink object for more details
	 cDe.Visibility = 'AllUsers';
	 lstContentDocumentLink.add(cDe);
	 }
	 }
	 if(lstContentDocumentLink != null && lstContentDocumentLink.size() > 0)
	 insert lstContentDocumentLink;

	 //Delete the DocumentLink from the SR - Cleanup of Files
	 delete [select id from ContentDocumentLink where ContentDocumentId in :docMap.values() and LinkedEntityId = :srId];

	 } */

	//revised


	@AuraEnabled
	public static RespondWrap amenSaveAction(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		boolean isDuplicate = false;

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		system.debug(reqWrap);

		HexaBPM_Amendment__c amendObj = reqWrap.amendObj;
		//amendObj.id = reqWrap.amendId;
		//amendObj.Id = ( amendObj.ServiceRequest__c != reqWrap.srId ? NULL : amendObj.Id);
		amendObj.ServiceRequest__c = reqWrap.srId;

		String passport = String.isNotBlank(amendObj.Passport_No__c) ? amendObj.Passport_No__c :'';
		String nationality = String.IsNotBlank(amendObj.Nationality_list__c) ? amendObj.Nationality_list__c.toLowerCase() :'';


		for(HexaBPM_Amendment__c amd :[Select ID, First_Name__c, Passport_No__c, Nationality_list__c, 
									   recordtype.developername, Role__c
									   FROM HexaBPM_Amendment__c
									   WHERE ServiceRequest__c = :amendObj.ServiceRequest__c
									   AND (recordtype.developername = 'Individual'
									   AND Id != :amendObj.Id)
									   AND Passport_No__c = :passport
									   AND Nationality_list__c = :nationality
									   Limit 1]) {
			System.debug('==> Dupliacate Amendment');
			System.debug(amd);

			amendObj.Id = amd.Id;
			// For director dup check.
			if(String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Authorised Signatory')) {
				System.debug(amd.Role__c);
				isDuplicate = true;
				break;
			}

			if(amd.Role__c != NULL
			   && !amd.Role__c.containsIgnorecase('Authorised Signatory')) {
				amendObj.Role__c = amd.Role__c + ';Authorised Signatory';
			} 
			else if(amd.Role__c == NULL) {
				amendObj.Role__c = 'Authorised Signatory';
			}


		}


		if(isDuplicate) {
			respWrap.errorMessage = 'This person already exist';
		} else {
			try {

				upsert amendObj;
				if(reqWrap.docMap != null) {

					Map<String, String> docMasterContentDocMap = reqWrap.docMap;
					String srId = reqWrap.srId;
					String amendID = amendObj.Id;


					OB_AmendmentSRDocHelper.RequestWrapper srDocReq = new OB_AmendmentSRDocHelper.RequestWrapper();
					OB_AmendmentSRDocHelper.ResponseWrapper srDocResp = new OB_AmendmentSRDocHelper.ResponseWrapper();
					srDocReq.amedId = amendObj.Id;
					srDocReq.srId = srId;
					srDocReq.docMasterContentDocMap = docMasterContentDocMap;

					srDocResp = OB_AmendmentSRDocHelper.reparentSRDocsToAmendment(srDocReq);

					//OB_ManageAuthorizedRepController.saveSRDoc(reqWrap.srId, amendObj.Id, reqWrap.docMap);
				}
				respWrap.amendWrappList = FetchUpdatedAmendListHelper(reqWrap.srId);
				respWrap.errorMessage = 'no error recorded';
				respWrap.updatedRec = amendObj;
			} catch(DMLException e) {
				string DMLError = e.getdmlMessage(0) + '';
				if(DMLError == null) {
					DMLError = e.getMessage() + '';
				}
				respWrap.errorMessage = DMLError;
			}
		}


		return respWrap;
	}

	public class RequestWrap {
		@AuraEnabled
		public String flowID { get; set; }

		@AuraEnabled
		public String pageId { get; set; }

		@AuraEnabled
		public String srId { get; set; }

		@AuraEnabled
		public String amendId { get; set; }

		@AuraEnabled
		public HexaBPM_Amendment__c amendObj { get; set; }

		@AuraEnabled
		public Boolean isAssigned { get; set; }

		@AuraEnabled
		public map<string, string> docMap { get; set; }


		public RequestWrap() {
		}
	}

	public class RespondWrap {

		@AuraEnabled
		public amendListWrapp amendWrapp { get; set; }

		@AuraEnabled
		public amendWrappList amendWrappList { get; set; }

		@AuraEnabled
		public string errorMessage { get; set; }

		@AuraEnabled
		public string recordId { get; set; }

		@AuraEnabled
		public HexaBPM_Amendment__c updatedRec { get; set; }

		@AuraEnabled
		public HexaBPM_Amendment__c newAmendToCreate { get; set; }

		@AuraEnabled
		public AmendmentWrapper newAmendWrappToCreate { get; set; }

		@AuraEnabled
		public HexaBPM__Section_Detail__c ButtonSection { get; set; }

		@AuraEnabled
		public HexaBPM__Service_Request__c srObj { get; set; }

		@AuraEnabled public SRWrapper srWrap { get; set; }




		public RespondWrap() {
			AmendWrapp = new amendListWrapp();
			amendWrappList = new amendWrappList();
		}
	}

	public class AmendmentWrapper {
		@AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
	 
		@AuraEnabled public boolean hasOtherRoles {get;set;}
		@AuraEnabled public map<string,string> declaration {get;set;}
		@AuraEnabled public HexaBPM__Service_Request__c serviceRequest {get;set;}

		@AuraEnabled public Boolean isIndividual { get; set; }
		@AuraEnabled public Boolean isBodyCorporate { get; set; }
		@AuraEnabled public String lookupLabel { get; set; }
		@AuraEnabled public String displayName { get; set; }
		@AuraEnabled public Boolean disableAllFlds { get; set; }
		@AuraEnabled public String relatedAmendID { get; set; }
		@AuraEnabled public String relatedRelId { get; set; }

		public AmendmentWrapper() {
				hasOtherRoles = false;
				serviceRequest = new HexaBPM__Service_Request__c();
				disableAllFlds = true;
		}
}

	public class amendWrappList {

		@AuraEnabled
		public list<AmendmentWrapper> AmendListForPicklist { get; set; }

		@AuraEnabled
		public list<AmendmentWrapper> AddedAmendList { get; set; }


		@AuraEnabled
		public object debugger ;


		public amendWrappList() {

			AmendListForPicklist = new list<AmendmentWrapper>();
			AddedAmendList = new list<AmendmentWrapper>();

		}
	}

	


	public class amendListWrapp {

		@AuraEnabled
		public list<HexaBPM_Amendment__c> otherRolesAmendList { get; set; }

		@AuraEnabled
		public list<HexaBPM_Amendment__c> currentRoleAndOtherAmendList { get; set; }

		@AuraEnabled
		public list<HexaBPM_Amendment__c> onlyCurrentRoleAmendList { get; set; }

		@AuraEnabled public string declarationText { get; set; }


		public amendListWrapp() {

			otherRolesAmendList = new list<HexaBPM_Amendment__c>();
			currentRoleAndOtherAmendList = new list<HexaBPM_Amendment__c>();
			onlyCurrentRoleAmendList = new list<HexaBPM_Amendment__c>();

		}
	}

	// dynamic button section
	@AuraEnabled
	public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId) {


		ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
		HexaBPM__Service_Request__c objRequest = new HexaBPM__Service_Request__c(Id = SRID);
		PageFlowControllerHelper objPB = new PageFlowControllerHelper();

		objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
		upsert objRequest;
		OB_RiskMatrixHelper.CalculateRisk(SRID);

		PageFlowControllerHelper.objSR = OB_QueryUtilityClass.QueryFullSR(objRequest.Id);

		PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
		system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
		respWrap.pageActionName = responseNextPage.pg;
		respWrap.communityName = responseNextPage.communityName;
		respWrap.CommunityPageName = responseNextPage.CommunityPageName;
		respWrap.sitePageName = responseNextPage.sitePageName;
		respWrap.strBaseUrl = responseNextPage.strBaseUrl;
		respWrap.srId = objRequest.Id;
		respWrap.flowId = responseNextPage.flowId;
		respWrap.pageId = responseNextPage.pageId;
		respWrap.isPublicSite = responseNextPage.isPublicSite;

		system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
		return respWrap;
	}
	public class ButtonResponseWrapper {
		@AuraEnabled public String pageActionName { get; set; }
		@AuraEnabled public string communityName { get; set; }
		@AuraEnabled public String errorMessage { get; set; }
		@AuraEnabled public string CommunityPageName { get; set; }
		@AuraEnabled public string sitePageName { get; set; }
		@AuraEnabled public string strBaseUrl { get; set; }
		@AuraEnabled public string srId { get; set; }
		@AuraEnabled public string flowId { get; set; }
		@AuraEnabled public string pageId { get; set; }
		@AuraEnabled public boolean isPublicSite { get; set; }

	}
	public class SRWrapper {
		@AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }

		@AuraEnabled public String declarationIndividualText { get; set; }
		@AuraEnabled public String declarationCorporateText { get; set; }

		@AuraEnabled public String viewSRURL { get; set; }
		@AuraEnabled public Boolean isDraft { get; set; }


		public SRWrapper() {
		}
	}
    
    @AuraEnabled
    public static Boolean fetchSRStatus(String srId){
        HexaBPM__SR_Status__c srStatus = [Select id from HexaBPM__SR_Status__c where HexaBPM__Code__c = 'RETURN_EDIT_RS_OFFICER_REVIEW' limit 1];
        HexaBPM__Service_Request__c obj = [Select id, HexaBPM__Internal_SR_Status__c, 
                                           Authorized_Signatory_Combinations__c,
                                           HexaBPM__Internal_Status_Name__c from HexaBPM__Service_Request__c 
                                           where Id = :srId limit 1];
        if(obj.HexaBPM__Internal_SR_Status__c == srStatus.Id){
            return true;
        }
        else{
            return false;
        }        
    }
     @AuraEnabled
    public static void updateSR(String SRID, String authorizeSignatories){
        HexaBPM__Service_Request__c obj = new HexaBPM__Service_Request__c();
        obj.Id = SRID;
        obj.Authorized_Signatory_Combinations__c = authorizeSignatories;
        update obj;
    }
}