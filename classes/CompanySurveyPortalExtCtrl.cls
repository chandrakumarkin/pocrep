/*
Author      :   Arun
Description :   This class is used Get the company Survey
--------------------------------------------------------------------------------------------------------------------------
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date            Updated By          Description
--------------------------------------------------------------------------------------------------------------------------             
V1.0    12/07/2021-Mar-2019     Arun                #17272 created in case company Survey for portal user 

*/
public class CompanySurveyPortalExtCtrl {
    
    public Company_Survey__c CompanySurveyObj { get; set; }
    public boolean IsValid {  get;   set; }
    // public boolean IsValidz {  get;   set; }
    public Contact ObjContact {  get;   set; }
    String ContactId;
    Public Attachment myfile{  get;   set; }
  Public Attachment tempfile{  get;   set; }
  public String filename{get;set;}
    public String body{get;set;}

    public CompanySurveyPortalExtCtrl (ApexPages.StandardController ctrl) {
        CompanySurveyObj = (Company_Survey__c) ctrl.getRecord();
         myfile = new Attachment();
    }

    public void pageLoad() 
    {
      IsValid = true;
     // IsValidz =true;
     ID contactId = [Select contactid from User where id =: Userinfo.getUserid()].contactId;
       
       if(contactId !=null)
       {
           ObjContact  = [Select AccountID,Email,Account.Name,Account.Registration_License_No__c from Contact where id =: contactId ];
       
       
        CompanySurveyObj.Email_Address__c = ObjContact.Email;
        CompanySurveyObj.Contact__c =ObjContact.id;
        CompanySurveyObj.Company__c =ObjContact.AccountID;

                List < Company_Survey__c > ListCompanySurvey = [SELECT id, Name, Contact__c 
                                                                FROM Company_Survey__c 
                                                                WHERE Contact__c =: Contactid];
                if (ListCompanySurvey.size() > 0) 
                {
                   // ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You have already submitted the Information'));

                   // IsValid = false;
                }
                }
         
        }
    
       
    

   public  void RowValueChange()
    {
        string ValParam= Apexpages.currentPage().getParameters().get('ValParam');
        string myParam= Apexpages.currentPage().getParameters().get('myParam');
        System.debug('================ValParam==========>'+ValParam);
        System.debug('================myParam==========>'+myParam);
        if(myParam=='true')
            CompanySurveyObj.put(ValParam,true);
        else   if(myParam=='false')
            CompanySurveyObj.put(ValParam,false);
        else
            CompanySurveyObj.put(ValParam,myParam);        
      
    }
    
     public void doAttachmentnew()
        {
         System.debug('================myfile.Name===========>'+myfile.Name);
         
        if(myfile.Name!=null)
       { 
           myfile.parentId =CompanySurveyObj.id;
           insert myfile;
          
        }
        SaveComapnySurvey();
         
            
   }
     public void doAttachment()
        {
         System.debug('================myfile.Name===========>'+myfile.Name);
         tempfile=myfile;
            
   }
 public void SaveComapnySurveyNew() 
    {
       System.debug('myfile==>'+myfile.id);
       upsert CompanySurveyObj;
    }
    public PageReference SaveComapnySurvey() 
    {
 
       SaveComapnySurveyNew();
      
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm, 'Your information has been submitted successfully .'));       
         IsValid = false;       
          return Null;
    }
}