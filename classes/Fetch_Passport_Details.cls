/**************************************************************************************************
  Author     :  Sravan Booragadda
  Company    :  NSI
  Class Name :  Fetch_Passport_Details
  Description:  This code would fetch passport details of Sharegolder/Business Owner along with company information.

Modification History 
======================================================================================================================================
V1.1    8/8/2016    Ravi    Remove the Legal_Structure_Type__c field from query line no : 65  as per # 2946
V1.2    31/1/2017   Sravan  Extending the functionality to fetch Bodycorporate details for Recognised companies like FRC, RLLP, RP, RLP according tkt # 3736
V1.3    12/04/2017  Sravan  Revised the template to have Individual Shareholder Details tkt # 3736
V1.4    10/Oct/2017 Arun    Restructure the Code  
V1.5   10/Feb/2019  Arun    Added to add Main company 
**************************************************************************************************/

public with sharing class Fetch_Passport_Details
{

    public string companyName{get;set;}
    public string entityType{get;set;}
    public string Servicetype{get;set;}
    public string companyActivities{get;set;}
    
    
     
    private id accountIds;
    Public list<Amendment__c> amendmentDetails{get;set;}
    public Service_Request__c serviceReq{get;set;}   

  //V1.3 revisions 
    Public List<Amendment__c> indShareHolderDet{get;set;}
   // Public List<Amendment__c> BCShareHolderDet{get;set;}
    
    Public List<MotherClass> BCShareHolderDet_new{get;set;}
    
    
    Public List<Amendment__c> indUBODet{get;Set;}
    public List<Amendment__c> bodycorporateUBODet{get;Set;}
    public List<Amendment__c> ListDirectors {get;Set;}// list of directors if BC is added  
  //V1.3
   
   public string HeaderText{get;set;}
   public string HeaderColText{get;set;}
    public string displayType{get;set;} 
    
    //request Legal Structures type
     public string  SRLegalStructures{get;set;}
      public string SRLegalStructuresfull{get;set;}
    
    public Fetch_Passport_Details(){
        displayType = apexpages.currentPage().getParameters().get('contentTyp');
    }
    
    Public void generateExcel()
    {
        amendmentDetails = new list<Amendment__c>();
        Service_Request__c serviceReq;
        id SRId = apexpages.currentPage().getParameters().get('SRID'); 
        companyActivities ='';
        entityType = '';
        companyName ='';    

        HeaderText='Change of Shareholders';
        HeaderColText='Existing Company';
        
        if(SRId !=null)
        {
           
           // amendmentDetails =    returnAmmendments(SRId);   
            
            serviceReq = [select Customer__r.name,Legal_Structures_Full__c,Legal_Structures_Full_Form__c,Foreign_Registration_Name__c,Legal_Structures__c,Foreign_Registration_Place__c,Foreign_Registration_Date__c,Service_Type__c,Customer__c,Customer__r.Company_Type__c from Service_Request__c where id =:SRId];            
               System.debug('serviceReq=='+serviceReq);  
                     
           if(serviceReq !=null)
            {
                fetchAmmendments(serviceReq);               
                accountIds = serviceReq.customer__c ; //amendmentDetails[0].Customer__c;
                companyName = serviceReq.Customer__r.name;
                entityType = serviceReq.Customer__r.Company_Type__c;   
                SRLegalStructuresfull=serviceReq.Legal_Structures_Full__c==null?serviceReq.Legal_Structures_Full_Form__c:serviceReq.Legal_Structures_Full__c;   
                
                   
               Servicetype=serviceReq.Service_Type__c;
               
                // fetch business actibities
                 for(License_Activity__c la : [select id,name,Activity__c,Activity__r.name,Account__c,Account__r.Company_Type__c from License_Activity__c where Account__c =:accountIds])
                 {
                    companyActivities += la.Activity__r.name+',';
                            
                 }     
                
                           
            }  

                if(Servicetype=='Application for Incorporation / Registration')
                {
                    HeaderText='New Trade License';
                    HeaderColText='New Company';
                    
                }

            
        }
        
    }
    
     //if company is UBO add  Directors in indUBODet list to display 
     public boolean IsCompanyUBO{get;set;}
    
     public void fetchAmmendments(Service_Request__c tempserviceReq)
    {   
    //V1.5 
    set<string> ListMainCompany=new set<string>();

        System.debug('==========fetchAmmendments======>'+tempserviceReq.Customer__c);
    
        id SRID=tempserviceReq.id;
     indShareHolderDet = new  List<Amendment__c>();
     //BCShareHolderDet = new  List<Amendment__c>();
     BCShareHolderDet_new = new  List<MotherClass>();
    
    indUBODet = new  List<Amendment__c>();
     IsCompanyUBO=false;
     bodycorporateUBODet = new  List<Amendment__c>(); 
     
     //If UBO is Corp Add Directors in indUBODet list
     ListDirectors=new List<Amendment__c>();

     
     Set<String> RegistedComapnies = new Set<String>{'FRC', 'RLP','RLLP','RP'};
     set<string> IndShareholdertype=new Set<string>{'Designated Member','Shareholder','Member','Limited Partner','General Partner','Founding Member','Founder'};
     
        // LTD, LLP, LP, GP, NPIO, LLC, LTD-IC, LTD PCC and LTD SPC
        //FRC, RLP, RLLP and RP, 
    
     
     
     
     
//04/10/2017 : Added sys_create__c so that same code can be used in Add sharehoader ...
             for(Amendment__c amnd :[select Date_of_Birth__c,Registration_No__c ,BC_UBO_Type__c ,Customer__r.name,Name_of_incorporating_Shareholder__c,Passport_No_Occupant__c ,Title_new__c ,Customer__r.Company_Type__c,Customer__c,Given_Name__c,Family_Name__c,id,name,Relationship_Type__c,Name_of_Body_Corporate_Shareholder__c,
                                       ServiceRequest__c,Amendment_Type__c,Passport_No__c,ServiceRequest__r.Legal_Structures__c,ServiceRequest__r.Foreign_Registration_Name__c,Nationality_list__c,Phone__c,Gender__c,Company_Name__c,
                                       Place_of_Registration__c,Registration_Date__c from Amendment__c where ServiceRequest__c =:SRID and ServiceRequest__r.Legal_Structures__c !=null and Relationship_Type__c!=null and (Amendment_Type__c='Individual' or Amendment_Type__c = 'Body Corporate') and Status__c !='Removed' and sys_create__c!=true])
            {
            
            
                        //Copy Director information 
                        if(amnd.Relationship_Type__c =='Director' && amnd.Amendment_Type__c=='Individual')
                        {
                            ListDirectors.add(amnd);
                        }
                        
                        if(amnd.Relationship_Type__c =='Council Member' && amnd.Amendment_Type__c=='Individual'){
                            ListDirectors.add(amnd);
                        
                        }
                            

                        //Set SR Legal stu to get UBO related details 
                        if(SRLegalStructures==null && amnd.ServiceRequest__r.Legal_Structures__c!=null)
                            SRLegalStructures=amnd.ServiceRequest__r.Legal_Structures__c;
                    
                 System.debug('====RegistedComapnies=->'+amnd.ServiceRequest__r.Legal_Structures__c);
                //is company not a Registed company ?
                //if(!RegistedComapnies.contains(amnd.ServiceRequest__r.Legal_Structures__c))
                //{
                    
                    if(amnd.Passport_No__c !=null && amnd.Amendment_Type__c=='Individual')
                    {
                    
                        
                    
                        //If Individual type amendment, add in the list to display under Individual sharehoaders
                            if(IndShareholdertype.contains(amnd.Relationship_Type__c))
                            {
                                  indShareHolderDet.add(amnd); 
                            }
                            else if(amnd.Relationship_Type__c =='UBO')
                            {
                                    //3rd section of UBO
                                    indUBODet.add(amnd);  
                                    
                                    ////V1.5 
                                    ListMainCompany.add(amnd.Name_of_incorporating_Shareholder__c);                                 
                            }
                        
                    }
                    else  if(amnd.Amendment_Type__c=='Body Corporate')
                    {
                        
                            if(IndShareholdertype.contains(amnd.Relationship_Type__c))
                            {
                                //2nd Sction of Mother compnay and comapny sharehoaders
                                  AddAmendmentInmother(amnd);//Add Mother compnay details  
                                  
                                  ////V1.5 
                                  if(ListMainCompany.contains(amnd.Company_Name__c))
                                    ListMainCompany.remove(amnd.Company_Name__c);
                        
                        
                            }
                            else if(amnd.Relationship_Type__c =='UBO')
                            {
                                bodycorporateUBODet.add(amnd);
                                //UBO Body corporate exist copy Directors in 3rd section 
                                IsCompanyUBO=true;
                            }
                        
                    }
                    
                     if(BCShareHolderDet_new.isEmpty()==true && RegistedComapnies.contains(amnd.ServiceRequest__r.Legal_Structures__c) && tempserviceReq.Foreign_Registration_Name__c!=null)
                    {
                        System.debug('=tempserviceReq===>'+tempserviceReq);
                        
                        MotherClass ObjMotherClass=new MotherClass();                        ObjMotherClass.No=1;                        ObjMotherClass.CompanyName=tempserviceReq.Foreign_Registration_Name__c;                        ObjMotherClass.Place=tempserviceReq.Foreign_Registration_Place__c;                        ObjMotherClass.dateIncorporation=tempserviceReq.Foreign_Registration_Date__c;
                        
                        BCShareHolderDet_new.add(ObjMotherClass);
                       ////V1.5 
                          if(ListMainCompany.contains(tempserviceReq.Foreign_Registration_Name__c))//remove  UBO relted main companies 
                            ListMainCompany.remove(tempserviceReq.Foreign_Registration_Name__c);
                        
                        
                        
                    }
                    
                    
               // }
                /*/* Commented on 27/May/2019
                else
                {
                    
                    
                    //2nd Section values of Recognised company 
                    if(BCShareHolderDet_new.isEmpty()==true && RegistedComapnies.contains(amnd.ServiceRequest__r.Legal_Structures__c))
                    {
                        System.debug('=tempserviceReq===>'+tempserviceReq);
                        MotherClass ObjMotherClass=new MotherClass();
                        ObjMotherClass.No=1;
                        ObjMotherClass.CompanyName=tempserviceReq.Foreign_Registration_Name__c;
                        ObjMotherClass.Place=tempserviceReq.Foreign_Registration_Place__c;
                        ObjMotherClass.dateIncorporation=tempserviceReq.Foreign_Registration_Date__c;
                        BCShareHolderDet_new.add(ObjMotherClass);
                        
                    
                        ////V1.5 
                          if(ListMainCompany.contains(tempserviceReq.Foreign_Registration_Name__c))//remove  UBO relted main companies 
                            ListMainCompany.remove(tempserviceReq.Foreign_Registration_Name__c);
                        
                        
                        
                    }
                    
                    
                    System.debug('==amnd==>'+amnd);     
                    //10:06:03.0 (139051237)|USER_DEBUG|[170]|DEBUG|==amnd==>Amendment__c:{Customer__c=0012000001NZiKOAA1, Title_new__c=Sheikh, Given_Name__c=UBO 1, Family_Name__c=-, Id=a0O3E000007hAL5UAM, 
                    //Name=A-89904, Relationship_Type__c=UBO, Name_of_Body_Corporate_Shareholder__c=UBO 1 -, ServiceRequest__c=a1I3E000001TrxFUAS, Amendment_Type__c=Individual, 
                    //Passport_No__c=S43434, Nationality_list__c=Afghanistan, Gender__c=Male, RecordTypeId=01220000000MbbrAAC, CurrencyIsoCode=AED}
                    
                    if(amnd.Relationship_Type__c =='UBO' && amnd.Amendment_Type__c=='Individual')

                        //Not sure why we have these conditions 27/11/2017
                //(&& amnd.Name_of_incorporating_Shareholder__c !=null && amnd.ServiceRequest__r.Foreign_Registration_Name__c !=null  && 
                  //  amnd.Name_of_incorporating_Shareholder__c.toLowerCase() == amnd.ServiceRequest__r.Foreign_Registration_Name__c)
                  
                    {
                        
                        indUBODet.add(amnd);
                    }
                    else if(amnd.Relationship_Type__c =='UBO' && amnd.Amendment_Type__c=='Body Corporate' )
                    
                //Not sure why we had these conditions 27/11/2017
                    //&& amnd.Name_of_incorporating_Shareholder__c !=null && amnd.ServiceRequest__r.Foreign_Registration_Name__c !=null  && 
                    //amnd.Name_of_incorporating_Shareholder__c.toLowerCase() == amnd.ServiceRequest__r.Foreign_Registration_Name__c)
                    {
                         bodycorporateUBODet.add(amnd);
                        //Body Corporate is UBO copy Directors
                            IsCompanyUBO=true;
                    }
                    
                    
                }*/
            
            
            
            }
       
       
       System.debug('ListMainCompany==>'+ListMainCompany);
          ////V1.5 
        if(ListMainCompany.size()>0)// Individual UBO added and company info not in submitted SR 
        {
            
            
            for(Relationship__c ObjRel:[select Object_Account__r.Name,Object_Account__r.Place_of_Registration__c,Object_Account__r.ROC_reg_incorp_Date__c from Relationship__c where Object_Account__r.Name in :ListMainCompany and  Subject_Account__c=:tempserviceReq.Customer__c and  Object_Account__c!=null and Active__c=true])
            {
                        
                        MotherClass ObjMotherClass=new MotherClass();
                        ObjMotherClass.No=BCShareHolderDet_new.size()+1;
                        
                        ObjMotherClass.CompanyName=ObjRel.Object_Account__r.Name;
                        ObjMotherClass.Place=ObjRel.Object_Account__r.Place_of_Registration__c;
                        
                        ObjMotherClass.dateIncorporation=ObjRel.Object_Account__r.ROC_reg_incorp_Date__c;
                        BCShareHolderDet_new.add(ObjMotherClass);
                        
                
            }
             System.debug('BCShareHolderDet_new==>'+BCShareHolderDet_new);
                        
                        
        }           
                
        if(IsCompanyUBO==true)
        {
             //if BC is UBO add Directors or members
            /*
                    Director (in case of LTDs, FRC), 
                    Member LLC (in case of LLC)., 
                    Founding member (in case of NPIO), 
                    Member/Designated member (in case of LLP, RLLP) and 
                    Partner (in case GP, RP, LP, RLP).
                    
                */
                
            //Request from AOR add Directors from SR 
            if((ListDirectors.isempty()==false || indShareHolderDet.Isempty()==false) && tempserviceReq.Service_Type__c=='Application for Incorporation / Registration')  
            {
                /*
                //
                if(ListDirectors.isempty()==false)
                indUBODet.addall(ListDirectors);//Add all the Compnay Directors
                else 
                indUBODet.addall(indShareHolderDet);//Add all the Compnay Directors 
                */
            } //Rquest is non AOR Add Directors from Account 
            else if(tempserviceReq.Customer__c!=null)
            {
              string RelationshipType;
                //'Has Director'
                if(SRLegalStructures=='LLC')
                    RelationshipType='Is Member LLC Of';
                
                else if(SRLegalStructures=='NPIO')
                    RelationshipType='Has Founding Member';
                else if(SRLegalStructures=='LLP')
                    RelationshipType='Has Designated Member';
                else if(SRLegalStructures=='RLLP')
                    RelationshipType='Has Member';
                else if(SRLegalStructures=='GP' || SRLegalStructures=='RP' || SRLegalStructures=='LP' || SRLegalStructures=='RLP')
                    RelationshipType='%Partner%';
                else if(SRLegalStructures == 'Foundation')
                    RelationshipType= 'has Council Member';
                else
                RelationshipType='Has Director';
                
                System.debug('====RelationshipType=>'+RelationshipType);
                
                
                for(Relationship__c ObjRel:[select Object_Contact__r.Birthdate,id,Object_Contact__r.Phone,Object_Contact__r.Gender__c,Object_Contact__r.Salutation,Object_Contact__r.FirstName,Object_Contact__r.LastName,Relationship_Passport__c,Relationship_Nationality__c,Relationship_Type_formula__c from Relationship__c where Subject_Account__c=:tempserviceReq.Customer__c and Relationship_Type__c like :RelationshipType and Object_Contact__c!=null and Active__c=true])
                {
                    System.debug('====ObjRel=>'+ObjRel);
                    Amendment__c ObjAmen=new Amendment__c(); ObjAmen.Title_new__c=ObjRel.Object_Contact__r.Salutation;ObjAmen.Given_Name__c=ObjRel.Object_Contact__r.FirstName;                   ObjAmen.Family_Name__c=ObjRel.Object_Contact__r.LastName;                ObjAmen.Nationality_list__c=ObjRel.Relationship_Nationality__c;                  ObjAmen.Passport_No__c=ObjRel.Relationship_Passport__c;                    ObjAmen.Relationship_Type__c=ObjRel.Relationship_Type_formula__c;                    ObjAmen.Gender__c=ObjRel.Object_Contact__r.Gender__c;                   ObjAmen.Phone__c=ObjRel.Object_Contact__r.Phone;ObjAmen.Date_of_Birth__c=ObjRel.Object_Contact__r.Birthdate;ListDirectors.add(ObjAmen);
                    
                }   
                //indUBODet.addall(ListDirectors);//Add all the Compnay Directors
                
                
            } 
        }           
                
        
      }           
      
    
    public void AddAmendmentInmother(Amendment__c amnd)
    {
                        MotherClass ObjMotherClass=new MotherClass();
                        ObjMotherClass.No=BCShareHolderDet_new.Size()+1;
                        ObjMotherClass.CompanyName=amnd.Company_Name__c;
                        ObjMotherClass.Place=amnd.Place_of_Registration__c;
                        ObjMotherClass.dateIncorporation=amnd.Registration_Date__c;
                        BCShareHolderDet_new.add(ObjMotherClass);
    }
        
                        
    
    public class MotherClass
    {
        public integer No{get;set;}
        public String CompanyName{get;set;}
        public String Registration{get;set;}
        public String Place {get;set;}
        public date dateIncorporation{get;set;}
        
        
    }
    
    // End V1.2
}