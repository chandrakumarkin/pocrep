@isTest
public class GSShipmentCreationController_Test{


    static testMethod void CreateAutomaticShipments(){
        Status__c sts = new Status__c();
        sts.Name = 'PENDING_COLLECTION';
        sts.Code__c = 'PENDING_COLLECTION';
        insert sts;
        
        Step_Template__c testSrTemplate = new Step_Template__c();    
        testSrTemplate.Name = 'Courier Delivery';
        testSrTemplate.Code__c = 'Courier Delivery';
        testSrTemplate.Step_RecordType_API_Name__c = 'Courier Delivery';
        insert testSrTemplate;
        
        Step_Template__c testSrTemplate1 = new Step_Template__c();    
        testSrTemplate1.Name = 'Collected';
        testSrTemplate1.Code__c = 'Collected';
        testSrTemplate1.Step_RecordType_API_Name__c = 'Collected';
        insert testSrTemplate1;
       
       Account objAccount          = new Account();
        objAccount.Name             = 'Test Custoer 1';
        objAccount.E_mail__c        = 'test@test.com';
        objAccount.BP_No__c         = '001234';
        objAccount.Company_Type__c  ='Financial - related';
        objAccount.Sector_Classification__c     = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c      = 'LTD';
        objAccount.ROC_Status__c                = 'Active';
        objAccount.Financial_Year_End__c        = '31st December';
        objAccount.Registered_Address__c        = 'Testr';
        insert objAccount;
        
        License__c objLic = new License__c();
        objLic.License_Issue_Date__c    = system.today();
        objLic.License_Expiry_Date__c   = system.today().addDays(365);
        objLic.Status__c                = 'Active';
        objLic.Account__c               = objAccount.id;
        insert objLic;
        
        objAccount.Active_License__c = objLic.id;
        update objAccount;
        Id portalUserId                 = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact              = new Contact();
        objContact.firstname            = 'Test Contact';
        objContact.lastname             = 'Test Contact1';
        objContact.accountId            = objAccount.id;
        objContact.recordTypeId         = portalUserId;
        objContact.Email                = 'test@difcportal.com';
        
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser    = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Non_Sponsored_Employment_Cancellation')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        SR_Status__c SRst = new SR_Status__c();
        SRst.Name ='Draft';
        SRst.Code__c ='DRAFT';
        insert SRst;
        
        Service_Request__c objSR             = new Service_Request__c();
        objSR.RecordTypeId = mapRecordTypeIds.get('Non_Sponsored_Employment_Cancellation');
        objSR.Customer__c                    = objAccount.Id;
        objSR.Email__c                       = 'vvvv@gmail.com';
        objSR.Send_SMS_To_Mobile__c          = '+971523830186';
        objSR.Nationality_list__c            = 'India';
        objSR.Type_of_Request__c              = 'DFSA Employee Card';
        objSR.External_SR_Status__c           =  SRst.id;
        objSR.Contact__c                      = objContact.id;
        insert objSR;
        
       //Service_request__C SR = [select id,External_Status_Name__c ,Contact__c,Customer__c from Service_request__C where id =:objSR.id limit 1] ;
      
      List<step__C> stepList = new List<step__c>();  
        
      Step__c thisStep = new Step__c();
      thisStep.Step_Template__c = testSrTemplate.Id;
      thisStep.Status__c = sts.Id;
      thisStep.sr__C = objSR.id;
     
      stepList.add(thisStep);
      
      
      Step__c thisStep1 = new Step__c();
      thisStep1.Step_Template__c = testSrTemplate.Id;
      thisStep1.Status__c = sts.Id;
      thisStep1.sr__C = objSR.id;
    
      stepList.add(thisStep1);
      
      Insert stepList;
      
      list<string> name = new List<string>();
      
      
      for(step__C step :[select id,Step_Name__c,sr__C from step__c where id in : stepList]){
        name.add(step.Step_Name__c);
      }
      
    
     List<step__c> step1 =[select id,Step_Name__c,sr__C from step__c where Step_Name__c  in : name and sr__C  =: objSR.id];
        
      Test.startTest();
           
          // GSShipmentCreationController obj = new GSShipmentCreationController();
           GSShipmentCreationController.GSCorierDeliveryShipmentMethod (new List<id> {objSR.id});
      Test.stopTest();
    }

}