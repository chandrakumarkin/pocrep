@isTest
private class ExemptedUBORegulations_Edit_Test 
{
    public static testMethod void ExemptedUBORegulations_Edittest()
    {
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Full_Exempted_UBO_Regulations')])
        {
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
         }
     
         Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        objAccount.Exempted_from_UBO_Regulations__c=false;
        insert objAccount;
        
        SR_Status__c objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        insert objSRStatus;
        
         Service_Request__c objSR = new Service_Request__c();
         
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordType.get('Full_Exempted_UBO_Regulations');
        objSR.Service_Category__c = 'New';
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        objSR.External_SR_Status__c=objSRStatus.id;
        objSR.Internal_SR_Status__c=objSRStatus.id;
        insert objSR;
        
        UBO_Data__c  OnjUBOData=new UBO_Data__c ();
        OnjUBOData.I_am_Exempt_entities__c='Yes';
      //  OnjUBOData.Name_of_Regulator_Exchange_Govt__c ='';
       // OnjUBOData.Country__c='';
        //OnjUBOData.Reason_for_exemption__c='';
        //OnjUBOData.Reason_for_exemption__c='';
        OnjUBOData.Service_Request__c =objSR.id;
        insert OnjUBOData;
        
        
        
        SR_Doc__c ObjDoc=new SR_Doc__c();
        ObjDoc.Service_Request__c=objSR.id;
        insert ObjDoc;
        
         Attachment ObjAttachment = new Attachment();
        ObjAttachment.name ='test';
        ObjAttachment.Body = Blob.valueOf('test body');
        ObjAttachment.ParentId = ObjDoc.id;
        insert ObjAttachment;
        
        
         ApexPages.StandardController sc = new ApexPages.StandardController(objSR);
         ExemptedUBORegulations_Edit ObjExp=new ExemptedUBORegulations_Edit(sc);
        
        ObjExp.ObjAttachment=ObjAttachment;
        
        ObjExp.SaveRecord();
        
        
        ObjExp.RemoveExemptedSaveRecord();
        
       
        
        
        SRExemptedUBOViewcontroller ObjSRExemptedUBOViewcontroller=new SRExemptedUBOViewcontroller();
        ObjSRExemptedUBOViewcontroller.Sr_Id=objSR.id;
        ObjSRExemptedUBOViewcontroller.ListUBOList();
        ObjSRExemptedUBOViewcontroller.getObjUBO();
        
        
        
         
        
         Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
         System.runAs(objUser)
         {
         
          Service_Request__c objSR1 = new Service_Request__c();
         ApexPages.StandardController sc1 = new ApexPages.StandardController(objSR1 );
         ExemptedUBORegulations_Edit ObjExp1=new ExemptedUBORegulations_Edit(sc1);
         
           }
        
        
        
            
         
     }
         
         
     
 }