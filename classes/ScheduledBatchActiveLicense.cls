/***********************************************************************************
 *  Author   : Arun 
 *  Purpose  : Start batch to update companies Active License
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date        Updated By  Description
 ----------------------------------------------------------------------------------------              
 
 V1.1   29/July/2019   Sai       Updated Operating location SOQL     
 
 **********************************************************************************************/


global  class ScheduledBatchActiveLicense implements Schedulable {
     
     global void execute(SchedulableContext sc)
     {
        string SQL;
        if(!test.isRunningtest())
        SQL='select id,Fund_Setup_Date__c,DFSA_Lic_Appr_Date__c,Sector_Classification__c,Registered_Active_Lease__c,FinTech_Classification__c,Discount_Applied__c,Active_Leases__c,Lease_Expiry_Date__c,(select id,Lease__c from Operating_Locations__r where Status__c=\'Active\' AND Area__c !=\'0\' ORDER BY Area_WF__c desc limit 1),(select id from Lease__r where Status__c=\'Active\'),(select id,BC_ROC_Status__c,BC_Registration_Date__c,Relationship_Type_formula__c,Relationship_Type__c from  Primary_Account__r where Relationship_Type__c in (\'Has Corporate Communication\',\'Has General Communication\',\'Has Exempt Fund\',\'Has QIF\',\'Emergency contact\',\'Has CEO/Regional Head/Chairman\') and Active__c=true) from Account where (ROC_Status__c=\'Active\' OR ROC_Status__c=\'CP-Active\' OR ROC_Status__c=\'CP-Expired\')';
        else
         SQL='select id,Fund_Setup_Date__c,DFSA_Lic_Appr_Date__c,Sector_Classification__c,Registered_Active_Lease__c,FinTech_Classification__c,Discount_Applied__c,Active_Leases__c,Lease_Expiry_Date__c,(select id,Lease__c from Operating_Locations__r where Status__c=\'Active\' AND Area__c !=\'0\' ORDER BY Area_WF__c desc limit 1),(select id from Lease__r where Status__c=\'Active\'),(select id,BC_ROC_Status__c,BC_Registration_Date__c,Relationship_Type_formula__c,Relationship_Type__c from  Primary_Account__r where Relationship_Type__c in (\'Has Corporate Communication\',\'Has General Communication\',\'Has Exempt Fund\',\'Has QIF\',\'Emergency contact\',\'Has CEO/Regional Head/Chairman\') and Active__c=true) from Account where (ROC_Status__c=\'Active\' OR ROC_Status__c=\'CP-Active\' OR ROC_Status__c=\'CP-Expired\') limit 2';
       
     
        BatchActiveLicense obj = new BatchActiveLicense(SQL);
       
        database.executeBatch(obj,2);   
    }
}