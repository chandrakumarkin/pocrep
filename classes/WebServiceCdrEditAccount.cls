public class WebServiceCdrEditAccount {
    
    public static String editJsonAccCdr(String AccID){
        
        Account Acc =   [SELECT id, Unified_License_Number__c, BP_No__c, Active_License__r.name, Active_License__r.License_Issue_Date__c, Active_License__r.License_Type__c, Active_License__r.License_Expiry_Date__c, Active_License__r.License_Terminated_Date__c,
                         Active_License__r.ROC_Status_List__c, Name, Arabic_Name__c, Authorized_Share_Capital__c, Home_Country__c, Company_Type__c, Trade_Name__c, Trading_Name_Arabic__c, (Select Id, License_Type__c from Licenses__r),
                         Sectors__c, (Select Id, Activity__r.Name, Activity__r.Activity_Code__c FROM License_Activities__r) from Account Where Id =:AccID];
        JSONGenerator editAccJson = JSON.createGenerator(true);
        editAccJson.writeStartArray();
        editAccJson.writeStartObject();
        if(!String.isBlank(Acc.BP_No__c))
        editAccJson.writeStringField('accountNumber', Acc.BP_No__c);
        if(!String.isBlank(Acc.Name))
        editAccJson.writeStringField('nameEn', Acc.Name);
        if(!String.isBlank(Acc.Arabic_Name__c))
        editAccJson.writeStringField('nameAr', Acc.Arabic_Name__c);
        if(Acc.Active_License__r != null && Acc.Active_License__r.License_Issue_Date__c != null)
        editAccJson.writeDateTimeField('establishedOn', Acc.Active_License__r.License_Issue_Date__c);
        editAccJson.writeStringField('capital', 'test');
        if(!String.isBlank(Acc.Home_Country__c))
        editAccJson.writeStringField('originCountry', Acc.Home_Country__c);
        editAccJson.writeStringField('zoneName', 'DIFC');
        if(!String.isBlank(Acc.Company_Type__c))
        editAccJson.writeStringField('companyType', Acc.Company_Type__c);
        if(!String.isBlank(Acc.Trade_Name__c))
        editAccJson.writeStringField('tradeNameEn', Acc.Trade_Name__c);
        if(!String.isBlank(Acc.Trading_Name_Arabic__c))
        editAccJson.writeStringField('tradeNameAr', Acc.Trading_Name_Arabic__c);
        editAccJson.writeFieldName('sectors');
        editAccJson.writeStartArray();
        if(!String.isBlank(Acc.Sectors__c))
        editAccJson.writeString(Acc.Sectors__c);
        editAccJson.writeEndArray();
        editAccJson.writeEndObject();
        System.debug('editAccJson:-'+editAccJson.getAsString());
        return editAccJson.getAsString();
        
    }
    
    public static void editAccountToCdr(String AccId){
        
            String jsonRequest;
            List<Log__c> ErrorObjLogList = new List<Log__c>();
            //Get authrization token 
            String Token = WebServiceCDRGenerateToken.cdrAuthToken();
            //WebService to Push the Edit the Form
            HttpRequest request = new HttpRequest();
            request.setEndPoint(System.label.Cdr_Edit_Account_Url);
            jsonRequest = editJsonAccCdr(AccId);
            request.setBody(jsonRequest);
            request.setTimeout(120000);
            request.setHeader('Accept', 'text/plain');
            request.setHeader('Content-Type', 'application/json-patch+json');
            request.setHeader('Authorization','Bearer'+' '+Token);
            request.setMethod('PUT');
            HttpResponse response = new HTTP().send(request);
            System.debug('responseStringresponse:'+response.toString());
            System.debug('STATUS:'+response.getStatus());
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('strJSONresponse:'+response.getBody());
            request.setTimeout(110000);
        
       if (response.getStatusCode() == 200)
        {           
            Log__c ErrorobjLog = new Log__c();
            ErrorobjLog.Type__c = 'CDR Account Edit';
            ErrorobjLog.Description__c = 'Request: '+ jsonRequest +'Response: '+ response.getBody() ;
            ErrorobjLog.Account__c = AccId;
            ErrorObjLogList.add(ErrorobjLog);
            
        }
        else
        {
            Log__c ErrorobjLog = new Log__c();
            ErrorobjLog.Type__c = 'CDR Account Edit Error';
            ErrorobjLog.Description__c = 'Request: '+ jsonRequest +'Response: '+ response.getBody() ;
            ErrorobjLog.Account__c = AccId;
            ErrorObjLogList.add(ErrorobjLog);      
        }
        
        Insert ErrorObjLogList;
        
    }

}