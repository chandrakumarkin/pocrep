@isTest
public class FitoutSiteVisitReportWaiverCtrl_Test {
    @testSetup
    static void createTestData(){
        Id contractorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Contractor_Account').getRecordTypeId();
        Account objAccount = new Account(Name = 'Test Account',Phone = '1234567890', recordTypeId=contractorRecordTypeId);
        insert objAccount;
        
        Fitout_Milestone_Penalty__c milestone = new Fitout_Milestone_Penalty__c();
        milestone.Account__c = objAccount.Id;
        milestone.Unit_Handover_Date__c = system.today();
        milestone.Type_of_Retail__c= 'Standard Retail';
        milestone.Additional_Tenant_email__c = 'testmilestone@difc.test.com';
        milestone.Building__c = 'Test DIFC Office';
        milestone.Floor__c = '4';
        milestone.Unit__c = 'Test Unit';
        insert milestone;
        
        SR_Template__c objTemplate = new SR_Template__c(Name = 'Site Visit Report for Fit Out', SR_RecordType_API_Name__c = 'Site_Visit_Report',Active__c = true);
        insert objTemplate;
        
        Step_Template__c objStepTemplate = new Step_Template__c(Name = 'Verify & Approve by FOSP Manager',Code__c = 'VERIFY_APPROVE_FOSP_MANAGER',Step_RecordType_API_Name__c = 'Verify_Approve_FOSP_Manager',Summary__c = 'Verify & Approve by FOSP Manager');
        insert objStepTemplate;
        
        SR_Status__c srStatus = new SR_Status__c(Name='Verified By DIFC Fit - Out Team',Code__c ='VERIFIED_BY_DIFC_FITOUT');
        insert srStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Email__c = 'testsr@difc.com';    
        // objSR3.Contact__c = objContact.Id;
        objSR.Express_Service__c = false;
        objSR.SR_Template__c = objTemplate.Id;
        objSR.Type_of_Request__c = 'With Instruction and Violation';
        objSR.Service_Category__c = 'Rectification with Fine';
        objSR.Duration__c = '2 working days';
        objSR.External_SR_Status__c = srStatus.Id;
        insert objSR;
        
        Lookup__c violation = new Lookup__c(Name='Breach of any Health and Safety Standard',Type__c='Fit - Out Violation',Code__c='Fit - Out Violation-Breach of any Health and Safety Standard',Fine_Amount__c=1000,X1st_Violation_Type__c='Fine Only');
        insert violation;
        
        Violation__c violationRecord = new Violation__c(Related_Request__c=objSR.id,Violations__c=violation.id, Waiver_Type_by_FOSP__c='Fine only');
        insert violationRecord;
        
        List<Status__c> lstStatus = new List<Status__c>();
        lstStatus.add(new Status__c(Name = 'Pending Review', Type__c = 'Pending Review', Code__c = 'PENDING REVIEW'));
        lstStatus.add(new Status__c(Name = 'Verified', Type__c = 'Verified', Code__c = 'VERIFIED'));
        lstStatus.add(new Status__c(Name = 'Completed', Type__c = 'Completed', Code__c = 'COMPLETED'));
        insert lstStatus;
        
        step__c objStep = new Step__c(Step_Template__c = objStepTemplate.id,No_PR__c = false,status__c = lstStatus[0].id);
        insert objStep;
        
    }
    
    public static testmethod void getViolationsTestmethod(){
        Test.startTest();
        List<Step__c> lstObjStep = [Select Id,Name,Step_Name__c FROM Step__c];
        List<Violation__c> violations = [Select Id,Name,Waiver_Type_by_FOSP__c FROM Violation__c];
        FitoutSiteVisitReportWaiverController.getViolations(lstObjStep[0].Id);
        FitoutSiteVisitReportWaiverController.saveViolations(lstObjStep[0], 'Request for Waiver', violations, 'Test Comments');
        Test.stopTest();
    }
    
}