/**
 * This test class is used to cover Contractor_Commercial_License_population
 */
@isTest
private class Contractor_Commercial_License_Test {

    static testMethod void myUnitTest() {
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request','Permit_to_Work','Permit_to_add_or_remove_equipment',
                                'Fit_Out_Contact','Revision_of_Fit_Out_Request','Update_Contact_Details','Data_Center_Requestor','NOC_Bloomberg','Individual_Tenant','Landlord','Tool','Lease_Application_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        
        insert testSrTemplate;
        
        SR_Steps__c srstep = new SR_Steps__c();
        insert srstep;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert SR1;
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;
        //SR.SR_Group__c='Fit-Out & Events';
        SR.Linked_SR__c = SR1.id;     
        
        insert SR;
        
        SR_Status__c srStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Approved', 'Approved');
        SR_Template__c srTemp1 = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Contractor Registration','Contractor_Registration','Fit - Out & Events');
        insert srTemp1;
        Service_Request__c servRecContractor = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        servRecContractor.Linked_SR__c =SR.id;
        servRecContractor.SR_Template__c = srTemp1.id;
        servRecContractor.External_SR_Status__c = srStatus.id;
        servRecContractor.Contractor__c = objAccount.id;
        servRecContractor.Linked_SR__c = SR.id;
        insert servRecContractor;
        
        //Select id,Status__c,createdById,service_request__r.createdById from SR_Doc__c
        SR_Doc__c srDoc = new SR_Doc__c(Name ='Contractor License Copy',service_request__c =servRecContractor.id);
        insert srDoc;
        Blob blb = Blob.valueof('body of the document');
        Attachment att = new Attachment(Name ='Contractor License Copy' , parentId =srDoc.id,body=blb);
        insert att;
        
        SR_Doc__c srDocFit = new SR_Doc__c(Name ='Contractor License Copy',service_request__c =SR.id);
        insert srDocFit;
        Test.startTest();
        	Contractor_Commercial_License_population.copyContractor_Commercial_License(new list<id>{srDocFit.id});
        Test.stopTest();
        /*
        Status__c status = Test_CC_FitOutCustomCode_DataFactory.getTestStepStatus('Pending Review', 'Pending_Review');        
        insert status;
        Step_Template__c testStepTemplates = new Step_Template__c(Code__c = 'UPLOAD_DETAILED_DESIGN_DOCUMENTS',Name='Upload Detailed Design Documents',Step_RecordType_API_Name__c = 'Upload_Detailed_Design_Documents');
        insert testStepTemplates;
        step__c step = Test_CC_FitOutCustomCode_DataFactory.getTestStep(servRec.id, status.Id, accRec.Name);
        step.Step_Template__c = testStepTemplates.Id;
        insert step;
        */
    }
}