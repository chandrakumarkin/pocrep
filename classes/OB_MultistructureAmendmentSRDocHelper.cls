/**
*Author : Maruf Bagwan
*Description : reparenting  logic for SRDOCS on Amendment.
**/ 

public without sharing class OB_MultistructureAmendmentSRDocHelper 
{
    @AuraEnabled
    public static ResponseWrapper reparentSRDocsToAmendment(RequestWrapper reqWrap) 
    {
        //reqest wrpper
        //RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
        system.debug('@@@@@@@@@ called reparenting  ');
        //response wrpper
        ResponseWrapper respWrap = new ResponseWrapper();
        String srId = reqWrap.srId;
        String amedId = reqWrap.amedId;
        Map<String, String> docMasterContentDocMap = reqWrap.docMasterContentDocMap;
        
        
        system.debug('@@@@@@@@@ docMasterContentDocMap ' + docMasterContentDocMap);
        
        //query amed to check if the SRd docs are ther of not.
        if(docMasterContentDocMap != NULL && docMasterContentDocMap.size() > 0) {
            String srTemplateId;
            //quering SR
            for(HexaBPM__Service_Request__c sr :[SELECT id, 
                                                 HexaBPM__SR_Template__c
                                                 FROM HexaBPM__Service_Request__c
                                                 WHERE id = :srId
                                                 LIMIT 1]) {
                                                     srTemplateId = sr.HexaBPM__SR_Template__c;
                                                 }
            system.debug('@@@@@@@@@ srTemplateId ' + srTemplateId);
            Map<String, HexaBPM__SR_Doc__c> docMasterSRDocsMap = new Map<String, HexaBPM__SR_Doc__c>();
            if(!String.isBlank(amedId)) {
                for(HexaBPM__SR_Doc__c srDocsObj :[SELECT id, 
                                                   HexaBPM__Document_Master__r.HexaBPM__Code__c, 
                                                   HexaBPM__SR_Template_Doc__c
                                                   FROM HexaBPM__SR_Doc__c
                                                   WHERE HexaBPM_Amendment__c = :amedId
                                                   AND HexaBPM__Document_Master__r.HexaBPM__Code__c IN :docMasterContentDocMap.keySet()
                                                  ]) {
                                                      docMasterSRDocsMap.put(srDocsObj.HexaBPM__Document_Master__r.HexaBPM__Code__c, srDocsObj);
                                                      
                                                  }
                system.debug('@@@@@@@@@ docMasterSRDocsMap ' + docMasterSRDocsMap);
            }
            
            system.debug('@@@@@@@@@ docMasterContentDocMap.keySet() ' + docMasterContentDocMap.keySet());
            // getting all SR template docs from SR.
            Map<String, HexaBPM__SR_Template_Docs__c> docMasterSRTempDocMap = new Map<String, HexaBPM__SR_Template_Docs__c>();
            Map<String, HexaBPM__SR_Template_Docs__c> srTempDocMasterMap = new Map<String, HexaBPM__SR_Template_Docs__c>();
            for(HexaBPM__SR_Template_Docs__c srTempDocs :[SELECT id, 
                                                          HexaBPM__Document_Master__r.Name, 
                                                          HexaBPM__Document_Master__c, 
                                                          HexaBPM__Document_Master__r.HexaBPM__Code__c
                                                          FROM HexaBPM__SR_Template_Docs__c
                                                          WHERE HexaBPM__SR_Template__c = :srTemplateId
                                                          AND HexaBPM__Document_Master__r.HexaBPM__Code__c IN :docMasterContentDocMap.keySet()
                                                         ]  ) {
                                                             docMasterSRTempDocMap.put(srTempDocs.HexaBPM__Document_Master__r.HexaBPM__Code__c, srTempDocs);
                                                             srTempDocMasterMap.put(srTempDocs.Id, srTempDocs);
                                                         }
            system.debug('@@@@@@@@@ docMasterSRTempDocMap ' + docMasterSRTempDocMap);
            
            List<HexaBPM__SR_Doc__c> lstOfSRDocToUpsert = new List<HexaBPM__SR_Doc__c>();
            for(String docMasterCode :docMasterContentDocMap.keySet()) {
                String contentDocID = docMasterContentDocMap.get(docMasterCode);
                system.debug('@@@@@@@@@ contentDocID  ' + contentDocID);
                system.debug('@@@@@@@@@ srIdSet  ' + reqWrap.srIdSet);
                for(Id subSRId:reqWrap.srIdSet){
                    HexaBPM__SR_Doc__c srDocsObj;
                    
                    //if srdoc present in amend.
                    if(docMasterSRDocsMap.size() > 0 && docMasterSRDocsMap.containsKey(docMasterCode)) {
                        srDocsObj = docMasterSRDocsMap.get(docMasterCode);
                    } else //if srdoc not present in amend.
                    {
                        system.debug('@@@@@@@@@ ceate sr doc ' + docMasterSRTempDocMap);
                        system.debug('@@@@@@@@@ docMasterCode ' + docMasterCode);
                        // create SR docs under amendment.
                        if(docMasterSRTempDocMap.containsKey(docMasterCode)) {
                            HexaBPM__SR_Template_Docs__c srTempDocs = docMasterSRTempDocMap.get(docMasterCode);
                            if(srTempDocs != NULL) {
                                srDocsObj = new HexaBPM__SR_Doc__c();
                                srDocsObj.Name = srTempDocs.HexaBPM__Document_Master__r.Name;
                                srDocsObj.HexaBPM__Service_Request__c = subSRId;
                                srDocsObj.HexaBPM_Amendment__c = amedId;
                                srDocsObj.HexaBPM__Document_Master__c = srTempDocs.HexaBPM__Document_Master__c;
                                srDocsObj.HexaBPM__SR_Template_Doc__c = srTempDocs.Id;
                                srDocsObj.HexaBPM__From_Finalize__c = true;
                            }                        
                        }
                        
                    }
                    if(srDocsObj != NULL) {
                        lstOfSRDocToUpsert.add(srDocsObj);
                    }
                }
            }
            
            system.debug('@@@@@@@@@ lstOfSRDocToUpsert ' + lstOfSRDocToUpsert);
            if(lstOfSRDocToUpsert != NULL && lstOfSRDocToUpsert.size() > 0) {
                upsert lstOfSRDocToUpsert;
            }
            
            
            List<ContentDocumentLink> conDocLnkToInsert = new List<ContentDocumentLink>();
            
            //reparenting
            for(HexaBPM__SR_Doc__c srDoc :lstOfSRDocToUpsert) {
                
                HexaBPM__SR_Template_Docs__c  srDocTemp = srTempDocMasterMap.get(srDoc.HexaBPM__SR_Template_Doc__c);
                system.debug('@@@@@@@@@ srDocTemp  ' + srDocTemp);
                String documentMasterCode = srDocTemp.HexaBPM__Document_Master__r.HexaBPM__Code__c;
                String  contentDocId = docMasterContentDocMap.get(documentMasterCode);
                
                ContentDocumentLink cDe = new ContentDocumentLink();
                cDe.ContentDocumentId = contentDocId;
                cDe.LinkedEntityId = srDoc.Id; // you can use objectId,GroupId etc
                cDe.ShareType = 'V'; // Viewer permission, checkout description of ContentDocumentLink object for more details
                cDe.Visibility = 'AllUsers';
                conDocLnkToInsert.add(cDe);
                
            }
            
            if(conDocLnkToInsert.size() > 0) {
                insert conDocLnkToInsert;
            }
            
            
            //Delete the DocumentLink from the SR - Cleanup of Files
            if(docMasterContentDocMap != NULL && docMasterContentDocMap.size() > 0) {
                delete [SELECT id
                        FROM ContentDocumentLink
                        WHERE ContentDocumentId in :docMasterContentDocMap.values() and LinkedEntityId = :srId];
            }
            
            
        }
        return respWrap;
        
    }
    
    public class RequestWrapper {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId { get; set; }
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public Set<Id> srIdSet { get; set; }
        @AuraEnabled public Id amedId { get; set; }
        @AuraEnabled public Map<String, String> docMasterContentDocMap { get; set; }
        
        public RequestWrapper() {
            srIdSet = new Set<Id>();
        }
    }
    
    public class ResponseWrapper {
        @AuraEnabled public String errorMessage { get; set; }
        public ResponseWrapper() {
        }
    }
    
    
}