/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestRORPUserCls {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
         map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('RORP_User_Access_Form')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        SR_Template__c objSRTemp = new SR_Template__c(Name = 'RORP_User_Access_Form',SR_RecordType_API_Name__c='RORP_User_Access_Form');
        insert objSRTemp;
        
        Lookup__c Nati = new Lookup__c(Type__c='Nationality',Name='USA');
        insert Nati;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        list<SR_Status__c> lstStatus = new list<SR_Status__c>();
        lstStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new SR_Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new SR_Status__c(Name='Approved',Code__c='Approved'));
        
        
        ApexPages.StandardController controller = new ApexPages.StandardController(new Service_Request__c());
        RORPUserCls objRORPUserCls = new RORPUserCls(controller);
        
        objRORPUserCls = new RORPUserCls();
        objRORPUserCls.PassportCopy = new Attachment();
        objRORPUserCls.sUser = new Service_Request__c();
        objRORPUserCls.sUser.First_Name__c = 'RB';
        objRORPUserCls.sUser.Last_Name__c = 'Nagaboina';
        objRORPUserCls.sUser.Position__c = 'Consultant';
        objRORPUserCls.sUser.Date_of_Birth__c = Date.newInstance(1947, 08, 15);
        objRORPUserCls.sUser.Nationality_list__c = 'USA';
        objRORPUserCls.sUser.Passport_Number__c = '123456';
        objRORPUserCls.sUser.Send_SMS_To_Mobile__c = '+971555789456';
        objRORPUserCls.sUser.Title__c = 'CEO';
        objRORPUserCls.sUser.Email__c = 'test@test.test';
        
        objRORPUserCls.saveInfo();  
        
        objRORPUserCls.PassportCopy = new Attachment();
        objRORPUserCls.PassportCopy.Body = blob.valueOf('Test Passport');
        objRORPUserCls.PassportCopy.Name = 'Test Passport.pdf';
        
        objRORPUserCls.saveInfo();
        
        insert lstStatus;
        
        objRORPUserCls.PassportCopy = new Attachment();
        objRORPUserCls.PassportCopy.Body = blob.valueOf('Test Passport');
        objRORPUserCls.PassportCopy.Name = 'Test Passport.pdf';
        
        objRORPUserCls.saveInfo();
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.First_Name__c = 'RB';
        objSR.Last_Name__c = 'Nagaboina';
        objSR.Position__c = 'Consultant';
        objSR.Date_of_Birth__c = Date.newInstance(1947, 08, 15);
        objSR.Nationality_list__c = 'USA';
        objSR.Passport_Number__c = '123456';
        objSR.Send_SMS_To_Mobile__c = '+971555789456';
        objSR.Title__c = 'CEO';
        objSR.Email__c = 'test@test.test';
        insert objSR;
        
        Step__c objStep = new Step__c();
        //for(Service_Request__c objS : [select Id from Service_Request__c where Passport_Number__c = '123456']){
            objStep.SR__c = objSR.Id;
        //\\}
        RORPUserCls.createUser(objStep);
      
    }
    
     static testMethod void updateRoleTest() {
         
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
       for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact'; 
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
        
       map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
       for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
       } 
       SR_Template__c objTemplate1 = new SR_Template__c();
       objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation1';
       objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation1';
       objTemplate1.Active__c = true;
       objTemplate1.SR_Group__c='GS';
       insert objTemplate1;
        
       Step_Template__c objStepType1 = new Step_Template__c();
       objStepType1.Name = 'test';
       objStepType1.Code__c = 'test';
       objStepType1.Step_RecordType_API_Name__c = 'General';
       objStepType1.Summary__c = 'Test';
       insert objStepType1;
        
	   Service_Request__c objSR3 = new Service_Request__c();
       objSR3.Customer__c = objAccount.Id;
       objSR3.Email__c = 'testsr@difc.com';    
       objSR3.Contact__c = objContact.Id;
       objSR3.Express_Service__c = false;
       objSR3.SR_Template__c = objTemplate1.Id;
       insert objSR3;
       
         
       Status__c objStatus = new Status__c();
       objStatus.Name = 'test';
       objStatus.Type__c = 'Start';
       objStatus.Code__c = 'test';
       insert objStatus;
       
       step__c objStep = new Step__c();
       objStep.SR__c = objSR3.Id;
       objStep.Step_Template__c = objStepType1.id ;
       objStep.No_PR__c = false;
       objStep.status__c = objStatus.id;
       insert objStep;
       
         RORPUserCls.updateRole(objStep);
     }
}