/******************************************************************************************
 *  Author   : Ravindra Babu Nagaboina
 *  Company  : NSI DMCC
 *  Date     : 03rd Oct 2016   
 *  Description : This class will be used as helper class for Mortgage module.                       
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.0  03/Oct/2016      Ravi                Created
*******************************************************************************************/
public without sharing class RORPMortgageUserContext {
	
	public User CurrentUser {get;set;}
    public String CustomerId {get;set;}
    public String CustomerName {get;set;}
    public String RecordTypeId {get;set;}
    public String SRId {get;set;}
    public Boolean IsPortaluser {get;set;}
    public string MenuText {get;set;}
    public string Description {get;set;}
    public Boolean IsDetail {get;set;}
	public String Type {get;set;}
	public string SapBPNo {get;set;}
	public string RecordTypeName {get;set;}
	
	public map<string,string> mapAmendmentRecordTypes {get;set;}	

	public static map<string,string> getmendmentRecordTypes(){
		map<string,string> mapRTs = new map<string,string>();
		for(RecordType objRT : [select Id,DeveloperName from RecordType where sObjectType='Amendment__c']){
			mapRTs.put(objRT.DeveloperName,objRT.Id);
		}
		return mapRTs;
	}
	
	public static string getSRDescription(string SRRecordType){
		string SRDescription = '';
		for(SR_Template__c objST : [select Id,SR_Description__c from SR_Template__c where SR_RecordType_API_Name__c=:SRRecordType ]){
			SRDescription = objST.SR_Description__c;
		}
		return SRDescription;
	}
}