/*********************************************************************************************************************
    Author      :   Saima Hidayat
    Description :   Mapping SAP Relationships to ROC Relationships and Legal Structures Full Forms
    Date        :   10-Sep-2014
    
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.1    08-02-2015  Saima         Changed 'designated member' mapping to 'Has member' 
V1.2    14-08-2016  Shabbir       Changed the business Activity "Investment Company" to "Investment Fund" as per #3164
v1.3    18-04-2018  Danish        Add new legal structure and relation ship type for foundation as per new requirment
V1.4    06/Jan/2019 Arun          Changed for new company law   
*********************************************************************************************************************/

public without sharing class configDetailsCls{


public static String getSAPReltype(String str){
    String reltype = null;
    str=str.toLowerCase();
    if(str=='guardian'){
        reltype = 'has guardian';
        return reltype;
    }
    if(str=='Council Member'){
        reltype = 'has Council Member';
        return reltype;
    }
    if(str=='registered agent'){
        reltype = 'registered agent';
        return reltype;
    }
    if(str=='founder'){
        reltype = 'has founder';
        return reltype;
    }
    if(str=='director'){
        reltype = 'has director';
        return reltype;
    }
    if(str=='director'){
        reltype = 'has director';
        return reltype;
    }else if(str=='company secretary'){
        reltype='has company secretary';
        return reltype;
    }else if(str == 'founding member'){
        reltype = 'has founding member';
        return reltype;
    }else if(str=='general partner'){
        reltype = 'has general partner';
        return reltype;
    }else if(str=='limited partner'){
        reltype = 'has limited partner';
        return reltype;
    }else if(str=='manager'){
        reltype = 'has manager';
        return reltype;
    }else if(str=='member'){
        reltype = 'is member llc of';
        return reltype;
    }else if(str=='shareholder'){
        reltype = 'is shareholder of';
        return reltype;
    }else if(str=='ubo'){
        reltype = 'beneficiary owner';
        return reltype;
    }else if(str=='services personnel'){
        reltype = 'has service document responsible for';
        return reltype;
    }else if(str=='authorized representative'){
        reltype = 'has authorized representative';
        return reltype;
    }else if(str=='authorized signatory'){
        reltype = 'is authorized signatory for';
        return reltype;
    }else if(str=='designated member'){
        reltype = 'has member';//V1.1
        return reltype;
    }else if(str=='auditor'){
        reltype = 'has auditor';
        return reltype;
    }
        //Start New company law change  V1.4
    else if(str=='ceo/regional head/chairman'){
        reltype = 'has ceo/regional head/chairman';
        return reltype;
    }
    else if(str=='general communication'){
        reltype = 'has corporate communication';
        return reltype;
    }
    //End 
    
    else
        return reltype = str;
    
}


public static String getLegaltype(String str)
{
    String legtype = '';
    
    if(str=='Foundation')
    {
        legtype = 'Foundation';
        return legtype;
    }
    
    if(str=='LTD')
    {
        legtype = 'Private Company. The name must end with Ltd or Limited';
        return legtype;
    }
    else if(str=='LTD SPC'){
        legtype='Special Purpose Company';
        return legtype;
    }else if(str == 'LTD PCC'){
        legtype = 'Protected Cell Company';
        return legtype;
    }else if(str=='LTD IC'){
        //legtype = 'Investment Company'; v1.2
        legtype = 'Investment Fund';
        return legtype;
    }else if(str=='LTD National'){
        legtype = 'National Entity';
        return legtype;
    }else if(str=='LTD Supra National'){
        legtype = 'Supra National Entity';
        return legtype;
    }else if(str=='LLC'){
        legtype = 'Limited Liability Company';
        return legtype;
    }else if(str=='LLP'){
        legtype = 'Limited Liability Partnership';
        return legtype;
    }else if(str=='RLLP'){
        legtype = 'Recognized Limited Liability Partnership';
        return legtype;
    }else if(str=='FRC'){
        legtype = 'Foreign Recognized Company';
        return legtype;
    }else if(str=='GP'){
        legtype = 'General Partnership';
        return legtype;
    }else if(str=='RP'){
        legtype = 'Recognized Partnership';
        return legtype;
    }else if(str=='RLP'){
        legtype = 'Recognized Limited Partnership';
        return legtype;
    }else if(str=='LP'){
        legtype = 'Limited Partnership';
        return legtype;
    }else if(str=='NPIO'){
        legtype = 'Non Profit Incorporated Organization';
        return legtype;
    }
    else
        return str;

}
}