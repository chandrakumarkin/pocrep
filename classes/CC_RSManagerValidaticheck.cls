/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-16-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-07-2020   Shoaib Tariq   Initial Version
**/
global without sharing class CC_RSManagerValidaticheck implements HexaBPM.iCustomCodeExecutable {
      /**
     * Check RSManagerStep is created
      */ 
      global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        HexaBPM__Service_Request__c thisRequest = [SELECT Id,HexaBPM__Customer__c ,RecordTypeId,Setting_Up__c FROM HexaBPM__Service_Request__c WHERE ID =: stp.HexaBPM__SR__c ];
        If([SELECT Id,Step_Template_Code__c FROM HexaBPM__Step__c
                                            WHERE HexaBPM__SR__c =: thisRequest.Id 
                                            AND (HexaBPM__Status__r.Name = 'In Progress' OR  HexaBPM__Status__r.Name = 'Approved')
                                            AND Step_Template_Code__c = 'RS_MANAGER_REVIEW' LIMIT 1 ].Size()>0){
            return 'True';                                      
        }
        return 'False';
    }
}