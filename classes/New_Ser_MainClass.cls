//V1.0 Zoeb Shaikh --- 16th March 2021 -- Included Auditor Type(Configurable to include multiple values for Auditor Type)


/*********************************************************************************************************************
--------------------------------------------------------------------------------------------------------------------------
Modification History 
----------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
----------------------------------------------------------------------------------------              

V1.1    20-June-2021 Arun       Arnu -Added Audit fields #16639

**********************************************************************************************************************/



public without sharing class New_Ser_MainClass{

public Service_Request__c SRData{get;set;}

 public string RecordTypeId;
 public map<string,string> mapParameters;
 
  public  Attachment ObjAttachment{get;set;}
  
 
  
 public Account ObjAccount{get;set;}

    public New_Ser_MainClass(ApexPages.StandardController controller) 
    {
    
  
    ObjAttachment=new Attachment();
     ObjAccount=new account();
    
        List<String> fields = new List<String> {'Agreement_Date__c','Legal_Structures__c','Financial_Year_End_mm_dd__c','Authority_Name__c','Confirm_Change_of_Entity_Name__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        
        SRData=(Service_Request__c )controller.getRecord();
        //if (Test.isRunningTest()) system.assertEquals(null, SRData);
        
    
        mapParameters = new map<string,string>();
        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();
            
        
        
       if(mapParameters.get('RecordType')!=null) 
       RecordTypeId= mapParameters.get('RecordType');
       
       
            for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Sub_Legal_Type_of_Entity__c,Contact.Account.Legal_Type_of_Entity__c,Contact.Account.Annual_Turnover_less_than_USD_5_million__c,Contact.AccountId,Contact.Account.Company_Type__c,Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  from User where Id=:userinfo.getUserId()])
            {
             if(SRData.id==null)
             {
                
                SRData.Customer__c = objUsr.Contact.AccountId; SRData.RecordTypeId=RecordTypeId;  SRData.Email__c = objUsr.Email; SRData.Send_SMS_To_Mobile__c = objUsr.Phone;ObjAccount= objUsr.Contact.Account;  SRData.Customer__r=ObjAccount; SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                SRData.Financial_Year_End_mm_dd__c=objUsr.Contact.Account.Financial_Year_End__c;
                //V1.1
               
                SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c; SRData.Confirm_Change_of_Trading_Name__c=objUsr.Contact.Account.Annual_Turnover_less_than_USD_5_million__c;  SRData.Authority_Name__c=objUsr.Contact.Account.Sub_Legal_Type_of_Entity__c;
                
                    
             }
             
            
               ObjAccount=objUsr.Contact.Account;
                
             
                
            }

            
        
        

    }

    
    public  void RowValueChange()
    {
            System.debug('=====================RowValueChange==========');
        string field_V= Apexpages.currentPage().getParameters().get('field_V');
        string field_F= Apexpages.currentPage().getParameters().get('field_F');
        System.debug(field_V);
        System.debug(field_F);
        
        if(!String.isblank(field_V) && !String.isblank(field_F))
        {
            SRData.put(field_F,field_V);    
        
        }
        
        //re-Set Audit values
        if(field_V=='No')
        {
               SRData.Monthly_Accommodation__c=null;
               SRData.Security_Time__c=null;
        } 

        
    }
    
    
    public List<SelectOption> getYear()
    {
       List<SelectOption> options = new List<SelectOption>();
       Integer This_Year=Date.today().year()+1;
       
       for(Integer  i=2015;i<This_Year;i++)
       {
               options.add(new SelectOption(string.valueof(i), string.valueof(i)));
       }     
       return options;
    }
    
    public List<SelectOption> getRegisteredAuditors()
    {
       //V1.0
       Id nonRegisteredAccountRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Non_Registered_Accounts').getRecordTypeId();
       String[] auditorType = (System.Label.Auditors_Type).split(',');
       system.debug('---AuditorType' +auditorType);
       List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
       for( Account Obj:[select id,Name from account where Auditor_Type__c != null and recordtypeid = :nonRegisteredAccountRecTypeId  and Auditor_Type__c IN :auditorType order by Name ])
       {
               options.add(new SelectOption(Obj.Name, Obj.Name));
       }     
       return options;
    }
    
    
    
    public  PageReference SaveConfirmation()
    {
     PageReference acctPage;
    try
    {
        upsert SRData;
        acctPage = new ApexPages.StandardController(SRData).view();
        acctPage.setRedirect(true);
     }  catch(DmlException e){}
     
        return acctPage;
        
        
     }
    
    

}