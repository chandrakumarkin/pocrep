/******************************************************************************************************
*  Author   : Kumar Utkarsh
*  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* Test Class of WebServiceGdrfaDocumentCompressClass.
*******************************************************************************************************/
@isTest
public class WebServiceGdrfaDocumentCompressTest {
    
    @testSetup static void testSetupdata(){
        List<Service_Request__c> listSR = new List<Service_Request__c>();
        Service_Request__c testSr = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        Id RecordTypeIdSr = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-New').getRecordTypeId();
        testSr.RecordTypeId = RecordTypeIdSr;
        testSr.Mobile_Number__c = '';
        testSr.First_Name_Arabic__c = 'كاركيرا';
        testSr.Date_of_Birth__c = Date.newInstance(2001, 10, 9);
        //testSr.Nationality__c = lookUpNationality.Id;
        testSr.Nationality_List__c = 'United Arab Emirates';
        testSr.Previous_Nationality__c = 'United Arab Emirates';
        testSr.Country_of_Birth__c = 'United Arab Emirates';
        //testSr.Country__c = 'United Arab Emirates';
        testSr.Marital_Status__c = 'Single';
        testSr.Religion__c = 'Hindhu';
        testSr.Qualification__c = 'UNIVERSITY DEGREE';
        testSr.P_O_Box_Emirate__c = 'Dubai';
        //testSr.Occupation_GS__c = 'GENERAL MANAGER';
        testSr.Passport_Type__c = 'Normal';
        testSr.Location__c = 'TestAbc';
        testSr.Mother_Full_Name__c = 'abc';
        testSr.Mother_s_full_name_Arabic__c = 'كاركيرا';
        testSr.Middle_Name_Arabic__c = 'كاركيرا';
        testSr.Establishment_Card_No__c = '123456';
        testSr.Passport_Place_of_Issue__c = 'United Arab Emirates';
        //testSr.Building__c = 'Test';
        testSr.Emirates_Id_Number__c = '123';
        testSr.Area__c = 'Zabeel 1';
        testSr.Street_Address__c = 'Test';
        testSr.Current_Registered_Street__c = 'Test';
        testSr.Sponsor_Mobile_No__c = '+971500000001';
        testSr.Address_Details__c = 'Test';
        testSr.Passport_Country_of_Issue__c = 'United Arab Emirates';
        testSr.Type_of_Request__c = 'Applicant Outside UAE';
        testSr.Identical_Business_Domicile__c = 'United Arab Emirates';
        testSr.City_Town__c = 'Abu Dhabi';
        testSr.Emirate__c = 'Abu Dhabi';
        testSr.City__c = 'Abu Dhabi';
        testSr.Building_Name__c = 'Al Wadi';
        testSr.Phone_No_Outside_UAE__c = '+971000000001';
        testSr.Last_Name_Arabic__c = 'كاركيرا';
        testSr.Place_of_Birth__c = 'Dubai';
        testSr.Visa_Duration__c = '3 Years';
        testSr.Would_you_like_to_opt_our_free_couri__c = 'Yes';
        listSR.add(testSR);
        insert listSR;
        
        SR_Doc__c srDoc = new SR_Doc__c();
        srDoc.Service_Request__c = testSr.Id;
        srDoc.Name = 'Coloured Photo';
        //srDoc.Doc_ID__c = attch.Id;
        //srDoc.GDRFA_Parent_Document_Id__c = '50f41f94';
        insert srDoc;
        
        Attachment attch = new Attachment();
        attch.Name = 'Test attch';
        attch.Body = Blob.valueOf('Unit Test Attachment Body');
        attch.ParentId = srDoc.Id;
        attch.ContentType = 'image/jpg';
        
        insert attch;
    }
    
    static testMethod void webServiceGdrfaCompressTest1(){
        List<Id> SrIds = new List<Id>();
        List<Service_Request__c> SrTest = [SELECT Id, Service_Type__c, Nationality__c, Previous_Nationality__c, Country_of_Birth__c, Gender__c, Marital_Status__c, Religion__c, Qualification__c, Occupation__c, Passport_Type__c,
                                           First_Name__c, First_Name_Arabic__c, Middle_Name__c, Middle_Name_Arabic__c, Location__c, Last_Name__c, Last_Name_Arabic__c, Mother_Full_Name__c, Establishment_Card_No__c,
                                           Mother_s_full_name_Arabic__c, Date_of_Birth__c, Place_of_Birth__c, Place_of_Birth_Arabic__c, First_Language__c, Passport_Number__c, Passport_Date_of_Issue__c, Passport_Date_of_Expiry__c,
                                           Passport_Place_of_Issue__c, Building__c, Emirates_Id_Number__c, Area__c, Street_Address__c, Current_Registered_Street__c, Mobile_Number__c, Sponsor_Mobile_No__c, Address_Details__c,
                                           Passport_Country_of_Issue__c, Type_of_Request__c, Identical_Business_Domicile__c, City_Town__c, Emirate__c, City__c, Building_Name__c, Phone_No_Outside_UAE__c, Visa_Duration__c
                                           FROM Service_Request__c WHERE Establishment_Card_No__c = '123456'];
        
        for(Service_Request__c Sr: SrTest){
            SrIds.add(Sr.Id);
        }
        
        Test.setMock(HttpCalloutMock.class, new webServiceGdrfaFileCompressorMock());
        Test.startTest();
        WebServiceGdrfaDocumentCompressClass.compressSrAttachment(SrIds);
        Test.stopTest();
    }
    
      static testMethod void webServiceGdrfaCompressTest2(){
        Service_Request__c SrTest = [SELECT Id, Service_Type__c, Nationality__c, Previous_Nationality__c, Country_of_Birth__c, Gender__c, Marital_Status__c, Religion__c, Qualification__c, Occupation__c, Passport_Type__c,
                                           First_Name__c, First_Name_Arabic__c, Middle_Name__c, Middle_Name_Arabic__c, Location__c, Last_Name__c, Last_Name_Arabic__c, Mother_Full_Name__c, Establishment_Card_No__c,
                                           Mother_s_full_name_Arabic__c, Date_of_Birth__c, Place_of_Birth__c, Place_of_Birth_Arabic__c, First_Language__c, Passport_Number__c, Passport_Date_of_Issue__c, Passport_Date_of_Expiry__c,
                                           Passport_Place_of_Issue__c, Building__c, Emirates_Id_Number__c, Area__c, Street_Address__c, Current_Registered_Street__c, Mobile_Number__c, Sponsor_Mobile_No__c, Address_Details__c,
                                           Passport_Country_of_Issue__c, Type_of_Request__c, Identical_Business_Domicile__c, City_Town__c, Emirate__c, City__c, Building_Name__c, Phone_No_Outside_UAE__c, Visa_Duration__c
                                           FROM Service_Request__c WHERE Establishment_Card_No__c = '123456'];
        
        Test.setMock(HttpCalloutMock.class, new webServiceGdrfaFileCompressorMock());
        Test.startTest();
        WebServiceGDRFACompressDocButtonApex.GDRFAImageCompressorCallout(SrTest.Id);
        Test.stopTest();
    }
}