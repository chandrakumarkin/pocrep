public class CommunicationTriggerHandler {
	
    public static void onbeforeInsert(List<CommunicationTrail__c> communicationTrailList){
        Set<String> emailTemplateSet = new Set<String>();
        for(CommunicationTrail__c commTrailObj : communicationTrailList){
            if(String.isNotBlank(commTrailObj.Email_template__c)){
                emailTemplateSet.add(commTrailObj.Email_template__c);
            }            
        }
        system.debug('### emailTemplateSet '+emailTemplateSet);
        if(!emailTemplateSet.isEmpty()){
            List<EmailTemplate> templateObjList = [SELECT Id, Name, Subject, HtmlValue,Body FROM EmailTemplate WHERE Name IN :emailTemplateSet];   
            system.debug('### templateObjList '+templateObjList);
			Set<String> queriedTemplateSet = new Set<String>(); 
            for(EmailTemplate emTempObj : templateObjList){
                queriedTemplateSet.add(emTempObj.Name);
            }
            system.debug('### queriedTemplateSet '+queriedTemplateSet);
            for(CommunicationTrail__c commTrailObj : communicationTrailList){
                if(String.isNotBlank(commTrailObj.Email_Template__c) && !queriedTemplateSet.isEmpty() &&  !queriedTemplateSet.contains(commTrailObj.Email_Template__c)){
                    commTrailObj.Email_Template__c.addError('Please enter valid Email Template Name.');
                    system.debug('in if loop');
                }
                else if(queriedTemplateSet.isEmpty()){
                    commTrailObj.Email_Template__c.addError('Please enter valid Email Template Name.');
                }
                if(String.isNotBlank(commTrailObj.Body__c) && commTrailObj.Body__c.length() > 10000){
                    system.debug('### Error in body ');
                    commTrailObj.Body__c.addError('Email body should be less than 10000 characters');
                }
            }
        }        
    }
    
    public static void onafterInsertUpdate (Map<Id,CommunicationTrail__c> communicationTrailMap, List<CommunicationTrail__c> comunicationTrailList){
        //delete existing 
        List<Recepient__c> existingRecepientList = [SELECT Id, Communication_Trail__c FROM Recepient__c WHERE Communication_Trail__c = :communicationTrailMap.keyset()];
        if(!existingRecepientList.isEmpty()){
            delete existingRecepientList;
        }	
        List<Recepient__c> recepientList = new List<Recepient__c>();
        Set<String> buildingNameSet = new Set<String>();
        Set<String> entityTypeSet = new Set<String>();
        Map<String,Set<Id>> buildingEntitySetIdMap = new Map<String,Set<Id>>();
        for(Id commId : communicationTrailMap.keySet()){
            if(communicationTrailMap.get(commId).Building_Name__c != null){
                buildingNameSet.addAll(communicationTrailMap.get(commId).Building_Name__c.split(';'));
            } 
            if(communicationTrailMap.get(commId).Entity_Type__c != null){
                entityTypeSet.add(communicationTrailMap.get(commId).Entity_Type__c);
            }
            if(buildingEntitySetIdMap.containsKey(communicationTrailMap.get(commId).Building_Name__c+';;;'+communicationTrailMap.get(commId).Entity_Type__c)){
                buildingEntitySetIdMap.get(communicationTrailMap.get(commId).Building_Name__c+';;;'+communicationTrailMap.get(commId).Entity_Type__c).add(commId);
            }
            else{
             	buildingEntitySetIdMap.put(communicationTrailMap.get(commId).Building_Name__c+';;;'+communicationTrailMap.get(commId).Entity_Type__c,new Set<Id>{commId});   
            }          
        }
        
        List<Account> accList = [SELECT Id, Company_Type__c,Building_Name__c,(SELECT Id, Active__c,Relationship_Type__c,Subject_Account__c,Object_Contact__c,
                           Object_Contact__r.Phone, Object_Contact__r.Email,Subject_Account__r.Building_Name__c, Subject_Account__r.Company_type__c
                           FROM Primary_Account__r WHERE Active__c = true AND Relationship_Type__c = 'Emergency contact'),
                           Name FROM Account 
                           WHERE Building_Name__c IN :buildingNameSet AND ROC_Status__c = 'Active'];
        List<Relationship__c> relationshipList = new List<Relationship__c>();
        for(Account acc : accList){
            if(acc.Primary_Account__r != null){
                relationshipList.addAll(acc.Primary_Account__r);
            }
        }        
        for(CommunicationTrail__c commtrail : comunicationTrailList){
            for(Relationship__c relationObj : relationshipList){                
                if(commtrail.Building_Name__c.contains(relationObj.Subject_Account__r.Building_Name__c)){
                    if((commtrail.Entity_Type__c == 'Retail' || commtrail.Entity_Type__c == 'Both') && relationObj.Subject_Account__r.Company_Type__c == 'Retail'){
                        //if(mapCommIdRelationship.containsKey(commtrail.Id)){
                            Recepient__c recepientObj = new Recepient__c();
                			recepientObj.Contact__c = relationObj.Object_Contact__c;                
                			recepientObj.Phone__c = relationObj.Object_Contact__r.Phone;
                            recepientObj.Email__c = relationObj.Object_Contact__r.Email;
                            recepientObj.Communication_Trail__c = commtrail.Id;
                           // recepientObj.Account__c = relationObj.Subject_Account__c;
                        	recepientObj.AccountLookup__c = relationObj.Subject_Account__c;
                            recepientObj.Entity_Type__c = relationObj.Subject_Account__r.Company_type__c;
                            recepientObj.Building_Name__c = relationObj.Subject_Account__r.Building_Name__c; 
                        	recepientObj.UniqueId__c = commtrail.Id+','+relationObj.Subject_Account__c+','+relationObj.Object_Contact__c;
							recepientList.add(recepientObj);   
                        	system.debug('## retail recepient '+recepientObj);
                            //mapCommIdRelationship.get(commtrail.Id).add(recepientObj);
                        /*}
                        else{
                            Recepient__c recepientObj = new Recepient__c();
                			recepientObj.Contact__c = relationObj.Object_Contact__c;                
                			recepientObj.Phone__c = relationObj.Object_Contact__r.Phone;
                            recepientObj.Email__c = relationObj.Object_Contact__r.Email;
                            recepientObj.Communication_Trail__c = commtrail.Id;
                            recepientObj.Account__c = relationObj.Subject_Account__c;
                            recepientObj.Entity_Type__c = relationObj.Subject_Account__r.Company_type__c;
                            recepientObj.Building_Name__c = relationObj.Subject_Account__r.Building_Name__c;
                            mapCommIdRelationship.put(commtrail.Id,new List<Recepient__c>{recepientObj});
                        }*/
                    }
                }
                if((commtrail.Entity_Type__c == 'Commercial' || commtrail.Entity_Type__c == 'Both') && relationObj.Subject_Account__r.Company_Type__c != 'Retail'){
                        //if(mapCommIdRelationship.containsKey(commtrail.Id)){
                            Recepient__c recepientObj = new Recepient__c();
                			recepientObj.Contact__c = relationObj.Object_Contact__c;                
                			recepientObj.Phone__c = relationObj.Object_Contact__r.Phone;
                            recepientObj.Email__c = relationObj.Object_Contact__r.Email;
                            recepientObj.Communication_Trail__c = commtrail.Id;
                            //recepientObj.Account__c = relationObj.Subject_Account__c;
                    		recepientObj.AccountLookup__c = relationObj.Subject_Account__c;
                            recepientObj.Entity_Type__c = relationObj.Subject_Account__r.Company_type__c;
                            recepientObj.Building_Name__c = relationObj.Subject_Account__r.Building_Name__c; 
                    		recepientObj.UniqueId__c = commtrail.Id+','+relationObj.Subject_Account__c+','+relationObj.Object_Contact__c;
                    		recepientList.add(recepientObj);
                    		system.debug('## commercial recepient '+recepientObj);
                            //mapCommIdRelationship.get(commtrail.Id).add(recepientObj);
                        /*}
                        else{
                            Recepient__c recepientObj = new Recepient__c();
                			recepientObj.Contact__c = relationObj.Object_Contact__c;                
                			recepientObj.Phone__c = relationObj.Object_Contact__r.Phone;
                            recepientObj.Email__c = relationObj.Object_Contact__r.Email;
                            recepientObj.Communication_Trail__c = commtrail.Id;
                            recepientObj.Account__c = relationObj.Subject_Account__c;
                            recepientObj.Entity_Type__c = relationObj.Subject_Account__r.Company_type__c;
                            recepientObj.Building_Name__c = relationObj.Subject_Account__r.Building_Name__c;
                            mapCommIdRelationship.put(commtrail.Id,new List<Recepient__c>{recepientObj});
                        }*/
                    }
            }
        }
        if(!recepientList.isEmpty()){
            system.debug('in upsert of recepientList');
            upsert recepientList UniqueId__c;
        }
        /*for(Id commId : communicationTrailMap.keySet()){
            List<String> buldingNameList = communicationTrailMap.get(commId).Building_Name__c != null ? communicationTrailMap.get(commId).Building_Name__c.split(';') : new List<String>();
            String relationshipType = 'Emergency contact';
            String query = 'SELECT Id,Active__c,Relationship_Type__c,Subject_Account__c, Subject_Account__r.Building_Name__c, Subject_Account__r.Company_type__c,'+
                'Object_Contact__c, Object_Contact__r.Phone, Object_Contact__r.Email FROM Relationship__c WHERE Active__c = true AND Relationship_Type__c = '+'\''+relationshipType+'\'';
            String type = 'Retail';
            if(!buldingNameList.isEmpty()) {
				String buildingString =  '('+ '\''+String.join(buldingNameList,',').replaceAll(',','\',\'').trim() + '\''+')'; 
                    query += 'AND Subject_Account__r.Building_Name__c IN '+buildingString;
            }
            if(String.isNotBlank(communicationTrailMap.get(commId).Entity_Type__c) && communicationTrailMap.get(commId).Entity_Type__c == 'Retail'){
                query+= 'AND Subject_Account__r.Company_Type__c = '+'\''+type+'\'';
            }
            else if(String.isNotBlank(communicationTrailMap.get(commId).Entity_Type__c) && communicationTrailMap.get(commId).Entity_Type__c == 'Commercial'){
                query+= 'AND Subject_Account__r.Company_Type__c != '+'\''+type+'\'';
            }                                                                                             
            system.debug('### query '+query);
            List<Relationship__c> relationshipList = Database.query(query);
            system.debug('### relationshipList '+relationshipList);
           
            for(Relationship__c relationObj : relationshipList){
                Recepient__c recepientObj = new Recepient__c();
                recepientObj.Contact__c = relationObj.Object_Contact__c;                
                recepientObj.Phone__c = relationObj.Object_Contact__r.Phone;
                recepientObj.Email__c = relationObj.Object_Contact__r.Email;
                recepientObj.Communication_Trail__c = commId;
                recepientObj.Account__c = relationObj.Subject_Account__c;
                recepientObj.Entity_Type__c = relationObj.Subject_Account__r.Company_type__c;
                recepientObj.Building_Name__c = relationObj.Subject_Account__r.Building_Name__c;
                recepientList.add(recepientObj);
            }
        }
        upsert recepientList;*/
        }
        /*Set<String> buldingNameSet = new Set<String>();
        for(CommunicationTrail__c commObj : communicationTrailMap.values()){
            if(commObj.Building_Name__c != null){
                buldingNameSet.addAll(commObj.Building_Name__c.split(';'));
            }
        }
        String relationshipType = 'Emergency contact';
        String query = 'SELECT Id,Active__c,Relationship_Type__c,Subject_Account__c, Subject_Account__r.Building_Name__c, Subject_Account__r.Company_type__c,'+
                'Object_Contact__c, Object_Contact__r.Phone, Object_Contact__r.Email FROM Relationship__c WHERE Active__c = true AND Relationship_Type__c = '+'\''+relationshipType+'\'';
        String type = 'Retail';
        if(!buldingNameSet.isEmpty()) {
            List<String> buildingNameList = new List<String> (buldingNameSet);
            String buildingString =  '('+ '\''+String.join(buildingNameList,',').replaceAll(',','\',\'').trim() + '\''+')'; 
            query += 'AND Subject_Account__r.Building_Name__c IN '+buildingString;
        }
        if(communicationObject != null && String.isNotBlank(communicationObject.Entity_Type__c) && communicationObject.Entity_Type__c == 'Retail'){
                query+= 'AND Subject_Account__r.Company_Type__c = '+'\''+type+'\'';
            }
            else if(communicationObject != null && String.isNotBlank(communicationObject.Entity_Type__c) && communicationObject.Entity_Type__c == 'Commercial'){
                query+= 'AND Subject_Account__r.Company_Type__c != '+'\''+type+'\'';
            }                                                                                             
            system.debug('### query '+query);*/
        
    
}