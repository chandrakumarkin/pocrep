/**
 * @description       : 
 * @author            : Mudasir
 * @group             : 
 * @last modified on  : 11-19-2020
 * @last modified by  : Mudasir
 * Modifications Log 
 * Ver   Date         Author    Modification
 * 1.0   10-07-2020   Mudasir   Initial Version
**/
public without sharing class DifcFitoutFeedbackSurveyController {
	@AuraEnabled 
	public static Step__c getStepDetails(String spepIdValue)
    {  
        Step__c stepRec =[Select id,name,SR__c,SR__r.Name,Step_name__c,Client_Name__c,SR_Status__c,SR_Menu_Text__c,SR__r.Contractor__r.Name,Comments__c,Step_Status__c,(Select id from Feedback_Surveys__r) from step__c where id=:spepIdValue];
        system.debug('Mudasir---'+stepRec);
        if(stepRec.Name =='Client Feedback' && stepRec.Feedback_Surveys__r != NULL && stepRec.Feedback_Surveys__r.size() > 0){
            system.debug('Mudasir feedback already provided');
        }
        return stepRec;
    }
    @AuraEnabled 
    public static List<Feedback_Survey__c> getRelatedQuestions(String stepIdVal)
    {
        List<Survey_Question__c> serveyQuestions = [SELECT Id,Question__c, Name from Survey_Question__c where active__c =true ORDER BY Name];
        List<Feedback_Survey__c> feedbackSurveyQuestions = new List<Feedback_Survey__c>();
        for(Survey_Question__c serveyQuestion: serveyQuestions){
            Feedback_Survey__c feedbackSurvey = new Feedback_Survey__c();
            feedbackSurvey.Question__c = serveyQuestion.Question__c;
            feedbackSurvey.Extra_Feedback__c = '';
            feedbackSurvey.Rating__c = 0;
            feedbackSurvey.Step__c = stepIdVal;
            feedbackSurvey.Survey_Question_ID__c = serveyQuestion.id; 
            feedbackSurvey.Remarks__c ='';
            system.debug('feedbackSurvey--'+feedbackSurvey.Question__c);
            feedbackSurveyQuestions.add(feedbackSurvey);
        }
        system.debug('feedbackSurveyQuestions--'+feedbackSurveyQuestions.size());
        return feedbackSurveyQuestions;
    }
    
    @AuraEnabled 
    public static void insertFitoutFeedback(List<Feedback_Survey__c> feedbackSurveys)
    {
        system.debug('feedbackSurveyQuestions--'+feedbackSurveys);
        insert feedbackSurveys;
        updateStep(feedbackSurveys[0].step__c,feedbackSurveys[0].remarks__c);
    } 
    
    @AuraEnabled 
    public static ServeyFeedbackWrapper getServeyFeedbackWrapperRecord(String spepIdValue){
        system.debug('Step Id is ---'+spepIdValue);
        ServeyFeedbackWrapper servFeedWrapper = new ServeyFeedbackWrapper();
        servFeedWrapper.stepRecordValue = getStepDetails(spepIdValue);
        servFeedWrapper.feedbackSurveyList= getRelatedQuestions(spepIdValue);
        servFeedWrapper.isOpenStep = servFeedWrapper.stepRecordValue != NULL && servFeedWrapper.stepRecordValue.Step_Status__c !='Feedback Updated' ? true : false;
        system.debug('servFeedWrapper---'+servFeedWrapper.feedbackSurveyList[0]);
        return servFeedWrapper;
    }
    public class ServeyFeedbackWrapper{
        @AuraEnabled 
        public Step__c stepRecordValue{get;set;}
        @AuraEnabled 
        public List<Feedback_Survey__c> feedbackSurveyList{get;set;}
        @AuraEnabled 
        public boolean isOpenStep{get;set;}
    }
    @future
    public static void updateStep(String stepId,String remarks)
    {
        Step__c step = new Step__c(id=stepId, Status__c=System.Label.Step_Status_Feedback_Updated,Comments__c =remarks);
        System.debug('step to update ---'+step);
        update step;
    } 
}