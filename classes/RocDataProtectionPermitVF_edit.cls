/******************************************************************************************************************
Author      :   Arun 
--------------------------------------------------------------------------------------------------------------------------
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By      Description
--------------------------------------------------------------------------------------------------------------------------             
V1.0   25-11-2020   Arun Singh       New class to support Notification of Personal Data Operations #12061
v1.1   13/3/2021 	Shikha Khanuja	 #13986 - Modified Code to allow removal/addition of DPC or DPO
v1.2   25/3/2021	Shikha Khanuja	 #13253	- Modified code to not show DPO question in case of edit
*******************************************************************************************************************/

public without sharing class RocDataProtectionPermitVF_edit {
    
    public Service_Request__c SRData{get;set;}
    
    public string RecordTypeId;
    public map<string,string> mapParameters;
    public string pageTitle {get;set;}
    public string pageDescription {get;set;}
    
    public string CheckBoxType                      {get;set;}
    //public list<Jurisdictions> DataProtections      {get;set;}
    public Integer RowIndex                         {get;set;}
    
    public List<Amendment__c> ListDataControlle{get;set;}
    public List<Data_Protection__c> ListDPJurisdiction{get;set;}
    
    //v1.1 - added disabled attribute
    public Boolean disableSave {get;set;}  
    public Boolean isEdit {get;set;}
    public Boolean isAdd {get;set;}    
    
    public Account ObjAccount{get;set;}
    
    public RocDataProtectionPermitVF_edit (ApexPages.StandardController controller) 
    {
        //v1.1 changes
        disableSave = false;
        //v1.2 changes
        isEdit = false;
        isAdd = false;
        
        ObjAccount=new account();
        ListDataControlle=new List<Amendment__c>();
        ListDPJurisdiction=new List<Data_Protection__c>();
        
        List<String> fields = new List<String> {'Purpose_of_Notification__c','Customer__c','Additional_Details__c','No_Accordance_with_Parts_7__c','Transferring_Personal_Data_outside_DIFC__c','Processing_any_senstive_data__c','In_Accordance_with_Article_12_1a__c','In_Accordance_with_Article_12_1a__c'};
            if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        
        SRData=(Service_Request__c )controller.getRecord();
        mapParameters = new map<string,string>();
        
        if(apexpages.currentPage().getParameters()!=null)
            mapParameters = apexpages.currentPage().getParameters();
        
        if(mapParameters.get('RecordType')!=null) 
            RecordTypeId= mapParameters.get('RecordType');
        
        
        for(SR_Template__c  page:[select id,Menutext__c,SR_Description__c from SR_Template__c where SR_RecordType_API_Name__c='Notification_of_Personal_Data_Operations' and Active__c=true]){
            
            pageTitle = page.Menutext__c;
            pageDescription = page.SR_Description__c ;
            
        }
                          
        for(User objUsr:[select id,ContactId,Email,Phone,Contact.AccountId,Contact.Account.Company_Type__c,Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Has_Permit_1__c,Contact.Account.Has_Permit_2__c,Contact.Account.Has_Permit_3__c,Contact.Account.Is_Foundation_Activity__c  from User where Id=:userinfo.getUserId()])
        {
            ObjAccount= objUsr.Contact.Account;
            if(SRData.id==null)
            {
                system.debug('### in null Id');
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                
                SRData.Customer__r=ObjAccount;
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                
                SRData.Transferring_Personal_Data_outside_DIFC__c=ObjAccount.Has_Permit_2__c;
                SRData.Processing_any_senstive_data__c=ObjAccount.Has_Permit_3__c;
                
                getDataControllerRel();
                RelationshipJurisdiction();
                FillSR();                    
            }
            else
            {
                //  JurisdictionInfo();
                system.debug('### in no null ');
                
            }
            
            
            
            
        }
        
        
        
    }
    
    public void FillSR()
    {
        
        for(Permit__c ObjPro:[select Id,No_Accordance_with_Article_14__c,No_Accordance_with_Article_15__c,No_Accordance_with_Article_40__c,No_Accordance_with_Parts_5_6__c, No_Accordance_with_Parts_7__c,Data_Subject_of_Article_12_11__c,No_Accordance_with_Article_16_17_18__c,In_accordance_with_Parts_7__c,Category_of_Article_3__c,In_accordance_with_Article_40__c,No_Accordance_with_Article_23_24_25__c,In_accordance_with_Parts_5_or_6__c,Methods_of_Article_40__c,In_accordance_with_Article_15__c,In_accordance_with_Article_23_24_25__c,In_accordance_with_Article_16_17_18__c,In_accordance_with_Article_3__c,In_accordance_with_Article_12_11__c,In_accordance_with_Article_13__c,In_accordance_with_Article_14__c,Name,Permit_Type__c,Personal_Data_Processing_Procedure__c,Purpose_of_Processing_Data__c,Other_Purpose_of_Processing_Data__c ,Data_Subjects_Personal_Data__c ,Other_Data_Subjects_Personal_Data__c ,Class_of_Personal_Data__c ,License__c ,Purpose_of_Notification__c ,Transferring_Personal_Data_Outside_DIFC__c,Processing_any_Special_Categories__c , In_accordance_with_Article_10_1__c , In_Accordance_with_Article_11__c ,Recognized_Jurisdictions__c,Adhering_to_Safe_Horbour__c ,In_Accordance_with_Article_12_1bj__c,Provisions_of_Article_12__c   from Permit__c where Account__c=:ObjAccount.id and  Status__c='Active'])
        {
            if( ObjPro.Name==label.Personal_Data_Processing_Operations)
            {
                
                SRData.Personal_Data_Processing_Procedure__c=ObjPro.Personal_Data_Processing_Procedure__c ;
                SRData.Purpose_of_Processing_Data__c=ObjPro.Purpose_of_Processing_Data__c ;
                SRData.Other_Purpose_of_Processing_Data__c=ObjPro.Other_Purpose_of_Processing_Data__c  ;
                SRData.Data_Subjects_Personal_Data__c=ObjPro.Data_Subjects_Personal_Data__c ;
                SRData.Other_Data_Subjects_Personal_Data__c=ObjPro.Other_Data_Subjects_Personal_Data__c  ;
                SRData.Class_of_Personal_Data__c=ObjPro.Class_of_Personal_Data__c  ;
                //  SRData.Purpose_of_Notification__c=ObjPro.Purpose_of_Notification__c ;
                SRData.Transferring_Personal_Data_outside_DIFC__c=ObjPro.Transferring_Personal_Data_Outside_DIFC__c;
                SRData.Processing_any_senstive_data__c=ObjPro.Processing_any_Special_Categories__c  ;
                
                
                SRData.In_accordance_with_Article_3__c=ObjPro.In_accordance_with_Article_3__c;
                SRData.In_accordance_with_Article_12_11__c=ObjPro.In_accordance_with_Article_12_11__c;
                SRData.In_accordance_with_Article_13__c=ObjPro.In_accordance_with_Article_13__c;
                SRData.In_accordance_with_Article_14__c=ObjPro.In_accordance_with_Article_14__c;
                SRData.In_accordance_with_Article_15__c=ObjPro.In_accordance_with_Article_15__c;
                SRData.In_accordance_with_Article_16_17_18__c=ObjPro.In_accordance_with_Article_16_17_18__c;
                SRData.In_accordance_with_Article_23_24_25__c=ObjPro.In_accordance_with_Article_23_24_25__c;
                SRData.No_Accordance_with_Article_23_24_25__c=ObjPro.No_Accordance_with_Article_23_24_25__c;
                SRData.Methods_of_Article_40__c=ObjPro.Methods_of_Article_40__c;
                SRData.In_accordance_with_Parts_5_or_6__c=ObjPro.In_accordance_with_Parts_5_or_6__c;
                SRData.Category_of_Article_3__c=ObjPro.Category_of_Article_3__c;
                SRData.In_accordance_with_Article_40__c=ObjPro.In_accordance_with_Article_40__c;
                SRData.In_accordance_with_Parts_7__c=ObjPro.In_accordance_with_Parts_7__c;
                
                SRData.In_Accordance_with_Article_11__c=ObjPro.In_Accordance_with_Article_11__c;
                SRData.Recognized_Jurisdictions__c=ObjPro.Recognized_Jurisdictions__c;
                SRData.In_Accordance_with_Article_12_1bj__c=ObjPro.In_Accordance_with_Article_12_1bj__c;
                SRData.Provisions_of_Article_12__c=ObjPro.Provisions_of_Article_12__c;
                SRData.No_Accordance_with_Article_16_17_18__c=  ObjPro.No_Accordance_with_Article_16_17_18__c;
                
                SRData.Data_Subject_of_Article_12_11__c=ObjPro.Data_Subject_of_Article_12_11__c;
                
                
                
                //Why not fields 11-Jan-2020 Arun v1.1 Start 
                SRData.No_Accordance_with_Article_14__c=ObjPro.No_Accordance_with_Article_14__c;
                SRData.No_Accordance_with_Article_15__c=ObjPro.No_Accordance_with_Article_15__c;
                SRData.No_Accordance_with_Article_40__c=ObjPro.No_Accordance_with_Article_40__c;
                SRData.No_Accordance_with_Parts_5_6__c=ObjPro.No_Accordance_with_Parts_5_6__c;
                SRData.No_Accordance_with_Parts_7__c=ObjPro.No_Accordance_with_Parts_7__c;
                //V1.1 Ends 
                
                
                
                
                
            }
            
        }
        //system.debug('### SR Data '+SRData);
        //system.debug('### purpose of '+SRData.Purpose_of_Notification__c);
        
        
    }
    public void getDataControllerRel()
    {
        
        
        Map<id,id> MapContactId=new   Map<id,id>();
        List<id> ListContactId=new   list<id>();
        //v1.1 Changes
        Set<Id> setContactId = new Set<Id>();
        Map<Id,Boolean> mapDPOfficer = new Map<Id,Boolean>();
        Map<Id, Amendment__c> mapRelationshipAmendment = new Map<Id,Amendment__c>();
        for(Relationship__c ObjRel:[Select Id, Object_Contact__r.FirstName, Object_Contact__r.LastName, Object_Contact__r.ADDRESS_LINE2__c,Object_Contact__c,
                                    Object_Contact__r.Address_Line_1_HC__c,Object_Contact__r.ADDRESS_LINE1__c, Object_Contact__r.CITY1__c,Object_Contact__r.Emirate_State_Province__c,
                                    Object_Contact__r.PO_Box_HC__c,Object_Contact__r.Postal_Code__c,Object_Contact__r.Country__c,Object_Contact__r.Phone,
                                    Object_Contact__r.Fax,Object_Contact__r.Email,Object_Contact__r.MobilePhone,Object_Contact__r.Passport_No__c, Is_DP_Officer__c
                                    from Relationship__c WHERE  Active__c = true AND Object_Contact__c != NULL AND Relationship_Type__c = 'Data Controller' and Subject_Account__c=:ObjAccount.id])
        {
            MapContactId.put(ObjRel.Object_Contact__c,ObjRel.id);
            ListContactId.add(ObjRel.Object_Contact__c);
            setContactId.add(ObjRel.Object_Contact__c);
            //v1.2 changes
            mapDPOfficer.put(ObjRel.Object_Contact__c,ObjRel.Is_DP_Officer__c);
            /*
Contact objDPController = new Contact();
objDPController = ObjRel.Object_Contact__r;

Amendment__c Amendment=new Amendment__c();
Amendment.First_Name__c = objDPController.FirstName;
Amendment.Last_name__c = objDPController.LastName;
Amendment.Passport_No__c = objDPController.Passport_No__c;  
Amendment.Amendment_Type__c='Individual';
Amendment.Relationship_Type__c='Data Controller';

// Full Address
Amendment.Apt_or_Villa_No__c= objDPController.Address_Line_1_HC__c;
Amendment.Building_Name__c= objDPController.ADDRESS_LINE1__c;
Amendment.Permanent_Native_Country__c= objDPController.Country__c;
Amendment.Permanent_Native_City__c= objDPController.CITY1__c;
Amendment.Emirate_State__c= objDPController.Emirate_State_Province__c;
Amendment.PO_Box__c= objDPController.PO_Box_HC__c;

//Contact Details
Amendment.Phone__c = objDPController.Phone ;
Amendment.Fax__c = objDPController.Fax ;
Amendment.Person_Email__c = objDPController.Email ;
Amendment.Mobile__c = objDPController.MobilePhone ;

Amendment.Contact__c=objDPController.id;
Amendment.sys_create__c=true;
Amendment.ServiceRequest__c=SRData.id;
Amendment.Customer__c=ObjAccount.id;

ListDataControlle.add(Amendment);
*/
            
        }  
        
        string ConRecTypeId=Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
        string strContactQuery = Apex_Utilcls.getAllFieldsSOQL('Contact');
        //v1.1 Changes
        strContactQuery = strContactQuery+' where id IN :setContactId order by CreatedDate';
        system.debug('ListContactId==>'+MapContactId.keySet());
        
        for(Contact objCon:database.query(strContactQuery))
        {
            Amendment__c objAmnd=new Amendment__c();
            objAmnd = Cls_AmendmentFieldMappingUtil.getIndvAmendment(objCon,objAmnd,'Data Controller');
            
            objAmnd.Relationship_Type__c='Data Controller';
            objAmnd.Amendment_Type__c='Individual';
            objAmnd.Contact__c=objCon.id;
            objAmnd.sys_create__c=true;
            objAmnd.Customer__c=ObjAccount.id;
            objAmnd.Relationship_Id__c=MapContactId.get(objCon.id);
            //v1.2 changes
            objAmnd.Is_DP_Officer__c = mapDPOfficer.containsKey(objCon.id) ? mapDPOfficer.get(objCon.id) == true ? 'Yes' : 'No' : 'No';
            
            ListDataControlle.add(objAmnd);
            system.debug('objAmnd==>'+objAmnd);
        }
        System.debug('ListDataControlle===>'+ListDataControlle);
        
        
    }
    
    
    public List<Amendment__c> getAmendmentController()
    {
        if(SRData.id!=null)
            ListDataControlle=[select Title_new__c,Apt_or_Villa_No__c,Permanent_Native_City__c,Given_Name__c,Family_Name__c,First_Name__c,Last_name__c,Passport_No__c,Amendment_Type__c,Relationship_Type__c,Address1__c,Address2__c, Building_Name__c,Address3__c,PO_Box__c,Emirate_State__c,Post_Code__c,Country_Of_Issue__c,Phone__c,Fax__c,Permanent_Native_Country__c,Person_Email__c,Mobile__c,ServiceRequest__c,Customer__c, Is_DP_Officer__c from Amendment__c where ServiceRequest__c!=null and ServiceRequest__c=:SRData.id and Relationship_Type__c='Data Controller' and Amendment_Type__c='Individual']; 
        for(Amendment__c objAmd : ListDataControlle){
            if(objAmd.Is_DP_Officer__c == 'Yes'){
                SRData.In_accordance_with_Article_16_17_18__c = 'Yes';
                break;
            }
        }
        return ListDataControlle;
    }
    public Amendment__c ObjNewAme{get;set;}
    public  void AddAmendment()
    {
        ObjNewAme=new Amendment__c();
        // ObjNewAme.RecordTypeId=Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Data_Controller').getRecordTypeId();
        ObjNewAme.Relationship_Type__c='Data Controller';
        ObjNewAme.Amendment_Type__c='Individual';
        //v1.2 changes
        isAdd = true;
        //isEdit = false;
    }
    
    
    public  void CancelSaveRow()
    {
        ObjNewAme=null;
        //v1.1 changes
        disableSave = false;
    }
    
    public void disableButton(){
        //v1.1 changes
        disableSave = false;    
    }
    
    public  PageReference SaveAmendment()
    {
        //v1.1 changes
        disableSave = true;
        if(SaveSR())
        {
            ObjNewAme.ServiceRequest__c=SRData.id;
            ObjNewAme.Customer__c=SRData.Customer__c;              
            upsert ObjNewAme; 
            if(ObjNewAme.Is_DP_Officer__c == 'Yes'){
                SRData.In_accordance_with_Article_16_17_18__c = 'Yes';
            }
            else{
                SRData.In_accordance_with_Article_16_17_18__c = 'No';
            }
            update SRData;                  
            CancelSaveRow();
            //v1.2 changes
            PageReference acctPage = new PageReference(Site.getBaseSecureUrl() + '/RocDataProtectionPermitVF_new?id='+SRData.Id);            
            acctPage.setRedirect(true);                  
            return acctPage;                                    
        }        
        return null;       
    }
    
    public void EditRow()
    {
        //v1.2 changes
        isEdit = true;
        isAdd = false;
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        //system.debug('### param '+param);
        //system.debug('### ListDataControlle size '+ListDataControlle.size());
        if(ListDataControlle.size()>=param)
        {
            ObjNewAme=ListDataControlle[param];
        }        
        //system.debug('### ObjNewAme '+ObjNewAme);
    }
    public void RemoveRow()
    {
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        if(ListDataControlle.size()>=param)
        {
            Amendment__c TempObjPro=ListDataControlle[param];
            ListDataControlle.remove(param);
            if(TempObjPro.id!=null)
                delete TempObjPro;
        }
        
    }
    
    
    
    //**************************************************************************************************************************
    //
    //*************************************************************************************************************************
    
    public void RelationshipJurisdiction()
    {
        //Assessed_the_Jurisdiction__c field no longer in use 
        for(Data_Protection__c ObjDP:[select Address_Line_2__c,City_Town__c,Country__c,Description_of_Safeguards__c,Email__c,Emirate_State_Province__c,Fax__c,IsActive__c,Is_Sensitive_Data__c,Mobile__c,Name_of_Jurisdiction__c,Person_Name__c,PO_Box__c,Post_Code__c,Service_Request__c,Address_Line_1__c,Telephone__c     from Data_Protection__c where Service_Request__c=null and Account_ID_18__c=:ObjAccount.id and Permit__r.Active__c=true AND RecordTypeId=:Schema.SObjectType.Data_Protection__c.getRecordTypeInfosByName().get('Jurisdiction').getRecordTypeId() order by Name ])
        {
            Data_Protection__c objDPTemp = objDP.clone(false);
            objDPTemp.Permit__c=null;
            ListDPJurisdiction.add(objDPTemp);
            
        }
        System.debug('ListDPJurisdiction==>'+ListDPJurisdiction);
    }
    
    public List<Data_Protection__c> getAmendmentJurisdiction()
    {
        if(SRData.id!=null)
            ListDPJurisdiction=[select Address_Line_2__c,City_Town__c,Country__c,Description_of_Safeguards__c,Email__c,
                                Emirate_State_Province__c,Fax__c,IsActive__c,Is_Sensitive_Data__c,Mobile__c,Name_of_Jurisdiction__c,Person_Name__c,PO_Box__c,Post_Code__c,Service_Request__c,Address_Line_1__c,Telephone__c   from Data_Protection__c where Service_Request__c!=null and Service_Request__c=:SRData.id AND RecordTypeId=:Schema.SObjectType.Data_Protection__c.getRecordTypeInfosByName().get('Jurisdiction').getRecordTypeId() order by Name]; 
        return ListDPJurisdiction;
    }
    
    public Data_Protection__c ObjJurisdiction{get;set;}
    public  void AddJurAmendment()
    {
        ObjJurisdiction=new Data_Protection__c();
    }
    public  void CancelJurSaveRow()
    {
        ObjJurisdiction=null;
    }
    public  void SaveJurAmendment()
    {
        
        if(SaveSR())
        {
            ObjJurisdiction.RecordTypeId=Schema.SObjectType.Data_Protection__c.getRecordTypeInfosByName().get('Jurisdiction').getRecordTypeId();
            ObjJurisdiction.Service_Request__c=SRData.id;
            upsert ObjJurisdiction;
            CancelJurSaveRow();
            
        }
        
        
        
    }
    public void EditJurRow()
    {
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        if(ListDPJurisdiction.size()>=param)
        {
            ObjJurisdiction=ListDPJurisdiction[param];
        }
        
    }
    public void RemoveJurRow()
    {
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        if(ListDPJurisdiction.size()>=param)
        {
            Data_Protection__c TempObjPro=ListDPJurisdiction[param];
            ListDPJurisdiction.remove(param);
            if(TempObjPro.id!=null)
                delete TempObjPro;
        }
        
    }
    
    
    
    
    
    //**************************************************************************************************************************
    //
    //*************************************************************************************************************************
    
    public boolean SaveSR()
    {
        boolean Saved=true;
        
        try     
        {  
            // if(SRData.id==null)
            upsert SRData;
            
            for(Data_Protection__c ObjP:ListDPJurisdiction)
                ObjP.Service_Request__c=SRData.id;
            
            
            for(Amendment__c Objand:ListDataControlle)
                Objand.ServiceRequest__c=SRData.id;
            
            upsert ListDPJurisdiction;
            upsert ListDataControlle;          
        }
        catch (Exception e)     
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
            ApexPages.addMessage(myMsg); 
            Saved=false;  
            //v1.1 changes
            disableSave = false;
        }  
        return Saved;
        
    }
    public  void RowValueChange()
    {
        string ValParam= Apexpages.currentPage().getParameters().get('ValParam');
        string myParam= Apexpages.currentPage().getParameters().get('myParam');
        System.debug('================ValParam==========>'+ValParam);
        System.debug('================myParam==========>'+myParam);
        if(myParam=='true')
            SRData.put(ValParam,true);
        else   if(myParam=='false')
            SRData.put(ValParam,false);
        else
            SRData.put(ValParam,myParam);        
        //v1.2 changes
        isEdit = false;
        isAdd = false;
        ObjNewAme=null;
    }
    
    
    
    public  PageReference SaveConfirmation()
    {
        
        System.debug('================SaveConfirmation======================');
        
        if(SaveSR())
        {
            try
            {
                
                
                
                
                system.debug('### ListDataControlle size'+ListDataControlle.size());
                if(ListDataControlle.size() <= 0 && SRData.Purpose_of_Notification__c == 'In relation to changing contact details only'){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please add at least one DP Contact or a DP Officer (DPO) before you submit this request.'));
                    return null;
                }
                else{
                    PageReference acctPage = new ApexPages.StandardController(SRData).view();
                    acctPage.setRedirect(true);
                    system.debug('### acctPage '+acctPage);
                    return acctPage;
                }
                
            }
            catch (Exception e)
            {
                ApexPages.addMessages(e);
                
            }
        }
        return null;
        
        
    }
    
    
    
    public void CheckTransferringData(){
        if(SRData != null){
            system.debug('SRData.Transferring_Personal_Data_outside_DIFC__c '+SRData.Transferring_Personal_Data_outside_DIFC__c);
            system.debug('CheckBoxType '+CheckBoxType);
            if(CheckBoxType == 'Sensitive Data'){
                SRData.In_accordance_with_Article_10_1__c = false;
            }else if(CheckBoxType == 'Outside of DIFC'){
                SRData.In_Accordance_with_Article_11__c = false;
                SRData.In_Accordance_with_Article_12_1bj__c = false;
                SRData.In_Accordance_with_Article_12_1a__c = false;
                SRData.Recognized_Jurisdictions__c = '';
                SRData.Provisions_of_Article_12__c = '';
                SRData.Adhering_to_Safe_Horbour__c = false;
                if(SRData.Transferring_Personal_Data_outside_DIFC__c == false){
                }else{
                    //JurisdictionInfo();
                }
            }else if(CheckBoxType == 'Article 11'){
                SRData.Recognized_Jurisdictions__c = '';
            }else if(CheckBoxType == 'Article 12 bj'){
                if(SRData.In_Accordance_with_Article_12_1bj__c == false)
                    SRData.Provisions_of_Article_12__c = '';
            }else if(CheckBoxType == 'Article 12 a'){
                if(SRData.In_Accordance_with_Article_12_1a__c == true){
                }
            }else if(CheckBoxType == 'Recognized Jurisdictions'){
                if(SRData.Recognized_Jurisdictions__c != null && SRData.Recognized_Jurisdictions__c.contains('US Dept')==false)
                    SRData.Adhering_to_Safe_Horbour__c = false; 
            }
            
            
            system.debug('SRData.In_Accordance_with_Article_12_1a__c '+SRData.In_Accordance_with_Article_12_1a__c);
        }
    }
    /*
public void AddJurisdiction(){
DataProtections = (DataProtections==null)?new list<Jurisdictions>():DataProtections;

Jurisdictions objJurisdictions = new Jurisdictions();
objJurisdictions.DataProtection = new Data_Protection__c();
objJurisdictions.DataProtection.RecordTypeId = Schema.SObjectType.Data_Protection__c.getRecordTypeInfosByName().get('Jurisdiction').getRecordTypeId();
objJurisdictions.DataProtection.IsActive__c = true;
objJurisdictions.DataProtection.Service_Request__c = SRData.Id;
objJurisdictions.Index = DataProtections.size();
DataProtections.add(objJurisdictions);
}

public void DeleteJurisdiction(){
system.debug('RowIndex '+RowIndex);

if(RowIndex != null && RowIndex < DataProtections.size()){
if(DataProtections[RowIndex].DataProtection.Id != null){
delete DataProtections[RowIndex].DataProtection;
}
DataProtections.remove(RowIndex);
Integer i=0;
for(Jurisdictions objJurisdictions : DataProtections){
objJurisdictions.Index = i;
i++;
}
}
}

public void JurisdictionInfo()
{
DataProtections = new list<Jurisdictions>();

for(Data_Protection__c objDP : [Select Id,IsActive__c,Name_of_Jurisdiction__c,Description_of_Safeguards__c,RecordTypeId from Data_Protection__c where Service_Request__c=:SRData.Id AND RecordTypeId=:Schema.SObjectType.Data_Protection__c.getRecordTypeInfosByName().get('Jurisdiction').getRecordTypeId() order by Name]){
Jurisdictions objJurisdictions = new Jurisdictions();
objJurisdictions.DataProtection = objDP;
objJurisdictions.Index = DataProtections.size();
DataProtections.add(objJurisdictions);
}




}

public class Jurisdictions{
public Data_Protection__c DataProtection {get;set;}
public Integer Index {get;set;}
public Boolean IsParent {get;set;}
public string Message {get;set;} 
}*/
}