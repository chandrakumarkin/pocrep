/*
    Author      : Durga Prasad
    Date        : 25-Dec-2019
    Description : Custom code to create Complaince on License Issuance

    v1: added logic for commercial permission
	v2: Added condition for CP for Pocket Shops as Pocket shops cannot be renewed. Hence no need to create complaince record
	v3:		07/03/2021		Shikha	Ticket #9563 - Update Start and End Date for packages if any once the ROC date is 
    ---------------------------------------------------------------------------------------
*/  
global without sharing class CC_CreateCompliance implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null){
            
            try{
                Account objAccount;
                list<Compliance__c> lstCompliance = new list<Compliance__c>();
                Compliance__c objCompliance;
                for(Account objAcc : [select Id,(select id from Compliances__r WHERE Status__c = 'Open' AND Name = 'Commercial Permission Renewal' ORDER BY createddate LIMIT 1),Name,OB_Active_Commercial_Permission__r.End_Date__c,OwnerID,Owner.ManagerID,Next_Renewal_Date__c,Principal_User__c from Account where Id=:stp.HexaBPM__SR__r.HexaBPM__Customer__c]){
                    system.debug(objAcc.Next_Renewal_Date__c);
                    system.debug(stp.HexaBPM__SR__r.HexaBPM__Customer__c);
                    system.debug(objAcc.Id);
                    objAccount = objAcc;
                }
                list<Contact> lstContacts = [select Id from Contact where AccountId=:objAccount.Id AND RecordType.DeveloperName='Portal_User' AND Id IN (select ContactId from User where Contact.AccountId=:objAccount.Id AND IsActive=true AND Community_User_Role__c INCLUDES ('Company Services'))];
                if(stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Commercial_Permission' || stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Commercial_Permission_Renewal'){

                    if(	stp.HexaBPM__SR__r.Type_of_commercial_permission__c != null 
                    && stp.HexaBPM__SR__r.Type_of_commercial_permission__c != 'Events' 
                    && stp.HexaBPM__SR__r.Type_of_commercial_permission__c !='Training or educational services offered by non-DIFC registered entities'
                    && stp.HexaBPM__SR__r.Type_of_commercial_permission__c !='Gate Avenue Pocket Shops' //V.2 - Pocket Shops cannot be renewed. Hence no need to create complaince

                    ){
                        objCompliance = new Compliance__c();
                        objCompliance.Name = 'Commercial Permission Renewal';
                        objCompliance.Account__c = objAccount.Id;
                        objCompliance.Start_Date__c = objAccount.OB_Active_Commercial_Permission__r.End_Date__c.addDays(-30);
                        objCompliance.End_Date__c = objAccount.OB_Active_Commercial_Permission__r.End_Date__c;
                        objCompliance.Exception_Date__c = objCompliance.End_Date__c.addDays(30);
                        objCompliance.Status__c = 'Created';
                        objCompliance.Customer_Email__c = stp.HexaBPM__SR__r.HexaBPM__Email__c;
                        objCompliance = RocCreateComplianceCls.PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                        lstCompliance.add(objCompliance); 
                    }

                    if(objAccount.Compliances__r != null && objAccount.Compliances__r.SIZE() >0){
                        objAccount.Compliances__r[0].Status__c = 'Fulfilled';
                        update objAccount.Compliances__r[0];
                    }
                  
                
                    
                }else{
                    objCompliance = new Compliance__c();
                objCompliance.Name = 'License Renewal';
                objCompliance.Account__c = objAccount.Id;
                objCompliance.Start_Date__c = objAccount.Next_Renewal_Date__c.addMonths(-1);
                objCompliance.End_Date__c = objAccount.Next_Renewal_Date__c;
                objCompliance.Exception_Date__c = objCompliance.End_Date__c.addDays(30);
                objCompliance.Status__c = 'Created';
                objCompliance = RocCreateComplianceCls.PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                lstCompliance.add(objCompliance);
            
                objCompliance = new Compliance__c();
                objCompliance.Name = 'Confirmation Statement';
                objCompliance.Account__c = objAccount.Id;
                objCompliance.Start_Date__c = objAccount.Next_Renewal_Date__c.addMonths(-1);
                objCompliance.End_Date__c = objAccount.Next_Renewal_Date__c;
                objCompliance.Exception_Date__c = objCompliance.End_Date__c.addDays(30);
                objCompliance.Status__c = 'Created';
                objCompliance = RocCreateComplianceCls.PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                lstCompliance.add(objCompliance);
                    
                if(stp.HexaBPM__SR__r.Type_of_Entity__c=='Public'){
                    objCompliance = new Compliance__c();
                    objCompliance.Name = 'Appointing an Auditor';
                    objCompliance.Account__c = objAccount.Id;
                    objCompliance.Start_Date__c = system.today();
                    objCompliance.End_Date__c = system.today().addMonths(18);
                    objCompliance.Status__c = 'Open';
                    objCompliance = RocCreateComplianceCls.PopulatePortalUsers(objAccount,objCompliance,lstContacts);
                    lstCompliance.add(objCompliance);
                }
                }
                
                if(!lstCompliance.isEmpty())
                    insert lstCompliance;
                //v3 started
                updatedDiscountedPackage(objAccount.Id);
                //v3 end
            }catch(Exception e){
                strResult = e.getMessage()+'';
                system.debug(strResult);
                Log__c objLog = new Log__c();
                objLog.Account__c = stp.HexaBPM__SR__r.HexaBPM__Customer__c;
                objLog.Description__c = e.getMessage();
                objLog.Type__c = 'Compliance Creation';
                insert objLog;
            }
            
        }
        return strResult;
    }
    
    //v3changes
    public static void updatedDiscountedPackage(String accountId){
        system.debug('### updated package code'+accountId);
        //String accountId = SR.HexaBPM__Customer__c;
        List<Company_Package__c> companyPackageList = [SELECT Id, Account__c, Account__r.ROC_reg_incorp_Date__c, Start_Date__c, End_Date__c, Package__c, 
                                                       Package__r.Package_Code__c, Package__r.Duration_in_Days__c
                                                       FROM Company_Package__c
                                                       WHERE Account__c =:accountId AND Package__r.Package_Code__c = 'VCFundManagerInitial'];
        system.debug('### companyPackageList '+companyPackageList);
        if(!companyPackageList.isEmpty()){
            for(Company_Package__c objPckg : companyPackageList){
                objPckg.Start_Date__c = Date.today();//objPckg.Account__r.ROC_reg_incorp_Date__c;
                objPckg.End_Date__c = objPckg.Start_Date__c.addDays(Integer.valueOf(objPckg.Package__r.Duration_in_Days__c));
            }
            update companyPackageList;
        }
    }

}