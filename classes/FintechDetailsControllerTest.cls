@isTest
public class FintechDetailsControllerTest{
    public static testmethod void Test1() {

            
        // create account
        RecordType rt =[SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType ='Account' and Name='Private_Public_National_Supra'];
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.RORP_License_No__c = '001234';
        objAccount.Registration_License_No__c = '0001';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.ROC_Status__c = 'Active';
        objAccount.RecordTypeId =rt.id;
        objAccount.OB_Sector_Classification__c ='FinTech';
        insert objAccount;    

     
       test.startTest();
      
        FintechDetailsController.saveAccountDetails(objAccount);
        FintechDetailsController.getAccountDetails();
    
        test.stoptest();
    

        
      }
      
      
}