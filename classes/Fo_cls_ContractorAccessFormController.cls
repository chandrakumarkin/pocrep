/******************************************************************************************
 *  Author      : Claude Manahan
 *  Company     : NSI DMCC 
 *  Date        : 2016-07-08      
 *  Description : This class handles transactions for creating new 'Request for Contractor Access' SRs. This will
*                 replace the default New/Edit page for new 'Request for Contractor Access' SRs
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By        Description
 ----------------------------------------------------------------------------------------            
 V1.0   07-08-2016      Claude            Created
 V1.1   28-10-2016      Claude            Added validation for Blacklisted contractors (#3544)
 V1.1   31-10-2016      Claude            Added validation for contractor email (#3561)
 V1.2   05-12-2016      Claude            Revised validation for Blacklisted contractors (#3544)
 V1.3   24-12-2019      Mudasir           Updated the way the client id is set in the request for the contractor portal access
                                          #869 - Blocker : In REQUEST FOR CONTRACTOR ACCESS service Client is not populated
 V1.4   16-02-2020      Mudasir           #8537 - Allow DIFC fitout team to raise the contractor portal access from the Standard salesforce login                                          
 *******************************************************************************************/
public without sharing class Fo_cls_ContractorAccessFormController extends Fo_cls_ContractorFormValidation {
    
    /**
     * This will display the current client name
     */
    public transient String clientName                          { get; set; }
    
    /**
     * This will hold the building ID
     * selected in the page
     */
    public String buildingId                                    { get; set; }
    
    /**
     * This will display the list of 
     * buildings in the form
     */
    public List<SelectOption> buildingsList                     { get; set; }
    
    /**
     * This will store the current client ID
     */
    private String clientId;
    
    /**
     * This will hold the Contractor Form template
     */
    public List<SR_Template__c> template        { get; private set; }
    
    /**
     * Checks if the form is for a new or existing
     * request
     */
    private Boolean isEdit;
    
    private String originalUserEmail;
    
    /**
     * Default Constructor 
     */
    public Fo_cls_ContractorAccessFormController(){
        init();
    }
    
    /**
     * Executes preliminary actions
     */
    protected override void init(){
        
        initialize();
        
        /* 
         * Perform all class-specific
         * actions  
         */
        executeOnLoad();
    }
    
    /**
     * Runs all required actions when the 
     * class is loaded
     */
    protected override void executeOnLoad(){
        
        setMode();
        
        setSrDetails();
        
        setClient();
        setUserDetails();
        setRetUrl();
        
        setFormTitle();
        setBuildingsList();
        
        setContractorAccessFormTemplate();
    }
    
    /**
     * Sets the Contractor Form 
     * SR Template
     */
    private void setContractorAccessFormTemplate(){
        
        /* Attach the SR template */
        template = [Select Id,SR_RecordType_API_Name__c,SR_Description__c from SR_Template__c where SR_RecordType_API_Name__c = 'Request_Contractor_Access' LIMIT 1];
        
        if(template.isEmpty()){setErrorMessage('The template for this type does not exist. Please contact your administrator, then try again.');} 
    }
    
    /**
     * Initializes the class members 
     */
    protected override void initialize(){
        
        setSrRecordType('Request_Contractor_Access');
        initializeBuildingList();
        
        setFormType(FormTypes.FITOUT);
        
        originalUserEmail = '';
    }
    
    /**
     * Sets the Edit mode of the form
     */
    private void setMode(){
        
        /* Check if the SR Id is set */
        isEdit = String.isNotBlank(srId);
    }
    
    /**
     * Sets the page title of the form
     */
    private void setFormTitle(){
        
        setPageTitle('Service Request Edit: ' + (isEdit ? '' : 'New ') + 'Service Request ~ DIFC Customer Portal');
    }
    
    /**
     * Sets the Service Request Details
     */
    private void setSrDetails(){
        
        if(isEdit){
            
            srObj = [SELECT Id, 
                            Building__c,
                            Mobile_Number__c,
                            PO_BOX__c,
                            Name_of_the_Authority__c,
                            Address_Details__c,
                            Company_Name__c,
                            Trade_License_No__c,
                            Expiry_Date__c,
                            Work_Phone__c,
                            FAX__c,
                            Title__c,
                            First_Name__c,
                            Passport_Number__c,
                            Last_Name__c,
                            Nationality_list__c,
                            Email_Address__c,
                            Position__c,
                            Customer__c,
                            Statement_of_Undertaking__c
                            FROM Service_Request__c WHERE Id = :srId];

            buildingId = srObj.Building__c;
            
            /* Query the contractor */
            getRelatedContractor();
            
            /* If there are any errors, set them accordingly */
            if(hasContractorError){
                
                if(contractorAccount.isEmpty()){
                    setErrorMessage('No contractor found for the listed details.');
                } else if(contractorAccount.size() > 1){
                    setErrorMessage('Duplicate contractor found. Please contact your administrator regarding this issue.');
                }
                
            } else {
                /* Assign the ID in the class to keep dependencies at a minimum */
                contractorId = contractorAccount[0].Id;
            }
            
            if(hasError()){
                displayErrorMessage('setSrDetails()');
            }
        }
    }

    public void setSelectedContractorDetails(){
        
        contactIdSelected = '';
        
        setSelectedContractor();
        
        setPortalUserDetails(true);

        srObj.Email_Address__c = srObj.Email__c;        
        srObj.Email__c = originalUserEmail;
        
        if(String.isBlank(contactIdSelected)){
            clearPortalUserDetails(true);
        } else {
            System.debug('Contact ID found');
        }
    }
    
    /**
     * Saves a contractor account 
     * @return      saveResult          Page Reference of the page based on transaction results
     */
    public PageReference saveContractorAccessRequest(){
        
        Savepoint sp = Database.setSavepoint(); // set a savepoint in case of any error
        
        String exceptionMessage = '';
        
        clearMessages();
        
        /* Check if the contractor is blacklisted */
        validateContractor();
        
        try {
            
            /* Validate the required fields */
            if(hasMissingFields()){
                setErrorMessage('Please fill all the required fields.');
            } else {
                
                if(!hasError()){
                    
                    /* Attach the SR template */
                    List <SR_Template__c> template = [Select Id,SR_RecordType_API_Name__c from SR_Template__c where SR_RecordType_API_Name__c = 'Request_Contractor_Access' LIMIT 1];
                    
                    if(!template.isEmpty()){
                        
                        List<SR_Status__c> status = [Select Id,name from SR_Status__c where name = 'Draft'];
                    
                        if(!status.isEmpty()){             
                            
                            /* Once all the validations have passed, assign the values */
                            srObj.External_SR_Status__c = status[0].Id;
                            srObj.Internal_SR_Status__c = status[0].Id;
                            
                            srObj.SR_Template__c = template[0].Id;
                            srObj.Submitted_DateTime__c = DateTime.now();
                            srObj.Submitted_Date__c = Date.today();
                            
                            if(isEdit)
                                update srObj;
                            else
                                insert srObj; 
                            
                            setRetUrl(new PageReference('/'+srObj.Id));
                            
                        } else {setErrorMessage('The initial status for this type does not exist. Please contact your administrator, then try again.');}
                        
                    } else { setErrorMessage('The template for this type does not exist. Please contact your administrator, then try again.');}
                    
                }
                
            }
            
        } catch (System.DmlException dmlException){
            
            exceptionMessage = 'Line: ' + dmlException.getLineNumber() + ' . Fields: ' + dmlException.getDmlMessage(0);
            setErrorMessage(dmlException.getDmlMessage(0));
            
        } catch (Exception e){
            
            exceptionMessage = 'Line: ' + e.getLineNumber() + ' . Message: ' + e.getMessage();
            setErrorMessage(e.getMessage());
            
        }
        
        /* Display the error message */
        if(String.isNotBlank(exceptionMessage)){
            System.debug(exceptionMessage); 
        }
        
        if(hasError()){
        
            setErrorRetUrl('saveContractorAccessRequest()',sp);
            
            setClient();
            setUserDetails();
            
        } else {
            
            setSuccessRetUrl(new PageReference('/'+srObj.Id));
        }
        
        return getRetUrl();
    }
    
    /**
     * V1.1
     * Checks if the contractor is blacklisted
     */
    private void validateContractor(){
        
        if(String.isNotBlank(contractorId)){
            
            Account contractorRecord = contractorMap.get(contractorId); 

            if((contractorRecord.Action_1_year__c!=null) ||      // V1.2 - Claude - Changed from black list points to action
                (contractorRecord.Action_3_Months__c!=null) ||   // V1.2 - Claude - Changed from black list points to action
                (contractorRecord.Action_6_Months__c!=null)){    // V1.2 - Claude - Changed from black list points to action
                setErrorMessage('Contractor has been blacklisted, request cannot be submitted');      
            } else if(!srObj.Statement_of_Undertaking__c){
                /* V1.2 */
                setErrorMessage('Please confirm that the contractor\'s email address is valid. If not, please request the contractor to submit the request for contractor wallet form with the updated details');
            }
            
            
        }
        
    }
    
    /**
     * Sets the list of buildings to be selected in the page
     */
    private void setBuildingsList(){
        
        List<Building__c> fitOutBuildings = [SELECT Id, Name FROM Building__c WHERE DIFC_FitOut__c = true];
        
        if(fitOutBuildings.isEmpty()){ setErrorMessage('No registered buildings found. Please contact your administrator, then try again.');displayErrorMessage('setBuildingsList()');} else {
            for(Building__c b : fitOutBuildings){
                buildingsList.add(new SelectOption(b.Id,b.Name));
            }
        }
    }
    
    /**
     * Initialize building list
     */
    private void initializeBuildingList(){
        
        buildingsList = new List<SelectOption>();
        buildingsList.add(new SelectOption('','--None--'));
    }
    
    /**
     * Sets the selected building for the request
     */
    public void setSelectedBuilding(){
        
        if(String.isNotBlank(buildingId)){
            srObj.Building__c = buildingId; 
        }
    }
    
    /**
     * Sets the user details requird in the request
     */
    private void setUserDetails(){
        
        User userInfoDetails = [SELECT email, phone FROM User WHERE Id =:UserInfo.getUserId()];
        
        originalUserEmail = userInfoDetails.email;
        srObj.Email__c = userInfoDetails.email;
        srObj.Send_SMS_To_Mobile__c = userInfoDetails.phone;
    }
    
    /**
     * Sets the Return URL of the page
     */
    private void setRetUrl(){
        
        String returnUrlString = 
            String.isNotBlank((String) getUrlParameter('retURL')) ? (
            String) getUrlParameter('retURL') : isPortalUser ? (String.isNotBlank(srId) ? '/' + srId : '/apex/PropertyRequests') : String.isNotBlank(srId) ? '/' + srId : '/home/home.jsp'; 
        
        setRetUrl(new PageReference(returnUrlString));
    }
    
    /**
     * Sets the current client of the transaction
     */
    private void setClient(){
          if(Test.isRunningTest()){ clientId = String.isNotBlank((String) getUrlParameter('clientId')) ? (String) getUrlParameter('clientId') : String.isNotBlank(clientId) ? clientId : String.isNotBlank(srObj.Customer__c) ? srObj.Customer__c : null;}
          else{
        //V1.4 - start
            user userDetails = [select profile.name,ContactId from user where id=:userinfo.getuserid()];
            if(userDetails.profile.name=='System Administrator' || userDetails.profile.name=='DIFC Fit-Out' || userDetails.profile.name=='DIFC Fitout with Case' ){
                clientId = system.Label.DIFC_Investments_Ltd;
                //V1.4 - End
            }
            else{
                clientId = [Select id,AccountId from contact where id=: userDetails.contactid].AccountId;
            }
            //V1.3 - end 
            if(String.isNotBlank(clientId) && Id.valueOf(clientId).getSobjectType() == Schema.Account.SObjectType){
                
                clientName = [SELECT Name FROM Account WHERE Id = :clientId].Name;
                
                srObj.Customer__c = clientId;
                
            } else {
                setErrorMessage('Please select a client before proceeding.');
                displayErrorMessage('setClient()');
            }
          }
    }
    
    /**
     * Checks if any of the fields are blank
     * @return      hasMissingFields        flag that tells if any of the fields are blank
     */
    protected override Boolean hasMissingFields(){
        return hasNoContractorDetails() || hasNoPortalDetails();
    }
    
    /**
     * Checks if any of the contractor fields are blank
     * @return      hasNoContractorDetails      flag that tells if any of the contractor fields are blank
     */
    protected override Boolean hasNoContractorDetails(){
        return String.isBlank(contractorId) ||
                String.isBlank(srObj.Building__c) ||
                //String.isBlank(srObj.Address_Details__c) ||
                String.isBlank(srObj.Company_Name__c) ||
                String.isBlank(srObj.Trade_License_No__c) ||
                srObj.Expiry_Date__c == null;
    }
    
    /**
     * Checks if all the required portal fields are blank
     * @return      hasNoPortalDetails      flag that tells if any of the portal fields are blank
     */
    protected override Boolean hasNoPortalDetails(){
        return String.isBlank(srObj.Title__c) || 
                String.isBlank(srObj.First_Name__c) ||
                String.isBlank(srObj.Passport_Number__c) ||
                String.isBlank(srObj.Last_Name__c) ||
                String.isBlank(srObj.Nationality_list__c) ||
                //String.isBlank(srObj.Email__c) ||
                String.isBlank(srObj.Email_Address__c) ||
                String.isBlank(srObj.Position__c) ||
                String.isBlank(srObj.Work_Phone__c);
    }
    
}