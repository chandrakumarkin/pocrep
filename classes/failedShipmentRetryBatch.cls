/***********************************************************************************
 *  Author   : shoaib tariq
 *  Company  : Tech Carrot
 *  Date     : 20-01-2020
  --------------------------------------------------------------------------------------------------------------------------
 Description : Thic class handels the failed shipments  
 ---------------------------------------------------------------------------------------------------------------------------
 Change Description
 Version      ModifiedBy            Date                Description
 V1.0         shoaib tariq        19-01-2020          create failed shipments
 ************************************************************************************/
global class failedShipmentRetryBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,schedulable  { 
    
    global List<Log__c> start(Database.BatchableContext bc) {
        return [SELECT Id,
                        Type__c,Description__c
                    FROM Log__c 
                    WHERE CreatedDate = Today 
                    AND Type__c like  '%Courier : Shipment Creation Error stepId%' 
                    AND Failed_Shipment_reattempted__c = false LIMIT 1];
    }
    
    global void execute(Database.BatchableContext bc, List<Log__c> scope){
        String StepId;
        Log__c updateFailedAttempt = new Log__c();
        
        for(Log__c thisLog : scope){
            if(thisLog.Type__c.contains('Courier : Shipment Creation Error stepId')){
                StepId = thisLog.Description__c.split('stepId')[1].split('#')[0];
                updateFailedAttempt = thisLog;
            }
        }
        system.debug('********'+StepId);
         system.debug('********'+updateFailedAttempt);
        Step__c thisStep = new Step__c();
        if(String.isNotBlank(StepId) && !test.isRunningTest()) { 
            thisStep = [SELECT Id,AWB_Number__c FROM Step__c WHERE Id =: StepId.Trim()];
        }
        system.debug('********thisStep'+StepId);
       
        
        if(String.isBlank(thisStep.AWB_Number__c) && !test.isRunningTest()) CC_CourierCls.CreateShipmentNormal(stepId.trim());
        updateFailedAttempt.Failed_Shipment_reattempted__c = true;
        Update updateFailedAttempt;
    } 
    
   global void finish(Database.BatchableContext bc){
        
      
   if([SELECT Id,Type__c,Description__c FROM Log__c WHERE CreatedDate = Today AND Type__c like  '%Courier : Shipment Creation Error stepId%' AND Failed_Shipment_reattempted__c = false  Limit 1].size()>0){
           
            Database.executeBatch(new failedShipmentRetryBatch(), 1);
        }  
    }  
    
    global void execute(SchedulableContext SC) {
      database.executebatch(new failedShipmentRetryBatch());
    }
}