/******************************************************************************************************
*  Author   : Kumar Utkarsh
*  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* This class contains logic of Entry Pemrit Form Creation and GDRFA callout.
*******************************************************************************************************/
public class WebServIntegrationGdrfaEntryPermClass {
    
    @InvocableMethod 
    public static void invokeEntryPermit(List<Id> SrIds)
    {
        System.debug('List<Id> SrIds:'+SrIds);
        Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
        for(Id SrId :SrIds){
        WebServiceGdrfaDocumentCompressClass.GDRFAImageCompressorCallout(String.valueOf(SrId));
        GDRFAEntryPermitAppCreate(String.valueOf(SrId), StatusReference.Id);
            }
    }
    
    public class Translate{
        public String TranslatedText{get;set;}
    }
    
    public class UploadDoc{
        public Boolean Success{get;set;}
    }
    
    public class ResponseBasicAuth
    {
        public string access_token{get;set;}
        public string token_type{get;set;}
    }
    
    public class MandateDocuments {
        public String applicationId;
        public List<Documents> Documents;
    }
    
    public class Documents {
        public String documentTypeId;
        public Boolean IsMandatory;
        public String documentNameEn;
        public String documentNameAr;
    }
    
    //Fetched the SR's for Entry Permit Request and build the JSON for submission
    public static String EntryPermitAuth(String ID, String accessToken, String tokenType)
    {
        Step__c entryPermitStep;
        String finalJSON;
        Service_Request__c SR = [SELECT Id, Service_Type__c,Nationality_list__c, Nationality__c, Previous_Nationality__c, Country_of_Birth__c, Gender__c, Marital_Status__c, Religion__c, Qualification__c, Occupation__c, Passport_Type__c,
                                 First_Name__c, First_Name_Arabic__c, Middle_Name__c, Middle_Name_Arabic__c, Location__c, Last_Name__c, Last_Name_Arabic__c, Mother_Full_Name__c, Establishment_Card_No__c, Occupation_GS__r.Code__c,
                                 Mother_s_full_name_Arabic__c, Date_of_Birth__c, Place_of_Birth__c, Place_of_Birth_Arabic__c, First_Language__c, Passport_Number__c, Passport_Date_of_Issue__c, Passport_Date_of_Expiry__c,
                                 Passport_Place_of_Issue__c, Building__c, Emirates_Id_Number__c, Area__c, Street_Address__c, Current_Registered_Street__c, Mobile_Number__c, Sponsor_Mobile_No__c, Address_Details__c,
                                 Passport_Country_of_Issue__c, Type_of_Request__c, Identical_Business_Domicile__c, City_Town__c, Emirate__c, City__c, Building_Name__c, Phone_No_Outside_UAE__c, Record_Type_Name__c
                                 FROM Service_Request__c WHERE Id =: ID ];
        
        If(SR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_New'){
            entryPermitStep = [SELECT Id, DNRD_Receipt_No__c FROM Step__c WHERE SR__c =: ID AND (Step_Status__c = 'Awaiting Review' OR Step_Status__c = 'Repush to GDRFA') AND (Step_Name__c = 'Entry Permit Form is Typed' OR Step_Name__c = 'Online Entry Permit is Typed') ];
        }
        
        String isInsideUAE;
        If(SR.Type_of_Request__c == 'Applicant Inside UAE'){
            isInsideUAE = 'Yes';
        }else{
            isInsideUAE = 'No';
        }
        
        String GDRFAIsInsideUAELookUp = WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Inside Outside Country',isInsideUAE); 
        //String GDRFACountryLookUpId = WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Identical_Business_Domicile__c); 
        system.debug('entryPermitStep:'+entryPermitStep);
        
        If(entryPermitStep != null){
            JSONGenerator jsonObj = JSON.createGenerator(true);
            jsonObj.writeStartObject();
            if(entryPermitStep.DNRD_Receipt_No__c != null){
                jsonObj.writeStringField('ApplicationNumber',entryPermitStep.DNRD_Receipt_No__c);
            }
            else if (entryPermitStep.DNRD_Receipt_No__c == null || entryPermitStep.DNRD_Receipt_No__c == ''){
                jsonObj.writeStringField('ApplicationNumber','');
            }
            jsonObj.writeFieldName('applicationData');
            jsonObj.writeStartObject();
            jsonObj.writeFieldName('Application');
            jsonObj.writeStartObject();
            jsonObj.writeFieldName('ApplicantDetails');
            jsonObj.writeStartObject();
            if(SR.First_Name__c != null ) {
                jsonObj.writeStringField('FirstNameE', SR.First_Name__c);
            }
            
            if(SR.First_Name_Arabic__c != null ) {
                jsonObj.writeStringField('FirstNameA', SR.First_Name_Arabic__c);
            }else if(SR.First_Name_Arabic__c == null && SR.First_Name__c != null){
                String FirstNameArabic = WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.First_Name__c, entryPermitStep, accessToken, tokenType);
                jsonObj.writeStringField('FirstNameA', FirstNameArabic);
            }
            
            if(SR.Middle_Name__c != null ) {
                jsonObj.writeStringField('MiddleNameE', SR.Middle_Name__c);
            }
            
            if(SR.Location__c != null ) {
                jsonObj.writeStringField('location', SR.Location__c);
            }
            
            if(SR.Middle_Name_Arabic__c != null ) {
                jsonObj.writeStringField('MiddleNameA', SR.Middle_Name_Arabic__c);
            }else if(SR.Middle_Name_Arabic__c == null && SR.Middle_Name__c != null){
                String MiddleNameArabic = WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.Middle_Name__c, entryPermitStep, accessToken, tokenType);
                jsonObj.writeStringField('MiddleNameA', MiddleNameArabic);
            }
            
            if(SR.Last_Name__c != null ) {
                jsonObj.writeStringField('LastNameE', SR.Last_Name__c);
            }
            
            if(SR.Last_Name_Arabic__c != null ) {
                jsonObj.writeStringField('LastNameA', SR.Last_Name_Arabic__c);
            }else if(SR.Last_Name_Arabic__c == null && SR.Last_Name__c != null){
                String LastNameArabic = WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.Last_Name__c, entryPermitStep, accessToken, tokenType);
                jsonObj.writeStringField('LastNameA', LastNameArabic);
            }
            
            if(SR.Mother_Full_Name__c != null ) {
                jsonObj.writeStringField('MotherNameE', SR.Mother_Full_Name__c);
            }
            
            if(SR.Mother_s_full_name_Arabic__c != null ) {
                jsonObj.writeStringField('MotherNameA', SR.Mother_s_full_name_Arabic__c);
            }else if(SR.Mother_s_full_name_Arabic__c == null && SR.Mother_Full_Name__c != null){
                String MothersNameArabic = WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.Mother_Full_Name__c, entryPermitStep, accessToken, tokenType);
                jsonObj.writeStringField('MotherNameA', MothersNameArabic);
            }
            if(SR.Nationality_list__c != null){
                if( WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country', SR.Nationality_list__c) != ''){
                    jsonObj.writeStringField('CurrentNationalityId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country', SR.Nationality_list__c));
                }
            }
            if(SR.Previous_Nationality__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Previous_Nationality__c) != ''){
                    jsonObj.writeStringField('PreviousNationality', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Previous_Nationality__c));
                }
            }
            if(SR.Date_of_Birth__c != null ) {
                jsonObj.writeDateField('BirthDate', SR.Date_of_Birth__c);
            }
            if(SR.Country_of_Birth__c != null || SR.Country_of_Birth__c != '') {
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Country_of_Birth__c) != ''){
                    jsonObj.writeStringField('BirthCountryId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Country_of_Birth__c));
                }
            }
            
            if(SR.Place_of_Birth__c != null ) {
                jsonObj.writeStringField('BirthPlaceE', SR.Place_of_Birth__c);
            }
            if(SR.Place_of_Birth_Arabic__c != null ) {
                jsonObj.writeStringField('BirthPlaceA', SR.Place_of_Birth_Arabic__c);
            }
            if(SR.Gender__c != null){
                jsonObj.writeStringField('SexId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Gender',SR.Gender__c));
            }
            if(SR.Marital_Status__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('List of Marital Status',SR.Marital_Status__c) != ''){
                    jsonObj.writeStringField('MaritalStatusId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('List of Marital Status',SR.Marital_Status__c));
                }
            }
            jsonObj.writeStringField('ReligionId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Religions',SR.Religion__c));
            jsonObj.writeStringField('Faith', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Faiths','Unknown'));
            if(SR.Qualification__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Education',SR.Qualification__c) != ''){
                    jsonObj.writeStringField('EducationId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Education',SR.Qualification__c));
                }
            }
            if(SR.Occupation_GS__r.Code__c != null){
                jsonObj.writeStringField('ProfessionId',SR.Occupation_GS__r.Code__c);
            }
            if(SR.First_Language__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Language',SR.First_Language__c) != ''){
                    jsonObj.writeStringField('Language1', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Language',SR.First_Language__c));
                }
            }
            if(SR.Passport_Number__c != null ) {
                jsonObj.writeStringField('PassportNo', SR.Passport_Number__c);
            }
            if(SR.Passport_Type__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Passport Type',SR.Passport_Type__c) != ''){
                    jsonObj.writeStringField('PassportTypeId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Passport Type',SR.Passport_Type__c));
                }
            }
            if(SR.Passport_Date_of_Issue__c != null ) {
                jsonObj.writeDateField('PassportIssueDate', SR.Passport_Date_of_Issue__c);
            }
            if(SR.Passport_Date_of_Expiry__c != null ) {
                jsonObj.writeDateField('PassportExpiryDate', SR.Passport_Date_of_Expiry__c);
            }
            if(SR.Passport_Place_of_Issue__c != null ) {
                jsonObj.writeStringField('PassportPlaceE', SR.Passport_Place_of_Issue__c);
            }
            if(SR.Passport_Place_of_Issue__c != null ) {
                String PassportPlaceArabic = WebServGDRFAAuthFeePaymentClass.getArabicTranslate(SR.Passport_Place_of_Issue__c, entryPermitStep, accessToken, tokenType);
                jsonObj.writeStringField('PassportPlaceA', PassportPlaceArabic);
            }
            if(SR.Passport_Country_of_Issue__c != null){
                if( WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Passport_Country_of_Issue__c) != ''){
                    jsonObj.writeStringField('PassportIssueCountryId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Passport_Country_of_Issue__c));
                    jsonObj.writeStringField('PassportIssueGovId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Passport_Country_of_Issue__c));
                }
            }
            jsonObj.writeStringField('IsInsideUAE', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Inside Outside Country',isInsideUAE));
            if(SR.Identical_Business_Domicile__c != null){
                if(WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Identical_Business_Domicile__c) != ''){
                    jsonObj.writeStringField('AddressOutsideCountryId',WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Country',SR.Identical_Business_Domicile__c)); 
                }
            }
            
            jsonObj.writeStringField('AddressOutsideCity', SR.City_Town__c);  
            jsonObj.writeStringField('AddressOutside1', SR.Address_Details__c); 
            jsonObj.writeStringField('AddressOutside2', '');
            jsonObj.writeStringField('AddressOutsideTelNo', SR.Phone_No_Outside_UAE__c);
            if(SR.Emirate__c != null ) {
                jsonObj.writeStringField('EmirateId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Emirates',SR.Emirate__c));
            }
            jsonObj.writeStringField('CityId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Cities',SR.Emirate__c));
            
            if(SR.Area__c != null ) {
                String GdrfaAreaId =  WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas',SR.Area__c);
                if(GdrfaAreaId != null && GdrfaAreaId != ''){
                    jsonObj.writeStringField('AreaId', GdrfaAreaId);
                }else if (GdrfaAreaId == null || GdrfaAreaId == ''){
                    jsonObj.writeStringField('AreaId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas','Zabeel 1'));
                } 
            }
            else if(SR.Area__c == null ){
                jsonObj.writeStringField('AreaId', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Areas','Zabeel 1'));
            }
            if(SR.Street_Address__c != null ) {
                jsonObj.writeStringField('Street', SR.Street_Address__c);
            }
            
            if(SR.Building_Name__c != null ) {
                jsonObj.writeStringField('Building', SR.Building_Name__c);
            }
            
            if(SR.Current_Registered_Street__c != null ) {
                jsonObj.writeStringField('Landmark', 'DIFC');
            }
            jsonObj.writeStringField('MakaniNo', '');
            if(SR.Mobile_Number__c != null ) {
                jsonObj.writeStringField('MobileNo', SR.Mobile_Number__c);
            }
            jsonObj.writeEndObject(); 
            jsonObj.writeFieldName('SponsorDetails');
            jsonObj.writeStartObject();
            jsonObj.writeStringField('SponsorEmail', 'gdrfanotifications@difc.ae');
            if(SR.Sponsor_Mobile_No__c != null ) {
                jsonObj.writeStringField('MobileNo', SR.Sponsor_Mobile_No__c);
            }
            if(SR.Establishment_Card_No__c != null){
                jsonObj.writeStringField('EstCode', SR.Establishment_Card_No__c.replaceAll('/',''));
            }
            jsonObj.writeEndObject();
            jsonObj.writeFieldName('ApplicationDetails');
            jsonObj.writeStartObject();
            jsonObj.writeBooleanField('IsDraft', false);
            jsonObj.writeEndObject();
            jsonObj.writeEndObject();
            jsonObj.writeEndObject();
            jsonObj.writeEndObject();
            finalJSON = jsonObj.getAsString();
            System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
            
        }
        WebServGDRFAAuthFeePaymentClass.masterDataMap = null;
        return finalJSON;
    }
    
    @future(callout=true)
    // Callout method for Entry Permit
    public static void GDRFAEntryPermitAppCreate(String ID, String StatusReference) 
    {
        map<Id,Id> mapStepStepTemplate = new map<Id,Id>();
        Step__c stepEntryPermit;
        String entryPermitJson; 
        stepEntryPermit = [SELECT Id, Step_Name__c, Status__c, SR_Step__c, Step_Template__c, SR__c FROM Step__c WHERE SR__c =: ID AND (Step_Status__c = 'Awaiting Review' OR Step_Status__c = 'Repush to GDRFA') AND (Step_Name__c = 'Entry Permit Form is Typed' OR Step_Name__c = 'Online Entry Permit is Typed') ];
        Attachment attachmentEntryPermit = new Attachment();
        attachmentEntryPermit.Name = 'Entry Permit Json.txt';
        attachmentEntryPermit.ParentId = stepEntryPermit.Id;
        attachmentEntryPermit.Body = Blob.valueOf('Error before Json Creation');
        attachmentEntryPermit.ContentType = 'text/plain';
        //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
        try{
            for(SR_Steps__c obj: [select id, Step_Template__c from SR_Steps__c where Step_Template_Code__c IN('Online entry permit is typed', 'Entry permit form is typed', 'Renewal form is typed', 'Cancellation Form Typed', 'Entry Permit is Issued', 'Completed', 'Passport is Ready for Collection', 'Visa Stamping Form is Typed') AND SR_Template__r.SR_Template_Code__c IN ('DIFC_Sponsorship_Visa_New','DIFC_Sponsorship_Visa_Renewal', 'DIFC_Sponsorship_Visa_Cancellation') ]){
                mapStepStepTemplate.put(obj.Step_Template__c,obj.Id);                                                                        
            }
            if(mapStepStepTemplate.containsKey(stepEntryPermit.Step_Template__c) && stepEntryPermit.SR_Step__c == null){
                stepEntryPermit.SR_Step__c = mapStepStepTemplate.get(stepEntryPermit.Step_Template__c);
            }
            String finalDocJSON;
            Boolean isAllUploadSuccess = false;
            MandateDocuments responseDocList;
            WebServGDRFAAuthFeePaymentClass.ResponseBasicAuth ObjResponseAuth = WebServGDRFAAuthFeePaymentClass.GDRFAAuth();
            List<Log__c> ErrorObjLogList = new List<Log__c>();
            //Get authrization code 
            string access_token= ObjResponseAuth.access_token;
            string token_type=ObjResponseAuth.token_type;
            //WebService to Push the Entry Permit Form
            HttpRequest request = new HttpRequest();
            request.setEndPoint(System.label.GDRFA_Entry_Permit_End_Point);  
            entryPermitJson = EntryPermitAuth(ID, access_token, token_type);
            attachmentEntryPermit.Body = Blob.valueOf(entryPermitJson);
            String jsonRequest = entryPermitJson;
            request.setBody(jsonRequest);
            request.setTimeout(120000);
            request.setHeader('apikey', System.label.GDRFA_API_Key);
            request.setHeader('Content-Type', 'application/json');
            request.setMethod('POST');
            request.setHeader('Authorization',token_type+' '+access_token);
            HttpResponse response = new HTTP().send(request);
            System.debug('responseStringresponse:'+response.toString());
            System.debug('STATUS:'+response.getStatus());
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('strJSONresponseEntryPermit:'+response.getBody());
            request.setTimeout(110000);           
            
            
            if (response.getStatusCode() == 200)
            {
                responseDocList = (MandateDocuments) System.JSON.deserialize(response.getBody(), MandateDocuments.class);
                System.debug('applicationId:'+responseDocList.applicationId);
                System.debug('Documents:'+responseDocList.Documents);
                Boolean mandateDocsExist = false;
                Set<String> setGDRFADocId = new Set<String>();
                map<Id, String> mapSRDocIdGDRFADocId = new map<Id, String>();
                map<Id, String> mapSRDocIdGDRFAname = new map<Id, String>();
                map<Id, String> mapSRDocIdGDRFAArabicName = new map<Id, String>();
                map<String, String> mapGdrfaDocIdGDRFAname = new map<String, String>();
                map<String, String> mapGdrfaDocIdGDRFAArabicname = new map<String, String>();
                map<String, Boolean> mapGdrfaDocIdGdrfaIsMandate = new map<String, Boolean>();
                map<String, Boolean> mapSRDocIdGdrfaIsMand = new map<String, Boolean>();
                map<Id, Boolean> mapSRDocMandate = new map<Id, Boolean>();
                if(responseDocList.Documents != null){
                    For(Documents docList : responseDocList.Documents){
                        setGDRFADocId.add(docList.documentTypeId);
                        mapGdrfaDocIdGdrfaIsMandate.put(docList.documentTypeId, docList.IsMandatory);
                        mapGdrfaDocIdGDRFAname.put(docList.documentTypeId, docList.documentNameEn);
                        mapGdrfaDocIdGDRFAArabicname.put(docList.documentTypeId, docList.documentNameAr);
                    }
                }
                System.debug('mapGdrfaDocIdGdrfaIsMandate:'+mapGdrfaDocIdGdrfaIsMandate);
                for(SR_Doc__c objDoc:  [SELECT GDRFA_Attchment_Doc_Id__c, GDRFA_Parent_Document_Id__c FROM SR_Doc__c
                                        WHERE Service_Request__c =: ID AND GDRFA_Parent_Document_Id__c IN: setGDRFADocId AND GDRFA_Parent_Document_Id__c != null]){
                                            mapSRDocIdGDRFADocId.put(objDoc.GDRFA_Attchment_Doc_Id__c, objDoc.GDRFA_Parent_Document_Id__c);
                                            mapSRDocIdGDRFAname.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGDRFAname.get(objDoc.GDRFA_Parent_Document_Id__c));
                                            mapSRDocIdGdrfaIsMand.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGdrfaIsMandate.get(objDoc.GDRFA_Parent_Document_Id__c));
                                            mapSRDocIdGDRFAArabicName.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGDRFAArabicname.get(objDoc.GDRFA_Parent_Document_Id__c));
                                        }
                System.debug('mapSRDocIdGDRFADocId:'+mapSRDocIdGDRFADocId);
                System.debug('mapSRDocIdGdrfaIsMand:'+mapSRDocIdGdrfaIsMand);
                for(Attachment objAttch: [SELECT Id, Name, ContentType, Body from Attachment where Id IN:mapSRDocIdGDRFADocId.keySet()]){
                    //Webservice to push Sr docs
                    JSONGenerator docJsonObj = JSON.createGenerator(true);
                    docJsonObj.writeStartObject();
                    docJsonObj.writeFieldName('Documents');
                    docJsonObj.writeStartObject();
                    System.debug('mapSRDocIds1:'+mapSRDocIdGDRFADocId.get(objAttch.Id));
                    docJsonObj.writeObjectField('documentTypeId', mapSRDocIdGDRFADocId.get(objAttch.Id));
                    docJsonObj.writeBlobField('document', objAttch.Body);    
                    docJsonObj.writeBooleanField('IsMandatory', mapSRDocIdGdrfaIsMand.get(objAttch.Id)); 
                    docJsonObj.writeStringField('MineType', objAttch.ContentType);
                    docJsonObj.writeStringField('documentNameEn', mapSRDocIdGDRFAname.get(objAttch.Id)); 
                    docJsonObj.writeStringField('documentNameAr', mapSRDocIdGDRFAArabicName.get(objAttch.Id)); 
                    docJsonObj.writeStringField('FileName', objAttch.Name); 
                    docJsonObj.writeEndObject();
                    docJsonObj.writeStringField('applicationId', responseDocList.applicationId);
                    docJsonObj.writeEndObject();
                    finalDocJSON = docJsonObj.getAsString();
                    System.debug('DEBUG Full Request finalJSON : ' +(docJsonObj.getAsString()));
                    HttpRequest requestUploadDoc = new HttpRequest();
                    requestUploadDoc.setEndPoint(System.label.GDRFA_Document_Upload_Endpoint);        
                    requestUploadDoc.setBody(docJsonObj.getAsString());
                    requestUploadDoc.setTimeout(120000);
                    requestUploadDoc.setHeader('apikey', System.label.GDRFA_API_Key);
                    requestUploadDoc.setHeader('Content-Type', 'application/json');
                    requestUploadDoc.setMethod('POST');
                    requestUploadDoc.setHeader('Authorization',token_type+' '+access_token);
                    HttpResponse responseUploadDoc = new HTTP().send(requestUploadDoc);
                    System.debug('responseUploadDoc:'+responseUploadDoc.toString());
                    System.debug('STATUS:'+responseUploadDoc.getStatus());
                    System.debug('STATUS_CODE:'+responseUploadDoc.getStatusCode());
                    System.debug('strJSONresponseUploadDoc:'+responseUploadDoc.getBody());
                    if (responseUploadDoc.getStatusCode() == 200)
                    {
                        UploadDoc docUpload = (UploadDoc) System.JSON.deserialize(responseUploadDoc.getBody(), UploadDoc.class);
                        System.debug('IsSuccess:'+docUpload.Success);
                        if(docUpload.Success){
                            isAllUploadSuccess = true;   
                        }else{
                            isAllUploadSuccess = false;
                        }   
                    }
                    else{
                        Log__c ErrorobjLog = new Log__c();
                        ErrorobjLog.Type__c = objAttch.Id+'-'+mapSRDocIdGDRFAname.get(objAttch.Id)+'-'+objAttch.Id+'-Document Upload Error';
                        ErrorobjLog.Description__c = 'Response: '+ responseUploadDoc.getBody();
                        ErrorobjLog.SR_Step__c = stepEntryPermit.Id;
                        ErrorObjLogList.add(ErrorobjLog); 
                        isAllUploadSuccess = false;
                        
                    }
                }
                if(!ErrorObjLogList.isEmpty() && ErrorObjLogList != null){
                    try {
                        insert ErrorObjLogList;
                    }
                    catch(Exception ex) {
                        System.Debug(ex);
                    }
                    isAllUploadSuccess = false;
                    //insert attachmentEntryPermit;
                    stepEntryPermit.Status__c = StatusReference;
                    stepEntryPermit.OwnerId = UserInfo.getUserId();
                    stepEntryPermit.Step_Notes__c = 'Document Upload Error';
                    if(responseDocList.applicationId != null){
                        stepEntryPermit.DNRD_Receipt_No__c = responseDocList.applicationId;
                    }
                    try{
                        Update stepEntryPermit; 
                    }catch(Exception ex) {
                        insert LogDetails.CreateLog(null, 'Entry Permit', 'SR Id is ' + stepEntryPermit.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                    }
                }
            }
            else{
                WebServGDRFAAuthFeePaymentClass.ErrorClass ErrorLog = (WebServGDRFAAuthFeePaymentClass.ErrorClass) System.JSON.deserialize(response.getBody(), WebServGDRFAAuthFeePaymentClass.ErrorClass.class);
                Log__c objLog = new Log__c();objLog.Type__c = 'Entry Permit Error' ;objLog.Description__c = jsonRequest +'Response: '+ response.getBody();objLog.SR_Step__c = stepEntryPermit.Id;insert objLog; 
                //insert attachmentEntryPermit;
                if(StatusReference != null){
                    stepEntryPermit.Status__c = StatusReference;
                }
                if(UserInfo.getUserId() != null){
                    stepEntryPermit.OwnerId = UserInfo.getUserId();
                }
                if(ErrorLog != null){
                    if(ErrorLog.errorMsg[0] != null){
                        stepEntryPermit.Step_Notes__c = ErrorLog.errorMsg[0].messageEn;
                    }
                }
                try{
                    Update stepEntryPermit; 
                }catch(Exception ex) {
                    insert LogDetails.CreateLog(null, 'Entry Permit on error', 'SR Id is ' + stepEntryPermit.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                }
                
            }
            //Webservice to get the Application Fee 
            if(isAllUploadSuccess){
                WebServGDRFAAuthFeePaymentClass.getApplicationFeeAPI(responseDocList.applicationId, access_token, token_type, ID, stepEntryPermit, StatusReference);
                //insert attachmentEntryPermit;
            }
            insert attachmentEntryPermit;
        }
        Catch(exception e){
            insert attachmentEntryPermit;
            system.debug('Error:- ' + e); 
            stepEntryPermit.Status__c = StatusReference;
            stepEntryPermit.OwnerId = UserInfo.getUserId();
            stepEntryPermit.Step_Notes__c = 'Error:-- ' + e;
            try{
                Update stepEntryPermit; 
            }catch(Exception ex) {
                insert LogDetails.CreateLog(null, 'Entry Permit on error on final exception', 'SR Id is ' + stepEntryPermit.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
            }
            Log__c objLog = new Log__c();
            objLog.Type__c = 'Entry Permit Error' + e;
            objLog.Description__c = 'Error:-: ' + e;
            objLog.SR_Step__c = stepEntryPermit.Id;
            insert objLog; 
            
        }
    }
    
}