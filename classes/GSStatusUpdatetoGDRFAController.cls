/**
 * @description       : 
 * @author            : Veera
 * @group             : 
 * @last modified on  : 01-04-2021
 * @last modified by  : Veera
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-04-2021   Veera Initial Version
**/

 public without sharing class GSStatusUpdatetoGDRFAController{
 
    @InvocableMethod(Label = 'GSStatusUpdatetoGDRFA' description = 'GSStatusUpdatetoGDRFA')
    public static void GSStatusUpdatetoGDRFAMethod(List < id > ids) {
        GSStatusUpdatetoGDRFAFuture(ids);
    }
    
     @future(callout = true)
    public static void GSStatusUpdatetoGDRFAFuture(List < id > ids) {
    
            List < Service_request__c > SRList = new List < Service_request__c > ();
            //List < SR_Status__c > SRstatus = [select id from SR_Status__c where code__c = 'The application is under process with GDRFA' limit 1];
            
             for (step__c step: [select id, sr__c, step_name__c, SR__r.Record_Type_Name__c from step__c where id in: ids]){
            
                    Service_request__c SR = new Service_request__c();
            
                    SR.id = step.sr__c;
                    SR.External_SR_Status__c = system.label.The_application_is_under_process_with_GDRFA_ID;
                    SR.Internal_SR_Status__c = system.label.The_application_is_under_process_with_GDRFA_ID;
                    SRList.add(SR);
                    
             } 
             
             if(SRList.size()>0)
             
               try{
               update SRList;    
               }catch (Exception ex){
               
               Log__c objLog = new Log__c( Type__c = 'GDRFA status update failed ',
               Description__c = 'Exception is : ' + ex.getMessage() + '\nLine # ' + ex.getLineNumber());
              
               insert objLog;
               }  
    }
 
 }