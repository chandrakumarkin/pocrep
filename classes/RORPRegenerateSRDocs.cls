/*
Created By  : Suchita - DIFC on 16 Feb 2021 
Description : This Class is extension class for RORPRegenerateSRDocs visualforce page
--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No        Date                Updated By          Description
v1.0      16 Feb 2021          Suchita Sharma,      Initial Version
v1.1      07 Jul 2021          Suchita Sharma       #DIFC2-16773 - Generated document in Draft mode 
----------------------------------------------------------------------------------------
*/
public class RORPRegenerateSRDocs {
    public String SRId {get;set;}
    public String RecordTypeId {get;set;}
    public Service_Request__c objSR {get;set;}
    public RORPRegenerateSRDocs(Apexpages.Standardcontroller con){
        SRId = con.getId();
    }
    
    public void regenerateSRDocs(){
        Map<id,List<SR_Doc__c>> SRdocsMap = new Map<id,List<SR_Doc__c>>();
        List<SR_Doc__c> SRdocsTempList= new List<SR_Doc__c>();
        List<Service_Request__c> SRTempList= new List<Service_Request__c>(); //v1.1
        boolean finalisedFlag;//v1.1
        if(SRId!=null){
            for(SR_Doc__c objSRDocs : [select Id,LastModifiedDate,Generate_Document__c,Service_Request__r.LastModifiedDate,Service_Request__r.finalizeAmendmentFlg__c 
                from SR_Doc__c where Service_Request__c =:SRId and Status__c = 'Generated' and Sys_IsGenerated_Doc__c=true 
                and Service_Request__r.External_Status_Name__c = 'Draft' LIMIT : (Limits.getLimitQueryRows()-Limits.getQueryRows())]){ //v1.1
                if(objSRDocs.LastModifiedDate < objSRDocs.Service_Request__r.LastModifiedDate){ // if any value updated on SR in draft state 
                    objSRDocs.Generate_Document__c=true;
                    if(!SRdocsMap.isEmpty() && SRdocsMap.containsKey(objSRDocs.id)){
                        // Do Nothing
                    }
                    else{
                        finalisedFlag=objSRDocs.Service_Request__r.finalizeAmendmentFlg__c; //v1.1
                        SRdocsTempList.add(objSRDocs);
                        SRdocsMap.put(objSRDocs.id,SRdocsTempList);
                    }
                }
                
                for(Amendment__c objAmd : [select Id,Name,LastModifiedDate from Amendment__c where ServiceRequest__c =:SRId
                     and ServiceRequest__r.External_Status_Name__c = 'Draft' and LastModifiedDate >:objSRDocs.LastModifiedDate 
                     LIMIT : (Limits.getLimitQueryRows()-Limits.getQueryRows()) ]){ // if amendment value updated on SR in draft state
                    objSRDocs.Generate_Document__c=true;
                    
                    if(!SRdocsMap.isEmpty() && SRdocsMap.containsKey(objSRDocs.id)){
                        // Do Nothing
                    }
                    else{
                        SRdocsTempList.add(objSRDocs);
                        SRdocsMap.put(objSRDocs.id,SRdocsTempList);
                    }
                }
                
            }
            //Start v1.1  
            if(finalisedFlag !=null ){
                objSR = new Service_Request__c();
                objSR.id = SRId;
                objSR.finalizeAmendmentFlg__c=finalisedFlag?false:true; // flip the flag for doc regeneration
                SRTempList.add(objSR);
            }
            if(!SRTempList.isEmpty()){
                try{
                    update SRTempList;
                }catch(exception ex){
                    Log__c objLog = new Log__c(Description__c='Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage(),Type__c = 'Custom code RORPRegenerateSRDocs class');
                    insert objLog;        
                }
            }
            //End v1.1 
            if(!SRdocsMap.isEmpty()){
                try{
                    update SRdocsTempList;
                }catch(exception ex){
                    Log__c objLog = new Log__c(Description__c='Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage(),Type__c = 'Custom code RORPRegenerateSRDocs class');
                    insert objLog;        
                }
            }
            
            
        }
        
    }
}