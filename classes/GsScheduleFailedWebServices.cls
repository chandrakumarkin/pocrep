global without sharing class GsScheduleFailedWebServices implements Schedulable {
	global void execute(SchedulableContext ctx) {
		GsBatchFailedWebServices objBatch = new GsBatchFailedWebServices();
		Id batchId = database.executeBatch(objBatch, 1);
	}
}