@isTest
public class closeCourierStepsBatchTest {
    
     static testMethod void myUnitTest() {
        
        List<Account> lstAccount = new List<Account>();
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        lstAccount.add(objAccount);
        string conRT;
         
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
   
        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
       
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'Fit - Out Service Request';
        objTemplate1.SR_RecordType_API_Name__c = 'Fit - Out Service Request';
        objTemplate1.Active__c = true;
        insert objTemplate1;
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Courier Collection';
        objStepType1.Code__c = 'Courier Collection';
        objStepType1.Step_RecordType_API_Name__c = 'Courier Collection';
        objStepType1.Summary__c = 'Courier Collection';
        insert objStepType1;
        
        Step_Template__c objStepType2 = new Step_Template__c();
        objStepType2.Name = 'Original Passport Received';
        objStepType2.Code__c = 'Original Passport Received';
        objStepType2.Step_RecordType_API_Name__c = 'Original Passport Received';
        objStepType2.Summary__c = 'Original Passport Received';
        insert objStepType2;
       
        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Email__c = 'testsr@difc.com';    
        objSR3.Contact__c = objContact.Id;
        objSR3.Express_Service__c = false;
        objSR3.SR_Template__c = objTemplate1.Id;
    
        insert objSR3;
    
       Status__c objStatus = new Status__c();
       objStatus.Name = 'Pending Collection';
       objStatus.Type__c = 'Pending Collection';
       objStatus.Code__c = 'Pending Collection';
       insert objStatus;
       
        Status__c CollectedStatus = new Status__c();
       CollectedStatus.Name = 'Collected';
       CollectedStatus.Type__c = 'Collected';
       CollectedStatus.Code__c = 'Collected';
       insert CollectedStatus ;
       
       
       
       Status__c Awaiting = new Status__c();
       Awaiting.Name = 'Awaiting Review';
       Awaiting.Type__c = 'Awaiting Review';
       Awaiting.Code__c = 'Awaiting Review';
       insert Awaiting ;
         
        SR_Status__c objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
        objSRStatus.Code__c = 'AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
        insert objSRStatus;
        
        step__c objStep1 = new Step__c();
       objStep1.SR__c = objSR3.Id;
       objStep1.Step_Template__c = objStepType2.id ;
       objStep1.No_PR__c = false;
       objStep1.status__c = Awaiting.id;
       insert objStep1;
        
       step__c objStep = new Step__c();
       objStep.SR__c = objSR3.Id;
       objStep.Step_Template__c = objStepType1.id ;
       objStep.No_PR__c = false;
       objStep.status__c = CollectedStatus.id;
       objStep.parent_step__c = objStep1.id;
       objStep.Closed_Date_Time__c =system.now().addMinutes(-65);
       objStep.Closed_Date__c =system.today();
       insert objStep;
       
       
         
       Shipment__c thishp = new Shipment__c();
       thishp.step__c     = objStep.Id;
       thishp.Status__c   = 'Delivered';
       insert thishp;
       
       List<step__c> step =[select id,step_name__c,Parent_Step__c,Parent_Step__r.step_name__c ,Parent_Step__r.Step_Status__c,parent_step__r.createdDate,parent_step__R.owner__C,SR_Status__c from step__c where id =:objStep.id];
      
       system.assertEquals( step[0].Parent_Step__r.Step_Status__c, 'Awaiting Review');
        system.assertEquals( step[0].Parent_Step__r.step_name__c , 'Original Passport Received');
      
       Test.startTest();
       GSCloseCollectedParentStepController.GSCloseCollectedParentStepControllerMethod(new List<Id>{objStep.id});
         database.executeBatch(new closeCourierStepsBatch());
          database.executeBatch(new OriginalPassportStepsBatch());
       Test.stopTest();
          
     }

}