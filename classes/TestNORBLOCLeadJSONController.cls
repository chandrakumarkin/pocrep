@isTest
private class TestNORBLOCLeadJSONController{

    static testMethod void myUnitTest() {
        
        
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insertNewAccounts[0].Working_from_Co_Working_Space__c = true;
        insertNewAccounts[0].Tax_Registration_Number_TRN__c = '11111111';
        insertNewAccounts[0].Registration_License_No__c = '6054';
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'LTD';
        insertNewAccounts[0].Office__c = 'Gate District';
        insertNewAccounts[0].Emirate__c = 'Dubai';
        insertNewAccounts[0].City__c = 'Dubai'; 
        insertNewAccounts[0].Country__c = 'UAE';
        insertNewAccounts[0].Place_of_Registration__c = 'India';
        insertNewAccounts[0].Building_Name__c ='DIFC';
        insertNewAccounts[0].Reason_for_exemption__c = 'Wholly owned by government';
        insertNewAccounts[0].Street__c = 'DIFC';
        insert insertNewAccounts;
        
        List<Account> insertNewAccounts1 = new List<Account>();
        insertNewAccounts1 =  OB_TestDataFactory.createAccounts(1);
        INSERT insertNewAccounts1;
        
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insertNewContacts[0].RecordtypeID = OB_QueryUtilityClass.getRecordtypeID('Contact', 'Portal_User'); 
        insertNewContacts[0].Are_you_a_resident_in_the_U_A_E__c  ='Yes';
        insertNewContacts[0].Passport_No__c = 'L4444';
        insertNewContacts[0].Birthdate = system.today();
        insertNewContacts[0].Issued_Country__c = 'India';
        insertNewContacts[0].Passport_Expiry_Date__c =  system.today()+100;
        insertNewContacts[0].Country__c = 'India';
        insertNewContacts[0].MobilePhone = '+97122334456';
        INSERT insertNewContacts;
        
        
        Country_Codes__c countryCodes = new Country_Codes__c();
        countryCodes.Name = 'India';
        countryCodes.Code__c = 'IN';
        countryCodes.Nationality__c = 'Indian';
        countryCodes.Alpha_3_Code__c =  'IND';
        INSERT countryCodes;
        
        Account_Share_Detail__c accountShare = new Account_Share_Detail__c();
        accountShare.Name = 'A';
        accountShare.Account__c = insertNewAccounts[0].Id;
        
        INSERT accountShare;
        
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'New_User_Registration', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].HexaBPM__Contact__c=insertNewContacts[0].id;
        insertNewSRs[1].HexaBPM__Contact__c=insertNewContacts[0].id;
        insertNewSRs[0].Foreign_Entity_Name__c='Test Forien';
        insertNewSRs[0].Place_of_Registration_parent_company__c='Russian Federation';
        
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].Foreign_entity_country_of_registration__c = 'Russian Federation';
        insertNewSRs[1].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','In_Principle');
        insertNewSRs[1].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','AOR_Financial');
        insert insertNewSRs;
        
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        
        HexaBPM_Amendment__c objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment3.Nationality_list__c = 'India';
        objAmendment3.Role__c = 'Shareholder';
        objAmendment3.Passport_No__c = '1r45';
        objAmendment3.Gender__c = 'Female';
        objAmendment3.Mobile__c = '+9712346572';
        objAmendment3.DI_Mobile__c = '+97124567882';
        objAmendment3.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment3);
        INSERT listAmendment;
        
       
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true);
        insert contentVersionInsert;
         List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
         HexaBPM__SR_Doc__c srDoc = new HexaBPM__SR_Doc__c();
        srDoc.HexaBPM_Amendment__c = listAmendment[0].Id;
        srDoc.HexaBPM__Doc_ID__c = listAmendment[0].Id;
        srDoc.Name = 'Passport';
        srDoc.HexaBPM__Service_Request__c=insertNewSRs[0].Id;
        srDoc.HexaBPM__Doc_ID__c = documents[0].Id;
        insert srDoc;
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = srDoc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        
        Shareholder_Detail__c shareDetail = new Shareholder_Detail__c();
        shareDetail.Shareholder_Account__c = insertNewAccounts1[0].Id;
        shareDetail.Shareholder_Contact__c = insertNewContacts[0].ID;
        shareDetail.Account__c = insertNewAccounts[0].Id;
        shareDetail.No_of_Shares__c = 30;
        shareDetail.Account_Share__c = accountShare.ID;
        shareDetail.Status__c = 'Active';
        system.debug('!!@###'+shareDetail);
        INSERT shareDetail;
        
        License__c licenseData = new License__c();
        licenseData.Name = 'License';
        licenseData.Account__c = insertNewAccounts[0].Id;
        licenseData.License_Expiry_Date__c = system.today()+365;
        INSERT licenseData;
        
        Relationship__c relation = new Relationship__c();
        relation.Object_Account__c = insertNewAccounts1[0].Id;
        relation.Subject_Account__c = insertNewAccounts [0].Id;
        relation.Object_Contact__c =  insertNewContacts[0].Id;
        relation.Active__c = true;
        relation.Relationship_Type__c = 'Is Shareholder Of';
        relation.Subject_Account__c = insertNewAccounts[0].Id;
        INSERT relation;
        
        Profile profile1 = [Select Id from Profile where name = 'DIFC Customer Community User Custom'];
        User portalAccountOwner1 = new User(ProfileId = profile1.Id,Username = System.now().millisecond() + 'test2@test.com',ContactId=insertNewContacts[0].Id,
                                            Alias = 'TestDIFC',Email='test123@Difc.com',EmailEncodingKey='UTF-8',Firstname='Test',Lastname='DIFC',
                                            LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Chicago',isActive=true,Community_User_Role__c='Company Services');
        Database.insert(portalAccountOwner1);
        
        system.runAs(portalAccountOwner1){
            Integer turnOverVal = 12333;
            NORBLOCLeadJSONController.norBlockLeadJSONMethod(insertNewAccounts[0].ID,turnOverVal );  
        }     
    }
}