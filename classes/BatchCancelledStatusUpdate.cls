/******************************************************************************************
 *  BatchStepStatusCancelupdate
 
 *  Author      : Veera
 *  Company     : techCarrot
 *  Date        : 2018-07-12     
 *  Modification 
        History :
----------------------------------------------------------------------
   Version  Date         Author             Remarks                                                 
  =======   ==========   =============  ==================================
  
*******************************************************************************************/


global with sharing class BatchCancelledStatusUpdate implements Database.Batchable <sObject > {

   global List<step__c> start(Database.BatchableContext BC){
   
     Date dt = Date.newInstance(2017,1,1);
   
        List<string> status = new List<String>{'Cancellation Pending','The Visa application has been rejected by GDRFA','Cancelled','Rejected','Refund Processed','Refund Processed by Receivable now pending with Payable','Process Completed','The applicant is medically unfit','Cancellation Approved'} ;
   
       List <step__c> lstSteps = [select id,SR__c from step__c where step_status__c = 'Cancelled' and sr__r.External_SR_Status__r.Name not in :status and Sys_SR_Group__c = 'GS' and createdDate > : dt ] ;
       
       return lstSteps ;
   
   }
   
   global void execute(Database.BatchableContext BC, list <step__c> steps) {   
      
      try{
      set<id> Ids = new set<id>();      
      for(step__c stp: steps){         
         Ids.add(stp.sr__c);   
      }
      
       List<SR_Status__c> SRstatus  = [ select id from SR_Status__c where Code__c = 'Cancelled' limit 1];
       List<Service_request__c> ListSR = new List<Service_request__c>(); 
       if(Ids.size()>0){
       
         for(Service_request__c SReq : [select id,External_SR_Status__c,Internal_SR_Status__c from service_request__c where id in : Ids] ){      
           Service_request__c sr = new service_request__c();           
           sr.id = sReq.id;
           sr.External_SR_Status__c = SRstatus[0].id;
           sr.Internal_SR_Status__c = SRstatus[0].id;
           sr.Is_Pending__c = false;
           ListSR.add(sr);         
         }
       }

       update ListSR;  
       
        }catch (Exception ex) {
           Log__c objLog = new Log__c();
           objLog.Type__c = 'Schedule Batch Relationship status Update ';
           objLog.Description__c = 'Exceptio is : ' + ex.getMessage() + '\nLine # ' + ex.getLineNumber();
           insert objLog;
         }  
       
    } 

 

   global void finish(Database.BatchableContext BC) {
   
   
   }
}