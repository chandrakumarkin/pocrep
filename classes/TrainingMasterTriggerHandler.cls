/*
    Author      :Rajil Ravindran
    Date        :01-Sep-2019
    Description :TriggerHandler for TrainingMasterTrigger
*/
public class TrainingMasterTriggerHandler {
    public static void Execute_BI(list<Training_Master__c> TriggerNew){
        //populateEmail(TriggerNew);
    }
    public static void Execute_BU(list<Training_Master__c> TriggerNew,map<Id, Training_Master__c> TriggerOldMap){
        //populateEmail(TriggerNew);
        updateStatus(TriggerNew,TriggerOldMap);
    }
    public static void Execute_AI(list<Training_Master__c> TriggerNew){
        createEvent(TriggerNew);
    }
    public static void Execute_AU(){
        updateEvent();
    }
    /*
    public static void populateEmail(list<Training_Master__c> lstTrainingMaster){
        string strEmail;
        for(Training_Master__c trainingMasterObj : lstTrainingMaster)
        {
            if(TrainingMasterCategory__c.getInstance(trainingMasterObj.Category__c) != null){
                strEmail = TrainingMasterCategory__c.getInstance(trainingMasterObj.Category__c).Email__c;
                trainingMasterObj.Team_Email__c = strEmail;
            }
        }
        
    }
    */
    /*
     * Method : updateStatus
     * Description: This method updates the status of TrainingMaster to Draft when the StartDatetime/Enddatetime changes.
    */
    public static void updateStatus(list<Training_Master__c> TriggerNew,map<Id, Training_Master__c> TriggerOldMap){
            for(Training_Master__c tm: TriggerNew){
                if((TriggerOldMap.get(tm.id).Start_DateTime__c != null && TriggerOldMap.get(tm.id).Start_DateTime__c != tm.Start_DateTime__c)
                  || (TriggerOldMap.get(tm.id).To_DateTime__c != null && TriggerOldMap.get(tm.id).To_DateTime__c != tm.To_DateTime__c)
                  ){
                    tm.Status__c = 'Draft';
                  }
            }
    }
    
    /*
     * Method     : createEvent
     * Description: This method creates an Event on TrainingMaster record creation.
    */
    public static void createEvent(list<Training_Master__c> lstTrainingMaster){
        list<Event> lstEvent = new list<Event>();
        for(Training_Master__c TM : [select id,OwnerId,Start_DateTime__c, To_DateTime__c,Name from Training_Master__c where id in :lstTrainingMaster]){
            if(TM.Start_DateTime__c != null && TM.To_DateTime__c != null)
                lstEvent.add(new Event(Subject = TM.Name, WhatId = TM.id, OwnerId = TM.OwnerId,
                                        StartDateTime = TM.Start_DateTime__c,
                                       EndDateTime = TM.To_DateTime__c, 
                                       IsReminderSet = true, ReminderDateTime = TM.Start_DateTime__c.addDays(-1)
                                      )
                            );
        }
        if(lstEvent.size() > 0)
            insert lstEvent;
    }
    /*
     *  Method     : updateEvent
        Description: This method updates the event startdate/enddatetime whenever there is an TrainingMaster update
                     for startdatetime and enddatetime.
    */
    public static void updateEvent(){
        map<id,Training_Master__c> mapTrainingMaster = new map<id,Training_Master__c>();
        mapTrainingMaster = (Map<id,Training_Master__c>) Trigger.newMap;
        list<Event> lstEvent = new list<Event>();
        for(Event event : [select id,Subject,StartDateTime,EndDateTime,WhatId,IsReminderSet,ReminderDateTime from Event where WhatId in :mapTrainingMaster.values()]){
                event.Subject = mapTrainingMaster.get(event.WhatId).Name;
                event.StartDateTime = mapTrainingMaster.get(event.WhatId).Start_DateTime__c;
                event.EndDateTime = mapTrainingMaster.get(event.WhatId).To_DateTime__c;
                event.IsReminderSet = true;
                event.ReminderDateTime = mapTrainingMaster.get(event.WhatId).Start_DateTime__c.addDays(-1);
                lstEvent.add(event);
        }
        if(lstEvent.size() > 0)
            update lstEvent;
    }
}