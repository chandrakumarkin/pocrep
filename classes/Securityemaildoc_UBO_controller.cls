/**************************************************************************************************
  Author     :  Arun Singh 
  Company    :  NSI
  Class Name :  Securityemaildoc_UBO_controller
  Description:  Use for send UBO email .

Modification History 
======================================================================================================================================

**************************************************************************************************/

public with sharing class Securityemaildoc_UBO_controller
{

    public string companyName{get;set;}
    public string entityType{get;set;}
    public string Servicetype{get;set;}
    public string companyActivities{get;set;}
    
    
    
    public List<UBO_Data__c> indUBODet{get;set;}
    public List<UBO_Data__c> bodycorporateUBODet{get;set;}
    public List<UBO_Data__c> ListDirectors{get;set;}
    
    public string SRLegalStructuresfull{get;set;}
    private id accountIds;
    public string displayType{get;set;} 
    private  Service_Request__c serviceReq;
    
    public Securityemaildoc_UBO_controller()
    {
        displayType = apexpages.currentPage().getParameters().get('contentTyp');
    }
    
    Public void generateExcel()
    {
        indUBODet=new  List<UBO_Data__c>();
        bodycorporateUBODet=new  List<UBO_Data__c>();
        ListDirectors=new  List<UBO_Data__c>();
        
    
        id SRId = apexpages.currentPage().getParameters().get('SRID'); 
        companyActivities ='';
        entityType = '';
        companyName ='';  
        
        if(SRId !=null)
        {
           
           // amendmentDetails =    returnAmmendments(SRId);   
            
            serviceReq = [select Customer__r.name,Customer__r.License_Activity_Details__c,Legal_Structures_Full__c,Legal_Structures_Full_Form__c,Foreign_Registration_Name__c,Legal_Structures__c,Foreign_Registration_Place__c,Foreign_Registration_Date__c,Service_Type__c,Customer__c,Customer__r.Company_Type__c from Service_Request__c where id =:SRId];            
            System.debug('serviceReq=='+serviceReq);  

            accountIds = serviceReq.customer__c ; //amendmentDetails[0].Customer__c;
            companyName = serviceReq.Customer__r.name;
            entityType = serviceReq.Customer__r.Company_Type__c;   
            SRLegalStructuresfull=serviceReq.Legal_Structures_Full__c==null?serviceReq.Legal_Structures_Full_Form__c:serviceReq.Legal_Structures_Full__c;  
           
            
            
            string Act='';
              for(License_Activity__c ObjActiv:[select Formula_Activity_Name_Eng__c from License_Activity__c where End_Date__c=null and Account__c=:accountIds])
            {
                Act+=ObjActiv.Formula_Activity_Name_Eng__c+';';
            }
            
            companyActivities=Act;
            
            
            Servicetype=serviceReq.Service_Type__c;
            fetchUBORecords(serviceReq);
            
            fetchAmdRecords(serviceReq);
        }
        
        
        //======================Old code ==================================================================================================
        
       
        
    }
    
    public List<Amendment__c> SectionA{get;set;} //Individual (Shareholder / Member / Partner / Founding Member / Founder) of the proposed entity 
    public List<Amendment__c> SectionB{get;set;}//Body Corporate (Shareholder / Member / Partner/ Founding Member / Founder ) of the proposed entity or the parent entity of the branch being set up in DIFC
    //List<Amendment__c> SectionA{get;set;}//List of Individual Directors for the proposed entity (Section only required if Section C is empty)
    
    //Shareholder=Is Shareholder Of
    //Member=
    public void fetchAmdRecords(Service_Request__c tempserviceReq)
    {
        List<String> AmdType=new List<String>();  
        AmdType.add('Individual');
        AmdType.add('Body Corporate');
        
        List<String> AmdRelType=new List<String>();  
        AmdRelType.add('Shareholder');
        AmdRelType.add('Member');
        AmdRelType.add('General Partner');
        AmdRelType.add('Limited Partner');
        AmdRelType.add('Founding Member');
        AmdRelType.add('Founder');
        AmdRelType.add('UBO');//Added to support old SR's
        
        SectionA=new List<Amendment__c>();
        SectionB=new List<Amendment__c>();
        
        //Relationship_Type__c
        for(Amendment__c ObjAmendment:[select Title_new__c,Family_Name__c,Given_Name__c,Nationality_list__c,Passport_No__c,Gender__c,Phone__c,Date_of_Birth__c,
        Company_Name__c,Place_of_Registration__c,Registration_Date__c,BC_UBO_Type__c,Registration_No__c,Relationship_Type__c,Amendment_Type__c  from Amendment__c where 
        ServiceRequest__c=:tempserviceReq.id and Amendment_Type__c in :AmdType and Relationship_Type__c in :AmdRelType])
        {
            if(ObjAmendment.Amendment_Type__c!=null)
            {
                if(ObjAmendment.Relationship_Type__c=='UBO')//only for old SR's which is in draft after new UBO live
                {
                    
                    UBO_Data__c ObjUBO=getAmdData(ObjAmendment);
                    
                    if(ObjAmendment.Amendment_Type__c=='Individual')
                    {
                          indUBODet.add(ObjUBO);
                        
                    }
                    if(ObjAmendment.Amendment_Type__c=='Body Corporate')
                    {
                             bodycorporateUBODet.add(ObjUBO);
                    }
                    
                }
                else
                {
                    
                    if(ObjAmendment.Amendment_Type__c=='Individual')
                    {
                        SectionA.add(ObjAmendment);
                        
                    }
                    if(ObjAmendment.Amendment_Type__c=='Body Corporate')
                    {
                            SectionB.add(ObjAmendment);
                    }
                
                    
                }
                
            
            }
            
            
            
        }
        
        if(serviceReq.Legal_Structures__c=='FRC' && serviceReq.Foreign_Registration_Name__c!=null)
        {
            Amendment__c ObjAmendmentFRC=new Amendment__c();
            ObjAmendmentFRC.Company_Name__c=serviceReq.Foreign_Registration_Name__c;
            ObjAmendmentFRC.Place_of_Registration__c=serviceReq.Foreign_Registration_Place__c;
            ObjAmendmentFRC.Registration_Date__c=serviceReq.Foreign_Registration_Date__c;
            //ObjAmendmentFRC.Registration_No__c=serviceReq.Foreign_Registration_Name__c;
            ObjAmendmentFRC.Amendment_Type__c='Body Corporate';
             SectionB.add(ObjAmendmentFRC);
        }
        
        
        
        
        
        //Fatch Director info if no record in 3 and 4 
        
        System.debug(indUBODet.IsEmpty());
        System.debug(bodycorporateUBODet.IsEmpty());
        System.debug(ListDirectors.IsEmpty());
        
        if(indUBODet.IsEmpty() && ListDirectors.isEmpty() && bodycorporateUBODet.IsEmpty()==false)
        {
            
        
        for(Amendment__c ObjAmendment:[select Title_new__c,Family_Name__c,Given_Name__c,Nationality_list__c,Passport_No__c,Gender__c,Phone__c,Date_of_Birth__c,
        Company_Name__c,Place_of_Registration__c,Registration_Date__c,BC_UBO_Type__c,Registration_No__c,Relationship_Type__c,Amendment_Type__c  from Amendment__c where 
        ServiceRequest__c=:tempserviceReq.id and Amendment_Type__c='Individual' and Relationship_Type__c='Director' and Status__c='Draft'])
        {
            System.debug('ObjAmendment===>'+ObjAmendment);
              UBO_Data__c ObjUBO=getAmdData(ObjAmendment);
               ListDirectors.add(ObjUBO);
                    
        }
        
        if(ListDirectors.isEmpty())
        getRelData([select Object_Contact__r.Salutation,Object_Contact__r.FirstName,Object_Contact__r.LastName, 
            Object_Contact__r.Nationality__c,Object_Contact__r.Passport_No__c,Object_Contact__r.Birthdate,Object_Contact__r.Gender__c,Object_Contact__r.Phone from Relationship__c where Object_Contact__c!=null and Active__c=true and Relationship_Type__c='Has Director' and Subject_Account__c=:accountIds]);
        
        
        }
        
            /*
        public List<UBO_Data__c> indUBODet{get;set;}
    public List<UBO_Data__c> bodycorporateUBODet{get;set;}
    public List<UBO_Data__c> ListDirectors{get;set;}
    
    
        */
        
        
    }
    
    public void getRelData(List<Relationship__c> ListRel)
    {
        
        for(Relationship__c ObjRel:ListRel)
        {
            System.debug('ObjRel===>'+ObjRel);
                Contact ObjAmendment =ObjRel.Object_Contact__r;
        
            UBO_Data__c ObjUBO=new UBO_Data__c();
            
            ObjUBO.Title__c=ObjAmendment.Salutation;
            ObjUBO.First_Name__c=ObjAmendment.FirstName;
            ObjUBO.Last_Name__c=ObjAmendment.LastName;
            ObjUBO.Nationality__c=ObjAmendment.Nationality__c;
            ObjUBO.Passport_Number__c=ObjAmendment.Passport_No__c;
            ObjUBO.Date_of_Birth__c=ObjAmendment.Birthdate;
            ObjUBO.Gender__c=ObjAmendment.Gender__c;
            ObjUBO.Phone_Number__c = ObjAmendment.Phone;
        
            ListDirectors.add(ObjUBO);
            
        }
        
        
    }
    
    public UBO_Data__c getAmdData(Amendment__c ObjAmendment)
    {
        
        UBO_Data__c ObjUBO=new UBO_Data__c();
        ObjUBO.Entity_Name__c=ObjAmendment.Company_Name__c;
        ObjUBO.Select_Regulator_Exchange_Govt__c=ObjAmendment.Place_of_Registration__c;
        ObjUBO.Date_Of_Registration__c=ObjAmendment.Registration_Date__c;
        ObjUBO.Reason_for_exemption__c=ObjAmendment.BC_UBO_Type__c;
        
        
        
        ObjUBO.Title__c=ObjAmendment.Title_new__c;
        ObjUBO.First_Name__c=ObjAmendment.Given_Name__c;
        ObjUBO.Last_Name__c=ObjAmendment.Family_Name__c;
        ObjUBO.Nationality__c=ObjAmendment.Nationality_list__c;
        ObjUBO.Passport_Number__c=ObjAmendment.Passport_No__c;
        ObjUBO.Date_of_Birth__c=ObjAmendment.Date_of_Birth__c;
        ObjUBO.Gender__c=ObjAmendment.Gender__c;
        ObjUBO.Phone_Number__c=ObjAmendment.Phone__c;
        
        //ObjUBO.Entity_Name__c=ObjAmendment.Company_Name__c;
        //ObjUBO.Entity_Name__c=ObjAmendment.Company_Name__c;
        
        
        return ObjUBO;
        
    }
    
    public void fetchUBORecords(Service_Request__c tempserviceReq)
    {
        string strContactQuery = Apex_Utilcls.getAllFieldsSOQL('UBO_Data__c');
        strContactQuery = strContactQuery+' where Service_Request__c=\''+tempserviceReq.id+'\' and Status__c!=\'Removed\'';
        System.debug('strContactQuery===>'+strContactQuery);
        strContactQuery=strContactQuery.Replace('Select Id','Select Id,Recordtype.name ');
        
        for(UBO_Data__c ObjUBO:database.query(strContactQuery))
        {
                if(ObjUBO.Recordtype.name=='Individual')
                {
                    if(ObjUBO.Entity_Type__c!=null)
                    {
                        if(ObjUBO.Entity_Type__c.contains('deemed'))
                         {
                            ListDirectors.add(ObjUBO);
                         }
                         else
                         {
                             indUBODet.add(ObjUBO);
                             
                         }
                    }
                    else
                        indUBODet.add(ObjUBO);
                        
                    
                }
                //BC type 
                else
                {
                    bodycorporateUBODet.add(ObjUBO);
                    
                }
                    
                      
        }
        
        
        
    }
    
}