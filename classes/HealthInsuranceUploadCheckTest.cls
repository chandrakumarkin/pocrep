@isTest
public class HealthInsuranceUploadCheckTest {
    
     static testMethod void myUnitTest() {
        
        List<Account> lstAccount = new List<Account>();
   		Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        lstAccount.add(objAccount);
        string conRT;
         
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
   
        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
       
         SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'Fit - Out Service Request';
        objTemplate1.SR_RecordType_API_Name__c = 'Fit - Out Service Request';
        objTemplate1.Active__c = true;
        insert objTemplate1;
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test Completed';
        objStepType1.Code__c = 'Medical Fitness Test Completed';
        objStepType1.Step_RecordType_API_Name__c = 'Medical Fitness Test Completed';
        objStepType1.Summary__c = 'Medical Fitness Test Completed';
        insert objStepType1;
       
        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Email__c = 'testsr@difc.com';    
        objSR3.Contact__c = objContact.Id;
        objSR3.Express_Service__c = false;
        objSR3.SR_Template__c = objTemplate1.Id;
    
        insert objSR3;
    
       Status__c objStatus = new Status__c();
       objStatus.Name = 'closed';
       objStatus.Type__c = 'closed';
       objStatus.Code__c = 'CLOSED';
       insert objStatus;
       
         
       Status__c objStatus1 = new Status__c();
       objStatus1.Name = 'Completed';
       objStatus1.Type__c = 'Completed';
       objStatus1.Code__c = 'Completed';
       insert objStatus1;
         
        SR_Status__c objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
        objSRStatus.Code__c = 'AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
        insert objSRStatus;
        
       step__c objStep = new Step__c();
       objStep.SR__c = objSR3.Id;
     //  objStep.Step_Name__c = 'Medical Fitness Test Completed';
       objStep.Step_Template__c = objStepType1.id ;
       objStep.No_PR__c = false;
       objStep.status__c = objStatus.id;
       insert objStep;
         
        SR_Doc__c srDoc = new SR_Doc__c();
        //srDoc.Document_Master__c = [Select Id from Document_Master__c where Name = 'Daily Car Parking Collection Document' limit 1].ID;
        srDoc.Service_Request__c = objSR3.ID;
        srDoc.Status__c = 'Pending Upload';
         srDoc.Name = 'Health Insurance Certificate';
        insert srDoc; 
    
        Test.startTest();
           HealthInsuranceUploadCheck.HealthInsuranceUpdate(new List<id>{srDoc.Id});
        Test.stopTest();
        	
     }
   
}