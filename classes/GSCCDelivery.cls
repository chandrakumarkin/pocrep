/**
 * @description       : Used this call to create shipment delivery once FD is posted for DIFC to others
 * @author            : Veera 
 * @group             : 
 * @last modified on  : 13-08-2021
 * @last modified by  : Veera
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   13-08-2021   Veera                                Initial Version
**/

public without sharing class GSCCDelivery{

     @InvocableMethod(Label = 'GSCCDeliveryFDDifctoOthers'  description = 'GSCCDeliveryFDDifctoOthers')
       public static void GSCCDeliveryMethod(List < id > ids) {
        
         System.enqueueJob(new GSCCDelivery_Queable(ids[0]));
       
       }
  
}