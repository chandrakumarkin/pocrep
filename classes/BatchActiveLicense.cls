/***********************************************************************************
 *  Author   : Arun 
 *  Purpose  : Batch to update companies Active License
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No           Date            Updated By       Description
 ----------------------------------------------------------------------------------------              
 V 1.0                          Arun              Initial Version
 v 1.1       08 May2019         Sai              Added Line 56 to Populate Operating location Building Name in Account (Ticket Num:6754)        
 v 1.200     22-Oct-2020        Arun             Added to support management detail and fund setup notifiation  #11667  
 v 1.3      31-Dec-2020         Suchita          Added logic to ensure clients update their passport details and other missing details(Ticket Num 13034)

**********************************************************************************************/

global class BatchActiveLicense implements Database.Batchable<sObject>
{

   global final String Query;

   global BatchActiveLicense (String q){

      Query=q; 
   }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<account> ListAccounts)
   {
       /*
     for(sobject s : scope){
     s.put(Field,Value); 
     }
     update scope;
     List<account> ListAccounts=[select id,Active_Leases__c,(select id from Lease__r where Status__c='Active') from Account where ROC_Status__c='Active' and Active_Leases__c=null limit 200];
     

        for(Account ObjAccount:ListAccounts)
        {
            ObjAccount.Active_Leases__c=ObjAccount.Lease__r.Size();
        }
        update ListAccounts;
        */
        //[select id,Active_Leases__c,Lease_Expiry_Date__c,(select id from Lease__r where Status__c='Active') from Account where ROC_Status__c='Active' and Lease_Expiry_Date__c=null limit 200]
        Map<Id,Account> MapAccount=new Map<Id,Account>();
       // List<id> ListExpiredId=new List<Id>();
       
       //Management Details
     //  List<id> CompanyProvided=new List<id>();
       Map<id,string> CompanyProvided=new Map<id,string>();
       //Not provided Management Details
       Map<id,string> CompanyNotProvided=new Map<id,string>();
       
        for(Account ObjAccount:ListAccounts)
        {
            ObjAccount.Active_Leases__c=ObjAccount.Lease__r.Size();
            
            ObjAccount.Registered_Active_Lease__c=null;
            
            for(Operating_Location__c opp:ObjAccount.Operating_Locations__r)
            {
                
                ObjAccount.Registered_Active_Lease__c=opp.Lease__c;
                ObjAccount.Operating_Location__c =opp.ID;      // This Logic added by Sai to Populate Operating location Building Name in Account
                
            }
            
            /***************Management Detail Check*********************/
 System.debug('=======================Management Detail Check*=========================');
           Set<string> RelationshipTypeCode = new Set<string>{'Senior Management','General Communication', 'Emergency contact' };
            
             for(Relationship__c RelObj:ObjAccount.Primary_Account__r)
            {
                RelationshipTypeCode.remove(RelObj.Relationship_Type_formula__c);
            }
            //Management Detail provided ?// if RelationshipTypeCode bank customer provided all the detail.
            //Remove from notifiation list 
            //Management Detail not provided or few proviced ?if RelationshipTypeCode not bank customer provided all the detail.
            //Add from notifiation list 
            
            if(RelationshipTypeCode.isEmpty())
            {
                ObjAccount.Contact_Details_Provided__c=true;
                CompanyProvided.put(ObjAccount.id,'');
            }
                
            else 
            {
                ObjAccount.Contact_Details_Provided__c=false;
                CompanyNotProvided.put(ObjAccount.id,RelationshipTypeCode.toString());
            
            }
                if(!CompanyNotProvided.isEmpty())
                CC_ROCCodeCls.AddEnactmentNotification(CompanyNotProvided,'Contact Info','ROC');
                if(!CompanyProvided.isEmpty())
                CC_ROCCodeCls.RemoveEnactmentNotification(CompanyProvided,'Contact Info');
            

            /***************Fund setup  Check*********************/
 System.debug('=============================================================Fund setup  Check====================================================');
     
            Set<string> FundTypeCode = new Set<string>{'Has Exempt Fund', 'Has QIF'};
            //Set Fund Setup Date value when company setup thire fund . if check not blank only then set
           System.debug('=====ObjAccount=>'+ObjAccount);
            if(ObjAccount.DFSA_Lic_Appr_Date__c!=null && ObjAccount.Sector_Classification__c=='Authorized Firm' && ObjAccount.Fund_Setup_Date__c==null &   ObjAccount.Discount_Applied__c=='Yes' && ObjAccount.FinTech_Classification__c=='Start Up Fund Manager new Regime')
            {
                
                for(Relationship__c RelObj:ObjAccount.Primary_Account__r)
                    if(FundTypeCode.contains(RelObj.Relationship_Type__c) && RelObj.BC_ROC_Status__c=='Active')
                    {
                    System.debug('=====RelObj=>'+RelObj);
                        ObjAccount.Fund_Setup_Date__c=RelObj.BC_Registration_Date__c;
                    }
                    
                
            }
            
            //if(ObjAccount.Lease__r.Size()==0)
           // {
              //  ListExpiredId.add(ObjAccount.id);
            //}
            
            MapAccount.put(ObjAccount.id,ObjAccount);
            //Start 13034   
            // Calling Methods for Relationship Missing details     
               List<Relationship__c> missingDetailsList = AccountsRelationshipMissingDetails.RelationshipMissingDetails(ObjAccount.id); 
               Map<id,string> MapAccountIds = new Map<id,String>(); 
               MapAccountIds.put(ObjAccount.id,'Relationship Missing Details'); 
               if(!missingDetailsList.isEmpty()){   
                   AccountsRelationshipMissingDetails.AddEnactmentNotiRelMissingDetails(ObjAccount.id,missingDetailsList);  
               }else if(missingDetailsList.isEmpty()){  
                   CC_ROCCodeCls.RemoveEnactmentNotification(MapAccountIds,'Relationship Missing Details'); 
               }// END 13034
        }
        
        for(Account tempObjAccount:[select id,Lease_Expiry_Date__c,(select id,Name,End_Date__c from Lease__r where Status__c!='Active' order by End_Date__c DESC limit 1) from Account  where id in : MapAccount.keyset()])
        {
             tempObjAccount.Lease_Expiry_Date__c =null;
            Account ObjAccount=MapAccount.get(tempObjAccount.id);
            if(tempObjAccount.Lease__r.Size()==1)
            {
                ObjAccount.Lease_Expiry_Date__c =   tempObjAccount.Lease__r[0].End_Date__c;
            }
            
            
        }
        Update MapAccount.values();
		    
    }

   global void finish(Database.BatchableContext BC)
   {
   }
}