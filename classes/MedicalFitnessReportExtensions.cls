/**********************************************************************************************************************
    Author      :   Ghunshyam
    Class Name  :   MedicalFitnessReportExtensions
    Description :   This class is extension for the MedicalFitnessReport page used by DHA team.
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    22-11-2018  Ghunshyam      Created
    V1.2    13-01-2019  Azfer          OverHaulled, revamp the Class
    V1.3    06-02-2019  Azfer          Added the Applicant Arrival field
**********************************************************************************************************************/
public class MedicalFitnessReportExtensions {

    public final Step__c Step;
    public List<SR_Doc__c> ListSRDocs { get; set; }
    public List<Attachment> ListAttachments { get; set; }
    public String ChangeOfStatusText { get; set; }
    public Boolean ShowMedicalDetails { get; set; }
    public Boolean ShowSecondMedical { get; set; }
    public Boolean ShowThirdMedical { get; set; }
    public Boolean Unfit { get; set;}
    
    public String LabelForVisaOrEntryNo { get; set; }
    public String VisaOrEntryNo { get; set; }
    // Code by Firdous Fatima
    
    public Boolean ShowLstVw { get;set; }
    public Boolean ShowMessage  { get;set; }
    public Id ListViewId { get;set; }
    string RetUrl;
    
    public void AppointmentDetails(){        
        ShowMessage = false;
        
        //Getting use information from labels
        Id CurrentUserId = UserInfo.getUserId();
        Id GroupId = Id.ValueOf( Label.DHA_Team_Public_Group );
        
        // Checking membership of the user from the group.
        List<GroupMember> ListGroupMember = [SELECT Id, Group.Name, Group.DeveloperName FROM GroupMember WHERE UserOrGroupId =: CurrentUserId AND GroupId =: GroupId];
        
        // if user is part of the Group show the list view else show the message.
        if( ListGroupMember.size() > 0 ){
            ShowLstVw = true;                                     
            ListViewId = Id.ValueOf( Label.DHA_Team_Listview );
        }else{
            ShowMessage = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.DHA_Team_Message ));
        }
        ShowMedicalDetails = false;        
        
    }
    
    
    //End of code by Firdous Fatima
    
    public MedicalFitnessReportExtensions(ApexPages.StandardController stdController){
        
        LabelForVisaOrEntryNo = '';
        VisaOrEntryNo = '';        
        
        Map<String, Attachment> MapNametoAttachment = new Map<String, Attachment>();            
        
        Id AppointmentId = ApexPages.currentPage().getParameters().get('appointmentId');
        RetUrl = ApexPages.currentPage().getParameters().get('retUrl');
        
        if( AppointmentId == null ){
            
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.DHA_Appointment_Not_Found ));
            ShowMedicalDetails = false;
            return;
        }

        Appointment__c AppointmentObj = [SELECT Id, Appointment_StartDate_Time__c FROM Appointment__c WHERE Id=: AppointmentId ];
        // Need to uncomment before deployment
        /*if( AppointmentObj.Appointment_StartDate_Time__c != null && AppointmentObj.Appointment_StartDate_Time__c.date() == System.today() ){
            ShowMedicalDetails = true;
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.DHA_Appointment_Rescheduled ));
            ShowMedicalDetails = false;
            return;
        }*/
        ShowMedicalDetails = true;
        ShowLstVw = false;
        
        string StepId = ApexPages.currentPage().getParameters().get('Id');         
        this.Step = (Step__c)stdController.getRecord(); 
        
        List<Step__c> ListStep = [SELECT Id, Applicant_Arrived__c, Application_Reference__c, Name, Medical_Process_Completed__c,Medical_Fitness__c, Second_Medical_Done__c,Third_Medical_Done__c,
                                    SR__c, SR__r.Residence_Visa_No__c, SR__r.Service_Type__c, SR__r.Type_of_Request__c
                                FROM Step__c 
                                WHERE Id =: StepId 
                                AND SR__r.SR_Group__c = 'GS'];
        //this.Step = ListStep[0];
        List<Step__c> ListChangeofStatusisCompletedStep = [SELECT Id, Step_Status__c,
                                                            SR__r.Type_of_Request__c
                                                            FROM Step__c 
                                                            WHERE SR__C =: ListStep[0].SR__c 
                                                            AND Step_Name__c =: Label.DHA_Step_Name
                                                            AND SR__r.SR_Group__c = 'GS'];
        if( ListChangeofStatusisCompletedStep.size() > 0 && 
            ListChangeofStatusisCompletedStep[0].SR__r.Type_of_Request__c  == 'Applicant Inside UAE' ){
            
            if( ListChangeofStatusisCompletedStep[0].Step_Status__c == 'Closed' ){
                ChangeOfStatusText = 'Completed';
            }else{
                ChangeOfStatusText = 'Not Completed';
            }
        }else{
            ChangeOfStatusText = 'N/A';
        }
        
        Set<String> SetDocumentsToShow = new Set<String>(); 
        system.debug(Label.DHA_Documents_To_Show);
        if( Label.DHA_Documents_To_Show.contains(';') ){
            SetDocumentsToShow.addAll( Label.DHA_Documents_To_Show.split(';') );
        }else{
            SetDocumentsToShow.add( Label.DHA_Documents_To_Show );
        }

        if( ListStep.size() > 0 ){
        

            if(ListStep[0].Medical_Fitness__c == 'No')
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.GSA_Candidate_Marked_Unfit));
                Unfit = true;
            }           
            if( ListStep[0].SR__c != null &&  ListStep[0].SR__r.Service_Type__c != null && 
                ListStep[0].SR__r.Service_Type__c.containsIgnoreCase( 'renewal' ) && SetDocumentsToShow.contains( Label.DHA_Remove_Document_For_Renewal ) ){
                SetDocumentsToShow.remove( Label.DHA_Remove_Document_For_Renewal );
            }

            if( ListStep[0].SR__c != null &&  ListStep[0].SR__r.Service_Type__c != null ){
                
                if( ListStep[0].SR__r.Service_Type__c.containsIgnoreCase( 'new' ) && !ListStep[0].SR__r.Service_Type__c.containsIgnoreCase( 'renewal' ) ){

                    LabelForVisaOrEntryNo = Label.DHA_Entry_Permit_Text;
                    List<Step__c> ListStepEntryPermit = [SELECT Id, SAP_IDNUM__c 
                                                        FROM Step__C 
                                                        WHERE Step_Name__c =: Label.DHA_Step_Name_Entry_No
                                                        AND SR__C =: ListStep[0].SR__c ]; 
                    if( ListStepEntryPermit.size() > 0 ){
                        if( ListStepEntryPermit[0].SAP_IDNUM__c != null && ListStepEntryPermit[0].SAP_IDNUM__c != '' ){
                            VisaOrEntryNo = ListStepEntryPermit[0].SAP_IDNUM__c;    
                        }

                        if( VisaOrEntryNo != '' && VisaOrEntryNo.contains('/') ){
                            VisaOrEntryNo = VisaOrEntryNo.replaceAll('/', '');
                        }
                    }
                }else if( ListStep[0].SR__r.Service_Type__c.containsIgnoreCase( 'renewal' ) ){
                    LabelForVisaOrEntryNo = Label.DHA_Visa_No;
                    if( ListStep[0].SR__r.Residence_Visa_No__c != null && ListStep[0].SR__r.Residence_Visa_No__c != '' ){
                        VisaOrEntryNo = ListStep[0].SR__r.Residence_Visa_No__c;    
                    }
                    if( ListStep[0].SR__r.Residence_Visa_No__c != null ){
                        VisaOrEntryNo = ListStep[0].SR__r.Residence_Visa_No__c;
                    }
                    
                    if( VisaOrEntryNo != '' && VisaOrEntryNo.contains('/') ){
                            VisaOrEntryNo = VisaOrEntryNo.replaceAll('/', '');
                    }
                }
            }

            
            ListSRDocs = [SELECT Id, Name, Document_Description_External__c, Status__c, Preview_Document__c 
                            FROM SR_Doc__c 
                            WHERE Service_Request__c =: ListStep[0].SR__c 
                            AND Name IN: SetDocumentsToShow ];         
    
            for( Attachment AttachmentObj : [SELECT Id, Name, Description, LastModifiedDate,
                                                ParentId, Parent.Name
                                            FROM Attachment WHERE ParentId =: ListSRDocs ] )
            {
                if( MapNametoAttachment.containsKey( AttachmentObj.Parent.Name ) ){
                    Attachment TempAttachmentObj = MapNametoAttachment.get( AttachmentObj.Parent.Name );
                    if( TempAttachmentObj.LastModifiedDate < AttachmentObj.LastModifiedDate ){
                        MapNametoAttachment.put( AttachmentObj.Parent.Name, AttachmentObj );
                    }
                }else{
                    MapNametoAttachment.put( AttachmentObj.Parent.Name, AttachmentObj );
                }
            }

            ListAttachments = MapNametoAttachment.values();
        }
    }
    
    public pagereference UpdateStep(){
        // update the Step, populate the KPI Fields and navigate back to the list view
        PageReference PageReferenceObj = NavigateToListView();
        Id StepId = ApexPages.currentPage().getParameters().get('Id');
        
        Step__c StepObj = new Step__c(Id = StepId);
        StepObj.Is_blood_test_done__c = step.Is_blood_test_done__c;
        StepObj.Is_x_ray_done__c = step.Is_x_ray_done__c;
        
        if(StepObj.Is_blood_test_done__c){
            StepObj.Blood_Test_Done_on__c = System.now();
        }else{
            StepObj.Blood_Test_Done_on__c = null;
        }
        
        if(StepObj.Is_x_ray_done__c){
            StepObj.X_ray_Done_On__c = System.now();
        }else {
            StepObj.X_ray_Done_On__c = null;
        }
        update StepObj;
        
        return PageReferenceObj;
    }
    
    public pagereference UpdateApplicantStatus(){
        // update the Step, populate the Applicant_Arrived__c Fields and navigate back to the list view
        // Applicant_Arrived__c  = true will mean that the applicant has arrived.
        PageReference PageReferenceObj = NavigateToListView();
        Id StepId = ApexPages.currentPage().getParameters().get('Id');
        
        Step__c StepObj = new Step__c(Id = StepId);
        StepObj.Applicant_Arrived__c = true;
        
        update StepObj;
        
        return PageReferenceObj;
    }
    
    public pagereference ProcessCompleted(){
        
        //Navigate back to the list view
        PageReference PageReferenceObj = NavigateToListView();
        
        Id StepId = ApexPages.currentPage().getParameters().get('Id');
        
        Step__c StepObj = [SELECT Id, Blood_Test_Done_on__c, Is_blood_test_done__c, Is_x_ray_done__c, X_ray_Done_On__c 
                            FROM Step__c
                            WHERE Id =: StepId ];
                            
        if( StepObj.Is_blood_test_done__c != step.Is_blood_test_done__c ){
            StepObj.Is_blood_test_done__c = step.Is_blood_test_done__c;
            if( StepObj.Is_blood_test_done__c ){
                StepObj.Blood_Test_Done_on__c = System.now();    
            }else{
                StepObj.Blood_Test_Done_on__c = null;
            }
        }

        if( StepObj.Is_x_ray_done__c != step.Is_x_ray_done__c ){
            StepObj.Is_x_ray_done__c = step.Is_x_ray_done__c;
            if( StepObj.Is_x_ray_done__c ){
                StepObj.X_ray_Done_On__c = System.now();    
            }else{
                StepObj.X_ray_Done_On__c = null;
            }
        }
        
        StepObj.Medical_Process_Completed__c = true;
        StepObj.Medical_Completed_On__c = System.now();
        
        update StepObj;
        ShowSecondMedical=true;
        
        return PageReferenceObj;
    }
    
    // Added by Hussain Fazal Ullah //
    public void SecondMedicalDone(){
        
        //Navigate back to the list view
        // PageReference PageReferenceObj = NavigateToListView();
        
        Id StepId = ApexPages.currentPage().getParameters().get('Id');
        
        Step__c StepObj = [SELECT Id,Medical_Process_Completed__c,Second_Medical_Done__c, Blood_Test_Done_on__c, Is_blood_test_done__c, Is_x_ray_done__c, X_ray_Done_On__c 
                            FROM Step__c
                            WHERE Id =: StepId];
        if( StepObj.Medical_Process_Completed__c == true)
        {
            StepObj.Second_Medical_Done__c =true;
            StepObj.Second_Medical_Completed_On__c = System.now();
            ShowThirdMedical = true;
            ShowSecondMedical = false;            
            ShowMedicalDetails = false;
 
        }
        update StepObj;
        if(RetUrl != '' && RetUrl != null){ callDelayedMedicalResults(); } else showLstVw = true;        
    }
    // Added by Hussain Fazal Ullah //  
    public void ThirdMedicalDone(){
        
        Id StepId = ApexPages.currentPage().getParameters().get('Id');
        
        Step__c StepObj = [SELECT Id,Medical_Process_Completed__c, Second_Medical_Done__c,Third_Medical_Done__c,Blood_Test_Done_on__c, Is_blood_test_done__c, Is_x_ray_done__c, X_ray_Done_On__c 
                            FROM Step__c
                            WHERE Id =: StepId];
        if( StepObj.Medical_Process_Completed__c == true && StepObj .Second_Medical_Done__c == true)
        {
            StepObj.Third_Medical_Done__c=true;
            StepObj.Third_Medical_Completed_On__c = System.now();            
            ShowMedicalDetails = false;
        }
        update StepObj;
        if(RetUrl != '' && RetUrl != null){ callDelayedMedicalResults(); } else showLstVw = true;
    }
    
    // Added by Hussain Fazal Ullah //  
    public void MarkUnfit(){
        
        Id StepId = ApexPages.currentPage().getParameters().get('Id');
        
        Step__c StepObj = [SELECT Id,Medical_Process_Completed__c,Medical_Fitness__c, Second_Medical_Done__c,Third_Medical_Done__c,Blood_Test_Done_on__c, Is_blood_test_done__c, Is_x_ray_done__c, X_ray_Done_On__c FROM Step__c WHERE Id =: StepId];        
        if( StepObj.Medical_Process_Completed__c == true)
        {
            StepObj.Medical_Fitness__c='No';
            StepObj.Marked_Unfit_On__c = System.now();
            Unfit = true;
            // Code by Firdous Fatima on 8th Dec, 2019            
            ShowMedicalDetails = false;
        }
        update StepObj;               
        if(RetUrl != '' && RetUrl != null){ callDelayedMedicalResults(); } else showLstVw = true;
        
        // End of code by Firdous Fatima
        
    }
        
    
    
    public pagereference CancelAndNavigateToListView(){
        PageReference PageReferenceObj ;
        // Check if retUrl was specified
        if(RetUrl != '' && RetUrl != null){            
            return new PageReference('/apex/DelayedMedicalResults');  
        }
        else{
            //Navigate back to the list view
            PageReferenceObj = NavigateToListView();
            return PageReferenceObj;
        }        
    }
    
    public Static pagereference NavigateToListView(){
        //method to return the pagereference of the apex page
        //the page show message or listview based on the current logged in user's group member ship
        return new PageReference('/apex/AppointmentDetails');
    }
    
    @RemoteAction
    public static AttachmentDetails getAttachmentDetails(String paramAttachmentId) {
        Attachment AttachmentObj = [SELECT Id, Body,Name, Parent.Name 
                                    FROM Attachment 
                                    WHERE Id=: paramAttachmentId ];
        
        AttachmentDetails AttachmentDetailsToReturn = new AttachmentDetails( AttachmentObj );
               
        return AttachmentDetailsToReturn;
    }
    
    public class AttachmentDetails{
        public String AttachmentName;
        public String AttachmentParentName;
        public String AttachmentBody;
        public String AttachmentId;
        
        public AttachmentDetails(Attachment paramAttachment){
            AttachmentName = paramAttachment.Name;
            AttachmentParentName = paramAttachment.Parent.Name;
            AttachmentBody = EncodingUtil.base64Encode( paramAttachment.Body );
            AttachmentId = paramAttachment.Id;
        }
    }
    
    public List<Appointment__c> newDelayedMedicalResultsList {get; set;}
    public boolean showDelayedList {get; set;}
    
    void callDelayedMedicalResults(){
        delayedMedicalResults delayed = new delayedMedicalResults(new ApexPages.StandardController(new Appointment__c()));
        newDelayedMedicalResultsList = delayed.delayedMedicalResultsList ;
        showDelayedList = true;
        
    }
}