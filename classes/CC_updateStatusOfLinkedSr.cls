/*
    Author      : Parteek akdkol
    Date        : 6-MAY-2020
    Description : Custom code to upadte status of linked srs
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_updateStatusOfLinkedSr implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR,HexaBPM__Step__c stp) {
        string strResult = 'Success';
        HexaBPM__Step__c step = new HexaBPM__Step__c(Id=stp.Id);
        HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__c);
        for(HexaBPM__Step_Transition__c stptran : [select HexaBPM__SR_Status_Internal__c,HexaBPM__SR_Status_External__c,HexaBPM__Transition__r.HexaBPM__To__c,HexaBPM__Transition__r.HexaBPM__From__c
         from HexaBPM__Step_Transition__c where HexaBPM__Transition__c!=null and HexaBPM__SR_Step__c=:stp.HexaBPM__SR_Step__c and HexaBPM__Transition__r.HexaBPM__To__c=:stp.HexaBPM__Status__c limit 1
        ]){
            system.debug('traaaan' + stptran);
            if(stptran.HexaBPM__SR_Status_Internal__c!=null)
              objSR.HexaBPM__Internal_SR_Status__c = stptran.HexaBPM__SR_Status_Internal__c;
            if(stptran.HexaBPM__SR_Status_External__c!=null)
              objSR.HexaBPM__External_SR_Status__c = stptran.HexaBPM__SR_Status_External__c;
            break;
        }
        try{
            system.debug('status getting' + stp.HexaBPM__Status__c);
            system.debug('stepper' + stp.HexaBPM__SR_Step__c);
            system.debug('status to fit' + objSR.HexaBPM__Internal_SR_Status__c);
            list<HexaBPM__Service_Request__c> srObjToUPdate = new list<HexaBPM__Service_Request__c>();
            for(HexaBPM__Service_Request__c srObj : [select id from HexaBPM__Service_Request__c WHERE HexaBPM__Linked_SR__c =:stp.HexaBPM__SR__c ]){

                if(objSR.HexaBPM__Internal_SR_Status__c!=null)
                srObj.HexaBPM__Internal_SR_Status__c = objSR.HexaBPM__Internal_SR_Status__c;

                if(objSR.HexaBPM__External_SR_Status__c!=null)
                srObj.HexaBPM__External_SR_Status__c = objSR.HexaBPM__External_SR_Status__c;

                srObjToUPdate.add(srObj);
            }

            system.debug('srObjToUPdate----<' + srObjToUPdate);
            if(srObjToUPdate.size() > 0){
                update srObjToUPdate;
            }

        }catch(DMLException e){
            //strResult = e.getmessage()+'';
            string DMLError = e.getdmlMessage(0)+'';
            if(DMLError==null){
                DMLError = e.getMessage()+'';
            }
            strResult = DMLError;
            
        }
        return strResult;
    }
}