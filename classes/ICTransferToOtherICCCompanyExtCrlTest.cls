@isTest
public class ICTransferToOtherICCCompanyExtCrlTest {
public static testMethod void TestMethod_1(){
    Id portalRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
    Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;
    Account accountObj = new Account ();
    accountObj.Name='Test Public Account Limited';
    accountObj.Trade_Name__c = 'Test Public Account Limited';
    Insert accountObj;
    
    Contact con = new Contact(FirstName='Test', LastName='Community Contact', Email='test@gmail.com', accountId=accountObj.Id, RecordTypeId=portalRecordTypeId);  
    insert con;  
                
    User user = new User(alias = 'test123', email='test123@noemail.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p, country='United States',IsActive =true,
            ContactId = con.Id,
            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
    
    insert user;
    
    Service_request__c servicerequestObj = new Service_request__c (); 
    servicerequestObj.Date_of_Resolution__c=System.Today();
    servicerequestObj.Entity_Name__c='Test Public Account Limited';
    servicerequestObj.Proposed_Trading_Name_1__c='Test Public Account Limited';
    servicerequestObj.RecordTypeId=Schema.SObjectType.Service_request__c.getRecordTypeInfosByDeveloperName().get('IC_transfer_to_other_ICC_Company').getRecordTypeId();
    
    insert servicerequestObj;
    
    Shareholder_Detail__c ShareholderObj = new Shareholder_Detail__c();
    ShareholderObj.Account__c=accountObj.id;
    PageReference pageRef = Page.ICTransferToOtherICCCompany;
    
    
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('RecordType',servicerequestObj.RecordTypeId);
    
    Test.Starttest();
    
    Apexpages.StandardController sc = new Apexpages.StandardController(servicerequestObj);
    ICTransferToOtherICCCompanyExtCrl ext = new  ICTransferToOtherICCCompanyExtCrl(sc);         
    
    ext.IsSuffixAvailable =true;
    ext.IsSuffixAvailableTrade =true;
    ext.selectedSuffix='PLC';
    ext.selectedSuffixTrade='PLC';
    ext.SaveRecord();
    
    ext.selectedCompanyId=accountObj.id;
    ext.setICCCompanydetails();
    Test.StopTest();
    
}
public static testMethod void TestMethod_2(){
    Id portalRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
    Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;
    Account accountObj = new Account ();
    accountObj.Name='Test Public Account Limited';
    accountObj.Trade_Name__c = 'Test Public Account Limited';
    Insert accountObj;
    
    Contact con = new Contact(FirstName='Test', LastName='Community Contact', Email='test@gmail.com', accountId=accountObj.Id, RecordTypeId=portalRecordTypeId);  
    insert con;  
                
    User user = new User(alias = 'test123', email='test123@noemail.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p, country='United States',IsActive =true,
            ContactId = con.Id,
            timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
    
    insert user;
    
    Service_request__c servicerequestObj = new Service_request__c (); 
    servicerequestObj.Date_of_Resolution__c=System.Today();
    servicerequestObj.RecordTypeId=Schema.SObjectType.Service_request__c.getRecordTypeInfosByDeveloperName().get('IC_transfer_to_other_ICC_Company').getRecordTypeId();
    
    insert servicerequestObj;

    PageReference pageRef = Page.ICTransferToOtherICCCompany;
    
    
    Test.setCurrentPage(pageRef);
    pageRef.getParameters().put('RecordType',servicerequestObj.RecordTypeId);
    pageRef.getParameters().put('type','IC_transfer_to_other_ICC_Company');
    Test.Starttest();
    
    Apexpages.StandardController sc = new Apexpages.StandardController(servicerequestObj);
    ICTransferToOtherICCCompanyExtCrl ext = new  ICTransferToOtherICCCompanyExtCrl(sc);         
    System.runAs(user){
        ext.PopulateInitialData();
    }
    ext.SaveRecord();

    Test.StopTest();
    
}
public static testMethod void TestMethod_3(){
    
    Service_request__C ServicerequestObj = new Service_request__C (); 

    PageReference pageRef = Page.ICTransferToOtherICCCompany;
    
    Test.setCurrentPage(pageRef);
    
    Apexpages.StandardController sc = new Apexpages.StandardController(ServicerequestObj);
    ICTransferToOtherICCCompanyExtCrl ext = new  ICTransferToOtherICCCompanyExtCrl(sc);         
    
}
}