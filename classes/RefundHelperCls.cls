/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 09/24/2020
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    15/12/2019  shoaib    Created
********/
public with sharing class RefundHelperCls {
 /**
     * Helper method to check zero refund
     * @param  searchValue,searchBy. 
     * @return List of tasks.          
     */
    public static boolean zeroRufundAmount(string srID){
     
        Step__c lineManagerStep = [SELECT Id,
                                        Refund_Amount__c 
                                        FROM Step__c 
                                        WHERE SR__c =:SrId 
                                        AND Step_Name__c ='GS Line Manager Approval' order by CreatedDate LIMIT 1 ];
        
      
        if(Integer.valueOf(lineManagerStep.refund_amount__c) > 0){
           return true;
        }
        return false;
     }
    /**
     * Helper method to change the status of SR If HOD rejects the step
     * @param  searchValue,searchBy. 
     * @return List of tasks.          
     */
    @future(callout=true) 
    public static void HodRejectionAction(string srID){
        Service_Request__c thisSr       = [SELECT Id,External_SR_Status__c,Internal_SR_Status__c FROM Service_Request__c WHERE ID =:srID];
        thisSr.External_SR_Status__c    = System.Label.Cancellation_Pending;
        thisSr.Internal_SR_Status__c    = System.Label.Cancellation_Pending;
        Update thisSr;

    }

     /**
     * Helper method to to check th refund amount 
     * @param  searchValue,searchBy. 
     * @return List of tasks.          
     */

    @future(callout=true)   
    public static void HodPreCheckRefundHelper(String SRId,String stpId,String stepName){
        Service_request__c srObj = new Service_Request__c();

        for(Service_Request__c sr :[select id,Name,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,record_Type_Name__c,
                                            Submitted_Date__c,Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                            Contact__r.BP_No__c,SAP_MATNR__c,
                                            (select id,Name,Status__c,Step_Name__c,Status__r.Name from Steps_SR__r)
                                            from service_request__c where id=:SRId]){
            srObj = sr;                                             
        }  
      
        SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP refundResp        = RefundRequestUtils.PushToSAP(srObj,'RFCK');
        List<SAPPortalBalanceRefundService.ZSF_S_REFUND_OP> respItemList = refundResp.item;

        List<Log__c>  lstLogs = new list<log__c>();
        for(SAPPortalBalanceRefundService.ZSF_S_REFUND_OP resp : respItemList){
         
            if(resp.RFAMT !=null || Test.isRunningTest()){
                // if(stepName == 'HOD Review'){
                //     string ProductId,PriceLineId;
                //     for(Pricing_Line__c objPL : [select Id,Pricing_Line__c.Product__c from Pricing_Line__c where Product__r.Name = 'Refund' ]){
                //         ProductId = objPL.Product__c;
                //         PriceLineId = objPL.Id;
                //     }
                //     SR_Price_Item__c objPriceItem                       = new SR_Price_Item__c(ServiceRequest__c=srObj.Id);
                //     objPriceItem.Price__c                               =  decimal.valueOf(resp.RFAMT); 
                //     objPriceItem.Unique_SR_PriceItem__c                 = 'Cancellation_Refund_'+srObj.Id;
                //     objPriceItem.Status__c                              = 'Added';
                //     objPriceItem.Product__c                             = ProductId;
                //     objPriceItem.Pricing_Line__c                        = PriceLineId;
                //     upsert objPriceItem Unique_SR_PriceItem__c;
                // }
        
                Step__c thisStep                    = new Step__c(Id = stpId);
                thisStep.refund_amount__c           = decimal.valueOf(resp.RFAMT); 
                Update thisStep;
           
            }else if(resp.RSTAT == 'E' && resp.SFMSG !='No Service Refund' && resp.SFMSG !='Not Applicable'){
                Log__c objLog                       = new Log__c();
                objLog.Type__c                      = 'HOD Cancellatio Pre check failed#'+resp.RENUM ;
                objLog.Description__c               = resp.SFMSG;
                lstLogs.add(objLog);           
            }     
        }
             if(lstLogs !=null && lstLogs.size()>0){
                insert  lstLogs;
            
        }  
    } 

    /**
     * Helper method to change the status of SR If HOD rejects the step
     * @param  searchValue,searchBy. 
     * @return List of tasks.          
     */
    @future(callout=true) 
    public static void LineManagerStepSRAction(string srID){
        Service_Request__c thisSr       = [SELECT Id,External_SR_Status__c,Internal_SR_Status__c FROM Service_Request__c WHERE ID =:srID];
        thisSr.External_SR_Status__c    = System.Label.Line_Manager_Rejection;
        thisSr.Internal_SR_Status__c    = System.Label.Line_Manager_Rejection;
        Update thisSr;

    }
    
     /**
     * Helper method to change the status of SR If HOD rejects the step
     * @param  searchValue,searchBy. 
     * @return List of tasks.          
     */
    @future(callout=true) 
    public static void HodRejection(string srID){
        Service_Request__c thisSr       = [SELECT Id,External_SR_Status__c,Internal_SR_Status__c FROM Service_Request__c WHERE ID =:srID];
        thisSr.External_SR_Status__c    = System.Label.Cancellation_Pending;
        thisSr.Internal_SR_Status__c    = System.Label.Cancellation_Pending;
        Update thisSr;
    }
    
    public static void RejectionStatus(string srID){
        Service_Request__c thisSr       = [SELECT Id,External_SR_Status__c,Internal_SR_Status__c FROM Service_Request__c WHERE ID =:srID];
        thisSr.External_SR_Status__c    = System.Label.Cancellation_Pending;
        thisSr.Internal_SR_Status__c    = System.Label.Cancellation_Pending;
        thisSr.Send_SMS_To_Mobile__c = '+97152123456';
        thisSr.Email__c = 'testclass@difc.ae.test';
        thisSr.Type_of_Request__c = 'Applicant Outside UAE';
        thisSr.Port_of_Entry__c = 'Dubai International Airport';
        thisSr.Title__c = 'Mr.';
        thisSr.First_Name__c = 'India';
        thisSr.Last_Name__c = 'Hyderabad';
        thisSr.Middle_Name__c = 'Andhra';
        thisSr.Nationality_list__c = 'India';
        thisSr.Previous_Nationality__c = 'India';
        thisSr.Qualification__c = 'B.A. LAW';
        thisSr.Gender__c = 'Male';
        Update thisSr;

    }
    
     public static void updateStatus(string srID){
        Service_Request__c thisSr       = [SELECT Id,External_SR_Status__c,Internal_SR_Status__c FROM Service_Request__c WHERE ID =:srID];
        thisSr.External_SR_Status__c    = System.Label.Cancellation_Pending;
        thisSr.Internal_SR_Status__c    = System.Label.Cancellation_Pending;
        thisSr.Send_SMS_To_Mobile__c = '+97152123456';
        thisSr.Email__c = 'testclass@difc.ae.test';
        thisSr.Type_of_Request__c = 'Applicant Outside UAE';
        thisSr.Port_of_Entry__c = 'Dubai International Airport';
        thisSr.Title__c = 'Mr.';
        thisSr.First_Name__c = 'India';
        thisSr.Last_Name__c = 'Hyderabad';
        thisSr.Middle_Name__c = 'Andhra';
        thisSr.Nationality_list__c = 'India';
        thisSr.Previous_Nationality__c = 'India';
        thisSr.Qualification__c = 'B.A. LAW';
        thisSr.Gender__c = 'Male';
        Update thisSr;

    }
    
     /**
     * Helper method to change the status of SR If HOD rejects the step
     * @param  searchValue,searchBy. 
     * @return List of tasks.          
     */
    @future(callout=true) 
    public static void AcceptedStatus(string srID){
        Service_Request__c thisSr       = [SELECT Id,External_SR_Status__c,Internal_SR_Status__c FROM Service_Request__c WHERE ID =:srID];
        thisSr.External_SR_Status__c    = System.Label.Cancellation_Pending;
        thisSr.Internal_SR_Status__c    = System.Label.Cancellation_Pending;
        thisSr.Send_SMS_To_Mobile__c = '+97152123456';
        thisSr.Email__c = 'testclass@difc.ae.test';
        thisSr.Type_of_Request__c = 'Applicant Outside UAE';
        thisSr.Port_of_Entry__c = 'Dubai International Airport';
        thisSr.Title__c = 'Mr.';
        thisSr.First_Name__c = 'India';
        thisSr.Last_Name__c = 'Hyderabad';
        thisSr.Middle_Name__c = 'Andhra';
        thisSr.Nationality_list__c = 'India';
        thisSr.Previous_Nationality__c = 'India';
        thisSr.Qualification__c = 'B.A. LAW';
        thisSr.Gender__c = 'Male';
        Update thisSr;

    }
    
}