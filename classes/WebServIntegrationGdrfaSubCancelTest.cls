/******************************************************************************************************
*  Author   : Kumar Utkarsh
*  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* Test Class of WebServIntegrationGdrfaSubCancelClass.
*******************************************************************************************************/
@isTest
public class WebServIntegrationGdrfaSubCancelTest {
    
@testSetup static void testSetupdata(){
        List<Service_Request__c> listSR = new List<Service_Request__c>();
        Service_Request__c testSr = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        Id RecordTypeIdSr = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-Cancellation').getRecordTypeId();
        testSr.RecordTypeId = RecordTypeIdSr;
        testSr.Mobile_Number__c = '';
        testSr.First_Name_Arabic__c = 'كاركيرا';
        testSr.Date_of_Birth__c = Date.newInstance(2001, 10, 9);
        //testSr.Nationality__c = lookUpNationality.Id;
        testSr.Nationality_List__c = 'United Arab Emirates';
        testSr.Previous_Nationality__c = 'United Arab Emirates';
        testSr.Country_of_Birth__c = 'United Arab Emirates';
        //testSr.Country__c = 'United Arab Emirates';
        testSr.Marital_Status__c = 'Single';
        testSr.Religion__c = 'Hindhu';
        testSr.Qualification__c = 'UNIVERSITY DEGREE';
        //testSr.Occupation_GS__c = 'GENERAL MANAGER';
        testSr.Passport_Type__c = 'Normal';
        testSr.Location__c = 'TestAbc';
        testSr.Mother_Full_Name__c = 'abc';
        testSr.Mother_s_full_name_Arabic__c = 'كاركيرا';
        testSr.Middle_Name_Arabic__c = 'كاركيرا';
        testSr.Establishment_Card_No__c = '123456';
        testSr.Passport_Place_of_Issue__c = 'United Arab Emirates';
        testSr.Residence_Visa_No__c = '123';
        //testSr.Building__c = 'Test';
        testSr.Emirates_Id_Number__c = '123';
        testSr.Area__c = 'Test';
        testSr.Street_Address__c = 'Test';
        testSr.Current_Registered_Street__c = 'Test';
        testSr.Sponsor_Mobile_No__c = '+971500000001';
        testSr.Address_Details__c = 'Test';
        testSr.Passport_Country_of_Issue__c = 'United Arab Emirates';
        testSr.Type_of_Request__c = 'Applicant Outside UAE';
        testSr.Identical_Business_Domicile__c = 'United Arab Emirates';
        testSr.City_Town__c = 'Abu Dhabi';
        testSr.Emirate__c = 'Abu Dhabi';
        testSr.City__c = 'Abu Dhabi';
        testSr.Building_Name__c = 'Test';
        testSr.Phone_No_Outside_UAE__c = '+971000000001';
        testSr.Last_Name_Arabic__c = 'كاركيرا';
        testSr.Place_of_Birth__c = 'Dubai';
        testSr.Visa_Duration__c = '3';
        listSR.add(testSr);
        
        Service_Request__c testSr1 = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        //Id RecordTypeIdSr1 = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-New').getRecordTypeId();
        testSr1.RecordTypeId = RecordTypeIdSr;
        testSr1.Mobile_Number__c = '';
        testSr1.Visa_Duration__c = '3';
        testSr1.First_Name_Arabic__c = 'كاركيرا';
        testSr1.Date_of_Birth__c = Date.newInstance(2001, 10, 9);
        //testSr1.Nationality__c = lookUpNationality.Id;
        testSr1.Nationality_List__c = 'United Arab Emirates';
        testSr1.Previous_Nationality__c = 'United Arab Emirates';
        testSr1.Country_of_Birth__c = 'United Arab Emirates';
        //testSr1.Country__c = 'United Arab Emirates';
        testSr1.Marital_Status__c = 'Single';
        testSr1.Religion__c = 'Hindhu';
        testSr1.Qualification__c = 'UNIVERSITY DEGREE';
        //testSr1.Occupation_GS__c = 'GENERAL MANAGER';
        testSr1.Passport_Type__c = 'Normal';
        testSr1.Location__c = 'TestAbc';
        testSr1.Mother_Full_Name__c = 'abc';
        //testSr1.Mother_s_full_name_Arabic__c = 'كاركيرا';
        //testSr1.Middle_Name_Arabic__c = 'كاركيرا';
        testSr1.Middle_Name__c = 'test';
        testSr1.Establishment_Card_No__c = '1234567';
        testSr1.Passport_Place_of_Issue__c = 'United Arab Emirates';
        //testSr1.Building__c = 'Test';
        testSr1.Emirates_Id_Number__c = '123';
        testSr1.Area__c = 'Test';
        testSr1.Street_Address__c = 'Test';
        testSr1.Current_Registered_Street__c = 'Test';
        testSr1.Sponsor_Mobile_No__c = '+971500000001';
        testSr1.Address_Details__c = 'Test';
        testSr1.Passport_Country_of_Issue__c = 'United Arab Emirates';
        testSr1.Type_of_Request__c = 'Applicant Outside UAE';
        testSr1.Identical_Business_Domicile__c = 'United Arab Emirates';
        testSr1.City_Town__c = 'Abu Dhabi';
        testSr1.Emirate__c = 'Abu Dhabi';
        testSr1.City__c = 'Abu Dhabi';
        testSr1.Building_Name__c = 'Test';
        testSr1.Phone_No_Outside_UAE__c = '+971000000001';
        testSr1.Last_Name_Arabic__c = 'كاركيرا';
        testSr1.Place_of_Birth__c = 'Dubai';
        listSR.add(testSr1);
        insert listSR;
    
        SR_Doc__c srDoc = new SR_Doc__c();
        srDoc.Service_Request__c = testSr.Id;
        srDoc.Name = 'Coloured Photo';
        //srDoc.Doc_ID__c = attch.Id;
        //srDoc.GDRFA_Parent_Document_Id__c = '50f41f94';
        insert srDoc;
        
        Attachment attch = new Attachment();
        attch.Name = 'Test attch.pdf';
        attch.Body = Blob.valueOf('Unit Test Attachment Body');
        attch.ParentId = srDoc.Id;
        attch.ContentType = 'image/jpg';
        insert attch;
    
        List<Step_Template__c> stepTemplateList = new List<Step_Template__c>();
        Step_Template__c stepTemplate = new Step_Template__c();
        stepTemplate.Name = 'Cancellation Form Typed';
        stepTemplate.Code__c = 'Cancellation Form Typed';
        stepTemplate.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate);
    
        Step_Template__c stepTemplate1 = new Step_Template__c();
        stepTemplate1.Name = 'Entry Permit Form is Typed';
        stepTemplate1.Code__c = 'Entry Permit Form is Typed';
        stepTemplate1.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate1);
        insert stepTemplateList;
            
        Status__c status = new Status__c();
        status.Code__c = 'AWAITING_REVIEW';
        status.Name = 'Awaiting Review';
        insert status;
        
        Status__c statusClosed = new Status__c();
        statusClosed.Code__c = 'CLOSED';
        statusClosed.Name = 'Closed';
        insert statusClosed;
        
        Status__c statusGSReview = new Status__c();
        statusGSReview.Code__c = 'DNRD_GS_Review';
        statusGSReview.Name = 'DNRD GS Review';
        insert statusGSReview;
        List<Step__c> stepList = new List<Step__c>();
        Step__c stepTest = new Step__c();
        stepTest.Step_Template__c = stepTemplate.Id;
        stepTest.Status__c = status.Id;
        stepTest.SR__c  = testSr.Id;
        stepList.add(stepTest);
        
        Step__c stepTest1 = new Step__c();
        stepTest1.Step_Template__c = stepTemplate.Id;
        stepTest1.Status__c = status.Id;
        stepTest1.SR__c  = testSr1.Id;
        stepList.add(stepTest1);
    
        Step__c stepTest2 = new Step__c();
        stepTest2.Step_Template__c = stepTemplate1.Id;
        stepTest2.Status__c = statusClosed.Id;
        stepTest2.SR__c  = testSr.Id;
        stepList.add(stepTest2);
        insert stepList;
        /*Step__c stepTest3 = new Step__c();
        stepTest3.Step_Template__c = stepTemplate1.Id;
        stepTest3.Status__c = statusClosed.Id;
        stepTest3.SR__c  = testSr1.Id;
        insert stepTest3;*/
        List<GDRFA_Master_Data__c> gdDataList = new List<GDRFA_Master_Data__c>();
        GDRFA_Master_Data__c lookUpData1 = new GDRFA_Master_Data__c();
        lookUpData1.Lookup_Type__c = 'Emirates';
        lookUpData1.Name_English__c = 'Abu Dhabi';
        lookUpData1.DIFC_Parent_City__c = 'Abu Dhabi';
        lookUpData1.DIFC_Parent_Emirate__c = 'Abu Dhabi';
        lookUpData1.Look_Up_Id__c = '123';
        lookUpData1.GDRFA_Unique_Value__c = 'Emirates + 123';
        gdDataList.add(lookUpData1);
        
        GDRFA_Master_Data__c lookUpData2 = new GDRFA_Master_Data__c();
        lookUpData2.Lookup_Type__c = 'Areas';
        lookUpData2.Name_English__c = 'Test';
        lookUpData2.DIFC_Name__c = 'Test';
        lookUpData2.DIFC_Parent_City__c = 'Abu Dhabi';
        lookUpData2.DIFC_Parent_Emirate__c = 'Abu Dhabi';
        lookUpData2.Look_Up_Id__c = '1';
        lookUpData2.GDRFA_Unique_Value__c = 'Areas + 1';
        gdDataList.add(lookUpData2);
        
        GDRFA_Master_Data__c lookUpData3 = new GDRFA_Master_Data__c();
        lookUpData3.Lookup_Type__c = 'Country';
        lookUpData3.Name_English__c = 'United Arab Emirates';
        lookUpData3.DIFC_Name__c = 'United Arab Emirates';
        lookUpData3.DIFC_Parent_City__c = 'United Arab Emirates';
        lookUpData3.DIFC_Parent_Emirate__c = 'United Arab Emirates';
        lookUpData3.Look_Up_Id__c = '26872';
        lookUpData3.GDRFA_Unique_Value__c = 'Country + 26872';
        gdDataList.add(lookUpData3);
        insert gdDataList;
    
        
    }
    
    static testMethod void webServiceGdrfaSubCancelTest1(){
        List<Id> SrIds = new List<Id>();
        List<Service_Request__c> SrTest = [SELECT Id, Service_Type__c, Nationality__c, Previous_Nationality__c, Country_of_Birth__c, Gender__c, Marital_Status__c, Religion__c, Qualification__c, Occupation__c, Passport_Type__c,
                                           First_Name__c, First_Name_Arabic__c, Middle_Name__c, Middle_Name_Arabic__c, Location__c, Last_Name__c, Last_Name_Arabic__c, Mother_Full_Name__c, Establishment_Card_No__c,
                                           Mother_s_full_name_Arabic__c, Date_of_Birth__c, Place_of_Birth__c, Place_of_Birth_Arabic__c, First_Language__c, Passport_Number__c, Passport_Date_of_Issue__c, Passport_Date_of_Expiry__c,
                                           Passport_Place_of_Issue__c, Building__c, Emirates_Id_Number__c, Area__c, Street_Address__c, Current_Registered_Street__c, Mobile_Number__c, Sponsor_Mobile_No__c, Address_Details__c,
                                           Passport_Country_of_Issue__c, Type_of_Request__c, Identical_Business_Domicile__c, City_Town__c, Emirate__c, City__c, Building_Name__c, Phone_No_Outside_UAE__c, Visa_Duration__c
                                           FROM Service_Request__c WHERE Establishment_Card_No__c = '123456'];
        for(Service_Request__c Sr: SrTest){
            SrIds.add(Sr.Id);
        }
        Test.setMock(HttpCalloutMock.class, new webServiceGdrfaLoginMock());
        Test.startTest();
        WebServIntegrationGdrfaSubCancelClass.invokeSubmitCancel(SrIds);
        Test.stopTest();
    }
    
    
    static testMethod void webServiceGdrfaAppStatusTest2(){
        List<Id> SrIds = new List<Id>();
        List<Service_Request__c> SrTest = [SELECT Id, Service_Type__c, Nationality__c, Previous_Nationality__c, Country_of_Birth__c, Gender__c, Marital_Status__c, Religion__c, Qualification__c, Occupation__c, Passport_Type__c,
                                           First_Name__c, First_Name_Arabic__c, Middle_Name__c, Middle_Name_Arabic__c, Location__c, Last_Name__c, Last_Name_Arabic__c, Mother_Full_Name__c, Establishment_Card_No__c,
                                           Mother_s_full_name_Arabic__c, Date_of_Birth__c, Place_of_Birth__c, Place_of_Birth_Arabic__c, First_Language__c, Passport_Number__c, Passport_Date_of_Issue__c, Passport_Date_of_Expiry__c,
                                           Passport_Place_of_Issue__c, Building__c, Emirates_Id_Number__c, Area__c, Street_Address__c, Current_Registered_Street__c, Mobile_Number__c, Sponsor_Mobile_No__c, Address_Details__c,
                                           Passport_Country_of_Issue__c, Type_of_Request__c, Identical_Business_Domicile__c, City_Town__c, Emirate__c, City__c, Building_Name__c, Phone_No_Outside_UAE__c, Visa_Duration__c
                                           FROM Service_Request__c WHERE Establishment_Card_No__c = '1234567'];
        for(Service_Request__c Sr: SrTest){
            SrIds.add(Sr.Id);
        }
        Test.setMock(HttpCalloutMock.class, new webServiceGdrfaLoginMockTest());
        Test.startTest();
        WebServIntegrationGdrfaSubCancelClass.invokeSubmitCancel(SrIds);
        Test.stopTest();
    }

}