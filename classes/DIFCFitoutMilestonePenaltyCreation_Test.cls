@isTest
public class DIFCFitoutMilestonePenaltyCreation_Test {
	public static testMethod void testDIFCFitoutMilestonePenaltyCreation() 
    {
    	Fitout_Milestone_Penalty__c fitoutPenSumRec 			= 	new Fitout_Milestone_Penalty__c();
        fitoutPenSumRec.Unit_Handover_Date__c 					= 	system.today();
        fitoutPenSumRec.Additional_Rent_Free_Period_Days__c		= 	1;
        fitoutPenSumRec.Additional_Tenant_email__c				=	'mudasir.saleem@gmail.com';
        fitoutPenSumRec.Next_Conceptual_Penalty_Date__c			=	system.today();
        fitoutPenSumRec.Next_Detail_Design_Penalty_Date__c		=	system.today();
        fitoutPenSumRec.Next_First_Inspection_Penalty_Date__c	=	system.today();
        fitoutPenSumRec.Next_Final_Inspection_Penalty_Date__c	=	system.today();
        insert fitoutPenSumRec;
        Test.startTest();
        	DIFCFitoutMilestonePenaltyCreationBatch abs= new DIFCFitoutMilestonePenaltyCreationBatch();
	    	String cronExpr = '0 0 0 15 3 ? 2099';
			String jobId = System.schedule('myJobTestJobName', cronExpr, abs);
        	//abs.createLogRecord(Database.Error err)
        Test.stopTest();
    }
    
}