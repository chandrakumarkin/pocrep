@IsTest
public class TrainingCancellationControllerTest {
    static testMethod void validateTrainingCancellation(){
        Training_Master__c trainingMaster = new Training_Master__c();
        trainingMaster.Name = 'Employee Service training';
        trainingMaster.Category__c = 'Employee Services';
        trainingMaster.Available_Seats__c = 10;
        trainingMaster.Status__c = 'Approved by HOD';
        trainingMaster.Start_DateTime__c = DateTime.now().addDays(1);
        trainingMaster.To_DateTime__c = DateTime.now().addDays(2);
        insert trainingMaster;
        
        //inserting training request.
        Training_Request__c trObj = new Training_Request__c();
        trObj.First_Name__c = 'Sam';
        trObj.Last_Name__c = 'S';
        trObj.TrainingMaster__c = trainingMaster.id;
        trObj.Status__c = 'Submitted';
        insert trObj;
        
        PageReference pageRef = Page.TrainingCancellation;
        pageRef.getParameters().put('id', String.valueOf(trainingMaster.Id));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(trainingMaster);
        TrainingCancellationController ctrl = new TrainingCancellationController(sc);//Instantiate the Class
        ctrl.cancelTraining();
        ctrl.cancel();
    }

}