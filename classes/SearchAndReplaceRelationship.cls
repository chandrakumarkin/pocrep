global class SearchAndReplaceRelationship implements Database.Batchable < sObject > {
global final String Query;
    global List<sObject> passedRecords= new List<sObject>();

global SearchAndReplaceRelationship(String q,List<sObject> listofRecords) {
Query = q;
        passedRecords = listofRecords;

}
global Database.QueryLocator start(Database.BatchableContext BC) {
 if(query != '')return Database.getQueryLocator(query);
  else return DataBase.getQueryLocator([Select id,Contact_Email__c ,Contact_Phone__c ,Contact_Fax__c from Relationship__c WHERE Id IN: passedRecords and Id != null]);
}
global void execute(Database.BatchableContext BC, List < Relationship__c > scope) {
for (Relationship__c ObjUser: scope) {
ObjUser.Contact_Email__c = ObjUser.Contact_Email__c != NULl ?ObjUser.Contact_Email__c+'.invalid' : 'Test@gmail.com.Invalid';
ObjUser.Contact_Phone__c = ObjUser.Contact_Phone__c != NULl ? '+971569803616': '+971569803616';
ObjUser.Contact_Fax__c  = ObjUser.Contact_Fax__c  != NULl ? '+971569803616': '+971569803616';
}
system.debug('scope---'+scope);
        Database.SaveResult[] srList = Database.update(scope, false);
        List<Log__c> logRecords = new List<Log__c>();
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                for(Database.Error err : sr.getErrors()) {
                    string errorresult = err.getMessage();
                    logRecords.add(new Log__c(
                      Type__c = 'SearchAndReplaceRelationship',
                      Description__c = 'Exception is : '+errorresult+'=relationship Id==>'+sr.getId()
                    ));
                    System.debug('The following error has occurred.');                    
                }
            }
        }
        if(logRecords.size()>0)insert logRecords;
}

global void finish(Database.BatchableContext BC) {}
}