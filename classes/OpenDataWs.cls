/*********************************************************************************************************************
*  Name     : OpenDataWs
*  Author     : Arun Singh
*  Purpose   : Created for DEWs
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
V1.0          Arun                   06-May-2020            Removd ROC status =active 

**********************************************************************************************************************/


public class OpenDataWs
{
    
    public static string OpenDataWsDataResult(String accID)
    {
        compnaies Objcompnaies=new compnaies();
        List<Company> ListCompany=new List<Company>();
        
         List<Individual> Individuals=new List<Individual>(); 
         List<BodyCorporate> BodyCorporates=new List<BodyCorporate>(); 
         List<License_Activity> LicenseActivities=new List<License_Activity>(); 
        
          List<string> RelCode=new List<string>();
            //RelCode.add('Is Shareholder Of');
            //RelCode.add('Has Director');
            
            RelCode.add('Shareholder');
            RelCode.add('Director');
            RelCode.add('Authorized Signatory');
          //Company Data
//ROC_Status__c='Active' and 1.0 
          
        for(Account eachAccount:[select (select Activity_ID_18__c,Activity_Name_Eng__c,Start_Date__c,End_Date__c from  License_Activities__r),Legal_Entity_Type__c, Name,Trade_Name__c,Office__c,Building_Name__c,Country__c,City__c,Emirate__c,PO_Box__c,Place_of_Registration__c,ROC_reg_incorp_Date__c,Registration_License_No__c,DFSA_Lic_Appr_Date__c,AML_Rating__c,ROC_Status__c,Financial_Year_End__c,Legal_Type_of_Entity__c,License_Type__c,DNFBP__c,Account_ID_18_Report__c from account where   id=:accID])
        {
         
            Company ObjCompany= new Company();
            ObjCompany.Company_Name=eachAccount.Name ;
            ObjCompany.Trading_Name=eachAccount.Trade_Name__c ;
            ObjCompany.Office_Address_Line_1=eachAccount.Office__c ;
            ObjCompany.Office_Address_Line_2=eachAccount.Building_Name__c ;
            ObjCompany.Office_Country=eachAccount.Country__c;
            ObjCompany.Office_City_Town=eachAccount.City__c ;
            ObjCompany.Office_State_Province=eachAccount.Emirate__c ;
            ObjCompany.Office_Po_Box_Postal_Code=eachAccount.PO_Box__c ;
            ObjCompany.Principal_Place_Of_Business='United Arab Emirates';
            //ObjCompany.Principal_Place_Of_Business=eachAccount. ;
            ObjCompany.Country_Of_Incorporation=eachAccount.Place_of_Registration__c ;
            ObjCompany.Date_Company_Incorporated=eachAccount.ROC_reg_incorp_Date__c ;
            ObjCompany.Registration_Number=eachAccount.Registration_License_No__c;
            ObjCompany.DFSA_DNFBP_Approval_Date=eachAccount.DFSA_Lic_Appr_Date__c;
            ObjCompany.Company_Risk_Rating=eachAccount.AML_Rating__c;
            ObjCompany.Company_Status=eachAccount.ROC_Status__c; 
            ObjCompany.Company_Financial_Year_End=eachAccount.Financial_Year_End__c; 
            ObjCompany.Legal_Structure=eachAccount.Legal_Type_of_Entity__c ;
            ObjCompany.Legal_Structure_full=eachAccount.Legal_Entity_Type__c;
            ObjCompany.Type_Of_Licence=eachAccount.License_Type__c; 
            ObjCompany.DNFBP=eachAccount.DNFBP__c ; 
            ObjCompany.Account_ID_18_Report=eachAccount.Account_ID_18_Report__c; 
            
            
            
        //Individuals
        for(Relationship__c ObjRel:[select Relationship_Type__c,System_Relationship_Type__c,Email__c,Primary__c,Individual_First_Name__c,Individual_Last_Name__c,Birth_date__c,Relationship_Nationality__c,Country__c,Gender__c,Account_ID_18__c,Relationship_ID_18__c,Active__c,Relationship_Type_formula__c,Object_Contact__r.Emirate_State_Province__c,Object_Contact__r.PO_Box_HC__c,Office_Unit_Number__c,Street__c,City__c from Relationship__c where Active__c=true and Object_Contact__c!=null and Subject_Account__c=:eachAccount.id and System_Relationship_Type__c in :RelCode])
        {
            if(ObjRel.Relationship_Type__c!='Is Authorized Signatory for')
            {
                Individual ObjIndividual=SetIndividualDetails(ObjRel);
                Individuals.add(ObjIndividual);
            
            }
            else if(ObjRel.Relationship_Type__c=='Is Authorized Signatory for' && ObjRel.Primary__c==true)
            {
                Individual ObjIndividual=SetIndividualDetails(ObjRel);
                Individuals.add(ObjIndividual);
                
            }
            
            
        }
        
        
        
        //BodyCorporates
        for(Relationship__c ObjRel:[select Relationship_Type__c,System_Relationship_Type__c,Relationship_Name__c,Primary__c,Object_Account__r.Registration_No__c,BC_Registration_Date__c,Place_of_Registration__c,Office_Unit_Number__c,Street__c,Body_Corporate_Country__c,City__c,Object_Account__r.Emirate__c,Object_Account__r.PO_Box__c,Account_ID_18__c,Relationship_ID_18__c,Active__c,Relationship_Type_formula__c from Relationship__c where Active__c=true and Object_Account__c!=null and Subject_Account__c=:eachAccount.id and System_Relationship_Type__c in :RelCode])
        {
            BodyCorporate ObjBc=SetBodyCorporateDetails(ObjRel);
            BodyCorporates.add(ObjBc);
            
        }
        //Company Activities
        for(License_Activity__c ObjLicense:eachAccount.License_Activities__r)
        {
            License_Activity ObjActivity=new License_Activity();
            ObjActivity.Activity_ID_18=ObjLicense.Activity_ID_18__c;
            ObjActivity.Activity_Name=ObjLicense.Activity_Name_Eng__c;
            ObjActivity.Start_Date=ObjLicense.Start_Date__c;
            ObjActivity.End_Date=ObjLicense.End_Date__c;
            LicenseActivities.add(ObjActivity);
            
        }
        
            ObjCompany.Individuals=Individuals;
            ObjCompany.BodyCorporates=BodyCorporates;
            ObjCompany.LicenseActivities=LicenseActivities;
            ListCompany.add(ObjCompany);
        }
        
        
    
        
        
        Objcompnaies.ListCompany=ListCompany;
        
         
        String finalJson = JSON.serialize(Objcompnaies);  
        system.debug('!!@@@@'+finalJson);
        return finalJson;
        
        
    }
    
    public static Individual SetIndividualDetails(Relationship__c ObjRel)
    {
        Individual ObjIndividual=new Individual();
        if(ObjRel.System_Relationship_Type__c!='Shareholder')//Do no share shaholder email address 
        ObjIndividual.Email=ObjRel.Email__c;
        
        ObjIndividual.First_Name=ObjRel.Individual_First_Name__c;
        ObjIndividual.Last_Name=ObjRel.Individual_Last_Name__c;
        ObjIndividual.Date_Of_Birth=ObjRel.Birth_date__c;
        ObjIndividual.Nationality=ObjRel.Relationship_Nationality__c;
        ObjIndividual.Gender=ObjRel.Gender__c;

        //Relationship Info
        ObjIndividual.Status=ObjRel.Active__c;
        ObjIndividual.Relationship_Type=ObjRel.System_Relationship_Type__c;
        ObjIndividual.Relationship_ID_18=ObjRel.Relationship_ID_18__c;
        ObjIndividual.Primary=ObjRel.Primary__c;
        
        
        // Address Data
        ObjIndividual.Residential_Address_Line_1=ObjRel.Office_Unit_Number__c;
        ObjIndividual.Residential_Address_Line_2=ObjRel.Street__c;
        ObjIndividual.Residential_City_Town=ObjRel.City__c;
        ObjIndividual.Residential_State_Province=ObjRel.Object_Contact__r.Emirate_State_Province__c;
        ObjIndividual.Residential_Po_Box_Postal_Code=ObjRel.Object_Contact__r.PO_Box_HC__c;
        ObjIndividual.Address_Country=ObjRel.Country__c; 
        
        System.debug('==>'+ObjIndividual);  
        return ObjIndividual;

    }
    
    public static BodyCorporate SetBodyCorporateDetails(Relationship__c ObjRel)
    {
        BodyCorporate ObjBodyCorporate=new BodyCorporate();
        
        ObjBodyCorporate.Entity_Name=ObjRel.Relationship_Name__c; 
        ObjBodyCorporate.Registration_Number=ObjRel.Object_Account__r.Registration_No__c; 
        ObjBodyCorporate.Date_Of_Registration=ObjRel.BC_Registration_Date__c; 
        ObjBodyCorporate.Country_Of_Registration =ObjRel.Place_of_Registration__c; 

        //Address Date 
        ObjBodyCorporate.Office_Address_Line_1=ObjRel.Office_Unit_Number__c;
        ObjBodyCorporate.Office_Address_Line_2=ObjRel.Street__c;
        ObjBodyCorporate.Office_Country=ObjRel.Body_Corporate_Country__c;
        ObjBodyCorporate.Office_City_Town=ObjRel.City__c;
        ObjBodyCorporate.Office_State_Province=ObjRel.Object_Account__r.Emirate__c;
        ObjBodyCorporate.Office_Po_Box_Postal_Code=ObjRel.Object_Account__r.PO_Box__c;

        //Relationship Info
        ObjBodyCorporate.Status=ObjRel.Active__c;
        ObjBodyCorporate.Relationship_Type=ObjRel.System_Relationship_Type__c;
        ObjBodyCorporate.Relationship_ID_18=ObjRel.Relationship_ID_18__c;
        ObjBodyCorporate.Primary=ObjRel.Primary__c;
    
        System.debug('==>'+ObjBodyCorporate);   
        return ObjBodyCorporate;
        
    }
    
    
    
    public class compnaies {
         public List<Company> ListCompany;
        
    }
    
    
    
    public class Company  {
        public string Company_Name;
        public string Trading_Name;
        public string Office_Address_Line_1;
        public string Office_Address_Line_2;
        public string  Office_Country;
        public string  Office_City_Town;
        public string  Office_State_Province;
        public string  Office_Po_Box_Postal_Code;
        public string  Principal_Place_Of_Business;
        public string  Country_Of_Incorporation;
        public Date  Date_Company_Incorporated;
        public string  Registration_Number;
        public Date  DFSA_DNFBP_Approval_Date;
        public string  Company_Risk_Rating;
        public string  Company_Status; 
        public string  Company_Financial_Year_End; 
        public string  Legal_Structure;
        public string  Legal_Structure_full; 
        public string  Type_Of_Licence; 
        public string  DNFBP; 
        public string  Account_ID_18_Report; 
        public List<Individual> Individuals; 
        public List<BodyCorporate> BodyCorporates; 
        public List<License_Activity> LicenseActivities; 
    }
    
    
    public class Individual   {
        
        public string  Email; 
        public string  First_Name; 
        public string  Last_Name; 
        public Date  Date_Of_Birth; 
        public string  Nationality; 
        public string  Gender; 
        

        
        // Only for Shareholder 
        public string Residential_Address_Line_1;
        public string Residential_Address_Line_2;
        public string  Residential_City_Town;
        public string  Residential_State_Province;
        public string  Residential_Po_Box_Postal_Code;
        public string  Address_Country; 
        
        //Relationship Info
        public boolean  Status; 
        public string  Relationship_Type; 
        public string  Relationship_ID_18; 
        public boolean Primary;

        
    }
    
    
    public class License_Activity
    {
    
        public string  Activity_Name; 
        public string  Activity_ID_18; 
        public Date  Start_Date; 
        public Date  End_Date; 
        
    }
    public class BodyCorporate   {
        
        public string  Entity_Name; 
        public string  Registration_Number; 
        public Date  Date_Of_Registration; 
        public string  Country_Of_Registration ; 
        
        
        //Address Date 
        public string Office_Address_Line_1;
        public string Office_Address_Line_2;
        public string  Office_Country;
        public string  Office_City_Town;
        public string  Office_State_Province;
        public string  Office_Po_Box_Postal_Code;
        
        //Relationship Info
        public boolean  Status; 
        public string  Relationship_Type; 
        public string  Relationship_ID_18; 
        public boolean Primary;
        
    }
    
    
    
    
   
}