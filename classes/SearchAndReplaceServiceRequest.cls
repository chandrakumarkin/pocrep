global class SearchAndReplaceServiceRequest implements Database.Batchable < sObject > {

    global final String Query;

    global List<sObject> passedRecords= new List<sObject>();

    global SearchAndReplaceServiceRequest(String q,List<sObject> listofRecords) {

        Query = q;
        passedRecords = listofRecords;

    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(query != '')return Database.getQueryLocator(query);
        else return DataBase.getQueryLocator([Select id,Contact_Number_After_Office_Hours__c,Contact_Number_During_Office_Hours__c,Current_Registered_Phone__c,Foreign_Registered_Phone__c,Send_SMS_To_Mobile__c,Mobile_Number__c,Courier_Mobile_Number__c,Phone_No_Outside_UAE__c,Courier_Cell_Phone__c,Residence_Phone_No__c,Mobile_No_Previous_Sponsor__c,Sponsor_Mobile_No__c,Office_Telephone_Previous_Sponsor__c,Sponsor_Office_Telephone__c,P_O_Box_Previous_Sponsor__c,Residence_Telephone_Previous_Sponsor__c,Sponsor_Residence_Telephone__c,Work_Phone__c,Email__c,Email_Address__c ,Additional_Email__c from Service_request__c WHERE Id IN: passedRecords and Id != null]);

    }

    global void execute(Database.BatchableContext BC, List < Service_Request__c > scope){
    
        for (Service_Request__c Sr: scope) {
        
            Sr.Contact_Number_After_Office_Hours__c='+971569803616';
            Sr.Contact_Number_During_Office_Hours__c='+971569803616';
            Sr.Current_Registered_Phone__c='+971569803616';
            Sr.Foreign_Registered_Phone__c='+971569803616';
            Sr.Send_SMS_To_Mobile__c='+971569803616';
            Sr.Mobile_Number__c='+971569803616';
            Sr.Courier_Mobile_Number__c='+971569803616';
            Sr.Phone_No_Outside_UAE__c='+971569803616';
            Sr.Courier_Cell_Phone__c='+971569803616';
            Sr.Residence_Phone_No__c='+971569803616';
            Sr.Mobile_No_Previous_Sponsor__c='+971569803616';
            Sr.Sponsor_Mobile_No__c='+971569803616';
            Sr.Office_Telephone_Previous_Sponsor__c='+971569803616';
            Sr.Sponsor_Office_Telephone__c='+971569803616';
            Sr.P_O_Box_Previous_Sponsor__c='+971569803616';
            Sr.Residence_Telephone_Previous_Sponsor__c='+971569803616';
            Sr.Sponsor_Residence_Telephone__c='+971569803616';
            Sr.Work_Phone__c='+971569803616';
            Sr.Email__c =Sr.Email__c != NULL ? Sr.Email__c +'.invalid' : 'test@difc.ae.invalid';
            Sr.Email_Address__c = Sr.Email_Address__c != NULL ? Sr.Email_Address__c +'.invalid' : 'test@difc.ae.invalid';
        }
        
        Database.SaveResult[] srList = Database.update(scope, false);
        List<Log__c> logRecords = new List<Log__c>();
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                for(Database.Error err : sr.getErrors()) {
                    string errorresult = err.getMessage();
                    logRecords.add(new Log__c(
                      Type__c = 'SearchAndReplaceServiceRequest',
                      Description__c = 'Exception is : '+errorresult+'=Contact Id==>'+sr.getId()
                    ));
                    System.debug('The following error has occurred.');                    
                }
            }
        }
        if(logRecords.size()>0)insert logRecords;
    }

    global void finish(Database.BatchableContext BC) {}
}