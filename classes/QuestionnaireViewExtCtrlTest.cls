@isTest(SeeAllData=false)
public class QuestionnaireViewExtCtrlTest {
    static testMethod void questionnaireViewMethod() {     
     
     Account acc = new Account();
     acc.Name = 'feedback view account';
     insert acc;
     
     Contact con = new Contact();
     con.FirstName = 'feedback';
     con.LastName = 'Form';
     con.AccountId = acc.id;
     insert con;     
     
     Questionnaire__c ff = new Questionnaire__c();
     ff.Account__c = con.AccountId;    
     ff.Contact__c = con.id;    
     insert ff;
     
     QuestionnaireChild__c qc = new QuestionnaireChild__c();
     qc.Questionnaire__c=ff.id;
     qc.LOB_Country__c=GlobalConstants.LOB_Country_OTHER;
     qc.Country__c='India';
     insert qc;
          
     Test.startTest();
     ApexPages.StandardController controller = new ApexPages.StandardController(ff);
     QuestionnaireViewExtCtrl feedctrl = new QuestionnaireViewExtCtrl(controller);
     PageReference pageRef = Page.QuestionnaireView; // Add your VF page Name here
     pageRef.getParameters().put('cid', con.Id);
     pageRef.getParameters().put('eid', 'aru@sd.com');
     Test.setCurrentPage(pageRef);     
     feedctrl.pageLoad();
     Test.stopTest();
    }  
}