@isTest
public class OB_DeactivateCPSRBatchTest {
    @testSetup
    static  void createTestData() {
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
             
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'Commercial_Permission';
       insert objsrTemp;
       
       list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(2, new List<string> {'Draft','Expired'}, new List<string> {'DRAFT','EXPIRED'}, new List<string> {'End',''});
        insert listSRStatus;
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New Commercial Permission').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.Type_of_commercial_permission__c='Gate Avenue Pocket Shops';
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.HexaBPM__External_SR_Status__c = listSRStatus[0].Id;
        objHexaSR.HexaBPM__Internal_SR_Status__c = listSRStatus[0].Id;
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
    }

    static testMethod void executeBatchTest() {
        list<HexaBPM__SR_Status__c> SRStatusList = [Select id from HexaBPM__SR_Status__c];
        List<HexaBPM__Service_Request__c> srList = [Select id,CreatedDate from HexaBPM__Service_Request__c];
        Datetime SRCreatedDate = Datetime.now().addMonths(-14);
        Test.setCreatedDate(srList[0].Id, SRCreatedDate);  
        test.startTest();
        Database.executeBatch(new OB_DeactivateCPSRBatch());
        String sch ='0 48 * * * ?'; 
        System.schedule('Schedule to update SR', sch,new OB_DeactivateCPSRBatch());
        srList = [Select id,CreatedDate,HexaBPM__Internal_SR_Status__c from HexaBPM__Service_Request__c];
        system.assertEquals(srList[0].HexaBPM__Internal_SR_Status__c,SRStatusList[0].Id);
        test.stopTest();
    }
}