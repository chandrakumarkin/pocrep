/*
 * Lightninh Component: OB_DataProtection
 * Data Protection Screen Logic
 ***/
public without sharing class OB_DataProtectionController {

    //reqest wrpper
    public static OB_DataProtectionController.RequestWrapper reqWrap;
    //response wrpper
    public static OB_DataProtectionController.ResponseWrapper respWrap;

    /*
     * Lightninh Component: OB_DataProtection
     *
     ***/
    @AuraEnabled
    public static OB_DataProtectionController.ResponseWrapper getDataController(String reqWrapPram) {
        //reqest wrpper
        reqWrap = (OB_DataProtectionController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_DataProtectionController.RequestWrapper.class);
        //response wrpper
        respWrap = new OB_DataProtectionController.ResponseWrapper();
        String srId = reqWrap.srId;


        respWrap = getDataControllerRecord(srId, respWrap);

        for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
                                                    AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
            respWrap.ButtonSection = sectionObj;
        }
        return respWrap;
    }


    /*
     * get service request and data controller amendment record
     *
     ****/
    public static OB_DataProtectionController.ResponseWrapper getDataControllerRecord(String srId, ResponseWrapper respWrap) {

        String flowId = reqWrap.flowId;
        String declarationCorporateText = '';
        for(Declarations__mdt declare :[Select ID, Declaration_Text__c, Application_Type__c, Field_API_Name__c, Record_Type_API_Name__c, Object_API_Name__c
                                        From Declarations__mdt
                                        Where Record_Type_API_Name__c = 'Body_Corporate'
                                        AND Field_API_Name__c = 'I_Agree_Data_Controller__c'
                                        AND Object_API_Name__c = 'HexaBPM_Amendment__c']) {


            declarationCorporateText = declare.Declaration_Text__c;

        }


        OB_DataProtectionController.AmendmentWrapper dpoAmendWrap = new OB_DataProtectionController.AmendmentWrapper();
        OB_DataProtectionController.SRWrapper srWrap = new OB_DataProtectionController.SRWrapper();
        List<OB_DataProtectionController.DataProtectionWrapper> jurisdictionList = new List<OB_DataProtectionController.DataProtectionWrapper>();

        if(String.IsNotBlank(srId)) {

            for(HexaBPM__Service_Request__c serObj :[SELECT id, Purpose_of_Notification__c, 
                                                      Transferring_Personal_Data_outside_DIFC__c, Processing_any_sensitive_data__c, 
                                                      Purpose_of_Processing_Data__c, Other_Purpose_of_Processing_Data__c, 
                                                      Data_Subjects_Personal_Data__c, Other_Data_Subjects_Personal_Data__c, 
                                                      Class_of_Personal_Data__c, In_accordance_with_Article_3__c, 
                                                      Category_of_Article_3__c, In_accordance_with_Article_12_11__c, 
                                                      Data_Subject_of_Article_12_11__c, In_accordance_with_Article_13__c, 
                                                      In_accordance_with_Article_14__c, No_Accordance_with_Article_14__c, 
                                                      In_accordance_with_Article_15__c, No_Accordance_with_Article_15__c, 
                                                      In_accordance_with_Article_16_17_18__c, No_Accordance_with_Article_16_17_18__c, 
                                                      In_accordance_with_Article_23_24_25__c, No_Accordance_with_Article_23_24_25__c, 
                                                      In_accordance_with_Article_26__c, Country_Jurisdiction_Article_26__c, 
                                                      In_accordance_with_Article_27__c, No_Accordance_with_Article_27__c, 
                                                      Reason_for_not_processing_Personal_Data__c, Methods_of_Article_40__c, 
                                                      In_accordance_with_Article_40__c, In_accordance_with_Parts_5_or_6__c, 
                                                      In_accordance_with_Parts_7__c, Yes_Accordance_with_Article_27_2_to_4__c, 
                                                      HexaBPM__External_SR_Status__c, HexaBPM__External_SR_Status__r.Name, 
                                                      HexaBPM__Internal_SR_Status__c, HexaBPM__Internal_Status_Name__c
                                                      FROM HexaBPM__Service_Request__c
                                                      WHERE id = :srId
                                                      Limit 1
                ] ) {

                srWrap.srObj = serObj;
                srWrap.declarationCorporateText = declarationCorporateText;

            }


            // set service request status
            /*if(srWrap.srObj != null && srWrap.srObj.Id != null) {
                srWrap.isDraft = OB_QueryUtilityClass.checkSRStatusDraft(srWrap.srObj);
                // if SR status is not draft then redirect to ob-reviewandconfirm lightning page
                if(!srWrap.isDraft) {
                    system.debug('=====flowId====' + flowId);
                    srWrap.viewSRURL = PageFlowControllerHelper.communityURLFormation(srWrap.srObj, flowId, 'ob-reviewandconfirm');
                    system.debug('=====viewSRURL====' + srWrap.viewSRURL);
                }
            } else {
                srWrap.isDraft = false;
            }*/


            //if(srWrap.isDraft) {


                // jurisdiction
                for(Data_Protection__c dataPrt :[Select Id, Assessed_the_Jurisdiction__c, 
                                             Name_of_Jurisdiction__c, Description_of_Safeguards__c
                                             From Data_Protection__c
                                             Where recordtype.developerName = 'Jurisdiction'
                                             AND Application__c = :srId
                                             AND IsActive__c = true
                                             Limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())]) {

                    OB_DataProtectionController.DataProtectionWrapper dataProtectWrap = new OB_DataProtectionController.DataProtectionWrapper();
                    dataProtectWrap.dataProtectObj = dataPrt;
                    jurisdictionList.add(dataProtectWrap);
                }

                //dpo
                for(HexaBPM_Amendment__c amed :[Select Id, Title__c, First_Name__c, 
                                           Last_Name__c, Mobile__c, Email__c, Address__c, Apartment_or_Villa_Number_c__c, 
                                           Permanent_Native_Country__c, Permanent_Native_City__c, 
                                           Emirate_State_Province__c, PO_Box__c, I_Agree_Data_Controller__c, 
                                           Role__c
                                           From HexaBPM_Amendment__c
                                           Where ServiceRequest__c = :srId
                                           AND Role__c includes('DPO')
                                           Limit 1]) {
                    dpoAmendWrap.amedObj = amed;
                }
                // no DPO record related to SR then initialize the DPO record
                if(dpoAmendWrap.amedObj == null) { 
                    // if not existing record then intialize the record
                    HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c();
                    amedObj.Role__c = 'DPO';
                    amedObj.ServiceRequest__c = srId;
                    Id recordTypeID = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
                    if(String.IsNotBlank(recordTypeID)) {
                        amedObj.recordtypeId = recordTypeID;
                    }

                    dpoAmendWrap.amedObj = amedObj;
                }
            //}
        } else {
            respWrap.errorMessage = 'Service Request is blank';
        }

        //Always one SR so can be in loop.
        respWrap.srWrap = srWrap;
        respWrap.dpoAmendWrap = dpoAmendWrap;
        if(jurisdictionList != null && jurisdictionList.size() != 0) {
            srWrap.jurisdictionList = jurisdictionList;
        }
        return respWrap;
    }


    @AuraEnabled
    public static OB_DataProtectionController.ResponseWrapper onAmedSaveDB(String reqWrapPram) {
        try{
            //reqest wrpper
            reqWrap = (OB_DataProtectionController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_DataProtectionController.RequestWrapper.class);

            //response wrpper
            respWrap = new OB_DataProtectionController.ResponseWrapper();
            system.debug('@@@@@@@ reqWrap.dpoAmendWrap ' + reqWrap.dpoAmendWrap.amedObj);
            Boolean isDeleteDataPtct = false;
            
            if(reqWrap.srWrap.srObj.Purpose_of_Notification__c == 'To inform the Commissioner of Data Protection that you do process personal data') {
            	
            	reqWrap.srWrap.srObj.Reason_for_not_processing_Personal_Data__c = '';
            	
            	//Other_Purpose_of_Processing_Data__c
            	if(String.IsnotBlank(reqWrap.srWrap.srObj.Purpose_of_Processing_Data__c) 
            		&& !reqWrap.srWrap.srObj.Purpose_of_Processing_Data__c.contains('Other, Please Specify')){
            		reqWrap.srWrap.srObj.Other_Purpose_of_Processing_Data__c = '';
            	}
            	
            	//Other_Data_Subjects_Personal_Data__c
            	if(String.IsnotBlank(reqWrap.srWrap.srObj.Data_Subjects_Personal_Data__c) 
            		&& !reqWrap.srWrap.srObj.Data_Subjects_Personal_Data__c.contains('Other, Please Specify')){
            		reqWrap.srWrap.srObj.Other_Data_Subjects_Personal_Data__c = '';
            	}
            	
            	//
            	if(reqWrap.srWrap.srObj.In_accordance_with_Article_3__c != 'Yes'){
            		reqWrap.srWrap.srObj.Category_of_Article_3__c='';	
            	}
            	
            	//
            	if(reqWrap.srWrap.srObj.In_accordance_with_Article_12_11__c != 'Yes'){
            		reqWrap.srWrap.srObj.Data_Subject_of_Article_12_11__c='';	
            	}
            	
            	//
            	if(reqWrap.srWrap.srObj.In_accordance_with_Article_14__c != 'No'){
            		reqWrap.srWrap.srObj.No_Accordance_with_Article_14__c='';	
            	}
            	
            	//
            	if(reqWrap.srWrap.srObj.In_accordance_with_Article_15__c != 'No'){
            		reqWrap.srWrap.srObj.No_Accordance_with_Article_15__c='';	
            	}
            	
            	//
            	if(reqWrap.srWrap.srObj.In_accordance_with_Article_16_17_18__c != 'No'){
            		reqWrap.srWrap.srObj.No_Accordance_with_Article_16_17_18__c='';	
            	}
            	
            	//
            	if(reqWrap.srWrap.srObj.In_accordance_with_Article_23_24_25__c != 'No'){
            		reqWrap.srWrap.srObj.No_Accordance_with_Article_23_24_25__c='';	
            	}
            	
            	//Transfer of Personal Data
            	if(reqWrap.srWrap.srObj.Transferring_Personal_Data_outside_DIFC__c == 'Yes'){
            		//26
            		if(reqWrap.srWrap.srObj.In_accordance_with_Article_26__c != 'Yes'){
            			reqWrap.srWrap.srObj.Country_Jurisdiction_Article_26__c='';	
            		}
            		//27
            		if(reqWrap.srWrap.srObj.In_accordance_with_Article_27__c != 'Yes'){
            			reqWrap.srWrap.srObj.Yes_Accordance_with_Article_27_2_to_4__c='';	
						// delete jurisdiction
						isDeleteDataPtct = true;
            		}else{
            			reqWrap.srWrap.srObj.No_Accordance_with_Article_27__c='';
            		}
            		
            		//40
            		if(reqWrap.srWrap.srObj.In_accordance_with_Article_40__c != 'Yes'){
            			reqWrap.srWrap.srObj.Methods_of_Article_40__c='';	
            		}
            	}else{
            	//  no Transfer of Personal Data
	        		reqWrap.srWrap.srObj.In_accordance_with_Article_26__c = '';
	            	reqWrap.srWrap.srObj.Country_Jurisdiction_Article_26__c = '';
	            	reqWrap.srWrap.srObj.In_accordance_with_Article_27__c = '';
	            	reqWrap.srWrap.srObj.Yes_Accordance_with_Article_27_2_to_4__c = '';
	            	reqWrap.srWrap.srObj.No_Accordance_with_Article_27__c = '';
	            	reqWrap.srWrap.srObj.In_accordance_with_Article_40__c = '';
	            	reqWrap.srWrap.srObj.Methods_of_Article_40__c = '';
	            	reqWrap.srWrap.srObj.In_accordance_with_Parts_5_or_6__c = '';
	            	reqWrap.srWrap.srObj.In_accordance_with_Parts_7__c = '';
            	}

            }else{
            	 reqWrap.srWrap.srObj.Transferring_Personal_Data_outside_DIFC__c = '';
            	 reqWrap.srWrap.srObj.Processing_any_sensitive_data__c = '';
            	 reqWrap.srWrap.srObj.Purpose_of_Processing_Data__c = '';
            	 reqWrap.srWrap.srObj.Other_Purpose_of_Processing_Data__c = '';
            	 reqWrap.srWrap.srObj.Data_Subjects_Personal_Data__c = '';
            	 reqWrap.srWrap.srObj.Other_Data_Subjects_Personal_Data__c = '';
            	 reqWrap.srWrap.srObj.Class_of_Personal_Data__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Article_3__c = '';
            	 reqWrap.srWrap.srObj.Category_of_Article_3__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Article_12_11__c = '';
            	 reqWrap.srWrap.srObj.Data_Subject_of_Article_12_11__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Article_13__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Article_14__c = '';
            	 reqWrap.srWrap.srObj.No_Accordance_with_Article_14__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Article_15__c = '';
            	 reqWrap.srWrap.srObj.No_Accordance_with_Article_15__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Article_16_17_18__c = '';
            	 reqWrap.srWrap.srObj.No_Accordance_with_Article_16_17_18__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Article_23_24_25__c = '';
            	 reqWrap.srWrap.srObj.No_Accordance_with_Article_23_24_25__c = '';
            	 reqWrap.srWrap.srObj.Transferring_Personal_Data_outside_DIFC__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Article_26__c = '';
            	 reqWrap.srWrap.srObj.Country_Jurisdiction_Article_26__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Article_27__c = '';
            	 reqWrap.srWrap.srObj.Yes_Accordance_with_Article_27_2_to_4__c = '';
            	 reqWrap.srWrap.srObj.No_Accordance_with_Article_27__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Article_40__c = '';
            	 reqWrap.srWrap.srObj.Methods_of_Article_40__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Parts_5_or_6__c = '';
            	 reqWrap.srWrap.srObj.In_accordance_with_Parts_7__c = '';
            }
            
            //dpo
            for(HexaBPM_Amendment__c amed :[Select Id, Title__c, First_Name__c, 
                                        Last_Name__c, Mobile__c, Email__c, Address__c, Apartment_or_Villa_Number_c__c, 
                                        Permanent_Native_Country__c, Permanent_Native_City__c, 
                                        Emirate_State_Province__c, PO_Box__c, I_Agree_Data_Controller__c, 
                                        Role__c
                                        From HexaBPM_Amendment__c
                                        Where ServiceRequest__c = :reqWrap.srWrap.srObj.Id
                                        AND Role__c includes('DPO')
                                        Limit 1]) {

                reqWrap.dpoAmendWrap.amedObj.Id = amed.Id;
            }
            system.debug('===dpo ID===='+reqWrap.dpoAmendWrap.amedObj.Id);
            if(reqWrap.dpoAmendWrap.amedObj.Id != null){
            	update reqWrap.dpoAmendWrap.amedObj;
            }else{
            	insert reqWrap.dpoAmendWrap.amedObj;
            }
            // DPO
            //upsert reqWrap.dpoAmendWrap.amedObj;
            // service Request
            upsert reqWrap.srWrap.srObj;
            
            if(reqWrap.srWrap.srObj.Purpose_of_Notification__c != 'To inform the Commissioner of Data Protection that you do process personal data') {
                isDeleteDataPtct = true;
            } else {
                if(reqWrap.srWrap.srObj.Transferring_Personal_Data_outside_DIFC__c == 'No') {

                    isDeleteDataPtct = true;
                } else {
                    if(reqWrap.srWrap.srObj.In_accordance_with_Article_27__c == 'No') {
                        isDeleteDataPtct = true;
                    }
                }
            }
            if(isDeleteDataPtct && reqWrap.srWrap != null && reqWrap.srWrap.srObj != null && String.IsNotBlank(reqWrap.srWrap.srObj.Id)) {
                List<Data_Protection__c> dataProtectionList = [Select ID From Data_Protection__c
                                                               Where Application__c = :reqWrap.srWrap.srObj.Id
                                                               AND recordtype.developerName = 'Jurisdiction'
                                                               AND IsActive__c = true
                                                               Limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())];
                if(dataProtectionList != null && dataProtectionList.size() != 0)
                    Delete dataProtectionList;
            }

            //calculate risk
            OB_RiskMatrixHelper.CalculateRisk(reqWrap.srWrap.srObj.Id);
            respWrap.dpoAmendWrap = reqWrap.dpoAmendWrap;
            respWrap.srWrap = reqWrap.srWrap;

            system.debug('@@@@@@@ insert record ' + reqWrap.dpoAmendWrap.amedObj);
        } catch(dmlException e) {
            //string DMLError = e.getMessage();
            //respWrap.errorMessage = DMLError;
            string DMLError = e.getdmlMessage(0) + '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
            respWrap.errorMessage = DMLError;
            system.debug('##########DMLError ' + DMLError);
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
            return respWrap;
        }
        return respWrap;
    }

    /*
     *OB:DataProtectionExist: Remove button
     **/
    @AuraEnabled
    public static OB_DataProtectionController.ResponseWrapper removeDataProtection(String reqWrapPram) {
        try{
            //reqest wrpper
            reqWrap = (OB_DataProtectionController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_DataProtectionController.RequestWrapper.class);

            //response wrpper
            respWrap = new OB_DataProtectionController.ResponseWrapper();
            String srId = reqWrap.srId;
            String jurisdictionId = reqWrap.jurisdictionId;
            respWrap.srWrap = reqWrap.srWrap;
            system.debug('====values===' + reqWrap.srWrap);
            if(String.IsNotBlank(jurisdictionId) && String.IsNotBlank(srId)) {
                Data_Protection__c deleteDataProtect = new Data_Protection__c(Id = jurisdictionId);
                delete deleteDataProtect;
                // fetch list again
                List<OB_DataProtectionController.DataProtectionWrapper> jurisdictionList = new List<OB_DataProtectionController.DataProtectionWrapper>();
                for(Data_Protection__c datProt :[Select Id, Assessed_the_Jurisdiction__c, 
                                                 Name_of_Jurisdiction__c, Description_of_Safeguards__c
                                                 From Data_Protection__c
                                                 Where recordtype.developerName = 'Jurisdiction'
                                                 AND Application__c = :srId
                                                 AND IsActive__c = true
                                                 Limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())]) {
                    OB_DataProtectionController.DataProtectionWrapper dataProtectWrap = new OB_DataProtectionController.DataProtectionWrapper();
                    dataProtectWrap.dataProtectObj = datProt;
                    jurisdictionList.add(dataProtectWrap);
                }
                respWrap.srWrap.jurisdictionList = jurisdictionList;
                //calculate risk
                OB_RiskMatrixHelper.CalculateRisk(srId);
            } else {
                respWrap.errorMessage = 'No Record exist';
            }

        } catch(dmlException e) {
            //string DMLError = e.getMessage();
            //respWrap.errorMessage = DMLError;
            string DMLError = e.getdmlMessage(0) + '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
            respWrap.errorMessage = DMLError;
            system.debug('##########DMLError ' + DMLError);
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
            return respWrap;
        }
        return respWrap;
    }

    /*
     *OB:DataProtectionJurisdiction: Confirm button
     **/
    @AuraEnabled
    public static OB_DataProtectionController.ResponseWrapper onDataProtectionSaveDB(String reqWrapPram) {
        try{

            //reqest wrpper
            reqWrap = (OB_DataProtectionController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_DataProtectionController.RequestWrapper.class);

            //response wrpper
            respWrap = new OB_DataProtectionController.ResponseWrapper();
            String srId = reqWrap.srId;
            List<OB_DataProtectionController.DataProtectionWrapper> jurisdictionList = new List<OB_DataProtectionController.DataProtectionWrapper>();

            if(String.IsNotBlank(srId)) {
                Data_Protection__c dataProtectObj = reqWrap.jurisdictionWrap.dataProtectObj;
                Boolean duplicate = false;
                system.debug('==List=old==' + dataProtectObj);
                system.debug('==jurisdictionList==' + jurisdictionList);
                for(Data_Protection__c dup :[Select ID
                                             From Data_Protection__c
                                             Where recordtype.developerName = 'Jurisdiction'
                                             AND Application__c = :srId
                                             AND IsActive__c = true
                                             AND Name_of_Jurisdiction__c = :dataProtectObj.Name_of_Jurisdiction__c
                                             AND Id != :dataProtectObj.Id
                                             Limit 1]) {
                    duplicate= true;
                }
                if(!duplicate) {
                	if(String.IsNotBlank(reqWrap.jurisdictionWrap.dataProtectObj.Assessed_the_Jurisdiction__c)
                		&& reqWrap.jurisdictionWrap.dataProtectObj.Assessed_the_Jurisdiction__c != 'Yes'){
                		
                		reqWrap.jurisdictionWrap.dataProtectObj.Description_of_Safeguards__c='';
                	}
                    upsert reqWrap.jurisdictionWrap.dataProtectObj;
                    // fetch list again
                    //List<OB_DataProtectionController.DataProtectionWrapper> jurisdictionList = new List<OB_DataProtectionController.DataProtectionWrapper>();
                    for(Data_Protection__c datProt :[Select Id, Assessed_the_Jurisdiction__c, 
                                                     Name_of_Jurisdiction__c, Description_of_Safeguards__c
                                                     From Data_Protection__c
                                                     Where recordtype.developerName = 'Jurisdiction'
                                                     AND Application__c = :srId
                                                     AND IsActive__c = true
                                                     Limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())]) {
                        OB_DataProtectionController.DataProtectionWrapper dataProtectWrap = new OB_DataProtectionController.DataProtectionWrapper();
                        dataProtectWrap.dataProtectObj = datProt;
                        jurisdictionList.add(dataProtectWrap);
                    }
                    //calculate risk
                    OB_RiskMatrixHelper.CalculateRisk(srId);

                    respWrap.srWrap = reqWrap.srWrap;
                    respWrap.srWrap.jurisdictionList = jurisdictionList;

                    system.debug('==List===' + respWrap.srWrap.jurisdictionList);
                } else {
                    respWrap.errorMessage = 'Duplicate Record';
                }
            }


        } catch(dmlException e) {
            //string DMLError = e.getMessage();
            //respWrap.errorMessage = DMLError;
            string DMLError = e.getdmlMessage(0) + '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
            respWrap.errorMessage = DMLError;
            system.debug('##########DMLError ' + DMLError);
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
            return respWrap;
        }
        respWrap.jurisdictionWrap = reqWrap.jurisdictionWrap;
        respWrap.srWrap = reqWrap.srWrap;
        return respWrap;
    }
    /*
     *OB:DataProtectionNew: New button
     **/
    @AuraEnabled
    public static OB_DataProtectionController.ResponseWrapper initDataProtectRecord(String reqWrapPram) {

        //reqest wrpper
        reqWrap = (OB_DataProtectionController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_DataProtectionController.RequestWrapper.class);

        //response wrpper
        respWrap = new OB_DataProtectionController.ResponseWrapper();
        String srId = reqWrap.srId;
        system.debug('@@@@@@@ reqWrap.srId ' + srId);

        OB_DataProtectionController.DataProtectionWrapper dataProtectWrap = new OB_DataProtectionController.DataProtectionWrapper();

        Data_Protection__c dataProtectObj = new Data_Protection__c();
        dataProtectObj.Application__c = srId;
        dataProtectObj.IsActive__c = true;

        Id recordTypeID = OB_QueryUtilityClass.getRecordtypeID('Data_Protection__c', 'Jurisdiction');
        if(String.IsNotBlank(recordTypeID)) {
            dataProtectObj.recordtypeId = recordTypeID;
        }

        dataProtectWrap.dataProtectObj = dataProtectObj;

        respWrap.jurisdictionWrap = dataProtectWrap;
        return respWrap;
    }
    /////////////////////////////////Wrapper  classes//////////////////////////////////////
    /*
     * data will come from client side
     */
    public class RequestWrapper{
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public string pageID { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public Id jurisdictionId { get; set; }
        @AuraEnabled public SRWrapper srWrap { get; set; }
        @AuraEnabled public DataProtectionWrapper jurisdictionWrap { get; set; }
        @AuraEnabled public AmendmentWrapper dpoAmendWrap { get; set; }

        public RequestWrapper() { 
        }
    }

    /*
     * store detail of service request with list of amendment
     */
    public class SRWrapper{
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public String declarationCorporateText { get; set; }
        @AuraEnabled public List<DataProtectionWrapper> jurisdictionList { get; set; }
        @AuraEnabled public Boolean isDraft { get; set; }
        @AuraEnabled public string viewSRURL { get; set; }
        public SRWrapper() {
            List<DataProtectionWrapper> jurisdictionList = new List<DataProtectionWrapper>();
        }
    }

    /*
     * store detail of amendment object
     */
    public class AmendmentWrapper{
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        public AmendmentWrapper() { 
        }
    }

    /*
     * store detail of Data Protection object
     */
    public class DataProtectionWrapper{
        @AuraEnabled public Data_Protection__c dataProtectObj { get; set; }

        public DataProtectionWrapper() { 
        }
    }

    /*
     * Result will send to client side
     */
    public class ResponseWrapper{
        @AuraEnabled public SRWrapper srWrap { get; set; }
        @AuraEnabled public DataProtectionWrapper jurisdictionWrap { get; set; }
        @AuraEnabled public AmendmentWrapper dpoAmendWrap { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled
        public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        public ResponseWrapper() { 
        }
    }

    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string PageId) {

        system.debug('=====PageId========'+PageId);
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
        PageFlowControllerHelper.objSR = objRequest;
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();


        if(OB_QueryUtilityClass.minRecValidationCheck(SRID, 'DPO')) {
            system.debug('-----------check----- '+PageId);
            objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, PageId);
            upsert objRequest;
        }



        OB_RiskMatrixHelper.CalculateRisk(SRID);

        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;

        system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
        return respWrap;
    }
    public class ButtonResponseWrapper {
        @AuraEnabled public String pageActionName { get; set; }
        @AuraEnabled public string communityName { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public string CommunityPageName { get; set; }
        @AuraEnabled public string sitePageName { get; set; }
        @AuraEnabled public string strBaseUrl { get; set; }
        @AuraEnabled public string srId { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public boolean isPublicSite { get; set; }

    }
}