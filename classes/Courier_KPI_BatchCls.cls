/*************************************************************************************************
 *  Name        : Courier_KPI_BatchCls
 *  Author      : Veera Punati
 *  Company     : VRK IT
 *  Date        : 21-06-2021    
 *  Purpose     : This class is to calculate Courier durations on Steps in Batches     
  
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By          Description
---------------------------------------------------------------------------------------------------------------------             


    
***************************************************************************************************/

Global without sharing class Courier_KPI_BatchCls implements Database.Batchable<sobject>{


   


   Private String SrQueryString;
   string C = 'Courier';
    
    Global Courier_KPI_BatchCls ()
    {
        SrQueryString = 'Select id,step_name__c,Created_Date_Time__c,Closed_Date_Time__c,Sys_SR_Group__c from step__c where Closed_Date_Time__c!=null  AND Closed_Date_Time__c = TODAY AND Sys_SR_Group__c =  \'GS\' and  step_name__c like \'%'+c+'%\'';
    }
    
     Global Database.QueryLocator start(Database.BatchableContext BC)
    {
    
      return Database.getQueryLocator(SrQueryString);
    }
    
    
    Global Void execute(Database.BatchableContext BC,list<step__C> stpbatch)
    {
    
     List<step__c> ListStep = new List<step__c>();
     map<string,BusinessHours> map_businessHour = new map<string,BusinessHours>();
    
      if(stpbatch.size()>0){
      
       decimal tot_GSCourier_time = 0;
       
         for(BusinessHours bh : [Select Id,name from BusinessHours where IsActive=true]){
                map_businessHour.put(bh.name,bh);
            }
            
        for(step__c stp : stpbatch) {   
        
        step__c s = new step__C(id = stp.id);
       
         if(stp.Closed_Date_Time__c!=null && stp.Created_Date_Time__c!=null){
           
              BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.Sys_SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.Sys_SR_Group__c);
                                }   
                            }
                            
                     if(bh!=null){
                                tot_GSCourier_time = tot_GSCourier_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.Created_Date_Time__c,stp.Closed_Date_Time__c)/3600000));
                                
                   }    

           s.Courier_Duration__c =      tot_GSCourier_time;      
         
         }
         
         ListStep.add(s);
        } 
         Database.SaveResult[] lstUpdateSRs;
        if(ListStep.size()>0) 
         lstUpdateSRs = Database.update(ListStep, false);
         String sStr = '';
            for(Database.SaveResult objResult : lstUpdateSRs){
                if(objResult.isSuccess() == false){
                    sStr += 'SR Id - '+objResult.getId()+' Error : '+objResult.getErrors()+'\n';
                }
            }
            if(sStr != ''){
                insert LogDetails.CreateLog(null, 'Schedule for courier total time failed', sStr);
            }
      
      }
    
    }
    
    global void finish(Database.BatchableContext BC){
    
    }
}