/*************************************************************************************************
 *  Name        : CLS_BatchPublicRegistry
 *  Author      : Kaavya Raghuram
 *  Company     : NSI JLT
 *  Date        : 2014-10-22     
 *  Purpose     : This class is to call the Public Registry webservice         
**************************************************************************************************/
global without sharing class CLS_BatchPublicRegistry implements Schedulable {
    global void execute(SchedulableContext ctx) {
        callPR();
    }
    
    @future(callout=true)
    public static void callPR(){
        try{
        
            //Initialising the class to generate XML for sending as request
            CLS_CreatePRXML cls= new CLS_CreatePRXML();
            String xmlstr= cls.getxmlRes(); //getting the required xml string for Accounts which are qualified to be pushed to PR
            system.debug('XXXMMLL==='+xmlstr);
            if(xmlstr!='No'){
            
                //Initialising the Public Registry webservice
                WS_PublicRegistry.hellowsdlPort test= new WS_PublicRegistry.hellowsdlPort();
                test.timeout_x = 100000;
                WS_PublicRegistry.statsRequest_element req= new WS_PublicRegistry.statsRequest_element();
                
                
                req.name=xmlstr;
                WS_PublicRegistry.statsResponse_element resp= new WS_PublicRegistry.statsResponse_element();
                
                //if(!System.Test.isRunningTest()){ //to prevent callouts while running test class
                    resp.return_x=test.stats(req.name);
                //}
                system.debug('RRR==>'+resp);
                system.debug('RRRRRXXX==>'+resp.return_x);
                List<String> ROCNos=resp.return_x.split(',');
                List<Account> acclist=[select id,Last_PR_Push__c,Registration_License_No__c,Push_to_PR__c from Account where Registration_License_No__c in: ROCNos];
                for(Account acc: acclist){
                    acc.Push_to_PR__c=false;
                    acc.Last_PR_Push__c=system.now();
                }
                update acclist;
            }
        }
        catch(Exception ex){
            system.debug('Exception is : '+ex.getMessage());
            Log__c objLog = new Log__c();
            objLog.Description__c ='Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
            objLog.Type__c = 'Webservice Callout for Public Registry';
            insert objLog;        
        }
    }
}