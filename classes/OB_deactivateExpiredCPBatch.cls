global with sharing class OB_deactivateExpiredCPBatch implements Database.Batchable<sObject>,schedulable {
   
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        Date dt = system.today();
        String commvalue = 'Yes';
        String statusValue = 'Active';
        set<string> cpTypeCheck = new set<string>{'Events','Training or educational services offered by non-DIFC registered entities'};
        String query = 'SELECT Id , Account__c FROM Commercial_Permission__c WHERE End_Date__c!= NULL  AND Account__r.Is_Commercial_Permission__c =: commvalue AND End_Date__c <: dt AND Status__c =:statusValue AND Account__c != NULL AND Account__r.OB_Type_of_commercial_permission__c IN:cpTypeCheck  ';
        return Database.getQueryLocator(query);
        
    }

    global void execute(Database.BatchableContext BC, List<Commercial_Permission__c> scope){
        
        markAccountAsExpired(scope);      
        
    }  
    
    // Finish Method
    global void finish(Database.BatchableContext BC){
    
    }
   
    public void markAccountAsExpired( List<Commercial_Permission__c> scope ){
        try{
                system.debug('inside method '+scope);
                set<string> setAccount = new set<string>();
                set<Id> cpId = new set<Id>();

                list<Account> accToUpdate = new list<Account>();
                list<Compliance__c> complToUpdate = new list<Compliance__c>();
                list<Commercial_Permission__c> cpToUPdate = new list<Commercial_Permission__c>();
                list<AccountContactRelation> ACRToUPdate = new list<AccountContactRelation>();

                for(Commercial_Permission__c cpObj : scope ){
                    //cpObj.Status__c = 'Expired';
                    //cpToUPdate.add(cpObj);
                    setAccount.add(cpObj.Account__c);
                }

                if(setAccount.size() > 0){
                    set<id>  accountWithoutOpenSr = OB_AgentEntityUtils.hasOpenSr(setAccount);
                    if(accountWithoutOpenSr.size() > 0){
                        system.debug('setaccounts===>' +setAccount);

                        for(Commercial_Permission__c cpObj : scope ){

                            /* for(string accId : accountWithoutOpenSr){
                                if(accId == cpObj.Account__c){
                                   cpObj.Status__c = 'Expired';
                                   cpToUPdate.add(cpObj);                                  
                                }
                            } */
                            if(accountWithoutOpenSr.contains(cpObj.Account__c)){
                                 cpObj.Status__c = 'Expired';
                                 cpToUPdate.add(cpObj);
                            }
                           
                            
                        }

                        //set account status to cp-expire
                        for(Account acc:[Select Id,(select id from AccountContactRelations where IsActive = true),OB_Active_Commercial_Permission__c  from Account where Id IN:accountWithoutOpenSr]){
                            acc.ROC_Status__c = 'CP-Expired';
                            accToUpdate.add(acc);   
                            //cpId.add(acc.OB_Active_Commercial_Permission__c);
                            for(AccountContactRelation ACR : acc.AccountContactRelations){
                                ACR.IsActive = false;
                                ACRToUPdate.add(ACR);
                            }
                        }

                        //set cp to inactive
                        /* for(Commercial_Permission__c cp:[Select Id  from Commercial_Permission__c where Id IN:cpId]){
                            cp.Status__c = 'Expired';
                            cpToUPdate.add(cp);   
                        } */


                        if(accToUpdate.size() > 0){
                            update accToUpdate;
                        }

                        /* if(complToUpdate.size() > 0){
                            update complToUpdate;
                        } */

                        if(cpToUPdate.size() > 0){
                            update cpToUPdate;
                        }

                        if(ACRToUPdate.size() > 0){
                            update ACRToUPdate;
                        }
                    }
                }
                
                
                
                        
        }catch (Exception e){
            Log__c objLog = new Log__c();
            objLog.Description__c = 'Exception OB_deactivateExpiredCPBatch.createCommpermissionRenewalSR'+e.getLineNumber()+' '+e.getMessage();
            objLog.Type__c = 'OB_deactivateExpiredCPBatch';
            insert objLog;
        }
    }
       
    global void execute(schedulablecontext sc){
        OB_deactivateExpiredCPBatch sc1 = new OB_deactivateExpiredCPBatch(); 
        database.executebatch(sc1,200);
    }

}