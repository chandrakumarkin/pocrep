/*
 * 
 * Author:Diana Correa
 * Purpose: Test class for mobile login controller.
 * 
 * 
 */

@isTest
public class TestMobileCustomLoginController {

    static testmethod void TestLogin(){
        
        MobileCustomLoginController mobileLoginController = new MobileCustomLoginController();
        
        mobileLoginController.forwardToCustomAuthPage();
        mobileLoginController.login();
            
    }
}