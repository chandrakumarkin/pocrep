/******************************************************************************************
 *  DIFC - Service Request Trigger
 
 *  Author   : Durga Prasad
 *  Company  : NSI JLT
                         
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        	Updated By    	Description
 ----------------------------------------------------------------------------------------              
 V1.1    15-june-2016 	Swati        	boolean to set for custom code change of entity name as per #2693                            

*******************************************************************************************/


public without sharing class RecursiveControlCls { 
  public static boolean isUpdatedAlready=false;
  //Added by Jinsy
  public static boolean isTriggerExecution = true;
  //Added by kaavya
  public static boolean trg_pendingreportsdate_rec = false;
  public static boolean Do_not_Reevaluate = false;
  
  public static boolean isAccountTrgExecuted = false;
  
  public static boolean runOnlyOnce = false;
  
  public static boolean runOnlyOnceForReciept = false;
  
  public static boolean runOnlyOnceToTrackHistory = false;
  
  public static boolean isThroughCustomCode = false; //V1.1 
  
  public static boolean isDataSenttoMashreq = false;  
  public static boolean isBPGenerated = false; 
}