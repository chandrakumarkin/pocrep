/******************************************************************************************
 *  Name        : OB_PricingLineDeepCloneController 
 *  Author      : Durga Prasad
 *  Description : Controller for PricingLine Deepl Clone 
*******************************************************************************************/
public without sharing class OB_PricingLineDeepCloneController {
    public string PricingLineId{get;set;}
    public string ProductId{get;set;}
    public OB_PricingLineDeepCloneController(){
      if(apexpages.currentpage().getparameters().get('Id')!=null)
        PricingLineId = apexpages.currentpage().getparameters().get('Id');
      if(apexpages.currentpage().getparameters().get('ProductId')!=null)
        ProductId = apexpages.currentpage().getparameters().get('ProductId');
    }
    public pagereference DeepClone(){
      pagereference pg;
      if(PricingLineId!=null){
        Savepoint svpoint = Database.setSavepoint();
        try{
          HexaBPM__Pricing_Line__c objPL = new HexaBPM__Pricing_Line__c();
            string QryString = 'Select Id,Name';
              for(string strField :getsObjectFields('HexaBPM__Pricing_Line__c')){
                  QryString += ',' + strField;
              }
            QryString += ' From HexaBPM__Pricing_Line__c where Id=:PricingLineId';
            for(HexaBPM__Pricing_Line__c objPLRec:database.query(QryString)){
                objPL = objPLRec.clone();
            }
          objPL.DEV_Id__c = null;
          insert objPL;
          
          list<HexaBPM__Condition__c> lstConditions = new list<HexaBPM__Condition__c>();
          QryString = 'Select Id,Name';
              for(string strField :getsObjectFields('HexaBPM__Condition__c')){
                  QryString += ',' + strField;
              }
            QryString += ' From HexaBPM__Condition__c where HexaBPM__Pricing_Line__c=:PricingLineId';
            for(HexaBPM__Condition__c objCond:database.query(QryString)){
                HexaBPM__Condition__c objNewCond = objCond.clone();
                objNewCond.HexaBPM__Pricing_Line__c = objPL.Id;
                objNewCond.HexaBPM__DEV_Id__c = null;
                objNewCond.HexaBPM__Code__c = null;
                lstConditions.add(objNewCond);
            }
            
            if(lstConditions.size()>0)
              insert lstConditions;
            
            list<HexaBPM__Dated_Pricing__c> lstDatedPricing = new list<HexaBPM__Dated_Pricing__c>();
          QryString = 'Select Id,Name';
              for(string strField :getsObjectFields('HexaBPM__Dated_Pricing__c')){
                  QryString += ',' + strField;
              }
            QryString += ' From HexaBPM__Dated_Pricing__c where HexaBPM__Pricing_Line__c=:PricingLineId';
            for(HexaBPM__Dated_Pricing__c objDP:database.query(QryString)){
                HexaBPM__Dated_Pricing__c objNewDP = objDP.clone();
                objNewDP.HexaBPM__Pricing_Line__c = objPL.Id;
                objNewDP.HexaBPM__DEV_Id__c = null;
                lstDatedPricing.add(objNewDP);
            }
            if(lstDatedPricing.size()>0)
              insert lstDatedPricing;
            pg = new pagereference('/'+objPL.Id);
          pg.setredirect(true);
        }catch(Exception e){
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getmessage()+''));
          Database.rollback(svpoint);
          return null;
        }
      }else{
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'No Pricing Line Id found in the URL'));
      }
      return pg;
    }
    public static list<string> getsObjectFields(string objName){
      list<string> fieldList = new list<string>();
        map<String,Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(objName.toLowerCase()).getDescribe().fields.getMap();
        if(fieldMap!= null){
          for (Schema.SObjectField f : fieldMap.values()){
              Schema.DescribeFieldResult fd = f.getDescribe();
              if(fd.isCustom()){
                 fieldList.add(fd.getName());
              }
          }
        }
        return fieldList;
    }
}