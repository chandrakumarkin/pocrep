/*
Author      : Leeba
Date        : 1-April-2020
Description : Test class for CC_UpdateContactOnApporval
--------------------------------------------------------------------------------------
*/
@isTest
public class CC_UpdateContactOnApporvalTest{
    
    public static testMethod void CC_UpdateContactOnApporval() {
        
        Account acc  = new Account();
        acc.name = 'test';      
        insert acc;
        
        Contact con = new Contact();
        con.LastName = 'test';
        insert con;
        
        HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
        objsrTemp.HexaBPM__Menu__c = 'Company Services';
        objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
        insert objsrTemp;
        
        
        
        Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
        
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id; 
        objHexaSR.HexaBPM__Contact__c = con.id;       
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;        
        insert objHexastep;
        
        Test.startTest();
        HexaBPM__Step__c step = [select HexaBPM__SR__c,HexaBPM__SR__r.Are_You_Resident_Of_UAE__c,HexaBPM__SR__r.Valid_UAE_residence_visa__c,HexaBPM__SR__r.Address_Line_1__c,HexaBPM__SR__r.Address_Line_2__c,
                                 HexaBPM__SR__r.City_Town__c,HexaBPM__SR__r.State_Province_Region__c,HexaBPM__SR__r.Country__c,HexaBPM__SR__r.Po_Box_Postal_Code__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.HexaBPM__Contact__c,HexaBPM__SR__r.Gender__c
                                 from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_UpdateContactOnApporval CC_UpdateContactOnApporvalObj = new CC_UpdateContactOnApporval();
        CC_UpdateContactOnApporvalObj.EvaluateCustomCode(objHexaSR,step); 
        
        CC_UpdateContactOnApporval CC_UpdateContactOnApporvalObj2 = new CC_UpdateContactOnApporval();
        CC_UpdateContactOnApporvalObj2.EvaluateCustomCode(null,null); 
        Test.stopTest();
    }
    
    
    
    
}