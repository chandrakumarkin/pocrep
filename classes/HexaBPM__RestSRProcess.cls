/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/Fetch_SR_Process/*')
global class RestSRProcess {
    global RestSRProcess() {

    }
    @HttpPost
    global static HexaBPM.RestSRProcess.SRProcessWrap GetSRProcess(String SRTemplateName) {
        return null;
    }
    global static String getAccessibleFieldsSoql(String obj, String whereClause) {
        return null;
    }
global class SRProcessWrap {
}
}
