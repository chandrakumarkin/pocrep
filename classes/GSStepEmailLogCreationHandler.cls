/******************************************************************************************
 *  Author   : 
 *  Company  : 
 *  Date     : 
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    15/12/2019  Created
V1.1    15/12/2019  shoaib    Updated
V1.2    15/12/2019  shoaib    Updated
V1.3    04/04/2020  Mudasir   Introduced the DHA reference number in the notification where the API name is Application_Reference__c
v1.4    07/12/2020  Shoaib    #Ticket 10308
V1.5    13/07/2020  Arun      Added code for Separate Services  #10270  
V1.6    27/04/2021  Veera     Added code for Type of Request 
V1.7    01/08/2021  Veera     fixed code for Fine Amount on Insert
**********************************************************************************************************************/
public class GSStepEmailLogCreationHandler {

    public static void CreateEmailLogOnInsert(step__c step) {

 boolean SeparateServices=false;
   Id ServiceCreatedBy;
 
        List < Email_Logs__c > emailLogs = new List < Email_Logs__c > ();
     
        if (step.Sys_SR_Group__c == 'GS' && system.label.isEmailLogonInsert == 'true') {

            List < GSStepandStatus__mdt > GSstepandStatus = [select MasterLabel, status__c from GSStepandStatus__mdt where MasterLabel =: step.Step_Name__c];
 
            if (GSstepandStatus.size() > 0) {

                string stepStatus = GSstepandStatus[0].status__c;

                if ((step.Status_Code__c == 'CLOSED' && stepStatus.contains('CLOSED')) ||
                    (step.Status_Code__c == 'AWAITING_ORIGINALS' && stepStatus.contains('AWAITING_ORIGINALS')) ||
                    (step.Status_Code__c == 'CANCELLED' && stepStatus.contains('CANCELLED')) ||
                    (step.Status_Code__c == 'VERIFIED' && stepStatus.contains('VERIFIED')) ||
                    (step.Status_Code__c == 'APPROVED' && stepStatus.contains('APPROVED')) ||
                    (step.Status_Code__c == 'AWAITING_REVIEW' && stepStatus.contains('AWAITING_REVIEW')) ||
                    (step.Status_Code__c == 'PENDING_REVIEW' && stepStatus.contains('PENDING_REVIEW')) ||
                    (step.Status_Code__c == 'REUPLOAD_DOCUMENT' && stepStatus.contains('REUPLOAD_DOCUMENT'))
                ) {

//1.5 Added SR__r.Customer__r.Separate_Services__c
                    step__c stp = [select id, name,SR__r.Createdbyid,SR__r.Customer__r.Separate_Services__c, Step_Notes__c, SAP_IDNUM__c, SR_Type_Step__c, Client_Name__c, SR_Temp_Menu__c, Biometrics_Required__c, Applicant_Name__c, SR_Template__c, SR_Menu_Text__c, Step_Name__c, SR__C, SR__r.Customer__c, SR__r.Customer__r.name, SR__r.Type_of_Request__c, SR__r.First_Name__c, Rejection_Reason__c, SR__r.Middle_Name__c, SR__r.Last_Name__c, SR__r.Name, SR__r.Service_Category__c, sr__r.Send_SMS_To_Mobile__c,sr__r.Fine_Amount_AED__c,Fine_Amount__c,Application_Reference__c, (select Id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c from Appointments__r order by LastModifiedDate desc limit 1) from step__c where id =: step.Id];

                    string TempCode = step.SR_Template__c;
                    system.debug('TempCode' + TempCode);

                    String bodyofTemp = '';
                    string subjectTemp = '';
                    
                    
                     //v1.5 Start 
                    SeparateServices=stp.SR__r.Customer__r.Separate_Services__c;
                    ServiceCreatedBy=stp.SR__r.Createdbyid;
                    //v1.5 ~End
                    

                    for (GSEmailTemplates__mdt emilTemplates: [select MasterLabel, Service_Types__c, Step_Status_Code__c, Email_Body__c, subject__c, Step_Name__c, SMS_body__c, SMS_Subject__c from GSEmailTemplates__mdt where Step_Name__c =: stp.Step_Name__c]) {

                        List < pending__c > pendings = new List < pending__c > ();

                        if (stp.Step_Name__c == 'Under process') {
                            List < pending__c > pending = [select id, Comments__c, createdDate from pending__c where step__c =: stp.id order by createdDate desc limit 1];
                            if (pending.size() > 0) pendings.add(pending[0]);
                        }

                        Email_Logs__c emLogs = new Email_Logs__c();

                        if (pendings.size() > 0 || stp.Step_Name__c != 'Under process') {

                            if ((emilTemplates.Service_Types__c.contains(TempCode) && emilTemplates.Step_Status_Code__c == step.Status_Code__c) || (emilTemplates.Service_Types__c.contains('GS_ALL') && emilTemplates.Step_Status_Code__c == step.Status_Code__c)) {
                                emLogs.name = stp.name;
                                bodyofTemp = emilTemplates.Email_Body__c;
                                subjectTemp = emilTemplates.subject__c;
                                string smsBody = emilTemplates.SMS_body__c;
                                string smsSubject = emilTemplates.SMS_Subject__c;

                                if (!string.isBlank(smsBody)) {

                                    if (smsBody.contains('{!Step__c.SR__c}')) {
                                        smsBody = smsBody.replace('{!Step__c.SR__c}', stp.SR__r.name);
                                    }

                                    if (smsBody.contains('{!Step__c.Applicant_Name__c}')) {
                                        smsBody = smsBody.replace('{!Step__c.Applicant_Name__c}', stp.Applicant_Name__c);
                                    }

                                    if (smsBody.contains('{!Step__c.Appointment_StartDate}')) {
                                        string AppointDate = stp.Appointments__r[0].Appointment_StartDate_Time__c.format('dd-MM-yyyy');
                                        smsBody = smsBody.replace('{!Step__c.Appointment_StartDate}', AppointDate);
                                    }
                  if (smsBody.contains('{!Step__c.Application_Reference__c}')) {
                                      smsBody = smsBody.replace('{!Step__c.Application_Reference__c}', stp.Application_Reference__c);
                  }
                                }

                                string mobileNo = '';

                                if (!string.isBlank(smsSubject)) {
                                    mobileNo = stp.sr__r.Send_SMS_To_Mobile__c;
                                    mobileNo = (stp.sr__r.Send_SMS_To_Mobile__c == '' || stp.sr__r.Send_SMS_To_Mobile__c == null) ? '' : stp.sr__r.Send_SMS_To_Mobile__c;
                                    if (smsSubject.contains('{!step__c.sr__r.Send_SMS_To_Mobile__c}')) smsSubject = smsSubject.replace('{!step__c.sr__r.Send_SMS_To_Mobile__c}', mobileNo);
                                }

                                if (!string.isBlank(subjectTemp)) {

                                    if (subjectTemp.contains('{!Step__c.SR__c}')) {
                                        subjectTemp = subjectTemp.replace('{!Step__c.SR__c}', stp.SR__r.name);
                                    }

                                }

                                if (bodyofTemp.contains('{!Step__c.pending__r.comments__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.pending__r.comments__c}', pendings[0].Comments__c);
                                }


                                if (bodyofTemp.contains('{!Step__c.SR__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.SR__c}', stp.SR__r.name);
                                }

                                string ApplicantName = '';
                                if (bodyofTemp.contains('{!Step__c.Applicant_Name__c}')) {
                                    ApplicantName = stp.Applicant_Name__c;
                                    if (ApplicantName == '' || ApplicantName == null) {
                                        ApplicantName = stp.SR__r.Customer__r.name;
                                    }
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Applicant_Name__c}', ApplicantName);
                                }

                                if (bodyofTemp.contains('{!step.SR__r.Customer__r.name}')) {
                                    bodyofTemp = bodyofTemp.replace('{!step.SR__r.Customer__r.name}', stp.SR__r.Customer__r.name);
                                }

                                if (bodyofTemp.contains('{!Step__c.SAP_IDNUM__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.SAP_IDNUM__c}', stp.SAP_IDNUM__c);
                                }

                                if (bodyofTemp.contains('{!Step__c.Biometrics_Required__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Biometrics_Required__c}', stp.Biometrics_Required__c);
                                }

                                if (bodyofTemp.contains('{!Step__c.Service_Type__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Service_Type__c}', stp.SR_Type_Step__c);
                                }

                                if (bodyofTemp.contains(' {!Step__c.SR_Menu_Text__c}')) {
                                    bodyofTemp = bodyofTemp.replace(' {!Step__c.SR_Menu_Text__c}', stp.SR_Menu_Text__c);
                                }

                                if (bodyofTemp.contains(' {!Step__c.Client_Name__c}')) {
                                    bodyofTemp = bodyofTemp.replace(' {!Step__c.Client_Name__c}', stp.Client_Name__c);
                                }

                                if (bodyofTemp.contains(' {!Step__c.SR_Temp_Menu__c}')) {
                                    bodyofTemp = bodyofTemp.replace(' {!Step__c.SR_Temp_Menu__c}', stp.SR_Temp_Menu__c);
                                }

                                if (bodyofTemp.contains('{!Step__c.Rejection_Reason__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Rejection_Reason__c}', stp.Rejection_Reason__c);
                                }

                                if (bodyofTemp.contains('{!$Label.Community_URL}')) {
                                    string Label = system.label.Community_URL;
                                    bodyofTemp = bodyofTemp.replace('{!$Label.Community_URL}', Label);
                                }

                                if (bodyofTemp.contains('{!Step__c.SRId__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.SRId__c}', stp.SR__c);
                                }
                                if (bodyofTemp.contains('{!Step__c.Application_Reference__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Application_Reference__c}', stp.Application_Reference__c);
                                }
                                
                                //v1.7 --start
                                string fineAmount = '';

                                if (bodyofTemp.contains('{!Step__c.SR__r.Fine_Amount_AED__c}')) {

                                    if (!(string.valueOf(stp.Fine_Amount__c) == '' || string.valueOf(stp.Fine_Amount__c) == null)) {
                                        bodyofTemp = bodyofTemp.replace('{!Step__c.SR__r.Fine_Amount_AED__c}', string.valueOf(stp.Fine_Amount__c));
                                    } else {
                                        fineAmount = (string.valueOf(stp.SR__r.Fine_Amount_AED__c) == '' || string.valueOf(stp.SR__r.Fine_Amount_AED__c) == null) ? '0' : string.valueOf(stp.SR__r.Fine_Amount_AED__c);
                                        bodyofTemp = bodyofTemp.replace('{!Step__c.SR__r.Fine_Amount_AED__c}', fineAmount);

                                    }
                                }
                                //v1.7 -- End

                                emLogs.subject__c = subjectTemp;
                                emLogs.Email_Body__c = bodyofTemp;
                                emLogs.Service_Request__c = stp.sr__c;
                                emLogs.step__c = stp.id;
                                emLogs.SMS_body__c = smsBody;
                                emLogs.SMS_Subject__c = smsSubject;

                          //1.4
                            if(SeparateServices)
                            {
                                emLogs.Portal_User_1__c =ServiceCreatedBy;
                            }
                            else
                            {
                            
                                List<User> usr            = new List<User>();
                                Set<String> contactIds    = new Set<String>(); 
                                Map<String,AccountContactRelation> accountContactMap = new  Map<String,AccountContactRelation>();
                             
                                for(AccountContactRelation ACR: [Select Id,contactId,Roles from AccountContactRelation where AccountId =: stp.SR__r.Customer__c and  isActive = true ]){
                                    contactIds.add(ACR.contactId);
                                    accountContactMap.put(ACR.contactId,ACR);
                                }
     
                                for(User thisUser : [select Id,ContactId,Community_User_Role__c FROM User where ContactId IN : contactIds and IsActive = true AND ID !=: ServiceCreatedBy]){
                                    if(accountContactMap.containsKey(thisUser.ContactId) &&
                                        accountContactMap.get(thisUser.ContactId).Roles != null &&
                                        accountContactMap.get(thisUser.ContactId).Roles.Contains('Employee Services')){
                                          
                                          usr.add(thisUser);
                                     }
                                }

                                emLogs.Portal_User_1__c = [SELECT Id FROM User WHERE Id =: stp.SR__r.Createdbyid AND Community_User_Role__c != null].size()> 0 ?
                                [SELECT Id FROM User WHERE Id =: stp.SR__r.Createdbyid AND Community_User_Role__c != null].Id : null;
                          
                            if (usr.size() > 0)
                                emLogs.Portal_User_2__c = usr[0].id;
                            if (usr.size() > 1)
                                emLogs.Portal_User_3__c = usr[1].id;
                            if (usr.size() > 2)
                                emLogs.Portal_User_4__c = usr[2].id;
                            if (usr.size() > 3)
                                emLogs.Portal_User_5__c = usr[3].id;
                                    }

                            }
                        }
                        if ((subjectTemp.length() > 0) && (bodyofTemp.length() > 0)){
                            emailLogs.add(emLogs);
                        }
                    }

                    try {
                        if (emailLogs.size() > 0) {
                            insert emailLogs;
                        }

                    } catch (Exception ex) {
                        Log__c objLog = new Log__c( Type__c = 'email Log record creation failed', Description__c = ex.getMessage() );
                        insert objLog;
                    }
                }
            }
        }
    }

    public static void CreateEmailLogOnUpdate(step__c step) {
    
     boolean SeparateServices=false;
     Id ServiceCreatedBy;
     
     Boolean accessCardBlock = false;
        if(step.Sys_SR_Group__c == 'GS'){
   
         accessCardBlock = StopEmailForAccessCardStep([SELECT Id,sr__c FROM step__c WHERE Id=:step.Id LIMIT 1].sr__c,step);
        
        }
        if (step.Sys_SR_Group__c == 'GS' && system.label.isEmailLogonUpdate == 'true' && !accessCardBlock) {

            List < GSStepandStatus__mdt > GSstepandStatus = [select MasterLabel, status__c from GSStepandStatus__mdt where MasterLabel =: step.Step_Name__c];

            if (GSstepandStatus.size() > 0) {
                string stepStatus = GSstepandStatus[0].status__c;
                if ((step.Status_Code__c == 'CLOSED' && stepStatus.contains('CLOSED')) ||
                    (step.Status_Code__c == 'AWAITING_ORIGINALS' && stepStatus.contains('AWAITING_ORIGINALS')) ||
                    (step.Status_Code__c == 'CANCELLED' && stepStatus.contains('CANCELLED')) ||
                    (step.Status_Code__c == 'VERIFIED' && stepStatus.contains('VERIFIED')) ||
                    (step.Status_Code__c == 'POSTED' && stepStatus.contains('POSTED')) ||
                    (step.Status_Code__c == 'APPROVED' && stepStatus.contains('APPROVED')) ||
                    (step.Status_Code__c == 'REUPLOAD_DOCUMENT' && stepStatus.contains('REUPLOAD_DOCUMENT')) ||
                    (step.Status_Code__c == 'SCHEDULED' && stepStatus.contains('SCHEDULED')) ||
                    (step.Status_Code__c == 'RE-SCHEDULED' && stepStatus.contains('RE-SCHEDULED')) ||
                    (step.Status_Code__c == 'REQUIRE_ADDL_INFO' && stepStatus.contains('REQUIRE_ADDL_INFO')) ||
                    (step.Status_Code__c == 'REJECTED' && stepStatus.contains('REJECTED'))
                ) {
                    List < Email_Logs__c > emailLogs = new List < Email_Logs__c > ();

                    step__c stp = [select id, name,SR__r.Createdbyid,SR__r.Customer__r.Separate_Services__c, No_PR__c, Step_Notes__c, SAP_IDNUM__c, SR_Type_Step__c, Client_Name__c, Fine_Amount__c, SR_Temp_Menu__c, Biometrics_Required__c, Applicant_Name__c, SR_Template__c, SR_Menu_Text__c, Step_Name__c, SR__C, SR__r.Customer__c, SR__r.Customer__r.name, SR__r.Type_of_Request__c, SR__r.First_Name__c, Rejection_Reason__c, SR__r.Middle_Name__c, SR__r.Last_Name__c, SR__r.Name, SR__r.Service_Category__c, sr__r.Send_SMS_To_Mobile__c, sr__r.Fine_Amount_AED__c, sr__r.contact__r.name,Application_Reference__c, (select Id, Appointment_StartDate_Time__c, Appointment_EndDate_Time__c from Appointments__r order by LastModifiedDate desc limit 1) from step__c where id =: step.Id];

                    string TempCode = step.SR_Template__c;
                    string serviceCategory = stp.sr__r.Service_Category__c;
                    system.debug('TempCode' + TempCode);
                    
                       //v1.4
                    SeparateServices=stp.SR__r.Customer__r.Separate_Services__c;
                    ServiceCreatedBy=stp.SR__r.Createdbyid;

                    for (GSEmailTemplates__mdt emilTemplates: [select MasterLabel, Service_Types__c, Step_Status_Code__c, Email_Body__c, subject__c, Step_Name__c, Service_Category__c, SMS_body__c, SMS_Subject__c from GSEmailTemplates__mdt where Step_Name__c =: stp.Step_Name__c]) {

                        if (TempCode == 'Absconding') {

                            TempCode = TempCode + '1';

                        }

                        system.debug(emilTemplates.Service_Types__c + 'Service Types');

                        if ((emilTemplates.Service_Types__c.contains(TempCode) && emilTemplates.Step_Status_Code__c == step.Status_Code__c) || (emilTemplates.Service_Types__c.contains('GS_ALL') && emilTemplates.Step_Status_Code__c == step.Status_Code__c)) 
                        {
                            Email_Logs__c emLogs = new Email_Logs__c();
                            emLogs.name = stp.name;
                            string bodyofTemp = '';
                            string subjectTemp = '';
                            string smsBody = emilTemplates.SMS_body__c;
                            string smsSubject = emilTemplates.SMS_Subject__c;

                            if (!string.isBlank(smsBody)) {

                                if (smsBody.contains('{!Step__c.SR__c}')) smsBody = smsBody.replace('{!Step__c.SR__c}', stp.SR__r.name);

                                if (smsBody.contains('{!Step__c.Applicant_Name__c}')) {
                                    smsBody = smsBody.replace('{!Step__c.Applicant_Name__c}', stp.Applicant_Name__c);
                                }

                                if (smsBody.contains('{!Step__c.Appointment_StartDate_Time__c}')) {
                                    string AppDateTime = (stp.Appointments__r[0].Appointment_StartDate_Time__c).format('HH:mm:ss');
                                    smsBody = smsBody.replace('{!Step__c.Appointment_StartDate_Time__c}', AppDateTime);
                                }
                                if (smsBody.contains('{!Step__c.Appointment_StartDate}')) {
                                    string AppointDate = stp.Appointments__r[0].Appointment_StartDate_Time__c.format('dd-MM-yyyy');
                                    smsBody = smsBody.replace('{!Step__c.Appointment_StartDate}', AppointDate);
                                }
                                if (smsBody.contains('{!Step__c.SR_Menu_Text__c}')) {
                                    smsBody = smsBody.replace(' {!Step__c.SR_Menu_Text__c}', stp.SR_Menu_Text__c);
                                }
                                if (smsBody.contains('{!Step__c.Application_Reference__c}')) {
                                    smsBody = smsBody.replace('{!Step__c.Application_Reference__c}', stp.Application_Reference__c);
                                }

                            }

                            string mobileNo = '';

                            if (!string.isBlank(smsSubject)) {
                                mobileNo = stp.sr__r.Send_SMS_To_Mobile__c;
                                mobileNo = (stp.sr__r.Send_SMS_To_Mobile__c == '' || stp.sr__r.Send_SMS_To_Mobile__c == null) ? '' : stp.sr__r.Send_SMS_To_Mobile__c;
                                if (smsSubject.contains('{!step__c.sr__r.Send_SMS_To_Mobile__c}')) smsSubject = smsSubject.replace('{!step__c.sr__r.Send_SMS_To_Mobile__c}', mobileNo);
                            }

                            system.debug('emilTemplates' + emilTemplates);

                            string serviceCate = stp.sr__r.Service_Category__c;
                            system.debug(emilTemplates.Service_Category__c);
                            if (TempCode == 'Index_Card_Other_Services' && (emilTemplates.Service_Category__c != null && emilTemplates.Service_Category__c != '' && emilTemplates.Service_Category__c.contains(serviceCate))) {


                                bodyofTemp = emilTemplates.Email_Body__c;
                                subjectTemp = emilTemplates.subject__c;

                            } else if ((emilTemplates.Service_Category__c == null || emilTemplates.Service_Category__c == '') && emilTemplates.Service_Category__c != 'Non_Difc') {

                                if (stp.step_name__c == 'Emirates ID Registration is completed' && stp.no_pr__c == false) {
                                    bodyofTemp = emilTemplates.Email_Body__c;
                                    subjectTemp = emilTemplates.subject__c;
                                } else if (stp.step_name__c != 'Emirates ID Registration is completed') {
                                    bodyofTemp = emilTemplates.Email_Body__c;
                                    subjectTemp = emilTemplates.subject__c;
                          

                            }       }
                            /**V1.1 **/
                              /**  else if ((TempCode == 'Non_DIFC_Sponsorship_Visa_Renewal' || TempCode == 'Non_DIFC_Sponsorship_Visa_New_Transfer' || TempCode == 'Non_DIFC_Sponsorship_Visa_Amendment' || TempCode == 'Non_DIFC_Sponsorship_Visa_Cancellation' || TempCode == 'Non_DIFC_Sponsorship_Visa_New' || TempCode == 'Medical_Test_for_Non_DIFC_Sponsorship') && emilTemplates.Service_Category__c == 'Non_Difc'
                                        || TempCode == 'New_Visitor_Access_Card') {**/
                                //Added New_Visitor_Access_Card by shoaib based on #7402
                            else if(String.isBlank(bodyofTemp ) ||  String.isBlank(subjectTemp )) {     
                                    bodyofTemp = emilTemplates.Email_Body__c;
                                    subjectTemp = emilTemplates.subject__c;
                            }

                            if (subjectTemp.contains('{!Step__c.SR__c}')) {
                                subjectTemp = subjectTemp.replace('{!Step__c.SR__c}', stp.SR__r.name);
                            }

                            if (bodyofTemp != null || bodyofTemp != '') {

                                system.debug('inside ' + bodyofTemp);

                                if (bodyofTemp.contains('{!Step__c.SR__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.SR__c}', stp.SR__r.name);
                                }

                                string fineAmount = '';

                                if (bodyofTemp.contains('{!Step__c.SR__r.Fine_Amount_AED__c}')) {

                                    if (!(string.valueOf(stp.Fine_Amount__c) == '' || string.valueOf(stp.Fine_Amount__c) == null)) {
                                        bodyofTemp = bodyofTemp.replace('{!Step__c.SR__r.Fine_Amount_AED__c}', string.valueOf(stp.Fine_Amount__c));
                                    } else {
                                        fineAmount = (string.valueOf(stp.SR__r.Fine_Amount_AED__c) == '' || string.valueOf(stp.SR__r.Fine_Amount_AED__c) == null) ? '0' : string.valueOf(stp.SR__r.Fine_Amount_AED__c);
                                        bodyofTemp = bodyofTemp.replace('{!Step__c.SR__r.Fine_Amount_AED__c}', fineAmount);

                                    }
                                }

                                string ApplicantName = '';
                                if (bodyofTemp.contains('{!Step__c.Applicant_Name__c}')) {

                                    ApplicantName = stp.Applicant_Name__c;

                                    if (ApplicantName == '' || ApplicantName == null) {

                                        ApplicantName = stp.sr__r.Contact__r.name;
                                    }

                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Applicant_Name__c}', ApplicantName);
                                }

                                if (bodyofTemp.contains('{!step.SR__r.Customer__r.name}')) {
                                    bodyofTemp = bodyofTemp.replace('{!step.SR__r.Customer__r.name}', stp.SR__r.Customer__r.name);
                                }

                                if (bodyofTemp.contains('{!Step__c.Appointment_StartDate_Time__c}')) {
                                    string AppDateTime = (stp.Appointments__r[0].Appointment_StartDate_Time__c).format('HH:mm:ss');

                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Appointment_StartDate_Time__c}', AppDateTime);
                                }

                                if (bodyofTemp.contains('{!Step__c.Appointment_StartDate}')) {

                                    string AppointDate = stp.Appointments__r[0].Appointment_StartDate_Time__c.format('dd-MM-yyyy');

                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Appointment_StartDate}', AppointDate);

                                }

                                if (bodyofTemp.contains('{!Step__c.Biometrics_Required__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Biometrics_Required__c}', stp.Biometrics_Required__c);
                                }

                                if (bodyofTemp.contains('{!Step__c.Service_Type__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Service_Type__c}', stp.SR_Type_Step__c);
                                }

                                if (bodyofTemp.contains(' {!Step__c.SR_Menu_Text__c}')) {
                                    bodyofTemp = bodyofTemp.replace(' {!Step__c.SR_Menu_Text__c}', stp.SR_Menu_Text__c);
                                }

                                if (bodyofTemp.contains('{!Step__c.Client_Name__c}')) {
                                    bodyofTemp = bodyofTemp.replace(' {!Step__c.Client_Name__c}', stp.Client_Name__c);
                                }

                                if (bodyofTemp.contains('{!Step__c.SR_Temp_Menu__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.SR_Temp_Menu__c}', stp.SR_Temp_Menu__c);
                                }

                                if (bodyofTemp.contains('{!Step__c.Rejection_Reason__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Rejection_Reason__c}', stp.Rejection_Reason__c);
                                }

                                if (bodyofTemp.contains('{!$Label.Community_URL}')) {
                                    string Label = system.label.Community_URL;
                                    bodyofTemp = bodyofTemp.replace('{!$Label.Community_URL}', Label);
                                }

                                if (bodyofTemp.contains('{!Step__c.SRId__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.SRId__c}', stp.SR__c);
                                }
                                string IDNumber = '';

                                if (bodyofTemp.contains('{!Step__c.SAP_IDNUM__c}')) {

                                    IDNumber = (stp.SAP_IDNUM__c == '' || stp.SAP_IDNUM__c == null) ? '' : stp.SAP_IDNUM__c;
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.SAP_IDNUM__c}', IDNumber);
                                }

                                if (bodyofTemp.contains('{!Step__c.Step_Notes__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Step_Notes__c}', stp.Step_Notes__c);
                                }
                                if (bodyofTemp.contains('{!Step__c.Application_Reference__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Application_Reference__c}', stp.Application_Reference__c);
                                }
                                
                                 //v1.6 -- srart
                                
                                string TypeofRequest ='';
                                
                                if(bodyofTemp.contains('{!Step__c.Sr__r.Type_of_Request__c}')){
                                    
                                     TypeofRequest = (stp.Sr__r.Type_of_Request__c == '') ? '' : stp.Sr__r.Type_of_Request__c;
                                    
                                     bodyofTemp = bodyofTemp.replace('{!Step__c.Sr__r.Type_of_Request__c}', TypeofRequest);
                                    
                                }
                                
                                // end

                            }

                            emLogs.subject__c = subjectTemp;
                            emLogs.Email_Body__c = bodyofTemp;
                            emLogs.Service_Request__c = stp.sr__c;
                            emLogs.step__c = stp.id;
                            emLogs.SMS_body__c = smsBody;
                            emLogs.SMS_Subject__c = smsSubject;
                            
                               if(SeparateServices)
                            {
                                emLogs.Portal_User_1__c =ServiceCreatedBy;
                                
                            }
                            else
                            {

                            //List < User > usr = [select id from user where Contact.AccountId =: stp.SR__r.Customer__c and IsActive = true and Community_User_Role__c INCLUDES('Employee Services')];
                            List<User> usr            = new List<User>();
                            Set<String> contactIds    = new Set<String>(); 
                            Map<String,AccountContactRelation> accountContactMap = new  Map<String,AccountContactRelation>();
                         
                            for(AccountContactRelation ACR: [Select Id,contactId,Roles from AccountContactRelation where AccountId =: stp.SR__r.Customer__c and  isActive = true ]){
                                contactIds.add(ACR.contactId);
                                accountContactMap.put(ACR.contactId,ACR);
                            }
 
                            for(User thisUser : [select Id,ContactId,Community_User_Role__c FROM User where ContactId IN : contactIds and IsActive = true AND ID !=: ServiceCreatedBy]){
                                if(accountContactMap.containsKey(thisUser.ContactId) &&
                                        accountContactMap.get(thisUser.ContactId).Roles != null &&
                                        accountContactMap.get(thisUser.ContactId).Roles.Contains('Employee Services')){
                                          
                                          usr.add(thisUser);
                                     }
                            }

                           // if (usr.size() > 0)
                               emLogs.Portal_User_1__c = [SELECT Id FROM User WHERE Id =: stp.SR__r.Createdbyid AND Community_User_Role__c != null].size()> 0 ?
                                                        [SELECT Id FROM User WHERE Id =: stp.SR__r.Createdbyid AND Community_User_Role__c != null].Id : null;
                            if (usr.size() > 0)
                                emLogs.Portal_User_2__c = usr[0].id;
                            if (usr.size() > 1)
                                emLogs.Portal_User_3__c = usr[1].id;
                            if (usr.size() > 2)
                                emLogs.Portal_User_4__c = usr[2].id;
                            if (usr.size() > 3)
                                emLogs.Portal_User_5__c = usr[3].id;
                                }

                            if ((emilTemplates.subject__c != null || emilTemplates.subject__c != '') && (emilTemplates.Email_Body__c != null || emilTemplates.Email_Body__c != '')){
                                emailLogs.add(emLogs);
                            }
                        }
                    }
                    try {
                        if (emailLogs.size() > 0){
                            insert emailLogs;
                        }
                    } catch (Exception ex) {
                        Log__c objLog = new Log__c( Type__c = 'email Log record creation failed', Description__c = ex.getMessage() );
                        insert objLog;
                    }
                }
            }
        }
        // Start - V1.6
        if(step.Sys_SR_Group__c == 'Other'){
            
            GSStepEmailLogCreationHandler_Refund.CreateEmailLogOnUpdate(step);
        }
        
        // End - V1.6
    }
    //Added By Shoaib
     /*
     * Description :If both access card and completed step is pesent send email on closure of access card
     */
    public static Boolean StopEmailForAccessCardStep(String srId,step__c newStep){
        
        Boolean isAccessClosed = false;
    
        step__c currentStep = [SELECT Id,
                                        Step_Name__c,
                                        Step_Status__c
                                FROM step__c
                                WHERE Id =:newStep.Id];
        
        if((currentStep.Step_Name__c =='Passport is ready for collection'|| currentStep.Step_Name__c =='Completed') 
             &&  currentStep.Step_Status__c =='Closed'
             &&  [SELECT Id,Name,Step_Status__c FROM step__c WHERE Step_Name__c ='Access Card' AND SR__c=:srId LIMIT 1].SIZE() > 0){
             
                 isAccessClosed = true;
           
          }
         return isAccessClosed; 
        }
}