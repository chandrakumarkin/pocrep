/*
    Author      : Leeba
    Date        : 1-April-2020
    Description : Test class for CC_WithdrawApplication
    --------------------------------------------------------------------------------------
*/
@isTest
public class CC_WithdrawApplicationTest{

       public static testMethod void CC_WithdrawApplication() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       Opportunity opp = new Opportunity();
       opp.Name = 'test';
       opp.Stagename = 'Created';
       opp.closedate = system.today();
       opp.AccountId = acc.id;
       insert opp;
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.opportunity__c = opp.id;
        insert objHexaSR;
        
        HexaBPM__Status__c objhexastatus = new HexaBPM__Status__c();
        objhexastatus.HexaBPM__Type__c = 'Intermediate';
        objhexastatus.HexaBPM__Code__c = 'REQUEST_WITHDRAWN';
        insert objhexastatus;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        objHexastep.HexaBPM__Status__c = objhexastatus.id;
        insert objHexastep;
        
        Test.startTest();
        HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Opportunity__c
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_WithdrawApplication CC_WithdrawApplicationObj = new CC_WithdrawApplication();
        CC_WithdrawApplicationObj.EvaluateCustomCode(objHexaSR,step); 
        Test.stopTest();
    }

}