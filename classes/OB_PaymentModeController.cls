/**
    *Author : Rajil Ravindran
    *Description : This controller is used to process the payment related to Name Reservation and Self Registration.  
    *Created Date : 16.10.2019
    Merul   v1.0  Changes in createReceiptForCard
    Merul   v2.0  changes for namerenewal in loadPriceItem.
    DUrga   v3.0  Exclude the Price Items which are Marked as Non-Deductable for payment
    Maurf   v4.0 Added profile name to DisplayRecords record wrapper and getTopUpBalanceAmount
    Sai     V4.1   DIFC-DOB Tasks Ticket 13
    prateek v5.0  exclude a non deductable price item
    
----------------------------------------------------------------------
Version     Date                Author          Remarks                                                 
=======     ==========          =============   ==================================
v2.0        10-May-2021             Arun           Fixed Contractor issue 

**/   
public without sharing class OB_PaymentModeController {
        public static decimal PSADeposit = 0;
        public static decimal PSAAvailable = 0;
        public static decimal dRecpNotSentAmount = 0;
        public static Boolean isFitOutContractor = false;
        public static Date LastUpdatedDate;
        public static Boolean BalanceFetchSuccessfull = false;
        public static Boolean hasEstablishmentCard = false; // V1.6 - Claude - Added exemption for new establishment card
        public static Boolean isUpdate = false;
    
    @AuraEnabled
    public static Account getBpNumberFromAccount(){
        try{
            system.debug('====OB_PaymentModeController=====');
            DateTime LastUpdatedDateTime;
            String loggedInUser = UserInfo.getUserId();
            list<User> userList = [select contactid,accountid,contact.Contractor__c from user where id =:loggedInUser];
            Account accObj;
            system.debug('====userList====='+userList);
            if(userList !=  null && userList.size() != 0){
                if(userList[0].contact.Contractor__c != null) {
                    accObj = [SELECT Name, BP_No__c,Balance_Updated_On__c FROM Account where id =: userList[0].contact.Contractor__c];
                }else if(userList[0].accountId != null) {
                    accObj = [SELECT Name, BP_No__c,Balance_Updated_On__c FROM Account where id =: userList[0].accountid];
                }else{
                    return null;
                }
                    
            }else{
                return null;
            } 
            system.debug('===accObj=='+accObj);
            return accObj;
        }catch(Exception e){
            return null;
        }
    }
    @AuraEnabled
    public static Receipt createReceiptForCard(String reqWrapPram ) 
    {
        Account acc = OB_QueryUtilityClass.getAccountRec( UserInfo.getUserId() );
        Receipt receiptObj = new Receipt();
       
        RequestWrapper reqWrap = (OB_PaymentModeController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_PaymentModeController.RequestWrapper.class);
        system.debug('==reqWrap===='+reqWrap);
        receiptObj.referenceNo = OB_PaymentModeController.createReceiptRecord(reqWrap);
        receiptObj.accRec = acc;

        return receiptObj;
    }
    public static string createReceiptRecord(RequestWrapper reqWrap){
        system.debug('=====reqWrap===='+reqWrap);
        string accountId;
        boolean isFitOutContractor = false;
        for(User usr : [Select Contact.AccountId,contact.Contractor__c,Contact.Account.RecordType.DeveloperName from User where Id =:UserInfo.getUserId()]){
            //accountId = usr.Contact.AccountID;
            if(usr.contact.Contractor__c != null){
                isFitOutContractor = (String.isNotBlank(usr.Contact.Contractor__c) && !usr.Contact.Contractor__c.equals(usr.Contact.AccountId) ) || 
                                usr.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account');
            }
            accountId = isFitOutContractor && String.isNotBlank(usr.Contact.Contractor__c) ? usr.Contact.Contractor__c : usr.Contact.AccountId;
        }
        Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get(reqWrap.paymentType).getRecordTypeId();
        //Create a receipt record with values entered by the customer
        Receipt__c receiptObj = new Receipt__c();
        receiptObj.Customer__c=accountId;
        if(String.isNotBlank(reqWrap.srId))
            receiptObj.OB_Application__c = reqWrap.srId;
        receiptObj.Amount__c=Decimal.valueof(reqWrap.amount); 
        receiptObj.Receipt_Type__c=reqWrap.paymentType;
        receiptObj.Transaction_Date__c=system.now(); 
        receiptObj.RecordTypeId=rt;
        receiptObj.Payment_Status__c=reqWrap.receiptStatus;
        insert receiptObj; 
        

        return receiptObj.Id;
        
    }
     @AuraEnabled
    public static string getRecordTypeId(){
        String recordTypeID = Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Cheque').getRecordTypeId();
        return recordTypeID;
    }
    
  
    @AuraEnabled
    public static PriceItem loadPriceItem(string srId, string category, boolean isBalanceRequired){
        
        //ResponseWrapper resp = new ResponseWrapper();
        PriceItem pI = new PriceItem();
        decimal totalPrice = 0;
        decimal totalPriceInAED = 0;
        
        string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        // Merul:  v 2.0  changes for namerenewal in loadPriceItem
        // adding  category == 'namerenewal'
        if(String.isNotBlank(category) && ( category == 'namereserve' ||  category == 'namerenewal' ) )
        {
            for(HexaBPM__SR_Price_Item__c priceItem :[select HexaBPM__Product__r.Name,Pricing_Line_Name__c,Total_Price_USD__c,Total_Price_AED__c from HexaBPM__SR_Price_Item__c 
                                                where HexaBPM__ServiceRequest__r.HexaBPM__Customer__c = :accountId 
                                                and HexaBPM__ServiceRequest__c = :srId  
                                                and HexaBPM__Product__r.ProductCode = 'OB Name Reservation'
                                                //and HexaBPM__Pricing_Line__r.Non_Deductible__c=false
                                                and HexaBPM__Status__c = 'Added' limit 1]){
                totalPrice = priceItem.Total_Price_USD__c;
                totalPriceInAED = priceItem.Total_Price_AED__c;
                ProductInfo prodObj = new ProductInfo();
                prodObj.name = priceItem.Pricing_Line_Name__c;
                prodObj.amount = priceItem.Total_Price_USD__c;
                prodObj.amountInAED = priceItem.Total_Price_AED__c;
                pI.lstProductInfo.add(prodObj);
            }
        }
        else
        {
            for(HexaBPM__SR_Price_Item__c srPriceItem : [select HexaBPM__Product__r.Name,Pricing_Line_Name__c, Total_Price_USD__c,Total_Price_AED__c,HexaBPM__Pricing_Line__r.Non_Deductible__c,HexaBPM__Pricing_Line__r.Name from HexaBPM__SR_Price_Item__c 
                                                            where HexaBPM__ServiceRequest__r.HexaBPM__Customer__c = :accountId 
                                                            and HexaBPM__ServiceRequest__c = :srId
                                                            and HexaBPM__Status__c = 'Added'  and HexaBPM__Product__r.ProductCode != 'OB Name Reservation']){
                if(!srPriceItem.HexaBPM__Pricing_Line__r.Non_Deductible__c ||
                (srPriceItem.HexaBPM__Pricing_Line__r.Non_Deductible__c && 
                srPriceItem.HexaBPM__Pricing_Line__r.Name== 'Self Registration Advance') ){//v3.0 //v5.0
                    totalPrice = totalPrice + srPriceItem.Total_Price_USD__c;
                    totalPriceInAED = totalPriceInAED + srPriceItem.Total_Price_AED__c;
                    //totalPriceInAED = srPriceItem.Total_Price_AED__c;
                    ProductInfo prodObj = new ProductInfo();
                    prodObj.name = srPriceItem.Pricing_Line_Name__c;
                    prodObj.amount = srPriceItem.Total_Price_USD__c;
                    prodObj.amountInAED = srPriceItem.Total_Price_AED__c;
                    pI.lstProductInfo.add(prodObj);
                }
            }
                     
        }
        pI.amount = totalPrice;
        pI.amountInAED = totalPriceInAED;
        pI.receiptRecordTypeId = OB_PaymentModeController.getRecordTypeId();
        
        pi.mapCountry = getCountryValues();
        if(isBalanceRequired){
            OB_PaymentModeController.DisplayRecords portalBalanceRecord = new OB_PaymentModeController.DisplayRecords();
            portalBalanceRecord = OB_PaymentModeController.getTopUpBalanceAmount();
            if(portalBalanceRecord != null && portalBalanceRecord.portalBalanceValue != null){
                pI.totalBalance = portalBalanceRecord.portalBalanceValue;
            }
        }
        
        return pI;
    }

    //This method is getting invoked from payWallet method in OB_PaymentMode Component 
    @AuraEnabled
    public static OB_PaymentController.ProductInfo updatePriceItem(string srId, string category){
        
        OB_PaymentController.ProductInfo productObj = new OB_PaymentController.ProductInfo();
        try{
            if(String.isNotBlank(category) && category == 'namereserve'){
                productObj = OB_PaymentController.createNameReservationSR(srId);
            }
            else{
                productObj = OB_PaymentController.updateSRLines(srId);
            }
        } catch (DMLException e){
            productObj.isError = true;
            string DMLError = e.getdmlMessage(0) + '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
            productObj.errorMessage = DMLError;
            System.debug('=====>'+ e.getMessage());
        }
        
        return productObj;
    }
    
    @AuraEnabled
    public static Id getRecTypeId(){
        Id recid = Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Cheque').getRecordTypeId();        
        return recid;
    }
    @AuraEnabled
    public static DisplayRecords getTopUpBalanceAmount()
    {
        DisplayRecords wrapObj = new DisplayRecords();
        isFitOutContractor = false;
        decimal PortalBalance = 0;
        decimal dSRPriceItemNotSentAmt = 0;
        string customerId;
        list<string> accountList = new list<string>();
        DateTime LastUpdatedDateTime;
        string loggedInUser = UserInfo.getUserId();
        list<User> userList = [SELECT ContactId,
                                      Contact.AccountId,
                                      AccountId,
                                      contact.Contractor__c,
                                      Contact.OB_Block_Homepage__c,
                                      Contact.Account.RecordType.DeveloperName,
                                      Profile.Name
                                 FROM user
                                WHERE Id =:loggedInUser];
        for(User userObj : userList)
        {
            // displayComponent
            wrapObj.profileName = userObj.Profile.Name;
            wrapObj.userAccountId = userObj.Contact.AccountId;
            wrapObj.displayComponent = (userObj.Contact.OB_Block_Homepage__c ? false : true);       
            
            if(userObj.contact.Contractor__c != null){
                isFitOutContractor = (String.isNotBlank(userObj.Contact.Contractor__c) && !userObj.Contact.Contractor__c.equals(userObj.Contact.AccountId) ) || 
                                userObj.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account');
            }
            system.debug('@@@@@@@@@@@@@@@ isFitOutContractor '+isFitOutContractor);

            customerId = isFitOutContractor && String.isNotBlank(userObj.Contact.Contractor__c) ? userObj.Contact.Contractor__c : userObj.Contact.AccountId;
            system.debug('@@@@@@@@@@@@@@@ customerId '+customerId);
            if(userObj.contact.Contractor__c != null){
                accountList.add(userObj.contact.Contractor__c);             
            }else{
                if(String.isNotBlank(userObj.Contact.AccountId))
                    accountList.add(userObj.Contact.AccountId);
            }
        }
        if((LastUpdatedDate == null || (LastUpdatedDate.day() != system.today().day() || LastUpdatedDate.month() != system.today().month()))){
            
            AccountBalanceInfoCls.getAccountBalance(accountList, '5000');
            
            System.debug('Balance is Updated.');
            
            isUpdate = true;
        }
        for(Account objAccount : [select Id,Index_Card_Status__c,Name,Portal_Balance__c,PSA_Deposit__c,Balance_Updated_On__c from Account where Id=:accountList]){
            
            LastUpdatedDate = (objAccount.Balance_Updated_On__c != null) ? Date.valueOf(objAccount.Balance_Updated_On__c) : system.today();
            
            PortalBalance = (objAccount.Portal_Balance__c != null) ? objAccount.Portal_Balance__c : 0;
            
            PSADeposit = (objAccount.PSA_Deposit__c != null) ? objAccount.PSA_Deposit__c : 0;
            
            LastUpdatedDateTime = (objAccount.Balance_Updated_On__c != null) ? Date.valueOf(objAccount.Balance_Updated_On__c) : system.now();
            
            //V1.6 - Claude - Start
            hasEstablishmentCard = String.isNotBlank(objAccount.Index_Card_Status__c) && !objAccount.Index_Card_Status__c.equals('Not Available') && !objAccount.Index_Card_Status__c.equals('Cancelled');
            //V1.6 - Claude - End
        }
        
        if(LastUpdatedDateTime != null){
            wrapObj.balanceTime = LastUpdatedDateTime.format('HH:mm');
        }
        /* V1.4 - Claude - Start - New Query to distinguish fit-out from the rest */
        String accountFilter = isFitOutContractor ? ' ServiceRequest__r.SR_Group__c = \'Fit-Out & Events\' AND ServiceRequest__r.Contractor__c = ' : ' ServiceRequest__r.SR_Group__c != \'Fit-Out & Events\' AND ServiceRequest__r.Customer__c = ';
        //string accountFilter = isFitOutContractor ? 'ServiceRequest__r.Contractor__c = ' : 'ServiceRequest__r.Customer__c = ' ;
        String priceItemQuery = 'select Price__c,Total_Service_Amount__c from SR_Price_Item__c where ServiceRequest__c!=null and Price__c!=null and '+accountFilter+' \''+CustomerId+'\' and (Status__c=\'Blocked\' OR Status__c=\'Consumed\') and Add_in_Account_Balance__c=true AND Excluded_Price_Item__c = false';// Add_in_Account_Balance__c is a field that validates whether or not the price item should be counted for Account balance check V1.9
            
        //string priceItemQuery = 'select Price__c,Total_Service_Amount__c from SR_Price_Item__c where ServiceRequest__c!=null and Price__c!=null and '+accountFilter+' \''+accountList+'\' and (Status__c=\'Blocked\' OR Status__c=\'Consumed\') and Add_in_Account_Balance__c=true AND Excluded_Price_Item__c = false';// Add_in_Account_Balance__c is a field that validates whether or not the price item should be counted for Account balance check V1.9
        /* V1.4 - Claude - End */
        system.debug('accountList>>>>>'+accountList);
        system.debug('customerId>>>>>'+customerId);
        system.debug('====priceItemQuery======'+priceItemQuery);
        system.debug('accountList.size()>>>>'+accountList.size());
        if(accountList != null && accountList.size()>0){
             
            for(SR_Price_Item__c objSRPriceItem : Database.query(priceItemQuery)){
                dSRPriceItemNotSentAmt += objSRPriceItem.Total_Service_Amount__c; // added as part of vat
            }
            
            List<String> productCodeList = Label.OB_ProductCODE.split(',');
            for(HexaBPM__SR_Price_Item__c SRP:[Select Id,Total_Price_USD__c,Total_Price_AED__c, Total_Service_Amount__c 
                                                from HexaBPM__SR_Price_Item__c 
                                                where HexaBPM__ServiceRequest__r.HexaBPM__Customer__c IN:accountList
                                                 and (HexaBPM__Status__c='Blocked' OR HexaBPM__Status__c='Consumed')
                                                 and HexaBPM__Product__c != null and HexaBPM__Pricing_Line__r.Non_Deductible__c =false
                                                 and HexaBPM__Product__r.ProductCode Not IN:  productCodeList ])
            {
                if(SRP.Total_Price_AED__c != null) //Total_Service_Amount__c
                    dSRPriceItemNotSentAmt += SRP.Total_Price_AED__c;
            }
        }
        
        
        system.debug('PortalBalance is : '+PortalBalance);
        system.debug('dSRPriceItemNotSentAmt is : '+dSRPriceItemNotSentAmt);
        
        PortalBalance = PortalBalance - dSRPriceItemNotSentAmt;
        
        for(Receipt__c objReceipt : [select Amount__c from Receipt__c where Customer__c=:accountList and Amount__c!=null and Payment_Status__c='Success' and Pushed_to_SAP__c=false]){
            PortalBalance += objReceipt.Amount__c;
        }
        System.debug('PortalBalance>>>>'+PortalBalance);
        try{
            if(userList!=null && userList.size()>0)
                PortalBalance -= RefundRequestUtils.getRefundAmount(userList[0].Contact.AccountId);  // V1.10 
               
        }catch(Exception e){
            
        }
        PSAAvailable = (PSADeposit>= 125000) ? 125000 :  PSACalculation(hasEstablishmentCard, accountList);//( PSADeposit - (dAvaPSA + (VisaIssued*2500)) );
        system.debug('PSAAvailable is : '+PSAAvailable);
        wrapObj.portalBalance = formatNumber(PortalBalance);
        wrapObj.portalBalanceValue = PortalBalance;
        BalanceFetchSuccessfull = true;
        return wrapObj;
    }
      /* ---------------------------------------------------------------------
     * Method name : getCurrentAccountId
     * description : used to get current account Id
     * --------------------------------------------------------------------*/
    
    @AuraEnabled
    public static user getCurrentAccountId(){
        User currentUser = new User();
        currentUser = [SELECT Id,Contact.AccountId,Contact.Account.Name FROM User WHERE Id =: UserInfo.getUserId()];
        return currentUser;
    }
    
    @Auraenabled
    public static decimal PSACalculation(Boolean hasEstablishmentCard,list<string> accountList){
        
        list<string> lstPSARecTypes = new list<string>{'DIFC_Sponsorship_Visa_New','Employment_Visa_from_DIFC_to_DIFC','Employment_Visa_Govt_to_DIFC','Visa_from_Individual_sponsor_to_DIFC'};
        
        map<Id,Service_Request__c> mapOpenSRs = new map<Id,Service_Request__c>([select Id,External_Status_Name__c from Service_Request__c where (NOT Occupation_GS__r.Name like '%STUDENT%') AND Customer__c=:accountList AND Record_Type_Name__c IN : lstPSARecTypes AND isClosedStatus__c != true AND Is_Rejected__c != true AND External_Status_Name__c != 'Cancelled' AND External_Status_Name__c != 'Rejected' AND External_Status_Name__c != 'Draft']);
        
        map<Id,Contact> mapActiveCons = new map<Id,Contact>([select Id from Contact where (NOT Job_Title__r.Name like '%STUDENT%') AND Id IN (select Object_Contact__c from Relationship__c where Subject_Account__c =:accountList AND Active__c = true AND Relationship_Type__c='Has DIFC Sponsored Employee')]);
        
        Integer iEmpCount = (mapActiveCons != null && !mapActiveCons.isEmpty()) ? mapActiveCons.size() : 0 ;
        
        system.debug('iEmpCount is : '+iEmpCount);
        
        if(!mapOpenSRs.isEmpty()) iEmpCount += mapOpenSRs.size();
        
        system.debug('iEmpCount is : '+iEmpCount);
        system.debug('PSADeposit is : '+PSADeposit);
        
        //V1.6 - Claude - Start
        if(hasEstablishmentCard) iEmpCount++;
        //V1.6 - Claude - End
        
        for(SR_Price_Item__c objSRItem : [select Id,Price__c from SR_Price_Item__c where 
                                                                                   Count_in_PSA__c = TRUE AND //V1.7 - Claude - Added PSA flag for SR Groups
                                                                                   ServiceRequest__r.Customer__c =: accountList AND (Status__c='Blocked' OR Status__c='Consumed') AND Pricing_Line__r.Product__r.Name = 'PSA']){ //AND ServiceRequest__r.External_Status_Name__c = 'Submitted'
            //dAvaPSA -= objSRItem.Price__c;
            PSADeposit += objSRItem.Price__c;
        }
        
        system.debug('PSADeposit is : '+PSADeposit);
        
          
        
        decimal dAvaPSA = (PSADeposit-(iEmpCount*2500)); // V1.6 - Claude - Added fixed 2500 as 'Establishment Card' // V1.10
        
        system.debug('dAvaPSA is : '+dAvaPSA);
        
        return dAvaPSA;
    }
    
    /* ---------------------------------------------------------------------
     * Method name : getButtonAction
     * description : used to perform button action and return a wrapper
     * --------------------------------------------------------------------*/
    @AuraEnabled
    public static String getSRLinesAmount(string SRID){ 
        list<SRLine> lines = new list<SRLine>();
        list<HexaBPM__SR_Price_Item__c> srPriceItems = [SELECT id,Total_Price_USD__c,HexaBPM__Product__r.Name,HexaBPM__Pricing_Line__r.Non_Deductible__c FROM HexaBPM__SR_Price_Item__c WHERE HexaBPM__ServiceRequest__c =: SRID AND HexaBPM__Status__c = 'Added'];
        for(HexaBPM__SR_Price_Item__c sri : srPriceItems){
            if(!sri.HexaBPM__Pricing_Line__r.Non_Deductible__c){//v3.0
                SRLine line = new SRLine();
                line.product = sri.HexaBPM__Product__r.Name;
                line.price = sri.Total_Price_USD__c;
                lines.add(line);
            }
           // totalPrice += sri.Total_Price_USD__c;
        }
        return json.serialize(lines);
    }
    /* ---------------------------------------------------------------------
     * Method name : createCheque
     * description : Creates Receipt of type cheque
     * --------------------------------------------------------------------*/
    @AuraEnabled
    public static Receipt createReceiptForCheque(Receipt__c receipt){ 
        Receipt receiptObj = new Receipt();
        string accountId;
        boolean isFitOutContractor = false;
        for(User usr : [Select Contact.AccountId,contact.Contractor__c,Contact.Account.RecordType.DeveloperName from User where Id =:UserInfo.getUserId()]){
            if(usr.contact.Contractor__c != null){
                isFitOutContractor = (String.isNotBlank(usr.Contact.Contractor__c) && !usr.Contact.Contractor__c.equals(usr.Contact.AccountId) ) || 
                                usr.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account');
            }
            accountId = isFitOutContractor && String.isNotBlank(usr.Contact.Contractor__c) ? usr.Contact.Contractor__c : usr.Contact.AccountId;
        }
        try{
            receipt.Customer__c = accountId;
            receipt.Bank_Name__c = receipt.OB_Cheque_Number__c + '-' + receipt.OB_Bank_Name__c;
            receipt.recordTypeId = getRecordTypeId();
            receipt.Payment_Status__c = 'Pending';
            insert receipt;
            for(Receipt__c obj: [select name from Receipt__c where id = :receipt.id])
                receiptObj.referenceNo = obj.Name;
        }
        catch(System.DmlException  ex){
            receiptObj.errorMessage = ex.getDmlMessage(0);
        }
        return receiptObj;
    }
    @AuraEnabled
    public static map <string,string> getCountryValues() {
        map<string,string> mapCountryValues = new map<string,string>();
        for(Country_Codes__c countryCodeObj : [select Code__c,Name from Country_Codes__c order by Name asc]){
            mapCountryValues.put(countryCodeObj.Code__c,countryCodeObj.Name);
        }
        /*
        HexaBPM__Service_Request__c serviceRequest = new HexaBPM__Service_Request__c();
        sObject objObject = serviceRequest;
        string fld = 'Country__c';
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        System.debug('objType is' + objType);
        // Describe the SObject using its object type.
        
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        System.debug('objDescribe is' + objDescribe);
        System.debug('Result is' + objObject.getSObjectType().getDescribe().fields.getMap());
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        System.debug('fieldMap is' + fieldMap);
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values = fieldMap.get(fld).getDescribe().getPickListValues();
        System.debug('values are '+values);
        // Add these values to the select option list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        */
        return mapCountryValues;
    }
    public static List<string> getCountries()
    {
        List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Contact.MailingCountry.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList;
    }  
    private static string formatNumber(decimal PortalBalance){
        
        List<String> args = new String[]{'0','number','###,###,###,##0.00'};
        string PortalBalanceFormatted = String.format(PortalBalance.format(), args); 
        if(String.isNotBlank(PortalBalanceFormatted) && PortalBalanceFormatted.indexOf('.') < 0)
             PortalBalanceFormatted = PortalBalanceFormatted + '.00';
        else if(String.isNotBlank(PortalBalanceFormatted) && PortalBalanceFormatted.right(2).contains('.'))
             PortalBalanceFormatted = PortalBalanceFormatted + '0';
        return PortalBalanceFormatted;
    }
    
    
    
      //v6.0
    @AuraEnabled
    public static String[] prepareCardRequest(Decimal amount,String srId){
    
    
    //v2.0 Arun : 10 May 2021 Added 
        string accountId;
        boolean isFitOutContractor = false;
        for(User usr : [Select Contact.AccountId,contact.Contractor__c,Contact.Account.RecordType.DeveloperName from User where Id =:UserInfo.getUserId()]){
            //accountId = usr.Contact.AccountID;
            if(usr.contact.Contractor__c != null){
                isFitOutContractor = (String.isNotBlank(usr.Contact.Contractor__c) && !usr.Contact.Contractor__c.equals(usr.Contact.AccountId) ) || 
                                usr.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account');
            }
            accountId = isFitOutContractor && String.isNotBlank(usr.Contact.Contractor__c) ? usr.Contact.Contractor__c : usr.Contact.AccountId;
        }
        
        
        
        String receiptCardRecTypeId = Schema.SobjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
        User[] usr = [ Select Id,Name,Contact.AccountId From User Where Id = :UserInfo.getUserId() ];
        
        Receipt__c receiptObj = new Receipt__c(
            RecordTypeId = receiptCardRecTypeId,
            Amount__c = amount,
            //Card_Type__c = 'Visa',
            Payment_Status__c = 'Pending',
            Receipt_Type__c = 'Card',
            Transaction_Date__c = System.now(),
            Customer__c =AccountId
        );
        
        if(String.isNotBlank(srId) && srId != 'null' ){
           // receiptObj.Service_Request__c = srId; //For CP(Commercial Permission) Accounts
        }
        
        insert receiptObj;
        
        
        //ADCB
        OB_PaymentModeController.CardPaymentRequestWrapper c = new OB_PaymentModeController.CardPaymentRequestWrapper();
        c.access_key           = Label.ADCB_Portal_accesskey;
        c.profile_id           = Label.ADCB_Portal_profile_id;
        c.transaction_uuid     = getUUID();
        c.customer_ip_address  = ( Test.isRunningTest() ? '' : Auth.SessionManagement.getCurrentSession()?.get('SourceIp') );
        c.signed_date_time     = getUTCDateTime();
        if(String.isNotBlank(srId) && srId != 'null'){
            c.reference_number     = srId+'_'+receiptObj.Id+'_CPSR';   //For CP-Account Initial SR Payment//
        }else{
            c.reference_number     = receiptObj.Id+'_TOPUP';   //For Topup Only//
        }
        c.amount               = String.valueOf(amount);
        c.currencyvall         = 'aed';
        c.unsigned_field_names = '';
        
        
        //AuthSession au = [SELECT Id, SourceIp FROM AuthSession WHERE UsersId = :UserInfo.getUserId() ORDER BY CreatedDate DESC LIMIT 1];
        //System.debug('u-->'+au);
        //System.debug('ZZZ Ip-->AuthSession IP-->'+au.SourceIp);
        System.debug('ZZZ Ip-->c.customer_ip_address-->'+c.customer_ip_address);
        
        c.oPassedParams.put('access_key',c.access_key);
        c.oPassedParams.put('profile_id',c.profile_id);
        c.oPassedParams.put('transaction_uuid',c.transaction_uuid);
        c.oPassedParams.put('customer_ip_address',c.customer_ip_address);
        c.oPassedParams.put('signed_field_names','access_key,customer_ip_address,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency');
        c.oPassedParams.put('unsigned_field_names',c.unsigned_field_names);
        c.oPassedParams.put('signed_date_time',c.signed_date_time);
        c.oPassedParams.put('locale','en');
        c.oPassedParams.put('transaction_type','sale');
        c.oPassedParams.put('reference_number',c.reference_number);
        c.oPassedParams.put('amount',c.amount);
        c.oPassedParams.put('currency',c.currencyvall);
        
        
        String secretKey = Label.ADCB_Portal_secretkey;
        
        c.finalTagLst.add(getParametersValuesHidden(c.oPassedParams));
        c.finalTagLst.add(getSignedData(c.oPassedParams,secretKey));
        System.debug('ZZZ PG request-->'+JSON.serialize(c));
        return c.finalTagLst;
    }
    
    //v6.0
    public static String getUTCDateTime() 
    {
        DateTime oUTSDateTime = System.now().addHours(-4);
        String strUTCDateTime = oUTSDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
        system.debug('ZZZ strUTCDateTime -->' + strUTCDateTime );
        return strUTCDateTime;
    } 
    
    //v6.0
    public static String getUUID(){
        Blob b = Crypto.generateAesKey(128);
        String h = EncodingUtil.convertToHex(b);
        String guid = h.substring(0,8) + '-' + h.substring(8,12) + '-' + h.substring(12,16) + '-' + h.substring(16,20) + '-' + h.substring(20);       
        system.debug('ZZZ guid -->' + guid);
        return guid;
    }
    
    //v6.0
    private Static String getParametersValuesHidden(Map<String,String> oPassedParams){
        String result = '';
        for (String oKey : oPassedParams.KeySet()){ 
            result += '\n <input type="hidden" id="' + oKey + '"  aura:id="' + oKey + '"  name="' + oKey + '" value="' + oPassedParams.get(oKey) + '" />';
        }
        system.debug('ZZZ ParametersValuesHidden' + result);
        return result;
    }
    
    //v6.0
    public static String getSignedData(Map<string,string> oPassedParams, String secretKey){
        String result = '';
        result += '\n <input type="hidden" aura:id="signature" id="signature" name="signature" value="' + sign(buildDataToSign(oPassedParams),secretKey) + '"/>';
        system.debug('-- getSignedData' + result);
        return result;
    }
    
    private static String buildDataToSign(Map<string,string> paramsArray) 
    {
        String[] signedFieldNames = paramsArray.get('signed_field_names').Split(',');
        List<string> dataToSign = new List<string>();
        
        for (String oSignedFieldName : signedFieldNames){
            dataToSign.Add(oSignedFieldName + '=' + paramsArray.get(oSignedFieldName));
        }
        
        system.debug('ZZZ commaSeparate-->' + commaSeparate(dataToSign));
        return commaSeparate(dataToSign);
    }
    private static String commaSeparate(List<string> dataToSign) {
        String result = '';
        for(String Str : dataToSign) {
            result += (result==''?'':',')+Str;
        }
        return result;                         
    }
    
    //v6.0
    private static String sign(String data, String secretKey) 
    {
        String result = EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(data), Blob.valueOf(secretKey)));
        System.debug('ZZZ sign ->'+result);
        return result;
    }
    
    
    //v6.0
    @AuraEnabled
    public static Receipt__c getReceiptData(String rptId){
        return [Select Id,Name,Payment_Status__c,Customer__r.Name,Transaction_Date__c,Card_Amount__c,Card_Type__c,Payment_Gateway_Ref_No__c From Receipt__c Where Id=:rptId LIMIT 1];
    }
    
    public class DisplayRecords {
        
        @AuraEnabled public string portalBalance {get;set;}
        @AuraEnabled public decimal portalBalanceValue {get;set;}
        @AuraEnabled public string balanceTime {get;set;}
        @AuraEnabled public boolean displayComponent {get;set;}
        @AuraEnabled public string profileName {get;set;}
        @AuraEnabled public string userAccountId {get;set;}
    }
    public class PriceItem{
        @AuraEnabled
        public decimal amount {get;set;}
        @AuraEnabled
        public decimal amountInAED {get;set;}
        @AuraEnabled
        public decimal totalBalance {get;set;}
        @AuraEnabled
        public string name {get;set;}
        @AuraEnabled
        public map<string,string> mapCountry{get;set;}
        @AuraEnabled
        public string receiptRecordTypeId {get;set;}
        @AuraEnabled
        public list<ProductInfo> lstProductInfo {get;set;}
        public PriceItem(){
            amount = 0;
            totalBalance = 0;
            lstProductInfo = new list<ProductInfo>();
            mapCountry = new map<string,string>();
        }
    }
    public class ProductInfo{
        @AuraEnabled
        public string name {get;set;}
        @AuraEnabled
        public decimal amount {get;set;}
        @AuraEnabled
        public decimal amountInAED {get;set;}
    }
    public class SRLine {     
        @AuraEnabled public string product;
        @AuraEnabled public Decimal price;
    }
    public class RequestWrapper{
        @AuraEnabled public String amount                   {get;set;}
        @AuraEnabled public String paymentType              {get;set;}
        @AuraEnabled public String checkoutPaymentID        {get;set;}
        @AuraEnabled public String receiptStatus            {get;set;}
        @AuraEnabled public String cardType                 {get;set;}
        @AuraEnabled public String bankReferenceNumber      {get;set;}
        @AuraEnabled public String srId                     {get;set;}
        @AuraEnabled public String receiptId                {get;set;}
    }

    // merul: Added Payment resp.
    public class ResponseWrapper
    {
        @AuraEnabled public PriceItem prcItm  {get;set;}
        @AuraEnabled public Account accountRec  {get;set;}
       
    }

    public class Receipt 
    {     
        @AuraEnabled public string referenceNo {get;set;}
        @AuraEnabled public string errorMessage {get;set;}
        //Merul
        @AuraEnabled public Account accRec {get;set;}
    }
    

    //v6.0
    public class CardPaymentRequestWrapper{
        /*
        @AuraEnabled public string access_key { get; set; }
        @AuraEnabled public string profile_id { get; set; }
        @AuraEnabled public string transaction_uuid { get; set; }
        @AuraEnabled public string signed_field_names { get; set; }
        @AuraEnabled public string unsigned_field_names { get; set; }
        @AuraEnabled public string signed_date_time { get; set; }
        @AuraEnabled public string locale { get; set; }
        @AuraEnabled public string transaction_type { get; set; }
        @AuraEnabled public string reference_number { get; set; }
        @AuraEnabled public string amount { get; set; }
        @AuraEnabled public string pCurrency { get; set; }
        @AuraEnabled public string signature { get; set; }
        */
        
        public string access_key{get;set;}
        public string profile_id{get;set;}
        public string transaction_uuid{get;set;}
        public string customer_ip_address{get;set;}
        public string unsigned_field_names{get;set;}
        public string signed_date_time{get;set;}
        public string reference_number{get;set;}
        public string amount{get;set;}
        public string currencyvall{get;set;}
        public string signature{get;set;}
        public Map<String,String> oPassedParams = new Map<String,String>();
        public List<String> finalTagLst = new List<String>();
        
    }


}