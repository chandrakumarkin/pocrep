@isTest
public class cls_PSA_Termination_ObjectionTest {
    
    static testmethod void newDependentMultipleVisa1() {
        test.startTest();
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.PSA_Actual__c    = 1000;
        objAccount.PSA_Guarantee__c = 500;
        objAccount.ROC_Status__c  = 'Active';
        objAccount.Legal_Type_of_Entity__c = 'test';
       // objAccount.Next_Renewal_Date__c = system.today()+30;
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        insert objContact;
          
        Relationship__c relBus = new Relationship__c();
        relBus.Object_Contact__c = objContact.id;
        relBus.Subject_Account__c =  objAccount.Id;
        //relBus.Relationship_Type__c = 'Beneficiary Owner';
        relBus.Relationship_Type__c ='Has DIFC Sponsored Employee';
        relBus.Active__c= true;
        insert relBus;
           
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Consulate_or_Embassy_Letter';
        objTemplate.SR_RecordType_API_Name__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Menutext__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Required Docs to Upload';
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        Lookup__c nat1 = new Lookup__c(Type__c='Nationality',Name='Afghanistan');
        insert nat1;
        Lookup__c nat2 = new Lookup__c(Type__c='Nationality',Name='India');
        insert nat2;
        Lookup__c nat3 = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat3;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='PSA_Termination_Objection' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        
        SR_Status__c objSRStatus1 = new SR_Status__c();
        objSRStatus1.Name = 'Submitted';
        objSRStatus1.Code__c = 'Submitted';
        insert objSRStatus1;
         
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.contact__c = objContact1.id;
        objSR.Sponsor__c = objContact1.Id;
        objSR.Express_Service__c = false;
        objSR.External_SR_Status__c = objSRStatus1.Id;
         
      
        PageReference pageRef = Page.Submitted_Multiple_Visa_step;
        Test.setCurrentPage(pageRef);
        system.runAs(objUser){
             ApexPages.StandardController sc                       = new ApexPages.StandardController(objSR);
             cls_PSA_Termination_Objection thisInstance               = new cls_PSA_Termination_Objection(sc);
             thisInstance.SaveRecord();
        }
         test.stopTest(); 
      }

}