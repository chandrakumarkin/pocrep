/**
 * Description : Controller for OB_manageUsers Component 
 *
 * ****************************************************************************************
 * History :
 * [06.FEB.2020] Prateek Kadkol - Code Creation
 Modification History
    ---------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By          Description
    ---------------------------------------------------------------------------------------------------------------------             
    V1.0    20-Nov-2020  Mudasir           Finance contact person changes
 */
public without sharing class OB_manageUsersController {

    @AuraEnabled
    public static RespondWrap fetchUserData(String requestWrapParam) {

        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap = new RespondWrap();
        

        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

        set<string> relContactId = new set<string>();
        set<string> relPortalUserContID = new set<string>();
        list<HexaBPM__Service_Request__c> relPromoteSr = new List<HexaBPM__Service_Request__c>();
        string contactId = '';
        // query user information
        for(user conObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())){
            if(conObj.ContactId!=null && conObj.contact.AccountId!=null) {
                    respWrap.accId = conObj.contact.AccountId;
                    contactId = conObj.ContactId;
                    respWrap.loggedInUser = conObj;
            }
            
        }
        respWrap.isSuperUser = OB_AgentEntityUtils.hasSpecifiedRole(contactId,respWrap.accId,'Super User');
        
        if(respWrap.isSuperUser == true || Test.isRunningTest()){
            // query related users
        for(User usr :[SELECT AccountId , contactId from User where  /* contact.AccountId =: respWrap.accId and */ contactId != null AND  IsActive = true]) {
            relPortalUserContID.add(usr.contactId);

        }

        // query relOBj
        if(respWrap.accId != null) {
            //V1.0 - added - Financial Services criteria
            for(AccountContactRelation relOBj :[SELECT Id,isActive, AccountId,  Contact.Id,Contact.Name,Contact.Email,Contact.OB_Block_Homepage__c,Roles, Access_Level__c FROM AccountContactRelation WHERE AccountId =: respWrap.accId 
            AND Contact.Id IN: relPortalUserContID AND Roles INCLUDES ('Property Services','Super User','Employee Services','Company Services','Financial Services')]) {
                respWrap.relatedUserList.add(relOBj);
                relContactId.add(relOBj.Contact.Id);
            }
        

        // query related promote user sr
        if(relContactId.size() > 0){
            string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('Super User Authorization').getRecordTypeId();
            for(HexaBPM__Service_Request__c promoteSr : [SELECT id,HexaBPM__Contact__c,HexaBPM__IsClosedStatus__c from HexaBPM__Service_Request__c WHERE recordTypeId =:recordId AND 
            HexaBPM__Customer__c =: respWrap.accId AND HexaBPM__Contact__c IN :relContactId AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__IsCancelled__c = false AND 
            HexaBPM__Is_Rejected__c = false]){
                respWrap.relatedPromoteSr.add(promoteSr);
            }
        }

    }

    

        //get role picklist value
        
        Schema.DescribeFieldResult userComunityRoles = user.Community_User_Role__c.getDescribe();
        List<Schema.PicklistEntry> userComunityRolesValues = userComunityRoles.getPicklistValues();
        for( Schema.PicklistEntry role : userComunityRolesValues){
            respWrap.userComunityRolesValues.add(role.getLabel());
        }    

        return respWrap;
        }else{
            return respWrap;
        }
        
    }

    @AuraEnabled
    public static RespondWrap createPromoteUserSr(String requestWrapParam) {

        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap = new RespondWrap();
        

        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

        string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('Super User Authorization').getRecordTypeId();
        string srTemplateId = '';
                
        for(HexaBPM__SR_Template__c srTempObj : [SELECT id from HexaBPM__SR_Template__c WHERE Name = 'Super User Authorization'
        AND HexaBPM__SR_RecordType_API_Name__c = 'Superuser_Authorization' AND HexaBPM__Active__c = true]){
            srTemplateId = srTempObj.Id;
        }

        if( srTemplateId!= null && recordId !=null){

            HexaBPM__Service_Request__c srObj = new HexaBPM__Service_Request__c();
            srObj.recordTypeId = recordId;
            srObj.HexaBPM__SR_Template__c = srTemplateId;
            srObj.HexaBPM__Customer__c = reqWrap.accId;
            srObj.HexaBPM__Contact__c = reqWrap.contactID;
            srObj.HexaBPM__Email__c = reqWrap.loggedInUser.email ;
            srObj.first_name__c = reqWrap.loggedInUser.FirstName ;
            srObj.last_name__c = reqWrap.loggedInUser.lastName ;
            srObj.Created_By_Agent__c = true;
            srObj.Community_User_Roles__c = reqWrap.role;
            srObj.Step_Notes__c=reqWrap.mode;

            
            /* if(reqWrap.role != null){
                list<String> rolesSelected = reqWrap.role.split(';');
            if(rolesSelected.contains('Company Services') ){
                srObj.Company_Services__c   = true;
            }
            if(rolesSelected.contains('Property Services') ){
                srObj.Property_Services__c  = true;
            }
            if(rolesSelected.contains('Employee Services') ){
                srObj.Employment_Services__c    = true;
            }
            } */
            
            //get navigationUrl
            string pageAPINameToQuery = srObj.Step_Notes__c == 'Edit'? 'Edit_User' : 'Upgrade_to_Super_User';
    for(HexaBPM__Page_Flow__c pfFlow : [SELECT id , (select id,Community_Page__c from HexaBPM__Pages__r WHERE HexaBPM__Page_Order__c = 1 AND HexaBPM__VF_Page_API_Name__c =:pageAPINameToQuery LIMIT 1) from HexaBPM__Page_Flow__c WHERE HexaBPM__Record_Type_API_Name__c = 'Superuser_Authorization' LIMIT 1]){
        respWrap.promoteNavUrl = pfFlow.HexaBPM__Pages__r[0].Community_Page__c+'?flowId='+pfFlow.Id+'&pageId='+pfFlow.HexaBPM__Pages__r[0].Id;
    }

            try{
            
             insert srObj;
             respWrap.insertedSr =srObj;
             
             
            }catch(DMLException e) {
            
             string DMLError = e.getdmlMessage(0) + '';
             if(DMLError == null) {
             DMLError = e.getMessage() + '';
             }
             respWrap.errorMessage = DMLError; 
            }

        }else{
            respWrap.errorMessage = 'SR Record Type Missing';
        }            
        return respWrap;
    }

    // ------------ Wrapper List ----------- //

    public class RequestWrap {

        @AuraEnabled public string selcRel;
        @AuraEnabled public string accId;
        @AuraEnabled public string contactID;
        @AuraEnabled public string role;
        @AuraEnabled public string emailId;
        @AuraEnabled public user loggedInUser ;
        @AuraEnabled public string mode ;

    }

    public class RespondWrap {

        @AuraEnabled public string accId;
        @AuraEnabled public boolean isSuperUser ;
        @AuraEnabled public user loggedInUser ;
        @AuraEnabled public string promoteNavUrl;
        @AuraEnabled public string errorMessage;
        @AuraEnabled public object insertedSr;
        @AuraEnabled public object debugger;
        @AuraEnabled public list<AccountContactRelation> relatedUserList = new list<AccountContactRelation>();
        @AuraEnabled public list<HexaBPM__Service_Request__c> relatedPromoteSr = new list<HexaBPM__Service_Request__c>();
        @AuraEnabled public List<String> userComunityRolesValues = new List<String>();

    }
}