@isTest
public class OB_CpRenewalRedirectorControllerTest {

     public static testmethod void test1(){
     
       Account acc  = new Account();
        acc.Is_Commercial_Permission__c = 'Yes';
       acc.name = 'test';   
        
       insert acc;
         
       Compliance__c comprec = new Compliance__c();
         comprec.Account__c = acc.id;
         comprec.End_Date__c = system.today();
         comprec.Status__c ='open';
         insert comprec;
         
       
       contact con = new Contact();
       con.LastName = 'test';
       con.FirstName = 'test';
       con.Email = 'test@test.com';
       insert con;  
       
       Lease__c lse = new Lease__c();
       lse.Name = '234';
       lse.Account__c = acc.id;
       lse.End_Date__c  = system.today()+30;
       insert lse; 

        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Contractor Community Plus User Custom']; 
         
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',contactid = con.Id);
       
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'Commercial_Permission';
       insert objsrTemp;
         
       HexaBPM__SR_Template__c objsrTemp1 = new HexaBPM__SR_Template__c();
       objsrTemp1.HexaBPM__Menu__c = 'Company Services';
       objsrTemp1.HexaBPM__SR_RecordType_API_Name__c = 'Commercial_Permission_Renewal';
       insert objsrTemp1;
         
       HexaBPM__SR_Status__c objSrstatus = new HexaBPM__SR_Status__c();
       objSrstatus.HexaBPM__Code__c = 'Approved';
       objSrstatus.HexaBPM__Type__c = 'End';
       insert objSrstatus;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New Commercial Permission').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Internal_SR_Status__c = objsrstatus.Id;
        objHexaSR.HexaBPM__External_SR_Status__c = objsrstatus.Id;
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.OB_Application__c = objHexaSR.Id;
        insert objSR;
        
        
       Company_Name__c objcomp = new Company_Name__c();
       objcomp.Application__c = objHexaSR.Id;
       objcomp.Entity_Name__c = 'test';
       objcomp.Trading_Name__c = 'test';
       objcomp.Arabic_Entity_Name__c = 'أكبر يساف';
       objcomp.Arabic_Trading_Name__c = 'أكبر يساف';
       objcomp.Status__c = 'Draft';
       objcomp.Date_of_Approval__c = system.today()-100;
       insert objcomp;
       
       Test.startTest();
         try{
             
              OB_CpRenewalRedirectorController.RequestWrap repWrapper = new OB_CpRenewalRedirectorController.RequestWrap();

            repWrapper.srId = '123';
            String requestString = JSON.serialize(repWrapper);
             
            OB_CpRenewalRedirectorController.redirectToPage(requestString); 
         }catch(Exception e){
             
         }
       Test.stopTest();

     
     }
}