/***********************************************************************
* Class : UnitAvailabilityClass
* Description : Utility class for Unit trigger to check the unit availability :Commercial Unit Offer Process 5633
* Test Class Details : test class written in consentUtilityTest
* Created Date : 06/09/2018
************************************************************************/
public class UnitAvailabilityUtilityClass{
    public static Boolean isUnitstatusCheked = false;
    
    public static void checkunitStatus(List<Unit__c> unitTrg){
        set<Id> unitID = new set<Id>();
        set<Id> tenancyUnitID = new set<Id>();
        Map<Id,Unit__c> availableList = new Map<Id,Unit__c>();
        Map<Id,Unit__c> occupiedList = new Map<Id,Unit__c>();
        List<Unit__c> unitList = new List<Unit__c>();
        if(unitTrg.size()>0){
            for(Unit__c itr:unitTrg){
                unitID.add(itr.id);
            }
        }
        if(unitID.size()>0){
            
            isUnitstatusCheked = true;
            
            List<Unit__c> utlst= [Select u.Id,u.UnitStatus__c,u.Unit_Start_Date__c,LeaseTerminationDate__c,u.Unit_End_Date__c,(Select Id,Status__c,Name, Lease_Status__c, Termination_Date__c,Lease__c From Tenancies__r where Lease_Status__c='Active' ) From Unit__c u where ID IN:unitID];
            for(Unit__c itr:utlst){
                if(itr.Tenancies__r.isEmpty()){
                system.debug('-----'+itr.Tenancies__r);
                    availableList.put(itr.Id,itr);
                }else if(!itr.Tenancies__r.isEmpty()){
                    for(Tenancy__c t:itr.Tenancies__r){
                        system.debug('----------'+t.Termination_Date__c+'---'+itr.Unit_End_Date__c);
                        if(t.Termination_Date__c!=null){
                            itr.LeaseTerminationDate__c = t.Termination_Date__c;
                            occupiedList.put(itr.Id,itr);
                        }
                        else if(t.Termination_Date__c==null && itr.Unit_End_Date__c!=Date.valueOf('4000-12-31')){                             
                                availableList.put(itr.id,itr);                           
                        }
                        else if(t.Termination_Date__c==null && itr.Unit_End_Date__c==Date.valueOf('4000-12-31')){                             
                                occupiedList.put(itr.id,itr);
                            
                        }
                    }
                }
            }
        }
        if(availableList.size()>0){
            for(Unit__c itr:availableList.values()){
                    itr.UnitStatus__c ='Available';
                    unitList.add(itr);
            }
        }
        if(occupiedList.size()>0){
            for(Unit__c itr:occupiedList.values()){
                    itr.UnitStatus__c ='Occupied';
                    unitList.add(itr);
            }
        }
        if(unitList.size()>0){
            update unitList;
        }
    }
}