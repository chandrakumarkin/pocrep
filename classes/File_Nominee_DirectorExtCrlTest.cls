@isTest
public class File_Nominee_DirectorExtCrlTest {
    public static testMethod void TestMethod_1(){
        
        Service_request__C ServicerequestObj = new Service_request__C (); 
        insert ServicerequestObj;

        Amendment__c AmendmentObj = new Amendment__c();

        AmendmentObj.ServiceRequest__c = ServicerequestObj.Id;
		AmendmentObj.Apt_or_Villa_No__c = 'TEST 1';
		AmendmentObj.Building_Name__c = 'TEST 1'; 
		AmendmentObj.Date_of_Birth__c = Date.today().AddYears(-19); 
		AmendmentObj.Emirate_State__c = 'TEST 1'; 
		AmendmentObj.Family_Name__c = 'TEST 1'; 
		AmendmentObj.Gender__c = 'Male'; 
		AmendmentObj.Given_Name__c = 'TEST 1'; 
		AmendmentObj.Job_Title__c = 'TEST 1'; 
		AmendmentObj.Middle_Name__c = 'TEST 1'; 
		//AmendmentObj.Mobile__c = '+971000000000'; 
		AmendmentObj.Nationality_list__c = 'TEST 1'; 
		AmendmentObj.Passport_Expiry_Date__c = Date.today().AddYears(10); 
		AmendmentObj.Passport_No__c = 'TEST 1'; 
		AmendmentObj.Permanent_Native_City__c = 'TEST 1'; 
		AmendmentObj.Permanent_Native_Country__c = 'TEST 1'; 
		AmendmentObj.Person_Email__c = 'TEST@TEST.COM'; 
		//AmendmentObj.Phone__c = '+971000000000'; 
		AmendmentObj.Place_of_Birth__c = 'TEST 1'; 
		AmendmentObj.PO_Box__c = 'TEST 1'; 
		AmendmentObj.Post_Code__c = 'TEST 1'; 
		AmendmentObj.Status__c = 'TEST 1'; 
		AmendmentObj.Street__c = 'TEST 1'; 
		AmendmentObj.Title_new__c = 'Mr'; 

		insert AmendmentObj;


        PageReference pageRef = Page.File_Nominee_Director;
        
        Test.setCurrentPage(pageRef);
        Test.Starttest();
        
        Apexpages.StandardController sc = new Apexpages.StandardController(ServicerequestObj);
        File_Nominee_DirectorExtCrl ext = new  File_Nominee_DirectorExtCrl(sc);         
		ext.AmendmentId = AmendmentObj.Id;
		
		List<selectoption> ListUser = ext.UserList;
		ext.SaveRecord();
		ext.AddRow();
        ext.AddAmmendmentObj();
		ext.UpdateAmmendment();
		ext.RemoveAmmendment();
		ext.CancelAmmendmentObj();
		ext.EditAmmendment();

        Test.StopTest();
        
    }

    public static testMethod void TestMethod_2(){
        
        Service_request__C ServicerequestObj = new Service_request__C (); 

        PageReference pageRef = Page.File_Nominee_Director;
        
        Test.setCurrentPage(pageRef);
        
        Apexpages.StandardController sc = new Apexpages.StandardController(ServicerequestObj);
        File_Nominee_DirectorExtCrl ext = new  File_Nominee_DirectorExtCrl(sc);         
        
    }
}