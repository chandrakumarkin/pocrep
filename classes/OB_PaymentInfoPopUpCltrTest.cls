/*
    Author      : Leeba
    Date        : 30-March-2020
    Description : Test class for OB_PaymentInfoPopUpCltr 
    --------------------------------------------------------------------------------------
*/
@isTest
public class OB_PaymentInfoPopUpCltrTest {

     public static testmethod void testMethod1(){
      
        Account acc  = new Account();
        acc.name = 'test';      
        insert acc;
       
       contact con = new Contact();
       con.LastName = 'test';
       con.FirstName = 'test';
       con.Email = 'test@test.com';
       insert con;   

        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community Plus User Custom']; 
         
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',contactid = con.Id);
       
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
                        
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        insert objHexaSR;
        
        test.startTest();
        
        OB_PaymentInfoPopUpCltr.RequestWrap reqWrapper = new OB_PaymentInfoPopUpCltr.RequestWrap();
        reqWrapper.srId = objHexaSR.id;
        OB_PaymentInfoPopUpCltr.RespondWrap restWrapTest = new OB_PaymentInfoPopUpCltr.RespondWrap();
        String requestString = JSON.serialize(reqWrapper);
        restWrapTest = OB_PaymentInfoPopUpCltr.getCompanyNameInfo(requestString);
        
        test.stopTest();
    }
}