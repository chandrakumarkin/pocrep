public class GSStepEmailLogCreationHandler_Refund{
    
    
     public static void CreateEmailLogOnUpdate(step__c step){

    //if(step.Sys_SR_Group__c == 'Other'){
    
    
             boolean SeparateServices=false;
             Id ServiceCreatedBy;
            
            List < GSStepandStatus__mdt > GSstepandStatus = [select MasterLabel, status__c from GSStepandStatus__mdt where MasterLabel =: step.Step_Name__c];
             if (GSstepandStatus.size() > 0) {
                 string stepStatus = GSstepandStatus[0].status__c;
                 
                 if ((step.Status_Code__c == 'REJECTED' && stepStatus.contains('REJECTED'))){
                       List < Email_Logs__c > emailLogs = new List < Email_Logs__c > ();
                       
                        step__c stp = [select id,name,SR__r.Createdbyid, SR_Template__c, sr__r.Service_Category__c,SR_Menu_Text__c,sr__r.Send_SMS_To_Mobile__c,
                        Step_Name__c, SR__C, SR__r.Customer__c, SR__r.Customer__r.name, Rejection_Reason__c,SR__r.Customer__r.Separate_Services__c,SR_Type_Step__c,
                        SR__r.Name, SR__r.Type_of_Refund__c,SR__r.Mode_of_Refund__c,SR__r.Refund_Amount__c from step__c where id =: step.Id];

                    string TempCode = step.SR_Template__c;
                    system.debug('TempCode' + TempCode);
                    SeparateServices=stp.SR__r.Customer__r.Separate_Services__c;
                    ServiceCreatedBy=stp.SR__r.Createdbyid;
                    for (GSEmailTemplates__mdt emilTemplates: [select MasterLabel, Service_Types__c, Step_Status_Code__c, Email_Body__c, subject__c, Step_Name__c, Service_Category__c, SMS_body__c, SMS_Subject__c from GSEmailTemplates__mdt where Step_Name__c =: stp.Step_Name__c and Service_Types__c ='Refund_Request']) {

                    
                      if ((emilTemplates.Service_Types__c.contains(TempCode) && emilTemplates.Step_Status_Code__c == step.Status_Code__c) )
                        {
                            Email_Logs__c emLogs = new Email_Logs__c();
                            emLogs.name = stp.name;
                            string bodyofTemp = '';
                            string subjectTemp = '';
                            string smsBody = emilTemplates.SMS_body__c;
                            string smsSubject = emilTemplates.SMS_Subject__c;
                            
                             if (!string.isBlank(smsBody)) {
                             
                                if (smsBody.contains('{!Step__c.SR__c}')) smsBody = smsBody.replace('{!Step__c.SR__c}', stp.SR__r.name);
                                if (smsBody.contains('{!Step__c.SR_Menu_Text__c}')) {
                                    smsBody = smsBody.replace(' {!Step__c.SR_Menu_Text__c}', stp.SR_Menu_Text__c);
                                }
                             }
                             
                             string mobileNo = '';

                            if (!string.isBlank(smsSubject)) {
                                mobileNo = stp.sr__r.Send_SMS_To_Mobile__c;
                                mobileNo = (stp.sr__r.Send_SMS_To_Mobile__c == '' || stp.sr__r.Send_SMS_To_Mobile__c == null) ? '' : stp.sr__r.Send_SMS_To_Mobile__c;
                                if (smsSubject.contains('{!step__c.sr__r.Send_SMS_To_Mobile__c}')) smsSubject = smsSubject.replace('{!step__c.sr__r.Send_SMS_To_Mobile__c}', mobileNo);
                            }
                                   
                           if(String.isBlank(bodyofTemp ) ||  String.isBlank(subjectTemp )){
                                   bodyofTemp = emilTemplates.Email_Body__c;
                                    subjectTemp = emilTemplates.subject__c;
                           }
                           
                           if (subjectTemp.contains('{!Step__c.SR__c}')) {
                                subjectTemp = subjectTemp.replace('{!Step__c.SR__c}', stp.SR__r.name);
                            }
                            
                             if (bodyofTemp != null || bodyofTemp != '') {
                                 
                                  if (bodyofTemp.contains('{!Step__c.SR__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.SR__c}', stp.SR__r.name);
                                  }
                                  
                                  if (bodyofTemp.contains('{!Step__c.SR_Temp_Menu__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.SR_Temp_Menu__c}', stp.SR_Temp_Menu__c);
                                   }
                                   
                                     if (bodyofTemp.contains('{!step.SR__r.Customer__r.name}')) {
                                    bodyofTemp = bodyofTemp.replace('{!step.SR__r.Customer__r.name}', stp.SR__r.Customer__r.name);
                                  }
                                  
                                     if (bodyofTemp.contains('{!Step__c.Service_Type__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Service_Type__c}', stp.SR_Type_Step__c);
                                    }
                                  
                                   if (bodyofTemp.contains('{!Step__c.Rejection_Reason__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Rejection_Reason__c}', stp.Rejection_Reason__c);
                                    }
                                    
                                    if (bodyofTemp.contains('{!Step__c.Type_of_Refund__c}')) {
                                    bodyofTemp = bodyofTemp.replace('{!Step__c.Type_of_Refund__c}', stp.sr__r.Type_of_Refund__c);
                                    }
                                    string TypeofRefund ='';
                                    if (bodyofTemp.contains('{!Step__c.Mode_of_Refund__c}')) {
                                        
                                        TypeofRefund = (stp.Sr__r.Mode_of_Refund__c == ''  || stp.Sr__r.Mode_of_Refund__c == null) ? '' : stp.Sr__r.Mode_of_Refund__c;
                                        
                                        bodyofTemp = bodyofTemp.replace('{!Step__c.Mode_of_Refund__c}', TypeofRefund);
                                    }
                                    
                                    string refundAmount ='';
                                    
                                    
                                    if (bodyofTemp.contains('{!Step__c.Refund_Amount__c}')) {
                                        
                                        
                                        if (!(string.valueOf(stp.SR__r.Refund_Amount__c) == '' || string.valueOf(stp.SR__r.Refund_Amount__c) == null)) {
                                        bodyofTemp = bodyofTemp.replace('{!Step__c.Refund_Amount__c}', string.valueOf(stp.SR__r.Refund_Amount__c));
                                        } 
                                        
                                        

                                    }
                                 
                             }
                             
                             
                            emLogs.subject__c = subjectTemp;
                            emLogs.Email_Body__c = bodyofTemp;
                            emLogs.Service_Request__c = stp.sr__c;
                            emLogs.step__c = stp.id;
                            emLogs.SMS_body__c = smsBody;
                            emLogs.SMS_Subject__c = smsSubject;
                            
                            
                             
                             if(SeparateServices)
                            {
                                emLogs.Portal_User_1__c =ServiceCreatedBy;
                            } else
                            {
                            
                                List<User> usr            = new List<User>();
                                Set<String> contactIds    = new Set<String>(); 
                                Map<String,AccountContactRelation> accountContactMap = new  Map<String,AccountContactRelation>();
                             
                                for(AccountContactRelation ACR: [Select Id,contactId,Roles from AccountContactRelation where AccountId =: stp.SR__r.Customer__c and  isActive = true ]){
                                    contactIds.add(ACR.contactId);
                                    accountContactMap.put(ACR.contactId,ACR);
                                }
     
                                for(User thisUser : [select Id,ContactId,Community_User_Role__c FROM User where ContactId IN : contactIds and IsActive = true AND ID !=: ServiceCreatedBy]){
                                    if(accountContactMap.containsKey(thisUser.ContactId) &&
                                        accountContactMap.get(thisUser.ContactId).Roles != null &&
                                        accountContactMap.get(thisUser.ContactId).Roles.Contains('Employee Services')){
                                          
                                          usr.add(thisUser);
                                     }
                                }

                                emLogs.Portal_User_1__c = [SELECT Id FROM User WHERE Id =: stp.SR__r.Createdbyid AND Community_User_Role__c != null].size()> 0 ?
                                [SELECT Id FROM User WHERE Id =: stp.SR__r.Createdbyid AND Community_User_Role__c != null].Id : null;
                          
                            if (usr.size() > 0)
                                emLogs.Portal_User_2__c = usr[0].id;
                            if (usr.size() > 1)
                                emLogs.Portal_User_3__c = usr[1].id;
                            if (usr.size() > 2)
                                emLogs.Portal_User_4__c = usr[2].id;
                            if (usr.size() > 3)
                                emLogs.Portal_User_5__c = usr[3].id;
                           }
                           
                           if ((emilTemplates.subject__c != null || emilTemplates.subject__c != '') && (emilTemplates.Email_Body__c != null || emilTemplates.Email_Body__c != '')){
                                emailLogs.add(emLogs);
                            }
                            
                        }
                    }
                    
                    try {
                        if (emailLogs.size() > 0){
                            insert emailLogs;
                        }
                    } catch (Exception ex) {
                        Log__c objLog = new Log__c( Type__c = 'email Log record creation failed', Description__c = ex.getMessage() );
                        insert objLog;
                    }
                 }
                
              }             
            
        }

     //}  
}