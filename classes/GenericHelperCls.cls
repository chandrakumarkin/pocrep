/*
    Created By  : Ravi on 21st July,2015
    Description : This class is a Helper class, contains some generic code
     
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date			Updated By		Description
 ----------------------------------------------------------------------------------------              
 V1.0	21/07/2015		Ravi			Created
 
 ----------------------------------------------------------------------------------------
*/
public without sharing class GenericHelperCls {
	public static string NextActivityCode(string currentCode){
		string nextCode = '';
		map<string,Integer> mapCharSet = new map<string,Integer>{'0'=>0,'1'=>1,'2'=>2,'3'=>3,'4'=>4,'5'=>5,'6'=>6,'7'=>7,'8'=>8,'9'=>9,
										'A'=>10,'B'=>11,'C'=>12,'D'=>13,'E'=>14,'F'=>15,'G'=>16,'H'=>17,'I'=>18,'J'=>19,
										'K'=>20,'L'=>21,'M'=>22,'N'=>23,'O'=>24,'P'=>25,'Q'=>26,'R'=>27,'S'=>28,'T'=>29,
										'U'=>30,'V'=>31,'W'=>32,'X'=>33,'Y'=>34,'Z'=>35};
		map<Integer,string> mapNumSet = new map<Integer,string>{0=>'0',1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6',7=>'7',8=>'8',9=>'9',
										10=>'A',11=>'B',12=>'C',13=>'D',14=>'E',15=>'F',16=>'G',17=>'H',18=>'I',19=>'J',
										20=>'K',21=>'L',22=>'M',23=>'N',24=>'O',25=>'P',26=>'Q',27=>'R',28=>'S',29=>'T',
										30=>'U',31=>'V',32=>'W',33=>'X',34=>'Y',35=>'Z'};
		
		string char1 = currentCode.left(1);
		string char2 = currentCode.left(2).right(1);
		string char3 = currentCode.right(1);
		system.debug('Char 1 '+char1);
		system.debug('Char 2 '+char2);
		system.debug('Char 3 '+char3);
		
		if((mapCharSet.containsKey(char1) && mapCharSet.get(char1) > 9) ||
			(mapCharSet.containsKey(char2) && mapCharSet.get(char2) > 9) ||
			 (mapCharSet.containsKey(char3) && mapCharSet.get(char3) > 9) || currentCode == '999' ){
			 	
			if(mapCharSet.get(char3) == 35){
				char3 = mapNumSet.get(0);
				if(mapCharSet.get(char2) == 35){
					char2 = mapNumSet.get(0);
					if(mapCharSet.get(char1) == 35){
						char1 = mapNumSet.get(0);
					}else{
						char1 = mapNumSet.get( mapCharSet.get(char1)+1 );
						nextCode = char1+char2+char3;
					}
				}else{
					char2 = mapNumSet.get( mapCharSet.get(char2)+1 );
					nextCode = char1+char2+char3;
				}
			}else{
				char3 = mapNumSet.get( mapCharSet.get(char3)+1 );
				nextCode = char1+char2+char3;
			}
		}else{
			String Code = '000'+(Integer.valueOf(currentCode)+1);
			nextCode = Code.right(3);
		}
		system.debug('nextCode is : '+nextCode);
		return nextCode;
	}
}