@isTest
public class UserFrmInlineControllerTest {
    static TestMethod void UserFrmInlineControllerTest(){
            Account objAccount          = new Account();
            objAccount.Name             = 'Test Custoer 1';
            objAccount.E_mail__c        = 'test@test.com';
            objAccount.BP_No__c         = '001234';
            objAccount.Company_Type__c  ='Financial - related';
            objAccount.Sector_Classification__c     = 'Authorised Market Institution';
            objAccount.Legal_Type_of_Entity__c      = 'LTD';
            objAccount.ROC_Status__c                = 'Active';
            objAccount.Financial_Year_End__c        = '31st December';
            insert objAccount;
            
            License__c objLic = new License__c();
            objLic.License_Issue_Date__c    = system.today();
            objLic.License_Expiry_Date__c   = system.today().addDays(365);
            objLic.Status__c                = 'Active';
            objLic.Account__c               = objAccount.id;
            insert objLic;
            
            objAccount.Active_License__c = objLic.id;
            update objAccount;
            Id portalUserId                 = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
            Contact objContact              = new Contact();
            objContact.firstname            = 'Test Contact';
            objContact.lastname             = 'Test Contact1';
            objContact.accountId            = objAccount.id;
            objContact.recordTypeId         = portalUserId;
            objContact.Email                = 'test@difcportal.com';
            
            insert objContact;
            
            Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
            User objUser    = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = objProfile.Id,
                            ContactId=objContact.Id, Community_User_Role__c='Company Services',
                            TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
            insert objUser;
            
            map<string,string> mapRecordTypeIds = new map<string,string>();
            
            for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Multiple_New_Dependent_Visa')]){
                mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
            }
            
            Service_Request__c objSR             = new Service_Request__c();
            objSR.RecordTypeId = mapRecordTypeIds.get('Multiple_New_Dependent_Visa');
            objSR.Customer__c                    = objAccount.Id;
            objSR.Financial_Year_End_mm_dd__c    ='31st December';
            insert objSR;
             Test.startTest(); 
            Cancelled_Employees__c thisEmployee = new Cancelled_Employees__c();
            thisEmployee.Designation__c ='Test';
            thisEmployee.Service_Request__c = objSR.Id;
            thisEmployee.Email__c       = 'test@west.com';
            insert thisEmployee;
            
            ApexPages.StandardController con           = new ApexPages.StandardController(objSR); 
            UserFrmInlineController controller          = new UserFrmInlineController(con);
            controller.LineManagerStepSRAction(objSR.Id);
        
            Test.stopTest();
        }
}