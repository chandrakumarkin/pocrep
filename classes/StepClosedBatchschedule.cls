/******************************************************************************************
 *  Author     : Ghanshyam Yadav
 *  Date       : 15-08-2018
 *  Description  : StepClosedBatch schedule
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    15-08-2018  Ghanshyam      Created
****************************************************************************************************************/


global class StepClosedBatchschedule implements Schedulable {

   global void execute(SchedulableContext SC) {
   
        StepClosedBatch stp = new StepClosedBatch(); 
        
        database.executebatch(stp,200);
   }
}