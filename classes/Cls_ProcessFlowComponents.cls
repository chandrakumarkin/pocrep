/***********************************************************************************
 *  Author   : Claude Manahan
 *  Company  : DIFC
 *  Date     : 08/05/2017
 *  Purpose  : Parent Class of controllers for custom pages in page flows. This contains all the 
 			   common methods used in every custom page in the process flow. If you need to add your 
 			   own logic, simply extend the class, and your methods.
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date        Updated By  Description
 ----------------------------------------------------------------------------------------              
V1.0	08/05/2017	Claude		Created
**********************************************************************************************/
public virtual class Cls_ProcessFlowComponents {
	
	/* 
	 * Mandatory navigation variables 
	 * for pages used in a process flow 
	 */
	public String strPageId						{get; set;}
	public String strActionId					{get; set;}
	public string strHiddenPageIds				{get; set;}
	public String strReqFieldIds				{get; set;}
	public String strNavigatePageId				{get; set;}
	public Boolean isProcessFlow				{get; set;}
	
	public string pageTitle						{get; set;}
    public string pageDescription				{get; set;}
    
    /* Main Service Request Instance */
    public Service_Request__c objSr				{get; set;}
	
	/* 
	 * Stores the URL Parameters passed 
	 * in the flow 
	 */
	protected map<string,string> mapParameters;
	
	public Cls_ProcessFlowComponents(){
		
		/* Default constructor actions; DO NOT CHANGE */
		setUrlParameters();	
		
		setServiceRequest();
		
		setHiddenPageIds();
		
		setCurrentPage();
	}
	
	/**
	 * Saves any changes in the Service Request
	 */
	public virtual PageReference CommitRecord(){
		
		saveRecord();
		
		return null;
	}
	
	/**
	 * Returns the base URL of the current instance
	 * in the page
	 * @return			baseUrl			The SFDC base URL
	 */
	public String getSalesforceUrl() {
        return System.Url.getSalesforceBaseUrl().toExternalForm();
    }
    
    /**
     * Returns the main page component
     * used in the process flow
     * @return 			pageBlock		The main page block
     */
    public virtual Component.Apex.PageBlock getDyncPgMainPB(){
        
        PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = objSr; 
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        
        Component.Apex.PageBlock pb = objPB.getDyncPgMainPB();
        
        strReqFieldIds = PreparePageBlockUtil.strReqFieldIds;
       
        return pb;
    }
	
	/**
	 * Redirects the user to the designated
	 * page
	 * @return 			pageRef			The reference to the selected page
	 */
    public virtual PageReference goTopage(){
    	return String.isNotBlank(strNavigatePageId) ? getNextPage() : null;
    }
    
    protected pageReference getNextPage(){
    	
    	PreparePageBlockUtil objSidebarRef = new PreparePageBlockUtil();
        PreparePageBlockUtil.strSideBarPageId = strNavigatePageId;
        PreparePageBlockUtil.objSR = objSr;
        
        return objSidebarRef.getSideBarReference();
    	
    }
    
    public pagereference Cancel_Request(){
    	
        pagereference pg = null;
        
        if(String.isNotBlank(objSR.Id)){
            
            list<Service_Request__c> lstSR = [select id,finalizeAmendmentFlg__c,Internal_Status_Name__c from Service_Request__c where Id=:objSR.Id];
            
            if(lstSR!=null && lstSR.size()>0){
            
                try{
            
                    if(lstSR[0].Internal_Status_Name__c=='Draft'){
                    	 
                    	 delete lstSR;
                    	 
                    	 pg = new pagereference('/apex/home');
                    	 pg.setRedirect(true);
                    	 
                    } else{
                         ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Request is already Submitted & not allowed to cancel.'));
                    }
                    
                } catch(DMLException e){
                	
                    setError(e);
                }
            }
            
        }else{
        	
            pg = new pagereference('/apex/home');
            pg.setRedirect(true);
        }
        
        return pg;
    }
    
    /**
     * Redirects the form to the page
     * configured in the navigation button
     * @return 			buttonAction			The reference to the selected page
     */
    public virtual PageReference DynamicButtonAction(){
    	
    	return (String.isNotBlank(strActionId)) ? getButtonAction() : null; 
    }
    
    /**
     * Checks if the button action
     * moves the form to the next page
     * @return			isNext			Tells if the button is meant to navigate to the next page
     */
    protected Boolean isNext(){
    	
    	for(Section_Detail__c objSec:[select id,Name,Component_Label__c,Navigation_Direction__c from Section_Detail__c where Id=:strActionId]){
            
            return objSec.Navigation_Direction__c.equals('Forward');
            
        }
    	
    	return false;
    }
    
    /**
     * Returns the action of the 'Forward' button
     * @return			buttonAction		The page reference of the next page
     */
    protected PageReference getButtonAction(){
    	
    	System.debug('strActionId==>' + strActionId);
    	
    	PreparePageBlockUtil.FlowId = mapParameters.get('FlowId'); 
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = objSr;
        PreparePageBlockUtil.ActionId = strActionId;
        
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        
        return objPB.getButtonAction();
    	
    }
	
	/**
	 * Saves any changes to the record
	 */
	protected void saveRecord(){
		
		try {
		
			upsert objSr;
		
		} catch(DMLException e){
            
            setError(e);
        }
        
	}
	
	/**
	 * Clears the action ID
	 */
	protected void clearAction(){
		strActionId = '';
	}
	
	/**
	 * Sets the error message in the page
	 * @params		e			The exception
	 */
	protected void setError(DMLException e){
		
		string DMLError = e.getdmlMessage(0)+'';
            
        if(DMLError==null) DMLError = e.getMessage()+'';
        
        setMessage(DMLError);
		
	}
	
	/**
	 * Sets the error message in the page
	 * @params		e			The exception
	 */
	protected void setError(Exception e){
		setMessage(e.getMessage());
	}
	
	/**
	 * Sets the error message in the page
	 * @params		message		The exception message
	 */
	protected void setMessage(String message){
		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,message));
	}
	
	/**
	 * Sets the URL Parameters map
	 */
	private void setUrlParameters(){
		if(mapParameters == null) mapParameters = ApexPages.currentPage().getParameters();
		
		if(mapParameters.containsKey('PageId')) strPageId = mapParameters.get('PageId');
		
	}
	
	/**
	 * Sets the hidden page IDs configured in the page flow
	 */
	private void setHiddenPageIds(){
		strHiddenPageIds = PreparePageBlockUtil.getHiddenPageIds(mapParameters.get('FlowId'),objSr);
	}
	
	/**
	 * Sets the page details
	 */
	private void setCurrentPage(){
		
		if(mapParameters.containsKey('FlowId') && mapParameters.containsKey('PageId')  && mapParameters.containsKey('Id')){
            
            for(Page__c page:[select id,Name,Page_Description__c from Page__c where Id=:mapParameters.get('PageId')]){
                
                pageTitle = page.Name;
                pageDescription = page.Page_Description__c;
            }
        }
		
	}
	
	/**
	 * Retrieves the Service Request cord
	 */
	protected virtual void setServiceRequest(){
		
		isProcessFlow = false;
		
		if(mapParameters.containsKey('Id')){	
			
			string serviceID = mapParameters.get('Id');
			
			list<service_request__c> templistSR = Database.Query(Apex_UtilCls.getAllFieldsSOQL('Service_Request__c') + ' WHERE Id = :serviceID');
			 
			if(templistSR!=null && !templistSR.isEmpty()) objSr = templistSR[0];	
		}
	}
}