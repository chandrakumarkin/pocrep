/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestGsUpgradeSRCls {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'AccountBal';
        objEP.URL__c = 'http://AccountBal/sampledata';
        lstCS.add(objEP);
        insert lstCS;
                
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstSRStatus.add(new SR_Status__c(Name='Posted',Code__c='POSTED'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Required Docs to Upload';
        insert objDocMaster;
        
        Document_Details__c objDocDetails = new Document_Details__c();
        objDocDetails.Account__c = objAccount.Id;
        objDocDetails.EXPIRY_DATE__c = system.today().addYears(1);
        objDocDetails.DOCUMENT_STATUS__c = 'Active';
        objDocDetails.ISSUE_DATE__c = system.today();
        objDocDetails.DOCUMENT_TYPE__c = 'PIC';
        insert objDocDetails;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'DIFC New Visa';
        lstProducts.add(objProd);
        
        insert lstProducts;
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC New Sponsorship Visa New';
        objPL.DEV_Id__c = 'GO-00001';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00001';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC New Sponsorship Visa New - Inside';
        objPL.DEV_Id__c = 'GO-00002';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00002';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            if(objP.Material_Code__c == 'GO-00002')
                objDP.Unit_Price__c = 2234;
            else
                objDP.Unit_Price__c = 1234;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }       
        
        GS_Upgrade_Matrix__c objGSMatrix = new GS_Upgrade_Matrix__c();
        objGSMatrix.Name = 'GO-00001';
        objGSMatrix.Material_Code__c = 'GO-00001';
        objGSMatrix.VIP_Material_Code__c = 'GO-VIP01';
        objGSMatrix.Cross_Material_Code__c = 'GO-00002';
        objGSMatrix.Entry_Permit_Material_Code__c = 'GO-00002';
        objGSMatrix.Entry_Permit_Margin__c = 1000;
        objGSMatrix.Cross_Material_Margin__c = 1000;
        objGSMatrix.VIP_Margin__c = 1000;
        insert objGSMatrix;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_New');
        //objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.Mobile_Number__c ='+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[1].Id;
        objSR.External_SR_Status__c = lstSRStatus[1].Id;
        insert objSR;
        
        objSR.Internal_SR_Status__c = lstSRStatus[1].Id;
        objSR.External_SR_Status__c = lstSRStatus[1].Id;
        
        update objSR;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 3210;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        lstPriceItems.add(objItem);
        
        insert lstPriceItems;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        insert objStep;
        
        delete [select Id from SR_Doc__c where Service_Request__c=:objSR.Id];
        
        //test.startTest();
               
        Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
        
        list<SAPGSWebServices.ZSF_S_GS_SERV_OP> lstGSItems = new list<SAPGSWebServices.ZSF_S_GS_SERV_OP>();
        SAPGSWebServices.ZSF_S_GS_SERV_OP objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'N';
        objGSItem.SOPRP = 'N';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        //objGSItem.SGUID = SRItemId;
        lstGSItems.add(objGSItem);
        objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'P';
        objGSItem.SOPRP = 'N';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        //objGSItem.SGUID = SRItemId;
        lstGSItems.add(objGSItem);
        TestGsCRMServiceMock.lstGSPriceItems = lstGSItems;
        //GsUpgradeSRCls.UpgradePushToSAP(lstPriceItems[0].Id);
        
        Apexpages.currentPage().getParameters().put('type','cross');
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
                
        GsUpgradeSRCls objGsUpgradeSRCls = new GsUpgradeSRCls();
        objGsUpgradeSRCls.UploadDocs();
        objGsUpgradeSRCls.ConfirmUpgrade();
                
        objSR.Internal_SR_Status__c = lstSRStatus[2].Id;
        objSR.External_SR_Status__c = lstSRStatus[2].Id;
        objSR.Upgrade_Docs_Attached__c = true;
        objSR.SAP_Unique_No__c = '12345675000215';
        update objSR;
        test.startTest();
        GsUpgradeSRCls thisGsUpgrade = new GsUpgradeSRCls();
            thisGsUpgrade.ServiceRequest = objSR;
            thisGsUpgrade.UpgradeType ='Cross';
            thisGsUpgrade.ServiceRequest.Upgrade_Docs_Attached__c = false;
            //thisGsUpgrade.ServiceRequest.Visa_Expiry_Date__c = system.today().addDays(300);
            thisGsUpgrade.ServiceRequest.Passport_Date_of_Expiry__c = system.today().addDays(300);
            thisGsUpgrade.ServiceRequest.Last_Date_of_Entry__c = system.today().addDays(-100);
           // thisGsUpgrade.ServiceRequest.Current_Visa_Status__c = 'Other';
            thisGsUpgrade.ServiceRequest.Avail_Courier_Services__c = 'No';
            thisGsUpgrade.ServiceRequest.Type_of_Request__c = 'Applicant Outside UAE';
            thisGsUpgrade.PriceItemInfo();
            thisGsUpgrade.CheckDocs();
        thisGsUpgrade.ConfirmPayment();
        thisGsUpgrade.CancelPayment();
        thisGsUpgrade.UploadDocs();
        thisGsUpgrade.ConfirmUpgrade();
        
        //GsUpgradeSRCls.UpgradePushToSAP(lstPriceItems[0].Id);
         thisGsUpgrade.crossVisastatus ='On Arrival Visa';
        thisGsUpgrade.DisableUploads();
        
        test.stopTest();
    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'AccountBal';
        objEP.URL__c = 'http://AccountBal/sampledata';
        lstCS.add(objEP);
        insert lstCS;
                
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Block_Trigger__c objBT = new Block_Trigger__c();
        objBT.Name = 'Block';
        objBT.Block_Web_Services__c = true;
        insert objBT;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='CLEARED',Code__c='CLEARED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstSRStatus.add(new SR_Status__c(Name='Posted',Code__c='POSTED'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company Services';
        objTemplate.Is_Exit_Process__c = true;
        objTemplate.Active__c = true;
        insert objTemplate;
                
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'DIFC New Visa';
        lstProducts.add(objProd);
        
        insert lstProducts;
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC New Sponsorship Visa New';
        objPL.DEV_Id__c = 'GO-00001';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00001';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            if(objP.Material_Code__c == 'GO-00002')
                objDP.Unit_Price__c = 2234;
            else
                objDP.Unit_Price__c = 1234;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }       
        
        GS_Upgrade_Matrix__c objGSMatrix = new GS_Upgrade_Matrix__c();
        objGSMatrix.Name = 'GO-00001';
        objGSMatrix.Material_Code__c = 'GO-00001';
        objGSMatrix.VIP_Material_Code__c = 'GO-VIP01';
        objGSMatrix.Cross_Material_Code__c = 'GO-00002';
        objGSMatrix.Entry_Permit_Material_Code__c = 'GO-00002';
        objGSMatrix.Entry_Permit_Margin__c = 1000;
        objGSMatrix.Cross_Material_Margin__c = 1000;
        objGSMatrix.VIP_Margin__c = 1000;
        insert objGSMatrix;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_New');
        //objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[1].Id;
        objSR.External_SR_Status__c = lstSRStatus[1].Id;
        objSR.Mobile_Number__c ='+97152123456';
        insert objSR;
        
        objSR.Internal_SR_Status__c = lstSRStatus[1].Id;
        objSR.External_SR_Status__c = lstSRStatus[1].Id;
        objSR.SAP_Unique_No__c ='12345678';
        update objSR;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 3210;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        lstPriceItems.add(objItem);
        
        insert lstPriceItems;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        insert objStep;
        
        Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
        
        list<SAPGSWebServices.ZSF_S_GS_SERV_OP> lstGSItems = new list<SAPGSWebServices.ZSF_S_GS_SERV_OP>();
        SAPGSWebServices.ZSF_S_GS_SERV_OP objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'N';
        objGSItem.SOPRP = 'N';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        //objGSItem.SGUID = SRItemId;
        lstGSItems.add(objGSItem);
        objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'P';
        objGSItem.SOPRP = 'N';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        //objGSItem.SGUID = SRItemId;
        lstGSItems.add(objGSItem);
        TestGsCRMServiceMock.lstGSPriceItems = lstGSItems;
        //GsUpgradeSRCls.UpgradePushToSAP(lstPriceItems[0].Id);
        

        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        Apexpages.currentPage().getParameters().put('type','VIP');
           
           GsUpgradeSRCls thisGsUpgrade = new GsUpgradeSRCls();
            thisGsUpgrade.ServiceRequest = objSR;
            thisGsUpgrade.UpgradeType ='Cross';
             thisGsUpgrade.UpgradeMTRCode = 'GO-00002';
            thisGsUpgrade.ServiceRequest.Upgrade_Docs_Attached__c = false;
            //thisGsUpgrade.ServiceRequest.Visa_Expiry_Date__c = system.today().addDays(300);
            thisGsUpgrade.ServiceRequest.Passport_Date_of_Expiry__c = system.today().addDays(300);
            thisGsUpgrade.ServiceRequest.Last_Date_of_Entry__c = system.today().addDays(-100);
           // thisGsUpgrade.ServiceRequest.Current_Visa_Status__c = 'Other';
            thisGsUpgrade.ServiceRequest.Avail_Courier_Services__c = 'No';
            thisGsUpgrade.ServiceRequest.Type_of_Request__c = 'Applicant Outside UAE';
            thisGsUpgrade.PriceItemInfo();
            thisGsUpgrade.CheckDocs();
        thisGsUpgrade.ConfirmPayment();
        thisGsUpgrade.CancelPayment();
        thisGsUpgrade.UploadDocs();
        thisGsUpgrade.ConfirmUpgrade();
        thisGsUpgrade.crossVisastatus ='Cancelled Visa';
        thisGsUpgrade.DisableUploads();
    }
      static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'AccountBal';
        objEP.URL__c = 'http://AccountBal/sampledata';
        lstCS.add(objEP);
        insert lstCS;
                
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstSRStatus.add(new SR_Status__c(Name='Posted',Code__c='POSTED'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Required Docs to Upload';
        insert objDocMaster;
        
        Document_Details__c objDocDetails = new Document_Details__c();
        objDocDetails.Account__c = objAccount.Id;
        objDocDetails.EXPIRY_DATE__c = system.today().addYears(1);
        objDocDetails.DOCUMENT_STATUS__c = 'Active';
        objDocDetails.ISSUE_DATE__c = system.today();
        objDocDetails.DOCUMENT_TYPE__c = 'PIC';
        insert objDocDetails;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'DIFC New Visa';
        lstProducts.add(objProd);
        
        insert lstProducts;
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC New Sponsorship Visa New';
        objPL.DEV_Id__c = 'GO-00001';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00001';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC New Sponsorship Visa New - Inside';
        objPL.DEV_Id__c = 'GO-00002';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00002';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            if(objP.Material_Code__c == 'GO-00002')
                objDP.Unit_Price__c = 2234;
            else
                objDP.Unit_Price__c = 1234;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }       
        
        GS_Upgrade_Matrix__c objGSMatrix = new GS_Upgrade_Matrix__c();
        objGSMatrix.Name = 'GO-00001';
        objGSMatrix.Material_Code__c = 'GO-00001';
        objGSMatrix.VIP_Material_Code__c = 'GO-VIP01';
        objGSMatrix.Cross_Material_Code__c = 'GO-00002';
        objGSMatrix.Entry_Permit_Material_Code__c = 'GO-00002';
        objGSMatrix.Entry_Permit_Margin__c = 1000;
        objGSMatrix.Cross_Material_Margin__c = 1000;
        objGSMatrix.VIP_Margin__c = 1000;
        insert objGSMatrix;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_New');
        //objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.Mobile_Number__c ='+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.SAP_Unique_No__c = '123456';
        insert objSR;
        
        objSR.Internal_SR_Status__c = lstSRStatus[1].Id;
        objSR.External_SR_Status__c = lstSRStatus[1].Id;
        objSR.SAP_Unique_No__c ='12343434343434';
        
        update objSR;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 3210;
        
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        lstPriceItems.add(objItem);
        
        insert lstPriceItems;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        insert objStep;
        
        //delete [select Id from SR_Doc__c where Service_Request__c=:objSR.Id];
        
        
            string issueFineId = '';
         for(RecordType rectyp:[select id from RecordType where DeveloperName='GS_Comfort_Letters' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        
        
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__r = objAccount;
        objSR1.Customer__c = objAccount.id;
        objSR1.RecordTypeId = issueFineId;
        objSR1.submitted_date__c = date.today();
        
    objSR1.Type_of_Service__c = 'Employment Confirmation Letter; ';
    objSR1.Emirate__c = 'Test';
    objSR1.Monthly_Salary_AED__c = 20000;
    objSR1.Passport_Number__c = 'Test';
    objSR1.Name_of_Addressee__c = 'Test';
    objSR1.Date_of_DIFC_Visa_Issuance__c = system.today();
    objSR1.Name_of_Police_Station__c = 'Test';
    objSR1.Service_Category__c = 'Test';
    objSR1.Others_Please_Specify__c = 'Test';
    objSR1.Is_Applicant_Salary_Above_AED_20_000__c = 'Yes';
    objSR1.car_registration_number__c = 'Test';
    objSR1.entity_name__c = 'Test';
    objSR1.company_name__c = 'Test';
    objSR1.business_name__c = 'Test';
    objSR1.Post_Code__c = 'Test';
    objSR1.Position__c = 'Test';
    objSR1.Sponsor_Street__c = 'Test';
    objSR1.Sponsor_First_Name__c = 'Test';
    objSR1.Sponsor_Last_Name__c = 'Test';
    objSR1.Sponsor_Middle_Name__c = 'Test';
    objSR1.Sponsor_Building__c = 'Test';
    objSR1.Sponsor_Mother_Full_Name__c = 'Test';
    objSR1.Sponsor_P_O_Box__c = 'Test';
    objSR1.Sponsor_Passport_No__c = 'Test';
    objSR1.Monthly_Accommodation__c = 10000;
    objSR1.Mortgage_Amount__c = 10000;
    objSR1.Mother_Full_Name__c = 'Test';
    objSR1.Building_Name__c = 'Test';
    objSR1.Middle_Name__c = 'Test';
    objSR1.First_Name__c = 'Test';
    objSR1.Last_Name__c = 'Test';
    objSR1.UID_Number__c = 'Test';
    objSR1.Cage_No__c = 'Test';
    objSR1.Other_Monthly_Allowances__c = 10000;
    objSR1.Monthly_Basic_Salary__c = 10000;
    objSR1.City_Town__c = 'Test';
    objSR1.Commercial_Activity__c = 'Test';
    objSR1.DIFC_store_specifications__c = 'Test';
    objSR1.Usage_Type__c = 'Test';
    
        insert objSR1; 
        
        
        
        Apexpages.currentPage().getParameters().put('type','VIP');
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        test.startTest();
        GsUpgradeSRCls thisGsUpgrade = new GsUpgradeSRCls();
        thisGsUpgrade.ServiceRequest = objSR;
        thisGsUpgrade.UpgradeType ='Cross';   
       
        thisGsUpgrade.ServiceRequest.Upgrade_Docs_Attached__c = false;
        thisGsUpgrade.ServiceRequest.Passport_Date_of_Expiry__c = system.today().addDays(300);
        thisGsUpgrade.ServiceRequest.Last_Date_of_Entry__c = system.today().addDays(-100);
        thisGsUpgrade.ServiceRequest.Avail_Courier_Services__c = 'No';
        thisGsUpgrade.ServiceRequest.Type_of_Request__c = 'Applicant Outside UAE';
        thisGsUpgrade.crossVisastatus ='Tourist Visa';
        thisGsUpgrade.DisableUploads();
        
        thisGsUpgrade.ConfirmUpgrade();
        test.stopTest();
    }
}