/*
        Author      :   Arun 
        Description :   Class to suport Lease Registration Security Deposit Service request 
        
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    29-06-2020 Arun Singh  Created
    
*/    

public with sharing class clsLeaseRegistration_Security_BP {

    public PageReference Pageload() {
    
    String SRID;
    SRID= ApexPages.currentPage().getParameters().get('id');
    
    Service_Request__c Objlist=[select id,Unit__c from Service_Request__c where id=:SRID];
    Security_Deposit_Cls ObjD=new Security_Deposit_Cls();
    ObjD.BP_creation(Objlist);


    PageReference ObjPage=Page.PaySecurityDeposit;
     ObjPage.getParameters().put('id',SRID);
    ObjPage.setRedirect(true);
        return ObjPage;
    }

}