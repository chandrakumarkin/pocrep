global class Execute_CC_CreateRelationClassUsingBatch implements Database.Batchable<sObject> {
    String srId = '';
    String accountId = '';
    
    private final String amendBodyCorpRecTypeName =  OB_ConstantUtility.AMENDMENT_CORPORATE_RT.developerName;
    private final String amendIndRecTypeName =  OB_ConstantUtility.AMENDMENT_INDIVIDUAL_RT.developerName;
    global Execute_CC_CreateRelationClassUsingBatch(String srqId){
        this.srId = srqId;
        //this.accountId = accId;
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        /*HexaBPM__Step__c stp = [Select Id,Name,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Name,HexaBPM__SR__c
FROM HexaBPM__Step__c
Where Id= ''
];*/
        /*String query = 'Select Id,Name,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Name,HexaBPM__SR__c FROM HexaBPM__Step__c';

query += ' Where Id=\''+stepId+'\'';*/

        
        String query ='';
        query += 'Select Id,Roles__c,Individual_Corporate_Name__c,Registered_with_DIFC__c,ServiceRequest__c,ServiceRequest__r.HexaBPM__Customer__c,Recordtype.developername,Registration_No__c,Reason_for_exemption_under_DIFC_UBO__c,UBO_Country_of_Registration__c,Decree_No__c,Country_of_Registration__c,Contact__c,customer__c,Partner_Type__c,ServiceRequest__r.HexaBPM__Parent_SR__c,Title__c,DI_First_Name__c,DI_Last_Name__c,Middle_Name__c,DI_Email__c,Mobile__c,Last_Name__c,First_Name__c,Date_of_Birth__c,Place_of_Birth__c,Passport_No__c,Nationality_list__c,Email__c,Apartment_or_Villa_Number_c__c,Address__c,PO_Box__c,Permanent_Native_City__c,Emirate_State_Province__c,Permanent_Native_Country__c,Passport_Expiry_Date__c,Gender__c,Country__c,Place_of_Issue__c,Passport_Issue_Date__c,Are_you_a_resident_in_the_U_A_E__c,Is_this_individual_a_PEP__c,Is_this_member_a_designated_Member__c,Company_Name__c,Entity_Name__r.Name,Former_Name__c,Date_Of_Registration__c,Telephone_No__c,Place_of_Registration__c,Entity_Name__c,Country_of_source_of_income__c,E_I_D_no__c,U_I_D_No__c,Emirate__c,Source_and_origin_of_funds_for_entity__c,Source_of_income_wealth__c,Type_of_Authority__c,Please_provide_the_type_of_authority__c,Financial_Service_Regulator__c,Trading_Name__c,Select_the_Qualified_Applicant_Type__c,Recognised_Exchange__c,Is_Shareholder_Qualifying_Applicant__c,Please_state_the_name_of_the_Regulator__c,Name_of_the_Regulated_Firm__c,Name_of_the_exchange__c,Jurisdiction__c, ';
        
        query += ' (Select Id,Status__c,Account__c,Account_Share__c,Account_Share__r.Status__c,Shareholder_Account__c,Shareholder_Contact__c,OB_amendment__c,OB_amendment__r.Other_contribution_type__c,OB_amendment__r.Type_of_Contribution__c,Sys_Proposed_No_of_Shares__c,No_of_Shares__c  From Shareholder_Details__r ) ';
        
        query += ' From HexaBPM_Amendment__c  ';
        query += ' Where Roles__c excludes (\'UBO\')  ';
        query += ' AND Roles__c != \'\'  ';
        query += ' AND ServiceRequest__c =\''+srId+'\'';
        query += ' AND ( Recordtype.developerName =:amendBodyCorpRecTypeName OR Recordtype.developerName =:amendIndRecTypeName )  ';
        
        
        
        System.debug('ZZZ query-->'+JSON.serialize(query));
        
        return Database.getQueryLocator(query);
    }
    
    
    global void execute(Database.BatchableContext BC, List<HexaBPM_Amendment__c> scope){
        System.debug('ZZZ scope.size()-->'+scope.size());
        
        string strResult = 'Success';
        //contantIndividualRecordTypeID = OB_QueryUtilityClass.getRecordtypeID('Contact', 'Individual');
        // intialize: store Amendment Id and shareholder relationship record
        //Map<Id,Relationship__c> amendmentIDRelatioshipMap = new Map<Id,Relationship__c>();
        
        // list of amendment record
        Map<Id,HexaBPM_Amendment__c> amndMap = new Map<Id,HexaBPM_Amendment__c>();
        
        // list of shareholder record per amendment
        Map<id,list<Shareholder_Detail__c>> sharholderExistingMap = new Map<id,list<Shareholder_Detail__c>>();
        
        // set account registration number for checking existing in system
        Set<String> registrationNumberSet = new Set<String>();
        
        // map of passport and passpport-Nationality 
        Map<String,String> passportNationalityMap = new Map<String,String>();
        
        // store existing registration number  and account ID 
        //Map<String,string> registrationNumberAccountIdMap = new Map<String,string>();
        
        // store existing passport number  and contact ID 
        Map<String,string> passportContactIdMap = new Map<String,string>(); 
        

		// insert new account and contact detail
        Map<string,sObject> accountContactNewMap = new Map<string,sObject>();
        
        String accountID = accountId;
        
        
        If(!scope.isEmpty()){
            //CC_CreateRelationship.createRelatioship(scope[0]);
            for(HexaBPM_Amendment__c amd : scope){
                if(amd.Recordtype.developerName == amendBodyCorpRecTypeName
                   &&  String.IsBlank(amd.Individual_Corporate_Name__c)){
                       // body corporate has blank company name bad data
                   }else{
                       amndMap.put(amd.Id,amd);
                   }
                // shareholder record detail    
                if(amd.Shareholder_Details__r != null && amd.Shareholder_Details__r.size() != 0){
                    sharholderExistingMap.put(amd.id,amd.Shareholder_Details__r);
                }
                
                if(amd.Recordtype.developername == amendIndRecTypeName
                   && String.IsNotBlank(amd.Passport_No__c)
                   && String.IsNotBlank(amd.Nationality_list__c)){
                       
                       String passport = String.isNotBlank(amd.Passport_No__c) ? amd.Passport_No__c.toLowerCase():'';
                       String nationality = String.IsNotBlank(amd.Nationality_list__c) ? amd.Nationality_list__c.toLowerCase() :'';
                       String passportNationlity = (String.isNotBlank(passport) ? passport + '_' :'') + (String.IsNotBlank(nationality) ? nationality :'');
                       
                       //get passport nationality for checking any existion contact record 
                       passportNationalityMap.put(passport,passportNationlity);
                       
                   }else if( amd.Entity_Name__c == null && String.IsNotBlank(amd.Registration_No__c)){
                       
                       //get registration for checking any existion account record
                       //registrationNumberSet.add(amd.Registration_No__c);
                       
                   }else if(amd.Recordtype.developername == amendIndRecTypeName
                            && String.IsNotBlank(amd.Roles__c)
                            && amd.Roles__c.containsIgnorecase('DPO')){
                                //For DPO passport nationality will be blank still will create relationship and contact for each DPO
                                // new contact list
                                Contact con = new Contact();
                                con.AccountId = amd.ServiceRequest__r.HexaBPM__Customer__c;
                                con = CC_CreateRelationship.getObjectContact(con,amd);
                                accountContactNewMap.put(amd.Id,con);
                            }
            }
            
            //CC_CreateRelationship.createRelatioshipBulk(amndMap, sharholderExistingMap, passportNationalityMap, accountContactNewMap);
            
            CC_CreateRelationshipQueueable tt = new CC_CreateRelationshipQueueable(amndMap, sharholderExistingMap, passportNationalityMap, accountContactNewMap);
            Id jobId = System.enqueueJob(tt);
            System.debug('ZZZ queueable jobid-->'+jobId);
            
        }
        
    }
    
    global void finish(Database.BatchableContext BC){
        
    }
}