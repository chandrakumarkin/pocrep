/*******************************************************************************************************************************
 *  Author        : Mudasir Wani
 *  Company       : DIFC
 *  Date     	  : 15th Novemnber 2019    
 *  Functionality : #7703 - Contractor Trade License should be auto populated for the Fitout project from the contractor wallet
                
********************************************************************************************************************************/

public without sharing class Contractor_Commercial_License_population {
    
    @InvocableMethod(label='Copy Contractor Commercial License' description='Copy the contractor license copy in this document') 
    public static void copyContractor_Commercial_License(List<Id> srDocIds){
        //Save the SR Doc as we need to create a document agains this record.
        Map<id,SR_Doc__c> srDocuments = New Map<id,SR_Doc__c>();
        Set<id> userIdSet = new Set<id>();
        for( SR_Doc__c srDoc : [Select id,Status__c,createdById,service_request__r.createdById from SR_Doc__c where id IN : srDocIds]){
            srDocuments.put(srDoc.service_request__r.createdById,srDoc);
        }
        //Get the contractor account 
        Map<id,id> accountIds = new Map<id,id>();
        for(User  userRec : [Select id,contact.Accountid,contact.Contractor__c from User where id IN : srDocuments.keyset()]){
            if(userRec.Contact != NULL && userRec.Contact.AccountId != NULL) accountIds.put(userRec.Contact.Contractor__c,userRec.id);
        }
        
        Map<id,id> accountAndSRDocParentIdMap = new Map<id,id>();
        //Now get the contractor wallet requests
        List<Service_request__c> contractorPortalSRs = new List<Service_request__c>();
        for(Service_request__c servRec : [Select id,Customer__c,(select id,name,Customer__c from SR_Docs__r where (Name='Contractor License Copy' OR Name='Contractor Commercial License') limit 1) 
                                                    from service_Request__c where Customer__c IN :accountIds.keyset() 
                                                    and (RecordType.DeveloperName ='Contractor_Registration' OR RecordType.DeveloperName='Update_License_Details') and External_status_Name__c ='Approved' order by createddate desc limit 1]){
                for(SR_Doc__c srDoc :servRec.SR_Docs__r ){ accountAndSRDocParentIdMap.put(srDoc.id,servRec.Customer__c);}
        }
        //Now fetch tha attachments related to the requests 
        //Prepare and insert new list of attachments 
        List<Attachment> attachmentList = new List<Attachment>();
        for(Attachment attach : [Select id,name,body,ParentId from Attachment where ParentId IN : accountAndSRDocParentIdMap.keyset()]){  Attachment attachRec = attach.clone();AttachRec.ParentId = srDocuments.get(accountIds.get(accountAndSRDocParentIdMap.get(attach.ParentId))).id;attachmentList.add(AttachRec); }
        insert attachmentList;
        
        //Now get the attachmentList and update the SR DOC 
        List<SR_Doc__c> srDocStatusUpdate = new List<SR_Doc__c>();
        for(Attachment document : attachmentList){ srDocStatusUpdate.add(new SR_Doc__c( id=document.parentId ,Doc_ID__c = string.valueOf(document.Id),Status__c = 'Uploaded'));}
        update srDocStatusUpdate;
    }
}