@isTest
public class DIFCFitoutMilestonePenaltyUtil_Test {
	public static testMethod void testMilestoneSummaryCreation() 
    {
        Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
        insert objAccount;
        
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.RF_Valid_To__c = system.today().addDays(10);
        objLease.RF_Valid_From__c = system.today();
        objLease.Lease_Types__c = 'Retail Lease';
        insert objLease;
        
        list<Building__c> lstBuilding = new list<Building__c>();
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP On Plan';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '2300';
        objBuilding.SAP_Building_No__c = '123456';
        lstBuilding.add(objBuilding);
        insert lstBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c =  lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'office';
        lstUnits.add(objUnit);
        
        List<Lease_Partner__c> ListLP=new List<Lease_Partner__c>();
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = objLease.Id;
        objLP.Account__c = objAccount.Id;
        objLP.Unit__c = lstUnits[0].Id;
        objLP.status__c = 'Draft';
        objLP.Type_of_Lease__c = 'Leased';
        objLP.Is_License_to_Occupy__c = true;
        objLP.Is_6_Series__c = false;
        ListLP.add(objLP);
        insert ListLP;
        Service_Request__c tempSr = new Service_Request__c();
        tempSr.Mobile_Number__c = '+9715000000000';
        tempSr.Foreign_Registered_Phone__c = '+9715000000000';
        //insert tempSr;
        
        
    	Fitout_Milestone_Penalty__c fitoutPenSumRec 			= 	new Fitout_Milestone_Penalty__c();
        fitoutPenSumRec.Unit_Handover_Date__c 					= 	system.today();
        fitoutPenSumRec.Additional_Rent_Free_Period_Days__c		= 	1;
        fitoutPenSumRec.Additional_Tenant_email__c				=	'mudasir.saleem@gmail.com';
        fitoutPenSumRec.Next_Conceptual_Penalty_Date__c			=	system.today();
        fitoutPenSumRec.Next_Detail_Design_Penalty_Date__c		=	system.today();
        fitoutPenSumRec.Next_First_Inspection_Penalty_Date__c	=	system.today();
        fitoutPenSumRec.Next_Final_Inspection_Penalty_Date__c	=	system.today();
        insert fitoutPenSumRec;
        
        Test.startTest();
        	Fitout_Milestone_Penalty__c fitoutMilestonePen = DIFCFitoutMilestonePenaltyUtil.createFitoutMilestonePenalty(ListLP[0].id, objAccount.id, null, system.today());
        	DIFCFitoutMilestonePenaltyUtil.upsertFitoutMilestonePenalty(new list<Fitout_Milestone_Penalty__c> {fitoutPenSumRec});
        Test.stopTest();
    }
}