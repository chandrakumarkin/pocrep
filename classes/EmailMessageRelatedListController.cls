public class EmailMessageRelatedListController {
    
    public final Case objCase;
    public List<EmailMessage> caseEmailMesssageList {get;set;}
    public String emailMessageId {get;set;}

    public EmailMessageRelatedListController(ApexPages.StandardController stdController){
        objCase = (Case)stdController.getRecord();
        caseEmailMesssageList = new List<EmailMessage>();
        caseEmailMesssageList = [SELECT Id,Incoming,Subject,TextBody,Status,FromAddress,MessageDate FROM EmailMessage WHERE ParentId = :objCase.Id ORDER BY MessageDate DESC]; 
        emailMessageId = '';
    }
    
    public void deleteEMessage(){
        system.debug('@@TEST');
        //emailMessageId = Apexpages.currentPage().getParameters().get('emailIdStr');
        system.debug('@@ID: '+emailMessageId);
        if(emailMessageId != '' && emailMessageId != null){
            List<EmailMessage> delEmailMsg = [SELECT Id FROM EmailMessage WHERE Id = :emailMessageId];
            if(!delEmailMsg.isEmpty()){
                delete delEmailMsg;
            }
        }
    }
}