/******************************************
*Class: CarParking_ReportNotification
*Description: Report will run this class to send the car parking daily collection report and submit the Account statement SR
*Modified:  v1.1 #5862 selva     Added check box to avoid duplication of SR in daily car parking collection 
*14/Nov/2019    v1.2 #7590 Added IsCarParkCalculated__c filter condition
******************************************/
public class CarParking_ReportNotification implements Reports.NotificationAction{

    public void execute(Reports.NotificationActionContext context){

        //Reports.ReportInstance c = context.getReportInstance();
        Reports.ReportResults results = context.getReportInstance().getReportResults();
        
        ID reportID = results.getReportMetadata().getID();
       
        string url = Label.Salesforce_URL+reportID ;
        String name = results.getReportMetadata().getName();
        Reports.ReportFactWithDetails factDetails  = (Reports.ReportFactWithDetails)results.getFactMap().get('0!T');
    //   Reports.ReportFactWithSummaries  factSummaries   = (Reports.ReportFactWithSummaries )results.getFactMap().get('!T');
        integer NumberOfRows = 0;
        if(!test.isRunningTest()){
          
                
                if(factDetails  != null)
               NumberOfRows = factDetails.getRows().size();
            else
                NumberOfRows = -1;
        }
           
            
        ApexPages.PageReference objPage = new ApexPages.PageReference('/'+reportID +'?csv=1');

      
       if(test.isRunningTest()){
           SubmitCarPendingSR(Blob.valueOf('UNIT.csv'));
       }
       else
          SubmitCarPendingSR(objPage.getContent());
      
       


    }
    
    
    void SubmitCarPendingSR(Blob aAttachmentBlob){
        
        //Added v1.1 #5862
        List<SR_PRICE_ITEM__C> updatesrPriceItem = new List<SR_PRICE_ITEM__C>();
        
        List<SR_PRICE_ITEM__C > lstSRPriceItem = [SELECT ID,IsCarParkCalculated__c,Total_Service_Amount__c,ServiceRequest__c,
                                                ServiceRequest__r.Submitted_Date__c ,Status__c FROM SR_PRICE_ITEM__C 
                                                where Is_Valid__c = true AND
                                                Status__c = 'Invoiced' AND ServiceRequest__r.Record_Type_Name__c = 'Car_Parking' and IsCarParkCalculated__c = false];
        
        system.debug('$$$$ WORKING' +lstSRPriceItem);
        Decimal totalAmount = 0 ;
        for(SR_PRICE_ITEM__C iPriceItem : lstSRPriceItem){
            totalAmount += iPriceItem.Total_Service_Amount__c;
            updatesrPriceItem.add(iPriceItem); //Added v1.1 #5862
        }
        
        SR_Template__c srTemplate = [Select ID, Estimated_Hours__c from SR_Template__c where Name = 'Daily Parking Collection' limit 1];
        ID SRSubmit = [select id,name from SR_Status__c where Code__C  = 'Submitted'].id; 
        
        Service_Request__c newSR = new Service_Request__c();
        newSR.Customer__c = [Select Id from Account where name = 'Test Account' limit 1].ID;
        newSR.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Daily Parking Collection').getRecordTypeId();
        
        insert newSR;
        
        newSR.Submitted_Date__c = system.today();
        newSR.Submitted_DateTime__c = system.now();
        newSR.Internal_SR_Status__c = SRSubmit;
        newSR.External_SR_Status__c = SRSubmit;
        newSR.Transaction_Amount__c = totalAmount;
        update newSR;
        
        
        //create document for daily car parking
        SR_Doc__c srDoc = new SR_Doc__c();
        srDoc.Document_Master__c = [Select Id from Document_Master__c where Name = 'Daily Car Parking Collection Document' limit 1].ID;
        srDoc.Service_Request__c = newSR.ID;
        srDoc.Name = 'Daily Car Parking Collection';
        insert srDoc;
        
        //add attachment in SR doc excel was generated
        
        Attachment attachment = new Attachment();
        attachment.Body = aAttachmentBlob;
        attachment.Name = String.valueOf('collectionDocument.csv');
        attachment.ParentId = srDoc.ID; 
        insert attachment;
        
        //UPDATE sr DOC ID
        
        srDoc.Doc_ID__c = attachment.ID;
        srDoc.Status__c = 'Uploaded';
        //update srDoc;
        Database.SaveResult updateResults = Database.update(srDoc, false);
        //for(Integer i=0;i<updateResults.size();i++){
            if (updateResults.isSuccess()){
                for(SR_PRICE_ITEM__C itr:updatesrPriceItem){
                        itr.IsCarParkCalculated__c = true;//v1.1
                }
                update updatesrPriceItem; //Added v1.1 #5862
            } 
        //}
    }   
            
    
}