/******************************************************************************************
 *  Name        : ContactTriggerHelper 
 *  Author      : Durga Prasad
 *  Description : Contract Trigger helper to create portal user
 ----------------------------------------------------------------------------------------                               
  Modification History 
 ----------------------------------------------------------------------------------------
  V.No    Date        Updated By    Description
 ---------------------------------------------------------------------------------------- 
  v1.1    17-Mar-2020 Durga         Code to check community user exists with same email and stop creating User.
  v1.2    19-Mar-2020 Durga         Code to assign the read only profile and assign role based on the contact data. 
  v1.3    19-Mar-2020 Durga         UpdateAccountContactRelation method to update the Role and Access Level. 
  v1.4    7-May-2020 Prateek        new logic for duplicate user in create portal user method
  v1.5    03-Jan-2021 Salma         Updating the username on the contact & CP SR when portal user is created
*******************************************************************************************/
global without sharing class OB_ContactTriggerHelper {
    
    /*
        Method      :   UpdateAccountContactRelation
        Description :   Method to update the AccountContactRole based on the contact value.
    */
    public static void UpdateAccountContactRelation(list<Contact> TriggerNew){//v1.3
        list<AccountContactRelation> lstACRTBU = new list<AccountContactRelation>();
        map<string,Contact> MapNewContacts = new map<string,Contact>();
        for(Contact con:TriggerNew){
            if(con.AccountId!=null && con.Send_Portal_Login_Link__c=='Yes'){
                MapNewContacts.put(con.Id,con);
            }
        }
        if(MapNewContacts.size()>0){
            for(AccountContactRelation ACR:[Select Id,Roles,Access_Level__c,ContactId from AccountContactRelation where ContactId IN:MapNewContacts.keyset() and IsActive=true and IsDirect=true]){
                if(MapNewContacts.get(ACR.ContactId).OB_Roles__c!=null && MapNewContacts.get(ACR.ContactId).OB_Access_Level__c!=null){
                    ACR.Roles = MapNewContacts.get(ACR.ContactId).OB_Roles__c;
                    ACR.Access_Level__c = MapNewContacts.get(ACR.ContactId).OB_Access_Level__c;
                    lstACRTBU.add(ACR);
                }else{
                    ACR.Roles = 'Company Services;Property Services;Super User';
                    ACR.Access_Level__c = 'Read/Write';
                    lstACRTBU.add(ACR);
                }
            }
        }
        if(lstACRTBU.size()>0)
            update lstACRTBU;
    }
    
    /*
        Method to create Relationship for Portal User Contact
    */
    public static void CreateRelationship(list<Contact> PortalUserContacts){
        list<Relationship__c> lstRelationToBeCreated = new list<Relationship__c>();
        for(Contact contact:PortalUserContacts){
            Relationship__c relaObject = new Relationship__c();
            relaObject.Object_Contact__c = contact.Id;
            relaObject.Subject_Account__c = contact.AccountId;
            relaObject.Relationship_Type__c = 'Has Principal User';
            relaObject.Start_Date__c = Date.today(); 
            relaObject.Active__c = true;
            relaObject.Relationship_Group__c = 'ROC';
            //relaObject.OwnerId = RecordOwnerId;
            lstRelationToBeCreated.add(relaObject);
        }
        if(lstRelationToBeCreated.size()>0){
            try{
                insert lstRelationToBeCreated;
            }catch(Exception e){
                Log__c objLog = new Log__c(Description__c = 'Exception on ContactTriggerHelper.CreateRelationship() - Line:26 :'+e.getMessage(),Type__c = 'Contact Trigger Code to create Relationship for Portal User Contacts');
                insert objLog;
            }
        }
    }

    public static void CreateACR(map<string,string> relToBeCreated){
        list<AccountContactRelation> lstACRTBU = new list<AccountContactRelation>();
      
        try{
            if(relToBeCreated.size()>0){
                for(string contactid : relToBeCreated.keySet()){
                    AccountContactRelation ACR = new AccountContactRelation();
                    ACR.ContactId = contactid;
                    ACR.Accountid = relToBeCreated.get(contactid);
                    ACR.Roles = 'Company Services;Property Services;Super User';
                    ACR.Access_Level__c = 'Read/Write';
                    lstACRTBU.add(ACR);
                }          
                
            }
    
            if(lstACRTBU.size()>0){
                insert lstACRTBU;
            }
        }catch(Exception e) {
                list<Log__c> lstLogs = new list<Log__c>();
                
                    lstLogs.add(new Log__c(Description__c = 'Exception on ContactTriggerHelper.CreatePortalUser() - Line:119 :'+e.getMessage(),Type__c = 'dup acr insert'));
                
                insert lstLogs;
        }
       
            
    }

    
    /*
        Method to create Portal User
    */
    @future
    public static void CreatePortalUser(set<string> setContactIds){
        system.debug('===CreatePortalUser=====');
        if(setContactIds!=null && setContactIds.size()>0){
            list<Contact> newContacts = new list<Contact>();
            set<string> setEmails = new set<string>();//v1.1
            map<string,string> MapUserByEmail = new map<string,string>();
            //v1.2
            for(Contact objcon:[Select Id,Title,FirstName,LastName,Email,MobilePhone,OB_Access_Level__c,OB_Roles__c,Phone,AccountId,Account.Name,Contact_Source__c,OB_Self_Registered_Contact__c from Contact where Id IN:setContactIds]){
                newContacts.add(objcon);
                if(objcon.Email!=null)//v1.1
                    setEmails.add(objcon.Email);//v1.1
            }
            if(setEmails.size()>0){//v1.1
                for(User usr:[Select Id,Email,contactId from User where Email IN:setEmails and IsActive=true and ContactId!=null]){
                    MapUserByEmail.put(usr.Email.tolowercase(),usr.contactId);
                }
            }
            try{
                list<User> lstPortalUser = new list<User>();
                List<Contact> contactList = new List<Contact>();//v1.5 
                Map<id,String> contactUserNameMap = new Map<id,String>();//v1.5
                set<Id> accountIds = new set<Id>();//v1.5
                map<string,string> newACRtoBeCreated = new map<string,string>();
                set<string> setProvidedEmail = new set<string>();
                for(Contact con:newContacts){
                    if(con.Email!=null){
                      String emailStr = con.Email.split('@')[0]+'%';
                      system.debug('===CreatePortalUser===== emailStr  '+emailStr);
                      setProvidedEmail.add(emailStr);
                      //setProvidedEmail.add(con.Email);
                    }
                }
                map<string,User> mapUserNames = new map<string,User>();
                if(setProvidedEmail.size()>0){
                    //for(User objUsr : [select Id,Username from User where Email IN:setProvidedEmail order by Username]){
                    for(User objUsr : [select Id,Username from User where Username like :setProvidedEmail order by Username]){
                        objUsr.Username = objUsr.Username.toLowerCase();
                        mapUserNames.put(objUsr.Username.tolowercase(),objUsr);
                    }
                }
                system.debug('mapUserNames==>'+mapUserNames);
                system.debug('setProvidedEmail==>'+setProvidedEmail);
                string customerCommunityProfileId = label.Onboarding_Profile_Id;
                
                system.debug('customerCommunityProfileId==>'+customerCommunityProfileId);
                for(Contact contact : newContacts) {
                    if(contact.Email!=null && MapUserByEmail.get(contact.Email.tolowercase())==null){//v1.1
                        if(contact.OB_Access_Level__c=='Read Only')//v1.2
                            customerCommunityProfileId = label.OB_OnboardingReadonlyProfile;
                        string newUserName = contact.Email.split('@')[0]+Label.Portal_UserDomain.toLowerCase();
                        newUserName = newUserName.tolowercase();
                        system.debug('newUserName==>'+newUserName);
                        system.debug(newUserName+' exists = '+mapUserNames.get(newUserName));
                        if(mapUserNames.containsKey(newUserName)){                         
                            for(Integer i=1;i<=Integer.valueof(Label.Username_limit);i++){
                                newUserName = contact.Email.split('@')[0]+'_'+i+Label.Portal_UserDomain.toLowerCase();
                                if(mapUserNames.containsKey(newUserName) == false){
                                    break;
                                }
                            }
                        }
                        system.debug('newUserName after check==>'+newUserName);
                        user newPortalUser = new user();
                        if(contact.OB_Self_Registered_Contact__c)
                            newPortalUser.OB_Created_By_User__c = 'Guest User License';
                        newPortalUser.IsActive = true;  
                        newPortalUser.FirstName = contact.FirstName;
                        newPortalUser.LastName = contact.LastName;
                        newPortalUser.MobilePhone = contact.MobilePhone;
                        newPortalUser.Phone = contact.Phone; 
                        newPortalUser.ContactId = contact.Id;
                        newPortalUser.OB_Registration_Source__c = contact.Contact_Source__c;
                        newPortalUser.Email = contact.Email;
                        newPortalUser.Username = newUserName;
                        system.debug('newPortalUser.Username==>'+newPortalUser.Username);
                        newPortalUser.CommunityNickname = (contact.LastName+string.valueof(Math.random()).substring(4,9));            
                        newPortalUser.Alias = string.valueof(contact.LastName.substring(0,1) + string.valueof(Math.random()).substring(4,9));  
                        newPortalUser.TimeZoneSidKey = 'Asia/Dubai';
                        newPortalUser.LocaleSidKey = 'en_GB';
                        newPortalUser.LanguageLocaleKey = 'en_US';
                        newPortalUser.EmailEncodingKey = 'UTF-8';
                        newPortalUser.CompanyName = contact.AccountId;
                        newPortalUser.Division__c = 'Other';
                        newPortalUser.Department__c = 'Other';
                        if(contact.OB_Roles__c!=null)//v1.2
                            newPortalUser.Community_User_Role__c = contact.OB_Roles__c;
                        else
                            newPortalUser.Community_User_Role__c = 'Company Services;Property Services';
                        newPortalUser.profileId = customerCommunityProfileId;
                        
                        Database.DMLOptions dmo = new Database.DMLOptions();
                        dmo.EmailHeader.triggerUserEmail = true;
                        newPortalUser.setOptions(dmo);
                        
                        lstPortalUser.add(newPortalUser);
                        contactList.add(new Contact(id=newPortalUser.ContactId,Portal_Username__c=newPortalUser.Username)); //v1.5 - Updating the username on the contact
                        contactUserNameMap.put(newPortalUser.ContactId,newPortalUser.Username);//v1.5
                        accountIds.add(contact.AccountId);//v1.5
                    }else if(contact.Email!=null && MapUserByEmail.get(contact.Email.tolowercase())!=null){
                        newACRtoBeCreated.put(MapUserByEmail.get(contact.Email.tolowercase()),contact.AccountId);
                    }
                }
                system.debug('===lstPortalUser=='+lstPortalUser);
                if(lstPortalUser.size()>0){
                    insert lstPortalUser;
                    if(contactList.size()>0)    update contactList; //v1.5 - Updating the username on the contact
                    /* v1.5  - Updating the Username on the CP SR, when user is created to send email notification to the client */
                    if(!contactUserNameMap.isEmpty() && !accountIds.isEmpty()){
                        List<HexaBPM__Service_Request__c> SRList = new List<HexaBPM__Service_Request__c>();
                        for(HexaBPM__Service_Request__c objSR : [Select id,HexaBPM__Contact__c from HexaBPM__Service_Request__c where HexaBPM__Contact__c IN :contactUserNameMap.keyset() AND HexaBPM__Customer__c IN:accountIds AND HexaBPM__Record_Type_Name__c='Commercial_Permission' AND HexaBPM__External_Status_Name__c='Draft']){
                            SRList.add(new HexaBPM__Service_Request__c(id=objSR.Id,Portal_User_Name__c=contactUserNameMap.get(objSR.HexaBPM__Contact__c)));
                        } 
                        if(!SRList.isEmpty())   update SRList;
                    }
                }
                if(newACRtoBeCreated.size()>0){
                    CreateACR(newACRtoBeCreated);
                }
                
                system.debug('=inserted==lstPortalUser=='+lstPortalUser);
            }catch(Exception e) {
                list<Log__c> lstLogs = new list<Log__c>();
                for(string ContactId:setContactIds){
                    lstLogs.add(new Log__c(Description__c = 'Exception on ContactTriggerHelper.CreatePortalUser() - Line:119 :'+e.getMessage(),Type__c = 'Contact Trigger Code to create Portal User for Contact:'+ContactId));
                }
                insert lstLogs;
            }
        }
    }
}