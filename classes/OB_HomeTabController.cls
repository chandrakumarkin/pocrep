/**
 * Description : used to controll the vat rgistration component
 *
 * ****************************************************************************************
 * History :
 * [31.OCT.2019] Prateek Kadkol - Code Creation
 */
public without sharing class OB_HomeTabController {

@AuraEnabled
public static RespondWrap getConDetails() {

    RespondWrap respWrap = new RespondWrap();
    respWrap.ShowVATComponent = false;
    respWrap.userProfileUpdationFields ='';

    List<RecordType> updatePortalRecordTypeList = [select Id,DeveloperName,SobjectType from RecordType where DeveloperName = 'Update_Passport_Details' 
        and SobjectType ='Service_Request__c' and IsActive = true limit 1
    ];

    for(user loggedinUser :OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
        if(loggedinUser.ContactId!=null && loggedinUser.contact.AccountId!=null) {
            if(loggedinUser.contact.Account.Tax_Registration_Number_TRN__c == null) {
                if(loggedinUser.contact.Account.VAT_Info_Updated_DateTime__c == null || loggedinUser.contact.Account.VAT_Info_Updated_DateTime__c.date().daysBetween(system.now().date()) > 0) {
                    if(loggedinUser.contact.Account.Is_Commercial_Permission__c != 'Yes'){
                        respWrap.ShowVATComponent = true;
                    }
                    
                }
            }

            string missingPortalFields ='';
            if(loggedinUser.contact.Date_of_Issue__c == null)
                missingPortalFields+= ( missingPortalFields!= '') ? ',Date of Issue' : 'Date of Issue';

            
            if(loggedinUser.contact.Nationality__c == null)missingPortalFields+= ( missingPortalFields!= '') ? ',Nationality' : 'Nationality';
            if(loggedinUser.contact.Gender__c == null || loggedinUser.contact.Gender__c == '')missingPortalFields+= ( missingPortalFields!= '') ? ',Gender' : 'Gender';
            if(loggedinUser.contact.Birthdate ==null)missingPortalFields+= ( missingPortalFields!= '') ? ',Birthdate' : 'Birthdate';
            if(loggedinUser.contact.MobilePhone == null)missingPortalFields+= ( missingPortalFields!= '') ? ',Mobile Phone' : 'Mobile Phone';
            if(loggedinUser.contact.Passport_Expiry_Date__c == null)missingPortalFields+= ( missingPortalFields!= '') ? ',Passport Expiry Date' : 'Passport Expiry Date';
            if(loggedinUser.contact.Place_of_Birth__c == null)missingPortalFields+= ( missingPortalFields!= '') ? ',Place Of Birth' : 'Place Of Birth';
            if(loggedinUser.contact.Passport_No__c== null)missingPortalFields+= ( missingPortalFields!= '') ? ',Passport No' : 'Passport No';
            
            respWrap.userProfileUpdationFields =missingPortalFields;
            if(updatePortalRecordTypeList.size() >0)respWrap.userProfileSRURL ='/UpdatePassportDetails?isDetail=false&RecordType='+updatePortalRecordTypeList[0].Id;
            respWrap.isPopupAllowedToshow = (loggedinUser.Contact.RecordTypeId != null && loggedinUser.Contact.RecordType.Name =='Portal User' &&
                loggedinUser.Contact.Role__c != null && loggedinUser.Contact.Role__c.split(';').indexOf('Company Services') != -1) ? 'true' :'false';
            
            respWrap.loggedInUser = loggedinUser;
        }   
    }            
    return respWrap;
}
@AuraEnabled
public static RespondWrap updateAccountVatRegistrationNumber(string regestrationNumber) {

    RespondWrap respWrap = new RespondWrap();
    try {
        string accountId = '';
        for(user loggedinUser :OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
            if(loggedinUser.ContactId!=null && loggedinUser.contact.AccountId!=null) {
                accountId = loggedinUser.contact.AccountId;
            }


        }

        if(accountId != null || accountId != '') {
            account accObj = new account(id = accountId);
            accObj.Tax_Registration_Number_TRN__c = regestrationNumber;

            update accObj;
            respWrap.loggedInContactAccount = accObj;

        } else {
            respWrap.errorMessage = 'account id is null';
        }
    } catch(exception e) {
        respWrap.errorMessage = e.getMessage();
    }

    return respWrap;
}



@AuraEnabled
public static RespondWrap updateLoggedinContact(user loggedInUser) {

    RespondWrap respWrap = new RespondWrap();

    respWrap.errorMessage = loggedInUser.contact.AccountId;

    /* if(loggedInUser.Contact.AccountId != null) {
       account accObj = new account(id = loggedInUser.contact.AccountId);
       accObj.VAT_Info_Updated_DateTime__c = system.now();

       try {
       update accObj;
       respWrap.loggedInContactAccount = accObj;
       }

       catch(exception e) {
       respWrap.errorMessage = e.getMessage();
       }

       } else {
       respWrap.errorMessage = 'account id is null';
       }


       if(respWrap.errorMessage == null) {
       respWrap = getConDetails();
       } */



    return respWrap;
}

// ------------ Wrapper List ----------- //



public class RespondWrap {

@AuraEnabled public Boolean ShowVATComponent { get; set; }
@AuraEnabled public user loggedInUser { get; set; }
@AuraEnabled public account loggedInContactAccount { get; set; }
@AuraEnabled public string errorMessage { get; set; }

@AuraEnabled public string userProfileUpdationFields { get; set; }
@AuraEnabled public string userProfileSRURL { get; set; }
@AuraEnabled public string isPopupAllowedToshow{ get; set; }


public RespondWrap() {
}
}
}