@isTest
public class UpdatePendingBatchTest{


  static testmethod void Test_Executebatchmethod(){
  
     Account acc = new Account();
    acc.Name = '3 Capital Trading Limited';
    insert acc;
    
    SR_Status__c ss = new SR_Status__c();
    ss.Name = 'Awaiting Re-upload';
   // ss.CurrencyIsoCode = 'AED - UAE Dirham';
    ss.Code__c = 'AWAITING_RE_UPLOAD';
    insert ss;
         
    SR_Template__c stl = new SR_Template__c();
    stl.Name = 'Add Audited Accounts';
    stl.SR_RecordType_API_Name__c = 'Add_Audited_Accounts';
    stl.SR_Group__c = 'ROC';
    stl.Is_Letter__c = True;
    insert stl;     
         
    Status__c sc = new Status__c();
    sc.Name = 'The application is under process with GDRFA';
    sc.Code__c = 'CLOSED';
    insert sc;
    
    Currency__c c = new Currency__c();
    c.Name = 'US Dollar';
    c.End_Date__c = date.today() + 2;
    c.Start_Date__c = date.today();
    c.Exchange_Rate__c = 12;
    insert c;
  
   Service_Request__c sr = new Service_Request__c();
    sr.First_Name__c = 'Ghanshyam';
    sr.Last_Name__c = 'Yadav';
    sr.SAP_PR_No__c = '1000149742';
    sr.Customer__c = acc.Id;
    sr.Currency_Rate__c = c.Id;
    sr.Express_Service__c = False;
    sr.SR_Group__c = 'GS';
    sr.Completed_Date_Time__c = system.now().addMinutes(-6);
    sr.External_SR_Status__c = ss.Id;
    sr.SR_Template__c = stl.id;
    sr.Internal_SR_Status__c = ss.Id;
    //sr.External_SR_Status__c = sc.Id;
    insert sr;
  
  pending__c p = new Pending__c();
  p.Service_Request__c =sr.id;
  insert p;
  
   test.startTest();    
        UpdatePendingBatch scb=new UpdatePendingBatch();
       ID batchprocessid = database.executebatch(scb);
         
   test.stopTest();
  
  
  
  }


}