public class FATCA_Detail_Override_Controller{
    //public String recType{get; private set;}
    public List<FATCA__c> fatcaList{get; set;}
    public List<FATCA__c> fatcaAList{get; set;}
    public List<FATCA__c> fatcaBList{get; set;}
    public Boolean portalUser{get; set;}
    FATCA_Submission__c fsub;
    public FATCA_Detail_Override_Controller(ApexPages.StandardController controller){
        String userType = UserInfo.getUserType();
        if(userType.contains('CustomerSuccess'))
            portalUser = true;
        else
            portalUser = false;
        
         if(!Test.isRunningTest())
            controller.addFields(new List<String>{'Status__c'});
        
        fsub = (FATCA_Submission__c)controller.getRecord();
        //recType = Schema.SObjectType.FATCA_Submission__c.RecordTypeInfosById.get(fsub.RecordTypeId).Name;
        fatcaList = [select Id,Template_ID__c,Comment__c,FATCA_Submission__c,Name,First_Name__c,Last_Name__c,Entity_Name__c,US_TIN__c,Account_Holder_Type__c,Birth_Date__c,Address__c,City__c,Country_Code_Address__c,Account_Number__c,Account_Balance__c,Payment_Amount__c,Payment_Type__c,Account_Type__c,Status__c from FATCA__c where Fatca_Submission__c = :fsub.id and Status__c != 'Removed'];
        fatcaAList = new List<Fatca__c>();
        fatcaBList = new List<Fatca__c>();
        for(fatca__c f: fatcaList){
            if(f.Template_Id__c == 'FATCA-REP-A')
                fatcaAList.add(f);
            if(f.Template_Id__c == 'FATCA-REP-B')
                fatcaBList.add(f);
        }
    }
    public PageReference submit(){
        fsub.Status__c = 'Submitted';
        update fsub;
        for(FATCA__c fatca : fatcaList)
            fatca.Status__c = 'Submitted';
        update fatcaList; 
        return new PageReference('/'+fsub.id);
    }
    public PageReference resubmit(){
        
        List<FATCA__c> fatcaListToUpdate = new List<FATCA__C>();
        for(FATCA__c fatca : fatcaList){
            if(fatca.Status__c == 'Updated'){
                fatca.Status__c = 'Resubmitted';
                fatcaListToUpdate.add(fatca);
            }
            if(fatca.Status__c == 'Rejected'){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please update the rejected records'));
                return null;
            }
        }
        if(fatcaListToUpdate.size()>0){
            update fatcaListToUpdate; 
            fsub.Status__c = 'Resubmitted';
            update fsub;
        }
        return new PageReference('/'+fsub.id);
        
    }
    
    // Method added by Shabbir Ahmed - 04/June/2015 to record FATCA Audit
    public void CreateFATCAAuditRecord(){
        String userType = UserInfo.getUserType();
        if(!userType.contains('Portal')){
            string url = '';
            string ipAddress = 'Not captured';
            if(Apexpages.currentPage() != NULL){
                url = Apexpages.currentPage().getUrl();
                ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
            }
            FATCAAudit.CreateAuditRecord(fsub.Id,url,ipAddress);        
        }   
    } 
}