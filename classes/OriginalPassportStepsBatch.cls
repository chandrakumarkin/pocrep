global class OriginalPassportStepsBatch implements Database.Batchable<sObject>,Schedulable{
    
   global void execute(SchedulableContext ctx) {
        database.executeBatch(new OriginalPassportStepsBatch(),1);
   }
   
  global List<Step__c> start(Database.BatchableContext bc) {
       
        DateTime dTime = system.now().addMinutes(-60);
        if(test.isRunningTest())
                dTime = system.now().addMinutes(5);
       return [select id,step_name__c ,Parent_Step__c,Parent_Step__r.step_name__c ,Parent_Step__r.Step_Status__c,parent_step__r.createdDate,parent_step__R.owner__C,SR_Status__c from step__c where step_name__c like '%Courier%' and 
       Step_Status__c in ('Collected','Delivered') and Closed_Date_Time__c <=:dTime AND Closed_Date__c = TODAY AND Parent_Step__r.Step_Status__c ='Awaiting Review' and Parent_Step__r.Step_Name__c='Original Passport Received' order by Parent_Step__r.CreatedDate desc]; 
    }
   
   global void execute(Database.BatchableContext bc, List<Step__c> scope){
        
        List<Step__c> listToUpdate = new List<Step__c>();
        
        for(Step__c thisStep : scope ){
        
        step__c step = new Step__c();
        
            if(thisStep.Parent_Step__r.Step_Name__c =='Original Passport Received'){
               
                 
                    step.Status__c ='a1M20000001iL9Z'; //Closed
                    step.id = thisStep.Parent_Step__c;
                 
                
            }
            listToUpdate.add(step);
         }
         
        update listToUpdate;    
        
    }
    
     global void finish(Database.BatchableContext bc){
       closeCourierStepsBatch objSchedule = new closeCourierStepsBatch();
        Datetime dt = DateCalculation(system.now(), 120);
        string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();
        for(CronTrigger obj : [select Id from CronTrigger where CronJobDetailId IN (SELECT Id FROM CronJobDetail where Name = 'Original Pass recived step Closure')])
         system.abortJob(obj.Id);
         system.schedule('Original Pass recived step Closure', sCornExp, objSchedule);
    } 
    
   public static DateTime DateCalculation(DateTime dt,Integer iFrequnacy){
    Datetime temp = dt.addMinutes(iFrequnacy);
    if(temp.hour() >= 18 || test.isRunningTest()){
      temp = temp.addDays(1);
      string sDay = temp.format('EEEE');
      system.debug('sDay is : '+sDay);
      if(sDay == 'Friday'){
        temp = temp.addDays(2);
      }else if(sDay == 'Saturday'){
        temp = temp.addDays(1);
      }
      DateTime tempDate = Datetime.newInstance(temp.year(), temp.month(), temp.day(), 7, 30, 0);
      return tempDate;
    }
    return temp;
  }

     
        
  }