/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class StateManager {
    @Future(callout=false)
    webService static void Send_SMS_to_Customer(String TemplateId, String strWhatId, String strTargetObjectId) {

    }
    @Future(callout=false)
    webService static void Update_Courier_Step_Status(String stepId, String SRId) {

    }
}
