@istest
public class TestEmergencyContactPreviewPageCtrl {
	
    @testSetup
    public static void setup(){
        createEmailTemplate();
        //system.debug('###tempObj '+tempObj);
        AppConfig__c appConfig = new AppConfig__c(Name='Attachment Accepted Formats',AppValue__c='png,jpg,jpeg,pdf,xlsx,doc,docx,gif,xls');
        insert appConfig;
    }
    
    @istest
    public static void testCommunicationTrailtrigger(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman','Both','Retail');
        system.debug('### commTrailList '+commTrailList);
        List<Recepient__c> recepientList = new List<Recepient__c>();
        Test.startTest();
        insert commTrailList;
        recepientList = [SELECT Id FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];
        system.assertEquals(2,recepientList.size());
        system.debug('### recepientList '+recepientList);
        commTrailList[0].Entity_Type__c = 'Both';
        update commTrailList;
        recepientList = [SELECT Id FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];
        system.debug('### updated recepientList '+recepientList);
        Test.stopTest();
        
    }
    
    @istest
    public static void testCommunicationTrailtriggerUpdate(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman','Both','Commercial');
        system.debug('### commTrailList '+commTrailList);
        List<Recepient__c> recepientList = new List<Recepient__c>();
        Test.startTest();
        insert commTrailList;
        recepientList = [SELECT Id FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];
        //	system.assertEquals(2,recepientList.size());
        system.debug('### recepientList '+recepientList);
        commTrailList[0].Entity_Type__c = 'Both';  
        commTrailList[0].Building_Name__c = 'Burj Daman;Currency Tower 2';    
        update commTrailList;
        recepientList = [SELECT Id FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];
        system.debug('### updated recepientList '+recepientList);
        Test.stopTest();
        system.assertEquals(5, recepientList.size());
    }
    
    @istest
    public static void testgetDetails(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman;Currency Tower 2','Both','Both');
        system.debug('### commTrailList '+commTrailList);
        List<Recepient__c> recepientList = new List<Recepient__c>();
        Test.startTest();
        insert commTrailList;        
        recepientList = [SELECT Id FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];
        system.debug('### updated recepientList '+recepientList);
        EmergencyContactPreviewPageCtrl.previewWrapper previewObject = EmergencyContactPreviewPageCtrl.getDetails(commTrailList[0].Id);
        system.debug('## previewWrapper '+previewObject);
        Test.stopTest();        
        system.assert(previewObject != null);
    }
    
    @istest
    public static void testSendMessage(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman;Currency Tower 2','Both','Both');
        insert commTrailList;
        List<Recepient__c> recepientList = [SELECT Id,Email__c, Phone__c, Contact__c FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.txt',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;   
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = commTrailList[0].id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        Map<String,String> responseMap = new Map<String,String>();
        Test.startTest();
        responseMap = EmergencyContactPreviewPageCtrl.sendMessage(recepientList,commTrailList[0],'dummy@test.com;test@test.com');
		test.stoptest();      
        system.debug('### responseMap '+responseMap);
    }
      
     @istest
    public static void testSendEmailMessage(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman;Currency Tower 2','Email','Both');
        insert commTrailList;
        List<Recepient__c> recepientList = [SELECT Id,Email__c, Phone__c, Contact__c FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];
        ContentVersion contentVersion = new ContentVersion(
            Title = 'Penguins',
            PathOnClient = 'Penguins.jpg',
            VersionData = Blob.valueOf('Test Content'),
            IsMajorVersion = true
        );
        insert contentVersion;   
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = commTrailList[0].id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        Map<String,String> responseMap = new Map<String,String>();
        Test.startTest();
        responseMap = EmergencyContactPreviewPageCtrl.sendMessage(recepientList,commTrailList[0],'');
		test.stoptest();      
        system.debug('### responseMap '+responseMap);
    }
    
    @istest
    public static void testSendSMSMessage(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman;Currency Tower 2','SMS','Both');
        insert commTrailList;
        List<Recepient__c> recepientList = [SELECT Id,Email__c, Phone__c, Contact__c FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];        
        Map<String,String> responseMap = new Map<String,String>();
        AppConfig__c appConfig = new AppConfig__c(Name='SMS Configuration',AppValue__c='sms_service@v-2gpw680s05orrl1u3fka31m1ohrh1pli86r8i5brltqp5hm52n.3n-8ar3uai.cs128.apex.sandbox.salesforce.com');
        insert appConfig;
        HttpCalloutMockTest HttpCalloutMockTestObj = new HttpCalloutMockTest(200, 'Pass', 'OK:123456789', new Map<String, String>());                             
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, HttpCalloutMockTestObj);
        responseMap = EmergencyContactPreviewPageCtrl.sendMessage(recepientList,commTrailList[0],'');
		test.stoptest();      
        system.debug('### responseMap '+responseMap);
    }
    
     @istest
    public static void testSendMessageException(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman;Currency Tower 2','Both','Both');
        insert commTrailList;
        List<Recepient__c> recepientList = [SELECT Id FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];                
        Map<String,String> responseMap = new Map<String,String>();
        Test.startTest();
        responseMap = EmergencyContactPreviewPageCtrl.sendMessage(recepientList,commTrailList[0],'');
		test.stoptest();      
        system.debug('### responseMap '+responseMap);
    }
    
    @istest
    public static void testSampleCommunication(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman;Currency Tower 2','Both','Both');
        insert commTrailList;        
        Map<String,String> responseMap = new Map<String,String>();
        AppConfig__c appConfig = new AppConfig__c(Name='SMS Configuration',AppValue__c='sms_service@v-2gpw680s05orrl1u3fka31m1ohrh1pli86r8i5brltqp5hm52n.3n-8ar3uai.cs128.apex.sandbox.salesforce.com');
        insert appConfig;
        HttpCalloutMockTest HttpCalloutMockTestObj = new HttpCalloutMockTest(200, 'Pass', 'OK:123456789', new Map<String, String>()); 
        String emailId = 'dummy@test.com';
        String phone = '+097123456782';
        Test.startTest();
        // Set mock callout class 
        Test.setMock(HttpCalloutMock.class, HttpCalloutMockTestObj);
        responseMap = EmergencyContactPreviewPageCtrl.sendSampleCommunication(emailId,phone,commTrailList[0].Id);
		test.stoptest();      
        system.debug('### responseMap '+responseMap);
    }
    
    @istest
    public static void testCommunicationEmailTemplate(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman;Currency Tower 2','Email','Both');
        commTrailList[0].Email_Template__c = 'Test';
        test.startTest();
        insert commTrailList;
        test.stopTest();
    }
    
    @istest
    public static void testEmailtemplateCommunication(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman;Currency Tower 2','Email','Both');  
        commTrailList[0].Email_Template__c = 'Test';
        insert commTrailList;     
        List<Id> commTrailIdList = new List<Id>();
        commTrailIdList.add(commTrailList[0].Id);
        List<Recepient__c> recepientList = [SELECT Id,Phone__c,Email__c, Contact__c FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];
        Map<String,String> responseMap = new Map<String,String>();           
        
        //system.debug('### email temp comm '+tempObj);        
        Test.startTest();
        EmailTemplate tempObj = [SELECT Id FROM EmailTemplate WHERE Name = 'Test'];
        system.debug('### tempObj'+tempObj);
        createAttachments(tempObj.Id,commTrailIdList); 
        List<Attachment> attachmentList = [SELECT Id, Name, Body, ContentType, BodyLength FROM Attachment WHERE ParentId = :tempObj.Id ];
        system.debug('### attachment in test class '+attachmentList);
        responseMap = EmergencyContactPreviewPageCtrl.sendMessage(recepientList,commTrailList[0],'');
        Test.stopTest();
        
    }
    
    @istest
    public static void testSampleEmailTemplateCommunication(){
        List<CommunicationTrail__c> commTrailList = createCommunicationTrails(1,'Burj Daman;Currency Tower 2','Email','Both');  
        commTrailList[0].Email_Template__c = 'Test';
        insert commTrailList;     
        List<Id> commTrailIdList = new List<Id>();
        commTrailIdList.add(commTrailList[0].Id);
        List<Recepient__c> recepientList = [SELECT Id,Phone__c,Email__c, Contact__c FROM Recepient__c WHERE Communication_Trail__c = :commTrailList[0].Id ];
        Map<String,String> responseMap = new Map<String,String>();           
        
        //system.debug('### email temp comm '+tempObj);        
        Test.startTest();
        EmailTemplate tempObj = [SELECT Id FROM EmailTemplate WHERE Name = 'Test'];
        system.debug('### tempObj'+tempObj);
        createAttachments(tempObj.Id,commTrailIdList);    
		String emailId = 'test@abc.com';		        
        responseMap = EmergencyContactPreviewPageCtrl.sendSampleCommunication(emailId,'',commTrailList[0].Id);
        Test.stopTest();
        
    }
    
    public static List<CommunicationTrail__c> createCommunicationTrails (Integer n, String BuildingName, String channel, String entityType){
        List<Account> accountList = new List<Account>();
        for(Integer i=0; i < 3; i++){
            Account acc = new Account();
            acc.Name = 'Test'+i;
            acc.Building_Name__c = 'Currency Tower 2';
            acc.Company_Type__c = 'Financial - Related';
            acc.ROC_Status__c = 'Active';
            accountList.add(acc);
        }
        for(Integer i=3; i<5; i++){
            Account acc = new Account();
            acc.Name = 'Test'+i;
            acc.Building_Name__c = 'Burj Daman';
            acc.Company_Type__c = 'Retail';
            acc.ROC_Status__c = 'Active';
            accountList.add(acc);
        }
        insert accountList;
        List<Contact> conList = new List<Contact>();
        Integer i=0;
        for(Account acc : accountList){
            Contact con = new Contact();
            con.FirstName = 'Test';
            con.LastName = 'Dummy '+acc.Name;
            con.Email = 'dummytest'+i+'@test.com';
            con.Phone = '+971123456789';
			con.AccountId = acc.Id;   
            conList.add(con);
            i++;
        }
        insert conList;
        List<Relationship__c> relationshipList = new List<Relationship__c>();
        Integer j = 0;
        for(Account acc : accountList){
            Relationship__c relationObj = new Relationship__c();
            relationObj.Subject_Account__c = acc.Id;
            relationObj.Object_Contact__c = conList[j].Id;
            relationObj.Relationship_Type__c = 'Emergency Contact';
            relationObj.Active__c = true;
            relationshipList.add(relationObj);
            j++;
        }
        insert relationshipList;
        List<CommunicationTrail__c> commTrailList = new List<CommunicationTrail__c>();
        for(Integer k=0; k < n; k++){
            CommunicationTrail__c commTrail = new CommunicationTrail__c();
            commTrail.Building_Name__c = BuildingName;
            commTrail.Entity_Type__c = entityType;
            commTrail.Channel__c = channel;
            commTrail.Subject__c = 'Test'+i;
            commTrail.Body__c = 'Emergency Update';
            commTrail.SMS_Body__c = 'Emergency Update';
            commTrailList.add(commTrail);
        }
        return commTrailList;
    }
     
    @future
    public static void createEmailTemplate(){
        EmailTemplate validEmailTemplate = new EmailTemplate();
		validEmailTemplate.isActive = true;
		validEmailTemplate.Name = 'Test';
		validEmailTemplate.DeveloperName = 'Test';
		validEmailTemplate.TemplateType = 'text';
		validEmailTemplate.FolderId = UserInfo.getUserId();
		insert validEmailTemplate;
        //return validEmailTemplate;
    }
    
    @future
    public static void createAttachments(String parentId, List<Id> commTrailList){
        Attachment formAttach = new Attachment();
        formAttach.Name = 'Unit Test Attachment.pdf';
        formAttach.body = Blob.valueOf('Unit Test Attachment Body');
        formAttach.ContentType='application/pdf';
        formAttach.parentId =parentId;
        insert formAttach; 
        system.debug('### created attachment '+formAttach);
        /*List<CommunicationTrail__c> commTrailList1 = new List<CommunicationTrail__c>();
        for(Id strId : commTrailList){
            CommunicationTrail__c commTrail = new CommunicationTrail__c();
            commTrail.Id = strId;
            commTrail.Email_Template__c = 'Test';
            commTrailList1.add(commTrail);
        }
        update commTrailList1;*/
    }
    
    
}