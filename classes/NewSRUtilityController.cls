/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 05-31-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   05-27-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public without sharing class NewSRUtilityController {
  
    @AuraEnabled
    public static boolean isSRTemplateAvailable(string recType)  {

        List<HexaBPM__SR_Template__c> srTemplateList =[
            SELECT id from HexaBPM__SR_Template__c WHERE HexaBPM__SR_RecordType_API_Name__c =:recType AND HexaBPM__Active__c = true limit 1
        ];
        string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(recType).getRecordTypeId();
        return (srTemplateList.size() >0 && recordId != null) ? true: false;
    }
    
    @AuraEnabled
    public static menuWrapperResponse createHexaBPMSRs(string recType)  {

        menuWrapperResponse respWrap =  new menuWrapperResponse();
        respWrap.isSRMenuAvailable = NewSRUtilityController.isSRTemplateAvailable(recType);
        if(respWrap.isSRMenuAvailable){
            string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(recType).getRecordTypeId();
            string srTemplateId = '';
            string accountID = '';
            User us = [select Id,contactId from User where Id=:userinfo.getUserId()];
            Contact cont = [SELECT Id, Name, Account.Name,Accountid FROM Contact WHERE Id= :us.contactId ];
            
            contact conObj = new contact();
            account AcctObj = new account();
            
            for(contact relCont : [SELECT id,AccountId,firstname,lastname , email, phone,Passport_No__c,Nationality__c,Birthdate,Gender__c,Place_of_Birth__c,
                Date_of_Issue__c,Passport_Expiry_Date__c,Place_of_Issue__c,Issued_Country__c,Country_of_Birth__c,OB_Passport_Document_Id__c,Account.Legal_Type_of_Entity__c,Account.Authorized_Signatory_Combinations_Arabic__c,
                Account.Authorized_Signatory_Combinations__c,Account.PO_Box__c,Account.Building_Name__c,Account.Name,Account.Trade_Name__c,Account.Arabic_Name__c,
                Account.OB_Type_of_commercial_permission__c,
                Account.Phone,
                Account.Estimated_FTE__c,
                Account.Place_of_Registration__c,
                Account.ROC_reg_incorp_Date__c,
                Account.Country__c,
                Account.City__c,
                Account.Emirate_State__c,
                Account.Postal_Code__c,
                Account.Hosting_Company__c,
                Account.ParentId
                from contact where id =: cont.Id]){
                conObj = relCont;
                accountID = relCont.AccountId;
            }

            if(conObj.AccountId != null){
                for( Account accObj :[select Id  ,fine_not_paid__c,Name,
                    (select id,Name,SR_Number__c from HexaBPM__Service_Requests__r WHERE HexaBPM__Record_Type_Name__c =:recType
                        AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__Is_Rejected__c = false 
                        AND HexaBPM__IsCancelled__c = false LIMIT 1
                    )
                    from account where id =: conObj.AccountId ]){
                    AcctObj = accObj;
                }
            }
            system.debug('=======accObj==============='+AcctObj);
            system.debug('=======AcctObj.HexaBPM__Service_Requests__r==============='+AcctObj.HexaBPM__Service_Requests__r);
            
            string checkCustomValidations = NewSRUtilityController.checkCustomValidations(AcctObj,recType);

            if(AcctObj.HexaBPM__Service_Requests__r != null && AcctObj.HexaBPM__Service_Requests__r.size() > 0){
                respWrap.errorMessage = 'There is a similar request ('+AcctObj.HexaBPM__Service_Requests__r[0].SR_Number__c+') raised through the portal. Please check under Status.';
            }else if(checkCustomValidations != ''){
                respWrap.errorMessage = checkCustomValidations;
            }else{  
                
                for(HexaBPM__SR_Template__c srTempObj : [SELECT id from HexaBPM__SR_Template__c WHERE 
                    HexaBPM__SR_RecordType_API_Name__c =:recType  AND HexaBPM__Active__c = true]){
                    srTemplateId = srTempObj.Id;
                }
        
                if( srTemplateId!= null && srTemplateId!= '' && recordId !=null){
        
                    HexaBPM__Service_Request__c srObj = new HexaBPM__Service_Request__c();
                    srObj.recordTypeId = recordId;
                    srObj.HexaBPM__SR_Template__c = srTemplateId;
                    srObj.HexaBPM__Customer__c = conObj.accountID;
                    srObj.HexaBPM__Contact__c = conObj.ID;
                    srObj.first_name__c = conObj.firstname; 
                    srObj.last_name__c = conObj.lastname; 
                    srObj.hexabpm__email__c = conObj.email;
                    srObj.hexabpm__send_sms_to_mobile__c = conObj.phone;  
                    srObj.Passport_No__c = conObj.Passport_No__c ;
                    srObj.Nationality__c = conObj.Nationality__c;
                    srObj.Date_of_Birth__c = conObj.Birthdate;
                    srObj.Gender__c = conObj.Gender__c;
                    srObj.Place_of_Birth__c = conObj.Place_of_Birth__c;
                    srObj.Date_of_Issue__c = conObj.Date_of_Issue__c;
                    srObj.Date_of_Expiry__c = conObj.Passport_Expiry_Date__c;
                    srObj.Place_of_Issuance__c = conObj.Place_of_Issue__c;
        
                    srObj.Arabic_Entity_Name__c = conObj.Account.Arabic_Name__c;
                    srObj.Po_Box_Postal_Code__c = conObj.Account.PO_Box__c;
                    srObj.Type_of_entity__c = conObj.Account.Legal_Type_of_Entity__c;
                    //srObj.Step_Notes__c=mode;
                    if(recType == GlobalConstants.ANNUAL_ASSESSMENT_REC_TYPE){
                        srObj.Annual_Assessment_Year__c = String.valueOf(System.Today().year());
                    }
                    if(recType == GlobalConstants.SUSPENSE_OF_LICENSE_REC_TYPE){
                        srObj.Entity_Name__c = AcctObj.Name;
                    }

                    try{
                    
                        insert srObj;
                        system.debug('=====srObj============='+srObj);
                        for(HexaBPM__Page_Flow__c pfFlow : [SELECT id , (select id from HexaBPM__Pages__r WHERE HexaBPM__Page_Order__c = 1 LIMIT 1) from HexaBPM__Page_Flow__c WHERE HexaBPM__Record_Type_API_Name__c =:recType  LIMIT 1]){
                            respWrap.pageFlowURl = '?flowId='+pfFlow.Id+'&pageId='+pfFlow.HexaBPM__Pages__r[0].Id +'&srId='+srObj.id;
                        }
                        System.debug('respWrap*****: '+respWrap);
                    }catch(DMLException e) {
                    
                        string DMLError = e.getdmlMessage(0) + '';
                        if(DMLError == null) {
                            DMLError = e.getMessage() + '';
                        }
                        respWrap.errorMessage = DMLError; 
                    }
                }
            }
        }else{
            respWrap.errorMessage = 'Kindly Contact Your System Administrator';
        }
        return respWrap;
    }

    public class menuWrapperResponse {
    
        @AuraEnabled public string errorMessage;
        @AuraEnabled public string pageFlowURl;
        @AuraEnabled public map<string, boolean> lockingMap = new map<string, boolean>();
        @AuraEnabled public object debugger;
        @AuraEnabled public string communityPage = 'ob-processflow';
        @AuraEnabled public boolean isSRMenuAvailable;
    }

    public static string checkCustomValidations(Account AcctObj,string recordType){
        string statusOfCheck ='';
        if(recordType !=''){
            if(recordType == GlobalConstants.SUSPENSE_OF_LICENSE_REC_TYPE){
                if(string.isNotBlank(AcctObj.fine_not_paid__c)){
                    statusOfCheck = 'You are not allowed to raise this SR. '+AcctObj.fine_not_paid__c + ' fine exist for the customer.';
                }else{

                    List<Service_Request__c> issuedFineSRs =
                        [select id,External_Status_Name__c,Name from Service_Request__c 
                            where Record_Type_Name__c ='Issue_Fine' and 
                            Customer__c =:AcctObj.Id and External_Status_Name__c = 'Issued'
                        ];
                    if(issuedFineSRs.size() >0){
                        statusOfCheck = 'You are not allowed to raise this SR. There is a fine request ('+issuedFineSRs[0].Name+') issued. Please check under Request in progress on the portal home page';
                    }
                }  
            }
        }  
        return statusOfCheck;
    }

    Public static string SendEmailNotifications(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp,
        string queueName,list<string> dualFiles,string emailTemp){
		string strResult ='success';
		system.debug('---stp-------------'+stp);
        system.debug('---stp-HexaBPM__SR__c------------'+stp.HexaBPM__SR__c);
        system.debug('---stp.HexaBPM__SR__r.HexaBPM__Email__c-------------'+stp.HexaBPM__SR__r.HexaBPM__Email__c);
        if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Email__c!=null){
             
            Contact tempContact = new Contact(email = stp.HexaBPM__SR__r.HexaBPM__Email__c, firstName = 'test', lastName = 'email',accountID=stp.HexaBPM__SR__r.HexaBPM__Customer__c );
            insert tempContact;
            String[] ToAddresses = new List<String>(); 
            list<Messaging.SingleEmailMessage> lstCPApprovalEmail = new list<Messaging.SingleEmailMessage>();
            if(queueName != ''){
                
            }else{
                ToAddresses = new String[]{stp.HexaBPM__SR__r.HexaBPM__Email__c};
                //String[] ccList = new String[]{stp.HexaBPM__SR__r.Account_Owner_Email__c};
                //system.debug('ccList' + ccList);
            }
            OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(tempContact.Id);
            mail.setToAddresses(ToAddresses);
           // mail.setCCAddresses( ccList);
            mail.setSaveAsActivity(false);

            if(lstOrgEA!=null && lstOrgEA.size()>0)
            mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
            mail.setWhatId(stp.HexaBPM__SR__c);

            for(EmailTemplate temp:[Select Id from EmailTemplate where DeveloperName=:emailTemp]){
                mail.setTemplateId(temp.Id);
                system.debug('$$$$$$$Template' + temp.Id);
            }
            
            list<Messaging.EmailFileAttachment> lstEmailAttachments = new list<Messaging.EmailFileAttachment>();
            string AttachmentId;
            list<string> AttachmentIdList = new list<string>();
            string SRDocId;
            for(HexaBPM__SR_Doc__c SRDoc:[Select Id,name,HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:stp.HexaBPM__SR__c and HexaBPM__Document_Master__r.HexaBPM__Code__c IN:dualFiles ]){
                if(SRDoc.HexaBPM__Doc_ID__c != null){
                    AttachmentIdList.add(SRDoc.HexaBPM__Doc_ID__c);
                }
            }
            system.debug('$$$$AttachList' + AttachmentIdList);
            if(AttachmentIdList.size() > 0){
               
                for(ContentVersion cv : [Select Id,PathOnClient,VersionData,Title,Application_Document__c from ContentVersion 
                                            where ContentDocumentId IN : AttachmentIdList order by CreatedDate desc]){
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName(cv.PathOnClient);
                    efa.setBody(cv.VersionData);
                    lstEmailAttachments.add(efa);
                }
                system.debug('$$$$AttachListCv' + lstEmailAttachments);
                if(lstEmailAttachments.size()>0){
                    mail.setFileAttachments(lstEmailAttachments);
                }
                    
            }
            lstCPApprovalEmail.add(mail);
            system.debug('lstCPApprovalEmail==>'+lstCPApprovalEmail);
            try{
                Messaging.SendEmailResult[] results = Messaging.sendEmail(lstCPApprovalEmail);

                if (results[0].success) {
                System.debug('The email was sent successfully.');
                } else {
                System.debug('The email failed to send: ' +  results[0].errors[0].message);
                }
                delete tempContact;
            }catch(Exception e){
                strResult = e.getMessage()+'';
            }
        }
        return strResult;
	}
    
}