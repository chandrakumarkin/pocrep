/*
    Created By  : RB Nagaboina
    Description : 
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 V1.1	09/11/2016		Ravi			Added the logic to create operating locations from lease as per #2516
 V1.2   20/08/2017      Sravan 			Added a Trigger handler for SR_Doc__c as per tkt # 4092
 ----------------------------------------------------------------------------------------
*/
public with sharing class TriggerFactoryCls {
	/**
	 * Public static method to create and execute a trigger handler
	 *
	 * Arguments:	Schema.sObjectType soType - Object type to process (SObject.sObjectType)
	 *
	 * Throws a TriggerException if no handler has been coded.
	 */
	public static void createHandler(Schema.sObjectType soType)
	{
		// Get a handler appropriate to the object being processed
		TriggerFactoryInterface handler = getHandler(soType);
		
		// Make sure we have a handler registered, new handlers must be registered in the getHandler method.
		if (handler == null)
		{
			throw new CommonCustomException('No Trigger Handler registered for Object Type: ' + soType);
		}
		
		// Execute the handler to fulfil the trigger
		try{
			execute(handler);
		}catch(Exception ex){
			throw new CommonCustomException(ex.getMessage());
		}
	}
	
	/**
	 * private static method to control the execution of the handler
	 *
	 * Arguments:	ITrigger handler - A Trigger Handler to execute
	 */	
	private static void execute(TriggerFactoryInterface handler)
	{
		
		if(trigger.isBefore){
			if(trigger.isInsert)
				handler.executeBeforeInsertTrigger(trigger.new);
			
			if(trigger.isUpdate)
				handler.executeBeforeUpdateTrigger(trigger.new,trigger.oldMap);
			
			if(trigger.isInsert || trigger.isUpdate)
				handler.executeBeforeInsertUpdateTrigger(trigger.new, trigger.oldMap);
		}else{
			if(trigger.isInsert)
				handler.executeAfterInsertTrigger(trigger.new);
			
			if(trigger.isUpdate)
				handler.executeAfterUpdateTrigger(trigger.new,trigger.oldMap);
			
			if(trigger.isInsert || trigger.isUpdate)
				handler.executeAfterInsertUpdateTrigger(trigger.new, trigger.oldMap);
		}
		
		/*// Before Trigger
		if (Trigger.isBefore)
		{
			// Call the bulk before to handle any caching of data and enable bulkification
			handler.bulkBefore();
			
			// Iterate through the records to be deleted passing them to the handler.
			if (Trigger.isDelete)
			{
				for (SObject so : Trigger.old)
				{
					handler.beforeDelete(so);
				}
			}
			// Iterate through the records to be inserted passing them to the handler.
			else if (Trigger.isInsert)
			{
				for (SObject so : Trigger.new)
				{
					handler.beforeInsert(so);
				}
			}
			// Iterate through the records to be updated passing them to the handler.
			else if (Trigger.isUpdate)
			{
				for (SObject so : Trigger.old)
				{
					handler.beforeUpdate(so, Trigger.newMap.get(so.Id));
				}
			}
		}
		else
		{
			// Call the bulk after to handle any caching of data and enable bulkification
			handler.bulkAfter();
			
			// Iterate through the records deleted passing them to the handler.
			if (Trigger.isDelete)
			{
				for (SObject so : Trigger.old)
				{
					handler.afterDelete(so);
				}
			}
			// Iterate through the records inserted passing them to the handler.
			else if (Trigger.isInsert)
			{
				for (SObject so : Trigger.new)
				{
					handler.afterInsert(so);
				}
			}
			// Iterate through the records updated passing them to the handler.
			else if (Trigger.isUpdate)
			{
				for (SObject so : Trigger.old)
				{
					handler.afterUpdate(so, Trigger.newMap.get(so.Id));
				}
			}
		}
		
		// Perform any post processing
		handler.andFinally();
		*/
	}
	
	/**
	 * private static method to get the appropriate handler for the object type.
	 * Modify this method to add any additional handlers.
	 *
	 * Arguments:	Schema.sObjectType soType - Object type tolocate (SObject.sObjectType)
	 *
	 * Returns:		ITrigger - A trigger handler if one exists or null.
	 */
	private static TriggerFactoryInterface getHandler(Schema.sObjectType soType)
	{
		if (soType == Relationship__c.sObjectType)
		{
			return new RelationshipTrgHandler();
		}
		
		if (soType == Step__c.sObjectType)
		{
			return new StepTrgHandler();
		}
		
		if(soType == Lease__c.sObjectType){ //v1.1
			return new LeaseTriggerHandler();
		}
		
		if(soType == Lease_Partner__c.sObjectType){//v1.1
			return new LeasePartnerTriggerHandler();
		}
		
		if(soType == SR_Doc__c.sObjectType){//v1.2
			return new SRDocTriggerHandler();
		}
		
		return null;
	}
}