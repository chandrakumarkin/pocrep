/*
* 
Description: Create UBO and Exempt Entity record on UBO object: 
1) If the UBO or exempt entity do not have another role, then the record can be deleted.
2) If the UBO or exempt entity has another role then only some information should be blanked out. The information that should be removed is:
1. UBO value should be removed from role
2. Exempt Entity should be removed from role
3. Clear out the values from the following fields:
Please select the type of ownership or control of the individual identified or deemed as UBO
Is it direct or indirect:
Reason for exemption under the DIFC Ultimate Beneficial Owners Regulations:
Name of the Government entity
Decree number establishing the entity under UAE Law
Please select the name of the Financial Services Regulator
If others, please specify the name of the recognised financial services regulator
Please select the name of the recognised exchange
If other, please provide the name of the exchange
Please select the name of the recognised Jurisdiction:
**/
global without sharing class CC_UBODataCreation implements HexaBPM.iCustomCodeExecutable {
  
 
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        system.debug('=====CC_UBODataCreation=======');
        string strResult = 'Success';
        HexaBPM__Step__c step = new HexaBPM__Step__c(Id=stp.Id);
        HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__c);
        system.debug('====linked data==='+stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c);
            
        List<Id> srIDList = new List<Id>();
        srIDList.add(objSR.Id);
        
        //Sabeeha - DIFC2-17401 - updating the UBO count on Account
        Account accRec = new Account();
        if(String.IsNotBlank(stp.HexaBPM__SR__r.HexaBPM__Customer__c)){
            accRec = [Select id, Company_UBOs__c, Count_Body_Corporation_UBO__c, Exempted_from_UBO_Regulations__c from Account where id =: stp.HexaBPM__SR__r.HexaBPM__Customer__c];
        }
        List<HexaBPM_Amendment__c> exemptUboRecordList = new List<HexaBPM_Amendment__c>();
        //ends here        
        
        if(String.IsNotBlank(stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c))
            srIDList.add(stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c);
        system.debug('=====CC_UBODataCreation======='+objSR.Id);
        Integer count = 0;
        if(String.IsNotBlank(objSR.Id)){
            List<UBO_Secure__c> uboRecordList = new List<UBO_Secure__c>();
            List<HexaBPM_Amendment__c> uboRecordDeleteList = new List<HexaBPM_Amendment__c>();
            List<HexaBPM_Amendment__c> uboRecordUpdateList = new List<HexaBPM_Amendment__c>();
            
            List<HexaBPM_Amendment__c> amendmentList = [Select Id,ServiceRequest__c,recordtype.developerName,Role__c,
                                        ServiceRequest__r.HexaBPM__Customer__c,ServiceRequest__r.HexaBPM__Customer__r.Name,
                                        Type_of_Ownership_or_Control__c,Please_select_if_applicable__c,
                                        Date_Of_Becoming_a_UBO__c,Title__c,First_Name__c,Middle_Name__c,
                                        Last_Name__c,Former_Name__c,Passport_No__c,Nationality_list__c,
                                        Date_of_Birth__c,Gender__c,Place_of_Birth__c,Passport_Issue_Date__c,
                                        Passport_Expiry_Date__c,Place_of_Issue__c,Mobile__c,Email__c,Is_this_individual_a_PEP__c,
                                        Apartment_or_Villa_Number_c__c,Address__c,Permanent_Native_Country__c,
                                        Permanent_Native_City__c,Emirate_State_Province__c,PO_Box__c,
                                        Registration_Date__c,Country_of_Registration__c,Reason_for_exemption_under_DIFC_UBO__c,
                                        Telephone_No__c
                                        From HexaBPM_Amendment__c
                                        Where Role__c Includes ('UBO', 'Exempt Entity')
                                        AND ServiceRequest__c IN: srIDList  
                                        AND (recordtype.developerName =: OB_ConstantUtility.AMENDMENT_INDIVIDUAL_RT.developerName OR 
                                        recordtype.developerName =: OB_ConstantUtility.AMENDMENT_CORPORATE_RT.developerName)
                                        Limit :(Limits.getLimitQueryRows() - Limits.getQueryRows()) ];
            if(amendmentList != null && amendmentList.size()>0){
                for(HexaBPM_Amendment__c itr : amendmentList){
                    if(itr.Role__c.containsIgnorecase('UBO') && itr.recordtype.developerName == OB_ConstantUtility.AMENDMENT_INDIVIDUAL_RT.developerName){
                        if(itr.ServiceRequest__c == objSR.Id){
                            UBO_Secure__c newRecord = new UBO_Secure__c(Service_Request__c = itr.ServiceRequest__c,OB_Amendment__c=itr.Id);
                            
                            uboRecordList.add(mapUBOField(newRecord,itr));
                        }
                        if(String.IsNotBlank(itr.Role__c)  && itr.Role__c.equalsIgnorecase('UBO')){
                            String role = itr.Role__c;
                            itr.Role__c = role.replace('UBO', '');
                            itr.Type_of_Ownership_or_Control__c = '';
                            itr.Please_select_if_applicable__c = '';
                            uboRecordUpdateList.add(itr);
                            //uboRecordDeleteList.add(itr);
                        }else{
                            String role = itr.Role__c;
                            if(itr.Role__c.containsIgnorecase('UBO')) {
                                itr.Role__c = role.replace('UBO', '');
                                itr.Type_of_Ownership_or_Control__c = '';
                                itr.Please_select_if_applicable__c = '';
                                uboRecordUpdateList.add(itr);
                            }
                        }
                    }
                    else if(itr.Role__c.containsIgnorecase('Exempt Entity') && itr.recordtype.developerName == OB_ConstantUtility.AMENDMENT_CORPORATE_RT.developerName){
                        if(itr.ServiceRequest__c == objSR.Id){
                            count = count+1;
                            exemptUboRecordList.add(itr);
                        }
                    }
                }
            }
        
            system.debug('=====CC_UBODataCreation====uboRecordList==='+uboRecordList);
            
            //Sabeeha - DIFC2-17401 - updating the UBO count on Account
            if(accRec != null){
                if(uboRecordList != null && uboRecordList.size() != 0){
                    accRec.Company_UBOs__c = uboRecordList.size();
                }
                if(exemptUboRecordList != null && exemptUboRecordList.size()>0){
                    accRec.Count_Body_Corporation_UBO__c = exemptUboRecordList.size();
                }
                if(count != 0 && count == amendmentList.size()){
                    accRec.Exempted_from_UBO_Regulations__c = true;
                }
            }//ends here
                        
                try{
                    if(uboRecordList != null && uboRecordList.size() != 0){
                        insert uboRecordList;
                    }
                    if(uboRecordUpdateList != null && uboRecordUpdateList.size() != 0){
                        update uboRecordUpdateList;
                    }
                    // delet UBO record
                    if(uboRecordDeleteList != null && uboRecordDeleteList.size() != 0){
                        delete uboRecordDeleteList;
                    }
                    //Sabeeha - DIFC2-17401 - updating the UBO count on Account
                    if(accRec != null){
                        update accRec;
                    }//ends here
                }catch(exception e){
                    strResult = e.getMessage();
                    insert LogDetails.CreateLog(null, 'CC_UBODataCreation : EvaluateCustomCode', 'Line Number : '+e.getLineNumber()+'\nException is : '+strResult);  
        
                }
            
        
        }
        system.debug('====strResult====='+strResult);
        return strResult;
    }
    
    /**
    * UBO Data Object and OB Amendment Object Field Mapping
    *
    **/
    public static UBO_Secure__c mapUBOField(UBO_Secure__c UBOSecure,HexaBPM_Amendment__c UBOData){
             
        //Reference fields
        UBOSecure.OB_Amendment__c = UBOData.Id;
        
        //Individual Fields
        UBOSecure.Date_of_Birth__c = EncryptExtension.generateEncryptValue(string.valueof(UBOData.Date_of_Birth__c));
        UBOSecure.Gender__c = EncryptExtension.generateEncryptValue(UBOData.Gender__c);
        UBOSecure.PassportExpiryDate__c = EncryptExtension.generateEncryptValue(string.valueof(UBOData.Passport_Expiry_Date__c));
        UBOSecure.Place_of_Birth__c = EncryptExtension.generateEncryptValue(UBOData.Place_of_Birth__c);
        UBOSecure.Title_new__c = EncryptExtension.generateEncryptValue(UBOData.Title__c);
        UBOSecure.DateofBecomingUBO__c = EncryptExtension.generateEncryptValue(string.valueof(UBOData.Date_Of_Becoming_a_UBO__c));
        UBOSecure.PassportDateofIssuance__c = EncryptExtension.generateEncryptValue(string.valueof(UBOData.Passport_Issue_Date__c));
        
        
        /*if(!string.isBlank(UBOData.First_Name_New__c))
        {
            UBOSecure.GivenName__c = EncryptExtension.generateEncryptValue(UBOData.First_Name__c);
            UBOSecure.Old_Given_Name__c= EncryptExtension.generateEncryptValue(UBOData.First_Name__c);
        }
        else
        {*/
            UBOSecure.GivenName__c= EncryptExtension.generateEncryptValue(UBOData.First_Name__c);
            
       // }
        
        
        /*if(!string.isBlank(UBOData.Last_Name_New__c))
        {
            UBOSecure.FamilyName__c = EncryptExtension.generateEncryptValue(UBOData.Last_Name_New__c);
            UBOSecure.Old_Family_Name__c= EncryptExtension.generateEncryptValue(UBOData.Last_Name__c);
        }
        else
        {*/
            UBOSecure.FamilyName__c= EncryptExtension.generateEncryptValue(UBOData.Last_Name__c);
        //}
        
        /*if(!string.isBlank(UBOData.Passport_Number_New__c))
        {
            UBOSecure.PassportNo__c = EncryptExtension.generateEncryptValue(UBOData.Passport_Number_New__c);
            UBOSecure.Old_PassportNo__c= EncryptExtension.generateEncryptValue(UBOData.Passport_Number__c);
        }
        else
        {*/
            UBOSecure.PassportNo__c = EncryptExtension.generateEncryptValue(UBOData.Passport_No__c);
        //}
        
        
        /*if(!string.isBlank(UBOData.Nationality_New__c))
        {
            
            UBOSecure.Nationality__c= EncryptExtension.generateEncryptValue(UBOData.Nationality_New__c);
            UBOSecure.Old_Nationality__c= EncryptExtension.generateEncryptValue(UBOData.Nationality__c);
            
        }
        else
        {*/
            UBOSecure.Nationality__c = EncryptExtension.generateEncryptValue(UBOData.Nationality_list__c);
            
        //}
       /* if(!string.isBlank(UBOData.Email_New__c))
        {
            UBOSecure.Person_Email__c = EncryptExtension.generateEncryptValue(UBOData.Email_New__c);
            UBOSecure.Old_Person_Email__c= EncryptExtension.generateEncryptValue(UBOData.Email__c);
        }
        else
        {*/
                UBOSecure.Person_Email__c = EncryptExtension.generateEncryptValue(UBOData.Email__c);
            //}
            //UBOSecure.Date_of_Cessation__c= EncryptExtension.generateEncryptValue(string.valueof(UBOData.Date_of_Cessation__c));
            //UBOSecure.Company_Name__c = UBOData.Entity_Name__c;
            //UBOSecure.Registration_Date__c = EncryptExtension.generateEncryptValue(string.valueof(UBOData.Date_Of_Registration__c));
            //UBOSecure.Name_of_the_regulator__c = EncryptExtension.generateEncryptValue(UBOData.Name_of_Regulator_Exchange_Govt__c);
            //UBOSecure.Place_of_Registration__c = EncryptExtension.generateEncryptValue(UBOData.Select_Regulator_Exchange_Govt__c);
            //UBOSecure.Exempted_Entity__c = EncryptExtension.generateEncryptValue(UBOData.Reason_for_exemption__c);
            
            UBOSecure.PhoneNumber__c = EncryptExtension.generateEncryptValue(UBOData.Mobile__c);
            UBOSecure.Apt_or_Villa_No__c = EncryptExtension.generateEncryptValue(UBOdata.Apartment_or_Villa_Number_c__c);// address 2
            UBOSecure.Building_Name__c = EncryptExtension.generateEncryptValue(UBOData.Address__c);// address 1
            //UBOSecure.Street_Address__c = EncryptExtension.generateEncryptValue(UBOData.Street_Address__c);
            UBOSecure.Emirate_State__c = EncryptExtension.generateEncryptValue(UBOData.Emirate_State_Province__c);
            UBOSecure.Permanent_Native_City__c = EncryptExtension.generateEncryptValue(UBOData.Permanent_Native_City__c);
            UBOSecure.Permanent_Native_Country__c  = EncryptExtension.generateEncryptValue(UBOData.Permanent_Native_Country__c);
            UBOSecure.PO_Box__c = EncryptExtension.generateEncryptValue(UBOData.PO_Box__c);
            UBOSecure.Post_Code__c = EncryptExtension.generateEncryptValue(UBOData.PO_Box__c);
            UBOSecure.Type_of_ownership__c = EncryptExtension.generateEncryptValue(UBOData.Type_of_Ownership_or_Control__c);
            UBOSecure.UBO_Direct_Indirect__c = UBOData.Please_select_if_applicable__c;
            UBOSecure.Client_Entity_Name__c = UBOData.ServiceRequest__r.HexaBPM__Customer__r.Name;
            
            // not encrypted in existing system but doing encrption for this call
            UBOSecure.PlaceofIssuance__c = EncryptExtension.generateEncryptValue(UBOData.Place_of_Issue__c);
            UBOSecure.Middlename__c = EncryptExtension.generateEncryptValue(UBOData.Middle_Name__c);
            UBOSecure.FormerName__c = EncryptExtension.generateEncryptValue(UBOData.Former_Name__c);
            
        return UBOSecure;
    }
}