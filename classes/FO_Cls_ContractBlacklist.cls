/**
 * Batchable class for updating the contractor blacklist points
 * @author	Claude Manahan. NSI-DMCC
 * @date	26-04-2016
 */
global class FO_Cls_ContractBlacklist implements Database.Batchable<sObject> {
	
	/* 
	 * Required fields:
	 * 1. Contractor__c - to get the account
	 * 2. Violation_Date__c
	 * 3. Blacklist_Point__c
	 *
	 * Sample Query: 
	 *
	 *   SELECT Id,
	 *  		contractor__c, 
	 *  		Violation_Date__c,
	 *  		Blacklist_Point__c
	 *  		FROM Violation__c 
	 *  		Where Contractor__c != '' AND 
	 *  		Violation_Date__c = THIS_YEAR 
	 *  		ORDER BY Violation_Date__c DESC
	 *
	 */
	global String violationQuery; 
	
	/*
	 * Batch constructor
	 * @params	query	the query to get all the accounts
	 */
	global FO_Cls_ContractBlacklist(String query){
		violationQuery = query; 
    }

    global Database.QueryLocator start(Database.BatchableContext BC){
  		return Database.getQueryLocator(violationQuery);
    }

    //global void execute(Database.BatchableContext BC, List<Violation__c> scope){// Commented by: Claude Manahan. 2016-06-20. Replaced by Account Scope
    global void execute(Database.BatchableContext BC, List<Account> scope){
    	
    	Savepoint spdata = Database.setSavepoint(); // create a savepoint to rollback any changes
    	
    	Date dateYearBefore = System.today().addYears(-1);
		
		Integer daysYearBefore = dateYearBefore.daysBetween(System.Today());
    	
    	try {
    		
    		Map<Id,List<Violation__c>> annualViolationMap = new Map<Id,List<Violation__c>>(); // map to store the violations per contractor
    		
    		for(Account contractorAccount : scope){
    			
    			if(contractorAccount.Violations__r.isEmpty()){
    				/* If the contractor does not have any violations, it is reset to 0 */
    				contractorAccount.Blacklist_Point_3_months__c = 0;
    				contractorAccount.Blacklist_Point_6_months__c = 0;
    				contractorAccount.Blacklist_Point_1_year__c = 0;
    			} else {
    				
    				/* If the contractor has violations, loop through the list and compute */
    				Integer blackListPointsThreeMonths = 0;
		            Integer blackListPointsSixMonths = 0;
		            Integer blackListPointsOneYear = 0;
		            
		            for(Violation__c v : contractorAccount.Violations__r ){
		            	
		            	/*if(v.Violation_Date__c.monthsBetween(System.Today()) <= 3){
		            		blackListPointsThreeMonths += Integer.valueOf(v.Blacklist_Point__c);
		            	}
		            	
		            	if(v.Violation_Date__c.monthsBetween(System.Today()) <= 6){
		            		blackListPointsSixMonths += Integer.valueOf(v.Blacklist_Point__c);
		            	}
		            	
		            	if(v.Violation_Date__c.monthsBetween(System.Today()) >= 6){
		            		blackListPointsOneYear += Integer.valueOf(v.Blacklist_Point__c);
		            	}*/
		            	
		            	if(v.Violation_Date__c.daysBetween(System.Today()) <= 90){
		            		blackListPointsThreeMonths += Integer.valueOf(v.Blacklist_Point__c);
		            	}
		            	
		            	if(v.Violation_Date__c.daysBetween(System.Today()) <= 180){
		            		blackListPointsSixMonths += Integer.valueOf(v.Blacklist_Point__c);
		            	}
		            	
		            	if(v.Violation_Date__c.daysBetween(System.Today()) <= daysYearBefore){
		            		blackListPointsOneYear += Integer.valueOf(v.Blacklist_Point__c);
		            	}
		            	
		            }
		            
		            contractorAccount.Blacklist_Point_3_months__c = blackListPointsThreeMonths;
	            	contractorAccount.Blacklist_Point_6_months__c = blackListPointsSixMonths;
	            	contractorAccount.Blacklist_Point_1_year__c = blackListPointsOneYear == 0 ? blackListPointsSixMonths : blackListPointsOneYear; 
    			}
    		}
    		
    		update scope;
    		
    	} catch (Exception e){
    		System.debug('Error occured for class FO_Cls_ContractBlacklist, at line' + e.getLineNumber());
    		System.debug('Message: ' + e.getMessage());
    		insert LogDetails.CreateLog(null, 'FIT-OUT : FO_Cls_ContractBlacklist', 'Line Number : '+e.getLineNumber()+'\nException is : '+e);
    		Database.rollback(spdata);
    	}
     	
    }

    global void finish(Database.BatchableContext BC){
    	// TODO: Insert Debug options if needed
    }
}