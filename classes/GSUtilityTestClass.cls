@isTest(seeallData=false)
public class GSUtilityTestClass 
{    
    public static testMethod void testData()
    {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
         string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
        
        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c'])
        {
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
        
        Document_Details__c doc = new Document_Details__c();
        doc.Contact__c=objContact.id; 
        doc.Document_Type__c ='Employment Visa';
        doc.Document_Number__c='1111';
        insert doc;
        
        SR_Status__c objSrStatus = new SR_Status__c();
        objSrStatus.Name='test';
        objSrStatus.Code__c='Pending';
        insert objSrStatus;
        
         list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstSRStatus.add(new SR_Status__c(Name='Posted',Code__c='POSTED'));
        insert lstSRStatus;
        
        SR_Status__c srs1= [select id,name,code__c from SR_Status__c where Code__c='Pending' limit 1];
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Fine_Amount_AED__c = 2500 ;
        objSR1.RecordTypeId = mapSRRecordTypes.get('DIFC_Sponsorship_Visa_Cancellation').Id;
        objSR1.Entity_Name__c = 'Test Entity Name';
        objSR1.Contact__c=objContact.id;
        objSR1.Pre_Entity_Name_Arabic__c='abc';
        objSR1.Document_Detail__c=doc.id;
        objSR1.ApplicantId_hidden__c=objContact.id;
        //objSR1.SponsorId_hidden__c=objContact.id;
        objSR1.Docdetail_hidden__c=doc.id;
        objSR1.Quantity__c=2;
        objSR1.Nationality_list__c='India';
        objSR1.External_SR_Status__c=lstSRStatus[1].Id;
        objSR1.Visa_Expiry_Date__c=system.today()+20;
        objSR1.Residence_Visa_No__c='201/2015/1234567';
        objSR1.Statement_of_Undertaking__c = true;
        objSR1.Would_you_like_to_opt_our_free_couri__c ='No';        
        objSR1.Send_SMS_To_Mobile__c ='+971589602235';
        
        insert objSR1;
        
        List<ID> srID = new List<Id>{objSR1.Id};
        GSUtilityClass guc = new GSUtilityClass();
        GSUtilityClass.CertificateUploadedGS(srID);
        GSUtilityClass.CheckServiceRequestForFine(objSR1.Id, 2500);
        GSUtilityClass.getMaritalStatusCode('Single');
        GSUtilityClass.getMaritalStatusCode('Married');
        GSUtilityClass.getMaritalStatusCode('Widow');
        GSUtilityClass.getMaritalStatusCode('Divorced');
        GSUtilityClass.getMaritalStatusCode('Child');
    }
    
    
}