/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/Fetch_PageFlow/*')
global class RestPageFlowProcess {
    global RestPageFlowProcess() {

    }
    @HttpPost
    global static HexaBPM.RestPageFlowProcess.FlowProcessWrap GetPageFlow(String SR_RecordType_API_Name) {
        return null;
    }
    global static String getAccessibleFieldsSoql(String obj, String whereClause) {
        return null;
    }
global class FlowProcessWrap {
}
}
