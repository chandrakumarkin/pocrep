global class FATCA_Build_Xml{
    webservice static String generateXml(String generate){

        // Method added by Shabbir Ahmed - 04/June/2015 to record FATCA Audit
        FATCAAudit.CreateAuditRecord(NULL, 'Generate XML', 'Not captured'); 
        
        XmlStreamWriter writer = new XmlStreamWriter();
        writer.writeStartDocument('UTF-8', '1.0');        
        writer.writeStartElement(null, 'DOCS_HEADER', null);
        writer.writeAttribute(null,null,'HEADER_ID',System.Label.FATCA_Header_ID);
        writer.writeAttribute(null,null,'AUTH_ID','004');
        //writer.writeAttribute(null,null,'REPORTING_PERIOD',String.valueOf(Date.newinstance(System.Today().year(),12,01)).substring(0,10));
        writer.writeAttribute(null,null,'REPORTING_PERIOD',System.Label.FATCA_Reporting_Period);
        
        Date asOnDate = Date.parse(System.Label.FATCA_As_On_Date);
        List<FATCA_Submission__c> fsubList = [Select Id,Name,GIIN__c,(Select Id,Template_ID__c,Payment_Type__c,Country_Code_Address__c,City__c,Address__c,Entity_Name__c,Name,First_Name__c,Last_Name__c,Birth_Date__c,Account_Type__c,Payment_Amount__c,Account_Balance__c,US_TIN__c,GIIN__c,Account_Number__c,Account_Holder_Type__c,Status__c from FATCA__r where Status__c='Submitted') from FATCA_Submission__c where Status__c='Submitted' and As_On_Date__c>:asOnDate];
        Map<String,Country_Codes__c> countryCodesMap = Country_Codes__c.getAll(); 
        for(FATCA_Submission__c fsub : fsubList){
            if(fsub.FATCA__r.size()==0){
                writer.writeStartElement(null, 'DOCS_DETAILS', null);
                writer.writeAttribute(null,null,'HEADER_ID',System.Label.FATCA_Header_ID);
                writer.writeAttribute(null,null,'GIIN',fsub.GIIN__c);
                writer.writeAttribute(null,null,'NIL_RETURN','YES');
                writer.writeEndElement();
                            
            }
            else{
                for(FATCA__c f : fsub.FATCA__r){
                    writer.writeStartElement(null, 'DOCS_DETAILS', null);
                    writer.writeAttribute(null,null,'HEADER_ID',System.Label.FATCA_Header_ID);
                    writer.writeAttribute(null,null,'TIN',f.US_TIN__c == '0' ? '000000000' : f.US_TIN__c);
                    writer.writeAttribute(null,null,'GIIN',f.GIIN__c);
                    writer.writeAttribute(null,null,'NIL_RETURN','NO');
                    writer.writeAttribute(null,null,'TRANS_ID',f.Name);
                    writer.writeAttribute(null,null,'ACCOUNT_HOLDER_TYPE',f.Account_Holder_Type__c);
                    writer.writeAttribute(null,null,'ACCOUNT_NUMBER',f.Account_Number__c);
                    writer.writeAttribute(null,null,'ACCOUNT_BALANCE',String.valueOf(f.Account_Balance__c));
                    writer.writeAttribute(null,null,'PAYMENT_AMOUNT',String.valueOf(f.Payment_Amount__c));
                    writer.writeAttribute(null,null,'PAYMENT_TYPE',f.Payment_Type__c);
                    writer.writeAttribute(null,null,'ACCOUNT_TYPE',f.Account_Type__c);
                    writer.writeAttribute(null,null,'STATUS',System.Label.FATCA_Status_New_Data);
                    
                    if(f.Template_Id__c == 'FATCA-REP-A')
                        writer.writeEmptyElement(null, 'CLIENT_P', null);
                    else if(f.Template_Id__c == 'FATCA-REP-B')
                        writer.writeEmptyElement(null, 'CLIENT_C', null);
                    else writer.writeEmptyElement(null, 'CLIENT', null); 
                    
                    writer.writeAttribute(null,null,'TIN',f.US_TIN__c == '0' ? '000000000' : f.US_TIN__c);
                    
                    if(f.Template_Id__c == 'FATCA-REP-A'){
                        writer.writeAttribute(null,null,'FNAME',f.First_Name__c);
                        writer.writeAttribute(null,null,'LNAME',f.Last_Name__c);
                        if(f.Birth_Date__c != null)
                            writer.writeAttribute(null,null,'DOB',String.valueOf(f.Birth_Date__c).substring(0,10));
                    } else if(f.Template_Id__c == 'FATCA-REP-B')
                        writer.writeAttribute(null,null,'NAME',f.Entity_Name__c);
                    String address = f.Address__c;
                    if(address != null){
                        address= address.replace(',\r\n', ' / ');
                        address= address.replace('\r\n', ' / ');
                        address= address.replace(',', ' / ');
                    }
                    writer.writeAttribute(null,null,'ADDRESS',address);
                    writer.writeAttribute(null,null,'CITY',f.City__c);
                    writer.writeAttribute(null,null,'COUNTRY_CODE',(countryCodesMap.containsKey(f.Country_Code_Address__c) == true?countryCodesMap.get(f.Country_Code_Address__c).code__c:''));
                    if(f.Template_Id__c == 'FATCA-REP-A')
                        writer.writeAttribute(null,null,'GIIN',f.GIIN__c);
                    writer.writeAttribute(null,null,'TRANS_ID',f.Name);
                    writer.writeEndElement();
                    
                }
            }
        }
        writer.writeEndElement();
        String xmlString = writer.getXmlString();
        writer.close();
        system.debug(xmlString);
        
        Blob targetBlob = Blob.valueOf(xmlString);
        Blob hash = Crypto.generateDigest('SHA-256', targetBlob);
        System.debug(EncodingUtil.base64Encode(hash));
        System.debug(EncodingUtil.convertToHex(hash));
        String hashString = EncodingUtil.base64Encode(hash);
        String hashHexString = EncodingUtil.convertToHex(hash);
        
        String hashBody = 'base64: '+hashString+'\n'+'hex: '+hashHexString;
        
        
        Folder docFolder = [Select Id from Folder where Name='FATCA' limit 1];
        
        Document xmlDoc = new Document();
        xmlDoc.Name = 'FATCA XML - '+System.now();
        xmlDoc.Body = targetBlob;
        xmlDoc.Type = 'txt';
        xmlDoc.FolderId = docFolder.Id;
        insert xmlDoc;
        
        Document xmlHash = new Document();
        xmlHash.Name = 'FATCA Hash - '+System.now();
        xmlHash.Body = Blob.valueOf(hashBody);
        xmlHash.Type = 'txt';
        xmlHash.FolderId = docFolder.Id;
        insert xmlHash;
        
        return 'XML has been Generated';
    }
    
    webservice static String generateUpdatedXml(String generate){
    
        // Method added by Shabbir Ahmed - 04/June/2015 to record FATCA Audit
        FATCAAudit.CreateAuditRecord(NULL, 'Generate Updated XML' , 'Not captured'); 
            
        XmlStreamWriter writer = new XmlStreamWriter();
        writer.writeStartDocument('UTF-8', '1.0');        
        writer.writeStartElement(null, 'DOCS_HEADER', null);
        writer.writeAttribute(null,null,'HEADER_ID',System.Label.FATCA_Header_ID);
        writer.writeAttribute(null,null,'AUTH_ID','004');
        writer.writeAttribute(null,null,'REPORTING_PERIOD',System.Label.FATCA_Reporting_Period);
        
        Date asOnDate = Date.parse(System.Label.FATCA_As_On_Date);
        List<FATCA_Submission__c> fsubList = [Select Id,Name,GIIN__c,(Select Id,Template_ID__c,Payment_Type__c,Country_Code_Address__c,City__c,Address__c,Entity_Name__c,Name,First_Name__c,Last_Name__c,Birth_Date__c,Account_Type__c,Payment_Amount__c,Account_Balance__c,US_TIN__c,GIIN__c,Account_Number__c,Account_Holder_Type__c,Status__c from FATCA__r where Status__c='Resubmitted' or Status__c='Removed') from FATCA_Submission__c where Status__c='Resubmitted' and As_On_Date__c>:asOnDate];
        Map<String,Country_Codes__c> countryCodesMap = Country_Codes__c.getAll(); 
        for(FATCA_Submission__c fsub : fsubList){
            if(fsub.FATCA__r.size()==0){
                writer.writeStartElement(null, 'DOCS_DETAILS', null);
                writer.writeAttribute(null,null,'HEADER_ID',System.Label.FATCA_Header_ID);
                writer.writeAttribute(null,null,'GIIN',fsub.GIIN__c);
                writer.writeAttribute(null,null,'NIL_RETURN','YES');
                writer.writeEndElement();
                            
            }
            else{
                for(FATCA__c f : fsub.FATCA__r){
                    writer.writeStartElement(null, 'DOCS_DETAILS', null);
                    writer.writeAttribute(null,null,'HEADER_ID',System.Label.FATCA_Header_ID);
                    writer.writeAttribute(null,null,'TIN',f.US_TIN__c == '0' ? '000000000' : f.US_TIN__c);
                    writer.writeAttribute(null,null,'GIIN',f.GIIN__c);
                    writer.writeAttribute(null,null,'TRANS_ID',f.Name);
                    writer.writeAttribute(null,null,'ACCOUNT_HOLDER_TYPE',f.Account_Holder_Type__c);
                    writer.writeAttribute(null,null,'ACCOUNT_NUMBER',f.Account_Number__c);
                    writer.writeAttribute(null,null,'ACCOUNT_BALANCE',String.valueOf(f.Account_Balance__c));
                    writer.writeAttribute(null,null,'PAYMENT_AMOUNT',String.valueOf(f.Payment_Amount__c));
                    writer.writeAttribute(null,null,'PAYMENT_TYPE',f.Payment_Type__c);
                    writer.writeAttribute(null,null,'ACCOUNT_TYPE',f.Account_Type__c);
                    
                    if(f.Status__c=='Resubmitted')
                        writer.writeAttribute(null,null,'STATUS',System.Label.FATCA_Status_Corrected_Data);
                    else if(f.Status__c=='Removed')
                        writer.writeAttribute(null,null,'STATUS',System.Label.FATCA_Status_Void_Data);
                    
                    if(f.Template_Id__c == 'FATCA-REP-A')
                        writer.writeEmptyElement(null, 'CLIENT_P', null);
                    else if(f.Template_Id__c == 'FATCA-REP-B')
                        writer.writeEmptyElement(null, 'CLIENT_C', null);
                    else writer.writeEmptyElement(null, 'CLIENT', null); 
                    
                    writer.writeAttribute(null,null,'TIN',f.US_TIN__c == '0' ? '000000000' : f.US_TIN__c);
                    
                    if(f.Template_Id__c == 'FATCA-REP-A'){
                        writer.writeAttribute(null,null,'FNAME',f.First_Name__c);
                        writer.writeAttribute(null,null,'LNAME',f.Last_Name__c);
                        if(f.Birth_Date__c != null)
                            writer.writeAttribute(null,null,'DOB',String.valueOf(f.Birth_Date__c).substring(0,10));
                    } else if(f.Template_Id__c == 'FATCA-REP-B')
                        writer.writeAttribute(null,null,'NAME',f.Entity_Name__c);
                    String address = f.Address__c;
                    if(address != null){
                        address= address.replace(',\r\n', ' / ');
                        address= address.replace('\r\n', ' / ');
                        address= address.replace(',', ' / ');
                    }
                    writer.writeAttribute(null,null,'ADDRESS',address);
                    writer.writeAttribute(null,null,'CITY',f.City__c);
                    writer.writeAttribute(null,null,'COUNTRY_CODE',(countryCodesMap.containsKey(f.Country_Code_Address__c) == true?countryCodesMap.get(f.Country_Code_Address__c).code__c:''));
                    if(f.Template_Id__c == 'FATCA-REP-A')
                        writer.writeAttribute(null,null,'GIIN',f.GIIN__c);
                    writer.writeAttribute(null,null,'TRANS_ID',f.Name);
                    writer.writeEndElement();
                    
                }
            }
        }
        writer.writeEndElement();
        String xmlString = writer.getXmlString();
        writer.close();
        system.debug(xmlString);
        
        Blob targetBlob = Blob.valueOf(xmlString);
        Blob hash = Crypto.generateDigest('SHA-256', targetBlob);
        System.debug(EncodingUtil.base64Encode(hash));
        System.debug(EncodingUtil.convertToHex(hash));
        String hashString = EncodingUtil.base64Encode(hash);
        String hashHexString = EncodingUtil.convertToHex(hash);
        
        String hashBody = 'base64: '+hashString+'\n'+'hex: '+hashHexString;
        
        
        Folder docFolder = [Select Id from Folder where Name='FATCA' limit 1];
        
        Document xmlDoc = new Document();
        xmlDoc.Name = 'Updated FATCA XML - '+System.today().year();
        xmlDoc.Body = targetBlob;
        xmlDoc.Type = 'txt';
        xmlDoc.FolderId = docFolder.Id;
        insert xmlDoc;
        
        Document xmlHash = new Document();
        xmlHash.Name = 'Updated FATCA Hash - '+System.today().year();
        xmlHash.Body = Blob.valueOf(hashBody);
        xmlHash.Type = 'txt';
        xmlHash.FolderId = docFolder.Id;
        insert xmlHash;
        
        List<Fatca__c> fatcaList = new List<Fatca__c>();
        for(FATCA_Submission__c fsub : fsubList){
            for(FATCA__c f : fsub.FATCA__r){
                f.Sent_To_MoF__c = true;
                fatcaList.add(f);
            }
        }
        update fatcaList;
        
        return 'XML has been Generated';
    }
}