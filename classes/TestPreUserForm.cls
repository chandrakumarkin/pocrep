/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 19-Nov-2014   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class TestPreUserForm{

 static Boolean flag;
 static String str;

    /*
    Commented by: Claude Manahan - 6-14-2016 
    public static testmethod void testUser() {
    
        PageReference pageRef = Page.UserAccessForm;
        String contInd;
        String conRT1;
        Account acc = new Account(Name = 'TestAccount',BP_No__c='08978');
        Account acc123 = new Account(Name = 'HDTM',BP_No__c='00010');
        acc123.BP_No__c = '00010';
        Lookup__c nat = new Lookup__c(Type__c='Nationality');
        insert acc;
        insert acc123;
        insert nat;
        License__c lic = new License__c(status__c='Active',Account__c=acc.id);
        License__c lic123 = new License__c(status__c='Active',Account__c=acc123.id);
        
        for(RecordType conInd : [select Id,Name,DeveloperName from RecordType where DeveloperName='Individual' AND SobjectType='Contact'])
	                 contInd = conInd.Id;
	    for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true])
                  conRT1 = objRT.Id;
        Contact cont = new Contact(firstname='Tim',lastname='Khan',accountId=acc.id,Email='test@test.com',RecordTypeId=conRT1);
        Contact cont1 = new Contact(firstname='Timmy',lastname='John',accountId=acc.id,Email='test12356@test.com',RecordTypeId=contInd);
        insert lic;
        insert lic123;
        insert cont;
        insert cont1;
       
        Relationship__c RelUser = new Relationship__c();
        RelUser.Object_Contact__c = cont.id;
        RelUser.Subject_Account__c = acc.id;
        RelUser.Relationship_Type__c = 'Has Principal User';
        RelUser.Start_Date__c = Date.today(); 
        RelUser.Active__c = true;
        insert RelUser;
        Relationship__c RelUsr1 = new Relationship__c();
        RelUsr1.Object_Contact__c = cont1.id;
        RelUsr1.Subject_Account__c = acc.id;
        RelUsr1.Relationship_Type__c = 'Has Principal User';
        RelUsr1.Start_Date__c = Date.today(); 
        RelUsr1.Active__c = true;
        insert RelUsr1;
        User_SR__c SR = new User_SR__c(Username__c='test@test.com',License_Number__c =lic.id);
        User usr1 = new User(username='test@test.com',email='test@test.com',lastname='Test',CompanyName=acc.id,contactId=cont1.Id,
                            alias = 'ADY232',profileid = Label.Profile_ID,emailencodingkey='UTF-8',
            				languagelocalekey='en_US',localesidkey='en_GB',timezonesidkey='Asia/Dubai');
        insert usr1;
        User usr2 = new User(username='test123@test.com',email='test123@test.com',lastname='Test123',CompanyName=acc.id,contactId=cont.Id,
                            alias = 'AyY632',profileid = Label.Profile_ID,emailencodingkey='UTF-8',
            				languagelocalekey='en_US',localesidkey='en_GB',timezonesidkey='Asia/Dubai');
        insert usr2;
        SR.Customer__c = acc.Id;
        //SR.Submitted_DateTime__c = DateTime.now();
        Date subDate = Date.today();
        //SR.Submitted_Date__c = subDate;
        SR.Title__c = 'Mrs.';
        SR.First_Name__c = 'Finley';
        SR.Middle_Name__c = 'Ashton';
        SR.Last_Name__c = 'Kate';
        SR.Position__c = 'Engineer';
        SR.Place_of_Birth__c = 'USA';
        SR.Date_of_Birth__c = Date.newinstance(1980,10,17);
        SR.Email__c = 'test@test.com';
        SR.Roles__c = 'Company Services';
        SR.Mobile_Number__c = '+971505748286';
        SR.Nationality__c = 'Pakistan';
        SR.Passport_Number__c = 'ASD121323';
        SR.User_Form_Action__c = 'Create New User'; 
        insert SR;
        
        List <License__c> tmp = [Select Account__r.Name, Account__c from License__c where name = :SR.License_Number__c];
        List <RecordType> objRT = [select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true];
        String conRT = objRT[0].Id;
        /*
        Test.setCurrentPageReference(pageRef);
        ApexPages.CurrentPage().getparameters().put('id', SR.id);
        ApexPages.CurrentPage().getparameters().put('BPno', '08978');
        
        ApexPages.StandardController sc = new ApexPages.standardController(SR);
        PreUserCls userform = new PreUserCls(sc);
        PreUserCls userformCls1 = new PreUserCls();
        list<selectoption> lstsel = new list<selectoption>();
        userform.BPNo = '8978';
        userform.strSelVal='';
        pageRef = userform.getAccount();
        pageRef = userform.saveInfo();
        userform.getFlag();
        userform.disableopt();
        userform.RePrepare();
        //userform.sUser.Id = SR.Id;
        userform.BPNo = '08600';
        pageRef = userform.getAccount();
        pageRef = userform.saveInfo();
        
        //userform.Submitted_DateTime__c = DateTime.now();
        //userform.sUser.Submitted_Date__c = subDate;
        userform.sUser.Title__c = 'Mrs.';
        userform.sUser.First_Name__c = 'Finley';
        userform.sUser.Middle_Name__c = 'Ashton';
        userform.sUser.Last_Name__c = 'Kate';
        userform.sUser.Position__c = 'Engineer';
        userform.sUser.Place_of_Birth__c = 'USA';
        userform.sUser.Date_of_Birth__c = Date.newinstance(1980,10,17);
        userform.sUser.Email__c = 'test12356@test.com';
        userform.compServices=true;
      
        Attachment PassportCopy=new Attachment();    
        PassportCopy.Name='PassportCopy.png';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        PassportCopy.body=bodyBlob;
        PassportCopy.parentId=SR.Id;
        insert PassportCopy;
        userform.PassportCopy = PassportCopy;
        userform.sUser.Roles__c = 'Company Services';
        userform.sUser.Mobile_Number__c = '+971505748286';
        userform.sUser.Nationality__c = 'Pakistan';
        userform.sUser.Passport_Number__c = 'ASD121323';
        userform.sUser.User_Form_Action__c = 'Create New User'; 
        pageRef = userform.saveInfo();
        userform.compServices=false;
        userform.empServices=true;
        pageRef = userform.saveInfo();
        userform.compServices=true;
        userform.empServices=true;
        pageRef = userform.saveInfo();
        userform.sUser.Date_of_Birth__c = Date.newinstance(2000,10,17);
        pageRef = userform.saveInfo();
 		PageReference pageRefa = Page.UserAccessForm;
        ApexPages.CurrentPage().getparameters().put('BPno','00010');
        ApexPages.StandardController scc3 = new ApexPages.standardController(SR);
        PreUserCls userformCl = new PreUserCls(scc3);
        SR.Customer__c = acc123.id;
        //pageRef = userformCl.getAccount();
        //pageRef = userformCl.saveInfo();
        
        ApexPages.CurrentPage().getparameters().put('BPno',null);
        List <License__c> lstlic = [Select id,Name from License__c where Id=:lic.Id];
        acc123.Active_License__c = lstlic[0].id;
        userform.compServices=false;
        userform.empServices=false;
        userform.sUser.License_Number__c='098';
        userform.BPNo = null;
        pageRef = userform.getAccount();
        //pageRef = userform.saveInfo();
        ApexPages.CurrentPage().getparameters().put('BPno','00010');
		userform.BPNo = '00010';
        userform.getAccount();
        //pageRef = userform.saveInfo();
        userform.getPassportCopy();
        PageReference pageRef1 = Page.UserAccessForm;
        ApexPages.CurrentPage().getparameters().put('id',null);
        ApexPages.CurrentPage().getparameters().put('BPno',null);
        ApexPages.StandardController scc = new ApexPages.standardController(SR);
        PreUserCls userformCls2 = new PreUserCls(scc);
        List <License__c> lstlic123 = [Select id,Name from License__c where Id=:lic123.Id];
        userformCls2.compServices=true;
        userformCls2.empServices=false;
        userformCls2.sUser.License_Number__c=lstlic[0].Name;
        userformCls2.sUser.Username__c = '';
        //pageRef1 = userformCls2.saveInfo();
        userformCls2.BPNo=null;
        userformCls2.sUser.License_Number__c=null;
        pageRef1 = userformCls2.getAccount();
        userformCls2.compServices=false;
        userformCls2.empServices=true;
        pageRef1 = userformCls2.getAccount();
        userformCls2.sUser.License_Number__c=lstlic[0].Name;
        userformCls2.getPassportCopy();
        userformCls2.disableopt();
        userformCls2.RePrepare();
        userformCls2.compServices=false;
        userformCls2.empServices=true;
        pageRef1 = userformCls2.saveInfo();
        userformCls2.compServices=true;
        userformCls2.empServices=true;
        pageRef1 = userformCls2.getAccount();
        pageRef1 = userformCls2.saveInfo();
        userformCls2.compServices=false;
        userformCls2.empServices=false;  
        pageRef1 = userformCls2.getAccount();
        pageRef1 = userformCls2.saveInfo();
        userformCls2.disablename1();
        userformCls2.enablename1();
        userformCls2.disableopt();
        userformCls2.enableopt();
        userformCls2.setFlag(flag);
        flag = userformCls2.getdisplayAcc();
        userformCls2.setdisplayAcc(flag);
        str = userformCls2.getaccountName();
        userformCls2.setaccountName(str);
        pageRef = userformCls2.ChangeOption();
        pageRef = userform.ChangeOption();
        
        userformCls2.BPNo=null;
        userformCls2.sUser.Title__c = 'Mrs.';
        userformCls2.sUser.First_Name__c = 'Finley';
        userformCls2.sUser.Middle_Name__c = 'Ashton';
        userformCls2.sUser.Last_Name__c = 'Kate';
        userformCls2.sUser.Position__c = 'Engineer';
        userformCls2.sUser.Place_of_Birth__c = 'USA';
        userformCls2.sUser.Date_of_Birth__c = Date.newinstance(1980,10,17);
        userformCls2.sUser.Email__c = 'test@test.com';
        userformCls2.compServices=true;
        userformCls2.empServices=true;
        userformCls2.PassportCopy = PassportCopy;
        userformCls2.sUser.Roles__c = 'Company Services';
        userformCls2.sUser.Mobile_Number__c = '+971505674534';
        userformCls2.sUser.Nationality__c = 'Pakistan';
        userformCls2.sUser.Passport_Number__c = 'ASD121323';
        userformCls2.sUser.User_Form_Action__c = 'Create New User'; 
        pageRef = userformCls2.saveInfo();
        userformCls2.PassportCopy = null;
        pageRef = userformCls2.saveInfo();
        userformCls2.sUser.Passport_Number__c = null;
        pageRef = userformCls2.saveInfo();
        userformCls2.sUser.Date_of_Birth__c = Date.newinstance(2012,10,17);
        pageRef = userformCls2.saveInfo();
        userformCls2.strSelVal = 'Create New User';
		userformCls2.ChangeOption();
        userformCls2.BPNo='08978';
        userformCls2.getAccount();
        //pageRef = userformCls2.saveInfo();
        
        userformCls2.BPNo='00010';
        ApexPages.CurrentPage().getparameters().put('BPno','00010');
        userformCls2.sUser.License_Number__c=lstlic[0].Name;
        userformCls2.getAccount();
        //pageRef = userformCls2.saveInfo();
        
        Contact cont12 = new Contact(firstname='Tim',lastname='Khan',accountId=acc.id);
        Contact cont13 = new Contact(firstname='Timmy',lastname='John',accountId=acc.id);
        insert cont12;
        insert cont13;
        User usr11 = new User(username='test43@test.com',email='test45@test.com',lastname='123Test',CompanyName=acc.id,contactId=cont12.Id,
                             alias = 'ADWE2',profileid = Label.Profile_ID,emailencodingkey='UTF-8',
            				languagelocalekey='en_US',localesidkey='en_GB',timezonesidkey='Asia/Dubai');
        insert usr11;
        User usr12 = new User(username='test167@test.com',email='test16@test.com',lastname='Test767',CompanyName=acc.id,contactId=cont13.Id,
                              alias = 'AH232',profileid = Label.Profile_ID,emailencodingkey='UTF-8',
            				languagelocalekey='en_US',localesidkey='en_GB',timezonesidkey='Asia/Dubai');
        insert usr12;
        //pageRef = userformCls2.saveInfo();
        
        //PreUserCls.createUser(SR.Id);
       
    }*/
    
	public static testmethod void testPreUser() { 
        
        Account acc = new Account(Name = 'TestAccount',BP_No__c='08978');
        insert acc;
        String conRT1;
        String contInd;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true])
                  conRT1 = objRT.Id;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='Individual' AND SobjectType='Contact' AND IsActive=true])
                  contInd = objRT.Id;
        Contact cont1 = new Contact(firstname='Timmy',lastname='John',accountId=acc.id,Email='test12356@test.com',RecordTypeId=contInd);
        insert cont1;
        Contact cont = new Contact(firstname='Tim',lastname='Khan',accountId=acc.id,Email='test@test.com',RecordTypeId=conRT1);
        insert cont;

        License__c lic1 = new License__c(status__c='Active',Account__c=acc.id);
        insert lic1;
        Relationship__c RelUser = new Relationship__c();
        RelUser.Object_Contact__c = cont.id;
        RelUser.Subject_Account__c = acc.id;
        RelUser.Relationship_Type__c = 'Has Principal User';
        RelUser.Start_Date__c = Date.today(); 
        RelUser.Active__c = true;
        insert RelUser;
        Relationship__c RelUsr1 = new Relationship__c();
        RelUsr1.Object_Contact__c = cont1.id;
        RelUsr1.Subject_Account__c = acc.id;
        RelUsr1.Relationship_Type__c = 'Has Principal User';
        RelUsr1.Start_Date__c = Date.today(); 
        RelUsr1.Active__c = true;
        insert RelUsr1;
        User_SR__c UserSR = new User_SR__c(Username__c='test@test.com',License_Number__c =lic1.id);
        UserSR.Customer__c = acc.Id;
        //SR.Submitted_DateTime__c = DateTime.now();
        //Date SRDate = Date.today();
        //SR.Submitted_Date__c = subDate;
        UserSR.Title__c = 'Mrs.';
        UserSR.First_Name__c = 'Finley';
        UserSR.Middle_Name__c = 'Ashton';
        UserSR.Last_Name__c = 'Kate';
        UserSR.Position__c = 'Engineer';
        UserSR.Place_of_Birth__c = 'USA';
        UserSR.Date_of_Birth__c = Date.newinstance(1980,10,17);
        UserSR.Email__c = 'test@test.com';
        UserSR.Roles__c = 'Company Services';
        UserSR.Mobile_Number__c = '+971505674534';
        UserSR.Nationality__c = 'Pakistan';
        UserSR.Passport_Number__c = 'ASD121323';
        UserSR.User_Form_Action__c = 'Create New User'; 
        insert UserSR;
        PreUserCls.createUser(UserSR.Id);
        Account accNew = new Account(Name = 'NewUserAccount',BP_No__c='00978');
        insert accNew;
        License__c lic = new License__c(status__c='Active',Account__c=accNew.id);
        insert lic;
        User_SR__c NewSR = new User_SR__c(License_Number__c =lic.id);
        NewSR.Customer__c = accNew.Id;
        NewSR.Title__c = 'Mrs.';
        NewSR.First_Name__c = 'Finley';
        NewSR.Middle_Name__c = 'Ashton';
        NewSR.Last_Name__c = 'Kate';
        NewSR.Position__c = 'Engineer';
        NewSR.Place_of_Birth__c = 'USA';
        NewSR.Date_of_Birth__c = Date.newinstance(1980,10,17);
        NewSR.Email__c = 'testnew@test.com';
        NewSR.Roles__c = 'Company Services';
        NewSR.Mobile_Number__c = '+971505674534';
        NewSR.Nationality__c = 'Pakistan';
        NewSR.Passport_Number__c = 'UYTT23';
        NewSR.User_Form_Action__c = 'Create New User'; 
        insert NewSR;
        PreUserCls.createUser(NewSR.Id);
        NewSR.Customer__c = accNew.Id;
        NewSR.Title__c = 'Mrs.';
        NewSR.First_Name__c = 'Finley';
        NewSR.Middle_Name__c = 'Ashton';
        NewSR.Last_Name__c = 'Kate';
        NewSR.Position__c = 'Engineer';
        NewSR.Place_of_Birth__c = 'USA';
        NewSR.Date_of_Birth__c = Date.newinstance(1980,10,17);
        NewSR.Email__c = 'testnew@test.com';
        NewSR.Roles__c = 'Company Services';
        NewSR.Mobile_Number__c = '+971505674534';
        NewSR.Nationality__c = 'Pakistan';
        NewSR.Passport_Number__c = 'UYTT23';
        NewSR.User_Form_Action__c = 'Create New User'; 
        update NewSR;
        PreUserCls.createUser(NewSR.Id);
        //PreUserCls.createUser(NewSR123.Id);  
        
    }
    public static testmethod void testPreview() {
		//
		CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        Service_Request__c SR = new Service_Request__c();
        Account acc = new Account(Name = 'TestAccount',BP_No__c='08978');
        insert acc;
        Account acc1 = new Account(Name = 'Testing123',BP_No__c='08934');
        insert acc1;
        SR.Customer__c = acc.id;
        SR.Send_SMS_To_Mobile__c = '+971505748282';
        insert SR;
        Service_Request__c SR1 = new Service_Request__c(Customer__c=acc1.id);
        SR1.Send_SMS_To_Mobile__c = '+971505748282';
        insert SR1;
        
        User_SR__c SR12 = new User_SR__c(Customer__c=acc1.id);
        insert SR12;
        /*
        User_SR__c SR = new User_SR__c(Customer__c=acc1.id);
        insert SR;
*/
        Attachment attach =new Attachment(Name='New User Access Form.pdf',parentId =SR1.id);
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        insert attach;
        
        SR_Doc__c doc=new SR_Doc__c(Name='User Access Form',Service_Request__c=SR.id,Doc_ID__c=attach.Id);
        insert doc;
        ApexPages.CurrentPage().getparameters().put('SRNo', SR.id);
        FormPreviewController PreForm = new FormPreviewController();
        PreForm.loadPDF();
        
        ApexPages.CurrentPage().getparameters().put('UserSRNo', SR.id);
        PreForm.loadDoc();
        FormPreviewController PreForm1 = new FormPreviewController();
        ApexPages.CurrentPage().getparameters().put('UserSRNo', SR1.id);
        //PreForm1.SRId=SR12.id;
        //PreForm1.loadPDF();
        PreForm1.loadDoc();
    }

}