/*
Author      :   Ravi
Description :   This class will be controller for the Listing website Listing Advanced Search page
--------------------------------------------------------------------------------------------------------------------------

Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No	Date		Updated By    	Description
--------------------------------------------------------------------------------------------------------------------------             
*/
public without sharing class ListingAdvancedSearch {
	
	public string JSONString {get;set;}
	public string buildingJSONString {get;set;}
	public ListingAdvancedSearch(){
		list<SearchDetails> lstSD = new list<SearchDetails>();
		SearchDetails objSD;
		for(Listing__c objL : [select Id,Name,Building_Name__c,Unit__c,Start_Date__c,End_Date__c,Listing_Type__c,Price__c,Sq_Feet__c,No_of_Bedrooms__c,No_of_Pax__c,Property_Type__c,
						Featured__c,Address_Details__c,Agent_Details_if_any__c,Amenities__c,Company_Name__c,Company_Profile__c,Food_and_Beverage__c,Listing_Description__c,Listing_Title__c,
						Location__c,No_of_Days__c,Website__c,Work_Phone__c,Unit__r.Name,Status__c,Unit__r.Building__r.Building_Coordinates__Latitude__s,Unit__r.Building__r.Building_Coordinates__Longitude__s,
						(select Id,Name,Doc_ID__c,Document_Master__r.Code__c from SR_Docs__r where Listing_Doc_Status__c = 'Active' AND Doc_ID__c != null)
						from Listing__c where Status__c = 'Active' AND Featured__c = true order by Published_Date__c desc]){
			objSD = new SearchDetails();
			objSD.BuildingName = objL.Building_Name__c;
			objSD.ListingDescription  = objL.Listing_Description__c;
			objSD.ListingTitle = objL.Listing_Title__c;
			objSD.ListingType = objL.Listing_Type__c;
			objSD.Location = objL.Location__c;
			objSD.Price = objL.Price__c != null ? objL.Price__c+'' : '';
			objSD.PropertyType = objL.Property_Type__c;
			objSD.SqrtFeet = objL.Sq_Feet__c != null ? objL.Sq_Feet__c+'' : '';
			objSD.Status = objL.Status__c;
			objSD.Unit = objL.Unit__r.Name;
			objSD.RecordId = objL.Id;
			Integer i = 0;
			for(SR_Doc__c objDoc : objL.SR_Docs__r){
				string dName = objDoc.Document_Master__r.Code__c;
				if(dName != 'Fit-Out Manual' && dName != 'Tenant Manual' && dName != 'Floor Plan'){
					if(dName == 'Photograph 1')
						objSD.Photo1 = Site.getPathPrefix()+'/servlet/servlet.FileDownload?file='+objDoc.Doc_ID__c;
					i++;
				}
			}
			objSD.NoOfPhotos = i+'';
			lstSD.add(objSD);
		}
		//if(!lstSD.isEmpty())
		JSONString = JSON.serialize(lstSD);
        
        
        list<MapViewDetails> lstMVD = new list<MapViewDetails>();
		MapViewDetails objMVD;
		map<Id,Integer> mapLCount = new map<Id,Integer>();
		for(Listing__c obj : [select Id,Unit__c,Unit__r.Building__c from Listing__c where Unit__r.Building__c != null AND Status__c = 'Active']){
			Integer i = mapLCount.containsKey(obj.Unit__r.Building__c) ? (mapLCount.get(obj.Unit__r.Building__c)+1) : 1;
			mapLCount.put(obj.Unit__r.Building__c,i);
		}
		for(Building__c objB : [select Id,Name,Building_No__c,Building_on_Website__c from Building__c where Building_No__c != null]){
			objMVD = new MapViewDetails();
			objMVD.BuildingName = objB.Name;
			objMVD.BuildingNo = objB.Building_No__c;
            objMVD.BuildingOnWebsite = objB.Building_on_Website__c;
			objMVD.AvailableUnits = mapLCount.containsKey(objB.Id) ? (mapLCount.get(objB.Id)+'') : '0';
			lstMVD.add(objMVD);
		}
		if(!lstMVD.isEmpty())
			buildingJSONString = JSON.serialize(lstMVD);
	}
		
	public class SearchDetails{
		public string BuildingName {get;set;}
		public string ListingDescription {get;set;}
		public string ListingTitle {get;set;}
		public string ListingType {get;set;}
		public string Location {get;set;}
		public string Price {get;set;}
		public string PropertyType {get;set;}
		public string SqrtFeet {get;set;}
		public string Status {get;set;}
		public string Unit {get;set;}
		public string RecordId {get;set;}
		
		public string Photo1 {get;set;}
		public string NoOfPhotos {get;set;}
	}
    public class MapViewDetails{
		public string BuildingName {get;set;}
		public string BuildingNo {get;set;}
		public string AvailableUnits {get;set;}
        public string BuildingOnWebsite {get;set;}
	}
}