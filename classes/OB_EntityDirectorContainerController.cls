/**
 *Author : Merul Shah
 *Description : This is the container for Entity Director module..
 * v1.0 Merul  26 march 2020  Libary implementation
 **/

public without sharing class OB_EntityDirectorContainerController {
  @AuraEnabled
  public static ResponseWrapper getExistingAmendment(String reqWrapPram) {
    String indRecTyp = OB_QueryUtilityClass.getRecordtypeID(
      'HexaBPM_Amendment__c',
      'Individual'
    );
    String bodyRecTyp = OB_QueryUtilityClass.getRecordtypeID(
      'HexaBPM_Amendment__c',
      'Body_Corporate'
    );

    //reqest wrpper
    OB_EntityDirectorContainerController.RequestWrapper reqWrap = (OB_EntityDirectorContainerController.RequestWrapper) JSON.deserializeStrict(
      reqWrapPram,
      OB_EntityDirectorContainerController.RequestWrapper.class
    );
    //response wrpper
    OB_EntityDirectorContainerController.ResponseWrapper respWrap = new OB_EntityDirectorContainerController.ResponseWrapper();

    //* v1.0 Merul  26 march 2020  Libary implementation
    Map<Id, AccountContactRelation> mapRelatedAccForAmed = new Map<Id, AccountContactRelation>();
    Map<Id, AccountContactRelation> mapRelatedAccForRelshp = new Map<Id, AccountContactRelation>();

    //* v1.0 Merul  26 march 2020  Libary implementation
    // Moving out the declarations
    OB_EntityDirectorContainerController.SRWrapper srWrap = new OB_EntityDirectorContainerController.SRWrapper();
    srWrap.amedWrapLst = new List<OB_EntityDirectorContainerController.AmendmentWrapper>();
    srWrap.amedShrHldrWrapLst = new List<OB_EntityDirectorContainerController.AmendmentWrapper>();

    //* v1.0 Merul  26 march 2020  Libary implementation
    HexaBPM__Service_Request__c currentSR = new HexaBPM__Service_Request__c();

    // * v1.0 Merul  26 march 2020  Libary implementation
    Map<String, AmendmentWrapper> mapExistingAmed = new Map<String, AmendmentWrapper>();
    Map<String, AmendmentWrapper> mapShareholder = new Map<String, AmendmentWrapper>();
    Map<String, AmendmentWrapper> mapRelShpToAmedWrap = new Map<String, AmendmentWrapper>();
    Map<String, AmendmentWrapper> mapShareholderCombine = new Map<String, AmendmentWrapper>();

    String srId = reqWrap.srId;
    String flowId = reqWrap.flowId;
    String pageId = reqWrap.pageId;
    system.debug('##### srId  ' + srId);

    //declaration text
    String declarationIndividualText = '';
    String declarationCorporateText = '';
    for (Declarations__mdt declare : [
      SELECT
        ID,
        Declaration_Text__c,
        Application_Type__c,
        Field_API_Name__c,
        Record_Type_API_Name__c,
        Object_API_Name__c
      FROM Declarations__mdt
      WHERE
        (Record_Type_API_Name__c = 'Individual'
        OR Record_Type_API_Name__c = 'Body_Corporate')
        AND Field_API_Name__c = 'I_Agree_Director__c'
        AND Object_API_Name__c = 'HexaBPM_Amendment__c'
    ]) {
      if (
        String.IsNotBlank(declare.Record_Type_API_Name__c) &&
        declare.Record_Type_API_Name__c.EqualsIgnorecase('Individual')
      ) {
        declarationIndividualText = declare.Declaration_Text__c;
      } else if (
        String.IsNotBlank(declare.Record_Type_API_Name__c) &&
        declare.Record_Type_API_Name__c.EqualsIgnorecase('Body_Corporate')
      ) {
        declarationCorporateText = declare.Declaration_Text__c;
      }
    }

    // getting all related account based on current loggedin user.
    User loggedInUser = new User();
    String contactId;

    for (User UserObj : [
      SELECT id, contactid, Contact.AccountId, ProfileId
      FROM User
      WHERE Id = :UserInfo.getUserId()
      LIMIT 1
    ]) {
      loggedInUser = UserObj;
      contactId = UserObj.contactid;
    }

    if (string.isNotBlank(contactId)) {
      // getting account contact relation ship

      for (AccountContactRelation accConRel : [
        SELECT
          Id,
          AccountId,
          ContactId,
          Roles,
          Access_Level__c,
          Account.ROC_Status__c
        FROM AccountContactRelation
        WHERE ContactId = :contactId AND Roles INCLUDES ('Company Services')
      ]) {
        if (
          accConRel.Account.ROC_Status__c == 'Under Formation' ||
          accConRel.Account.ROC_Status__c == 'Account Created'
        ) {
          mapRelatedAccForAmed.put(accConRel.AccountId, accConRel);
        }

        if (
          accConRel.Account.ROC_Status__c == 'Active' ||
          accConRel.Account.ROC_Status__c == 'Not Renewed'
        ) {
          mapRelatedAccForRelshp.put(accConRel.AccountId, accConRel);
        }
      }

      reqWrap.mapRelatedAccForRelshp = mapRelatedAccForRelshp;
      system.debug(
        '@@@@@@@@@@ mapRelatedAccForRelshp ' + mapRelatedAccForRelshp
      );

      reqWrap.mapRelatedAccForAmed = mapRelatedAccForAmed;
      system.debug('@@@@@@@@@@ mapRelatedAcc ' + mapRelatedAccForAmed);

      // getting relationship

      List<Relationship__c> lstRel = OB_libraryHelper.getRelationShip(
        mapRelatedAccForRelshp
      );
      for (Relationship__c relShip : lstRel) {
        system.debug('############## relShip ' + relShip);
        OB_EntityDirectorContainerController.AmendmentWrapper amedWrap = new OB_EntityDirectorContainerController.AmendmentWrapper();
        amedWrap.amedObj = new HexaBPM_Amendment__c();
        if (relShip.Amendment_ID__c != null) {
          amedWrap.relatedAmendID = relShip.Amendment_ID__c;
        } else {
          amedWrap.relatedAmendID = relShip.Sys_Amendment_Id__c;
        }
        amedWrap.RelatedRelId = relShip.id;
        // amedWrap.lookupLabel = (amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name :'');
        if (relShip.Object_Contact__c != null) {
          amedWrap.amedObj = OB_libraryHelper.setAmedFromObjContact(
            amedWrap.amedObj,
            relShip
          );
          amedWrap.isIndividual = true;
          amedWrap.displayName =
            (String.isNotBlank(amedWrap.amedObj.First_Name__c)
              ? amedWrap.amedObj.First_Name__c
              : '') +
            ' ' +
            (String.isNotBlank(amedWrap.amedObj.Last_Name__c)
              ? amedWrap.amedObj.Last_Name__c
              : '');
          //amedWrap.amedObj.recordTypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Individual');
          amedWrap.amedObj.recordTypeId = indRecTyp;

          system.debug(
            '@@@@@@@@@@@   amedWrap.amedObj.recordTypeId  ' +
            amedWrap.amedObj.recordTypeId
          );

          String passport = (String.isNotBlank(amedWrap.amedObj.Passport_No__c)
            ? amedWrap.amedObj.Passport_No__c
            : '');
          String nationality = (String.isNotBlank(
              amedWrap.amedObj.Nationality_list__c
            )
            ? amedWrap.amedObj.Nationality_list__c
            : '');
          String uniqKey = passport + nationality;
          if (String.isNotBlank(uniqKey)) {
            mapRelShpToAmedWrap.put(uniqKey.toLowercase(), amedWrap);
          }
        } else if (relShip.Object_Account__c != null) {
          amedWrap.amedObj = OB_libraryHelper.setAmedFromObjAccount(
            amedWrap.amedObj,
            relShip
          );
          amedWrap.isBodyCorporate = true;
          amedWrap.displayName = amedWrap.amedObj.Company_Name__c;
          //amedWrap.amedObj.recordTypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Body_Corporate');
          amedWrap.amedObj.recordTypeId = bodyRecTyp;

          if (String.isNotBlank(amedWrap.amedObj.Registration_No__c)) {
            mapRelShpToAmedWrap.put(
              amedWrap.amedObj.Registration_No__c.toLowercase(),
              amedWrap
            );
          }
        }
      }
      system.debug('@@@@@@@@@@ mapRelShpToAmedWrap ' + mapRelShpToAmedWrap);

      system.debug('@@@@@@@@@@ reqWrap.srId ' + reqWrap.srId);
      List<HexaBPM__Service_Request__c> lstSerReq = OB_libraryHelper.getSR(
        srId,
        mapRelatedAccForAmed
      );
      respWrap.debugger = lstSerReq;

      for (HexaBPM__Service_Request__c serObj : lstSerReq) {
        // WHERE id = :srId
        // draft condition remaining.
        // for current SR.
        if (serObj.Id == srId) {
          system.debug('@@@@@@@@@@  current serObj ' + serObj);
          srWrap.srObj = serObj;
          currentSR = serObj;
          srWrap.sEntityType = OB_DIFCNamingUtility.manageDIFCLable(
            serObj,
            'Director'
          );

          if (
            String.IsNotBlank(declarationCorporateText) &&
            declarationCorporateText.contains('Director')
          ) {
            declarationCorporateText = declarationCorporateText.replace(
              'Director',
              srWrap.sEntityType.toLowerCase()
            );
          }
          if (
            String.IsNotBlank(declarationIndividualText) &&
            declarationIndividualText.contains('Director')
          ) {
            declarationIndividualText = declarationIndividualText.replace(
              'Director',
              srWrap.sEntityType.toLowerCase()
            );
          }
          srWrap.declarationIndividualText = declarationIndividualText;
          srWrap.declarationCorporateText = declarationCorporateText;

          // for current SR amendment.
          for (HexaBPM_Amendment__c amed : serObj.Amendments__r) {
            OB_EntityDirectorContainerController.AmendmentWrapper amedWrap = new OB_EntityDirectorContainerController.AmendmentWrapper();
            amedWrap.amedObj = amed;
            amedWrap.isIndividual = (amed.RecordType.DeveloperName ==
              'Individual'
              ? true
              : false);
            amedWrap.isBodyCorporate = (amed.RecordType.DeveloperName ==
              'Body_Corporate'
              ? true
              : false);
            amedWrap.lookupLabel = (amed.Entity_Name__c != null
              ? amed.Entity_Name__r.Name
              : '');

            if (
              String.IsNotBlank(serObj.legal_structures__c) &&
              serObj.legal_structures__c.equalsIgnorecase('Foundation') &&
              String.IsNotBlank(amed.Role__c) &&
              amed.Role__c.containsIgnoreCase('Guardian') &&
              amed.RecordType.DeveloperName ==
              OB_ConstantUtility.AMENDMENT_INDIVIDUAL_RT.developerName
            ) {
              // no individual Guardian list to be displayed
              system.debug('break');
              continue;
            }
            
           if(amed.Role__c.containsIgnoreCase('Qualified Applicant') &&
              amed.RecordType.DeveloperName =='Body_Corporate'
            ) {
              // no individual Guardian list to be displayed
              system.debug('break');
              continue;
            }

            if (amed.Role__c.containsIgnoreCase('Corporate Service Provider') &&
              amed.RecordType.DeveloperName =='Body_Corporate'
            ) {
              // no individual Guardian list to be displayed
              system.debug('break');
              continue;
            }

            if (amed.RecordType.DeveloperName == 'Individual') {
              system.debug('amed Individual' + amed);

              String passport = (String.isNotBlank(amed.Passport_No__c)
                ? amed.Passport_No__c
                : '');
              String nationality = (String.isNotBlank(amed.Nationality_list__c)
                ? amed.Nationality_list__c
                : '');
              String uniqKey = passport + nationality;
              //srWrap.amedWrapLst.add(amedWrap);
              amedWrap.displayName =
                (String.isNotBlank(amedWrap.amedObj.First_Name__c)
                  ? amedWrap.amedObj.First_Name__c
                  : '') +
                ' ' +
                (String.isNotBlank(amedWrap.amedObj.Last_Name__c)
                  ? amedWrap.amedObj.Last_Name__c
                  : '');

              if (String.isNotBlank(uniqKey)) {
                mapExistingAmed.put(uniqKey.toLowercase(), amedWrap);
              }
            } else if (amed.RecordType.DeveloperName == 'Body_Corporate') {
              amedWrap.displayName = amedWrap.amedObj.Company_Name__c;
              if (amedWrap.displayName == null) {
                amedWrap.displayName = amedWrap.amedObj.Individual_Corporate_Name__c;
              }
              
              if (String.isNotBlank(amed.Registration_No__c)) {
                mapExistingAmed.put(
                  amed.Registration_No__c.toLowercase(),
                  amedWrap
                );
              }
            }

            system.debug('index of ' + amed.Role__c.indexOf('Director'));

            if (
              !(String.isBlank(amed.Role__c)) &&
              amed.Role__c.indexOf('Director') != -1
            ) {
              srWrap.amedWrapLst.add(amedWrap);
            } else {
              srWrap.amedShrHldrWrapLst.add(amedWrap);
            }

            system.debug('@@@@  srWrap.amedWrapLst' + srWrap.amedWrapLst);
            system.debug(
              '@@@@  amedShrHldrWrapLst' + srWrap.amedShrHldrWrapLst
            );
          }
        } else {
        }
      }

      // for amedment libary from all other SR.
      for (HexaBPM__Service_Request__c serObj : lstSerReq) {
        for (HexaBPM_Amendment__c amed : serObj.Amendments__r) {
          if (serObj.Id != srId) {
            OB_EntityDirectorContainerController.AmendmentWrapper amedWrap = new OB_EntityDirectorContainerController.AmendmentWrapper();
            amedWrap.amedObj = amed;
            amedWrap.relatedAmendID = amed.Id;
            if (serObj.Id != srId) {
              amedWrap.amedObj.Id = null;
            }
            amedWrap.isIndividual = (amed.RecordType.DeveloperName ==
              'Individual'
              ? true
              : false);
            amedWrap.isBodyCorporate = (amed.RecordType.DeveloperName ==
              'Body_Corporate'
              ? true
              : false);
            amedWrap.lookupLabel = (amed.Entity_Name__c != null
              ? amed.Entity_Name__r.Name
              : '');

            if (
              !(String.isBlank(amed.Role__c)) &&
              amed.Role__c.indexOf('Shareholder') != -1
            ) {
              if (amed.RecordType.DeveloperName == 'Body_Corporate') {
                System.debug(
                  '@@@@@@@@@@@@@@@@@  amed.Registration_No__c ' +
                  amed.Registration_No__c
                );
                if (String.isNotBlank(amed.Registration_No__c)) {
                  amedWrap.displayName = amedWrap.amedObj.Company_Name__c;
                  if (amedWrap.displayName == null) {
                    amedWrap.displayName = amedWrap.amedObj.Individual_Corporate_Name__c;
                  }
                  mapShareholder.put(
                    amed.Registration_No__c.toLowercase(),
                    amedWrap
                  );
                }
              }

              if (amed.RecordType.DeveloperName == 'Individual') {
                String passport = (String.isNotBlank(amed.Passport_No__c)
                  ? amed.Passport_No__c
                  : '');
                String nationality = (String.isNotBlank(
                    amed.Nationality_list__c
                  )
                  ? amed.Nationality_list__c
                  : '');
                String uniqKey = passport + nationality;
                System.debug('@@@@@@@@@@@@@@@@@ uniqKey ' + uniqKey);
                // srWrap.amedShrHldrWrapLst.add(amedWrap);
                //if( String.isNotBlank( uniqKey ) )
                if (String.isNotBlank(uniqKey)) {
                  amedWrap.displayName =
                    (String.isNotBlank(amedWrap.amedObj.First_Name__c)
                      ? amedWrap.amedObj.First_Name__c
                      : '') +
                    ' ' +
                    (String.isNotBlank(amedWrap.amedObj.Last_Name__c)
                      ? amedWrap.amedObj.Last_Name__c
                      : '');
                  mapShareholder.put(uniqKey.toLowercase(), amedWrap);
                }
              }
            }
          }
        }
      }
      System.debug(
        '@@@@@@@@@@@@@@@@@ mapShareholder ' + mapShareholder.keySet()
      );

      system.debug('@@@@@@@@@@ mapExistingAmed ' + mapShareholder);
      system.debug(
        '@@@@@@@@@@ mapExistingAmed.size() ' + mapShareholder.size()
      );
      system.debug('@@@@@@@@@@ mapRelShpToAmedWrap ' + mapRelShpToAmedWrap);
      system.debug(
        '@@@@@@@@@@ mapRelShpToAmedWrap.size() ' + mapRelShpToAmedWrap.size()
      );

      //mapShareholderCombine
      for (String key : mapShareholder.keySet()) {
        if (!mapExistingAmed.containsKey(key.toLowercase())) {
          mapShareholderCombine.put(key, mapShareholder.get(key));
        }
      }

      for (String key : mapRelShpToAmedWrap.keySet()) {
        if (!mapExistingAmed.containsKey(key.toLowercase())) {
          mapShareholderCombine.put(key, mapRelShpToAmedWrap.get(key));
        }
      }
      system.debug('@@@@@@@@@@ mapShareholderCombine ' + mapShareholderCombine);
      system.debug(
        '@@@@@@@@@@ mapShareholderCombine.size() ' +
        mapShareholderCombine.size()
      );

      if (mapShareholderCombine.size() > 0) {
        srWrap.amedShrHldrWrapLst.addAll(mapShareholderCombine.values());
        // srWrap.amedShrHldrWrapLst.sort();
      }
      System.debug(
        '@@@@@@@@@@@@@@@@@ srWrap.amedShrHldrWrapLst  ' +
        srWrap.amedShrHldrWrapLst
      );

      //Always one SR so can be in loop.
      respWrap.srWrap = srWrap;

      for (HexaBPM__Section_Detail__c sectionObj : [
        SELECT id, HexaBPM__Component_Label__c, name
        FROM HexaBPM__Section_Detail__c
        WHERE
          HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
          AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId
        LIMIT 1
      ]) {
        respWrap.ButtonSection = sectionObj;
      }
    }

    return respWrap;
  }

  @AuraEnabled
  public HexaBPM__Section_Detail__c ButtonSection { get; set; }

  @AuraEnabled
  public static ButtonResponseWrapper getButtonAction(
    string SRID,
    string ButtonId,
    string pageId
  ) {
    ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
    HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(
      SRID
    );
    PageFlowControllerHelper.objSR = objRequest;
    PageFlowControllerHelper objPB = new PageFlowControllerHelper();

    objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
    upsert objRequest;
    OB_RiskMatrixHelper.CalculateRisk(SRID);

    PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(
      ButtonId
    );
    system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
    respWrap.pageActionName = responseNextPage.pg;
    respWrap.communityName = responseNextPage.communityName;
    respWrap.CommunityPageName = responseNextPage.CommunityPageName;
    respWrap.sitePageName = responseNextPage.sitePageName;
    respWrap.strBaseUrl = responseNextPage.strBaseUrl;
    respWrap.srId = objRequest.Id;
    respWrap.flowId = responseNextPage.flowId;
    respWrap.pageId = responseNextPage.pageId;
    respWrap.isPublicSite = responseNextPage.isPublicSite;

    system.debug(
      '@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName
    );
    return respWrap;
  }

  @AuraEnabled
  public static ButtonResponseWrapper getDirectorButtonAction(
    string SRID,
    string ButtonId,
    string pageId
  ) {
    ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
    HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(
      SRID
    );
    PageFlowControllerHelper.objSR = objRequest;
    PageFlowControllerHelper objPB = new PageFlowControllerHelper();

    objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
    upsert objRequest;
    OB_RiskMatrixHelper.CalculateRisk(SRID);

    PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(
      ButtonId
    );
    system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
    respWrap.pageActionName = responseNextPage.pg;
    respWrap.communityName = responseNextPage.communityName;
    respWrap.CommunityPageName = responseNextPage.CommunityPageName;
    respWrap.sitePageName = responseNextPage.sitePageName;
    respWrap.strBaseUrl = responseNextPage.strBaseUrl;
    respWrap.srId = objRequest.Id;
    respWrap.flowId = responseNextPage.flowId;
    respWrap.pageId = responseNextPage.pageId;
    respWrap.isPublicSite = responseNextPage.isPublicSite;

    system.debug(
      '@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName
    );
    return respWrap;
  }

  public class ButtonResponseWrapper {
    @AuraEnabled
    public String pageActionName { get; set; }
    @AuraEnabled
    public string communityName { get; set; }
    @AuraEnabled
    public String errorMessage { get; set; }
    @AuraEnabled
    public string CommunityPageName { get; set; }
    @AuraEnabled
    public string sitePageName { get; set; }
    @AuraEnabled
    public string strBaseUrl { get; set; }
    @AuraEnabled
    public string srId { get; set; }
    @AuraEnabled
    public string flowId { get; set; }
    @AuraEnabled
    public string pageId { get; set; }
    @AuraEnabled
    public boolean isPublicSite { get; set; }
  }

  public class SRWrapper {
    @AuraEnabled
    public HexaBPM__Service_Request__c srObj { get; set; }
    @AuraEnabled
    public List<OB_EntityDirectorContainerController.AmendmentWrapper> amedWrapLst {
      get;
      set;
    }
    @AuraEnabled
    public List<OB_EntityDirectorContainerController.AmendmentWrapper> amedShrHldrWrapLst {
      get;
      set;
    }
    @AuraEnabled
    public String declarationIndividualText { get; set; }
    @AuraEnabled
    public String declarationCorporateText { get; set; }

    @AuraEnabled
    public String viewSRURL { get; set; }
    @AuraEnabled
    public Boolean isDraft { get; set; }
    @AuraEnabled
    public String sEntityType { get; set; }

    public SRWrapper() {
    }
  }

  public class AmendmentWrapper {
    @AuraEnabled
    public HexaBPM_Amendment__c amedObj { get; set; }
    @AuraEnabled
    public Boolean isIndividual { get; set; }
    @AuraEnabled
    public Boolean isBodyCorporate { get; set; }
    @AuraEnabled
    public String lookupLabel { get; set; }
    @AuraEnabled
    public String displayName { get; set; }
    @AuraEnabled
    public String relatedAmendID { get; set; }
    @AuraEnabled
    public String relatedRelId { get; set; }

    public AmendmentWrapper() {
    }
  }

  public class RequestWrapper {
    @AuraEnabled
    public Id flowId { get; set; }
    @AuraEnabled
    public Id pageId { get; set; }
    @AuraEnabled
    public Id srId { get; set; }
    @AuraEnabled
    public AmendmentWrapper amedWrap { get; set; }
    @AuraEnabled
    public String amendmentID { get; set; }
    @AuraEnabled
    public Map<Id, AccountContactRelation> mapRelatedAccForRelshp { get; set; }
    @AuraEnabled
    public Map<Id, AccountContactRelation> mapRelatedAccForAmed { get; set; }

    public RequestWrapper() {
      mapRelatedAccForRelshp = new Map<Id, AccountContactRelation>();
      mapRelatedAccForAmed = new Map<Id, AccountContactRelation>();
    }
  }

  public class RequestWrapper2 {
    @AuraEnabled
    public string flowId { get; set; }
    @AuraEnabled
    public string pageId { get; set; }
    @AuraEnabled
    public string srId { get; set; }

    @AuraEnabled
    public string srIdParent { get; set; }
    @AuraEnabled
    public string srIdRelated { get; set; }
    @AuraEnabled
    public string srIdChild { get; set; }

    @AuraEnabled
    public String amendmentID { get; set; }
    @AuraEnabled
    public String amendmentIDRel { get; set; }
    @AuraEnabled
    public String amendmentIDParent { get; set; }

    @AuraEnabled
    public Map<Id, AccountContactRelation> mapRelatedAccForRelshp { get; set; }
    @AuraEnabled
    public Map<Id, AccountContactRelation> mapRelatedAccForAmed { get; set; }
    @AuraEnabled
    public Map<Id, AccountContactRelation> mapRelatedAccForAmedRel { get; set; }
    @AuraEnabled
    public Map<Id, AccountContactRelation> mapRelatedAccForAmedParent {
      get;
      set;
    }
    @AuraEnabled
    public Map<Id, AccountContactRelation> mapRelatedAccForAmedChild {
      get;
      set;
    }
    @AuraEnabled
    public Map<Id, AccountContactRelation> mapRelatedAccForAmedDebug {
      get;
      set;
    }

    public RequestWrapper2() {
      mapRelatedAccForRelshp = new Map<Id, AccountContactRelation>();
      mapRelatedAccForAmed = new Map<Id, AccountContactRelation>();
      mapRelatedAccForAmedRel = new Map<Id, AccountContactRelation>();
      mapRelatedAccForAmedParent = new Map<Id, AccountContactRelation>();
      mapRelatedAccForAmedDebug = new Map<Id, AccountContactRelation>();
      mapRelatedAccForAmedChild = new Map<Id, AccountContactRelation>();
      flowId = '';
      pageId = '';
      srId = '';
      srIdParent = '';
      srIdRelated = '';
      srIdChild = '';
      amendmentID = '';
      amendmentIDRel = '';
      amendmentIDParent = '';
    }
  }

  public class ResponseWrapper {
    @AuraEnabled
    public SRWrapper srWrap { get; set; }
    @AuraEnabled
    public HexaBPM__Section_Detail__c ButtonSection { get; set; }
    @AuraEnabled
    public boolean isSRLocked { get; set; }
    @AuraEnabled
    public string communityReviewPageURL { get; set; }
    @AuraEnabled
    public object debugger { get; set; }

    public ResponseWrapper() {
    }
  }
}