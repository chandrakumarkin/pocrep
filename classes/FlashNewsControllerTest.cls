/*
* FlashNewsController
*/
@isTest
public with sharing class FlashNewsControllerTest {
	
	@isTest
    private static void initFlashNewsController(){
    	UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
		insert r;
    	Profile adminProfile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    	//User admin = [SELECT Id, Username, UserRoleId FROM User WHERE Profile.Name = 'System Administrator' AND UserRoleId = :userRole_1.Id LIMIT 1];
    	
    	User admin = new User(Alias = 'tstuassr', Email='testu12r@difc.portal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = adminProfile.Id,
                        Community_User_Role__c='Employee Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser12@testorg.com',UserRoleId = r.Id);
        insert admin;
    	system.runAs(admin){
    		// create account
    	List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insertNewAccounts[0].Legal_Type_of_Entity__c='All;LLP;LTD;';
        insertNewAccounts[0].License_Activity__c='All;LLP;LTD;';
        insertNewAccounts[0].Executive_Company__c=true;
        insertNewAccounts[0].Initial_Allotment_of_Shares_Done__c=true;
        insertNewAccounts[0].Company_Code__c='Du';
        insert insertNewAccounts;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insertNewContacts[0].Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Previous_Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Country_of_Birth__c  = 'Russian Federation';
        insertNewContacts[0].Country__c  = 'Russian Federation';
        insertNewContacts[0].Issued_Country__c  = 'Russian Federation';
        insertNewContacts[0].Gender__c  = 'Male';
        insertNewContacts[0].UID_Number__c  = '9712';
        insertNewContacts[0].MobilePhone  = '+97123576565';
        insertNewContacts[0].Passport_No__c  = '+97123576565';
        insertNewContacts[0].recordTypeId = portalUserId;
        insertNewContacts[0].Role__c = 'Property Services,Listing Services';
        insert insertNewContacts;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=insertNewContacts[0].Id, Community_User_Role__c='Property Services;Employee Services;Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        List<Service_Notification__c> newsList = new List<Service_Notification__c>();
         Service_Notification__c serv = new Service_Notification__c();
         serv.Department__c = 'Company Services';
         serv.Notification_Details__c = 'Test';
         newsList.add(serv);
         
         insert newsList;
         
         test.startTest();
	        system.runAs(objUser){
	        	
	        	FlashNewsController.getFlashNews();
	        }
        
    	}
    }
    
    
    @isTest
    private static void initFlashNewsForNewProfile(){
    	UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
		insert r;
    	Profile adminProfile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    	//User admin = [SELECT Id, Username, UserRoleId FROM User WHERE Profile.Name = 'System Administrator' AND UserRoleId = :userRole_1.Id LIMIT 1];
    	
    	User admin = new User(Alias = 'tstuassr', Email='testu12r@difc.portal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = adminProfile.Id,
                        Community_User_Role__c='Employee Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser12@testorg.com',UserRoleId = r.Id);
        insert admin;
    	system.runAs(admin){
    		// create account
    	List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insertNewAccounts[0].Legal_Type_of_Entity__c='All;LLP;LTD;';
        insertNewAccounts[0].License_Activity__c='All;LLP;LTD;';
        insertNewAccounts[0].Executive_Company__c=true;
        insertNewAccounts[0].Initial_Allotment_of_Shares_Done__c=true;
        insertNewAccounts[0].Company_Code__c='Du';
        insert insertNewAccounts;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insertNewContacts[0].Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Previous_Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Country_of_Birth__c  = 'Russian Federation';
        insertNewContacts[0].Country__c  = 'Russian Federation';
        insertNewContacts[0].Issued_Country__c  = 'Russian Federation';
        insertNewContacts[0].Gender__c  = 'Male';
        insertNewContacts[0].UID_Number__c  = '9712';
        insertNewContacts[0].MobilePhone  = '+97123576565';
        insertNewContacts[0].Passport_No__c  = '+97123576565';
        insertNewContacts[0].recordTypeId = portalUserId;
        insertNewContacts[0].Role__c = 'Property Services,Listing Services';
        insert insertNewContacts;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Contractor Community Plus User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=insertNewContacts[0].Id, Community_User_Role__c='Property Services;Employee Services;Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        List<Service_Notification__c> newsList = new List<Service_Notification__c>();
         Service_Notification__c serv = new Service_Notification__c();
         serv.Department__c = 'Company Services';
         serv.Notification_Details__c = 'Test';
         newsList.add(serv);
         
         insert newsList;
         
         test.startTest();
	        system.runAs(objUser){
	        	
	        	FlashNewsController.getFlashNews();
	        }
        
    	}
    }
}