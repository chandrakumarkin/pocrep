/*********************************************************************************************************************
*  Name     : GsMedicalTestScheduledHandler 
*  Author   : 
*  Purpose  : This is custom code class for GS processes.
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
V1.0          Veera Narayana        19th March 2018          Created               
V1.1          Mudasir               6th-Feb-2020             #7441 - DHA Payment Process - Change for Domestic Helper PR processing
V1.1          Shoaib                6th-mar-2021             Push DHA reference on reschedule.

**********************************************************************************************************************/
global without sharing class GsMedicalTestScheduledHandler {
    
      public static string CurrentUserId {get;set;} 
    
    
     public static string SendMedicalServiceToSAP(Step__c objStep){
         
         GsMedicalTestScheduledHandler.MedicalTestScheduled(objStep.Id);
         
          return 'Success';
         
     }

Private static string  decryptedSAPPassWrd = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456' ;

@future(callout=true)
public static void MedicalTestScheduled(string StepId){

       
       Step__c objStep = [select Id,Name,SAP_Seq__c,Step_Status__c,OwnerId,Owner.Name,LastModifiedBy.SAP_User_Id__c,is_Rescheduled__c,Application_Reference__c,
                                    SAP_AMAIL__c,SAP_BANFN__c,SAP_BIOMT__c,SAP_BNFPO__c,SAP_CMAIL__c,SAP_CRMAC__c,SAP_EACTV__c,SAP_EMAIL__c,SAP_ERDAT__c,SAP_ERNAM__c,
                                    SAP_ERTIM__c,SAP_ETIME__c,SAP_FINES__c,SAP_FMAIL__c,SAP_GSDAT__c,SAP_GSTIM__c,SAP_IDDES__c,SAP_IDNUM__c,
                                    SAP_IDTYP__c,SAP_MDATE__c,SAP_MEDIC__c,SAP_MEDRT__c,SAP_MEDST__c,SAP_MFDAT__c,SAP_MFSTM__c,SAP_NMAIL__c,SAP_POSNR__c,SAP_RESCH__c,
                                    SAP_RMATN__c,SAP_STIME__c,SAP_TMAIL__c,SAP_UNQNO__c,SAP_ACRES__c,ID_End_Date__c,ID_Start_Date__c,SAP_MATNR__c,
                                    SR__c,SR__r.SAP_Unique_No__c,SR__r.SAP_PR_No__c,SR__r.SAP_SGUID__c,SR__r.SAP_MATNR__c,SR__r.Is_Pending__c,
                                    SR__r.SAP_UMATN__c,SR__r.SAP_OSGUID__c,SR__r.Name,DNRD_Receipt_No__c,SAP_OSGUID__c,SAP_UMATN__c,
                                    Step_Template__c,Step_Template__r.Name,Step_Id__c,No_PR__c,status__r.name,
                                    SAP_DOCDL__c,SAP_DOCRC__c,SAP_DOCCO__c,SAP_DOCCR__c,SAP_CTOTL__c,SAP_CTEXT__c,                                  
                                    (select Id,Appointment_StartDate_Time__c,Appointment_EndDate_Time__c,Is_Client_Requested_for_Reschedule__c from Appointments__r order by LastModifiedDate desc limit 1)
                                    from Step__c where Id = : StepId limit 1];
       
       
        string result = 'Success';
        
         String Month = string.valueOf(system.today().month()).length()==1?'0'+system.today().month():''+system.today().month();
        String Day = string.valueOf(system.today().day()).length()==1?'0'+system.today().day():''+system.today().day();
        String Hours = string.valueOf(system.now().hour()).length()==1?'0'+system.now().hour():''+system.now().hour();
        String Minitues = string.valueOf(system.now().minute()).length()==1?'0'+system.now().minute():''+system.now().minute();
        String Seconds = string.valueOf(system.now().second()).length()==1?'0'+system.now().second():''+system.now().second();
        
        SAPGSWebServices.ZSF_S_GS_ACTIVITY item;
        list<SAPGSWebServices.ZSF_S_GS_ACTIVITY> items = new list<SAPGSWebServices.ZSF_S_GS_ACTIVITY>();
        item = new SAPGSWebServices.ZSF_S_GS_ACTIVITY();    
        item.WSTYP = 'ACTV';
        item.RENUM = objStep.SR__r.Name;//string.valueOf(objStep.SR__r.SAP_SGUID__c).substring(0,15);
        item.SGUID = objStep.SR__r.SAP_SGUID__c; 
        item.OSGUID = objStep.SAP_OSGUID__c;
        item.ORENUM = objStep.SR__r.Name;
        item.UMATN = objStep.SAP_UMATN__c;
        
        if((item.OSGUID == null || item.OSGUID == '') && objStep.SR__r.SAP_OSGUID__c != null && objStep.SAP_OSGUID__c != objStep.SR__r.SAP_OSGUID__c)
            item.OSGUID = objStep.SR__r.SAP_OSGUID__c;
        if(item.UMATN == null || item.UMATN == '') // if SR is Upgraded when the Step is open UMATN wouldnot update on Step
            item.UMATN = objStep.SR__r.SAP_UMATN__c;
        
        item.BUKRS = '5000';
        item.GSDAT = GsPendingInfo.SAPHANADateFormat(objStep.SAP_GSDAT__c);//v1.6
        item.GSTIM = GsPendingInfo.SAPHANATimeFormat(objStep.SAP_GSTIM__c);//v1.6
        item.UNQNO = objStep.SAP_UNQNO__c;
        item.MATNR = objStep.SAP_MATNR__c;
        item.POSNR = objStep.SAP_POSNR__c;
        item.SEQNR = objStep.SAP_Seq__c;
        item.ACTVT = objStep.Step_Template__r.Name;
        item.CDATE = GsPendingInfo.SAPDateFormat(system.now());
        item.CTIME = GsPendingInfo.SAPTimeFormat(system.now());       
        item.ACTIN = 'CHG'; 
        item.ACRES = objStep.SAP_ACRES__c;
        item.AMAIL = objStep.SAP_AMAIL__c;
        item.EMAIL = objStep.SAP_EMAIL__c;
        item.NMAIL = objStep.SAP_NMAIL__c;
        item.CMAIL = objStep.SAP_CMAIL__c;
        item.FMAIL = objStep.SAP_FMAIL__c;
        item.TMAIL = objStep.SAP_TMAIL__c;
        item.MEDIC = objStep.SAP_MEDIC__c;
        item.BIOMT = objStep.SAP_BIOMT__c;
        item.CRMAC = objStep.SAP_CRMAC__c;
        item.RMATN = objStep.SAP_RMATN__c;
        item.RESCH = (objStep.status__r.name == 'Re-Scheduled')?'X':'';        
        
        
        item.AEDAT = system.today().year()+'-'+Month+'-'+Day;
        item.AETIM = Hours+':'+Minitues+':'+Seconds;        
        item.AENAM = objStep.LastModifiedBy.SAP_User_Id__c;
        
        item.MEDST = (objStep.Application_Reference__c != '' || objStep.Application_Reference__c !=null)? 'A':'';
        item.BNFPO = objStep.SAP_BNFPO__c; 
        for(appointment__c objApp : objStep.appointments__r) {        
        
        item.MDRCL = (objApp.Is_Client_Requested_for_Reschedule__c== true)?'X':' ';        
        //V1.1 - Start
        system.debug('Test------'+objStep.status__r.name+'--DNRD Receipt Number ---'+objStep.DNRD_Receipt_No__c);
       // if(objStep.status__r.name == 'Scheduled'){
            item.PRMRK = objStep.Application_Reference__c;
        //}
        system.debug('Test------'+item.PRMRK);
        //item.BNFPO = objStep.DNRD_Receipt_No__c; 
        //V1.1 - END
          if(objApp.Is_Client_Requested_for_Reschedule__c == true || item.RESCH == 'X'){
              item.MDATE = GsPendingInfo.SAPDateFormat(objApp.Appointment_StartDate_Time__c);
              item.STIME = GsPendingInfo.SAPTimeFormat(objApp.Appointment_StartDate_Time__c);
              item.ETIME = GsPendingInfo.SAPTimeFormat(objApp.Appointment_EndDate_Time__c);
          }        
          else{
            item.MDATE = GsPendingInfo.SAPDateFormat(objApp.Appointment_StartDate_Time__c);
            item.STIME = GsPendingInfo.SAPTimeFormat(objApp.Appointment_StartDate_Time__c); 
            item.MFDAT = GsPendingInfo.SAPDateFormat(objApp.Appointment_StartDate_Time__c);
            item.MFSTM = GsPendingInfo.SAPTimeFormat(objApp.Appointment_StartDate_Time__c); 
            item.ETIME = GsPendingInfo.SAPTimeFormat(objApp.Appointment_EndDate_Time__c);
          }
        }
        
        items.add(item);
        
        if(items != null && !items.isEmpty()){
            SAPGSWebServices.ZSF_TT_GS_ACTIVITY objActivityData = new SAPGSWebServices.ZSF_TT_GS_ACTIVITY();
            objActivityData.item = items;
            
            SAPGSWebServices.serv_actv objSFData = new SAPGSWebServices.serv_actv();        
            objSFData.timeout_x = 120000;
            objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
            objSFData.inputHttpHeaders_x= new Map<String, String>();
         // Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c);  Commented for V1.5
            Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+GsMedicalTestScheduledHandler.decryptedSAPPassWrd); 
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
            
            system.debug('request $$$ : '+objActivityData);
            
            SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element response = objSFData.Z_SF_GS_ACTIVITY(objActivityData,null);
            
            list<Log__c> lstLogs = new list<Log__c>();
            
            system.debug('response $$$ : '+response);
            
            //#9102 - Introduce the new functionality where we will capture the web service load
            /*
            String requestStringValue   = items != NULL && items.size() > 0 ? (String)JSON.serialize(items) : 'NULL';
            String responseStringValue  = response != NULL ? (String)JSON.serialize(response) : 'NULL';
            String recordIdsValue = '';
            DIFCApexCodeUtility.logWebServiceLoad('GsMedicalTestScheduledHandler' , StepId ,'GS' ,requestStringValue,responseStringValue, StepId);
            */
            //#9102- END 
            
          if(response.EX_GS_SERV_OUT != null && response.EX_GS_SERV_OUT.item != null){
                for(SAPGSWebServices.ZSF_S_GS_SERV_OP objStepRes : response.EX_GS_SERV_OUT.item){
                    if(objStepRes.ACCTP != 'P'){
                        result = 'Fail - '+objStepRes.SFMSG;
                        Log__c objLog = new Log__c();objLog.Type__c = 'Medical Fitness Test schedule is failed for SR# '+objStepRes.RENUM;objLog.Description__c = objStepRes.SFMSG;lstLogs.add(objLog);
                    }
                }
            }
           if(!lstLogs.isEmpty())
                insert lstLogs; 
             
        }
       
    }
}