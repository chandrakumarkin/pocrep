@isTest(seeAllData=false)

public class GSMedicalStatusUpdateTest{

  public static testMethod void TestMethod1(){
  
  
  
     service_request__c sr = Testutility.TestInsertServiceRequest();    
     
     
     Step_Template__c stepTemplate = new Step_Template__c();
     stepTemplate.name = 'Medical Fitness Test Completed';
     stepTemplate.code__c = 'Medical Fitness Test Completed';
     stepTemplate.Step_RecordType_API_Name__c='General';
     insert stepTemplate;
     
     
   
     
      Step__c objStep1 = new Step__c();
      objStep1.SR__c = sr.Id;
      objStep1.Step_Template__c = stepTemplate.id ;
      //objStep1.status__c = srs.id;
      objstep1.Step_Notes__c ='test1';
      insert objStep1;
      
       List<id> ids = new List<id>{objStep1.id};
       
     SR_Doc__c srDoc = new SR_Doc__c();
     srDoc.Service_Request__c =sr.id;    
     srDoc.Step__c = objStep1.id;
     srDoc.Document_Description_External__c='Health Insurance Certificate';
     srDoc.Is_Not_Required__c = true;
     srDoc.Name ='Health Insurance Certificate';
     srDoc.Requirement__c ='Copy';
     srDoc.SAP_step_Seq__c ='0020';
     insert srDoc;
  
   
    GSMedicalStatusUpdate.createLineManagerForRejection(ids); 
    
  
  }
  
    public static testMethod void TestMethod2(){
  
  
  
     service_request__c sr = Testutility.TestInsertServiceRequest();    
     
     
     Step_Template__c stepTemplate = new Step_Template__c();
     stepTemplate.name = 'Medical Fitness Test Completed';
     stepTemplate.code__c = 'Medical Fitness Test Completed';
     stepTemplate.Step_RecordType_API_Name__c='General';
     insert stepTemplate;
     
     
   
     
      Step__c objStep1 = new Step__c();
      objStep1.SR__c = sr.Id;
      objStep1.Step_Template__c = stepTemplate.id ;
      //objStep1.status__c = srs.id;
      objstep1.Step_Notes__c ='test1';
      insert objStep1;
      
       List<id> ids = new List<id>{objStep1.id};
       
     SR_Doc__c srDoc = new SR_Doc__c();
     srDoc.Service_Request__c =sr.id;    
     srDoc.Step__c = objStep1.id;
     srDoc.Document_Description_External__c='Health Insurance Certificate';
     srDoc.Is_Not_Required__c = true;
     srDoc.Name ='Health Insurance Certificate';
     srDoc.Requirement__c ='Copy';
     srDoc.SAP_step_Seq__c ='0020';
     srDoc.status__c ='Uploaded';
     insert srDoc;
  
   
    GSMedicalStatusUpdate.createLineManagerForRejection(ids); 
    
  
  }

}