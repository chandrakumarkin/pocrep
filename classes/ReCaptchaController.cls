/*
Author      :   Ravi/Anvishka
Description :   This class will be controller for the Listing website Property Details Page
--------------------------------------------------------------------------------------------------------------------------

Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No	Date		Updated By    	Description
--------------------------------------------------------------------------------------------------------------------------      
V1.0	28-07-2016	Claude			Added User_Action__c as part of queried fields; modified logic in displaying unit       
*/
public class ReCaptchaController {
	
	private static String baseUrl = 'https://www.google.com/recaptcha/api/siteverify';
	public String message {get; set;}
    public String name {get; set;}
    public String email {get; set;}
    
    public String shareMessage {get; set;}
    public String shareName {get; set;}
    public String shareEmail {get; set;}
    public String shareEmailTo {get; set;}
    private String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
	// The keys you get by signing up for reCAPTCHA for your domain
	private static String secret = '6LeRBBETAAAAAC67vzPZUYKp3qHYVH9gVEf_QBr8';
	public string buildingJSONString {get;set;}
	public string JSONString {get;set;}
	public Inquiry__c obbInquiry {get;set;}
	
	
	public ReCaptchaController(){
		string lId = Apexpages.currentPage().getParameters().get('Id');
		PropertyDetails objPD;
		
		obbInquiry = new Inquiry__c(Listing__c=lId);
		obbInquiry.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Interest').getRecordTypeId();
		
		/* V1.0 - Claude - Added User_Action__c field */
		for(Listing__c objL : [select id,Name,User_Action__c,Building_Name__c,Unit__c,Unit__r.Building__r.Building_on_Website__c,Start_Date__c,End_Date__c,Listing_Type__c,Price__c,Sq_Feet__c,No_of_Bedrooms__c,No_of_Pax__c,Property_Type__c,
						Featured__c,Address_Details__c,Agent_Details_if_any__c,Amenities__c,Cage_No__c,Leasing_Type__c,Company_Name__c,Company_Profile__c,Food_and_Beverage__c,Listing_Description__c,Listing_Title__c,
						Location__c,No_of_Days__c,Website__c,Work_Phone__c,Unit__r.Name,Status__c,Unit__r.Building__r.Building_Coordinates__Latitude__s,Unit__r.Building__r.Building_Coordinates__Longitude__s,
						Contact_Email__c,Contact_Phone__c,Contact_Time_Zone__c,Unit_Delivery_Condition__c,First_Name__c,Last_Name__c,Street_Address__c,Created_by_DIFC_Backend__c,
						(select Id,Name,Doc_ID__c,Document_Master__r.Code__c from SR_Docs__r where Listing_Doc_Status__c = 'Active' AND Doc_ID__c != null)
						from Listing__c where Id=:lId]){
                            
			if(objL.Listing_Type__c == 'Sale'|| (objL.Created_by_DIFC_Backend__c == true && (objL.Property_Type__c == 'Office' || objL.Property_Type__c == 'Residential'))) {
                obbInquiry.Email__c = 'officelisting@difc.ae';   
                //obbInquiry.Email__c = 'anviksha.k@gmail.com';
            } else if (objL.Created_by_DIFC_Backend__c == true && (objL.Property_Type__c == 'Retail' || objL.Property_Type__c == 'Storage')) {
                obbInquiry.Email__c = 'retaillisting@difc.ae';
                //obbInquiry.Email__c = 'anviksha.k@gmail.com';
            } else if (objL.Created_by_DIFC_Backend__c == true && objL.Property_Type__c == 'Data Centre (Co-Location Service)') {
                obbInquiry.Email__c = 'datacentrelisting@difc.ae';
                //obbInquiry.Email__c = 'anviksha.k@gmail.com';
            } else if (objL.Created_by_DIFC_Backend__c == true && objL.Property_Type__c == 'Business Centre') {
                obbInquiry.Email__c = 'businesscentrelisting@difc.ae';
                //obbInquiry.Email__c = 'anviksha.k@gmail.com';
            }
            else
				obbInquiry.Email__c = objL.Contact_Email__c;
            obbInquiry.Building_Name__c = objL.Building_Name__c;
            obbInquiry.Unit_No__c = objL.Unit__r.Name;
            
			objPD = new PropertyDetails();
			objPD.AddressDetails = objL.Address_Details__c;
			objPD.AgentDetails = objL.Agent_Details_if_any__c;
			objPD.Amenities = objL.Amenities__c;
			objPD.BuildingName = objL.Unit__r.Building__r.Building_on_Website__c != null ? objL.Unit__r.Building__r.Building_on_Website__c : objL.Building_Name__c;
            objPD.CageNo = objL.Cage_No__c != null ? objL.Cage_No__c : '';
			objPD.CompanyName = objL.Company_Name__c;
			objPD.CompanyProfile = objL.Company_Profile__c;
			objPD.EndDate = objL.End_Date__c != null ? objL.End_Date__c.format() : '';
			objPD.FnB = objL.Food_and_Beverage__c+'';
			objPD.IsFeatured = objL.Featured__c+'';
            objPD.LeasingType = objL.Leasing_Type__c;                
			objPD.ListingDescription = objL.Listing_Description__c;
			objPD.ListingTitle = objL.Listing_Title__c;
			objPD.ListingType = objL.Listing_Type__c;
			objPD.Location = objL.Location__c;
			objPD.NoOfBedRooms = objL.No_of_Bedrooms__c;
			objPD.NoOfDays = objL.No_of_Days__c != null ? objL.No_of_Days__c+'' : '';
			objPD.NoOfPAX = objL.No_of_Pax__c != null ? objL.No_of_Pax__c+'' : '';
			objPD.Price = objL.Price__c != null ? objL.Price__c+'' : '';
			objPD.PropertyType = objL.Property_Type__c;
			objPD.SqrtFeet = objL.Sq_Feet__c != null ? objL.Sq_Feet__c+'' : '';
			objPD.StartDate = objL.Start_Date__c != null ? objL.Start_Date__c.format() : '';
			objPD.Status = objL.Status__c;
			objPD.Unit = objL.Unit__r.Name;
			objPD.WebSite = objL.Website__c;
			objPD.WorkPhone = objL.Work_Phone__c;
			objPD.ContactEmail = objL.Contact_Email__c;
			objPD.ContactPhone = objL.Contact_Phone__c;
			objPD.ContactTimezone = objL.Contact_Time_Zone__c;
			
			/* V1.0 - Claude - Added new field */
			objPd.UserAction = objL.User_Action__c;
			
			objPD.Latitude = objL.Unit__r.Building__r.Building_Coordinates__Latitude__s;
			objPD.Longitude = objL.Unit__r.Building__r.Building_Coordinates__Longitude__s;
			
			objPD.UnitDeliveryCondition = objL.Unit_Delivery_Condition__c;
			objPD.FirstName = objL.First_Name__c;
			objPD.LastName = objL.Last_Name__c;
			objPD.StreetAddress = objL.Street_Address__c;
			objPD.CreatedbyDIFCBackend = objL.Created_by_DIFC_Backend__c+'';
			for(SR_Doc__c objDoc : objL.SR_Docs__r){
				string dName = objDoc.Document_Master__r.Code__c;
				string dURL = Site.getPathPrefix()+'/servlet/servlet.FileDownload?file='+objDoc.Doc_ID__c;
				if(dName == 'Photograph 1')
					objPD.Photo1 = dURL;
				else if(dName == 'Photograph 2')
					objPD.Photo2 = dURL;
				else if(dName == 'Photograph 3')
					objPD.Photo3 = dURL;
				else if(dName == 'Photograph 4')
					objPD.Photo4 = dURL;
				else if(dName == 'Photograph 5')
					objPD.Photo5 = dURL;
				else if(dName == 'Photograph 6')
					objPD.Photo6 = dURL;
				else if(dName == 'Photograph 7')
					objPD.Photo7 = dURL;
				else if(dName == 'Photograph 8')
					objPD.Photo8 = dURL;
				else if(dName == 'Photograph 9')
					objPD.Photo9 = dURL;
				else if(dName == 'Photograph 10')
					objPD.Photo10 = dURL;
				else if(dName == 'Fit-Out Manual')
					objPD.FitoutManual = dURL;
				else if(dName == 'Tenant Manual')
					objPD.TenantManual = dURL;
				else if(dName == 'Floor Plan')
					objPD.FloorPlan = dURL;
			}
			
		}
		if(objPD != null)
			JSONString = JSON.serialize(objPD);
        
        
        list<MapViewDetails> lstMVD = new list<MapViewDetails>();
		MapViewDetails objMVD;
		map<Id,Integer> mapLCount = new map<Id,Integer>();
		for(Listing__c obj : [select Id,Unit__c,Unit__r.Building__c from Listing__c where Unit__r.Building__c != null AND Status__c = 'Active']){
			Integer i = mapLCount.containsKey(obj.Unit__r.Building__c) ? (mapLCount.get(obj.Unit__r.Building__c)+1) : 1;
			mapLCount.put(obj.Unit__r.Building__c,i);
		}
		for(Building__c objB : [select Id,Name,Building_No__c,Building_on_Website__c from Building__c where Building_No__c != null]){
			objMVD = new MapViewDetails();
			objMVD.BuildingName = objB.Name;
			objMVD.BuildingNo = objB.Building_No__c;
            objMVD.BuildingOnWebsite = objB.Building_on_Website__c;
			objMVD.AvailableUnits = mapLCount.containsKey(objB.Id) ? (mapLCount.get(objB.Id)+'') : '0';
			lstMVD.add(objMVD);
		}
		if(!lstMVD.isEmpty())
			buildingJSONString = JSON.serialize(lstMVD);
	}
	
	public String sitekey {
		get { return '6LeRBBETAAAAAHDagNGgb_LXO_4MpbOIK0oT55co'; }
	}
	public String response  { 
		get { return ApexPages.currentPage().getParameters().get('g-recaptcha-response'); }
	}
	
	// this method is called when the button is clicked
	public PageReference doVerify () {
		String responseBody = makeRequest(baseUrl,
				'secret=' + secret +
				'&response='+ response
		);
		String success = getValueFromJson(responseBody, 'success');
        
        Pattern MyPattern = Pattern.compile(emailRegex);
    	Matcher MyMatcher = MyPattern.matcher(email);
        if(!MyMatcher.matches()) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check the email id');
            ApexPages.addMessage(errorMsg);
        }
		else if(success.equalsIgnoreCase('true') && message != null && name != null && email != null && !message.equalsIgnoreCase('')
          && !name.equalsIgnoreCase('') && !email.equalsIgnoreCase('')){
			ApexPages.Message successMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Message sent');
            obbInquiry.Sender_Name__c = name;
            obbInquiry.Message__c = message;
            obbInquiry.Sender_Email__c = email;
            upsert obbInquiry;
            ApexPages.addMessage(successMsg);
		}else if(success.equalsIgnoreCase('false')){
			ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the captcha');
            ApexPages.addMessage(errorMsg);
        } else {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter all the fields');
            ApexPages.addMessage(errorMsg);
        }
		return null;
	}

	/**
	 * Make request to verify captcha
	 * @return 		response message from google
	 */
	private static String makeRequest(string url, string body)  {
		HttpResponse response = null;
		HttpRequest req = new HttpRequest();   
		req.setEndpoint(url);
		req.setMethod('POST');
		req.setBody (body);
		
		try {
			Http http = new Http();
			response = http.send(req);
			return response.getBody();
		} catch(System.Exception e) {
			System.debug('ERROR: ' + e);
		}
		return test.isRunningTest() ? '{"success":true}' : '{"success":false}';
	}   
	
	/**
	 * to get value of the given json string
	 * @params		
	 *	- strJson		json string given
	 *	- field			json key to get the value from
	 * @return			string value
	 */
	public static string getValueFromJson ( String strJson, String field ){
		JSONParser parser = JSON.createParser(strJson);
		while (parser.nextToken() != null) {
			if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
				if(parser.getText() == field){
					// Get the value.
					parser.nextToken();
					return parser.getText();
				}
			}
		}
		return null;
	}
	
	public class PropertyDetails{
		public string AddressDetails {get;set;}
		public string AgentDetails {get;set;}
		public string Amenities {get;set;}
		public string BuildingName {get;set;}
        public string CageNo {get;set;}
		public string CompanyName {get;set;}
		public string CompanyProfile {get;set;}
		public string EndDate {get;set;}
		public string IsFeatured {get;set;}
		public string FnB {get;set;}
        public string LeasingType {get;set;}
		public string ListingDescription {get;set;}
		public string ListingTitle {get;set;}
		public string ListingType {get;set;}
		public string Location {get;set;}
		public string NoOfDays {get;set;}
		public string NoOfBedRooms {get;set;}
		public string NoOfPAX {get;set;}
		public string Price {get;set;}
		public string PropertyType {get;set;}
		public string SqrtFeet {get;set;}
		public string StartDate {get;set;}
		public string Status {get;set;}
		public string Unit {get;set;}
		public string WebSite {get;set;}
		public string WorkPhone {get;set;}
		public string ContactEmail {get;set;}
		public string ContactPhone {get;set;}
		public string ContactTimezone {get;set;}
		
		public string UnitDeliveryCondition {get;set;}
		public string FirstName {get;set;}
		public string LastName {get;set;}
		public string StreetAddress {get;set;}
		public string CreatedbyDIFCBackend {get;set;}
		
		public decimal Longitude {get;set;}
		public decimal Latitude {get;set;}
		
		public string Photo1 {get;set;}
		public string Photo2 {get;set;}
		public string Photo3 {get;set;}
		public string Photo4 {get;set;}
		public string Photo5 {get;set;}
		public string Photo6 {get;set;}
		public string Photo7 {get;set;}
		public string Photo8 {get;set;}
		public string Photo9 {get;set;}
		public string Photo10 {get;set;}
		public string FitoutManual {get;set;}
		public string TenantManual {get;set;}
		public string ClientOffer {get;set;}
		public string FloorPlan {get;set;}
		
		public string UserAction	{get; set;}
		
	}
	public class MapViewDetails{
		public string BuildingName {get;set;}
		public string BuildingNo {get;set;}
		public string AvailableUnits {get;set;}
        public string BuildingOnWebsite {get;set;}
	}
}