/******************************************************************************************
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Description : Process Builder handler for Relationships for CRM
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    23-06-2016  Claude        Created
****************************************************************************************************************/
public class CRM_RelationshipHandler {
    
    private static Boolean hasRun = false;
    
    private static Boolean isBatch = false;
    
    private static Map<Id,Account> accountsTobeUpdated;
    
    @InvocableMethod(label='Update Affiliated Relationstips' description='Updates the number of affiliated accounts for a specific record')
    public static void updateAffiliatedRelationships(List<Id> accountIds){

        /* START OF LOGIC FOR UPDATING AFFILIATED RELATIONSHIPS COUNTER */
        
        if(!hasRun){
            
            hasRun = true;
            
            accountsTobeUpdated = new Map<Id,Account>();
            
            isBatch = accountIds.size() > 1;
            
            updateRelationships(accountIds);
            
            /* If it's a single update, update the deactivated account */
            if(!isBatch){
                
                // START OF UPDATING DEACTIVATED Relationships
                /* Do a separate process for the deactivated relationsips */
                
                /* Query the list of deactivated relationships */
                List<Relationship__c> lstRelationships3 = [SELECT ID, Subject_Account__c, Object_Account__C FROM Relationship__c WHERE Subject_Account__c IN :accountIds AND Active__c = FALSE AND Relationship_Type__c = 'Has Affiliates with Same Management'];
                
                Set<Id> deactivatedAccounts = new Set<Id>();
                
                for(Relationship__c r : lstRelationships3){ 
                    
                    deactivatedAccounts.add(r.Object_Account__C);
                }
                
                updateRelationships(new List<Id>(deactivatedAccounts));
                // END OF UPDATING DEACTIVATED Relationships
            } else {
                // TODO: map each deactivated account by their original parent
                
            }
            
            ID jobID = System.enqueueJob(new CRM_batchedRelationshipHandler(accountsTobeUpdated.values())); 
        }
        
    }
    
    private static void updateRelationships(List<Id> accountIds){
        
        Set<Id> accountIdsUnique = new Set<ID>(accountIds);
        
        /* Query the subject relationships; this will also query the grand parents */
        List<Relationship__c> lstRelationships = [SELECT ID, Subject_Account__c, Object_Account__C FROM Relationship__c WHERE Subject_Account__c IN :accountIds AND Active__c = TRUE AND Relationship_Type__c = 'Has Affiliates with Same Management'];
        
        /* Query the parents */
        List<Relationship__c> lstRelationships2 = [SELECT ID, Object_Account__C, Subject_Account__c FROM Relationship__c WHERE Object_Account__C IN :accountIds AND Active__c = TRUE AND Relationship_Type__c = 'Has Affiliates with Same Management'];
        
        /* Get the children */
        Map<id,Set<Id>> mapOfChildren = new Map<id,Set<Id>>();
        
        for(Relationship__c r : lstRelationships){ 
            
            Set<Id> childrenIds = mapOfChildren.containsKey(r.Subject_Account__c) ? mapOfChildren.get(r.Subject_Account__c) : new Set<Id>();
            
            childrenIds.add(r.Object_Account__C);
            
            mapOfChildren.put(r.Subject_Account__c,childrenIds);
            
        }
        
        /* Get the accounts' parents */
        Map<id,Set<Id>> mapOfParents = new Map<id,Set<Id>>();
        
        for(Relationship__c r : lstRelationships2){ 
            
            Set<Id> parentIds = mapOfParents.containsKey(r.Object_Account__C) ? mapOfParents.get(r.Object_Account__C) : new Set<Id>();
            
            parentIds.add(r.Subject_Account__c);
            
            mapOfParents.put(r.Object_Account__C,parentIds);
            
        }
        
        /* Start updating the count of the IDs passed */
        for(Id i : accountIds){
            
            Decimal totalCount = 1;
            
            /* Count the children */
            if(mapOfChildren.containskey(i)){
                totalCount += mapOfChildren.get(i).size();
            }
            
            System.debug('Child Count >> ' + totalCount);
            
            /* Count the parents */
            if(mapOfParents.containskey(i)){
                
                System.debug('Has Parents >> ' + mapOfParents.get(i).size());
                
                totalCount += mapOfParents.get(i).size();
                
            } else {
                
                if(mapOfChildren.containskey(i)){
                    
                    System.debug('Has Grandchildren. >> ' + mapOfChildren.get(i).size() );
                    
                    for(ID i2 : mapOfChildren.get(i)){
                        
                        if(mapOfChildren.containsKey(i2)){
                            totalCount += mapOfChildren.get(i2).size();
                        } 
                    }
                    
                } else {
                    
                    System.debug('No Parents.');
                    
                }
                
            }
            
            if(mapOfParents.containskey(i)){
                System.debug(i + ' >> Parents >> ' + mapOfParents.get(i));
            }
            
            System.debug('Parent Count >> ' + totalCount);
            
            /* Count the children */
            if(mapOfChildren.containskey(i)){
                
                for(Id i2 : mapOfChildren.get(i)){
                    
                    if(!accountsTobeUpdated.containsKey(i2)){
                        
                        Decimal offSet = isBatch && (mapOfParents.containskey(i2) && mapOfParents.containskey(i)) ? mapOfParents.get(i).size() : 0;
                        
                        accountsTobeUpdated.put(i2,new Account(Id = i2, No_of_Affiliated_Accounts__c = totalCount - offSet));
                    }
                }
                
            }
            
            /* Count the parents */
            if(mapOfParents.containskey(i)){
                
                for(Id i2 : mapOfParents.get(i)){
                    
                    if(!accountsTobeUpdated.containsKey(i2)){
                        
                        Decimal offSet = isBatch && (mapOfParents.containskey(i2) && mapOfParents.containskey(i)) ? mapOfParents.get(i).size() : 0;
                        
                        accountsTobeUpdated.put(i2,new Account(Id = i2, No_of_Affiliated_Accounts__c = totalCount - offSet));
                    }
                }
                
            } 
            
            totalCount = (!mapOfParents.containsKey(i) && !mapOfChildren.containsKey(i)) ? 0 : totalCount;
            
            Decimal offSet = isBatch && (mapOfParents.containskey(i) && mapOfParents.containskey(i)) ? mapOfParents.get(i).size() : 0;
            
            System.debug('Total Count >> ' + (totalCount - offSet));
            
            if(!accountsTobeUpdated.containsKey(i)){
                accountsTobeUpdated.put(i,new Account(Id = i, No_of_Affiliated_Accounts__c = totalCount - offSet));
            } else {
                accountsTobeUpdated.get(i).No_of_Affiliated_Accounts__c = totalCount - offSet;
            }
        }
    }
    
}