@istest
public class TestBatch_UpdateActiveLease {
    
    static testMethod void testActiveLease(){
        Account acc = new Account();
        acc.name= 'test';
        acc.ROC_Status__c='Pending Dissolution';
        insert acc;
        
        Lease__c  lse = new Lease__c();     
        lse.Account__c = acc.id;
        lse.Lease_Types__c = 'Leased Property Registration';
        lse.Start_date__c = system.Today();
        lse.End_Date__c = system.Today() +10;
        lse.Status__c = 'Active';
        lse.Square_Feet__c = '100000';
        lse.Type__c= 'Leased';
        insert lse;
        
        Test.startTest();
        Database.executeBatch(new Batch_UpdateActiveLease());
        Test.stopTest();
    }
    
    
}