@isTest
public class VisaNomineeDetailCtrlExtTest {

    
    static testMethod void TestMethod_1(){
        PageReference pgref = Page.VisaNomineeDetails;
        Test.setCurrentPageReference(pgref); 
      
        Long_Term_Visa_Nomination__c ObjFeed = new Long_Term_Visa_Nomination__c();
        ApexPages.StandardController sc = new ApexPages.standardController(ObjFeed);
        VisaNomineeDetailCtrlExt VisaNomineeDetailCtrlExtobj = new VisaNomineeDetailCtrlExt(sc);

        //Long_Term_Visa_Nomination__c LongTermVisaNominationObj = new Long_Term_Visa_Nomination__c();
        //insert LongTermVisaNominationObj ;
        
        Attachment AttachmentObj = new Attachment();
        
        AttachmentObj.body = Blob.valueOf('123456789');
        AttachmentObj.Name = '123.jpg';
        
        
        Attachment AttachmentObj1 = new Attachment();
        
        AttachmentObj1.body = Blob.valueOf('123456789');
        AttachmentObj1.Name = '123.jpg';
        
        VisaNomineeDetailCtrlExtobj.getAttachmentObj();
        VisaNomineeDetailCtrlExtobj.getConsentFormObj();
        
        VisaNomineeDetailCtrlExtobj.AttachmentObj = AttachmentObj;
        VisaNomineeDetailCtrlExtobj.ConsentFormObj = AttachmentObj1;

        VisaNomineeDetailCtrlExtobj.SubmitForm();
        VisaNomineeDetailCtrlExtobj.getAttachmentObj();
        VisaNomineeDetailCtrlExtobj.getConsentFormObj();
      
    }
    
}