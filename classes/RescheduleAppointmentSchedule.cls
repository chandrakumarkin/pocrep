global class RescheduleAppointmentSchedule implements Schedulable {

    global void execute(SchedulableContext sc) {
        RescheduleAppointmentBatch RescheduleAppointmentBatchObj = new RescheduleAppointmentBatch();
        database.executebatch( RescheduleAppointmentBatchObj, 1 );
    }

}