/*******************************************************************************************************
 *  Name        : GS_POST_TO_SAP
 *  Author      : Mudasir
 *  Date        : 28th January 2019     
 *  Purpose     : This class is created to provide the re push functionality to the GS team  
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date                       Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    28th January 2019          Mudasir       Created this class
*********************************************************************************************************/

global class GS_POST_TO_SAP {
  /***********************************************************************************************************
    * Accept the step Id and call the necessary method to try to post the record to SAP
    ************************************************************************************************************/
     webservice static string  postTheCurrentRecordToSAP(String stepId, String srStepId){
         try{
             //Get the business rule which has a condition with step_Status__c as approved or verified
            system.debug('### stepId '+stepId);
            system.debug('### srStepId '+srStepId);
            List<Condition__c> ConditionRules = [Select id,Business_Rule__c from condition__c where Business_Rule__r.SR_Steps__c =:srStepId AND Field_Name__c ='Step_Status__c' AND (Value__c ='Approved' OR Value__c='Verified')];
            system.debug('### ConditionRules '+ConditionRules);
        //Get the action associated to that business rule and check the values of Class Name and Method Name
            Action__c actionRec = [Select id,name,Class_name__c,Custom_Method__c from Action__c where Class_name__c != Null AND Custom_Method__c != Null AND Business_Rule__c =:ConditionRules[0].Business_Rule__c];
            system.debug('### actionRec '+actionRec);
            //Now call the necessary method.
            //Fetch step record 
            Step__c stepRec = [SELECT Id ,sr__r.DIFC_License_No__c,sr__r.Type_of_Request__c,sr__r.Monthly_Salary_AED__c,sr__r.Domestic_Helper__c,sr__r.Relation__c,sr__r.Sponsor__c,sr__r.ApplicantId_hidden__c,sr__r.PO_BOX__c,sr__r.Nationality__c,sr__r.Phone_No_Outside_UAE__c,sr__r.City_Town__c,sr__r.Identical_Business_Domicile__c,sr__r.Work_Phone__c,sr__r.Mobile_Number__c,sr__r.Residence_Phone_No__c,sr__r.Street_Address__c,sr__r.Building_Name__c,sr__r.City__c,sr__r.Area__c,sr__r.Emirate__c,sr__r.Monthly_Accommodation__c,sr__r.Other_Monthly_Allowances__c,sr__r.Monthly_Basic_Salary__c,sr__r.Email_Address__c,sr__r.Mother_Full_Name__c,sr__r.Third_Language__c,sr__r.Second_Language__c,sr__r.First_Language__c,sr__r.Qualification__c,sr__r.Previous_Nationality__c,sr__r.Marital_Status__c,sr__r.Religion__c,sr__r.Passport_Date_of_Expiry__c,sr__r.Passport_Date_of_Issue__c,sr__r.Passport_Country_of_Issue__c,sr__r.Passport_Place_of_Issue__c,SR__R.Passport_Type__c,SR__R.Passport_Number__c,SR__R.Country_of_Birth__c,SR__r.Date_of_Birth__c,SR__r.Place_of_Birth__c,SR__r.Gender__c,SR__r.Middle_Name_Arabic__c,SR__r.First_Name_Arabic__c,SR__r.Last_Name_Arabic__c,sr__r.Occupation_GS__c,sr__r.Nationality_list__c,sr__r.Last_Name__c,sr__r.Middle_Name__c,SR__r.First_Name__c,Sr__r.Title__c,SR__r.Customer__c,SR__r.Record_Type_Name__c,SR__r.Service_Category__c,SAP_DOCCR__c, SAP_DOCDL__c, SAP_DOCRC__c, SR_Record_Type__c, Sys_Courier_Check__c, Awaiting_Originals_Email_Text__c, Inspection_Date__c, Inspectors_Name__c, Insurance_Amount__c, X3DaysAfterVerification__c, Is_Rejected_Status__c, Is_it_Approval_task__c, License_No__c, Sys_Additional_Email__c, Assigned_to_client__c, Bypass_Doc_Check__c, Liquidator_Appmt_Date__c, Liquidator_Stmt_Intmt_Date__c, Liquidator_Stmt_Prep_Date__c, Liquidator__c, Liquidator_s_Address__c, Liquidator_s_Name__c, List_of_Company_names__c, Location__c, Medical_Fitness_Required__c, Medical_Fitness__c, Medical_Urgency__c, DIFC_Internal__c, Date_and_Time_of_Induction__c, Mobile_No__c, Date_and_Time_of_Inspection__c, Date_and_Time_of_Inspection_for_email__c, Newspaper_Name__c, No_of_Days__c, Notes_Updated__c, Number_of_Keys__c, Date_of_DCD_Inspection__c, Date_of_DEWA_Inspection__c, Event_Completed_Satisfactorily__c, Event_Premise_Status__c, Expected_Attendants__c, IDAMA_HSE__c, Inspection_Checklist__c, Inspection_Status__c, Look_and_Feel_Schedule__c, Name1__c, Name_2__c, Name_3__c, Name__c, Notification_Date__c, Owner__c, Position1__c, Position__c, Step_Assignee_Name__c, Step_Created_Date__c, Step_Description__c, Valid_From__c, Valid_To__c, Waiver_for_under_formation_company__c, Parent_Step__c, Client_Email_for_fit_out__c, Payment_Terms__c, Comments__c, Contractor_email_for_fit_out__c, Planned_End_Date__c, Planned_Start_Date__c, Preferred_Courier_Date__c, Preferred_Courier_Time_New__c, Custom_SLA__c, Business_Hours_Duration__c, Priority__c, Internal_Comments__c, Area_for_fit_out__c, Project_Name__c, Reason__c, Received_Date__c, Received_From__c, Receiver_Details__c, Refund_Amount__c, Refund_Item__c, Refund__c, Rejection_Reason__c, Step_open_for_more_than_2_hours__c, Cheque_No__c, Receipt_No__c, Applicant_Name__c, Returned_By__c, Reviewed_By_User__c, Exempt_Educational_certificates__c, SROwnerId__c, SR_Menu_Text__c, SR_Number_Step__c, SR_Status__c, SR_Step__c, SR_Template__c, SR_Type_Step__c, SR_Type__c, SR__c, Sanction_Approval_Step__c, Schedule_Date__c, Biometrics_Required__c, Signatory_Name__c, Signatory_Title__c, Signatory__c, Step_Closed_Hours__c, Start_Date__c, Status_Code__c, Status_Type__c, Status__c, Step_Name__c, Step_No__c, Step_Note_Added__c, Step_Notes__c, Step_Status__c, Step_Template__c, Step_Type__c, Sub_Step__c, Summary__c, Sys_Step_Loop_No__c, Task_Number__c, Telephone_Number__c, To_Date__c, Total_Consume_Amnt__c, Principal_User_1__c, Principal_User_2__c, Principal_User_3__c, Principal_User_4__c, Principal_User_5__c, DNRD_Family_File_Opening_No__c, DNRD_Overstay_Fine_No__c, Units__c, Create_BP__c, Valid_Steps__c, Visa_amendment__c, set_no_trigger__c, Step_Duration__c, Is_Closed__c, Last_Modified_Time__c, Modified_Date__c, Created_Date_Time__c, Sys_Step_OwnerId__c, Add_Remove_Pending__c, Customer__c, DNRD_Receipt_No__c, Fine_Amount__c, ID_End_Date__c, ID_Start_Date__c, ID_Type__c, Is_Cancelled__c, No_PR__c, Pending__c, Pre_Verification_Done__c, Push_to_SAP__c, SAP_ACRES__c, SAP_AMAIL__c, SAP_BANFN__c, SAP_BIOMT__c, SAP_BNFPO__c, SAP_CMAIL__c, SAP_CRMAC__c, SAP_EACTV__c, SAP_EMAIL__c, SAP_ERDAT__c, SAP_ERNAM__c, SAP_ERTIM__c, SAP_ETIME__c, SAP_FINES__c, SAP_FMAIL__c, SAP_GSDAT__c, SAP_GSTIM__c, SAP_IDDES__c, SAP_IDEDT__c, SAP_IDNUM__c, SAP_IDSDT__c, SAP_IDTYP__c, SAP_MATNR__c, SAP_MDATE__c, SAP_MEDIC__c, SAP_MEDRT__c, SAP_MEDST__c, SAP_MFDAT__c, SAP_MFSTM__c, SAP_NMAIL__c, SAP_POSNR__c, SAP_PR_No__c, SAP_RESCH__c, SAP_RMATN__c, SAP_SO_No__c, SAP_STIME__c, SAP_Seq__c, SAP_TMAIL__c, SAP_UNQNO__c, SR_Group__c, SR_Temp_Menu__c, Status_Change_to_Verified_Date__c, Step_Id__c, Sys_SR_Group__c, Updated_from_SAP__c, Verified_By__c, created_date__c, Is_it_an_Express_Service__c, Is_Letter__c, PreGoLive__c, Type_of_Request__c, Sponsor_Name__c, Originals_Awaited_Date__c, First_Typist_Step__c, SAP_OSGUID__c, SAP_UMATN__c, SR_Record_Type_Text__c, Letter_Email_Text_ROC2__c, Closed_Time__c, First_Name_Arabic__c, Last_Name_Arabic__c, Update_Arabic_Names__c, Push_Visa_Details_to_Secupass__c, Owner_SAP_Id__c, Company_Type__c, BD_Sector_Classification__c, Sector_Classification__c, Legal_Type_of_Entity__c, Create_Picture__c, Letter_Email_Text_General__c, Step_Code__c, Processing_Days__c, Main_SR_Status__c, Closed_Duration__c, Legal_Structures__c, SR_Created_Date__c, Estimated_Hours__c, Put_on_Hold__c, Paused_Date__c, Resumption_Date__c, AWB_Track__c, Survey__c, Application_Reference__c, is_Rescheduled__c, Medical_Appointment_Calendar__c, X6monthsaftercreate__c, Bypass_StpOwner__c, Bypass_DNRD_validation__c, isCollectedStep__c, Place_of_Birth_Arabic__c, New_Passport__c, New_passport_expiry_date__c, New_passport_No__c, New_passport_place_of_issue__c, Email_Address_for_Signage__c, Middle_Name_Arabic__c, UID_Number__c, Mother_s_Full_Name_Arabic__c, Ok_to_Proceed__c, Ok_to_Proceed1__c, Related_Look_and_Feel__c, Is_KPI_Applicable__c, Is_It_Express_Service__c, OK_to_Priocess__c, Provide_Look_And_Feel_Request__c, Current_Visa_number__c, Bypass_Ok_To_Pay__c, Ok_to_Pay__c, Visa_Quantity__c, Department__c, On_Hold_reason__c, step_Closed_Hours1__c, Collected_PRO__c, Step_Closedcc_Hours__c,sr__r.Kindly_Note__c,sr__r.Residence_Visa_No__c,sr__r.Will_the_applicant_be_enrolled_into_DEWS__c,sr__r.Reason_for_non_enrolment__c,sr__r.Type_of_Wage_Payment__c FROM Step__c where id=:stepId];
            if(actionRec.Class_name__c=='GsPricingPushCC'){
                String customMethod =actionRec.Custom_Method__c;
                switch on customMethod {
                    when 'SendServiceToSAP' {if(!Test.isRunningTest()){GsPricingPushCC.SendServiceToSAP(stepRec);}}  
                    when else {    
                        // code block 4
                    }
                }
            }
            /*if(actionRec.Class_name__c=='CC_GSCodeCls'){
                String customMethod =actionRec.Custom_Method__c;
                switch on customMethod {
                    when 'Renew_POBox' {
                        // code block 1
                    }  
                    when else {    
                        // code block 4
                    }
                }
            }*/
            if(actionRec.Class_name__c=='GsContactCreation'){
                String customMethod =actionRec.Custom_Method__c;
                switch on customMethod {
                    when 'ProcessContactDetails' {if(!Test.isRunningTest()){GsContactCreation.ProcessContactDetails(stepRec);}}  
                    when else {    
                        // code block 4
                    }
                }
            }
            /*if(actionRec.Class_name__c=='GsEntryPermitUploadHandler'){
                String customMethod =actionRec.Custom_Method__c;
                switch on customMethod {
                    when 'SendServiceToSAP' {
                        // Code to run when GS team faces issue of documents not uploaded but documents are actually updated in Salesforce 
                        // Service_request__c serid = [Select id,name,Submitted_DateTime__c from service_request__c where id='a1I0J00000DdarA'];
                        // GsEntryPermitUploadHandler.EntryPermitStatusInSAP(serid.id,'0040',serid.Submitted_DateTime__c);
    
                    }  
                    when else {    
                        // code block 4
                    }
                }
            }*/
      //return 'We have successfully called the services--'+actionRec.Class_name__c+'-----'+actionRec.Custom_Method__c;
      return 'You have successfully initiated the service, if the service is not posted please check the link '+system.Label.GS_Webservice_Help_Page;
         }catch(exception ex){return 'Exception occured , Please coordinate with the support team - '+ex.getMessage()+' stack trace '+ex.getStackTraceString();}
       
    }
     webservice static string  createAnAirwayBill(String stepId){
      system.debug('createAnAirwayBill---stepId---'+stepId);
         String messageValue ='success';
         try{
             if([Select count() from shipment__c where Step__c =:stepId] == 0){CC_CourierCls.CreateShipmentNormal(stepId); }
             else{messageValue = 'Shipment already exist on the Step';}
         } Catch(Exception ex){
             messageValue = ex.getMessage();
         }
         return messageValue;
    }

}