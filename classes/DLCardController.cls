/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 08/13/2020
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08/13/2020   shoaib        Created
**********************************************************************************************************************/
public class DLCardController {
    
 Private static final String RECORD_TYPE_ID   =   Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('DL_Employee_Card').getRecordTypeId();
    
    @AuraEnabled
    public static wrapperClass getserviceRequest(String recordId){
     
      Service_Request__c thisServiceRequest = new  Service_Request__c();
      Account thisAccount                   = new Account();
        
      User  objUser =  [SELECT Id,
                                Name,ContactId,AccountId,Phone,Email,Contact.Account.Name,Contact.Account.Active_License__c
                           FROM User
                           WHERE Id=: userinfo.getUserId()]; 
        
        if(String.isBlank(recordId)){
            thisServiceRequest.Customer__c              = objUser.AccountId;
            thisServiceRequest.License__c               = objUser.Contact.Account.Active_License__c;
            thisServiceRequest.Email__c                 = objUser.Email;
            thisServiceRequest.Send_SMS_To_Mobile__c    = objUser.Phone;
        }
        
        else{
          thisServiceRequest = [SELECT Id,
                                        Title__c,Service_Category__c,Type_of_Amendment__c,Nationality_list__c,Express_Service__c,Occupation__c,First_Name__c,First_Name_Arabic__c,Passport_Number__c,Middle_Name__c,Middle_Name_Arabic__c,
                                        Customer__c,Passport_Date_of_Issue__c,Last_Name__c,Last_Name_Arabic__c,Passport_Date_of_Expiry__c,
                                        Date_of_Birth__c,Residence_Visa_No__c,Residence_Visa_Expiry_Date__c,Would_you_like_to_opt_our_free_couri__c,
                                        Use_Registered_Address__c,Consignee_FName__c,Courier_Mobile_Number__c,Courier_Cell_Phone__c,Consignee_LName__c,
                                        Apt_or_Villa_No__c,Statement_of_Undertaking__c,Duration__c,Occupation_GS__c,Send_SMS_To_Mobile__c,Email__c
                                      FROM Service_Request__c 
                                      WHERE Id =: recordId limit 1 ];  
        }
        string acountId = String.isNotBlank(objUser.AccountId) ? objUser.AccountId : thisServiceRequest.Customer__c;
        thisAccount = [SELECT Id,
                                Name,
                                    Active_License__r.Name,
                                        Index_Card__r.Name,
                                        OB_Active_Commercial_Permission__r.Name
                       FROM Account 
                       WHERE Id=:acountId  
                       LIMIT 1];
        
      return new wrapperClass(thisAccount,thisServiceRequest);
    }
    
    public class wrapperClass {
       @AuraEnabled
       public Account newAccount {get;set;}
       @AuraEnabled
       public Service_Request__c newSR {get;set;}
       @AuraEnabled
       public List<String> optionList {get;set;}
       @AuraEnabled
       public List<String> occuptationList {get;set;}
      
        public wrapperClass(Account newAccount,Service_Request__c newSR){
            this.newAccount = newAccount;
            this.newSR      = newSR;
            this.optionList = DLCardController.getPickListValuesIntoList();
         } 
    }
    
    @AuraEnabled
    public static string getRegistratedAccounts(String accountId){
      return [SELECT Id,Registered_Address__c FROM Account WHERE Id =: accountId].Registered_Address__c;
    }

    @AuraEnabled
    public static wrapperClass getSelectedContact(String contactId){

      Service_Request__c thisServiceRequest = new  Service_Request__c();

      Account thisAccount                   = new Account();

      User  objUser =  [SELECT Id,
                              Name,ContactId,AccountId,Phone,Email,Contact.Account.Name,Contact.Account.Active_License__c
                          FROM User
                          WHERE Id=: userinfo.getUserId()]; 

        thisServiceRequest.Customer__c              = objUser.AccountId;
        thisServiceRequest.License__c               = objUser.Contact.Account.Active_License__c;
        thisServiceRequest.Email__c                 = objUser.Email;
        thisServiceRequest.Send_SMS_To_Mobile__c    = objUser.Phone;
    
      Relationship__c thisRel = [SELECT Id,Object_Contact__c,
                                 Object_Contact__r.Title,
                                 Object_Contact__r.FirstName,
                                 Object_Contact__r.LastName,
                                 Object_Contact__r.Issued_Country__c,
                                 Object_Contact__r.Gender__c,
                                 Contact_Nationality__c,
                                 Passport_Issue_Date__c,
                                 Birth_date__c,
                                 Object_Contact__r.Occupation__c,
                                 Object_Contact__r.Marital_Status__c,
                                 Object_Contact__r.Passport_Type__c,
                                 Passport_Expiry_Date__c,
                                 Visa_Expiry_Date__c,
                                 Object_Contact__r.Visa_Number__c,
                                 Individual_Job_Title__c,
                                 Salutation__c,
                                 Passport_No__c,
                                 Visa_Number__c
                                 FROM Relationship__c 
                                 WHERE Relationship_Type__c ='DL Employee Card' 
                                 AND Object_Contact__c= :contactId];
                                  
    

      thisServiceRequest.Title__c                         = thisRel.Salutation__c;
      //thisServiceRequest.contact__c                       = thisRel.Object_Contact__c;
      thisServiceRequest.Nationality_list__c              = thisRel.Contact_Nationality__c;
      thisServiceRequest.Commercial_Activity__c           = thisRel.Individual_Job_Title__c;
      thisServiceRequest.First_Name__c                    = thisRel.Object_Contact__r.FirstName;
      thisServiceRequest.Last_Name__c                     = thisRel.Object_Contact__r.LastName;
      thisServiceRequest.Date_of_Birth__c                 = thisRel.Birth_date__c;
      thisServiceRequest.Passport_Date_of_Expiry__c       = thisRel.Passport_Expiry_Date__c;
      thisServiceRequest.Passport_Number__c               = thisRel.Passport_No__c;
      thisServiceRequest.Passport_Date_of_Issue__c        = thisRel.Passport_Issue_Date__c;
      thisServiceRequest.Residence_Visa_No__c             = thisRel.Visa_Number__c;
      thisServiceRequest.Residence_Visa_Expiry_Date__c    = thisRel.Visa_Expiry_Date__c;

      thisAccount = [SELECT Id,
                          Name,
                          Active_License__r.Name,
                          Index_Card__r.Name,
                          OB_Active_Commercial_Permission__r.Name
                      FROM Account 
                      WHERE Id=:objUser.AccountId 
                      LIMIT 1];

        return new wrapperClass(thisAccount,thisServiceRequest);
    }
    
    @AuraEnabled
    public static List<String> getPickListValuesIntoList(){
       List<String> pickListValuesList= new List<String>();
        Schema.DescribeFieldResult fieldResult = Service_Request__c.Nationality_list__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        }     
        return pickListValuesList;
    }
    
    
    
    @AuraEnabled
    public static string saveRequest(Service_Request__c newServiceRequest){
        String occuptionId  = '';
        
        if(!test.isRunningTest()){
            occuptionId = [SELECT ID,Name FROM Lookup__c WHERE Name =: newServiceRequest.Occupation_GS__c OR Id =:newServiceRequest.Occupation_GS__c limit 1].Id;
        }
        
        String message = '';
            
         User  objUser =  [SELECT Id,
                                Name,ContactId,AccountId,Phone,Email,Contact.Account.Name,Contact.Account.Active_License__c
                             FROM User
                             WHERE Id=: userinfo.getUserId()]; 
        
         Service_Request__c thisServiceRequest         = new  Service_Request__c();
         thisServiceRequest                            = newServiceRequest;
         thisServiceRequest.RecordTypeId               = RECORD_TYPE_ID;
         if(!test.isRunningTest())    thisServiceRequest.Occupation_GS__c           = occuptionId;
         thisServiceRequest.Email__c                   = objUser.Email;
         thisServiceRequest.Send_SMS_To_Mobile__c      = objUser.Phone;
         thisServiceRequest.Nationality_list__c        = newServiceRequest.Nationality_list__c;
         thisServiceRequest.License__c                 = objUser.Contact.Account.Active_License__c;

         if(String.isBlank(thisServiceRequest.Service_Category__c)){
          thisServiceRequest.Service_Category__c        ='New DL Employee Card';
          thisServiceRequest.Would_you_like_to_opt_our_free_couri__c = 'Yes';
        }
        
        if(thisServiceRequest.Service_Category__c == 'Amendment' || thisServiceRequest.Service_Category__c=='Renewal'){
          thisServiceRequest.Would_you_like_to_opt_our_free_couri__c = 'Yes';
        }
         thisServiceRequest.Duration__c                = '3 years';
        try{
            
        if(String.isNotBlank( thisServiceRequest.Id)){
            update thisServiceRequest;
        }
            
        else{
            insert thisServiceRequest;
        }
        
        message =  thisServiceRequest.Id;
        }
        catch(Exception ex){
        message = 'Error' +':' +ex.getMessage().split(',')[1];
        }
        return message;
      }
}