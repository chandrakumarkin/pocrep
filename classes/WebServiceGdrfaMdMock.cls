/******************************************************************************************************
 *  Author   : Kumar Utkarsh
 *  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* Mock Class for WebServiceFetchMDFromGDRFAClass to fetch Gdrfa Lookup Details.
*******************************************************************************************************/
@isTest
global class WebServiceGdrfaMdMock implements HttpCalloutMock{
    //Implement http mock callout here
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request){
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setStatusCode(200);
        response.setBody('[{"name": { "ar": "انستان","en": "test"},"loopkupId": "983798209280928", "IsOutsideCountryCancelReason": true, "parentLookupId":"8736"}]');     
        return response;
    }
}