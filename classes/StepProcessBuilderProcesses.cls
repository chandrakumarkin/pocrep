/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-21-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   10-20-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public with sharing class StepProcessBuilderProcesses {
    @InvocableMethod(label='stepProcessBuilderInvocation' description='stepProcessBuilderInvocation') 
    public static void stepProcessBuilderStepName(List<id> stepIds){
		for(Step__c stepRec : [Select id,step_name__c from Step__c where id in :stepIds]){
			if(stepRec.step_Name__c =='Medical Fitness Test Scheduled')SendMedicalServiceToSAP(stepRec.id);
		}
    }
    public static string SendMedicalServiceToSAP(Id objStepId)
    {
         if (!test.isRunningTest())GsMedicalTestScheduledHandler.MedicalTestScheduled(objStepId);
         return 'Success';
    }
}