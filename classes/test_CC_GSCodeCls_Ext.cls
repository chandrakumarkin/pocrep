@isTest(seeAllData=false)
public class test_CC_GSCodeCls_Ext{
    
    public Static Step__c ObjstepXP;
    public Static Step__c ObjstepXP1;
    public static Service_Request__c objSR;
    public static SR_Status__c objIssue;
    public static SR_Status__c objIssue1;
    
     public static void testData() 
     {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
         map<string,id> mapOfRecords = new map<string,id>();
        for(RecordType rectyp:[select id,DeveloperName from RecordType where sObjectType='Service_Request__c' AND 
                               (DeveloperName='Multiple_Cancellation_Dependent')])
        {
            mapOfRecords.put(rectyp.DeveloperName,rectyp.id);
        }
        
        
        
        objIssue = new SR_Status__c();
        objIssue.name = 'Submitted';
        objIssue.Code__c = 'Submitted';
        insert objIssue;
        
        objIssue1 = new SR_Status__c();
        objIssue1.name = 'Draft';
        objIssue1.Code__c = 'Draft';
        insert objIssue1;
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Express_Service__c = true;
        objSR1.Customer__r = objAccount;
        objSR1.Customer__c = objAccount.id;
        objSR1.RecordTypeId = mapOfRecords.get('Multiple_Cancellation_Dependent');
        objSR1.submitted_date__c = date.today();
        objSR1.Express_Service__c = false;
        objSR1.External_SR_Status__c = objIssue.ID;
        objSR1.Internal_SR_Status__c = objIssue.ID;
        objSR1.Apt_or_Villa_No__c = 'Dubai';
        objSR1.Send_SMS_To_Mobile__c='+971569803616';
        //objSR1.Linked_SR__c = '';
        INSERT objSR1; 
        
        objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = mapOfRecords.get('Multiple_Cancellation_Dependent');
        objSR.submitted_date__c = date.today();
        objSR.Express_Service__c = false;
        objSR.External_SR_Status__c = objIssue1.ID;
        objSR.Internal_SR_Status__c = objIssue1.ID;
        objSR.Apt_or_Villa_No__c = 'Dubai';
        objSR.Send_SMS_To_Mobile__c='+971569803616';
        objSR.Linked_SR__c = objSR1.ID;
        insert objSR;
        
        SR_Price_Item__c price = new SR_Price_Item__c();
        price.ServiceRequest__c = objSR.ID;
        price.Status__c = 'Blocked';
        INSERT price;
        
        ObjstepXP=new Step__c();
        ObjstepXP.SR_Group__c='GS';
        ObjstepXP.SR__c=objSR.id;
        insert ObjstepXP;
        
        ObjstepXP1 =new Step__c();
        ObjstepXP1.SR_Group__c='GS';
        ObjstepXP1.SR__c=objSR1.id;
        insert ObjstepXP1;
        
        system.debug('##@@@'+ObjstepXP);
     }
     static testmethod void positiveTest(){
        
        testData();
        TEST.startTest();
        CC_GSCodeCls_Ext.CC_GSCodeCls_ExtMain(ObjstepXP,'MultipleRelatedSRsubmit');
        CC_GSCodeCls_Ext.MultipleRelatedSRsubmitPrices(objSR.ID);
        CC_GSCodeCls_Ext.MultipleRelatedSRsubmit(ObjstepXP);
        CC_GSCodeCls_Ext.HodPreCheckRefund(ObjstepXP);
        CC_GSCodeCls_Ext.HodRejectionAction(ObjstepXP);
        CC_GSCodeCls_Ext.HodPreCheckRefundQueable(ObjstepXP);
        CC_GSCodeCls_Ext.GSNonSponCustomAttacMethod(ObjstepXP);
        //CC_GSCodeCls_Ext.GDRFAActivityPushMethod(ObjstepXP);
      //  CC_GSCodeCls_Ext.PSA_Termination_Objection(ObjstepXP);
        TEST.StopTest();
     }
     
     static testmethod void positiveTest2(){
        
        testData();
         objIssue = new SR_Status__c();
        objIssue.name = 'Cancelled';
        objIssue.Code__c = 'Cancelled';
        insert objIssue;
        
        TEST.startTest();
        CC_GSCodeCls_Ext.CC_GSCodeCls_ExtMain(ObjstepXP,'MULTIPLE_RELATED_SR_SUBMIT');
        CC_GSCodeCls_Ext.MultipleRelatedSRsubmit(ObjstepXP);
        CC_GSCodeCls_Ext.PSA_Termination_Objection(ObjstepXP);
        
        TEST.StopTest();
     }
     
     static testmethod void positiveTest3(){
        
        testData();
         objIssue = new SR_Status__c();
        objIssue.name = 'Cancelled';
        objIssue.Code__c = 'Cancelled';
        insert objIssue;
        
        TEST.startTest();
        CC_GSCodeCls_Ext.CC_GSCodeCls_ExtMain(ObjstepXP,'PSA_Termination_Objection_approved');
        CC_GSCodeCls_Ext.MultipleRelatedSRsubmit(ObjstepXP1);
        CC_GSCodeCls_Ext.PSA_Termination_Objection(ObjstepXP);
        TEST.StopTest();
     }
     
      
}