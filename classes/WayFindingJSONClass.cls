/*********************************************************************************************************************
      Author: Sai Kalyan
      Purpose: 
      -------------------------------------------------------------------------------------------------------------------------
      Modification History 
      ----------------------------------------------------------------------------------------
      V.No    Date           Updated By      Description
      ----------------------------------------------------------------------------------------              
      V1.1    24-JUN-2019    Sai             created Class
      v1.2    12-Feb-2020    Arun            Added arabic varibales 
      v1.3   13-Feb-2020     selva           # 8438
      V1.4   29-APR-2020     Sai             Updated Account SOQL Query 
      **********************************************************************************************************************/
      
      public class WayFindingJSONClass 
      {

        public static String jsonParser(String rStartNumber,String mode) {
      
          List<Company_information> lstCompanyinfo = new List<Company_information>();
          List<Company_information_list> lstCompanyinfo1 = new List<Company_information_list>();
          try{
            Company_information cInformation;
            Company_information_list cInformationList = new Company_information_list();
            String CompanyUnit;
            List<string> ImageURLs;
            OperatingLocations oLocations;
            LicenseActivity Activity;
            List<LicenseActivity> lstActivity  = new List<LicenseActivity>();
            List<OperatingLocations> lstOpeartingLocation;
            List<BusinessHours> lstBusinessHours;
            
            List<Open> lstOpen;
            Map<String,String> mapSectorClassification = new Map<String,String>();
            
            for(WayFinding_Categories__c wayfinding:[SELECT Category__c,Sector_Classification__c From WayFinding_Categories__c]){
              mapSectorClassification.put(wayfinding.Sector_Classification__c,wayfinding.Category__c);
            }
            
            wayFinalJson wayJSON = new wayFinalJson();
            WayJSONList wayJSONLIST = new WayJSONList();
            String Active = 'Active';
            String cpActive = 'CP-Active';  
            
            //V1.4  
            String query = 'SELECT Id,Operating_Location__c,Operating_Location__r.Lease_Types__c,Hosting_Company__c,Hosting_Company__r.Operating_Location__r.Lease_Types__c,Building_Lease_Formula__c,Arabic_Name__c,Trading_Name_Arabic__c,Arabic_Office__c,Arabic_Building_Name__c,Arabic_Street__c,Arabic_City__c,Arabic_Emirate__c,Arabic_Country__c, Description,Name,Company_Type__c,Operating_Location__r.Unit__r.Unit_Unique_Ref__c,Sector_Classification__c,Phone,Trade_Name__c,(SELECT Type__c,Activity_Name_Eng__c From License_Activities__r where End_Date__c =null),Registration_License_No__c,Website,Operating_Location__r.Floor__c,Hide_the_Company__c,Hide_Company_SVC__c,RORP_License_No__c,Registered_Address__c,Building_Name__c,Street__c,City__c,Country__c,Postal_Code__c,(SELECT IsRegistered__c,Unit__r.Name,Unit__r.Unit_Unique_Ref__c,Unit__r.Building__r.Building_No__c,Unit__r.Building__r.Company_Code__c,Unit__r.Building__r.Building_Coordinates__c,Building_Name__c,Floor__c,Area__c,Area_WF__c,Lease_Types__c From Operating_Locations__r where Status__c=:Active and Area__c != \''+0+'\' ORDER BY Area_WF__c DESC),(SELECT Sunday__c,Monday__c,Tuesday__c,Wednesday__c,Thursday__c,Friday__c,Saturday__c,Saturday_To__c,Sunday_To__c,Monday_To__c,Tuesday_To__c,Wednesday_To__c,Thursday_To__c,Friday_To__c From Business_Hours__r),(select id,Image_URL__c From Company_Images__r where Active__c=true) From Account WHERE Hide_Company_SVC__c = false AND (ROC_Status__c=:Active OR ROC_Status__c=:cpActive)';
            if(rStartNumber !=null && rStartNumber !=''){
              query += ' AND Registration_License_No__c =: rStartNumber';
            }
             system.debug('!!@@@'+Mode);
             List<Account> accList = Database.query(query);
             system.debug('--accList----'+accList);
            for(Account eachAccount: accList){
              String str = '';
              system.debug('!!@$$$$@@'+Mode);
              cInformation = new Company_information();
              if(mode != null && mode == '' && mode != 'List'){
                
                system.debug('!!@#@##'+mode);
                ImageURLs=new List<string>();
                for(Company_Images__c ObjImage:eachAccount.Company_Images__r)
                {
                  ImageURLs.add(ObjImage.Image_URL__c);
                }
                cInformation.CompanyName = eachAccount.Name;
                      
                if(eachAccount.Registration_License_No__c !=null){
                  cInformation.RegistrationNo = String.valueOf(eachAccount.Registration_License_No__c);
                }
                else{
                  cInformation.RegistrationNo = '';
                }
                
                if(eachAccount.Registered_Address__c !=null){
                  cInformation.RegisteredAddress = eachAccount.Registered_Address__c;
                }
                else{
                  cInformation.RegisteredAddress = '';
                }
                
                //Hussain Added
                 if(eachAccount.Description !=null){
                  cInformation.Description = eachAccount.Description;
                  cInformation.Description_ar = ''; 
                }
                else{
                  cInformation.Description = '';
                 cInformation.Description_ar = ''; 
                }
                
                system.debug('!!#@###'+cInformation);
                //Start Arabic text 
                //Arabic_Name__c,Trading_Name_Arabic__c,Arabic_Office__c,Arabic_Building_Name__c,Arabic_Street__c,Arabic_City__c,Arabic_Emirate__c,Arabic_Country__c,
                if(eachAccount.Arabic_Name__c !=null){
                    cInformation.CompanyName_ar=eachAccount.Arabic_Name__c;
                }
                else{
                    cInformation.CompanyName_ar= '';
                }
                if(eachAccount.Trading_Name_Arabic__c !=null){
                    cInformation.TradeName_ar=eachAccount.Trading_Name_Arabic__c;
                }
                else{
                    cInformation.TradeName_ar= '';
                }
                if(eachAccount.Arabic_Office__c !=null){
                    cInformation.Office_ar=eachAccount.Arabic_Office__c;
                }
                else{
                    cInformation.Office_ar = '';
                }
                if(eachAccount.Arabic_Building_Name__c !=null){
                    cInformation.BuildingName_ar=eachAccount.Arabic_Building_Name__c;
                }
                else{
                    cInformation.BuildingName_ar='';
                }
                if(eachAccount.Arabic_Street__c !=null ){
                    cInformation.Street_ar=eachAccount.Arabic_Street__c;
                }
                else{
                    cInformation.Street_ar= '';
                }
                if(eachAccount.Arabic_City__c !=null){
                cInformation.City_ar=eachAccount.Arabic_City__c;
                }
                else{
                    cInformation.City_ar='';
                }
                if(eachAccount.Arabic_Emirate__c !=null){
                cInformation.Emirate_ar=eachAccount.Arabic_Emirate__c;
                }
                else{
                    cInformation.Emirate_ar = '';
                }
                if(eachAccount.Arabic_Country__c !=null){
                cInformation.Country_ar=eachAccount.Arabic_Country__c;
                }
                else{
                    cInformation.Country_ar=''; 
                }
          
                //End  Arabic text 
                
                
                if(eachAccount.Building_Name__c !=null){
                  /** //v1.3
                  String BuildingName = eachAccount.Building_Name__c.toUpperCase();
                  BuildingName = BuildingName.replaceAll(' ','-');
                  cInformation.ReferenceCell_ID  = BuildingName; 
                  cInformation.BuildingName = eachAccount.Building_Name__c;
                  **/
                  //v1.3 start
                  String BuildingName;
                  if(String.isNotBlank(eachAccount.Building_Lease_Formula__c)){
                    BuildingName = eachAccount.Building_Lease_Formula__c.toUpperCase();
                    BuildingName = BuildingName.replaceAll(' ','-');
                  } 
                     
                  
                  cInformation.ReferenceCell_ID  = BuildingName; //v1.3
                  cInformation.BuildingName = eachAccount.Building_Lease_Formula__c;
                       //v1.3 end
                }else{
                  cInformation.BuildingName = '';
                  cInformation.ReferenceCell_ID = '';
                }
                
                if(eachAccount.Operating_Location__c != null && eachAccount.Operating_Location__r.Lease_Types__c != null && eachAccount.Operating_Location__r.Lease_Types__c == 'CoWorking-Innovation')cInformation.ReferenceCell_ID = 'INNOVATION';
                else if(eachAccount.Operating_Location__c != null && eachAccount.Operating_Location__r.Lease_Types__c != null && eachAccount.Operating_Location__r.Lease_Types__c == 'CoWorking')cInformation.ReferenceCell_ID = 'FINTECH';
                if(eachAccount.Operating_Location__c == null && eachAccount.Hosting_Company__c != null && eachAccount.Hosting_Company__r.Lease_Types__c != null && eachAccount.Hosting_Company__r.Lease_Types__c == 'CoWorking-Innovation')cInformation.ReferenceCell_ID = 'INNOVATION';
                else if(eachAccount.Operating_Location__c == null && eachAccount.Hosting_Company__c != null && eachAccount.Hosting_Company__r.Lease_Types__c != null && eachAccount.Hosting_Company__r.Lease_Types__c == 'CoWorking')cInformation.ReferenceCell_ID = 'FINTECH';
           
                
                if(eachAccount.Sector_Classification__c !=null){
                  
                  Categories eachCategory = new Categories();
                  List<Categories> lstCategory = new List<Categories>();
                  String Categories;
                  if(mapSectorClassification !=null && mapSectorClassification.get(eachAccount.Sector_Classification__c) !=null){
                     Categories = mapSectorClassification.get(eachAccount.Sector_Classification__c);
                  }
                  
                  if(Categories !=null && Categories !=''){
                    for(String eachString:Categories.split(',')){
                      eachCategory = new Categories();
                      eachCategory.Category = eachString;
                      lstCategory.add(eachCategory);
                    }
                  }
                  cInformation.CategoriesList = lstCategory;
                }
                else{
                  cInformation.CategoriesList = new List<Categories>();
                }       
                if(eachAccount.Street__c !=null){
                  cInformation.Street = eachAccount.Street__c;
                }
                else{
                  cInformation.Street = '';
                }
                
                if(eachAccount.Hide_the_Company__c !=null){
                  cInformation.HidetheCompany = String.valueOf(eachAccount.Hide_the_Company__c);
                }
                else{
                  cInformation.HideTheCompany = '';
                }
                
             /*   if(eachAccount.Hide_Company_SVC__c !=null){
                  cInformation.HidetheCompanySPC = String.valueOf(eachAccount.Hide_Company_SVC__c);
                }
                else{
                  cInformation.HidetheCompanySPC = '';
                }*/
              
             
                if(eachAccount.Company_Type__c !=null){
                  cInformation.CompanyType = eachAccount.Company_Type__c;
                }
                else{
                  cInformation.CompanyType = '';
                }
                
                if(eachAccount.City__c !=null){
                  cInformation.City = eachAccount.City__c;
                }
                else{
                  cInformation.City = '';
                }
                
                if(eachAccount.Country__c !=null){
                  cInformation.Country = eachAccount.Country__c;
                }
                else{
                  cInformation.Country = '';
                }
                
                if(eachAccount.Postal_Code__c !=null){
                  cInformation.PostalCode = eachAccount.Postal_Code__c;
                }
                else{
                  cInformation.PostalCode = '';
                }
                
                if(eachAccount.Operating_Location__r.Floor__c !=null){
                  cInformation.FloorNumber = eachAccount.Operating_Location__r.Floor__c;
                }
                else{
                  cInformation.FloorNumber = '';
                }
                
                if(eachAccount.Phone !=null){
                  cInformation.PhoneNumber = eachAccount.Phone;
                }
                else{
                  cInformation.PhoneNumber = '';
                }
                
                if(eachAccount.Sector_Classification__c !=null){
                  cInformation.SectorClassification = eachAccount.Sector_Classification__c;
                }
                else{
                  cInformation.SectorClassification = '';
                }
                if(eachAccount.Website !=null){
                  cInformation.Website = eachAccount.Website;
                }
                else{
                  cInformation.Website = '';
                }
                
                if(eachAccount.Trade_Name__c !=null){
                  cInformation.TradeName = eachAccount.Trade_Name__c;
                }
                else{
                  cInformation.TradeName = '';
                }
                if(eachAccount.Company_Type__c !=null){
                  if(eachAccount.Company_Type__c =='Retail' && eachAccount.Trade_Name__c !=null){
                     cInformation.DisplayName = eachAccount.Trade_Name__c;
                     if(eachAccount.Trading_Name_Arabic__c !=null){
                        cInformation.DisplayName_ar =  eachAccount.Trading_Name_Arabic__c;
                     }
                     else{
                        cInformation.DisplayName_ar ='';
                     }
                  }
                  else if(eachAccount.Company_Type__c !='Retail'){
                    cInformation.DisplayName = eachAccount.Name;
                     if(eachAccount.Arabic_Name__c !=null){
                        cInformation.DisplayName_ar =  eachAccount.Arabic_Name__c;
                     }
                     else{
                        cInformation.DisplayName_ar = '';
                     }
                  }
                  else{
                    cInformation.DisplayName = '';
                     cInformation.DisplayName_ar ='';
                  }
                }
                else{
                  cInformation.DisplayName = '';
                  cInformation.DisplayName_ar ='';
                }
                
                system.debug('==eachAccount.Operating_Locations__r====='+eachAccount.Operating_Locations__r);
  
                if(eachAccount.Operating_Locations__r !=null){
                  lstOpeartingLocation = new List<OperatingLocations>(); 
                  for(Operating_Location__c eachLocation:eachAccount.Operating_Locations__r){
                    
                    oLocations = new OperatingLocations();
                    if(eachLocation.Unit__r.Unit_Unique_Ref__c !=null){
                      oLocations.UnitUniqueRef = eachLocation.Unit__r.Unit_Unique_Ref__c;
                      CompanyUnit = eachLocation.Unit__r.Unit_Unique_Ref__c;
                    }
                    else{
                      oLocations.UnitUniqueRef = '';
                      CompanyUnit = '';
                    }
                    
                    //Added Unit no
                    oLocations.UnitNo =eachLocation.Unit__r.Name;
                    
                    if(eachLocation.Building_Name__c !=null){
                      oLocations.buildingName = eachLocation.Building_Name__c;
                    }
                    else{
                      oLocations.buildingName = '';
                    }
                    system.debug('==111===oLocations.buildingName========='+oLocations.buildingName);
                    system.debug('=====eachLocation.Lease_Types__c=========='+eachLocation.Lease_Types__c);
                    if(eachLocation.Lease_Types__c != null && eachLocation.Lease_Types__c == 'CoWorking-Innovation')oLocations.buildingName ='INNOVATION';
                    else if(eachLocation.Lease_Types__c != null && eachLocation.Lease_Types__c == 'CoWorking')oLocations.buildingName ='FINTECH';       
                    system.debug('==222===oLocations.buildingName========='+oLocations.buildingName);

                    if(eachLocation.Floor__c !=null){
                      oLocations.Floor = eachLocation.Floor__c;
                    }
                    else{
                      oLocations.Floor = '';
                    }
                    if(eachLocation.Unit__r.Building__r.Company_Code__c =='2300'){
                      oLocations.DIFC_own = 'false';//3rd party builings 
                    }
                    else
                      oLocations.DIFC_own = 'true';
                    
                    if(eachLocation.Area__c !=null){
                      oLocations.Area = eachLocation.Area__c;
                    }
                    else{
                      oLocations.Area = '';
                    }
                    if(eachLocation.Unit__r.Building__r.Building_No__c !=null){
                      oLocations.BuildingNo = eachLocation.Unit__r.Building__r.Building_No__c;
                    }
                    else{
                      oLocations.BuildingNo = '';
                    }
                    
                    if(oLocations.buildingName =='FINTECH' || oLocations.buildingName =='INNOVATION') oLocations.BuildingNo = oLocations.buildingName;
                    
                    if(String.valueOf(eachLocation.Unit__r.Building__r.Building_Coordinates__c) !=null){
                      Location LocationCoordinates = eachLocation.Unit__r.Building__r.Building_Coordinates__c;
                      oLocations.BuildingLatitude = String.valueOf(LocationCoordinates.latitude);
                      oLocations.BuildingLongitude = String.valueOf(LocationCoordinates.longitude);
                    }
                    else{
                      //oLocations.BuildingCoordinates = '';
                      oLocations.BuildingLatitude = '';
                      oLocations.BuildingLongitude = '';
                    }
                    
                    if(eachLocation.IsRegistered__c !=null && eachLocation.IsRegistered__c == true){
                      oLocations.Registered = 'True';
                    }
                    else{
                      oLocations.Registered = 'False';
                    }
                    lstOpeartingLocation.add(oLocations);
                  }
                }
                
                for(License_Activity__c eachLicenseActivity:eachAccount.License_Activities__r){
                  
                  Activity = new LicenseActivity();
                  if(eachLicenseActivity.Activity_Name_Eng__c !=null){
                    if(eachLicenseActivity.Activity_Name_Eng__c !=null){
                      Activity.ActivityName = eachLicenseActivity.Activity_Name_Eng__c;
                    }
                    else{
                      Activity.ActivityName  = '';
                    }
                    if(eachLicenseActivity.Type__c !=null){
                      Activity.Type = eachLicenseActivity.Type__c;
                    }
                    else{
                      Activity.Type = '';
                    }
                    lstActivity.add(Activity);
                  }
                }
                if(eachAccount.Business_Hours__r !=null){
                  lstBusinessHours = new List<BusinessHours>();
                  
                  lstOpen = new List<Open>();
                  system.debug('!!@##@@@'+eachAccount.Business_Hours__r);
                  for(Company_Business_Hours__c businessHour:eachAccount.Business_Hours__r)
                 {
                 if(businessHour.Sunday__c != null && businessHour.Sunday_To__c !=null)
                  {          
                             str='[';
                             str+='{"day":"Sunday","open":{"from":"'+businessHour.Sunday__c+'","to":"'+businessHour.Sunday_To__c+'"}},';
                  
                  }
                  else
                  {
                    str+='{"day":"Sunday","open":{"from":"'+''+'","to":"'+''+'"}},';
                    
                  }
                  
                  if(businessHour.Sunday__c != null && businessHour.Sunday_To__c !=null)
                  {
                    str+='{"day":"Monday","open":{"from":"'+businessHour.Monday__c+'","to":"'+businessHour.Monday_To__c+'"}},';
                  
                  }
                  else
                  {
                    str+='{"day":"Monday","open":{"from":"'+''+'","to":"'+''+'"}},';
                    
                  }
                             
                   if(businessHour.Sunday__c != null && businessHour.Sunday_To__c !=null)
                  {
                    str+='{"day":"Tuesday","open":{"from":"'+businessHour.Tuesday__c+'","to":"'+businessHour.Tuesday_To__c+'"}},';
                  
                  }
                  else
                  {
                    str+='{"day":"Tuesday","open":{"from":"'+''+'","to":"'+''+'"}},';
                    
                  }
                  
                  if(businessHour.Sunday__c != null && businessHour.Sunday_To__c !=null)
                  {
                    str+='{"day":"Wednesday","open":{"from":"'+businessHour.Wednesday__c+'","to":"'+businessHour.Wednesday_To__c+'"}},';
                  }
                  else
                  {
                    str+='{"day":"Wednesday","open":{"from":"'+''+'","to":"'+''+'"}},';
                    
                  }
                             
                  if(businessHour.Sunday__c != null && businessHour.Sunday_To__c !=null)
                  {
                   str+='{"day":"Thursday","open":{"from":"'+businessHour.Thursday__c+'","to":"'+businessHour.Thursday_To__c+'"}},';
                  }
                            
                  else
                  {
                    str+='{"day":"Thursday","open":{"from":"'+''+'","to":"'+''+'"}},';
                    
                  }
                  if(businessHour.Sunday__c != null && businessHour.Sunday_To__c !=null)
                  {
                    str+='{"day":"Friday","open":{"from":"'+businessHour.Friday__c+'","to":"'+businessHour.Friday_To__c+'"}},';
                  }
                  else
                  {
                    str+='{"day":"Friday","open":{"from":"'+''+'","to":"'+''+'"}},';
                    
                  }
                            if(businessHour.Sunday__c != null && businessHour.Sunday_To__c !=null)
                  {
                    str+='{"day":"Saturday","open":{"from":"'+businessHour.Saturday__c+'","to":"'+businessHour.Saturday_To__c+'"}}';
                    str+=']';
                  }
                  else
                  {
                    str+='{"day":"Saturday","open":{"from":"'+''+'","to":"'+''+'"}},';
                    
                  }
                     
                    
                    
                    
                    
                    
                  }
                }
                
                system.debug('!!#@###'+cInformation);
                cInformation.CompanyUnitUniqueRef = CompanyUnit; 
                cInformation.companyBusinessHours = str;
                //cInformation.companyBusinessHours = lstBusinessHours;
                //cInformation.parseJson=cInformation.companyBusinessHours;
              
                //cInformation.OpenHours = lstOpen;
                
                cInformation.LicenseActivites = lstActivity;
                cInformation.operatingLocation = lstOpeartingLocation;
                cInformation.CardImages =ImageURLs;
                lstCompanyinfo.add(cInformation);
              }
              
              else {
                
                cInformationList = new Company_information_list();
                if(eachAccount.Company_Type__c !=null){
                  if(eachAccount.Company_Type__c =='Retail' && eachAccount.Trade_Name__c !=null){
                    cInformationList.DisplayName = eachAccount.Trade_Name__c;
                    cInformationList.DisplayName_ar =  eachAccount.Trading_Name_Arabic__c;
                  }
                  else if(eachAccount.Company_Type__c !='Retail'){
                    cInformationList.DisplayName = eachAccount.Name;
                     cInformationList.DisplayName_ar =  eachAccount.Arabic_Name__c;
                  }
                  else{
                    cInformationList.DisplayName = '';
                     cInformationList.DisplayName_ar = '';
                  }
                }
                else{
                  cInformationList.DisplayName = '';
                  cInformationList.DisplayName_ar = '';
                }
                
                if(eachAccount.Registration_License_No__c !=null){
                  cInformationList.RegistrationNo = String.valueOf(eachAccount.Registration_License_No__c);
                }
                else{
                  cInformationList.RegistrationNo = '';
                }
                
                if(eachAccount.Operating_Location__r.Unit__r.Unit_Unique_Ref__c !=null){
                  cInformationList.CompanyUnitUniqueRef = eachAccount.Operating_Location__r.Unit__r.Unit_Unique_Ref__c;
                }
                lstCompanyinfo1.add(cInformationList);
              }
            }
            String finalJsonformet;
            system.debug('(((())))'+lstCompanyinfo1);
            if(mode != null && mode != 'List'){
              wayJSON.CompanyInformation = lstCompanyinfo;
              finalJsonformet = JSON.serialize(wayJSON);
            }
            else{
              wayJSONLIST.CompanyInformation = lstCompanyinfo1;
              finalJsonformet = JSON.serialize(wayJSONLIST);
            }
            system.debug('$$$$$'+finalJsonformet);
            
            return finalJsonformet;
          }
          catch(exception ex){
            return null;
          }
        }
        
        
        
      
         public class Company_information {
           
          public String CompanyName;
          public String RegisteredAddress;
          public String BuildingName;
          public String Street;
          public String City;
          public String Country;
          public String PostalCode;   
          public String RegistrationNo; 
          public String CompanyUnitUniqueRef;   
          public String HidetheCompany;  
          //public String HidetheCompanySPC;
          public String CompanyType;
          public String PhoneNumber;
          public String FloorNumber; 
          public String Website;
          public String SectorClassification;
          public String TradeName;
          public String DisplayName;
          public String ReferenceCell_ID;
          public String Description;
          
          //Start v1.2
         public String DisplayName_ar;
         public String Description_ar;
         public String CompanyName_ar;
         public String TradeName_ar;
          
          public String Office_ar;
          public String BuildingName_ar;
          public String Street_ar;
          public String City_ar;
          public String Emirate_ar;
          public String Country_ar;
                 //Arabic_Name__c,Trading_Name_Arabic__c,Arabic_Office__c,Arabic_Building_Name__c,Arabic_Street__c,Arabic_City__c,Arabic_Emirate__c,Arabic_Country__c,
           //End v1.2
          
          public List<OperatingLocations> OperatingLocation;
          public  List<String> CardImages;
          public List<LicenseActivity> LicenseActivites;
          //public List<BusinessHours> CompanyBusinessHours;
          public String CompanyBusinessHours;
          //public List<Open> OpenHours;
          public List<Categories> CategoriesList;
        }
        public class Company_information_list
        {
          public String RegistrationNo; 
          public String CompanyUnitUniqueRef;      
          public String DisplayName;
          // v1.2
          public String DisplayName_ar;
        }
        
        
        public class BusinessHours{
          
          public String day;
          public open open;
           /** public String Sunday;
          public String Monday;
          public String Tuesday;
          public String Wednesday;
          public String Thursday;
          public String Friday;
          public String Saturday;
          public String Open;
          public String FromTime;
          public String To;**/
          
         
          
        }
        
        public class Open
        {
          public String FromTime;
          public String To;
        }
        
        
        public class OperatingLocations{
          public String UnitUniqueRef;
          public String BuildingName;
          public String UnitNo;
          public String Floor;
          public String Registered;
          public String DIFC_own;
          public String Area;
          public String BuildingLatitude;
          public String BuildingLongitude;
          
          public String BuildingNo;   
        }
        
        public class LicenseActivity{
          
          public String ActivityName;
          public String Type;
        }
        
        public class Categories{
          
          public String Category;
        }
        public class wayFinalJson {   
          public List<Company_information> CompanyInformation;
        }
        public class WayJSONList{
          public List<Company_information_list> CompanyInformation;
        }
            
   
       
      }