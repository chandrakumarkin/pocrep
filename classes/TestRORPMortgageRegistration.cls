/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestRORPMortgageRegistration {
    
    private static Service_Request__c testSr;
    
    private static List<Status__c> testStatuses;
    
    private static map<string,string> mapRecordType;
    
    private static void executesrValidationsTest(){
        
        list<Building__c> lstBuilding = new list<Building__c>();
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP On Plan';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.SAP_Building_No__c = '123456';
        lstBuilding.add(objBuilding);
        insert lstBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        mapRecordType = new map<string,string>();
        
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Company_Temporary_Work_Permit_Request_Letter','Mortgage_Registration','Discharge_Mortgage','Mortgage_Variation','RORP_Account','RORP_Contact','Mortgage_Unit','Individual_Mortgager','Company_Mortgager')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'RORP Customer';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        objAccount.RecordTypeId = mapRecordType.get('RORP_Account');
        objAccount.Issuing_Authority__c = 'DMCC';
        objAccount.RORP_License_No__c = '1234567';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Boina';
        objContact.Birthdate = system.today().addYears(-20);
        objContact.Nationality__c = 'India';
        objContact.RecordTypeId = mapRecordType.get('RORP_Contact');
        insert objContact;
        
        list<Lease__c> lstLeases = new list<Lease__c>();
        Lease__c objLease = new Lease__c();
        objLease.Name = '987654321';
        objLease.Lease_Types__c = 'Mortgage Property Registration';
        objLease.Status__c = 'Active';
        objLease.Type__c = 'Purchased';
        lstLeases.add(objLease);
        insert lstLeases;
        
        list<Lease_Partner__c> lstLPs = new list<Lease_Partner__c>();
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = lstLeases[0].Id;
        objLP.Unit__c = lstUnits[0].Id;
        objLP.Status__c = 'Active';
        objLP.Contact__c = objContact.Id;
        lstLPs.add(objLP);
        
        objLP = new Lease_Partner__c();
        objLP.Lease__c = lstLeases[0].Id;
        objLP.Unit__c = lstUnits[0].Id;
        objLP.Status__c = 'Active';
        objLP.Account__c = objAccount.Id;
        lstLPs.add(objLP);
        insert lstLPs;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.RecordTypeId = mapRecordType.get('Mortgage_Registration');
        objSR.Customer__c = objAccount.Id;
        insert objSR;
        
        Product2 testFineProduct = Test_CC_FitOutCustomCode_DataFactory.getTestProduct('RORP One','RORP');
        
        Product2 testFineProduct2 = Test_CC_FitOutCustomCode_DataFactory.getTestProduct('RORP Two','RORP');
            
        insert new list<Product2>{testFineProduct,testFineProduct2};
        
        Pricing_Line__c testPricingLine = Test_CC_FitOutCustomCode_DataFactory.getTestPricingLine(testFineProduct.Id,'Other');
        
        Pricing_Line__c testPricingLine2 = Test_CC_FitOutCustomCode_DataFactory.getTestPricingLine(testFineProduct2.Id,'Other Two');
        
        insert new List<Pricing_Line__c>{testPricingLine,testPricingLine2};
        
        SR_Price_Item__c testSrPriceLineItem 
                = Test_CC_FitOutCustomCode_DataFactory.getTestSrPriceItem(objSR.Id,testPricingLine.Id,testFineProduct.Id);
                
        testSrPriceLineItem.Status__c = 'Consumed';
        
        SR_Price_Item__c testSrPriceLineItem2 
                = Test_CC_FitOutCustomCode_DataFactory.getTestSrPriceItem(objSR.Id,testPricingLine2.Id,testFineProduct2.Id);
                
        testSrPriceLineItem2.Status__c = 'Consumed';
            
        insert new List<SR_Price_Item__c>{testSrPriceLineItem,testSrPriceLineItem2}; // create the price line item
        
        list<Amendment__c> lstAmds = new list<Amendment__c>();
        Amendment__c objAmd = new Amendment__c();
        objAmd.RecordTypeId = mapRecordType.get('Mortgage_Unit');
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.Amendment_Type__c = 'Mortgage Registration';
        objAmd.Address3__c = 'Test address';
        objAmd.Unit__c = objUnit.Id;
        objAmd.Status__c = 'Active';
        lstAmds.add(objAmd);
        
        objAmd = new Amendment__c();
        objAmd.RecordTypeId = mapRecordType.get('Individual_Mortgager');
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.Amendment_Type__c = 'Individual Mortgager';
        objAmd.Unit__c = objUnit.Id;
        objAmd.Status__c = 'Active';
        objAmd.Date_of_Birth__c = system.today().addYears(-20);
        objAmd.Nationality_list__c = 'India';
        objAmd.Given_Name__c = 'Test';
        objAmd.Family_Name__c = 'Class';
        lstAmds.add(objAmd);
        
        objAmd = new Amendment__c();
        objAmd.RecordTypeId = mapRecordType.get('Company_Mortgager');
        objAmd.ServiceRequest__c = objSR.Id;
        //objAmd.Amendment_Type__c = 'Company Mortgager';
        objAmd.Amendment_Type__c = 'Off Plan Mortgage';
        objAmd.Unit__c = objUnit.Id;
        objAmd.Status__c = 'Active';
        objAmd.CL_Certificate_No__c = '1234567';
        objAmd.IssuingAuthority__c = 'Others';
        objAmd.Issuing_Authority_Other__c = 'DMCC';
        objAmd.Company_Name__c = 'TRest Com';
        lstAmds.add(objAmd);
        
        insert lstAmds;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.SR__r = objSR;
        
        insert Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Approved','APPROVED');
        
        testStatuses = new List<Status__c>();
        
        testStatuses.add(new Status__c(Name='Closed',Code__c='CLOSED'));
        testStatuses.add(new Status__c(Name='Document Ready',Code__c='DOCUMENT_READY'));
        testStatuses.add(new Status__c(Name='Push To Sap',Code__c='PUSH_TO_SAP'));
        
        insert testStatuses;
        
        RORPMortgageCustomCode.ValidateMortgageRegistration(objStep);
        RORPMortgageCustomCode.UpdateAmendments(objStep);
        
        testSr = objSR;
    }
    
    static testMethod void myUnitTest1() {
        
        executesrValidationsTest();
        
        List<Step_Template__c> testStepTemplates = new List<Step_Template__c>();
        
        testStepTemplates.add(new Step_Template__c(Code__c = 'Code 1',Name='New Document Generation & Registrar Signature',Step_RecordType_API_Name__c = 'Company_Closure_Information'));
        testStepTemplates.add(new Step_Template__c(Code__c = 'Code 2',Name='Registrar Approval',Step_RecordType_API_Name__c = 'Company_Closure_Information'));
        
        insert testStepTemplates;
        
        Test.startTest();
        
        testStepPriceItemStatus(testStepTemplates[0].Id,null);
        
        testStepPriceItemStatus(testStepTemplates[1].Id,null);

        Test.stopTest();
    }
    
    static testMethod void myUnitTest2(){
        
        executesrValidationsTest();
        
        Step_Template__c testStepTemplate = new Step_Template__c(Code__c = 'Code 3',Name='Check SR',Step_RecordType_API_Name__c = 'Company_Closure_Information');
        
        insert testStepTemplate;
        
        testSr.RecordTypeId = mapRecordType.get('Company_Temporary_Work_Permit_Request_Letter');
        
        update testSr;
        
        Test.startTest();
        
        testStepPriceItemStatus(testStepTemplate.Id,testStatuses[2].Id);
        
        Test.stopTest();
    }
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        list<Building__c> lstBuilding = new list<Building__c>();
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP On Plan';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.SAP_Building_No__c = '123456';
        lstBuilding.add(objBuilding);
        
        objBuilding = new Building__c();
        objBuilding.Name = 'RORP Off Plan';
        objBuilding.Building_No__c = '000002';
        objBuilding.Company_Code__c = '5300';
        objBuilding.SAP_Building_No__c = '1234567';
        //objBuilding.Permit_Date__c = system.today().addMonths(-1);
        //objBuilding.Building_Permit_Note__c = 'test class data';
        lstBuilding.add(objBuilding);
        
        insert lstBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c =  lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Residential';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[1].Id;
        objUnit.Building_Name__c = 'RORP Off Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c =  lstBuilding[1].Id;
        objUnit.Building_Name__c = 'RORP Off Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Residential';
        lstUnits.add(objUnit);
        insert lstUnits;
        
        mapRecordType = new map<string,string>();

        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Mortgage_Registration','Discharge_Mortgage','Mortgage_Variation','RORP_Account')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        list<Lease__c> lstLeases = new list<Lease__c>();
        list<Tenancy__c> lstTens = new list<Tenancy__c>();
        
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Start_date__c = system.today().addMonths(13);
        objLease.End_Date__c = system.today().addMonths(25);
        objLease.Status__c = 'Furure Lease';
        objLease.Type__c = 'Leased';
        lstLeases.add(objLease);
        //insert lstLeases;
        
        Tenancy__c objTen = new Tenancy__c();
        objTen.Lease__c = lstLeases[0].Id;
        objTen.Unit__c = lstUnits[0].Id;
        objTen.Start_Date__c = system.today().addMonths(13);
        objTen.End_Date__c = system.today().addMonths(25);
        lstTens.add(objTen);
        //insert lstTens;
        
        Product2 objProduct = new Product2();
        objProduct.Name = 'Mortgage Registration';
        objProduct.IsActive = true; 
        insert objProduct;
        
        list<Pricing_Line__c> lstPricingLines = new list<Pricing_Line__c>();
        Pricing_Line__c objPricingLine = new Pricing_Line__c();
        objPricingLine.Name = 'Caveat Registration';
        objPricingLine.Product__c = objProduct.Id;
        objPricingLine.Active__c = true;
        objPricingLine.Priority__c = 12;
        lstPricingLines.add(objPricingLine);
        
        objPricingLine = new Pricing_Line__c();
        objPricingLine.Name = 'Islamic Mortgage';
        objPricingLine.Product__c = objProduct.Id;
        objPricingLine.Active__c = true;
        objPricingLine.Priority__c = 12;
        lstPricingLines.add(objPricingLine);
        
        objPricingLine = new Pricing_Line__c();
        objPricingLine.Name = 'Conventional Registration';
        objPricingLine.Product__c = objProduct.Id;
        objPricingLine.Active__c = true;
        objPricingLine.Priority__c = 12;
        lstPricingLines.add(objPricingLine);
        
        insert lstPricingLines;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        for(Pricing_Line__c objPL : lstPricingLines){
            Dated_Pricing__c objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objPL.Id;
            objDP.Unit_Price__c = 1200;
            objDP.Appllication_Type__c = 'Add';
            objDP.Date_From__c = system.today().addYears(-2);
            objDP.Use_List_Price__c = true;
            lstDP.add(objDP);
        }
        insert lstDP;
        
        test.startTest();
            
            Apexpages.currentPage().getParameters().put('RecordType',mapRecordType.get('Mortgage_Registration'));
            Apexpages.currentPage().getParameters().put('isDetail','false');
            
            Apexpages.Standardcontroller objSCon = new Apexpages.Standardcontroller(new Service_Request__c());
            RORPMortgageRegistration objRegistration = new RORPMortgageRegistration(objSCon);
            
            objRegistration.AddNewUnits();
            objRegistration.BuildingChange();
            objRegistration.BuildingId = lstBuilding[0].Id;
            objRegistration.SearchString = '12';
            objRegistration.SearchUnits();
           //objRegistration.SelectMortgagorTypeSec();
        
            Service_Request__c objSR = new Service_Request__c();
            objSR.RecordTypeId = mapRecordType.get('Mortgage_Registration');
            objSR.Customer__c = objAccount.Id;
            insert objSR;
            RORPMortgageRegistration.AddNewPriceItem(objSR.id,objPricingLine);
            objRegistration.insertPriceItems(objSR);
            
            if(objRegistration.AllUnits != null && objRegistration.AllUnits.size() > 0){
                objRegistration.AllUnits[0].isChecked = true;
                objRegistration.AddUnits();
            }
            
            objRegistration.CancelUnits();
            objRegistration.AddToMortgage();
            objRegistration.ServiceRequest.Customer__c = objAccount.Id;
            objRegistration.ServiceRequest.Start_Date__c = system.today().addDays(1);
            objRegistration.ServiceRequest.End_Date__c = system.today().addYears(-3);
            objRegistration.MortgageUnit.Amendment.Unit_Rent__c = 120000;
            objRegistration.MortgageUnit.Amendment.Nature_of_Mortgage__c = 'Other';
            
            objRegistration.AddToMortgage();
            objRegistration.ServiceRequest.Start_Date__c = system.today().addDays(-1);
            objRegistration.ServiceRequest.End_Date__c = system.today().addYears(3);
            objRegistration.ServiceRequest.Degree_of_Mortgage__c = 'Primary';
            objRegistration.ServiceRequest.Type_of_Mortgage__c = 'Islamic';
            objRegistration.MortgageUnit.Amendment.Unit_Rent__c = 120000;
            objRegistration.MortgageUnit.Amendment.Nature_of_Mortgage__c = 'Freehold';
            
            objRegistration.AddToMortgage();
            
            objRegistration.SelectMortgagorType();
            
            objRegistration.MortgagorType = 'Individual';
            objRegistration.SelectMortgagorType();
            
            objRegistration.IndividualMortgagor.Title_new__c = 'Mr.';
            objRegistration.IndividualMortgagor.Given_Name__c = 'test';
            objRegistration.IndividualMortgagor.Family_Name__c = 'class';
            objRegistration.IndividualMortgagor.Date_of_Birth__c = system.today().addYears(-20);
            objRegistration.IndividualMortgagor.Gender__c = 'Male';
            objRegistration.IndividualMortgagor.Nationality_list__c = 'India';
            objRegistration.IndividualMortgagor.Passport_No__c = '123456';
            objRegistration.IndividualMortgagor.Passport_Issue_Date__c = system.today().addDays(-1);
            objRegistration.IndividualMortgagor.Passport_Expiry_Date__c = system.today().addYears(10);
            objRegistration.IndividualMortgagor.Country_of_Issuance__c = 'India';
            objRegistration.IndividualMortgagor.Place_of_Birth__c = 'India';
            for(RORPMortgageRegistration.UnitDetails objUD : objRegistration.MortgageUnits){
                objUD.Amendment.Building2__c = lstBuilding[0].Id;
            }
            objRegistration.SaveSR();
            objRegistration.EditRequest();
            objRegistration.RowIndex = 0;
            objRegistration.EditAmendment();
            objRegistration.CancelMortgage();
            
            objRegistration.ContextObj.IsDetail = false;
            objRegistration.CancelRequest();
            
            Apexpages.currentPage().getParameters().put('id',objRegistration.ServiceRequest.Id);
            objRegistration = new RORPMortgageRegistration(objSCon);
            objRegistration.ServiceRequest.Required_Docs_not_Uploaded__c = false;
            objRegistration.SubmitRequest();
            
            objRegistration.RowIndex = 0;
            objRegistration.ServiceRequest.Type_of_Mortgage__c = 'Conventional';
            objRegistration.EditAmendment();
            
            objRegistration.ServiceRequest.Submitted_Date__c = system.today();          
            objRegistration.AddToMortgage();
            objRegistration.SaveSR();
            
            RORPMortgageCustomCode.CreateSAPAttributes(objRegistration.ServiceRequest.Id);
            
            objRegistration.RowIndex = 0;
            objRegistration.RemoveUnits();
            
            RORPMortgageRegistration.UnitDetails objUnitDetails = new RORPMortgageRegistration.UnitDetails();
            objUnitDetails.MortgageDegree = '';
            objUnitDetails.MortgageType = '';
            objUnitDetails.MortgageNature = '';
            objUnitDetails.MortgageValue = null;
            
            objRegistration.ContextObj.Description = '';
            objRegistration.ContextObj.Type = '';
            objRegistration.ContextObj.SapBPNo = '';
            
            RORPMortgageUserContext.getSRDescription('Mortgage_Registration');
            //objRegistration.SelectMortgagorTypeSec();
            RORPMortgageRegistration.getPircingLines('ProductName');
           // RORPMortgageRegistration.AddNewPriceItem('srId',objPricingLine);
        test.stopTest();
        
    }
    
    @future
    private static void testStepPriceItemStatus(String templateId, String statusId){
        
        List<Id> idList = new List<Id>{testSr.Id};
        
        Step__c objStep = new Step__c();
        objStep.SR__c = testSr.Id;
        objStep.Step_Template__c = templateId;
        
        if(String.isNotBlank(statusId)) objStep.Status__c = statusId;
        
        insert objStep;
        
        try {
            SRValidations.StepPriceItemStatusChange(idList);
        } catch (Exception e){
            
        }
    }
}