@isTest
public class PendingActionsLightningControllerTest {
    public static testmethod void test1(){
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insert insertNewAccounts;
        
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insert insertNewSRs;
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
        listSRSteps[0].HexaBPM__Step_No__c = 1;
        insert listSRSteps;
        
        List<group> queues = [SELECT id from group where type='queue' and name = 'Customer'];
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(1, insertNewSRs, listSRSteps);
        insert insertNewSteps;
        insertNewSteps[0].ownerid = queues[0].id;
        update insertNewSteps;
        
        string conRT;
        for (RecordType objRT: [select Id, Name, DeveloperName from RecordType where DeveloperName = 'GS_Contact' AND SobjectType = 'Contact']){
                 conRT = objRT.Id;
            }
           
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = insertNewAccounts[0].Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
        
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c = 'GS';
        insert objTemplate1;
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = insertNewAccounts[0].Id;
        objSR1.Email__c = 'testsr123@difc.com';
        objSR1.Contact__c = objContact.Id;
        objSR1.Express_Service__c = false;
        objSR1.SR_Template__c = objTemplate1.Id;
        objSR1.Pre_GoLive__c = false;
        objSR1.Statement_of_Undertaking__c = true;
        insert objSR1;
        
         Status__c objAwaiting = new Status__c();
        objAwaiting.Name = 'AWAITING_REVIEW';
        objAwaiting.Type__c = 'Start';
        objAwaiting.Code__c = 'AWAITING_REVIEW';
        insert objAwaiting;
        
        step__c objStep = new Step__c();
        objStep.SR__c = objSR1.Id;
        objStep.Biometrics_Required__c = 'No';
       // objStep.Step_Template__c = objStepType1.id;
        objStep.No_PR__c = false;
        objStep.status__c = objAwaiting.id;
        objStep.AppointmentCreation__c = 'Create';
        objStep.OwnerId = userinfo.getUserId();
        objStep.SR_Group__c = 'RORP'; 
        insert objStep;

        //Compliance Custom Setting Records
        OB_Compliance_Notification_Link__c complianceLink = new OB_Compliance_Notification_Link__c();
        complianceLink.Name = 'Appointing an Auditor';
        complianceLink.OB_Link__c = '/test';
        insert complianceLink;

        //inserting compliance for accounts
        Compliance__c complianceRecord = new Compliance__c();
        complianceRecord.Account__c = insertNewAccounts[0].Id;
        complianceRecord.Status__c = 'Open';
        insert complianceRecord;
        
        Test.startTest();
           Id p = [select id from profile where name='DIFC Customer Community User Custom'].id;      
            Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id,Role__c='Mortgage Registration');
            insert con;  
                      
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = con.Id, Community_User_Role__c = 'Fit-Out Services',
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
        	objStep.OwnerId = user.id;
        	update objStep;
        system.runAs(user){
            List<HexaBPM__Step__c> NewSteps = [SELECT id,HexaBPM__SR__r.HexaBPM__Customer__c,Is_Client_Step__c,HexaBPM__Status_Type__c From HexaBPM__Step__c];
            system.debug('listSRSteps'+ listSRSteps[0].ownerid);
            system.debug('NewSteps==>'+NewSteps);
            PendingActionsLightningController.getNextStep();
            //PendingActionsLightningController.getPendingApproval(1);
            PendingActionsLightningController.getCurrentAccountId();
            try{
                PendingActionsLightningController.createCPRenewal();
                PendingActionsLightningController.createCPRenewalService();
            }catch(Exception e){
                
            }
        }
       Test.stopTest();
    }
}