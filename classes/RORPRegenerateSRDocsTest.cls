@isTest  
public class RORPRegenerateSRDocsTest {
    public static testMethod void TestMethod1(){
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;
       
        Account accountObj = new Account ();
        accountObj.Name='Test Public Account Limited';
        accountObj.Trade_Name__c = 'Test Public Account Limited';
        Insert accountObj;
        
        Contact con = new Contact(LastName ='testCon',AccountId = accountObj.Id);
        insert con;  
        
        SR_Status__c srStatus = new SR_Status__c(Name ='Draft',Code__c='DRAFT');
        insert srStatus;
        Test.Starttest();
        Date myDate = Date.today();
        Service_request__c servicerequestObj = new Service_request__c ();
        
        //Time myTime2 = Time.newInstance(9, 0, 0, 0);
        //DateTime dt2 = DateTime.newInstanceGMT(myDate, myTime2);
        datetime dt2 = datetime.now();
        dt2=dt2.addHours(-1);
        //Time myTime = Time.newInstance(8, 0, 0, 0);
        //DateTime dt1 = DateTime.newInstanceGMT(myDate, myTime);
        DateTime dt1=datetime.now();
        servicerequestObj.CreatedDate=dt2;
        servicerequestObj.LastModifiedDate=dt2;
        servicerequestObj.RecordTypeId=Schema.SObjectType.Service_request__c.getRecordTypeInfosByDeveloperName().get('Freehold_Transfer_Registration').getRecordTypeId();
        servicerequestObj.External_SR_Status__c = srStatus.id;
        insert servicerequestObj;
        
        Amendment__c amendmentObj = new Amendment__c (ServiceRequest__c=servicerequestObj.id,Amendment_Type__c ='Seller',Passport_No__c='a3',Family_Name__c='Test',Given_Name__c='User',Job_Title__c='Developer',Nationality_list__c='United Arab Emirates',CreatedDate=dt1,LastModifiedDate=dt1);
        
        SR_Doc__c SRDocsObj = new SR_Doc__c();
        SRDocsObj.Status__c='Generated';
        SRDocsObj.Sys_IsGenerated_Doc__c=true;
        SRDocsObj.Service_Request__c=servicerequestObj.Id;
         SRDocsObj.CreatedDate=dt1;
        SRDocsObj.LastModifiedDate=dt1;
        
        insert amendmentObj;
        insert SRDocsObj;
        
        PageReference pageRef = Page.RORPRegenerateSRDocs;
        
        Test.setCurrentPage(pageRef);
        
        pageRef.getParameters().put('RecordType',servicerequestObj.RecordTypeId);
        
        update amendmentObj;
        update servicerequestObj;
        Apexpages.StandardController sc = new Apexpages.StandardController(servicerequestObj);
        RORPRegenerateSRDocs ext = new  RORPRegenerateSRDocs(sc);         
        ext.regenerateSRDocs();
        ext.RecordTypeId=servicerequestObj.RecordTypeId;
        ext.objSR=servicerequestObj;
        Test.StopTest();
        
    }
    
}