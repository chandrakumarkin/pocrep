/*********************************************************************************************************************
*  Name     : ServiceRequestsAddressChangeBatch 
*  Author     : Ananya Acharya
*  Purpose   : This is to identify all approved service request and create certificate for child customers.
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
V1.0          Ananya                                          15/06/2021
//questions
////1? Which approved Service Request records will be picked up: Today, LAST_N_DAYS, ?
//2? we need to store any information for failure to track failed for processed records to be picked in next batch run        
*/
global class ServiceRequestsAddressChangeBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    public string query{get;set;}
    Map<Id,Service_Request__c> insertedMap = new Map<Id,Service_Request__c>();
    Set<Id> totalServiceRequestIds = new Set<Id>();
    Map<Id,Id> serviceRequestCustomerMap = new Map<Id,Id>();
    Map<Id,Set<Id>> accountContactMap = new Map<Id,Set<Id>>();
                
    global ServiceRequestsAddressChangeBatch(){
        if(test.isRunningTest()){
            // Get only those approved service request whose client is parent company and record type = change of address
            query = 'SELECT Id, Name, RecordTypeId,External_Status_Name__c,Customer__c,Customer__r.Hosting_Company__c FROM Service_Request__c'
                +' WHERE createddate  = today';
            
        }
        else{
            // Get only those approved service request whose client is parent company and record type = change of address
            query = 'SELECT Id, Name, RecordTypeId,External_Status_Name__c,Customer__c,Customer__r.Hosting_Company__c FROM Service_Request__c'
                +' where Recordtype.DeveloperName = \'Change_of_Address_Details\''
                +' and External_Status_Name__c = \'Approved\'' 
                +' and Customer__r.Hosting_Company__c = null ' 
                +' and Recordtype.DeveloperName = \'Change_of_Address_Details\' and Completed_Date_Time__c  = today';
            
        }
        this.query = query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> dataList) {
        try {
            //init Get all relevant status codes
            Map<string,string> srStatusMap = new map<string,string>(); // Map with status code as Key and Id as Value ** SR Statuses **
            for(SR_Status__c srStatus: [select id,code__c from SR_Status__c where Code__c IN  ('SUBMITTED','DRAFT','APPROVED')])
                srStatusMap.put(srStatus.code__c,srStatus.id);
            
            
            //1. Get Parent Child Account Relations for the service request 
            Map<Id, List<Account>> parentChildAccountMap = getParentChildClientMap((List<Service_Request__c>)dataList);
            if(parentChildAccountMap!=null && parentChildAccountMap.size()>0){
                
                //2. for every child account, create service request of SR Template to create Certificates
                List<Service_Request__c> newChildSRList = new List<Service_Request__c>();
                SR_Template__c SR_Template=[select id,SR_RecordType_API_Name__c from  SR_Template__c where SR_RecordType_API_Name__c='Company_conversion_Certificate_for_creation_of_Commercial_License_for_Hosting_co'];
                if(SR_Template!=null){
                    for(Id parentClientId: parentChildAccountMap.keyset()){
                        for(Account childClient: parentChildAccountMap.get(parentClientId)){
                            service_request__c serviceRequest = new Service_Request__c(SR_group__c='ROC'); // 
                            serviceRequest.SR_Template__c =  SR_Template.id;
                            serviceRequest.Customer__c = childClient.id;
                            serviceRequest.Legal_Structures__c=childClient.Legal_Type_of_Entity__c;
                            serviceRequest.License_Number__c = childClient.Active_License__r.Name ;
                            serviceRequest.Current_License_Expiry_Date__c = childClient.Active_License__r.License_Expiry_Date__c;
                            serviceRequest.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Hosting company change update').getRecordTypeId();
                            serviceRequest.internal_SR_Status__c = srStatusMap.get('DRAFT');
                            serviceRequest.External_sr_status__c =srStatusMap.get('DRAFT');
                            serviceRequest.finalizeAmendmentFlg__c=true;
                            serviceRequest.Process_Completed__c=true;
                            
                            serviceRequest.Completed_Date_Time__c=DateTime.now();
                            //serviceRequest.Internal_SR_Status__r.Name = 'APPROVED';
                            //serviceRequest.Currency_Rate__c='a0E2000000rIql7';
                            serviceRequest.Submitted_Date__c = Date.Today(); 
                            serviceRequest.Submitted_DateTime__c = dateTime.Now();
                            newChildSRList.add(serviceRequest);
                        }
                    }
                }
                
                //3. Insert Service Request and track failures to process it in next batch 
                if(newChildSRList.size()>0){
                    Database.SaveResult [] results = Database.insert(newChildSRList , false);
                    for(Integer j=0;j<results.size();j++){
                    newChildSRList[j].internal_SR_Status__c = srStatusMap.get('SUBMITTED');
                    newChildSRList[j].External_sr_status__c = srStatusMap.get('SUBMITTED');
                    Database.SaveResult [] results1 = Database.Update(newChildSRList , false);
                    }
                    for(Integer i=0;i<results.size();i++){
                        if (results.get(i).isSuccess()){
                            Id insertedId = results.get(i).getId();
                            Service_Request__c request = newChildSRList.get(i);
                            insertedMap.put(insertedId,request);
                            totalServiceRequestIds.add(insertedId);
                        }
                        else if(!results.get(i).isSuccess()){
                            // DML operation failed
                            Database.Error error = results.get(i).getErrors().get(0);
                            String failedDML = error.getMessage();
                            //store error log for failed records
                            system.debug(newChildSRList.get(i));
                        }
                    }
                }
            }
        } catch(Exception e) {
            System.debug(e);
        } 
    }
    
    public Map<Id, List<Account>> getParentChildClientMap(List<Service_Request__c> requestList){
        Map<Id, List<Account>> childAccountMap = new Map<Id, List<Account>>();
        
        for(Service_Request__c request : requestList){
            if(request.Customer__r.Hosting_Company__c==null && request.Customer__c!=null)   
                serviceRequestCustomerMap.put(request.Id,request.Customer__c);
        }
        
        if(serviceRequestCustomerMap.size() > 0){   
            totalServiceRequestIds.addAll(serviceRequestCustomerMap.keySet());
            Map<Id,Account> parentAccountMap = new Map<Id,Account>([SELECT Id, PO_Box__c, 
                                                                    (SELECT Id,Name,Email,Phone,AccountId FROM Contacts where RecordType.DeveloperName = 'Portal_User' and Account.ROC_Status__c IN ('Active','Not Renewed')) 
                                                                    from Account where Id in:serviceRequestCustomerMap.values() And ROC_Status__c='Active']);
            
            for (Account childaccount: [SELECT ID,Hosting_Company__c,Legal_Type_of_Entity__c,
                                        Active_License__r.Name,Active_License__r.License_Expiry_Date__c,ROC_Status__c,
                                        (SELECT Id,Name,Email,Phone,AccountId FROM Contacts where RecordType.DeveloperName = 'Portal_User' and Account.ROC_Status__c IN ('Active','Not Renewed'))  
                                        from Account where Hosting_Company__c in: serviceRequestCustomerMap.values() And ROC_Status__c='Active']){
                if(childAccountMap.get(childaccount.Hosting_Company__c)==null)
                    childAccountMap.put(childaccount.Hosting_Company__c,new List<Account>{childaccount});
                else
                    childAccountMap.get(childaccount.Hosting_Company__c).add(childaccount);
            }
            
            for(Id parentAccountId: childAccountMap.keyset()){
                /*Parent Account and related contacts
                if(parentAccountMap.containsKey(parentAccountId)){
                    for(Contact parentContact: parentAccountMap.get(parentAccountId).Contacts){
                        if(accountContactMap.get(parentAccountId)==null)
                            accountContactMap.put(parentAccountId,new Set<Id>{parentContact.Id});
                        else
                            accountContactMap.get(parentAccountId).add(parentContact.Id);
                    }
                }*/
                //Child Account and related contact
                for(Account childAccount: childAccountMap.get(parentAccountId)){
                    for(Contact contactRec: childAccount.Contacts){
                        if(accountContactMap.get(childAccount.Id)==null)
                            accountContactMap.put(childAccount.Id,new Set<Id>{contactRec.Id});
                        else
                            accountContactMap.get(childAccount.Id).add(contactRec.Id);
                    }
                }
            }
        }
        return childAccountMap;
    }
    
    public void sendLicenseGenerationNotification(Map<Id,Set<Id>> accountContactMap,List<string> userEmails,string orgWideAddress, string templateId,list<Messaging.Emailfileattachment> emailAttachments){
        OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address =: orgWideAddress];
        list<Messaging.SingleEmailMessage> singleMailingList = new list<Messaging.SingleEmailMessage>();
        for(Id requestId : insertedMap.keySet()){
            for(Id contactId: accountContactMap.get(insertedMap.get(requestId).Customer__c)){
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                if(userEmails.size()>0)
                    mail.setCCAddresses(userEmails);
                mail.setReplyTo('portal@difc.ae');
                if(templateId!=null && templateId!=''){
                    mail.setWhatId(requestId);
                    mail.setTemplateId(templateId);
                    mail.setTargetObjectId(contactId);
                    if(lstOrgEA!=null && lstOrgEA.size()>0)
                        mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
                    if(!emailAttachments.isEmpty())
                        mail.setFileAttachments(emailAttachments);
                    singleMailingList.add(mail);
                }     
            }
        }
        if(singleMailingList.size()>0){
            Messaging.sendEmail(singleMailingList);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        //Send out notifications
        if(accountContactMap.size()>0){
            //1. Get Queue and ROC_Office emails
            List<string> userEmails = new List<string>();
            for(User objUser : [select Id,Email from User where Id IN (select UserOrGroupId from GroupMember where Group.DeveloperName='ROC_Office' AND Group.Type='Queue')]){
                userEmails.add(objUser.Email);
            }
            
            //2. Get Attachments
            list<Messaging.Emailfileattachment> emailAttachments = new list<Messaging.Emailfileattachment>();
            list<string> srDocIds = new list<string>();
            for(SR_Doc__c objSRDoc : [select Id,Doc_ID__c, Document_Master__c, Document_Master__r.Code__c from SR_Doc__c where Service_Request__c=:totalServiceRequestIds AND Document_Master__r.Code__c IN ('E-Commercial License')])
                srDocIds.add(objSRDoc.Doc_ID__c);
            /*for(Attachment attach : [select Id,Name,Body,ContentType from Attachment where Id IN : srDocIds order by CreatedDate desc]){
                Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                string contentType = attach.Name.indexOf('.') > 0 ? '.'+attach.Name.substring(attach.Name.lastIndexOf('.'),attach.Name.length()) : '';
                efa.setFileName('E-Commercial License'+contentType);
                efa.setBody(attach.Body);
                emailAttachments.add(efa);
            } */
           
            //3. trigger notifications
            sendLicenseGenerationNotification(accountContactMap,userEmails,'portal@difc.ae',[select Id from EmailTemplate where DeveloperName='License_Updation_Notification_Notification'].Id,emailAttachments);
        }
            
    }
}