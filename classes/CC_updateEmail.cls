/*
    Author      : Prateek Kadkol
    Date        : 10-FEB-2020
    Description : This upadtes the email in the related applications
    -------------------------------------------------------------------------------------------------
*    Version    Author    Date           Comment
*  ------------------------------------------------------------------------------------------------
*     v1.1      Suchita  6-Jan-2021     Code to update the role and access level for employee services
*/

global without sharing class CC_updateEmail implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp)
    {
        system.debug('@@@@@@@@@ entered into CC_UpdateContactOnApporvalsss');
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
            try{
                list<HexaBPM__Service_Request__c> updateSrList =  new list<HexaBPM__Service_Request__c>();
                list<AccountContactRelation> updateACR =  new list<AccountContactRelation>(); //12450
                if(stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Converted_User_Registration'){

                    for(account accObj : [select id,(select id from HexaBPM__Service_Requests__r WHERE id !=: stp.HexaBPM__SR__c)
                    from account WHERE id =: stp.HexaBPM__SR__r.HexaBPM__Customer__c  ]){
                        if(accObj.HexaBPM__Service_Requests__r != null && accObj.HexaBPM__Service_Requests__r.size() > 0){
                            for(HexaBPM__Service_Request__c relSr : accObj.HexaBPM__Service_Requests__r){
                                relSr.HexaBPM__Email__c = stp.HexaBPM__SR__r.HexaBPM__Email__c;
                                updateSrList.add(relSr);
                            }
                        }
                    }

                    contact conToUpdate = new contact(id = stp.HexaBPM__SR__r.HexaBPM__Contact__c);
                    conToUpdate.Email = stp.HexaBPM__SR__r.HexaBPM__Email__c;
                    conToUpdate.Does_the_individual_reside_in_the_UAE__c     = stp.HexaBPM__SR__r.Are_You_Resident_Of_UAE__c;
                    conToUpdate.Does_the_individual_have_a_valid_UAE_res__c  = stp.HexaBPM__SR__r.Valid_UAE_residence_visa__c;
                    conToUpdate.Address_Line_1_HC__c                         = stp.HexaBPM__SR__r.Address_Line_1__c;
                    conToUpdate.Address_Line_2_HC__c                         = stp.HexaBPM__SR__r.Address_Line_2__c;
                    conToUpdate.CITY1__c                                     = stp.HexaBPM__SR__r.City_Town__c;
                    conToUpdate.Emirate_State_Province__c                    = stp.HexaBPM__SR__r.State_Province_Region__c;
                    conToUpdate.COUNTRY_ENGLISH2__c                          = stp.HexaBPM__SR__r.Country__c;
                    conToUpdate.PO_Box__c                                    = stp.HexaBPM__SR__r.Po_Box_Postal_Code__c;
                    update conToUpdate;

                    for(user customerUser : [select id from user where contactId =:stp.HexaBPM__SR__r.HexaBPM__Contact__c LIMIT 1]){
                        customerUser.Email = stp.HexaBPM__SR__r.HexaBPM__Email__c;
                        update customerUser;
                    }
                    if(updateSrList.size() > 0 ){
                    System.debug('before update: '+updateSrList);
                        update updateSrList;
                    }
                    
                    //Logic to update Account Contact Relationship role Ticket Number 12450 
                    if(stp.HexaBPM__SR__r.Access_to_Employee_Services__c!=null && stp.HexaBPM__SR__r.Access_to_Employee_Services__c=='Yes'){
                        for(AccountContactRelation acrList : [select Id,accountid,Roles from AccountContactRelation where accountid=:stp.HexaBPM__SR__r.HexaBPM__Customer__c and IsActive=true and ContactId=:stp.HexaBPM__SR__r.HexaBPM__Contact__c and IsDirect=true LIMIT : (Limits.getLimitQueryRows()-Limits.getQueryRows())]){acrList.Roles=acrList.Roles+';Employee Services';
                            updateACR.add(acrList);
                        }
                    }
                    if(updateACR.size() >0){
                        update updateACR;
                    }
                    // End 12450
                }

            }catch(DMLException e){
                /*
                    string DMLError = e.getdmlMessage(0)+'';
                    if(DMLError==null){
                        DMLError = e.getMessage()+'';
                    }
                    strResult = DMLError;
                */
                strResult =  e.getdmlMessage(0)+''; 
            }
        }
        return strResult;
    }   
}