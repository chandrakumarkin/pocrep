/**
    *Author      : Rajil
    *CreatedDate : 10-11-2019
    *Description : This class is used to retrieve the next steps for Digital Onboarding Home Page and the pending actions for the community portal user.
* Modification History:
*****************************************************************************************************************
* Version       Date                Author              Remarks                                                 
* 2.0           20/03/2020          Maruf               Added Profile Name in ResponseWrapper and getNextStep
* 2.1           04/March/202        Arun                Added code to support any document upload step 
* 2.2           13/June/2020        Arun                Added code to #10270 
* 2.3           27th-July-2020      Mudasir             #DIFC2-8425 - Fitout Client satisfaction process update 
* 2.4           13/Dec/2020         Arun                #12654 Added code to show pending action iteams
* 2.5			15/Feb/2021			Shikha				#3758 - Added code to redirect user to Document Viewer for submitting certificate
														of continuation for transfer from DIFC
*****************************************************************************************************************
**/
public with sharing class PendingActionsLightningController {
    
    @AuraEnabled
    public static ResponseWrapper getNextStep() 
    {
        ResponseWrapper respWrap = new ResponseWrapper();

        String userId = Userinfo.getUserId();
        String CommunityUserRoles = '';
        String userAccountId = '';
        String accId = '';
        String contractorId = '';
        System.debug('The user ID: ' + userID);
        
        //v2.2 added SeparateServices
        Boolean SeparateServices=false;
    
        
        for(User usr:[SELECT id,contact.AccountId,
                             contact.contractor__c,
                             Separate_Services__c,//v2.2
                             contact.Role__c,
                             Account_Id__c,
                             Community_User_Role__c,
                             contact.Account.Legal_Type_of_Entity__c,
                             AccountId,
                             contact.OB_Block_Homepage__c,
                             Profile.Name 
                        FROM User 
                       WHERE id =:Userinfo.getUserId()])
        {
            accId =  usr.contact.accountId;
            respWrap.profileName = usr.Profile.Name;
            respWrap.initialAccountId = usr.contact.AccountId;
            
            SeparateServices=usr.Separate_Services__c;
            
            respWrap.displayComponent = ( usr.contact.OB_Block_Homepage__c ? false : true );
            if(usr.contact.Role__c !=null && usr.contact.Role__c!='')
            {
                CommunityUserRoles = usr.contact.Role__c;
            }
            else if(usr.Community_User_Role__c!=null && usr.Community_User_Role__c!='')
            {
                CommunityUserRoles = usr.Community_User_Role__c;
            }
            userAccountId = usr.AccountId;
            if(usr.contact!=null && usr.contact.contractor__c!=null)
            {
              contractorId = usr.contact.contractor__c;
            }

        }
        List<HexaBPM__Step__c> lstNewSteps = new List<HexaBPM__Step__c>();
        List<Step__c> allsteps = new List<Step__c>();
        List<HexaBPM__Step__c> allNewSteps = new List<HexaBPM__Step__c>();
        list<PendingApproval> lstPendingApproval = new list<PendingApproval>();
        
        //accId  = '0013N00000AZ5JnQAL';
        //userAccountId  = '0013N00000AZ5JnQAL';
        //CommunityUserRoles = 'Company Services';
        
        String newStep = 'SELECT id, HexaBPM__Status__c,Entity_Action_URL__c,'+ 
                        ' HexaBPM__SR__r.Name,HexaBPM__SR__r.HexaBPM__SR_Template__r.Name, HexaBPM__SR__r.HexaBPM__External_Status_Name__c,HexaBPM__Summary__c '+
                        ' From HexaBPM__Step__c ';
        newStep += 'where HexaBPM__SR__r.HexaBPM__Customer__c =\''+userAccountId +'\' '; 
        newStep += ' AND Is_Client_Step__c=true'; 
        newStep += ' AND HexaBPM__Status_Type__c != \'End\' ';
        newStep += ' ORDER BY CreatedDate ASC';
        /*
        if(limitRow != null && limitRow > 0)
            newStep += ' Limit '+limitRow;
        */
        system.debug('newStep----->'+ newStep);
        // else
        //     newStep += ' Limit '+(Limits.getLimitQueryRows()-Limits.getQueryRows()) ;

        //2.1 added Step_Template__r.Step_Type__c //2.3- Added Step_Template__r.Page_Name__c 
        String oldStep = ' SELECT id,  Name,Start_URL__c, Step_Template__r.Code__c,Step_Template__r.Step_Type__c,Step_Template__r.Page_Name__c , Assigned_to_client__c,Step_Name__c,SR_Group__c,';
        oldStep += ' Step_Status__c, CreatedDate, SR__c,SR__r.Name,SR__r.SR_Template__r.Name, SR__r.External_Status_Name__c, Summary__c, ';
        oldStep += ' SR__r.Portal_Service_Request_Name__c  ';
        oldStep += ' from Step__c ';

        if(String.isNotBlank(CommunityUserRoles) && CommunityUserRoles.contains('Fit-Out Services')){
            oldStep += ' where SR__r.contractor__c = \''+contractorId+'\'';
            oldStep += ' AND Owner__c=\'Contractor\' AND Status_Type__c != \'End\''; //AND Status_Type__c != \'End\'
            oldStep += ' AND SR__r.IsCancelled__c = false';
            oldStep += ' AND SR__r.Is_Rejected__c = false';
            oldStep += ' AND SR__r.isClosedStatus__c = false';
            oldStep += ' AND SR__r.External_Status_Name__c != \'Draft\' ';
            oldStep += ' AND SR__r.Record_Type_Name__c  != \'DM_SR\' ';
            //oldStep += ' AND SR__r.Portal_User_Access__c   = true ';
            oldStep += ' AND SR__r.Pre_GoLive__c = false';
            oldStep += ' ORDER BY CreatedDate ASC,SR_Group__c asc';
            /*
            if(limitRow != null && limitRow > 0)
                oldStep += ' Limit '+limitRow;
            */
            // else
            //     oldStep += ' Limit '+(Limits.getLimitQueryRows()-Limits.getQueryRows()) ;
           
        } else{
            oldStep += ' where SR__r.Customer__c = \''+userAccountId+'\'';
            oldStep += ' AND (OwnerId=\''+userId+'\' OR Owner.Name=\'Client Entry User\' OR Owner.Name=\'Client Entry Approver\' OR Assigned_to_client__c =true) ';
            oldStep += ' AND Status_Type__c != \'End\' ';// AND SR__r.Pre_GoLive__c = false ';
            oldStep += ' AND SR__r.IsCancelled__c = false';
            oldStep += ' AND SR__r.Is_Rejected__c = false';
            oldStep += ' AND (SR__r.isClosedStatus__c = false OR (Step_Template__r.Step_Type__c=\'Until Closed\' AND Closed_Date__c=null) ) '; //2.1 
            oldStep += ' AND SR__r.External_Status_Name__c != \'Draft\' ';
            oldStep += ' AND SR__r.Record_Type_Name__c  != \'DM_SR\' ';
            //oldStep += ' AND SR__r.Portal_User_Access__c   = true ';
            oldStep += ' AND SR__r.Pre_GoLive__c = false';
            
            //v2.2
            if(SeparateServices)
            {
                  oldStep += ' AND SR__r.CreatedById =\'' + userID + '\'';
            }
            
            oldStep += ' ORDER BY CreatedDate ASC,SR_Group__c asc';
            /*
            if(limitRow != null && limitRow > 0)
                oldStep += ' Limit '+limitRow;
            */
        }
       system.debug('----newStep-----'+newStep);
       system.debug('----oldStep-----'+oldStep);
        if(String.IsNotBlank(newStep))
            lstNewSteps.addAll((List<HexaBPM__Step__c>)database.query(newStep));
        system.debug('lstNewSteps'+ lstNewSteps);
        
        for(HexaBPM__Step__c obj: lstNewSteps){
            PendingApproval pObj = new PendingApproval();
            pObj.stepId = obj.Id;
            pObj.srName = obj.HexaBPM__SR__r.Name;
            pObj.srType = obj.HexaBPM__SR__r.HexaBPM__SR_Template__r.Name;
            pObj.summary = obj.HexaBPM__Summary__c;
            pObj.url = obj.Entity_Action_URL__c;

            if(pObj.srType != NULL && pObj.srType  == 'Update details / Apply for Commercial Permission Renewal'){
                pObj.isCPRenewal = true;
            }

            lstPendingApproval.add(pObj);
        }
       
        list<Step__c> lstOldSteps = new list<Step__c>();
        if(String.IsNotBlank(oldStep)){
            //lstOldSteps = database.query(oldStep);
            System.debug('oldStep>>>');
            System.debug(oldStep);
            lstOldSteps.addAll((List<Step__c>)database.query(oldStep));
        }
        respWrap.debugger = lstOldSteps;
        System.debug('CommunityUserRoles>>>>');
        System.debug(CommunityUserRoles);
        if(String.isNotBlank(CommunityUserRoles)){
            for(Step__c obj: lstOldSteps){
                System.debug('obj.SR_Group__c>>'+obj.SR_Group__c);
                if((CommunityUserRoles.contains('Company Services') && (obj.SR_Group__c == 'Fit-Out & Events' || obj.SR_Group__c == 'ROC' || obj.SR_Group__c == 'RORP' || obj.SR_Group__c == 'BC'))
                    || (CommunityUserRoles.contains('Company Services') == false && CommunityUserRoles.contains('Property Services') && obj.SR_Group__c == 'RORP')
                    || (obj.SR_Group__c == 'GS' && CommunityUserRoles.contains('Employee Services')  && obj.SR__r.External_Status_Name__c !='Cancelled')
                    || (obj.SR_Group__c == 'IT' && CommunityUserRoles.contains('IT Services'))
                    || (obj.SR_Group__c == 'Listing' && CommunityUserRoles.contains('Listing Services'))
                    || (obj.SR_Group__c == 'Fit-Out & Events' && (CommunityUserRoles.contains('Fit-Out Services') || CommunityUserRoles.contains('Company Services')))
                    || (obj.SR_Group__c == 'Fit-Out & Events' && CommunityUserRoles.contains('Event Services'))
                    || (obj.SR_Group__c == 'Other')
                    || (obj.SR_Group__c =='RORP' && (CommunityUserRoles.contains('Mortgage Registration') || CommunityUserRoles.contains('Discharge of Mortgage') || CommunityUserRoles.contains('Variation of Mortgage')))
                
                ){
                    PendingApproval pObj = new PendingApproval();
                    pObj.stepId = obj.Id;
                    pObj.srName = obj.SR__r.Name;
                    pObj.srType = obj.SR__r.SR_Template__r.Name;
                    pObj.summary = obj.Summary__c;
                    //Shikha - adding for transfer from DIFC
                    if(String.isNotBlank(obj.Step_Template__r.Code__c) && (obj.Step_Template__r.Code__c == 'RE_UPLOAD_DOCUMENT' || 
                        (obj.SR__r.SR_Template__r.Name == 'Transfer from DIFC' && obj.Step_Template__r.Code__c == 'SUBMISSION_OF_CERTIFICATE_OF_CONTINUATION'))){
                        if(obj.SR_Group__c =='Fit-Out & Events'){
                            pObj.url = System.Label.Community_URL + '/apex/FO_DocumentViewer?id='+obj.SR__c+'&stepId='+obj.Id+'&retURL='+System.Label.OB_Community_URL;
                        }else{
                           pObj.url = System.Label.Community_URL + '/apex/DocumentViewer?id='+obj.SR__c+'&retURL='+System.Label.OB_Community_URL; 
                        }
                    }
                    ////2.1 start 
                    else if(obj.Step_Template__r.Step_Type__c=='Upload')
                    {
                         if(obj.SR_Group__c =='Fit-Out & Events'){
                            pObj.url = System.Label.Community_URL + '/apex/FO_DocumentViewer?id='+obj.SR__c+'&stepId='+obj.Id+'&retURL='+System.Label.OB_Community_URL;
                        }else{
                           pObj.url = System.Label.Community_URL + '/apex/DocumentViewer?id='+obj.SR__c+'&retURL='+System.Label.OB_Community_URL; 
                        }
                    }//End
                    //2.3 - Start
                    else if(obj.Step_Template__r.Step_Type__c=='Custom Page')
                    {
                       pObj.url = System.Label.Community_URL + '/apex/'+obj.Step_Template__r.Page_Name__c+'?stepId='+obj.Id;
                    }
                    //2.3 - End
                    else{
                        if(obj.SR_Group__c =='Fit-Out & Events'){
                            pObj.url = System.Label.Community_URL + '/'+pObj.stepId+'?retURL='+System.Label.OB_Community_URL;
                        }else{
                            ////2.1 start 
                            pObj.url = obj.Start_URL__c;//System.Label.Community_URL + '/'+pObj.stepId+'/e?retURL='+System.Label.OB_Community_URL;
                        }
                    }
                        
                    lstPendingApproval.add(pObj); 
                }
            }
        }
        //Setting up Compliance records
        map<string,string> mapComplianceLink = new map<string,string>();
        for(OB_Compliance_Notification_Link__c compliance:[select Name,OB_Link__c from OB_Compliance_Notification_Link__c]){
            mapComplianceLink.put(compliance.Name,compliance.OB_Link__c);
        }
        string endDate;
        respWrap.hasSubmitCp = OB_AgentEntityUtils.checkForSubmittedCPRenwal(accId);
        for(Compliance__c objComp : [select Id,Name,Account__c,Start_Date__c,End_Date__c,Exception_Date__c,Fine__c
                                        from Compliance__c where Account__c=:userAccountId 
                                        AND (Status__c = 'Open' OR Status__c='Defaulted') order by End_Date__c]){
            PendingApproval pObj = new PendingApproval();
            pObj.srType = 'Notification';
            pObj.srName = objComp.Name;
            if(objComp.End_Date__c != null)
                endDate = DateTime.newInstance(objComp.End_Date__c.year(),objComp.End_Date__c.month(),objComp.End_Date__c.day()).format('dd-MM-YYYY');
            pObj.summary = objComp.Name + ' is due on ' + endDate;
            if(objComp.Name != NULL && objComp.Name == 'Commercial Permission Renewal'){
                pObj.isCPRenewal = true;
            }
            if(mapComplianceLink.get(objComp.Name) != null)
                pObj.url = System.Label.OB_SitePrefixURL + mapComplianceLink.get(objComp.Name) +'&retURL='+System.Label.OB_Community_URL;

            if(!(pObj.srName == 'Commercial Permission Renewal' && respWrap.hasSubmitCp == true)){
                lstPendingApproval.add(pObj); 
            }
            
        }  
        
        
        
        // List of Pending Ieams
      //2.4
      
     
      if(!string.isblank(CommunityUserRoles))
      {
           List<String> UserRoleinList = CommunityUserRoles.split(';');
           
            for(Pending_Item__c ObjIteam:[select id,Action_Name__c,Type__c,Details__c,URL__c  from Pending_Item__c where Account__c=:accId and User_Role__c in : UserRoleinList and Status__c='Open'])
            {
                
                PendingApproval pObj = new PendingApproval();
                pObj.stepId = ObjIteam.Id;
                pObj.srName =ObjIteam.Details__c;
                pObj.srType= ObjIteam.Type__c;
                pObj.summary = ObjIteam.Action_Name__c;
                pObj.url = ObjIteam.URL__c;
                lstPendingApproval.add(pObj);
                
            }
       }
        
        
        
        respWrap.lstPendingApproval = lstPendingApproval;
        //return lstPendingApproval;
        system.debug('-----------'+respWrap);
        return respWrap;
    }

    /* ---------------------------------------------------------------------
     * Method name : createCPRenewal
     * description : used to get create cp renewal sr
     * --------------------------------------------------------------------*/
    
    @AuraEnabled
    public static OB_AgentEntityUtils.RespondWrap createCPRenewal(){

        string contactID = '';
        for(User usr:[SELECT id,contactID FROM User  WHERE id =:Userinfo.getUserId()]){
            contactID = usr.contactID;
        }

        if(contactID != ''){
            OB_AgentEntityUtils.RespondWrap respWrap = OB_AgentEntityUtils.createNewCpRenewalSr(contactID,'cp-renewal');
            return respWrap;

            /* if(respWrap.errorMessage != null){
                return respWrap.errorMessage;
            }else{
                return respWrap.pageFlowURl;
            } */
            
        }else{
            return null;
        }
        
        
    }

    @AuraEnabled
    public static OB_AgentEntityUtils.RespondWrap createCPRenewalService(){

        string contactID = '';
        string accID = '';
        for(User usr:[SELECT id,contactID,contact.AccountID FROM User  WHERE id =:Userinfo.getUserId()]){
            contactID = usr.contactID;
            accID = usr.contact.AccountID;
        }

        if(contactID != ''){

            boolean hasOPenCp = OB_AgentEntityUtils.checkForOpenCompliance(accID);
            if(hasOPenCp){
                OB_AgentEntityUtils.RespondWrap respWrap = OB_AgentEntityUtils.createNewCpRenewalSr(contactID,'cp-renewal');
                return respWrap;
            }else{
                OB_AgentEntityUtils.RespondWrap respWrap = OB_AgentEntityUtils.createNewCpRenewalSr(contactID,'update');
                return respWrap;
            }
           
           
        }else{
            return null;
        }
        
        
    }

    
     /* ---------------------------------------------------------------------
     * Method name : getCurrentAccountId
     * description : used to get current account Id
     * --------------------------------------------------------------------*/
    
    @AuraEnabled
    public static User getCurrentAccountId(){
        User currentUser = new User();
        currentUser = [SELECT Id,Contact.AccountId,Contact.Account.Name FROM User WHERE Id =: UserInfo.getUserId()];
        return currentUser;
    }
    
    /*
    @AuraEnabled
    public static list<PendingApproval> getPendingApproval(integer limitRow){
        String userId = Userinfo.getUserId();
        String CommunityUserRoles = '';
        String userAccountId = '';
        String contractorId = '';
        System.debug('The user ID: ' + userID);
        
        for(User usr:[select id,contact.contractor__c,contact.Role__c,Account_Id__c,
                        Community_User_Role__c,
                        AccountId from User where id=:userId]){
            
            if(usr.contact.Role__c !=null && usr.contact.Role__c!=''){
                CommunityUserRoles = usr.contact.Role__c;
            }else if(usr.Community_User_Role__c!=null && usr.Community_User_Role__c!=''){
                CommunityUserRoles = usr.Community_User_Role__c;
            }
            userAccountId = usr.AccountId;
            if(usr.contact!=null && usr.contact.contractor__c!=null){
              contractorId = usr.contact.contractor__c;
            }
        }
        List<HexaBPM__Step__c> lstNewSteps = new List<HexaBPM__Step__c>();
        List<Step__c> allsteps = new List<Step__c>();
        List<HexaBPM__Step__c> allNewSteps = new List<HexaBPM__Step__c>();
        list<PendingApproval> lstPendingApproval = new list<PendingApproval>();

        
        
        String newStep = 'SELECT id, Name, HexaBPM__Status__c,HexaBPM__Status__r.Name,HexaBPM__Step_Template__r.Name, CreatedDate,'+ 
                        ' HexaBPM__SR__c, HexaBPM__Step_Template__c, HexaBPM__SR__r.HexaBPM__External_Status_Name__c,HexaBPM__Summary__c '+
                        ' From HexaBPM__Step__c ';
        newStep += 'where HexaBPM__SR__r.HexaBPM__Customer__c =\''+userAccountId +'\' '; 
        newStep += ' AND Is_Client_Step__c=false';
        newStep += ' AND HexaBPM__SR_Step__r.Visible_in_community__c = true';
        newStep += ' AND HexaBPM__Status_Type__c != \'End\' ';
        newStep += ' ORDER BY CreatedDate desc';
        if(limitRow != null && limitRow > 0)
            newStep += ' Limit '+limitRow;
        else
            newStep += ' Limit '+(Limits.getLimitQueryRows()-Limits.getQueryRows()) ;
                      
        String oldStep = ' SELECT id, SR_Record_Type__c, Name, Assigned_to_client__c,Step_Name__c,SR_Group__c,';
        oldStep += ' Step_Status__c, CreatedDate, SR__c, SR__r.External_Status_Name__c, Summary__c, ';
        oldStep += ' SR__r.Portal_Service_Request_Name__c  ';
        oldStep += ' from Step__c ';
        
        if(String.isNotBlank(CommunityUserRoles) && CommunityUserRoles.contains('Fit-Out Services')){
            oldStep += ' where SR__r.contractor__c = \''+contractorId+'\'';
            oldStep += ' AND (Owner__c=\'Contractor\') AND Status_Type__c != \'End\'';
            oldStep += ' AND SR__r.Pre_GoLive__c = false';
            oldStep += ' ORDER BY CreatedDate ASC,SR_Group__c asc';
            if(limitRow != null && limitRow > 0)
                oldStep += ' Limit '+limitRow;
            else
                oldStep += ' Limit '+(Limits.getLimitQueryRows()-Limits.getQueryRows()) ;
           
        } else{
            oldStep += ' where SR__r.Customer__c = \''+userAccountId+'\'';
            oldStep += ' AND (OwnerId=\''+userId+'\' OR Owner.Name=\'Client Entry User\' OR Owner.Name=\'Client Entry Approver\' OR Assigned_to_client__c =true) ';
            oldStep += ' AND Status_Type__c != \'End\' AND SR__r.Pre_GoLive__c = false ';
            oldStep += ' ORDER BY CreatedDate ASC,SR_Group__c asc';
            if(limitRow != null && limitRow > 0)
                oldStep += ' Limit '+limitRow;
            else
                oldStep += ' Limit '+(Limits.getLimitQueryRows()-Limits.getQueryRows()) ;
        }
       
        if(String.IsNotBlank(newStep))
            lstNewSteps.addAll((List<HexaBPM__Step__c>)database.query(newStep));
        
        map<string,HexaBPM__Step__c> mapPendingApproval = new map<string,HexaBPM__Step__c>();
        string key;
        for(HexaBPM__Step__c obj: lstNewSteps){
            key = obj.HexaBPM__SR__c + '_' + obj.HexaBPM__Step_Template__r.Name;
            if(!mapPendingApproval.containsKey(key))
                mapPendingApproval.put(key,obj);
        }
        for (string stepKey : mapPendingApproval.keySet()) {
            if(mapPendingApproval.get(stepKey) != null){
                PendingApproval pObj = new PendingApproval();
                pObj.summary = mapPendingApproval.get(stepKey).HexaBPM__Summary__c;
                pObj.stepId = mapPendingApproval.get(stepKey).Id; 
                pObj.status = mapPendingApproval.get(stepKey).HexaBPM__Status__r.Name;
                lstPendingApproval.add(pObj);
            }
        }
       
        list<Step__c> lstOldSteps = new list<Step__c>();
        if(String.IsNotBlank(oldStep))
            lstOldSteps.addAll((List<Step__c>)database.query(oldStep));

        for(Step__c obj: lstOldSteps){
            PendingApproval pObj = new PendingApproval();
            pObj.summary = obj.Summary__c;
            pObj.stepId = obj.Id;
            lstPendingApproval.add(pObj);
        }
                
        return lstPendingApproval;
    }
    */
    public class PendingApproval{
        @AuraEnabled
        public string srName {get;set;}
        @AuraEnabled
        public string srType {get;set;}
        @AuraEnabled
        public string summary {get;set;}
        @AuraEnabled
        public string stepId {get;set;}
        @AuraEnabled
        public string status {get;set;}
        @AuraEnabled
        public string url {get;set;}
        @AuraEnabled
        public boolean isCPRenewal {get;set;}
        @AuraEnabled
        public boolean disableStartNowButton {get;set;}
        
    }


    public class ResponseWrapper
    {
        @AuraEnabled
        public list<PendingApproval> lstPendingApproval {get;set;}

        @AuraEnabled
        public Boolean displayComponent {get;set;}

        @AuraEnabled
        public object debugger {get;set;}

        @AuraEnabled
        public boolean hasSubmitCp {get;set;}
        
        @AuraEnabled
        public String profileName{get;set;}  
        
        @AuraEnabled
        public String initialAccountId{get;set;}  
        
    }

}