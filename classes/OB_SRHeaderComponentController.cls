/**
 * Author : Merul Shah
 * Description : Used for SR index for fetcing latest workign SR.
   v 1.1  08 March 2020  Merul Changes for blocking homepage
    v1.2 step query added for withdrawal
    v1.3 new logic for fetchinf page flow
 **/
public without sharing class OB_SRHeaderComponentController{
	
    @AuraEnabled
    public static ResponseWrapper loadPageFlow(){
        // Current loggedin User AccountId.
        string accId;
        string ContactId;
        boolean SelfRegistrationUser = false;
        boolean CommercialPermission = false;
        boolean MultiStructureAccount = false;
        string Entitytype;
        string SectorClassification;
        map<string, HexaBPM__Service_Request__c> rtSerReqMap = new map<string, HexaBPM__Service_Request__c>();
        Boolean displayCompnent = false;
    
        //declarations
        OB_SRHeaderComponentController.ResponseWrapper respWrap = new OB_SRHeaderComponentController.ResponseWrapper();
    
    	
    
        // v 1.1  08 March 2020  Merul Changes for blocking homepage
        for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())){
            respWrap.loggedinUser = usrObj;
            if(usrObj.ContactId!=null ){
            	ContactId = usrObj.ContactId;
            	accId = usrObj.contact.AccountId;
            	/*
            	if(usrObj.Contact.Account.OB_Is_Multi_Structure_Application__c=='Yes')
            		MultiStructureAccount = true;
            	*/
	            if(usrObj.Contact.Account.Is_Commercial_Permission__c!='Yes'){
	                displayCompnent = (usrObj.contact.OB_Block_Homepage__c ? false : true );
	                if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null){
	                    Entitytype = usrObj.contact.Account.Company_Type__c;
	                    SectorClassification = usrObj.contact.Account.OB_Sector_Classification__c;
	                    if(usrObj.OB_Created_by_User_License__c == 'Guest User License'){
	                        SelfRegistrationUser = true;
	                    }
	                }
	            }else if(usrObj.Contact.Account.Is_Commercial_Permission__c=='Yes'){
	                CommercialPermission = true;
	                displayCompnent = true;
	            }
            }
        }
    	
    	if(accId!=null && ContactId!=null){
    		for(AccountContactRelation ACR:[Select Id from AccountContactRelation where ContactId=:ContactId and AccountId=:accId and IsActive=true and Multi_Structure_Account__c=true]){
    			MultiStructureAccount = true;
    		}
    	}
    	
        system.debug(accId);
        system.debug('displayCompnent' + displayCompnent);
        system.debug(ContactId);
        
        set<string> setPageFlowId = new set<string>();
        // deserilazation of reqWrapPram remaining.
        map<Id, OB_SRHeaderComponentController.PageFlowWrapper> pageFlowWrapMap = new map<Id, OB_SRHeaderComponentController.PageFlowWrapper>();
        map<string, OB_SRHeaderComponentController.PageFlowWrapper> rtPgFlwMap = new map<string, OB_SRHeaderComponentController.PageFlowWrapper>();
    
        string strQuery = 'select Id,Name,HexaBPM__Master_Object__c,HexaBPM__Preview_Flow__c,HexaBPM__Record_Type_API_Name__c,HexaBPM__Flow_Description__c,SLA_Display__c,Applicable_for_SelfReg_User__c,(SELECT id,Name,Community_Page__c,Community_URL__c,SLA_Display__c,Completion_Weightage__c,HexaBPM__Page_Order__c,HexaBPM__Page_Flow__r.Id,HexaBPM__Render_by_Default__c FROM HexaBPM__Pages__r ORDER BY HexaBPM__Page_Order__c) FROM HexaBPM__Page_Flow__c';
        boolean DisplayIndex = true;
        set<string> setExcludeRecordTypes = new set<string>();
        /* if(SelfRegistrationUser) {
            setExcludeRecordTypes.add('Converted_User_Registration');
        } else {
            setExcludeRecordTypes.add('New_User_Registration');
        } */
        //v1.3
        string rectypeToExclude = OB_AgentEntityUtils.pageFlowQueryCheck(accId);
        system.debug('rectypeToExclude ' +rectypeToExclude);
        if(rectypeToExclude != null && rectypeToExclude != ''){
            setExcludeRecordTypes.add(rectypeToExclude);
        }
        if(Entitytype != null && SectorClassification != null) {
        	if(MultiStructureAccount){
	            setExcludeRecordTypes.add('AOR_NF_R');
                setExcludeRecordTypes.add('In_Principle');
                setExcludeRecordTypes.add('Converted_User_Registration');
                setExcludeRecordTypes.add('New_User_Registration');
        	}else{
        		if(Entitytype.tolowercase() == 'Financial - related' || (Entitytype.tolowercase() == 'Non - financial' && SectorClassification == 'Investment Fund')) {
	                setExcludeRecordTypes.add('AOR_NF_R');
	                setExcludeRecordTypes.add('In_Principle');
	            }else {
	                setExcludeRecordTypes.add('AOR_Financial');
	            }
        	}
        }
        system.debug('SelfRegistrationUser==>' + SelfRegistrationUser);
        system.debug('CommercialPermission==>' + CommercialPermission);
        system.debug('setExcludeRecordTypes==>' + setExcludeRecordTypes);
        system.debug('Entitytype==>' + Entitytype);
        system.debug('SectorClassification==>' + SectorClassification);
        
        if(!CommercialPermission){
            strQuery = strQuery + ' WHERE Display_as_Index__c=:DisplayIndex and HexaBPM__Record_Type_API_Name__c NOT IN:setExcludeRecordTypes and Commercial_Permission__c=:CommercialPermission ORDER BY Seq__c asc';
        }else{
            strQuery = strQuery + ' WHERE Display_as_Index__c=:DisplayIndex and Commercial_Permission__c=:CommercialPermission ORDER BY Seq__c asc';
        }
        set<string> setRecordTypeNames = new set<string>();
        list<HexaBPM__Page_Flow__c> lstPageFlows = new list<HexaBPM__Page_Flow__c>();
        for(HexaBPM__Page_Flow__c pgf :database.query(strQuery)){
            setRecordTypeNames.add(pgf.HexaBPM__Record_Type_API_Name__c);
            lstPageFlows.add(pgf);
        }
        //[SELECT Id, HexaBPM__Record_Type_API_Name__c from HexaBPM__Page_Flow__c where Display_as_Index__c = true and HexaBPM__Record_Type_API_Name__c != null]
        system.debug('setRecordTypeNames==>' + setRecordTypeNames);
    
        if(setRecordTypeNames.size() > 0){
            map<String, Schema.SObjectType> m = Schema.getGlobalDescribe();
            SObjectType objtype;
            DescribeSObjectResult objDef1;
            map<String, SObjectField> fieldmap;
            if(m.get('HexaBPM__Service_Request__c') != null)
                objtype = m.get('HexaBPM__Service_Request__c');
            if(objtype != null)
                objDef1 = objtype.getDescribe();
            if(objDef1 != null)
                fieldmap = objDef1.fields.getmap();
    
            set<string> setSRFields = new set<string>();
    
            if(fieldmap != null) {
                for(Schema.SObjectField strFld :fieldmap.values()) {
                    Schema.DescribeFieldResult fd = strFld.getDescribe();
                    if(fd.isCustom()) {
                        setSRFields.add(string.valueOf(strFld).toLowerCase());
                    }
                }
            }
            string SRqry = 'Select Id,Name';
            if(setSRFields.size() > 0) {
                for(string srField :setSRFields) {
                    SRqry += ',' + srField;
                }
            }
            
            boolean RejectedSR = false;
            //v1.2
            string withdrwanCode = 'BD_ENTITY_WITHDRAWAL_REVIEW';
            string endString = 'End';
            SRqry += ',(select id from HexaBPM__Steps_SR__r where Step_Template_Code__c=:withdrwanCode and HexaBPM__Status_Type__c!=: endString) From HexaBPM__Service_Request__c where HexaBPM__Record_Type_Name__c IN:setRecordTypeNames AND HexaBPM__Customer__c=:accId and HexaBPM__Is_Rejected__c=:RejectedSR and HexaBPM__IsCancelled__c=:RejectedSR ORDER BY CreatedDate Desc';
    
            for(HexaBPM__Service_Request__c srRequest :database.query(SRqry)) {
                if( srRequest.HexaBPM__Record_Type_Name__c == 'Converted_User_Registration' && displayCompnent == false){
                    if(srRequest.HexaBPM__Contact__c == ContactId){
                        if(!rtSerReqMap.containsKey(srRequest.HexaBPM__Record_Type_Name__c.toLowerCase())) {
                            rtSerReqMap.put(srRequest.HexaBPM__Record_Type_Name__c.toLowerCase(), srRequest);
                        }
                    }                   
                }else{
                    if(!rtSerReqMap.containsKey(srRequest.HexaBPM__Record_Type_Name__c.toLowerCase())) {
                        rtSerReqMap.put(srRequest.HexaBPM__Record_Type_Name__c.toLowerCase(), srRequest);
                    }
                }
               
            }
        }
    
        respWrap.debugger=lstPageFlows;
        system.debug('lstPageFlows==>' + lstPageFlows);
        for(HexaBPM__Page_Flow__c pgflow :lstPageFlows) 
        {
    
    
            map<string, OB_SRHeaderComponentController.PageWrapper> pageWrapMap = new map<string, OB_SRHeaderComponentController.PageWrapper>();
            OB_SRHeaderComponentController.PageFlowWrapper pageFlowWrap = new OB_SRHeaderComponentController.PageFlowWrapper();
            pageFlowWrap.relatedLatestSR = rtSerReqMap.get(pgflow.HexaBPM__Record_Type_API_Name__c.tolowercase());
           
            
            
            pageFlowWrap.pageFlowObj = pgflow;
    
            string SRId = '';
            string PageTracker;
            if(pageFlowWrap.relatedLatestSR != NULL && pageFlowWrap.relatedLatestSR.Id != NULL) {
                SRId = pageFlowWrap.relatedLatestSR.Id;
                PageTracker = pageFlowWrap.relatedLatestSR.Page_Tracker__c;
                map<string, boolean> lockingDetail = new map<string, boolean>();
                if(pageFlowWrap.relatedLatestSR.HexaBPM__Internal_Status_Name__c == 'Draft') {
                    pageFlowWrap.ApplicationStatus = 'In Progress';
                }else{
                    //pageFlowWrap.ApplicationStatus = 'Completed';//Commented
                    pageFlowWrap.ApplicationStatus = pageFlowWrap.relatedLatestSR.HexaBPM__External_Status_Name__c;
                }
                //for locking unlocking
                // && !pageFlowWrap.relatedLatestSR.HexaBPM__IsCancelled__c && !pageFlowWrap.relatedLatestSR.HexaBPM__Is_Rejected__c && !pageFlowWrap.relatedLatestSR.HexaBPM__IsClosedStatus__c
                if(pageFlowWrap.relatedLatestSR.HexaBPM__Internal_Status_Name__c != 'Draft' && SRId != null) {
                    pageFlowWrap.lockingMap = OB_LockAndUnlockSRHelper.GetPageLockDetails(pgflow.id, SRId);
                }
            } else {
                pageFlowWrap.ApplicationStatus = 'Not Started';
            }
            map<string, string> MapHiddenPageIds = PageFlowControllerHelper.getHiddenPageIdsMap(pgflow.Id, rtSerReqMap.get(pgflow.HexaBPM__Record_Type_API_Name__c.tolowercase()));
            system.debug('MapHiddenPageIds==>' + MapHiddenPageIds);
            for(HexaBPM__Page__c page :pgflow.HexaBPM__Pages__r) {
                if(page.HexaBPM__Render_by_Default__c || (!page.HexaBPM__Render_by_Default__c && MapHiddenPageIds.get(page.Id) == null)) {
                    // || (!page.HexaBPM__Render_by_Default__c && MapHiddenPageIds.get(page.Id)==null)
                    OB_SRHeaderComponentController.PageWrapper pageWrap = new OB_SRHeaderComponentController.PageWrapper();
    
                    if(!pageFlowWrap.lockingMap.isEmpty()) {
                        if(pageFlowWrap.lockingMap.containsKey(page.id)) {
                            pageWrap.isLocked = String.valueOf(pageFlowWrap.lockingMap.get(page.id));
                        }
                    } else if(SRId == '') {
                        pageWrap.isLocked = String.valueOf(true);
                    }
                    pageWrap.pageObj = page;
                    pageWrap.communityPageURL = page.Community_URL__c + '?pageId=' + page.Id + '&flowId=' + pgflow.Id + '&srId=' + SRId;
                    if(PageTracker != null && PageTracker != '') {
                        if(PageTracker.containsIgnoreCase(page.Id)) {
                            pageWrap.srPageStatus = 'Completed';
                        } else {
                            pageWrap.srPageStatus = 'Not Started';
                        }
                    } else {
                        pageWrap.srPageStatus = 'Not Started';
                    }
                    pageWrapMap.put(page.Id, pageWrap);
                }
            }
            // Assigning a wrapper list.
            pageFlowWrap.pageWrapLst = pageWrapMap.values();
            rtPgFlwMap.put(pgflow.HexaBPM__Record_Type_API_Name__c.toLowerCase(), pageFlowWrap);
            if( displayCompnent || pgflow.HexaBPM__Record_Type_API_Name__c == 'Converted_User_Registration')
            {
                pageFlowWrapMap.put(pgflow.Id, pageFlowWrap);
            }
            
        }
        system.debug('pageFlowWrapMap ---> ' + pageFlowWrapMap);
        //Getting latest SR for each page flow based on record type ID.
        respWrap.pageFlowWrapLst = pageFlowWrapMap.values();
        system.debug(pageFlowWrapMap.values());
        return respWrap;
    }
    
    
    public static string getCommunityURLBasedOnSR(HexaBPM__Service_Request__c relatedLatestSR, OB_SRHeaderComponentController.PageWrapper pgWrapper) {
        string pg;
        string strBaseUrl = System.Url.getSalesforceBaseURL().toExternalForm();
        // getting community name.
        string communityName = getBaseUrl() + '/s';
        string CommunityPageName = '';
        if(pgWrapper != null && pgWrapper.pageObj != null && pgWrapper.pageObj.Community_Page__c != null)
            CommunityPageName = pgWrapper.pageObj.Community_Page__c;
        string flowId = pgWrapper.pageObj.HexaBPM__Page_Flow__r.Id;
        string pageId = pgWrapper.pageObj.Id;
        system.debug('-------------->'+relatedLatestSR.Id); //&& relatedLatestSR.Id != ''
        if(relatedLatestSR != null && relatedLatestSR.Id != null) {
            pg = strBaseUrl + '/' + communityName + '/' + CommunityPageName + '?srId=' + relatedLatestSR.Id + '&flowId=' + flowId + '&pageId=' + pageId;
        } else {
            pg = strBaseUrl + '/' + communityName + '/' + CommunityPageName + '?flowId=' + flowId + '&pageId=' + pageId;
        }
        return pg;
    }
    //Merul
    @AuraEnabled
    public static string getBaseUrl() {
        if(Network.getNetworkId() != null) {
            return [SELECT Id, UrlPathPrefix FROM Network WHERE Id = : Network.getNetworkId()].UrlPathPrefix;
        }
        return '';
    }
    
    
    
    
    
    @AuraEnabled
    public static ResponseWrapper fetchLockingUnlockingWrap(string flowId, string srId) {
        OB_SRHeaderComponentController.ResponseWrapper respWrap = new OB_SRHeaderComponentController.ResponseWrapper();
        respWrap.lockingMap = new map<string, boolean>();
        map<string, boolean> lockingDetail = new map<string, boolean>();
    
    
        lockingDetail = OB_LockAndUnlockSRHelper.GetPageLockDetails(flowId, srId);
        respWrap.lockingMap = lockingDetail;
        return respWrap;
    }
    
    @AuraEnabled
    public static ResponseWrapper withdrawApplication(string requestWrapParam) {
        //declaration of wrapper
        RequestWrapper reqWrap = new RequestWrapper();
        ResponseWrapper respWrap = new ResponseWrapper();
        //deseriliaze.
        reqWrap = (RequestWrapper) JSON.deserializeStrict(requestWrapParam, RequestWrapper.class);
        if(reqWrap.srToWithDraw!=null && reqWrap.srToWithDraw!='') {
            boolean isOpenSR = false;
            boolean hasOpenWithdrawStep = false;
            string SRTEmplateId;
            for(HexaBPM__Service_Request__c SR:[Select Id,HexaBPM__SR_Template__c,HexaBPM__IsClosedStatus__c,HexaBPM__IsCancelled__c,HexaBPM__Is_Rejected__c,HexaBPM__Internal_Status_Name__c from HexaBPM__Service_Request__c where Id=:reqWrap.srToWithDraw]) {
                SRTEmplateId = SR.HexaBPM__SR_Template__c;
                if(!SR.HexaBPM__IsClosedStatus__c && !SR.HexaBPM__IsCancelled__c && !SR.HexaBPM__Is_Rejected__c && SR.HexaBPM__Internal_Status_Name__c!='Draft')
                    isOpenSR = true;
            }
            
            if(isOpenSR) {
                for(HexaBPM__Step__c stp:[Select Id from HexaBPM__Step__c where HexaBPM__SR__c=:reqWrap.srToWithDraw and Step_Template_Code__c='BD_ENTITY_WITHDRAWAL_REVIEW' and HexaBPM__Status_Type__c!='End']) {
                    hasOpenWithdrawStep = true;
                }
                if(!hasOpenWithdrawStep) {
                    string SRStepId;
                    for(HexaBPM__SR_Steps__c SRStep:[Select Id from HexaBPM__SR_Steps__c where HexaBPM__Step_Template_Code__c='BD_ENTITY_WITHDRAWAL_REVIEW' and HexaBPM__SR_Template__c=:SRTEmplateId]) {
                        SRStepId = SRStep.Id;
                    }
                    try{
                        HexaBPM__Step__c objWithDrwaStep = OB_SRHeaderComponentControllerHelper.CreateActionItem(reqWrap.srToWithDraw,SRStepId);
                        objWithDrwaStep.withdrawal_Reason__c = reqWrap.withdrawalReason;
                        insert objWithDrwaStep;
                        respWrap.debugger = objWithDrwaStep;
                    }catch(Exception e) {
                        string DMLError = e.getdmlMessage(0) + '';
                        if(DMLError == null) {
                            DMLError = e.getMessage() + '';
                        }
                        respWrap.errorMessage = DMLError;
                    }
                }
            }
        }
        return respWrap;
    }
    public class PageFlowWrapper {
    @AuraEnabled public HexaBPM__Page_Flow__c pageFlowObj { get; set; }
    @AuraEnabled public string ApplicationStatus { get; set; }
    @AuraEnabled public list<OB_SRHeaderComponentController.PageWrapper> pageWrapLst { get; set; }
    @AuraEnabled public HexaBPM__Service_Request__c relatedLatestSR { get; set; }
    
    @AuraEnabled public HexaBPM__Page__c lastVisitedPage { get; set; }
    @AuraEnabled public string lastVisitedURL { get; set; }
    @AuraEnabled public map<string, boolean> lockingMap { get; set; }
    public PageFlowWrapper() {
        ApplicationStatus = 'Not Started';
        lockingMap = new map<string, boolean>();
        
    }
    }
    public class PageWrapper {
    @AuraEnabled public HexaBPM__Page__c pageObj { get; set; }
    @AuraEnabled public string communityPageURL { get; set; }
    @AuraEnabled public string isLocked { get; set; }
    @AuraEnabled public string srPageStatus { get; set; }
    public PageWrapper() {
    }
    }
    public class RequestWrapper {
    @AuraEnabled public Id flowId { get; set; }
    @AuraEnabled public string withdrawalReason { get; set; }
    @AuraEnabled public string srToWithDraw { get; set; }
    @AuraEnabled public Id pageId { get; set; }
    @AuraEnabled public Id srId { get; set; }
    @AuraEnabled public string fieldApi { get; set; }
    @AuraEnabled public string fieldVal { get; set; }
    @AuraEnabled public string cmdActionId { get; set; }
    @AuraEnabled public HexaBPM__Service_Request__c objRequest { get; set; }
    @AuraEnabled public map<string, string> objRequestMap { get; set; }
    public RequestWrapper() {
    }
    }
    public class ResponseWrapper {
    @AuraEnabled public list<OB_SRHeaderComponentController.PageFlowWrapper> pageFlowWrapLst { get; set; }
    @AuraEnabled public object debugger { get; set; }
    @AuraEnabled public map<string, boolean> lockingMap { get; set; }
    @AuraEnabled public string errorMessage { get; set; }
    @AuraEnabled public user loggedinUser { get; set; }
    public ResponseWrapper() {
    }
    }
    }