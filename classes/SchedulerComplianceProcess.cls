/***********************************************************************************
 *  Author   : Swati
 *  Company  : NSI
 *  Date     : 4th May 2016
 *  Purpose  : This class is to schedule jobs for compliance 
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date        Updated By  Description
 ----------------------------------------------------------------------------------------              
      
 
 **********************************************************************************************/


global  class SchedulerComplianceProcess implements Schedulable {
     
     global void execute(SchedulableContext sc){
        ScheduleComplianceProcess obj = new ScheduleComplianceProcess();
        database.executeBatch(obj,500);
    }
}