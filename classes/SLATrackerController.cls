public without sharing class SLATrackerController{
    public string caseId{get;Set;}  
    public string caseStatus{get;Set;}  
    Public string MileStoneName{get;set;}
    public boolean isOverDue{get;set;}
    Public SLATrackerWrapper objTrackerDetails{get;set;} 
    Public SLATrackerWrapper objCaseAgeDetail{get;set;} 
    public SLATrackerController(ApexPages.StandardController controller) {
        isOverDue= true;
        caseId = apexpages.currentpage().getParameters().get('Id');
        SLADifference();     
    } 
    
    
    public void SLADifference(){
        objTrackerDetails = new SLATrackerWrapper();
        objCaseAgeDetail = new SLATrackerWrapper();
        
        Case cs = [SELECT id,
                          isClosed,
                          CreatedDate,
                          GN_Case_Age__c,
                          GN_Sys_Case_Queue__c,
                          GN_Due_Date_Time__c,
                          businessHoursId,
                          Status 
                     FROM Case 
                    WHERE id=:caseId];
        caseStatus = cs.Status;
        
        DateTime CurrentDateTime = DateTime.now();//.addHours(4);
        Decimal ageInMilliSeconds =  BusinessHours.diff(cs.businessHoursId,cs.CreatedDate,CurrentDateTime); 
        system.debug('Agein miliseconds==>'+ageInMilliSeconds );
        
        //objTrackerDetails.ageHours = ageInMilliSeconds.divide((1000 * 60 * 60 * 8),3);
        objTrackerDetails.ageHours = ageInMilliSeconds.divide((1000 * 60 * 60),2);
        system.debug('Age==>'+objTrackerDetails.ageHours);
        
        //CASE AGE
        long CaseAgeMillis = BusinessHours.diff(cs.businessHoursId,cs.CreatedDate,CurrentDateTime);
        objCaseAgeDetail.DisplayText='Case Age'; 
        objCaseAgeDetail.RemainingDays = Math.floor(CaseAgeMillis/(1000 * 60 * 60 * 8));
        objCaseAgeDetail.RemainingHours = Math.floor(Math.MOD(CaseAgeMillis,(1000 * 60 * 60 * 8))/(1000 * 60 * 60));
        objCaseAgeDetail.RemainingMinutes= Math.floor(Math.MOD(CaseAgeMillis,(1000 * 60 * 60))/(1000 * 60));   
        objCaseAgeDetail.RemainingSeconds= Math.floor(Math.MOD(CaseAgeMillis,(1000 * 60))/1000);
        objCaseAgeDetail.BackgroundStyle = 'background:green'; 
        
        
        long differenceInMilliSeconds = 0;
        objTrackerDetails = new SLATrackerWrapper();
        objTrackerDetails.RemainingDays = Math.floor(differenceInMilliSeconds/(1000 * 60 * 60 * 8));
        objTrackerDetails.RemainingHours =  Math.floor(Math.MOD(differenceInMilliSeconds,(1000 * 60 * 60 * 8))/(1000 * 60 * 60));
        objTrackerDetails.RemainingMinutes = Math.floor(Math.MOD(differenceInMilliSeconds,(1000 * 60 * 60))/(1000 * 60));
        objTrackerDetails.RemainingSeconds = Math.floor(Math.MOD(differenceInMilliSeconds,(1000 * 60))/1000);
        objTrackerDetails.BackgroundStyle = 'background:green';    
        objTrackerDetails.DisplayText='SLA Due in'; 
        
        
        if(!cs.isClosed){
            Datetime startDateTme = null;
            Datetime dueDateTme = null;
            Id bussHoursId = null;
            long diffPauseTime = 0;
            Integer ctrSize = 1;
            for(Case_SLA__c objCSLA : [select Id,Parent__c,From__c,Due_Date_Time__c,Until__c,Business_Hours_Id__c from Case_SLA__c where Parent__c =:caseId AND Owner__c = :cs.GN_Sys_Case_Queue__c ORDER BY CreatedDate DESC]){
                if(objCSLA.Until__c == null){
                    //differenceInMilliSeconds = 60000;
                    differenceInMilliSeconds = differenceInMilliSeconds + BusinessHours.diff(objCSLA.Business_Hours_Id__c,objCSLA.From__c,CurrentDateTime);
                    System.debug('@@SLADIFF1: '+Math.floor(Math.MOD(BusinessHours.diff(objCSLA.Business_Hours_Id__c,objCSLA.From__c,CurrentDateTime),(1000 * 60 * 60))/(1000 * 60)));
                    if(objCSLA.From__c != null){
                        startDateTme = objCSLA.From__c;
                    }
                } else{
                    differenceInMilliSeconds = differenceInMilliSeconds + BusinessHours.diff(objCSLA.Business_Hours_Id__c,objCSLA.From__c,objCSLA.Until__c);
                    System.debug('@@SLADIFF2: '+Math.floor(Math.MOD(BusinessHours.diff(objCSLA.Business_Hours_Id__c,objCSLA.From__c,objCSLA.Until__c),(1000 * 60 * 60))/(1000 * 60)));
                    if(objCSLA.From__c != null){
                        startDateTme = objCSLA.From__c;
                    }
                }
                system.debug('@@STARTNEW--: '+startDateTme);
                ctrSize = ctrSize + 1;
                bussHoursId = objCSLA.Business_Hours_Id__c;
            }
            
            differenceInMilliSeconds = BusinessHours.diff(bussHoursId,startDateTme,cs.GN_Due_Date_Time__c) - differenceInMilliSeconds;
            
            objTrackerDetails.RemainingDays = Math.floor(differenceInMilliSeconds/(1000 * 60 * 60 * 8));
            objTrackerDetails.RemainingHours =  Math.floor(Math.MOD(differenceInMilliSeconds,(1000 * 60 * 60 * 8))/(1000 * 60 * 60));
            objTrackerDetails.RemainingMinutes = Math.floor(Math.MOD(differenceInMilliSeconds,(1000 * 60 * 60))/(1000 * 60));
            objTrackerDetails.RemainingSeconds = Math.floor(Math.MOD(differenceInMilliSeconds,(1000 * 60))/1000);
            objTrackerDetails.BackgroundStyle = 'background:green';    
            objTrackerDetails.DisplayText='SLA Due in'; 
            
        } else{
            objTrackerDetails.BackgroundStyle = 'background:grey !important;';     
            objTrackerDetails.DisplayText= cs.Status;  
            
            objCaseAgeDetail.BackgroundStyle = 'background:grey !important;';
        }
    }
    
    public class SLATrackerWrapper{
        Public DateTime MileStoneStartTime{get;set;}
        Public DateTime MileStoneEndTime{get;set;}
        Public Double RemainingDays{get;set;}
        Public Double RemainingHours{get;set;}
        Public Double RemainingMinutes{get;set;}
        Public Double RemainingSeconds{get;Set;}
        Public string MilestoneName{get;set;}
        public string BackgroundStyle{get;set;}
        Public boolean isOverDue{get;set;}
        Public DateTime completedDate{get;set;}
        public boolean SLAMet{get;set;}
        Public string BusinessHourId;
        
        //**by Kaavya
        Public Double ageHours{get;set;}
        Public string DisplayText{get;set;}
        
        public SLATrackerWrapper(){  
            RemainingDays = 0;         
            RemainingHours =0;
            RemainingMinutes = 0;
            RemainingSeconds = 0; 
            isOverDue = false; 
            SLAMet = false;
            
            //ageHours=0.000; //**by Kaavya
            ageHours=0.00;
        }
    }
}