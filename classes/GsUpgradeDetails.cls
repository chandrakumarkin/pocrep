/*
    Author      :   Ravi
    Description :   This class is used process the updrage process of  SR
    --------------------------------------------------------------------------------------------------------------------------
  Modification History
   --------------------------------------------------------------------------------------------------------------------------
  V.No  Date    Updated By      Description
  --------------------------------------------------------------------------------------------------------------------------             
   V1.0    12-03-2015  Ravi      Created
   V1.1  23-07-2015  Ravi      Added the logic for blocking the web services to SAP when SAP is goes down 
   V1.2    03-10-2016  Sravan          As per Tkt # 3392 : Decrypt the SAP password.
   V1.3    07-Aug-2016 Ravi            If the step 'Entry Permit is issued' is closed then system should should not charge the full amount in upgrade, right now it checking only step is created or not but it should check 'Closure' of this step. as per #3202
   V1.4    12-dec-2018 Azfer     Added a check to check the size of the list before using the values. Comment Style AP:001
*/
global without sharing class GsUpgradeDetails {
  
  public static decimal dActualAmt = 0,dUpgradeAmt = 0 ,taxActualAmt = 0, txtupgradeAmt = 0,upgradeTaxRate=0;
  public static string UpgradeMTRCode,UpgradeTaxCode;
  
  Private static string  decryptedSAPPassWrd = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456';  // V1.2
  
  webservice static string UpgradeToVIP(string SRId){
    string result = UpgradeCheck(SRId,false);
    return result;
  }
  
  webservice static string UpgradeToCross(string SRId){
    string result = UpgradeCheck(SRId,true);
    return result;
  }
  
  public static string UpgradeCheck(string SRId,Boolean isCorssUpgrade){
    Service_Request__c objSR = new Service_Request__c();
    list<SAPGSWebServices.ZSF_S_GS_SERV> items = new list<SAPGSWebServices.ZSF_S_GS_SERV>();
    SAPGSWebServices.ZSF_S_GS_SERV item;
    //String UpgradeMTRCode;
    //decimal dActualAmt = 0,dUpgradeAmt = 0;
    string sOSGUID;
    //v1.1
    if(Block_Trigger__c.getAll() != null && Block_Trigger__c.getAll().get('Block') != null && Block_Trigger__c.getAll().get('Block').Block_Web_Services__c){
      return 'Please try after sometime, system under the maintenance. Apologies for the inconvenience.';
    }
    List<SR_Price_Item__c> LstSRPRice = new List<SR_Price_Item__c>();
    
    for(Service_Request__c obj : [select Id,Name,SAP_OSGUID__c,Submitted_Date__c,Change_Activity__c,Submitted_DateTime__c,Customer__c,Internal_Status_Name__c,Customer__r.BP_No__c,SAP_MATNR__c,SAP_PR_No__c,SAP_SO_No__c,SAP_SGUID__c,SAP_Unique_No__c,SAP_UMATN__c,(select Id,Step_Name__c from Steps_SR__r where Step_Name__c =: Label.Entry_Permit_Step_Name AND Status_Code__c = 'CLOSED'),(select Id,Pricing_Line__r.Material_Code__c,Price__c,VAT_Amount__c,VAT_Description__c,Pricing_Line__r.TAX_CODE__c, Tax_Rate__c,DIFC_Margin__c  from SR_Price_Items1__r where Product__r.Name != 'PSA' AND Pricing_Line__r.Additional_Item__c = false AND Product__r.Is_Guarantee_Product__c = false and product__r.Name != 'Innovation Fee' and product__r.Name != 'Knowledge Fee') from Service_Request__c where Id=:SRId]){
      objSR = obj;
      for(SR_Price_Item__c SRP: obj.SR_Price_Items1__r){
          LstSRPRice.add(SRP);
      }
      
      sOSGUID = (obj.SAP_OSGUID__c != null && obj.SAP_OSGUID__c != null) ? obj.SAP_OSGUID__c : obj.SAP_SGUID__c ;
    }
    
    if(objSR != null && objSR.Id != null){
      /*if(isCorssUpgrade == false && ((objSR.SAP_MATNR__c != null && objSR.SAP_MATNR__c.indexOf('VIP') >= 0) || (objSR.SAP_UMATN__c != null && objSR.SAP_UMATN__c.indexOf('VIP') >= 0) )){
        return 'This service is already a VIP service. It cannot be upgraded furthur.';
      }else if(isCorssUpgrade && objSR.SAP_UMATN__c != null){
        if(objSR.SAP_UMATN__c.indexOf('VIP') >= 0)
          return 'This service is already a VIP service. It cannot be upgraded furthur.';
        return 'This service is already upgraded.';
      }
      if(objSR.SAP_UMATN__c != null){
        if(objSR.SAP_UMATN__c.indexOf('VIP') >= 0)
          return 'This service is already a VIP service. It cannot be upgraded furthur.';
        return 'This service is already upgraded.';
      }*/
      string MeterialCode = objSR.SAP_UMATN__c;
      
      if(MeterialCode == null || MeterialCode == '')
        MeterialCode = objSR.SAP_MATNR__c != null ? objSR.SAP_MATNR__c : (objSR.SR_Price_Items1__r != null && objSR.SR_Price_Items1__r.size() > 0) ? objSR.SR_Price_Items1__r[0].Pricing_Line__r.Material_Code__c : '';
      
      /*if(objSR.SR_Price_Items1__r != null && objSR.SR_Price_Items1__r.size() > 0){
        dActualAmt = objSR.SR_Price_Items1__r[0].Price__c;
        taxActualAmt = objSR.SR_Price_Items1__r[0].DIFC_Margin__c;
        upgradeTaxRate = objSR.SR_Price_Items1__r[0].Tax_Rate__c;
      }
      */
     for(SR_Price_Item__c SrPrice : LstSRPRice){
          
         dActualAmt += SrPrice.Price__c;
         //taxActualAmt = SrPrice.DIFC_Margin__c;
         //upgradeTaxRate = SrPrice.Tax_Rate__c;
      }
      /* Added the check for the size of the list
       Starts AP:001 */
       if( LstSRPRice.size() > 0 ){
           taxActualAmt = LstSRPRice[0].DIFC_Margin__c;
           upgradeTaxRate = LstSRPRice[0].Tax_Rate__c;
      }
      /* Ends AP:001 */
      for(GS_Upgrade_Matrix__c objGS : [select Id,Name,Cross_Material_Margin__c,Entry_Permit_Margin__c,VIP_Margin__c,Entry_Permit_Material_Code__c,VIP_Material_Code__c,Material_Code__c,Cross_Material_Code__c from GS_Upgrade_Matrix__c where Material_Code__c =: MeterialCode]){
        system.debug(objGS.VIP_Margin__c  + 'objGS');
        if(isCorssUpgrade ==  true){
        
          if(objSR.Steps_SR__r != null && objSR.Steps_SR__r.size() > 0){
            UpgradeMTRCode = objGS.Entry_Permit_Material_Code__c;  
              if(objGS.Entry_Permit_Margin__c!= null)          
            txtupgradeAmt = objGS.Entry_Permit_Margin__c!=0.00?((objGS.Entry_Permit_Margin__c)*(Decimal.valueof(Label.Vat_Tax_Percentage))).setScale(2):0.00;//added as part of vat
          }
          else{
            UpgradeMTRCode = objGS.Cross_Material_Code__c;
             if(objGS.Cross_Material_Margin__c!= null)
            txtupgradeAmt = objGS.Cross_Material_Margin__c!=0.00?((objGS.Cross_Material_Margin__c)*(Decimal.valueof(Label.Vat_Tax_Percentage))).setScale(2):0.00;//added as part of vat
          }
        
        }else{
          UpgradeMTRCode = objGS.VIP_Material_Code__c;
          if(objGS.VIP_Margin__c != null){
          txtupgradeAmt = objGS.VIP_Margin__c != 0.00 ? ((objGS.VIP_Margin__c)*(Decimal.valueof(Label.Vat_Tax_Percentage))).setScale(2):0.00;//added as part of vat
          }
        }
      }
      system.debug('UpgradeMTRCode is $$$ : '+UpgradeMTRCode);
      system.debug('MeterialCode is $$$ : '+MeterialCode);
      if(UpgradeMTRCode == null || UpgradeMTRCode == ''){
        if(isCorssUpgrade == false && MeterialCode.contains('VIP'))
          return 'This service is already a VIP service. It cannot be upgraded furthur.';
        else{
          if(objSR.SAP_UMATN__c != null && objSR.SAP_UMATN__c != '')
            return 'Sorry, this service is already upgraded.';
          else  
            return 'Sorry, Your request is not applicable for an upgrade at this point.';
        }  
      }
      if(objSR.SAP_Unique_No__c == null || objSR.SAP_Unique_No__c == ''){
        if(isCorssUpgrade == false && objSR.Internal_Status_Name__c == 'Draft'){
          update new Service_Request__c(Id=objSR.Id,Express_Service__c=true);
          return 'Service has been upgraded successfully.';
        }
        return 'Success';
      }
      //added on 5th July by Ravi to prevent the immediate Upgrade
      if(objSR.Change_Activity__c){
        return 'Your previous upgrade is in progress, please wait for sometime and try.';
      }
      //for(Pricing_Line__c objPL : [select Id,Material_Code__c,Name,(select Id,Unit_Price__c,Date_From__c,Date_To__c from Dated_Pricing__r where Unit_Price__c != null AND ( Date_To__c = null OR Date_To__c >=: system.today() )) from Pricing_Line__c where Material_Code__c =: objSR.SAP_MATNR__c OR Material_Code__c =: UpgradeMTRCode])
      for(Pricing_Line__c objPL : [select Id,Material_Code__c,Name,TAX_CODE__c,DIFC_Margin__c,(select Id,Unit_Price__c,Date_From__c,Date_To__c from Dated_Pricing__r where Unit_Price__c != null order by Date_From__c) from Pricing_Line__c where Material_Code__c =: MeterialCode OR Material_Code__c =: UpgradeMTRCode]){
        for(Dated_Pricing__c objDP : objPL.Dated_Pricing__r){
          if(objPL.Material_Code__c == MeterialCode){
            if(objSR.Submitted_Date__c < objDP.Date_To__c && objDP.Date_To__c != null){
              dActualAmt = dActualAmt != 0 ? dActualAmt : objDP.Unit_Price__c;
              /*if(objPL.TAX_CODE__c!='S0')
              taxActualAmt = (taxActualAmt*(Decimal.valueof(Label.Vat_Tax_Percentage))).setScale(2);*/
              break;
            }else if(objDP.Date_To__c == null || objDP.Date_To__c >= system.today()){
              dActualAmt = dActualAmt != 0 ? dActualAmt : objDP.Unit_Price__c;
              /*if(objPL.TAX_CODE__c!='S0')
              taxActualAmt = (taxActualAmt*(Decimal.valueof(Label.Vat_Tax_Percentage))).setScale(2);*/
              break;
            }  
          }else if(objPL.Material_Code__c == UpgradeMTRCode){
            if(objDP.Date_To__c == null || objDP.Date_To__c >= system.today()){
               /*List<SR_Price_Item__c> srPrice =[select id,Tax_Rate__c,DIFC_Margin__c from SR_Price_Item__c where Material_Code__c =: UpgradeMTRCode and Tax_Rate__c!=0 limit 1];
               if(srPrice.size()>0)
               upgradeTaxRate = srPrice[0].Tax_Rate__c;*/
              dUpgradeAmt = objDP.Unit_Price__c;              
              //txtupgradeAmt = objDP.DIFC_Margin__c;              
              //if(objPL.TAX_CODE__c!='S0')
             // txtupgradeAmt = ((objPL.DIFC_Margin__c)*(Decimal.valueof(Label.Vat_Tax_Percentage))).setScale(2);
              UpgradeTaxCode=objPL.TAX_CODE__c;
              break;
            }
          }
         
        }
      }
      
      
      
      
      User CurrentUser = new User();
    
      for(User objUser : [select Id,SAP_User_Id__c,ContactId from User where Id=:Userinfo.getUserId()])
        CurrentUser = objUser;
    
     
      
      item = new SAPGSWebServices.ZSF_S_GS_SERV();     
      item.WSTYP = 'UPGC';
      item.ORENUM = objSR.Name;//string.valueOf(objSR.SAP_SGUID__c).substring(0,15);
      item.OSGUID = sOSGUID;//objSR.SAP_SGUID__c;
      //item.BUKRS = '5000';
      item.BUKRS = System.label.CompanyCode5000;
      item.KUNNR = objSR.Customer__r.BP_No__c;
      //item.TDATE = GsPendingInfo.SAPDateFormat(system.now());
      item.TDATE = objSR.Submitted_DateTime__c != null ? GsPendingInfo.SAPDateFormat(objSR.Submitted_DateTime__c) : GsPendingInfo.SAPDateFormat(system.now());// system.today().year()+'-'+Month+'-'+Day;//'2014-09-16';
      item.UNQNO = objSR.SAP_Unique_No__c;
      item.VBELN = objSR.SAP_SO_No__c;
      item.BELNR = objSR.SAP_Unique_No__c.substring(0,10);
      item.BANFN = objSR.SAP_PR_No__c;
      item.WAERS = 'AED';
      item.WRBTR = ''+(dUpgradeAmt-dActualAmt);
      item.MATNR = MeterialCode;//objSR.SAP_MATNR__c;
      item.UMATN = UpgradeMTRCode;
      item.POSNR = '000010'; // Item # of SD Document, Always - 000010
      item.RUSER = CurrentUser.ContactId != null ? WebService_Details__c.getAll().get('Credentials').SAP_User_Id__c : CurrentUser.SAP_User_Id__c;
      item.RTIME = GsPendingInfo.SAPTimeFormat(system.now());
      
      //Added VAT
            item.TAX_CODE = UpgradeTaxCode;
            //item.TAX_AMT = ''+(txtupgradeAmt-taxActualAmt);
            item.TAX_AMT = ''+txtupgradeAmt;
            item.TAX_RATE = String.valueOf(upgradeTaxRate);            
            item.TOTAL =  string.valueOf(Decimal.valueOf(item.TAX_AMT)+ Decimal.valueOf(item.WRBTR));
     //Added VAT
            
      items.add(item);
    }
    system.debug('dActualAmt is $$$ : '+dActualAmt);
    system.debug('dUpgradeAmt is $$$ : '+dUpgradeAmt);
    system.debug('request is $$$ : '+items);
    if(!items.isEmpty()){
      SAPGSWebServices.ZSF_TT_GS_SERV objGSData = new SAPGSWebServices.ZSF_TT_GS_SERV();
      objGSData.item = items;
      
      SAPGSWebServices.serv_actv objSFData = new SAPGSWebServices.serv_actv();    
      objSFData.timeout_x = 120000;
      objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
      objSFData.inputHttpHeaders_x= new Map<String, String>();
     // Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c); Commented for V1.2
      Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+GsUpgradeDetails.decryptedSAPPassWrd); // V1.2
      String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
      objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
      
      SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element response;
      
      response = objSFData.Z_SF_GS_ACTIVITY(null, objGSData);
      system.debug('response is $$$ : '+response);
      
      if(response != null && response.EX_GS_SERV_OUT != null){
        for(SAPGSWebServices.ZSF_S_GS_SERV_OP objRes : response.EX_GS_SERV_OUT.item){
          if(objRes.ACCTP != 'P'){
            return objRes.SFMSG;
          }
        }
      }else{
        return 'Please try after sometime or contact support';
      }
    }
    
    return 'Success';
  }
  
  webservice static string checkCurrentPrice(string SRId){
    Date dt = Date.newInstance(2015, 10, 11);
    list<SR_Price_Item__c> lstSRPriceItems = new list<SR_Price_Item__c>();
    //map<Id,Pricing_Line__c> mapPricingLine = new map<Id,Pricing_Line__c>([select Id,Name,(select Id,Unit_Price__c,Date_From__c,Date_To__c from Dated_Pricing__r where Date_To__c = null) from Pricing_Line__c where Id IN (select Pricing_Line__c from Dated_Pricing__c where Date_From__c =: dt) ]);
    map<Id,Pricing_Line__c> mapPricingLine = new map<Id,Pricing_Line__c>([select Id,Name,(select Id,Unit_Price__c,Date_From__c,Date_To__c from Dated_Pricing__r where Date_To__c = null) from Pricing_Line__c where Variable_Price__c = false AND Id IN (select Pricing_Line__c from SR_Price_Item__c where ServiceRequest__c =: SRId) ]);
    
    for(SR_Price_Item__c objItem : [select Id,Price__c,Pricing_Line__c from SR_Price_Item__c where ServiceRequest__c =: SRId AND Pricing_Line__r.Variable_Price__c = false]){ // AND ServiceRequest__r.CreatedDate <: dt
      if(mapPricingLine != null && mapPricingLine.containsKey(objItem.Pricing_Line__c) && mapPricingLine.get(objItem.Pricing_Line__c).Dated_Pricing__r != null &&  mapPricingLine.get(objItem.Pricing_Line__c).Dated_Pricing__r.size() > 0 && mapPricingLine.get(objItem.Pricing_Line__c).Dated_Pricing__r[0].Unit_Price__c != objItem.Price__c){
        SR_Price_Item__c objItemTemp = new SR_Price_Item__c(Id=objItem.Id);
        objItemTemp.Price__c = mapPricingLine.get(objItem.Pricing_Line__c).Dated_Pricing__r[0].Unit_Price__c;
        lstSRPriceItems.add(objItemTemp);
      }
    }
    if(!lstSRPriceItems.isEmpty())
      update lstSRPriceItems;
    return 'Success';
  }
}