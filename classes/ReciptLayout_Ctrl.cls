public with sharing class ReciptLayout_Ctrl{
    
    public Receipt__c rcpt {get;set;}
    
    public ReciptLayout_Ctrl(){
        
        ID receiptID = ApexPages.currentPage().getParameters().get('ID');
        GetReceiptInfo(receiptID );
    }
    
    
    void GetReceiptInfo(Id aReceiptID){
        
        rcpt = [Select Name ,CreatedBy.Name, Receipt_Type__c,Customer__r.Name,Customer__r.BP_No__c,Amount__c,Cheque_Date__c,
                Card_Type__c,Card_Amount__c,Bank_Ref_No__c,Payment_Status__c,Bank_Name__c ,Processing_Charge__c ,
                Payment_Gateway_Ref_No__c,Transfer_Date__c ,View__c ,
                SAP_Receipt_No__c,SAP_Receipt_Date__c,Transaction_Date__c FROM
                Receipt__c WHERE Id =: aReceiptID];
        if(rcpt.SAP_Receipt_Date__c != NULL){
            Integer offset = UserInfo.getTimezone().getOffset(rcpt.SAP_Receipt_Date__c);
            rcpt.SAP_Receipt_Date__c=  rcpt.SAP_Receipt_Date__c.addSeconds(offset/1000);
        }
        
    }
    
    
}