global class DocuSchedule implements Schedulable
{
    global void execute(SchedulableContext sc)
    {
        
        // We now call the batch class to be scheduled
        DocuCollectionBatch b = new DocuCollectionBatch ();
       
        //Parameters of ExecuteBatch(context,BatchSize)
        database.executebatch(b,200);
    }
   
}