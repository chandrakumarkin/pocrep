global with sharing class GsScheduleBatchVisaExcpEmpNotifications implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable {
  
  global void execute(SchedulableContext ctx) {
    Database.executeBatch(new GsScheduleBatchVisaExcpEmpNotifications(), 100);
  }
    
  
   global list<Visa_Exception__c> start(Database.BatchableContext BC ){
  
    Date expiryDate15 = system.today().addDays(15);
    Date expiryDateTemp = system.today().addMonths(1);  
    List<Visa_Exception__c>  lstvisaEx = [select id,Account__c,Portal_User_1__c,Portal_User_2__c,Portal_User_3__c,Portal_User_4__c,Portal_User_5__c,Sys_Send_Email__c from Visa_Exception__c where Visa_Exception_Review_Date__c =: expiryDate15 and  Account__c!= '']; 
      return lstvisaEx;
  }
  
  global void execute(Database.BatchableContext BC, list<Visa_Exception__c> lstvisaExs){
    list<Id> lstAccountIds = new list<Id>();
    map<Id,list<string>> mapEmpServiceUsers = new map<Id,list<string>>();
    list<Visa_Exception__c> lstContactInsert = new list<Visa_Exception__c>();
    
    for(Visa_Exception__c  objvisaEx : lstvisaExs){
      lstAccountIds.add(objvisaEx.Account__c);
    }
    
    for(User objUser : [select Id,Email,Contact.AccountId from User where IsActive = true AND User.Community_User_Role__c includes ('Employee Services') AND ContactId != null AND ContactId IN (select Id from Contact where AccountId IN : lstAccountIds AND RecordType.DeveloperName = 'Portal_User') ]){
      list<string> lstUserIds = mapEmpServiceUsers.containsKey(objUser.Contact.AccountId) ? mapEmpServiceUsers.get(objUser.Contact.AccountId) : new list<string>();
      lstUserIds.add(objUser.Id);
      mapEmpServiceUsers.put(objUser.Contact.AccountId,lstUserIds);
    }
    
    for(Visa_Exception__c objvisaEx : lstvisaExs){
      list<string> lst = mapEmpServiceUsers.get(objvisaEx.Account__c);
      if(lst != null && !lst.isEmpty()){
        Visa_Exception__c  objTemp = new Visa_Exception__c(Id= objvisaEx .Id);
        objTemp.Sys_Send_Email__c = true;
        if(lst.size() > 0)
          objTemp.Portal_User_1__c = lst[0];
        if(lst.size() > 1)
          objTemp.Portal_User_2__c = lst[1];
        if(lst.size() > 2)
          objTemp.Portal_User_3__c = lst[2];
        if(lst.size() > 3)
          objTemp.Portal_User_4__c = lst[3];
        if(lst.size() > 4)
          objTemp.Portal_User_5__c = lst[4];
        lstContactInsert.add(objTemp);
      }
    }
    if(!lstContactInsert.isEmpty()){
      update lstContactInsert;
    }
  }
  
  global void finish(Database.BatchableContext BC){
  }
  
 
}