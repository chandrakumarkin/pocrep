/*********************************************************************************************************************
    Author      :   Saima Hidayat
    Description :   Shares/Membership Interest Details for the Shareholder or Member
    Date        :   21-Sep-2014
    
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.1    09-02-2015  Saima         Removed condition for currency picklist is null in calcshares func.
V1.2  04-02-2016  Saima      Added code for restrict reduction of no. of shares in existing active class.
V1.3  04-09-2016  Sravan      Added code for Ticket # 2825
V1.4    14-11-2016  Sravan      Added Status field in the Accountshare query Tkt # 3315
v1.7    13-02-2018  Danish    Create a field on account ' Min capital requirements' to allows submission if min requirement of 50,000 is not required  tkt # 4804   
V1.8    11-10-2018  Arun     Removed validation for Authorized Share Capital and Share tkt #5858
V1.9   09-Jan-2019  Arun     Removed Authorized Share Capital for all comapnies 
*********************************************************************************************************************/
public without sharing class ShareCapitalCls {

    
    /* Start of Properties for Dynamic Flow */
    public boolean isChangesSaved = false;
    public boolean isProcessFlow{get;set;}
    public string strPageId{get;set;}
    public string strHiddenPageIds{get;set;}
    public string strActionId{get;set;}
    public map<string,string> mapParameters;
    public Service_Request__c objSRFlow{get;set;}
    
    public string pageTitle{get;set;}
    public string pageDescription{get;set;}
    public string checkFrmDetail{set;get;}   // V1.3

    /* End of Properties for Dynamic Flow */
    
    Public string srStatus{get;set;} // V1.4 
    
    String SRId;
    public boolean delBtn{get;set;}
    Public String SRtype{get;set;}
    Public String AutType{get;set;}
    Public String strType{get;set;}
    public Service_Request__c obSR{get;set;}
    public Integer RowId{get;set;}
    public List<Account_Share_Detail__c> accShare {get; set;}
    Decimal productShares=0;
    Decimal sumShares = 0;
    Decimal totalAuthorized = 0;
    Decimal totalshare;
    Decimal sum;
    map<string,string> mapCurrencyIds = new map<string,string>();
    map<string,string> mapCurrencyNames = new map<string,string>();
    map<string,decimal> mapCurrencyRates = new map<string,decimal>();
    
   
    public ShareCapitalCls(){
   
      SRId=ApexPages.currentPage().getParameters().get('Id');
      /* Start of Properties Initialization for Dynamic Flow */
      isProcessFlow = false;
      isChangesSaved = false;
      mapParameters = new map<string,string>();
      if(apexpages.currentPage().getParameters()!=null)
      mapParameters = apexpages.currentPage().getParameters();
      //V1.3
      if(mapParameters !=null && mapParameters.containsKey('fd'))
      {
        checkFrmDetail =  mapParameters.get('fd'); // V1.3
      }
      
      objSRFlow = new Service_Request__c();
      objSRFlow.Id = SRId;
      if(objSRFlow.Id!=null){
         strPageId = mapParameters.get('PageId');
           if(strPageId!=null && strPageId!=''){
                for(Page__c page:[select id,Name,Page_Description__c from Page__c where Id=:strPageId]){
                    pageTitle = page.Name;
                    pageDescription = page.Page_Description__c;
                }
           }
           if(mapParameters!=null && mapParameters.get('FlowId')!=null){
             set<string> SetstrFields = Cls_Evaluate_Conditions.FetchObjectFields(mapParameters.get('FlowId'),'Service_Request__c');
             string strQuery = 'select Id';
             SetstrFields.add('legal_structures__c');
             SetstrFields.add('customer__c');
             SetstrFields.add('finalizeamendmentflg__c');
             SetstrFields.add('share_capital_membership_interest__c');
             SetstrFields.add('record_type_name__c');
             SetstrFields.add('currency_list__c');
             SetstrFields.add('currency_rate__c');
             SetstrFields.add('External_Status_Code__c');
             SetstrFields.add('isClosedStatus__c');
          //   SetstrFields.add('Record_type_Name__c'); // V1.3
             if(SetstrFields!=null && SetstrFields.size()>0){
               for(String strFld:SetstrFields){
                 if(strFld.toLowerCase()!='id')
              strQuery += ','+strFld.toLowerCase();
               }
             }
             
             strQuery = strQuery+' from Service_Request__c where Id=:SRId';
             system.debug('strQuery===>'+strQuery);
             for(Service_Request__c SR:database.query(strQuery)){
               objSRFlow = SR;
             }
             strHiddenPageIds = PreparePageBlockUtil.getHiddenPageIds(mapParameters.get('FlowId'),objSRFlow);
           }
        }
        /* End of Properties Initialization for Dynamic Flow */
        
        List <Service_Request__c> srDetails = [Select recordtypeid,Share_Capital_Membership_Interest__c,Issued_Share_Capital_Membership_Interest__c,currency_list__c,
                                               No_of_Authorized_Share_Membership_Int__c,No_of_Issued_Share_Membership_Interest__c,External_Status_Code__c,
                                               Customer__r.Min_capital_requirements__c from Service_Request__c
                                               where id=:objSRFlow.Id];
                                               
        srStatus = srDetails[0].External_Status_Code__c; // V1.4
        
        obSR = new Service_Request__c(Id=SRId,recordtypeid=srDetails[0].recordtypeid,legal_structures__c=objSRFlow.legal_structures__c,Customer__c=objSRFlow.Customer__c,Share_Capital_Membership_Interest__c=srDetails[0].Share_Capital_Membership_Interest__c,
                                       Issued_Share_Capital_Membership_Interest__c=srDetails[0].Issued_Share_Capital_Membership_Interest__c,
                                       No_of_Authorized_Share_Membership_Int__c=srDetails[0].No_of_Authorized_Share_Membership_Int__c,
                                       No_of_Issued_Share_Membership_Interest__c=srDetails[0].No_of_Issued_Share_Membership_Interest__c,currency_list__c=srDetails[0].currency_list__c,
                                       Customer__r = srDetails[0].Customer__r);
       // obSR.Customer__r.Min_capital_requirements__c = srDetails[0].Customer__r.Min_capital_requirements__c;
        //obSR = srDetails[0];
        strType= mapParameters.get('Type');
        if(objSRFlow.record_type_name__c =='Change_in_Authorized_Shares_or_Nominal_Value'){
          if(strType=='amendshares' && strType!=null && objSRFlow.legal_structures__c=='LLC'){
            strType = 'Member';
          }else{
            strType = 'Shareholder';
          }
        }
        
        if(strType == 'Shareholder'){
          SRtype = 'Shares';
          AutType = 'Share Capital';
        }
        else{
          SRtype = 'Membership Interest';
          AutType = 'Membership Interest';
        }
        
        for(Currency__c curr : [Select id,Name,Exchange_Rate__c from Currency__c where Active__c=true]){
            mapCurrencyIds.put(curr.Name,curr.Id);
            mapCurrencyNames.put(curr.Id,curr.Name);
            mapCurrencyRates.put(curr.Name,curr.Exchange_Rate__c);
        }
        
        accShare = new List<Account_Share_Detail__c>();
        List <Account_Share_Detail__c> accountShare = [Select id,Name,Sys_Proposed_No_of_Shares_per_Class__c,Sys_Proposed_Nominal_Value__c,Nominal_Value__c,No_of_shares_per_class__c,Total_Issued__c,Status__c,Total_Issues_Shares_sys_Proposed__c              
                                                from Account_Share_Detail__c 
                                                where Account__c =:obSR.Customer__c AND Status__c!='Inactive'];  // V1.4 // Added Total_Issues_Shares_sys_Proposed__c for V1.3 
         if(accountShare!=null && accountShare.size()>0){
             accShare.addAll(accountShare);
         }else{
             addRow();
         }
         if(objSRFlow.record_type_name__c =='Change_in_Authorized_Shares_or_Nominal_Value'){
               delBtn = false;
         }else
             delBtn = true;
             
   }
   
   public Component.Apex.PageBlock getDyncPgMainPB(){
        PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = objSRFlow; 
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        return objPB.getDyncPgMainPB();
    }
    
    public pagereference DynamicButtonAction(){
        system.debug('strActionId==>'+strActionId);
        
        if(strActionId!=null && strActionId!=''){
          boolean isNext = false;
          boolean isAllowToproceed = false;
        for(Section_Detail__c objSec:[select id,Name,Component_Label__c,Navigation_Direction__c from Section_Detail__c where Id=:strActionId]){
          if(objSec.Navigation_Direction__c=='Forward'){
            isNext = true;
          }else{
            isNext = false;
          }
        }
        if(isNext==true){
           for(Account_Share_Detail__c acc:[select id from Account_Share_Detail__c where Account__c=:obSR.Customer__c AND Status__c!='Inactive']){
             isAllowToproceed = true;
           }
          
        }else{
          isAllowToproceed = true;
        }
        if(isAllowToproceed==true){
          PreparePageBlockUtil.FlowId = mapParameters.get('FlowId'); 
          PreparePageBlockUtil.PageId = mapParameters.get('PageId');
          PreparePageBlockUtil.objSR = objSRFlow;
          PreparePageBlockUtil.ActionId = strActionId;
            
            PreparePageBlockUtil objPB = new PreparePageBlockUtil();
            pagereference pg = objPB.getButtonAction();
            system.debug('pg==>'+pg);
            return pg;
        }else{
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please add atleast one class detail to proceed.'));
          return null;
        }
        }
        return null;
        }
        
       
    public void Save_ShareDetail(){
      SRId=ApexPages.currentPage().getParameters().get('Id');
      list<Service_Request__c> lstSR = [Select id,finalizeAmendmentFlg__c From Service_Request__c where id=:SRId];
      obSR.finalizeAmendmentFlg__c = lstSR[0].finalizeAmendmentFlg__c;
      system.debug('======>>>>> '+SRId+'  SR:   '+lstSR[0].finalizeAmendmentFlg__c);
      //if(!obSR.finalizeAmendmentFlg__c ){ // Commented for V1.3
        
        
        if(!obSR.finalizeAmendmentFlg__c || (obSR.finalizeAmendmentFlg__c && checkFrmDetail =='1'))
        { // V1.3
          
         /* if( field is no longer valid Share_Capital_Membership_Interest__c V1.9
          
          (obSR.Share_Capital_Membership_Interest__c==NULL||obSR.currency_list__c ==null ||obSR.currency_list__c =='')
          
          &&
          obSR.legal_structures__c!='Public Company'
                && obSR.legal_structures__c!='LTD')
          {
           Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill in the required fields below!'));
                return;
           }
          else{*/
              
            sumShares = 0;
            totalAuthorized = 0;
            if(obSR.currency_list__c!=null && mapCurrencyIds.get(obSR.currency_list__c)!=null){
               obSR.Currency_Rate__c = mapCurrencyIds.get(obSR.currency_list__c);
            }
            Map<String,Account_Share_Detail__c> m1 = new Map<String,Account_Share_Detail__c>();
            for(Account_Share_Detail__c acclist: accShare){
                if(m1.get(acclist.Name)==NULL){
                   m1.put(acclist.Name,acclist);
                }
                else{
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Duplicate class names found!'));
                    return;
                }
            }
              
            for(Integer i=0; i<accShare.size();i++){
           
            if(accShare[i].Name == NULL || accShare[i].Sys_Proposed_Nominal_Value__c == NULL || accShare[i].Sys_Proposed_No_of_Shares_per_Class__c == NULL||accShare[i].Sys_Proposed_Nominal_Value__c == 0 || accShare[i].Sys_Proposed_No_of_Shares_per_Class__c == 0)
            {
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill in the required fields below!'));
                return;
                
            }else if(accShare[i].Sys_Proposed_No_of_Shares_per_Class__c < accShare[i].Total_Issued__c){ //V1.2 - Code for Existing Active Class
              
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'The no. of shares for Class '+accShare[i].Name+' cannot be less than issued shares!'));
                  return;
                                
            }else if(checkFrmDetail =='1' && objSRFlow.Record_type_Name__c =='Application_of_Registration' && objSRFlow.isClosedStatus__c == false && accShare[i].Total_Issues_Shares_sys_Proposed__c !=null && accShare[i].Sys_Proposed_No_of_Shares_per_Class__c < accShare[i].Total_Issues_Shares_sys_Proposed__c){ // V1.3
                Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'The no. of shares for Class '+accShare[i].Name+' cannot be less than issued shares!'));
                  return;
                  
            }else{
                productShares = accShare[i].Sys_Proposed_Nominal_Value__c * accShare[i].Sys_Proposed_No_of_Shares_per_Class__c;
                sumShares = sumShares + accShare[i].Sys_Proposed_No_of_Shares_per_Class__c;
                totalAuthorized = totalAuthorized+productShares;
                accShare[i].Currency__c = obSR.Currency_Rate__c;
                }
            }
          
          boolean IsValid=false;
           
          //V1.8
          //Tickect 5858
          
          if( obSR.legal_structures__c!='LTD')//v. 1.8 Remove the requirement of Min share capital. 
            {
                
            
            
                //V1.8
              //if(totalAuthorized > obSR.Share_Capital_Membership_Interest__c && obSR.legal_structures__c!='Public Company')
              //{
                //Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'The total amount of '+SRtype+' is greater than the Authorized '+SRtype+' !'));
                //return;
              //}
              
              //V1.8
               if(obSR.legal_structures__c=='Public Company')//The min share capital must be at USD 100,000. Set a validation for the same on the system. 
              {
                  
                   if(obSR.Customer__r.Min_capital_requirements__c==false
                           &&  (totalAuthorized*mapCurrencyRates.get(obSR.currency_list__c)<100000)
                  )
                  {
                      Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'The total amount of Share must be greater than or equal to $100,000!'));
                      return;
                  }
                  else
                      IsValid=true;
                  
              }
              else{
                  
                 
                  if(!obSR.Customer__r.Min_capital_requirements__c
                
                  && obSR.legal_structures__c!='LTD SPC' && obSR.legal_structures__c!='LTD IC' && totalAuthorized*mapCurrencyRates.get(obSR.currency_list__c)<50000)
                  {
                      Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'The total amount of Share must be greater than or equal to $50,000!'));
                      return;
                  }
                  else if(!obSR.Customer__r.Min_capital_requirements__c && obSR.legal_structures__c=='LTD SPC' && totalAuthorized*mapCurrencyRates.get(obSR.currency_list__c)<100)
                  {
                      Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'The total amount of Share must be greater than or equal to $100!'));
                      return;
                  }
                  else
                  {
                 
                    IsValid=true;
                 
                    }
               }
            
            }
            else IsValid=true;
           
            
            
            if(IsValid)
            {
                 obSR.No_of_Authorized_Share_Membership_Int__c = sumShares;
                upsert accShare;
                  update obSR;
                
            }
            
              //}//V1.9
            //Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.CONFIRM,SRtype+' details are saved sucessfully!'));
            return;
      }else{
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Your request has been finalised and cannot be updated.'));
          return;
        }
    }
        
   public void delRow(){
       if(!obSR.finalizeAmendmentFlg__c){
        if(accShare.size() == 1){
           Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Atleast one class must be defined!'));
           return;
       }else{
           List <Account_Share_Detail__c> acc = [Select id from Account_Share_Detail__c where id= :accShare[RowId].id];
           
           if(acc.size()==0)
             accShare.remove(RowId);
           else{
               delete acc;
               accShare.remove(RowId);
           }
    
       }
       calcShares();
       }else{
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Your request has been finalised and cannot be updated.'));
          return;
      }
   }
   public void addRow(){
       system.debug(obSR.finalizeAmendmentFlg__c);
       if(!obSR.finalizeAmendmentFlg__c){
         accShare.add(new Account_Share_Detail__c(Account__c = obSR.Customer__c,Status__c='Draft'));    
       }else{
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Your request has been finalised and cannot be updated.'));
          return;
    }
   }

    public string strNavigatePageId{get;set;}
    public pagereference goTopage(){
      if(strNavigatePageId!=null && strNavigatePageId!=''){
          PreparePageBlockUtil objSidebarRef = new PreparePageBlockUtil();
          PreparePageBlockUtil.strSideBarPageId = strNavigatePageId;
          PreparePageBlockUtil.objSR = objSRFlow;
          return objSidebarRef.getSideBarReference();
      }
      return null;
  }
    
    
    public void calcShares(){
   
    totalshare=0;
    sum=0;
    sumShares=0;
    
    if(obSR.Share_Capital_Membership_Interest__c==null && obSR.No_of_Authorized_Share_Membership_Int__c==null){//V1.1
      list<Account> acc = [Select id,Authorized_Share_Capital__c,Currency__c,No_of_Authorized_Shares__c from Account where id=:obSR.Customer__c ];
      obSR.Share_Capital_Membership_Interest__c = acc[0].Authorized_Share_Capital__c;
      obSR.Currency_list__c = mapCurrencyNames.get(acc[0].Currency__c);
      obSR.No_of_Authorized_Share_Membership_Int__c = acc[0].No_of_Authorized_Shares__c;
    }
    
    for(Shareholder_Detail__c shlist:[Select id,Sys_Proposed_No_of_Shares__c,No_of_Shares__c,Nominal_Value__c from Shareholder_Detail__c where Account__c =:obSR.Customer__c AND Status__c!='Inactive']){
      if(shlist.Sys_Proposed_No_of_Shares__c != null && shlist.Nominal_Value__c!= null){
        totalshare = totalshare + shlist.Nominal_Value__c;
        sum = sum + shlist.Sys_Proposed_No_of_Shares__c;
       }
      System.debug('totalshare--->shlist.Nominal_Value__c'+totalshare+'---->'+shlist.Nominal_Value__c);
      System.debug('sum--->sh.Sys_Proposed_No_of_Shares__c'+sum+'----->'+shlist.Sys_Proposed_No_of_Shares__c);
    }
    for(Account_Share_Detail__c acclt :[Select id,Sys_Proposed_Nominal_Value__c,Sys_Proposed_No_of_Shares_per_Class__c,Nominal_Value__c,No_of_shares_per_class__c 
                                                from Account_Share_Detail__c 
                                                where Account__c =:obSR.Customer__c AND Status__c!='Inactive']){
     if(acclt!=null && acclt.Sys_Proposed_No_of_Shares_per_Class__c!=Null && acclt.Sys_Proposed_No_of_Shares_per_Class__c!=0){
        sumShares = sumShares+acclt.Sys_Proposed_No_of_Shares_per_Class__c; 
     } 
         System.debug('acclt.Sys_Proposed_No_of_Shares_per_Class__c--->'+sumShares+'---'+acclt.Sys_Proposed_No_of_Shares_per_Class__c);                                  
    }
    obSR.No_of_Authorized_Share_Membership_Int__c = sumShares;
    obSR.Issued_Share_Capital_Membership_Interest__c = totalshare;
    obSR.No_of_Issued_Share_Membership_Interest__c = sum;
    //update obSR;
 
  }
  
  public pagereference BackToServiceRequest()
  {
    pagereference pg;
    if(obSR.id !=null)
      pg = new pagereference('/'+obSR.id);
      
     return pg;  
  }
 
    
    
}