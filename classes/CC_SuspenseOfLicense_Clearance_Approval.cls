/*
    Author      : 
    Date        : 10-Jun-2021
    Description : Custom code to create Draft AOR Financial Service Request
    ---------------------------------------------------------------------------------------
*/
global with sharing class CC_SuspenseOfLicense_Clearance_Approval implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
                
            List<Sobject> sobjectList= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(stp.HexaBPM__SR__c),'HexaBPM__Customer__r.Index_Card_Status__c');
            List<HexaBPM__Service_Request__c> srList = (List<HexaBPM__Service_Request__c>) sobjectList;
            HexaBPM__Service_Request__c srRecord = (srList.size() >0) ? srList[0]:new HexaBPM__Service_Request__c();
          
            try{
                if(stp.HexaBPM__Step_No__c == 90 || stp.HexaBPM__Step_No__c == 90.0){
                    strResult = NewSRUtilityController.SendEmailNotifications(srRecord,stp,'',new list<string>{'E-Certificate of Status'},'License_Suspension_Approval_Notification');
                    system.debug('=====1111====strResult==========='+strResult);
                    if(strResult == 'success'){
                        List<Account> accList = [select Id,Suspension_End_Date__c,Date_of_Suspension__c,ROC_Status__c from Account
                            where Id =:srRecord.HexaBPM__Customer__c limit 1 
                        ];   
                        if(accList.size() >0){
                            List<Compliance__c> complainceList = [select Id,Status__c,Start_Date__c,End_Date__c,
                                Exception_Date__c from Compliance__c where Account__c =:srRecord.HexaBPM__Customer__c
                                and ( Status__c ='Open' OR Status__c ='Created') and 
                                (Name = 'License Renewal' OR Name = 'Confirmation Statement' OR Name ='Notification Renewal')
                            ]; 
        
                            if(srRecord.Approval_Date_Sharing_of_Office__c != null){
                                accList[0].Date_of_Suspension__c = srRecord.Approval_Date_Sharing_of_Office__c;
                                accList[0].ROC_Status__c = 'Inactive - Suspended';
                                Integer noYear = (srRecord.Duration_of_suspension__c == '3 years') ? 3 : (srRecord.Duration_of_suspension__c == '2 years') ? 2 : 1;
                                accList[0].Suspension_End_Date__c = accList[0].Date_of_Suspension__c.addYears(noYear);
                                update accList; 
                                List<Compliance__c> updatedList = new List<Compliance__c>();
                                for(Compliance__c comp : complainceList){
                                    comp.Start_Date__c = comp.Start_Date__c.addYears(noYear);
                                    comp.End_Date__c = comp.End_Date__c.addYears(noYear);
                                    comp.Exception_Date__c = comp.Exception_Date__c.addYears(noYear);
                                    if(accList[0].Suspension_End_Date__c > comp.Exception_Date__c) comp.Exception_Date__c = accList[0].Suspension_End_Date__c;
                                    updatedList.add(comp);
                                }
                                if(updatedList.size() >0)update updatedList;
                            }
                        }
                    } 
                }else{
                    Integer noYear = (srRecord.Duration_of_suspension__c == '3 years') ? 3 : (srRecord.Duration_of_suspension__c == '2 years') ? 2 : 1;
                    srRecord.DFSA_DNFBP_Approved_Date__c = system.today().addYears(noYear);
                    update srRecord;
                }
            }catch (Exception ex) {
                strResult = ex.getMessage();
                system.debug('=====excee====strResult==========='+strResult);
                Log__c objLog = new Log__c(Description__c = ex.getMessage(),Type__c = 'CC_SuspenseOfLicense_Clearance Approval Method');
                insert objLog;

            }
        }
        return strResult;
    }
}