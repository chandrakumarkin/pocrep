/**
 * Description : Controller for OB_VoidDocusignDocument component
 *
 * ****************************************************************************************
 * History :
 * [06.FEB.2020] Prateek Kadkol - Code Creation
 */
public without sharing class OB_VoidDocusignDocumentController {

    @AuraEnabled
    public static RespondWrap voidEnvelope(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        
        string docuSignID  = '';
        for(dfsle__Envelope__c envObj : [select id,dfsle__DocuSignId__c from dfsle__Envelope__c where id = : reqWrap.evelopeId]){
            docuSignID = envObj.dfsle__DocuSignId__c;
        }

        if(docuSignID != ''){

            try{
            
                dfsle.UUID envId = dfsle.UUID.parse(docuSignID);
                respWrap.voidSuccsess  = dfsle.StatusService.voidEnvelope(envId,'tst');
             
            }catch(DMLException e) {
            
               string DMLError = e.getdmlMessage(0) + '';
               if(DMLError == null) {
                 DMLError = e.getMessage() + '';
                }
                  respWrap.errorMessage = DMLError;
            }
            
        }
        
        return respWrap;
    }
    
    @AuraEnabled
	public static RespondWrap checkRender(String requestWrapParam)  {


	
			//declaration of wrapper
			RequestWrap reqWrap = new RequestWrap();
			RespondWrap respWrap =  new RespondWrap();
			
			//deseriliaze.
			reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
	
			respWrap.renderButton = false;

			
			
			for(HexaBPM__Step__c stepOBj : [SELECT ID FROM HexaBPM__Step__c WHERE Step_Template_Code__c = 'ENTITY_SIGNING' 
			AND HexaBPM__Status_Type__c != 'END' AND HexaBPM__SR__c =:reqWrap.srId  LIMIT 1]){
				
				respWrap.renderButton = true;
			}
			return respWrap;
	}
    
    @AuraEnabled
	public static RespondWrap checkReRender(String requestWrapParam)  {


	
			//declaration of wrapper
			RequestWrap reqWrap = new RequestWrap();
			RespondWrap respWrap =  new RespondWrap();
			
			//deseriliaze.
			reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
	
			respWrap.renderButton = false;

			
			
			for(HexaBPM__Step__c stepOBj : [SELECT ID FROM HexaBPM__Step__c WHERE Step_Template_Code__c = 'ENTITY_SIGNING' 
			AND HexaBPM__Status_Type__c != 'END' AND HexaBPM__SR__c =:reqWrap.srId  LIMIT 1]){
				
				respWrap.renderButton = true;
			}
			return respWrap;
	}
        
        
    // ------------ Wrapper List ----------- //
        
    public class RequestWrap{
    
        @AuraEnabled public String evelopeId;  
        @AuraEnabled public String srId;
    }
        
    public class RespondWrap{
    
        @AuraEnabled public boolean voidSuccsess;
        @AuraEnabled public string errorMessage;
        @AuraEnabled public boolean renderButton ;
    }
    
}