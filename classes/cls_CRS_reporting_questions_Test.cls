@isTest
public without sharing class cls_CRS_reporting_questions_Test
{

static testmethod void myTest1(){
      
    map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DLD_NOC')])
        {
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = 'Yes';
        insert objAccount;
        
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
         system.runAs(objUser){
   
   CRS_Form__c ObjForm=new CRS_Form__c();
   
   
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(ObjForm);
        
        cls_CRS_reporting_questions ObjCRS=new cls_CRS_reporting_questions(con );
        ObjCRS.submitNull();
         Apexpages.currentPage().getParameters().put('myParam','Yes');
        ObjCRS.RFI_Selected();
        ObjCRS.NullReturn_Other();
        ObjCRS.ThirdParty_Yes();
        ObjCRS.due_diligence_procedures_yes();
        ObjCRS.clearly_identified_group_other_check();
        ObjCRS.due_diligence_procedures_check();
        ObjCRS.Pre_existingAccountss_check();
        ObjCRS.Undocumented_Accounts_check();
        ObjCRS.Excluded_Accounts_Accounts_check();
        ObjCRS.High_Value_Accounts_check();
        ObjCRS.Lower_Value_Accounts_check();
        
         CRS_Question__c  ObjCrsQ=new CRS_Question__c ();
         ObjCrsQ.Account__c=objAccount.id;
         insert ObjCrsQ;
         
    Apexpages.Standardcontroller CRscon = new Apexpages.Standardcontroller(ObjCrsQ);
        
        clsCRS_reporting_questions_file  CRSQu=new clsCRS_reporting_questions_file(CRscon );
        CRSQu.SaveRecord();
           ObjCRS.RediectURL();        
       
        
     }
          
    }
  }