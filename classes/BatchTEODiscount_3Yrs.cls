public class BatchTEODiscount_3Yrs implements Schedulable,Database.Batchable<sObject>{
    public Database.QueryLocator start (Database.BatchableContext bc){        
        String q = 'SELECT Id, Executive_Company__c, TEO_Discount__c, ROC_reg_incorp_Date__c FROM Account WHERE Executive_Company__c = TRUE';
        system.debug('### q'+q);
        return Database.getQueryLocator(q);
    }
    
    public void execute(Database.BatchableContext bc, List<Account> accList){
        List<Account> updatedAccList = new List<Account>();
        system.debug('### accList '+accList);
        system.debug('### accList size '+accList.size());
        for(Account acc : accList){
            system.debug('### acc.ROC_reg_incorp_Date__c '+acc.ROC_reg_incorp_Date__c);
            if(acc.ROC_reg_incorp_Date__c != null && acc.ROC_reg_incorp_Date__c.addYears(3) <= Date.today()){                                
                system.debug('### acc.ROC_reg_incorp_Date__c.addYears(3) >= Date.today() '+(acc.ROC_reg_incorp_Date__c.addYears(3) >= Date.today()));
                acc.TEO_Discount__c = false;
                updatedAccList.add(acc);
            }            
        }
        system.debug('### updatedAccList '+updatedAccList);
        system.debug('### updatedAccList size '+updatedAccList.size());
        if(!updatedAccList.isEmpty()){
            update updatedAccList;
        }
    }
    
    public void finish (Database.BatchableContext bc){
        system.debug('### account field is updated');
    }
    
    public void execute(SchedulableContext ctx){
        Database.executeBatch(new BatchTEODiscount_3Yrs());
    }
    
    public static void updateTEODiscount(){
        List<Account> accList = [SELECT Id, Executive_Company__c, TEO_Discount__c,ROC_reg_incorp_Date__c 
                                 FROM Account 
                                 WHERE Executive_Company__c = TRUE];
        List<Account> updatedAccList = new list<Account>();
        for(Account a : accList){
            system.debug('### a ROC_reg_incorp_Date__c '+a.ROC_reg_incorp_Date__c);
            if(a.ROC_reg_incorp_Date__c != null && a.ROC_reg_incorp_Date__c <= Date.today() && Date.today() <= a.ROC_reg_incorp_Date__c.addYears(3) ){
                a.TEO_Discount__c = true;
                updatedAccList.add(a);
            }
        }
        system.debug('### updatedAccList '+updatedAccList);
        if(!updatedAccList.isEmpty()){
            update updatedAccList;
        }
    }
}