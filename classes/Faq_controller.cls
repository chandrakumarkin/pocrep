/******************************************************************************************
 *  Company     : NSI JLT
 *  Date        : 12-Dec-2014
 *  Description : Controls the FAQs page in the customer portal
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    12-Dec-2014  DIFC Admin    Created
V1.1     7-Dec-2016  Claude        Added user roles and new fields for FAQs (#3737)
****************************************************************************************************************/
public class Faq_controller {

    /**
     * This will store the subheadings of each 
     * main section, mapped according to the
     * order set in the 'FAQ Order' Custom Setting
     * @See 'FAQ Order'
     */
    public Map<String,Map<Integer,String>> faqsMapSubHeadingMap {get; set;}
    
    /**
     * This will store the mapped list of FAQs
     * with the sub-sections numbered based
     * on the 'FAQ Order' Custom Setting
     */
    public Map<String,Map<Integer,List<Faq__c>>> faqsMapSorted {get; set;}
    
    /* Default Constructor */
    public Faq_controller(){}
    
    /**
     * Loads the FAQ records
     */
    public void load_faqs(){     
        
        /* This will store the subheading's order equivalent */
        Map<String,Integer> faqsMapSubHeadingOrder = new Map<String,Integer>();
        
        /* This will store the mapped FAQ Records */
        Map<String,Map<String,List<Faq__c>>> faqsMap = new Map<String,Map<String,List<Faq__c>>>();
        
        /* Initialize Maps */
        faqsMapSorted = new Map<String,Map<Integer,List<Faq__c>>>();
        faqsMapSubHeadingMap = new Map<String,Map<Integer,String>>();
        
        /* Get the USER's Contact role */
        List<Contact> usersContact = [SELECT Role__c FROM Contact where id IN (SELECT ContactId FROM User WHERE Id =: UserInfo.getUserId())];
        
        if(!usersContact.isEmpty() || Test.isRunningTest()){
            
            String contactRole = Test.isRunningTest() ? 'Company Services' : usersContact[0].Role__c;
        
            /* Query the faq records */
            List<Faq__c> faqsInternal = [SELECT id, name,   Screenshot__c, Heading__c, Sub_heading__c, Type__c, Question__c, Answer__c FROM Faq__c WHERE Type__c != NULL AND Heading__c != NULL ORDER BY name];
        
            /* Create the wrapper list */
            for(Faq__c f : faqsInternal){
                
                Map<String,List<Faq__c>> listOfFaqsPerCategory = faqsMap.containsKey(f.Heading__c) ? faqsMap.get(f.Heading__c) : new Map<String,List<Faq__c>>();
                
                String subHeading = String.isBlank(f.Sub_heading__c) ? '' : f.Sub_heading__c;
                
                List<Faq__c> faqRecords = listOfFaqsPerCategory.containsKey(subHeading) ? listOfFaqsPerCategory.get(subHeading) : new List<Faq__c>();
                
                /* NOTE: If you want to add more conditions, add them in this set of conditions
                         as a Boolean variable */
                Boolean isClientFaq = !contactRole.equals('Fit-Out Services') &&  // Client 
                                        !contactRole.equals('Event Services') &&
                                        ( f.Type__c.contains('Client') || f.Type__c.contains('General') );
                                        
                Boolean isContractor = contactRole.equals('Fit-Out Services') &&  // Contractor
                                        !contactRole.equals('Event Services') &&
                                        ( f.Type__c.contains('Contractor') || f.Type__c.contains('General')  ) && 
                                        !f.Type__c.contains('Client');
                                        
                Boolean isEvent = !contactRole.equals('Fit-Out Services') &&  // Event Organizer
                                    contactRole.equals('Event Services') &&
                                    f.Type__c.contains('Event Organizer') &&
                                    !f.Type__c.contains('Client');     
                                    
                /* After all conditions have been met, set them as OR conditions */                                    
                Boolean isVisible = isClientFaq ||  isContractor  ||   isEvent;                               
                
                if(isVisible){
                    faqRecords.add(f);
                    listOfFaqsPerCategory.put(subHeading ,faqRecords);
                    faqsMap.put(f.Heading__c,listOfFaqsPerCategory);
                }
                
                
            }
            
            /* 
             * There is always a case wherein two or more 'FAQ Order' 
             * records have the same order, and that the list itself 
             * is not in the proper order. So, the setting must be ordered
             * accordingly.
             * 
             * IMPORTANT:
             * 
             * The custom setting is only mandatory if the FAQ records
             * are using sub-headings. By default, those who do not
             * have sub-headings do not need to be set. However, the
             * index '0' is reserved for these types of FAQ records, 
             * and therefore cannot be used in the custom setting. Also,
             * make sure that the subheadings are ordered properly. 
             * 
             * i.e. If subheadings 'Apple' and 'Orange' of heading 'Fruits'
             *      have the same index of '1', one of them will be overwritten
             *      in the view.
             */
            
            /* Get the order, IF any */
            Map<String,FAQ_Order__c> mapOfFAQOrder = FAQ_Order__c.getAll();

            /* Order the FAQOrder records */
            List<Integer> faqOrder = new List<Integer>();
            
            for(FAQ_Order__c f : mapOfFAQOrder.values()){
                faqOrder.add(Integer.valueOf(f.Order__c));
            }
            
            /* Order the list */
            faqOrder.sort();

            Set<String> faqSubheadingNames = new Set<String>();
            
            faqsMapSubHeadingOrder.put('',0); // Blank sub-headings will not be counted

            for(Integer i : faqOrder){
                
                System.debug(i);
                
                /* Populate the map with the key-value pair format 'Sub-heading' => 'Order' */
                for(FAQ_Order__c f: mapOfFAQOrder.values()){
                
                    if(Integer.valueOf(f.Order__c)== i){
                        faqsMapSubHeadingOrder.put(f.Name,i);
                    }
                
                }
                
            }
            
            /* Change the sub-headings to number to bypass the ordering algorithm of apex:repeat */
            for(String heading : faqsMap.keySet()){
            
                /* Get the list of faq records */
                Map<String,List<Faq__c>> listOfFaqsPerCategory = faqsMap.get(heading); 
            
                Map<Integer,List<Faq__c>> listOfFaqsPerCategorySorted = new Map<Integer,List<Faq__c>>();
            
                for(String subHeading : listOfFaqsPerCategory.keySet()){
                    
                    /* Add each sub-heading as a numbered key */    
                    listOfFaqsPerCategorySorted.put(faqsMapSubHeadingOrder.get(subHeading),listOfFaqsPerCategory.get(subheading));
                }
            
                faqsMapSorted.put(heading,listOfFaqsPerCategorySorted);
                
            }
            
            /* Create the map of sub-sections per header */
            for(String heading : faqsMap.keySet()){
                
                Map<Integer,String> orderedSubSectionMap = new Map<Integer,String>();
                
                /* Get the sub-sections */
                for(String k :faqsMap.get(heading).keySet()){
                    
                    Integer order = faqsMapSubHeadingOrder.get(k);
                    orderedSubSectionMap.put(order,k);
                }
                
                /* Populate the map for retrieval in the VF page */
                faqsMapSubHeadingMap.put(heading,orderedSubSectionMap);
                
            }
        }
    }
}