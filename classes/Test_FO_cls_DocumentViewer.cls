/******************************************************************************************
 *  Author   	: Claude Manahan
 *  Company  	: NSI JLT
 *  Description : Test Class for FO_cls_DocumentViewer
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    23-06-2016	Claude		  Created
****************************************************************************************************************/
@isTest
private class Test_FO_cls_DocumentViewer {

    @testSetup static void setTestData() {
    	
    	System.runAs(new User(Id = UserINfo.getUSerId())){
	    	WebService_Details__c testCredentials = new WebService_Details__c();
			
			testCredentials.Name = 'Credentials';
			testCredentials.Password__c = '12345678';
			testCredentials.SAP_User_Id__c = 'TEST_SAP';
			testCredentials.Username__c = 'TEST_SAP';
			
			insert testCredentials;
    	}
    	
    	Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
    	Account objContractor = Test_CC_FitOutCustomCode_DataFactory.getTestContractor();
		
		insert new List<Account>{objAccount,objContractor};
		
		Contact objContact = Test_CC_FitOutCustomCode_DataFactory.getTestContact(objAccount.Id);
        
        insert objContact;
        
        /* Create Documents for fit-out Manuals */
		Document fitOutManual = Test_CC_FitOutCustomCode_DataFactory.getTestDocument(UserInfo.getUserId(),'DIFC_Fit_Out_Manual');
		Document eventManual = Test_CC_FitOutCustomCode_DataFactory.getTestDocument(UserInfo.getUserId(),'DIFC_Event_Manual');
		Building__c testBuilding = Test_CC_FitOutCustomCode_DataFactory.getTestBuilding();
		
		insert new List<Document>{fitOutManual,eventManual};
		
		SR_Status__c testSubmittedStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Submitted','SUBMITTED');
        SR_Status__c testDraftStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Draft','DRAFT');
        SR_Status__c testApprovedStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Approved','APPROVED');
        
		insert new List<SR_Status__c>{testSubmittedStatus,testDraftStatus,testApprovedStatus};
		
    	Step_Template__c reUploadstep = new Step_Template__c(Code__c = 'RE_UPLOAD_DOCUMENT',Name='Re-upload Document',Step_RecordType_API_Name__c = 'General');
    	
    	Status__c reUploadStatus = new Status__c(Name='Awaiting Re-upload',Code__c='AWAITING_RE_UPLOAD');
    	Status__c testStepStatus = new Status__c(Name='Approved',Code__c='APPROVED');
    	
    	Step_Template__c printPassTemplate = new Step_Template__c(Code__c = 'PRINT PASS',Name='Print Pass',Step_RecordType_API_Name__c = 'Print_Pass');
    	
    	insert printPassTemplate;
		insert reUploadstep;
		insert new List<Status__c>{reUploadStatus,testStepStatus};
		
		insert Test_CC_FitOutCustomCode_DataFactory.getCountryLookupList();
		insert Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
		insert testBuilding;    
		
		/* Create other dependencies needed for the tests */
		List<Unit__c> testUnits = Test_CC_FitOutCustomCode_DataFactory.getTestUnits(testBuilding.Id);
		Lease__c testLease = Test_CC_FitOutCustomCode_DataFactory.getTestLease(objAccount.Id);
		
		insert testUnits;
		insert testLease;	
    }
    
    static TestMethod void testClsDocumentViewerFitOutPortal(){
		Test_FitoutServiceRequestUtils.testClsDocumentViewerFitOutPortal();
	}
	
	static TestMethod void testClsDocumentViewerEventPortal(){
		Test_FitoutServiceRequestUtils.testClsDocumentViewerEventPortal();
	}
	
	static TestMethod void testClsDocumentViewerDifc(){
		Test_FitoutServiceRequestUtils.testClsDocumentViewerDifc();
	}
	
	static TestMethod void testClsDocumentViewerSystemAdmin(){
		Test_FitoutServiceRequestUtils.testClsDocumentViewerSystemAdmin();
	}
    
}