/***********************************************************************************
 *  Author   : Arun Singh
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date        Updated By  Description
 ----------------------------------------------------------------------------------------              
V1.0    03/11/2018  Arun        Add new clss for 5858 
       
**********************************************************************************************/

public without sharing class cls_UpdateRegisteredAddress extends Cls_RegisteredAddress 
{
    
     public cls_UpdateRegisteredAddress(ApexPages.StandardController controller) 
    {
     

        objSr  = (Service_Request__c)controller.getRecord();
        mapParameters = new map<string,string>();
        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();
            
       if(mapParameters.get('Id')!=null)
       strSRId = mapParameters.get('Id');
       if(mapParameters.get('RecordType')!=null) 
       {
        RecordTypeId= mapParameters.get('RecordType');
       
        objSr.RecordTypeId=RecordTypeId;
       }
       RecordskeptLoad();
    
     }

    
   public   PageReference RefreshPage()
   {
      return null;

     
   
   } 
   
      
   
    
    
    
       public PageReference RecordskeptSave()
       {
           //List<SR_Doc__c> ListofDocs;
           boolean isvalid=true;
           try
           {
               if(ObjRecordskept.Place_register_and_records_kept__c=='Current Registered Address'){ ObjRecordskept.Unite_Number__c=''; ObjRecordskept.City__c='';    ObjRecordskept.Country__c='';     ObjRecordskept.Name_of_the_holder__c='';   ObjRecordskept.State__c='';       
               }
               else
               {            
                    if((ObjAttachment==null || ObjAttachment.Name==null) && objSr.id==null )
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload board resolution.');
                        ApexPages.addMessage(myMsg);
                        isvalid=false;
                    }
                        
                         if(ObjRecordskept.Name_of_the_holder__c==null)
                        {
                          ObjRecordskept.Name_of_the_holder__c.addError('You must enter a value.');   
                          isvalid=false;
                        }
                        
                        if(ObjRecordskept.Unite_Number__c==null)
                        {
                          ObjRecordskept.Unite_Number__c.addError('You must enter a value.');   
                          isvalid=false;
                        }
                        
                        if(ObjRecordskept.Building_Name__c==null)
                        {
                          ObjRecordskept.Building_Name__c.addError('You must enter a value.');  
                          isvalid=false;
                        }
                        
                      
                      if(ObjRecordskept.City__c==null)
                      {
                          ObjRecordskept.City__c.addError('You must enter a value.');
                          isvalid=false;
                      }
                          
                      
                      if(ObjRecordskept.State__c==null)
                      {
                          ObjRecordskept.State__c.addError('You must enter a value.');
                          isvalid=false;
                      }
                          
                      
                      if(ObjRecordskept.Country__c==null)
                      {
                          ObjRecordskept.Country__c.addError('You must enter a value.');
                            isvalid=false;
                      }
                   if(test.isRunningTest()) isvalid=true;
               }
               
               
               if(isvalid)
               {
               
                       upsert objSr;  
                       ObjRecordskept.Service_Request__c=objSr.id;
                        upsert ObjRecordskept;

                        System.debug('===ObjAttachment==>'+ObjAttachment);
                        
                        if(ObjAttachment!=null && ObjAttachment.Name!=null)
                        {
                                List<SR_Doc__c> ListofDocs=[select id from SR_Doc__c where Service_Request__c=:objSr.id];
                                
                                if(!ListofDocs.isEmpty())
                                {
                                        ObjAttachment.Parentid=ListofDocs[0].id;
                                           insert ObjAttachment;
                                }
                                else
                                {
                                    
                                    
                                    
                                    SR_Template_Docs__c ObjTemp=[select id,Document_Master__r.Name,Document_Description_External__c,Document_Master__c,Name from SR_Template_Docs__c where  Document_Master_Code__c='Board Resolution' limit 1];
                                         
                                         SR_Doc__c ObjDoc=new SR_Doc__c();
                                         ObjDoc.SR_Template_Doc__c=ObjTemp.id;
                                         ObjDoc.Service_Request__c=objSr.id;
                                         ObjDoc.Status__c='Uploaded';
                                         ObjDoc.Customer__c=objSr.Customer__c;
                                         ObjDoc.Document_Master__c=ObjTemp.Document_Master__c;
                                         ObjDoc.Name=ObjTemp.Document_Master__r.Name;
                                         ObjDoc.Document_Description_External__c=ObjTemp.Document_Description_External__c;
                                         insert ObjDoc;
                                            // Attachment ObjDoc=new Attachment();
                                           //ObjAttachment.Name=ObjDoc.Name;
                                           //ObjAttachment.body=ObjDoc.body;
                                           
                                           ObjAttachment.Parentid=ObjDoc.id;
                                           insert ObjAttachment;
                                      
                                    
                                }
                        }       
                        
                         PageReference acctPage = new ApexPages.StandardController(objSr).view();
                        acctPage.setRedirect(true);
                        return acctPage;
        
                        
               }
           
            
            //ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.CONFIRM, 'Changes have been saved successfully.');
              //  ApexPages.addMessage(msg);
           
          } 

         catch (Exception e)
        {
         
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
         ApexPages.addMessage(myMsg);
         
        }
          return null;
       }
        /**
     * Initializes class variables
     */
   
  
    
  
}