/******************************************************************************************
 *  Author   	: Claude Manahan
 *  Company  	: NSI JLT
 *  Description : Batchable class that will handle a bulk update when relationships are created
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    23-06-2016	Claude		  Created
****************************************************************************************************************/
global class CRM_batchedRelationshipHandler implements Queueable {
	
	global List<Account> accountsToUpdate;
	
	global CRM_batchedRelationshipHandler(List<Account> accountsToUpdate){
		this.accountsToUpdate = accountsToUpdate;
	}
	
	global void execute(QueueableContext SC) {
   		
		System.debug('Relationships created');
		
		update accountsToUpdate;
   	}
    
}