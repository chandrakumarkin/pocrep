/*************************************************************************************************
 *  Name        : CLS_Payment
 *  Author      : Kaavya Raghuram
 *  Company     : NSI JLT
 *  Date        : 2014-09-18     
 *  Purpose     : This class is used to create a transaction receipt record for submitting to Payment Gateway  
 -----------------------------------------------------------------------------------------------------------------
 Modification History
 -----------------------------------------------------------------------------------------------------------------
 V.No       Modified By               Description
 V1.0       Sravan                    Added code to decrypt Merchant ID      
***************************************************************************************************/

global class CLS_Payment{
    
    
    webservice static String Paymentformsubmit(string accId,Decimal amt,String serv){
        try{
            
            //Create a receipt record with values entered by the customer
            Receipt__c R = new Receipt__c();
            R.Customer__c=accId;
            R.Amount__c=amt; 
            R.Receipt_Type__c='Card';
            R.Transaction_Date__c=system.now();           
                   
            Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
            R.RecordTypeId=rt;
            insert R;
            Receipt__c newR;
            
            //Query the newly created receipt record
            newR =[select id,name,Amount__c,Card_Amount__c from Receipt__c where id=:R.id];
            
            // Create a pipe separated string of all fields to be sent in the request 
            //String message=newR.name+'|AED|'+amt+'|https://dev-difc.cs18.force.com/VF_PaymentStatusUpdate|https://dev-difc.cs18.force.com/VF_PaymentStatusUpdate|01|INTERNET|||||||Soloman|Vandy|Abhinav Nagar|Borivali|New York|NY|100255|US|merchant@test.com|34|344|34355344|3453453523|||John|Lenon|Phoenix Mill|Lower Parel|Mumbai|NY|123123|US|45|455|4534534|1321223122||test||1|Book|FALSE|aaa|bbb|ccc|ddd|eee|';
            String message=newR.name+'|AED|'+newR.Card_Amount__c+'|'+Label.ForceSite_Domain+'/VF_PaymentStatusUpdate|'+Label.ForceSite_Domain+'/VF_PaymentStatusUpdate|01|INTERNET|||||||||||||||||||||||||||||||||||||||||||||';
            
            system.debug('RRReqqMessage===>'+message);
            
            //Get the encryption key from custom setting
            String keyval= Label.Encryption_Key;
            Blob cryptoKey = EncodingUtil.base64Decode(keyval);
            
            //Get the initialization vector from custom setting
            Blob iv = Blob.valueof('0123456789abcdef');
            
            //Convert the request string to Blob
            Blob data = Blob.valueOf(message);
            
            //Encrypt the data using Salesforce Crypto class
            Blob encryptedData = Crypto.encrypt('AES256', cryptoKey,iv, data);
            system.debug('EEEE=>'+encryptedData) ;
            
            // Convert the encrypted data to string
            String encryptedMessage  = EncodingUtil.base64Encode(encryptedData); 
            System.debug('EEEESSSS=>'+encryptedMessage);
            
            string decryptedMerchantID =  test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(Label.Merchant_ID) :'123456'; // V1.0
            //Append the encrypted string to the MerchantID
            String req= decryptedMerchantID + '|' + encryptedMessage + '|';
            system.debug('RRRRR===>'+req);
            return req;        
        }                
        catch(Exception e){
            return null;     
        }
    }
    
   
    
}