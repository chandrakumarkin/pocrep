/**
    *Author      : Rajil
    *CreatedDate : 10-12-2019
    *Description : Test class for OB_QuickContactController
  
      Version    Author    Date           Comment
  ------------------------------------------------------------------------------------------------
     v1.1      Leeba     2-April-2020   updated the class to cover new methods
**/
@isTest
public class OB_QuickContactControllerTest {
     @isTest
    private static void initTest(){
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 

        //Opportunity Creation
        Opportunity o = new Opportunity();
        o.AccountId = insertNewAccounts[0].Id;
        o.Name = 'Test_Joe_123';
        o.StageName = 'Prospecting';
        o.CloseDate = date.today();
        o.Type = 'New Business';
        insert o; 
                    
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                Community_User_Role__c = 'Company Services',
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;

         //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'ENTITY_NAME_REVIEW'}, new List<String> {'ENTITY_NAME_REVIEW'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        // listStepTemplate[0].Step_Template_Code__c = 'ENTITY_NAME_REVIEW';
        // update listStepTemplate[0];

        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;
        
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[0].id;
        insert insertNewSRs;

        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
        insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insert insertNewSteps;
        
        HexaBPM__Page_Flow__c objpgflow = new HexaBPM__Page_Flow__c();
        objpgflow.Name = 'Super User Authorization';
        objpgflow.HexaBPM__Record_Type_API_Name__c = 'Superuser_Authorization';
        objpgflow.Display_as_Index__c = true;
        insert objpgflow;
        
        system.runAs(user){
            OB_QuickContactController.viewContactInfo();
            OB_QuickContactController.loadPageFlow();
        }
    }


}