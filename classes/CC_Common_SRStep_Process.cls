public without sharing class CC_Common_SRStep_Process{

    public static string RemoveEntachmentRemainder(Step__c objStep){
        
        string strResult = 'Success';
        
        List<Step__c> stepList = [select Id,SR__c,SR__r.Customer__c from Step__c where Id=:objStep.Id limit 1];
        if(stepList.size() >0){
            List<Enactment_Notification__c> entList = [select Id,Send_Email__c,Account__c,Type__c from Enactment_Notification__c 
                where Account__c =:objStep.SR__r.Customer__c and Send_Email__c = true and Type__c = 'Liquidator Pending Dissolution Customer'
            ];
            List<Enactment_Notification__c> entUpdateList = new List<Enactment_Notification__c>();
            
            for(Enactment_Notification__c ent: entList){
                ent.Send_Email__c= false;
                entUpdateList.add(ent);
            }
            if(entUpdateList.size() >0){
                update entUpdateList;
            }
        }
        return strResult;
    } 
}