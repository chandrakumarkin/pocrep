/*
Author      :   Ravi
Description :   This class will be controller for the Listing website Listing Advanced Search Results page
--------------------------------------------------------------------------------------------------------------------------

Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No	Date		Updated By    	Description
--------------------------------------------------------------------------------------------------------------------------             
*/
public without sharing class ListingSearchResults {
	
	public string ResultsJSONString {get;set;} 
	public string JSONFeaturedString {get;set;}
	public Integer TotalPages {get;set;}
	public Integer ResultCount {get;set;}
	private static String baseUrl = 'https://www.google.com/recaptcha/api/siteverify';
    public String name {get; set;}
    public String email {get; set;}
    //public String emailTo {get; set;}
    private String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
	// The keys you get by signing up for reCAPTCHA for your domain
	private static String secret = '6LeRBBETAAAAAC67vzPZUYKp3qHYVH9gVEf_QBr8';
    public Inquiry__c objInquiry {get; set;}
    public string buildingJSONString {get;set;}
    public Boolean isResults {get;set;}

	map<string,string> mapParameters = new map<string,string>{
		'rentOrSale'=>'Listing_Type__c',
		'buildingName'=>'Building_Name__c',
		'propertyType'=>'Property_Type__c',
		'minPrice'=>'Price__c',
		'maxPrice'=>'Price__c',
		'minSize'=>'Sq_Feet__c',
		'maxSize'=>'Sq_Feet__c',
		'leasingType'=>'Leasing_Type__c',
		'fnbNonFnb'=>'Food_and_Beverage__c',
		'bedroomsNum'=>'No_of_Bedrooms__c',
        'cageNo'=>'Cage_No__c',
		'pax'=>'No_of_Pax__c',
		'filter'=>'',
		'page'=>''
	};
	
	public ListingSearchResults(){
		list<SearchDetails> lstSD = new list<SearchDetails>();
		list<SearchDetails> lstFSD = new list<SearchDetails>();
		SearchDetails objSD;
		
		map<string,string> mapPageParameters = Apexpages.currentPage().getParameters();
		
		string sQry = 'select Id,Name,Building_Name__c,Unit__c,Unit__r.Building__r.Building_on_Website__c,Start_Date__c,End_Date__c,Listing_Type__c,Price__c,Sq_Feet__c,Cage_No__c,No_of_Bedrooms__c,No_of_Pax__c,Property_Type__c,';
		sQry += 'Featured__c,Address_Details__c,Agent_Details_if_any__c,Amenities__c,Company_Name__c,Company_Profile__c,Food_and_Beverage__c,Listing_Description__c,Listing_Title__c,';
		sQry += 'Location__c,No_of_Days__c,Website__c,Work_Phone__c,Unit__r.Name,Status__c,Unit__r.Building__r.Building_Coordinates__Latitude__s,Unit__r.Building__r.Building_Coordinates__Longitude__s,';
		sQry += '(select Id,Name,Doc_ID__c,Document_Master__r.Code__c from SR_Docs__r where Listing_Doc_Status__c = \'Active\' AND Doc_ID__c != null)';
		sQry += 'from Listing__c where Status__c = \'Active\' ';
		
		if(mapPageParameters != null){
			for(string key : mapPageParameters.keySet()){
				if(mapPageParameters.get(key) != null && mapPageParameters.get(key) != ''){
					if(key == 'minPrice' && isNumber(mapPageParameters.get(key)) ){
						sQry += ' AND Price__c >= '+mapPageParameters.get(key);
					}else if(key == 'maxPrice' && isNumber(mapPageParameters.get(key)) ){
						sQry += ' AND Price__c <= '+mapPageParameters.get(key);
					}else if(key == 'minSize' && isNumber(mapPageParameters.get(key)) ){
						sQry += ' AND Sq_Feet__c >= '+mapPageParameters.get(key);
					}else if(key == 'maxSize' && isNumber(mapPageParameters.get(key)) ){
						sQry += ' AND Sq_Feet__c <= '+mapPageParameters.get(key);
					}else if(key=='rentOrSale'){
						sQry += ' AND Listing_Type__c = \''+mapPageParameters.get(key)+'\'';
					}else if(key=='buildingName'){
						sQry += ' AND Unit__r.Building__r.Building_on_Website__c = \''+mapPageParameters.get(key)+'\'';
                        
						system.debug('Building_Name__c is : '+mapPageParameters.get(key));
					}else if(key=='propertyType'){
						sQry += ' AND Property_Type__c = \''+mapPageParameters.get(key)+'\'';
					}else if(key=='leasingType'){
						sQry += ' AND Leasing_Type__c = \''+mapPageParameters.get(key)+'\'';
					}else if(key=='bedroomsNum'){
						sQry += ' AND No_of_Bedrooms__c = \''+mapPageParameters.get(key)+'\'';
					}else if(key=='pax'){
						sQry += ' AND No_of_Pax__c = ' +mapPageParameters.get(key);
					}else if(key=='fnbNonFnb'){
						sQry += ' AND Food_and_Beverage__c = '+(mapPageParameters.get(key)=='Fnb' ? true : false);
                    }else if(key=='cageNo') {
                        sQry += ' AND Cage_No__c = \''+mapPageParameters.get(key)+'\'';
                    }
				}
			}
			if(mapPageParameters.containsKey('filter') && mapPageParameters.get('filter') != null){
				sQry += ' order by ';
				string str = mapPageParameters.get('filter');
				if(str == 'published'){
					sQry += ' Published_Date__c desc';
				}else if(str == 'area-max'){
					sQry += ' Sq_Feet__c desc';
				}else if(str == 'area-min'){
					sQry += ' Sq_Feet__c asc';
				}else if(str == 'price-high'){
					sQry += ' Price__c desc';
				}else if(str == 'price-low'){
					sQry += ' Price__c asc';
				}
			}else{
				sQry += ' order by Published_Date__c desc';
			}
		}
		system.debug('sQry is  :'+sQry);
		for(Listing__c obj : database.query(sQry)){
			objSD = PrepareSD(obj);
			if(obj.Featured__c)
				lstFSD.add(objSD);
            
			lstSD.add(objSD);
		}
		if(lstSD.isEmpty() && !lstFSD.isEmpty())
			isResults = true;
		if(lstFSD.isEmpty()){
			for(Listing__c obj : [select Id,Name,Building_Name__c,Unit__c,Unit__r.Building__r.Building_on_Website__c,Start_Date__c,End_Date__c,Listing_Type__c,Price__c,Sq_Feet__c,No_of_Bedrooms__c,No_of_Pax__c,Property_Type__c,
						Featured__c,Address_Details__c,Agent_Details_if_any__c,Amenities__c,Company_Name__c,Company_Profile__c,Food_and_Beverage__c,Listing_Description__c,Listing_Title__c,
						Location__c,No_of_Days__c,Website__c,Work_Phone__c,Unit__r.Name,Status__c,Unit__r.Building__r.Building_Coordinates__Latitude__s,Unit__r.Building__r.Building_Coordinates__Longitude__s,
						(select Id,Name,Doc_ID__c,Document_Master__r.Code__c from SR_Docs__r where Listing_Doc_Status__c = 'Active' AND Doc_ID__c != null)
						from Listing__c where Status__c = 'Active' AND Featured__c = true order by Published_Date__c desc]){
				objSD = PrepareSD(obj);
				lstFSD.add(objSD);
			}
		}
		/*lstSD.addAll(lstSD);
		lstSD.addAll(lstSD);
		lstSD.addAll(lstSD);
		lstSD.addAll(lstSD);*/
		decimal iC = Math.mod(lstSD.size(), 24) > 0 ? (lstSD.size()/24)+1 : (lstSD.size()/24); 
		TotalPages = iC.intValue();
		ResultCount = lstSD.size();
		Integer pageNo = (mapPageParameters.get('page') != null && mapPageParameters.get('page') != '') ? Integer.valueOf(mapPageParameters.get('page')) : 1;
		if(!lstSD.isEmpty()){
			list<SearchDetails> lstSDTemp = new list<SearchDetails>();
			for(Integer i = ((pageNo-1)*24);i<((pageNo-1)*24+24);i++){
				if(i<ResultCount)
					lstSDTemp.add(lstSD[i]);
			}
			lstSD = lstSDTemp;
		}
		ResultsJSONString = JSON.serialize(lstSD);
		JSONFeaturedString = JSON.serialize(lstFSD);
        system.debug('ResultsJSONString is : '+ResultsJSONString);
        system.debug('JSONFeaturedString is : '+JSONFeaturedString);
        
        objInquiry = new Inquiry__c();
       	objInquiry.RecordTypeId = Schema.SObjectType.Inquiry__c.getRecordTypeInfosByName().get('Inquiry').getRecordTypeId();
        list<MapViewDetails> lstMVD = new list<MapViewDetails>();
		MapViewDetails objMVD;
		map<Id,Integer> mapLCount = new map<Id,Integer>();
		for(Listing__c obj : [select Id,Unit__c,Unit__r.Building__c from Listing__c where Unit__r.Building__c != null AND Status__c = 'Active']){
			Integer i = mapLCount.containsKey(obj.Unit__r.Building__c) ? (mapLCount.get(obj.Unit__r.Building__c)+1) : 1;
			mapLCount.put(obj.Unit__r.Building__c,i);
		}
		for(Building__c objB : [select Id,Name,Building_No__c,Building_on_Website__c from Building__c where Building_No__c != null]){
			objMVD = new MapViewDetails();
			objMVD.BuildingName = objB.Name;
			objMVD.BuildingNo = objB.Building_No__c;
            objMVD.BuildingOnWebsite = objB.Building_on_Website__c;
			objMVD.AvailableUnits = mapLCount.containsKey(objB.Id) ? (mapLCount.get(objB.Id)+'') : '0';
			lstMVD.add(objMVD);
		}
		if(!lstMVD.isEmpty())
			buildingJSONString = JSON.serialize(lstMVD);
	}
	
	public SearchDetails PrepareSD(Listing__c obj){
		SearchDetails objSD = new SearchDetails();
		objSD.BuildingName = obj.Unit__r.Building__r.Building_on_Website__c != null ? obj.Unit__r.Building__r.Building_on_Website__c : obj.Building_Name__c;
		objSD.ListingDescription = obj.Listing_Description__c;
		objSD.ListingTitle = obj.Listing_Title__c;
		objSD.ListingType = obj.Listing_Type__c;
		objSD.Location = obj.Location__c;
		objSD.Price = obj.Price__c != null ? obj.Price__c+'' : '';
		objSD.PropertyType = obj.Property_Type__c;
		objSD.RecordId = obj.Id;
		objSD.SqrtFeet = obj.Sq_Feet__c != null ? obj.Sq_Feet__c+'' : '';
		objSD.Status = obj.Status__c;
		objSD.Unit = obj.Unit__r.Name;
		Integer i = 0;
		for(SR_Doc__c objDoc : obj.SR_Docs__r){
			string dName = objDoc.Document_Master__r.Code__c;
			if(dName != 'Fit-Out Manual' && dName != 'Tenant Manual' && dName != 'Floor Plan'){
				if(dName == 'Photograph 1')
					objSD.Photo1 = Site.getPathPrefix()+'/servlet/servlet.FileDownload?file='+objDoc.Doc_ID__c;
				i++;
			}
		}
		objSD.NoOfPhotos = i+'';
		return objSD;
	}
	
	public static Boolean isNumber(String str){
	     return Pattern.matches('[a-zA-Z]+',str) ? false : true ;
	}
		
	public class SearchDetails{
		public string BuildingName {get;set;}
		public string ListingDescription {get;set;}
		public string ListingTitle {get;set;}
		public string ListingType {get;set;}
		public string Location {get;set;}
		public string Price {get;set;}
		public string PropertyType {get;set;}
		public string SqrtFeet {get;set;}
		public string Status {get;set;}
		public string Unit {get;set;}
		public string RecordId {get;set;}
		
		public string Photo1 {get;set;}
		public string NoOfPhotos {get;set;}
	}
    
    public String sitekey {
		get { return '6LeRBBETAAAAAHDagNGgb_LXO_4MpbOIK0oT55co'; }
	}
	public String response  { 
		get { return ApexPages.currentPage().getParameters().get('g-recaptcha-response'); }
	}
    
    public PageReference doVerify () {
		String responseBody = makeRequest(baseUrl,
				'secret=' + secret +
				'&response='+ response
		);
		String success = getValueFromJson(responseBody, 'success');
        
        Pattern MyPattern = Pattern.compile(emailRegex);
    	Matcher MyMatcher = MyPattern.matcher(email);
        if(!MyMatcher.matches()) {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check the email id');
            ApexPages.addMessage(errorMsg);
        }
		else if(success.equalsIgnoreCase('true') && name != null && email != null
          && !name.equalsIgnoreCase('') && !email.equalsIgnoreCase('')){
			
              //list<String> actualNames = (Apexpages.currentPage().getParameters().get('buildingNames')).split(',');
              objInquiry.Building_Name__c = Apexpages.currentPage().getParameters().get('buildingName');//actualNames.get(0);
              
              objInquiry.Email__c = email;
              objInquiry.Listing_Type__c = Apexpages.currentPage().getParameters().get('rentOrSale');
              objInquiry.Sender_Name__c = name;
              if(Apexpages.currentPage().getParameters().get('maxPrice') != '')
              	objInquiry.Max_Price__c = Decimal.valueOf(Apexpages.currentPage().getParameters().get('maxPrice'));
              if(Apexpages.currentPage().getParameters().get('minPrice') != '')
              	objInquiry.Min_Price__c = Decimal.valueOf(Apexpages.currentPage().getParameters().get('minPrice'));
              if(Apexpages.currentPage().getParameters().get('minSize') != '')
              	objInquiry.Max_Sq_Feet__c = Decimal.valueOf(Apexpages.currentPage().getParameters().get('minSize'));
              if(Apexpages.currentPage().getParameters().get('maxSize') != '')
              	objInquiry.Min_Sq_Feet__c = Decimal.valueOf(Apexpages.currentPage().getParameters().get('maxSize'));
              objInquiry.Property_Type__c = Apexpages.currentPage().getParameters().get('propertyType');
              
              if(Apexpages.currentPage().getParameters().get('bedroomsNum') != null)
              objInquiry.No_of_Bedrooms__c = Apexpages.currentPage().getParameters().get('bedroomsNum');
              if(Apexpages.currentPage().getParameters().get('pax') != null && Apexpages.currentPage().getParameters().get('pax') != '')
              	objInquiry.No_of_Pax__c = Decimal.valueOf(Apexpages.currentPage().getParameters().get('pax'));
              
            upsert objInquiry;
            ApexPages.Message successMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Registered');
           	ApexPages.addMessage(successMsg);
		}else if(success.equalsIgnoreCase('false')){
			ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter the captcha');
            ApexPages.addMessage(errorMsg);
        } else {
            ApexPages.Message errorMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please enter all the fields');
            ApexPages.addMessage(errorMsg);
        }
		return null;
	}
    
    /**
	 * Make request to verify captcha
	 * @return 		response message from google
	 */
	private static String makeRequest(string url, string body)  {
		HttpResponse response = null;
		HttpRequest req = new HttpRequest();   
		req.setEndpoint(url);
		req.setMethod('POST');
		req.setBody (body);
		
		try {
			Http http = new Http();
			response = http.send(req);
			return response.getBody();
		} catch(System.Exception e) {
			System.debug('ERROR: ' + e);
		}
		return '{"success":false}';
	} 
    
    /**
	 * to get value of the given json string
	 * @params		
	 *	- strJson		json string given
	 *	- field			json key to get the value from
	 * @return			string value
	 */
	public static string getValueFromJson ( String strJson, String field ){
		JSONParser parser = JSON.createParser(strJson);
		while (parser.nextToken() != null) {
			if ((parser.getCurrentToken() == JSONToken.FIELD_NAME)) {
				if(parser.getText() == field){
					// Get the value.
					parser.nextToken();
					return parser.getText();
				}
			}
		}
		return null;
	}
    
    public class MapViewDetails{
		public string BuildingName {get;set;}
		public string BuildingNo {get;set;}
		public string AvailableUnits {get;set;}
        public string BuildingOnWebsite {get;set;}
	}
}