/******************************************************************************************
 *  Name        : OB_StepTriggerHandler 
 *  Author      : Durga Kandula
 *  Company     : PwC
 *  Date        : 30-Oct-2019
 *  Description : Trigger Handler for OB_StepTrigger
 ----------------------------------------------------------------------------------------                               
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   30-Oct-2019  Durga         Initial Version
 V2.0   24-Apr-2019  Merul         added calculateDueDate
 V3.0   24-Apr-2019  Merul         added calculateBusinessHR
*******************************************************************************************/
public without sharing class OB_StepTriggerHandler 
{
    public static void OnBeforeInsert(list<HexaBPM__Step__c> TriggerNew)
    {
      
        UpdateStepEmail(TriggerNew);
        CopyStepNotesToComments(TriggerNew);
        UpdateStepOwner(TriggerNew);
        RetainPreviousOwner(TriggerNew);
    }   
    
    public static void onAfterInsert(list<HexaBPM__Step__c> TriggerNew)
    {
        Map<Id,HexaBPM__Step__c> mapStepDB = new Map<Id,HexaBPM__Step__c>( 
                                                                                [
                                                                                    SELECT id,
                                                                                            HexaBPM__SR_Step__c,
                                                                                            HexaBPM__SR_Step__r.HexaBPM__Estimated_Hours__c
                                                                                      FROM HexaBPM__Step__c
                                                                                     WHERE id =:TriggerNew
                                                                                ]
                                                                            );
         system.debug('$$$$$$$$$$$onAfterInsert');                                                                   

        calculateDueDate( TriggerNew, mapStepDB );
    }   


   
    
    public static void OnBeforeUpdate(list<HexaBPM__Step__c> TriggerOld,
                                        list<HexaBPM__Step__c> TriggerNew,
                                        map<Id,HexaBPM__Step__c> TriggerOldMap,
                                        map<Id,HexaBPM__Step__c> TriggerNewMap)
    {
        Map<Id,HexaBPM__Step__c> mapStepDB = new Map<Id,HexaBPM__Step__c>( 
                                                                            [
                                                                                SELECT id,
                                                                                        HexaBPM__SR_Step__c,
                                                                                        HexaBPM__SR_Step__r.ownerId
                                                                                FROM HexaBPM__Step__c
                                                                                WHERE id =:TriggerNew
                                                                            ]
                                                                        );
        calculateBusinessHR(TriggerOld,TriggerNew,TriggerOldMap,TriggerNewMap,mapStepDB);
    }



    //V3.0   24-Apr-2019  Merul         added calculateBusinessHR
    public static void calculateBusinessHR(list<HexaBPM__Step__c> TriggerOld,
                                            list<HexaBPM__Step__c> TriggerNew,
                                            map<Id,HexaBPM__Step__c> TriggerOldMap,
                                            map<Id,HexaBPM__Step__c> TriggerNewMap,
                                            Map<Id,HexaBPM__Step__c> mapStepDB
                                          )
    {
            
            Map<Id,Id> mapQueueIdBussId = new Map<Id,Id>();
            List<HexaBPM__Step__c> stepToUpdate = new List<HexaBPM__Step__c>();
 
            for( OBBusinessHour__c bussHrMD : [SELECT id,
                                                    Business_Hour_Id__c,
                                                    Queue_Id__c 
                                                FROM OBBusinessHour__c ])
            {
                mapQueueIdBussId.put( Id.valueOf(bussHrMD.Queue_Id__c) , Id.valueOf(bussHrMD.Business_Hour_Id__c) );
            }
            system.debug('############ mapQueueIdBussId '+mapQueueIdBussId);

            for(HexaBPM__Step__c stepNew : TriggerNew)
            {
                 
                HexaBPM__Step__c stepDB = mapStepDB.get(stepNew.Id);
                Boolean isUpdate = TriggerOldMap != NULL
                                        && TriggerOldMap.get(stepNew.Id).HexaBPM__Closed_Date_Time__c != stepNew.HexaBPM__Closed_Date_Time__c;
                
                system.debug('############ isUpdate '+isUpdate);
                if( isUpdate )
                {
                    system.debug('$$$$$$$$$$$$$$$$$$$$ stepDB.HexaBPM__SR_Step__r.OwnerId '+stepDB.HexaBPM__SR_Step__r.OwnerId);
                    
                    String businessHrsId;
                    if(test.isRunningTest())
                    {
                        businessHrsId = mapQueueIdBussId.get( userinfo.getUserId() );
                    }
                    else
                    {
                        businessHrsId = mapQueueIdBussId.get(stepDB.HexaBPM__SR_Step__r.OwnerId); 
                    }
                    system.debug('$$$$$$$$$$$$$$$$$$$$ businessHrsId '+businessHrsId);
                    system.debug('$$$$$$$$$$$$$$$$$$$$ stepNew.CreatedDate  '+stepNew.CreatedDate);
                    system.debug('$$$$$$$$$$$$$$$$$$$$ stepNew.HexaBPM__Closed_Date_Time__c '+stepNew.HexaBPM__Closed_Date_Time__c);
                    if( String.isNotBlank(businessHrsId))
                    {
                         // OB_Actual_Business_Hours__c 
                        Long milliSec = BusinessHours.diff( Id.valueOf( businessHrsId ), stepNew.CreatedDate,  stepNew.HexaBPM__Closed_Date_Time__c );
                        if( milliSec != NULL)
                        {
                            system.debug('$$$$$$$$$$$$$$$$$$$$  milliSec '+ milliSec);
                            stepNew.OB_Actual_Business_Hours__c = ( Decimal.valueOf(milliSec) / 3600000 );
                            system.debug('$$$$$$$$$$$$$$$$$$$$  stepNew.OB_Actual_Business_Hours__c '+ stepNew.OB_Actual_Business_Hours__c);
                        }
                    }
                   
                 
                } 

            }
    }

    //  V2.0   30-Oct-2019  Merul      added calculateDueDate
    public static void calculateDueDate(list<HexaBPM__Step__c> TriggerNew , Map<Id,HexaBPM__Step__c> mapStepDB )
    {
           system.debug('$$$$$$$$$$$ calculateDueDate  ');   
           system.debug('$$$$$$$$$$$ calculateDueDate  mapStepDB '+mapStepDB); 
           Map<Id,Id> mapQueueIdBussId = new Map<Id,Id>();
           List<HexaBPM__Step__c> stepToUpdate = new List<HexaBPM__Step__c>();

           for( OBBusinessHour__c bussHrMD : [SELECT id,
                                                         Business_Hour_Id__c,
                                                         Queue_Id__c 
                                                    FROM OBBusinessHour__c ])
           {
               mapQueueIdBussId.put( Id.valueOf(bussHrMD.Queue_Id__c) , Id.valueOf(bussHrMD.Business_Hour_Id__c) );
           }
           system.debug('$$$$$$$$$$ mapQueueIdBussId'+mapQueueIdBussId);

            for( HexaBPM__Step__c stepObjTemp : TriggerNew )
            {
                HexaBPM__Step__c stepObj = new HexaBPM__Step__c(id=stepObjTemp.Id);
                system.debug('$$$$$$$$$$ stepObj owner '+stepObjTemp.ownerId);
                HexaBPM__Step__c stepObjDB = mapStepDB.get(stepObj.Id);

                system.debug('$$$$$$$$$$ stepObjDB.HexaBPM__SR_Step__r.HexaBPM__Estimated_Hours__c '+stepObjDB.HexaBPM__SR_Step__r.HexaBPM__Estimated_Hours__c);
                String businessHrsId;
                if(test.isRunningTest())
                {
                    businessHrsId = mapQueueIdBussId.get( userinfo.getUserId() );
                }
                else
                {
                    businessHrsId = mapQueueIdBussId.get(stepObjTemp.OwnerId); 
                }
                
                
                if( stepObjDB.HexaBPM__SR_Step__r.HexaBPM__Estimated_Hours__c != null &&  businessHrsId !=null)
                {
                    ID BHId = Id.valueOf(businessHrsId);
                    Long sla = stepObjDB.HexaBPM__SR_Step__r.HexaBPM__Estimated_Hours__c.longvalue();
                    sla = sla * 60 * 60 * 1000L;
                    system.debug('########## BHId '+BHId);
                    system.debug('########## sla '+sla);
                    datetime CreatedTime = system.now();
                    stepObj.HexaBPM__Due_Date__c = BusinessHours.add(BHId, CreatedTime, sla);
                    stepToUpdate.add( stepObj );
                }
            }

            if( stepToUpdate.size() > 0 )
            {
                update stepToUpdate;
            }

    }

    
    /*
        Method Name :   UpdateStepEmail
        Description :   Method to copy SR Email to Step Email
    
    */
    public static void UpdateStepEmail(list<HexaBPM__Step__c> TriggerNew){
        for(HexaBPM__Step__c stp:TriggerNew){
            stp.Email__c = stp.Customer_Email__c;
        }
    }
    
    /*
        Method Name :   CopyStepNotesToComments
        Description :   Method to copy step notes to internal comments
    
    */
    public static void CopyStepNotesToComments(list<HexaBPM__Step__c> TriggerNew){
        for(HexaBPM__Step__c stp:TriggerNew){
            if(stp.HexaBPM__Step_Notes__c!=null && stp.Is_Client_Step__c)
                stp.Internal_Comments__c = stp.HexaBPM__Step_Notes__c;
        }
    }
    
    /*
        Method Name :   UpdateStepOwner
        Description :   Method to override the step owner in case of RM Step / Opportunity Owner Assignment step 
    
    */
    public static void UpdateStepOwner(list<HexaBPM__Step__c> TriggerNew){
        map<string,HexaBPM__Service_Request__c> MapSRs = new map<string,HexaBPM__Service_Request__c>();
        set<string> setSRIDs = new set<string>();
        for(HexaBPM__Step__c stp:TriggerNew){
            if(stp.Assigned_to_Opportunity_Owner__c && stp.Opportunity_Owner__c!=null)
                stp.OwnerId = stp.Opportunity_Owner__c;
            else if(stp.HexaBPM__SR__c!=null && stp.Step_Template_Code__c=='RM_Assignment')
                setSRIDs.add(stp.HexaBPM__SR__c);
            if(stp.Entity_Type__c=='Retail' && stp.Retail_Queue_Id__c!=null && stp.Retail_Queue_Id__c.startswith('00G'))
                stp.OwnerId = stp.Retail_Queue_Id__c;

            if(stp.Assigned_to_Account_Owner__c && stp.Account_Owner__c!=null){
                stp.OwnerId = stp.Account_Owner__c;
            }
            
        }
        if(setSRIDs.size()>0){
            for(HexaBPM__Service_Request__c objSR:[Select Id,Entity_Type__c,Business_Sector__c,Type_of_Property__c from HexaBPM__Service_Request__c where Id IN:setSRIDs]){
                MapSRs.put(objSR.Id,objSR);
            }
            for(HexaBPM__Step__c stp:TriggerNew){
                if(stp.HexaBPM__SR__c!=null && stp.Step_Template_Code__c=='RM_Assignment'){
                    if(MapSRs.get(stp.HexaBPM__SR__c).Entity_Type__c=='Non - financial' && MapSRs.get(stp.HexaBPM__SR__c).Business_Sector__c=='FinTech'){
                        stp.OwnerId = label.OB_Fintech_Queue_Id;
                    }else if(MapSRs.get(stp.HexaBPM__SR__c).Entity_Type__c=='Non - financial' && MapSRs.get(stp.HexaBPM__SR__c).Business_Sector__c!='FinTech'){
                        stp.OwnerId = label.OB_BD_Non_Financial_Queue_Id;
                    }else if(MapSRs.get(stp.HexaBPM__SR__c).Entity_Type__c=='Retail'){
                        if(MapSRs.get(stp.HexaBPM__SR__c).Type_of_Property__c=='Lease a Kiosk')
                            stp.OwnerId = label.OB_Speciality_Leasing_Queue_Id;
                        else
                            stp.OwnerId = label.OB_Retail_Queue_Id;
                    }
                }
            }
        }
    }
    
    /*
        Method Name :   RetainPreviousOwner
        Description :   Method to assign the owner as previous user who accepts the step
    
    */
    public static void RetainPreviousOwner(list<HexaBPM__Step__c> TriggerNew){
        set<string> setSRIDs = new set<string>();
        set<string> setSRStepIDs = new set<string>();
        map<string,string> MapStepOwner = new map<string,string>();
        for(HexaBPM__Step__c stp:TriggerNew){
            if(stp.Step_Template_Code__c!=null && stp.HexaBPM__SR__c!=null && !stp.Is_Client_Step__c && string.valueof(stp.OwnerId).startswith('00G')){
                setSRIDs.add(stp.HexaBPM__SR__c);
                if(stp.Step_Template_Code__c!='BD_DETAILED_REVIEW')
                    setSRStepIDs.add(stp.Step_Template_Code__c);
                else
                    setSRStepIDs.add('BD_QUICK_REVIEW');
            }
        }
        if(setSRIDs.size()>0){
            for(HexaBPM__Step__c step:[Select Id,OwnerId,HexaBPM__SR__c,Step_Template_Code__c from HexaBPM__Step__c where HexaBPM__SR__c IN:setSRIDs and Step_Template_Code__c IN:setSRStepIDs order by createddate desc]){
                if(MapStepOwner.get(step.HexaBPM__SR__c+'-'+step.Step_Template_Code__c)==null)
                    MapStepOwner.put(step.HexaBPM__SR__c+'-'+step.Step_Template_Code__c,step.OwnerId);
            }
        }
        for(HexaBPM__Step__c stp:TriggerNew){
            if(stp.Step_Template_Code__c!=null && stp.HexaBPM__SR__c!=null && !stp.Is_Client_Step__c && string.valueof(stp.OwnerId).startswith('00G')){
                if(stp.Step_Template_Code__c!='BD_DETAILED_REVIEW' && MapStepOwner.get(stp.HexaBPM__SR__c+'-'+stp.Step_Template_Code__c)!=null)
                    stp.OwnerId = MapStepOwner.get(stp.HexaBPM__SR__c+'-'+stp.Step_Template_Code__c);
                else if(stp.Step_Template_Code__c=='BD_DETAILED_REVIEW' && MapStepOwner.get(stp.HexaBPM__SR__c+'-BD_QUICK_REVIEW')!=null)
                    stp.OwnerId = MapStepOwner.get(stp.HexaBPM__SR__c+'-BD_QUICK_REVIEW');
            }
        }
    }
}