@isTest(seeAllData=false)

public class Testutility{

   public static testMethod service_request__c TestInsertServiceRequest(){
       
        
     Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
        List<user> users = new List<user>();
       
       Relationship__c rel = new Relationship__c();
        rel.Relationship_Type__c ='Is Public Relationship officer for';
        rel.Subject_Account__c= objAccount.id;
        rel.Object_Contact__c= objContact.id;
        rel.Active__c = true;
        insert rel;
        
        
        Document_Details__c doc = new Document_Details__c();
        doc.Contact__c=objContact.id; 
        doc.Document_Type__c ='Employment Visa';
        doc.Document_Number__c='1111';
        insert doc;
        
        Lookup__c lk= new Lookup__c();
        lk.Type__c='Nationality';
        lk.name='India';
        insert lk;
        
        Product2 objProduct = new Product2();
        objProduct.Name = 'DIFC Product';
        objProduct.IsActive = true;
        insert objProduct;
        
        Pricebook2 objPricebook = new Pricebook2();
        objPricebook.Name = 'Test Price Book';
        objPricebook.IsActive = true;
        objPricebook.Description = 'Test Description';
        insert objPricebook;
        
        if(objPricebook.IsActive == false){
            objPricebook.IsActive = true;
            update objPricebook;
        }
        
        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
        
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c='GS';
        insert objTemplate1;
        
        SR_Template__c objTemplate2 = new SR_Template__c();
        objTemplate2.Name = 'Access_Card';
        objTemplate2.SR_RecordType_API_Name__c = 'Access_Card';
        objTemplate2.Active__c = true;
        objTemplate2.SR_Group__c='GS';
        insert objTemplate2;
        
        
        SR_Template_Item__c objSRTempItem = new SR_Template_Item__c();
        objSRTempItem.Product__c = objProduct.Id;
        objSRTempItem.SR_Template__c = objTemplate1.Id;
        objSRTempItem.On_Submit__c = true;
        insert objSRTempItem;
        
       List<SR_Status__c>  srsList = new List<SR_Status__c>();
       
       SR_Status__c srs1  = new SR_Status__c();       
       srs1.name ='Pending';
       srs1.code__c = 'Pending';
       srsList.add(srs1);
       
       SR_Status__c srs2  = new SR_Status__c();       
       srs2.name ='AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
       srs2.code__c = 'AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
       srsList.add(srs2);
       
       
        SR_Status__c srs3  = new SR_Status__c();       
       srs3.name ='Medical fitness certificate received from DHA';
       srs3.code__c = 'Medical fitness certificate received from DHA';
       srsList.add(srs3);
       
       
       insert srsList;
       
        
       //  SR_Status__c srs1= [select id,name,code__c from SR_Status__c where Code__c='Pending' limit 1];
        
        
        system.Test.startTest();
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.RecordTypeId = mapSRRecordTypes.get('DIFC_Sponsorship_Visa_Cancellation').Id;
        objSR1.Entity_Name__c = 'Test Entity Name';
        objSR1.Contact__c=objContact.id;
        objSR1.Pre_Entity_Name_Arabic__c='abc';
        objSR1.Document_Detail__c=doc.id;
        objSR1.ApplicantId_hidden__c=objContact.id;
        //objSR1.SponsorId_hidden__c=objContact.id;
        objSR1.Docdetail_hidden__c=doc.id;
        objSR1.Quantity__c=2;
        objSR1.Nationality_list__c='India';
        objSR1.External_SR_Status__c=srsList[0].id;
        objSR1.Visa_Expiry_Date__c=system.today()+20;
        objSR1.Residence_Visa_No__c='201/2015/1234567';
        objSR1.Statement_of_Undertaking__c = true;
        objSR1.Would_you_like_to_opt_our_free_couri__c ='No';        
        objSR1.Send_SMS_To_Mobile__c ='+971589602235';
        insert objSR1;
        
        return objSR1;
       
    }

}