/*
* Test Class: OB_IdentifyCompanySecretaryController
**/
@isTest
public class OB_IdentifyCSControllerTest {
    @isTest
    private static void initCSTest(){
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        insertNewAccounts[0].ROC_Status__c = 'Active';
        update insertNewAccounts[0];
        
        
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insertNewContacts[0].accountId = insertNewAccounts[0].id;
        insert insertNewContacts;
        
         Relationship__c rel2 = new Relationship__c();
        rel2.Relationship_Type__c ='Is Public Relationship officer for';
        rel2.Subject_Account__c= insertNewAccounts[0].Id;
        rel2.Object_Contact__c= insertNewContacts[0].id;
        rel2.Active__c = true;
        insert rel2;
        
         for( 
				 AccountContactRelation accConRel :  [
																								 SELECT Id,
																												 AccountId,
																												 ContactId,
																												 Roles,
																												 Access_Level__c,
																												 Account.ROC_Status__c
																								 FROM AccountContactRelation
																								 WHERE ContactId =:insertNewContacts[0].id
                     AND AccountId =: insertNewAccounts[0].Id
																								 
																				 
																						 ]
         ){
            AccountContactRelation newe = new AccountContactRelation(id = accConRel.id);
       
            newe.Roles = 'Company Services';
        upsert newe; 
         }

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('AOR_Financial', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Company Secretary';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'ob-identify-company-secretary';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;

        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        //insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        //insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].HexaBPM__customer__c=insertNewAccounts[0].Id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule.HexaBPM__Section_Detail__c = sec.id;
        objPgNavRule.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule.HexaBPM__Section__c=section.id;
        objPgNavRule.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule;
        
        HexaBPM__Page_Flow_Condition__c ObjPgFlowCond = new HexaBPM__Page_Flow_Condition__c();
        ObjPgFlowCond.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        ObjPgFlowCond.HexaBPM__Field_Name__c = 'Id';
        ObjPgFlowCond.HexaBPM__Operator__c = '!='; 
        ObjPgFlowCond.HexaBPM__Value__c = 'null';
        ObjPgFlowCond.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id; 
        insert ObjPgFlowCond;
        
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id;
        insert ObjPgFlowAction;
        
        HexaBPM__Document_Master__c doc= new HexaBPM__Document_Master__c();
        doc.HexaBPM__Available_to_client__c = true;
        doc.HexaBPM__Code__c = 'UBO_FOUNDATION';
        doc.Download_URL__c = 'test';
        insert doc;
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Role__c = 'Shareholder;Guardian;Company Secretary';
        objAmendment.Passport_No__c = '12345';
        objAmendment.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment);

        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment1.Nationality_list__c = 'India';
        objAmendment1.Passport_No__c = '1234';
        objAmendment1.Role__c = 'Company Secretary';
        objAmendment1.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment1.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment1);

        HexaBPM_Amendment__c objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        objAmendment2.Nationality_list__c = 'India';
        objAmendment2.Registration_No__c = '1234';
        objAmendment2.Role__c = 'Company Secretary';
        objAmendment2.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment2.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment2);
        insert listAmendment;

        Account acc = new Account();
        acc.Name = 'testAccount';
        acc.Is_Designated_Member__c = false;
        insert acc;
        
        Relationship__c rel = new Relationship__c();
        rel.Subject_Account__c = insertNewAccounts[0].id;
        rel.Object_Contact__c = insertNewContacts[0].Id;
        rel.Object_Account__c = acc.id;
        insert rel;

		HexaBPM__SR_Doc__c srDoc = new HexaBPM__SR_Doc__c();
        srDoc.HexaBPM_Amendment__c = listAmendment[0].Id;
        srDoc.HexaBPM__Doc_ID__c = listAmendment[0].Id;
        srDoc.Name = 'Passport';
        srDoc.HexaBPM__Service_Request__c=insertNewSRs[0].Id;
        insert srDoc;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = srDoc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        Id p = [select id from profile where name='DIFC Customer Community User Custom'].id;      
        
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = insertNewContacts[0].Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        system.runAs(user){
            test.startTest();
        
        OB_IdentifyCompanySecretaryController.RequestWrapper reqWrapper = new OB_IdentifyCompanySecretaryController.RequestWrapper();
        //reqWrapper.flowId = listPageFlow[0].Id;
        reqWrapper.pageId = listPage[0].Id;
        reqWrapper.srId = insertNewSRs[0].Id;
        String requestString = JSON.serialize(reqWrapper);
        OB_IdentifyCompanySecretaryController.ResponseWrapper respWrap = OB_IdentifyCompanySecretaryController.getExistingAmendment(requestString);
        
        system.debug('=get existing==srwrao=='+respWrap.srWrap.srObj);
        reqWrapper.srId = respWrap.srWrap.srObj.Id;

        reqWrapper.recordtypename= 'Individual';
        OB_IdentifyCompanySecretaryController.ResponseWrapper respWrap1 = OB_IdentifyCompanySecretaryController.initAmendmentDB(JSON.serialize(reqWrapper));
        
        system.debug('====test=amend=='+respWrap1.amedWrap.amedObj);
        respWrap1.amedWrap.amedObj.Passport_No__c = '1234';
        respWrap1.amedWrap.amedObj.Nationality_list__c = 'India';
        reqWrapper.amedWrap = respWrap1.amedWrap;
		reqWrapper.docMap = new map<string,string>{'GENERAL' => documents[0].Id};
        system.debug('=after===test=amend=='+reqWrapper.amedWrap.amedObj);
        OB_IdentifyCompanySecretaryController.ResponseWrapper respWrap3 = OB_IdentifyCompanySecretaryController.onAmedSaveDB(JSON.serialize(reqWrapper));
        
        respWrap1.amedWrap.amedObj.Passport_No__c = '123RAT4';
        respWrap1.amedWrap.amedObj.Nationality_list__c = 'India';
        reqWrapper.amedWrap = respWrap1.amedWrap;
		reqWrapper.docMap = new map<string,string>{'GENERAL' => documents[0].Id};
        system.debug('=after===test=amend=='+reqWrapper.amedWrap.amedObj);
       	respWrap3 = OB_IdentifyCompanySecretaryController.onAmedSaveDB(JSON.serialize(reqWrapper));
       	reqWrapper.amedWrap = respWrap3.amedWrap;
       	respWrap3 = OB_IdentifyCompanySecretaryController.onAmedSaveDB(JSON.serialize(reqWrapper));
        //reqWrapper.fileId = documents[0].Id;
        //OB_IdentifyCompanySecretaryController.ResponseWrapper  ocrproce = OB_IdentifyCompanySecretaryController.processOCR(JSON.serialize(reqWrapper));
        reqWrapper.amendmentID = respWrap3.amedWrap.amedObj.Id;
        OB_IdentifyCompanySecretaryController.ResponseWrapper  deleteOne = OB_IdentifyCompanySecretaryController.removeAmendment(JSON.serialize(reqWrapper));
        OB_IdentifyCompanySecretaryController.getButtonAction(insertNewSRs[0].Id,sec.Id,listPage[0].Id);
        
        reqWrapper.recordtypename= 'Body_Corporate';
        OB_IdentifyCompanySecretaryController.ResponseWrapper bodyRequest = OB_IdentifyCompanySecretaryController.initAmendmentDB(JSON.serialize(reqWrapper));
        bodyRequest.amedWrap.amedObj.Registration_No__c = '1234';
        reqWrapper.amedWrap = bodyRequest.amedWrap;
        bodyRequest = OB_IdentifyCompanySecretaryController.onAmedSaveDB(JSON.serialize(reqWrapper));
        bodyRequest.amedWrap.amedObj.Registration_No__c = '234wer';
        bodyRequest.amedWrap.amedObj.Role__c = 'Company Secretary';
        reqWrapper.amedWrap = bodyRequest.amedWrap;
        bodyRequest = OB_IdentifyCompanySecretaryController.onAmedSaveDB(JSON.serialize(reqWrapper));


        OB_IdentifyCompanySecretaryController.AmendmentWrapper amdwrapper = new OB_IdentifyCompanySecretaryController.AmendmentWrapper();
        amdwrapper.amedObj = listAmendment[0];
        amdWrapper = OB_IdentifyCompanySecretaryHelper.setAmedFromObjAccount(amdwrapper,rel);
        amdWrapper = OB_IdentifyCompanySecretaryHelper.setAmedFromObjContact(amdwrapper,rel);
       
        test.stopTest();
        }
        
        
        
    }
    

}