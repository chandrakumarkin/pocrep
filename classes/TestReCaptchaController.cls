/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestReCaptchaController {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'Test Building';
        objBuilding.Building_No__c = '00000001';
        insert objBuilding;
        
        Unit__c objUnit = new Unit__c();
        objUnit.Building__c = objBuilding.Id;
        objUnit.SAP_Unit_No__c = '0000000000001';
        objUnit.Unit_Square_Feet__c = 1234;
        insert objUnit;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.BP_No__c = '0000400001';
        insert objAccount;
		
		Lease__c objLease = new Lease__c();
		objLease.Account__c = objAccount.Id;
		objLease.Type__c = 'Purchased';
		objLease.Status__c = 'Active';
		objLease.Lease_Types__c = 'Purchased Prop. Registration';
		insert objLease;
		
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Unit__c = objUnit.Id;
        objLP.Account__c = objAccount.Id;
        objLP.Status__c = 'Active';
        objLP.Lease__c = objLease.Id;
        
 		list<Document_Master__c> lstDMs = new list<Document_Master__c>();
 		Document_Master__c objDM = new Document_Master__c();
 		objDM.Name = 'Photograph 1';
 		objDM.Description__c = 'Test';
 		objDM.Code__c = 'Photograph 1';
 		lstDMs.add(objDM);
 		objDM = new Document_Master__c();
 		objDM.Name = 'Photograph 2';
 		objDM.Description__c = 'Test';
 		objDM.Code__c = 'Photograph 2';
 		lstDMs.add(objDM);
 		objDM = new Document_Master__c();
 		objDM.Name = 'Floor Plan';
 		objDM.Description__c = 'Test';
 		objDM.Code__c = 'Floor Plan';
 		lstDMs.add(objDM);
 		insert lstDMs;
 		
 		map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Update_Existing_Listing','New_Property_Listing','Renew_Existing_Listing','De_Listing')]){
        	mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
		}
 		
 		Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('New_Property_Listing');
 		objSR.Listing_Unit__c = objUnit.Id;
 		objSR.Price__c = 12345;
 		objSR.I_agree__c = true;
 		insert objSR;
 		
 		Listing__c objListing = new Listing__c();
 		objListing.Unit__c = objUnit.id;
 		objListing.Property_Type__c = 'Office';
 		objListing.Price__c = 12345;
 		insert objListing;
 		
 		list<SR_Doc__c> lstDocs = new list<SR_Doc__c>();
 		SR_Doc__c objSRDoc = new SR_Doc__c();
 		objSRDoc.Service_Request__c = objSR.Id;
 		objSRDoc.Status__c = 'Uploaded';
 		objSRDoc.Document_Master__c = lstDMs[0].Id;
 		objSRDoc.Is_Not_Required__c = false;
 		objSRDoc.Listing__c = objListing.Id;
 		objSRDoc.Listing_Doc_Status__c = 'Active';
 		objSRDoc.Doc_ID__c = objSR.Id;
 		lstDocs.add(objSRDoc);
 		
 		objSRDoc = new SR_Doc__c();
 		objSRDoc.Service_Request__c = objSR.Id;
 		objSRDoc.Status__c = 'Uploaded';
 		objSRDoc.Document_Master__c = lstDMs[1].Id;
 		objSRDoc.Is_Not_Required__c = false;
 		objSRDoc.Listing__c = objListing.Id;
 		objSRDoc.Listing_Doc_Status__c = 'Active';
 		objSRDoc.Doc_ID__c = objSR.Id;
 		lstDocs.add(objSRDoc);
 		
 		objSRDoc = new SR_Doc__c();
 		objSRDoc.Service_Request__c = objSR.Id;
 		objSRDoc.Status__c = 'Uploaded';
 		objSRDoc.Document_Master__c = lstDMs[2].Id;
 		objSRDoc.Is_Not_Required__c = false;
 		objSRDoc.Listing__c = objListing.Id;
 		objSRDoc.Listing_Doc_Status__c = 'Active';
 		objSRDoc.Doc_ID__c = objSR.Id;
 		lstDocs.add(objSRDoc);
 		insert lstDocs;
 		
 		Apexpages.currentPage().getParameters().put('id',objListing.Id);
 		Apexpages.currentPage().getParameters().put('emailTo','test@test.com');
 		ReCaptchaController objReCaptchaController = new ReCaptchaController();
 		
 		//objReCaptchaController.doVerify();
 		
 		objReCaptchaController.email = 'test@test.com';
 		objReCaptchaController.doVerify();
    }
}