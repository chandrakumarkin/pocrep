//Customized DIFC portal forget password.

public class PortalForgotPassword 
{

public String UserName{get;set;}
public String NewPassword{get;set;}
public String NewcPassword{get;set;}
public boolean DataValueisset{get;set;}
public string DataKey{get;set;}
integer Counter;

//To set new password 
public void NewPasswordSet() 
{
    
    if(Counter==null)
        Counter=0;
    String Userid;
    Datetime DateTimeSet;
    String URLUserName;
    string url=ApexPages.currentPage().getParameters().get('src');
    if(!String.isblank(url))
    {

    Counter++;
    System.debug('url=='+url);
   
   if(!Test.isRunningTest())
    DataKey=decryptedURLData(url);
    
    System.debug('DataKey==>'+DataKey);
    
    List<Forget_Passwords__c> Forgetpass=[select id,Is_Used__c,User_Name__c from Forget_Passwords__c where Is_Active__c=false and Security_Key__c=:DataKey limit 1];
    
    List<string> ListDate=DataKey.split('#');


    Userid=ListDate[0];
    DateTimeSet=Datetime.valueof(ListDate[1]);
    //System.debug('DateTimeSet==>'+DateTimeSet);
    System.debug('Userid==>'+Userid);
    System.debug('DateTimeSet==>'+DateTimeSet);
     System.debug('Forgetpass==>'+Forgetpass);
 
    if(String.isblank(NewPassword)==false && NewPassword==NewcPassword && Forgetpass.isEmpty()==false  && Counter<5)
    {
        
    try{
            System.setPassword(Userid,NewPassword);
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm, 'Your password has been set successfully!'));
            Forgetpass[0].Is_Used__c=true;
            update Forgetpass[0];
            DataValueisset=true;
           }
             catch (Exception e) 
             {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
             }

    
    }
    else if(String.isblank(NewPassword)==false && NewPassword!=NewcPassword)        
    {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Password does not match the confirm password.'));
    }
    else
    {
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Your reset link expired after 24 hours or has already been used.'));
    }
    //return new PageReference ('https://uatfull-difcportal.cs82.force.com/customers/signin');
  }
   
  // return new PageReference ('https://uatfull-difcportal.cs82.force.com/customers/signin');
    

}

//Submit forget password 
public void forgotPassword() 
{

            User ObjectUser=[select Id,Email,ContactID,username from User where isActive=true and username=:UserName];
            System.debug('ObjectUser===>'+ObjectUser);
            if(ObjectUser==null)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'We couldn"t find a DIFC user associated with '+UserName));
            }
            else
            {
                
                
               
                      DataKey=ObjectUser.id+ '#' +System.datetime.Now();
                     string Datvalue=encryptURLData(DataKey);
                     System.debug('==Datvalue==>'+Datvalue);
                   
                   OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
                   
                   
                    Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                    
                    //set default sender if provided 
                    if ( owea.size() > 0 ) {
                    message.setOrgWideEmailAddressId(owea.get(0).Id);
                        }

                    // Set recipients to two contact IDs.
                    // Replace IDs with valid record IDs in your org.
                    List<String> Userd=new List<string>();
                    Userd.add(ObjectUser.id);
                    
                    

                    message.toAddresses = Userd;
                    message.optOutPolicy = 'FILTER';
                    message.subject = 'Finish resetting your Salesforce password';
                    message.plainTextBody = 'DIFC Portal recently received a request to reset the password for the username '+ObjectUser.username+'. To finish resetting your password, go to the following link.  \n This link expires in 24 hours.  \n  \n '+ Label.Community_URL +'/PortalForgotSetPassword?src='+Datvalue;
                    
                    Messaging.SingleEmailMessage[] messages = 
                    new List<Messaging.SingleEmailMessage> {message};
                     Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);
                if (results[0].success) 
                {
                    
                    Forget_Passwords__c ObjForget=new Forget_Passwords__c();
                    ObjForget.Is_Used__c=false;
                    ObjForget.Security_Key__c=DataKey;
                    ObjForget.User_Name__c=UserName;
                    insert ObjForget;
                    DataValueisset=true;
                    
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm, 'Please check your email and click the secure link.'));
                    UserName='';
                }
             
                 else 
                 {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'We apologize for the inconvenience.  Please contact portal@difc.ae for help.'));
             
                }

    
            }


}
String key1='8275918283182858';
public string encryptURLData(string DataValue)
{
    
    
    Blob Key = Blob.valueOf(key1);
    Blob data = Blob.valueOf(DataValue);
    Blob encryptedData = Crypto.encryptWithManagedIV('AES128', key, data);
  
     
    
    return encodingUtil.URLEncode( EncodingUtil.base64Encode(encryptedData),'UTF-8');
    

}
public string decryptedURLData(string DataValue)
{
    Blob Key = Blob.valueOf(key1);
 Blob data = EncodingUtil.base64Decode(DataValue);
 Blob decryptedData = Crypto.decryptWithManagedIV('AES128', key, data);
 return decryptedData.toString();
}

}