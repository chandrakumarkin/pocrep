@isTest(seeAllData=false)
private class Test_cls_ClientRegistrationHandbook {

    static testMethod void myUnitTest() {
        
        Page_Flow__c objPF = new Page_Flow__c();
		
		objPF.Name = 'Test Flow';
		objPF.Master_Object__c = 'Service_Request__c';
		objPF.Flow_Description__c = 'Test';
		objPF.Record_Type_API_Name__c = 'Application_Of_Registration';
		
		insert objPF;
    
		Page__c objPg = new Page__c();
		
		objPg.Name = 'Page1';
		objPg.Page_Description__c = 'Page1';
		objPg.Is_Custom_Component__c = false;
		objPg.Page_Order__c = 1;
		objPg.What_Id__c = 'Service_Request__c';
		objPg.Render_By_Default__c = true;
		objPg.No_Quick_navigation__c = false;
		objPg.VF_Page_API_Name__c = 'Process_Flow';
		objPg.Page_Flow__c = objPF.Id;

		insert objPg;
		
		Page__c objPg2 = new Page__c();
        
        objPg2.Name = 'Page2';
        objPg2.Page_Description__c = 'Page2';
        objPg2.Is_Custom_Component__c = true;
        objPg2.Page_Order__c = 2;
        objPg2.What_Id__c = 'Service_Request__c';
        objPg2.Render_By_Default__c = true;
        objPg2.No_Quick_navigation__c = false;
        objPg2.VF_Page_API_Name__c = 'AmendmentDetails?Type=Authorized Representative';
        
        objPg2.Page_Flow__c = objPF.Id;
        insert objPg2;
		
		Section__c ButtonSec = new Section__c();
        
        ButtonSec.Page__c = objPg.Id;
        ButtonSec.Name = 'Page1 Section2';
        ButtonSec.Section_Description__c = 'Test';
        ButtonSec.Default_Rendering__c = true;
        ButtonSec.Layout__c = '1';
        ButtonSec.Order__c = 2;
        ButtonSec.Section_Title__c = 'Test Type';
        ButtonSec.Section_Type__c = 'CommandButtonSection';
		
		insert ButtonSec;
		
		Section_Detail__c NextBtn = new Section_Detail__c();
        
        NextBtn.Section__c = ButtonSec.Id;
        NextBtn.Order__c = 2;
        NextBtn.Render_By_Default__c = true;
        NextBtn.Component_Type__c = 'Command Button';
        NextBtn.Navigation_Directions__c = 'Forward';
        NextBtn.Button_Location__c = 'Top';
        NextBtn.Button_Position__c = 'Left';
        NextBtn.Component_Label__c = 'Client';
        
        insert NextBtn;
		
		Account objAccount = new Account();
	    
	    objAccount.Name = 'Test Account';
	    objAccount.Phone = '1234567890';
	  
	    insert objAccount;
      
        Service_Request__c objSR = new Service_Request__c();
        
        objSR.Customer__c = objAccount.Id;
        
        insert objSR;
        
        Test.startTest();
        
        Test.setCurrentPage(Page.ClientRegistrationHandbook);
        
        ApexPages.CurrentPage().GetParameters().put('Id',objSR.Id);
        ApexPages.CurrentPage().GetParameters().put('FlowId',objPF.Id);
        ApexPages.CurrentPage().GetParameters().put('PageId',objPg.Id);      
        
        cls_ClientRegistrationHandbook testClientRegPage = new cls_ClientRegistrationHandbook();
        
        System.debug(testClientRegPage.strPageId );
	    System.debug(testClientRegPage.documentId );
	    System.debug(testClientRegPage.strNavigatePageId);
	    System.debug(testClientRegPage.pageTitle);
	    System.debug(testClientRegPage.pageDescription);
	    System.debug(testClientRegPage.strHiddenPageIds);
	    System.debug(testClientRegPage.strActionId);
	    
	    System.debug(testClientRegPage.mapParameters);
	    
	    testClientRegPage.strNavigatePageId = objPg2.Id;
	    testClientRegPage.strActionId = NextBtn.Id;
	    
	    testClientRegPage.getSalesforceUrl();
	    
	    testClientRegPage.getDyncPgMainPB();
	    
	    testClientRegPage.goTopage();
	    
	    testClientRegPage.DynamicButtonAction();
        
        Test.stopTest();
    }
}