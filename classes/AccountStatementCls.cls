/******************************************************************************************
 *  Author   			 : Mudasir Wani
 *  Company  			 : DIFC
 *  Date     			 : 25th-Nov-2018
 * 	Access Level		 : Without sharing
 *  Access reason 		 : Without sharing is used as any contractor user can login in case of multiple active users for the contractor
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        	Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    25th-Nov-2018  	Mudasir         Created
V1.1    26th-Feb-2019  	Mudasir         #6407- Created date replaced with submitted date , Show the actual service request not the Fit-Out project request and Calculate only Invoiced price items

********************************************************/
public without sharing class AccountStatementCls {
    public Decimal totalCredit{get;set;}
    public Decimal totalDebit{get;set;}
    public Decimal remainingBalance{get;set;}
    public Decimal refundedAmount{get;set;}
    public Set<String> serviceTypeSet{get;set;}
    public string accountType {get;set;}
    public Date fromDate {get;set;}
    public Date toDate {get;set;}
    public String fromDateString {get;set;}
    public String toDateString {get;set;}
    public String activeLicenseNumber{get;set;}
    public String bpNumber{get;set;}
    Public Account accountRecord{get;set;}
    public List<SR_Price_Item__c> totalPayments{get;set;}
    public List<SR_Price_Item__c> totalFines{get;set;}
    public Map<id,List<debitTransactions>> debitTransactionMap{get;set;}
    public List<debitTransactions> debitTransactionList{get;set;}
    public Step__c stepRecord {get;set;}
        
   class debitTransactions{
       public String typeOfRequest{get;set;}
       public Decimal paymentAmount{get;set;}
       public String projectName{get;set;}
       public DateTime createdDate{get;set;}
       public String description{get;set;}
       public String Project_ReferenceCurrent_No{get;set;}
       public String CustomerName{get;set;}
       debitTransactions(String typeOfRequest,Decimal paymentAmount,String projectName,DateTime createdDate , String description, String Project_ReferenceCurrent_No,String CustomerName){
           this.typeOfRequest = typeOfRequest;
           this.paymentAmount = paymentAmount;
           this.createdDate = createdDate;
           this.projectName = projectName;
           this.description = description;
           this.Project_ReferenceCurrent_No = Project_ReferenceCurrent_No;
           this.CustomerName = CustomerName;
       }
   }
    public AccountStatementCls(){
        stepRecord = new step__c();
    	totalDebit = 0.0;
    	totalCredit = 0.0;
    	refundedAmount = 0.0;
        fromDateString ='';
        toDateString ='';
        remainingBalance = totalCredit - totalDebit;
    	serviceTypeSet = new Set<String>(); 
        accountType = 'Contractor_Account';//Populate this dynamically 
        debitTransactionMap =  new Map<id,List<debitTransactions>>();
        debitTransactionList = new List<debitTransactions>();
    }
    public void generateStatemet(){
    	//Move the below statuses to custom setting or label
        //set<string> setPriceStatuses = new set<string>{'Blocked' , 'Consumed' , 'Invoiced'};
        set<string> setPriceStatuses = new set<string>{'Invoiced'};
    	try{
    		accountRecord = [Select id,name,BP_No__c,Tax_Registration_Number_TRN__c,(Select id,name,Amount__c,Receipt_Type__c,Transaction_Date__c,Customer__r.Name,Customer__r.Tax_Registration_Number_TRN__c from Receipts__r 
    									where Payment_Status__c='Success' and createdDate >=: fromDate and createdDate <=: todate order by createdDate desc),(Select id,Fine_Amount_Formula__c,createdDate,Related_Request__r.Linked_SR__c,Related_Request__c,Violations__r.Name,
    									Related_request__r.service_type__c from Violations__r),(Select id from Service_Requests1__r),
                         				(Select id,Name,Service_type__c,Refund_Amount__c,CreatedDate,Submitted_Date__c,Customer__r.Name from Service_Requests__r where RecordType.Developername ='Request_for_Contractor_Wallet_Refund' and External_Status_Name__c='Approved')
    									from Account where RORP_License_No__c =:activeLicenseNumber and RecordType.DeveloperName =:accountType limit 1];
	    	//Now fetch the Price items related to the service requests 
	    	Set<id> serviceRequestIdSet = new Set<id>();
	        for(Service_Request__c servReqRec : accountRecord.Service_Requests1__r){
	    		serviceRequestIdSet.add(servReqRec.id);	    		
	    	}
	        List<Service_Request__c> serviceRequestsWithPriceItems = [Select Current_Registered_Name__c,Customer__r.Name,id,Linked_SR__c, Linked_SR__r.Service_Type__c,Refund_Amount__c,(Select id,name,Total_Service_Amount__c,
	        														Pricing_Line__c,SRPriceLine_Text__c,ServiceRequest__c,ServiceRequest__r.service_type__c,ServiceRequest__r.Name,createdDate, Servicerequest__r.Submitted_Date__c, 
	        														ServiceRequest__r.Linked_SR__c,ServiceRequest__r.Linked_SR__r.Name,ServiceRequest__r.Linked_SR__r.Current_Registered_Name__c,ServiceRequest__r.Linked_SR__r.Customer__r.Name,ServiceRequest__r.Current_Registered_Name__c  from SR_Price_Items1__r 
	        														where Status__c IN: setPriceStatuses) from Service_Request__c where id IN :serviceRequestIdSet and createdDate >=: fromDate and createdDate <=: todate order by createdDate];
	    	totalPayments = getAllPriceITems(serviceRequestsWithPriceItems);
	        totalCredit = calculateTotalCredit(accountRecord.Receipts__r);
	    	totalDebit = calculateTotalDebit(accountRecord.Violations__r , totalPayments);
	        for(Service_Request__c servReqRecRefund : accountRecord.Service_Requests__r){
	            if(debitTransactionMap != Null && debitTransactionMap.keyset() != Null && debitTransactionMap.keyset().contains(servReqRecRefund.id) && servReqRecRefund.Refund_Amount__c > 0){
	        		debitTransactionMap.get(servReqRecRefund.id).add(new debitTransactions(servReqRecRefund.service_Type__c,(Decimal)servReqRecRefund.Refund_Amount__c,servReqRecRefund.Service_Type__c,servReqRecRefund.Submitted_Date__c,servReqRecRefund.Service_Type__c,'',servReqRecRefund.Customer__r.name));
	            }else{
	                debitTransactionMap.put(servReqRecRefund.id,new List<debitTransactions>{new debitTransactions(servReqRecRefund.service_Type__c,(Decimal)servReqRecRefund.Refund_Amount__c,servReqRecRefund.Name,servReqRecRefund.Submitted_Date__c,servReqRecRefund.Service_Type__c,'',servReqRecRefund.Customer__r.name)});
	                totalDebit = totalDebit + servReqRecRefund.Refund_Amount__c;
	                //populate refund amount 
		        	refundedAmount = refundedAmount + servReqRecRefund.Refund_Amount__c;
	            }
	        }
	        remainingBalance = totalCredit - totalDebit;
	        if(debitTransactionMap.size() > 0){
	        	for(String keys : debitTransactionMap.keyset()){
	        		debitTransactionList.addAll(debitTransactionMap.get(keys));
	        	}
	        }
    	}Catch(Exception ex){
    		
    	}
    }
    public List<SR_Price_Item__c> getAllPriceITems(List<Service_request__c> serviceRequests){
    	List<SR_Price_Item__c> priceItems = new List<SR_Price_Item__c>();
    	for(Service_request__c servReqList : serviceRequests){
            for(SR_Price_Item__c priceItemRec : servReqList.SR_Price_Items1__r){
	    		priceItems.add(priceItemRec);
	    	}
    	}
        return priceItems;
    }
    public Decimal calculateTotalCredit(List<Receipt__c> receipts){
    	for(Receipt__c receiptRec : receipts){
    		totalCredit = totalCredit + receiptRec.Amount__c;
    	}
        return totalCredit;
    }
    public Decimal calculateTotalDebit(List<Violation__c> violations ,List<SR_Price_Item__c> priceItems){
        for(SR_Price_Item__c priceItemRec : priceItems){
    		totalDebit = totalDebit + priceItemRec.Total_Service_Amount__c;
            //debitTransactions = new debitTransactions(priceItemRec.ServiceRequest__r.service_Type__c,null,priceItemRec.ServiceRequest__c,priceItemRec.createdDate);
            Id serviceRequestId = priceItemRec.ServiceRequest__r.service_type__c =='Fit - Out Service Request' ? priceItemRec.ServiceRequest__c :priceItemRec.ServiceRequest__r.Linked_SR__c;
            //String serviceRequestName= priceItemRec.ServiceRequest__r.service_type__c =='Fit - Out Service Request' ? priceItemRec.ServiceRequest__r.Name :priceItemRec.ServiceRequest__r.Linked_SR__r.Name;
            String serviceRequestName= priceItemRec.ServiceRequest__r.Name;
            String Project_Reference_No= priceItemRec.ServiceRequest__r.service_type__c =='Fit - Out Service Request' ? priceItemRec.ServiceRequest__r.Current_Registered_Name__c :priceItemRec.ServiceRequest__r.Linked_SR__r.Current_Registered_Name__c;
        	if(debitTransactionMap != Null && debitTransactionMap.keyset() != Null && debitTransactionMap.keyset().contains(serviceRequestId) && priceItemRec.Total_Service_Amount__c > 0){
        		debitTransactionMap.get(serviceRequestId).add(new debitTransactions(priceItemRec.ServiceRequest__r.service_Type__c,(Decimal)priceItemRec.Total_Service_Amount__c,serviceRequestName,priceItemRec.ServiceRequest__r.Submitted_Date__c,priceItemRec.SRPriceLine_Text__c,Project_Reference_No, priceItemRec.ServiceRequest__r.Linked_SR__r.Customer__r.Name));
        	}else
        	if(priceItemRec.Total_Service_Amount__c >0)debitTransactionMap.put(serviceRequestId,new List<debitTransactions> { new debitTransactions(priceItemRec.ServiceRequest__r.service_Type__c,(Decimal)priceItemRec.Total_Service_Amount__c,serviceRequestName,priceItemRec.ServiceRequest__r.Submitted_Date__c,priceItemRec.SRPriceLine_Text__c,Project_Reference_No, priceItemRec.ServiceRequest__r.Linked_SR__r.Customer__r.Name)});
    	}
        return totalDebit;
    }
    public PageReference search(){
    	debitTransactionMap =  new Map<id,List<debitTransactions>>();
    	debitTransactionList = new List<debitTransactions>();
    	totalPayments = new List<SR_Price_Item__c>();
        totalCredit = 0.0;
        totalDebit = 0.0;
        remainingBalance = 0.0;
        generateStatemet();
        return null;
    }
    public PageReference generatePDF(){
    	PageReference pageRef = Page.AccountStatementPDF;
		return pageRef;
    }
    public PageReference generateStatement(){
       if(stepRecord.Date_of_DCD_Inspection__c != null) fromDate = stepRecord.Date_of_DCD_Inspection__c;
        if(stepRecord.Date_of_DEWA_Inspection__c != null) toDate = stepRecord.Date_of_DEWA_Inspection__c;
        if(fromDate > toDate){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'From Date should not be greater than To Date'));
            return null; 
        }
        if(toDate != Null && toDate >= System.today()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'To Date should be less than today'));
            return null;
        }
        if(toDate == Null){
        	toDate = system.today()-1;
        }
    	//Get the account associated with the loggedin user 
    	User userDetail = [SELECT Contact.Account.RORP_License_No__c,Id,Name FROM User where Id =:UserInfo.getUserid()];
    	activeLicenseNumber = userDetail.Contact.Account.RORP_License_No__c != Null ? String.valueOf(userDetail.Contact.Account.RORP_License_No__c) : null; 
    	generateStatemet();
    	return generatePDF();
    }
}