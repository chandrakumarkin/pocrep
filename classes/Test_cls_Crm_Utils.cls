/******************************************************************************************
 *  Name        : Test_cls_Crm_Utils 
 *  Author      : Claude Manahan
 *  Company     : DIFC
 *  Date        : 2017-01-06
 *  Description : Utility Class for running CRM test classes
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   01-06-2016   Claude        Created
*******************************************************************************************/
@isTest(seeAllData=false)
public class Test_cls_Crm_Utils {
    
    /**
     * Default Record Type for Accounts
     */
    public static final String CRM_DEFAULT_RECORDTYPE = 'Financial';
    
    /**
     * Default Company Type for Accounts
     */
    public static final String CRM_DEFAULT_COMPANYTYPE = 'Financial - related';
    
    /**
     * Default customer name for contacts
     */
    public static final String CRM_DEFAULT_CUSTOMERNAME = 'Test CRM Customer';
    
    /**
     * Default name for opportunities
     */
    public static final String CRM_DEFAULT_OPPORTUNITYNAME = 'Test CRM Opportunity';
    
    /**
     * Default sector lead classification for Accounts
     */
    public static final String CRM_DEFAULT_SECTORCLASSIFICATION = 'Business Entities (BE)';  
    
    /**
     * Default QUEUE name for Account Owners
     */
    public static final String CRM_DEFAULT_QUEUE_NAME = 'BD Financial';
    
    /**
     * Default Opportunity stage
     */
    public static final String CRM_DEFAULT_STAGE = 'Created';    
    
    /**
     * Default CRON expression for scheduled jobs
     */
    public static final String CRON_EXP = '0 0 0 3 9 ? ' + System.Today().addYears(10).year();
    
    /**
     * Test contact ID for testing CRM methods
     */
    private static String testContactId = '';
    
    /**
     * Test contact ID for testing CRM methods
     */
    private static String testEmailMessageId = '';
    
    /**
     * Loads the required data before the tests begin
     */
    public static void prepareSetup(){
    	
    	/* NOTE: THESE DETAILS DO NOT EXIST, AND ARE ONLY USED FOR TESTING */
        WebService_Details__c testCredentials = new WebService_Details__c();
        
        testCredentials.Name = 'Credentials';
        testCredentials.Password__c = 'thisistest12345678';
        testCredentials.SAP_User_Id__c = 'TEST_SAP';
        testCredentials.Username__c = 'TEST_SAP';
        
        insert testCredentials;
        
        Endpoint_URLs__c testCRmUrl = new Endpoint_URLs__c();
        
        Endpoint_URLs__c testCRMUrl2 = new Endpoint_URLs__c();
        
        testCRmUrl.Name = 'CRM';
        testCRmUrl.url__c = 'TEST Url';
        
        testCRMUrl2.Name = 'AccountBal';
        testCRMUrl2.url__c = 'TEST Url';
        
        insert new List<Endpoint_URLs__c>{testCRmUrl,testCRMUrl2};
    }
    
    /**
     * Returns a test Messaging.InboundEmail instance
     * @param		plainText		The plain text content of the email
     * @return		email			The Messaging.InboundEmail instance
     */
    public static Messaging.InboundEmail getTestInboundEmail(String plainText){
    	
    	Messaging.InboundEmail email  = new Messaging.InboundEmail();
    	
    	email.plainTextBody = plainText;
    	email.fromAddress ='test@test.com';
        email.ccAddresses = new String[] {'Jon Smith <jsmith@testemail.com>'};
        email.subject = 'Dummy Account Name 123';
        
        return email;
    }
    
    /**
     * Returns a new System Administrator
     * @return      testUser        The test System Administrator user record
     */
    public static User getTestUser(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
        
        return new User(Alias = 'standt', Email='test.difcadminuser@testorg.com', 
          Username = 'test.difcadminuser@testorg.com',CommunityNickname = 'tadm16',
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', ProfileId = p.Id, 
          TimeZoneSidKey='America/Los_Angeles');
    }
    
    /**
     * Returns a sample Contact
     * @return      testContact     The sample contact
     */
    public static Contact getTestContact(){
        
        return new Contact(
            FirstName='Test First Name' , LastName = 'Contact Last Name'
        );
    }
    
    /**
     * Returns a sample Test Account
     * @params      accountName     The name of the account
     * @params      companyType     The type of Account
     * @return      objAccount      The sample account
     */
    public static Account getTestCrmAccount(String accountName, String companyType){
        
        Account objAccount = new Account();
        
        objAccount.ROC_Status__c  = 'Account Created';
        objAccount.Name = accountName;
        objAccount.E_mail__c = 'test.tester@difc.test';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = companyType;
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Contractor_License_Expiry_Date__c = System.Today() + 300;
        
        return objAccount;
    }
    
    /**
     * Returns a sample Test Account
     * @params      opportunityName     The name of the opportunity
     * @params      accountId           The account record ID
     * @params      stageName           The stage the opportunity will be initially set
     * @return      testOpportunity     The sample opportunity record
     */
    public static Opportunity getTestOpportunity(String opportunityName, String accountId, String stageName){
        
        return new Opportunity(
            Name = opportunityName,
            AccountId = accountId,
            StageName = stageName,
            CloseDate = System.Today() + 50
        );
    }
    
    /**
     * Returns a sample Lead Record
     * @return      testLead            The sample lead record
     */
    public static Lead getTestLead(){
    	return getTestLead(CRM_DEFAULT_RECORDTYPE,CRM_DEFAULT_COMPANYTYPE);
    }
    
    /**
     * Returns a sample Lead Record
     * @params      recordTypeName      The lead's record type
     * @params      companyType         The type of Account the converted account record will be using
     * @return      testLead            The sample lead record
     */
    public static Lead getTestLead(String recordTypeName, String companyType){
        
        RecordType crmLeadRecordType = 
            [SELECT Id FROM RecordType 
                    WHERE sObjectType = 'Lead' AND 
                          DeveloperName = :recordTypeName];
                          
        return new Lead(
            LastName = 'Test CRM Lead',
            Company = 'Test CRM Company',
            Email = 'test.crm@difc.org.dev',
            MobilePhone = '+971 12345678',
            Status = 'Open',
            Priority_to_DIFC__c = 'T1: Global with FTE',
            BD_Sector_Classification__c = 'Business Entities (BE)',
            Company_Type__c = companyType,
            LeadSource = 'Company Website',
            Country__c = 'United Arab Emirates',
            City__c = 'Dubai',
            Geographic_Region__c = 'United Arab Emirates'
        );
    }
    
    /**
     * Returns the current contact ID
     * used in the test
     * @params 		testEmailMessageIdTemp		The email message ID to be used in the test
     */
    public static void setTestEmailMessageId(String testEmailMessageIdTemp){
    	
    	testEmailMessageId = testEmailMessageIdTemp;
    	
    }
    
    /**
     * Returns the current contact ID
     * used in the test
     * @return 		testEmailMessageId 			The test email message ID
     */
    public static String getTestEmailMessageId(){
    	
    	return testEmailMessageId;
    	
    }
    
    /**
     * Returns the current contact ID
     * used in the test
     * @params 		testContactIdTemp		The contact ID to be used in the test
     */
    public static void setTestContactId(String testContactIdTemp){
    	
    	testContactId = testContactIdTemp;
    	
    }
    
    /**
     * Returns the current contact ID
     * used in the test
     * @return 		testContactId 			The test contact ID
     */
    public static String getTestContactId(){
    	
    	return testContactId;
    	
    }
    
    /**
     * A Webservice mock class for 
     * testing SAPWebServiceDetailsClass
     */
    public class CRMWebserviceMock implements WebServiceMock {
       public void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           
           SAPCRMWebService.Z_SF_INBOUND_DATAResponse_element response_x = new SAPCRMWebService.Z_SF_INBOUND_DATAResponse_element();
           response.put('response_x', response_x ); 
       }
    }
    
}