public with sharing class Beneficial_or_Ultimate_Ownercontroller 
{
 
 public string Sr_Id{get;set;}
 public String UboViewMode{get;set;}
 public Id UboId {get;set;}
 
     public List<UBO_Data__c> getListofAllRemoved()
{
    
      string strContactQuery = Apex_Utilcls.getAllFieldsSOQL('UBO_Data__c');
        strContactQuery = strContactQuery+' where Service_Request__c=\''+Sr_Id+'\'  and  Status__c=\'Removed\'';
        System.debug('strContactQuery===>'+strContactQuery);
      return database.query(strContactQuery);
      
   // return  [select Place_of_Registration__c,Reason_for_exemption__c,RecordType.Name,RecordType.ID,Passport_Number__c,Passport_Expiry_Date__c,Apartment_or_Villa_Number__c,BC_UBO__c,Building_Name__c,City_Town__c,Country__c,Date_Of_Becoming_a_UBO__c,
    //    Date_of_Birth__c,Date_Of_Registration__c,Email__c,Emirate_State_Province__c,Entity_Name__c,Last_Name__c,First_Name__c,Entity_Type__c from UBO_Data__c where Service_Request__c=:Sr_Id and Recordtype.name='Individual'];
    
}

    
    public List<UBO_Data__c> getListofAll()
{
    
      string strContactQuery = Apex_Utilcls.getAllFieldsSOQL('UBO_Data__c');
        strContactQuery = strContactQuery+' where Service_Request__c=\''+Sr_Id+'\'  and  Recordtype.name=\'Individual\' and Status__c!=\'Removed\'';
        System.debug('strContactQuery===>'+strContactQuery);
      return database.query(strContactQuery);
      
   // return  [select Place_of_Registration__c,Reason_for_exemption__c,RecordType.Name,RecordType.ID,Passport_Number__c,Passport_Expiry_Date__c,Apartment_or_Villa_Number__c,BC_UBO__c,Building_Name__c,City_Town__c,Country__c,Date_Of_Becoming_a_UBO__c,
    //    Date_of_Birth__c,Date_Of_Registration__c,Email__c,Emirate_State_Province__c,Entity_Name__c,Last_Name__c,First_Name__c,Entity_Type__c from UBO_Data__c where Service_Request__c=:Sr_Id and Recordtype.name='Individual'];
    
}

   public List<UBO_Data__c> getListofBCAll()
{
      string strContactQuery = Apex_Utilcls.getAllFieldsSOQL('UBO_Data__c');
        strContactQuery = strContactQuery+' where Service_Request__c=\''+Sr_Id+'\'  and  Recordtype.name!=\'Individual\' and Status__c!=\'Removed\'';
        System.debug('strContactQuery===>'+strContactQuery);
      return database.query(strContactQuery);
      
   // return  [select Place_of_Registration__c,Reason_for_exemption__c,RecordType.Name,RecordType.ID,Passport_Number__c,Passport_Expiry_Date__c,Apartment_or_Villa_Number__c,BC_UBO__c,Building_Name__c,City_Town__c,Country__c,Date_Of_Becoming_a_UBO__c,
     //    Date_of_Birth__c,Date_Of_Registration__c,Email__c,Emirate_State_Province__c,Entity_Name__c,Last_Name__c,First_Name__c,Entity_Type__c from UBO_Data__c where Service_Request__c=:Sr_Id and Recordtype.name!='Individual'];
    
}


  
    


}