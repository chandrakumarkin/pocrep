/*
Author      : Maruf Bagwan
Date        : 01-April-2020
Description : Custom code to create AOR for Approved Multi Structre Application
-------------------------------------------------------------------------------------------------
*/
global without sharing class CC_ProcessMultiStructureSR implements HexaBPM.iCustomCodeExecutable {
  global string EvaluateCustomCode(
    HexaBPM__Service_Request__c SR,
    HexaBPM__Step__c stp
  ) {
    try {
      insert LogDetails.CreateLog(
        null,
        'CC_ProcessMultiStructureInit : processMultiStructSR',
        'Line Number : ' +
        stp.HexaBPM__SR__c +
        '\nException is : '
      );

      list<Account> accountList = new List<Account>();
      set<string> setAccountIds = new Set<string>();
      Id inPrincipalRecType = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName()
        .get('In_Principle')
        .getRecordTypeId();
      list<HexaBPM__Service_Request__c> inPrincipalSRList = new List<HexaBPM__Service_Request__c>(
        [
          SELECT Id
          FROM HexaBPM__Service_Request__c
          WHERE
            RecordTypeId = :inPrincipalRecType
            AND HexaBPM__Customer__c = :stp.HexaBPM__SR__r.HexaBPM__Customer__c
            AND Id != :stp.HexaBPM__SR__c
            AND HexaBPM__Is_Rejected__c = FALSE
            AND HexaBPM__IsCancelled__c = FALSE
        ]
      );
      system.debug('inPrincipalSRList==>' + inPrincipalSRList);

      map<string, string> MapAccountContact = new Map<string, string>();

      if (
        inPrincipalSRList == null ||
        (inPrincipalSRList != null &&
        inPrincipalSRList.size() == 0)
      ) {
        Id AccRecId = OB_QueryUtilityClass.getRecordtypeID(
          'Account',
          'Business_Development'
        );
        for (HexaBPM__Service_Request__c objSR : [
          SELECT
            Id,
            Entity_Name__c,
            HexaBPM__Contact__c,
            HexaBPM__Customer__c,
            Entity_Type__c,
            Business_Sector__c,
            Legal_Structures__c,
            HexaBPM__Parent_SR__c,
            HexaBPM__Parent_SR__r.Business_Sector__c,
            HexaBPM__Parent_SR__r.HexaBPM__Customer__r.ownerId,
            HexaBPM__Parent_SR__r.first_name__c,
            HexaBPM__Parent_SR__r.last_name__c,
            HexaBPM__Parent_SR__r.Entity_Name__c,
            HexaBPM__Parent_SR__r.HexaBPM__Send_SMS_to_Mobile__c,
            HexaBPM__Parent_SR__r.HexaBPM__Email__c,
            HexaBPM__Parent_SR__r.Entity_Type__c,
            (
              SELECT Id, Application__c
              FROM Company_Names__r
              WHERE Status__c = 'Approved'
              LIMIT 1
            )
          FROM HexaBPM__Service_Request__c
          WHERE
            HexaBPM__Parent_SR__c = :stp.HexaBPM__SR__c
            AND HexaBPM__IsClosedStatus__c = TRUE
        ]) {
          system.debug('in loop' + objSR.HexaBPM__Customer__c);
          if (
            objSR.Company_Names__r != null &&
            objSR.Company_Names__r.size() > 0 &&
            objSR.HexaBPM__Customer__c != null
          ) {
            system.debug('in if loop' + objSR.HexaBPM__Customer__c);
            MapAccountContact.put(
              objSR.HexaBPM__Customer__c,
              objSR.HexaBPM__Contact__c
            );
            setAccountIds.add(objSR.HexaBPM__Customer__c);
            system.debug(
              'objSR.HexaBPM__Customer__r.ownerId' +
              objSR.HexaBPM__Parent_SR__r.HexaBPM__Customer__r.ownerId
            );
            Account acc = new Account(
              Id = objSR.HexaBPM__Customer__c,
              Description = objSR.Id,
              OB_Is_Multi_Structure_Application__c = 'Yes',
              ownerId = objSR.HexaBPM__Parent_SR__r.HexaBPM__Customer__r.ownerId
            );
            accountList.add(acc);
          }
        }
        if (accountList.size() > 0) {
          /* string relNewUserSr = getRelSr();
                    if(relNewUserSr != '' || relNewUserSr != null){
                        for(account accObj : accountList){
                            accObj.OB_related_new_user_sr_Id__c = relNewUserSr;
                        }
                    } */
          update accountList;

          list<AccountContactRelation> lstACR = new List<AccountContactRelation>();
          for (Account acc : accountList) {
            AccountContactRelation objAccContactRel = new AccountContactRelation();
            objAccContactRel.AccountId = acc.Id;
            if (MapAccountContact.get(acc.Id) != null)
              objAccContactRel.ContactId = MapAccountContact.get(acc.Id);
            else if (stp.HexaBPM__SR__r.HexaBPM__Contact__c != null)
              objAccContactRel.ContactId = stp.HexaBPM__SR__r.HexaBPM__Contact__c;
            objAccContactRel.Multi_Structure_Account__c = true;
            objAccContactRel.IsActive = true;
            objAccContactRel.Access_Level__c = 'Read/Write';
            objAccContactRel.Roles = 'Super User;Company Services;Property Services';
            lstACR.add(objAccContactRel);
          }
          if (lstACR.size() > 0) {
            insert lstACR;
          }
        }
        system.debug('sizeee' + setAccountIds);
        if (setAccountIds.size() > 0)
          processMultiStructSR(stp.HexaBPM__SR__c, setAccountIds);
      }
      return 'Success';
    } catch (Exception e) {
      insert LogDetails.CreateLog(
        null,
        'CC_ProcessMultiStructure : processMultiStructSR',
        'Line Number : ' +
        e.getLineNumber() +
        '\nException is : ' +
        e.getMessage()
      );
      return e.getMessage();
    }
  }

  public static void copyNewUserSrToChildMultistructure(
    string parentAccountId,
    set<string> setAccountIds
  ) {
    system.debug('setAccountIdsNew' + setAccountIds);
    system.debug('parentAccountIdNew' + parentAccountId);

    set<string> flowType = new Set<string>{
      'Converted_User_Registration',
      'New_User_Registration'
    };
    string relSrId = '';
    string RecordTypeName = '';
    for (HexaBPM__Service_Request__c srObj : [
      SELECT id, HexaBPM__Record_Type_Name__c
      FROM HexaBPM__Service_Request__c
      WHERE
        HexaBPM__Record_Type_Name__c IN :flowType
        AND HexaBPM__Customer__c = :parentAccountId
      LIMIT 1
    ]) {
      relSrId = srObj.id;
      RecordTypeName = srObj.HexaBPM__Record_Type_Name__c;
    }

    system.debug(RecordTypeName + 'RecordTypeName');

    system.debug('relSrId' + relSrId);

    try {
      map<string, list<HexaBPM__SR_Doc__c>> MapSRDocContentIds = new Map<string, list<HexaBPM__SR_Doc__c>>(); //v1.3 changed type from map<string,HexaBPM__SR_Doc__c> to map<string,list<HexaBPM__SR_Doc__c>>
      map<string, HexaBPM__SR_Doc__c> MapInprincipleDocuments = new Map<string, HexaBPM__SR_Doc__c>();

      for (HexaBPM__SR_Doc__c doc : [
        SELECT
          Id,
          Name,
          HexaBPM__Document_Master__c,
          HexaBPM__SR_Template_Doc__c,
          File_Name__c,
          HexaBPM_Amendment__c,
          HexaBPM__Customer__c,
          HexaBPM__Document_Description_External__c,
          HexaBPM__Status__c,
          HexaBPM__Doc_ID__c,
          HexaBPM__From_Finalize__c
        FROM HexaBPM__SR_Doc__c
        WHERE
          HexaBPM__Service_Request__c = :relSrId
          AND HexaBPM__Sys_IsGenerated_Doc__c = FALSE
          AND HexaBPM__Doc_ID__c != NULL
      ]) {
        if (doc.HexaBPM_Amendment__c != null) {
          list<HexaBPM__SR_Doc__c> lstSRDocs = new List<HexaBPM__SR_Doc__c>(); //v1.3
          if (MapSRDocContentIds.get(doc.HexaBPM_Amendment__c) != null)
            lstSRDocs = MapSRDocContentIds.get(doc.HexaBPM_Amendment__c);
          lstSRDocs.add(doc);
          MapSRDocContentIds.put(doc.HexaBPM_Amendment__c, lstSRDocs); //v1.3
        } else {
          MapInprincipleDocuments.put(doc.HexaBPM__Document_Master__c, doc);
        }
      }

      map<String, Schema.SObjectType> m = Schema.getGlobalDescribe();
      SObjectType objtype;
      DescribeSObjectResult objDef1;
      map<string, sObjectField> fieldmap;
      if (m.get('HexaBPM__Service_Request__c') != null) {
        objtype = m.get('HexaBPM__Service_Request__c');
        objDef1 = objtype.getDescribe();
        fieldmap = objDef1.fields.getmap();
      }
      set<string> setFields = new Set<string>();
      if (fieldmap != null) {
        for (Schema.SObjectField strFld : fieldmap.values()) {
          Schema.DescribeFieldResult fd = strFld.getDescribe();
          if (fd.isCustom())
            setFields.add(string.valueOf(strFld).toLowerCase());
        }
      }
      string SRqry = 'Select Name';
      if (setFields.size() > 0) {
        for (string srField : setFields) {
          SRqry += ',' + srField;
        }
      }
      SRqry += ' From HexaBPM__Service_Request__c where Id=: relSrId';

      list<HexaBPM__Service_Request__c> objDraftSrList = new List<HexaBPM__Service_Request__c>();

      string SRRecordTypeId;
      for (RecordType rt : [
        SELECT Id
        FROM RecordType
        WHERE
          sobjectType = 'HexaBPM__Service_Request__c'
          AND DeveloperName = :RecordTypeName
          AND IsActive = TRUE
      ]) {
        SRRecordTypeId = rt.Id;
      }

      for (HexaBPM__Service_Request__c objSR : database.query(SRqry)) {
        for (string accId : setAccountIds) {
          HexaBPM__Service_Request__c objDraftSR = new HexaBPM__Service_Request__c();
          objDraftSR = objSR.Clone();
          objDraftSR.HexaBPM__Parent_SR__c = relSrId;
          if (SRRecordTypeId != null)
            objDraftSR.recordTypeId = SRRecordTypeId;

          objDraftSR.HexaBPM__Customer__c = accId;

          objDraftSR.I_Agree__c = false;
          objDraftSR.HexaBPM__Submitted_Date__c = system.today();
          objDraftSR.HexaBPM__Submitted_DateTime__c = system.now();
          objDraftSR.HexaBPM__Required_Docs_not_Uploaded__c = false;
          objDraftSR.HexaBPM__FinalizeAmendmentFlg__c = false;
          objDraftSR.Id = null;
          //v1.4
          objDraftSR.HexaBPM__Closed_Date_Time__c = system.today();
          objDraftSrList.add(objDraftSR);
        }
      }

      if (objDraftSrList.size() > 0) {
        insert objDraftSrList;

        //approved status
        /* string approvedId = '';
        for(HexaBPM__SR_Status__c statuObj : [select id from HexaBPM__SR_Status__c where HexaBPM__Code__c ='APPROVED' limit 1]){
            approvedId = statuObj.id;
        }
        system.debug('vds' + approvedId); */

        list<string> newSr = new List<string>();
        for (HexaBPM__Service_Request__c srObj : objDraftSrList) {
          newSr.add(srObj.id);
          // srObj.HexaBPM__Internal_SR_Status__c = approvedId;
          //srObj.HexaBPM__External_SR_Status__c = approvedId;
        }

        update objDraftSrList;

        system.debug('vds1' + objDraftSrList[0].HexaBPM__Internal_SR_Status__c);

        list<HexaBPM__SR_Doc__c> srDocToDel = new List<HexaBPM__SR_Doc__c>();
        for (HexaBPM__SR_Doc__c doc : [
          SELECT Id
          FROM HexaBPM__SR_Doc__c
          WHERE HexaBPM__Service_Request__c IN :newSr
        ]) {
          srDocToDel.add(doc);
        }

        if (srDocToDel.size() > 0) {
          delete srDocToDel;
        }
      }

      //v1.2
      if (m.get('HexaBPM_Amendment__c') != null) {
        objtype = m.get('HexaBPM_Amendment__c');
        objDef1 = objtype.getDescribe();
        fieldmap = objDef1.fields.getmap();
      }
      setFields = new Set<string>();
      if (fieldmap != null) {
        for (Schema.SObjectField strFld : fieldmap.values()) {
          Schema.DescribeFieldResult fd = strFld.getDescribe();
          if (fd.isCustom())
            setFields.add(string.valueOf(strFld).toLowerCase());
        }
        setFields.add('recordtypeid');
      }
      string Amndqry = 'Select Id,Name';
      if (setFields.size() > 0) {
        for (string AmndField : setFields) {
          Amndqry += ',' + AmndField;
        }
      }
      Amndqry += ' From HexaBPM_Amendment__c where ServiceRequest__c=:relSrId';
      list<HexaBPM_Amendment__c> lstAmendments = new List<HexaBPM_Amendment__c>();
      for (HexaBPM_Amendment__c Amnd : database.query(Amndqry)) {
        for (HexaBPM__Service_Request__c objDraftSR : objDraftSrList) {
          HexaBPM_Amendment__c newAmnd = Amnd.Clone();
          newAmnd.Copied_from_Amendment__c = Amnd.Id;
          system.debug('############newAmnd ' + newAmnd);
          newAmnd.ServiceRequest__c = objDraftSR.Id;
          lstAmendments.add(newAmnd);
        }
      }

      system.debug('$$$$$$$$$ relAmend ' + lstAmendments.size());

      map<string, string> MapOldAmendmentId = new Map<string, string>();
      list<HexaBPM__SR_Doc__c> lstSRDocTBU = new List<HexaBPM__SR_Doc__c>();

      if (lstAmendments.size() > 0) {
        insert lstAmendments;
        for (HexaBPM_Amendment__c amnd : lstAmendments) {
          if (
            amnd.Copied_from_Amendment__c != null &&
            MapSRDocContentIds.get(amnd.Copied_from_Amendment__c) != null
          ) {
            for (
              HexaBPM__SR_Doc__c objOldDoc : MapSRDocContentIds.get(
                amnd.Copied_from_Amendment__c
              )
            ) {
              //v1.3
              HexaBPM__SR_Doc__c objSRDocClone = objOldDoc.clone();
              objSRDocClone.HexaBPM_Amendment__c = amnd.Id;
              objSRDocClone.HexaBPM__Service_Request__c = amnd.ServiceRequest__c;
              objSRDocClone.HexaBPM__From_Finalize__c = true;
              objSRDocClone.In_Principle_Document__c = objOldDoc.Id;
              objSRDocClone.HexaBPM__Doc_ID__c = objOldDoc.HexaBPM__Doc_ID__c;
              objSRDocClone.File_Name__c = objOldDoc.File_Name__c;
              objSRDocClone.HexaBPM__Status__c = objOldDoc.HexaBPM__Status__c;
              lstSRDocTBU.add(objSRDocClone);
            }
          }
        }
      }
      for (HexaBPM__SR_Doc__c doc : MapInprincipleDocuments.values()) {
        for (HexaBPM__Service_Request__c objDraftSR : objDraftSrList) {
          HexaBPM__SR_Doc__c objSRDocClone = doc.clone();
          objSRDocClone.HexaBPM__From_Finalize__c = true;
          objSRDocClone.HexaBPM__Service_Request__c = objDraftSR.Id;
          objSRDocClone.In_Principle_Document__c = doc.Id;
          lstSRDocTBU.add(objSRDocClone);
        }
      }

      list<ContentDocumentLink> lstCDL = new List<ContentDocumentLink>();
      if (lstSRDocTBU.size() > 0) {
        upsert lstSRDocTBU;
        for (HexaBPM__SR_Doc__c srdocObj : lstSRDocTBU) {
          if (srdocObj.HexaBPM__Doc_ID__c != null) {
            ContentDocumentLink contDocLnkCloned = new ContentDocumentLink();
            contDocLnkCloned.LinkedEntityId = srdocObj.id;
            contDocLnkCloned.contentdocumentid = srdocObj.HexaBPM__Doc_ID__c;
            contDocLnkCloned.ShareType = 'V';
            lstCDL.add(contDocLnkCloned);
          }
        }
        system.debug('lstCDL' + lstCDL);
        if (lstCDL.size() > 0) {
          insert lstCDL;
        }
      }
    } catch (Exception e) {
      system.debug('$$$$$$$$$$ ' + e.getMessage());
      system.debug('$$$$$$$$$$ ' + e.getLineNumber());
      Log__c objLog = new Log__c();
      objLog.Description__c =
        'Exception on copy new user sr details - Line:129 :' + e.getMessage();
      objLog.Type__c = 'copy new user sr details';
      insert objLog;
    }
  }

  @future
  public static void processMultiStructSR(
    string SRId,
    set<string> setAccountIds
  ) {
    if (SRId != null && setAccountIds != null && setAccountIds.size() > 0) {
      try {
        map<string, string> MapSubSRAccountIds = new Map<string, string>();
        for (Account acc : [
          SELECT Id, Description
          FROM Account
          WHERE Id IN :setAccountIds
        ]) {
          MapSubSRAccountIds.put(acc.Description, acc.Id);
        }
        map<String, Schema.SObjectType> m = Schema.getGlobalDescribe();
        SObjectType objtype;
        DescribeSObjectResult objDef1;
        map<String, SObjectField> fieldmap;
        if (m.get('HexaBPM_Amendment__c') != null) {
          objtype = m.get('HexaBPM_Amendment__c');
          objDef1 = objtype.getDescribe();
          fieldmap = objDef1.fields.getmap();
        }
        set<string> setFields = new Set<string>();
        if (fieldmap != null) {
          for (Schema.SObjectField strFld : fieldmap.values()) {
            Schema.DescribeFieldResult fd = strFld.getDescribe();
            if (fd.isCustom())
              setFields.add(string.valueOf(strFld).toLowerCase());
          }
        }
        string Amndqry = 'Select Id,Name,recordTypeId';
        if (setFields.size() > 0) {
          for (string AmndField : setFields) {
            Amndqry += ',' + AmndField;
          }
        }
        Amndqry += ' From HexaBPM_Amendment__c where OB_MultiStructure_SR__c=:SRId';

        map<string, list<HexaBPM_Amendment__c>> MapMultiStructureAmendments = new Map<string, list<HexaBPM_Amendment__c>>();
        for (HexaBPM_Amendment__c amnd : database.query(Amndqry)) {
          if (amnd.OB_Linked_Multistructure_Applications__c != null) {
            list<string> listChildSRIDs = new List<string>();
            if (amnd.OB_Linked_Multistructure_Applications__c.indexof(',') > -1)
              listChildSRIDs = amnd.OB_Linked_Multistructure_Applications__c.split(
                ','
              );
            else
              listChildSRIDs.add(amnd.OB_Linked_Multistructure_Applications__c);

            for (string MS_SRID : listChildSRIDs) {
              list<HexaBPM_Amendment__c> lstAmnd = new List<HexaBPM_Amendment__c>();
              if (MapMultiStructureAmendments.get(MS_SRID) != null)
                lstAmnd = MapMultiStructureAmendments.get(MS_SRID);
              lstAmnd.add(amnd);
              MapMultiStructureAmendments.put(MS_SRID, lstAmnd);
            }
          }
        }

        list<HexaBPM__Service_Request__c> lstAOR = new List<HexaBPM__Service_Request__c>();
        map<string, HexaBPM__Service_Request__c> MapNewAORS = new Map<string, HexaBPM__Service_Request__c>();
        map<string, list<License_Activity__c>> Map_MS_Activities = new Map<string, list<License_Activity__c>>();
        map<string, list<Company_Name__c>> Map_CompanyNames = new Map<string, list<Company_Name__c>>();

        system.debug('MapSubSRAccountIds==>' + MapSubSRAccountIds);
        system.debug(
          'MapSubSRAccountIds==>' +
          [
            SELECT Id, HexaBPM__IsClosedStatus__c
            FROM HexaBPM__Service_Request__c
            WHERE Id IN :MapSubSRAccountIds.keySet()
          ]
        );

        /* string OpportunityRecordTypeName = 'BD_Non_Financial';
                    
                    if(stp.HexaBPM__SR__r.Entity_Type__c=='Retail')
                        OpportunityRecordTypeName = 'BD_Retail'; 

                
                for(string  accId : setAccountIds ){
                    Opportunity objOpp = new Opportunity();
                    for(RecordType rctyp:[Select Id from RecordType where sobjectType='Opportunity' and DeveloperName= 'BD_Non_Financial']){
                        objOpp.RecordTypeId = rctyp.Id;
                    }
                    objOpp.AccountId = accId;
                    objOpp.CloseDate = system.today();
                    objOpp.Name = stp.HexaBPM__SR__r.entity_name__c+'-Opportunity';
                    objOpp.StageName = 'Created';
                    objOpp.Type = stp.HexaBPM__SR__r.Setting_Up__c;
                    objOpp.Sector_Classification__c = stp.HexaBPM__SR__r.Business_Sector__c;
                    if(!Test.isRunningTest()){
                        objOpp.OwnerId = label.Portal_User_Default_Owner;
                    }
                    insert objOpp;
                }*/

        list<Opportunity> newOpp = new List<Opportunity>();
        list<string> oppRectype = new List<string>{
          'BD_Non_Financial',
          'Retail'
        };
        map<string, string> oppRectypeMap = new Map<string, string>();

        for (RecordType rctyp : [
          SELECT Id, DeveloperName
          FROM RecordType
          WHERE sobjectType = 'Opportunity' AND DeveloperName IN :oppRectype
        ]) {
          oppRectypeMap.put(rctyp.DeveloperName, rctyp.Id);
        }

        system.debug('oppmappp' + oppRectypeMap);

        string ParentAccountId = '';

        HexaBPM__Service_Request__c parentInprincipleSr = new HexaBPM__Service_Request__c();
        for (HexaBPM__Service_Request__c objSR : [
          SELECT Id, Compliance_Rating__c
          FROM HexaBPM__Service_Request__c
          WHERE id = :SRId
        ]) {
          parentInprincipleSr = objSR;
        }
        system.debug('parentInprincipleSr' + parentInprincipleSr);
        system.debug('parentInprincipleSrId' + SRId);

        Id aorFinanceRecType = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName()
          .get('AOR_Financial')
          .getRecordTypeId();
        for (HexaBPM__Service_Request__c objSR : [
          SELECT
            Id,
            HexaBPM__Customer__c,
            HexaBPM__Contact__c,
            HexaBPM__IsClosedStatus__c,
            Entity_Name__c,
            HexaBPM__Parent_SR__r.setting_up__c,
            HexaBPM__Parent_SR__r.legal_structures__c,
            HexaBPM__Parent_SR__r.type_of_entity__c,
            Entity_Type__c,
            Business_Sector__c,
            Legal_Structures__c,
            HexaBPM__Parent_SR__c,
            HexaBPM__Parent_SR__r.Business_Sector__c,
            type_of_entity__c,
            setting_up__c,
            HexaBPM__Parent_SR__r.brief_background__c,
            HexaBPM__Parent_SR__r.Overview_of_Fund_Structure__c,
            HexaBPM__Parent_SR__r.purpose_of_the_foundation__c,
            HexaBPM__Parent_SR__r.reasons_for_choosing_difc__c,
            HexaBPM__Parent_SR__r.Common_Ancestor__c,
            HexaBPM__Parent_SR__r.ownership_control_structures_include__c,
            HexaBPM__Parent_SR__r.estmd_no_of_employees_in_1st_yr__c,
            HexaBPM__Parent_SR__r.HexaBPM__Customer__r.ownerId,
            HexaBPM__Parent_SR__r.Source_of_income__c,
            HexaBPM__Parent_SR__r.Explanation_of_Sources_and_Funds__c,
            HexaBPM__Parent_SR__r.Country_of_Source_Funds__c,
            HexaBPM__Parent_SR__r.Distribution_Channel_Countries__c,
            HexaBPM__Parent_SR__r.first_name__c,
            HexaBPM__Parent_SR__r.last_name__c,
            HexaBPM__Parent_SR__r.Entity_Name__c,
            HexaBPM__Parent_SR__r.HexaBPM__Send_SMS_to_Mobile__c,
            HexaBPM__Parent_SR__r.HexaBPM__Customer__c,
            HexaBPM__Parent_SR__r.HexaBPM__Email__c,
            HexaBPM__Parent_SR__r.Entity_Type__c,
            (
              SELECT Id, Application__c
              FROM Company_Names__r
              WHERE Status__c = 'Approved'
              LIMIT 1
            ),
            (SELECT Id, Account__c, Application__c FROM Activities__r)
          FROM HexaBPM__Service_Request__c
          WHERE
            Id IN :MapSubSRAccountIds.keyset()
            AND HexaBPM__IsClosedStatus__c = TRUE
        ]) {
          if (
            objSR.Company_Names__r != null &&
            objSR.Company_Names__r.size() > 0
          ) {
            if (objSR.Activities__r != null && objSR.Activities__r.size() > 0)
              Map_MS_Activities.put(objSR.Id, objSR.Activities__r);
            if (
              objSR.Company_Names__r != null &
              objSR.Company_Names__r.size() > 0
            )
              Map_CompanyNames.put(objSR.Id, objSR.Company_Names__r);

            ParentAccountId = objSR.HexaBPM__Parent_SR__r.HexaBPM__Customer__c;

            HexaBPM__Service_Request__c multiStructAOR = new HexaBPM__Service_Request__c();
            //multiStructAOR.type_of_entity__c = objSR.HexaBPM__Parent_SR__r.type_of_entity__c;
            //multiStructAOR.setting_up__c = objSR.HexaBPM__Parent_SR__r.setting_up__c;
            //multiStructAOR.legal_structures__c = objSR.HexaBPM__Parent_SR__r.legal_structures__c;
            multiStructAOR.brief_background__c = objSR.HexaBPM__Parent_SR__r.brief_background__c;
            multiStructAOR.Overview_of_Fund_Structure__c = objSR.HexaBPM__Parent_SR__r.Overview_of_Fund_Structure__c;
            multiStructAOR.purpose_of_the_foundation__c = objSR.HexaBPM__Parent_SR__r.purpose_of_the_foundation__c;
            multiStructAOR.reasons_for_choosing_difc__c = objSR.HexaBPM__Parent_SR__r.reasons_for_choosing_difc__c;
            multiStructAOR.Common_Ancestor__c = objSR.HexaBPM__Parent_SR__r.Common_Ancestor__c;
            multiStructAOR.ownership_control_structures_include__c = objSR.HexaBPM__Parent_SR__r.ownership_control_structures_include__c;
            multiStructAOR.estmd_no_of_employees_in_1st_yr__c = objSR.HexaBPM__Parent_SR__r.estmd_no_of_employees_in_1st_yr__c;
            multiStructAOR.Source_of_income__c = objSR.HexaBPM__Parent_SR__r.Source_of_income__c;
            multiStructAOR.Explanation_of_Sources_and_Funds__c = objSR.HexaBPM__Parent_SR__r.Explanation_of_Sources_and_Funds__c;
            multiStructAOR.Country_of_Source_Funds__c = objSR.HexaBPM__Parent_SR__r.Country_of_Source_Funds__c;
            multiStructAOR.Distribution_Channel_Countries__c = objSR.HexaBPM__Parent_SR__r.Distribution_Channel_Countries__c;

            multiStructAOR.HexaBPM__Contact__c = objSR.HexaBPM__Contact__c;
            multiStructAOR.Compliance_Rating__c = parentInprincipleSr.Compliance_Rating__c;

            multiStructAOR.In_Principle_Date__c = system.today();
            multiStructAOR.OB_Is_Multi_Structure_Application__c = 'Yes';
            multiStructAOR.HexaBPM__Customer__c = objSR.HexaBPM__Customer__c;
            multiStructAOR.first_name__c = objSR.HexaBPM__Parent_SR__r.first_name__c;
            multiStructAOR.last_name__c = objSR.HexaBPM__Parent_SR__r.last_name__c;
            multiStructAOR.HexaBPM__Send_SMS_to_Mobile__c = objSR.HexaBPM__Parent_SR__r.HexaBPM__Send_SMS_to_Mobile__c;
            multiStructAOR.HexaBPM__Email__c = objSR.HexaBPM__Parent_SR__r.HexaBPM__Email__c;
            multiStructAOR.Entity_Name__c = objSR.Entity_Name__c;
            multiStructAOR.Business_Sector__c = objSR.Business_Sector__c;
            multiStructAOR.Entity_Type__c = objSR.Entity_Type__c;
            multiStructAOR.Legal_Structures__c = objSR.Legal_Structures__c;
            multiStructAOR.RecordTypeId = aorFinanceRecType;
            multiStructAOR.HexaBPM__Linked_SR__c = objSR.Id;
            multiStructAOR.HexaBPM__Parent_SR__c = SRId;

            multiStructAOR.type_of_entity__c = objSR.type_of_entity__c;
            multiStructAOR.setting_up__c = objSR.setting_up__c;

            lstAOR.add(multiStructAOR);

            string OpportunityRecordID = '';

            if (objSR.Entity_Type__c == 'Retail') {
              OpportunityRecordID = oppRectypeMap.get('BD_Retail');
            } else {
              OpportunityRecordID = oppRectypeMap.get('BD_Non_Financial');
            }

            system.debug(
              'oppOwner' +
              objSR.HexaBPM__Parent_SR__r.HexaBPM__Customer__r.ownerId
            );

            if (OpportunityRecordID != '') {
              Opportunity objOpp = new Opportunity();
              objOpp.RecordTypeId = OpportunityRecordID;
              objOpp.ownerId = objSR.HexaBPM__Parent_SR__r.HexaBPM__Customer__r.ownerId;
              objOpp.AccountId = objSR.HexaBPM__Customer__c;
              objOpp.CloseDate = system.today();
              objOpp.Name = objSR.entity_name__c + '-Opportunity';
              objOpp.StageName = 'Created';
              objOpp.Type = objSR.HexaBPM__Parent_SR__r.setting_up__c;
              objOpp.Sector_Classification__c = objSR.Business_Sector__c;
              /* if(!Test.isRunningTest()){
                                                                    objOpp.OwnerId = label.Portal_User_Default_Owner;
                                                                } */
              newOpp.add(objOpp);
            }
          }
          //MapNewAORS.put(objSR.Id,multiStructAOR);
        }

        system.debug('AOR' + lstAOR);
        system.debug('newOpp' + newOpp);
        if (lstAOR.size() > 0) {
          insert lstAOR;

          for (HexaBPM__Service_Request__c AOR_SR : lstAOR) {
            MapNewAORS.put(AOR_SR.HexaBPM__Linked_SR__c, AOR_SR);
          }

          list<License_Activity__c> lstLAToUpdate = new List<License_Activity__c>();
          for (string MS_SRID : Map_MS_Activities.keyset()) {
            for (License_Activity__c LA : Map_MS_Activities.get(MS_SRID)) {
              LA.Account__c = MapNewAORS.get(MS_SRID).HexaBPM__Customer__c;
              LA.Application__c = MapNewAORS.get(MS_SRID).Id;
              lstLAToUpdate.add(LA);
            }
          }
          list<Company_Name__c> lstCNToUpdate = new List<Company_Name__c>();
          for (string MS_SRID : Map_CompanyNames.keyset()) {
            for (Company_Name__c LA : Map_CompanyNames.get(MS_SRID)) {
              LA.OB_Related_To__c = MapNewAORS.get(MS_SRID)
                .HexaBPM__Customer__c;
              LA.Application__c = MapNewAORS.get(MS_SRID).Id;
              lstCNToUpdate.add(LA);
            }
          }
          set<string> amendIDs = new Set<string>();

          list<HexaBPM_Amendment__c> shareHolderToInsert = new List<HexaBPM_Amendment__c>();
          for (string MS_SRID : MapMultiStructureAmendments.keyset()) {
            if (
              MapNewAORS.get(MS_SRID) != null &&
              MapNewAORS.get(MS_SRID).Id != null
            ) {
              for (
                HexaBPM_Amendment__c Amnd : MapMultiStructureAmendments.get(
                  MS_SRID
                )
              ) {
                amendIDs.add(Amnd.id);
                HexaBPM_Amendment__c objNewAmnd = Amnd.Clone();
                objNewAmnd.OB_MultiStructure_SR__c = null;
                objNewAmnd.Customer__c = MapNewAORS.get(MS_SRID)
                  .HexaBPM__Customer__c;
                objNewAmnd.ServiceRequest__c = MapNewAORS.get(MS_SRID).Id;
                objNewAmnd.Copied_from_Amendment__c = Amnd.id;
                shareHolderToInsert.add(objNewAmnd);
              }
            }
          }

          system.debug('amendIDs' + amendIDs);
          //srDocs
          set<string> docListId = new Set<string>();
          if (amendIDs.size() > 0) {
            for (HexaBPM__SR_Doc__c docObj : [
              SELECT id
              FROM HexaBPM__SR_Doc__c
              WHERE HexaBPM_Amendment__c IN :amendIDs
            ]) {
              docListId.add(docObj.id);
            }
          }
          system.debug('docListId' + docListId);

          map<string, list<HexaBPM__SR_Doc__c>> olddocMap = new Map<string, list<HexaBPM__SR_Doc__c>>();

          if (docListId.size() > 0) {
            map<String, Schema.SObjectType> m2 = Schema.getGlobalDescribe();
            SObjectType objtype2;
            DescribeSObjectResult objDef2;
            map<String, SObjectField> fieldmap2;
            if (m2.get('HexaBPM__SR_Doc__c') != null) {
              objtype2 = m2.get('HexaBPM__SR_Doc__c');
              objDef2 = objtype2.getDescribe();
              fieldmap2 = objDef2.fields.getmap();
            }
            set<string> setFieldsDoc = new Set<string>();
            if (fieldmap2 != null) {
              for (Schema.SObjectField strFld : fieldmap2.values()) {
                Schema.DescribeFieldResult fd2 = strFld.getDescribe();
                if (fd2.isCustom())
                  setFieldsDoc.add(string.valueOf(strFld).toLowerCase());
              }
            }
            string docQuery = 'Select Id,Name';
            if (setFields.size() > 0) {
              for (string docField : setFieldsDoc) {
                docQuery += ',' + docField;
              }
            }
            docQuery += ' From HexaBPM__SR_Doc__c where id in:docListId';
            for (HexaBPM__SR_Doc__c doc : database.query(docQuery)) {
              if (olddocMap.get(doc.HexaBPM_Amendment__c) == null) {
                list<HexaBPM__SR_Doc__c> srList = new List<HexaBPM__SR_Doc__c>();
                srList.add(doc);
                olddocMap.put(doc.HexaBPM_Amendment__c, srList);
              } else {
                olddocMap.get(doc.HexaBPM_Amendment__c).add(doc);
              }
            }
          }

          list<HexaBPM__SR_Doc__c> lstSRDocTBU = new List<HexaBPM__SR_Doc__c>();
          if (shareHolderToInsert.size() > 0)
            insert shareHolderToInsert;

          for (HexaBPM_Amendment__c amnd : shareHolderToInsert) {
            if (
              !olddocMap.isEmpty() &&
              olddocMap.get(amnd.Copied_from_Amendment__c) != null
            ) {
              //HexaBPM__SR_Doc__c objOldDoc = olddocMap.get(amnd.Copied_from_Amendment__c);
              list<HexaBPM__SR_Doc__c> srList = olddocMap.get(
                amnd.Copied_from_Amendment__c
              );
              for (HexaBPM__SR_Doc__c objOldDoc : srList) {
                HexaBPM__SR_Doc__c objSRDocClone = objOldDoc.clone();
                objSRDocClone.HexaBPM_Amendment__c = amnd.Id;
                //objSRDocClone.HexaBPM__Service_Request__c = objDraftSR.Id;
                //objSRDocClone.HexaBPM__From_Finalize__c = true;
                //objSRDocClone.In_Principle_Document__c = objOldDoc.Id;
                objSRDocClone.HexaBPM__Doc_ID__c = objOldDoc.HexaBPM__Doc_ID__c;
                objSRDocClone.File_Name__c = objOldDoc.File_Name__c;
                objSRDocClone.HexaBPM__Status__c = objOldDoc.HexaBPM__Status__c;
                lstSRDocTBU.add(objSRDocClone);
              }
            }
          }

          if (lstLAToUpdate.size() > 0)
            update lstLAToUpdate;
          if (lstCNToUpdate.size() > 0)
            update lstCNToUpdate;
          if (newOpp.size() > 0)
            insert newOpp;

          if (lstSRDocTBU.size() > 0) {
            insert lstSRDocTBU;

            list<ContentDocumentLink> lstCDL = new List<ContentDocumentLink>();
            for (HexaBPM__SR_Doc__c srdocObj : lstSRDocTBU) {
              if (srdocObj.HexaBPM__Doc_ID__c != null) {
                ContentDocumentLink contDocLnkCloned = new ContentDocumentLink();
                contDocLnkCloned.LinkedEntityId = srdocObj.id;
                contDocLnkCloned.contentdocumentid = srdocObj.HexaBPM__Doc_ID__c;
                contDocLnkCloned.ShareType = 'V';
                lstCDL.add(contDocLnkCloned);
              }
            }
            system.debug('lstCDL' + lstCDL);
            if (lstCDL.size() > 0) {
              insert lstCDL;
            }
          }
        }

        copyNewUserSrToChildMultistructure(ParentAccountId, setAccountIds);
      } catch (Exception e) {
        insert LogDetails.CreateLog(
          null,
          'CC_ProcessMultiStructure : processMultiStructSR',
          'Line Number : ' +
          e.getLineNumber() +
          '\nException is : ' +
          e.getMessage()
        );
      }
    }
  }
}