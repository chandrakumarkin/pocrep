/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 5/12/2020
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    4/28/2020  shoaib    Created
**********************************************************************************************************************/
public class ViewCancelledEmployeeController {
    
    @AuraEnabled
    Public static OuterWrapperClass getWrapperList(String searchKey,Integer pageNumber,String jsonstring){
        
        List<viewCancelledWrapper> thisList = new List<viewCancelledWrapper>();
        
        Integer pSize     = 10;
        Integer pNumber   = (Integer) pageNumber;
        Integer offsets   = (pNumber - 1) * pSize;
        Integer recordEnd = pSize * pNumber;
        
        String soqlQuery = 'SELECT Id ,Account__r.Name,Designation__c,Experience__c,Area_of_relevant_experience__c,Email__c,EMPLOYEE_CONSENT__c,Experience_In_Years__c,';
        soqlQuery += ' First_Name__c,Last_Name__c,LinkedIn_Profile__c,Mobile__c FROM Cancelled_Employees__c ';
        
      
        
        if(String.isNotBlank(jsonstring)){
           List < Object > fieldList  = (List < Object > ) JSON.deserializeUntyped(jsonstring);
        
            for (Object thisObject: fieldList) {
                Map < String, Object > dataMap = (Map < String, Object > ) thisObject;
                String selectedField               = (String) dataMap.get('selectedField');
                String operator                    = (String) dataMap.get('operator');
                String value                       = (String) dataMap.get('value'); 
                
                if(String.isNotBlank(selectedField) && String.isNotBlank(value)){
                    soqlQuery += ' WHERE ' +selectedField+ ' like \'' + string.escapeSingleQuotes(value) + '%\' AND EMPLOYEE_CONSENT__c =\'Yes\'';
                }
                else{
                    soqlQuery += ' WHERE  EMPLOYEE_CONSENT__c = \'Yes\'';
                }
              }
           
          }
        Integer totalRecords = Database.query(soqlQuery).size();
        soqlQuery += ' LIMIT 10 OFFSET: offsets';
        
        Set<String> employeeIds = new  Set<String>();
        
        for(Cancelled_Employees__c thisCancelledEmployee : Database.query(soqlQuery)){
            employeeIds.add(thisCancelledEmployee.Id);
        }
        
        Map<String,Attachment> attachmentMap = new  Map<String,Attachment>();
       
        for(Attachment thisAttachment : [SELECT Id, Name, ParentId, Parent.Type FROM Attachment where Parent.Type = 'Cancelled_Employees__c' and ParentId IN:employeeIds]){
            if(!attachmentMap.containsKey(thisAttachment.ParentId)){
                attachmentMap.put(thisAttachment.ParentId, thisAttachment);
            }
        }
        
        for(Cancelled_Employees__c thisCancelledEmployee : Database.query(soqlQuery)){
            if(attachmentMap.containsKey(thisCancelledEmployee.Id)){
                thisList.add(new viewCancelledWrapper(thisCancelledEmployee,attachmentMap.get(thisCancelledEmployee.Id))); 
            }
            else{
                thisList.add(new viewCancelledWrapper(thisCancelledEmployee,new Attachment())); 
            }
         
        }
        
        OuterWrapperClass thisOuterWrapper    = new OuterWrapperClass();
        thisOuterWrapper.thisRelationShipList = thisList;
        thisOuterWrapper.pageNumber           = pNumber;
        thisOuterWrapper.recordStart          = offsets + 1;
        thisOuterWrapper.recordEnd            = totalRecords >= recordEnd ? recordEnd : totalRecords;
        thisOuterWrapper.totalRecords         = totalRecords;

        return thisOuterWrapper;
     }
    
    public class OuterWrapperClass {
        @AuraEnabled
        public List <viewCancelledWrapper> thisRelationShipList {
            get;
            set;
        }
        @AuraEnabled
        public Integer pageNumber {
            get;
            set;
        }
        @AuraEnabled
        public Integer totalRecords {
            get;
            set;
        }
        @AuraEnabled
        public Integer recordStart {
            get;
            set;
        }
        @AuraEnabled
        public Integer recordEnd {
            get;
            set;
        }

        public outerWrapperClass() {

        }
    }
    
    public class viewCancelledWrapper {
        
        @AuraEnabled
        public Cancelled_Employees__c thisEmployee {get; set;}
        @AuraEnabled
        public Attachment thisAttachment {get; set;}
        @AuraEnabled
        public Boolean isDisabled {get; set;}
        @AuraEnabled
        public Boolean isLinkedInDisabled {get; set;}
        @AuraEnabled
        public Boolean isjobFunction {get; set;}
        
        public viewCancelledWrapper(Cancelled_Employees__c thisEmployee ,Attachment thisAttachment ){
            
            this.thisEmployee = thisEmployee;
            this.thisAttachment = thisAttachment;
            
            if(String.isBlank(thisAttachment.Id)){
                isDisabled = true;
            }
            if(String.isBlank(thisEmployee.LinkedIn_Profile__c)){
                isLinkedInDisabled = true;
            }
             if(String.isBlank(thisEmployee.Area_of_relevant_experience__c)){
                isjobFunction = true;
            }
        } 
    }

}