/******************************************************************************************
 *  Author      : Durga Prasad
 *  Company     : PwC
 *  Date        : 03-Feb-2020     
 *  Description : helper class for OB_AmendmentTriggerHandler
 *  Modification History :
 ----------------------------------------------------------------------------------------
    V.No    Date          Updated By    Description
    =====   ===========   ==========    ============
    v.1.0   03/Feb/2020   Durga         Invoking the helper method to create History Track records.
    v.2.0   18/Mar/2020   Maruf         updated the obbject name from "Application_History__c" to "Application_Amendment_History__c"
*******************************************************************************************/
public without sharing class OB_AmendmentTriggerHelper {
	
	/*
        Method Name :   PopulateMultiStructureEntityName
        Description :   Method to Populate Mlti Structure Child SR's Entity Name
    */
	public static void PopulateMultiStructureEntityName(list<HexaBPM_Amendment__c> TriggerNew,map<Id,HexaBPM_Amendment__c> TriggerOldMap,boolean IsInsert){
		list<string> MultiStructureSRIDS = new list<string>();
		map<string,string> MapCompanyNameBySRID = new map<string,string>();
		if(IsInsert){
			for(HexaBPM_Amendment__c amnd:TriggerNew){
				if(amnd.OB_Linked_Multistructure_Applications__c!=null){
					MultiStructureSRIDS = amnd.OB_Linked_Multistructure_Applications__c.split(',');
				}
			}
			system.debug('MultiStructureSRIDS===>'+MultiStructureSRIDS);
			if(MultiStructureSRIDS.size()>0){
				for(HexaBPM__Service_Request__c objSR:[Select Id,Entity_Name__c from HexaBPM__Service_Request__c where Id IN:MultiStructureSRIDS]){
					MapCompanyNameBySRID.put(objSR.Id,objSR.Entity_Name__c);
				}
			}
			system.debug('MapCompanyNameBySRID===>'+MapCompanyNameBySRID);
			for(HexaBPM_Amendment__c amnd:TriggerNew){
				if(amnd.OB_Linked_Multistructure_Applications__c!=null){
					string EntityNames = '';
					for(string SRID : amnd.OB_Linked_Multistructure_Applications__c.split(',')){
						if(MapCompanyNameBySRID.get(SRID)!=null){
							if(EntityNames=='')
								EntityNames = MapCompanyNameBySRID.get(SRID);
							else
								EntityNames = EntityNames+','+MapCompanyNameBySRID.get(SRID);
						}
					}
					amnd.Relates_To__c = EntityNames;
					system.debug('EntityNames===>'+EntityNames);
				}else{
					amnd.Relates_To__c = null;
				}
			}
		}else{
			MultiStructureSRIDS = new list<string>();
			MapCompanyNameBySRID = new map<string,string>();
			for(HexaBPM_Amendment__c amnd:TriggerNew){
				if(amnd.OB_Linked_Multistructure_Applications__c != TriggerOldMap.get(amnd.Id).OB_Linked_Multistructure_Applications__c){
					if(amnd.OB_Linked_Multistructure_Applications__c!=null)
						MultiStructureSRIDS = amnd.OB_Linked_Multistructure_Applications__c.split(',');
				}
			}
			if(MultiStructureSRIDS.size()>0){
				for(HexaBPM__Service_Request__c objSR:[Select Id,Entity_Name__c from HexaBPM__Service_Request__c where Id IN:MultiStructureSRIDS]){
					MapCompanyNameBySRID.put(objSR.Id,objSR.Entity_Name__c);
				}
			}
			for(HexaBPM_Amendment__c amnd:TriggerNew){
				if(amnd.OB_Linked_Multistructure_Applications__c != TriggerOldMap.get(amnd.Id).OB_Linked_Multistructure_Applications__c){
					if(amnd.OB_Linked_Multistructure_Applications__c!=null){
						string EntityNames = '';
						for(string SRID : amnd.OB_Linked_Multistructure_Applications__c.split(',')){
							if(MapCompanyNameBySRID.get(SRID)!=null){
								if(EntityNames=='')
									EntityNames = MapCompanyNameBySRID.get(SRID);
								else
									EntityNames = EntityNames+','+MapCompanyNameBySRID.get(SRID);
							}
						}
						amnd.Relates_To__c = EntityNames;
					}else{
						amnd.Relates_To__c = null;
					}
				}
			}
		}
	}

    /*
        Method Name :   CreateHistoryRecords
        Description :   Method to create the Field History Tracking object
    */
    public static void CreateHistoryRecords(list<HexaBPM_Amendment__c> TriggerNew,map<Id,HexaBPM_Amendment__c> TriggerOldMap,boolean IsInsert){
        map<string,string> mapHistoryFields = new map<string,string>();
        map<string,History_Tracking__c> mapHistoryCS = new map<string,History_Tracking__c>();
        if(History_Tracking__c.getAll()!=null){
            mapHistoryCS = History_Tracking__c.getAll();//Getting the Objects from Custom Setting which has to be displayed in the screen for the users
            for(History_Tracking__c objHisCS:mapHistoryCS.values()){
                if(objHisCS.Object_Name__c!=null && objHisCS.Object_Name__c.tolowercase()=='hexabpm_amendment__c' && objHisCS.Field_Name__c!=null){
                    mapHistoryFields.put(objHisCS.Field_Name__c,objHisCS.Field_Label__c);
                }
            }
        }
        list<sobject> lstApplicationHistory = new list<sobject>();
        if(IsInsert){
            for(HexaBPM_Amendment__c app:TriggerNew){
                if((system.test.isrunningtest() || (app.Application_Status__c!=null && app.Application_Status__c.tolowercase()!='draft')) && mapHistoryFields!=null && mapHistoryFields.size()>0){
                    for(string objFld:mapHistoryFields.keyset()){
                        string NewValue = '';
                        if(app.get(objFld)!=null)
                            NewValue = app.get(objFld)+'';
                        sobject objHistory = OB_ObjectHistoryTrackingUtil.CreateObjectHistory('Application_Amendment_History__c',NewValue,'',app.Id,mapHistoryFields.get(objFld),objFld);
                        lstApplicationHistory.add(objHistory);
                    }
                }
            }
        }else{
            for(HexaBPM_Amendment__c app:TriggerNew){
                if((system.test.isrunningtest() || (app.Application_Status__c!=null && app.Application_Status__c.tolowercase()!='draft')) && mapHistoryFields!=null && mapHistoryFields.size()>0 && TriggerOldMap.get(app.Id)!=app){
                    for(string objFld:mapHistoryFields.keyset()){
                        if(app.get(objFld) != trigger.oldMap.get(app.Id).get(objFld)){
                            string OldValue = '';
                            string NewValue = '';
                            if(app.get(objFld)!=null)
                                NewValue = app.get(objFld)+'';
                            if(trigger.oldMap.get(app.Id).get(objFld)!=null)
                                OldValue = trigger.oldMap.get(app.Id).get(objFld)+'';
                            sobject objHistory = OB_ObjectHistoryTrackingUtil.CreateObjectHistory('Application_Amendment_History__c',NewValue,OldValue,app.Id,mapHistoryFields.get(objFld),objFld);
                            lstApplicationHistory.add(objHistory);
                        }
                    }
                }
            }
        }
        if(lstApplicationHistory.size()>0)
            insert lstApplicationHistory;
    }
}