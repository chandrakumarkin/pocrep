/*********************************************************************************************************************
*  Name     : LiquidatorPendingDissolutionBatch 
*  Author   : 
*  Purpose  : This class used to send email to Liquidator when Company dissolution pending more than one year
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
V1.1         DIFC                26-July-2021               Draft Version
**/

global class LiquidatorPendingDissolutionBatch implements database.batchable<sobject>,Database.AllowsCallouts, Database.Stateful{
    
    public static string testStatus ='';
    global database.querylocator start(database.batchablecontext bc){
        
        Date dt= Date.newInstance(system.today().year()-1, system.today().month(), system.today().Day());

        system.debug('----dt---'+dt);
        /*string s ='select Id,Name,BP_No__c,Pending_Dissolution_Date__c,ROC_Status__c, ';
        s+=' (select Name,Id,RecordType.Name,Email from Contacts where RecordType.Name = \'Liquidator\' and Email != null order by CreatedDate desc limit 1 ) ';
        s+=' from Account where Pending_Dissolution_Date__c != null and ROC_Status__c =\'Pending Dissolution\'';
        s+= ' and Pending_Dissolution_Date__c <:dt';
        system.debug('=query==test='+s);
        system.debug('=query==test='+database.query(s));
        */
        string s= 'select Name,Id,RecordType.Name,Email,AccountId,Account.Name,Account.BP_No__c,Account.Pending_Dissolution_Date__c,Account.ROC_Status__c from Contact where RecordType.Name = \'Liquidator\' and Email != null'; 
        s+=' and Account.Pending_Dissolution_Date__c != null and Account.ROC_Status__c =\'Pending Dissolution\' and Account.Pending_Dissolution_Date__c <:dt' ;
        s+=' order by email asc ';
        return database.getquerylocator(s);
    }
    
    global void execute(database.batchablecontext bc, List<Contact> accounts){
        try{
            system.debug('=========accounts ====size==========='+accounts.size());
            system.debug('=========accounts ==============='+accounts);

            Map<string,List<Contact>> liquidatorPendingDissolutionMap = new Map<string,List<Contact>>();
            Map<string,contact> contactMap = new Map<string,contact>();
            for(Contact acc : accounts){
                system.debug('=========errorresult ==============='+acc);
                if(liquidatorPendingDissolutionMap.get(acc.Email) == null)liquidatorPendingDissolutionMap.put(acc.Email,new List<Contact>());
                liquidatorPendingDissolutionMap.get(acc.Email).add(acc);
                contactMap.put(acc.Email,acc);
            }
            system.debug('=========liquidatorPendingDissolutionMap ======keyset========='+liquidatorPendingDissolutionMap.keyset());
            List<Enactment_Notification__c> entachmentNotificationList = new List<Enactment_Notification__c>();
        
            for(string emailAddress : liquidatorPendingDissolutionMap.keyset()){
            
                system.debug('=========emailAddress ==============='+emailAddress);
                String htmlBody = '';

                //open table..
                htmlBody = '<table border="1" style="border-collapse: collapse"><caption></caption><tr><th>Company Name</th><th>Registered No.</th></tr>';
            
                //iterate over list and output columns/data into table rows...
                for(Contact acc : liquidatorPendingDissolutionMap.get(emailAddress)){
                    htmlBody += '<tr><td>' + acc.Account.Name + '</td><td>' + acc.Account.BP_No__c + '</td></tr>';
                }
                //close table...
                htmlBody += '</table>';
                Enactment_Notification__c ObjNoti=new Enactment_Notification__c(Type__c='Liquidator Pending Dissolution',Send_Email__c=true);  
                
                if(contactMap.get(emailAddress) != null && contactMap.containskey(emailAddress)){
                    ObjNoti.Portal_User_1__c=contactMap.get(emailAddress).Id;  
                    ObjNoti.Description__c = contactMap.get(emailAddress).Name;  
                    Enactment_Notification__c objAcct=new Enactment_Notification__c(Account__c=contactMap.get(emailAddress).AccountId,Type__c='Liquidator Pending Dissolution Customer',Send_Email__c=true);
                    entachmentNotificationList.add(objAcct);
                }
                ObjNoti.Account_Business_Activity_Name__c = htmlBody;
                entachmentNotificationList.add(ObjNoti);
                system.debug('=========ObjNoti ==============='+ObjNoti);

            }
            system.debug('=========entachmentNotificationList ==============='+entachmentNotificationList.size());
            
            if(entachmentNotificationList.size()>0)insert entachmentNotificationList;
            
        }catch(Exception e){
            string errorresult = e.getMessage();
            system.debug('=========errorresult ==============='+errorresult +'\n------Line Number :------'+e.getLineNumber()+'\nException is : '+errorresult);
            insert LogDetails.CreateLog(null, 'LiquidatorPendingDissolutionBatch: execute', 'Line Number : '+e.getLineNumber()+'\nException is : '+errorresult);
        }
    }
    
    global void finish(database.batchablecontext bc){
    
    }
}