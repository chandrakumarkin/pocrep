/*
* DocuCollectionBatch Class
* Author      : Hussain Fazal Ullah 
* Date        : 2019-08-28     
* Modification History:
*****************************************************************************************************************
* Version       Date                Author                  Remarks                 Description     
* 1.0           2019-08-28          Hussain Fazal Ullah     Created The Class       Created the batch class to send 
                                                                                    email reminder if the documents 
                                                                                    are not collected

This test class is used for DocuCollectionBatch Class and PSAReminderBatch Class
*****************************************************************************************************************/

@isTest
public class PSAReminderBatchTest
{
    static testMethod void testMethod1()
    {       
        List<Account> lstAccount= new List<Account>();
       
        {
            Account acc = new Account();
            acc.Name ='Name';
            acc.PSA_Deposit_Claculated__c =-2000;
            lstAccount.add(acc);
        }
        insert lstAccount;
        
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE sObjectType = 'Service_Request__c' AND DeveloperName = 'Non_DIFC_Sponsorship_Visa_Renewal'];
        
        Contact con = new Contact(LastName ='testCon',AccountId = lstAccount[0].Id ,Email = 'abc@hotmail.com');
        insert con;   
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = lstAccount[0].Id;
        objSR.RecordTypeId = rt.id;
        
       
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Emirates_Id_Number__c = '784-2001-1234567-1';
        objSR.Expiry_Date__c = Date.today();
        insert objSR;
        
        Step_Template__c objTemp = new Step_Template__c();
        objTemp.Name ='Collected';
        objTemp.Code__c = 'Collected';
        objTemp.Step_RecordType_API_Name__c ='General';
        insert objTemp;
        
        Status__c stepStatus = new Status__c();
        stepStatus.Name = 'Awaiting Review';
        stepStatus.Code__c ='AWAITING_REVIEW';
        insert stepStatus;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.DNRD_Receipt_No__c = '009988776655';
        objStep.Step_Template__c = objTemp.id;
        objStep.Status__c = stepStatus.id;
        //objStep.Step_Name__c = 'Front Desk Review';
        insert objStep;
        
        
        List<User> lstUser = new List <User>();
        Profile pf= [Select Id from profile where Name='System Administrator']; 
        
        String orgId=UserInfo.getOrganizationId(); 
        String dateString=String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','') ;
        Integer RandomId=Integer.valueOf(Math.rint(Math.random()*1000000)); 
        String uniqueName=orgId+dateString+RandomId; 
        User uu=new User(firstname = 'ABC', 
                         lastName = 'XYZ', 
                         email = uniqueName + '@test' + orgId + '.org', 
                         Username = uniqueName + '@test' + orgId + '.org', 
                         EmailEncodingKey = 'ISO-8859-1', 
                         Alias = uniqueName.substring(18, 23), 
                         TimeZoneSidKey = 'America/Los_Angeles', 
                         LocaleSidKey = 'en_US', 
                         LanguageLocaleKey = 'en_US',
                         Community_User_Role__c ='Employee Services',
                         CompanyName = lstAccount[0].name,
                         IsActive = true,
                         ProfileId = pf.id
                        ); 
			insert uu;
        List<String> lstEmailId = new List<String>();
        Map<Id,String> mapContactEmail = new Map<Id,String>();
        mapContactEmail.put( lstAccount[0].id,uu.Email);
        

        Test.startTest();
        DocuSchedule   obj = new DocuSchedule ();
        
        DocuCollectionBatch  sh1 = new DocuCollectionBatch ();
        String sch = '0 0 23 * * ?'; 
        String jobId = system.schedule('A job', sch, obj); 
        
         	            
            DocuCollectionBatch obj2 = new DocuCollectionBatch();
            DataBase.executeBatch(obj2);
        
        Test.stopTest();
           
      
    }
      static void testEmailUtility()
    {
     Test.StartTest();
        
        Integer invocations = Limits.getEmailInvocations();
        Test.stopTest();
        
        System.assertEquals(1, invocations, 'An email has not been sent');
    }
}