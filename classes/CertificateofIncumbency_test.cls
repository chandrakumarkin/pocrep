/***********************************************************
*Apex Class : CertificateofIncumbency_test 
*Description: Test class for CertificateofIncumbency
*Creaed Date : 02-Jul-2019
*Assembla : #4979
************************************************************/
@isTest
private class CertificateofIncumbency_test {
    static testMethod void myUnitTest() {
        test.startTest();
       
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.RecordTypeId = [select Id,Name,DeveloperName from RecordType where DeveloperName ='DFSA_and_Share' AND SobjectType='Account' limit 1].id;
        objAccount.Financial_Year_End__c = '31st December';
        //objAccount.Active_License__c = objLic.id;
        insert objAccount;
      
        Service_Request__c  srRec = new Service_Request__c ();
        srRec.Customer__c= objAccount.Id;
        srRec.Doyouwishtoaddpassportnumbers__c= 'Yes';
        srRec.Confirm_Change_of_Entity_Name__c= 'Yes';
        srRec.Confirm_Change_of_Trading_Name__c = 'Yes';
        insert srRec;
        
        PageReference pageRef = Page.CertificateofIncumbency;
        Test.setCurrentPage(pageRef); 
        Apexpages.currentPage().getParameters().put('id',srRec.id);     
        CertificateofIncumbency clsObj = new CertificateofIncumbency();
        srRec.RecordTypeId =[select Id,Name,DeveloperName from RecordType where DeveloperName='Certificate_of_Incumbency' AND SobjectType='Service_Request__c'].id;
        clsObj.SaveConfirmation();
        clsObj.EditSR();
        clsObj.legalType = 'LTD';
        //clsObj.SubmitRequest();
        //clsObj.CancelRequest();
        clsobj.CancelEco();
        clsobj.DynamicButtonAction();
        test.stopTest();
    }
    static testMethod void myUnitTest2() {
        test.startTest();
       
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.RecordTypeId = [select Id,Name,DeveloperName from RecordType where DeveloperName ='DFSA_and_Share' AND SobjectType='Account' limit 1].id;
        objAccount.Financial_Year_End__c = '31st December';
        //objAccount.Active_License__c = objLic.id;
        insert objAccount;
       
       
        Service_Request__c  srRec = new Service_Request__c ();
        srRec.Customer__c= objAccount.Id;
        srRec.Doyouwishtoaddpassportnumbers__c=null;
        srRec.Confirm_Change_of_Entity_Name__c= null;
        srRec.Confirm_Change_of_Trading_Name__c =null;
        insert srRec;
       
        
        PageReference pageRef = Page.CertificateofIncumbency;
        Test.setCurrentPage(pageRef); 
        Apexpages.currentPage().getParameters().put('id',null);     
        CertificateofIncumbency clsObj = new CertificateofIncumbency();
        clsObj.legalType = 'LTD';
        clsObj.SRData = srRec;
        srRec.RecordTypeId =[select Id,Name,DeveloperName from RecordType where DeveloperName='Certificate_of_Incumbency' AND SobjectType='Service_Request__c'].id;
        clsobj.DynamicButtonAction();
        clsObj.SaveConfirmation();
        srRec.Doyouwishtoaddpassportnumbers__c='Yes';
        srRec.Confirm_Change_of_Entity_Name__c= 'Yes';
        srRec.Confirm_Change_of_Trading_Name__c =null;
        update srRec;
        clsobj.DynamicButtonAction();
        clsObj.SaveConfirmation();
        clsobj.CancelEco();
        test.stopTest();
    }
}