@isTest
public class GSDFSACancPriceItemController_Test{

  static testMethod void GSDFSACancPriceItemControllerUnitTest1(){
    
        Test.startTest();
        Account objAccount          = new Account();
        objAccount.Name             = 'Test Custoer 1';
        objAccount.E_mail__c        = 'test@test.com';
        objAccount.BP_No__c         = '001234';
        objAccount.Company_Type__c  ='Financial - related';
        objAccount.Sector_Classification__c     = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c      = 'LTD';
        objAccount.ROC_Status__c                = 'Active';
        objAccount.Financial_Year_End__c        = '31st December';
        objAccount.Registered_Address__c        = 'Testr';
        insert objAccount;
        
        License__c objLic = new License__c();
        objLic.License_Issue_Date__c    = system.today();
        objLic.License_Expiry_Date__c   = system.today().addDays(365);
        objLic.Status__c                = 'Active';
        objLic.Account__c               = objAccount.id;
        insert objLic;
        
        objAccount.Active_License__c = objLic.id;
        update objAccount;
        Id portalUserId                 = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact              = new Contact();
        objContact.firstname            = 'Test Contact';
        objContact.lastname             = 'Test Contact1';
        objContact.accountId            = objAccount.id;
        objContact.recordTypeId         = portalUserId;
        objContact.Email                = 'test@difcportal.com';
        
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser    = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Non_Sponsored_Employment_Cancellation')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        SR_Status__c SRst = new SR_Status__c();
        SRst.Name ='Draft';
        SRst.Code__c ='DRAFT';
        insert SRst;
        
        Service_Request__c objSR             = new Service_Request__c();
        objSR.RecordTypeId = mapRecordTypeIds.get('Non_Sponsored_Employment_Cancellation');
        objSR.Customer__c                    = objAccount.Id;
        objSR.Email__c                       = 'vvvv@gmail.com';
        objSR.Send_SMS_To_Mobile__c          = '+971523830186';
        objSR.Nationality_list__c            = 'India';
        objSR.Type_of_Request__c              = 'DFSA Employee Card';
        objSR.External_SR_Status__c           =  SRst.id;
        objSR.Contact__c                     = objContact.id;
        insert objSR;
        
       Service_request__C SR = [select id,External_Status_Name__c ,Contact__c,Customer__c from Service_request__C where id =:objSR.id limit 1] ;
        
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Non-Sponsored Employment Cancellation';
        objProd.ProductCode = 'Non-Sponsored Employment Cancellation';
        lstProducts.add(objProd);
        insert lstProducts;
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'Non-Sponsored Employment Cancellation';
        objPL.DEV_Id__c = 'GO-00229_Zero';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00229_Zero';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            objDP.Unit_Price__c = 0;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
    
        
        System.runAs(objUser) {
        
        Relationship__c  rel = new Relationship__c ();
        rel.Relationship_Type__c ='Has DIFC Local /GCC Employees';
        rel.Subject_Account__c  =objAccount.Id;
        rel.Object_Contact__c = objContact.id;
        rel.Active__C  = true;
        insert rel;
        
        GSDFSACancPriceItemController.GSDFSACancPriceItemsMethod(new List<id> {SR.id});
        
        }
        Test.stopTest();
    }
 }