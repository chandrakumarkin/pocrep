/*****************************************************************
Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date           Updated By           Description
 ----------------------------------------------------------------------------------------              
 V1.0    12-Dec-2015    Shabbir Ahmed        Custom batch class to delete all the record from any object 
*****************************************************************/

global class BatchMassDeleteRecords implements  Database.Batchable<sObject> {

    global final string query;
    
    global BatchMassDeleteRecords(String q){
       query = q;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
    
       return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
      delete scope;
    }
    
    
    global void finish(Database.BatchableContext BC){
        
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
      TotalJobItems, CreatedBy.Email
      from AsyncApexJob where Id =:BC.getJobId()];
        
      // Send an email to the Apex job's submitter notifying of job completion.  
      Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

	  OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'portal@difc.ae'];
	  if (owea != null && owea.size() > 0) {
	    mail.setOrgWideEmailAddressId(owea.get(0).Id);
	  }  

      String[] toAddresses = new String[] {'shabbir.ahmed@difc.ae'};
      mail.setToAddresses(toAddresses);
      mail.setSubject('Company Transaction Batch ' + a.Status);
      mail.setPlainTextBody('The batch Apex job processed ' + a.TotalJobItems +
        ' batches with '+ a.NumberOfErrors + ' failures.');
    
      Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail }); 
        
    }

}