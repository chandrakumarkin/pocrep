@istest
public with sharing class OB_CPRegisteredAddressControllerTest 
{

    public static testmethod void getOperatingLocationsTest()
    {
       // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        Account hostingCompany = new Account(Name = 'hostingCompany');
        insert hostingCompany;
        
       
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(1, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        section.HexaBPM__Default_Rendering__c=true;
        insert section;

        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
      
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
       
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(9, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        
        // for pescribe company.
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].Id;
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[0].Business_Sector__c = 'Prescribed Companies';
        insertNewSRs[0].Type_of_Entity__c = 'Private';
        insertNewSRs[0].Name_of_the_Hosting_Affiliate__c = hostingCompany.Id;
        
        // for Hosting  company.
        insertNewSRs[1].HexaBPM__Customer__c = insertNewAccounts[0].Id;
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[1].Name_of_the_Hosting_Affiliate__c = hostingCompany.Id;
        
        // for reg Agent  company.
        insertNewSRs[2].HexaBPM__Customer__c = insertNewAccounts[0].Id;
        insertNewSRs[2].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[2].Registered_Agent__c = hostingCompany.Id;
        
         // for co working  company.
        insertNewSRs[3].HexaBPM__Customer__c =  hostingCompany.Id;
        insertNewSRs[3].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[3].Co_working_Facility__c = 'Yes';
        
        // for Fund_Administrator_Name__c
        insertNewSRs[4].HexaBPM__Customer__c =  insertNewAccounts[0].Id;
        insertNewSRs[4].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[4].Fund_Administrator_Name__c = hostingCompany.Id;
        
         // for Fund_Manager__c
        insertNewSRs[5].HexaBPM__Customer__c =  insertNewAccounts[0].Id;
        insertNewSRs[5].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[5].Fund_Manager__c = hostingCompany.Id;
        
         // for error
        insertNewSRs[6].HexaBPM__Customer__c =  insertNewAccounts[0].Id;
        insertNewSRs[6].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        //insertNewSRs[5].Fund_Manager__c = hostingCompany.Id;
        
         // for own and self hosting
        insertNewSRs[7].HexaBPM__Customer__c =  hostingCompany.Id;
        insertNewSRs[7].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        
        // for CSP 
        insertNewSRs[8].HexaBPM__Customer__c =  insertNewAccounts[0].Id;
        insertNewSRs[8].Corporate_Service_Provider_Name__c = hostingCompany.Id;
        insertNewSRs[8].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
         
             
                     
        insert insertNewSRs;
        system.debug('######### insertNewSRs'+insertNewSRs);
        
        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1,new List<String>{'Low'},new List<Integer>{1}, new List<String>{'Low'});
        insert listCS;
        
       
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;
        
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(1, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insertNewSteps[0].HexaBPM__SR__c = insertNewSRs[0].Id;
        insert insertNewSteps;
        
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'DIFC';
        objBuilding.Building_No__c = '1234';
        objBuilding.Company_Code__c = '5200';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'DIFC Retail Outlet General';
        lstUnits.add(objUnit);
        insert lstUnits;
        
        
        Operating_Location__c objOL = new Operating_Location__c();
        objOL.Account__c = hostingCompany.Id; //  insertNewAccounts[0].Id
        //objOL.Building_Name__c = 'Test';
        objOL.Unit__c = lstUnits[0].Id;
        objOL.Status__c = 'Active';
        objOL.Type__c = 'Leased';
        objOL.IsRegistered__c = true;
        insert objOL;
        
         //create amendments
        list<HexaBPM_Amendment__c> listAmendments = new list<HexaBPM_Amendment__c>() ;
        listAmendments = OB_TestDataFactory.createApplicationAmendment(2,listCS,insertNewSRs);
        listAmendments[0].ServiceRequest__c = insertNewSRs[0].Id;
        listAmendments[0].Amendment_Type__c = 'Operating Location';
        listAmendments[0].Status__c = 'Active';
        listAmendments[0].Unit__c =  lstUnits[0].Id; 
        
        // For CSP
        listAmendments[1].ServiceRequest__c = insertNewSRs[8].Id;
        listAmendments[1].Amendment_Type__c = 'Operating Location';
        listAmendments[1].Status__c = 'Active';
        listAmendments[1].Unit__c =  lstUnits[0].Id; 
        
        
        insert listAmendments;
        
        
        test.startTest();
        OB_CPRegisteredAddressController.RequestWrapper reqParam = new OB_CPRegisteredAddressController.RequestWrapper();
        
         	 reqParam.flowId = listPageFlow[0].Id;
             reqParam.pageId = listPage[0].Id;
             reqParam.srId = insertNewSRs[0].Id;
             //reqParam.stepId = insertNewSteps[0].Id;
             //reqParam.stepObj = insertNewSteps[0];
             //For pescribe company
             
        	 insertNewSRs[8].Name_of_Affiliated_DIFC_Entities__c = hostingCompany.Id;
             insertNewSRs[8].Type_of_commercial_permission__c = 'Dual license of DED licensed firms with an affiliate in DIFC';
             update insertNewSRs[8];
        	
        
        
        	// for own and self hosting
            /*
                reqParam.srId =   insertNewSRs[7].Id;
                String reqParamStringSelf = JSON.serialize(reqParam);    
                OB_CPRegisteredAddressController.getOperatingLocations(reqParamStringSelf);
            */
        	
        
        
        	//For save
        	reqParam.srId =   insertNewSRs[8].Id;
        	
        	
           
            OB_CPRegisteredAddressController.SRWrapper srWrapsave = new OB_CPRegisteredAddressController.SRWrapper();    
            srWrapsave.srObj = insertNewSRs[8];
            reqParam.srWrap = srWrapsave;
            reqParam.oprLocWrap = new OB_CPRegisteredAddressController.OperationLocationWrapper();
        	reqParam.oprLocWrap.oprLocObj = objOL;
        	String reqParamSave = JSON.serialize(reqParam);     
            OB_CPRegisteredAddressController.onAmedSaveToDB(reqParamSave);
            
            //for remove
            reqParam.amedWrap = new OB_CPRegisteredAddressController.AmendmentWrapper();
            reqParam.amedWrap.amedObj = listAmendments[1];
            String reqParamRemove = JSON.serialize(reqParam); 
            OB_CPRegisteredAddressController.removeOprLocAmed(reqParamRemove);
        
        
        	// Button Action
        	OB_CPRegisteredAddressController.getButtonAction(insertNewSRs[8].Id,sec.Id ,listPage[0].Id); 
        
        	
            String reqParamStringSelf = JSON.serialize(reqParam);    
            OB_CPRegisteredAddressController.getCPRegisterAddress(reqParamStringSelf);
        	
        test.stopTest();
       
    }
}