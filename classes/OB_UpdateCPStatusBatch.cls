/**********************************************************************************************************************
* Name               : OB_UpdateCPStatusBatch
* Description        : This batch class is used to activate & deactivate the Commercial Permisssion for Pocket Shops based on the lease status
* Created Date       : 30-11-2020
* Created By         : PWC Digital Middle East
* --------------------------------------------------------------------------------------------------------------------*
* Version       Author                  Date             Comment
* 1.0           salma.rasheed@pwc.com   10-12-2020       Initial Draft
**********************************************************************************************************************/
global class OB_UpdateCPStatusBatch implements Database.Batchable<sObject>,Schedulable  {

    /**********************************************************************************************
    * @Description  : Scheduler execute method to schedule code execution
    * @Params       : SchedulableContext        scheduling details
    * @Return       : none
    **********************************************************************************************/ 
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this,10);
    }

    /**********************************************************************************************
    * @Description  : Batch start method to define the context
    * @Params       : BatchableContext        batch context
    * @Return       : Query locator
    **********************************************************************************************/ 
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        // Get all the Commercial Permissions where Type of CP is Pocket Shops
        String queryString = 'Select id,End_Date__c,Start_Date__c,Status__c,Account__c,Account__r.Active_Leases__c from Commercial_Permission__c Where Account__r.OB_Type_of_commercial_permission__c=\'Gate Avenue Pocket Shops\'';
        system.debug(queryString);
        //if(test.isRunningTest()){queryString='select id from HexaBPM__Service_Request__c ';}
        return Database.getQueryLocator(queryString);
    }

    /**********************************************************************************************
    * @Description  : Batch execute method to process the records in current scope
    * @Params       : scope     records in current scope
    * @Return       : none
    **********************************************************************************************/ 
    global void execute(Database.BatchableContext bc, List<Commercial_Permission__c> scope){
        //Map<Id,Lease__c> accountLeaseMap = new Map<Id,Lease__c>();
        List<Commercial_Permission__c> CPList = new List<Commercial_Permission__c>();
        List<Account> accountList = new List<Account>();
        //Set<Id> accountIds = new Set<Id>();
        //Map<Id,Commercial_Permission__c> CPMap = new Map<Id,Commercial_Permission__c>();
        try{ 
            // for(Commercial_Permission__c objCP : scope){
            //     accountIds.add(objCP.Account__c);
            // }
            // for(Lease__c objLease : [Select id,Account__c from Lease__c where Account__c IN:accountIds]){
            //     accountLeaseMap.put(objLease.Account__c,objLease);
            // }
            // for(Commercial_Permission__c objCP : [Select id,End_Date__c,Start_Date__c,Status__c,Account__c from Commercial_Permission__c where Account__c IN:accountIds]){
            //     if(accountLeaseMap.containsKey(objCP.Account__c)){
            //         Lease__c objLease = accountLeaseMap.get(objCP.Account__c);
            //         /* Check if the current date of lease expiry or renewal is within the CP duration  */
            //         if(objCP.End_Date__c>System.Today()){
            //             if(objLease.Status__c=='Expired' && objLease.End_Date__c<System.Today() && objCP.Status__c!='Expired'){
            //                 CPList.add(new Commercial_Permission__c(Id=objCP.Id,Status__c='Expired'));
            //             }else if(objLease.Status__c=='Active' && objLease.End_Date__c>=System.Today() && objCP.Status__c!='Active'){
            //                 CPList.add(new Commercial_Permission__c(Id=objCP.Id,Status__c='Active'));
            //             }
            //         }    
            //     }
            // }
            for(Commercial_Permission__c objCP : scope){
                if(objCP.End_Date__c>=System.Today()){
                    if(objCP.Account__r.Active_Leases__c==0 && objCP.Status__c!='Expired'){
                        CPList.add(new Commercial_Permission__c(Id=objCP.Id,Status__c='Expired'));
                        accountList.add(new Account(id=objCP.Account__c,ROC_Status__c='CP-Expired'));
                    }else if(objCP.Account__r.Active_Leases__c>=1 && objCP.Status__c!='Active'){
                        CPList.add(new Commercial_Permission__c(Id=objCP.Id,Status__c='Active'));
                        accountList.add(new Account(id=objCP.Account__c,ROC_Status__c='CP-Active'));
                    }
                }
            }
            system.debug('CPList size===>'+CPList.size());
            system.debug('CPList ===>'+CPList);
            if(!CPList.isEmpty()) update CPList;   
            if(!accountList.isEmpty()) update accountList;   
        }catch (Exception e){  Log__c objLog = new Log__c(Description__c = 'Exception on OB_UpdateCPStatusBatch.execute() :'+e.getStackTraceString(),Type__c = 'Activate Deactivate CP');    insert objLog;}
    }

    /**********************************************************************************************
    * @Description  : Batch finish method
    * @Params       : BatchableContext  batch contect details
    * @Return       : none
    **********************************************************************************************/ 
    global void finish(Database.BatchableContext bc){}    
}