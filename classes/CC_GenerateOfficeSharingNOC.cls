/*
    Author      : Durga Prasad
    Date        : 30-Nov-2019
    Description : Custom code to create NOC of Office Sharing SR Doc
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_GenerateOfficeSharingNOC implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        system.debug('====CC_GenerateOfficeSharingNOC===');
        if(stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__SR_Template__c!=null){
            list<HexaBPM__SR_Doc__c> lstDocAddedThroughCode = new list<HexaBPM__SR_Doc__c>();
            for(HexaBPM__SR_Template_Docs__c SRTmpDoc:[Select Id,HexaBPM__Document_Master__c,HexaBPM__Group_No__c,HexaBPM__Document_Description__c,HexaBPM__Document_Master__r.Name,HexaBPM__Document_Description_External__c,HexaBPM__Generate_Document__c,HexaBPM__Added_through_Code__c,HexaBPM__Document_Master__r.HexaBPM__LetterTemplate__c from HexaBPM__SR_Template_Docs__c where HexaBPM__SR_Template__c=:stp.HexaBPM__SR__r.HexaBPM__SR_Template__c and HexaBPM__Added_through_Code__c=true and HexaBPM__Document_Master__r.HexaBPM__Code__c='NOC_SHARING']){
                HexaBPM__SR_Doc__c objSRDoc = new HexaBPM__SR_Doc__c();
                objSRDoc.Name = SRTmpDoc.HexaBPM__Document_Master__r.Name;
                objSRDoc.HexaBPM__Service_Request__c = stp.HexaBPM__SR__c;
                objSRDoc.HexaBPM__SR_Template_Doc__c = SRTmpDoc.Id;
                objSRDoc.HexaBPM__Status__c = 'Pending Upload';
                objSRDoc.HexaBPM__Document_Master__c = SRTmpDoc.HexaBPM__Document_Master__c;
                objSRDoc.HexaBPM__Group_No__c = SRTmpDoc.HexaBPM__Group_No__c;
                objSRDoc.HexaBPM__Is_Not_Required__c = true;
                objSRDoc.HexaBPM__Generate_Document__c = SRTmpDoc.HexaBPM__Generate_Document__c;
                objSRDoc.HexaBPM__Document_Description_External__c = SRTmpDoc.HexaBPM__Document_Description_External__c;
                objSRDoc.HexaBPM__Sys_IsGenerated_Doc__c = SRTmpDoc.HexaBPM__Generate_Document__c;
                objSRDoc.HexaBPM__Status__c = 'Generated';
                objSRDoc.HexaBPM__Letter_Template_Id__c = SRTmpDoc.HexaBPM__Document_Master__r.HexaBPM__LetterTemplate__c;
                string tempString  = string.valueof(Crypto.getRandomInteger());
                objSRDoc.HexaBPM__Sys_RandomNo__c = tempString.right(4);
                objSRDoc.HexaBPM__Unique_SR_Doc__c = SRTmpDoc.HexaBPM__Document_Master__r.Name+'_'+stp.HexaBPM__SR__c+'_Generate';
                objSRDoc.HexaBPM__From_Finalize__c = true;
                lstDocAddedThroughCode.add(objSRDoc);
            }
            try{
            	system.debug('==lstDocAddedThroughCode='+lstDocAddedThroughCode);
                if(lstDocAddedThroughCode.size()>0)
                    upsert lstDocAddedThroughCode;
                    
            	
            	List<HexaBPM__Service_Request__c> srAORUpdate = new List<HexaBPM__Service_Request__c>();
            	Map<String,String> SRDocMasterDocIdMap = new Map<String,String>();
            	for(HexaBPM__Service_Request__c Serv: [Select ID,
            										Sharing_Space_with_Affiliated_Entity__c,
            										Name_of_the_Hosting_Affiliate__c
            										From HexaBPM__Service_Request__c
            										Where HexaBPM__Parent_SR__c =: stp.HexaBPM__SR__c
            										Limit 1]){
            		Serv.Sharing_Space_with_Affiliated_Entity__c = stp.HexaBPM__SR__r.Sharing_Space_with_Affiliated_Entity__c;
            		Serv.Name_of_the_Hosting_Affiliate__c = stp.HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c;
            		srAORUpdate.add(Serv);
            		
            	}
            	if(srAORUpdate.size()>0)
            	update srAORUpdate;
            	
            	system.debug('===srAORUpdate==='+srAORUpdate);
            	if(srAORUpdate != null && srAORUpdate.size() != 0){
            		for(HexaBPM__SR_Doc__c docu: [Select Id,HexaBPM__Document_Master__c,
            										HexaBPM__Document_Master__r.HexaBPM__Code__c
            										From HexaBPM__SR_Doc__c
            										Where HexaBPM__Service_Request__c =: srAORUpdate[0].Id 
            										AND (HexaBPM__Document_Master__r.HexaBPM__Code__c='NOC_SHARING'
	            										or HexaBPM__Document_Master__r.HexaBPM__Code__c='Landlord_NOC') ]){
            			
        				SRDocMasterDocIdMap.put( docu.HexaBPM__Document_Master__r.HexaBPM__Code__c, docu.Id);
            			
            		}
            		system.debug('===SRDocMasterDocIdMap==='+SRDocMasterDocIdMap);
            		list<HexaBPM__SR_Doc__c> upsertSRDOC = new List<HexaBPM__SR_Doc__c>();
	            	for(HexaBPM__SR_Doc__c doc:[Select Id,Name,HexaBPM__Document_Master__c,HexaBPM__SR_Template_Doc__c,
	            								File_Name__c,HexaBPM_Amendment__c,HexaBPM__Customer__c,
	            								HexaBPM__Document_Description_External__c,HexaBPM__Status__c,
	            								HexaBPM__Doc_ID__c,HexaBPM__From_Finalize__c,
	            								HexaBPM__Sys_IsGenerated_Doc__c,HexaBPM__Is_Not_Required__c,
	            								HexaBPM__Group_No__c, HexaBPM__Generate_Document__c,
	            								HexaBPM__Letter_Template_Id__c,
	            								HexaBPM__Document_Master__r.HexaBPM__Code__c
	            								from HexaBPM__SR_Doc__c
	            								where HexaBPM__Service_Request__c=:stp.HexaBPM__SR__c
	            								and (HexaBPM__Document_Master__r.HexaBPM__Code__c='NOC_SHARING'
	            								or HexaBPM__Document_Master__r.HexaBPM__Code__c='Landlord_NOC')]){
	                    
	                    HexaBPM__SR_Doc__c objSRDocClone = doc.clone();
	                    objSRDocClone.HexaBPM__From_Finalize__c = true;
	                    objSRDocClone.HexaBPM__From_Finalize__c = true;
	                    objSRDocClone.HexaBPM__Service_Request__c = srAORUpdate[0].Id;
	                    objSRDocClone.In_Principle_Document__c = doc.Id;
	                    if(SRDocMasterDocIdMap != null && SRDocMasterDocIdMap.size() != 0 
	                    	&& SRDocMasterDocIdMap.containskey(doc.HexaBPM__Document_Master__r.HexaBPM__Code__c)){
	                    		
		                	objSRDocClone.Id = SRDocMasterDocIdMap.get(doc.HexaBPM__Document_Master__r.HexaBPM__Code__c);
		                	
	                	}
	                	if(doc.HexaBPM__Document_Master__r.HexaBPM__Code__c == 'NOC_SHARING'){
	                		objSRDocClone.HexaBPM__Generate_Document__c = true;
	                	}
	                	upsertSRDOC.add(objSRDocClone);
	                }
	                system.debug('=before==upsertSRDOC==='+upsertSRDOC);
	                if(upsertSRDOC.size()>0)
	                	upsert upsertSRDOC;
	                
	                system.debug('===upsertSRDOC==='+upsertSRDOC);
                }
                
            }catch(Exception e){
            	strResult = e.getMessage()+'';
            }
            system.debug('===strResult==='+strResult);
        }
        return strResult;
    }
}