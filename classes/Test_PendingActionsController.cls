@isTest(seeAllData=false)
private class Test_PendingActionsController {
    
    @testSetup static void setTestData() {
        
        //Test_FitoutServiceRequestUtils.init();
        Status__c pendingStatus = new Status__c(Name='Pending Customer Inputs',Code__c='PENDING_CUSTOMER_INPUTS');
        Status__c testStepStatus = new Status__c(Name='Approved',Code__c='APPROVED');
        
        Step_Template__c printPassTemplate = new Step_Template__c(Code__c = 'PRINT PASS',Name='Print Pass',Step_RecordType_API_Name__c = 'Print_Pass');
        
        insert printPassTemplate;
        
        Building__c testBuilding = Test_CC_FitOutCustomCode_DataFactory.getTestBuilding();
        
        insert testBuilding;
        
        Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
        
        insert objAccount;
        
        List<Unit__c> testUnits = Test_CC_FitOutCustomCode_DataFactory.getTestUnits(testBuilding.Id);
        Lease__c testLease = Test_CC_FitOutCustomCode_DataFactory.getTestLease(objAccount.Id);
        
        insert testUnits;
        insert testLease;
        
        SR_Status__c testSubmittedStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Submitted','SUBMITTED');
        SR_Status__c testDraftStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Draft','DRAFT');
        
        Lookup__c uaeLookup = Test_CC_FitOutCustomCode_DataFactory.getCountryLookup('101','United Arab Emirates','Nationality','AE');
        
        insert uaeLookup;
        
        List<CountryCodes__c> testCountryCodes = Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
        
        insert testCountryCodes;
        
        insert new List<SR_Status__c>{testSubmittedStatus,testDraftStatus};
        insert new List<Status__c>{testStepStatus,pendingStatus};
        
        Test_FitoutServiceRequestUtils.prepareSetup();
    }
    
    static testMethod void test_pendingActions(){
        PendingActionsController pendingActionsCont = new PendingActionsController();
        pendingActionsCont.Steps();
    }
    
    // Method Added by Shabbir Ahmed
    static testMethod void test_pendingActions2(){
        
        Group cuser;
          Group contractorQueue;
          
          System.runAs(new User(Id=UserInfo.getUserId())){
            
              cuser= new Group();
              cuser.name= 'Client Entry User';
              cuser.type='Queue';
              insert cuser;
              
              contractorQueue = new Group();
              contractorQueue.name= 'Contractor';
              contractorQueue.type='Queue';
              insert contractorQueue;
            
              QueueSobject CQueue = new QueueSObject(QueueId = cuser.Id, SobjectType = 'Step__c');
              insert CQueue;
              
              QueueSobject CQueue2 = new QueueSObject(QueueId = contractorQueue.Id, SobjectType = 'Step__c');
              insert CQueue2;
          }
        
        Profile testProfile = [SELECT Id 
                               FROM profile
                               WHERE Name = 'DIFC Customer Community User Custom' 
                               LIMIT 1];
   
            Account a = new Account(Name='Test Account Name' , ROC_Status__c='Active', Legal_Type_of_Entity__c='All;LLP;LTD;');
            insert a;
            
            
          
          Contact c = new Contact(FirstName='test1' , LastName = 'Contact Last Name', AccountId = a.id);
          insert c;
          
          Status__c Status = new Status__c();
          Status.Name = 'Pending Customer Inputs';
          Status.Code__c = 'Pending Customer Inputs';
          insert Status;
         
          User user = new User();
          
          user.ProfileID = testProfile.Id;
          user.EmailEncodingKey = 'ISO-8859-1';
          user.LanguageLocaleKey = 'en_US';
          user.TimeZoneSidKey = 'America/New_York';
          user.LocaleSidKey = 'en_US';
          user.FirstName = 'first';
          user.LastName = 'last';
          user.Username = 'test@appirio.com';   
          user.CommunityNickname = 'testUser123';
          user.Alias = 't1';
          user.Email = 'no@email.com';
          user.IsActive = true;
          user.ContactId = c.Id;
          user.Community_User_Role__c='Company Services;Employee Services;Listing Services;Event Services;Fit-Out Services;IT Services;Mortgage Registration';
          insert user;
          
          Status__c testStatus = [SELECT ID from Status__c WHERE Name='Pending Customer Inputs' LIMIT 1];
            
            Service_Request__c objSR  = new Service_Request__c();
            objSR.customer__c = a.Id;
            objSR.SR_Group__c = 'ROC';
            
            Service_Request__c objSR2  = new Service_Request__c();
            objSR2.customer__c = a.Id;
            objSR2.SR_Group__c = 'RORP';
            
            Service_Request__c objSR3  = new Service_Request__c();
            objSR2.customer__c = a.Id;
            objSR2.SR_Group__c = 'Listing';
            
            Service_Request__c objSR4 = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
            
            objSR4.Type_of_Request__c = 'Type B';
            objSR4.customer__c = a.Id;
            objSR4.SR_Group__c = 'Fit-Out & Events';
            objSR4.contractor__c= a.Id;
            objSR4.Building__c = null;
            objSR4.Summary__c = 'Fit_Out_Induction_Request';
            objSR4.Pre_GoLive__c = false;
            
            Service_Request__c objSR5  = new Service_Request__c();
            objSR2.customer__c = a.Id;
            objSR2.SR_Group__c = 'IT';
            
            List<Service_Request__c> srList = new List<Service_Request__c>{objSR,objSR2,objSR3,objSR4,objSR5};
            
            insert srList;
            
            srList = [SELECT ID FROM Service_Request__c WHERE CreatedDate = TODAY];
            
            Step__c objStep = new Step__c();
            objStep.SR__c = srList[0].Id;
            objStep.OwnerId = contractorQueue.Id;
            objStep.Step_notes__c = 'Test';
            objStep.Status__c = testStatus.Id;
            
            Step__c objStep2 = new Step__c();
            objStep2.SR__c = srList[1].Id;
            objStep2.OwnerId = cuser.Id;
            objStep2.Step_notes__c = 'Test 2';
            objStep2.Status__c = testStatus.Id;
            
            Step__c objStep3 = new Step__c();
            objStep2.SR__c = srList[2].Id;
            objStep2.OwnerId = contractorQueue.Id;
            objStep2.Step_notes__c = 'Test 3';
            objStep2.Status__c = testStatus.Id;
            
            Step__c objStep4 = new Step__c();
            objStep4.SR__c = srList[3].Id;
            objStep4.OwnerId = contractorQueue.Id;
            objStep4.Step_notes__c = 'Test 4';
            objStep4.Status__c = testStatus.Id;
            
            Step__c objStep5 = new Step__c();
            objStep4.SR__c = srList[4].Id;
            objStep4.OwnerId = contractorQueue.Id;
            objStep4.Step_notes__c = 'Test 4';
            objStep4.Status__c = testStatus.Id;
            
            insert new List<Step__c>{objStep,objStep2,objStep3,objStep4,objStep5};
          
        system.runAs(user) {
            
            Test.startTest();
            PendingActionsController pendingActionsCont = new PendingActionsController();
            pendingActionsCont.Steps();
            Test.stopTest();
        }
        
               
    }
    
    //Added by Kaavya
    static testMethod void test_pendingActions3(){
        
          Group cuser;
          Group contractorQueue;
          
          System.runAs(new User(Id=UserInfo.getUserId())){
            
              cuser= new Group();
              cuser.name= 'Client Entry User';
              cuser.type='Queue';
              insert cuser;
              
              contractorQueue = new Group();
              contractorQueue.name= 'Contractor';
              contractorQueue.type='Queue';
              insert contractorQueue;
            
              QueueSobject CQueue = new QueueSObject(QueueId = cuser.Id, SobjectType = 'Step__c');
              insert CQueue;
              
              QueueSobject CQueue2 = new QueueSObject(QueueId = contractorQueue.Id, SobjectType = 'Step__c');
              insert CQueue2;
          }
          
          Account a = new Account(Name='Test Account Name' , ROC_Status__c='Active', Legal_Type_of_Entity__c='All;LLP;LTD;');
          insert a;
          
          Account contr= new Account();
          contr.name='TestContractor';
          contr.Issuing_Authority__c='Dubai';
          contr.RORP_License_No__c='123';
          insert contr;
          
          Contact c1 = new Contact(FirstName='test1' , LastName = 'Contact Last Name', AccountId = a.id,contractor__c=contr.id,Role__c='Fit-Out Services;');
          insert c1;
          
          Profile testProfile = [SELECT Id 
                               FROM profile
                               WHERE Name = 'DIFC Contractor Community User Custom' 
                               LIMIT 1];
            
          User user1 = new User();
          user1.ProfileID = testProfile.Id;
          user1.EmailEncodingKey = 'ISO-8859-1';
          user1.LanguageLocaleKey = 'en_US';
          user1.TimeZoneSidKey = 'America/New_York';
          user1.LocaleSidKey = 'en_US';
          user1.FirstName = 'first';
          user1.LastName = 'last';
          user1.Username = 'test1@appirio.com';   
          user1.CommunityNickname = 'test1User123';
          user1.Alias = 't1';
          user1.Email = 'no@email.com';
          user1.IsActive = true;
          user1.ContactId = c1.Id;
          user1.Community_User_Role__c='Fit-Out Services;Property Services';
          insert user1;
          
          SR_Template__c testFitoutTemplate = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Fit - Out Service Request','Fit_Out_Service_Request','Fit-Out & Events');
            
            insert testFitoutTemplate;
            
            Service_Request__c objSR1 = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
            
            objSr1.SR_Template__c = testFitoutTemplate.Id;
            objSR1.Type_of_Request__c = 'Type B';
            objSR1.customer__c = contr.Id;
            objSR1.SR_Group__c = 'Fit-Out & Events';
            objSR1.contractor__c= contr.Id;
            objSR1.Building__c = null;
            objSr1.Summary__c = 'Fit_Out_Induction_Request';
            objSr1.Pre_GoLive__c = false;
            
            Test_CC_FitOutCustomCode_DataFactory.setToDraft(objSR1);
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(objSR1,'Fit_Out_Service_Request'); //Set the record type to 'Request Event Contractor Access;
            
            insert objSR1;
            
            SR_Steps__c testSrStep = new SR_Steps__c();
        
            testSrStep.Step_No__c = 82;
            testSrSTep.Assigned_to_client__c = true;
    
            insert testSrStep;
            
            Step__c objStep1 = new Step__c(); 
            objStep1.SR__c = objSR1.Id;
            objStep1.OwnerId = contractorQueue.Id;
            objStep1.Step_notes__c = 'Test';
            objStep1.Status__c = [SELECT Id FROM Status__c WHERE Name = 'Pending Customer Inputs' LIMIT 1].Id;
            objStep1.SR_Step__c = testSrSTep.Id;
            insert objStep1;
          
          system.runAs(user1) {
            
            Test.StartTest();
            
            /*Contact usrContact = [SELECT Id, Role__c FROM Contact WHERE Id =:objUser.ContactId];
            
            usrContact.Role__c = 'Company Services;Fit-Out Services;Property Services';
            usrContact.contractor__c = objUser.AccountId;
            
            update usrContact;*/
            
            PendingActionsController pendingActionsCont = new PendingActionsController();
            pendingActionsCont.Steps();
            
            System.debug('Access Type:' + pendingActionsCont.accessType);
            System.debug('ROCaccess:' + pendingActionsCont.ROCaccess);
            System.debug('GSaccess:' + pendingActionsCont.GSaccess);
            System.debug('ITaccess:' + pendingActionsCont.ITaccess);
            System.debug('PROPaccess:' + pendingActionsCont.PROPaccess);
            
            System.debug('stepmap:' + pendingActionsCont.stepmap);
            System.debug('ListingAccess:' + pendingActionsCont.ListingAccess);
            System.debug('FitOutAccess:' + pendingActionsCont.FitOutAccess);
            System.debug('isEvent:' + pendingActionsCont.isEvent);
            
            Test.stopTest();
        }  
    }
    
    //Added by Kaavya
    static testMethod void test_pendingActions4(){
        
          Group cuser;
          Group contractorQueue;
          
          System.runAs(new User(Id=UserInfo.getUserId())){
            
              cuser= new Group();
              cuser.name= 'Client Entry User';
              cuser.type='Queue';
              insert cuser;
              
              contractorQueue = new Group();
              contractorQueue.name= 'Contractor';
              contractorQueue.type='Queue';
              insert contractorQueue;
            
              QueueSobject CQueue = new QueueSObject(QueueId = cuser.Id, SobjectType = 'Step__c');
              insert CQueue;
              
              QueueSobject CQueue2 = new QueueSObject(QueueId = contractorQueue.Id, SobjectType = 'Step__c');
              insert CQueue2;
          }
          
          Test_FitoutServiceRequestUtils.useDefaultUser();
          Test_FitoutServiceRequestUtils.setToInternalUser();
          Test_FitoutServiceRequestUtils.prepareTestUser(true);
          
          Account a = new Account(Name='Test Account Name' , ROC_Status__c='Active', Legal_Type_of_Entity__c='All;LLP;LTD;');
          insert a;
          
          Account contr= new Account();
          contr.name='TestContractor';
          contr.Issuing_Authority__c='Dubai';
          contr.RORP_License_No__c='123';
          insert contr;
          
          SR_Template__c testFitoutTemplate = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Fit - Out Service Request','Fit_Out_Service_Request','Fit-Out & Events');
            
            insert testFitoutTemplate;
            
            Service_Request__c objSR1 = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
            
            objSr1.SR_Template__c = testFitoutTemplate.Id;
            objSR1.Type_of_Request__c = 'Type B';
            objSR1.customer__c = a.Id;
            objSR1.SR_Group__c = 'Fit-Out & Events';
            objSR1.contractor__c= contr.Id;
            objSR1.Building__c = null;
            objSr1.Summary__c = 'Fit_Out_Induction_Request';
            objSr1.Pre_GoLive__c = false;
            
            Test_CC_FitOutCustomCode_DataFactory.setToDraft(objSR1);
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(objSR1,'Fit_Out_Service_Request'); //Set the record type to 'Request Event Contractor Access;
            
            insert objSR1;
            
            SR_Steps__c testSrStep = new SR_Steps__c();
        
            testSrStep.Step_No__c = 82;
            testSrSTep.Assigned_to_client__c = true;
    
            insert testSrStep;
            
            Step__c objStep1 = new Step__c(); 
            objStep1.SR__c = objSR1.Id;
            objStep1.OwnerId = cuser.Id;
            objStep1.Step_notes__c = 'Test';
            objStep1.Status__c = [SELECT Id FROM Status__c WHERE Name = 'Pending Customer Inputs' LIMIT 1].Id;
            objStep1.SR_Step__c = testSrSTep.Id;
            insert objStep1;
            
          system.runAs(new User(Id=Userinfo.getUserId())) {
            
            Test.StartTest();
            
            PendingActionsController pendingActionsCont = new PendingActionsController();
            pendingActionsCont.Steps();
            
            Test.stopTest();
        }  
    }
    
    
    static testmethod void test_pendingActions5(){
    
    
          Account a = new Account(Name='Test Account Name' , ROC_Status__c='Active', Legal_Type_of_Entity__c='All;LLP;LTD;');
            insert a;
            
            
          
          Contact c = new Contact(FirstName='test1' , LastName = 'Contact Last Name', AccountId = a.id);
          insert c;
          
          Status__c Status = new Status__c();
          Status.Name = 'Pending Customer Inputs';
          Status.Code__c = 'Pending Customer Inputs';
          insert Status;
    
    
         Profile testProfile = [SELECT Id 
                               FROM profile
                               WHERE Name = 'DIFC Mortgage Community User' 
                               LIMIT 1];
            
          User user1 = new User();
          user1.ProfileID = testProfile.Id;
          user1.EmailEncodingKey = 'ISO-8859-1';
          user1.LanguageLocaleKey = 'en_US';
          user1.TimeZoneSidKey = 'America/New_York';
          user1.LocaleSidKey = 'en_US';
          user1.FirstName = 'first';
          user1.LastName = 'last';
          user1.Username = 'test1@appirio.com';   
          user1.CommunityNickname = 'test1User123';
          user1.Alias = 't1';
          user1.Email = 'no@email.com';
          user1.IsActive = true;
          user1.ContactId = c.Id;
          user1.Community_User_Role__c='Mortgage Registration';
          insert user1;
          
          Service_Request__c objSR  = new Service_Request__c();
            objSR.customer__c = a.Id;
            objSR.SR_Group__c = 'RORP';
            
            insert objSR;
            
            Step__c objStep = new Step__c();
            objStep.SR__c = objSR.Id;
            objStep.OwnerId =user1.Id;
            objStep.Step_notes__c = 'Test';
            objStep.Status__c = Status.Id;
            
            
            insert objStep;
            
            system.runAs(user1) {
            
            Test.startTest();
            PendingActionsController pendingActionsCont = new PendingActionsController();
            pendingActionsCont.Steps();
            Test.stopTest();
        }
        
    }
    
    
}