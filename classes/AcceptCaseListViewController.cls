/*
    Author      : Rajil Ravindran
    Company     : PwC
    Date        : 02-May-2019
    Description : Gets the selected case records from the list view and update the status to invalid and update them.
    -------------------------------------------------------------------------
*/
public with sharing class AcceptCaseListViewController 
{
    private ApexPages.StandardSetController standardController;
    public Set<Id> caseIds = new Set<Id>();
    public AcceptCaseListViewController(ApexPages.StandardSetController standardController)
    {
        this.standardController = standardController;
        caseIds = new Set<Id>();
    }
    public PageReference acceptCases()
    {       
        // Get the selected records (optional, you can use getSelected to obtain ID's and do your own SOQL)
        List<Case> selectedCases = (List<Case>) standardController.getSelected();

        // Update records       
        for(Case selectedCase : selectedCases)
        {
            selectedCase.OwnerId = Userinfo.getUserId();
            selectedCase.status = 'In Progress';
            caseIds.add(selectedCase.Id);
        }   
        system.debug('##CASE: '+selectedCases);
        return null;        
    }
    public PageReference updateCases()
    {       
        Boolean hasAcceptError = false;
        Boolean hasInvalidError = false;
        Set<String> acceptedCases = new Set<String>();
        Set<String> invalidCases = new Set<String>();
        for(Case objCase : [SELECT Id,OwnerId,Status,CaseNumber FROM Case WHERE Id IN :caseIds]){
            String ownerStr = objCase.OwnerId;
            if(ownerStr != '' && ownerStr.subString(0,3) == '005'){
                hasAcceptError = true;
                acceptedCases.add(objCase.CaseNumber);
            }
            if(objCase.Status == 'Invalid'){
                hasInvalidError = true;
                invalidCases.add(objCase.CaseNumber);
            }
        }
        
        if(hasAcceptError == true){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Case Number(s): '+acceptedCases +' have already been accepted. Please re-select the records.');ApexPages.addMessage(msg);
            return null;
        }
        
        if(hasInvalidError == true){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Case Number(s): '+invalidCases +' have already been marked as invalid. Please re-select the records.');ApexPages.addMessage(msg);
            return null;
        }
        
        if(hasAcceptError == false && hasInvalidError == false){
            // Call StandardSetController 'save' method to update (optional, you can use your own DML)
            try{
                return standardController.save();   
            }
            catch(Exception ex){ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage());ApexPages.addMessage(msg);
                return null;
            }
        }
        
        return null;
    }
}