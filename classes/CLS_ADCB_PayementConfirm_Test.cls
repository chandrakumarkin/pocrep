@isTest(seealldata=false)
private class CLS_ADCB_PayementConfirm_Test{
    
    public static Account objAccount;
    public static SR_Template__c objTemplate;
    public static Contact objContact;
    public static Service_Request__c objSR;
    public static Service_Request__c objSR2;
    public static List<Step__c> objStep2;
    public static List<Amendment__c> objAmd;
    
    @testsetup
    static void initializaData(){
        Test.startTest();
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insertNewAccounts[0].Name = 'Samayra Public Limited Company PLC';
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        //insertNewAccounts[0].Next_Renewal_Date__c = Date.today();
        insert insertNewAccounts;
        
        
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile Where Name='DIFC Customer Community Plus User Custom' Limit 1];
        User user1 = new User(
            Username = System.now().millisecond() + 'test12345@test.com',
            ContactId = con.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'portaltest12345@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'PortalTestUser',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
        
        Test.stopTest();
    }
    
    public static void testData(){
        //objAccount = new Account();
        objStep2 = new List<Step__c>();
        //objAccount.Name = 'Test Account';
        objAmd = new List<Amendment__c>();
        //insert objAccount;
        objAccount = [Select Id,name From Account Where Legal_Type_of_Entity__c ='Public Company' LIMIT 1];
        System.debug('zzz objAccount'+objAccount);
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        /*objContact = new Contact();
objContact.FirstName = 'Test Contact';
objContact.LastName = 'Test Contact';
objContact.AccountId = objAccount.Id;
objContact.Email = 'test@difc.com';
insert objContact;*/
        
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Email__c = 'testsr@difc.com';
        // objSR.Contact__c = objContact.Id;
        objSR.SR_Template__c = objTemplate.Id;
        objSR.Bank_Name__c = 'Yes';
        objSR.Sys_Estimated_Share_Capital__c=100.00;
        objSR.Entity_Name__c = 'Samyra PLC';
        insert objSR;
        
        objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount.Id;
        objSR2.Email__c = 'testsr@difc.com';
        // objSR.Contact__c = objContact.Id;
        objSR2.SR_Template__c = objTemplate.Id;
        objSR2.Entity_Name__c = 'Samyra Two PLC';
        insert objSR2;
        
        Amendment__c objAmd1 = new Amendment__c();
        objAmd1.ServiceRequest__c = objSR.Id;
        objAmd1.Amendment_Type__c = 'Unit';
        objAmd1.PO_Box__c = '12345';
        objAmd1.First_Name__c = 'Test';
        objAmd1.Last_Name__c = 'Test';
        objAmd1.sys_create__c = false;
        objAmd1.Status__c = 'New';
        objAmd1.Nominal_Value__c=100.00;
        Insert objAmd1;
        
        Amendment__c objAmd2 = new Amendment__c();
        objAmd2.ServiceRequest__c = objSR.Id;
        objAmd2.Amendment_Type__c = 'Tenant';
        objAmd2.PO_Box__c = '12345';
        objAmd2.First_Name__c = 'Test';
        objAmd2.Last_Name__c = 'Test';
        objAmd2.sys_create__c = false;
        objAmd2.Status__c = 'New';
        objAmd2.Nominal_Value__c=100.00;
        Insert objAmd2;
        
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration','Body_Corporate_Tenant','Landlord')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        boolean hasPrimary =true;
        
        list<Amendment__c> lstAmdendments = new list<Amendment__c>();
        
        
        
        
        /***
Receipt__c ObjRec=new Receipt__c();
ObjRec.Receivable_Type__c='Lease Security Deposit';
ObjRec.Amount__c=ObjAmd.Nominal_Value__c;
ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
ObjRec.Customer__c=ObjSR.Customer__c;
ObjRec.Receipt_Type__c='Card';
ObjRec.Amendment__c=ObjAmd.Id;
ObjRec.Tenant__c=ListAmd[0].id;
ObjRec.Service_Request__c=ObjSR.id;
************/
        step_Template__c stpTemp1 = new Step_template__c(Name='Security Deposit Payment',Step_RecordType_API_Name__c='SECURITY_DEPOSIT_PAYMENT',Code__c='SECURITY_DEPOSIT_PAYMENT');
        insert stpTemp1;
        
        objStep2 = new List<step__c>();
        step__c verfStep = new step__c();
        verfStep.sr__c = objSR.id;
        verfStep.Step_Template__c = stpTemp1.id;
        //verfStep.status__c = stepStatus.id;
        objStep2.add(verfStep);
    }
    
    
    
    
    
    static testMethod void myUnitTest333(){
        Test.startTest();
        testData();
        CLS_ADCB_PayementConfirm obj =new CLS_ADCB_PayementConfirm();
        string SId = objSR.Id;
        Map<String,String> tempParameter = new Map<String,String>();
        tempParameter.put('transaction_id','transaction_id');
        tempParameter.put('score_card_scheme','score_card_scheme');
        tempParameter.put('message','message');
        tempParameter.put('decision','ACCEPT');
        obj.updateServiceRequest(objSR.Id,tempParameter);
        obj.updateReceipt(); 
        Test.stopTest();
    }
    /*

static testMethod void myUnitTest2(){
testData();

PageReference pageRef = Page.PaySecurityDeposit;
Test.setCurrentPage(pageRef);
system.currentPageReference().getParameters().put('id',objSR.Id);
Apexpages.currentPage().getParameters().put('id',objSR.Id);
PaySecurityDepositcontroller obj =new PaySecurityDepositcontroller();
obj.PageLoad();
obj.getSignedData();
//obj.sign('data','assf32432');
Map<string,string> paramsArray = new Map<string,string>();
paramsArray.put('signed_field_names','signed_field_names');
//obj.buildDataToSign(paramsArray);
obj.getSignedData();
obj.getParametersValuesHidden();
//obj.getUTCDateTime();
obj.getUUID();
}
static testMethod void myUnitTest3(){
testData();

}
static testMethod void myUnitTest4(){
//testData();
//CLS_ADCB_Payment.Paymentformsubmit(objAccount.Id,1000.00,objSR.Id);
}
static testMethod void myUnitTest5(){
testData();
PageReference pageRef = Page.PaySecurityDeposit;
Test.setCurrentPage(pageRef);
system.currentPageReference().getParameters().put('id',objSR.Id);
Apexpages.currentPage().getParameters().put('id',objSR.Id);
clsLeaseRegistration_Security_BP obj = new clsLeaseRegistration_Security_BP();
obj.Pageload();
}
*/
    
    static testMethod void testAdcbPG1(){
        
        User puser = [Select Id,Name,Contact.AccountId From User Where LastName='PortalTestUser' OR Email='portaltest12345@test.com' LIMIT 1];
        Test.startTest();
        System.runAs(puser){
            testData();
            Receipt__c ObjRec=new Receipt__c();
            ObjRec.Receivable_Type__c='Lease Security Deposit';
            ObjRec.Amount__c= 100;
            ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
            ObjRec.Customer__c=ObjSR.Customer__c;
            ObjRec.Receipt_Type__c='Card';
            ObjRec.Payment_Status__c = 'Pending';
            ObjRec.Receipt_Type__c = 'Card';
            ObjRec.Transaction_Date__c = System.now();
            insert ObjRec;
            
            /*
PageReference pageRef = Page.ADCB_PayementConfirm;
Test.setCurrentPage(pageRef);
system.currentPageReference().getParameters().put('req_reference_number',ObjRec.Id+'_TOPUP');
Apexpages.currentPage().getParameters().put('req_reference_number',ObjRec.Id+'_TOPUP');*/
            
            CLS_ADCB_PayementConfirm obj =new CLS_ADCB_PayementConfirm();
            string SId = objSR.Id;
            Map<String,String> tempParameter = new Map<String,String>();
            tempParameter.put('transaction_id','transaction_id');
            tempParameter.put('score_card_scheme','score_card_scheme');
            tempParameter.put('req_reference_number',ObjRec.Id+'_TOPUP');//Receipt__c
            tempParameter.put('message','message');
            tempParameter.put('decision','ACCEPT');
            tempParameter.put('card_type_name','VISA');
            obj.Parameter = tempParameter;
            //obj.updateServiceRequest(objSR.Id,tempParameter);
            obj.updateReceipt();
        }
        Test.stopTest();
        
    }
    
    static testMethod void testAdcbPG2(){
        
        User puser = [Select Id,Name,Contact.AccountId From User Where LastName='PortalTestUser' OR Email='portaltest12345@test.com' LIMIT 1];
        Test.startTest();
        System.runAs(puser){
            testData();
            Receipt__c ObjRec=new Receipt__c();
            ObjRec.Receivable_Type__c='Lease Security Deposit';
            ObjRec.Amount__c= 100;
            ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
            ObjRec.Customer__c=ObjSR.Customer__c;
            ObjRec.Receipt_Type__c='Card';
            ObjRec.Payment_Status__c = 'Pending';
            ObjRec.Receipt_Type__c = 'Card';
            ObjRec.Transaction_Date__c = System.now();
            ObjRec.Service_Request__c = objSR.Id;
            insert ObjRec;
            
            /*
PageReference pageRef = Page.ADCB_PayementConfirm;
Test.setCurrentPage(pageRef);
system.currentPageReference().getParameters().put('req_reference_number',ObjRec.Id+'_TOPUP');
Apexpages.currentPage().getParameters().put('req_reference_number',ObjRec.Id+'_TOPUP');*/
            
            CLS_ADCB_PayementConfirm obj =new CLS_ADCB_PayementConfirm();
            string SId = objSR.Id;
            Map<String,String> tempParameter = new Map<String,String>();
            tempParameter.put('transaction_id','transaction_id');
            tempParameter.put('score_card_scheme','score_card_scheme');
            tempParameter.put('req_reference_number',objSR.Id+'_'+ObjRec.Id+'_CPSR');//Receipt__c
            tempParameter.put('message','message');
            tempParameter.put('decision','ACCEPT');
            tempParameter.put('card_type_name','VISA');
            
            obj.Parameter = tempParameter;
            //obj.updateServiceRequest(objSR.Id,tempParameter);
            obj.updateReceipt();
        }
        Test.stopTest();
        
    }
    
    static testMethod void testAdcbPG3(){
        
        User puser = [Select Id,Name,Contact.AccountId From User Where LastName='PortalTestUser' OR Email='portaltest12345@test.com' LIMIT 1];
        Test.startTest();
        System.runAs(puser){
            testData();
            Receipt__c ObjRec=new Receipt__c();
            ObjRec.Receivable_Type__c='Lease Security Deposit';
            ObjRec.Amount__c= 100;
            ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
            ObjRec.Customer__c=ObjSR.Customer__c;
            ObjRec.Receipt_Type__c='Card';
            ObjRec.Payment_Status__c = 'Pending';
            ObjRec.Receipt_Type__c = 'Card';
            ObjRec.Transaction_Date__c = System.now();
            ObjRec.Service_Request__c = objSR.Id;
            insert ObjRec;
            
            /*
PageReference pageRef = Page.ADCB_PayementConfirm;
Test.setCurrentPage(pageRef);
system.currentPageReference().getParameters().put('req_reference_number',ObjRec.Id+'_TOPUP');
Apexpages.currentPage().getParameters().put('req_reference_number',ObjRec.Id+'_TOPUP');*/
            
            CLS_ADCB_PayementConfirm obj =new CLS_ADCB_PayementConfirm();
            string SId = objSR.Id;
            Map<String,String> tempParameter = new Map<String,String>();
            tempParameter.put('transaction_id','transaction_id');
            tempParameter.put('score_card_scheme','score_card_scheme');
            tempParameter.put('req_reference_number',objSR.Id+'_'+ObjRec.Id);
            tempParameter.put('message','message');
            tempParameter.put('decision','ACCEPT');
            tempParameter.put('card_type_name','VISA');
            
            obj.Parameter = tempParameter;
            
            obj.updateReceipt();
            
            obj.updateServiceRequest(objSR.Id,tempParameter);
        }
        Test.stopTest();
        
    }
    
    static testMethod void testAdcbPG4(){
        
        User puser = [Select Id,Name,Contact.AccountId From User Where LastName='PortalTestUser' OR Email='portaltest12345@test.com' LIMIT 1];
        Test.startTest();
        System.runAs(puser){
            testData();
            Receipt__c ObjRec=new Receipt__c();
            ObjRec.Receivable_Type__c='Lease Security Deposit';
            ObjRec.Amount__c= 100;
            ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
            ObjRec.Customer__c=ObjSR.Customer__c;
            ObjRec.Receipt_Type__c='Card';
            ObjRec.Payment_Status__c = 'Pending';
            ObjRec.Receipt_Type__c = 'Card';
            ObjRec.Transaction_Date__c = System.now();
            insert ObjRec;
            
            /*
PageReference pageRef = Page.ADCB_PayementConfirm;
Test.setCurrentPage(pageRef);
system.currentPageReference().getParameters().put('req_reference_number',ObjRec.Id+'_TOPUP');
Apexpages.currentPage().getParameters().put('req_reference_number',ObjRec.Id+'_TOPUP');*/
            
            CLS_ADCB_PayementConfirm obj =new CLS_ADCB_PayementConfirm();
            string SId = objSR.Id;
            Map<String,String> tempParameter = new Map<String,String>();
            tempParameter.put('transaction_id','transaction_id');
            tempParameter.put('score_card_scheme','score_card_scheme');
            tempParameter.put('req_reference_number',ObjRec.Id);
            tempParameter.put('message','message');
            tempParameter.put('decision','FAILED');
            tempParameter.put('card_type_name','VISA');
            
            obj.Parameter = tempParameter;
            obj.updateReceipt(); //Failed response for receipt

        }
        Test.stopTest();
        
    }
    
    static testMethod void testAdcbPG5(){
        
        User puser = [Select Id,Name,Contact.AccountId From User Where LastName='PortalTestUser' OR Email='portaltest12345@test.com' LIMIT 1];
        Test.startTest();
        System.runAs(puser){
            testData();
            Receipt__c ObjRec=new Receipt__c();
            ObjRec.Receivable_Type__c='Lease Security Deposit';
            ObjRec.Amount__c= 100;
            ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
            ObjRec.Customer__c=ObjSR.Customer__c;
            ObjRec.Receipt_Type__c='Card';
            ObjRec.Payment_Status__c = 'Pending';
            ObjRec.Receipt_Type__c = 'Card';
            ObjRec.Transaction_Date__c = System.now();
            insert ObjRec;
            
            /*
                PageReference pageRef = Page.ADCB_PayementConfirm;
                Test.setCurrentPage(pageRef);
                system.currentPageReference().getParameters().put('req_reference_number',ObjRec.Id+'_TOPUP');
                Apexpages.currentPage().getParameters().put('req_reference_number',ObjRec.Id+'_TOPUP');
            */
            
            CLS_ADCB_PayementConfirm obj =new CLS_ADCB_PayementConfirm();
            string SId = objSR.Id;
            Map<String,String> tempParameter = new Map<String,String>();
            tempParameter.put('transaction_id','transaction_id');
            tempParameter.put('score_card_scheme','score_card_scheme');
            tempParameter.put('req_reference_number',ObjSR.Id);
            tempParameter.put('message','message');
            tempParameter.put('decision','FAILED');
            tempParameter.put('card_type_name','VISA');
            
            obj.Parameter = tempParameter;
            obj.updateReceipt(); //Failed response for SR's

        }
        Test.stopTest();
        
    }
    
    static testMethod void testNewPaymentController(){
        
        User puser = [Select Id,Name,Contact.AccountId From User Where LastName='PortalTestUser' OR Email='portaltest12345@test.com' LIMIT 1];
        Test.startTest();
        System.runAs(puser){
            testData();
           /* Receipt__c ObjRec=new Receipt__c();
            ObjRec.Receivable_Type__c='Lease Security Deposit';
            ObjRec.Amount__c= 100;
            ObjRec.Recordtypeid=Schema.SObjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
            ObjRec.Customer__c=ObjSR.Customer__c;
            ObjRec.Receipt_Type__c='Card';
            ObjRec.Payment_Status__c = 'Pending';
            ObjRec.Receipt_Type__c = 'Card';
            ObjRec.Transaction_Date__c = System.now();
            insert ObjRec;
           */ 
            PageReference myVfPage = Page.NewPayment;
            Test.setCurrentPage(myVfPage);
            
            ApexPages.currentPage().getParameters().put('amountToPay','1000');
            NewPaymentController  obj =new NewPaymentController ();
            obj.setupPGForm();
            obj.getSignedData();
        }
        Test.stopTest();
    }
    
}