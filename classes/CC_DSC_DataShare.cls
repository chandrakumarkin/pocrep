/*********************************************************************************************************************
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    09-Oct-2019 Arun        Corrected for FRC companies      
 
**********************************************************************************************************************/

public without sharing class CC_DSC_DataShare
{

 public void PushDateTODSC(string  AccountId)
 {
 
 sendAccountsToDsc([SELECT Id,
                      Name,                                                 // Company Name in English
                      Arabic_Name__c,                                       // Company Name in Arabic
                      Company_Type__c,                                      // Company Type
                      Office__c,                                            // Address - Office No.
                      Building_Name__c,                                     // Address - Building Name
                      Registration_License_No__c,                           // License No
                      Liquidated_Date__c,                                   // Liquidation Date - non-financial / retail
                      DFSA_Withdrawn_Date__c,                               // Withdrawn Date - financial
                      Legal_Type_of_Entity__c,                              // Legal entity (internal)
                      (select id, Type__c,Formula_Activity_Name_Eng__c FROM License_Activities__r where Include_Activity__c = true),        // Activities //v1.1
                      Active_License__r.License_Issue_Date__c,              // License Iddue Date
                      Active_License__r.License_Expiry_Date__c,             // License Expiry Date
                      Active_License__r.Status__c,                          // License Status 
                      Sector_Classification__c,                             // License Type
                      PO_Box__c,                                            // PO Box
                      Phone,                                                // COmpany Tel No
                      ROC_Status__c 
                      FROM Account WHERE Registration_License_No__c != '' and Id=:AccountId]);
                      
 }
  public void sendAccountsToDsc(List<Account> accountRecords)
  {
        
        Set<Id> accountIds = new Set<Id>();
        
             List<string> Rel=new List<string>();
            Rel.add('Is Shareholder Of');
            Rel.add('has founder');
            Rel.add('Has Founding Member');
            Rel.add('Has General Partner');
            Rel.add('Has Member');
            Rel.add('Has Limited Partner');
            Rel.add('Has Service Document Responsible for');
            
        
        for(Account a : accountRecords){accountIds.add(a.Id);}
        
        try {
            
            Map<Id,String> infoCountMap = getCurrentEmployeeCount(accountIds);
            
           
            
            Map<Id,List<Relationship__c>> MapRela=getRelationships(accountIds,Rel);
            list<servicesDscGovAeFreezonesintegratio.License> lstCompanyLicenses = new list<servicesDscGovAeFreezonesintegratio.License>();
            for(Account a : accountRecords)//Start : Compnay Data 
            {
                //ShareHolder Detail 
                
                 list<servicesDscGovAeFreezonesintegratio.Shareholder> lstCompanyShareholders = new list<servicesDscGovAeFreezonesintegratio.Shareholder>();
                if(MapRela!=null)
                {
                if(MapRela.get(a.id)!=null)
                for(Relationship__c Objrel:MapRela.get(a.id))
                {
                    servicesDscGovAeFreezonesintegratio.Shareholder Shar=new servicesDscGovAeFreezonesintegratio.Shareholder();
                    Shar.ShareholderName=Objrel.Relationship_Name__c;
                    Shar.ShareHolderCode=Objrel.Name;
                    Shar.Gender=Objrel.Gender__c;
                    Shar.RegistrationDate= String.valueOf(Objrel.Start_Date__c);
                    system.debug('==Shar=='+Shar.RegistrationDate);
                    lstCompanyShareholders.add(Shar);
                
                }
                }
                
            
            
            
                //End ShareHolder Detail
    
                list<servicesDscGovAeFreezonesintegratio.EconomicActivity> lstEcnomicActivity = new list<servicesDscGovAeFreezonesintegratio.EconomicActivity>();
    
                if(!a.License_Activities__r.isEmpty()){
                    
                    for(License_Activity__c l : a.License_Activities__r){
                        
                        servicesDscGovAeFreezonesintegratio sc = new servicesDscGovAeFreezonesintegratio();
    
                        servicesDscGovAeFreezonesintegratio.EconomicActivity ecnomicActivity = new servicesDscGovAeFreezonesintegratio.EconomicActivity();
                        
                        ecnomicActivity.ActivityDescription = l.Formula_Activity_Name_Eng__c; // type of activity
                        ecnomicActivity.ActivityCode = l.Type__c;
                        //ecnomicActivity.ActivityRevenue = '0';
                        
                        lstEcnomicActivity.add(ecnomicActivity);
                        
                    }
                    
                }
                
                list<servicesDscGovAeFreezonesintegratio.Shareholder> listShareHolders = new list<servicesDscGovAeFreezonesintegratio.Shareholder>();
                
                servicesDscGovAeFreezonesintegratio.License companyALicense = new servicesDscGovAeFreezonesintegratio.License();
                
                companyALicense.LicenseNo = a.Registration_License_No__c;
                companyALicense.LicenseNameAr = a.Arabic_Name__c;
                companyALicense.LicenseNameEn = a.Name;
                companyALicense.LicenseStatus = a.Active_License__r.Status__c;
                companyALicense.LicenseType = a.Sector_Classification__c;
                companyALicense.LegalEntity = a.Legal_Type_of_Entity__c;
                companyALicense.ZoneName = 'DIFC';
                companyALicense.EconomicActivities = lstEcnomicActivity;
                companyALicense.TotalEmployment = infoCountMap.get(a.Id); 
                
                companyALicense.LicenseCancelDate = a.Company_Type__c.equals('Financial - related') ? (a.DFSA_Withdrawn_Date__c != null ? String.valueOf(a.DFSA_Withdrawn_Date__c) : '') :
                                                    (a.Liquidated_Date__c != null ? String.valueOf(a.Liquidated_Date__c) : '');  
                 
                companyALicense.LicenseIssueDate = String.valueOf(a.Active_License__r.License_Issue_Date__c);
                companyALicense.LicenseExpiryDate = String.valueOf(a.Active_License__r.License_Expiry_Date__c);

                companyALicense.CommunityName='ZAABEEL SECOND';
                companyALicense.CommunityNumber='337';
                companyALicense.ShareHolders = lstCompanyShareholders;
                
                lstCompanyLicenses.add(companyALicense);
                
            }   
            //End : Compnay Data 
            servicesDscGovAeFreezonesintegratio.LicensesList_element liscenesList = new servicesDscGovAeFreezonesintegratio.LicensesList_element();
                
            liscenesList.Licenses = lstCompanyLicenses;
            liscenesList.Period = Datetime.valueOf(Label.DSCDIFC_Period); 
            //liscenesList.Token = '80EC52FB-F50F-465E-992E-25A0064331C3';UAT 
             liscenesList.Token = PasswordCryptoGraphy.DecryptPassword(Label.DSCDIFC_Password);
            
            tempuriOrg.BasicHttpBinding_IFZLicenseService tempURI = new tempuriOrg.BasicHttpBinding_IFZLicenseService();
        
            servicesDscGovAeFreezonesintegratio.ReplyMessage_element response_x = new servicesDscGovAeFreezonesintegratio.ReplyMessage_element();
            
            response_x = tempURI.UpdateLicenses(liscenesList);
            
            system.debug('response_x ' + response_x);
            
            insert LogDetails.CreateLog(null, 'ROC: Schedule Batch License Job', String.valueOf(response_x));
            
        } catch (Exception e){
            
            insert LogDetails.CreateLog(null, 'ROC: Schedule Batch License Job', 'Line Number : '+e.getLineNumber()+'\nException is : '+e);
                            
        }
    }
    
    public Map<Id,List<Relationship__c>> getRelationships(set<Id> clientIds,List<string> RelCode)
    {
        Map<id,List<Relationship__c>> RelMap=new Map<id,List<Relationship__c>>();
        
        for(Relationship__c ObjRel:[select name,Subject_Account__c,Relationship_Name__c,Start_Date__c,Relationship_Nationality__c,Gender__c from Relationship__c where 
        Subject_Account__c in : clientIds and Active__c=true and Relationship_Type__c in :RelCode])
        {
            List<Relationship__c> TempRel;
            if(RelMap.containsKey(ObjRel.Subject_Account__c))
            {
                TempRel=RelMap.get(ObjRel.Subject_Account__c);
            }
            else
                TempRel=new List<Relationship__c>();
            
            TempRel.add(ObjRel);
            RelMap.put(ObjRel.Subject_Account__c,TempRel);
        }
            
    //License No=?
    //Shareholder Name? Relationship_Name__c
    //Shareholder Code?name
    //Emirates ID Number ?
    //Gender?Gender__c
    //Registration/Creation Date ?Start_Date__c 
    //PermanentResidance
    //Nationality
    //PercentageOfContributionToCapi
    return RelMap;
    }
    private Map<Id,String> getCurrentEmployeeCount(set<Id> clientIds)
    {
        
        Map<Id,String> infoCountMap = new Map<Id,String>();
        
        Map<Id,Integer> infoCountMapNum = new Map<Id,Integer>();
        
        for(Id clientId : clientIds){
            infoCountMapNum.put(clientId,0);
        } 
        
        for(Contact c : [select Id, (select Id, 
                                          Active__c,
                                          Object_Contact__c,
                                          Subject_Account__c,
                                          Relationship_Type__c from Secondary_Contact__r where 
                                          Subject_Account__c IN :clientIds AND Active__c = true AND 
                                          (Relationship_Type__c='Has DIFC Sponsored Employee' OR (Document_Active__c = true AND 
                                            (Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee') AND End_Date__c > TODAY ) ) AND 
                                             object_contact__c != NULL AND
                                             Object_Contact__r.RecordType.DeveloperName = 'GS_Contact') FROM Contact WHERE 
                                             RecordType.DeveloperName = 'GS_Contact' AND ID in 
                                             (select Object_Contact__c from Relationship__c where 
                                                      Subject_Account__c IN :clientIds AND Active__c = true AND 
                                                      (Relationship_Type__c='Has DIFC Sponsored Employee' OR (Document_Active__c = true AND 
                                                        (Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee') AND End_Date__c > TODAY ) ) AND 
                                                         object_contact__c != NULL AND
                                                         Object_Contact__r.RecordType.DeveloperName = 'GS_Contact')
                                             ]){
            
            if(!c.Secondary_Contact__r.isEmpty() &&
                infoCountMapNum.containsKey(c.Secondary_Contact__r[0].Subject_Account__c)){     
                infoCountMapNum.put(c.Secondary_Contact__r[0].Subject_Account__c,infoCountMapNum.get(c.Secondary_Contact__r[0].Subject_Account__c) + 1);
            }
        }
        
        for(Id i : clientIds) infoCountMap.put(i,String.valueOf(infoCountMapNum.get(i)));
        
        return infoCountMap;
    }

 
 
 
}