public without sharing class OB_PaymentInfoPopUpCltr 
{
    @AuraEnabled
    public static RespondWrap getCompanyNameInfo(String requestWrapParam)  
    {

        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap = new RespondWrap();

        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        String srId = reqWrap.srId;
        string accountId;
        
        for(User user :[select contact.AccountId,contact.Contractor__c from User where id = :UserInfo.getUserId()])
        {
            accountId = ( String.isNotBlank( user.contact.Contractor__c )? user.contact.Contractor__c :user.contact.AccountId);
        }
        

        //string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        //Account accRec = new Account();
       
        List<HexaBPM__Service_Request__c> lstSR = [
                                                    SELECT id,
                                                            HexaBPM__Customer__c 
                                                    FROM HexaBPM__Service_Request__c
                                                    WHERE  id =:srId 
                                                    LIMIT 1
                                                ];

       
        List<Account> accLst = [ 
                                  SELECT id,
                                         Name 
                                    FROM Account
                                   WHERE id =:accountId  
                                   LIMIT 1
                               ];
       
        if( 
            accLst != NULL 
                && accLst.size() > 0 
          )
        {
            respWrap.accRec = accLst[0];
        }


        if( accLst != NULL 
                    && lstSR != NULL 
                            && lstSR.size() > 0 
                                && accLst.size() > 0 )
        {
            respWrap.isOpen = accLst[0].Id != lstSR[0].HexaBPM__Customer__c;
        }
        return respWrap;

    }

    // ------------ Wrapper List ----------- //
    public class RequestWrap 
    {
        @AuraEnabled 
        public String  srId;

        public RequestWrap()
        {
        }
    }

    public class RespondWrap 
    {
        @AuraEnabled 
        public Account accRec;

        @AuraEnabled 
        public Boolean isOpen;

        public RespondWrap()
        {
            isOpen = true;
        }

    }


}