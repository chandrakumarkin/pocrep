/*********************************************************************************************************************
--------------------------------------------------------------------------------------------------------------------------
Modification History 
----------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
----------------------------------------------------------------------------------------              
V1.1    18-Nov-2020 Arun        exposed company data to insurance 

**********************************************************************************************************************/

@RestResource(urlMapping = '/DIFCinsurance/*')
global class CLS_DIFC_insurance{

    @Httpget 
    global static  void jsonResponse()
    {
        //Name from URL
          String Mode =  RestContext.request.params.get('mode');
          if(!test.isRunningTest())
          {
                RestContext.response.addHeader('Content-Type', 'application/json');
          }
      
      try
      {
       
        RestContext.response.responseBody = Blob.valueOf(jsonResString(Mode,RestContext.request.params));
      }
      catch(Exception ex)
      {
        
      }
      
      
    }
    
    public static string jsonResString(string Mode,Map<string,String> params)
    {
     //Dubai_Data__c ObjSQL=Dubai_Data__c.getInstance(Mode );

if(Mode=='listcompanies')
{
    
     //List<Dubai_Data_Meta__mdt> DataList= [SELECT SQL_Values__c   FROM Dubai_Data_Meta__mdt   WHERE DeveloperName =:Mode];
                                                          
      
      List<compnayList> ListcompnayLis=new List<compnayList>();
       

         for(Account ObjAccount:[select Name,Registration_License_No__c,Account_ID_18_Report__c from account where  Registration_License_No__c!=null AND ROC_Status__c  in('Active','Not Renewed')  order by Registration_License_No__c])
         {
             compnayList Objcompany=new compnayList();
             Objcompany.CompanyName=ObjAccount.Name;
             Objcompany.LicenseNo=ObjAccount.Registration_License_No__c;
             ListcompnayLis.add(Objcompany);
         }
        return    JSON.serialize( ListcompnayLis);
        
}
else if(Mode=='authorized')
{
    string Registration_License_No=params.get('lic');
    string EmailId=params.get('email');
    string Mobile='+'+params.get('mobile').trim();
    if(Registration_License_No!=null && EmailId!=null &&  Mobile!=null)
    {
        list<Relationship__c> ListRel=[select  id,Company_Registration_No__c,Subject_Account__r.Name from Relationship__c where Object_Contact__c!=null and Company_Registration_No__c=: Registration_License_No and Email__c=:EmailId and Relationship_Type__c='Is Authorized Signatory for' and Active__c=true limit 1];
        RelationshipData ObjRel=new RelationshipData();
        System.debug('ListRel==>'+ListRel);
        System.debug('Registration_License_No==>'+Registration_License_No);
        System.debug('EmailId==>'+EmailId);
        System.debug('Mobile==>'+Mobile);
        
        if(ListRel.Size()>0) 
        {
            
            ObjRel.CompanyName=ListRel[0].Subject_Account__r.Name;
            ObjRel.LicenseNo=ListRel[0].Company_Registration_No__c;
            ObjRel.IDNumber=ListRel[0].id;
            ObjRel.IsAuth='true';
            
        }
        else
        {
        
            ObjRel.CompanyName='';
            ObjRel.LicenseNo='';
            ObjRel.IDNumber='';
            ObjRel.IsAuth='false';
            
            
            
        }
            return    JSON.serialize( ObjRel);
        
    }
    
    
}
    
    return  '';

    
    
    }
    
    class compnayList
    {
        public string CompanyName{get;set;}
        public string LicenseNo{get;set;}
    }
    class RelationshipData
    {
        public string CompanyName{get;set;}
        public string LicenseNo{get;set;}
        public string IDNumber{get;set;}
        public string IsAuth{get;set;}
    }
    
 }