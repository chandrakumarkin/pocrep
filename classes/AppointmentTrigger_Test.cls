@isTest

public class AppointmentTrigger_Test{


   static testMethod void myUnitTest1(){
   
   Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact'; 
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
   
     map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
   
      
        
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c='GS';
        insert objTemplate1;
        
        
        
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test scheduled';
        objStepType1.Code__c = 'Medical Fitness Test scheduled';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Medical Fitness Test scheduled';
        insert objStepType1;
        
        
    Service_Request__c objSR3 = new Service_Request__c();
    objSR3.Customer__c = objAccount.Id;
    objSR3.Email__c = 'testsr@difc.com';    
    objSR3.Contact__c = objContact.Id;
    objSR3.Express_Service__c = false;
    objSR3.SR_Template__c = objTemplate1.Id;
    
    insert objSR3;
    
    Status__c objStatus = new Status__c();
    objStatus.Name = 'Re-Scheduled';
    objStatus.Type__c = 'Start';
    objStatus.Code__c = 'RE-SCHEDULED';
    insert objStatus;
        
        step__c objStep = new Step__c();
        objStep.SR__c = objSR3.Id;
        //objStep2.OwnerId = mapNameOwnerId.get('Customer');
        //objStep2.Biometrics_Required__c = 'No';
        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        objStep.status__c = objStatus.id;
        insert objStep;
   
   Appointment__c  app = new Appointment__c ();
   
   
   
   
   app.Applicantion_Type__c ='Normal';
   app.Appointment_StartDate_Time__c =  datetime.newInstance(2018, 3, 27, 8, 30, 0);
   app.step__c = objStep.id;
   insert app;
   
   CalendarDemoCtrl CalendarDemoCtrl = new CalendarDemoCtrl();
   
   
   
   
   Appointment__c app2 =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Service_Type__c  from appointment__c where id =: app.id];  
   
   app2.Appointment_StartDate_Time__c  = app2.Appointment_StartDate_Time__c.addMinutes(10); 
   update app2;
   
   
   Appointment__c app1 =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Service_Type__c from appointment__c where id =: app.id];
   
   
   
   app1.Applicantion_Type__c ='Express';
   app1.Appointment_StartDate_Time__c  = app1.Appointment_StartDate_Time__c.addMinutes(10); 
   update app1;
   
   
   Appointment_Schedule_Counter__c appCounter = new Appointment_Schedule_Counter__c();
   appCounter .Appointment_Time__c = app1.Appointment_StartDate_Time__c  ;
   appCounter .Application_Type__c = 'Normal';
   
   insert appCounter ;
   
   step__C step =[select id,createdDate,SR__c from step__c where id =:objStep.id ];
    //Datetime dt = datetime.newInstance(2018,3,31,8,30,0);
    //test.setCreatedDate(objStep.id,dt);
   
   
   Test.startTest();
   
   BusinessHours BH =[SELECT Id,FridayStartTime,MondayEndTime,MondayStartTime,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,TuesdayStartTime,WednesdayStartTime,WednesdayEndTime FROM BusinessHours where name  ='GS- Medical Appointment Normal' limit 1];
   
   
    
  
   date nxtDate = Date.newInstance(step.createdDate.year(),step.createdDate.month(),step.createdDate.day()).addDays(1);
   AppointmentCreationHandler.MedicalAppointmentCreate(step);
   AppointmentCreationHandler.appointmentBookingNxtMethod (BH,nxtDate ,'Normal', false);
   //AppointmentCreationHandler.updateScheduleCounter(app1,true,appCounters );
   
   //AppointmentCreationHandler AppointmentCreationHandler  = new AppointmentCreationHandler ();
   
   //AppointmentCreationHandler.MedicalAppointmentCreate(step); 
   Test.stopTest();
   
   BusinessHours BH1 =[SELECT Id,FridayStartTime,MondayEndTime,MondayStartTime,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,TuesdayStartTime,WednesdayStartTime,WednesdayEndTime FROM BusinessHours where name  ='GS- Medical Appointment Normal' limit 1];
   
   Date dt1 = date.newInstance(2018,4,1);
   AppointmentCreationHandler.getStartTimeofDay(BH1 ,dt1);
   }
   
   static testMethod void myUnitTest2(){
   
   Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
   
     map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
   
      
        
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c='GS';
        insert objTemplate1;
        
        
        
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test scheduled';
        objStepType1.Code__c = 'Medical Fitness Test scheduled';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Medical Fitness Test scheduled';
        insert objStepType1;
        
        
    Service_Request__c objSR3 = new Service_Request__c();
    objSR3.Customer__c = objAccount.Id;
    objSR3.Email__c = 'testsr@difc.com';    
    objSR3.Contact__c = objContact.Id;
    objSR3.Express_Service__c = true;
    objSR3.SR_Template__c = objTemplate1.Id;
    
    insert objSR3;
    
    Status__c objStatus = new Status__c();
    objStatus.Name = 'Re-Scheduled';
    objStatus.Type__c = 'Start';
    objStatus.Code__c = 'RE-SCHEDULED';
    insert objStatus;
        
        step__c objStep = new Step__c();
        objStep.SR__c = objSR3.Id;
        
        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        objStep.status__c = objStatus.id;
        insert objStep;
   
   Appointment__c  app = new Appointment__c ();
   
   
   
   
   app.Applicantion_Type__c ='Normal';
   app.Appointment_StartDate_Time__c =  datetime.newInstance(2018, 3, 26, 8, 30, 0);
   app.step__c = objStep.id;
   insert app;
   
   CalendarDemoCtrl CalendarDemoCtrl = new CalendarDemoCtrl();
   
   
   
   
   Appointment__c app2 =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Service_Type__c  from appointment__c where id =: app.id];  
   
   app2.Appointment_StartDate_Time__c  = app2.Appointment_StartDate_Time__c.addMinutes(10); 
   update app2;
   
   
   Appointment__c app1 =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Service_Type__c   from appointment__c where id =: app.id];
   
   
   
   app1.Applicantion_Type__c ='Express';
   app1.Appointment_StartDate_Time__c  = app1.Appointment_StartDate_Time__c.addMinutes(10); 
   update app1;
   
   List<Appointment_Schedule_Counter__c > appCounters = new List<Appointment_Schedule_Counter__c >();
   Appointment_Schedule_Counter__c appCounter = new Appointment_Schedule_Counter__c();
   appCounter .Appointment_Time__c = app1.Appointment_StartDate_Time__c  ;
   appCounter .Application_Type__c = 'Normal';
   appCounters.add(appCounter); 
   
   dateTime nxtdt1 = datetime.newInstance(2018,3,28,10,30,0);
   
   Appointment_Schedule_Counter__c appCounter1 = new Appointment_Schedule_Counter__c();
   appCounter1 .Appointment_Time__c =  nxtdt1 ;
   appCounter1 .Application_Type__c = 'Normal';
   appCounters.add(appCounter1); 
   
   
   
   insert appCounters ;
   Datetime dt = datetime.newInstance(2018,3,27,8,30,0);
   test.setCreatedDate(objStep.id,dt);
   
   Test.startTest();
   step__C step =[select id,createdDate,SR__c from step__c where id =:objStep.id ];
    system.assertEquals(step.createdDate,dt);
   BusinessHours BH =[SELECT Id,FridayStartTime,MondayEndTime,MondayStartTime,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,TuesdayStartTime,WednesdayStartTime,WednesdayEndTime FROM BusinessHours where name  ='GS- Medical Appointment Express' limit 1];
  
   //date nxtDate = Date.newInstance(step.createdDate.year(),step.createdDate.month(),step.createdDate.day()).addDays(2);
   
   date nxtDate = date.newInstance(2018,3,28);
   
   
   AppointmentCreationHandler.MedicalAppointmentCreate(step);
    AppointmentCreationHandler.appointmentBookingMethod(BH,appCounters,false);
   AppointmentCreationHandler.appointmentBookingNxtMethod (BH,nxtDate ,'Normal', false);
   AppointmentCreationHandler.updateScheduleCounter(app1,true,appCounters );
   
   Test.stopTest();
   BusinessHours BH1 =[SELECT Id,FridayStartTime,MondayEndTime,MondayStartTime,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,TuesdayStartTime,WednesdayStartTime,WednesdayEndTime FROM BusinessHours where name  ='GS- Medical Appointment Express' limit 1];
  
   Date dt1 = date.newInstance(2018,3,30);
   AppointmentCreationHandler.getStartTimeofDay(BH1 ,dt1);
   
   }
   
   
   static testMethod void myUnitTest3(){
   
   Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
   
     map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
   
      
        
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c='GS';
        insert objTemplate1;
        
        
        
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test scheduled';
        objStepType1.Code__c = 'Medical Fitness Test scheduled';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Medical Fitness Test scheduled';
        insert objStepType1;
        
        
    Service_Request__c objSR3 = new Service_Request__c();
    objSR3.Customer__c = objAccount.Id;
    objSR3.Email__c = 'testsr@difc.com';    
    objSR3.Contact__c = objContact.Id;
    objSR3.Express_Service__c = true;
    objSR3.SR_Template__c = objTemplate1.Id;
    
    insert objSR3;
    
    Status__c objStatus = new Status__c();
    objStatus.Name = 'Re-Scheduled';
    objStatus.Type__c = 'Start';
    objStatus.Code__c = 'RE-SCHEDULED';
    insert objStatus;
        
        step__c objStep = new Step__c();
        objStep.SR__c = objSR3.Id;
        
        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        objStep.status__c = objStatus.id;
        insert objStep;
   
   Appointment__c  app = new Appointment__c ();
   
   
   
   
   app.Applicantion_Type__c ='Express';
   app.Appointment_StartDate_Time__c =  datetime.newInstance(2018, 3, 25, 10, 30, 0);
   app.step__c = objStep.id;
   insert app;
   
   CalendarDemoCtrl CalendarDemoCtrl = new CalendarDemoCtrl();
   
   
   
   
   Appointment__c app2 =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Service_Type__c from appointment__c where id =: app.id];     
   app2.Appointment_StartDate_Time__c  = app2.Appointment_StartDate_Time__c.addMinutes(10); 
   update app2;
   
   
   Appointment__c app1 =[select id,Applicantion_Type__c,Appointment_StartDate_Time__c,Service_Type__c from appointment__c where id =: app.id]; 
   app1.Applicantion_Type__c ='Express';
   app1.Appointment_StartDate_Time__c  = app1.Appointment_StartDate_Time__c.addMinutes(10); 
   update app1;
   
   List<Appointment_Schedule_Counter__c > appCounters = new List<Appointment_Schedule_Counter__c >();
   Appointment_Schedule_Counter__c appCounter = new Appointment_Schedule_Counter__c();
   appCounter .Appointment_Time__c = app1.Appointment_StartDate_Time__c  ;
   appCounter .Application_Type__c = 'Normal';
   appCounters.add(appCounter); 
   
   dateTime nxtdt1 = datetime.newInstance(2018,3,28,10,30,0);
   
   Appointment_Schedule_Counter__c appCounter1 = new Appointment_Schedule_Counter__c();
   appCounter1 .Appointment_Time__c =  nxtdt1 ;
   appCounter1 .Application_Type__c = 'Normal';
   appCounters.add(appCounter1); 
   
   
   
   insert appCounters ;
   Datetime dt = datetime.newInstance(2018,3,26,8,30,0);
   test.setCreatedDate(objStep.id,dt);
   
   Test.startTest();
   step__C step =[select id,createdDate,SR__c from step__c where id =:objStep.id ];
    system.assertEquals(step.createdDate,dt);
   BusinessHours BH =[SELECT Id,FridayStartTime,MondayEndTime,MondayStartTime,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,TuesdayStartTime,WednesdayStartTime,WednesdayEndTime FROM BusinessHours where name  ='GS- Medical Appointment Express' limit 1];
  
   //date nxtDate = Date.newInstance(step.createdDate.year(),step.createdDate.month(),step.createdDate.day()).addDays(2);
   
   date nxtDate = date.newInstance(2018,3,28);
   
   
   AppointmentCreationHandler.MedicalAppointmentCreate(step);
    AppointmentCreationHandler.appointmentBookingMethod(BH,appCounters,false);
   AppointmentCreationHandler.appointmentBookingNxtMethod (BH,nxtDate ,'Normal', false);
   AppointmentCreationHandler.updateScheduleCounter(app1,true,appCounters );
   
   Test.stopTest();
   BusinessHours BH1 =[SELECT Id,FridayStartTime,MondayEndTime,MondayStartTime,SaturdayEndTime,SaturdayStartTime,SundayEndTime,SundayStartTime,ThursdayEndTime,ThursdayStartTime,TuesdayEndTime,TuesdayStartTime,WednesdayStartTime,WednesdayEndTime FROM BusinessHours where name  ='GS- Medical Appointment Express' limit 1];
  
   Date dt1 = date.newInstance(2018,3,30);
   AppointmentCreationHandler.getStartTimeofDay(BH1 ,dt1);
   
   }


}