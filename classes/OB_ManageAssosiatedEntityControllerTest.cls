/*
    Author      : Leeba
    Date        : 30-March-2020
    Description : Test class for OB_ManageAssosiatedEntityControllerr
    --------------------------------------------------------------------------------------
*/
@isTest
public without sharing class OB_ManageAssosiatedEntityControllerTest {
    
    
    public static testmethod void test1(){
    
     // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
    
        Account acc = new Account();
        acc.Name = 'test account';
        insert acc;
        
         Account ConAcc = new Account();
        ConAcc.Name = 'contactor account';
        insert ConAcc;
           
    
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId =insertNewAccounts[0].id,Role__c ='Company Services');
        con.Contractor__c = ConAcc.Id;
		insert con; 
        
        /*
        AccountContactRelation accConRel = new AccountContactRelation();
        accConRel.AccountId = insertNewAccounts[0].Id;
        accConRel.ContactId = con.Id;
        accConRel.Access_Level__c = 'Read Only';
        
        insert accConRel;
        */
         // create document master
        
        
         /*
        AccountContactRelation acccon = new AccountContactRelation();
        acccon.AccountId =  insertNewAccounts[0].Id;
        acccon.ContactId = con.Id;
        acccon.Roles = 'Company Services';
        acccon.Access_Level__c = 'Read Only';
        insert acccon;  
         */
        
        
     
          //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('New_User_Registration', 1);
        insert listPageFlow;
        
        //create page
        HexaBPM__Page__c objPage = new HexaBPM__Page__c();
        objPage.HexaBPM__Page_Flow__c = listPageFlow[0].id;
        objPage.HexaBPM__Page_Order__c = 2;
        insert objPage;
        
        HexaBPM__Document_Master__c docmaster = new HexaBPM__Document_Master__c ();
        docmaster.HexaBPM__Code__c ='Passport Copy';
        insert docmaster;      
                
        HexaBPM__SR_Template_Docs__c objSRTempDoc = new HexaBPM__SR_Template_Docs__c();
        objSRTempDoc.Visible_to_GS__c = true;
        objSRTempDoc.HexaBPM__Document_Master__c = docmaster.id;
        insert objSRTempDoc;
        
        HexaBPM__SR_Doc__c objSRDoc = new HexaBPM__SR_Doc__c();
        objSRDoc.Name = 'Passport Copy';
        objSRDoc.HexaBPM__SR_Template_Doc__c = objSRTempDoc.Id;
        
        
        
        //old step data 
        
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c = 'GS';
        insert objTemplate1;
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = insertNewAccounts[0].id;
        objSR1.Email__c = 'testsr123@difc.com';
        objSR1.Tenant_Type_RORP__c  = 'Contractor';
        //objSR1.Contact__c = con.Id;
        objSR1.Express_Service__c = false;
        objSR1.SR_Template__c = objTemplate1.Id;
        objSR1.Pre_GoLive__c = false;
        objSR1.Statement_of_Undertaking__c = true;
        objSR1.contractor__c=ConAcc.Id;
        insert objSR1;
            
                Status__c objAwaiting = new Status__c();
        objAwaiting.Name = 'AWAITING_REVIEW';
        objAwaiting.Type__c = 'Start';
        objAwaiting.Code__c = 'AWAITING_REVIEW';
        insert objAwaiting;
        
        SR_Steps__c srStep = new SR_Steps__c();
        srStep.Assigned_to_client__c= false ;
        insert srStep;
        
        step__c objStep = new Step__c();
        objStep.SR__c = objSR1.Id;
        objStep.Biometrics_Required__c = 'No';
        //objStep.Step_Template__c = objStepType1.id;
        objStep.No_PR__c = false;
        objStep.status__c = objAwaiting.id;
        objStep.AppointmentCreation__c = 'Create';
        //objStep.Assigned_to_client__c = true;
        objStep.SR_Step__c = srStep.id;
        objStep.SR_Group__c = 'RORP'; 
        
        insert objStep;
                
    
    test.startTest();
          
          
            Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
                 
                          
                User user = new User(alias = 'test123', email='test123@noemail.com',
                        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                        localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                        ContactId = con.Id, 
                        timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
               user.Community_User_Role__c = 'Fit-Out Services';
                insert user;
        objStep.OwnerId = user.id;
        update objStep;
        
            system.runAs(user)
            {
            
            
                
            OB_ManageAssosiatedEntityController.RequestWrap reqWrapper = new OB_ManageAssosiatedEntityController.RequestWrap();
            reqWrapper.contactId= con.id;
            reqWrapper.mainAccIdToSet = con.id;
           

            OB_ManageAssosiatedEntityController.RespondWrap restWrapTest = new OB_ManageAssosiatedEntityController.RespondWrap();
            
            String requestString = JSON.serialize(reqWrapper);
                
            restWrapTest = OB_ManageAssosiatedEntityController.setContactMainAccount(requestString);
            
            reqWrapper.mainAccIdToSet = insertNewAccounts[0].id;
            requestString = JSON.serialize(reqWrapper);
            restWrapTest = OB_ManageAssosiatedEntityController.setContactMainAccount(requestString);    
            
            restWrapTest = OB_ManageAssosiatedEntityController.createNewEntitySr(requestString);
                
                HexaBPM__SR_Template__c objSrTemp = new HexaBPM__SR_Template__c();
        objSrTemp.Name = 'New User Registration';
        objSrTemp.HexaBPM__Active__c = true;
        objSrTemp.HexaBPM__SR_RecordType_API_Name__c = 'New_User_Registration';
        insert objSrTemp;
                
                
                
                 restWrapTest = OB_ManageAssosiatedEntityController.createNewEntitySr(requestString);
                
            
           
            restWrapTest = OB_ManageAssosiatedEntityController.fetchEntityData();
              
                reqWrapper.LoggedInUser = restWrapTest.LoggedInUser;
                reqWrapper.relatedAccountIdsSet = new list<string>{ConAcc.id};
               
                  requestString = JSON.serialize(reqWrapper);
                
                    OB_ManageAssosiatedEntityController.getPendingActions (requestString);
                
        }  
        con.Role__c = 'Property Services';
                update con;
        
        system.runAs(user)
        {
             OB_ManageAssosiatedEntityController.fetchEntityData ();
            
        }
        objStep.SR_Group__c = 'GS'; 
                //update objStep;
                con.Role__c = 'Employee Services';
                //update con;
        
        system.runAs(user)
        {
             OB_ManageAssosiatedEntityController.fetchEntityData ();
        }
        con.Role__c = '';
                update con;
        
        system.runAs(user)
        {
            
            //OB_ManageAssosiatedEntityController.getPendingActions (Null);
        }
        
        test.stopTest();
    }
    
    public static testmethod void test2()
     {
    
     	// create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
    
        Account acc = new Account();
        acc.Name = 'test account';
        insert acc;
         
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = acc.id,Role__c ='Fit-Out Services');
		insert con; 
        
     	List<AccountContactRelation> accContactRel = [
                                                            SELECT Id,
                                                                    AccountId,
                                                                    ContactId,
                                                                    Roles,
                                                                    Access_Level__c 
                                                            FROM AccountContactRelation
                                                            WHERE ContactId =: con.Id
                                                              AND AccountId =: acc.Id
                                                      ];

		
		accContactRel[0].Access_Level__c = 'Read Only';  
        update accContactRel[0];
         
         
        /*
            AccountContactRelation accConRel = new AccountContactRelation();
            accConRel.AccountId = acc.Id;
            accConRel.ContactId = con.Id;
            accConRel.Access_Level__c = 'Read Only';
            insert accConRel;
      */
    test.startTest();
          
          
            Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
            User user = new User(alias = 'test123', email='test123@noemail.com',
                        emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                        localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                        ContactId = con.Id,
                        timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
               //user.Community_User_Role__c = 'Fit-Out Services';
                insert user;
            system.runAs(user)
            {
            
            
            OB_ManageAssosiatedEntityController.RequestWrap reqWrapper = new OB_ManageAssosiatedEntityController.RequestWrap();
            reqWrapper.contactId= con.id;
                
            reqWrapper.mainAccIdToSet = acc.id;
              
           
            OB_ManageAssosiatedEntityController.RespondWrap restWrapTest = new OB_ManageAssosiatedEntityController.RespondWrap();
            //reqWrapper.contactId = restWrapTest.loggedInUser.ContactId;
            String requestString = JSON.serialize(reqWrapper);
            
           
                
            //restWrapTest = OB_ManageAssosiatedEntityController.fetchEntityData();
                  
            
        }
        
        
        test.stopTest();
    }

    
    
     

}