/*
    Author      :   Sravan
    Description :   This class consists the actions for User Access form
    
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    V1.1    18-12-2016  Sravan        Method created to approve the user access step as per tkt # 3216
    V1.2    10-04-2017  Sravan        Modified the subject line for the Email 
    v1.3    11-may-2020 selva         Added method to auto approve the Economic Substance notification  
    v1.4    17-may-2020 selva         #9208
    v1.5    21-Nov-2020 Arun          #12454    
    -------------------------------------------------------------------------------------------------------------------------- 
*/

public without Sharing  class  UserAccessFormActions{
    
    public UserAccessFormActions()
    {
        
    }
    
    // Method to send an email to user with the User Accessform document generated
    @future(Callout=true)
    public static void sendUserAccessFormEmail(Set<id> SRDocIDs)
    {
    //v1.5 Changed logic 
         Contact securityContact = new Contact();
         
         try
         {
             
    
            id owEmailID;
            for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress]) {
                 owEmailID =owa.id; 
            }
            
             map<id,SR_Doc__c> srDocDetails = new map<id,SR_DOC__c>([select id,Name,Service_Request__c,Service_Request__r.name,Service_Request__r.Portal_Service_Request_Name__c,Service_Request__r.First_Name__c,Service_Request__r.Last_Name__c,customer__c,customer__r.Name,Applicant_Email__c,Service_Request__r.Customer__r.name from SR_Doc__c where id in :SRDocIDs]);
             
             
              EmailTemplate et = [Select Id,Name from EmailTemplate where DeveloperName= 'User_access_form_template'];
            
                    
            for(id srDOcID : srDocDetails.KeySet())
            {
                if(srDocDetails.containsKey(srDocID) && srDocDetails.get(srDocID).Applicant_Email__c !=null)
                {
                 
                   Savepoint sp = Database.SetSavePoint();
                   securityContact.LastName = srDocDetails.get(srDocID).Service_Request__r.Customer__r.name;
                    securityContact.FirstName =  srDocDetails.get(srDocID).Service_Request__r.First_Name__c;
                    securityContact.email = srDocDetails.get(srDocID).Applicant_Email__c;
                    securityContact.Accountid=srDocDetails.get(srDocID).customer__c;
                    insert securityContact;
        
                 
                    List<messaging.SingleEmailMessage> messages = new List<messaging.SingleEmailMessage>();
                    messaging.SingleEmailMessage message = new messaging.SingleEmailMessage();
                    message.SetToAddresses(new list<string>{srDocDetails.get(srDocID).Applicant_Email__c});   
                    message.setTemplateId(et.id);
                    message.setWhatId(srDocDetails.get(srDocID).Service_Request__c);
                    
                    message.SettargetObjectId(securityContact.id);
                    message.SetOrgWideEmailAddressID(owEmailID);
                    messages.add(message);
                    //messages.setSaveAsActivity(false);
                    List<Messaging.SendEmailResult> results = Messaging.sendEmail(messages);
                    if(results!=null && results.size() >0 && !results[0].isSuccess())
                    delete securityContact;
                
                }
            
            }
         }
        catch(exception e)
        {
            
            if(securityContact.Id!=null)delete securityContact;
             Log__c objLog = new Log__c(); objLog.Type__c = 'User Access form Actions';objLog.Description__c = e.getMessage()+' Line # '+e.getLineNumber(); insert objLog; 
        }
            
        
        
        
        
    }
    
    @future
    //start V1.1
    Public static void ApproveUserstep(string stepSerialized){
        
        step__c stp = (step__c)Json.deserialize(stepSerialized,step__c.class);
        
        List<Status__c> statusList = new List<Status__c>([select id,name from Status__c where name ='Approved']);
        List<SR_status__c> srStatusList = new list<SR_status__c>([select id,name from SR_status__c where name ='Approved']);
        List<service_request__c> serviceReqDetails = new List<service_request__c>([select id,Customer__r.ROC_reg_incorp_Date__c,Customer__r.Financial_Year_End__c,Record_Type_Name__c,Customer__r.ROC_Status__c from Service_Request__c where id=:stp.SR__c]);
        system.debug('*&*&*&'+stp);
        if(stp !=null && stp.Status__c!=null && stp.Step_Status__c != 'Approved' && serviceReqDetails.size()>0 && serviceReqDetails[0].Customer__r.ROC_Status__c =='Under Formation' && statusList.size() >0 && srStatusList.size()>0)
        {
            stp.status__c = statusList[0].id;
            serviceReqDetails[0].External_SR_Status__c = srStatusList[0].id;
            serviceReqDetails[0].Internal_SR_Status__c = srStatusList[0].id;
            try{
                stp.ownerID = userinfo.getUserID();
                Update stp;
                
                Update serviceReqDetails;
                
            }
            catch(Exception e){
                
               Log__c objLog = new Log__c();   objLog.Type__c = 'User Access form Actions';   objLog.Description__c = 'Unable to create User for Underformation company'+e.getMessage()+' Line # '+e.getLineNumber();                      insert objLog;
            }// End V1.1
        }else if(serviceReqDetails.size()>0){ // v1.3 start
            if(serviceReqDetails[0].Record_Type_Name__c=='EconomySubstance' && stp !=null && stp.Status__c!=null && stp.Step_Status__c != 'Approved'){
                Boolean isFinancialYearMatched = false;
                Date incorporationDate = serviceReqDetails[0].Customer__r.ROC_reg_incorp_Date__c;
                List<EconomicSubstanceFinancialYearEnd__mdt> finList = [select AllwithBranch2018EndDate__c,AllwithBranch2018StartDate__c,EntitiesJantoJun2019StartDate__c,EntitiesJantoJun2019EndDate__c,EntitiesFromJul2019StartDate__c,EntitiesJantoJun2019EndDate_1__c,EntitiesFromJul2019EndDate__c from EconomicSubstanceFinancialYearEnd__mdt where Financial_Year_End__c=:serviceReqDetails[0].Customer__r.Financial_Year_End__c limit 1];
                if(finList.size()>0){
                    isFinancialYearMatched =  true;
                }
                if(incorporationDate >= Date.newInstance(2019,01,01) && incorporationDate <= Date.newInstance(2019,06,30)){
                    isFinancialYearMatched =  false;
                }
                if(incorporationDate >= Date.newInstance(2019,07,01)){
                    isFinancialYearMatched =  false; // v1.4
                }
                /*******************
                for(EconomicSubstanceFinancialYearEnd__mdt itr :finList){
                    if(incorporationDate >= Date.newInstance(2019,01,01) && incorporationDate <= Date.newInstance(2019,06,30)){
                        isFinancialYearMatched =  true;
                        if(incorporationDate!=null){
                            Datetime incorporationDateJun2019 = datetime.newInstance(incorporationDate.year(), incorporationDate.month(),incorporationDate.day());
                        
                            StartDateRangeOptions.add(new selectoption(incorporationDateJun2019.format('dd/MM/yyyy'),incorporationDateJun2019.format('dd/MM/yyyy')));
                        }
                        if(itr.EntitiesJantoJun2019StartDate__c!=null){
                            Datetime EntitiesJantoJun2019StartDate = datetime.newInstance(itr.EntitiesJantoJun2019StartDate__c.year(), itr.EntitiesJantoJun2019StartDate__c.month(),itr.EntitiesJantoJun2019StartDate__c.day());
                        
                            StartDateRangeOptions.add(new selectoption(EntitiesJantoJun2019StartDate.format('dd/MM/yyyy'),EntitiesJantoJun2019StartDate.format('dd/MM/yyyy')));
                        }
                        if(itr.EntitiesJantoJun2019EndDate__c!=null){
                            Datetime EntitiesJantoJun2019EndDate = datetime.newInstance(itr.EntitiesJantoJun2019EndDate__c.year(), itr.EntitiesJantoJun2019EndDate__c.month(),itr.EntitiesJantoJun2019EndDate__c.day());
                        
                            EndDateRangeOptions.add(new selectoption(EntitiesJantoJun2019EndDate.format('dd/MM/yyyy'),EntitiesJantoJun2019EndDate.format('dd/MM/yyyy')));
                        }
                        if(itr.EntitiesJantoJun2019EndDate_1__c!=null){
                            Datetime EntitiesJantoJun2019EndDate_1 = datetime.newInstance(itr.EntitiesJantoJun2019EndDate_1__c.year(), itr.EntitiesJantoJun2019EndDate_1__c.month(),itr.EntitiesJantoJun2019EndDate_1__c.day());
                        
                            EndDateRangeOptions.add(new selectoption(EntitiesJantoJun2019EndDate_1.format('dd/MM/yyyy'),EntitiesJantoJun2019EndDate_1.format('dd/MM/yyyy')));
                        }
                        system.debug('--Yellow--'+incorporationDate+'---'+EndDateRangeOptions);
                    }
                }
                ******************/
                if(isFinancialYearMatched){
                    stp.status__c = statusList[0].id;serviceReqDetails[0].External_SR_Status__c = srStatusList[0].id; serviceReqDetails[0].Internal_SR_Status__c = srStatusList[0].id;
                    try{
                        //Group rocid = [select Id from Group where  Type = 'Queue' AND NAME = 'RoC_Officer'];
                        //stp.ownerID = rocid.Id;
                        //stp.ownerID = rocid.Id;
                        Update stp;
                        
                        Update serviceReqDetails;
                        
                    }
                    catch(Exception e){
                        
                       Log__c objLog = new Log__c(); objLog.Type__c = 'User Access form Actions';  objLog.Description__c = 'Unable to create User for Underformation company'+e.getMessage()+' Line # '+e.getLineNumber();               insert objLog;
                    }
                }
            }
        }// v1.3 End
    }
   
}