public class cls_ClientInformation_edit {

    public Service_Request__c SRData {get;set;}
    public map < string, string > mapParameters;
    public string RecordTypeId;
    public List < Amendment__c > listOfAmendment {get;set;}
    public Amendment__c CEODetails {get;set;}
    public Amendment__c CorporateCommunication{get;set;} 
    public List<RecordType> recordtypeIdValue;
    public Integer indexOfList{get;set;}
    Public List<Relationship__c> relationships{get;set;}
    public cls_ClientInformation_edit(ApexPages.StandardController controller) 
    {
        recordtypeIdValue = [SELECT id from RecordType where (name='Individual' or name='CEO') and sobjectType = 'Amendment__c' order by Name];
        listOfAmendment = new List < Amendment__c > ();
        CEODetails = new Amendment__c();
        CEODetails.Amendment_Type__c = 'Individual';
        CEODetails.Relationship_Type__c = 'CEO';
        CEODetails.recordtypeId = recordtypeIdValue[0].id;
        CEODetails.Status__c = 'Draft';
        SRData = (Service_Request__c) controller.getRecord();
        //New Sr Set record type 
        indexOfList = 0;
        mapParameters = new map < string, string > ();
        if (apexpages.currentPage().getParameters() != null)
            mapParameters = apexpages.currentPage().getParameters();
        if (mapParameters.get('RecordType') != null) RecordTypeId = mapParameters.get('RecordType');
        //system.assertEquals(null,SRData);
        if (SRData.id != null) {
           populateData();
        } 
        else {
            boolean relExist = getRelationships(recordtypeIdValue);
            if(!relExist)
            {
                CEODetails.Amendment_Type__c = 'Individual';
                CEODetails.Relationship_Type__c = 'CEO';
                CEODetails.recordtypeId = recordtypeIdValue[0].id;
                CEODetails.Status__c = 'Draft';
                CEODetails.ServiceRequest__c = SRData.id;
                
                CorporateCommunication = new Amendment__c();
                CorporateCommunication.Amendment_Type__c = 'Individual';
                CorporateCommunication.recordtypeId = recordtypeIdValue[1].id;
                CorporateCommunication.Relationship_Type__c = 'Corporate Communication';
                CorporateCommunication.Status__c = 'Draft';
                CorporateCommunication.Nationality_list__c ='UAE';
                CorporateCommunication.ServiceRequest__c = SRData.id;            }
        }
        
        //Set customer details
        for (User objUsr: [select id, ContactId, Email, Phone, Contact.AccountId, Contact.Account.Company_Type__c, Contact.Account.Financial_Year_End__c, Contact.Account.Next_Renewal_Date__c, Contact.Account.Name, Contact.Account.Sector_Classification__c, Contact.Account.License_Activity__c, Contact.Account.Is_Foundation_Activity__c from User where Id =: userinfo.getUserId()]) {
            if (SRData.id == null) {
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.Customer__r = objUsr.Contact.Account;
                SRData.RecordTypeId = RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
            }
        }
    }
    public PageReference save(){
        try{
            upsert SRData;
            CEODetails.ServiceRequest__c = SRData.id;
            upsert CEODetails;
            if(CorporateCommunication != Null){
                CorporateCommunication.ServiceRequest__c = SRData.id;
                CorporateCommunication.Passport_No__c =CorporateCommunication.Person_Email__c;
                upsert CorporateCommunication;
            }
            
            PageReference acctPage = new ApexPages.StandardController(SRData).view();
            acctPage.setRedirect(true);
            return acctPage;
        }catch(Exception ex){   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,+ex.getMessage());  ApexPages.addMessage(myMsg);           return null;   }
        
    }
    public pageReference AddNewAmendment(){
        CorporateCommunication = new Amendment__c();
        CorporateCommunication.Amendment_Type__c = 'Individual';
        CorporateCommunication.recordtypeId = recordtypeIdValue[1].id;
        CorporateCommunication.Relationship_Type__c = 'Corporate Communication';
        CorporateCommunication.Status__c = 'Draft';
        //Business have removed the fields from the UI but existing code is highly dependent on these field values
        CorporateCommunication.Nationality_list__c ='UAE';
        CorporateCommunication.ServiceRequest__c = SRData.id;
        return null;
    }
    public pageReference EditAmendment()
    {
        if(indexOfList !=null) CorporateCommunication = listOfAmendment[indexOfList];
        return null;
    }
    public pageReference removeAmendment()
    {
        if(indexOfList !=null) {
            Amendment__c removeAmend = listOfAmendment[indexOfList];
            removeAmend.status__c='Remove';
            update removeAmend;
        }
        populateData();
        PageReference acctPage = new ApexPages.StandardController(SRData).view();
        acctPage.setRedirect(true);
        return acctPage;
    }
    @testVisible
    private void populateData()
    {
         for (Amendment__c ObjAmed: [Select id, Customer__c, Status__c, Name_of_Declaration_Signatory__c, Name, Nationality__c, Title_new__c, Family_Name__c, Given_Name__c, Date_of_Birth__c, Place_of_Birth__c, Passport_No__c, Nationality_list__c, Phone__c, Non_Recognized_Auditors__c,
                    Person_Email__c, Occupation__c, Sys_Secondary__c, Capacity__c, Other_Directorships__c, Date_of_Appointment_GP__c, Name_of_incorporating_Shareholder__c, Name_of_Body_Corporate_Shareholder__c, Recognized_Auditors__c, Mobile__c, Job_Title__c,
                    Shareholder_enjoy_direct_benefits__c, First_Name__c, First_Name_Arabic__c, Representation_Authority__c, Type_of_Authority_Other__c, Amount_of_Contribution_US__c, Contribution_Type__c, DIFC_Recognized_Auditor__c,
                    Contribution_Type_Other__c, Percentage_held__c, Class_of_Units_Other_Rights__c, Apt_or_Villa_No__c, Building_Name__c, Street__c, PO_Box__c, Auditors__c, sys_create__c, Last_Name_Arabic__c, Middle_Name_Arabic__c,
                    Permanent_Native_City__c, Emirate_State__c, Post_Code__c, Address3__c, Permanent_Native_Country__c, Class__c, New_No_of_Shares__c, Total_Nominal_Value__c, Relationship_Type__c, Is_Designated_Member__c, Data_Center_Access_ID__c,
                    Company_Name__c, Former_Name__c, Registration_Date__c, Date_of_Appointment_SP__c, Date_of_Appointment_Auditor__c, Emirate__c, Amendment_Type__c, Place_of_Registration__c, Financial_Year_End__c
                    from Amendment__c where ServiceRequest__c =: SRData.Id and Relationship_Type__c != null and(Status__c = 'Active'
                        or Status__c = 'Draft') AND Related__c = ''
                ]) {
                    if (ObjAmed.Relationship_Type__c == 'CEO'){
                       CEODetails = ObjAmed; 
                       CEODetails.ServiceRequest__c = SRData.id;
                       CEODetails.recordtypeId = recordtypeIdValue[0].id;
                    }
                  else{
                      //ObjAmed.Relationship_Type__c = 'Has Corporate Communication';
                      ObjAmed.Relationship_Type__c = 'Corporate Communication';
                      ObjAmed.ServiceRequest__c = SRData.id;
                      ObjAmed.recordtypeId = recordtypeIdValue[1].id;
                      //CorporateCommunication = ObjAmed;
                      listOfAmendment.add(ObjAmed);
                    }
            }
        if(listOfAmendment.size() ==0){
            CorporateCommunication = new Amendment__c();
            CorporateCommunication.Relationship_Type__c = 'Corporate Communication';
            CorporateCommunication.ServiceRequest__c = SRData.id;
            CorporateCommunication.recordtypeId = recordtypeIdValue[1].id;
            CorporateCommunication.Status__c = 'Draft';
            CorporateCommunication.Nationality_list__c ='UAE';
        }
    }
    @testVisible
    private boolean getRelationships(List<RecordType> recordtypeIdValue){
        //Get Relationships from the account related to the service request
        try{
            List<Amendment__c> amendmentList = new List<Amendment__c>();
            String recordTypeIdVal ='';
            for(Relationship__c relRec : [Select id,name,Relationship_Type__c,Object_Contact__c,Object_Contact__r.Email,Object_Contact__r.firstName,Object_Contact__r.LastName,Object_Contact__r.Phone,Object_Contact__r.MobilePhone from Relationship__c where Subject_Account__c =: SRData.Customer__c AND (Relationship_Type__c ='Has CEO/Regional Head/Chairman' OR Relationship_Type__c ='Has Corporate Communication')])
            {
                recordTypeIdVal = relRec.Relationship_Type__c =='Has CEO/Regional Head/Chairman' ? recordtypeIdValue[0].id :recordtypeIdValue[1].id;
                
                amendmentList.add(new Amendment__c(Amendment_Type__c=relRec.Relationship_Type__c,Relationship_Type__c = relRec.Relationship_Type_formula__c,Status__c='Draft',ServiceRequest__c= SRData.id,recordtypeId =recordTypeIdVal));
            }
            if(amendmentList != Null && amendmentList.size() > 0)
            {
                insert amendmentList;
                return true;
            }
            return false;
        }catch(Exception ex){
            return false;
        }
    }
}