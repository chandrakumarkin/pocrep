@isTest
public class FreeholdTransferRegistrationExtCrlTest {
    public static testMethod void TestMethod1(){
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;
       

        
        Account accountObj = new Account ();
        accountObj.Name='Test Public Account Limited';
        accountObj.Trade_Name__c = 'Test Public Account Limited';
        Insert accountObj;
       
        Contact con = new Contact(LastName ='testCon',AccountId = accountObj.Id);
        insert con;  
                  
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert user;
        
        Service_request__c servicerequestObj = new Service_request__c (); 
        servicerequestObj.RecordTypeId=Schema.SObjectType.Service_request__c.getRecordTypeInfosByDeveloperName().get('Freehold_Transfer_Registration').getRecordTypeId();
        
        insert servicerequestObj;
    
        Amendment__c amendmentObj = new Amendment__c();
        amendmentObj.RecordTypeId=Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Individual_Seller').getRecordTypeId();
        amendmentObj.Amendment_Type__c='Seller';
        amendmentObj.ServiceRequest__c = servicerequestObj.Id;
        PageReference pageRef = Page.FreeholdTransferRegistration;
        
    
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('RecordType',servicerequestObj.RecordTypeId);
        
        Test.Starttest();
        
        Apexpages.StandardController sc = new Apexpages.StandardController(servicerequestObj);
        FreeholdTransferRegistrationExtCrl ext = new  FreeholdTransferRegistrationExtCrl(sc);         
        ext.SaveRecord();

        Test.StopTest();
        
    }
    public static testMethod void TestMethod_2(){
        
        Service_request__C ServicerequestObj = new Service_request__C (); 

        PageReference pageRef = Page.FreeholdTransferRegistration;
        
        Test.setCurrentPage(pageRef);
        
        Apexpages.StandardController sc = new Apexpages.StandardController(servicerequestObj);
        FreeholdTransferRegistrationExtCrl ext = new  FreeholdTransferRegistrationExtCrl(sc);        
    }
 }