/*
    Author      : Durga Prasad
    Date        : 15-Dec-2019
    Description : Custom code to check the atleast one Company name to be marked as 
                  Approved on Company Name check SR Approval
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_EntityNameSRValidation implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
            set<string> setSRIDs = new set<string>();
            setSRIDs.add(stp.HexaBPM__SR__c);
            if(stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c!=null)
                setSRIDs.add(stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c);
            boolean hasApprovedName = false;
            for(Company_Name__c CN:[Select Id from Company_Name__c where Application__c IN:setSRIDs and Status__c='Approved' limit 1]){
                hasApprovedName = true;
            }
            if(!hasApprovedName)
                strResult = 'Please mark atleast one company name as approved';
        }
        return strResult;
    }
}