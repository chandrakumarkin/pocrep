global class TempSalesforceStepDataBatch implements 
    Database.Batchable<sObject>, Database.Stateful , schedulable {
     
    global void execute(SchedulableContext ctx) {
        TempSalesforceStepDataBatch bachable = new TempSalesforceStepDataBatch();
        database.executeBatch(bachable);
    }
    // instance member to retain state across transactions
    global Integer recordsProcessed = 0;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'Select id,name,Step_Id__c,Salesforce_Step_Unique_Identifier__c from Temp_SAP_Step_Data__c'
        );
    }
    global void execute(Database.BatchableContext bc, List<Temp_SAP_Step_Data__c> TempSAPStepData){
        // Fetch the assiciated records from Step object and insert into the Temp_Salesforce_Step_Data__c
        Set<String> stepIds = new Set<String>();
        for(Temp_SAP_Step_Data__c tempStep : TempSAPStepData){
            stepIds.add(tempStep.Salesforce_Step_Unique_Identifier__c);
        }
        //system.assertEquals(null, stepIds);
        List<Temp_Salesforce_Step_Data__c> testSalesforceStepDataList = new List<Temp_Salesforce_Step_Data__c>();
        for(Step__c stepRec : [Select Completion_Date__c,Fine_Amount__c,ID_Start_Date__c,SAP_MFDAT__c,SAP_MFSTM__c,SAP_MDATE__c,Push_to_SAP__c,
                                        SAP_ACRES__c,SAP_AEDAT__c,SAP_AENAM__c,SAP_AETIM__c,SAP_BANFN__c,SAP_BIOMT__c,SAP_BNFPO__c,SAP_CMAIL__c,
                                        SAP_CRMAC__c,SAP_CTEXT__c,SAP_CTOTL__c,SAP_DOCCO__c,SAP_DOCCR__c,SAP_DOCDL__c,SAP_DOCRC__c,SAP_EACTV__c,
                                        SAP_EMAIL__c,SAP_ERDAT__c,SAP_ERNAM__c,SAP_ERTIM__c,SAP_ETIME__c,SAP_FINES__c,SAP_FMAIL__c,SAP_GSDAT__c,
                                        SAP_GSTIM__c,SAP_IDDES__c,SAP_IDEDT__c,SAP_IDSDT__c,SAP_IDTYP__c,SAP_MATNR__c,SAP_MEDIC__c,SAP_MEDRT__c,
                                        SAP_MEDST__c,SAP_NMAIL__c,SAP_POSNR__c,SAP_RESCH__c,SAP_RMATN__c,SAP_Seq__c,SAP_STIME__c,SAP_TMAIL__c,
                                        SAP_UNQNO__c,Status__c,Status__r.ID,Step_Id__c,Step_Name__c,Step_Template__c,UID_Number__c,Updated_from_SAP__c 
                                        from Step__c 
                                        where Step_Id__c IN :stepIds]){
            
            Temp_Salesforce_Step_Data__c salesforceTempStep         =       new Temp_Salesforce_Step_Data__c();
            salesforceTempStep.Completion_Date__c                   =       stepRec.Completion_Date__c;
            salesforceTempStep.Fine_Amount__c                       =       stepRec.Fine_Amount__c;
            //salesforceTempStep.ID_Number__c                       =       stepRec.ID_Number__c;
            salesforceTempStep.ID_Start_Date__c                     =       stepRec.ID_Start_Date__c;
            salesforceTempStep.Medical_First_Scheduled_Date__c      =       stepRec.SAP_MFDAT__c;
            salesforceTempStep.Medical_First_Scheduled_Time__c      =       stepRec.SAP_MFSTM__c;
            salesforceTempStep.Medical_Scheduled_Date__c            =       stepRec.SAP_MDATE__c != NULL ? setStringToDateFormat(stepRec.SAP_MDATE__c) : NULL; //stepRec.SAP_MDATE__c;
            //salesforceTempStep.Owner_ID__c                        =       stepRec.Owner_ID__c;     
            salesforceTempStep.Push_to_SAP__c                       =       stepRec.Push_to_SAP__c;
            salesforceTempStep.SAP_ACRES__c                         =       stepRec.SAP_ACRES__c;     
            salesforceTempStep.SAP_AEDAT__c                         =       stepRec.SAP_AEDAT__c;      
            salesforceTempStep.SAP_AENAM__c                         =       stepRec.SAP_AENAM__c;      
            salesforceTempStep.SAP_AETIM__c                         =       stepRec.SAP_AETIM__c;      
            salesforceTempStep.SAP_BANFN__c                         =       stepRec.SAP_BANFN__c;      
            salesforceTempStep.SAP_BIOMT__c                         =       stepRec.SAP_BIOMT__c;      
            salesforceTempStep.SAP_BNFPO__c                         =       stepRec.SAP_BNFPO__c;      
            salesforceTempStep.SAP_CMAIL__c                         =       stepRec.SAP_CMAIL__c;      
            salesforceTempStep.SAP_CRMAC__c                         =       stepRec.SAP_CRMAC__c;      
            salesforceTempStep.SAP_CTEXT__c                         =       stepRec.SAP_CTEXT__c;      
            salesforceTempStep.SAP_CTOTL__c                         =       stepRec.SAP_CTOTL__c;      
            salesforceTempStep.SAP_DOCCO__c                         =       stepRec.SAP_DOCCO__c;      
            salesforceTempStep.SAP_DOCCR__c                         =       stepRec.SAP_DOCCR__c;      
            salesforceTempStep.SAP_DOCDL__c                         =       stepRec.SAP_DOCDL__c;      
            salesforceTempStep.SAP_DOCRC__c                         =       stepRec.SAP_DOCRC__c;      
            salesforceTempStep.SAP_EACTV__c                         =       stepRec.SAP_EACTV__c;      
            salesforceTempStep.SAP_EMAIL__c                         =       stepRec.SAP_EMAIL__c;      
            salesforceTempStep.SAP_ERDAT__c                         =       stepRec.SAP_ERDAT__c;      
            salesforceTempStep.SAP_ERNAM__c                         =       stepRec.SAP_ERNAM__c;      
            salesforceTempStep.SAP_ERTIM__c                         =       stepRec.SAP_ERTIM__c;      
            salesforceTempStep.SAP_ETIME__c                         =       stepRec.SAP_ETIME__c;      
            salesforceTempStep.SAP_FINES__c                         =       stepRec.SAP_FINES__c;      
            salesforceTempStep.SAP_FMAIL__c                         =       stepRec.SAP_FMAIL__c;      
            salesforceTempStep.SAP_GSDAT__c                         =       stepRec.SAP_GSDAT__c;      
            salesforceTempStep.SAP_GSTIM__c                         =       stepRec.SAP_GSTIM__c;      
            salesforceTempStep.SAP_IDDES__c                         =       stepRec.SAP_IDDES__c;      
            salesforceTempStep.SAP_IDEDT__c                         =       stepRec.SAP_IDEDT__c;      
            salesforceTempStep.SAP_IDSDT__c                         =       stepRec.SAP_IDSDT__c;      
            salesforceTempStep.SAP_IDTYP__c                         =       stepRec.SAP_IDTYP__c;      
            salesforceTempStep.SAP_MATNR__c                         =       stepRec.SAP_MATNR__c;      
            salesforceTempStep.SAP_MEDIC__c                         =       stepRec.SAP_MEDIC__c;      
            salesforceTempStep.SAP_MEDRT__c                         =       stepRec.SAP_MEDRT__c;      
            salesforceTempStep.SAP_MEDST__c                         =       stepRec.SAP_MEDST__c;      
            salesforceTempStep.SAP_NMAIL__c                         =       stepRec.SAP_NMAIL__c;      
            salesforceTempStep.SAP_POSNR__c                         =       stepRec.SAP_POSNR__c;      
            salesforceTempStep.SAP_RESCH__c                         =       stepRec.SAP_RESCH__c;      
            salesforceTempStep.SAP_RMATN__c                         =       stepRec.SAP_RMATN__c;      
            salesforceTempStep.SAP_Seq__c                           =       stepRec.SAP_Seq__c;        
            salesforceTempStep.SAP_STIME__c                         =       stepRec.SAP_STIME__c;       
            salesforceTempStep.SAP_TMAIL__c                         =       stepRec.SAP_TMAIL__c;       
            salesforceTempStep.SAP_UNQNO__c                         =       stepRec.SAP_UNQNO__c;       
            salesforceTempStep.Status__c                            =       stepRec.Status__c;          
            salesforceTempStep.Status_ID__c                         =       stepRec.status__r.ID;       
            salesforceTempStep.Step_Id__c                           =       stepRec.Step_Id__c;         
            salesforceTempStep.Step_Name__c                         =       stepRec.Step_Name__c;       
            salesforceTempStep.Step_Template__c                     =       stepRec.Step_Template__c;   
            salesforceTempStep.Step_Template_Id__c                  =       stepRec.Step_Template__c;
            salesforceTempStep.UID_Number__c                        =       stepRec.UID_Number__c;      
            salesforceTempStep.Updated_from_SAP__c                  =       stepRec.Updated_from_SAP__c;
            
            testSalesforceStepDataList.add(salesforceTempStep);
        }
        insert testSalesforceStepDataList;
    }    
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed. Shazam!');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()];
        // call some utility to send email
        //EmailUtils.sendMessage(job, recordsProcessed);
    } 
    global Date setStringToDateFormat(String myDate) {
        String[] myDateOnly = myDate.split(' ');
        String[] strDate = myDateOnly[0].split('/');
        Integer myIntDate = integer.valueOf(strDate[1]);
        Integer myIntMonth = integer.valueOf(strDate[0]);
        Integer myIntYear = integer.valueOf(strDate[2]);
        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
        return d;
    }   
}