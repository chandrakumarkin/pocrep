public class visaExceptionTriggerHandler {

  public static void updateAccountExceptionQuota (Visa_Exception__c  visaException){
  
  Account account = new Account();
  
  Account acc  =[select id,Exception_Visa_Quota__c from Account where id = :visaException.Account__c ];
        if(visaException.Exception_Visa_Quota__c >0){
        
          account.id = acc.id;
          account.Exception_Visa_Quota__c  = acc.Exception_Visa_Quota__c + visaException.Exception_Visa_Quota__c ;
          account.is_Visa_Exception_Enabled__c  = false;
        
        }
        
        update Account;
    
    }
  
  
  }