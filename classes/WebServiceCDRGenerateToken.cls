/******************************************************************************************************
*  Author   : Kumar Utkarsh
*  Date     : 27-May-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    27-May-2021  Utkarsh         Created
* This class contains logic oto fetch the access token of CDR classes.
*******************************************************************************************************/
public class WebServiceCDRGenerateToken {
    
    public class responseAuthToken{
        public Boolean isSuccess;
        public Object errorMessage;
        public BearerToken bearerToken;
    }
    
    public Class BearerToken{
        public String expiresOn;
        public String token;
    }
    
    //fetch the Bearer Token
    public Static String cdrAuthToken(){
        String token;
        responseAuthToken bearerTokenList = new responseAuthToken();
        HttpRequest AuthRequest = new HttpRequest();
        AuthRequest.setEndpoint(System.label.Cdr_Auth_Token_Generation_URL); 
        AuthRequest.setMethod('POST');
        AuthRequest.setHeader('accept','text/plain');
        AuthRequest.setHeader('Content-Type','application/json');
        AuthRequest.setTimeout(120000);
        JSONGenerator jsonObj = JSON.createGenerator(true);
        jsonObj.writeStartObject();
        jsonObj.writeStringField('clientId', System.label.Cdr_Client_Id); 
        jsonObj.writeStringField('clientSecret', System.label.Cdr_ClientSecret);
        jsonObj.writeEndObject();
        String finalJSON = jsonObj.getAsString();
        System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
        AuthRequest.setBody(jsonObj.getAsString());
        if(!Test.isRunningTest())
        {
            System.HttpResponse response = new System.Http().send(AuthRequest);
            system.debug('Response ' +response.getBody()); 
            if((response.getStatusCode() == 200)){
                bearerTokenList = (responseAuthToken) System.JSON.deserialize(response.getBody(), responseAuthToken.class);
                system.debug('token'+bearerTokenList);
                if(bearerTokenList.bearerToken != null){
                    if(bearerTokenList.bearerToken.token != null && bearerTokenList.bearerToken.token != '')
                        token = bearerTokenList.bearerToken.token;
                }
            }
        }
        else
        {
            token = 'Test';
        }
        System.debug('token:'+token);
        return token;
        
    }
    
}