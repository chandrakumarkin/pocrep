@isTest
public class OB_WelcomeTextHelperTest 
{
    /*
        static testMethod void welcomeTextHelperTest() 
        {
            // create account
            List<Account> insertNewAccounts = new List<Account>();
            insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
            insert insertNewAccounts;
             //create contact
            List<Contact> insertNewContacts = new List<Contact>();
            
            test.startTest();
           
            insertNewContacts = OB_TestDataFactory.createContacts(1, insertNewAccounts);
            insertNewContacts[0].Send_Portal_Login_Link__c = 'Yes';
            insert insertNewContacts;
            
            test.stopTest();
            List<User> portalUser = [SELECT Id,OB_Registration_Source__c 
                                       FROM User 
                                       WHERE ContactId = :insertNewContacts[0].Id];
                system.runAs(portalUser[0]){
                    OB_WelcomeTextHelper objOB_WelcomeTextHelper = new OB_WelcomeTextHelper();
                    objOB_WelcomeTextHelper.getwelcomeText();
                }
        }
	*/
    
    static testMethod void welcomeTextHelperTest()
    {
         
        
        	Set<String> customerUserTypes = new Set<String> {'CSPLiteUser', 'PowerPartner', 'PowerCustomerSuccess',   'CustomerSuccess'};
            Account acc = new Account (
            Name = 'newAcc1'
            );  
            insert acc;
            Contact con = new Contact (
            AccountId = acc.id,
            LastName = 'portalTestUser'
            );
            insert con;
            Profile p = [select Id,name from Profile where UserType in :customerUserTypes limit 1];
             
            User newUser = new User(
            profileId = p.id,
            username = 'newUser@yahoo.com',
            email = 'pb@ff.com',
            emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',
            languagelocalekey = 'en_US',
            timezonesidkey = 'America/Los_Angeles',
            alias='nuser',
            lastname='lastname',
            //Contact_Owner_Phone__c = '+971589736044',
            contactId = con.id
            );
            insert newUser;
        
        
        
        	Test.startTest();
         
             OB_WelcomeTextHelper obweltxt = new OB_WelcomeTextHelper();
             obweltxt.UID = newUser.Id;
             obweltxt.getwelcomeText();
                        
                   
                
    		Test.stopTest();  
     
    }
}