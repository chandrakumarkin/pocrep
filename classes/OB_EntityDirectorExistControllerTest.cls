@isTest
public class OB_EntityDirectorExistControllerTest 
{
    
    private static testmethod void deleteAmedTest()
    {
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Identify ultimate beneficial owners';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;

        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Non Profit Incorporated Organisation (NPIO)','Services'}, 
                                                   new List<string>{'Non Profit Incorporated Organisation (NPIO)','Company'}, 
                                                   new List<string>{'NPIO','Recognized Company'});
       
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[2].id;
        insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[2].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule.HexaBPM__Section_Detail__c = sec.id;
        objPgNavRule.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule.HexaBPM__Section__c=section.id;
        objPgNavRule.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule;
        
        HexaBPM__Page_Flow_Condition__c ObjPgFlowCond = new HexaBPM__Page_Flow_Condition__c();
        ObjPgFlowCond.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        ObjPgFlowCond.HexaBPM__Field_Name__c = 'Id';
        ObjPgFlowCond.HexaBPM__Operator__c = '!='; 
        ObjPgFlowCond.HexaBPM__Value__c = 'null';
        ObjPgFlowCond.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id; 
        insert ObjPgFlowCond;
        
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id;
        insert ObjPgFlowAction;
        
        HexaBPM__Document_Master__c doc= new HexaBPM__Document_Master__c();
        doc.HexaBPM__Available_to_client__c = true;
        doc.HexaBPM__Code__c = 'UBO_NPIO';
        doc.Download_URL__c = 'test';
        insert doc;
        String BCRecTypId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = BCRecTypId;
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Permanent_Native_Country__c = 'United Arab Emirates';
        objAmendment.Role__c = 'Authorised Signatory;Director;UBO';
        objAmendment.Registration_No__c = '1234';
        objAmendment.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment);

        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = BCRecTypId;
        objAmendment1.Nationality_list__c = 'BCRecTypId';
        objAmendment1.Registration_No__c = '1234';
        objAmendment1.Role__c = 'Authorised Signatory;Director;UBO';
        objAmendment1.Permanent_Native_Country__c = 'United Arab Emirates';
        objAmendment1.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment1.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment1);
        insert listAmendment;
        system.debug('@@@@@@@@@@ listAmendment test data '+listAmendment );

        test.startTest();
        OB_EntityDirectorExistController.RequestWrapper reqWrapper = new OB_EntityDirectorExistController.RequestWrapper();
        OB_EntityDirectorExistController.AmendmentWrapper dummyAmed = new  OB_EntityDirectorExistController.AmendmentWrapper();
        dummyAmed.amedID = listAmendment[0].Id;
        dummyAmed.isIndividual = false;
        dummyAmed.isBodyCorporate = false;
        dummyAmed.lookupLabel = '';  
        
        reqWrapper.flowId = listPageFlow[0].Id;
        reqWrapper.pageId = listPage[0].Id;
        reqWrapper.srId = insertNewSRs[0].Id;
         //reqWrapper.fileId = '';
        //reqWrapper.amendmentID = listAmendment[0].Id;
        reqWrapper.amedWrap = new OB_EntityDirectorExistController.AmendmentWrapper();
        reqWrapper.amedWrap.amedObj = listAmendment[0];
        reqWrapper.amedId = listAmendment[0].Id;
        OB_EntityDirectorExistController.deleteAmed(JSON.serialize(reqWrapper));
        
        
       
      /*
        String requestString = JSON.serialize(reqWrapper);
        OB_EntityDirectorContainerController.ResponseWrapper respWrap = OB_EntityDirectorContainerController.getExistingAmendment(requestString);
        OB_EntityDirectorContainerController.getButtonAction(insertNewSRs[0].Id,sec.Id,listPage[0].Id);
        
      
            system.debug('=get existing==srwrao=='+respWrap.srWrap.srObj);
            reqWrapper.typeOfInformation ='Foundation or its Council' ;
            reqWrapper.srWrap = respWrap.srWrap;
    
            reqWrapper.recordtypename= 'Individual';
            OB_EntityUBOContainerController.ResponseWrapper respWrap1 = OB_EntityUBOContainerController.initAmendmentDB(JSON.serialize(reqWrapper));
            
            system.debug('====test=amend=='+respWrap1.amedWrap.amedObj);
            respWrap1.amedWrap.amedObj.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council' ;
            respWrap1.amedWrap.amedObj.Passport_No__c = '1234';
            respWrap1.amedWrap.amedObj.Nationality_list__c = 'India';
            reqWrapper.amedWrap = respWrap1.amedWrap;
            OB_EntityUBOContainerController.getButtonAction(insertNewSRs[0].Id,sec.Id,listPage[0].Id,respWrap.srWrap.srObj);
            
            OB_EntityUBOContainerController.ResponseWrapper finalRes = OB_EntityUBOContainerController.getExistingAmendment(requestString);
            reqWrapper.typeOfInformation ='Foundation or its Council' ;
            reqWrapper.srWrap = finalRes.srWrap;
    
            reqWrapper.recordtypename= 'Individual';
            OB_EntityUBOContainerController.ResponseWrapper finalRes1 = OB_EntityUBOContainerController.initAmendmentDB(JSON.serialize(reqWrapper));
            system.debug('====test=amend=='+respWrap1.amedWrap.amedObj);
            finalRes1.amedWrap.amedObj.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council' ;
            finalRes1.amedWrap.amedObj.Passport_No__c = '1234';
            finalRes1.amedWrap.amedObj.Nationality_list__c = 'India';
            reqWrapper.amedWrap = finalRes1.amedWrap;
            finalRes1 = OB_EntityUBOContainerController.onAmedSaveDB(JSON.serialize(reqWrapper));
		*/

        test.stopTest();
        
    }

}