/*
 	Author		:Rajil Ravindran
	Date		:01-Sep-2019
	Description	:TriggerHandler for TrainingMasterTrigger
				
*/
public class TrainingRequestTriggerHandler {
    public static void Execute_AI(list<Training_Request__c> TriggerNew){
        rollupData(TriggerNew);
    }
    public static void Execute_AUnDelete(list<Training_Request__c> TriggerNew){
        rollupData(TriggerNew);
    }
    public static void Execute_AD(list<Training_Request__c> TriggerOld){
        rollupData(TriggerOld);
    }
    public static void Execute_AU(list<Training_Request__c> TriggerNew){
        rollupData(TriggerNew);
    }
    public static void rollupData(list<Training_Request__c> lstTrainingRequest){
        Set<Id> setTrainingMaster = new Set<Id>();
        for(Training_Request__c tr:lstTrainingRequest){
            if(tr.TrainingMaster__c != null){
                setTrainingMaster.add(tr.TrainingMaster__c);
            }
        }
        AggregateResult[] groupedResults = [SELECT count(id)seatCount, TrainingMaster__c FROM Training_Request__c 
                                             WHERE TrainingMaster__c in :setTrainingMaster 
                                             and status__c in ('Accepted','Submitted','Invitation Sent','Rescheduled') 
                                             GROUP BY TrainingMaster__c];
		Map<id,Training_Master__c> Training_MasterMap = new Map<Id,Training_Master__c>();
        for(Training_Master__c tm : [SELECT id, Seats_Occupied__c FROM Training_Master__c WHERE Id in :setTrainingMaster]){
            tm.Seats_Occupied__c = 0;
            Training_MasterMap.put(tm.id,tm);
        }
        
        Id trainingMasterId;
        
        for(AggregateResult ar: groupedResults) {
            trainingMasterId= (Id) ar.get('TrainingMaster__c');
            if(trainingMasterId != null)
          		Training_MasterMap.get(trainingMasterId).Seats_Occupied__c = (Integer)ar.get('seatCount');
        }

	if(Training_MasterMap != null)
        update Training_MasterMap.values();
    }
}