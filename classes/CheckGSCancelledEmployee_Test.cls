@isTest
public class CheckGSCancelledEmployee_Test{
     
     static testMethod void testGSCancelledEmployee_Draft()
     {
         
          Account acc = new Account();
          acc.Name = 'Test Account';
          insert acc;
          
           Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
        Service_Request__c sr = new Service_Request__c();
         sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-Cancellation').getRecordTypeId();
         sr.Customer__c = acc.ID;
         sr.Contact__c = objContact.ID;
         
        insert sr;
      
      
      SR_Status__c srStatus = new SR_Status__c();
      srStatus.Code__c = 'PROCESS_COMPLETED';
      srStatus.Name = 'Process Completed';
      insert srStatus;
      
      sr.External_SR_Status__c = srStatus.ID;
      sr.Internal_SR_Status__c = srStatus.ID;
      sr.Completed_Date_Time__c = DateTime.Now();
      update sr;
      
      SR_Template__c srTemp = new SR_Template__c ();
      srTemp.Name = 'Car Parking Process';
      srTemp.SR_RecordType_API_Name__c = 'Car Parking';
      insert srTemp;
      
      SR_Status__c srStatus1 = new SR_Status__c ();
      srStatus1 .Code__c = 'Submitted';
      srStatus1 .Name = 'Submitted';
      insert srStatus1 ;
      
      String cronExpr = '0 0 0 15 3 ? 2022';
      test.startTest();
       System.schedule('test', cronExpr , new Scheduled_CheckGSCancelledEmployee());
       
      test.stopTest();
       
      
     }


}