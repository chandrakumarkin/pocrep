/*
    Created By  : Suchita- DIFC on 22 Feb,2021
    Description : Test class for CC_RORPCodeCls
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 ----------------------------------------------------------------------------------------
*/
@isTest(seealldata=false)
private class CC_RORPCodeClsTest {


    public static Account objAccount;
    public static Service_Request__c objSR;
    public static Step__c objStep;
    public static Amendment__c amndRemCon;
    public static Unit__c objUnit;
    public static Lease__c objLease;
    public static Tenancy__c objTen;
    public static Building__c objBuilding;
    public static Amendment__c objAmendLandlord;
    public static Amendment__c objAmendTenant;
    public static Contact objContact;
    public static Lease_Partner__c objLP;
    public static map<string,string> mapRecordType = new map<string,string>();

    
    static testMethod void LeaseApprovalValidationsTest() { 

        //init();
          /*
        mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration' ,  'Landlord' ,'Occupant' ,'Individual_Tenant' , 'RORP_Contact' , 'Body_Corporate_Tenant','Lease_Unit')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        
        
        
        Account objAccount2  = new Account();
        objAccount2.Name = 'Test Account123';
        objAccount2.Place_of_Registration__c ='Pakistan';
        objAccount2.ROC_Status__c = 'Under Formation';
        objAccount2.BP_No__c='0000008978';
        
        insert objAccount2;  
        
        License__c objLicense = new License__c();
        objLicense.Account__c = objAccount2.Id;
        objLicense.License_Expiry_Date__c = system.today().addYears(2);
        objLicense.Status__c = 'Active';
        insert objLicense;

        Building__c objBuilding2 = new Building__c();
        objBuilding2.Name = 'Central Park Towers 2';
        objBuilding2.Company_Code__c = '5300';
        objBuilding2.Permit_Date__c = Date.today();
        insert objBuilding2;        
        
        Unit__c objUnit2 = new Unit__c();
        objUnit2.Name='102';
        objUnit2.Unit_Usage_Type__c = 'Residential 3 Bedroom';
        objUnit2.Building__c = objBuilding2.id;
        objUnit2.Building_Name__c = 'Central Park Towers 2';
        objUnit2.Unit_Square_Feet__c = 2300;
        insert objUnit2;  
    
        Lease__c leaseSR = new Lease__c(Type__c = 'Leased',Status__c='Active',SAP_Lease_Number__c='01012');
        insert LeaseSR;
        
    
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount2.Id;
        objSR2.RecordTypeId = mapRecordType.get('Lease_Registration');
        objSR2.Unit__c = objUnit2.Id;
        objSR2.Building__c = objBuilding2.Id;
        objSR2.Type_of_Lease__c = 'Lease';
        objSR2.SR_Group__c = 'RORP';
        objSR2.Lease_Contract_No__c = '01012';
        objSR2.Service_Category__c ='New';
        objSR2.Lease_Commencement_Date__c = Date.today();
        objSR2.Lease_Expiry_Date__c = Date.today().adddays(61); 
        objSR2.SAP_MATNR__c = '23';
        insert objSR2;
        
        Amendment__c amndUnit = new Amendment__c (Company_Name__c = 'Test Company Two',Unit__c = objUnit2.ID, RecordTypeId=mapRecordType.get('Lease_Unit'),ServiceRequest__c=objSR2.ID ,  Amendment_Type__c = 'Unit', Status__c = 'Active' );
        insert amndUnit;
        
        list<Lease__c> lstLeases = new list<Lease__c>();
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount2.Id;
        objLease.Status__c = 'Future Lease';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Purchased';
        objLease.Name = '1345';
        objLease.Lease_Types__c = 'Retail Lease';
        lstLeases.add( objLease );
        
        objLease = new Lease__c();
        objLease.Account__c = objAccount2.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Purchased';
        objLease.Name = '1345';
        objLease.Lease_Types__c = 'Retail Lease';
        lstLeases.add( objLease );
        insert lstLeases;
        
        list<Lease_Partner__c> lstLPs = new list<Lease_Partner__c>();
        
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = lstLeases[0].Id;
        objLP.Account__c = objAccount2.Id;
        objLP.Unit__c = objUnit2.Id;
        objLP.SAP_PARTNER__c = '12345';
        objLP.Start_date__c = system.today();
        objLP.End_Date__c = system.today().addYears(3);
        objLP.Square_Feet__c = '123456';
        objLP.Lease_Types__c = 'Retail Lease';
        objLP.Industry__c = 'Kiosik';
        objLP.Type_of_Lease__c = 'Leased';
        objLP.Status__c = 'Active';
        lstLPs.add(objLP);
        
        objLP = new Lease_Partner__c();
        objLP.Lease__c = lstLeases[1].Id;
        //objLP.Account__c = objAccount.Id;
        objLP.Unit__c = objUnit2.Id;
        objLP.SAP_PARTNER__c = '12345';
        objLP.Is_6_Series__c = true;
        objLP.Start_date__c = system.today();
        objLP.End_Date__c = system.today().addYears(3);
        objLP.Square_Feet__c = '123456';
        objLP.Lease_Types__c = 'Retail Lease';
        objLP.Industry__c = 'Kiosik';
        objLP.Type_of_Lease__c = 'Leased';
        objLP.Status__c = 'Active';
        lstLPs.add(objLP);
        
        insert lstLPs;
        
        Amendment__c objTenant = new Amendment__c();
        
        objTenant.ServiceRequest__c = objSR2.ID;
        objTenant.RecordTypeId = mapRecordType.get('Body_Corporate_Tenant');
        objTenant.Amendment_Type__c = 'Tenant';
        
        objTenant.Is_Primary__c = true;
        objTenant.Manager__c = true;
        
        objTenant.Company_Name__c = 'Test';
        objTenant.Shareholder_Company__c = objAccount2.ID;
        objTenant.IssuingAuthority__c = 'DIFC - Under Formation';
        objTenant.Office_Unit_Number__c = '123';
        objTenant.Non_Reg_Company__c = objAccount2.ID;
        objTenant.Given_Name__c = 'RB';
        insert objTenant;
        
        
        
        
        objAccount2.Active_License__c = objLicense.Id;
        objAccount2.ROC_Status__c = 'Active';
        update objAccount2;
 
        Step__c objStep2  = new Step__c();
        objStep2.SR__c = objSR2.Id;
        insert objStep2;
        
        for(Step__c obj : [select Id,Name,SR__r.Id,SR__r.Name,SR__r.Customer__c,SR__r.Customer__r.Name,SR__r.Unit__c,SR__r.Building__c,SR__r.Type_of_Lease__c,SR__r.SR_Group__c,SR__r.Lease_Contract_No__c,SR__r.Service_Category__c,
                SR__r.Lease_Commencement_Date__c,SR__r.Lease_Expiry_Date__c,SR__r.SAP_MATNR__c,SR__r.Record_Type_Name__c ,SR__r.External_Status_Name__c from Step__c where Id=:objStep2.Id
                ]){
            objStep2 = obj;
        }
       
        //objstep2.SR__r = objSR2;
        
        
        objAmendLandlord = new Amendment__c (RecordTypeId=mapRecordType.get('Landlord'),ServiceRequest__c=objSR2.id , Emirates_ID_Number__c = '32323' ,  Amendment_Type__c = 'Landlord' ,Type_of_Landlord__c = 'Individual', Given_Name__c = 'shab' ,Family_Name__c = 'ahmed',Passport_No__c ='123490' , Nationality_list__c = 'Pakistan');
        //insert objAmendLandlord;
        objAmendTenant = new Amendment__c (RecordTypeId=mapRecordType.get('Individual_Tenant'),ServiceRequest__c=objSR2.id , Emirates_ID_Number__c = '3232334',  Amendment_Type__c = 'Tenant' , Given_Name__c = 'shab2' ,Family_Name__c = 'ahmed2',Passport_No__c ='87123490' , Nationality_list__c = 'Pakistan');
        //insert objAmendTenant; 

        Amendment__c objAmendTenantBodyCorp = new Amendment__c (Company_Name__c = 'John Enterprise' , 
        Issuing_Authority_Other__c = 'JAFZA' , CL_Certificate_No__c = '3433' ,
        RecordTypeId=mapRecordType.get('Body_Corporate_Tenant'),ServiceRequest__c = objSR2.id , Emirates_ID_Number__c = '232323342', 
         Amendment_Type__c = 'Tenant' , Given_Name__c = 'shab2' ,Family_Name__c = 'ahmed2',Passport_No__c ='87123490' , Nationality_list__c = 'Pakistan',
         First_Name__c = 'abc' , Last_Name__c = 'adf' , Passport_No_Occupant__c = 'a3fdf' , Nationality_Occupant__c = 'Pakistan',Permanent_Native_Country__c='Pakistan');
        insert new List<Amendment__c>{objAmendLandlord,objAmendTenant,objAmendTenantBodyCorp}; 
        
        Lease_Partner__c objLP1 = new Lease_Partner__c (Unit__c=objUnit2.id,Lease__c=LeaseSR.id);
        insert objLP1;
        Step__c objTest = objstep2;
        
        
        test.startTest();
        try{CC_RORPCodeCls.createTenants(objstep2);}catch(Exception ex){}
        test.stopTest();
       
        */
       
    }
    
    static testMethod void LeaseApprovalValidationsTest2() { 

        //init();
        
        mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration' ,  'Landlord' ,'Occupant' ,'Individual_Tenant' , 'RORP_Contact' , 'Body_Corporate_Tenant','Lease_Unit')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        
        
        
        Account objAccount2  = new Account();
        objAccount2.Name = 'Test Account123';
        objAccount2.Place_of_Registration__c ='Pakistan';
        objAccount2.ROC_Status__c = 'Under Formation';
        objAccount2.BP_No__c='0000008978';
        
        insert objAccount2;  
        
        License__c objLicense = new License__c();
        objLicense.Account__c = objAccount2.Id;
        objLicense.License_Expiry_Date__c = system.today().addYears(2);
        objLicense.Status__c = 'Active';
        insert objLicense;
        

        Building__c objBuilding2 = new Building__c();
        objBuilding2.Name = 'Central Park Towers 2';
        objBuilding2.Company_Code__c = '5300';
        objBuilding2.Permit_Date__c = Date.today();
        insert objBuilding2;        
        
        Unit__c objUnit2 = new Unit__c();
        objUnit2.Name='102';
        objUnit2.Unit_Usage_Type__c = 'Residential 3 Bedroom';
        objUnit2.Building__c = objBuilding2.id;
        objUnit2.Building_Name__c = 'Central Park Towers 2';
        objUnit2.Unit_Square_Feet__c = 2300;
        insert objUnit2;  
    
        Lease__c leaseSR = new Lease__c(Type__c = 'Leased',Status__c='Active',SAP_Lease_Number__c='01012');
        insert LeaseSR;
        
    
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount2.Id;
        objSR2.RecordTypeId = mapRecordType.get('Lease_Registration');
        objSR2.Unit__c = objUnit2.Id;
        objSR2.Building__c = objBuilding2.Id;
        objSR2.Type_of_Lease__c = 'Lease';
        objSR2.SR_Group__c = 'RORP';
        objSR2.Lease_Contract_No__c = '01012';
        objSR2.Service_Category__c ='New';
        objSR2.Lease_Commencement_Date__c = Date.today();
        objSR2.Lease_Expiry_Date__c = Date.today().adddays(61); 
        objSR2.SAP_MATNR__c = '23';
        insert objSR2;
        
        Amendment__c amndUnit = new Amendment__c (Company_Name__c = 'Test Company Two',Unit__c = objUnit2.ID, RecordTypeId=mapRecordType.get('Lease_Unit'),ServiceRequest__c=objSR2.ID ,  Amendment_Type__c = 'Unit', Status__c = 'Active' );
        insert amndUnit;
        
        list<Lease__c> lstLeases = new list<Lease__c>();
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount2.Id;
        objLease.Status__c = 'Future Lease';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Purchased';
        objLease.Name = '1345';
        objLease.Lease_Types__c = 'Retail Lease';
        lstLeases.add( objLease );
        
        objLease = new Lease__c();
        objLease.Account__c = objAccount2.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Purchased';
        objLease.Name = '1345';
        objLease.Lease_Types__c = 'Retail Lease';
        lstLeases.add( objLease );
        insert lstLeases;
        
        list<Lease_Partner__c> lstLPs = new list<Lease_Partner__c>();
        
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = lstLeases[0].Id;
        objLP.Account__c = objAccount2.Id;
        objLP.Unit__c = objUnit2.Id;
        objLP.SAP_PARTNER__c = '12345';
        objLP.Start_date__c = system.today();
        objLP.End_Date__c = system.today().addYears(3);
        objLP.Square_Feet__c = '123456';
        objLP.Lease_Types__c = 'Retail Lease';
        objLP.Industry__c = 'Kiosik';
        objLP.Type_of_Lease__c = 'Leased';
        objLP.Status__c = 'Active';
        lstLPs.add(objLP);
        
        objLP = new Lease_Partner__c();
        objLP.Lease__c = lstLeases[1].Id;
        //objLP.Account__c = objAccount.Id;
        objLP.Unit__c = objUnit2.Id;
        objLP.SAP_PARTNER__c = '12345';
        objLP.Is_6_Series__c = true;
        objLP.Start_date__c = system.today();
        objLP.End_Date__c = system.today().addYears(3);
        objLP.Square_Feet__c = '123456';
        objLP.Lease_Types__c = 'Retail Lease';
        objLP.Industry__c = 'Kiosik';
        objLP.Type_of_Lease__c = 'Leased';
        objLP.Status__c = 'Active';
        lstLPs.add(objLP);
        
        insert lstLPs;
        test.startTest();
        Amendment__c objTenant = new Amendment__c();
        
        objTenant.ServiceRequest__c = objSR2.ID;
        objTenant.RecordTypeId = mapRecordType.get('Body_Corporate_Tenant');
        objTenant.Amendment_Type__c = 'Tenant';
        
        objTenant.Is_Primary__c = true;
        objTenant.Manager__c = true;
        
        objTenant.Company_Name__c = 'Test';
        objTenant.Shareholder_Company__c = objAccount2.ID;
        objTenant.IssuingAuthority__c = 'DIFC - Under Formation';
        objTenant.Office_Unit_Number__c = '123';
        objTenant.Non_Reg_Company__c = objAccount2.ID;
        objTenant.Given_Name__c = 'RB';
        insert objTenant;
        
        
        
        
        objAccount2.Active_License__c = objLicense.Id;
        objAccount2.ROC_Status__c = 'Active';
        update objAccount2;
 
        Step__c objStep2  = new Step__c();
        objStep2.SR__c = objSR2.Id;
        insert objStep2;
       
        for(Step__c obj : [select Id,Name,SR__r.Id,SR__r.Name,SR__r.Customer__c,SR__r.Customer__r.Name,SR__r.Unit__c,SR__r.Building__c,SR__r.Type_of_Lease__c,SR__r.SR_Group__c,SR__r.Lease_Contract_No__c,SR__r.Service_Category__c,
                SR__r.Lease_Commencement_Date__c,SR__r.Lease_Expiry_Date__c,SR__r.SAP_MATNR__c,SR__r.Record_Type_Name__c ,SR__r.External_Status_Name__c from Step__c where Id=:objStep2.Id
                ]){
            objStep2 = obj;
        }
       
        //objstep2.SR__r = objSR2;
        
        
        objAmendLandlord = new Amendment__c (RecordTypeId=mapRecordType.get('Landlord'),ServiceRequest__c=objSR2.id , Emirates_ID_Number__c = '32323' ,  Amendment_Type__c = 'Landlord' ,Type_of_Landlord__c = 'Individual', Given_Name__c = 'shab' ,Family_Name__c = 'ahmed',Passport_No__c ='123490' , Nationality_list__c = 'Pakistan');
        //insert objAmendLandlord;
        objAmendTenant = new Amendment__c (RecordTypeId=mapRecordType.get('Individual_Tenant'),ServiceRequest__c=objSR2.id , Emirates_ID_Number__c = '3232334',  Amendment_Type__c = 'Tenant' , Given_Name__c = 'shab2' ,Family_Name__c = 'ahmed2',Passport_No__c ='87123490' , Nationality_list__c = 'Pakistan');
        //insert objAmendTenant; 

        Amendment__c objAmendTenantBodyCorp = new Amendment__c (Company_Name__c = 'John Enterprise' , 
        Issuing_Authority_Other__c = 'JAFZA' , CL_Certificate_No__c = '3433' ,
        RecordTypeId=mapRecordType.get('Body_Corporate_Tenant'),ServiceRequest__c = objSR2.id , Emirates_ID_Number__c = '232323342', 
         Amendment_Type__c = 'Tenant' , Given_Name__c = 'shab2' ,Family_Name__c = 'ahmed2',Passport_No__c ='87123490' , Nationality_list__c = 'Pakistan',
         First_Name__c = 'abc' , Last_Name__c = 'adf' , Passport_No_Occupant__c = 'a3fdf' , Nationality_Occupant__c = 'Pakistan',Permanent_Native_Country__c='Pakistan');
        insert new List<Amendment__c>{objAmendLandlord,objAmendTenant,objAmendTenantBodyCorp}; 
        
        Lease_Partner__c objLP1 = new Lease_Partner__c (Unit__c=objUnit2.id,Lease__c=LeaseSR.id);
        insert objLP1;
        Step__c objTest = objstep2;
        
      
        try{CC_RORPCodeCls.LeaseApprovalValidations(objTest);}catch(Exception ex){}
        try{CC_RORPCodeCls.LeaseTermination(objstep2);}catch(Exception ex){}
        try{CC_RORPCodeCls.UpdateSAPDate(objstep2);}catch(Exception ex){}
        try{CC_RORPCodeCls.UpdateLeaseSR(objstep2.SR__r.Customer__c);}catch(Exception ex){}
        try{CC_RORPCodeCls.SendRORPGeneratedDoc(objstep2);}catch(Exception ex){}
        test.stopTest();
        
    }
 }