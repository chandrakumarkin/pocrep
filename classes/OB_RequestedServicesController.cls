/*****************************************************************************************************************************
Created By  : Rachita - PWC November 2019
Description :  * Description: Get the list of service request of looged in user's account
               Search by service number, status and Request Type
--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No     Date              Updated By          Description
V1.01    8th-Jan-2020       Mudasir Wani        Added logic to show the requests for the Contractor login 
v1.2     5th-MAY-2020       Prateek Kadkol      added navURL logic
V1.3     13-June-2020       Arun Singh          Added logic to support Separate Services #10270
v1.4     18-July-2021       Arun Singh          #17470 
****************************************************************************************************************************/
public class OB_RequestedServicesController {

    // collection of both service request object
    public static List<sObject> sObjectList;
    public static Set<String> userAccessTypeSet;
    public static String requestType;
    // current logged in user account detail
    public static string userAccountID;
    
    //v1.3 added SeparateServices
    public static Boolean SeparateServices;
    
    public static string userID;
    public static Set<String> userRole;
    public static Boolean isContractorOnly;
    public static  List<Account> accupdateList;
    private static string userLoggedInAsValue;
    private static User userRecord;
    
    @AuraEnabled public static String getLoggedInUserAccountName(){
    
    /*Contains logged in user Id*/
    String userId = UserInfo.getUserId();
    
    /*Contains logged in user type*/
    String userType = UserInfo.getUserType();
    
    /*Contains logged in user contactID*/
    ID userContactId;
    String accountName;
    
    // query user information
    for (User usr: [Select contactid, Contact.AccountId, Contact.Account.Name from User where Id =: userId]) {
        if (usr.contactid != null) {
            userContactId = usr.contactid;
            if (usr.Contact.AccountId != null) {
                accountName = usr.Contact.Account.Name;
            }
        }
    }
    return accountName;
    }
    
    /*
     * To get the loggedIN user contact and account
     **/
    public static void getLoggedInUserAccount() {
    
        /*Contains logged in user Id*/
        userId = UserInfo.getUserId();
    
        /* isContractor*/
        isContractorOnly = false;
        
        //v1.3
        SeparateServices=false;// Normal list of service 
    
        /*Contains logged in user type*/
        String userType = UserInfo.getUserType();
    
        /*Contains logged in user contactID*/
        ID userContactId;
        userLoggedInAsValue = 'Client';
    
        // query user information
        //v1.3 added Contact.Account.Separate_Services__c
        for (User usr: [Select contactid,Separate_Services__c, Contact.AccountId, Contact.Account.Name,profileId,OB_Is_New_Community_Profile__c,
                contact.Account.RecordType.DeveloperName, Community_User_Role__c,
                contact.Contractor__c, contact.Contractor__r.RecordType.DeveloperName, contact.OB_Home_Page_Rendering__c, contact.Email, contact.Passport_No__c, contact.Nationality__c
                from User where Id =: userId
            ]) 
            {
                    userRecord = new User();
                    userRecord = usr;

                    if (usr.contactid != null) 
                    {
                        userContactId = usr.contactid;
                        userLoggedInAsValue = usr.contact.OB_Home_Page_Rendering__c;
                        //isContractorOnly = ( String.isNotBlank(usr.contact.Account.RecordType.DeveloperName) && usr.contact.Account.RecordType.DeveloperName.equals('Contractor_Account') ) || (usr.contact.Account.RecordType.DeveloperName.equals('Event_Account') && String.isBlank(usr.contact.Contractor__c));
                        /*if(String.isNotBlank(usr.contact.Contractor__c) && usr.contact.Contractor__c != null ){
                        system.debug('====usr.contact.Contractor__r.RecordType===='+usr.contact.Contractor__r.RecordType.DeveloperName);
                        isContractorOnly =  usr.contact.Contractor__r.RecordType.DeveloperName.equals('Contractor_Account') ? true : false;
                        }else{
                        isContractorOnly = false;
                        }*/
            
                        userRole = String.isNotBlank(usr.Community_User_Role__c) ? new set < String > (usr.Community_User_Role__c.split(';')) : null;
                        Profile profDIFCContr;
                        for(Profile prof : [
                                            SELECT id,Name 
                                              FROM profile
                                             WHERE name = 'DIFC Contractor Community Plus User Custom'
                                        ])
                        {
                            profDIFCContr = prof;
                        }
                
                        //userRole = String.isNotBlank(usr.Community_User_Role__c) ? new set < String > (usr.Community_User_Role__c.split(';')) : null;
                        //v1.04    15 March 2020      Merul Shah          Changing Community_User_Role__c and Contact.Role__c to AccountContact Role value.
                        //userRole = String.isNotBlank(currentUserLst[0].Community_User_Role__c) ? new set < String > (currentUserLst[0].Community_User_Role__c.split(';')) : null;
                        if( usr.OB_Is_New_Community_Profile__c )
                        {
                                for( AccountContactRelation accContactRel : [
                                                                                SELECT Id,
                                                                                    AccountId,
                                                                                    ContactId,
                                                                                    Roles,
                                                                                    Access_Level__c 
                                                                                FROM AccountContactRelation
                                                                            WHERE ContactId =: usr.Contact.Id
                                                                                AND AccountId =: usr.Contact.Account.Id
                                                                        ]
                                )
                                {
                                    
                                    //hasSRAccess = ( accContactRel.Access_Level__c == 'Read/Write' ? true : false);
                                    system.debug('### profDIFCContr '+profDIFCContr);
                                    system.debug('### accContactRel  '+accContactRel);
                                    //Shikha added
                                    userRole =  usr.profileId == profDIFCContr.Id ? userRole : new set <String>((accContactRel != null && accContactRel.Roles != null) ? accContactRel.Roles.split(';') : new List<String>());
                                    //commenting
                                    //userRole =  usr.profileId == profDIFCContr.Id ? userRole : new set <String>(accContactRel.Roles.split(';'));
                                    system.debug('########## rel based userRole '+userRole);
                                }
                        }
                        
                        if (usr.Contact.AccountId != null) {
                            userAccountID = usr.Contact.AccountId;
                            //Added v1.3
                            SeparateServices=usr.Separate_Services__c;
            
                        }

                        system.debug('########## userRole '+userRole);
            }
        }
    
    }
    /*
     * Used in: DIFC_RequestedServicesList Lightning component
     **/
    @AuraEnabled public static ResponseWrapper getRequestedServices(String reqWrapPram) {
    
        getLoggedInUserAccount();
        sObjectList = new List < sObject > ();
        // request detail from lightning component
        RequestWrapper reqWrap = (OB_RequestedServicesController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_RequestedServicesController.RequestWrapper.class);
        OB_RequestedServicesController.ResponseWrapper respWrap = new OB_RequestedServicesController.ResponseWrapper();
        system.debug(userRole + ' == reqWrap ==== ' + reqWrap);
    
        if (String.IsNotBlank(reqWrap.requestType)) {
            requestType = reqWrap.isInit ? ' ' : reqWrap.requestType;
        }
        if (reqWrap.userAccessTypeSet != null && reqWrap.userAccessTypeSet.size() != 0) {
            userAccessTypeSet = reqWrap.userAccessTypeSet;
        }
        if (reqWrap.isInit == true && userRole != null && userRole.size() != 0) {
            userAccessTypeSet = new Set < String > ();
    
            // Company Services - Clients & Contractors - will appear in the dropdown if company services roles exists on the user or if property services role exists
            if (userRole.contains('Company Services')) {
                userAccessTypeSet.add('Company Services');
                userAccessTypeSet.add('Property Services');
                requestType = 'Company Services';
            }
            // Property Services - Clients & Contractors - will appear in the dropdown if company services roles exists on the user or if property services role exists
            if (!userRole.contains('Company Services') && userRole.contains('Property Services')) {
                userAccessTypeSet.add('Property Services');
                requestType = 'Property Services';
            }
            // Listing Services - Clients & Contractors - will appear in the dropdown if listing services roles exists on the user or if property services role exists
            if (!userRole.contains('Company Services') && !userRole.contains('Property Services') && userRole.contains('Listing Services')) {
                
                userAccessTypeSet.add('Property Services');
                requestType = 'Property Services';
            }
            //Employee Services - Clients - to be shown in the dropdown if this role exists on the user
            if (userRole.contains('Employee Services')) {
                userAccessTypeSet.add('Employee Services');
                if (String.IsBlank(requestType)) {
                    requestType = 'Employee Services';
                }
            }
            //Fitout Services - Contractors - will appear in the dropdown as property services if this role exists on the profile
            if (userRole.contains('Fit-Out Services')) {
                userAccessTypeSet.add('Property Services');
                if (String.IsBlank(requestType)) {
                    requestType = 'Property Services';
                }
            }
            //Other Services avaliable always
            userAccessTypeSet.add('Other Services');
            if (String.IsBlank(requestType)) {
                requestType = 'Other Services';
            }
    
            respWrap.requestType = requestType;
            respWrap.userAccessTypeSet = userAccessTypeSet;
        }
    
        system.debug('=====userAccessTypeSet===' + userAccessTypeSet);
        system.debug('=====requestType===' + requestType);
        if (String.isNotBlank(userID)) {
            sObjectList = getServices(userAccountID, reqWrap,SeparateServices);
        }
        respWrap.sObjectList = sObjectList;
        respWrap.userRole = userRole;
        //v1.2
        respWrap.hexaSrNavLInk =  OB_AgentEntityUtils.getHEXASrLink();


        return respWrap;
        //return sObjectList;
    }
    /*
     In Progress SR: Displaying != draft SR
     search based on sr number
     search based on status
     *
     */
    public static list<sObject> getServices(string userAccountID, RequestWrapper reqWrap,boolean SeparateServices) {
    
        Boolean isNewRequest = true;
    
        //if(userRole != null && userRole.size() != 0){
        system.debug(isContractorOnly + '==userAccountID====' + userAccountID);
        system.debug('==userID====' + userID);
    
        /*******************query form for old service request object*************************/
        // get service request detail of that looged user account
        String strQuery = ' Select Id,Name,First_Name__c,Last_Name__c,External_Status_Name__c,CreatedDate,SR_Template__c,View_Invoice__c,SR_Template__r.Name,SR_Template__r.Menutext__c, SR_Template__r.SR_RecordType_API_Name__c ,Portal_User_Access__c From Service_Request__c WHERE Customer__c =: userAccountID ';
        // get service request detail of that looged user account
        String newStrQuery = ' Select Id,HexaBPM__Record_Type_Name__c,SR_Number__c,HexaBPM__External_Status_Name__c,View_Invoice__c,CreatedDate,HexaBPM__SR_Template__c,HexaBPM__SR_Template__r.Name,HexaBPM__SR_Template__r.HexaBPM__Menutext__c From HexaBPM__Service_Request__c';
        newStrQuery += ' WHERE HexaBPM__Customer__c =\''+ userAccountID+'\'';
    
        //strQuery += ' AND SR_Template__r.Menu__c IN: userRole' ;
        //newStrQuery += ' AND HexaBPM__SR_Template__r.HexaBPM__Menu__c IN: userRole' ;
        system.debug('userLoggedInAsValue--------' + userLoggedInAsValue);
        //V1.01 - Start -Mudasir 
        if (userLoggedInAsValue == 'Contractor') {
            //strQuery += ' AND createdbyId =  \''+userID+'\'';
            strQuery += ' AND (Contractor__c =\'' + userRecord.contact.Contractor__c + '\' OR (Email_Address__c =\'' + userRecord.contact.Email + '\' AND  Passport_Number__c =\'' + userRecord.contact.Passport_No__c + '\' AND Nationality_list__c =\'' + userRecord.contact.Nationality__c + '\' AND Service_Type__c=\'Request for Contractor Portal Access\'))';
            //newStrQuery += ' AND createdbyId =  \''+userID+'\''; //the query is used for the Hexa_BPM
        }
        //V1.01 - End - Mudasir 
        // sr number
        if (String.IsNotBlank(reqWrap.serviceNumber)) {
            strQuery += ' AND Name like  \'%' + reqWrap.serviceNumber + '%\'';
            newStrQuery += ' AND SR_Number__c like  \'%' + reqWrap.serviceNumber + '%\'';
        }
    
        system.debug('=reqWrap.inProgressHeaderDisplay===' + reqWrap.inProgressHeaderDisplay);
        // sr status
        if (String.IsNotBlank(reqWrap.serviceStatus) && !reqWrap.inProgressHeaderDisplay) {
    
            if (String.IsNotBlank(reqWrap.serviceStatus) && reqWrap.serviceStatus.equalsIgnorecase('In Progress')) {
    
                strQuery += ' AND IsCancelled__c = false AND Is_Rejected__c = false AND isClosedStatus__c = false AND External_Status_Name__c != \'Draft\'  AND Record_Type_Name__c != \'DM_SR\' AND Portal_User_Access__c = true AND Pre_GoLive__c = false ';
                newStrQuery += ' AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__Is_Rejected__c = false AND HexaBPM__IsCancelled__c = false AND HexaBPM__External_Status_Name__c != \'Draft\'';
    
            } else if (String.IsNotBlank(reqWrap.serviceStatus) && reqWrap.serviceStatus.equalsIgnorecase('All')) {
                strQuery += ' AND Record_Type_Name__c != \'DM_SR\' AND Portal_User_Access__c = true AND Pre_GoLive__c = false ';
                //newStrQuery += ' AND HexaBPM__External_Status_Name__c =\''+ reqWrap.serviceStatus+'\'';
    
            } else if (reqWrap.serviceStatus.equalsIgnorecase('Approved')) {
                strQuery += ' AND Record_Type_Name__c != \'DM_SR\' AND External_Status_Name__c = \'Approved\' and Portal_User_Access__c = true and Pre_GoLive__c = false ';
                newStrQuery += ' AND HexaBPM__IsClosedStatus__c = true ';
    
            }
            /*else if(reqWrap.serviceStatus.equalsIgnorecase('Lease Requests')){
             strQuery += ' AND Record_Type_Name__c != \'DM_SR\' AND (Record_Type_Name__c = \'Lease_Registration\' OR Record_Type_Name__c = \'Surrender_Termination_of_Lease_and_Sublease\') AND SR_Group__c = \'RORP\' and Portal_User_Access__c = true and Pre_GoLive__c = false ';
             isNewRequest = false;
    
             }else if(reqWrap.serviceStatus.equalsIgnorecase('Freehold Requests')){
             strQuery += ' AND Record_Type_Name__c = \'Freehold_Transfer_Registration\'';
             isNewRequest = false;
    
             }*/
            else if (reqWrap.serviceStatus.equalsIgnorecase('Submitted')) { //V1.4 added  SR_Template__r.Active__c=true 
                strQuery += ' AND Record_Type_Name__c != \'DM_SR\' AND SR_Template__r.Active__c=true  AND External_Status_Name__c = \'Submitted\' and Portal_User_Access__c = true and Pre_GoLive__c = false';
                newStrQuery += ' AND HexaBPM__External_Status_Name__c = \'Submitted\' ';
    
            } else if (reqWrap.serviceStatus.equalsIgnorecase('Draft')) { //V1.4 added  SR_Template__r.Active__c=true 
                strQuery += ' AND Record_Type_Name__c != \'DM_SR\' AND SR_Template__r.Active__c=true AND External_Status_Name__c = \'Draft\' and Portal_User_Access__c = true and Pre_GoLive__c = false ';
                newStrQuery += ' AND HexaBPM__External_Status_Name__c = \'Draft\' ';
    
            }
            // sr request Type
            if (String.IsNotBlank(requestType) && !requestType.equalsIgnorecase('All')) {
                // company service also query open a business sr type 
                if(requestType.equalsIgnorecase('Company Services')){
                  strQuery += ' AND (SR_Template__r.Menu__c =\'' + requestType + '\' OR SR_Template__r.Menu__c =\'Open a Business\' OR SR_Template__r.SR_RecordType_API_Name__c =\'Index_Card_Other_Services\'  OR SR_Template__r.SR_RecordType_API_Name__c =\'New_Index_Card\') ';
                }else{
                  strQuery += ' AND SR_Template__r.Menu__c =\'' + requestType + '\'';
                }
                
                newStrQuery += ' AND HexaBPM__SR_Template__r.HexaBPM__Menu__c =\'' + requestType + '\'';
            }
            
            
            //v1.3
            if(SeparateServices)
            {
                 strQuery += ' AND CreatedById =\'' + userID + '\'';
                 newStrQuery += ' AND CreatedById =\'' + userID + '\'';
                
            }
            
            if (String.IsNotBlank(reqWrap.oldFieldSortingQuery)) {
                strQuery += ' Order by ' + reqWrap.oldFieldSortingQuery;
            }
            if (String.IsNotBlank(reqWrap.NewFieldSortingQuery)) {
                newStrQuery += ' Order by ' + reqWrap.NewFieldSortingQuery;
            }
            strQuery += ' Limit ' + (Limits.getLimitQueryRows() - Limits.getQueryRows());
            system.debug('=====strQuery======' + strQuery);
    
            /*************************query form for new service request object*****************************************/
    
            newStrQuery += ' Limit ' + (Limits.getLimitQueryRows() - Limits.getQueryRows());
            system.debug('=====newStrQuery======' + newStrQuery);
    
        } else {
            // inprogress logic
    
            strQuery += ' AND IsCancelled__c = false AND Is_Rejected__c = false AND isClosedStatus__c = false AND External_Status_Name__c != \'Draft\' AND Record_Type_Name__c != \'DM_SR\' AND Portal_User_Access__c = true AND Pre_GoLive__c = false ';
            newStrQuery += ' AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__Is_Rejected__c = false AND HexaBPM__IsCancelled__c = false AND HexaBPM__External_Status_Name__c != \'Draft\'';
    
            // sr request Type
            if (String.IsNotBlank(requestType) && !requestType.equalsIgnorecase('All')) {
                strQuery += ' AND SR_Template__r.Menu__c =\'' + requestType + '\'';
                newStrQuery += ' AND HexaBPM__SR_Template__r.HexaBPM__Menu__c =\'' + requestType + '\'';
            }
            
            //v1.3
            if(SeparateServices)
            {
                 strQuery += ' AND CreatedById =\'' + userID + '\'';
                 newStrQuery += ' AND CreatedById =\'' + userID + '\'';
                
            }
            
            strQuery += ' Limit 10 ';
            system.debug('=====strQuery======' + strQuery);
    
            /*************************query form for new service request object*****************************************/
            newStrQuery += ' Limit 10';
            system.debug('=====newStrQuery======' + newStrQuery);
        }
    
    
    
        sObjectList = new List < sObject > ();
        /********************************get list of object service request object******************************/
        if (String.IsNotBlank(userAccountID)) {
    
            sObjectList.addAll((List < sObject > ) database.query(strQuery));
    
            if(isNewRequest){
              list<HexaBPM__Service_Request__c> lstApplications = OB_RequestedServicesControllerHelper.ApplicationQueryResult(newStrQuery);
              if(lstApplications!=null && lstApplications.size()>0){
                  //sObjectList.AddAll((list<sObject>) database.query(newStrQuery));
                  sObjectList.AddAll((list<sObject>) lstApplications);
              }
            }
        }
        system.debug('=====sObjectList========' + sObjectList);
        //}
        return sObjectList;
    }
    
    
    /*
     * Used in: DIFC_VATRegistrationForm Lightning component
     * Description: will update account with the provided registration number, when user click on "Confirm" button
     */
    @AuraEnabled public static void updateAccountVatRegistrationNumber(String reqWrapPram) {
        /*accupdateList = new List<Account>();
    
         getLoggedInUserAccount();
         // request detail from lightning component
         RequestWrapper reqWrap = (OB_RequestedServicesController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_RequestedServicesController.RequestWrapper.class);
         system.debug('==reqWrap===='+reqWrap);
    
         system.debug('==update account===userAccountID==='+userAccountID);
         if(String.IsNotBlank(userAccountID)){
         // taxRegistrationNumber
         for(Account acc:[Select Id,Tax_Registration_Number_TRN__c,
         VAT_Info_Updated__c,
         VAT_Info_Updated_DateTime__c,
         VAT_Option__c From Account Where Id =: userAccountID]){
    
         acc.Tax_Registration_Number_TRN__c = reqWrap.taxRegistrationNumber;
         acc.VAT_Info_Updated__c = true;
         acc.VAT_Info_Updated_DateTime__c = Date.today();
         acc.VAT_Option__c = 'Is Vat';
         accupdateList.add(acc);
    
         }
         system.debug('=====accupdateList==='+accupdateList);
         if(!accupdateList.IsEmpty()){
         update accupdateList;
         }
         }*/
    }
    
    /*
     * Used in: DIFC_HomeTab Lightning component
     * Description: check if company has vat registration or not for displaying the registration section. This will check on load of page
     **/
    @AuraEnabled public static ResponseWrapper checkVATNumber() {
        OB_RequestedServicesController.ResponseWrapper respWrap = new OB_RequestedServicesController.ResponseWrapper();
        /*getLoggedInUserAccount();
         respWrap.isVATNotRegistered = true;
         if(String.IsNotBlank(userAccountID)){
         for(Account acc:[Select Id,VAT_Option__c,Tax_Registration_Number_TRN__c,VAT_Info_Updated__c From Account Where Id =: userAccountID]){
         if((String.IsnotBlank(acc.VAT_Option__c) && acc.VAT_Option__c.equalsIgnorecase('Is Not Vat'))
         || (String.IsnotBlank(acc.VAT_Option__c) && acc.VAT_Option__c.equalsIgnorecase('Is Vat')
         && String.IsnotBlank(acc.Tax_Registration_Number_TRN__c))){
         respWrap.isVATNotRegistered = false;
    
         }
         }
    
         }*/
        return respWrap;
    }
    
    /*
     * Used in: DIFC_HomeTab Lightning component
     * Description: will update the Vat option field to "IS Not VAT" if user click on "I AM NOT ELIGIBLE"
     */
    @AuraEnabled public static void updateAccountField() {
        /*accupdateList = new List<Account>();
         getLoggedInUserAccount();
         system.debug('==update account===userAccountID==='+userAccountID);
         if(String.IsNotBlank(userAccountID)){
         for(Account acc:[Select Id,VAT_Option__c From Account Where Id =: userAccountID]){
         acc.VAT_Option__c = 'Is Not Vat';
         accupdateList.add(acc);
         }
         system.debug('=====accupdateList==='+accupdateList);
         if(!accupdateList.IsEmpty()){
         update accupdateList;
         }
         }*/
    }
    
    /**********************************Wrapper classes*********************************************************/
    public class ResponseWrapper {
        //@AuraEnabled public Boolean isVATNotRegistered        {get;set;}
        @AuraEnabled public List < sObject > sObjectList {get;set;}
        @AuraEnabled public Set < String > userRole { get;set;}
        @Auraenabled public Set < String > userAccessTypeSet {get;set;}
        @AuraEnabled public String requestType {get;set;}
        @AuraEnabled public String superUserNav {get;set;}
        @AuraEnabled public map<string,string> hexaSrNavLInk {get;set;}

    }
    
    public class RequestWrapper {
        @AuraEnabled public String serviceNumber { get; set;}
        @AuraEnabled public String requestType {get;set;}
        @AuraEnabled public String serviceStatus {get;set;}
        //@AuraEnabled public String taxRegistrationNumber     {get;set;}
        @AuraEnabled public Boolean inProgressHeaderDisplay {get;set;}
        @AuraEnabled public String oldFieldSortingQuery {get;set;}
        @AuraEnabled public String NewFieldSortingQuery {get;set;}
        @Auraenabled public Boolean isInit {get;set;}
        @Auraenabled public Set < String > userAccessTypeSet {get;set;}
    }
    }