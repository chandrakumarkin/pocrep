/**
 * @description       : 
 * @author            : Shoaib Tariq
 * @group             : 
 * @last modified on  : 04-13-2021
 * @last modified by  : Shoaib Tariq
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   04-13-2021   Shoaib Tariq   Initial Version
**/
@isTest(seealldata=false)
public with sharing class Application_KPI_BatchClsTest {
    
    static testMethod void myUnitTest(){
     
    HexaBPM__Service_Request__c newSr    = new HexaBPM__Service_Request__c();
    newSr.HexaBPM__Closed_Date_Time__c   = system.now();
    newSr.HexaBPM__Submitted_DateTime__c = system.now();
    insert newSr;
        
    Database.executeBatch(new Application_KPI_BatchCls());
     
    }
}