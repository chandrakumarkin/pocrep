/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global interface iStepAssignmentExecutable {
    List<HexaBPM__Step__c> Assign_Step_Owner_SR_Submit(Map<String,HexaBPM__Service_Request__c> param0, List<HexaBPM__Step__c> param1, Map<String,HexaBPM__SR_Steps__c> param2);
    List<HexaBPM__Step__c> Assign_Step_Owner_Step_Closure(Map<String,HexaBPM__Step__c> param0, List<HexaBPM__Step__c> param1, Map<String,HexaBPM__SR_Steps__c> param2);
}
