/******************************************************************************************************
 *  Author   : Kumar Utkarsh
 *  Date     : 02-May-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    02-May-2021  Utkarsh         Created
* This is a scheduler class for WebServiceGdrfaFetchTrxReceiptBatch.
*******************************************************************************************************/
global class WebServiceGdrfaFetchTrxReceiptScheduler implements Schedulable {

   global void execute(SchedulableContext sc) {
      WebServiceGdrfaFetchTrxReceiptBatch trxReceiptBatch = new WebServiceGdrfaFetchTrxReceiptBatch(); 
      database.executebatch(trxReceiptBatch, 1);
   }
    
}