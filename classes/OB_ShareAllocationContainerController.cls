public without sharing class OB_ShareAllocationContainerController {


	@AuraEnabled
	public static ResponseWrapper getExistingAmendShareHolder(String reqWrapPram) 
    {
		//reqest wrpper
		OB_ShareAllocationContainerController.RequestWrapper reqWrap = (OB_ShareAllocationContainerController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_ShareAllocationContainerController.RequestWrapper.class);
		//response wrpper
		OB_ShareAllocationContainerController.ResponseWrapper respWrap = new OB_ShareAllocationContainerController.ResponseWrapper();
		String customerId;

		Map<Id, AmendmentWrapper> amedWrapMap = new Map<Id, AmendmentWrapper>();
        Set<String> roles = new Set<String>{'Shareholder'};
        

		respWrap.accShrDetailWrapLst = new List<AccountShareDetailWrapper>();
		respWrap.shrDetailWrapLst = new List<ShareholderDetailWrapper>();
		respWrap.amedWrapLst = new List<AmendmentWrapper>();

		String srId = reqWrap.srId;
		system.debug('##### srId  ' + srId);

		String flowId = reqWrap.flowId;
        



		for(HexaBPM_Amendment__c amed : getAmendmentBySrId(srId)) 
         {
			if(amed.Role__c != NULL && amed.Role__c.contains('Shareholder') )
            {
                customerId = amed.ServiceRequest__r.HexaBPM__customer__c;
                OB_ShareAllocationContainerController.AmendmentWrapper amedWrap = new OB_ShareAllocationContainerController.AmendmentWrapper();
                amedWrap.shrDetailWrapLst = new List<ShareholderDetailWrapper>();
    
                amedWrap.amedObj = amed;
                amedWrap.isIndividual = (amed.RecordType.DeveloperName == 'Individual' ? true :false);
                amedWrap.isBodyCorporate = (amed.RecordType.DeveloperName == 'Body_Corporate' ? true :false);
                amedWrap.lookupLabel = (amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name :'');
                amedWrapMap.put(amed.Id, amedWrap);
           }
    	}
	

		for(Shareholder_Detail__c shrDetail :[SELECT id, 
                                                      No_of_Shares_Allotted__c, 
                                                      Account_Share__c, 
                                                      OB_Amendment__c
										      FROM Shareholder_Detail__c
										      WHERE Status__c != 'Inactive'
										      AND OB_Amendment__c = :amedWrapMap.keySet()
		    ] ) 
        {
			AmendmentWrapper amedWrap = amedWrapMap.get(shrDetail.OB_Amendment__c);
			HexaBPM_Amendment__c amed = amedWrap.amedObj;

			ShareholderDetailWrapper shrDetailWrap = new ShareholderDetailWrapper();
			shrDetailWrap.shareHolderDetObj = shrDetail;
			shrDetailWrap.shareHolderDetObj.OB_Amendment__c = amed.Id;
			shrDetailWrap.shareHolderDetObj.Account__c = customerId;
			shrDetailWrap.amedObj = amed;
            system.debug('######## shrDetailWrap  '+shrDetailWrap);
			amedWrap.shrDetailWrapLst.add(shrDetailWrap);

			amedWrapMap.put(amed.Id, amedWrap);


		}
		system.debug('######## amedWrapMap '+amedWrapMap);

		if(amedWrapMap != NULL && amedWrapMap.size() > 0) {

			respWrap.amedWrapLst = amedWrapMap.values();
		}




		// Getting the list of classes.
		for(Account_Share_Detail__c accShareDetail :[SELECT id, 
												            Name
												     FROM Account_Share_Detail__c
												     WHERE Account__c = :customerId
												     AND Status__c != 'Inactive'
		    ] ) 
        {

			AccountShareDetailWrapper accShrDtl = new AccountShareDetailWrapper();
			accShrDtl.accShrDetObj = accShareDetail;
			respWrap.accShrDetailWrapLst.add(accShrDtl);

		}



		for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
												    AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) 
        {
			respWrap.ButtonSection = sectionObj;
		}
		return respWrap;

	}
    
    public static List<HexaBPM_Amendment__c> getAmendmentBySrId(String srId)
    {
        return [SELECT id, 
                        Role__c, 
                        Passport_No__c, 
                        Nationality_list__c, 
                        Date_of_Birth__c, 
                        Passport_Issue_Date__c, 
                        Passport_Expiry_Date__c, 
                        Place_of_Issue__c, 
                        Are_you_a_resident_in_the_U_A_E__c, 
                        RecordType.DeveloperName, 
                        ServiceRequest__r.Id, 
                        ServiceRequest__r.HexaBPM__customer__c, 
                        Is_this_Entity_registered_with_DIFC__c, 
                        Registered_No__c, 
                        Place_of_Registration__c, 
                        Country_of_Registration__c, 
                        Individual_Corporate_Name__c, 
                        Entity_Name__c, 
                        Entity_Name__r.Name
                   FROM HexaBPM_Amendment__c
                  WHERE ServiceRequest__c = :srId
            	];
    }





	@AuraEnabled
	public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId) 
    {
		 

		ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
		HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
		PageFlowControllerHelper.objSR = objRequest;
		PageFlowControllerHelper objPB = new PageFlowControllerHelper();

		objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
		upsert objRequest;
        
        // ShareAllocation validation.
        OB_ShareAllocationValidateHelper.RequestWrapper shrAllocationValidReq = new OB_ShareAllocationValidateHelper.RequestWrapper();
        shrAllocationValidReq.srId = SRID;
        OB_ShareAllocationValidateHelper.ResponseWrapper shrAllocationValidResp = OB_ShareAllocationValidateHelper.validateShareHolderDetails(shrAllocationValidReq);
        
        if( shrAllocationValidResp != NULL && !String.isBlank(shrAllocationValidResp.finalAllocationStatus) )
        {
            respWrap.errorMsg = shrAllocationValidResp.finalAllocationStatus;
            return respWrap;
        }
        
        
		OB_RiskMatrixHelper.CalculateRisk(SRID);

		PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
		system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
		respWrap.pageActionName = responseNextPage.pg;
		respWrap.communityName = responseNextPage.communityName;
		respWrap.CommunityPageName = responseNextPage.CommunityPageName;
		respWrap.sitePageName = responseNextPage.sitePageName;
		respWrap.strBaseUrl = responseNextPage.strBaseUrl;
		respWrap.srId = objRequest.Id;
		respWrap.flowId = responseNextPage.flowId;
		respWrap.pageId = responseNextPage.pageId;
		respWrap.isPublicSite = responseNextPage.isPublicSite;

		system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
		return respWrap;
	}
	public class ButtonResponseWrapper {
		@AuraEnabled public String pageActionName { get; set; }
		@AuraEnabled public string communityName { get; set; }
		@AuraEnabled public String errorMessage { get; set; }
		@AuraEnabled public string CommunityPageName { get; set; }
		@AuraEnabled public string sitePageName { get; set; }
		@AuraEnabled public string strBaseUrl { get; set; }
		@AuraEnabled public string srId { get; set; }
		@AuraEnabled public string flowId { get; set; }
		@AuraEnabled public string pageId { get; set; }
		@AuraEnabled public boolean isPublicSite { get; set; }
        @AuraEnabled public String errorMsg { get; set; }

	}

	

	public class AmendmentWrapper {
		@AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
		@AuraEnabled public Boolean isIndividual { get; set; }
		@AuraEnabled public Boolean isBodyCorporate { get; set; }
		@AuraEnabled public String lookupLabel { get; set; }
		@AuraEnabled public List<ShareholderDetailWrapper> shrDetailWrapLst { get; set; }



		public AmendmentWrapper() {
		}
	}


	public class AccountShareDetailWrapper {
		@AuraEnabled public Account_Share_Detail__c accShrDetObj { get; set; }
		public AccountShareDetailWrapper() {
		}
	}

	public class ShareholderDetailWrapper {
		@AuraEnabled public Shareholder_Detail__c shareHolderDetObj { get; set; }
		@AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }

		public ShareholderDetailWrapper() {
		}
	}


	public class RequestWrapper {
		@AuraEnabled public Id flowId { get; set; }
		@AuraEnabled public Id pageId { get; set; }
		@AuraEnabled public Id srId { get; set; }
		@AuraEnabled public AmendmentWrapper amedWrap { get; set; }
		@AuraEnabled public String amendmentID { get; set; }
		@AuraEnabled public List<ShareholderDetailWrapper> shrDetailWrapLst { get; set; }


		public RequestWrapper() {
		}
	}

	public class ResponseWrapper {
		//@AuraEnabled public SRWrapper srWrap { get; set; }
		@AuraEnabled public List<AccountShareDetailWrapper> accShrDetailWrapLst { get; set; }
		@AuraEnabled public List<ShareholderDetailWrapper> shrDetailWrapLst { get; set; }
		@AuraEnabled public List<AmendmentWrapper> amedWrapLst { get; set; }
        


		@AuraEnabled
		public HexaBPM__Section_Detail__c ButtonSection { get; set; }


		public ResponseWrapper() {
		}
	}


}