/*
* OB_RequestedServicesController
*/
@isTest
public with sharing class OB_RequestedServicesControllerTest {
	@isTest
    private static void initOB_RequestedServices(){ 
    	UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
		insert r;
    	Profile adminProfile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    	//User admin = [SELECT Id, Username, UserRoleId FROM User WHERE Profile.Name = 'System Administrator' AND UserRoleId = :userRole_1.Id LIMIT 1];
    	
    	User admin = new User(Alias = 'tstuassr', Email='testu12r@difc.portal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = adminProfile.Id,
                        Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser12@testorg.com',UserRoleId = r.Id);
        insert admin;
    	system.runAs(admin){
    	// create account
    	List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insertNewContacts[0].Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Previous_Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Country_of_Birth__c  = 'Russian Federation';
        insertNewContacts[0].Country__c  = 'Russian Federation';
        insertNewContacts[0].Issued_Country__c  = 'Russian Federation';
        insertNewContacts[0].Gender__c  = 'Male';
        insertNewContacts[0].UID_Number__c  = '9712';
        insertNewContacts[0].MobilePhone  = '+97123576565';
        insertNewContacts[0].Passport_No__c  = '+97123576565';
        insertNewContacts[0].recordTypeId = portalUserId;
        insert insertNewContacts;
        
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=insertNewContacts[0].Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(1, new List<string> {'New_User_Registration'});
        insert createSRTemplateList;
        system.runAs(objUser){
	        //create SR
	        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
	        insertNewSRs = OB_TestDataFactory.createSR(9, new List<string> {'New_User_Registration'}, insertNewAccounts, 
	                                                   new List<string>{'Non - financial','Non - financial','Non - financial','Non - financial','Non - financial','Retail','Non - financial','Non - financial','Non - financial'}, 
	                                                   new List<string>{'Foundation', 'Audit and Accounting', 'Audit and Accounting', 'Audit and Accounting','Non Profit Incorporated Organisation (NPIO)','Services','Audit and Accounting','Audit and Accounting','Audit and Accounting'}, 
	                                                   new List<string>{'Foundation', 'Partnership', 'Partnership', 'Partnership', 'Non Profit Incorporated Organisation (NPIO)','Company','Partnership','Partnership','Partnership'}, 
	                                                   new List<string>{'Foundation','General Partnership (GP)','Limited Partnership (LP)', 'Limited Liability Partnership (LLP)','NPIO','Recognized Company','Recognized Limited Partnership (RLP)','Recognized Limited Liability Partnership (RLLP)','Recognized Partnership (RP)'});
	        
	        insert insertNewSRs;
	        system.debug('---->'+insertNewSRs);
        }
        test.startTest();
	        system.runAs(objUser){
	        	
	        	OB_RequestedServicesController.getLoggedInUserAccountName();
	        	
	        	OB_RequestedServicesController.RequestWrapper reqWrapper = new OB_RequestedServicesController.RequestWrapper();
		        reqWrapper.serviceStatus = 'In Progress';
		        reqWrapper.inProgressHeaderDisplay = false;
		        reqWrapper.isInit = true;
		        OB_RequestedServicesController.ResponseWrapper respWrap = OB_RequestedServicesController.getRequestedServices(JSON.serialize(reqWrapper));
		        reqWrapper.serviceNumber = 'SR-12';
		        reqWrapper.serviceStatus = 'All';
		        respWrap = OB_RequestedServicesController.getRequestedServices(JSON.serialize(reqWrapper));
		        reqWrapper.serviceStatus = 'Approved';
		        respWrap = OB_RequestedServicesController.getRequestedServices(JSON.serialize(reqWrapper));
		        reqWrapper.serviceStatus = 'Lease Requests';
		        respWrap = OB_RequestedServicesController.getRequestedServices(JSON.serialize(reqWrapper));
		        reqWrapper.serviceStatus = 'Freehold Requests';
		        respWrap = OB_RequestedServicesController.getRequestedServices(JSON.serialize(reqWrapper));
		        reqWrapper.serviceStatus = 'Submitted';
		        respWrap = OB_RequestedServicesController.getRequestedServices(JSON.serialize(reqWrapper));
		        reqWrapper.serviceStatus = 'Draft';
		        reqWrapper.requestType = 'Property Services';
		        respWrap = OB_RequestedServicesController.getRequestedServices(JSON.serialize(reqWrapper));
        		reqWrapper.inProgressHeaderDisplay = true;
        		respWrap = OB_RequestedServicesController.getRequestedServices(JSON.serialize(reqWrapper));
	        	//OB_RequestedServicesController.getRequestedServices();
	        }
        Test.stopTest();
    	}
    }
}