public with sharing class SRTemplateDeepClone {
    public string strOldSRTemplateId;
    public string strSRTemplateName{get;set;}
    public string strSRRecTypeName{get;set;}
    public SRTemplateDeepClone(){
        strSRTemplateName = '';
        strSRRecTypeName = '';
        strOldSRTemplateId = ApexPages.currentPage().getParameters().get('Id');
    }
    public pagereference CancelClone(){
        pagereference ref = new pagereference('/'+strOldSRTemplateId);
        ref.setredirect(true);
        return ref;
    }
    public pagereference CloneProcessRecords(){
        //
        //when user click Confirm button this method is invoked
        //Clone sr template record and geneate the id for new template, which we need for inserting the children
        //sequence of children based on foreign keys : 1. sr templ, 2. sr step, 3. sr templ or sr templ doc, 5. Condition for temp docs 
        // 6. step trans or busines rule 8.action or condition
        //clone and create lists for first level children (SR_Template_Item__c,SR_Template_Docs__c,SR_Steps__c) And then replace sr template and insert it.
        //prepare the maps for second level children with step no as key or bus rule name as key
            system.debug('strSRTemplateName==>'+strSRTemplateName+'********');
            system.debug('strSRRecTypeName==>'+strSRRecTypeName+'********');
            
            strSRTemplateName = strSRTemplateName.trim();
            strSRRecTypeName = strSRRecTypeName.trim();
            
            list<sr_template__c> lstExistingSrtemp = [select id from sr_template__c where name=:strSRTemplateName or SR_RecordType_API_Name__c=:strSRRecTypeName];
            system.debug('lstExistingSrtemp==>'+lstExistingSrtemp);
            string logMessage = '';
            if(lstExistingSrtemp==null || (lstExistingSrtemp!=null && lstExistingSrtemp.size()==0)){
                /* Start of Code to Insertion New SR-Template */
                    Savepoint svpoint = Database.setSavepoint();
                    SR_Template__c srTemplate = new SR_Template__c();
                    for(SR_Template__c SRtempl:[select Id,Name,Active__c,Available_for_menu__c,Do_not_use_owner__c,Menu__c,Menutext__c,ORA_Process_Identification__c,ORA_SR_Type__c,ORA_SR_Type_Process_Name__c,Refund_Item__c,SR_RecordType_API_Name__c,Sub_menu__c from SR_Template__c where id=:strOldSRTemplateId]){
                        srTemplate = SRtempl.clone(false, true);
                    }
                    srTemplate.Name = strSRTemplateName;
                    if(srTemplate.Name.length()>80){
                        srTemplate.Name = srTemplate.Name.subString(0,80);
                    }
                    srTemplate.SR_RecordType_API_Name__c = strSRRecTypeName;
                    try{
                        insert srTemplate;
                    }catch(Exception e){
                        Database.rollback(svpoint);
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,string.valueOf(e.getMessage())));
                    }
                /* End of Code to Insertion New SR-Template */
                    
                if(srTemplate!=null && srTemplate.Id!=null){
                    
                    map<string,list<Business_Rule__c>> MapBrs = new map<string,list<Business_Rule__c>>();
                    map<string,list<Step_Transition__c>> MapStpTransitions = new map<string,list<Step_Transition__c>>();
                    map<String,list<Condition__c>> MapBRConditions = new map<string,list<Condition__c>>();
                    map<String,list<Action__c>> MapBRActions = new map<string,list<Action__c>>();
                    
                    map<string,list<Condition__c>> MapDocConditions = new map<string,list<Condition__c>>();
                    
                    map<string,string> mapNewSRStepNameId = new map<string,string>();
                    map<string,string> mapNewBRNamesId = new map<string,string>();
                    
                    list<SR_Steps__c> LstNewSRSteps = new list<SR_Steps__c>();
                    list<SR_Template_Item__c> lstSRTemplateItemsToBeInsert = new list<SR_Template_Item__c>();
                    list<SR_Template_Docs__c> lstSRTemplateDocsToBeInsert = new list<SR_Template_Docs__c>();
                    
                    map<decimal,id> mapStepNoId = new map<decimal,id>();
                    
                    map<string,decimal> mapTemplItemNoConsumedSTepNo = new  map<string,decimal>();
                    map<string,decimal> mapTemplItemNoEvaluatedSTepNo = new  map<string,decimal>();
                    
                    map<string,decimal> mapTemplDocNoCourCollSTepNo = new  map<string,decimal>();
                    map<string,decimal> mapTemplDocNoCourDelivSTepNo = new  map<string,decimal>();
                    map<string,decimal> mapTemplDocNoEvaluatedSTepNo = new  map<string,decimal>();
                    
                    
                    /* Start of Code to Prepare list of New SRTemplateItems for the SR-Template */
                    integer TemplItemNo = 1;
                    for(SR_Template_Item__c SRTmplItem:[select id,Name,Sys_Template_Name__c,Evaluated_at__c,Evaluated_at__r.Step_No__c,Consumed_at__c,Consumed_at__r.Step_No__c,On_Submit__c,Product__c,SR_Template__c from SR_Template_Item__c where SR_Template__c=:strOldSRTemplateId]){
                        
                        SR_Template_Item__c CloneTemplItem = SRTmplItem.clone(false, true);
                        CloneTemplItem.Sys_Template_Name__c = SRTmplItem.Id+'_'+TemplItemNo; //unqiue id for Sys_Template_Name__c
                        CloneTemplItem.SR_Template__c = srTemplate.Id;
                        CloneTemplItem.Consumed_at__c = null;// these are lookup for sr steps
                        CloneTemplItem.Evaluated_at__c = null;// these are lookup for sr steps
                        lstSRTemplateItemsToBeInsert.add(CloneTemplItem);
                        //Step_No__c is unique for a given sr template.
                        //this will be used to fetch id later on
                        //lstSRTemplateItemsToBeInsert is prepared
                        //tow more maps for consumed at and evalutated at are also prepared, ths is to replace the ids later on
                        if(SRTmplItem.Consumed_at__c!=null && SRTmplItem.Consumed_at__r.Step_No__c!=null){
                            mapTemplItemNoConsumedSTepNo.put(CloneTemplItem.Sys_Template_Name__c,SRTmplItem.Consumed_at__r.Step_No__c);
                        }
                        if(SRTmplItem.Evaluated_at__c!=null && SRTmplItem.Evaluated_at__r.Step_No__c!=null){
                            mapTemplItemNoEvaluatedSTepNo.put(CloneTemplItem.Sys_Template_Name__c,SRTmplItem.Evaluated_at__r.Step_No__c);
                        }
                        
                        TemplItemNo++;
                    }
                    /* End of Code to Prepare New list of SRTemplateItems for the SR-Template */
                    
                    /* Start of Code to Prepare list of New SRTemplateDocs for the SR-Template */
                    integer TemplDocNo = 1;
                    for(SR_Template_Docs__c SRTmplDoc:[select id,Name,Sys_Template_Doc_Name__c,Evaluated_at__c,Evaluated_at__r.Step_No__c,Courier_collected_at__c,Courier_collected_at__r.Step_No__c,Courier_delivered_at__c,Courier_delivered_at__r.Step_No__c,On_Submit__c,Document_Master__c,SR_Template__c,Generate_Document__c,Group_No__c,Optional__c,Quantity_evaluated_by__c,SR_Template_Docs_Condition__c,
                    (select id,Name,Class_Name__c,Close_Bracket__c,Conditional_Operator__c,Field_Name__c,Field_Type__c,Object_Name__c,Open_Bracket__c,Operator__c,S_No__c,SR_Template_Docs__c,Value__c from Conditions__r) from SR_Template_Docs__c where SR_Template__c=:strOldSRTemplateId]){
                        
                        SR_Template_Docs__c CloneTemplDoc = SRTmplDoc.clone(false, true);
                        CloneTemplDoc.SR_Template__c = srTemplate.Id;
                        CloneTemplDoc.Courier_collected_at__c = null;// all these three are lookups for sr steps, which will be genereated later
                        CloneTemplDoc.Courier_delivered_at__c = null;  // so create three maps for each of this 
                        CloneTemplDoc.Sys_Template_Doc_Name__c = SRTmplDoc.Id+'_'+TemplDocNo;
                        CloneTemplDoc.Evaluated_at__c = null;
                        lstSRTemplateDocsToBeInsert.add(CloneTemplDoc);
                        
                        if(SRTmplDoc.Conditions__r!=null && SRTmplDoc.Conditions__r.size()>0){
                            //deepClone is a std method used to clone list of records
                            //map is created with key as Sys_Template_Doc_Name__c and value as list of conditions.
                            MapDocConditions.put(CloneTemplDoc.Sys_Template_Doc_Name__c,SRTmplDoc.Conditions__r.deepClone(false,false,false));
                        }
                        
                        if(SRTmplDoc.Courier_collected_at__c!=null && SRTmplDoc.Courier_collected_at__r.Step_No__c!=null){
                            mapTemplDocNoCourCollSTepNo.put(CloneTemplDoc.Sys_Template_Doc_Name__c,SRTmplDoc.Courier_collected_at__r.Step_No__c);
                        }
                        if(SRTmplDoc.Courier_delivered_at__c!=null && SRTmplDoc.Courier_delivered_at__r.Step_No__c!=null){
                            mapTemplDocNoCourDelivSTepNo.put(CloneTemplDoc.Sys_Template_Doc_Name__c,SRTmplDoc.Courier_delivered_at__r.Step_No__c);
                        }
                        if(SRTmplDoc.Evaluated_at__c!=null && SRTmplDoc.Evaluated_at__r.Step_No__c!=null){
                            mapTemplDocNoEvaluatedSTepNo.put(CloneTemplDoc.Sys_Template_Doc_Name__c,SRTmplDoc.Evaluated_at__r.Step_No__c);
                        }
                        
                        TemplDocNo++;
                    }
                    /* End of Code to Prepare list of New SRTemplateDocs for the SR-Template */
                    
                    
                    set<id> setOldSRStepIds = new set<id>();
                    set<id> setBRIds = new set<id>();
                    //
                    //query sr step with bus rule and step transition
                    //
                    //
                    for(SR_Steps__c SRstp:[Select Id,Active__c,Start_Status__c,Do_not_use_owner__c,Sys_SR_Step_Name__c,Estimated_Hours__c,Sys_Create_Condition__c,Sys_Custom_Conditions_Actions__c,Sys_Default_Actions__c,No_of_days_on_closure__c,SR_Step_Id__c,SR_Template__c,Step_No__c,Step_RecordType_API_Name__c,Step_Status_on_closure__c,Step_Template__c,Summary__c,Unique_Step_No__c,
                    (Select id,Name,Action_Type__c,Sys_BR_Name__c,Business_Rule_Condition__c,Description__c,Execute_on_Insert__c,Execute_on_Update__c,SR_Steps__c,Text_Condition__c from Business_Rules__r),
                    (Select id,Evaluate_Refund__c,Sys_Step_Transition_Name__c,Parent_SR_Status__c,Parent_Step_Status__c,SR_Status_External__c,SR_Status_Internal__c,SR_Step__c,SR_Template__c,Transition__c from Step_Transitions__r) from SR_Steps__c where SR_Template__c=:strOldSRTemplateId]){
                        system.debug('******** IN SR Steps Query ********');
                        SR_Steps__c NewSRStp = SRstp.clone(false, true);
                        NewSRStp.SR_Step_Id__c = srTemplate.Name+''+SRstp.Step_No__c+''+SRstp.Summary__c;
                        NewSRStp.Unique_Step_No__c = SRstp.Step_No__c+''+srTemplate.Id;
                        NewSRStp.SR_Template__c = srTemplate.Id;
                        LstNewSRSteps.add(NewSRStp);
                        
                        if(SRstp.Business_Rules__r!=null && SRstp.Business_Rules__r.size()>0){
                            for(Business_Rule__c BusRule:SRstp.Business_Rules__r){
                                setBRIds.add(BusRule.id);
                            }
                            //MapBrs.put(NewSRStp.Unique_Step_No__c+'',SRstp.Business_Rules__r.deepClone(false,false,false));
                        }
                        if(SRstp.Step_Transitions__r!=null && SRstp.Step_Transitions__r.size()>0){
                            //
                            //map of step_no as key with values as list of step transitions 
                            //
                            MapStpTransitions.put(NewSRStp.Unique_Step_No__c+'',SRstp.Step_Transitions__r.deepClone(false,false,false));
                        }
                    }
                    
                    Map<string,list<Business_Rule__c>> MapResivedBRs = new map<string,list<Business_Rule__c>>();
                    
                    if(setBRIds!=null && setBRIds.size()>0){
                        integer brNumber=1;
                        for(Business_Rule__c br:[Select id,Name,Action_Type__c,Sys_BR_Name__c,Business_Rule_Condition__c,Description__c,Execute_on_Insert__c,Execute_on_Update__c,SR_Steps__c,SR_Steps__r.Step_No__c,Text_Condition__c,
                        (select Id,BRCondition__c,Business_Rule__r.SR_Steps__c,Business_Rule__r.Name,Business_Rule__c,Class_Name__c,Close_Bracket__c,Conditional_Operator__c,Field_Name__c,Field_Type__c,
                        Object_Name__c,Open_Bracket__c,Operator__c,Pricing_Line__c,Property_Level__c,S_No__c,SR_Template_Docs__c,Value__c from Conditions_Business_Rule__r),
                        (select id,Name,Action_Type__c,Business_Rule__r.SR_Steps__c,Business_Rule__c,Business_Rule__r.Name,Class_Name__c,Custom_Method__c,Field_Name__c,
                        Field_Type__c,Field_Value__c,Priority__c,SR_RecordType__c,Step_Template__c,Value_or_Field__c from Action_Business_Rule__r) from Business_Rule__c where ID IN:setBRIds]){
                            
                            list<Business_Rule__c> lstBrs = new list<Business_Rule__c>();
                            Business_Rule__c newBr = br.clone(false, true);
                            newBr.Name = strSRTemplateName+'-BR';
                            newBr.Sys_BR_Name__c = br.SR_Steps__c+'_'+brNumber;
                            
                            string strUniqueStepNo = br.SR_Steps__r.Step_No__c+''+srTemplate.Id;
                            if(strUniqueStepNo!=null){
                                //
                                // prepare a map for Step no as the key and list of bus rule as value 
                                //
                                if(MapBrs.get(strUniqueStepNo)!=null && MapBrs.get(strUniqueStepNo).size()>0){
                                    lstBrs = MapBrs.get(strUniqueStepNo);
                                    lstBrs.add(newBr);
                                    MapBrs.put(strUniqueStepNo,lstBrs);
                                }else{
                                    lstBrs.add(newBr);
                                    MapBrs.put(strUniqueStepNo,lstBrs);
                                }
                            }
                            //
                            //prepare two maps for contions and action for the given bus rule.
                            //
                            if(br.Conditions_Business_Rule__r!=null && br.Conditions_Business_Rule__r.size()>0){
                                list<Condition__c> lstCondition = new list<Condition__c>();
                                lstCondition = br.Conditions_Business_Rule__r.deepClone(false,false,false);
                                MapBRConditions.put(newBr.Sys_BR_Name__c,lstCondition);
                            }
                            if(br.Action_Business_Rule__r!=null && br.Action_Business_Rule__r.size()>0){
                                list<Action__c> lstActns = new list<Action__c>();
                                lstActns = br.Action_Business_Rule__r.deepClone(false,false,false);
                                MapBRActions.put(newBr.Sys_BR_Name__c,lstActns);
                            }
                            brNumber++;
                        }
                        
                    }
                    
                    /*
                    if(setBRIds!=null && setBRIds.size()>0){
                        integer Cond = 1;
                        for(Condition__c cnd:[select Id,BRCondition__c,Business_Rule__r.SR_Steps__c,Business_Rule__r.Name,Business_Rule__c,Class_Name__c,Close_Bracket__c,Conditional_Operator__c,Field_Name__c,Field_Type__c,
                        Object_Name__c,Open_Bracket__c,Operator__c,Pricing_Line__c,Property_Level__c,S_No__c,SR_Template_Docs__c,Value__c from Condition__c where Business_Rule__c IN:setBRIds]){
                            String BRName = cnd.Business_Rule__r.Name;
                            //BRName = Cond+'-'+srTemplate.Name;
                            if(BRName!=null && BRName.length()>80){
                                BRName = BRName.substring(0,80);
                            }
                            Condition__c cloneCond = cnd.clone(false, true);
                            list<Condition__c> lstCondition = new list<Condition__c>();
                            if(MapBRConditions.get(BRName)!=null && MapBRConditions.get(BRName).size()>0){
                                lstCondition = MapBRConditions.get(BRName);
                                lstCondition.add(cloneCond);
                                MapBRConditions.put(BRName,lstCondition);
                            }else{
                                lstCondition.add(cloneCond);
                                MapBRConditions.put(BRName,lstCondition);
                            }
                            Cond++;
                        }
                        integer BRNo = 1;
                        for(Action__c act:[select id,Name,Action_Type__c,Business_Rule__r.SR_Steps__c,Business_Rule__c,Business_Rule__r.Name,Class_Name__c,Custom_Method__c,Field_Name__c,Field_Type__c,Field_Value__c,Priority__c,SR_RecordType__c,Step_Template__c,Value_or_Field__c from Action__c where Business_Rule__c IN:setBRIds]){
                            String BRName = act.Business_Rule__r.Name;
                            //BRName = BRNo+'-'+srTemplate.Name;
                            if(BRName!=null && BRName.length()>80){
                                BRName = BRName.substring(0,80);
                            }
                            Action__c Cloneact = act.clone(false, true);
                            list<Action__c> lstActns = new list<Action__c>();
                            if(MapBRActions.get(BRName)!=null && MapBRActions.get(BRName).size()>0){
                                lstActns = MapBRActions.get(BRName);
                                lstActns.add(Cloneact);
                                MapBRActions.put(BRName,lstActns);
                            }else{
                                lstActns.add(Cloneact);
                                MapBRActions.put(BRName,lstActns);
                            }
                            BRNo++;
                        }
                    }
                    */
                    
                    system.debug('LstNewSRSteps==>'+LstNewSRSteps);
                    if(LstNewSRSteps!=null && LstNewSRSteps.size()>0){
                        try{
                            insert LstNewSRSteps;
                        }catch(Exception e){
                            Database.rollback(svpoint);
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,string.valueOf(e.getMessage())));
                        }
                        
                        for(SR_Steps__c InsSRStep:LstNewSRSteps){
                            mapNewSRStepNameId.put(InsSRStep.Unique_Step_No__c,InsSRStep.Id);
                            mapStepNoId.put(InsSRStep.Step_No__c,InsSRStep.Id);
                            //prepare a new map with step no as the key and new step id as the value
                        }
                        system.debug('mapNewSRStepNameId==>'+mapNewSRStepNameId);
                        system.debug('mapStepNoId==>'+mapStepNoId);
                        
                        //
                        // to insert the tmplate items, for that we need to replace the cons at and eval at step id , this is done below
                        //
                        if(lstSRTemplateItemsToBeInsert!=null && lstSRTemplateItemsToBeInsert.size()>0){
                            for(integer i=0;i<lstSRTemplateItemsToBeInsert.size();i++){
                                decimal ConsumedStepNo;
                                decimal EvaluatedStepNo;
                                string SysNameField = lstSRTemplateItemsToBeInsert[i].Sys_Template_Name__c;
                                if(SysNameField!=null && mapTemplItemNoConsumedSTepNo.get(SysNameField)!=null){
                                    //
                                    //to get the consumed at step id , pass the step no into mapStepNoId
                                    //
                                    ConsumedStepNo = mapTemplItemNoConsumedSTepNo.get(SysNameField);
                                    system.debug('ConsumedStepNo==>'+ConsumedStepNo);
                                    if(ConsumedStepNo!=null && mapStepNoId.get(ConsumedStepNo)!=null){
                                        lstSRTemplateItemsToBeInsert[i].Consumed_at__c = mapStepNoId.get(ConsumedStepNo);
                                    }
                                }

                                    //to get the evaluate at step id , pass the step no into mapStepNoId
                                    //                              
                                if(SysNameField!=null && mapTemplItemNoEvaluatedSTepNo.get(SysNameField)!=null){
                                    EvaluatedStepNo = mapTemplItemNoEvaluatedSTepNo.get(SysNameField);
                                    system.debug('EvaluatedStepNo==>'+EvaluatedStepNo);
                                    if(EvaluatedStepNo!=null && mapStepNoId.get(EvaluatedStepNo)!=null){
                                        lstSRTemplateItemsToBeInsert[i].Evaluated_at__c = mapStepNoId.get(EvaluatedStepNo);
                                    }
                                }
                            }
                            try{
                                insert lstSRTemplateItemsToBeInsert;
                            }catch(Exception e){
                                Database.rollback(svpoint);
                                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,string.valueOf(e.getMessage())));
                            }
                        }
                        
                        //
                        // to insert the tmplate docs, for that we need to replace the cour collec at, cour delv at,  and eval at step id , this is done below
                        //
                        if(lstSRTemplateDocsToBeInsert!=null && lstSRTemplateDocsToBeInsert.size()>0){
                            for(integer i=0;i<lstSRTemplateDocsToBeInsert.size();i++){
                                decimal CourierCollecNo;
                                decimal CourierDelivNo;
                                decimal EvaluatedStepNo;
                                string SysTemplDocName = lstSRTemplateDocsToBeInsert[i].Sys_Template_Doc_Name__c;
                                //CloneTemplDoc.Sys_Template_Doc_Name__c
                                if(SysTemplDocName!=null && mapTemplDocNoCourCollSTepNo.get(SysTemplDocName)!=null){
                                    CourierCollecNo = mapTemplDocNoCourCollSTepNo.get(SysTemplDocName);
                                    system.debug('CourierCollecNo==>'+CourierCollecNo);
                                    if(CourierCollecNo!=null && mapStepNoId.get(CourierCollecNo)!=null){
                                        lstSRTemplateDocsToBeInsert[i].Courier_collected_at__c = mapStepNoId.get(CourierCollecNo);
                                    }
                                }
                                if(SysTemplDocName!=null && mapTemplDocNoCourDelivSTepNo.get(SysTemplDocName)!=null){
                                    CourierDelivNo = mapTemplDocNoCourDelivSTepNo.get(SysTemplDocName);
                                    system.debug('CourierDelivNo==>'+CourierDelivNo);
                                    if(CourierDelivNo!=null && mapStepNoId.get(CourierDelivNo)!=null){
                                        lstSRTemplateDocsToBeInsert[i].Courier_delivered_at__c = mapStepNoId.get(CourierDelivNo);
                                    }
                                }
                                if(SysTemplDocName!=null && mapTemplDocNoEvaluatedSTepNo.get(SysTemplDocName)!=null){
                                    EvaluatedStepNo = mapTemplDocNoEvaluatedSTepNo.get(SysTemplDocName);
                                    system.debug('EvaluatedStepNo==>'+EvaluatedStepNo);
                                    if(EvaluatedStepNo!=null && mapStepNoId.get(EvaluatedStepNo)!=null){
                                        lstSRTemplateDocsToBeInsert[i].Evaluated_at__c = mapStepNoId.get(EvaluatedStepNo);
                                    }
                                }
                            }
                            
                            try{
                                insert lstSRTemplateDocsToBeInsert;
                            }catch(Exception e){
                                Database.rollback(svpoint);
                                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,string.valueOf(e.getMessage())));
                            }
                            
                            map<string,id> mapTemplDocNoID = new map<string,id>();
                            //integer newTemplDocNo=1;
                            for(SR_Template_Docs__c TemplDoc:lstSRTemplateDocsToBeInsert){
                                // create map with templ doc name as key and id as value
                                // this is to replace the id 
                                mapTemplDocNoID.put(TemplDoc.Sys_Template_Doc_Name__c,TemplDoc.id);
                                //newTemplDocNo++;
                            }
                            // replace id in the MapDocConditions
                            list<Condition__c> lstDocCondtionsToBeInsert = new list<Condition__c>();
                            if(MapDocConditions!=null && MapDocConditions.size()>0){
                                for(string DocName:MapDocConditions.keyset()){
                                    for(Condition__c Clonecon:MapDocConditions.get(DocName)){
                                        if(mapTemplDocNoID.get(DocName)!=null){
                                            Clonecon.SR_Template_Docs__c = mapTemplDocNoID.get(DocName);
                                            lstDocCondtionsToBeInsert.add(Clonecon);
                                        }
                                    }
                                }
                            }
                            if(lstDocCondtionsToBeInsert!=null && lstDocCondtionsToBeInsert.size()>0){
                                try{
                                    insert lstDocCondtionsToBeInsert;
                                }catch(Exception e){
                                    Database.rollback(svpoint);
                                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,string.valueOf(e.getMessage())));
                                }
                            }
                        }
                    }
                    
                    list<Business_Rule__c> lstBRsTobeInserted = new list<Business_Rule__c>();
                    list<Step_Transition__c> lstStpTransitions = new list<Step_Transition__c>();
                    list<Condition__c> lstCondTobeInserted = new list<Condition__c>();
                    list<Action__c> lstActTobeInserted = new list<Action__c>();
                    
                    system.debug('MapBrs==>'+MapBrs.keySet());
                    //
                    // replace ids for the business rules for every sr step
                    //
                    for(string SRStepNo:MapBrs.keySet()){
                        for(Business_Rule__c newBR : MapBrs.get(SRStepNo)){
                            //newBR.Name = ''+srTemplate.Name;
                            if(newBR.Name.length()>80){
                                newBR.Name = newBR.Name.substring(0,80);
                            }
                            newBR.SR_Steps__c = mapNewSRStepNameId.get(SRStepNo);
                            system.debug('newBR.Sys_BR_Name__c==>'+newBR.Sys_BR_Name__c);
                            lstBRsTobeInserted.add(newBR);
                        }
                    }
                    
                    if(lstBRsTobeInserted!=null && lstBRsTobeInserted.size()>0){
                        try{
                            insert lstBRsTobeInserted;
                        }catch(Exception e){
                            Database.rollback(svpoint);
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,string.valueOf(e.getMessage())));
                        }
                        for(Business_Rule__c br:lstBRsTobeInserted){
                            mapNewBRNamesId.put(br.Sys_BR_Name__c,br.Id);
                        }
                    }
                    
                    system.debug('mapNewBRNamesId==>'+mapNewBRNamesId);
                    
                    for(string SRStepNo:MapStpTransitions.keySet()){
                        for(Step_Transition__c newStpTrans : MapStpTransitions.get(SRStepNo)){
                            newStpTrans.SR_Template__c = srTemplate.Id;
                            newStpTrans.SR_Step__c = mapNewSRStepNameId.get(SRStepNo);
                            lstStpTransitions.add(newStpTrans);
                        }
                    }
                    
                    if(lstStpTransitions!=null && lstStpTransitions.size()>0){
                        try{
                            insert lstStpTransitions;
                        }catch(Exception e){
                            Database.rollback(svpoint);
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,string.valueOf(e.getMessage())));
                        }
                    }
                    
                    system.debug('MapBRConditions==>'+MapBRConditions);
                    system.debug('MapBRActions==>'+MapBRActions);
                    //
                    //replace the business rule id for the new template
                    //
                    for(string BRName:MapBRConditions.keySet()){
                        for(Condition__c newCond : MapBRConditions.get(BRName)){
                            if(mapNewBRNamesId.get(BRName)!=null){
                                newCond.Business_Rule__c = mapNewBRNamesId.get(BRName);
                                newCond.Pricing_Line__c = null;
                                newCond.SR_Template_Docs__c = null;
                                lstCondTobeInserted.add(newCond);
                            }
                        }
                    }
                    
                    if(lstCondTobeInserted!=null && lstCondTobeInserted.size()>0){
                        try{
                            insert lstCondTobeInserted;
                        }catch(Exception e){
                            Database.rollback(svpoint);
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,string.valueOf(e.getMessage())));
                        }
                    }
                    //
                    //replace the business rule id for actions
                    //
                    for(string BRName:MapBRActions.keySet()){
                        for(Action__c newAct : MapBRActions.get(BRName)){
                            if(mapNewBRNamesId.get(BRName)!=null){
                                newAct.Business_Rule__c = mapNewBRNamesId.get(BRName);
                                lstActTobeInserted.add(newAct);
                            }
                        }
                    }
                    
                    if(lstActTobeInserted!=null && lstActTobeInserted.size()>0){
                        try{
                            insert lstActTobeInserted;
                        }catch(Exception e){
                            Database.rollback(svpoint);
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,string.valueOf(e.getMessage())));
                        }
                    }
                    
                    pagereference ref = new pagereference('/'+srTemplate.id);
                    ref.setredirect(true);
                    return ref;
                    
                }else{
                    return null;
                }
            }else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Duplicate SR-Template found with this SR-Template Name (or) SR Record Type Name'));
                return null;
            }
    }
}