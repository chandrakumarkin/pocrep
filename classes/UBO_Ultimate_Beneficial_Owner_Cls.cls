/*************************************************
* Class : UBO_Ultimate_Beneficial_Owner_Cls 
* Description : Class to Separate the UBO process from amendment to UBO data.Assembla #6408
* Date : 24/02/2019 
    
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By      Description
--------------------------------------------------------------------------------------------------------------------------            
1.0    19/Mar/2019   Arun           #6500  fixes 
2.0    23/Jun/2010   Sai            UBO Update and Remove Changes 
**************************************************/    
public without sharing class UBO_Ultimate_Beneficial_Owner_Cls extends Cls_ProcessFlowComponents 
{
   
    public List<UBO_Data__c> ListofUBOs{get;set;}
    public UBO_Data__c ObjUBO{get;set;}
    private Map<string,id> RecordTypeIds;
    public string UBOType{get;set;}
    private account AccountObj{get;set;}
    public string srID {get;set;}
    public Id UboId {get;set;}
    public date RemovedDate{get;set;}
    public UBO_Data__c ObjRemoveUBO{get;set;}
    public boolean AllowtoAdd{get;set;}
    public string FormMode {get;set;}
    public string Listtype {get;set;}//Field to control what to show to screen 
    public boolean IsShowSaveButton {get;set;}//Field to control what to show to screen 
    public boolean isUpdate{get;set;}    //This variable used to Display Update UBO Form.
    public boolean isRemove{get;set;}    //This variable used to Display Remove UBO Form.
    public Boolean isSanctionedNationality{get;set;}                                       
    public boolean isRLP{get;set;}
    
    public List<UBO_Data__c> getListofAll()
    {
        isUpdate = FALSE;
        isRemove = FALSE;
        ListofUBOs = new List<UBO_Data__c>();
        ListofUBOs=  [select Place_of_Registration__c,Status_Formula__c,Middle_Name__c,Are_you_a_resident_in_the_U_A_E__c,U_I_D_No__c,Former_Name__c,Place_of_Issuance__c,Individual_UBO__c ,Nationality__c,Reason_for_exemption__c,RecordType.Name,RecordType.ID,Passport_Number__c,
                      Passport_Expiry_Date__c,Apartment_or_Villa_Number__c,BC_UBO__c,Building_Name__c,City_Town__c,Country__c,Date_Of_Becoming_a_UBO__c,
                      Date_of_Birth__c,Date_Of_Registration__c,Status__c,Income_and_Origin_of_Wealth__c,Email__c,Emirate_State_Province__c,Entity_Name__c,Last_Name__c,First_Name__c,Type__c,
                      Entity_Type__c from UBO_Data__c where Service_Request__c=:objSr.Id and Recordtype.name='Individual'];
         
        for(UBO_Data__c Obj:ListofUBOs)
        {
            if(Obj.Entity_Type__c!=null)
            if(Obj.Entity_Type__c.contains('deemed'))
            {
                AllowtoAdd=false;
            }
                         
        }
        return ListofUBOs;
    }

   public List<UBO_Data__c> getListofBCAll()
   {
        isUpdate = FALSE;
        isRemove = FALSE;
        List<UBO_Data__c> lstUBOData = new List<UBO_Data__c>();
        
        lstUBOData = [select Place_of_Registration__c,Individual_UBO__c,Nationality__c,Reason_for_exemption__c,RecordType.Name,RecordType.ID,Passport_Number__c,
                 Passport_Expiry_Date__c,Apartment_or_Villa_Number__c,BC_UBO__c,Building_Name__c,City_Town__c,Country__c,Date_Of_Becoming_a_UBO__c,
                 Date_of_Birth__c,Date_Of_Registration__c,Email__c,Emirate_State_Province__c,Entity_Name__c,Last_Name__c,First_Name__c,Entity_Type__c 
                 from UBO_Data__c where Service_Request__c=:objSr.Id and Recordtype.name ='Body Corporation UBO' and Status__c!='Remove'];
        system.debug('!!!@@@'+lstUBOData);
        return lstUBOData;
    
   }




public UBO_Ultimate_Beneficial_Owner_Cls()
{
        
        ListofUBOs = new List<UBO_Data__c>();
        isUpdate = FALSE;
        isRemove = FALSE;
        ObjRemoveUBO=new UBO_Data__c ();
        IsShowSaveButton=false;//Do not save button
        FormMode='Add';
        AllowtoAdd=true;
        srID = ApexPages.currentPage().getParameters().get('SRid');
        //UBOType = ApexPages.currentPage().getParameters().get('UBOType');
        
         RecordTypeIds=new Map<string,id>();
          for(RecordType objRT:[select Id,Name,DeveloperName from RecordType where  SobjectType='UBO_Data__c'])
          {
              RecordTypeIds.put(objRT.DeveloperName,objRT.id);
          }
         
         
         
         if(ListofUBOs==null)
         ListofUBOs=new List<UBO_Data__c>();
     
     
    // ListofUBOs=[select Type__c,Place_of_Registration__c,Reason_for_exemption__c,RecordType.Name,RecordType.ID,Passport_Number__c,Passport_Expiry_Date__c,Apartment_or_Villa_Number__c,BC_UBO__c,Building_Name__c,City_Town__c,Country__c,Date_Of_Becoming_a_UBO__c,
       //  Date_of_Birth__c,Date_Of_Registration__c,Email__c,Emirate_State_Province__c,Entity_Name__c,Last_Name__c,First_Name__c,Entity_Type__c from UBO_Data__c where Service_Request__c=:objSr.Id];
         //fatch only if no record added in UBO list 
       
        //if(ListofUBOs.isEmpty())
         //    ExistingRecords();
            // else
             
             //Check if customer selected all option then do not show any button to add UBO or exp 
             //getItems();
             getListofAll();
             
             
             
             
         
    
        
 }
 
 
   public override pagereference DynamicButtonAction(){
        
        isUpdate = FALSE;
        isRemove = FALSE;
        if(IsValidInfo()==true || !isNext())
        //if(!isNext())
        {
            upsert ListofUBOs;
            return getButtonAction();
        }
       else  
       {
           
       }
        clearAction();
        
        return null;
    }
  
 public boolean IsValidInfo()
 {
     
     isUpdate = FALSE;
     isRemove = FALSE;
     Boolean LastValueSelectedFound=false;
     Boolean NotLastValueSelectedFound=false;
     boolean IsValidData=true;
     
        for(UBO_Data__c Obj:getListofAll())
        {
            if(Obj.Entity_Type__c!=null)
            {
                if(Obj.Entity_Type__c.contains('deemed'))
                         {
                             LastValueSelectedFound=true;
                         }
                         else
                             NotLastValueSelectedFound=true;
                         
                    if(LastValueSelectedFound==true && NotLastValueSelectedFound==true)
                    {
                            IsValidData=false;
                            setMessage('Members of the governing body cannot be deemed as a UBO if the entity has already identified natural persons as UBOs');
        
                    }
            
                
            }
            else
            {
                //V1.0  
             //   setMessage('Please update the type of ownership or control of existing UBOs.');
               //  IsValidData=false;
            }
                 
             
            //else
            //  IsValidData=false;
            
            /*

            Integer result=EntyTypestring.indexOf(Obj.Entity_Type__c);
            
            
             System.debug('Obj===>'+Obj);
             System.debug('result===>'+result);

            if(result==EntyTypestring.size()-1)
            {
                LastValueSelectedFound=true;
            }
            else
            {
                NotLastValueSelectedFound=true;
            }
            
            if(LastValueSelectedFound==true && NotLastValueSelectedFound==true)
            {
                IsValidData=false;
            }
            */
            
        }
        /*V1.0  
         for(UBO_Data__c Obj:getListofBCAll())
        {
            if(Obj.Reason_for_exemption__c==null)
            {
                setMessage('Please update the type of exempt entity.');
                 IsValidData=false;
                
            }
        }
        */
     
     System.debug('IsValidData===>'+IsValidData);
     
    return IsValidData;
 } 
    
    
 public PageReference AddUBO()
 {
    
    isUpdate = FALSE; 
    isRemove = FALSE;
    ObjUBO =new UBO_Data__c();
    UBOType='Exempt_entities_UBO';
    return null;
 }

 public PageReference CancelUBO()
 {
    isUpdate = FALSE;
    isRemove = FALSE;
    ObjUBO =null;
    EntityTypeSeleted=null;
    ListRelCompanies=null;
    getListofAll();
    IsShowSaveButton=false;
    return null;
 }
  
 public void SaveUBO()
 {
     boolean isvalid=true;
     
     isRemove = FALSE;
     try
     {
         
    //V1.0   
    system.debug('---save--'+ListofUBOs);
     if(EntityTypeSeleted!='ListShare')
     {
        
          //if(FormMode!='Edit')
         //ListofUBOs.add(ObjUBO); 
         //setInfoMessage('Record save successfully');
         //Code to find duplicate crecords 
         if(ObjUBO.id==null && ObjUBO.Passport_Number__c !=null)
         {
             for(UBO_Data__c Obj:getListofAll())
            {
                if(Obj.Passport_Number__c==ObjUBO.Passport_Number__c && 
                Obj.Nationality__c==ObjUBO.Nationality__c)
                {
                    isvalid=false;
                    setMessage('There is a UBO already existing with similar Passport Number :'+ObjUBO.Passport_Number__c);
                
                }
                
                
                
            }
             
         }
         
         if(isvalid)
         
         upsert ObjUBO;
         system.debug('$$$$$'+ObjUBO);
     }
     else
     {
         
         for(CompnayRelationships ObjRel:ListRelCompanies)
         {
             if(ObjRel.IsSelected==true)
             {
                UBO_Data__c TempObjUBO =new UBO_Data__c();
                TempObjUBO.RecordTypeId=RecordTypeIds.get(UBOType);
                TempObjUBO.Entity_Type__c=ObjUBO.Entity_Type__c;
                TempObjUBO.Service_Request__c = objSr.Id;
               
                TempObjUBO=SetUBOData(TempObjUBO,ObjRel.ComRecords.Object_Contact__r,ObjRel.ComRecords.Start_Date__c,ObjRel.ComRecords.id);
                System.debug('==================TempObjUBO============>'+TempObjUBO);
                ListofUBOs.add(TempObjUBO); 
               
                 
             }
             
            
             
         }
         System.debug('==================ListofUBOs============>'+ListofUBOs);
         //setInfoMessage('Record save successfully');
          upsert ListofUBOs;
     }
    system.debug('!!@@#@@@'+ObjUBO+'Sai Kalyan$$$$'+ListofUBOs);
    if(isvalid) 
   CancelUBO();// clear all the values 
   isUpdate = TRUE;
     //PageReference pg= new PageReference(ApexPages.currentPage().getParameters().get('retURL')); 
     //return null;
     }
     catch(DmlException e)
     {
           //Apexpages.addMessage(new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getMessage()));
     }
     
 }
  
   @TestVisible private UBO_Data__c SetUBOData(UBO_Data__c TempObjUBO, Contact ObjContact,date DateUBO,id RelId)
  {
            isUpdate = FALSE;
            isRemove = FALSE;
            TempObjUBO.Title__c=ObjContact.Salutation;
            TempObjUBO.First_Name__c=ObjContact.FirstName;
            TempObjUBO.Last_Name__c=ObjContact.LastName;
            TempObjUBO.Gender__c=ObjContact.Gender__c;
            TempObjUBO.Date_of_Birth__c=ObjContact.Birthdate;
            TempObjUBO.Place_of_Birth__c=ObjContact.Place_of_Birth__c;
            TempObjUBO.Passport_Number__c=ObjContact.Passport_No__c;
            TempObjUBO.Passport_Expiry_Date__c=ObjContact.Passport_Expiry_Date__c;
            TempObjUBO.Passport_Data_of_Issuance__c=ObjContact.Date_of_Issue__c;
            TempObjUBO.Nationality__c=ObjContact.Nationality__c;
            TempObjUBO.Phone_Number__c=ObjContact.Phone;
            TempObjUBO.Email__c=ObjContact.Email;
            TempObjUBO.Date_Of_Becoming_a_UBO__c=DateUBO;
            
            TempObjUBO.Relationship__c=RelId;
            TempObjUBO.RecordTypeId=RecordTypeIds.get('Individual');
            
            TempObjUBO.Individual_UBO__c=ObjContact.id;
            TempObjUBO.Apartment_or_Villa_Number__c=ObjContact.Office_Unit_Number__c;
            TempObjUBO.Building_Name__c=ObjContact.ADDRESS_LINE1__c;
            TempObjUBO.Street_Address__c=ObjContact.ADDRESS_LINE2__c;//Changed 19/Mar/2019
            TempObjUBO.PO_Box__c=ObjContact.PO_Box_HC__c;
            TempObjUBO.City_Town__c=ObjContact.CITY1__c;//Changed 19/Mar/2019
              
            
            
            TempObjUBO.Emirate_State_Province__c=ObjContact.Emirate_State_Province__c;
            TempObjUBO.Post_Code__c=ObjContact.Postal_Code__c;
            TempObjUBO.Country__c=ObjContact.Country__c;
            return TempObjUBO;
            
      
  }
  
   @TestVisible private UBO_Data__c SetUBODataBC(UBO_Data__c TempObjUBO, Account  ObjAccount,date DateUBO,id RelId)
  {
            isUpdate = FALSE;
            isRemove = FALSE;
            TempObjUBO.Entity_Name__c=ObjAccount.Name;
            //TempObjUBO.Reason_for_exemption__c=ObjContact.BC_UBO_Type__c;
            TempObjUBO.Date_Of_Registration__c=ObjAccount.ROC_reg_incorp_Date__c;
            TempObjUBO.Select_Regulator_Exchange_Govt__c=ObjAccount.Place_of_Registration__c;
            TempObjUBO.Phone_Number__c=ObjAccount.Phone;
            TempObjUBO.Select_Regulator_Exchange_Govt__c =ObjAccount.Jurisdiction__c;
            TempObjUBO.Name_of_Regulator_Exchange_Govt__c =ObjAccount.Name_of_Regulator_Exchange_Govt__c;
            TempObjUBO.Reason_for_exemption__c =ObjAccount.Reason_for_exemption__c;
            TempObjUBO.Relationship__c=RelId;
            TempObjUBO.RecordTypeId=RecordTypeIds.get('Body_Corporation_UBO');
            TempObjUBO.BC_UBO__c=ObjAccount.id;
             
           // TempObjUBO.Apartment_or_Villa_Number__c=ObjAccount.;
           system.debug('!!@@!!!'+ObjAccount.Apartment_Villa_Office_Number__c+'$$$###'+ObjAccount.City__c+'()'+'***'+ObjAccount.Street__c);
           TempObjUBO.Apartment_or_Villa_Number__c = ObjAccount.Apartment_Villa_Office_Number__c;
           TempObjUBO.Street_Address__c = ObjAccount.Street__c;
           TempObjUBO.City_Town__c = ObjAccount.City__c;
            TempObjUBO.Building_Name__c=ObjAccount.Building_Name__c;
            TempObjUBO.PO_Box__c=ObjAccount.PO_Box__c;
            TempObjUBO.Emirate_State_Province__c=ObjAccount.Emirate_State__c;
            TempObjUBO.Post_Code__c=ObjAccount.Postal_Code__c;
            TempObjUBO.Country__c=ObjAccount.Country__c;
            return TempObjUBO;
            
      
  }
  
 public PageReference AddBCUBO()
 {
     isUpdate = FALSE;
     isRemove = FALSE;
     IsShowSaveButton=true;
     ObjUBO =new UBO_Data__c();
     ObjUBO.Legal_Type_of_Entity__c=objSR.Legal_Structures__c;
     UBOType='Body_Corporation_UBO';
     ObjUBO.RecordTypeId=RecordTypeIds.get(UBOType);
     ObjUBO.I_am_Exempt_entities__c='Yes';
      ObjUBO.Service_Request__c = objSr.Id;
     return null;
 }
 public PageReference AddIndUBO()
 {
    isUpdate = FALSE; 
    isRemove = FALSE;
   
   
    ObjUBO =new UBO_Data__c();
    UBOType='Individual';
    EntityTypeSeleted=null;
     ObjUBO.Legal_Type_of_Entity__c=objSR.Legal_Structures__c;
    //EntityTypeSeleted='Openform';
    ObjUBO.RecordTypeId=RecordTypeIds.get(UBOType);
    ObjUBO.I_am_Exempt_entities__c='No';
     ObjUBO.Service_Request__c = objSr.Id;
     return null;
 
 }
 
 public string EntityTypeSeleted{get;set;}// 
 public List<CompnayRelationships> ListRelCompanies{get;set;}
 
  public void AddIndTypeSeletedNew()
  {
     isUpdate = FALSE;
     isRemove = FALSE;
     IsShowSaveButton=true;
     List<String> RelCodes ;
     if(ObjUBO.Type__c!=null)
     {
         
         if(ObjUBO.Type__c=='Direct')
         {
                EntityTypeSeleted='ListShare';
                Listtype='LastindexShare';
                RelCodes = new List<String>{'Is Shareholder Of'};
                ListRelCompanies=new List<CompnayRelationships>();
                for(Relationship__c Obj:getRelationships(RelCodes)){
                CompnayRelationships ObjCom=new CompnayRelationships();
                ObjCom.ComRecords=Obj;
                ObjCom.IsSelected=false;
                ListRelCompanies.add(ObjCom);
                }
        
             
         }
         else
         {
              EntityTypeSeleted='Openform';
                isRLP = FALSE;
                 Account eachAccount = new Account();
                 Service_Request__c objSRequest = new Service_Request__c();
                 objSRequest = [SELECT Customer__c FROM Service_Request__c where ID=:objSR.ID];
                 if(objSRequest.Customer__c !=null){
                    eachAccount = [SELECT Legal_Type_of_Entity__c FROM Account where ID=:objSR.Customer__c];
                    if(eachAccount.Legal_Type_of_Entity__c !=null && (eachAccount.Legal_Type_of_Entity__c== 'RLLP' || eachAccount.Legal_Type_of_Entity__c =='LLP')){
                        isRLP = TRUE;       
                    }
                    system.debug('!!@@@!!!'+isRLP);
                 }
              
         }
         
     }
     else if(ObjUBO.Entity_Type__c !=null && ObjUBO.Entity_Type__c.contains('deemed'))
     {
          Listtype='Lastindex';
         /*
         In case the clients selects “The individual members of the governing body who are deemed to be UBO”, 
         the system will pre-populate the details of the 
         director /general partners/member/founding members/council members as applicable
         */
        EntityTypeSeleted='ListShare';
        
         RelCodes = new List<String>{'Has Director', 'Has General Partner','Has Member','Has Founding Member','has Council Member'};
         
        
        ListRelCompanies=new List<CompnayRelationships>();
        for(Relationship__c Obj:getRelationships(RelCodes)){
            CompnayRelationships ObjCom=new CompnayRelationships();
            ObjCom.ComRecords=Obj;
            ObjCom.IsSelected=true;
            ListRelCompanies.add(ObjCom);
        }
        
         
     }
     else{
          EntityTypeSeleted='Openform';
     }
 }
 
 public List<Relationship__c> getRelationshipsBC(List<String> RelCodes)
{
    isUpdate = FALSE;
    isRemove = FALSE;
    return [select id,Relationship_Type_formula__c,
         Object_Account__r.Name,	
         Object_Account__r.ROC_reg_incorp_Date__c,
         
         Start_Date__c,
         Object_Account__r.Phone,
         BC_UBO_Type__c,
         Object_Account__r.Place_of_Registration__c,
         Object_Account__r.Office__c,
         Object_Account__r.Building_Name__c,
         Object_Account__r.Street__c,
         Object_Account__r.PO_Box__c,
         Object_Account__r.City__c,
         Object_Account__r.Postal_Code__c,
         Object_Account__r.Country__c,
         Object_Account__r.Emirate__c,
         Object_Account__r.Jurisdiction__c,Object_Account__r.Apartment_Villa_Office_Number__c, 
         Object_Account__r.Name_of_Regulator_Exchange_Govt__c, 
         Object_Account__r.Reason_for_exemption__c,Object_Account__r.Emirate_State__c     
         from Relationship__c where Subject_Account__c=:objSr.Customer__c and Relationship_Type__c in :RelCodes and Object_Account__c!=null and Active__c=true];
    
}

 
public List<Relationship__c> getRelationships(List<String> RelCodes)
{
    isUpdate = FALSE;
    isRemove = FALSE;
    return [select id,Relationship_Type_formula__c,
         Object_Contact__r.Salutation,
         Object_Contact__r.FirstName,
         Object_Contact__r.LastName,
         Object_Contact__r.Gender__c,
         
         Object_Contact__r.Birthdate,
         Object_Contact__r.Place_of_Birth__c,
         Object_Contact__r.Email,
         Object_Contact__r.Phone,
         
         Object_Contact__r.Passport_No__c,
         Object_Contact__r.Date_of_Issue__c,
         Object_Contact__r.Passport_Expiry_Date__c,
         Object_Contact__r.Nationality__c,
         
         Start_Date__c,
         
         Object_Contact__r.Office_Unit_Number__c,
         Object_Contact__r.ADDRESS_LINE1__c,
         Object_Contact__r.PO_Box_HC__c,
         Object_Contact__r.Emirate_State_Province__c,
         Object_Contact__r.Country__c,
         Object_Contact__r.ADDRESS_LINE2__c,
         Object_Contact__r.CITY1__c,
         Object_Contact__r.Postal_Code__c        
         from Relationship__c where Subject_Account__c=:objSr.Customer__c and Relationship_Type__c in :RelCodes and Object_Contact__c!=null and Active__c=true];
    
}
public class CompnayRelationships
{
    public boolean IsSelected{get;set;}
    public Relationship__c ComRecords{get;set;}
    
}  
 
 //Get all the existing UBO records from rel
 public void ExistingRecords()
 {
    isUpdate = FALSE;
    isRemove = FALSE;
  System.debug('=============ExistingRecords==================');
  System.debug('Apt_or_Villa_No__c=='+objSr.Apt_or_Villa_No__c);
 if(objSr.Apt_or_Villa_No__c == null)
 {
     
         List<UBO_Data__c> lstUBORecord = new List<UBO_Data__c>();
         lstUBORecord  = [SELECT Id FROM UBO_Data__c where Service_Request__c=:objSr.Id AND RecordTypeId =:Label.Body_Corporation_UBO_Recordtype_ID];
         
        Id serviceRequestID;
        //if(ListofUBOs==null)
    
         ListofUBOs=new List<UBO_Data__c>();
         List<String> RelCodes = new List<String>{'Beneficiary Owner','UBO'};
         
         for(Relationship__c ObjRel:getRelationshipsBC(RelCodes))
         {
            if(lstUBORecord !=null && lstUBORecord.size()==0){
                UBO_Data__c TempObjUBO =new UBO_Data__c();
                TempObjUBO.Service_Request__c=objSr.Id;
                TempObjUBO.RecordTypeId=RecordTypeIds.get('Body_Corporation_UBO');
                TempObjUBO.Reason_for_exemption__c=ObjRel.BC_UBO_Type__c;
                TempObjUBO=SetUBODataBC(TempObjUBO,ObjRel.Object_Account__r,ObjRel.Start_Date__c,ObjRel.id);
                TempObjUBO.Status__c = 'Update';
                if(TempObjUBO.RecordTypeId ==Label.Body_Corporation_UBO_Recordtype_ID){
                	//ListofUBOs.add(TempObjUBO); 
                }
            }
         }
         
         System.debug('Apt_or_Villa_No__c=='+ListofUBOs);
         upsert ListofUBOs;
         System.debug('ListofUBOs==>'+ListofUBOs);
        
         objSr.Apt_or_Villa_No__c='Dataset';
         update objSr;
 }
 
 }
 

 
public List<SelectOption> EntyTypeoptions{get;set;}
public List<string> EntyTypestring{get;set;}    

 
    public void setInfoMessage(String message){
        isUpdate = FALSE;
        isRemove = FALSE;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,message));
      }
      
      
    public PageReference EditUBOInd() 
    {
        isUpdate = FALSE;
        isRemove = FALSE;
         FormMode='Edit';
        SetUBOData(UboId);
        
         UBOType='Individual';
           //EntityTypeSeleted='Openform';
        EntityTypeSeleted='Openform';
    IsShowSaveButton =true;
    
        return null;
        
    }
    
    
    public pagereference updateUBO(){
       
        isUpdate = TRUE;
        isRemove = FALSE;
        IsShowSaveButton = TRUE;
        
        ObjUBO = [SELECT First_Name__c,Last_Name__c,Nationality__c,Passport_Number__c,Date_of_Birth__c,First_Name_New__c ,Last_Name_New__c,
        Passport_Number_New__c,Nationality_New__c,Email_New__c,Phone_Number__c,Passport_Expiry_Date__c,Passport_Data_of_Issuance__c,Building_Name__c,
        City_Town__c,Country__c,PO_Box__c,Post_Code__c,Name_Contact_Details__c,Update_Passport_Details__c,Update_Address_Details__c,
        Are_you_a_resident_in_the_U_A_E__c,Entity_Type__c,Type__c,Apartment_or_Villa_Number__c,Street_Address__c       
        FROM UBO_Data__c where ID=:UboId];
        ObjUBO.Legal_Type_of_Entity__c=objSR.Legal_Structures__c;
        ObjUBO.RecordTypeId= Label.Individual_UBO;
        ObjUBO.I_am_Exempt_entities__c='No';
        UBOType = '';
        Boolean isSRUpdate = FALSE;
        
        for(UBO_Data__c eachUBO:[SELECT Id,Status__c FROM UBO_Data__c where Service_Request__c=:objSr.ID]){
            if(eachUBO.Status__c !='Draft'){
                isSRUpdate =TRUE;
            }
        }
        
        if(isSRUpdate){
            objSr.Apt_or_Villa_No__c='';
            UPDATE objSr;
        }
            
        return null;
    }
    
    
    
    public pagereference updateUBOForm(){
      
        isUpdate = TRUE;
        isRemove = FALSE;
        EntityTypeSeleted = '';
        IsShowSaveButton = TRUE;
        ObjUBO =new UBO_Data__c();
        ObjUBO.Status__c = 'Update';
        ObjUBO.Legal_Type_of_Entity__c=objSR.Legal_Structures__c;
        ObjUBO.RecordTypeId=Label.Individual_UBO;
        ObjUBO.I_am_Exempt_entities__c='No';
        Boolean isSRUpdate =FALSE;
        for(UBO_Data__c eachUBO:[SELECT Id,Status__c FROM UBO_Data__c where Service_Request__c=:objSr.ID]){
            if(eachUBO.Status__c !='New'){
                isSRUpdate =TRUE;
            }
        }
        
        if(isSRUpdate){
            objSr.Apt_or_Villa_No__c='';
            UPDATE objSr;
        }
        ObjUBO.Service_Request__c = objSr.Id;
        return null;
    }
  
    
    public pagereference removeUBOForm(){
        
        isUpdate = FALSE;
        isRemove = TRUE;
         EntityTypeSeleted = '';
        IsShowSaveButton = TRUE;
        ObjUBO =new UBO_Data__c();
        ObjUBO.Status__c = 'Remove';
        ObjUBO.Legal_Type_of_Entity__c=objSR.Legal_Structures__c;
        ObjUBO.RecordTypeId=Label.Individual_UBO;
        ObjUBO.I_am_Exempt_entities__c='No';
        ObjUBO.Service_Request__c = objSr.Id;
        return null;
    } 
    
    public PageReference EditUBOBC() 
    {
        isUpdate = FALSE;
        isRemove = FALSE;
        FormMode='Edit';
        SetUBOData(UboId);
         UBOType='Body_Corporation_UBO';
         IsShowSaveButton =true;
         ObjUBO.Id = UboId;
          ObjUBO.Status__c = 'Update';
        return null;
        
    }
    
   @TestVisible private void SetUBOData(Id TempObjUBOId)
  {
        isUpdate = FALSE;
        isRemove = FALSE;
      //Set values when customer click on Edit 
        string strContactQuery = Apex_Utilcls.getAllFieldsSOQL('UBO_Data__c');
        strContactQuery = strContactQuery+' where Service_Request__c=\''+objSr.Id+'\'  and  Id =\''+TempObjUBOId+'\'';
        System.debug('strContactQuery===>'+strContactQuery);
      ObjUBO=database.query(strContactQuery);
       ObjUBO.Legal_Type_of_Entity__c=objSR.Legal_Structures__c;
      
      
    //  [select Place_of_Registration__c,Title__c ,Nationality__c,Reason_for_exemption__c,RecordType.Name,RecordType.ID,Passport_Number__c,Passport_Expiry_Date__c,Apartment_or_Villa_Number__c,BC_UBO__c,Building_Name__c,City_Town__c,Country__c,Date_Of_Becoming_a_UBO__c,
      //   Date_of_Birth__c,Date_Of_Registration__c,Email__c,Emirate_State_Province__c,Entity_Name__c,Last_Name__c,First_Name__c,Entity_Type__c from UBO_Data__c where Service_Request__c=:objSr.Id and Id=:TempObjUBOId];
          
            
  }
  
    public PageReference removeUBOFromList() 
    {
        /*
      system.debug('strAmndId==>'+UboId); 
      system.debug('strAmndId==>'+RemovedDate); 
      if(UboId!=null)
      {
       integer index = 0;
        for(UBO_Data__c itr:ListofUBOs){
            if(itr.Id == UboId){
                ListofUBOs.remove(index);
                break;
            }
             index ++ ;
        }
        delete new UBO_Data__c(Id = UboId);
        getListofAll();
        }
        */
        isUpdate = FALSE;
        isRemove = FALSE;
         try
     {
        UBO_Data__c ObjUBO_Data =new UBO_Data__c(Id = UboId);
        ObjUBO_Data.Status__c='Remove';
        ObjUBO_Data.Date_of_Cessation__c=RemovedDate;
        update ObjUBO_Data;
        getListofAll();
         //AllowtoAdd=true;
     } catch(DmlException e)
     {
         
     }
     
        return null;
    }
    
    public PageReference removeUBO() 
    {
        isUpdate = FALSE;
        isRemove = FALSE;
        integer index = 0;
        for(UBO_Data__c itr:ListofUBOs){
            if(itr.Id == UboId){
                ListofUBOs.remove(index);
                break;
            }
             index ++ ;
        }
        delete new UBO_Data__c(Id = UboId);
        getListofAll();
        return null;
    }
    
    
    // Below Logic added by Sai for TR Check 
     public void nationalityCheck(){
        
        isSanctionedNationality = FALSE;
        TR_Sanctioned_Nationality__c TRNationalty;
        if(ObjUBO.Nationality__c !=null){
            TRNationalty = TR_Sanctioned_Nationality__c.getValues(ObjUBO.Nationality__c);
        }
      
        if(TRNationalty !=null &&  String.isNotBlank(TRNationalty.Name)){
            isSanctionedNationality = true;
        }
        //return isSanctionedNationality;
    }
}