@isTest
public class CC_Create_LoopTaskTest {
    public static testmethod void test1(){
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        //insertNewAccounts[0].Next_Renewal_Date__c = Date.today();
        insert insertNewAccounts;
        
        License__c lic = new License__c(name='12345',Account__c=insertNewAccounts[0].id, License_Expiry_Date__c = Date.today());
        insert lic;
        
        insertNewAccounts[0].Active_License__c = lic.id;
        update insertNewAccounts;
        
        
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        createSRTemplateList[0].ownerid = userinfo.getUserId();
        insert createSRTemplateList;
        
        system.debug('============='+ createSRTemplateList[0].ownerid);
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].Passport_No__c = 'A12345';
        insertNewSRs[0].Nationality__c = 'India';
        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
        insertNewSRs[0].Gender__c = 'Male';
        insertNewSRs[0].Place_of_Birth__c = 'Dubai';
        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
        insertNewSRs[0].Last_Name__c = 'test';
        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Business_Sector__c = 'Hotel';
        insertNewSRs[0].entity_name__c = 'Tentity';
        insertNewSRs[1].License_Application__c = lic.id;
       // insertNewSRs[0].Passport_Expiry_Date__c = Date.today().addmonths(20);
       // insertNewSRs[0].Place_of_Issue__c = 'Dubai';
        insert insertNewSRs;
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'REGISTRAR_REVIEW'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
        listSRSteps[0].HexaBPM__Step_No__c = 1;
        listSRSteps[0].ownerId = userinfo.getUserId();
        //listSRSteps[0].HexaBPM__Step_Template_Code__c = 'CLO_REVIEW';
         listSRSteps[0].HexaBPM__Estimated_Hours__c = 10;
        insert listSRSteps;
        
       
        
        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Awaiting Verification',HexaBPM__Code__c='AWAITING_VERIFICATION',HexaBPM__Type__c='Start');
        
        HexaBPM__Status__c status1 = new HexaBPM__Status__c
        (Name='Re-Upload Document',HexaBPM__Code__c='REUPLOAD_DOCUMENT',HexaBPM__Type__c='End');
        insert status1;
        
         List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        insertNewSteps[0].HexaBPM__Status__c = status.id;
       // insertNewSteps[0].Step_Template_Code__c = 'CLO_REVIEW';
        insertNewSteps[0].Relationship_Manager__c = userinfo.getUserId();
        insertNewSteps[1].Relationship_Manager__c = userinfo.getUserId();
        insertNewSteps[1].HexaBPM__Sys_Step_Loop_No__c ='A_5';
        insert insertNewSteps;
        
        HexaBPM__Transition__c transition = new HexaBPM__Transition__c(HexaBPM__From__c=status.id,HexaBPM__To__c=status1.id);
        insert transition;
        
        HexaBPM__SR_Status__c srStatus = new HexaBPM__SR_Status__c(Name='Awaiting Re-Upload',HexaBPM__Code__c='AWAITING_RE-UPLOAD');
        insert srStatus;
        
        HexaBPM__Step_Transition__c stepTransition = new HexaBPM__Step_Transition__c
        (HexaBPM__SR_Step__c=listSRSteps[0].id,HexaBPM__Transition__c=transition.id,HexaBPM__SR_Status_Internal__c=listSRStatus[1].id,HexaBPM__SR_Status_External__c=listSRStatus[1].id);
        insert stepTransition;
        
        Company_Name__c companyName = new Company_Name__c(Application__c = insertNewSRs[1].id,Status__c='Approved' , Entity_Name__c = insertNewAccounts[0].id);
        insert companyName;
        
         // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'GENERAL'});
        listDocMaster[0].HexaBPM__Code__c='NOC_SHARING';
        insert listDocMaster;
        
        Group testGroup = new Group(Name='Client', Type='Queue');
		insert testGroup;
        
        
        
        Test.startTest();
        	CC_Create_LoopTask CC_Create_LoopTaskObj = new CC_Create_LoopTask();
        	CC_Create_LoopTaskObj.GenerateLoopTask(insertNewSteps , new map < string, Id >(), 
                                                   new map < Id, string >(), new map < Id, string >(), new map < Id, string >(),
                                                   new map < Id, Id >(), new map < Id, string >(), new map <string,Id>(), listSRSteps, 
                                                   new HexaBPM__Action__c(HexaBPM__Field_Value__c = '1') , insertNewSteps[0]); 
        	
        	//List<Group> queues = [SELECT id FROM Group where TYPE = 'Queue' and Name = 'Client'];
        	Id p = [select id from profile where name='DIFC Customer Community User Custom'].id;      
            Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
            insert con; 
        
        	User u = [SELECT id from user where id =: userinfo.getUserId()];
            system.runAs(u){
                List<QueueSobject> queues = new List<QueueSobject>();
                QueueSobject mappingObject = new QueueSobject(QueueId = testGroup.Id, SobjectType = 'HexaBPM__SR_Steps__c');
                QueueSobject mappingObject1 = new QueueSobject(QueueId = testGroup.Id, SobjectType = 'HexaBPM__Step__c');
                QueueSobject mappingObject2 = new QueueSobject(QueueId = testGroup.Id, SobjectType = 'HexaBPM__SR_Template__c');
                queues.add(mappingObject);
                queues.add(mappingObject1) ;  
                queues.add(mappingObject2) ;
        		insert queues;
            }
        
        	listSRSteps[0].ownerId = testGroup.id;
       		//listSRSteps[0].HexaBPM__Do_not_use_owner__c = true;
        	update listSRSteps;
                      
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
        	
            system.runAs(user){
                CC_Create_LoopTaskObj.GenerateLoopTask(insertNewSteps , new map < string, Id >{'Client' => testGroup.id}, 
                                                       new map < Id, string >(), new map < Id, string >{testGroup.id => 'Client'}, new map < Id, string >(),
                                                   new map < Id, Id >(), new map < Id, string >(), new map <string,Id>(), listSRSteps, 
                                                   new HexaBPM__Action__c(HexaBPM__Field_Value__c = '1') , insertNewSteps[0]);              
            }
        	
            system.runAs(u){
                 createSRTemplateList[0].HexaBPM__Do_not_use_owner__c = false;
                createSRTemplateList[0].ownerid = userinfo.getUserId();
                update createSRTemplateList;
                listSRSteps[0].HexaBPM__SR_Template__c = createSRTemplateList[0].id;
                listSRSteps[0].HexaBPM__Do_not_use_owner__c = true;
                update listSRSteps;
                List<HexaBPM__SR_Steps__c> listSRSteps1 = [select id,HexaBPM__SR_Template__c,HexaBPM__SR_Template__r.HexaBPM__Do_not_use_owner__c,HexaBPM__SR_Template__r.OwnerId,
                                                           HexaBPM__Step_No__c,HexaBPM__Step_Template__c,HexaBPM__Start_Status__c,HexaBPM__Step_RecordType_API_Name__c,
                                                           HexaBPM__Step_Template__r.HexaBPM__Step_RecordType_API_Name__c,HexaBPM__Summary__c,HexaBPM__Estimated_Hours__c,
                                                           HexaBPM__Do_not_use_owner__c from HexaBPM__SR_Steps__c];
                CC_Create_LoopTaskObj.GenerateLoopTask(insertNewSteps , new map < string, Id >{'Client' => testGroup.id}, 
                                                       new map < Id, string >(), new map < Id, string >{testGroup.id => 'Client'}, new map < Id, string >(),
                                                   new map < Id, Id >(), new map < Id, string >(), new map <string,Id>(), listSRSteps1, 
                                                   new HexaBPM__Action__c(HexaBPM__Field_Value__c = '1.0') , insertNewSteps[0]);
                
                createSRTemplateList[0].ownerid = testGroup.id;
                update createSRTemplateList;
                List<HexaBPM__SR_Steps__c> listSRSteps2 = [select id,HexaBPM__SR_Template__c,HexaBPM__SR_Template__r.HexaBPM__Do_not_use_owner__c,HexaBPM__SR_Template__r.OwnerId,
                                                           HexaBPM__Step_No__c,HexaBPM__Step_Template__c,HexaBPM__Start_Status__c,HexaBPM__Step_RecordType_API_Name__c,
                                                           HexaBPM__Step_Template__r.HexaBPM__Step_RecordType_API_Name__c,HexaBPM__Summary__c,HexaBPM__Estimated_Hours__c,ownerid,
                                                           HexaBPM__Do_not_use_owner__c from HexaBPM__SR_Steps__c];
                CC_Create_LoopTaskObj.GenerateLoopTask(insertNewSteps , new map < string, Id >{'Client' => testGroup.id}, 
                                                       new map < Id, string >(), new map < Id, string >{testGroup.id => 'Client'}, new map < Id, string >{testGroup.id => 'Client'},
                                                   new map < Id, Id >(), new map < Id, string >(), new map <string,Id>(), listSRSteps2, 
                                                   new HexaBPM__Action__c(HexaBPM__Field_Value__c = '1.0') , insertNewSteps[0]);
                
                
                createSRTemplateList[0].HexaBPM__Do_not_use_owner__c = true;
                update createSRTemplateList;
                listStepTemplate[0].ownerid = userinfo.getUserId();
                update listStepTemplate;
                listSRSteps[0].HexaBPM__Step_Template__c = listStepTemplate[0].id;
                update listSRSteps;
                
              List<HexaBPM__SR_Steps__c> listSRSteps3 = [select id,HexaBPM__SR_Template__c,HexaBPM__SR_Template__r.HexaBPM__Do_not_use_owner__c,HexaBPM__SR_Template__r.OwnerId,
                                                           HexaBPM__Step_No__c,HexaBPM__Step_Template__c,HexaBPM__Start_Status__c,HexaBPM__Step_RecordType_API_Name__c,
                                                           HexaBPM__Step_Template__r.HexaBPM__Step_RecordType_API_Name__c,HexaBPM__Summary__c,HexaBPM__Estimated_Hours__c,ownerid,
                                                           HexaBPM__Step_Template__r.OwnerId,
                                                           HexaBPM__Do_not_use_owner__c 
                                                      from HexaBPM__SR_Steps__c];
                 CC_Create_LoopTaskObj.GenerateLoopTask(insertNewSteps , new map < string, Id >{'Client' => testGroup.id}, 
                                                       new map < Id, string >(), new map < Id, string >{testGroup.id => 'Client'}, new map < Id, string >{testGroup.id => 'Client'},
                                                   new map < Id, Id >(), new map < Id, string >(), new map <string,Id>(), listSRSteps3, 
                                                   new HexaBPM__Action__c(HexaBPM__Field_Value__c = '1.0') , insertNewSteps[0]); 
            }
        Test.stopTest();

    }

	 public static testmethod void test2()
     {
       
         
        CC_Create_LoopTask CC_Create_LoopTaskObj = new CC_Create_LoopTask(); 

        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        //insertNewAccounts[0].Next_Renewal_Date__c = Date.today();
        insert insertNewAccounts;
        
         /*
        License__c lic = new License__c(name='12345',Account__c=insertNewAccounts[0].id, License_Expiry_Date__c = Date.today());
        insert lic;
        
        insertNewAccounts[0].Active_License__c = lic.id;
        update insertNewAccounts;
        */
        
         
        Group testGroup = new Group(Name='Client', Type='Queue');
		insert testGroup;
        //QueuesObject StepOwnerQueue = new QueueSObject(QueueID = testGroup.id, SobjectType = 'HexaBPM__Step_Template__c');
        //insert StepOwnerQueue; 
        /* 
            //create lead
            List<Lead> insertNewLeads = new List<Lead>();
            insertNewLeads = OB_TestDataFactory.createLead(2);
            insert insertNewLeads;
            //create License Activity Master
            List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
            listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
            insert listLicenseActivityMaster;
            
            //create lead activity
            List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
            listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
            insert listLeadActivity;
		*/
         
        
        
       
        Test.startTest();
        	
        	
        	//List<Group> queues = [SELECT id FROM Group where TYPE = 'Queue' and Name = 'Client'];
        	Id p = [select id from profile where name='DIFC Customer Community User Custom'].id;      
            Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
            insert con; 
        
        	User u = [SELECT id from user where id =: userinfo.getUserId()];
            system.runAs(u)
            {
                List<QueueSobject> queues = new List<QueueSobject>();
                QueueSobject mappingObject = new QueueSobject(QueueId = testGroup.Id, SobjectType = 'HexaBPM__SR_Steps__c');
                QueueSobject mappingObject1 = new QueueSobject(QueueId = testGroup.Id, SobjectType = 'HexaBPM__Step__c');
                QueueSobject mappingObject2 = new QueueSobject(QueueId = testGroup.Id, SobjectType = 'HexaBPM__SR_Template__c');
                QueueSobject mappingObject3 = new QueueSobject(QueueId = testGroup.Id, SobjectType = 'HexaBPM__Step_Template__c');
                    
                queues.add(mappingObject);
                queues.add(mappingObject1) ;  
                queues.add(mappingObject2) ;
                queues.add(mappingObject3) ;
        		insert queues;
                
                
                //create createSRTemplate
                List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
                createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
                //createSRTemplateList[0].ownerid = userinfo.getUserId();
                insert createSRTemplateList;
                
               
        
                //create SR status
                list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
                listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});
                insert listSRStatus;
                //create SR
                List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
                insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts, 
                                                           new List<string>{'Non - financial','Retail'}, 
                                                           new List<string>{'Foundation','Services'}, 
                                                           new List<string>{'Foundation','Company'}, 
                                                           new List<string>{'Foundation','Recognized Company'});
                //insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
                //insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
                insertNewSRs[1].Setting_Up__c='Branch';
                insertNewSRs[0].Setting_Up__c='Branch';
                //insertNewSRs[1].Page_Tracker__c = listPage[0].id;
                //insertNewSRs[0].Page_Tracker__c = listPage[0].id;
                insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
                insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
                insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
                insertNewSRs[0].Passport_No__c = 'A12345';
                insertNewSRs[0].Nationality__c = 'India';
                insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
                insertNewSRs[0].Gender__c = 'Male';
                insertNewSRs[0].Place_of_Birth__c = 'Dubai';
                insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
                insertNewSRs[0].Last_Name__c = 'test';
                insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
                insertNewSRs[0].Entity_Type__c = 'Retail';
                insertNewSRs[0].Business_Sector__c = 'Hotel';
                insertNewSRs[0].entity_name__c = 'Tentity';
                //insertNewSRs[1].License_Application__c = lic.id;
                insert insertNewSRs;
                
                 //create step template
                List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
                listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'REGISTRAR_REVIEW'}
                                                                         , new List<String> {'General'},  new List<String> {'RM_Assignment'});
                 
                for(HexaBPM__Step_Template__c stpTemp : listStepTemplate)
                    {
                        stpTemp.OwnerId = testGroup.Id;
                    }
                  insert listStepTemplate;
                
                	//create SR Step
                 List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
                 listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
                 listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
                 listSRSteps[0].HexaBPM__Step_No__c = 1;
                 listSRSteps[0].ownerId = testGroup.id;
                  listSRSteps[0].HexaBPM__Estimated_Hours__c = 10;
                 listSRSteps[0].HexaBPM__Step_Template__c = listStepTemplate[0].Id;
                 
                 //listSRSteps[0].HexaBPM__Step_Template_Code__c = 'CLO_REVIEW';
                 insert listSRSteps;
                 
                 
                 
                 HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Awaiting Verification',HexaBPM__Code__c='AWAITING_VERIFICATION',HexaBPM__Type__c='Start');
                 HexaBPM__Status__c status1 = new HexaBPM__Status__c
                     (Name='Re-Upload Document',HexaBPM__Code__c='REUPLOAD_DOCUMENT',HexaBPM__Type__c='End');
                 insert status1;
                 
                 List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
                 insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
                 insertNewSteps[0].HexaBPM__Status__c = status.id;
                 // insertNewSteps[0].Step_Template_Code__c = 'CLO_REVIEW';
                 insertNewSteps[0].Relationship_Manager__c = userinfo.getUserId();
                 insertNewSteps[1].Relationship_Manager__c = userinfo.getUserId();
                 insertNewSteps[1].HexaBPM__Sys_Step_Loop_No__c ='A_5';
                 insert insertNewSteps;
                    
            
               
                
                
            
                createSRTemplateList[0].HexaBPM__Do_not_use_owner__c = false;
                createSRTemplateList[0].ownerid = testGroup.id;
                update createSRTemplateList;
                
                listSRSteps[0].HexaBPM__SR_Template__c = createSRTemplateList[0].id;
                listSRSteps[0].HexaBPM__Do_not_use_owner__c = true;
                update listSRSteps;
                
                List<HexaBPM__SR_Steps__c> listSRSteps1 = [select id,HexaBPM__SR_Template__c,HexaBPM__SR_Template__r.HexaBPM__Do_not_use_owner__c,HexaBPM__SR_Template__r.OwnerId,
                                                               HexaBPM__Step_No__c,HexaBPM__Step_Template__c,HexaBPM__Start_Status__c,HexaBPM__Step_RecordType_API_Name__c,
                                                               HexaBPM__Step_Template__r.HexaBPM__Step_RecordType_API_Name__c,HexaBPM__Summary__c,HexaBPM__Estimated_Hours__c,
                                                               HexaBPM__Do_not_use_owner__c,OwnerId 
                                                           from HexaBPM__SR_Steps__c];
                
                CC_Create_LoopTaskObj.GenerateLoopTask(insertNewSteps , new map < string, Id >{'Client' => testGroup.id}, 
                                                       new map < Id, string >(), new map < Id, string >{testGroup.id => 'Client'}, new map < Id, string >(),
                                                   new map < Id, Id >(), new map < Id, string >(), new map <string,Id>(), listSRSteps1, 
                                                   new HexaBPM__Action__c(HexaBPM__Field_Value__c = '1.0') , insertNewSteps[0]);
                
               
            }
        Test.stopTest();

    }
}