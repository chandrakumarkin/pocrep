/******************************************************************************************************
*  Author   : Kumar Utkarsh
*  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* This class contains the logics to resize the SR doc Attchment associated with the Service Requests.
*******************************************************************************************************/
public class WebServiceGdrfaDocumentCompressClass {
    
    @InvocableMethod 
    public static void compressSrAttachment(List<Id> SrIds){
        System.debug('List<Id> SrIds:'+SrIds);
        for(Id SrId :SrIds){
            GDRFAImageCompressorCallout(String.valueOf(SrId));
        }
    }
    
    @future(callout=true)
    public static void GDRFAImageCompressorCallout(String srIds){
        system.debug('srIds:-'+srIds);
        List<Attachment> listAttachment = new List<Attachment>();
        map<String, SR_Doc__c> mapAttchIdSrDoc = new map<String, SR_Doc__c>();
        for(SR_Doc__c srDoc : [SELECT Id, Service_Request__c, GDRFA_Parent_Document_Id__c, Doc_ID__c FROM SR_Doc__c WHERE Service_Request__c =: srIds AND GDRFA_Parent_Document_Id__c != null AND Doc_ID__c != null])
        {
            mapAttchIdSrDoc.put(srDoc.Doc_ID__c, srDoc);
        }
        
        integer filesize = 150000;
        integer imageSize = 10;
        if(Test.isRunningTest()){
            filesize = 0;
            imageSize = 0;
        }           
        
        for(Attachment ObjAttached: [SELECT Id, Name, ContentType, ParentId, Body, BodyLength FROM Attachment WHERE Id IN:mapAttchIdSrDoc.keySet()  AND (Not Name like '%.pdf%') AND BodyLength  >= :filesize]){
            HttpRequest req = new HttpRequest();
            req.setEndpoint(System.label.GDRFA_Mobile_Access_Endpoint);
            req.setMethod('POST');
            req.setHeader('Username', System.label.GDRFA_Mobile_Access_Username);
            req.setHeader('Password', System.label.GDRFA_Mobile_Access_Password);
            req.setHeader('Content-Type', 'application/json');
            System.debug('ParentId: '+ObjAttached.ParentId+' {"imagebytes":"'+EncodingUtil.base64Encode(ObjAttached.Body)+'"}');
            req.setBody('{"imagebytes":"'+EncodingUtil.base64Encode(ObjAttached.Body)+'"}'); 
            req.setTimeout(120000);
            Http http = new Http();
            HTTPResponse res = http.send(req);
            map<String, String> values = (map<String, String>) JSON.deserialize(res.getBody(), map<String, String>.class);
            System.debug(values.get('imagebytes'));  
            if(values.get('imagebytes') != 'Error' && EncodingUtil.base64Decode(values.get('imagebytes')).size() >= imageSize){
                Attachment ObjNewAttached = new Attachment();
                if(ObjAttached.ParentId != null){
                ObjNewAttached.ParentId = ObjAttached.ParentId;
                    }
                ObjNewAttached.Name = 'GDRFA__RESIZE150_DIFC_WB_API_Data_'+ObjAttached.Name;
                ObjNewAttached.Body = EncodingUtil.base64Decode(values.get('imagebytes'));
                ObjNewAttached.ContentType = 'image/jpeg';
                listAttachment.add(ObjNewAttached);
            }else{
                //Do-nothing
            }
        }
        if(listAttachment.size() > 0){
            insert listAttachment;
        }
    }
}