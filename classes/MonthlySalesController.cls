/**
 * @description       : 
 * @author            : Mudasir
 * @group             : 
 * @last modified on  : 11-14-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author    Modification
 * 1.0   08-26-2020   Mudasir   Initial Version
 * Review comments - 1 - Allow_All_Year_Submissions
 * 					 2 - External_SR_Status__c harcoded value should be moved to Label 
 * 					 3 - Create a log for the webservices failure
**/
public with sharing class MonthlySalesController {
    public Integer currentYearValue = 0;
    public static Set<String> submittedMonthsOfCurrentYear;
    public static Map<string,Lease__c> leaseMap;
    @AuraEnabled 
	public static List<Service_request__c> getSalesData(String customerIdVal, integer selectedYearValue, String leaseId)
    {  
        system.debug('leaseId---'+leaseId+'---customerIdVal---'+customerIdVal+'---selectedYearValue---'+selectedYearValue);
        List<Service_request__c> salesList
                    =[Select id,Year__c,Lease__c,Lease__r.Name,Transaction_Amount__c,name,Client_Name__c, Submitted_Date__c,Month__c,
                        Description__c,External_Status_name__c,External_SR_Status__c,External_SR_Status__r.Name,Internal_SR_Status__c from service_request__c  
                        where recordType.DeveloperName = 'Monthly_Sales' AND 
                        CALENDAR_YEAR(CreatedDate) =: selectedYearValue AND 
                        Lease__c =: LeaseId AND Lease__c != NULL
                        order by Dedicated_Cable_Quantity__c DESC 
                    ];
        system.debug('MonthlySalesController---'+salesList);
        submittedMonthsOfCurrentYear = new Set<String>();
        for(Service_request__c servRec : salesList){
            submittedMonthsOfCurrentYear.add(servRec.Month__c);
        }
        return salesList;
    }

    @AuraEnabled //Prepare Year select option 
    public static List<String> getMonthOptions(Integer selectedYearValue , String allowAllYearSubmission){
        List<String> options = new List<String>();
        Date currentDate = Date.Today();
        Integer currentMonth =  currentDate.Month(); 
        Integer currentYear =  currentDate.Year();
        Map<Integer , String> monthsOfTheYear = getCalanderMonths();  
        if(selectedYearValue == currentYear){
            if(system.label.Allow_All_Year_Submissions == 'true' || allowAllYearSubmission=='Allow_All_Year_Submissions')
            {
                for ( integer monthValue = 0; monthValue < currentMonth-1 ;monthValue++) {
                    //Put Picklist Value & Label in Map
                    system.debug('monthValue---'+monthValue+'---currentMonth---'+currentMonth
                                    +'---Month Value---'+monthsOfTheYear.get(monthValue)+
                                    '---'+submittedMonthsOfCurrentYear);
                    if(submittedMonthsOfCurrentYear.size() == 0 || !submittedMonthsOfCurrentYear.contains(monthsOfTheYear.get(monthValue)))
                    {
                        options.add(monthsOfTheYear.get(monthValue));
                    }
                }
            }else{
                if(!submittedMonthsOfCurrentYear.contains(monthsOfTheYear.get(currentMonth-1)))
                {
                    options.add(monthsOfTheYear.get(currentMonth-1));
                }
            }
        } 
        system.debug('getMonthOptions------'+options+'---selectedYearValue---'+selectedYearValue);
        return options;
    }

    @AuraEnabled 
    public static SalesWrapperClass getMonthlySalesWrapperData(String currentLoggedInUser , Integer selectedYearValue , String leaseId,Map<String,Lease__c> customerLeaseMapVal){
        system.debug('Current Logged in User is ---'+currentLoggedInUser+'---selectedYearValue---'+selectedYearValue+'---leaseId---'+leaseId);
        User userRec = getUserAccountDetails(currentLoggedInUser);
        salesWrapperClass servFeedWrapper = new salesWrapperClass();
        servFeedWrapper.yearOptions= getYearOptions();
        servFeedWrapper.serviceRequests = getSalesData(currentLoggedInUser,selectedYearValue,leaseId);
        servFeedWrapper.calanderMonths= getCalanderMonths();
        servFeedWrapper.customerLeaseMap = customerLeaseMapVal;
        servFeedWrapper.customerLeases = getCustomerLeases(getUserAccountId(currentLoggedInUser)); 
        servFeedWrapper.unsubmittedMonths= getMonthOptions(selectedYearValue , userRec.Contact.Account.Temp__c);
        Id recordIdValue = Schema.SObjectType.Service_request__c.getRecordTypeInfosByDeveloperName().get('Monthly_Sales').getRecordTypeId();
        servFeedWrapper.createNewServiceRequest = new Service_Request__c(RecordTypeId =recordIdValue );
        return servFeedWrapper;
    }

    @AuraEnabled 
    public static SalesWrapperClass initiateMonthlySalesView(String currentLoggedInUser ,Integer selectedYearValue){
        system.debug('Current Logged in User is ---'+currentLoggedInUser);
        Id customerId = getUserAccountId(currentLoggedInUser);
        User userRec = getUserAccountDetails(currentLoggedInUser);
        salesWrapperClass servFeedWrapper = new salesWrapperClass();
        servFeedWrapper.customerLeases= getCustomerLeases(customerId); 
        servFeedWrapper.customerLeaseMap = leaseMap;
        //getMonthlySalesWrapperData(currentLoggedInUser , selectedYearValue , servFeedWrapper.customerLeases[0].id);      
        servFeedWrapper.yearOptions= getYearOptions();
        servFeedWrapper.leaseNumberId = servFeedWrapper.customerLeases[0].Name;
        servFeedWrapper.serviceRequests = getSalesData(currentLoggedInUser,selectedYearValue,servFeedWrapper.customerLeases[0].id);
        servFeedWrapper.calanderMonths= getCalanderMonths();
        servFeedWrapper.unsubmittedMonths= getMonthOptions(selectedYearValue , userRec.Contact.Account.Temp__c);
        Id recordIdValue = Schema.SObjectType.Service_request__c.getRecordTypeInfosByDeveloperName().get('Monthly_Sales').getRecordTypeId();
        servFeedWrapper.createNewServiceRequest = new Service_Request__c(RecordTypeId =recordIdValue );
        servFeedWrapper.selectedYear = selectedYearValue;
        return servFeedWrapper;
    }

    @AuraEnabled 
    public static SalesWrapperClass getMonthlySalesViewForLease(String currentLoggedInUser ,Integer selectedYearValue, String leaseId,Map<String,Lease__c> customerLeaseMapVal){
        system.debug('leaseMap---'+leaseMap+'---Current Logged in User is ---'+currentLoggedInUser+'---leaseId---'+leaseId);
        Id customerId = getUserAccountId(currentLoggedInUser);
        User userRec = getUserAccountDetails(currentLoggedInUser);
        salesWrapperClass servFeedWrapper = new salesWrapperClass();
        servFeedWrapper.customerLeases= getCustomerLeases(customerId); 
        servFeedWrapper.customerleaseMap = customerLeaseMapVal;
        //getMonthlySalesWrapperData(currentLoggedInUser , selectedYearValue , servFeedWrapper.customerLeases[0].id);      
        servFeedWrapper.yearOptions= getYearOptions();
        servFeedWrapper.leaseNumberId = leaseMap.get(leaseId).Name;
        servFeedWrapper.serviceRequests = getSalesData(currentLoggedInUser,selectedYearValue,customerLeaseMapVal.get(leaseId).id);
        servFeedWrapper.calanderMonths= getCalanderMonths();
        servFeedWrapper.unsubmittedMonths= getMonthOptions(selectedYearValue , userRec.Contact.Account.Temp__c);
        Id recordIdValue = Schema.SObjectType.Service_request__c.getRecordTypeInfosByDeveloperName().get('Monthly_Sales').getRecordTypeId();
        servFeedWrapper.createNewServiceRequest = new Service_Request__c(RecordTypeId =recordIdValue );
        servFeedWrapper.selectedYear = selectedYearValue;
        return servFeedWrapper;
    }
    
    private static List<Lease__c> getCustomerLeases(ID customerId){
        List<Lease__c> customerLeases;
        try
        {
            leaseMap = new Map<string , Lease__c>();
            system.debug('customerId---'+customerId);
            customerLeases = [Select id,Name,Account__r.BP_No__c from Lease__c where Account__c=:customerId and Consider_For_Monthly_Sales_Submission__c=true];
            for(Lease__c leaseRec : customerLeases){
                leaseMap.put(leaseRec.Name , leaseRec);
            }
            system.debug('leaseMap---'+leaseMap);
        }catch(Exception e){
            system.debug(e.getMessage());
        }
        return customerLeases;
    }
    @AuraEnabled 
    public static SalesWrapperClass createNewMonthlySalesSR(
                    Service_Request__c serviceReqRecord, String fileName, String base64Data , 
                    String contentType,Integer selectedYearValue,String currentLoggedInUser,String selectedMonth, String leaseId , Map<string,Lease__c> customerLeaseMapVal)
    {
        system.debug('Current Logged in User is ---'+currentLoggedInUser+'---selectedYearValue---'+selectedYearValue+'---leaseId---'+leaseId+'---customerLeaseMapVal---'+customerLeaseMapVal);
        system.debug('serviceReqRecord is ---'+serviceReqRecord+'---base64Data---'+base64Data+'---contentType---'+contentType);
        serviceReqRecord = insertNewServiceRequest(serviceReqRecord,currentLoggedInUser,selectedYearValue,selectedMonth,customerLeaseMapVal.get(leaseId).id,customerLeaseMapVal.get(leaseId).Name,customerLeaseMapVal.get(leaseId).Account__R.BP_No__c);
        Attachment attach = EmployeeComponentController.SaveFile(serviceReqRecord.id,fileName,base64Data,contentType);
        insert attach;
        system.debug('attach------------'+attach);
        salesWrapperClass servFeedWrapper = getMonthlySalesWrapperData(currentLoggedInUser,selectedYearValue,leaseMap.get(leaseId).id,customerLeaseMapVal);
        //salesWrapperClass servFeedWrapper = getMonthlySalesViewForLease(currentLoggedInUser,selectedYearValue,leaseMap.get(leaseId).id,customerLeaseMapVal);
        return servFeedWrapper;
    }

    private static Service_request__c insertNewServiceRequest (Service_Request__c servRequest, Id loggedInUserId , Integer selectedYear , String selectedMonth, String leaseId,String leaseNumber,String customerBPNumber){
        system.debug('servRequest--insertNewServiceRequest---'+servRequest+'---loggedInUserId---'+loggedInUserId+'---selectedYear---'+selectedYear+'---selectedMonth---'+selectedMonth);
        //Get the account related to the loggeding user 
        ID AccountID = null;
        try
        {
            //Id loggedInUserId = UserInfo.getUserId();
            //system.debug('loggedInUserId');
            AccountID = [Select ContactId,AccountId__c from User where Id=:loggedInUserId].AccountId__c;
            //AccountID =  [Select accountId from Contact where id=:ContactIdVal].accountId;
            system.debug('AccountID----Inside---'+AccountID);
            getCustomerLeases(AccountID);
            servRequest.customer__c = AccountID;
        }catch(Exception e)
        {
            system.debug(e.getMessage());
        }
        system.debug('AccountID----outside---'+AccountID);
        system.debug('leaseId----outside---'+leaseId);
        servRequest.year__c = String.valueof(selectedYear);
        servRequest.month__c = selectedMonth;
        servRequest.Lease__c  = leaseId;
        servRequest.Submitted_Date__c  = system.today();
        servRequest.Dedicated_Cable_Quantity__c = Decimal.valueOf(getCalanderMonthValue(selectedMonth));
        servRequest.External_SR_Status__c  = System.Label.Approved_SR_Status_Id;
        servRequest.Internal_SR_Status__c  = System.Label.Approved_SR_Status_Id;
        system.debug('servRequest----'+servRequest);
        insert servRequest;
        if(!Test.isRunningTest())
        {
            try{
                webserviceCallForMonthlySalesNew(new List<id>{servRequest.id});
            }Catch(Exception ex){
                system.debug('Exception is '+ex.getMessage()+ ' at >>' + ex.getLineNumber());            
                Log__c objLog = new Log__c();
                objLog.Type__c = 'Monthly Sales Submission';
                objLog.Description__c = ex.getMessage();                
                insert objLog;  
            }
        }
        system.debug('servRequest----------'+servRequest);
        return servRequest;
    }
    @AuraEnabled //Prepare calander Months - Move this to the common utility
    public static Map<Integer , String> getCalanderMonths(){
        Map<Integer , String> calanderMonths = new Map<Integer , String>();
        calanderMonths.put(0,'JANUARY');
        calanderMonths.put(1,'FEBRUARY');
        calanderMonths.put(2,'MARCH');
        calanderMonths.put(3,'APRIL');
        calanderMonths.put(4,'MAY');
        calanderMonths.put(5,'JUNE');
        calanderMonths.put(6,'JULY');
        calanderMonths.put(7,'AUGUST');
        calanderMonths.put(8,'SEPTEMBER');
        calanderMonths.put(9,'OCTOBER');
        calanderMonths.put(10,'NOVEMBER');
        calanderMonths.put(11,'DECEMBER');
        return calanderMonths;
    }
    //Prepare calander Months - Move this to the common utility
    public static string getCalanderMonthValue(String monthName){
        string monthValue ='';
        switch on monthName {
            when 'JANUARY' {monthValue = '1';}
            when 'FEBRUARY' {monthValue = '2';}
            when 'MARCH' {monthValue = '3';}
            when 'APRIL' {monthValue = '4';}
            when 'MAY' {monthValue = '5';}
            when 'JUNE' {monthValue = '6';}
            when 'JULY' {monthValue = '7';}
            when 'AUGUST' {monthValue = '8';}
            when 'SEPTEMBER' {monthValue = '9';}
            when 'OCTOBER' {monthValue = '10';} 
            when 'NOVEMBER' {monthValue = '11';}
            when 'DECEMBER' {monthValue = '12';} 
            when else {		
                // code block 4
            }
        }
        return monthValue;
    }
    @AuraEnabled //Prepare Year select option 
    public static List<Integer> getYearOptions(){
        List<Integer> options = new List<Integer>();
        Date currentDate = Date.Today();
        Integer currentYear =  currentDate.Year();      
        for (Integer startedYear = 2020 ; currentYear >= startedYear ; currentYear--) {
            options.add(currentYear);
        }
        //options.sort();
        return options;
    }
    private static ID getUserAccountId(Id userId){
        ID AccountID = null;
        try
        {
            AccountID = [Select ContactId,AccountId__c,Contact.Account.temp__c from User where Id=:userId].AccountId__c;
        }catch(Exception e)
        {
            system.debug(e.getMessage());
        }
        return AccountID;
    }
    private static User getUserAccountDetails(Id userId){
        User userAccDetails;
        try
        {
            userAccDetails = [Select ContactId,AccountId__c,Contact.Account.temp__c from User where Id=:userId];
        }catch(Exception e)
        {
            system.debug(e.getMessage());
        }
        return userAccDetails;
    }
    public class SalesWrapperClass
    {
        @AuraEnabled 
        public list<Service_Request__c> serviceRequests{get;set;}
        @AuraEnabled 
        public list<Lease__c> customerLeases{get;set;}
        @AuraEnabled 
        public Map<Integer , String> calanderMonths{get;set;}
        @AuraEnabled 
        public List<Integer> yearOptions{get;set;}
        @AuraEnabled 
        public List<String> unsubmittedMonths{get;set;}
        @AuraEnabled 
        public Service_Request__c createNewServiceRequest{get;set;}
        @AuraEnabled 
        public String leaseNumberId{get;set;}
        @AuraEnabled 
        public Integer selectedYear{get;set;}
        @AuraEnabled 
        public Map<String,Lease__c> customerleaseMap{get;set;}
        
    }
    @future(callout=true)
    public static void webserviceCallForMonthlySalesNew(List<id> serviceRequestIds){        
        Service_Request__c servRequest = new Service_Request__c();
        if(!Test.isRunningTest())
        servRequest = [Select id,Month__c,name,customer__r.BP_No__c,Lease__r.Name,Year__c,Transaction_Amount__c from Service_Request__c where id in : serviceRequestIds];
        else 
        {
            servRequest = new Service_Request__c(Transaction_Amount__c=100,Month__c='2',Year__c='2020');
            insert servRequest;
        }
        
        string result = 'Success';
        FinanceContactPersonWebservice.ZSF_SRENT_ST item;
        list<FinanceContactPersonWebservice.ZSF_SRENT_ST> items = new list<FinanceContactPersonWebservice.ZSF_SRENT_ST>();
            
        item = new FinanceContactPersonWebservice.ZSF_SRENT_ST();
        item.RENUM = servRequest.name;
        item.SGUID = servRequest.Id; 
        item.PARTNER = servRequest.customer__r.BP_No__c; 
        item.RECNNR = servRequest.Lease__r.Name; 
        item.GJAHR = servRequest.Year__c;        
        item.MONAT = getCalanderMonthValue(servRequest.Month__c);
        item.SAMT = String.valueof(servRequest.Transaction_Amount__c); 
        items.add(item);

        if(items != null && !items.isEmpty()){
            FinanceContactPersonWebservice.ZSF_SRENT_TT  objActivityData = new FinanceContactPersonWebservice.ZSF_SRENT_TT();
            objActivityData.item = items;	
            FinanceContactPersonWebservice.ZRE_SALES_RENT  objSFData = new FinanceContactPersonWebservice.ZRE_SALES_RENT();       
            objSFData.timeout_x = 120000;
            objSFData.clientCertName_x = Test.isRunningTest() ? 'test' : WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
            objSFData.inputHttpHeaders_x= new Map<String, String>();
            string decryptedSAPPassWrd = test.isrunningTest()  ==   false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) : '123456';
            Blob headerValue = Test.isRunningTest() ? Blob.valueOf('test') : Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+decryptedSAPPassWrd); 
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);	
            system.debug('request $$$ : '+objActivityData);	
            if(!Test.isRunningTest())
            {
                FinanceContactPersonWebservice.ZSF_SRENT_TT_EX EX_LOG_TT = objSFData.ZRE_LEASE_SALES_RENT(objActivityData);	
                system.debug('response $$$ : '+EX_LOG_TT);
                result = JSON.serialize(EX_LOG_TT);
            }
        }
        servRequest.comments__c = result;
        if(!test.isRunningTest()) update servRequest;
    }
}