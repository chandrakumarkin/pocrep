@isTest
private class DFSAControllerTest {

    static testMethod void myUnitTest() {
        
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        List<Contact> lstContact = new List<Contact>();
        lstContact = OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert lstContact;
        system.debug('!!@###'+lstContact);
        Profile profile1 = [Select Id from Profile where name = 'DIFC Customer Community User Custom'];
        User portalAccountOwner1 = new User(ProfileId = profile1.Id,Username = System.now().millisecond() + 'test2@test.com',Alias = 'Test11',
                                            Email='TestUser@Difc.ae',EmailEncodingKey='UTF-8',Firstname='Test',Lastname='DIFC',LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',TimeZoneSidKey='America/Chicago',ContactId=lstContact[0].Id,isActive=true);
        
        INSERT portalAccountOwner1;
        
        DFSA_Security_Users__c sUsers = new DFSA_Security_Users__c();
        sUsers.Position_Title__c = 'CEO';
        sUsers.Designation__c = 'CTO';
        sUsers.E_mail_Address__c = 'test@test.com';
        sUsers.Individual_E_mail_Address__c = 'test1@test.com';
        sUsers.Individual_Signed_on_Registration_Form__c = 'Test';
        sUsers.Name_of_individual__c = 'Test 2';
        //INSERT sUsers;
        
        if(portalAccountOwner1 !=null){
            DFSAController.insertNewSecurityUsers(sUsers);
        }         
    }
}