@isTest
public class FitoutLWCController_Test {
    
    public static testmethod void updateStepStatusTestmethod(){
        
        Account objAccount = new Account(Name = 'Test Account',Phone = '1234567890');
        insert objAccount;
        
        Fitout_Milestone_Penalty__c milestone = new Fitout_Milestone_Penalty__c();
        milestone.Account__c = objAccount.Id;
        milestone.Unit_Handover_Date__c = system.today();
        milestone.Type_of_Retail__c= 'Standard Retail';
        milestone.Additional_Tenant_email__c = 'testmilestone@difc.test.com';
        milestone.Building__c = 'Test DIFC Office';
        milestone.Floor__c = '4';
        milestone.Unit__c = 'Test Unit';
        insert milestone;
        
        Step_Template__c objStepTemplate = new Step_Template__c(Name = 'Verify & Approve by FOSP',Code__c = 'VERIFY_APPROVE_FOSP',Step_RecordType_API_Name__c = 'Verify_Approve_FOSP',Summary__c = 'Verify & Approve by FOSP');
        insert objStepTemplate;
        
        List<Status__c> lstStatus = new List<Status__c>();
        lstStatus.add(new Status__c(Name = 'Pending Review', Type__c = 'Pending Review', Code__c = 'PENDING REVIEW'));
        lstStatus.add(new Status__c(Name = 'Verified', Type__c = 'Verified', Code__c = 'VERIFIED'));
        lstStatus.add(new Status__c(Name = 'Completed', Type__c = 'Completed', Code__c = 'COMPLETED'));
        insert lstStatus;
        
        step__c objStep = new Step__c(Step_Template__c = objStepTemplate.id,No_PR__c = false,status__c = lstStatus[0].id);
        insert objStep;
        
        Test.startTest();
        FitoutLWCController.updateStepStatus(objStep.Id, 'Verified');
        Test.stopTest();
    }
}