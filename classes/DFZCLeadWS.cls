@RestResource(urlMapping='/api/CreateLead/*')
global with sharing class DFZCLeadWS{

  
    
    @HttpPost
    global static DFZCCreateResponce doPost() 
    {
    
        RestRequest req = RestContext.request;
        DFZCCreateResponce ObjRes=new DFZCCreateResponce();
        System.debug('===============doPost=============================');
        System.debug('=====PushedLeads=>'+req.requestBody.toString());
        string postBody=req.requestBody.toString();
        List<DFZCLead> PushedLeads = (List<DFZCLead>)JSON.deserialize(postBody,List<DFZCLead>.class);
  
   
          List<Lead> ListLeads=new List<Lead>();
          for(DFZCLead TempLead:PushedLeads)
           {
               Lead ObjLead=new Lead();
               ObjLead.Milestone_ID__c=TempLead.leadId;
               ObjLead.FirstName=TempLead.firstName;
               ObjLead.LastName=TempLead.lastName;
               ObjLead.MobilePhone=TempLead.phoneNumber;
               ObjLead.Email=TempLead.email;
               ObjLead.LeadSource='DFZC Business Gateway';
               
               if(TempLead.areaOfInterest!=null)
               ObjLead.Sector_Classification__c=string.join(TempLead.areaOfInterest,',');
               
               ObjLead.SubZone__c=TempLead.subZoneName;
                //ObjLead.firstName= TempLead.submittedOn;
               ObjLead.gdprConsent__c=TempLead.gdprConsent;
               
               ObjLead.Place_of_Trip__c=TempLead.ipAddress;
               ObjLead.Preference__c=TempLead.contactPreference;
               if(!string.isblank(TempLead.companyName))
               ObjLead.Company=TempLead.companyName;
                else
                ObjLead.Company='NA';
               ObjLead.Your_Enquiry__c=TempLead.additionalComments;
              
               ListLeads.add(ObjLead);
            
           }
      
       
       try
       {
        ObjRes.errorMessage='';
        
          Database.DMLOptions dmo = new Database.DMLOptions();
            dmo.assignmentRuleHeader.useDefaultRule = true;
    
    
        
        List<Database.SaveResult> updateResults = Database.insert(ListLeads,dmo );
       
        ObjRes.isSuccess=true;
       
       List<failedLead> ListFail=new List<failedLead>();
        
        for(Integer i=0;i<updateResults.size();i++)
        {
            if (!updateResults.get(i).isSuccess())
            {
                failedLead ObjfailedLead=new failedLead();
                ObjfailedLead.leadId=ListLeads[i].Milestone_ID__c;
                ObjfailedLead.errorMessage=updateResults.get(i).getErrors()[0].getMessage();
                ListFail.add(ObjfailedLead);
            
            }
        }
        ObjRes.isSuccess=ListFail.isEmpty();
        ObjRes.failedLeads=ListFail;

       }
       catch(DmlException e)
       {
         
            ObjRes.isSuccess=false;
            ObjRes.errorMessage=e.getMessage();
           // jsonString = JSON.serialize(ObjRes);
           
       }
         
         return ObjRes;
       
       //    return  '';
    }
 

 global class  DFZCCreateResponce
{
    boolean isSuccess{get;set;}
    string errorMessage{get;set;}
    List<failedLead> failedLeads{get;set;}
    
}
public  class  failedLead
{
    string leadId{get;set;}
    string errorMessage{get;set;}
}


global  class  DFZCLead
{
    string leadId{get;set;}
    string firstName{get;set;}
    string lastName{get;set;}
    string phoneNumber{get;set;}
    string email{get;set;}
    List<string> areaOfInterest{get;set;}
    string subZoneName{get;set;}
    string ipAddress{get;set;}
    string contactPreference{get;set;}
    string companyName{get;set;}
    string additionalComments{get;set;}
    Datetime submittedOn{get;set;}
    
    boolean gdprConsent{get;set;} 
    
    //Only use in case of get Lead details
    string status{get;set;}
    Datetime updatedOn{get;set;}

    
    
}
    
}