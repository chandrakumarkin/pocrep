public without sharing class PDFDocumentViewerCls_CaptchaQR {

    public string documentNumber {get; set;}

    public transient string AttachValBase64{get;set;}
    //public transient blob AttachBlobVal{get;set;}
    public string AtchContentType{get;set;}

    public boolean blobError {set; get;}

    public boolean isCaptchaEnabled {set; get;}
    public String debugMessage {get; set;}

   
    public void PageLoad()
    {
        loadDocument(apexpages.currentPage().getparameters().get('ref'));
         
    }
         
   
    
    public void loadDocument(String ReferenceNumber){
    System.debug(ReferenceNumber);
        if(ReferenceNumber!=null && ReferenceNumber!='')
        {

            try {
            	
            	if(ReferenceNumber.startsWith('SR') ){
                	List<HexaBPM__SR_Doc__c> srDoc = new List<HexaBPM__SR_Doc__c>();
                	srDoc = [select HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where Document_No_Id__c=:ReferenceNumber.trim()];
                	
                	for(ContentVersion eachVersion:[SELECT Id,VersionData,Application_Document__c,FileExtension FROM Contentversion where ContentDocumentID=:srDoc[0].HexaBPM__Doc_ID__c]){
                		//AttachBlobVal = eachVersion.VersionData;  
                		AtchContentType = eachVersion.FileExtension;
	                    AttachValBase64 = EncodingUtil.Base64Encode(eachVersion.VersionData);
                	}
            	}
            	else {
            		list<SR_Doc__c> lstSRDoc = [select id,Name,Doc_ID__c,Status__c,Document_Validity__c from SR_Doc__c where Document_No_Id__c=:ReferenceNumber.trim() ];
            		if(lstSRDoc!= null && lstSRDoc.size() == 1)
	                {
	                    for(Attachment attch:[Select Id,Body,ContentType from Attachment where Id=:lstSRDoc[0].Doc_ID__c]){
	                        //AttachBlobVal = attch.Body;  
	                        AtchContentType = attch.ContentType;
	                        AttachValBase64 = EncodingUtil.Base64Encode(attch.body);
	                    }
	                }
            	}

                if(AttachValBase64 == null){
                    AttachValBase64 = '0';
                    blobError = true;
                }else {
                    blobError = false;
                }
                
            }catch(Exception ex){  debugMessage = ex.getMessage();
                System.debug('Error occured:' + ex.getMessage());
            }
        }
    }
    



}