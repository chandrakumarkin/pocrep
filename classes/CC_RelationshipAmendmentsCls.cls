/***************************************************************************************************
    Author Name :   Durga Prasad
    Class Name  :   CC_RelationshipAmendmentsCls
    Description :   Custom code which will be used for the specific Relationship Amendment processes.
    ________________________________________________________________________________________________
    Modification History
    ________________________________________________________________________________________________
    Date			Name				Description
    ________________________________________________________________________________________________
    V1.0			Saima				Fixed relationship type and commented SR update code
    
*****************************************************************************************************/
public without sharing class CC_RelationshipAmendmentsCls {
    
    /*
        Method Name :   Convert_General_or_Limited_Partner
        Description :   Creates the new Relationship with selected Relationship in SR, 
                        InActivate the Old Relationship and the new relationship type is changed according to the process.
    */
    public static string Convert_General_or_Limited_Partner(Step__c stp){
        string strResult = 'Success';
        if(stp!=null && stp.SR__c!=null && stp.SR__r.Relationship__c!=null && stp.SR__r.Type_of_Partner__c!=null){
            Relationship__c objOldRel = new Relationship__c();
            for(Relationship__c rel:[select id,Active__c,Appointment_Date__c,End_Date__c,Object_Account__c,Object_Contact__c,Title__c,
            Relationship_Type__c,SAP_Partner_1__c,SAP_Partner2__c,Start_Date__c,Subject_Account__c,Subject_Contact__c,Ter_Date__c 
            from Relationship__c where Id=:stp.SR__r.Relationship__c and Active__c=true]){
                objOldRel = rel;
            }
            if(objOldRel!=null && objOldRel.Id!=null){
                Relationship__c objNewRel = new Relationship__c();
                objNewRel = objOldRel.clone();
                if(stp.SR__r.Type_of_Partner__c=='General Partner'){
                    objNewRel.Relationship_Type__c = 'Has Limited Partner';
                }else if(stp.SR__r.Type_of_Partner__c=='Limited Partner'){
                    objNewRel.Relationship_Type__c = 'Has General Partner';
                }
                try{
                    insert objNewRel;
                    
                    //V1.0
                    //Service_Request__c objSR = new Service_Request__c(Id=stp.SR__c);
                    //objSR.Relationship__c = null;
                    //objSR.Sys_Relationship_Id__c = stp.SR__r.Relationship__c;
                    //update objSR;
                    
                    objOldRel.Active__c = false;
                    update objOldRel;
                    
                }catch(Exception e){
                    string DMLError = e.getdmlMessage(0)+'';
                    if(DMLError==null){
                        DMLError = e.getMessage()+'';
                    }
                    strResult = DMLError;
                    return strResult;
                }
            }
        }
        return strResult;
    }
}