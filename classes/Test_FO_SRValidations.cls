/******************************************************************************************
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Description : Test Class for FO_SrValidations Class
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    06-06-2016  Claude        Created
V1.1    06-22-2016  Claude        Reduced test methods to 5
V1.2    16-10-2016  Claude        Added new custom setting for validating Request for Isolation Services
V2.0    03-Nov-2018 Mudasir       Commented code for testFoSrValidationsSiteVisitWithoutViolations as we were getting 101 SOQL error  
V2.1    31-Jan-2021 Mudasir       Added test class code for checkUserSubmitRequestPermission and fitoutTemporaryFitoutPermitValidations  
****************************************************************************************************************/
@isTest
private class Test_FO_SRValidations {

    /**
     * Sets the required Data for conducting the 
     * tests
     */
    @testSetup static void setTestData() {
        Test_FitoutServiceRequestUtils.prepareFitOutTestData();
    }
    
    static testMethod void testReissuePermitValidation(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.testReissuePermitValidation();
    }
    
    static TestMethod void testFoSrValidationsWorkType(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.testFoSrValidationsWorkType();
    }
    
    static TestMethod void testFoSrValidationsSiteVisitWithoutViolations(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
        //V2.0 
        //Test_FitoutServiceRequestUtils.testFoSrValidationsSiteVisitWithoutViolations();
    }
    
    static TestMethod void testFoSrValidationsEventServiceRequest(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.testFoSrValidationsEventServiceRequest();
    }
    
    static TestMethod void testFoSrValidationContractorAccess(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.testFoSrValidationContractorAccess();
        
    }
    static testMethod void runDummyMethod()
    {
        FO_SRValidations.dummyMethod(); 
    }
    
    static testMethod void runCheckUserSubmitRequestPermission()
    {
        FO_SRValidations.checkUserSubmitRequestPermission(); 
    }
    
    static testMethod void runFitoutTemporaryFitoutPermitValidations()
    {
        Service_Request__c servReq = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        FO_SRValidations.fitoutTemporaryFitoutPermitValidations(servReq.id); 
    }
}