/**
*Author : Merul Shah
*Description : This cmp save or delete Share allocation.
**/
public without sharing class OB_ShareAllocationInnerDetailController
{
    
    
    @AuraEnabled   
    public static ResponseWrapper  saveShareHolderDetails(String reqWrapPram)
    {
         //reqest wrpper
         RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
         //response wrpper
         ResponseWrapper respWrap = new ResponseWrapper();
         
         try
         {
             Shareholder_Detail__c shrhlrdDetl =  reqWrap.shrDetailWrap.shareHolderDetObj;
             shrhlrdDetl.Sys_Proposed_No_of_Shares__c = shrhlrdDetl.No_of_Shares_Allotted__c;
                
             upsert reqWrap.shrDetailWrap.shareHolderDetObj;
             respWrap.shrDetailWrap = reqWrap.shrDetailWrap;
         }
         catch(DMLException  e)
         {
            respWrap.errorMsg = OB_QueryUtilityClass.getMessageFromDMLException(e); 
            return respWrap;
         }
         return respWrap;
         
    }
    
    @AuraEnabled   
    public static ResponseWrapper  deleteShareHolderDetails(String reqWrapPram)
    {
         //reqest wrpper
         RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
         //response wrpper
         ResponseWrapper respWrap = new ResponseWrapper();
         
         try
         {
             delete reqWrap.shrDetailWrap.shareHolderDetObj;
         }
         catch(DMLException e)
         {
            respWrap.errorMsg = OB_QueryUtilityClass.getMessageFromDMLException(e); 
            return respWrap;
         }
         
         return respWrap;
         
    }
    /*
    public class SRWrapper 
    {
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        //@AuraEnabled public List<OB_ShareAllocationDetailController.AmendmentWrapper> amedWrapLst { get; set; }
        @AuraEnabled public List<AmendmentWrapper> amedShrHldrWrapLst { get; set; }
        
        public SRWrapper()
        {}
    }
    */
    
    public class AmendmentWrapper 
    {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public Boolean isIndividual { get; set; }
        @AuraEnabled public Boolean isBodyCorporate { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        @AuraEnabled public List<ShareholderDetailWrapper> shrDetailWrapLst{get;set;}
      
        public AmendmentWrapper()
        {}
    }
    
    
    public class AccountShareDetailWrapper 
    {
        @AuraEnabled public Account_Share_Detail__c accShrDetObj { get; set; }
        public AccountShareDetailWrapper()
        {}
    }
    
    public class ShareholderDetailWrapper 
    {
        @AuraEnabled public Shareholder_Detail__c shareHolderDetObj { get; set; }
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        
        public ShareholderDetailWrapper()
        {}
    }
    
    
    public class RequestWrapper 
    {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        @AuraEnabled public String amendmentID{get;set;}
        @AuraEnabled public ShareholderDetailWrapper shrDetailWrap {get;set;}
      
        
        public RequestWrapper()
        {}
    }

    public class ResponseWrapper 
    {
        //@AuraEnabled public SRWrapper  srWrap{get;set;}
        @AuraEnabled public AmendmentWrapper  amedWrap{get;set;}
         
        @AuraEnabled public List<AccountShareDetailWrapper> accShrDetailWrapLst{get;set;}
        @AuraEnabled public List<ShareholderDetailWrapper> shrDetailWrapLst{get;set;}
        @AuraEnabled public String errorMsg {get;set;}
        
        
        //@AuraEnabled public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        @AuraEnabled public ShareholderDetailWrapper shrDetailWrap { get; set; }
        
        public ResponseWrapper()
        {}
    }
    
}