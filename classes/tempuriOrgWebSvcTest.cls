@isTest
private class tempuriOrgWebSvcTest {
    
    static testmethod void testDSC() {
        
        /*
        Test.setMock(WebServiceMock.class, new tempuriOrgMockImpl());
        
        tempuriOrg parentObject = new tempuriOrg();
        
        tempuriOrg.BasicHttpBinding_IFZLicenseService testRequest = new tempuriOrg.BasicHttpBinding_IFZLicenseService(); 
        
        testRequest.UpdateLicenses(new servicesDscGovAeFreezonesintegratio.LicensesList_element());
        
        new servicesDscGovAeFreezonesintegratio.EconomicActivity();
        new servicesDscGovAeFreezonesintegratio.License();
        new servicesDscGovAeFreezonesintegratio.LicensesList_element();
        new servicesDscGovAeFreezonesintegratio.ReplyMessage_element();
        new servicesDscGovAeFreezonesintegratio.Shareholder();*/
        
        Account objAccount = new Account();
        
        objAccount.Name = 'Test Custoer 4';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001231';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.Has_Permit_1__c = true;
        objAccount.ROC_Status__c = 'Active';
        objAccount.Registration_License_No__c = '1234567890';
        objAccount.Last_PR_Push__c=date.today();
        
        insert objAccount;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Custoe5';
        objAccount1.E_mail__c = 'test@test1.com';
        objAccount1.BP_No__c = '0012312';
        objAccount1.Company_Type__c = 'Financial - related';
        objAccount1.Sector_Classification__c = 'Authorised Market Institution';
        objAccount1.Legal_Type_of_Entity__c = 'LTD';
        objAccount1.Has_Permit_1__c = true;
        //objAccount1.ROC_Status__c = 'Active';
        objAccount1.Registration_License_No__c = '4567890';
        objAccount1.Last_PR_Push__c=date.today();
        
        insert objAccount1;
        
        Relationship__c ObjRel=new Relationship__c();
        ObjRel.Subject_Account__c=objAccount.id;
        ObjRel.Object_Account__c=objAccount1.id;
        ObjRel.Relationship_Type__c='Is Shareholder Of';
        ObjRel.Active__c=true;
        ObjRel.Start_Date__c=date.today();
        insert ObjRel;
        
        
        
        
        
        
        
        License__c objLicense = new License__c();
        
        objLicense.Account__c = objAccount.Id;
        objLicense.License_Expiry_Date__c = system.today().addYears(2);
        objLicense.Status__c = 'Active';
        
        insert objLicense;
        
        License_Activity_Master__c lam = new License_Activity_Master__c(Name = 'Test Master Activity',Activity_Code__c = '1');
        
        insert lam;
        
        License_Activity__c lcc = new License_Activity__c(Account__c = objAccount.id,Activity__c = lam.id,Start_Date__c = Date.newInstance(2015,12,17),End_Date__c = Date.newInstance(2015,12,18));
        
        insert lcc;
        
        Test.startTest();
        
        Test.setMock(WebServiceMock.class, new tempuriOrgMockImpl());
        
       CC_DSC_DataShare PushDateTODSC=new CC_DSC_DataShare();
       PushDateTODSC.PushDateTODSC(objAccount.id);
       
        
        ROCScheduleBatchLicenseUpdate testUpdate = new ROCScheduleBatchLicenseUpdate();
        
        Database.executeBatch(testUpdate,20); // Execute a batch job directly
        
        String jobId = System.schedule('ROCScheduleBatchLicenseUpdate',Test_CC_FitOutCustomCode_DataFactory.CRON_EXP,new ROCScheduleBatchLicenseUpdate());
        
        Test.stopTest();
    }
    
    public class tempuriOrgMockImpl implements WebServiceMock {
        
        public void doInvoke(
                Object stub,
                Object request,
                Map<String, Object> response,
                String endpoint,
                String soapAction,
                String requestName,
                String responseNS,
                String responseName,
                String responseType) {
           
            System.debug(LoggingLevel.INFO, 'tempuriOrgMockImpl.doInvoke() - ' +
                '\n request: ' + request +
                '\n response: ' + response +
                '\n endpoint: ' + endpoint +
                '\n soapAction: ' + soapAction +
                '\n requestName: ' + requestName +
                '\n responseNS: ' + responseNS +
                '\n responseName: ' + responseName +
                '\n responseType: ' + responseType);
    
            if(request instanceOf servicesDscGovAeFreezonesintegratio.LicensesList_element) {
                response.put( 'response_x', new servicesDscGovAeFreezonesintegratio.ReplyMessage_element());
            }
        }
    }
}