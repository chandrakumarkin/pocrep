/*
Created By  : Suchita - DIFC on 14 Dec 2020 
Description : This Class is extension class for Questionnaire visualforce page
--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No        Date                Updated By          Description
v1.0      14 Dec 2020          Suchita Sharma,      Initial Version

----------------------------------------------------------------------------------------
*/
public class QuestionnaireExtCtrl {
    public Questionnaire__c QuestionnaireObj {get;set;}
    
    public boolean IsValid {get;set;}
    public boolean GWP {get;set;}
    public boolean ShowOtherSectionFlag {get;set;}
    public boolean ShowOtherSectionFlag1 {get;set;}
    public List<QuestionnaireChild__c> questionnaireChildList {get; set;}
    public List<QuestionnaireChild__c> questionnaireChildList2 {get; set;}
    public List<QuestionnaireChild__c> questionnaireChildList3 {get; set;}
    public List<QuestionnaireChild__c> questionnaireChildList4 {get; set;}
    public List<QuestionnaireChild__c> questionnaireChildList5 {get; set;}
    
    
    public List<QuestionnaireChild__c> lobUAEQCList {get; set;}
    public List<QuestionnaireChild__c> lobMEQCList {get; set;}
    public List<QuestionnaireChild__c> lobGCCQCList {get; set;}
    public List<QuestionnaireChild__c> lobAfricaQCList {get; set;}
    public List<QuestionnaireChild__c> lobOTHERQCList {get; set;}
    
    public string indexNumber{get;set;}


    String ContactId;
    public Boolean disabl {get;set;}
    
    
    public QuestionnaireExtCtrl(ApexPages.StandardController ctrl) {
        QuestionnaireObj = (Questionnaire__c) ctrl.getRecord();
        ShowOtherSectionFlag = false;
        ShowOtherSectionFlag1 = false;
        GWP = false;
        questionnaireChildList = new List<QuestionnaireChild__c>();
        AddRow();
        questionnaireChildList2 = new List<QuestionnaireChild__c>();
        AddRowWrittenPremiums();
        questionnaireChildList3 = new List<QuestionnaireChild__c>();
        AddRowOther();
        
        questionnaireChildList4 = new List<QuestionnaireChild__c>();
        AddRowquestionnaireChildList4();

        questionnaireChildList5 = new List<QuestionnaireChild__c>();
        AddRowOther1();
        
        lobUAEQCList = new List<QuestionnaireChild__c>(); 
        lobMEQCList = new List<QuestionnaireChild__c>(); 
        lobGCCQCList = new List<QuestionnaireChild__c>(); 
        lobAfricaQCList = new List<QuestionnaireChild__c>(); 
        lobOTHERQCList = new List<QuestionnaireChild__c>(); 
        addLOBLineiTemsOnPageLoad();
    }
    public void addLOBLineiTemsOnPageLoad() {
        
        lobUAEQCList.add(new QuestionnaireChild__c(LOB_Country__c = GlobalConstants.LOB_Country_UAE));
        lobMEQCList.add(new QuestionnaireChild__c(LOB_Country__c = GlobalConstants.LOB_Country_ME_EXCLUDE_UAE));
        lobGCCQCList.add(new QuestionnaireChild__c(LOB_Country__c = GlobalConstants.LOB_Country_GCC));
        lobAfricaQCList.add(new QuestionnaireChild__c(LOB_Country__c = GlobalConstants.LOB_Country_AFRICA));
        lobOTHERQCList.add(new QuestionnaireChild__c(LOB_Country__c = GlobalConstants.LOB_Country_OTHER));
    }
 
     public string lobcountry {get;set;}

    
    public PageReference addLOBLineiTems(){
        system.debug('=======lobcountry ==================='+lobcountry);
        system.debug('===111====lobUAEQCList ==================='+lobUAEQCList);
        system.debug('===111====lobUAEQCList =========sizee=========='+lobUAEQCList.size());

        if(lobcountry == GlobalConstants.LOB_Country_UAE)lobUAEQCList.add(new QuestionnaireChild__c(LOB_Country__c = GlobalConstants.LOB_Country_UAE));
        else if(lobcountry == GlobalConstants.LOB_Country_ME_EXCLUDE_UAE)lobMEQCList.add(new QuestionnaireChild__c(LOB_Country__c = GlobalConstants.LOB_Country_ME_EXCLUDE_UAE));
        else if(lobcountry == GlobalConstants.LOB_Country_GCC)lobGCCQCList.add(new QuestionnaireChild__c(LOB_Country__c = GlobalConstants.LOB_Country_GCC));
        else if(lobcountry == GlobalConstants.LOB_Country_AFRICA)lobAfricaQCList.add(new QuestionnaireChild__c(LOB_Country__c = GlobalConstants.LOB_Country_AFRICA));
        else if(lobcountry == GlobalConstants.LOB_Country_OTHER)lobOTHERQCList.add(new QuestionnaireChild__c(LOB_Country__c = GlobalConstants.LOB_Country_OTHER));
        system.debug('===222====lobUAEQCList ==================='+lobUAEQCList);
        system.debug('===222====lobUAEQCList =========sizee=========='+lobUAEQCList.size());
        return null;
    }
    
    public PageReference AddRowWrittenPremiums() {
        questionnaireChildList2.add(new QuestionnaireChild__c(Type_Of_Operation__c = GlobalConstants.TYPEOPERATION_PREMIUMS_2019));
        return null;
    }
    public PageReference AddRowOther() {
        questionnaireChildList3.add(new QuestionnaireChild__c(Type_Of_Operation__c = GlobalConstants.TYPEOPERATION_OTHER_CLAIMS_PAID));
        return null;
    } 
    
    public PageReference AddRowOther1() {
        questionnaireChildList5.add(new QuestionnaireChild__c(Type_Of_Operation__c = GlobalConstants.TYPEOPERATION_OTHER_RISKS_PAID));
        return null;
    } 
    
    public PageReference AddRowquestionnaireChildList4() {
        questionnaireChildList4.add(new QuestionnaireChild__c(Type_Of_Operation__c = GlobalConstants.TYPEOPERATION_PREMIUMS_2020 ));
        return null;
    } 
    
    public PageReference AddRow() {
        questionnaireChildList.add(new QuestionnaireChild__c());
        return null;
    } 
    
    public void pageLoad() {
        IsValid = true;
        disabl = False;
        ContactId = ApexPages.currentPage().getParameters().get('cid');
        string emailId = ApexPages.currentPage().getParameters().get('eid');
      //  string gwpString = ApexPages.currentPage().getParameters().get('gwp');
        QuestionnaireObj.Email_Address__c = emailId;
       // if(string.isblank(gwpString) == false){
           // if(gwpString=='true'){
           //     GWP = true;
          //  }else{
                GWP =false;
           // }
       // }
        
        if (string.isblank(ContactId) == false && string.isblank(emailId) == false){
            List < Contact > ListContact = [SELECT Id, FirstName, LastName 
                                            FROM Contact 
                                            WHERE id =: ContactId];
            if (ListContact.size() < 1){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You are not a valid Contact'));
                IsValid = false;
            }else{
                QuestionnaireObj.Contact__c = ListContact[0].id;
                List <Questionnaire__c> ListQuestionnaire = [SELECT id, Name, Contact__c 
                                                             FROM Questionnaire__c 
                                                             WHERE Contact__c =: Contactid];
                if (ListQuestionnaire.size() > 0){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You have already submitted the Information'));
                    IsValid = false;
                }
            }
        }else if(string.isblank(emailId) == false){
            //Nothing
        }else{
            IsValid = false;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You are not a valid Contact'));
        }
    }
    
    public PageReference SaveQuestionnaire(){
        List < contact > ListContact = [SELECT Id, Name, AccountId 
                                        FROM Contact 
                                        WHERE id =: Contactid 
                                        AND AccountId != Null];
        if (ListContact.size() > 0) {
            QuestionnaireObj.Account__c = ListContact[0].AccountId;
        } 
        upsert QuestionnaireObj;
        List<QuestionnaireChild__c> qcList = new List<QuestionnaireChild__c>();
        for(QuestionnaireChild__c qc : questionnaireChildList){
            qc.Questionnaire__c = QuestionnaireObj.id;
            qcList.add(qc);

        }
       
        
        for(QuestionnaireChild__c qc3 : questionnaireChildList3){
            if(qc3.Other_business_line__c!=null){
                qc3.Questionnaire__c = QuestionnaireObj.id;
                qcList.add(qc3);
            }
        }
        if(QuestionnaireObj.Do_you_underwrite_risks__c=='Yes'){
        
            for(QuestionnaireChild__c qc2 : questionnaireChildList2){
                if(qc2.line_of_business__c!=null){
                    qc2.Questionnaire__c = QuestionnaireObj.id;
                    qcList.add(qc2);
                }
            }
            
            for(QuestionnaireChild__c qc3 : questionnaireChildList4){
                //if(qc3.Other_business_line__c!=null){
                    qc3.Questionnaire__c = QuestionnaireObj.id;
                    qcList.add(qc3);
                //}
            }
            
            for(QuestionnaireChild__c qc3 : questionnaireChildList5){
                if(qc3.Other_business_line__c!=null){
                    qc3.Questionnaire__c = QuestionnaireObj.id;
                    qcList.add(qc3);
                }
            }
            
            for(QuestionnaireChild__c qc3 : lobUAEQCList){
                    qc3.Questionnaire__c = QuestionnaireObj.id;
                    qcList.add(qc3);
            }
            
            for(QuestionnaireChild__c qc3 : lobMEQCList){
                    qc3.Questionnaire__c = QuestionnaireObj.id;
                    qcList.add(qc3);
            }
            
            for(QuestionnaireChild__c qc3 : lobGCCQCList){
                    qc3.Questionnaire__c = QuestionnaireObj.id;
                    qcList.add(qc3);
            }
            
            for(QuestionnaireChild__c qc3 : lobAfricaQCList){
                    qc3.Questionnaire__c = QuestionnaireObj.id;
                    qcList.add(qc3);
            }
            
            for(QuestionnaireChild__c qc3 : lobOTHERQCList){
                    qc3.Questionnaire__c = QuestionnaireObj.id;
                    qcList.add(qc3);
            }
            
            
        }
        
        
        if(qcList != null){
             upsert qcList;
        }
        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm, 'Your Information has been saved successfully.'));
        disabl =True;
        IsValid = false;
        return Null;
    }
    //Method for Reredering sections by setting the values of params
    public  void RowValueChange(){
        string ValParam= Apexpages.currentPage().getParameters().get('ValParam');
        string myParam= Apexpages.currentPage().getParameters().get('myParam');
        if(myParam=='true'){
            QuestionnaireObj.put(ValParam,true);
        }else if(myParam=='false'){
            QuestionnaireObj.put(ValParam,false);  
        }else{
            QuestionnaireObj.put(ValParam,myParam);
        }
    }
    
    public void showOtherSection(){
        ShowOtherSectionFlag = true;  
    }
    public void showOtherSection1(){
        ShowOtherSectionFlag1 = true;  
    }
    /*
    Still its in progress for remove rows on table
    public void questionnaireChildList2Remove(){
       if(indexNumber != null && indexNumber != '')
        questionnaireChildList2.remove(integer.ValueOf(indexNumber)); 
    }
    
    public void lobUAEQCListRemove(){
       if(indexNumber != null && indexNumber != '')
        lobUAEQCList.remove(integer.ValueOf(indexNumber)); 
    }
    
    public void lobMEQCListRemove(){
       if(indexNumber != null && indexNumber != '')
        lobMEQCList.remove(integer.ValueOf(indexNumber)); 
    }
        
    public void lobGCCQCListRemove(){
        if(indexNumber != null && indexNumber != '')
        lobGCCQCList.remove(integer.ValueOf(indexNumber)); 
    }
    
    public void lobAfricaQCListRemove(){
        if(indexNumber != null && indexNumber != '')
        lobAfricaQCList.remove(integer.ValueOf(indexNumber)); 
    }
    
    public void lobOTHERQCListRemove(){
        if(indexNumber != null && indexNumber != '')
        lobOTHERQCList.remove(integer.ValueOf(indexNumber)); 
    }*/
    
}