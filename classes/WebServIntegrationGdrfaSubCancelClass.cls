/******************************************************************************************************
*  Author   : Kumar Utkarsh
*  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
V1.1    22-Apr-2021  Utkarsh         Modified
* This class contains logic of Submit VISA Cancellation Form Creation and GDRFA callout.
*******************************************************************************************************/
public class WebServIntegrationGdrfaSubCancelClass {
    
    @InvocableMethod 
    public static void invokeSubmitCancel(List<Id> SrIds)
    {
        System.debug('List<Id> SrIds:'+SrIds);
        Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
        for(Id SrId :SrIds){
        WebServiceGdrfaDocumentCompressClass.GDRFAImageCompressorCallout(String.valueOf(SrId));
        GDRFASubmitCancelCallout(String.valueOf(SrId), StatusReference.Id);
        }
    }
    
    public class MandateDocuments {
        public String applicationId;
        public List<Documents> Documents;
    }
    
    public class Documents {
        public String documentTypeId;
        public Boolean IsMandatory;
        public String documentNameEn;
        public String documentNameAr;
    } 
    
    public class UploadDoc{
        public Boolean Success{get;set;}
    }    
    
    //Fetched the SR's for Cancellation Request and build the JSON for submission
    public static String SubmitCancellation(String ID)
    {
        Step__c visaCancelStep;
        //Step__c entryPermitStep;
        String finalVisaCancelJSON;
        Service_Request__c SR = [SELECT Id, Service_Type__c, Nationality__c, Previous_Nationality__c, Country_of_Birth__c, Gender__c, Marital_Status__c, Religion__c, Qualification__c, Occupation__c, Passport_Type__c,
                                 First_Name__c, First_Name_Arabic__c, Middle_Name__c, Middle_Name_Arabic__c, Location__c, Last_Name__c, Last_Name_Arabic__c, Mother_Full_Name__c, P_O_Box_Emirate__c, Email__c,
                                 Mother_s_full_name_Arabic__c, Date_of_Birth__c, Place_of_Birth__c, Place_of_Birth_Arabic__c, First_Language__c, Passport_Number__c, Passport_Date_of_Issue__c, Passport_Date_of_Expiry__c,
                                 Passport_Place_of_Issue__c, Building__c, Emirates_Id_Number__c, Area__c, Street_Address__c, Current_Registered_Street__c, Mobile_Number__c, Sponsor_Mobile_No__c, Address_Details__c,
                                 Passport_Country_of_Issue__c, Type_of_Request__c, Identical_Business_Domicile__c, City_Town__c, Emirate__c, City__c, Building_Name__c, Phone_No_Outside_UAE__c, Establishment_Card_No__c,
                                 Reason_for_Cancellations__c, Residence_Visa_No__c, Record_Type_Name__c FROM Service_Request__c WHERE Id =: ID ];
        
        If(SR.Record_Type_Name__c == 'DIFC_Sponsorship_Visa_Cancellation'){
            //entryPermitStep = [SELECT Id, DNRD_Receipt_No__c FROM Step__c WHERE SR__c =: Id AND Step_Status__c = 'Closed' AND (Step_Name__c = 'Entry Permit Form is Typed' OR Step_Name__c = 'Online Entry Permit is Typed') ];
            visaCancelStep = [SELECT Id, DNRD_Receipt_No__c, Step_Name__c, Step_Status__c, SR__c FROM Step__c WHERE SR__c =: Id AND (Step_Status__c = 'Awaiting Review' OR Step_Status__c = 'Repush to GDRFA') AND Step_Name__c = 'Cancellation Form Typed' ];
        }
        
        String isInsideUAE;
        If(SR.Type_of_Request__c == 'Applicant Inside UAE'){
            isInsideUAE = 'Yes';
        }else{
            isInsideUAE = 'No';
        }
        
        If(visaCancelStep != null){
            JSONGenerator jsonVisaCancelObj = JSON.createGenerator(true);
            jsonVisaCancelObj.writeStartObject();
            if(visaCancelStep.DNRD_Receipt_No__c != null && visaCancelStep.DNRD_Receipt_No__c != ''){
                jsonVisaCancelObj.writeStringField('ApplicationNumber', visaCancelStep.DNRD_Receipt_No__c);
            }
            if(SR.Residence_Visa_No__c != null){
                jsonVisaCancelObj.writeStringField('residenceNo', SR.Residence_Visa_No__c.replaceAll('/',''));
            }
            jsonVisaCancelObj.writeFieldName('applicationData');
            jsonVisaCancelObj.writeStartObject();
            jsonVisaCancelObj.writeFieldName('Application');
            jsonVisaCancelObj.writeStartObject();
            jsonVisaCancelObj.writeFieldName('ApplicantDetails');
            jsonVisaCancelObj.writeStartObject();
            jsonVisaCancelObj.writeStringField('IsInsideUAE', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Inside Outside Country',isInsideUAE));
            jsonVisaCancelObj.writeEndObject();
            jsonVisaCancelObj.writeFieldName('SponsorDetails');
            jsonVisaCancelObj.writeStartObject();
            if(SR.Establishment_Card_No__c != null){
                jsonVisaCancelObj.writeStringField('EstCode', SR.Establishment_Card_No__c.replaceAll('/',''));
            }
            jsonVisaCancelObj.writeEndObject();
            jsonVisaCancelObj.writeFieldName('ApplicationDetails');
            jsonVisaCancelObj.writeStartObject();
            if(!String.isBlank(SR.Reason_for_Cancellations__c)){
            jsonVisaCancelObj.writeStringField('CancellationReason', WebServGDRFAAuthFeePaymentClass.fetchGDRFALookUpId('Cancellation Reasons',SR.Reason_for_Cancellations__c));
            }else if(String.isBlank(SR.Reason_for_Cancellations__c)){
            jsonVisaCancelObj.writeStringField('CancellationReason', '1');   
            }
            jsonVisaCancelObj.writeBooleanField('isDraft', false);
            jsonVisaCancelObj.writeEndObject();
            jsonVisaCancelObj.writeEndObject();
            jsonVisaCancelObj.writeEndObject();
            jsonVisaCancelObj.writeEndObject();
            finalVisaCancelJSON = jsonVisaCancelObj.getAsString();
            System.debug('DEBUG Full Request finalJSON : ' +(jsonVisaCancelObj.getAsString()));
        }
        WebServGDRFAAuthFeePaymentClass.masterDataMap = null;
        return finalVisaCancelJSON; 
    }
    
    @future(callout=true)
    //Callout method for VISA Cancellation
    public static void GDRFASubmitCancelCallout(String ID, String StatusReference) 
    { 
        Step__c visaCancelStep;
        map<Id,Id> mapStepStepTemplate = new map<Id,Id>();
        String visaCancelJson;
        Attachment attachment = new Attachment();
        attachment.Name = 'VISA Cancel Json.txt';
        attachment.ParentId = ID;
        attachment.Body = Blob.valueOf('Error before Json Creation');
        attachment.ContentType = 'text/plain';
        //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
        try{
            String finalDocJSON;
            Boolean isAllUploadSuccess = false;
            MandateDocuments responseDocList;
            visaCancelStep = [SELECT Id, DNRD_Receipt_No__c, Step_Name__c, SR_Step__c, Step_Template__c, Step_Status__c, SR__c FROM Step__c WHERE SR__c =: ID AND (Step_Status__c = 'Awaiting Review' OR Step_Status__c = 'Repush to GDRFA') AND Step_Name__c = 'Cancellation Form Typed'];
            for(SR_Steps__c obj: [select id, Step_Template__c from SR_Steps__c where Step_Template_Code__c IN('Online entry permit is typed', 'Entry permit form is typed', 'Renewal form is typed', 'Cancellation Form Typed', 'Entry Permit is Issued', 'Completed', 'Passport is Ready for Collection', 'Visa Stamping Form is Typed') AND SR_Template__r.SR_Template_Code__c IN ('DIFC_Sponsorship_Visa_New','DIFC_Sponsorship_Visa_Renewal', 'DIFC_Sponsorship_Visa_Cancellation') ]){
                mapStepStepTemplate.put(obj.Step_Template__c,obj.Id);                                                                        
            }
            if(mapStepStepTemplate.containsKey(visaCancelStep.Step_Template__c) && visaCancelStep.SR_Step__c == null){
                visaCancelStep.SR_Step__c = mapStepStepTemplate.get(visaCancelStep.Step_Template__c);
            }
            WebServGDRFAAuthFeePaymentClass.ResponseBasicAuth ObjResponseAuth = WebServGDRFAAuthFeePaymentClass.GDRFAAuth();
            List<Log__c> ErrorObjLogList = new List<Log__c>();
            //Get authrization code 
            string access_token= ObjResponseAuth.access_token;
            string token_type=ObjResponseAuth.token_type;
            //WebService to Push the Submit Cancel Form
            HttpRequest request = new HttpRequest();
            request.setEndPoint(System.label.GDRFA_Submit_Cancel_Residence); 
            visaCancelJson = SubmitCancellation(ID);
            attachment.Body = Blob.valueOf(visaCancelJson);
            attachment.ParentId = visaCancelStep.Id;
            request.setBody(visaCancelJson);
            request.setTimeout(120000);
            request.setHeader('apikey', System.label.GDRFA_API_Key);
            request.setHeader('Content-Type', 'application/json');
            request.setMethod('POST');
            request.setHeader('Authorization',token_type+' '+access_token);
            HttpResponse response = new HTTP().send(request);
            System.debug('responseStringresponse:'+response.toString());
            System.debug('STATUS:'+response.getStatus());
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('strJSONresponseSubmitCancel:'+response.getBody());
            request.setTimeout(101000);

            if (response.getStatusCode() == 200)
            {
                responseDocList = (MandateDocuments) System.JSON.deserialize(response.getBody(), MandateDocuments.class);
                System.debug('applicationId:'+responseDocList.applicationId);
                System.debug('Documents:'+responseDocList.Documents);
                Boolean mandateDocsExist = false;
                Set<String> setGDRFADocId = new Set<String>();
                map<Id, String> mapSRDocIdGDRFADocId = new map<Id, String>();
                map<Id, String> mapSRDocIdGDRFAname = new map<Id, String>();
                map<Id, String> mapSRDocIdGDRFAArabicName = new map<Id, String>();
                map<String, String> mapGdrfaDocIdGDRFAname = new map<String, String>();
                map<String, String> mapGdrfaDocIdGDRFAArabicname = new map<String, String>();
                map<String, Boolean> mapGdrfaDocIdGdrfaIsMandate = new map<String, Boolean>();
                map<String, Boolean> mapSRDocIdGdrfaIsMand = new map<String, Boolean>();
                map<Id, Boolean> mapSRDocMandate = new map<Id, Boolean>();
                if(responseDocList.Documents != null){
                    For(Documents docList : responseDocList.Documents){
                        setGDRFADocId.add(docList.documentTypeId);
                        mapGdrfaDocIdGdrfaIsMandate.put(docList.documentTypeId, docList.IsMandatory);
                        mapGdrfaDocIdGDRFAname.put(docList.documentTypeId, docList.documentNameEn);
                        mapGdrfaDocIdGDRFAArabicname.put(docList.documentTypeId, docList.documentNameAr);
                    }
                }
                System.debug('mapGdrfaDocIdGdrfaIsMandate:'+mapGdrfaDocIdGdrfaIsMandate);
                for(SR_Doc__c objDoc:  [SELECT GDRFA_Attchment_Doc_Id__c, GDRFA_Parent_Document_Id__c FROM SR_Doc__c
                                        WHERE Service_Request__c =: ID AND GDRFA_Parent_Document_Id__c IN: setGDRFADocId AND GDRFA_Parent_Document_Id__c != null AND GDRFA_Attchment_Doc_Id__c != null]){
                                            mapSRDocIdGDRFADocId.put(objDoc.GDRFA_Attchment_Doc_Id__c, objDoc.GDRFA_Parent_Document_Id__c);
                                            mapSRDocIdGDRFAname.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGDRFAname.get(objDoc.GDRFA_Parent_Document_Id__c));
                                            mapSRDocIdGdrfaIsMand.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGdrfaIsMandate.get(objDoc.GDRFA_Parent_Document_Id__c));
                                            mapSRDocIdGDRFAArabicName.put(objDoc.GDRFA_Attchment_Doc_Id__c, mapGdrfaDocIdGDRFAArabicname.get(objDoc.GDRFA_Parent_Document_Id__c));
                                        }
                System.debug('mapSRDocIdGDRFADocId:'+mapSRDocIdGDRFADocId);
                System.debug('mapSRDocIdGdrfaIsMand:'+mapSRDocIdGdrfaIsMand);
                for(Attachment objAttch: [SELECT Id, Name, ContentType, Body from Attachment where Id IN:mapSRDocIdGDRFADocId.keySet()]){
                    //Webservice to push Sr docs
                    JSONGenerator docJsonObj = JSON.createGenerator(true);
                    docJsonObj.writeStartObject();
                    docJsonObj.writeFieldName('Documents');
                    docJsonObj.writeStartObject();
                    System.debug('mapSRDocIds1:'+mapSRDocIdGDRFADocId.get(objAttch.Id));
                    docJsonObj.writeObjectField('documentTypeId', mapSRDocIdGDRFADocId.get(objAttch.Id));
                    docJsonObj.writeBlobField('document', objAttch.Body);    
                    docJsonObj.writeBooleanField('IsMandatory', mapSRDocIdGdrfaIsMand.get(objAttch.Id)); 
                    docJsonObj.writeStringField('MineType', objAttch.ContentType);
                    docJsonObj.writeStringField('documentNameEn', mapSRDocIdGDRFAname.get(objAttch.Id)); 
                    docJsonObj.writeStringField('documentNameAr', mapSRDocIdGDRFAArabicName.get(objAttch.Id)); 
                    docJsonObj.writeStringField('FileName', objAttch.Name); 
                    docJsonObj.writeEndObject();
                    docJsonObj.writeStringField('applicationId', responseDocList.applicationId);
                    docJsonObj.writeEndObject();
                    finalDocJSON = docJsonObj.getAsString();
                    System.debug('DEBUG Full Request finalJSON : ' +(docJsonObj.getAsString()));
                    HttpRequest requestUploadDoc = new HttpRequest();
                    requestUploadDoc.setEndPoint(System.label.GDRFA_Document_Upload_Endpoint);        
                    requestUploadDoc.setBody(docJsonObj.getAsString());
                    requestUploadDoc.setTimeout(120000);
                    requestUploadDoc.setHeader('apikey', System.label.GDRFA_API_Key);
                    requestUploadDoc.setHeader('Content-Type', 'application/json');
                    requestUploadDoc.setMethod('POST');
                    requestUploadDoc.setHeader('Authorization',token_type+' '+access_token);
                    HttpResponse responseUploadDoc = new HTTP().send(requestUploadDoc);
                    System.debug('responseUploadDoc:'+responseUploadDoc.toString());
                    System.debug('STATUS:'+responseUploadDoc.getStatus());
                    System.debug('STATUS_CODE:'+responseUploadDoc.getStatusCode());
                    System.debug('strJSONresponseUploadDoc:'+responseUploadDoc.getBody());
                    if (responseUploadDoc.getStatusCode() == 200)
                    {
                        UploadDoc docUpload = (UploadDoc) System.JSON.deserialize(responseUploadDoc.getBody(), UploadDoc.class);
                        System.debug('IsSuccess:'+docUpload.Success);
                        if(docUpload.Success){
                            isAllUploadSuccess = true;   
                        }else{
                            isAllUploadSuccess = false;
                        }   
                    }
                    else{
                        Log__c ErrorobjLog = new Log__c();
                        ErrorobjLog.Type__c = objAttch.Id+'-'+mapSRDocIdGDRFAname.get(objAttch.Id)+'-'+objAttch.Id+'-Document Upload Error';
                        ErrorobjLog.Description__c = 'Response: '+ responseUploadDoc.getBody();
                        ErrorobjLog.SR_Step__c = visaCancelStep.Id;
                        ErrorObjLogList.add(ErrorobjLog); 
                        isAllUploadSuccess = false;
                    }
                }
                if(!ErrorObjLogList.isEmpty() && ErrorObjLogList != null){
                    try{
                        insert ErrorObjLogList;
                    }catch(Exception ex) {
                        System.Debug(ex);
                    }
                    isAllUploadSuccess = false;
                    //insert attachment;
                    //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
                    visaCancelStep.Status__c = StatusReference;
                    visaCancelStep.Step_Notes__c = 'Document Upload Error';
                    visaCancelStep.OwnerId = UserInfo.getUserId();
                    try{
                        Update visaCancelStep;
                    }catch(Exception ex) {
                        insert LogDetails.CreateLog(null, 'VISA Cancellation Step', 'SR Id is ' + visaCancelStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                    }
                    
                }   
            }
            else {
                WebServGDRFAAuthFeePaymentClass.ErrorClass ErrorLog = (WebServGDRFAAuthFeePaymentClass.ErrorClass) System.JSON.deserialize(response.getBody(), WebServGDRFAAuthFeePaymentClass.ErrorClass.class);
                Log__c objLog = new Log__c();
                objLog.Type__c = 'VISA Cancel Error ';
                objLog.Description__c = 'Request: '+visaCancelJson +'Response: '+ response.getBody();
                objLog.SR_Step__c = visaCancelStep.Id;
                insert objLog;
                //insert attachment;
                
                //Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
                if(ErrorLog.errorMsg[0].messageEn != null){
                    visaCancelStep.Step_Notes__c = ErrorLog.errorMsg[0].messageEn;
                }
                visaCancelStep.Status__c = StatusReference;
                visaCancelStep.OwnerId = UserInfo.getUserId();
                try{
                    update visaCancelStep;
                }catch(Exception ex) {
                    insert LogDetails.CreateLog(null, 'VISA Cancellation Step on Error', 'SR Id is ' + visaCancelStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
                }
                
            }
            //Webservice to get the Application Fee 
            if(isAllUploadSuccess){
                WebServGDRFAAuthFeePaymentClass.getApplicationFeeAPI(responseDocList.applicationId, access_token, token_type, ID, visaCancelStep, StatusReference);
                //insert attachment;
            }
            insert attachment;
        }catch(exception e){
            insert attachment;
            system.debug('Error:- ' + e); 
            visaCancelStep.Status__c = StatusReference;
            visaCancelStep.OwnerId = UserInfo.getUserId();
            visaCancelStep.Step_Notes__c = 'Error:- ' + e;
            try{
                Update visaCancelStep; 
            }catch(Exception ex) {
                insert LogDetails.CreateLog(null, 'VISA Cancellation Step on Exception', 'SR Id is ' + visaCancelStep.SR__c + 'Line # '+ex.getLineNumber()+'\n'+ex.getMessage());
            }
            Log__c objLog = new Log__c();objLog.Type__c = 'VISA Cancellation' + e;objLog.Description__c = 'Error:- ' + e;objLog.SR_Step__c = visaCancelStep.Id;
            insert objLog; 
        }
    }
}