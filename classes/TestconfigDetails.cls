/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 11-Dec-2014   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class TestconfigDetails {

    public static testmethod void testconDet() {
        configDetailsCls config = new configDetailsCls();
        configDetailsCls.getSAPReltype('Director');
        configDetailsCls.getSAPReltype('company secretary');
        configDetailsCls.getSAPReltype('founding member');
        configDetailsCls.getSAPReltype('general partner');
        configDetailsCls.getSAPReltype('limited partner');
        configDetailsCls.getSAPReltype('manager');
        configDetailsCls.getSAPReltype('member');
        configDetailsCls.getSAPReltype('shareholder');
        configDetailsCls.getSAPReltype('ubo');
        configDetailsCls.getSAPReltype('services personnel');
        configDetailsCls.getSAPReltype('authorized representative');
        configDetailsCls.getSAPReltype('authorized signatory');
        configDetailsCls.getSAPReltype('designated member');
        configDetailsCls.getSAPReltype('auditor');
        configDetailsCls.getSAPReltype('author');
        configDetailsCls.getLegaltype('LTD');
        configDetailsCls.getLegaltype('LTD SPC');
        configDetailsCls.getLegaltype('LTD PCC');
        configDetailsCls.getLegaltype('LTD IC');
        configDetailsCls.getLegaltype('LTD National');
        configDetailsCls.getLegaltype('LTD Supra National');
        configDetailsCls.getLegaltype('LLC');
        configDetailsCls.getLegaltype('LLP');
        configDetailsCls.getLegaltype('RLLP');
        configDetailsCls.getLegaltype('FRC');
        configDetailsCls.getLegaltype('GP');
        configDetailsCls.getLegaltype('RP');
        configDetailsCls.getLegaltype('RLP');
        configDetailsCls.getLegaltype('LP');
        configDetailsCls.getLegaltype('NPIO');
        configDetailsCls.getLegaltype('NO');
        
    }
/*
public static testmethod void testAutoComplete() {
    AutoCompleteCls autoCls = new AutoCompleteCls();
    ApexPages.CurrentPage().getparameters().put('objectname','Service_Request__c');
    ApexPages.CurrentPage().getparameters().put('aname','');
    autoCls.clearValues();
    Service_Request__c objSR = new Service_Request__c();
    insert objSR;
    autoCls.searchSuggestions();
    autoCls.avoidRefresh();
    boolean flag = autoCls.getHasparams();
    boolean flag1 = autocls.getHasnoresults();
    list<String> strlist = autoCls.getResultsname();
}*/

    public static testmethod void testRocViewContact() {
        Apexpages.currentPage().getParameters().put('type','Account');
        Account audRecAcc = new Account();
        audRecAcc.Name = 'Test Customer 3';
        audRecAcc.E_mail__c = 'test@test.com';
        audRecAcc.Authorized_Share_Capital__c = 500000;
        
        insert audRecAcc;
        
        Contact cont = new Contact(firstname='Tim',lastname='Khan',accountId=audRecAcc.id,Email='test@test.com');
        insert cont;
        Apexpages.currentPage().getParameters().put('id',audRecAcc.id);
        RocViewClient obj = new RocViewClient();
        
        Apexpages.currentPage().getParameters().put('id',cont.id);
        Apexpages.currentPage().getParameters().put('type','Contact');
        obj = new RocViewClient();
    }
    
    public static testmethod void testDeepDelete() {
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        SR_Steps__c srStp = new SR_Steps__c();
        srStp.Active__c = true;
        srStp.SR_Template__c = objTemplate.id;
        insert srStp;
        
        Business_Rule__c busRule = new Business_Rule__c();
        busRule.SR_Steps__c = srStp.id;
        insert busRule;
        
        Step_Transition__c stpTrans = new Step_Transition__c();
        stpTrans.SR_Template__c = objTemplate.id;
        stpTrans.SR_Step__c = srStp.id;
        insert stpTrans;
        
        SR_Template_Item__c stpItem = new SR_Template_Item__c();
        stpItem.SR_Template__c = objTemplate.id;
        insert stpItem;
        
        Document_Master__c docMaster = new Document_Master__c();
        docMaster.Name = 'Test Document';
        docMaster.Code__c = 'END';
        insert docMaster;
        
        SR_Template_Docs__c srDocs= new SR_Template_Docs__c();
        srDocs.SR_Template__c = objTemplate.id;
        srDocs.Document_Master__c = docMaster.id;
        insert srDocs;
        
        Condition__c cond = new Condition__c();
        cond.SR_Template_Docs__c = srDocs.id;
        insert cond;
        
        DeepDelete_SRTemplateCls.DeepDelete(objTemplate.id);
    }
    

}