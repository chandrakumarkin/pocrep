global class GsBatchFailedWebServices implements Schedulable,Database.Batchable<sObject>,Database.AllowsCallouts {


global void execute(SchedulableContext ctx) {
       
        database.executeBatch(new GsBatchFailedWebServices(), 1);
    }
    
    
    global list<Service_Request__c> start(Database.BatchableContext BC ){
        list<Service_Request__c> lstFailedSRs = new list<Service_Request__c>();
        Boolean BlockWebServices = false;
        if(Block_Trigger__c.getAll() != null && Block_Trigger__c.getAll().get('Block') != null && Block_Trigger__c.getAll().get('Block').Block_Web_Services__c)
            BlockWebServices = true;
        if(BlockWebServices == false){
        
            integer Mins = Integer.valueof(system.label.GS_Failed_webService_Counter);
            DateTime dTime = system.now().addMinutes(-Mins);
            if(test.isRunningTest())
                dTime = system.now().addMinutes(5);
            for(Service_Request__c objSR : [select Id,Internal_Status_Name__c,SR_Template__r.Menu__c,Contact__c,Sponsor__c from Service_Request__c
                                        where SR_Template__r.Menu__c = 'Employee Services' AND SAP_Unique_No__c = null AND External_Status_Name__c != 'Draft' AND External_Status_Name__c != 'Submitted' AND External_Status_Name__c != 'Rejected' AND External_Status_Name__c != 'Cancelled' AND External_Status_Name__c != 'Closed'  
                                        AND Is_Rejected__c = false AND isClosedStatus__c = false
                                        AND ID IN (select SR__c from Step__c where step_name__c ='Front Desk Review' and Closed_Date_Time__c <=:dTime AND Closed_Date__c = TODAY AND Sys_SR_Group__c= 'GS' AND (Step_Status__c = 'Verified' OR Step_Status__c='Approved')) order by Name ])
                                        {
                    lstFailedSRs.add(objSR);
            }
        }
        return lstFailedSRs;
    }
    
    global void execute(Database.BatchableContext BC, list<Service_Request__c> lstSRs){
        Service_Request__c objSR;
        try{
            for(Service_Request__c obj : [select Id,Internal_Status_Name__c,SR_Template__r.Menu__c,Contact__c,Sponsor__c,SAP_Unique_No__c,SR_Template__r.Company_related__c,
                                        (select Id,BP_No__c from Employee__r where RecordType.DeveloperName='GS_Contact'), 
                                        (select Id from Steps_SR__r where Step_Status__c = 'Verified' OR Step_Status__c='Approved')
                                        from Service_Request__c where Id=:lstSRs[0].Id]){
                if(obj.SAP_Unique_No__c != null)
                    return;
                else
                    objSR = obj;
            }
            if(objSR != null){
                system.debug('SR Id : '+objSR.Id);
                if(objSR.Employee__r != null && objSR.Employee__r.size() > 0 && objSR.Steps_SR__r != null && objSR.Steps_SR__r.size() > 0){
                    GsSapWebServiceDetails.ContactServiceCallCommon(objSR.Employee__r[0].Id, objSR.Id, objSR.Steps_SR__r[0].Id);
                }else if(objSR.Steps_SR__r != null && objSR.Steps_SR__r.size() > 0){
                    GsPricingPushCC.PushToSAPNormal(objSR.Id, objSR.Steps_SR__r[0].Id);
                //}else if(objSR.Record_Type_Name__c == 'Over_Stay_Fine' || objSR.Record_Type_Name__c == 'Upgrade_to_VIP_after_Medical_Examination'){
                    //GsPricingPushCC.PushToSAPNormal(objSR.Id, objSR.Steps_SR__r[0].Id);
                }
            }
        }catch(Exception ex){
            Log__c objLog = new Log__c();
            objLog.Type__c = 'Schedule GS Web Service Batch Process';
            objLog.Description__c = 'SR ID : '+lstSRs+'\nExceptio is : '+ex.getMessage()+'\nLine # '+ex.getLineNumber();
            insert objLog;
        }
    }
    
    global void finish(Database.BatchableContext BC){
     
        GsScheduleFailedWebServices objSchedule = new GsScheduleFailedWebServices();
        Datetime dt = system.now().addMinutes(5);
        string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();
        for(CronTrigger obj : [select Id from CronTrigger where CronJobDetailId IN (SELECT Id FROM CronJobDetail where Name = 'Gs ScheduleFailedWebServices')])
        system.abortJob(obj.Id);
        system.schedule('Gs ScheduleFailedWebServices', sCornExp, objSchedule);
       
     
    }
    
    
    
  
}