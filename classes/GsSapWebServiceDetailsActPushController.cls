public without sharing class GsSapWebServiceDetailsActPushController {

 Private static string decryptedSAPPassWrd = test.isrunningTest() == false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) : '123456';
  public static string PushActivityToSAPNew(list < string > lstStepIds, string GDRFAno, string TraxNo) {
            string result = 'Success';
            
            try {
                SAPGSWebServices.ZSF_S_GS_ACTIVITY item;
                list < SAPGSWebServices.ZSF_S_GS_ACTIVITY > items = new list < SAPGSWebServices.ZSF_S_GS_ACTIVITY > ();
                map < string, string > mapStepSRIds = new map < string, string > ();
                //Service_Request__c.LastModifiedBy
                for (Step__c objStep: [select Id, Name, SAP_Seq__c, Step_Status__c, OwnerId, Owner.Name, LastModifiedBy.SAP_User_Id__c, Biometrics_Required__c,
                        SAP_AMAIL__c, SAP_BANFN__c, SAP_BIOMT__c, SAP_BNFPO__c, SAP_CMAIL__c, SAP_CRMAC__c, SAP_EACTV__c, SAP_EMAIL__c, SAP_ERDAT__c, SAP_ERNAM__c,
                        SAP_ERTIM__c, SAP_ETIME__c, SAP_FINES__c, SAP_FMAIL__c, SAP_GSDAT__c, SAP_GSTIM__c, SAP_IDDES__c, SAP_IDNUM__c, DNRD_ID_Number__c,
                        SAP_IDTYP__c, SAP_MDATE__c, SAP_MEDIC__c, SAP_MEDRT__c, SAP_MEDST__c, SAP_MFDAT__c, SAP_MFSTM__c, SAP_NMAIL__c, SAP_POSNR__c, SAP_RESCH__c,
                        SAP_RMATN__c, SAP_STIME__c, SAP_TMAIL__c, SAP_UNQNO__c, SAP_ACRES__c, ID_End_Date__c, ID_Start_Date__c, SAP_MATNR__c,
                        SR__c, SR__r.SAP_Unique_No__c, SR__r.SAP_PR_No__c, SR__r.SAP_SGUID__c, SR__r.SAP_MATNR__c, SR__r.Is_Pending__c,
                        SR__r.SAP_UMATN__c, SR__r.SAP_OSGUID__c, SR__r.Name, DNRD_Receipt_No__c, SAP_OSGUID__c, SAP_UMATN__c,
                        Step_Template__c, Step_Template__r.Name, Step_Id__c, No_PR__c,
                        SAP_DOCDL__c, SAP_DOCRC__c, SAP_DOCCO__c, SAP_DOCCR__c, SAP_CTOTL__c, SAP_CTEXT__c,
                        (select Id, Name, Comments__c, Start_Date__c, End_Date__c, CreatedBy.SAP_User_Id__c, LastModifiedBy.SAP_User_Id__c from Pendings__r order by LastModifiedDate desc limit 1)
                        from Step__c where Id IN: lstStepIds
                    ]) {
    
                    item = GsSapWebServiceDetails.PrepareActivity(objStep);
                    if (objStep.No_PR__c)
                        item.PRDEL = 'X';
                    if (objStep.Pendings__r != null) {
                        for (Pending__c objP: objStep.Pendings__r) {
                            item.PEDAT = objP.Start_Date__c != null ? GsPendingInfo.SAPDateFormat(objP.Start_Date__c) : '';
                            item.PEEND = objP.End_Date__c != null ? GsPendingInfo.SAPDateFormat(objP.End_Date__c) : '';
                            item.PSTIM = objP.Start_Date__c != null ? GsPendingInfo.SAPTimeFormat(objP.Start_Date__c) : '';
                            item.PETIM = objP.End_Date__c != null ? GsPendingInfo.SAPTimeFormat(objP.End_Date__c) : '';
                            item.PSUSR = objP.CreatedBy.SAP_User_Id__c;
                            item.PEUSR = objP.LastModifiedBy.SAP_User_Id__c;
                            item.PRMRK = objP.Comments__c;
                            item.PESTR = 'X';
                            item.PEEND = 'X';
                        }
                    }
                   
                    
                    if(TraxNo!=null || TraxNo !='' || GDRFAno != null || GDRFAno !='')
                        item.PRMRK = GDRFAno +','+ TraxNo;
                    
                    //added by Ravi for GS Courier Process
                    item.DOCCO = objStep.SAP_DOCCO__c;
                    item.DOCCR = objStep.SAP_DOCCR__c;
                    item.DOCDL = objStep.SAP_DOCDL__c;
                    item.DOCRC = objStep.SAP_DOCRC__c;
                    item.CTOTL = objStep.SAP_CTOTL__c;
                    item.CTEXT = objStep.SAP_CTEXT__c;
    
                    items.add(item);
                    system.debug('request $$$ : ' + items);
    
                    mapStepSRIds.put(objStep.SR__r.SAP_Unique_No__c, objStep.SR__c);
                    mapStepSRIds.put(item.SGUID, objStep.Id);
                    GsSapWebServiceDetails.sOSGID = objStep.SR__r.SAP_OSGUID__c;
                }
                if (items != null && !items.isEmpty()) {
                    SAPGSWebServices.ZSF_TT_GS_ACTIVITY objActivityData = new SAPGSWebServices.ZSF_TT_GS_ACTIVITY();
                    objActivityData.item = items;
    
                    SAPGSWebServices.serv_actv objSFData = new SAPGSWebServices.serv_actv();
                    objSFData.timeout_x = 120000;
                    objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
                    objSFData.inputHttpHeaders_x = new Map < String, String > ();
                    //  Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c); Commented for V1.7
                    Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c + ':' + GsSapWebServiceDetailsActPushController.decryptedSAPPassWrd); // Added to V1.7
                    String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                    objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
    
                    system.debug('request $$$ : ' + objActivityData);
    
                    SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element response = objSFData.Z_SF_GS_ACTIVITY(objActivityData, null);
    
                    list < Step__c > lstStepUpdate = new list < Step__c > ();
                    list < Log__c > lstLogs = new list < Log__c > ();
    
                    if (response.EX_GS_SERV_OUT != null && response.EX_GS_SERV_OUT.item != null) {
                        for (SAPGSWebServices.ZSF_S_GS_SERV_OP objStepRes: response.EX_GS_SERV_OUT.item) {
    
                            if (objStepRes.ACCTP == 'P') {
                                if (objStepRes.ACTIN == 'COM') {
                                    Step__c objStep = new Step__c(Id = mapStepSRIds.get(objStepRes.SGUID));
                                    objStep.Push_to_SAP__c = true;
                                    if (GsSapWebServiceDetails.IsAutoClosure == true && GsSapWebServiceDetails.StatusId != null)
                                        objStep.Status__c = GsSapWebServiceDetails.StatusId;
                                    lstStepUpdate.add(objStep);
                                }
                            } else { result = objStepRes.SFMSG; lstLogs.add(new log__c( Type__c = 'Activity Update Process for ' + objStepRes.SGUID, Description__c = objStepRes.SFMSG ));
                                //Log__c objLog = new Log__c();
                                //objLog.Type__c = 'Activity Update Process for ' + objStepRes.SGUID;
                                //objLog.Description__c = objStepRes.SFMSG;
                                //lstLogs.add(objLog);
                            }
                        }
                    }
                    Savepoint sp = Database.setSavepoint();
                    try {
                        system.debug('response.EX_GS_NEXT_ACT.item : ' + response.EX_GS_NEXT_ACT.item );
    
                        if (response != null && response.EX_GS_NEXT_ACT != null && response.EX_GS_NEXT_ACT.item != null) {
                            list < Step__c > lstSteps = GsSapWebServiceDetails.ProcessSteps(response.EX_GS_NEXT_ACT.item, null, mapStepSRIds);
                            if (lstSteps != null && !lstSteps.isEmpty()) {
                                upsert lstSteps Step_Id__c; //insert lstSteps;
                                string SRId;
                                for (Step__c objStep: lstSteps)
                                    SRId = objStep.SR__c;
                                /* Commented as per Kaavya, SR Status will change when user change the Step Status
                                if(SRId != null && GsSapWebServiceDetails.SRStatusId != null){
                                    Service_Request__c objReq = new Service_Request__c(Id=SRId);
                                    objReq.Internal_SR_Status__c = GsSapWebServiceDetails.SRStatusId;
                                    objReq.External_SR_Status__c = GsSapWebServiceDetails.SRStatusId;
                                    update objReq;
                                }*/
                            }
                        }
                        if (!lstStepUpdate.isEmpty())
                            update lstStepUpdate;
                    } catch (exception ex) { lstLogs.add(new log__C( Type__c = 'Activity Update Process - Unexpected Error', Description__c = ex.getMessage() )); Database.rollback(sp); return ex.getMessage();
                        /*Log__c objLog = new Log__c();
                        //objLog.Type__c = 'Activity Update Process - Unexpected Error';
                        //objLog.Description__c = ex.getMessage();
                        //lstLogs.add(objLog);*/
                        //Database.rollback(sp); 
                        //return ex.getMessage();
                    }
                    if (!lstLogs.isEmpty()) insert lstLogs;
                }
            } catch (Exception ex) { insert new Log__c( Type__c = 'Activity Update Process - ', Description__c = ex.getMessage() + '\nLine# ' + ex.getLineNumber() + '\n\nSteps Ids : ' + lstStepIds ); return ex.getMessage();
                /*Log__c objLog = new Log__c();
                objLog.Type__c = 'Activity Update Process - ';
                objLog.Description__c = ex.getMessage() + '\nLine# ' + ex.getLineNumber() + '\n\nSteps Ids : ' + lstStepIds;
                insert objLog;*/
                //return ex.getMessage();
            }
            return result;
        }
    


}