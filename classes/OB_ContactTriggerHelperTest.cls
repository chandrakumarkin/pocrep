/******************************************************************************************
 *  Name        : OB_ContactTriggerHelper 
 *  Author      : Rajil Ravindran
 *  Description : Test Class for OB_ContactTriggerHelper
*******************************************************************************************/
@isTest
public class OB_ContactTriggerHelperTest {
    static testMethod void unitTest() {
        Account portalAccount = new Account(
                Name = 'portalAccount'
        );
        Contact portalContact = new Contact(
                FirstName = 'portalContactFirst',
                Lastname = 'portalContactLast',
                AccountId = portalAccount.Id,
                Email = 'portalContact' + System.currentTimeMillis() + '@test.com'
        );
        insert portalContact;
        list<Contact> lstContact = new list<Contact>();
        lstContact.add(portalContact);
        set<string> setContactIds = new set<string>();
        setContactIds.add(portalContact.Id);
        OB_ContactTriggerHelper.CreateRelationship(lstContact);
        OB_ContactTriggerHelper.CreatePortalUser(setContactIds);
    }
     static testMethod void unitTest2() {
        Account portalAccount = new Account(
                Name = 'portalAccount'
        );
        Contact portalContact = new Contact(
                FirstName = 'portalContactFirst',
                Lastname = 'portalContactLast',
                AccountId = portalAccount.Id,
                Email = 'portalContact' + System.currentTimeMillis() + '@test.com'
        );
        insert portalContact;
         ID ROCrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Contact').getRecordTypeId();
         portalContact.RecordTypeId = ROCrectypeid;
         update portalContact;
        list<Contact> lstContact = new list<Contact>();
        lstContact.add(portalContact);
        set<string> setContactIds = new set<string>();
        setContactIds.add(portalContact.Id);
        OB_ContactTriggerHelper.CreateRelationship(lstContact);
        OB_ContactTriggerHelper.CreatePortalUser(setContactIds);
    }
     static testMethod void unitTest3() {
        Account portalAccount = new Account(
                Name = 'portalAccount'
        );
        Contact portalContact = new Contact(
                FirstName = 'portalContactFirst',
                Lastname = 'portalContactLast',
                AccountId = portalAccount.Id,
                Email = 'portalContact' + System.currentTimeMillis() + '@test.com',
            Send_Portal_Login_Link__c = 'Yes'
        );
        insert portalContact;
         ID GSrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
         portalContact.RecordTypeId = GSrectypeid;
         update portalContact;
        list<Contact> lstContact = new list<Contact>();
        lstContact.add(portalContact);
        set<string> setContactIds = new set<string>();
        setContactIds.add(portalContact.Id);
        OB_ContactTriggerHelper.CreateRelationship(lstContact);
        OB_ContactTriggerHelper.CreatePortalUser(setContactIds);
         OB_ContactTriggerHelper.UpdateAccountContactRelation(lstContact);
         OB_ContactTriggerHelper.CreateACR(new map<string,string>{portalContact.id => portalAccount.Id});
    }
    
    

}