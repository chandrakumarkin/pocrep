/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestGSRelationShipCreation {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
       /* list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
		Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        list<Relationship_Codes__c> lstRelCodes = new list<Relationship_Codes__c>();
        Relationship_Codes__c objRel = new Relationship_Codes__c();
        objRel.Name = 'GS Protal Contact';
        objRel.Code__c = 'Z1234';
        objRel.SAP_Relationship_Name__c = 'GS Portal Contact';
        lstRelCodes.add(objRel);
        //insert lstRelCodes;
        objRel = new Relationship_Codes__c();
        objRel.Name = 'New';
        objRel.Code__c = 'Z1235';
        objRel.SAP_Relationship_Name__c = 'New';
        lstRelCodes.add(objRel);
        insert lstRelCodes;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Contact objCon = new Contact();
        objCOn.AccountId = objAccount.Id;
        objCOn.FirstName = 'Test';
        objCOn.LastName = '123456';
        objCOn.Country__c = 'India';
        insert objCOn;
		
		Contact objConNew = new Contact();
        objConNew.AccountId = objAccount.Id;
        objConNew.FirstName = 'Test';
        objConNew.LastName = '123456';
        objConNew.Country__c = 'India';
        objConNew.BP_No__c = '0123456';
        insert objConNew;
		    	
    	GsHelperCls.AlreadyFired = false;
    	
    	Relationship__c objRelationShip = new Relationship__c();
    	objRelationShip.Subject_Account__c = objAccount.Id;
    	objRelationShip.Object_Contact__c = objCon.Id;
    	objRelationShip.Relationship_Group__c = 'GS';
    	objRelationShip.Start_Date__c = system.today();
    	objRelationShip.Relationship_Type__c = 'GS Portal Contact';
    	insert objRelationShip;
    	
    	GsHelperCls.AlreadyFired = false;
    	
    	Relationship__c objRelationShipNew = new Relationship__c();
    	objRelationShipNew.Subject_Account__c = objAccount.Id;
    	objRelationShipNew.Object_Contact__c = objConNew.Id;
    	objRelationShipNew.Relationship_Group__c = 'GS';
    	objRelationShipNew.Start_Date__c = system.today();
    	objRelationShipNew.Relationship_Type__c = 'New';
    	insert objRelationShipNew;
    	
    	GsHelperCls.AlreadyFired = false;
    	
    	Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
    	
    	list<GsSAPCRMService.ZSF_IN_LOG> lstCRMResponse = new list<GsSAPCRMService.ZSF_IN_LOG>();
        GsSAPCRMService.ZSF_IN_LOG objOut = new GsSAPCRMService.ZSF_IN_LOG();
        objOut.ACTION = 'P';
        objOut.SFREFNO = objCon.Id;
        objOut.SFGUID = objCon.Id;
        objOut.PARTNER1 = '001234';
        objOut.PARTNER2 = '001239';
        objOut.MSG = 'Partner Created Successfully';
        objOut.STATUS = 'P';
		lstCRMResponse.add(objOut);
    	objOut = new GsSAPCRMService.ZSF_IN_LOG();
        objOut.ACTION = 'N';
        objOut.STATUS = 'S';
        objOut.SFREFNO = objRelationShip.Id;
        objOut.SFGUID = objRelationShip.Id;
        objOut.PARTNER1 = '001234';
        //objOut.PARTNER2 = '001239';
        objOut.STATUS = 'N';
        objOut.MSG = 'Partner Created Successfully';
		lstCRMResponse.add(objOut);
    	
    	TestGsCRMServiceMock.lstCRMResponse = lstCRMResponse;
    	
    	objRelationShip.End_Date__c = system.today().addYears(1);
    	update objRelationShip;
    	
    	lstCRMResponse = new list<GsSAPCRMService.ZSF_IN_LOG>();
        objOut = new GsSAPCRMService.ZSF_IN_LOG();
        objOut.ACTION = 'P';
        objOut.SFREFNO = objCon.Id;
        objOut.SFGUID = objCon.Id;
        objOut.PARTNER1 = '001234';
        objOut.PARTNER2 = '001239';
        objOut.MSG = 'Partner Created Successfully';
		lstCRMResponse.add(objOut);
    	objOut = new GsSAPCRMService.ZSF_IN_LOG();
        objOut.ACTION = 'N';
        objOut.STATUS = 'S';
        objOut.SFREFNO = objRelationShip.Id;
        objOut.SFGUID = objRelationShip.Id;
        objOut.PARTNER1 = '001234';
        //objOut.PARTNER2 = '001239';
        objOut.MSG = 'Partner Created Successfully';
		lstCRMResponse.add(objOut);
    	
    	TestGsCRMServiceMock.lstCRMResponse = lstCRMResponse;
    	GsHelperCls.AlreadyFired = false;
    	objRelationShipNew.End_Date__c = system.today().addYears(1);
    	update objRelationShipNew;
    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
		Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        Relationship_Codes__c objRel = new Relationship_Codes__c();
        objRel.Name = 'GS Protal Contact';
        objRel.Code__c = 'Z1234';
        objRel.SAP_Relationship_Name__c = 'GS Portal Contact';
        insert objRel;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
		Contact objConNew = new Contact();
        objConNew.AccountId = objAccount.Id;
        objConNew.FirstName = 'Test';
        objConNew.LastName = '123456';
        objConNew.Country__c = 'India';
        objConNew.BP_No__c = '0123456';
        insert objConNew;
		    	
    	GsHelperCls.AlreadyFired = false;
		
		Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
		    	
    	Relationship__c objRelationShipNew = new Relationship__c();
    	objRelationShipNew.Subject_Account__c = objAccount.Id;
    	objRelationShipNew.Object_Contact__c = objConNew.Id;
    	objRelationShipNew.Relationship_Group__c = 'GS';
    	objRelationShipNew.Start_Date__c = system.today();
    	objRelationShipNew.Relationship_Type__c = 'GS Portal Contact';
    	insert objRelationShipNew;
    	
    	GsHelperCls.AlreadyFired = false;
    	
    	list<GsSAPCRMService.ZSF_IN_LOG> lstCRMResponse = new list<GsSAPCRMService.ZSF_IN_LOG>();
        GsSAPCRMService.ZSF_IN_LOG objOut = new GsSAPCRMService.ZSF_IN_LOG();
        objOut.ACTION = 'P';
        objOut.STATUS = 'S';
        objOut.SFREFNO = objRelationShipNew.Id;
        objOut.SFGUID = objRelationShipNew.Id;
        objOut.PARTNER1 = '001234';
        objOut.PARTNER2 = '001239';
        objOut.MSG = 'Partner Created Successfully';
		lstCRMResponse.add(objOut);
    	
    	TestGsCRMServiceMock.lstCRMResponse = lstCRMResponse;
    	
    	//objRelationShipNew.End_Date__c = system.today().addYears(1);
    	//update objRelationShipNew;
    	GsRelationshipCreation.CreateRelationship(new list<string>{objRelationShipNew.Id});
 		*/
 		GsRelationshipCreation obj = new GsRelationshipCreation();
    }
}