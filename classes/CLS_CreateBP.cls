/******************************************************************************************
 *  Name        : CLS_CreateBP 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2016-05-30
 *  Description : This class handle transactions for creating BPs
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   30-05-2016   Claude        Created
 V1.1   06-10-2016   Claude        Added logic to update sector lead lookup for accounts
 V1.2   06-07-2017   Claude        Added logic to prevent existing Account's from being sent to SAP again
 V1.3   12-02-2020   Sai           Commented Account BP Generation Logic as we are handling this on Account
*******************************************************************************************/
global without sharing class CLS_CreateBP extends CLS_LeadUtils {
    
    private Set<String> bpNumberSet;
    private List<User> sectorLeadUsers;
    private List<Relationship__c> lstRels;
    
    private Map<String,Id> sectordLeadUserMap;
    private Map<String,String> mapAccountIdsBP;
    
    private Map<Id,Account> listOfNewAccts;
    private Map<Id,Opportunity> convertedOppMap;
    private Map<String,Sector_Lead_Mapping__c> sectorLeadMap;
    
    /* V1.1 - Claude */
    /**
     * This will store the active sector leads
     * assigned for each account
     */
    private static Map<String,String> accountBdSectorLeadMap;
    
    public CLS_CreateBP(){}
    
    /**
     * Checks each lead using custom validations
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    public override void validateLeads(){
        
        Set<String> companyNames = new Set<String>();
        Set<String> companyIds = new Set<String>();
        
        for(Lead convLead : newLeads){
             
            if(String.isBlank(convLead.Lead_RecType__c) || !convLead.Lead_RecType__c.equals('Event')){
                    
                if(String.isBlank(listOfNewAccts.get(convLead.ConvertedAccountId).Company_Type__c)){
                    setErrorMessage(convLead,'Account Company Type is required.');
                } else if(!String.valueOf(convLead.Company_Type__c).equals(listOfNewAccts.get(convLead.ConvertedAccountId).Company_Type__c)){
                    setErrorMessage(convLead,'Company type of the lead doesn\'t match with the company type of the selected account');
                } 
            }
            // TODO: Add your validation after this line, then use the setErrorMessage() method to add your messages
        }
        
        validateAccountName(newLeads);
    }
    
    private void validateAccountName(List<Lead> convertedLeads){
        
        /* V1.0 Custom validation for lead company names - Start */
        
        Set<String> companyNameSet = new Set<String>();
        Set<Id> accountIds = new Set<Id>();
        
        // Get the lead name set
        for(Lead l : convertedLeads){
            companyNameSet.add(l.Company);
            
            if(String.isNotBlank(l.ConvertedAccountId)){
                accountIDs.add(l.ConvertedAccountId);   
            }
            
        }
        
        // query the existing accounts
        List<Account> existingAccounts = [SELECT Id, Name FROM Account WHERE Name in :companyNameSet AND ID NOT in :accountIds AND ROC_Status__c != ''];
        
        if(!existingAccounts.isEmpty()){
            
            Map<String,Integer> existingAcctsCtr = new Map<String,Integer>();
            
            // Count each instance of the name
            for(Account a : existingAccounts){
                
                Integer nameCount = existingAcctsCtr.get(a.Name) == null ? 0 : existingAcctsCtr.get(a.Name);
                
                nameCount++; // Increment the occurrence 
                
                existingAcctsCtr.put(a.Name,NameCount);
                
            }
            
            // Check each lead for the name validation
            for(Lead l : convertedLeads){
                
               
                
                if(String.isNotBlank(l.Company) && existingAcctsCtr.get(l.Company) >= 1 && l.isConverted){
                    setErrorMessage(l,'This company name already exists.');
                }
            }
        }
        
        /* V1.0 Custom validation for lead company names - End */
        
    }
    
    /**
     * Contains methods that will process the lead values
     */
    public override void processLeads(){
        createBP();
    }
    
    /**
     * Contains methods that will execute after the lead
     * values have been processed
     */
    public override void executeTransactions(){
        
        updateAccountSectorLeads();
        
        createFirstOppHistory();
        
        reParentLeads();
        
        pushRelationshipsToSap();
        
      //  createLicenseActivities();
        
        updateClientOppEmail();
    }
    
    
    void createLicenseActivities(){
        
        Map<Id,Set<String>> acctActivityMap = new Map<Id,Set<String>>();
        
        for(Lead a : newLeadMap.values()){
            
            if(String.isNotBlank(a.Activities__c)){
                
                acctActivityMap.put(a.ConvertedAccountId,new Set<String>(a.Activities__c.split(';')));
                
            }
            
        }
        
        if(!acctActivityMap.isEmpty()) CRM_cls_AccountHandler.createLicenseActivities(acctActivityMap,listOfNewAccts);
        
    }
    webservice static void createLicenseActivitiesFromOppty(string opptyID ,string activities){
        
        Map<ID,Account> mapOfAccount = new Map<ID,Account>();
        Map<ID,Set<string>> ActivityMap = new Map<ID,Set<string>>();
        
        
        Opportunity opty = [Select ID, AccountID,Sector_Classification__c from Opportunity
                            where ID =: opptyID];
        
        Account iAcc =  [Select ID,Sector_Classification__c ,Company_Type__c FROM Account where ID =: opty.AccountID];
      
        
        if(iAcc.Sector_Classification__c == ''  || iAcc.Sector_Classification__c == NULL){
            
            iAcc.Sector_Classification__c = opty.Sector_Classification__c;
           // update iAcc;
        }
        
            
        mapOfAccount.put(iAcc.ID,iAcc);
           
        if(String.isNotBlank(activities)){
            
            ActivityMap.put(opty.accountID,new Set<String>(activities.split(';')));
            
        }
        
     
        if(!ActivityMap.isEmpty()) CRM_cls_AccountHandler.createLicenseActivities(ActivityMap ,mapOfAccount );
        
    }

    void updateClientOppEmail(){
        
        List<Opportunity> oppsToUpdate = new List<Opportunity>();
        
        for(Lead a : newLeadMap.values()){
            
            if(String.isNotBlank(a.Email)){
                
                oppsToUpdate.add(new Opportunity(Id = a.ConvertedOpportunityId, Lead_Email__c = a.Email, Lead_First_Name__c = String.isNotBlank(a.FirstName) ? a.FirstName : '', Lead_Last_Name__c = a.LastName));
                
            }
            
        }
        
        if(!oppsToUpdate.isEmpty()) update oppsToUpdate;
        
    }
    
    /**
     * Creates a new relationship with type 'Has the Employee Responsible' whenever a lead
     * is converted
     * @author      Kaavya Raghuram
     * @date        2016-05-30
     */
    void createBP(){
        
        /* Only execute if there are any leads at all */
        if(!newLeads.isEmpty()){
            
            setBpMap();
            
            listOfNewAccts = getRelatedAccounts(mapAccountIdsBP.keySet()); // query the account records
            
            /* If there are any accounts, execute */
            if(!listOfNewAccts.isEmpty() || test.isRunningTest()){
                
                setSectorLeadMap();  // Get all the sector lead mapping records from the custom setting
               
                if(isSectorLeadMapSet()){
                    
                    validateLeads();                        // validate each lead before proceeding
                    setSectorLeadUserMap();                 // set the sector lead map
                    
                    lstRels = new list<Relationship__c>();  // initialize relationship list
                    
                    for(Lead convLead : newLeads){
                        
                        if(String.isBlank(listOfNewAccts.get(convLead.ConvertedAccountId).BP_No__c)){
                            
                            if(ConvLead.Lead_RecType__c!='Event'){
                                
                                if(listOfNewAccts.get(convLead.ConvertedAccountId) != null){
                                
                                    if(sectorLeadMap != null &&
                                        String.isNotBlank(listOfNewAccts.get(convLead.ConvertedAccountId).BD_Sector__c) &&
                                         sectorLeadMap.get(listOfNewAccts.get(convLead.ConvertedAccountId).BD_Sector__c) != null){
                                        
                                        // Get the BP Number
                                        String sectorLeadBPNumber = 
                                            sectorLeadMap.get(listOfNewAccts.get(convLead.ConvertedAccountId).BD_Sector__c).User_BP__c;
                                        
                                        if(String.isNotBlank(sectorLeadBPNumber)){
                                            
                                            /*
                                             * Create a relationship for each new account 
                                             */
                                            if(String.isNotBlank(sectordLeadUserMap.get(sectorLeadBPNumber))){
                                                
                                                Relationship__c SecL= new Relationship__c();
                                                SecL.Subject_Account__c=ConvLead.ConvertedAccountId;
                                                Secl.User__c = sectordLeadUserMap.get(sectorLeadBPNumber); 
                                                SecL.Active__c = true;
                                                SecL.Relationship_Type__c = 'Has the Employee Responsible';                            
                                                SecL.Start_Date__c = system.today();
                                                SecL.Push_to_SAP__c=true;
                                                SecL.Is_Employee_Relationship__c = true;
                                                lstRels.add(SecL);
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }   
                    }
                }
            }
        }
    }
    
    /**
     * Updates the sector lead field for each new account
     * @author      Claude Manahan
     * @date        2016-30-05 
     */
    void updateAccountSectorLeads(){
        
        for(Account a : listOfNewAccts.values()){
        
            if(isSectorLeadMapSet()){
                
                if(sectorLeadMap.containsKey(a.BD_Sector__c) && 
                    String.isNotBlank(a.BD_Sector__c) &&
                    String.isNotBlank(sectorLeadMap.get(a.BD_Sector__c).User_BP__c)){
                    
                    String bpNumber = sectorLeadMap.get(a.BD_Sector__c).User_BP__c; // Get the BP Number
                
                    /* Get the User based on the BP Number */
                    if(String.isNotBlank(bpNumber)){
                        a.Sector_Lead_lookup__c = sectordLeadUserMap.get(bpNumber);
                    }
                }
            }
        }
        
        update listOfNewAccts.values();
        
    }
    
    
    /**
     * Creates the first opportunity stage history
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    void createFirstOppHistory(){
        
        Set<String> convertedOppIds = new Set<String>();
        
        for(Lead l : newLeads){
            
            if(String.isNotBlank(l.ConvertedOpportunityId)){
                convertedOppIds.add(l.ConvertedOpportunityId);  
            }
            
        }
        
        if(!convertedOppIds.isEmpty()){
            
            List<Opportunity_Stage_History__c> newOppHistoryList
                     = new List<Opportunity_Stage_History__c>();        // List to store the new stage history records
            
            Map<Id,Opportunity> listOfNewOpps 
                    = getRelatedOpportunties(convertedOppIds);          // get the related opportunities
            if(listOfNewOpps!=null && listOfNewOpps.size()>0){
            for(String oppId : convertedOppIds){
              
                if(listOfNewOpps.get(oppId)!=null && String.isNotBlank(listOfNewOpps.get(oppId).Account.BD_Sector__c) && listOfNewOpps.get(oppId).Oppty_Rec_Type__c!=null && 
                    !listOfNewOpps.get(oppId).Oppty_Rec_Type__c.equals('Retail on Boarding') &&
                    sectorLeadMap != null &&
                    sectorLeadMap.containsKey(listOfNewOpps.get(oppId).Account.BD_Sector__c)){
                    
                    String bpNumber = sectorLeadMap.get(listOfNewOpps.get(oppId).Account.BD_Sector__c).User_BP__c; // Get the BP Number
                
                    /* Get the User based on the BP Number */
                    if(String.isNotBlank(bpNumber)){
                        
                        Opportunity_Stage_History__c newOppStageHistory = new Opportunity_Stage_History__c();
                        
                        newOppStageHistory.Comments__c = 'Opportunity Created';                      // Set default comment
                        newOppStageHistory.Last_Modified_User__c = listOfNewOpps.get(oppId).OwnerId; // get the owner
                        newOppStageHistory.Related_Opportunity__c = oppId;                           // copy the opportunity
                        newOppStageHistory.Sector_Lead__c = sectordLeadUserMap.get(bpNumber);        // get the sector lead of the account
                        newOppStageHistory.Stage__c = listOfNewOpps.get(oppId).StageName;            // get the stage
                        newOppStageHistory.Stage_History_Last_Modified_Date__c = System.Now();       // set the time to NOW()
                        
                        newOppHistoryList.add(newOppStageHistory);
                    }
                    
                }
            }
            }
            /* Create the new stage history records */
            if(!newOppHistoryList.isEmpty()){
                insert newOppHistoryList;
            }
            
        }
        
    }
    
    /**
     * Pushes the new relationships to SAP
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    void pushRelationshipsToSap(){
        
        List<String> lstRelIds = new List<String>();
        Map<String,String> mappedContactIds = new Map<String,String>();
        
        /* Add the new relationship IDs */
        if(lstRels != null && lstRels.size()>0 && !lstRels.isEmpty()){
            
            insert lstRels;
            
            for(Relationship__c rel : lstRels){
                lstRelIds.add(rel.id);
            }
            
        }
        //V1.3
        /* Push the new relationships to SAP */
      /*  if(!mapAccountIdsBP.isEmpty()){
            
            SAPWebServiceDetails.CRMPartnerCall(lstRelIds,null,mapAccountIdsBP);
            
        }*/
        
    }
    
    /**
     * Gets all the Sector Lead Mapping custom settings values
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    public void setSectorLeadMap(){ 
        sectorLeadMap = Sector_Lead_Mapping__c.getAll();                
    }
    
    /**
     * Checks if the custom settings is set
     * @author      Claude Manahan
     * @date        2016-30-05
     * @return      isSectorLeadMapSet      flag that checks if the custom settings have been set
     */
    public Boolean isSectorLeadMapSet(){
        return !sectorLeadMap.isEmpty();
    }
    
    /**
     * Sets the Sector Lead Map
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    public void setSectorLeadUserMap(){
        
        /* Initialize */
        sectordLeadUserMap = new Map<String,Id>();  // Map to store the sector lead users mapped by their BP Number
        //bpNumberMap = new Map<String,Id>();           // Map to store the User ID based on the BP Number
        bpNumberSet = new Set<String>();            // Set to store the bp numbers
        
        /* Get the BP Numbers for querying */
        setBpNumberSet();
        
        if(hasBpNumbers()){
            
            setSectorLeadUserList();
            
            if(hasSectorLeadUsers()){
                
                for(User u : sectorLeadUsers){
                    sectordLeadUserMap.put(u.BP_No__c,u.Id);
                }
                
            }
            
        }
        
    }
    
    /**
     * Sets the Account BP Map
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    void setBpMap(){
        
        mapAccountIdsBP = new map<string,string>(); // Map to store the account IDs which will be pushed to SAP
            
        /* Get the Account ids for each lead */     
        for(Lead l : newLeads){
            
            if(l.IsConverted){
                mapAccountIdsBP.put(l.ConvertedAccountId,'ConvertedAccount');   // add the account IDs
            }
            
        }
        
        //V1.2 - Claude - Start
        
        /* Filter the existing Accounts out of the map */
        for(Account a : [SELECT Id, CreatedDate FROM Account WHERE Id IN :mapAccountIdsBP.keySet()]){
            
            //mapAccountIdsBP.remove(a.Id);
            for(Lead l : newLeads){
                
                if(l.ConvertedDate > a.CreatedDate){
                    mapAccountIdsBP.remove(a.Id);
                }
                
            }
           
        }
        
        //V1.2 - Claude - End
    }
    
    /**
     * Sets the BP Number Set
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    void setBpNumberSet(){
        
        for(Sector_Lead_Mapping__c s : sectorLeadMap.values()){
            
            if(String.isNotBlank(s.User_BP__c)){
                bpNumberSet.add(s.User_BP__c);  
            }
        }   
    }
    
    /**
     * Checks if there are any BP numbers retrieved
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    public Boolean hasBpNumbers(){
        return !bpNumberSet.isEmpty();
    }
    
    /**
     * Sets the Sector Lead User list
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    void setSectorLeadUserList(){
        /* Get the sector lead user records by their BP Numbers */
        sectorLeadUsers = [SELECT Id, BP_No__c FROM User WHERE BP_No__c in :bpNumberSet];
    }
    
    /**
     * Checks if there are any sector lead users within the transaction
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    public Boolean hasSectorLeadUsers(){
        return !sectorLeadUsers.isEmpty();
    }
    
    /**
     * Creates new contact relationships for the related contacts of the lead
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    static List<Relationship__c> getContactRelationships(List<String> accountIds, Set<String> existingContacts){
        
        List<Relationship__c> newRelationShipContacts = new List<Relationship__c>();
        
        List<Contact> newContacts = [SELECT Id, Function__c, AccountId FROM Contact WHERE AccountId in :accountIds and Id NOT in :existingContacts];
        
        if(!newContacts.isEmpty()){
            
            for(Contact c : newContacts){
            
                newRelationShipContacts.add(
                    new Relationship__c(
                        Active__c = true,
                        Function__c = c.Function__c,
                        Object_Contact__c = c.Id,
                        Relationship_Type__C = 'Has Contact Person',
                        Start_Date__c = System.Today(),
                        Subject_Account__c = c.AccountId
                    )
                );
            
            }
        }
        
        return newRelationShipContacts;
        
    }
    
    /**
     * Reparents any related contacts to the lead
     * @author      Claude Manahan
     * @date        2016-30-05
     */
    void reParentLeads(){
        
        /* 
         * NOTE: This method sets the related lead field to blank,
         *       and the account id of each contact based on the converted
         *       account record
         */
        
        // Query the related contacts for each lead  
        List<Contact> relatedLeadContacts = 
            [SELECT Related_Lead__c, AccountId, Id FROM Contact WHERE Related_Lead__c in :newLeadMap.keyset() AND Related_Lead__c != ''];
        
        if(!relatedLeadContacts.isEmpty()){
            
            Map<Id,Id> convertedAccountIdMap = getConvertedAccountIdMap();
            
            if(!convertedAccountIdMap.isEmpty()){
                
                for(Contact c : relatedLeadContacts){
                    
                    c.AccountId = convertedAccountIdMap.get(c.Related_Lead__c);
                    c.Related_Lead__c = null;
                }
            }
            
            update relatedLeadContacts;
            
        } 
        
    }
    
    /**
     * Sets the BD Sector Lead Map for
     * each account
     * @params          accountIds      The list of account record IDs
     */
    public static void setBdUserId(List<Id> accountIDs){
        
        List<Relationship__c> activeBdManager = [SELECT Subject_Account__c,User__c FROM Relationship__C WHERE Relationship_Type__c = 'Has the Employee Responsible' AND Active__c = true AND Subject_Account__c IN :accountIDs ORDER BY Name DESC];
        
        if(!activeBDManager.isEmpty()){
            
            accountBdSectorLeadMap = new Map<String,String>();
            
            for(Relationship__C r : activeBdManager){
                
                if(!accountBdSectorLeadMap.containsKey(r.Subject_Account__c)){
                    accountBdSectorLeadMap.put(r.Subject_Account__c,r.User__c); 
                }
                
            }
            
        }
        
    }
    
    /**
     * Gets the assigned BD Sector Lead, if any
     * 
     */
    public static String getBdUserId(String accountRecordId){
        
       
        return String.isNotBlank(accountRecordId) && 
                accountBdSectorLeadMap.containsKey(accountRecordId) &&
                String.isNotBlank(accountBdSectorLeadMap.get(accountRecordId)) ? accountBdSectorLeadMap.get(accountRecordId) : '';
        
    }
    
    @InvocableMethod(label='Update Account Relationstips' description='Triggers an update for the account relationships')
    public static void updateAccountRelationships(List<Id> listOfAccountIds){
        
        List<Relationship__c> existingRelationships = [SELECT ID, Object_Contact__c FROM Relationship__c WHERE Push_To_Sap__c = true and Partner_1__c != '' AND Partner_2__c != '' and Subject_Account__c in :listOfAccountIds];
        
        Set<String> existingContacts = new Set<String>();
        
        existingContacts.add(''); // Add a dummy value
        
        if(!existingRelationships.isEmpty()){
            // Get all contacts that already has a partner
            for(Relationship__c r : existingRelationships){
                existingContacts.add(r.Object_Contact__c);
            }
        }
        
        List<Relationship__c> relatedRelationships = getContactRelationships(listOfAccountIds,existingContacts);
        
        List<Account> accountList = new List<Account>();
        
        /*
         * V1.1 - Claude 
         * Update the Sector Lead lookup whenever a new
         * lead is assigned.
         */
         
        if(!Sector_Lead_Mapping__c.getAll().isEmpty()){
            
            setBdUserId(listOfAccountIds);  // Retrieve all the active sector leads; it is assumed the flow before this class has already fired
        
            for(Id i : listOfAccountIds){
                
                String sectorLeadId = getBdUserId(i);
                
                if(String.isNotBlank(sectorLeadId)){
                    accountList.add(new Account(Id = i, Sector_Lead_lookup__c = sectorLeadId)); 
                }
            }
            
            
        }
        
        if(!relatedRelationships.isEmpty() || !accountList.isEmpty()){
            ID jobID = System.enqueueJob(new CLS_queueableRelationship(relatedRelationships,accountList));      
        }
        
    }
    
        
}