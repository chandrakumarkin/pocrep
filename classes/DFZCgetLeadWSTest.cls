@isTest
public class DFZCgetLeadWSTest {
    
    @isTest static void testGetLeadData() {
        
       
        Lead ld = new Lead();
        ld.Milestone_ID__c='LD123';
        ld.FirstName='Lead Test';
        ld.LastName='WS New';
        ld.MobilePhone='+971512512512';
        ld.Email='gg@gg.com';
        ld.Preference__c='preference';
        ld.Company='Shelby&Co';
        ld.Your_Enquiry__c='imkmmkmkm';
        ld.Sector_Classification__c='Art Gallery';
        ld.Place_of_Trip__c='trip';
        ld.SubZone__c='subzone';
        ld.gdprConsent__c=true;
        ld.Place_of_Trip__c='placeoftrip';
        ld.Status = 'In Progress';
        insert ld;
        
        
        String reqBody = '[{"leadId":"LD123"}]';
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        System.debug('Base URL: ' + sfdcBaseURL );
        
        RestRequest request = new RestRequest();
        request.requestUri = sfdcBaseURL+'/services/apexrest/api/GetLead/';
        System.debug('ZZZ requestUri-->'+request.requestUri);   
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(reqBody);
        RestContext.request = request;
       
        DFZCgetLeadWS.DFZCCreateResponce resp = DFZCgetLeadWS.doPost();

        
    }
}