/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 11-Dec-2014   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class TestReview_and_Final_Validations {

    public static testmethod void testRFVal() {
        Cls_Review_and_Final_Validations RF = new Cls_Review_and_Final_Validations();
        list<Amendment__c> amendlist = new list<Amendment__c> ();
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_Details_Add_or_Remove_Director','Add_or_Remove_Auditor','Change_Add_or_Remove_Beneficial_or_Ultimate_Owner')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.License_Activity__c = '030';
        objAccount.Authorized_Share_Capital__c = 500;
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Annual_Turnover_less_than_USD_5_million__c = 'No';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        insert objAccount;
        list<Currency__c> currlist = new list<Currency__c>();
        Currency__c curr =new Currency__c(Name='US Dollar',Exchange_Rate__c=1.00,Active__c=true);
        currlist.add(curr);
        insert currlist;
        list<Service_Request__c> srlist = new list<Service_Request__c>();
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
       // objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.legal_structures__c ='LTD SPC';
        objSR.currency_list__c = 'US Dollar';
        objSR.Currency_Rate__c = curr.id;
        objSR.RecordTypeId = mapRecordType.get('Application_of_Registration');
        objSR.Share_Capital_Membership_Interest__c =90;
        objSR.No_of_Authorized_Share_Membership_Int__c = 5;
        srlist.add(objSR);       
        //insert objSR;
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Currency_Rate__c = curr.id;
       // objSR1.Send_SMS_To_Mobile__c = '+971123456789';
        objSR1.legal_structures__c ='LTD';
        objSR1.RecordTypeId = mapRecordType.get('Application_of_Registration');
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c =50000;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 560;
        srlist.add(objSR1);  
        //insert objSR1;
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount.Id;
        objSR2.Currency_Rate__c = curr.id;
       // objSR1.Send_SMS_To_Mobile__c = '+971123456789';
        objSR2.legal_structures__c ='LTD';
        objSR2.RecordTypeId = mapRecordType.get('Add_or_Remove_Auditor');
        objSR2.currency_list__c = 'US Dollar';
        objSR2.Share_Capital_Membership_Interest__c =50000;
        objSR2.No_of_Authorized_Share_Membership_Int__c = 560;
        srlist.add(objSR2);  
        
        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Currency_Rate__c = curr.id;
       // objSR1.Send_SMS_To_Mobile__c = '+971123456789';
        objSR3.legal_structures__c ='LTD IC';
        objSR3.RecordTypeId = mapRecordType.get('Change_Add_or_Remove_Beneficial_or_Ultimate_Owner');
        objSR3.currency_list__c = 'US Dollar';
        objSR3.Share_Capital_Membership_Interest__c =50000;
        objSR3.No_of_Authorized_Share_Membership_Int__c = 560;
        srlist.add(objSR3);  
        
        insert srlist;
        
        objSR  = [Select id,Confirm_Change_of_Trading_Name__c,record_type_name__c,issued_share_capital_in_usd__c,Share_Capital_Membership_Interest__c,Currency_Rate__c,No_of_Authorized_Share_Membership_Int__c,Name from Service_Request__c where id=:objSR.id];
        objSR1 = [Select id,record_type_name__c,Sector_Classification__c,issued_share_capital_in_usd__c,Share_Capital_Membership_Interest__c,Currency_Rate__c,No_of_Authorized_Share_Membership_Int__c,Name from Service_Request__c where id=:objSR1.id];
        objSR2 = [Select id,record_type_name__c,issued_share_capital_in_usd__c,Share_Capital_Membership_Interest__c,Currency_Rate__c,No_of_Authorized_Share_Membership_Int__c,Name,Customer__r.Legal_Type_of_Entity__c,Customer__r.Annual_Turnover_less_than_USD_5_million__c  from Service_Request__c where id=:objSR2.id];
        objSR3 = [Select id,record_type_name__c,issued_share_capital_in_usd__c,Share_Capital_Membership_Interest__c,Currency_Rate__c,No_of_Authorized_Share_Membership_Int__c,Name from Service_Request__c where id=:objSR3.id];

        
        Amendment__c amnd1 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Director',Representation_Authority__c='Separately');
        amendlist.add(amnd1);
        Amendment__c amnd8 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Shareholder',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd8);
        Amendment__c amnd17 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Company Secretary',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd17);
        amnd17 = new Amendment__c (ServiceRequest__c=objSR2.id,Relationship_Type__c='Founder',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd17);
        insert amendlist;
        Cls_Review_and_Final_Validations.Valid_Amendments('Director','LTD IC',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Founding Member','NPIO',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('UBO','LTD',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('UBO','LLC',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Director','LTD',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Director','LTD IC',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Designated Member','RLLP',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Authorized Signatory','LTD',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Auditor','LTD',objSR2,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Company Secretary','LTD',objSR1,amendlist);
        //objSR2.RecordTypeId = mapRecordType.get('Change_Details_Add_or_Remove_Founder');
        Cls_Review_and_Final_Validations.Valid_Amendments('Founder','Foundation',objSR2,amendlist);
        Amendment__c amnd2 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Authorized Signatory',Representation_Authority__c='Jointly');
        amendlist.add(amnd2);
        Amendment__c amnd3 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='General Partner',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd3);
        Amendment__c amnd4 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Founding Member',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd4);
        Amendment__c amnd5 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Shareholder',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd5);
        Amendment__c amnd15 = new Amendment__c (ServiceRequest__c=objSR1.id,Relationship_Type__c='Shareholder',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd15);
        Amendment__c amnd20 = new Amendment__c (ServiceRequest__c=objSR1.id,Relationship_Type__c='Designated Member',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd20);
        upsert amendlist;
        
        Cls_Review_and_Final_Validations.Valid_Amendments('Designated Member','RLLP',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Services Personnel','RLLP',objSR1,amendlist);
        //Cls_Review_and_Final_Validations.Valid_Amendments('Shareholder','LTD PCC',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Director','LTD',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Director','FRC',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Shareholder','LTD SPC',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Limited Partner','LP',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Services Personnel','LTD',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('UBO','NPIO',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Founding Member','NPIO',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Founding Member','NPIO',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('UBO','LLP',objSR3,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('UBO','FRC',objSR3,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('General Partner','GP',objSR,amendlist);
        
        Cls_Review_and_Final_Validations.Valid_Amendments('Manager','LLC',objSR1,amendlist);
                
        Amendment__c amnd6 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Limited Partner',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd6);
        Amendment__c amnd13 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Founding Member',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd13);
        Amendment__c amnd14 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Founding Member',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd14);
        Amendment__c amnd9 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Services Personnel');
        amendlist.add(amnd9);
        Amendment__c amnd10 = new Amendment__c (ServiceRequest__c=objSR3.id,Relationship_Type__c='UBO');
        amendlist.add(amnd10);
        Amendment__c amnd11 = new Amendment__c (ServiceRequest__c=objSR1.id,Relationship_Type__c='Director',Representation_Authority__c='Separately');
        amendlist.add(amnd11);
        Amendment__c amnd7 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Shareholder',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd7);
        Amendment__c amnd12 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Member',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd12);
        Amendment__c amnd16 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='General Partner',Amendment_Type__c='Body Corporate');
        amendlist.add(amnd16);
        Amendment__c amnd18 = new Amendment__c (ServiceRequest__c=objSR3.id,Relationship_Type__c='UBO');
        amendlist.add(amnd18);
        
        Amendment__c amnd19 = new Amendment__c (ServiceRequest__c=objSR3.id,Relationship_Type__c='Council Member');
        amendlist.add(amnd19);
           Amendment__c amnd23 = new Amendment__c (ServiceRequest__c=objSR3.id,Relationship_Type__c='Guardian');
        amendlist.add(amnd23);
           Amendment__c amnd21 = new Amendment__c (ServiceRequest__c=objSR3.id,Relationship_Type__c='Founder');
        amendlist.add(amnd21);
           Amendment__c amnd22 = new Amendment__c (ServiceRequest__c=objSR3.id,Relationship_Type__c='Registered Agent');
        amendlist.add(amnd22);
        
        upsert amendlist;
        
        
        Cls_Review_and_Final_Validations.Valid_Amendments('Member','LLC',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('UBO','LTD',objSR3,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('UBO','RLP',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Services Personnel','RLLP',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Director','LTD',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Authorized Signatory','LLC',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('General Partner','GP',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Founding Member','NPIO',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Shareholder','LTD SPC',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Shareholder','LTD IC',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Shareholder','LTD',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Limited Partner','LP',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Services Personnel','LTD',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('UBO','LLC',objSR1,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('UBO','LTD',objSR3,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('UBO','FRC',objSR,amendlist);
        Cls_Review_and_Final_Validations.Valid_Amendments('Director','FRC',objSR,amendlist);
           Cls_Review_and_Final_Validations.Valid_Amendments_Foundation('Council Member','Foundation',objSR,amendlist);
              Cls_Review_and_Final_Validations.Valid_Amendments_Foundation('Guardian','Foundation',objSR,amendlist);
                 Cls_Review_and_Final_Validations.Valid_Amendments_Foundation('Founder','Foundation',objSR,amendlist);
                    Cls_Review_and_Final_Validations.Valid_Amendments_Foundation('Registered Foundation','Foundation',objSR,amendlist);
        
        
    }

}