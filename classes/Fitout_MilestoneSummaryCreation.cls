/**
 * @description       : 
 * @author            : Mudasir
 * @group             : 
 * @last modified on  : 12-06-2020
 * @last modified by  : Mudasir Wani
 * Functionality 	  : Create a Milestone summary report request and submit if once the Hanover date is reached as per the lease
 * Modifications Log 
 * Ver   Date         Author    Modification
 * 1.0   11-23-2020   Mudasir   Initial Version
 
**/
global with sharing class Fitout_MilestoneSummaryCreation implements Database.Batchable < sObject > , Schedulable ,Database.Stateful{
    final String query = system.label.MilestoneSummaryCreationSOQL;
	String unitsWithoutProjectsSOQL ='';
	public List<Log__c> logList;
    global Database.QueryLocator start(Database.BatchableContext bc) {	
		logList	= new List<Log__c>();
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Lease__c > scope) {
		List<Service_Request__c> hoardingServiceRequests = new List<Service_Request__c>();
		List<Fitout_Milestone_Penalty__c> fitoutMilestonePenalties = new List<Fitout_Milestone_Penalty__c>();
        Set<id> leaseIdSet = new Set<id>();
        Map<id,String> leaseWithAllUnits = new Map<id,String>();
        for(Lease__c leaseRec : (List<Lease__c>) scope)
        {
            leaseIdSet.add(leaseRec.id); 
        }
        if(leaseIdSet.size() > 0){
            leaseWithAllUnits = getAssociatedUnitNumbers(leaseIdSet);
        }
    	for(Lease__c leaseRec : (List<Lease__c>) scope){		  
		   Fitout_Milestone_Penalty__c fitoutMilestonePenalty = new Fitout_Milestone_Penalty__c();
		   fitoutMilestonePenalty.Account__c 							=	leaseRec.Account__c;
		   fitoutMilestonePenalty.Lease__c	 							=	leaseRec.id;
           fitoutMilestonePenalty.Lease_Commencement_Date__c			=	leaseRec.Start_date__c;
           fitoutMilestonePenalty.Unique_Id__c							=	leaseRec.Account__c +''+ leaseRec.id +''+ String.valueOf(leaseRec.Start_date__c.format());
           fitoutMilestonePenalty.Unit_Numbers__c  						=	leaseWithAllUnits.get(leaseRec.id);
		   fitoutMilestonePenalties.add(fitoutMilestonePenalty);	   
		}
		if(fitoutMilestonePenalties != null && fitoutMilestonePenalties.size() > 0)
		{
			Database.SaveResult[] penaltyList =  Database.insert(fitoutMilestonePenalties , false);
			for (Database.SaveResult penaltyRec : penaltyList) {
				if (penaltyRec.isSuccess()) {
				}
				else {
					// Operation failed, so get all errors                
					for(Database.Error err : penaltyRec.getErrors()) {
						logList.add(createLogRecord(err , penaltyRec.getId()));
					}
				}
			}
		}
		
    }
    global void finish(Database.BatchableContext BC) {
		if(logList != null && logList.size() > 0){
			Database.insert(logList);
		}
    }
    global void execute(SchedulableContext ctx) {
        Fitout_MilestoneSummaryCreation bachable = new Fitout_MilestoneSummaryCreation();
        database.executeBatch(bachable);
	}
	public static Map<id,String> getAssociatedUnitNumbers(Set<Id> leaseIdSet){
        Map<id,String> unitsAssociatedWithLeaseMap = new Map<id,String>();
        for(Lease_Partner__c leasePartner : [Select Lease__c,Unit__r.Name from Lease_Partner__c where Lease__c IN : leaseIdSet])
        {
            if(unitsAssociatedWithLeaseMap.keyset().contains(leasePartner.Lease__c)){
                unitsAssociatedWithLeaseMap.put(leasePartner.Lease__c,unitsAssociatedWithLeaseMap.get(leasePartner.Lease__c) +','+leasePartner.Unit__r.Name);
            }
            else{
                unitsAssociatedWithLeaseMap.put(leasePartner.Lease__c,leasePartner.Unit__r.Name);
            }
        }
        return unitsAssociatedWithLeaseMap;
    }
    @testVisible
    private Log__c createLogRecord(Database.Error err , Id recordId){
        Log__c objLog 				= 	new Log__c();
        objLog.Description__c 		=	'Message==>'+err.getMessage() +'---Field resulting in error ---'+ err.getFields();
        objLog.Type__c 				= 	'Fitout Milestone Summary Creation';
        objLog.sObject_Record_Id__c =	recordId;
        return objLog;
    }
}