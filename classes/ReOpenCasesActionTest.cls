/*
    Author      : Kim Noceda -- PWC
    Date        : 05-May-2018
    Description : test class for ReOpenCasesAction
    --------------------------------------------------------------------------------------
*/
@isTest
private class ReOpenCasesActionTest{

    static testmethod void testReOpen1(){
        Case objCase = new Case();
        objCase.Subject = 'test Subject';
        objCase.Origin = 'Email';
        objCase.GN_Department__c = 'Business Centre';
        insert objCase;
        
        Case_SLA__c objSLA = new Case_SLA__c();
        objSLA.Parent__c = objCase.Id;
        objSLA.From__c = System.Now();
        objSLA.Until__c = null;
        insert objSLA;
        
        List<Case_SLA__c> objSLAList = [SELECT Id, Parent__c, From__c, Until__c, Change_Type__c, 
                                        Owner__c, Status__c, Business_Hours_Id__c, Due_Date_Time__c 
                                        FROM Case_SLA__c WHERE Parent__c =: objCase.Id LIMIT 1];
        
        
        List<BusinessHours> busHours = [SELECT Id from BusinessHours WHERE isDefault = true LIMIT 1];
        objSLAList[0].Due_Date_Time__c = System.Now().addHours(2);
        objSLAList[0].Until__c = System.Now().addHours(2);
        objSLAList[0].Business_Hours_Id__c = busHours[0].Id;
        update objSLAList;
        
        List<Id> listcaseid = new List<Id>();
        listcaseid.add(objCase.Id);
        
        ReOpenCasesAction.createSLA(listcaseid);
    }
}