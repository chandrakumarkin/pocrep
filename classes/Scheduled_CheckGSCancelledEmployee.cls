/*************************************************************************************************
 *  Name        :  
 *  Author      : Danish Farooq
 *  Date        : 01-07-2018    
 *  Purpose     : Schd class to check cancelled/renewal employee
 *  TestClass   : CheckGSCancelledEmployee_Test
**************************************************************************************************/


global  class Scheduled_CheckGSCancelledEmployee implements Schedulable {
     
     global void execute(SchedulableContext sc)
     {
    
        database.executeBatch(new CheckGSCancelledEmployee());
    }
}