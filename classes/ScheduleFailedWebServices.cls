/*
    * Created By    : Ravi on 28th Oct,2014
    * Description   : This is a Schedule class which will run daily once/twice and collets the SR Price Items,Receipts,Partners & Relationships and push to SAP.
    
Modification History :
------------------------------------------------------------------------------------------------------------
V No    Date        Modified By     Description
============================================================================================================
V1.1    10/01/2016  Ravi            Added the logic to repush the RORP faild web services
V1.2    11/29/2016  Claude          Added Exemption for FIT-OUT Contractors 

*/
global without sharing class ScheduleFailedWebServices implements Schedulable {
    global void execute(SchedulableContext ctx) {       
        //Process failed Receipts
        Boolean BlockWebServices = false;
        if(Block_Trigger__c.getAll() != null && Block_Trigger__c.getAll().get('Block') != null && Block_Trigger__c.getAll().get('Block').Block_Web_Services__c)
            BlockWebServices = true;
        if(BlockWebServices == false){
            list<string> lstReceiptIds = new list<string>();
            for(Receipt__c objReceipt : [select Id from Receipt__c where Pushed_to_SAP__c = false AND Payment_Status__c = 'Success']){
                lstReceiptIds.add(objReceipt.Id);
            }
            if(!lstReceiptIds.isEmpty()) SAPWebServiceDetails.ECCReceiptCall(lstReceiptIds, false);
            //Process Service calls (Send failed SR Price Items)
            list<string> lstSRIds = new list<string>();
            list<string> lstRefundSRIds = new list<string>();
            list<string> lstSRIdsDP = new list<string>();
            list<string> lstRORPRelIds = new list<string>();
            
            //for(SR_Price_Item__c objSRPItem : [select Id,ServiceRequest__c,ServiceRequest__r.Record_Type_Name__c,Status__c,Company_Code__c from SR_Price_Item__c where (Transaction_Id__c = null OR Transaction_Id__c = '') AND Company_Code__c != '5000' AND ServiceRequest__r.SR_Group__c = 'ROC' AND ServiceRequest__r.Internal_Status_Name__c != 'DRAFT' AND Status__c != 'Invoiced' AND (Status__c = 'Blocked' OR Status__c = 'Consumed') ]){
            for(SR_Price_Item__c objSRPItem : [select Id,ServiceRequest__c,ServiceRequest__r.Record_Type_Name__c,Status__c,Company_Code__c from SR_Price_Item__c where (Transaction_Id__c = null OR Transaction_Id__c = '') AND Company_Code__c !=: System.Label.CompanyCode5000 AND ServiceRequest__r.SR_Group__c = 'ROC' AND ServiceRequest__r.Internal_Status_Name__c != 'DRAFT' AND Status__c != 'Invoiced' AND (Status__c = 'Blocked' OR Status__c = 'Consumed') ]){                
                if(objSRPItem.ServiceRequest__r.Record_Type_Name__c != 'Refund_SR' && objSRPItem.ServiceRequest__r.Record_Type_Name__c != 'Objection_SR'){
                    //if(objSRPItem.Company_Code__c != '1100')
                    if(objSRPItem.Company_Code__c != system.Label.CompanyCode1100) lstSRIds.add(objSRPItem.ServiceRequest__c);
                    // else if(objSRPItem.Company_Code__c == '1100' && objSRPItem.Status__c == 'Consumed')
                    else if(objSRPItem.Company_Code__c == system.Label.CompanyCode1100 && objSRPItem.Status__c == 'Consumed') lstSRIdsDP.add(objSRPItem.ServiceRequest__c);
                        
                }else if(objSRPItem.ServiceRequest__r.Record_Type_Name__c == 'Refund_SR' || objSRPItem.ServiceRequest__r.Record_Type_Name__c == 'Objection_SR') lstRefundSRIds.add(objSRPItem.ServiceRequest__c);
            }
            if(!lstSRIds.isEmpty()) SAPWebServiceDetails.ECCServiceCallForSchedule(lstSRIds, false);
            
            if(!lstRefundSRIds.isEmpty()) FineSRDetailsCls.PushObjectionSRtoSAP(lstRefundSRIds);
            
            if(!lstSRIdsDP.isEmpty()) SAPWebServiceDetails.ECCServiceCallToPushDPFuture(lstSRIdsDP, false);
            
            //Processing of Partners
            map<string,string> mapContactIds = new map<string,string>();
            map<string,string> mapAccountIds = new map<string,string>();
            list<string> lstRelationShipIds = new list<string>();
            
            list<string> lstRelationShipIdUpdates = new list<string>();
            map<string,string> mapRelationShipPartners = new map<string,string>();
            Date dt = Date.newInstance(2015, 4, 29);
            for(Relationship__c objRelationShip : [select Id,Partner_1__c,Partner_2__c,Push_to_SAP__c,Subject_Account__c,Object_Contact__c,Object_Account__c,Relationship_Group__c from Relationship__c where Relationship_Type__c != 'Is Fit Out Contractor' AND Relationship_Type__c != 'Data Controller' AND Partner_1__c != null AND ((Partner_2__c = null OR Partner_2__c = '') OR Push_to_SAP__c=false) AND CreatedDate>=:dt]){ // V1.2 - Claude - Added 'Relationship_Type__c != 'Is Fit Out Contractor' as part of query
                if(objRelationShip.Relationship_Group__c != 'RORP'){
                    if(objRelationShip.Partner_1__c != null && (objRelationShip.Partner_2__c == null || objRelationShip.Partner_2__c == '') && objRelationShip.Push_to_SAP__c == false){
                        if(objRelationShip.Object_Contact__c != null){
                            mapContactIds.put(objRelationShip.Object_Contact__c,objRelationShip.Partner_1__c);
                        }
                        if(objRelationShip.Object_Account__c != null){
                            mapAccountIds.put(objRelationShip.Object_Account__c,objRelationShip.Partner_1__c);
                        }
                        lstRelationShipIds.add(objRelationShip.Id);
                    }else if(objRelationShip.Partner_1__c != null && objRelationShip.Partner_2__c != null && objRelationShip.Push_to_SAP__c == false){
                        lstRelationShipIdUpdates.add(objRelationShip.Id);
                        if(objRelationShip.Object_Contact__c != null){ mapRelationShipPartners.put(objRelationShip.Object_Contact__c,objRelationShip.Partner_2__c); }
                        if(objRelationShip.Object_Account__c != null){ mapRelationShipPartners.put(objRelationShip.Object_Account__c,objRelationShip.Partner_2__c); }
                    }
                }else if(objRelationShip.Push_to_SAP__c == false && objRelationShip.Relationship_Group__c == 'RORP' && objRelationShip.Subject_Account__c != null && objRelationShip.Object_Contact__c != null){
                    lstRORPRelIds.add(objRelationShip.Id);
                }
            }
            
            if(!mapContactIds.isEmpty()){
                map<string,string> mapContactIdsTemp = new map<string,string>();
                for(string key : mapContactIds.keySet()){
                    mapContactIdsTemp.put(key,mapContactIds.get(key));
                    if(mapContactIdsTemp.size() >= 10){
                        SAPWebServiceDetails.CRMPartnerCall(lstRelationShipIds,mapContactIdsTemp,null);
                        mapContactIdsTemp = new map<string,string>();
                    }
                }
                if(!mapContactIdsTemp.isEmpty()) SAPWebServiceDetails.CRMPartnerCall(lstRelationShipIds,mapContactIdsTemp,null);
            }
            if(!mapAccountIds.isEmpty()){
                map<string,string> mapAccountIdsTemp = new map<string,string>();
                for(string key : mapAccountIds.keySet()){
                    mapAccountIdsTemp.put(key,mapAccountIds.get(key));
                    if(mapAccountIdsTemp.size() >= 10){
                        SAPWebServiceDetails.CRMPartnerCall(lstRelationShipIds,null,mapAccountIdsTemp);
                        mapAccountIdsTemp = new map<string,string>();
                    }
                }
                if(!mapAccountIdsTemp.isEmpty()) SAPWebServiceDetails.CRMPartnerCall(lstRelationShipIds,null,mapAccountIdsTemp);
                //SAPWebServiceDetails.CRMPartnerCall(lstRelationShipIds,null,mapAccountIds);
            }
            if(!mapRelationShipPartners.isEmpty()){
                list<string> lstRelationShipIdUpdatesTemp = new list<string>();
                for(String RelId : lstRelationShipIdUpdates){
                    lstRelationShipIdUpdatesTemp.add(RelId);
                    if(lstRelationShipIdUpdatesTemp.size() >= 10){
                        SAPWebServiceDetails.CRMCreateRelationship(lstRelationShipIdUpdatesTemp,mapRelationShipPartners);
                        lstRelationShipIdUpdatesTemp = new list<string>();
                    }   
                }
                if(!lstRelationShipIdUpdatesTemp.isEmpty()) SAPWebServiceDetails.CRMCreateRelationship(lstRelationShipIdUpdatesTemp,mapRelationShipPartners);
                //SAPWebServiceDetails.CRMCreateRelationship(lstRelationShipIdUpdates,mapRelationShipPartners);
            }
            
            /*if(!lstGSRelIds.isEmpty()){
                GsRelationshipCreation.CreateContact(lstGSRelIds);
            }*/
            
            //V1.1
            map<Id,Id> mapRORPContactIds = new map<Id,Id>();
            map<Id,Id> mapRelIds = new map<Id,Id>();
            Datetime dNew = system.now().addHours(-1);
            for(Contact objCon : [select Id,Name,(select Id,ServiceRequest__c from Amendments__r where Should_Push_to_SAP__c = true) from Contact where RecordType.DeveloperName='RORP_Contact' AND BP_No__c = null AND LastModifiedDate <=:dNew ]){
                mapRORPContactIds.put(objCon.Id,null);
                if(objCon.Amendments__r != null){
                    for(Amendment__c objA : objCon.Amendments__r){
                        mapRORPContactIds.put(objCon.Id,objA.ServiceRequest__c);
                    }
                }
            }
            if(!mapRORPContactIds.isEmpty()){
                
            }
        }
    }
}