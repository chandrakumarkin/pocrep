/*****************************************************************************************************************************
    Author      :   Sai Kalyan
    Date        :   02-June-2020
    Description :   Create a Registered Agent Controller
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    02-June-2020   Sai        Intial Version
*****************************************************************************************************************************/
public class RegisteredAgentController {
     
     public Service_Request__c serviceRequest{get;set;}
     public String CustomerId;
     public Amendment__c objAmnd{get;set;}
     public List<Amendment__c> lstAmendIndDetails{get;set;}
     public String selectedAgentValue{get;set;}
     public boolean isDisplayMessage{get;set;}
     public List<Amendment__c> lstAmendFinalDetails;
     public boolean hidepopup{get;set;}
     public boolean isWarning{get;set;}
     public  List<selectoption> lstOptions{get;set;}
     
     public RegisteredAgentController(){
        
        User eachUser = new User();
        serviceRequest = new Service_Request__c();
        objAmnd = new Amendment__c();
        hidepopup = true; 
        isWarning = false;
        lstAmendFinalDetails = new List<Amendment__c>();
        string recordtypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Add or Remove a Registered Agent').getRecordTypeId();
        eachUser = [SELECT Contact.AccountID,Contact.Account.Name,Email,Phone,Contact.Account.Legal_Type_of_Entity__c FROM User where ID=:userinfo.getuserId()];
       system.debug(')))(((*'+eachUser.Contact.AccountID);
         CustomerId = eachUser.Contact.AccountID;
        serviceRequest.Entity_Name__c = eachUser.Contact.Account.Name;
        serviceRequest.Email__c = eachUser.Email;
        serviceRequest.Send_SMS_To_Mobile__c = eachUser.Phone;
        serviceRequest.Customer__c = eachUser.Contact.AccountId;
        serviceRequest.RecordTypeId=recordtypeId;
        serviceRequest.Legal_Structures__c=eachUser.Contact.Account.Legal_Type_of_Entity__c;
     }
     
     public void existingAgents(){
        
            lstAmendIndDetails  =  new List<Amendment__c>();
            system.debug('@@##@@@'+CustomerId);
            string recordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Body Corporate').getRecordTypeId();
            for(Relationship__c eachRelation:[SELECT Subject_Account__c,Sys_Amendment_Id__c,Object_Account__r.Name,Object_Account__r.Apartment_Villa_Office_Number__c,
                               Object_Account__r.Building_Name__c,Object_Account__r.Street__c,Object_Account__r.City__c,Object_Account__r.Country__c,
                               Object_Account__r.Emirate_State__c FROM Relationship__c where Subject_Account__c=:CustomerId AND 
                               Active__c=true AND Relationship_Type__c ='Registered agent']){
                
                objAmnd = new Amendment__c();
                objAmnd.Relationship_Type__c = 'Registered Agent';
                objAmnd.Shareholder_Company__c = eachRelation.Object_Account__c;
                objAmnd.Apt_or_Villa_No__c = eachRelation.Object_Account__r.Apartment_Villa_Office_Number__c;
                objAmnd.Building_Name__c = eachRelation.Object_Account__r.Building_Name__c;
                objAmnd.Street__c = eachRelation.Object_Account__r.Street__c;
                objAmnd.Permanent_Native_City__c = eachRelation.Object_Account__r.City__c;
                objAmnd.Permanent_Native_Country__c = eachRelation.Object_Account__r.Country__c;
                objAmnd.Emirate_State__c = eachRelation.Object_Account__r.Emirate_State__c;
                objAmnd.Company_Name__c = eachRelation.Object_Account__r.Name;
                objAmnd.recordtypeId = recordtypeId;
                objAmnd.Related__c = eachRelation.Sys_Amendment_Id__c;
                objAmnd.sys_create__c = true;
                objAmnd.Customer__c = eachRelation.Subject_Account__c;
                lstAmendIndDetails.add(objAmnd);    
                lstAmendFinalDetails.add(objAmnd);                
            }
            
        lstOptions = new List<Selectoption>();
        lstOptions.add(new SelectOption('-None-','-None-'));
        for(License_Activity__c eachActivity:[SELECT Account__r.Name,Account__c,Activity__r.Name FROM License_Activity__c where 
                                              (Account__r.ROC_Status__c='Active' OR Account__r.ROC_Status__c='Not Renewed') 
                                              AND Account__r.DFSA_Approval__c='Yes' AND Activity__r.Name='Corporate Service Provider']){
            lstOptions.add(new SelectOption(eachActivity.Account__c,eachActivity.Account__r.Name));

        }
     }
     
    public void displayMessage(){
        
        isDisplayMessage = false;
        if(selectedAgentValue !='-None-'){
            isDisplayMessage = true; 
        }
    }
   
    public void removeAgents(){
        
        hidepopup = false;
        isWarning = true;
        if(lstAmendFinalDetails.size() >0){
        	lstAmendFinalDetails[0].Status__c ='Remove';
        }
        lstAmendIndDetails = new List<Amendment__c>();
      
   }
    
    public pagereference SaveRecord(){
        
        try     
        {
        	if(lstAmendFinalDetails.size() == 0 || (lstAmendFinalDetails.size() >0 && lstAmendFinalDetails[0].Status__c == 'Remove')){
            Pagereference pref = null;
                Account accountRecord = new Account();
                string recordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Body Corporate').getRecordTypeId();
                if(String.isNotBlank(selectedAgentValue) && selectedAgentValue !='-None-'){
                    system.debug('&&&&'+selectedAgentValue); 
                    accountRecord = [SELECT Name,Apartment_Villa_Office_Number__c,Phone,Building_Name__c,Place_of_Registration_Text__c,ROC_reg_incorp_Date__c,Street__c,City__c,Country__c,Emirate_State__c
                                     FROM Account where ID=:selectedAgentValue];
                        Amendment__c objAmnd = new Amendment__c();
                        objAmnd.Status__c = 'Active';
                        objAmnd.Relationship_Type__c = 'Registered Agent';
                        objAmnd.Shareholder_Company__c = accountRecord.Id;
                        objAmnd.Apt_or_Villa_No__c = accountRecord.Apartment_Villa_Office_Number__c;
                        objAmnd.Building_Name__c = accountRecord.Building_Name__c;
                        objAmnd.Street__c = accountRecord.Street__c;
                        objAmnd.Permanent_Native_City__c = accountRecord.City__c;
                        objAmnd.Permanent_Native_Country__c = accountRecord.Country__c;
                        objAmnd.Emirate_State__c = accountRecord.Emirate_State__c;
                        objAmnd.Company_Name__c = accountRecord.Name;
                        objAmnd.recordtypeId = recordtypeId;
                        objAmnd.Customer__c = CustomerId;
                        objAmnd.Registration_Date__c = accountRecord.ROC_reg_incorp_Date__c;
                        objAmnd.Place_of_Registration__c = accountRecord.Place_of_Registration_Text__c;
                        serviceRequest.Mother_Full_Name__c = 'added';
                        lstAmendFinalDetails.add(objAmnd);
                }
                
                if(lstAmendFinalDetails.size() !=null && lstAmendFinalDetails.size()>0){
                    
                    UPSERT serviceRequest;
                    List<Amendment__c> lstDetails = new List<Amendment__c>();
                    for(Amendment__c eachAmendment:lstAmendFinalDetails){
                        eachAmendment.ServiceRequest__c = serviceRequest.ID;
                        lstDetails.add(eachAmendment);
                    }
                    if(lstDetails.size()>0){
                        INSERT lstDetails;
                    }
                    PageReference srRecordView = new pagereference('/'+serviceRequest.Id+'?nooverride=1');        
                    srRecordView.setRedirect(true);        
                    pref = srRecordView;
                }
                else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please add or remove a Registered agent to continue.'));
                }
          return pref;
        }
        else{
        	ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You are only allowed to have one registered agent.'));
            return null;
        }
        }
        catch(exception ex){
            return null;
        }
        
    }
     
}