/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestGsBatchDelaySR {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        objTemplate.SR_Group__c = 'GS';
        insert objTemplate;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstSRStatus.add(new SR_Status__c(Name='Verified',Code__c='VERIFIED'));
        lstSRStatus.add(new SR_Status__c(Name='Closed',Code__c='CLosed'));
        insert lstSRStatus;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        Step_Template__c objStepTemplate = new Step_Template__c();
        objStepTemplate.Code__c = 'Collected';
        objStepTemplate.Name = 'Collected';
        objStepTemplate.DEV_Id__c = 'Collected';
        objStepTemplate.SR_Status__c = lstSRStatus[3].Id;
        objStepTemplate.Step_RecordType_API_Name__c = 'General';
        insert objStepTemplate;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
        	mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_New');
        objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[0].Id;
        objSR.External_SR_Status__c = lstSRStatus[0].Id;
        objSR.Adhering_to_Safe_Horbour__c = true;
        objSR.SR_Group__c = 'GS';
        insert objSR;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Status__c = lstStatus[0].Id;
        objStep.Step_Template__c = objStepTemplate.Id;
        insert objStep;
        
        test.startTest();
        	GsScheduleDelaySR objSchedule = new GsScheduleDelaySR();
	    	Datetime dt = system.now().addMinutes(1);
	    	string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();
	    	system.schedule('GsBatchDelaySR Test Class', sCornExp, objSchedule);
        test.stopTest();
    }
}