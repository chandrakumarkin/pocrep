/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class TestScheduleFailedWebServices {

    static testMethod void TestScheduleRelationShips() {
        // TO DO: implement unit test
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        list<Relationship_Codes__c> lstCS = new list<Relationship_Codes__c>();
        lstCS.add(new Relationship_Codes__c(Name='Has Manager',Code__c='1234'));
        lstCS.add(new Relationship_Codes__c(Name='Has Director',Code__c='1235'));
        insert lstCS;
        
        Block_Trigger__c objBT = new Block_Trigger__c();
        objBT.Name = 'Block';
        objBT.Block_Relationship_Trg__c = false;
        insert objBT;
        
        Country_Codes__c objCountryCodes = new Country_Codes__c();
        objCountryCodes.Code__c = 'IN';
        objCountryCodes.Name = 'India';
        objCountryCodes.Nationality__c = 'India';
        insert objCountryCodes;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        insert objEP;
        
        Education_Details__c objED = new Education_Details__c();
        objED.Name = 'B.Tech';
        objED.Code__c = '12';
        insert objED;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Account objAccountNew = new Account();
        objAccountNew.Name = 'Test Custoer 1';
        objAccountNew.E_mail__c = 'test@test.com';
        insert objAccountNew;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        insert objContact;
        
        Contact objContactNew = new Contact();
        objContactNew.LastName = 'Test Contact';
        objContactNew.Email = 'test@difctesst.com';
        objContactNew.MobilePhone = '+9711234567';
        objContactNew.Occupation__c = 'Director';
        objContactNew.Salutation = 'Mr.';
        objContactNew.FirstName = 'Test';
        objContactNew.Nationality__c ='India';
        objContactNew.Birthdate = system.today().addYears(-24);
        objContactNew.RELIGION__c = 'Hindu';
        objContactNew.Marital_Status__c = 'Single';
        objContactNew.Gender__c = 'Male';
        objContactNew.Qualification__c = 'B.Tech';
        objContactNew.Mothers_Name__c = 'My Mom';
        objContactNew.Gross_Monthly_Salary__c = 10000;
        objContactNew.ADDRESS2_LINE1__c = 'Test Street';
        objContactNew.Country__c = 'India';
        objContactNew.BP_No__c = '0012489';
        insert objContactNew;
        
        Contact objContactNew2 = new Contact();
        objContactNew2.LastName = 'Test Contact';
        objContactNew2.Email = 'test@difctesst.com';
        objContactNew2.MobilePhone = '+9711234567';
        objContactNew2.Occupation__c = 'Director';
        objContactNew2.Salutation = 'Mr.';
        objContactNew2.FirstName = 'Test';
        objContactNew2.Nationality__c ='India';
        objContactNew2.Birthdate = system.today().addYears(-24);
        objContactNew2.RELIGION__c = 'Hindu';
        objContactNew2.Marital_Status__c = 'Single';
        objContactNew2.Gender__c = 'Male';
        objContactNew2.Qualification__c = 'B.Tech';
        objContactNew2.Mothers_Name__c = 'My Mom';
        objContactNew2.Gross_Monthly_Salary__c = 10000;
        objContactNew2.ADDRESS2_LINE1__c = 'Test Street';
        objContactNew2.Country__c = 'India';
        insert objContactNew2;
		
		list<Relationship__c> lstRel = new list<Relationship__c>();
		
		//System.assertEquals(false,String.isBlank(objContactNew.Id));
		
		Relationship__c objRel = new Relationship__c();
		objRel.Active__c = true;
		objRel.Subject_Account__c = objAccount.Id;
		objRel.Object_Contact__c = objContact.Id;
		objRel.Relationship_Type__c = 'Has Manager'; 
		objRel.Start_Date__c = system.today();
		//objRel.Push_to_SAP__c = true;
		lstRel.add( objRel);
		objRel = new Relationship__c();
		objRel.Active__c = true;
		objRel.Subject_Account__c = objAccount.Id;
		objRel.Object_Contact__c = objContactNew.Id;
		objRel.Relationship_Type__c = 'Has Director'; 
		objRel.Start_Date__c = system.today();
		//objRel.Push_to_SAP__c = true;
		lstRel.add( objRel);
		objRel = new Relationship__c();
		objRel.Active__c = true;
		objRel.Subject_Account__c = objAccount.Id;
		objRel.Object_Account__c = objAccountNew.Id;
		objRel.Relationship_Type__c = 'Has Director'; 
		objRel.Start_Date__c = system.today();
		//objRel.Push_to_SAP__c = true;
		lstRel.add( objRel);
		
		objRel = new Relationship__c();
		objRel.Active__c = true;
		objRel.Subject_Account__c = objAccount.Id;
		objRel.Object_Contact__c = objContactNew2.Id;
		objRel.Relationship_Type__c = 'Has Director'; 
		objRel.Start_Date__c = system.today();
		lstRel.add( objRel);
		
		insert lstRel;
		
		list<SAPCRMWebService.ZSF_IN_LOG> lstItemsTemp = new list<SAPCRMWebService.ZSF_IN_LOG>();
        SAPCRMWebService.ZSF_IN_LOG objItem =  new SAPCRMWebService.ZSF_IN_LOG();
        objItem.ACTION = 'P';
        objItem.SFREFNO = objContact.Id;
        objItem.SFGUID = objContact.Id;
        objItem.PARTNER1 = '001234';
        objItem.PARTNER2 = '001201';
        objItem.MSG = 'Partner Created Successfully';
        objItem.STATUS = '1';
        lstItemsTemp.add(objItem);
		
		TestPartnerCreationCls.lstItems = lstItemsTemp;
		Test.setMock(WebServiceMock.class, new TestPartnerCreationCls());		
		test.startTest();
		system.schedule('ScheduleFailedWebServices', '0 0 0 3 9 ? 2022', new ScheduleFailedWebServices());
		test.stopTest();
		        
    }
    static testMethod void TestScheduleServices() {
        // TO DO: implement unit test
         User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://eccdev.com/sampledata';
        insert objEP;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        //objSR.Internal_SR_Status__c = lstStatus[0].Id;
        //objSR.External_SR_Status__c = lstStatus[0].Id;
        insert objSR;
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Status__c = 'Added';
        objSRItem.Price__c = 1000;
        insert objSRItem;
        
        Test.setMock(WebServiceMock.class, new TestServiceCreationCls());
        
        list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = objSRItem.Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        
        list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
        SAPECCWebService.ZSF_S_ACC_BAL objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        
        TestServiceCreationCls.resItems = resItems;
        TestServiceCreationCls.resActBals = resActBals;
        test.startTest();
 		system.schedule('ScheduleFailedWebServices_Service', '0 0 0 3 9 ? 2022', new ScheduleFailedWebServices());
		test.stopTest();
    }
    static testMethod void TestScheduleReceipts() {
        // TO DO: implement unit test
        
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        WebService_Details__c objWSD = new WebService_Details__c();
        objWSD.Name = 'Credentials';
        objWSD.SAP_User_Id__c = 'testuser';
        insert objWSD;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Registration_License_No__c = '1234';
        insert objAccount;
        
        map<string,string> mapReceiptRecordTypes = new map<string,string>();
        for(Schema.RecordTypeInfo obj :  Receipt__c.SObjectType.getDescribe().getRecordTypeInfos()){
        	mapReceiptRecordTypes.put(obj.getName(),obj.getRecordTypeId());
        }
        system.debug('mapReceiptRecordTypes are : '+mapReceiptRecordTypes);
        //Endpoint_URLs__c.getAll().get('ECC').URL__c
        Endpoint_URLs__c objEU = new Endpoint_URLs__c();
        objEU.Name = 'ECC';
        objEU.URL__c = 'http://difc.ae/ecc';
        insert objEU;
        
        list<Receipt__c> lstReceipts = new list<Receipt__c>();
        Receipt__c objReceipt = new Receipt__c();
        objReceipt.Customer__c = objAccount.Id;
        //objReceipt.Payment_Status__c = 'Success';
        objReceipt.Amount__c = 10000;
        objReceipt.Receipt_Type__c = 'Card';
        lstReceipts.add(objReceipt);
        objReceipt = new Receipt__c();
        objReceipt.Customer__c = objAccount.Id;
        objReceipt.Payment_Status__c = 'Success';
        objReceipt.Amount__c = 10000;
        objReceipt.Receipt_Type__c = 'Cash';
        lstReceipts.add(objReceipt);
        objReceipt = new Receipt__c();
        objReceipt.Customer__c = objAccount.Id;
        objReceipt.Payment_Status__c = 'Success';
        objReceipt.Amount__c = 10000;
        objReceipt.Receipt_Type__c = 'Cheque';
        lstReceipts.add(objReceipt);
        objReceipt = new Receipt__c();
        objReceipt.Customer__c = objAccount.Id;
        objReceipt.Payment_Status__c = 'Success';
        objReceipt.Amount__c = 10000;
        objReceipt.Receipt_Type__c = 'Bank Transfer';
        objReceipt.Transfer_Date__c = system.today();
        lstReceipts.add(objReceipt);
        insert lstReceipts;
        
        list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = lstReceipts[0].Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'E';
        objTemp.SGUID = lstReceipts[1].Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        
        list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
        SAPECCWebService.ZSF_S_ACC_BAL objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
		
		TestReceiptCreationCls.resItems = resItems;
        TestReceiptCreationCls.resActBals = resActBals;
		Test.setMock(WebServiceMock.class, new TestReceiptCreationCls());
        test.startTest();
 		system.schedule('ScheduleFailedWebServices_Receipt', '0 0 0 3 9 ? 2022', new ScheduleFailedWebServices());
		test.stopTest();
    }
}