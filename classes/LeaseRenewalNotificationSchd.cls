/*************************************************************************************************
 *  Name        : LeaseRenewalNotificationSchd 
 *  Author      : Danish Farooq
 *  Date        : 22-01-2018    
 *  Purpose     : Send lease renewal notification  schduler class Ticket # 3377
 *  TestClass   : LeaseRenewalNotificationTest
**************************************************************************************************/

global without sharing class LeaseRenewalNotificationSchd implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new LeaseRenewalNotification());  
        
    }
  
}