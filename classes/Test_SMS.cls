@isTest(seeAllData=false)
public class Test_SMS {
    static testMethod void myUnitTest() {        
        CLS_SendSMS.sendsms('0097152651234', 'Hi..! This is Test Class.');
        
        Messaging.InboundEmail email = new Messaging.InboundEmail() ;
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
       
        // setup the data for the email
        email.subject = 'Phone=0097152651234';
        email.fromAddress = 'someaddress@email.com';
        email.plainTextBody = 'email body\n2225256325\nTitle';
      
        // call the email service class and test it with the data in the testMethod
        DynamicSMSContent objDynamicSMSContent = new DynamicSMSContent();
        objDynamicSMSContent.handleInboundEmail(email, env);
    }
}