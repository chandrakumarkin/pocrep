/********************************************************************************************************************************************************
 *  Author   	: Mudasir Wani
 *  Date     	: 04-April-2020
 *  Description	: This class will handle the DML functioalities across the DIFC organization
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date       			Updated By    		Description
---------------------------------------------------------------------------------------------------------------------  
V1.0	04-April-2020		Mudasir 			Created this class 
V1.1	03-May-2020			Mudasir				#9102 - Introduce the new functionality where we will capture the web service load  

*********************************************************************************************************************************************************/

public without sharing class DIFCDMLUtils {
     /*public static List<Relationship__c> getRelationshipList(String objSOQLFieldsToQuery , String onjSOQLFilters){
    	String SOQLQueryString = prepareQuery('Relationship__c',objSOQLFieldsToQuery,onjSOQLFilters);
    	List<Relationship__c> relationshipRecords = Database.Query(SOQLQueryString);
    	return relationshipRecords;
    }*/
    
    /**
     * @description Method to insert objects to DataBase with improved audit
     * @param sObjects - sObjects to be insert
     * @param field - field that identifies Saveresult with inserted object
     * @return map where key is a value from field specified as param: field
     * and the value is Save result for sObject which is identified by key
     */
    /*public static Map<String,Database.SaveResult> improvedInsert(List<SObject> sObjects, Schema.sObjectField field) {
        Map<String,Database.SaveResult> saveResultByField = new Map<String,Database.SaveResult>();
        List<Database.SaveResult> results = Database.insert(sObjects, false);
        for (Integer i = 0;i<sObjects.size();i++) {
            saveResultByField.put((String)sObjects.get(i).get(field),results.get(i));
        }
        return  saveResultByField;
    }*/
    /*public static List<Account> insertAccounts(){
    	// Create two accounts, one of which is missing a required field
		Account[] accts = new List<Account>{
	    new Account(Name='Account1'),
	    new Account()};
		Database.SaveResult[] srList = Database.insert(accts, false);
	
		// Iterate through each returned result
		for (Database.SaveResult sr : srList) {
	    if (sr.isSuccess()) {
			        // Operation was successful, so get the ID of the record that was processed
			        System.debug('Successfully inserted account. Account ID: ' + sr.getId());
			    }
			    else {
			        // Operation failed, so get all errors                
			        for(Database.Error err : sr.getErrors()) {
			            System.debug('The following error has occurred.');                    
			            System.debug(err.getStatusCode() + ': ' + err.getMessage());
			            System.debug('Account fields that affected this error: ' + err.getFields());
			        }
			    }
			}
		return accts;
    }
    */
    /***********************************************************************
	*	Parameter 		:	Web_Service_Load__c 
	*	Return 			:	Web_Service_Load__c record
	*	Created By		:	Mudasir Wani
	*	Created Date	:	03-May-2020
	*	Class Version	:	V1.1 - #9102 - Introduce the new functionality where we will capture the web service load  
	*************************************************************************/
    public static Web_Service_Load__c  insertWebServiceLoad(Web_Service_Load__c webServiceLoad){
    	try{
    		insert webServiceLoad;
    	}catch(Exception e){
    		//Create Log 
    		
    	}
    	return webServiceLoad;
    }
}