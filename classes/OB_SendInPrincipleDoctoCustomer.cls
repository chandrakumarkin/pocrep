/*
    Author      : Rachita
    Date        : 10-Apr-2020
    Description : Custom code to Send Certificate of Status as Email
    --------------------------------------------------------------------------------------------------
*/
public class OB_SendInPrincipleDoctoCustomer {
	//input details that comes to apex from flow
	public class FlowInputs{
        @InvocableVariable
        public Id srDocId;
        
        @InvocableVariable
        public string documentMasterCode;
    }
  	@InvocableMethod(label='Send an email from apex class' description='sends an email with attachment In Principle document after generation of an doc')
  	public static void sendEmailFunction(List<FlowInputs> ids) {
    	system.debug('=====getAccountNames====='+ids);
    	try{
		    string srDocId = '';
		    string documentMasterCode =''; 
		    for(FlowInputs inputValue: ids){
		    	srDocId = inputValue.srDocId;
		    	documentMasterCode = inputValue.documentMasterCode;
		    }
		    if(String.IsNotBlank(srDocId) && String.IsNotBlank(documentMasterCode)){
		    	string strTemplateId;
		    	OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
		    	if(documentMasterCode.equalsIgnorecase('CERTIFICATE_OF_STATUS')){
		    		for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName=: Label.OB_EmailTemplateNameInPrinciple]){
						strTemplateId = objET.Id;
					}
		    	}else{
		    		for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName= 'OB_Send_NOC_Sharing_to_Customer']){
						strTemplateId = objET.Id;
					}
		    	}
		    	
		    	if(String.IsNotBlank(strTemplateId)){
		    		list<string> lstIds = new list<string>();// attachment id
		    		String srId = ''; // service request id
			        String emailSendId = ''; // service request's contact email
			        String accountOwnerEmailId = '';// service request's account owner
			        String accountId = '';// service request's account 
			        list<Messaging.Emailfileattachment> lstAttach = new list<Messaging.Emailfileattachment>();
			        Messaging.Emailfileattachment efa;
			        
		    		for(HexaBPM__SR_Doc__c objSRDoc : [Select Id,HexaBPM__Document_Master__c,HexaBPM__Doc_ID__c,
		        									HexaBPM__Document_Master__r.HexaBPM__Code__c,
		        									HexaBPM__Service_Request__c,
		        									HexaBPM__Service_Request__r.HexaBPM__Email__c,
		        									HexaBPM__Service_Request__r.HexaBPM__Customer__c,
		        									HexaBPM__Service_Request__r.HexaBPM__Customer__r.Owner.Email
		        									From HexaBPM__SR_Doc__c 
		        									Where Id =:srDocId
		        									AND HexaBPM__Service_Request__c != null 
		        									AND HexaBPM__Service_Request__r.HexaBPM__Customer__c != null 
		        									AND HexaBPM__Doc_ID__c != null
		        									AND HexaBPM__Document_Master__r.HexaBPM__Code__c =: documentMasterCode]){
			            lstIds.add(objSRDoc.HexaBPM__Doc_ID__c);
			            if(objSRDoc.HexaBPM__Service_Request__c != null){
			            	srId = objSRDoc.HexaBPM__Service_Request__c;
			            	
			            	emailSendId = objSRDoc.HexaBPM__Service_Request__r.HexaBPM__Email__c;
				            if(objSRDoc.HexaBPM__Service_Request__r.HexaBPM__Customer__c != null){
				            	accountId = objSRDoc.HexaBPM__Service_Request__r.HexaBPM__Customer__c;
				            	accountOwnerEmailId = objSRDoc.HexaBPM__Service_Request__r.HexaBPM__Customer__r.Owner.Email;
				            }
			            }
		        	} 
		        	for(ContentVersion cv : [Select Id,PathOnClient,VersionData,Title,Application_Document__c from ContentVersion 
		        							where ContentDocumentId IN : lstIds order by CreatedDate desc ]){
			            efa = new Messaging.Emailfileattachment();
			            efa.setFileName(cv.PathOnClient);
			            efa.setBody(cv.VersionData);
			            lstAttach.add(efa);
			        }
			        system.debug(accountOwnerEmailId+'=====lstAttach======='+lstAttach);
			        if(lstAttach != null && lstAttach.size() != 0 && String.IsNotBlank(accountId)){
			            // temporay contact creation for sending an email
			            Contact tempContact = new Contact(email = emailSendId, firstName = 'test', lastName = 'email',AccountId =accountId);
			 			insert tempContact;
			 			if(tempContact != null && tempContact.Id != null){
			 				list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
			 				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			 				
			 				mail = new Messaging.SingleEmailMessage();
				            mail.setCCAddresses(new List<String>{accountOwnerEmailId});
				            mail.setToAddresses(new List<String>{emailSendId});
				            //mail.setReplyTo('portal@difc.ae');
				            mail.setWhatId(srId);
				            mail.setTemplateId(strTemplateId);
				            //mail.setTreatTargetObjectAsRecipient(false);
				            mail.setSaveAsActivity(false);
				            if(lstOrgEA!=null && lstOrgEA.size()>0)
				            	mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
				            if(!lstAttach.isEmpty())
			                	mail.setFileAttachments(lstAttach);
				            mail.setTargetObjectId(tempContact.Id);
				            mails.add(mail);
				            
				            //system.debug('=====mails======='+mails);
				           	if(mails != null && !mails.isEmpty()){
				                if(!test.isRunningTest()){
				                    Messaging.sendEmail(mails);
				                }  
				                delete tempContact;  
				           	}
			 			}
			        }
		    	}
	    	}
    	}catch(Exception e){
        	insert LogDetails.CreateLog(null, 'OB_SendInPrincipleDoctoCustomer:sendEmailFunction', 'Line Number : '+e.getLineNumber()+'\nException is : '+e.getMessage());
    	}
	}    
}