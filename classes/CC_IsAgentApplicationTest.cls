/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CC_IsAgentApplicationTest {

    static testMethod void myUnitTest() {
        list<Account> insertNewAccounts = new list<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insert insertNewAccounts;
        
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].Passport_No__c = 'A12345';
        insertNewSRs[0].Nationality__c = 'India';
        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
        insertNewSRs[0].Gender__c = 'Male';
        insertNewSRs[0].Place_of_Birth__c = 'Dubai';
        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
        insertNewSRs[0].Last_Name__c = 'test';
        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Business_Sector__c = 'Hotel';
        insertNewSRs[0].entity_name__c = 'Tentity';
        insertNewSRs[0].Progress_Completion__c = 10.0;
        insertNewSRs[0].HexaBPM__Contact__c = con.id;
        insert insertNewSRs;
        
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs[0].id;
        insertNewSRs[0].HexaBPM__Parent_SR__c = insertNewSRs[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        update insertNewSRs;
        
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'REGISTRAR_REVIEW'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
        listSRSteps[0].HexaBPM__Step_No__c = 1;
        //listSRSteps[0].HexaBPM__Step_Template_Code__c = 'CLO_REVIEW';
        insert listSRSteps;
        
       
        
        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Awaiting Verification',HexaBPM__Code__c='AWAITING_VERIFICATION',HexaBPM__Type__c='Start');
        
        HexaBPM__Status__c status1 = new HexaBPM__Status__c
        (Name='Re-Upload Document',HexaBPM__Code__c='REUPLOAD_DOCUMENT',HexaBPM__Type__c='End');
        insert status1;
        
        list<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        insertNewSteps[0].HexaBPM__Status__c = status.id;
        insertNewSteps[0].Relationship_Manager__c = userinfo.getUserId();
        insertNewSteps[1].Relationship_Manager__c = userinfo.getUserId();
        insert insertNewSteps;
        
        CC_IsAgentApplication objcc = new CC_IsAgentApplication();
        objcc.EvaluateCustomCode(null,insertNewSteps[0]);
        
        objcc.EvaluateCustomCode(insertNewSRs[0],null);
        
    }
}