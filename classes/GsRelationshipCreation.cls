/*
    Author      :   Ravi
    Description :   This class is used to created the GS Relationships and the BP in SAP 
    --------------------------------------------------------------------------------------------------------------------------
	Modification History
 	--------------------------------------------------------------------------------------------------------------------------
	V.No	Date		Updated By    	Description
	--------------------------------------------------------------------------------------------------------------------------             
 	V1.0    24-04-2015	Ravi			Created   
*/
public without sharing class GsRelationshipCreation {
	/*@future(callout=true)
	public static void CreateContact(list<string> lstRelIds){
		list<GsSAPCRMService.ZSFS_GSDAT_IN> items = new list<GsSAPCRMService.ZSFS_GSDAT_IN>();
		GsSAPCRMService.ZSFS_GSDAT_IN item;
		map<string,string> mapConBPs = new map<string,string>();
		list<Log__c> lstLogs = new list<Log__c>();
		Log__c objLog;		
		try{
			map<Id,Contact> mapContacts = new map<Id,Contact>([select Id,BP_No__c,Account.BP_No__c,Authorization_Group__c,
									AccountId,Salutation,FirstName,Middle_Names__c,LastName,CurrencyIsoCode,
									First_Name_Arabic__c,Middle_Name_Arabic__c,Last_Name_Arabic__c,Nationality__c,Occupation__c,Gender__c,Place_of_Birth__c,
									Birthdate,Country_of_Birth__c,Passport_No__c,Passport_Type__c,Place_of_Issue__c,Issued_Country__c,Date_of_Issue__c,Passport_Expiry_Date__c,
									RELIGION__c,Marital_Status__c,Previous_Nationality__c,Qualification__c,Spoken_Language1__c,Spoken_Language2__c,Spoken_Language3__c,
									Mother_Full_Name__c,Email,BASIC_SALARY__c,Other_Allowance__c,Accomodation_Amount__c,Emirate__c,Area__c,City__c,MobilePhone,Domicile__c,
									Gross_Monthly_Salary__c,ADDRESS_LINE1__c,Country__c,Job_Title__c,Job_Title__r.Name,CITY1__c,OtherPhone,ADDRESS_LINE2__c,
									Postal_Code__c,PO_Box_HC__c,Phone,Fax,Work_Phone__c,Address_outside_UAE__c
									from Contact where Id IN (select Object_Contact__c from Relationship__c where Id IN : lstRelIds)]);
		
			if(mapContacts != null && !mapContacts.isEmpty()){
				for(Contact objContact : mapContacts.values()){
					if(objContact.BP_No__c != null && objContact.BP_No__c != ''){
						mapConBPs.put(objContact.Id,objContact.BP_No__c);
					}else{
						item = GsSapWebServiceDetails.PrepareGsContact(objContact);
						items.add(item);
					}
				}
			}
			if(!items.isEmpty()){
				GsSAPCRMService.ZSFTT_GSDAT_IN objGsData = new GsSAPCRMService.ZSFTT_GSDAT_IN();
				objGsData.item = items;
				system.debug('request is $$$ : '+items);
				GsSAPCRMService.gs_data objSFData = new GsSAPCRMService.gs_data();
				objSFData.timeout_x = 120000;
				objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
				objSFData.inputHttpHeaders_x= new Map<String, String>();
				Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c);
				String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
				objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
				GsSAPCRMService.ZSF_IN_LOGT response = objSFData.Z_SF_GS_PROCESS(objGsData);
				system.debug('response is : $$ '+response);
				
				if(response != null){
					for(GsSAPCRMService.ZSF_IN_LOG objRes : response.item){
						if(objRes.SFGUID != null && objRes.SFGUID.indexOf('003') == 0){
							if(objRes.PARTNER2 != null && objRes.PARTNER2 != ''){
								mapConBPs.put(objRes.SFGUID,objRes.PARTNER2);
							}else{
								objLog = new Log__c();
								//objLog.Account__r = new Account(BP_No__c=objRes.PARTNER1);	
								//objLog.Description__c = 'Partner1 No is : '+objRes.PARTNER1+'\n\n Contact is : '+objRes.SFGUID+'\n\n Error is : '+objRes.MSG;
								objLog.Type__c = 'GS Relationship : Contact Creation process';
								objLog.Description__c = objRes.MSG+'\nFor Contact : '+objRes.SFGUID;
								lstLogs.add(objLog);
							}
						}
					}
				}//end of response if
			}
			if(!mapConBPs.isEmpty())
				CreateRelationship(lstRelIds,mapConBPs,lstLogs);
			else if(!lstLogs.isEmpty())
				insert lstLogs;
				
		}catch(Exception ex){
			objLog = new Log__c();
			objLog.Type__c = 'GS Relationship : Contact Creation ';
			objLog.Description__c = ex.getMessage()+'\nLine Number : '+ex.getLineNumber();
			insert objLog;
		}
	}
	
	@future(callout=true)
	public static void CreateRelationship(list<string> lstRelIds){
		CreateRelationship(lstRelIds, new map<string,string>(), new list<Log__c>());
	}
	
	public static void CreateRelationship(list<string> lstRelIds,map<string,string> mapConBPs,list<Log__c> lstLogs){
		list<GsSAPCRMService.ZSFS_GSDAT_IN> items = new list<GsSAPCRMService.ZSFS_GSDAT_IN>();
		GsSAPCRMService.ZSFS_GSDAT_IN item;
		lstLogs = lstLogs != null ? lstLogs : new list<Log__c>();
		Log__c objLog;
		
		map<string,Contact> mapContacts = new map<string,Contact>();
		map<string,Relationship__c> mapRelationships = new map<string,Relationship__c>();
		Relationship__c objRel;
		
		//Relationship__c.Object_Account__c
		try{
			for(Relationship__c objRelationShip : [select Id,Name,Partner_1__c,Start_Date__c,End_Date__c,Partner_2__c,Object_Contact__c,Object_Account__c,Relationship_Type__c,
							Subject_Account__c,Subject_Account__r.BP_No__c,Object_Contact__r.BP_No__c from Relationship__c where Id IN : lstRelIds AND Relationship_Group__c='GS']){
				if(Relationship_Codes__c.getAll().get(objRelationShip.Relationship_Type__c) != null && Relationship_Codes__c.getAll().get(objRelationShip.Relationship_Type__c).Code__c != null){
					item = new GsSAPCRMService.ZSFS_GSDAT_IN();
					
					item.SFREFNO = string.valueOf(objRelationShip.Id).substring(0,15);
					item.SFGUID = objRelationShip.Id;
					item.PARTNER1 = objRelationShip.Partner_1__c;
					if(objRelationShip.Partner_2__c != null && objRelationShip.Partner_2__c != '')
						item.PARTNER2 = objRelationShip.Partner_2__c;
					else if(mapConBPs != null && mapConBPs.containsKey(objRelationShip.Object_Contact__c)){
						item.PARTNER2 = mapConBPs.get(objRelationShip.Object_Contact__c);
					}	
					item.ACTION = 'RC';
					item.RELTYP = Relationship_Codes__c.getAll().get(objRelationShip.Relationship_Type__c).Code__c;
					item.DATE_FROM = objRelationShip.Start_Date__c != null ? GsPendingInfo.SAPDateFormat(objRelationShip.Start_Date__c):GsPendingInfo.SAPDateFormat(system.today());
					item.DATE_TO = objRelationShip.End_Date__c != null ? GsPendingInfo.SAPDateFormat(objRelationShip.End_Date__c):'9999-12-31';
					item.ERNAM = 'SALESFORCE';
					item.ERTIM = GsPendingInfo.SAPTimeFormat(system.now());
					item.ERDAT = GsPendingInfo.SAPDateFormat(system.now());
					items.add(item);
				}else{
					objLog = new Log__c();
					objLog.Type__c = 'GS Relationship Creation';
					objLog.Description__c = 'Relationship - '+objRelationShip.Relationship_Type__c+' was not listed in the custom setting';
					lstLogs.add(objLog);
				}
			}
			if(!items.isEmpty()){
				GsSAPCRMService.ZSFTT_GSDAT_IN objGsData = new GsSAPCRMService.ZSFTT_GSDAT_IN();
				objGsData.item = items;
				system.debug('request is $$$ : '+items);
				GsSAPCRMService.gs_data objSFData = new GsSAPCRMService.gs_data();
				objSFData.timeout_x = 120000;
				objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
				objSFData.inputHttpHeaders_x= new Map<String, String>();
				Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c);
				String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
				objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
				GsSAPCRMService.ZSF_IN_LOGT response = objSFData.Z_SF_GS_PROCESS(objGsData);
				system.debug('response is : $$ '+response);
				
				if(response != null){
					for(GsSAPCRMService.ZSF_IN_LOG objRes : response.item){
						if(objRes.STATUS == 'S'){
							objRel = new Relationship__c(Id=objRes.SFGUID);
							objRel.Push_to_SAP__c = true;
							mapRelationships.put(objRel.Id,objRel);
						}else{
							objLog = new Log__c();
							objLog.Type__c = 'GS Relationship : Relationship Process ';
							objLog.Description__c = objRes.MSG+'\nPartner2 is : '+objRes.PARTNER2;
							lstLogs.add(objLog);
						}
					}
				}//end of response if
				Savepoint sp = Database.setSavepoint();
				try{
					if(mapConBPs != null && mapConBPs.isEmpty() == false){
						for(string ContactId : mapConBPs.keySet()){
							mapContacts.put(ContactId,new Contact(Id=ContactId,BP_No__c=mapConBPs.get(ContactId),BP_Created_Time__c=system.now()));
						}
						if(!mapContacts.isEmpty())
							update mapContacts.values();
					}
					if(!mapRelationships.isEmpty())
						update mapRelationships.values();
				}catch(Exception ex){
					database.rollback(sp);
					objLog = new Log__c();
					objLog.Type__c = 'GS Master Data Update Process ';
					objLog.Description__c = ex.getMessage()+'\nLine # : '+ex.getLineNumber();
					lstLogs.add(objLog);
				}
				if(!lstLogs.isEmpty())
					insert lstLogs;
			}
		}catch(Exception ex){
			objLog = new Log__c();
			objLog.Type__c = 'GS Relationship : Relationship Creation ';
			objLog.Description__c = ex.getMessage()+'\nLine Number : '+ex.getLineNumber();
			insert objLog;
		}
	}
	*/
}