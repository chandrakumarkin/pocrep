/******************************************************************************************************
 *  Author   : Kumar Utkarsh
 *  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* This is a Test class for webServiceGdrfaAppStatusEnquiryBatch and webServiceGdrfaAppStatusQueueable
*******************************************************************************************************/
@isTest
public class webServiceGdrfaAppStatusEnquiryBatchTest {
    
    @testSetup static void testSetupdata(){
        
        Service_Request__c testSr = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        Id RecordTypeIdSr = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-Cancellation').getRecordTypeId();
        testSr.RecordTypeId = RecordTypeIdSr;
        testSr.Mobile_Number__c = '';
        testSr.Date_of_Birth__c = Date.newInstance(2001, 02, 03);
        insert testSr;
        // Create Status__c data
        Status__c status = new Status__c();
        status.Code__c = 'AWAITING_REVIEW';
        insert status;
        
        GDRFA_Master_Data__c mdData = new GDRFA_Master_Data__c();
        mdData.Lookup_Type__c = 'Country';
        mdData.Name_English__c = 'United Arab Emirates';
        mdData.Look_Up_Id__c = '101';
        mdData.GDRFA_Unique_Value__c = 'Country-101';
        insert mdData;
        
        Status__c status1 = new Status__c();
        status1.Code__c = 'CLOSED';
        status1.Name = 'closed';
        insert status1;
        
        Status__c status2 = new Status__c();
        status2.Code__c = 'DNRD_GS_Review';
        insert status2;
        
        Status__c status3 = new Status__c();
        status3.Code__c = 'PENDING';
        insert status3;
        
        Status__c status4 = new Status__c();
        status4.Code__c = 'GDRFA_Returned';
        insert status4;
        
        List<Step_Template__c> stepTemplateList = new List<Step_Template__c>();
        
        Step_Template__c stepTemplate = new Step_Template__c();
        stepTemplate.Name = 'Completed';
        stepTemplate.Code__c = 'Completed';
        stepTemplate.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate);
        
        Step_Template__c stepTemplate5 = new Step_Template__c();
        stepTemplate5.Name = 'Entry Permit is Issued';
        stepTemplate5.Code__c = 'Entry Permit is Issued';
        stepTemplate5.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate5);
        
        Step_Template__c stepTemplate6 = new Step_Template__c();
        stepTemplate6.Name = 'Passport is Ready for Collection';
        stepTemplate6.Code__c = 'Passport is Ready for Collection';
        stepTemplate6.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate6);
        
        Step_Template__c stepTemplate1 = new Step_Template__c();
        stepTemplate1.Name = 'Cancellation Form Typed';
        stepTemplate1.Code__c = 'Cancellation Form Typed';
        stepTemplate1.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate1);
        
        Step_Template__c stepTemplate2 = new Step_Template__c();
        stepTemplate2.Name = 'Renewal Form is Typed';
        stepTemplate2.Code__c = 'Renewal Form is Typed';
        stepTemplate2.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate2);
        
        Step_Template__c stepTemplate3 = new Step_Template__c();
        stepTemplate3.Name = 'Entry Permit Form is Typed';
        stepTemplate3.Code__c = 'Entry Permit Form is Typed';
        stepTemplate3.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate3);
        
        Step_Template__c stepTemplate4 = new Step_Template__c();
        stepTemplate4.Name = 'Visa Stamping Form is Typed';
        stepTemplate4.Code__c = 'Visa Stamping Form is Typed';
        stepTemplate4.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate4);
        insert stepTemplateList;
        
        SR_Template__c srTemplate = new SR_Template__c();
        srTemplate.Name = 'DIFC Sponsorship Visa-Cancellation';
        srTemplate.SR_Group__c = 'GS';
        srTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        insert srTemplate;
        
        SR_Steps__c srStep1 = new SR_Steps__c();
        srStep1.Step_Template__c  = stepTemplate.Id;
        srStep1.SR_Template__c = srTemplate.Id;
        Insert srStep1;
        
        List<Step__c> stepList = new List<Step__c>();
        // Create Step__c data
        Step__c step = new Step__c();
        step.Step_Template__c = stepTemplate5.Id;
        step.Status__c = status.Id;
        step.SR__c  = testSr.Id;
        stepList.add(step);
        
        Step__c step1 = new Step__c();
        step1.Step_Template__c = stepTemplate6.Id;
        step1.Status__c = status.Id;
        step1.SAP_IDNUM__c = '123';
        step1.SR__c = testSr.Id;
        stepList.add(step1);
        
        Step__c step2 = new Step__c();
        step2.Step_Template__c = stepTemplate.Id;
        step2.Status__c = status.Id;
        step2.SR__c = testSr.Id;
        stepList.add(step2);
        
        Step__c step3 = new Step__c();
        step3.Step_Template__c = stepTemplate1.Id;
        step3.Transaction_Number__c = '1232421337';
        step3.DNRD_Receipt_No__c = '123456132197';
        step3.Payment_Date__c = Date.newInstance(2021, 02, 03);
        step3.Status__c = status1.Id;
        //step3.Status_Code__c = 'CLOSED';
        step3.SR__c = testSr.Id;
        stepList.add(step3);
        
        Step__c step4 = new Step__c();
        step4.Step_Template__c = stepTemplate2.Id;
        step4.Transaction_Number__c = '123242131';
        step4.DNRD_Receipt_No__c = '1234561321212';
        step4.Payment_Date__c = Date.newInstance(2021, 02, 03);
        step4.Status__c = status1.Id;
        step4.SR__c = testSr.Id;
        stepList.add(step4);
        
        Step__c step5 = new Step__c();
        step5.Step_Template__c = stepTemplate3.Id;
        step5.Transaction_Number__c = '1232421786';
        step5.DNRD_Receipt_No__c = '1234561321192';
        step5.Payment_Date__c = Date.newInstance(2021, 02, 03);
        step5.Status__c = status1.Id;
        step5.SR__c = testSr.Id;
        stepList.add(step5);
        insert stepList;
        
        Document_Master__c docMaster = new Document_Master__c();
        docMaster.code__c = 'Cancellation Confirmation';
        insert docMaster;
        
        SR_Doc__c srDoc = new SR_Doc__c();
        srDoc.Service_Request__c = testSr.Id;
        srDoc.Document_Master__c = docMaster.Id;
        srDoc.Name = 'Coloured Photo';
        Insert srDoc; 

    }    
    
    static testMethod void webServiceGdrfaAppStatusAdditionalDocTest(){
        WebServiceGdrfaAppStatusEnquiryBatch mbc = new WebServiceGdrfaAppStatusEnquiryBatch();
        List<Step__c> scope = [SELECT Id, SAP_IDNUM__c, SR__c, Step_Name__c, Step_Template__c, SR_Step__c, SR__r.Record_Type_Name__c from step__c WHERE Step_Name__c IN('Entry Permit is Issued', 'Completed','Passport is Ready for Collection') ];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WebServiceGdrfaAppStatusPrintVisaMock());
        mbc.start(null);
        mbc.execute(null, scope);
        mbc.finish(null);
        Test.stopTest();
    }
    
     static testMethod void webServiceGdrfaAppStatusTest(){
        WebServiceGdrfaAppStatusEnquiryBatch mbc = new WebServiceGdrfaAppStatusEnquiryBatch();
        List<Step__c> scope = [SELECT Id, SAP_IDNUM__c, SR__c, Step_Name__c, Step_Template__c, SR_Step__c, SR__r.Record_Type_Name__c from step__c WHERE Step_Name__c IN('Entry Permit is Issued', 'Completed','Passport is Ready for Collection')];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WebServiceGdrfaPrintVisaAppStatusMock());
        mbc.start(null);
        mbc.execute(null, scope);
        mbc.finish(null);
        Test.stopTest();
    }
    
    static testMethod void webServiceGdrfaAppStatusApprovedTest(){
        WebServiceGdrfaAppStatusEnquiryBatch mbc = new WebServiceGdrfaAppStatusEnquiryBatch();
        List<Step__c> scope = [SELECT Id, SAP_IDNUM__c, SR__c, Step_Name__c, Step_Template__c, SR_Step__c, SR__r.Record_Type_Name__c from step__c WHERE Step_Name__c IN('Entry Permit is Issued', 'Completed','Passport is Ready for Collection')];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WebServiceGdrfaAppStatusQueueableMock());
        mbc.start(null);
        mbc.execute(null, scope);
        mbc.finish(null);
        Test.stopTest();
    }
    
     static testMethod void webServiceGdrfaAppStatusRejectTest(){
        WebServiceGdrfaAppStatusEnquiryBatch mbc = new WebServiceGdrfaAppStatusEnquiryBatch();
        List<Step__c> scope = [SELECT Id, SAP_IDNUM__c, SR__c, Step_Name__c, Step_Template__c, SR_Step__c, SR__r.Record_Type_Name__c from step__c WHERE Step_Name__c IN('Entry Permit is Issued', 'Completed','Passport is Ready for Collection')];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new WebServiceGdrfaAppStatusQueueMock());
        mbc.start(null);
        mbc.execute(null, scope);
        mbc.finish(null);
        Test.stopTest();
    }
}