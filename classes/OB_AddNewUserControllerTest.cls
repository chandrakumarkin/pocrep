/*
    Author      : Leeba
    Date        : 30-March-2020
    Description : Test class for OB_AddNewUserController
    --------------------------------------------------------------------------------------
*/
@isTest
public class OB_AddNewUserControllerTest {
     
    public static testmethod void testMethod1() {
        
            // create account
            List<Account> insertNewAccounts = new List<Account>();
            insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
            insert insertNewAccounts;
            
           Account acc  = new Account();
           acc.name = 'test';      
           insert acc;
        
            //create contact
            Contact con = new Contact(LastName ='testCon',Email ='test2@test.com',AccountId = acc.id);
            insert con; 
            
            Id profileToTestWith = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
            
           
            User user = new User(alias = 'test123', email='test@test.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profileToTestWith, country='United States',IsActive =true,
                                 ContactId = con.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user;
            
            
            AccountContactRelation acccon = new AccountContactRelation();
            acccon.AccountId =  insertNewAccounts[0].Id;
            acccon.ContactId = con.Id;
            acccon.Roles = 'Company Services';
            acccon.Access_Level__c = 'Read Only';
            insert acccon;
            
            Contact con1 = new Contact();
            con1.lastName = 'test';
            con1.Email = 'test1@test.com';
            
            HexaBPM__SR_Template__c objSrTemp = new HexaBPM__SR_Template__c();
            objSrTemp.Name = 'Complete your profile';
            objSrTemp.HexaBPM__Active__c = true;
            objSrTemp.HexaBPM__SR_RecordType_API_Name__c = 'Converted_User_Registration';
            insert objSrTemp;
            
            
            test.startTest();
            
            OB_AddNewUserController.RequestWrap reqParam = new OB_AddNewUserController.RequestWrap();
            OB_AddNewUserController.RespondWrap resp = new OB_AddNewUserController.RespondWrap();
            OB_AddNewUserController.relCheckWrap relcheck = new OB_AddNewUserController.relCheckWrap();
            OB_AddNewUserController.RespondWrap resp1 = new OB_AddNewUserController.RespondWrap();
        
             reqParam.accIdToRelate = con1.Id;
             reqParam.relId = acccon.Id;
             reqParam.RolesSelected = 'Company Services';
             reqParam.AccessLevel = 'Read Only';
             reqParam.toDeactivate= true;
             reqParam.contToInsert = con1;
             
             relcheck.relExist = false;
             relcheck.isDeact = false;
             relcheck.relRec = acccon;
             
             set<string> strconid = new set<string>();
             strconid.add(con.id);
            
             
             String reqParamString = JSON.serialize(reqParam); 
             String reqParamString1 = JSON.serialize(relcheck);
             resp = OB_AddNewUserController.initContact();
             resp = OB_AddNewUserController.getConAccess(reqParamString);
      
           OB_AddNewUserController.updateRelationshipRecord(con.id,insertNewAccounts[0].Id,reqParam.RolesSelected,reqParam.AccessLevel );  
       
            
             resp = OB_AddNewUserController.updateRelationship(reqParamString);
             resp = OB_AddNewUserController.addDupContact(reqParamString);
             resp = OB_AddNewUserController.insertNewCont(reqParamString);
             OB_AddNewUserController.relCheck(strconid,insertNewAccounts[0].Id);
        OB_AddNewUserController.creteCompleateProfileSr(con1);
             
        
        test.stopTest();
    
    }

}