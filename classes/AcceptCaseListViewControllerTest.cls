@isTest
private class AcceptCaseListViewControllerTest{
    static testmethod void test1(){
        Case objCase = new Case();
        objCase.Subject = 'test subject';
        insert objCase;       
        
        List<Case> caseList = new List<Case>();
        caseList.add(objCase);
        
        test.startTest();
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(caseList);
            sc.setSelected(caseList);   
            AcceptCaseListViewController ctrlr = new AcceptCaseListViewController(sc);  
            ctrlr.acceptCases();
            ctrlr.updateCases();
        test.stopTest();
    }
    
    static testmethod void test2(){
    
        Group testGroup = new Group(Name='QUEUE NAME', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            //Associating queue with group AND to the Case object
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
    
        Case objCase = new Case();
        objCase.Subject = 'test subject';
        objCase.Status = 'Invalid';
        objCase.OwnerId = testGroup.id;
        insert objCase;       
        
        List<Case> caseList = new List<Case>();
        caseList.add(objCase);
        
        test.startTest();
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(caseList);
            sc.setSelected(caseList);   
            AcceptCaseListViewController ctrlr = new AcceptCaseListViewController(sc);  
            ctrlr.acceptCases();
            ctrlr.updateCases();
        test.stopTest();
    }
    
    static testmethod void test3(){
    
        Group testGroup = new Group(Name='QUEUE NAME', Type='Queue');
        insert testGroup;
        
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            //Associating queue with group AND to the Case object
            QueuesObject testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Case');
            insert testQueue;
        }
    
        Case objCase = new Case();
        objCase.Subject = 'test subject';
        objCase.Status = 'New';
        objCase.OwnerId = testGroup.id;
        insert objCase;       
        
        List<Case> caseList = new List<Case>();
        caseList.add(objCase);
        
        test.startTest();
            ApexPages.StandardSetController sc = new ApexPages.StandardSetController(caseList);
            sc.setSelected(caseList);   
            AcceptCaseListViewController ctrlr = new AcceptCaseListViewController(sc);  
            ctrlr.acceptCases();
            ctrlr.updateCases();
        test.stopTest();
    }
    
    static testmethod void test4(){
        
      //  Test.setMock(HttpCalloutMock.class, new OfficeRndAttachmentQueueableMockTest());

    
        Ticket__c tic = new Ticket__c();
        tic.Completed_Date__c  = system.now();
        tic.Created_On__c = system.now();
        insert tic;
        
         Endpoint_URLs__c endP = new Endpoint_URLs__c();
         endP.URL__c = 'test.salesforce.com';
         endP.Name ='Office Rnd Attachment';
         insert endP;
         
         WebService_Details__c we = new WebService_Details__c();
         we.Name ='Credentials';
         we.Certificate_Name__c ='test';
         insert we;
         Test.setMock(WebServiceMock.class, new sapComDocumentSapSoapFunctionsMcSRMDMock());

        Test.startTest();
            sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachment_element ttRefundOp = new sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachment_element();
            ttRefundOp.ImExtension ='gfg';
            ttRefundOp.ImFileName ='kdda';
            ttRefundOp.ImMid ='sasad';
            ttRefundOp.Xstr='test';
            
            sapComDocumentSapSoapFunctionsMcSRMDMock.ttRefundOp = ttRefundOp;
               
            Office_RND_Membership__c off = new Office_RND_Membership__c();
            off.End_Date__c = system.today();
            off.Start_Date__c= system.today();
            off.membershipid__c ='dfghjk';
            off.Customer_Email__c ='test@gmail.com';
            insert off;
            
            off.Start_Date__c= system.today().addDays(-1);
            update off;
            
        Test.stopTest();
        
        
    }
}