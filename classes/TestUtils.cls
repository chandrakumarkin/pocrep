public class TestUtils {
    public static Lead createLead() {
        Lead thisLead                           = new Lead();
        thisLead.LastName                       = 'test company';
        thisLead.Status                         = 'Not Contacted';
        thisLead.Company                        = 'Company';
        thisLead.Email                          = 'test@ascential.com';
        return thisLead;
    }

   

    public static Account createAccount() {
       
        Account thisAccount                      = new Account();
        thisAccount.Name                         = 'Test Account';
        thisAccount.BillingCountry               = 'United Kingdom';
        thisAccount.BillingState                 = 'London';
        thisAccount.BillingPostalCode            = '123ABC';
        thisAccount.BillingStreet                = 'Street';
        thisAccount.BillingCity                  = 'City';
        return thisAccount;
    }
    
    public  static Contact createContact(Id AccountId) {
        Contact thisContact         = new Contact();
        thisContact.FirstName       = 'FirstName';
        thisContact.LastName        = 'LastName';
        thisContact.AccountId       =  AccountId;
        thisContact.Title           = 'Title';
        thisContact.Email                            = 'test@ascential.com';
        return thisContact;
    }

 }