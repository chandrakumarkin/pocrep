/***********************************************************************************
 *  Author   : Swati
 *  Company  : NSI
 *  Date     : 30/03/2016
 *  Purpose  : This class is to populate document description on SR template 
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date        Updated By  Description
 ----------------------------------------------------------------------------------------              
      
 
 **********************************************************************************************/
public without sharing class Cls_SRTemplateDescription {

    public SR_Template__c objSRTemp {get; set;}
    
    
    public  Cls_SRTemplateDescription(){
        objSRTemp = new SR_Template__c();
        string typeOfSRTemp = ApexPages.CurrentPage().GetParameters().Get('type');
        if(string.isNotBlank(typeOfSRTemp)){
            getRelatedSRTemplate(typeOfSRTemp);
        }
    }
    
    public void getRelatedSRTemplate(string typeOfSRTemp){
        List<SR_Template__c> listOfSRTemplate = new List<SR_Template__c>();
        listOfSRTemplate = [select name,SR_Description__c, SR_Document_Description_Text__c,Sub_menu__c,Menu__c 
                            from SR_Template__c 
                            where SR_RecordType_API_Name__c =: typeOfSRTemp limit 1];
        if(listOfSRTemplate!=null && !listOfSRTemplate.isEmpty()){
            objSRTemp = listOfSRTemplate[0];  
            
        }
    } 
    
    
}