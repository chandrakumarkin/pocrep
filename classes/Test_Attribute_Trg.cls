@isTest(seeAllData=false)
public class Test_Attribute_Trg {
    static testMethod void myUnitTest() {
        
        
        Account acc= new Account();
        acc.name='test';
        acc.Push_to_PR__c=true;
        insert acc;
        
        SAP_Attribute__c SAP= new  SAP_Attribute__c();
        SAP.Home_Country__c='UAE';
        SAP.Business_Activity__c='Test';
        SAP.DFSA_Approval__c='Test';
        SAP.DIFC_Data_Center__c='Test';
        SAP.DIFC_IT_Team_Members__c='Test';
        SAP.DIFC_Type_Operation__c='Test';
        SAP.Data_Center_Access__c='Test';
        SAP.Former_Name__c='Test';
        SAP.Pending_Dissolution_Date__c='05.12.2014';
        SAP.Financial_Year_End__c='Test';
        SAP.Account__c=acc.id;
        insert SAP;
    }
}