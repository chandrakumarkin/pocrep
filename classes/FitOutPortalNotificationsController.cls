public with sharing class FitOutPortalNotificationsController {
    public list<FitOut_Portal_Notification__mdt> fitOutNotifications { get; set; }

  
    
    public FitOutPortalNotificationsController (){
    
        fitOutNotifications = new list<FitOut_Portal_Notification__mdt>();
        
        for(FitOut_Portal_Notification__mdt fitout : [select MasterLabel,Notification_Details__c from FitOut_Portal_Notification__mdt where is_active__c = true order by Display_Order__c ASC]){
           fitOutNotifications.add(fitout);
        }
        
        
        
    }
}