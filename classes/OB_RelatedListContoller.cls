/**
 * Description : Controller for OB_RelatedList component
 *
 * ****************************************************************************************
 * History :
 * [5.NOV.2019] Prateek Kadkol - Code Creation
 */
public without sharing class OB_RelatedListContoller {

	@AuraEnabled
	public static RespondWrap fetchRelatedRecords(String requestWrapParam) {
		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

		string reledId = reqWrap.parentRecordId;
	

		string queryString = 'SELECT ' + reqWrap.fieldsToQuery + ' from ' + reqWrap.objectToQueryFrom + ' WHERE ' + reqWrap.parentRelationFieldname + ' =: reledId ';

		if(reqWrap.additionalFilters != null ){
			queryString += ' ' + reqWrap.additionalFilters;
		}


		respWrap.relatedRecList.AddAll((List<sObject>) database.query(queryString));
		

		return respWrap;
	}






	// ------------ Wrapper List ----------- //

	public class RequestWrap {

		@AuraEnabled
		public String parentRelationFieldname { get; set; }

		@AuraEnabled
		public String fieldsToQuery { get; set; }

		@AuraEnabled
		public String parentRecordId { get; set; }

		@AuraEnabled
		public String objectToQueryFrom { get; set; }

		@AuraEnabled
		public String additionalFilters { get; set; }

		

		public RequestWrap() {
		}
	}

	public class RespondWrap {

		@AuraEnabled public
		List<SObject> relatedRecList { get; set; }
		@AuraEnabled
		public Integer totalRecords = 0;
		

		public RespondWrap() {
			relatedRecList = new List<SObject>();
		}
	}

}