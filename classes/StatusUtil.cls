public without sharing class StatusUtil {
    public static set<string> getAllStatus(){
        set<string> setAllStatus = new set<String>();
        for(Status__c stat:[select id,name from Status__c]){
            setAllStatus.add(stat.name);
        }
        system.debug('setAllStatus====>'+setAllStatus);
        return setAllStatus;
    }
}