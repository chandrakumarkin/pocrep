/**
 * @description       : 
 * @author            : Shoaib Tariq
 * @group             : 
 * @last modified on  : 04-19-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   04-13-2021   Shoaib Tariq   Initial Version
**/
global with sharing class Application_KPI_BatchCls implements Database.Batchable<sobject>,Schedulable{
  
    
    global void execute(SchedulableContext sc) {
      Application_KPI_BatchCls apiBatch = new Application_KPI_BatchCls(); 
      database.executebatch(apiBatch);
    }

    global List<HexaBPM__Service_Request__c> start(Database.BatchableContext BC) {
          
        DateTime d1 = system.now();
        DateTime d2 = system.now()-1;
        return [Select id,HexaBPM__Closed_Date_Time__c,Total_Application_Time_hrs__c,HexaBPM__Submitted_DateTime__c from HexaBPM__Service_Request__c where Total_Application_Time_hrs__c = null AND HexaBPM__Closed_Date_Time__c <:d1 AND HexaBPM__Closed_Date_Time__c >:d2 AND HexaBPM__Submitted_DateTime__c != NULL];         
    }

  
    global Void execute(Database.BatchableContext BC,list<HexaBPM__Service_Request__c> SRBatchList){

        Map<string,datetime> map_end_date                = new Map<string,datetime>();
        Map<string,datetime> map_submitted_date          = new Map<string,datetime>();
        Map<string,BusinessHours> map_businessHour       = new Map<string,BusinessHours>();
        List<HexaBPM__Service_Request__c> hexaBPMList    = new List<HexaBPM__Service_Request__c>();
        
        for(HexaBPM__Service_Request__c lstSR :SRBatchList){

            map_end_date.put(lstSR.id,lstSR.HexaBPM__Closed_Date_Time__c);
            map_submitted_date.put(lstSR.id,lstSR.HexaBPM__Submitted_DateTime__c);
        }

        for(HexaBPM__Service_Request__c lstSR :SRBatchList){
           lstSR.Total_Application_Time_hrs__c = getTotalHours(map_end_date,map_submitted_date,lstSR);
           hexaBPMList.add(lstSR);
        }

       update hexaBPMList;
    }

    global void finish(Database.BatchableContext BC){
        
      
    }

    private static Decimal getTotalHours(Map<string,datetime> map_end_date,Map<string,datetime> map_submitted_date,HexaBPM__Service_Request__c thisSr){
        
        decimal totalHours = 0;
        ID BHid                 = id.valueOf(Label.Business_Hours_Id);
        string businessHourName = 'ROC';
         
        ID businessHoursId = String.isNotBlank(businessHourName) && Business_Hours__c.getAll().containsKey(businessHourName) ?  Id.valueOf(Business_Hours__c.getInstance(businessHourName).Business_Hours_Id__c) : Id.ValueOf( [SELECT Id FROM BusinessHours WHERE Name =: businessHourName limit 1].Id);
        BHId = businessHoursId;
    
        if(BHid!=null){       
            totalHours  =    ((decimal)((decimal)BusinessHours.diff(BHId,map_submitted_date.get(thisSr.Id),map_end_date.get(thisSr.Id))/3600000));
        }

        return totalHours;
    }
        
}