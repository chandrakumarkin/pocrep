@isTest(SeeAllData=false)
private class TestRocOperatingLocationsAdd_Cls {

  static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_of_Address_Details')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Change Of Business Activity';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Application_of_Registration';
        insert objPageFlow;
        
        Page__c objPage = new Page__c();
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        insert objPage;
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        
        Account objAccountHost = new Account();
        objAccountHost.Name = 'Test Hosting Company';
        objAccountHost.E_mail__c = 'test@test.com';
        objAccountHost.BP_No__c = '001235';
        objAccountHost.Company_Type__c = 'Financial - related';
        objAccountHost.Sector_Classification__c = 'Investment Fund';
        objAccountHost.Corporate_Service_Provider__c = true;
        insert objAccountHost;
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'DIFC';
        objBuilding.Building_No__c = '1234';
        objBuilding.Company_Code__c = '5300';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '2nd Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        objUnit = new  Unit__c();
        objUnit.Name = '1227';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '3rd Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        insert lstUnits;
        
        Operating_Location__c objOL = new Operating_Location__c();
        objOL.Account__c = objAccount.Id;
        objOL.Unit__c = lstUnits[0].Id;
        objOL.Status__c = 'Active';
        objOL.Type__c = 'Purchased';
        insert objOL;
        
        objOL = new Operating_Location__c();
        objOL.Account__c = objAccountHost.Id;
        objOL.Unit__c = lstUnits[2].Id;
        objOL.Status__c = 'Active';
        objOL.Type__c = 'Purchased';
        objOl.IsRegistered__c = true;
        insert objOL;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.RecordTypeId = mapRecordTypeIds.get('Application_of_Registration');
        insert objSR;
        test.startTest();
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('PageId',objPage.Id);
        
        RocOperatingLocationsAdd_Cls objOperatingLocations = new RocOperatingLocationsAdd_Cls();
        objOperatingLocations.CreateAmendments();
        objOperatingLocations.SelectedType = 'Purchased';
        objOperatingLocations.AddNewOperatingLocation();
        objOperatingLocations.SelectedBuilding = objBuilding.Id;
        objOperatingLocations.getUnits();
        objOperatingLocations.SelectedUnit = lstUnits[1].Id;
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.AmendmentAOR.Phone__c = '+97123';
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.AmendmentAOR.Phone__c = '+9711234567890';
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.AmendmentAOR.PO_Box__c = '123456';
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.CancelOperatingLocation();
        
        //Leased Properies
        objOperatingLocations.SelectedType = 'Leased';
        objOperatingLocations.AddNewOperatingLocation();
        objOperatingLocations.SelectedBuilding = objBuilding.Id;
        objOperatingLocations.getUnits();
        objOperatingLocations.SelectedUnit = lstUnits[2].Id;
        objOperatingLocations.SaveOperatingLocation();
        
        //Sharing Properties
        objOperatingLocations.CancelOperatingLocation();
        objOperatingLocations.SearchHostingCompanies();
        objOperatingLocations.SelectedType = 'Sharing';
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.HostingCompanyId = objAccountHost.Id;
        objOperatingLocations.SelectedType = 'Sharing';
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.SelectedType = 'Sharing';
        objOperatingLocations.HostingCompanyId = objAccountHost.Id;
        objOperatingLocations.SaveOperatingLocation();
        for(RocOperatingLocationsAdd_Cls.ExistingWrapper obj : objOperatingLocations.ExistingLocations){
            if(obj.Amendment.Operating_Type__c == 'Sharing'){
                objOperatingLocations.RowNo = obj.Index;
            }
        }
        objOperatingLocations.inactivateOperatingLocation();
        
        objOperatingLocations.ExistingOperatingLocations();
        objOperatingLocations.RowNo = 0;
        objOperatingLocations.EditRow();
        objOperatingLocations.CancelRow();
        objOperatingLocations.SaveRow();
        objOperatingLocations.inactivateOperatingLocation();
        
        objOperatingLocations.strNavigatePageId = objPage.Id;
        objOperatingLocations.strActionId = objSD.Id;
        
        objOperatingLocations.goTopage();
        objOperatingLocations.getDyncPgMainPB();
        objOperatingLocations.ExistingOperatingLocations();
        objOperatingLocations.RowNo = 0;
        objOperatingLocations.ExistingLocations[0].Amendment.IsRegistered__c = true;
        
        testPopulateFundCompanies(objSr.Id,ObjPageFlow.Id,objPage.Id,objAccountHost.Id);
        
        testInvestmentFund(objSr.Id,ObjPageFlow.Id,objPage.Id);
       
        test.stopTest();
    }
    
    
      static testMethod void myUnitTestFINAL2() {
        // TO DO: implement unit test
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_of_Address_Details')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Change Of Business Activity';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Application_of_Registration';
        insert objPageFlow;
        
        Page__c objPage = new Page__c();
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        insert objPage;
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        
        Account objAccountHost = new Account();
        objAccountHost.Name = 'Test Hosting Company';
        objAccountHost.E_mail__c = 'test@test.com';
        objAccountHost.BP_No__c = '001235';
        objAccountHost.Company_Type__c = 'Financial - related';
        objAccountHost.Sector_Classification__c = 'Investment Fund';
        objAccountHost.Corporate_Service_Provider__c = true;
        insert objAccountHost;
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'DIFC';
        objBuilding.Building_No__c = '1234';
        objBuilding.Company_Code__c = '5300';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '2nd Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        objUnit = new  Unit__c();
        objUnit.Name = '1227';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '3rd Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        insert lstUnits;
        
        Operating_Location__c objOL = new Operating_Location__c();
        objOL.Account__c = objAccount.Id;
        objOL.Unit__c = lstUnits[0].Id;
        objOL.Status__c = 'Active';
        objOL.Type__c = 'Leased';
        insert objOL;
        
        objOL = new Operating_Location__c();
        objOL.Account__c = objAccountHost.Id;
        objOL.Unit__c = lstUnits[2].Id;
        objOL.Status__c = 'Active';
        objOL.Type__c = 'Purchased';
        objOl.IsRegistered__c = true;
        insert objOL;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.RecordTypeId = mapRecordTypeIds.get('Application_of_Registration');
        insert objSR;
        test.startTest();
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('PageId',objPage.Id);
        
        RocOperatingLocationsAdd_Cls objOperatingLocations = new RocOperatingLocationsAdd_Cls();
        objOperatingLocations.AddNewOperatingLocation();
        objOperatingLocations.CreateAmendments();
        objOperatingLocations.SelectedType = 'Purchased';
        objOperatingLocations.AddNewOperatingLocation();
        objOperatingLocations.SelectedBuilding = objBuilding.Id;
        objOperatingLocations.getUnits();
        objOperatingLocations.SelectedUnit = lstUnits[1].Id;
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.AmendmentAOR.Phone__c = '+97123';
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.AmendmentAOR.Phone__c = '+9711234567890';
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.AmendmentAOR.PO_Box__c = '123456';
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.CancelOperatingLocation();
        
        //Leased Properies
        objOperatingLocations.SelectedType = 'Leased';
        objOperatingLocations.AddNewOperatingLocation();
        objOperatingLocations.SelectedBuilding = objBuilding.Id;
        objOperatingLocations.getUnits();
        objOperatingLocations.SelectedUnit = lstUnits[2].Id;
        objOperatingLocations.SaveOperatingLocation();
        
        //Sharing Properties
        objOperatingLocations.CancelOperatingLocation();
        objOperatingLocations.SearchHostingCompanies();
        objOperatingLocations.SelectedType = 'Sharing';
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.HostingCompanyId = objAccountHost.Id;
        objOperatingLocations.SelectedType = 'Sharing';
        objOperatingLocations.SaveOperatingLocation();
        objOperatingLocations.SelectedType = 'Sharing';
        objOperatingLocations.HostingCompanyId = objAccountHost.Id;
        objOperatingLocations.SaveOperatingLocation();
        for(RocOperatingLocationsAdd_Cls.ExistingWrapper obj : objOperatingLocations.ExistingLocations){
            if(obj.Amendment.Operating_Type__c == 'Sharing'){
                objOperatingLocations.RowNo = obj.Index;
            }
        }
        objOperatingLocations.inactivateOperatingLocation();
        
        objOperatingLocations.ExistingOperatingLocations();
        objOperatingLocations.RowNo = 0;
        objOperatingLocations.EditRow();
        objOperatingLocations.CancelRow();
        objOperatingLocations.SaveRow();
        objOperatingLocations.inactivateOperatingLocation();
        
        objOperatingLocations.strNavigatePageId = objPage.Id;
        objOperatingLocations.strActionId = objSD.Id;
        
        objOperatingLocations.goTopage();
        objOperatingLocations.getDyncPgMainPB();
        objOperatingLocations.ExistingOperatingLocations();
        objOperatingLocations.RowNo = 0;
        objOperatingLocations.ExistingLocations[0].Amendment.IsRegistered__c = true;
        
        testPopulateFundCompanies(objSr.Id,ObjPageFlow.Id,objPage.Id,objAccountHost.Id);
        
        testInvestmentFund(objSr.Id,ObjPageFlow.Id,objPage.Id);
       
        test.stopTest();
    }
    @future
    private static void testPopulateFundCompanies(String srId, String pageId, String flowId, String accountId){
        
        Apexpages.currentPage().getParameters().put('id',srId);
        Apexpages.currentPage().getParameters().put('FlowId',pageId);
        Apexpages.currentPage().getParameters().put('PageId',flowId);
        
        RocOperatingLocationsAdd_Cls objOperatingLocations = new RocOperatingLocationsAdd_Cls();
        
        objOperatingLocations.SelectedInvestmentFundType = 'Yes';
        
        objOperatingLocations.populateFundCompanies();
        
        objOperatingLocations.SelectedInvestmentFundType = 'No';
        
        objOperatingLocations.populateFundCompanies();
        
        objOperatingLocations.SelectedInvestmentFundType = '';
        
        objOperatingLocations.populateFundCompanies();
        
        objOperatingLocations.fatchFundCompanies('test');
        
        objOperatingLocations.SelectedInvestmentFundCompany = accountId;
        
        objOperatingLocations.populateContactDetailsOfFundCompany();
        objOperatingLocations.getDyncPgMainPB();
    }
    
    @future
    private static void testInvestmentFund(String srId, String pageId, String flowId){
        
        Apexpages.currentPage().getParameters().put('id',srId);
        Apexpages.currentPage().getParameters().put('FlowId',pageId);
        Apexpages.currentPage().getParameters().put('PageId',flowId);
        
        RocOperatingLocationsAdd_Cls objOperatingLocations = new RocOperatingLocationsAdd_Cls();
        
        delete [select id from Amendment__c where Amendment_Type__c ='Operating Location' AND ServiceRequest__c=:srId AND Operating_Location_Status__c != 'Inactive'];
        
        objOperatingLocations.SelectedInvestmentFundType = 'Test Company';
        objOperatingLocations.SelectedInvestmentFundCompany = 'Test Data';
        
        objOperatingLocations.SaveInvestmentFundOperatingLocation();
        
    }
    
    static testmethod void test1(){
    	
    	map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_of_Address_Details')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
    	RocOperatingLocationsAdd_Cls objOperatingLocations = new RocOperatingLocationsAdd_Cls();
    	objOperatingLocations.SelectedInvestmentFundType = 'Test Company';
        objOperatingLocations.SelectedInvestmentFundCompany = 'Test Data';
        objOperatingLocations.isGP =true;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
		objAccount.License_Activity__c = '345';      
        insert objAccount;
        
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('Change_of_Address_Details');
        objSR.Statement_of_Undertaking__c = false;
        insert objSR;
        
        objOperatingLocations.SaveInvestmentFundOperatingLocation();
    }
   
    static testmethod void TestBackOfficeSR(){
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_of_Address_Details')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Sys_Office__c = 'Level - 14';
        objAccount.Building_Name__c = 'The Gate';
        objAccount.Street__c = 'DIFC';
        objAccount.PO_Box__c = '12345';
        objAccount.City__c = 'Dubai';
        objAccount.Emirate__c = 'Dubai';
        objAccount.Country__c = 'UAE';
        insert objAccount;
        
        Account objAccountHost = new Account();
        objAccountHost.Name = 'Test Hosting Company';
        objAccountHost.E_mail__c = 'test@test.com';
        objAccountHost.BP_No__c = '001235';
        objAccountHost.Company_Type__c = 'Financial - related';
        objAccountHost.Sector_Classification__c = 'Authorised Market Institution';
        objAccountHost.Corporate_Service_Provider__c = true;
        insert objAccountHost;
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'DIFC';
        objBuilding.Building_No__c = '1234';
        objBuilding.Company_Code__c = '5300';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '2nd Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        objUnit = new  Unit__c();
        objUnit.Name = '1227';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '3rd Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        insert lstUnits;
        
        Operating_Location__c objOL = new Operating_Location__c();
        objOL.Account__c = objAccount.Id;
        objOL.Unit__c = lstUnits[0].Id;
        objOL.Status__c = 'Active';
        objOL.Type__c = 'Leased';
        insert objOL;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.RecordTypeId = mapRecordTypeIds.get('Change_of_Address_Details');
        insert objSR;
        
        Amendment__c objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.Amendment_Type__c = 'Operating Location';
        objAmd.Operating_Location_Status__c = 'Active';
        objAmd.Operating_Type__c = 'Leased';
        objAmd.Unit__c = lstUnits[0].Id;
        objAmd.IsRegistered__c = true;
        insert objAmd;
        
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        
        RocOperatingLocationsAdd_Cls objCls = new RocOperatingLocationsAdd_Cls();
        objCls.BackendSRUpdate();
        objCls.ExistingOperatingLocations();
        objCls.UpdateAddressOnSR();
        objCls.DynamicButtonAction(); //Added by kaavya
         
        objAmd.Operating_Type__c = 'Sharing';
        objAmd.Unit__c = lstUnits[0].Id;
        objAmd.IsRegistered__c = true;
        objAmd.Hosting_Company__c = objAccountHost.Id;
        update objAmd;
        objCls.ExistingOperatingLocations();
        objCls.UpdateAddressOnSR();
        List<Amendment__c> lstAmendments= new List<Amendment__c >{objAmd};
        objCls.PendingOperatingLocations = lstAmendments;
    }
    
    static testmethod void difcFinalTest(){
    	
    	 map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='Application_of_Registration' AND SobjectType='Service_Request__c' AND IsActive=true]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
       
        
    	Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1 Ltd';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Sys_Office__c = 'Level - 14';
        objAccount.Building_Name__c = 'The Gate';
        objAccount.Street__c = 'DIFC';
        objAccount.PO_Box__c = '12345';
        objAccount.City__c = 'Dubai';
        objAccount.Emirate__c = 'Dubai';
        objAccount.Country__c = 'UAE';
        objAccount.Corporate_Service_Provider__c = true;
        insert objAccount;
        
          
         Building__c objBuilding = new Building__c();
        objBuilding.Name = 'DIFC';
        objBuilding.Building_No__c = '1234';
        objBuilding.Company_Code__c = '5300';
        insert objBuilding;
        
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Custoer 2';
        objAccount1.E_mail__c = 'test@test.com';
        objAccount1.BP_No__c = '001235';
        objAccount1.Company_Type__c = 'Financial - related';
        objAccount1.Sector_Classification__c = 'Authorised Market Institution';
        objAccount1.Sys_Office__c = 'Level - 14';
        objAccount1.Building_Name__c = 'The Gate';
        objAccount1.Street__c = 'DIFC';
        objAccount1.PO_Box__c = '12345';
        objAccount1.City__c = 'Dubai';
        objAccount1.Emirate__c = 'Dubai';
        objAccount1.Country__c = 'UAE';
        objAccount1.Corporate_Service_Provider__c = true;
        objAccount1.Registration_License_No__c = '4444';
        objAccount1.ROC_Status__c = 'Active';
        insert objAccount1;
        
        
         contact con = new contact();
	     con.AccountId =objAccount.Id;
	     con.role__c ='Company Services';   
	     con.firstname ='test';  
	     con.lastname ='test';
	     insert con;
     
        User usr = new User();
        usr.IsActive = true;
        usr.CompanyName = objAccount.id;
        usr.username = 'testDIFC@difcportal.ae';
        usr.contactId=con.Id;
        usr.firstname='Test';
        usr.lastname='Test';
        usr.email='test@difc.ae';
        usr.Title = 'Mr.';
        usr.communityNickname = (usr.firstname +string.valueof(Math.random()).substring(4,9));
        usr.alias = string.valueof(usr.firstname.substring(0,1) + string.valueof(Math.random()).substring(4,9));            
        usr.profileid = Label.Profile_ID;
        usr.emailencodingkey='UTF-8';
        usr.languagelocalekey='en_US';
        usr.localesidkey='en_GB';
        usr.timezonesidkey='Asia/Dubai';
        usr.Community_User_Role__c = 'Employee Services;Company Services';
       // usr.UserType = 'Customer Portal User';
        insert usr;
        
        
    	Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.RecordTypeId = mapRecordTypeIds.get('Application_of_Registration');
        objSR.Legal_Structures__c = 'Ltd';
        objSR.Statement_of_Undertaking__c =true;
        insert objSR;
        system.debug(')))))))'+objSR+'((***))'+objSR.RecordType.Name);
         ApexPages.currentPage().getParameters().put('Id',objSR.Id);
          list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '2nd Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        objUnit = new  Unit__c();
        objUnit.Name = '1227';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '3rd Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        lstUnits.add(objUnit);
        insert lstUnits;
        
        Amendment__c objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.Amendment_Type__c = 'Operating Location';
        objAmd.Operating_Location_Status__c = 'Active';
        objAmd.Operating_Type__c = 'Leased';
        objAmd.Unit__c = lstUnits[0].Id;
        objAmd.IsRegistered__c = true;
        objAmd.Phone__c = '+971544621510';
        objAmd.PO_Box__c = '9999';
        objAmd.Status__c = 'Removed';
        insert objAmd;
        
         Lease__c lease= new Lease__c ();
        lease.Account__c = objAccount.ID;
        lease.status__c = 'Active';
        lease.End_Date__c = Date.Today().AddDays(5);
        lease.Square_Feet__c = '58';
        lease.Type__c = 'Leased Property';
        lease.Is_License_to_Occupy__c  = true;
        lease.No_of_Desks__c = 2;
        lease.Lease_Types__c ='Leased Property';
        
        insert lease;
        
    	/*Section_Detail__c details = new Section_Detail__c();
    	details.Navigation_Direction__c = 'Forward';
    	details.Name = Test;
    	INSERT details;*/
    	
    	 Building__c b = new Building__c();
        b.Company_Code__c = '2300';
        insert b;
        
        Unit__c unit = new Unit__c();
        unit.Building__c = b.ID;
        unit.Unit_Usage_Type__c='retail';
        
        insert unit;
        
        Tenancy__c ten = new Tenancy__c ();
        ten.End_Date__c = Date.Today().AddDays(5);
        ten.Unit__c = unit.ID;
        ten.lease__c = lease.ID;
        ten.LeaseRenewalEmailSent__c=false;
        insert ten;
        
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = lease.Id;
        objLP.Account__c = objAccount.Id;
        objLP.Unit__c = unit.Id;
        objLP.SAP_PARTNER__c = '12345';
        objLP.Start_date__c = system.today();
        objLP.End_Date__c = system.today().addYears(3);
        objLP.Square_Feet__c = '123456';
        objLP.Lease_Types__c = 'Retail Lease';
        objLP.Industry__c = 'Kiosik';
        objLP.Type_of_Lease__c = 'Leased';
        objLP.Status__c = 'Active';
        INSERT objLP;
        
    	RocOperatingLocationsAdd_Cls objCls = new RocOperatingLocationsAdd_Cls();
    	objCls.HostingCompanyName = 'Test Customer 2';
    	objCls.ContactNumber = '+97177665544332';
    	RocOperatingLocationsAdd_Cls.TenancyWrapper wrapper1 = new RocOperatingLocationsAdd_Cls.TenancyWrapper();
    	
    	RocOperatingLocationsAdd_Cls.OperatingWrapper wrapper = new RocOperatingLocationsAdd_Cls.OperatingWrapper();
    	wrapper.LocationType = 'Sharing';
    	wrapper.HostingName = 'Test Account 1';
    	wrapper.Tenancy = ten;
    	
    	wrapper1.Checked = true;
    	wrapper1.Tenancy = ten; 
    	wrapper1.LeasePartner =  objLP;
    	//objCls.strActionId = details.Id;
    	objCls.DynamicButtonAction();
    	objCls.CorporateServiceProviderName = 'Test';
    	objCls.checkMultipleLeases();	
    	objCls.AddressType = 'Change Registered Address';
    	objCls.POBox = '44333';
    	objCls.CorporateServiceProviderId ='Test';
    	objCls.SelectedInvestmentFundType = 'Yes';
    	objCls.SelectedInvestmentFundCompany = 'Yes';
    	objCls.AddNewOperatingLocation();
    	objCls.isGP = false;
    	objCls.isFundManagerNotExist = false;
    	objCls.UpdateContactNumber();
    	objCls.ChangeAddressType();
    	objCls.UpdatePOBox();
    	objCls.CancelAddressChange();
    	objCls.SearchCorporateProviders();
    	objCls.SearchHostingCompanies();
    	objCls.OtherLocationPanChanged();
    	objCls.SaveInvestmentFundOperatingLocation();
    	objCls.SaveOperatingLocation();
    	system.runas(usr){
    		objCls.CreateAmendments();
    	}
    }
   	static testmethod void testfinal3(){
		
			RocOperatingLocationsAdd_Cls objCls = new RocOperatingLocationsAdd_Cls();
			objCls.AddressType = 'Change Registered Address';
			objCls.ChangeAddressType();
    		//objCls.ContactNumber = '+97177665544332';
		   	objCls.UpdateContactNumber();	
   	}
   	
   	static testmethod void testfinal4(){
		
			RocOperatingLocationsAdd_Cls objCls = new RocOperatingLocationsAdd_Cls();
    	    objCls.ContactNumber = '77665544332';
		   	objCls.UpdateContactNumber();	
		   	objCls.POBox = '88889';
		   	objCls.AddressType = 'Change the PO Box';
			objCls.ChangeAddressType();
   	}
   	
   	static testmethod void testfinal5(){
		
			RocOperatingLocationsAdd_Cls objCls = new RocOperatingLocationsAdd_Cls();
    	    objCls.ContactNumber = '77665544332';
		   	objCls.UpdateContactNumber();	
		   	objCls.POBox = '88889';
		   	objCls.AddressType = 'Change the Company Contact Numbe';
			objCls.ChangeAddressType();
   	}
   	
}