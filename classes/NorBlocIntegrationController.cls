/******************************************************************************************
 *  Class Name  : NorBlocIntegration
 *  Author      : Sai kalyan Sanisetty 
 *  Company     : DIFC
 *  Date        : 21 NOV 2019        
 *  Description :                   
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1    21 NOV 2019           Sai                Initial Version  
*******************************************************************************************/


public class NorBlocIntegrationController{
    
    @future(callout=true)
    public static void norBlocIntegration(String accountID,Integer turnOverVal){
      
      User eachUser = new User();
      String importCustomerData;
      eachUser = [SELECT Id,Email,Contact.Email,Contact.AccountId FROM User where ID=:userinfo.getuserID()];
      if(eachUser.Contact.Email !=null || test.isRunningTest()){ //selva
        try{
          Http http = new Http();
          HttpRequest authorizationRequest = new HttpRequest();
          authorizationRequest.setEndpoint(Label.NorBlocauthorizationHeader);
          authorizationRequest.setMethod('POST');
          authorizationRequest.setHeader('Authorization', Label.NorBlocAuthorization);
              authorizationRequest.setHeader('Content-Type', 'application/x-www-form-urlencoded');
              authorizationRequest.setHeader('Content-Length', '0');
              authorizationRequest.setTimeout(120000);
              String requestBody = 'grant_type=client_credentials&scope=customer.manage,customer.import';
              authorizationRequest.setBody(requestBody);  
              HttpResponse authorizationresponse = http.send(authorizationRequest);
              if(authorizationresponse.getstatuscode() == 200){
                
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(authorizationresponse.getBody());
                  Object BearerCode1 = results.get('access_token');
                  String BearerCode = 'Bearer '+String.valueOf(BearerCode1);
                  Http httpImportCustomer = new Http();
                  HttpRequest importCustomerRequest = new HttpRequest();
                  importCustomerRequest.setEndpoint(Label.NorBlocImportCustomerData);
            importCustomerRequest.setMethod('PUT');
            importCustomerRequest.setHeader('Authorization', BearerCode);
                importCustomerRequest.setHeader('Content-Type', 'application/json');
                importCustomerRequest.setTimeout(120000);
                importCustomerData = NORBLOCLeadJSONController.norBlockLeadJSONMethod(accountID,turnOverVal);
                system.debug('@@####'+importCustomerData);
                importCustomerRequest.setBody(importCustomerData);
                 
                HttpResponse importCustomerresponse = http.send(importCustomerRequest);
                String output = importCustomerresponse.getBody();
          if(importCustomerresponse.getstatuscode() == 200 || importCustomerresponse.getstatuscode() == 204){
                  Http httpOTP = new Http();
              HttpRequest authorizationRequestOTP = new HttpRequest();
              authorizationRequestOTP.setEndpoint(Label.SendOTPEndpoint);
              authorizationRequestOTP.setMethod('POST');
              authorizationRequestOTP.setHeader('Authorization',Label.NorBlocAuthorization);
                  authorizationRequestOTP.setHeader('Content-Type', 'application/x-www-form-urlencoded');
                        String requestBodyOTP = '';
                        if(eachUser.Contact.Email !=null){
                          requestBodyOTP = 'grant_type=client_credentials&email='+EncodingUtil.urlEncode(eachUser.Contact.Email, 'UTF-8');
                        }
                        authorizationRequestOTP.setBody(requestBodyOTP); 
                  authorizationRequestOTP.setTimeout(120000);
                  system.debug('!!@##$##'+authorizationRequestOTP);
                  HttpResponse authorizationresponseOTP = httpOTP.send(authorizationRequestOTP);
                  
                  if(authorizationresponseOTP.getstatuscode() == 200 || authorizationresponseOTP.getstatuscode() == 204){
                    Account eachAccount = new Account();
                    eachAccount = [SELECT Id,is_Bank_Account_Created__c FROM Account where Id=:eachUser.Contact.AccountId];
                    eachAccount.is_Bank_Account_Created__c = true;
                    eachAccount.Turn_Over_Amount__c = turnOverVal;
                    UPDATE eachAccount;
                    
                  if(String.isNotBlank(importCustomerData)){
                        Attachment attachment = new Attachment();
                  attachment.Body = Blob.valueOf(importCustomerData);
                  attachment.Name = String.valueOf('Open a Bank Account.txt');
                  attachment.ParentId = eachUser.Contact.AccountId; 
                  insert attachment;
                  
                 
                  }
                     
                  }
                 
          }
        
              }
        
       } 
       
        catch(Exception ex){
          system.debug('$$$$$$$'+ex.getmessage());
        } 
      }
    }
}