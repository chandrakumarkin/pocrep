global class PortalBalanceNew {
    
   private static string decryptedSAPPassWrd = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456' ; // V1.6
    
    @future(callout=true)
    public static void getAccountBalanceFuture(list<string> lstAccountIds,string CompanyType){
        AccountBalanceInfoCls.getAccountBalance(lstAccountIds, CompanyType);
    }
    
    webservice static string getAccountBalance(list<string> lstAccountIds,string CompanyType){
        
        try{
            
            //v1.1
            if(Block_Trigger__c.getAll() != null && Block_Trigger__c.getAll().get('Block').Block_Web_Services__c){
                return 'Please try after sometime, system under the maintenance. Apologies for the inconvenience.';
            }
            
            if(lstAccountIds != null && !lstAccountIds.isEmpty()){
                
                list<AccountBalenseService.ZSF_S_ACC_BALANCE> items = new list<AccountBalenseService.ZSF_S_ACC_BALANCE>();
                
                AccountBalenseService.ZSF_S_ACC_BALANCE item;
                
                // CompanyType = (CompanyType!=null && CompanyType != '')?CompanyType:'5100';
                CompanyType = (CompanyType!=null && CompanyType != '')?CompanyType:System.label.CompanyCode2100;
                for(Account objAccount : [select Id,BP_No__c from Account where Id IN : lstAccountIds]){
                    
                    item = new AccountBalenseService.ZSF_S_ACC_BALANCE();
                    item.KUNNR = objAccount.BP_No__c;
                    item.BUKRS = CompanyType;
                    items.add(item);
                }
                
                AccountBalenseService.ZSF_TT_ACC_BALANCE objTTActBal = new AccountBalenseService.ZSF_TT_ACC_BALANCE();
                
                objTTActBal.item = items;
                
                AccountBalenseService.account objActBal = new AccountBalenseService.account();
                
                objActBal.timeout_x = 100000;
                objActBal.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;           
                objActBal.inputHttpHeaders_x= new Map<String, String>();
                //Blob headerValue = Blob.valueOf('informatica:1234567');
             // Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c); Commented for V1.6
                
                Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+PortalBalanceNew.decryptedSAPPassWrd); // Added for V1.6
                
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                
                objActBal.inputHttpHeaders_x.put('Authorization', authorizationHeader);
                
                AccountBalenseService.Z_SF_ACCOUNT_BALANCEResponse_element objAccountBalResponse= objActBal.Z_SF_ACCOUNT_BALANCE(objTTActBal);
                
                map<string,decimal[]> mapAccountBalInfo = new map<string,decimal[]>();
                
                if(objAccountBalResponse.EX_SF_RS_ACC != null && objAccountBalResponse.EX_SF_RS_ACC.item != null){
                    
                    for(AccountBalenseService.ZSF_S_ACC_BAL objAccountBalInfo : objAccountBalResponse.EX_SF_RS_ACC.item){
                        
                        system.debug('objAccountBalInfo $$$ '+objAccountBalInfo);
                        
                        if(objAccountBalInfo.KUNNR != null){
                            
                            decimal[] dActBals;
                            
                            dActBals = mapAccountBalInfo.containsKey(objAccountBalInfo.KUNNR) ? mapAccountBalInfo.get(objAccountBalInfo.KUNNR) : new decimal[]{0,0,0,0}; // V1.5 - Claude - Added another value to collection
                                
                            if(objAccountBalInfo.UMSKZ == 'D')
                                dActBals[0] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                            if(objAccountBalInfo.UMSKZ == 'H')
                                dActBals[1] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                            if(objAccountBalInfo.UMSKZ == '9')
                                dActBals[2] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                            if(objAccountBalInfo.UMSKZ == 'O') // V1.5 - Claude - Added new type per SAP adjustments
                                dActBals[3] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                            
                            mapAccountBalInfo.put(objAccountBalInfo.KUNNR,dActBals);
                        }
                        
                    }//end of for 
                    
                    if(!mapAccountBalInfo.isEmpty()){
                        
                        list<Account> lstUpdateAccounts = new list<Account>();
                        list<Log__c> lstLogs = new list<Log__c>();
                        
                        Account objAccount;
                        
                        for(Account objAccountTemp :[select Id,
                                                            BP_No__c,
                                                            RecordType.DeveloperName // V1.5 - Claude - Added record type for FIT-OUT contractors  
                                                            from Account where BP_No__c IN : mapAccountBalInfo.keySet() AND BP_No__c != null]){
                            
                            objAccount = new Account(Id=objAccountTemp.Id);
                            
                            system.debug('objAccount.BP_No__c '+objAccountTemp.BP_No__c);
                            system.debug('objAccount.BP_No__c '+mapAccountBalInfo.get(objAccountTemp.BP_No__c));
                            
                            /* V1.5 - Claude */
                            objAccount.Portal_Balance__c = String.isNotBlank(objAccountTemp.RecordType.DeveloperName) && 
                                                        (objAccountTemp.RecordType.DeveloperName.equals('Contractor_Account') || 
                                                            objAccountTemp.RecordType.DeveloperName.equals('Event_Account')) ? 
                                                            mapAccountBalInfo.get(objAccountTemp.BP_No__c)[3] : 
                                                            mapAccountBalInfo.get(objAccountTemp.BP_No__c)[0];
                                                            
                            /* V1.5 - Claude */
                            objAccount.PSA_Actual__c = mapAccountBalInfo.get(objAccountTemp.BP_No__c)[1];
                            objAccount.PSA_Guarantee__c = mapAccountBalInfo.get(objAccountTemp.BP_No__c)[2];
                            objAccount.Balance_Updated_On__c = system.now();
                            
                            lstUpdateAccounts.add(objAccount);
                        }
                        
                        if(!lstUpdateAccounts.isEmpty()) update lstUpdateAccounts;
                    }
                    
                }//end of if
            }
            
        } catch(Exception ex){
            
            system.debug('Exception is '+ex.getMessage()+ ' at >>' + ex.getLineNumber());
            
            Log__c objLog = new Log__c();
            objLog.Type__c = 'Account Balance Callout';
            objLog.Description__c = ex.getMessage();
            
            insert objLog;
            
            return 'Fail';
        }
        
        return 'Success';
    }
    
    webservice static string getPortalBalance(string AccountBpNo){
        
        try{
            //v1.1
            if(Block_Trigger__c.getAll() != null && Block_Trigger__c.getAll().get('Block') != null && Block_Trigger__c.getAll().get('Block').Block_Web_Services__c){
                return 'Please try after sometime, system under the maintenance. Apologies for the inconvenience.';
            }
            
            if(AccountBpNo != null && AccountBpNo != ''){
                
                list<AccountBalenseService.ZSF_S_ACC_BALANCE> items = new list<AccountBalenseService.ZSF_S_ACC_BALANCE>();
                
                AccountBalenseService.ZSF_S_ACC_BALANCE item;
                
                item = new AccountBalenseService.ZSF_S_ACC_BALANCE();
                item.KUNNR = AccountBpNo;
                //item.BUKRS = '5000';
                item.BUKRS = System.label.CompanyCode5000;
                
                
                items.add(item);
                
                AccountBalenseService.ZSF_TT_ACC_BALANCE objTTActBal = new AccountBalenseService.ZSF_TT_ACC_BALANCE();
                
                objTTActBal.item = items;
                
                AccountBalenseService.account objActBal = new AccountBalenseService.account();
                
                objActBal.timeout_x = 100000;
                objActBal.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;           
                objActBal.inputHttpHeaders_x= new Map<String, String>();
                //Blob headerValue = Blob.valueOf('informatica:1234567');
             // Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c); Commented for V1.6
                
                Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+PortalBalanceNew.decryptedSAPPassWrd); // Added for V1.6
                
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                
                objActBal.inputHttpHeaders_x.put('Authorization', authorizationHeader);
                
                AccountBalenseService.Z_SF_ACCOUNT_BALANCEResponse_element objAccountBalResponse= objActBal.Z_SF_ACCOUNT_BALANCE(objTTActBal);
                
                map<string,decimal[]> mapAccountBalInfo = new map<string,decimal[]>();
                
                system.debug('objAccountBalResponse.EX_SF_RS_ACC $$$ '+objAccountBalResponse.EX_SF_RS_ACC);
                system.debug('objAccountBalResponse.EX_SF_RS_ACC.item $$$ '+objAccountBalResponse.EX_SF_RS_ACC.item);
                
                if(objAccountBalResponse.EX_SF_RS_ACC != null && objAccountBalResponse.EX_SF_RS_ACC.item != null){
                   
                    for(AccountBalenseService.ZSF_S_ACC_BAL objAccountBalInfo : objAccountBalResponse.EX_SF_RS_ACC.item){
                        
                        system.debug('objAccountBalInfo $$$ '+objAccountBalInfo);
                        
                        if(objAccountBalInfo.KUNNR != null){
                            
                            decimal[] dActBals;
                            
                            dActBals = mapAccountBalInfo.containsKey(objAccountBalInfo.KUNNR) ? mapAccountBalInfo.get(objAccountBalInfo.KUNNR) : new decimal[]{0,0,0,0}; // V1.6 - Claude - Added another value to collection
                                
                            if(objAccountBalInfo.UMSKZ == 'D')
                                dActBals[0] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                            if(objAccountBalInfo.UMSKZ == 'H')
                                dActBals[1] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                            if(objAccountBalInfo.UMSKZ == '9')
                                dActBals[2] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;    
                            if(objAccountBalInfo.UMSKZ == 'O')
                                dActBals[3] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                            
                            mapAccountBalInfo.put(objAccountBalInfo.KUNNR,dActBals);
                        }
                        
                    }//end of for 
                    
                    //added by Ravi on 19th May, to display the Visa Info
                    String CustomerId;
                    //Integer ExceptionalQuota = 0;
                    //string hostingCompanyID; // Added for hosting company  V1.3
                    
                    Boolean isContractor = false;
                    
                    Boolean hasEstablishmentCard = false;
                    
                    for(Account objAccount : [select Id,
                                                     Index_Card_Status__c,     // V1.8 - Claude - Added index card status for 'New Index Card' request
                                                     Exception_Visa_Quota__c,
                                                     RecordType.DeveloperName, // V1.4 - Claude - Added record type to check if contractor
                                                     Hosting_company__c from Account where BP_No__c=:AccountBpNo]){
                        
                        CustomerId = objAccount.Id;
                        
                        hasEstablishmentCard = String.isNotBlank(objAccount.Index_Card_Status__c) && !objAccount.Index_Card_Status__c.equals('Not Available') && !objAccount.Index_Card_Status__c.equals('Cancelled');
                        
                        isContractor = objAccount.RecordType.DeveloperName.equals('Contractor_Account') || objAccount.RecordType.DeveloperName.equals('Event_Account'); //V1.4 - Claude - Checks if the account is a contractor
                         
                        //hostingCompanyID = objAccount.Hosting_company__c;
                        //ExceptionalQuota = objAccount.Exception_Visa_Quota__c != null ? Integer.valueOf(objAccount.Exception_Visa_Quota__c) : 0;
                    }
                    
                    decimal[] VisaInfo = GsHelperCls.CalculateVisa(CustomerId);
                    
                    //V1.2
                   // decimal[] currentInfoCount =  GsHelperCls.getCurrentEmployeeCount(CustomerId); // Commented  as Per V1.3
                   // decimal[] pipeLineInfoCount =  GsHelperCls.getPipelineEmployeeCount(CustomerId); // Comented as per V1.3
                    decimal[] currentInfoCount =  GsHelperCls.getCurrentEmployeeCount(new set<string>{CustomerId}); // Modified as Per V1.3
                    decimal[] pipeLineInfoCount =  GsHelperCls.getPipelineEmployeeCount(new set<string>{CustomerId});  // Modified according to V1.3                    
                   
                    string rStr = '';
                    decimal PortalBalance = 0;
                    
                    if(!mapAccountBalInfo.isEmpty()){
                        
                        Account objAccount = new Account(BP_No__c=AccountBpNo);
                        
                        System.debug('Is Contractor? ' + isContractor);
                        System.debug('Account Balance Map: ' + mapAccountBalInfo.get(AccountBpNo));
                        
                        objAccount.Portal_Balance__c = isContractor ? mapAccountBalInfo.get(AccountBpNo)[3] : mapAccountBalInfo.get(AccountBpNo)[0];
                        
                        PortalBalance += isContractor ? mapAccountBalInfo.get(AccountBpNo)[3] : mapAccountBalInfo.get(AccountBpNo)[0];
                        
                        objAccount.PSA_Actual__c = mapAccountBalInfo.get(AccountBpNo)[1];
                        objAccount.PSA_Guarantee__c = mapAccountBalInfo.get(AccountBpNo)[2];
                        objAccount.Balance_Updated_On__c = system.now();
                        
                        //added on 19th May by Ravi
                        objAccount.Calculated_Visa_Quota__c = VisaInfo[0];
                        objAccount.Utilize_Visa_Quota__c = VisaInfo[1]+VisaInfo[3];
                        
                        //upsert objAccount BP_No__c;
                        
                        decimal[] dPSA = PSACalculation(CustomerId,(mapAccountBalInfo.get(AccountBpNo)[1] + mapAccountBalInfo.get(AccountBpNo)[2]),hasEstablishmentCard);
                        
                        if(!hasEstablishmentCard && dPSA[1] < 0) dPSA[1] = 0; // V1.8 - Claude - Added exemption for new establishment cards
                        
                        rStr += 'PSA Deposit : '+(dPSA[0])+'\n';
                        
                        if((dPSA[0]) >= 125000 ){
                            rStr += 'PSA Available : 125000\n';
                        }else{
                            rStr += 'PSA Available : '+dPSA[1]+' \n';
                        }
                        objAccount.Sponsored_Employee__c = currentInfoCount[0]+pipeLineInfoCount[0];
                        objAccount.GCC_Nationals__c = currentInfoCount[1]+pipeLineInfoCount[1];
                        objAccount.Seconded_Employees__c = currentInfoCount[2]+pipeLineInfoCount[2];
                        objAccount.ICD_Employees__c = currentInfoCount[4]+pipeLineInfoCount[4];
                        objAccount.Equity_Card_Holder__c = currentInfoCount[5]+pipeLineInfoCount[5];
                        objAccount.DL_Employee_Card__c	= currentInfoCount[6]+pipeLineInfoCount[6];
                        objAccount.PSA_Available__c = dPSA[1];
                        objAccount.PSA_Deposit_in_SAP__c = dPSA[0];
                        upsert objAccount BP_No__c; 
                         
                        rStr += 'Employment Quota : '+(VisaInfo[0]+VisaInfo[2])+'\n';
                        rStr += 'Number of Employees - Current : '+VisaInfo[1]+'\n';
                        rStr += '    Current - Sponsored : '+currentInfoCount[0]+'\n';
                        rStr += '    Current - GCC Nationals : '+currentInfoCount[1]+'\n';
                        rStr += '    Current - Seconded : '+currentInfoCount[2]+'\n';
                        rStr += '    Current - Temporary : '+currentInfoCount[3]+'\n';
                        rStr += '    Current - ICD : '+currentInfoCount[4]+'\n';
                        rStr += '    Current - Employee Card Non Sponsored : '+currentInfoCount[5]+'\n';
                        rStr += '    Current - DL Employee Card : '+currentInfoCount[6]+'\n';
                        rStr += 'Number of Employees - Pipeline : '+VisaInfo[3]+'\n';
                        rStr += '    Pipeline - Sponsored : '+pipeLineInfoCount[0]+'\n';
                        rStr += '    Pipeline - GCC Nationals : '+pipeLineInfoCount[1]+'\n';
                        rStr += '    Pipeline - Seconded : '+pipeLineInfoCount[2]+'\n';
                        rStr += '    Pipeline - Temporary : '+pipeLineInfoCount[3]+'\n';
                        rStr += '    Pipeline - ICD : '+pipeLineInfoCount[4]+'\n';
                        rStr += '    Current - Employee Card Non Sponsored : '+pipeLineInfoCount[5]+'\n';
                        rStr += '    Current - DL Employee Card : '+pipeLineInfoCount[6]+'\n';
                        
                        
                        //v1.7
                        system.debug('GsHelperCls.VisaQuotaObj  '+GsHelperCls.VisaQuotaObj);
                        
                        if(GsHelperCls.VisaQuotaObj != null){
                                 
                            if(GsHelperCls.VisaQuotaObj.isHostingCompany == true &&  CustomerId != GsHelperCls.VisaQuotaObj.AccountObj.Id &&
                                GsHelperCls.VisaQuotaObj.SharingCompanies != null && GsHelperCls.VisaQuotaObj.SharingCompanies.isEmpty() == false){
                                
                                rStr += '\nDetails of Hosting Company :\n';
                                rStr += 'Account Name: '+GsHelperCls.VisaQuotaObj.AccountObj.Name+'\n';
                                rStr += 'Exceptional Quota: '+GsHelperCls.VisaQuotaObj.dCalculateVisas[1]+' | ';
                                rStr += 'Employment Quota: '+GsHelperCls.VisaQuotaObj.dCalculateVisas[0]+'\n';
                                if(GsHelperCls.VisaQuotaObj.dUtilizedQuota != null){
                                    rStr += 'No of Employees Current: '+GsHelperCls.VisaQuotaObj.dUtilizedQuota[0]+' | ';
                                    rStr += 'No of Employees Pipeline: '+GsHelperCls.VisaQuotaObj.dUtilizedQuota[1]+'\n';
                                }
                                rStr += 'Total Number of Visas by sharing companies : '+GsHelperCls.VisaQuotaObj.dUtilizedVisasByChilds+'\n';
                            }
                            
                            if(GsHelperCls.VisaQuotaObj.SharingCompanies != null && GsHelperCls.VisaQuotaObj.SharingCompanies.isEmpty() == false){
                                 rStr += '\n Details of Accounts and Visa Quota Utilized from Hosting Company : \n';
                                 for(GsHelperCls.VisaQuota objQ : GsHelperCls.VisaQuotaObj.SharingCompanies){
                                    if(CustomerId != objQ.AccountObj.Id)
                                        rStr += '    '+objQ.AccountObj.Name+' : '+objQ.dUtilizedVisaFromHC+'\n';
                                 }
                            }
                            
                        }
                                                
                    }
                    
                    /*V1.4 - Claude - Replaced by new fit-out logic
                    for(SR_Price_Item__c objSRPriceItem : [select Price__c from SR_Price_Item__c where ServiceRequest__c!=null and Price__c!=null and ServiceRequest__r.Customer__r.BP_No__c=:AccountBpNo and (Status__c='Blocked' OR Status__c='Consumed')]){
                        PortalBalance -= objSRPriceItem.Price__c;
                    }*/
                    
                    //V1.12 - Azfer  - Start - Updated the SOQL added a new and clause in the SOQL added Excluded_Price_Item__c = false
                    /* V1.4 - Claude - Start - New SOQL */
                    String accountFilter = isContractor ? ' ServiceRequest__r.SR_Group__c = \'Fit-Out & Events\' AND ServiceRequest__r.Contractor__r.BP_No__c = ' : ' ServiceRequest__r.SR_Group__c != \'Fit-Out & Events\' AND ServiceRequest__r.customer__r.BP_No__c = ';
                    String priceItemQuery = 'select Price__c,Total_Service_Amount__c from SR_Price_Item__c where ServiceRequest__c!=null and Price__c!=null and '+accountFilter+' \''+AccountBpNo+'\''+' and (Status__c=\'Blocked\' OR Status__c=\'Consumed\') and  Add_in_Account_Balance__c=true AND Excluded_Price_Item__c = false'; // V1.10
                    /* V1.4 - Claude - End */
                    //V1.12 - Azfer  - END
                    
                    System.debug('The Query: '+ priceItemQuery);
                    
                    for(SR_Price_Item__c objSRPriceItem : Database.query(priceItemQuery)){
                        System.debug(objSRPriceItem);
                        //PortalBalance -= objSRPriceItem.Price__c;
                        PortalBalance -= objSRPriceItem.Total_Service_Amount__c ; // added as VAT
                    }
                    //V1.13 - Start 
                     List<String> productCodeList = Label.OB_ProductCODE.split(',');
                     for(HexaBPM__SR_Price_Item__c SRP:[Select Id,Total_Price_USD__c,Total_Price_AED__c, Total_Service_Amount__c 
                                                from HexaBPM__SR_Price_Item__c 
                                                where HexaBPM__ServiceRequest__r.HexaBPM__Customer__c =: AccountBpNo
                                                 and (HexaBPM__Status__c='Blocked' OR HexaBPM__Status__c='Consumed')
                                                 and HexaBPM__Product__c != null and HexaBPM__Pricing_Line__r.Non_Deductible__c =false
                                                 and HexaBPM__Product__r.ProductCode Not IN:  productCodeList ]){
                		if(SRP.Total_Service_Amount__c != null) //Total_Service_Amount__c
                    	 PortalBalance -= SRP.Total_Service_Amount__c ;
            		}
                  
                    //V1.13 - End
                    for(Receipt__c objReceipt : [select Amount__c from Receipt__c where Customer__r.BP_No__c=:AccountBpNo and Amount__c!=null and Payment_Status__c='Success' and Pushed_to_SAP__c=false]){
                        PortalBalance += objReceipt.Amount__c;
                    }                 
                    PortalBalance-=RefundrequestUtils.getRefundAmount(CustomerId);
                    rStr = 'Available Portal balance is : AED '+PortalBalance+(isContractor ? '' : '\n'+rStr); //V1.5 - Claude - Added condition for contractors
                    //return 'Available Portal balance is : AED '+PortalBalance;
                    return rStr;
                }//end of if            
            }
            
        } catch(Exception ex) {
            
            system.debug('Exception is : '+ex.getMessage());
        }
        
        return 'Please try again';
    }   
    
    public static decimal[] PSACalculation(string CustomerId,decimal PSADeposit, Boolean hasEstablishmentCard){
        
        list<string> lstPSARecTypes = new list<string>{'DIFC_Sponsorship_Visa_New','Employment_Visa_from_DIFC_to_DIFC','Employment_Visa_Govt_to_DIFC','Visa_from_Individual_sponsor_to_DIFC'};
        
        map<Id,Service_Request__c> mapOpenSRs = new map<Id,Service_Request__c>([select Id,External_Status_Name__c from Service_Request__c where (NOT Occupation_GS__r.Name like '%STUDENT%') AND Customer__c=:CustomerId AND Record_Type_Name__c IN : lstPSARecTypes AND isClosedStatus__c != true AND Is_Rejected__c != true AND External_Status_Name__c != 'Cancelled' AND External_Status_Name__c != 'Rejected' AND External_Status_Name__c != 'Draft']);
        map<Id,Contact> mapActiveCons = new map<Id,Contact>([select Id from Contact where (NOT Job_Title__r.Name like '%STUDENT%') AND Id IN (select Object_Contact__c from Relationship__c where Subject_Account__c =:CustomerId AND Active__c = true AND Relationship_Type__c='Has DIFC Sponsored Employee')]);
        
        Integer iEmpCount = (mapActiveCons != null && !mapActiveCons.isEmpty()) ? mapActiveCons.size() : 0 ;
        
        if(!mapOpenSRs.isEmpty()) iEmpCount += mapOpenSRs.size();
        
        system.debug('PSADeposit is : '+PSADeposit);
        
        for(SR_Price_Item__c objSRItem : [select Id,Price__c from SR_Price_Item__c
                                                             where Count_in_PSA__c = TRUE AND //V1.9 - Claude - Added PSA flag for SR Groups
                                                             ServiceRequest__r.Customer__c =: CustomerId AND (Status__c='Blocked' OR Status__c='Consumed') AND Pricing_Line__r.Product__r.Name = 'PSA']){ // AND ServiceRequest__r.External_Status_Name__c = 'Submitted'
            PSADeposit += objSRItem.Price__c;
        }
        
        system.debug('PSADeposit is : '+PSADeposit);
        
        // v1.8 - Claude - Start
        if(hasEstablishmentCard) iEmpCount++;
        // v1.8 - Claude - End
        
       
        
        PSADeposit -=RefundRequestUtils.getPSAAmount(CustomerId); // V1.11
        
        String isFintechCompany = [SELECT Id,FinTech_Classification__c FROM Account WHERE Id =:CustomerId].size() > 0 ?
            									 [SELECT Id,FinTech_Classification__c FROM Account WHERE Id =:CustomerId].FinTech_Classification__c : '';
        Boolean isFintechUser   = false;
        
        if(String.isNotBlank(isFintechCompany) && isFintechCompany =='FinTech'){isFintechUser = true;}
        
        decimal dAvaPSA = isFintechUser ? (PSADeposit-(iEmpCount*1000)) :(PSADeposit-(iEmpCount*2500));  
        //decimal[] dPSA = new decimal[]{PSADeposit,dAvaPSA};
        return new decimal[]{PSADeposit,dAvaPSA};//dAvaPSA;
    }
    
    public static void testHack(){
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;i++;
        i++;
        i++;
    }
}