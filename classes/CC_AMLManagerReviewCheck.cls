/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-16-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-07-2020   Shoaib Tariq   Initial Version
**/
global without sharing class CC_AMLManagerReviewCheck implements HexaBPM.iCustomCodeExecutable {

    Private static final String  NON_FINANCIAL         = 'AOR_NF_R';
    Private static final String  FINANCIAL             = 'AOR_Financial';
     
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'False';
        if(stp!=null && stp.HexaBPM__SR__c!=null){

           HexaBPM__Service_Request__c thisRequest = [ SELECT Id,HexaBPM__Customer__c ,RecordTypeId,Setting_Up__c FROM HexaBPM__Service_Request__c WHERE ID =: stp.HexaBPM__SR__c];

            Map<String,Boolean> stepApprovedMap                = New  Map<String,Boolean>();
            Map<Id,Account> accountmap                         = New  Map<Id,Account>();
            stepApprovedMap                                    = getstepApprovedMap(stp.HexaBPM__SR__c);
            accountmap                                         = getAccountmap(thisRequest.HexaBPM__Customer__c );

            //Non-Financial - Non Branch
           if(thisRequest.RecordTypeId == getRecordTypeId(NON_FINANCIAL)
                && thisRequest.Setting_Up__c != 'Branch' 
                && (stepApprovedMap.containsKey('RS_OFFICER_REVIEW') && stepApprovedMap.get('RS_OFFICER_REVIEW'))
                && (stepApprovedMap.containsKey('ENTITY_SIGNING')    && stepApprovedMap.get('ENTITY_SIGNING'))
                && (accountmap.containsKey(thisRequest.HexaBPM__Customer__c )        && accountmap.get(thisRequest.HexaBPM__Customer__c ).Security_Review_Status__c =='Approved')){

                    return 'True';
                }
            //Non-Financial - Branch
            if(thisRequest.RecordTypeId == getRecordTypeId(NON_FINANCIAL)
                && thisRequest.Setting_Up__c == 'Branch'
                && (stepApprovedMap.containsKey('RS_OFFICER_REVIEW') && stepApprovedMap.get('RS_OFFICER_REVIEW'))
                && (stepApprovedMap.containsKey('SECURITY_REVIEW')   && stepApprovedMap.get('SECURITY_REVIEW'))){

                    return 'True';
                }
             //Financial - Non Branch
            if(thisRequest.RecordTypeId == getRecordTypeId(FINANCIAL)
                && thisRequest.Setting_Up__c != 'Branch' 
                && (stepApprovedMap.containsKey('RS_OFFICER_DETAILED_REVIEW') && stepApprovedMap.get('RS_OFFICER_DETAILED_REVIEW'))
                && (stepApprovedMap.containsKey('ENTITY_SIGNING')             && stepApprovedMap.get('ENTITY_SIGNING'))
                && (stepApprovedMap.containsKey('SECURITY_REVIEW')            && stepApprovedMap.get('SECURITY_REVIEW'))){

                    return 'True';
                }
             //Financial - Branch
            if(thisRequest.RecordTypeId == getRecordTypeId(FINANCIAL)
                && thisRequest.Setting_Up__c == 'Branch' 
                && (stepApprovedMap.containsKey('RS_OFFICER_REVIEW') && stepApprovedMap.get('RS_OFFICER_REVIEW'))
                && (stepApprovedMap.containsKey('SECURITY_REVIEW')   && stepApprovedMap.get('SECURITY_REVIEW'))){

                    return 'True';
                }
        }
        return strResult;
    }
    /**
     * Return record type ID
      */ 
    private Id getRecordTypeId(String recordTypeName){
        return Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(recordTypeName).getRecordTypeId();
    } 
    
    /**
     * Return record type ID
      */ 
      private Map<Id,Account> getAccountmap(String AccountId){
        return new Map<Id,Account>([SELECT ID,Security_Review_Status__c FROM Account WHERE Id =: AccountId]);
       } 
    
     /**
     * Return ApprovedStepMap
      */ 
    private Map<String,Boolean> getstepApprovedMap(String SrId){
       Map<String,Boolean> stepApprovedMap    = new  Map<String,Boolean>();
        for(HexaBPM__Step__c thisStep : [SELECT Id,Step_Template_Code__c,HexaBPM__Status__c FROM HexaBPM__Step__c WHERE HexaBPM__SR__c =: SrId AND HexaBPM__Status__r.Name = 'Approved' ]){
      
              stepApprovedMap.put(thisStep.Step_Template_Code__c,True);

         }
        return stepApprovedMap;
    } 
}