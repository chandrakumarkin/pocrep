@isTest(SeeAllData=false)
public class Test_ctlrProgressBar {
static testMethod void ctlrProgressBarMethod() {
Account acc = new Account();
  acc.Name = 'Shyam';
    acc.Transferred_to_ROC__c = True;
    acc.ROC_Status__c = 'Active';
    acc.ROC_reg_incorp_Date__c = system.today();
    acc.Transferred_to_ROC_Date__c = system.today();
    insert acc;

    SR_Template__c st = new SR_Template__c();
    st.Name = 'SR_Template';
    st.Is_Letter__c = True;
    st.Menutext__c = 'Application for Incorporation / Registration';
    st.SR_RecordType_API_Name__c  = 'Mortgage_Registration';
    insert st;

    SR_Template__c st1 = new SR_Template__c();
    st1.Name = 'SR_Template';
    st1.Is_Letter__c = True;
    st1.Menutext__c = 'User Access Request Form';
    st1.SR_RecordType_API_Name__c  = 'Mortgage_Registration';
    insert st1;  
 
    SR_Template__c st2 = new SR_Template__c();
    st2.Name = 'SR_Template';
    st2.Is_Letter__c = True;
    st2.Menutext__c = 'Lease Registration';
    st2.SR_RecordType_API_Name__c  = 'Mortgage_Registration';
    insert st2;    
    
    SR_Status__c is = new SR_Status__c();
    is.Name = 'Approved';
    is.Code__c = 'AWAITING_RE_UPLOAD';
    insert is;  
    
   Service_Request__c sr = new Service_Request__c();
    sr.SR_Template__c = st.Id;
    sr.Express_Service__c = False;
    sr.SR_Group__c = 'GS';
    sr.Internal_SR_Status__c = is.id;
    sr.Completed_Date_Time__c = system.now().addminutes(-2);
    sr.Submitted_DateTime__c = system.now();
    sr.Completed_Date_Time__c = system.now();
    sr.Customer__c = acc.id;
    try{
    insert sr;
    }
    catch(Exception e){
        
    }
   Service_Request__c sr1 = new Service_Request__c();
    sr1.SR_Template__c = st1.Id;
    sr1.Express_Service__c = False;
    sr1.SR_Group__c = 'GS';
    sr1.Internal_SR_Status__c = is.id;
    sr1.Completed_Date_Time__c = system.now().addminutes(-2);
    sr1.Submitted_DateTime__c = system.now();
    sr1.Completed_Date_Time__c = system.now();
    sr1.Customer__c = acc.id;
    try{
    insert sr1;
    }
    catch(Exception e){
        
    }
   Service_Request__c sr2 = new Service_Request__c();
    sr2.SR_Template__c = st2.Id;
    sr2.Express_Service__c = False;
    sr2.SR_Group__c = 'GS';
    sr2.Internal_SR_Status__c = is.id;
    sr2.Completed_Date_Time__c = system.now().addminutes(-2);
    sr2.Submitted_DateTime__c = system.now();
    sr2.Completed_Date_Time__c = system.now();
    sr2.Customer__c = acc.id;
    try{
    insert sr2;
    }
    catch(Exception e){
        
    }   
   Map<String,Service_Request__c> SrMap = new Map<String,Service_Request__c>();
   SrMap.put(sr.Service_Type__c,sr);
   SrMap.put(sr1.Service_Type__c,sr1);
   SrMap.put(sr2.Service_Type__c,sr2);
   
    Test.startTest();  
  //  ctlrProgressBar.Accountwrapper accwrap = new ctlrProgressBar.Accountwrapper('name',system.today(),system.today(),'True');    
  /* ctlrProgressBar cpbr = new ctlrProgressBar();
    for(ctlrProgressBar.Accountwrapper wrp : cpbr.Accountwrapperlist){
        wrp.name = 'Wrapper Name';
        wrp.submittedDate = system.today();
        wrp.approvedDate = system.today();
        wrp.selected = 'true';        
    } 
    */
    ctlrProgressBar.getVisitHistory(acc.Id);
    Test.stopTest();
}       
}