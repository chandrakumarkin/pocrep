@RestResource(urlMapping='/difccomdata/*')
global class DCCATradeLicenseInfoWS{
    
   @HttpPost 
    global static  DCCATradeLicenseResponseWrapper jsonResponse()
    {     
        RestRequest req = RestContext.request;
        DCCATradeLicenseResponseWrapper ObjRes=new DCCATradeLicenseResponseWrapper();
        System.debug('ZZZ DCCATradeLicenseInfoWS.cls-->doPost_M !!!');  
        System.debug('ZZZ=====Input Data=>'+req.requestBody.toString());
        string postBody = req.requestBody.toString();
        
        try
        {
            ObjRes.message='';
            DCCAInputRequestWrapper reqData = (DCCAInputRequestWrapper)JSON.deserialize(postBody,DCCAInputRequestWrapper.class);
            
            if(reqData != null && String.isNotBlank(reqData.registrationNumber)){
                Account[] acc = [SELECT Id,Name,Legal_Type_of_Entity__c,Next_Renewal_Date__c,ROC_reg_incorp_Date__c,Registration_License_No__c,License_Type__c,Trading_Name_Arabic__c,Trade_Name__c,Unified_License_Number__c,
                                 (Select Id,Name,Activity_Name_Eng__c,Start_Date__c,End_Date__c,Activity__c,Activity__r.Type__c,Activity__r.Activity_Code__c 
                                  FROM License_Activities__r
                                  WHERE Activity__c != null
                                 )
                                 FROM ACCOUNT
                                 WHERE Registration_License_No__c = :reqData.registrationNumber
                                 
                                ];
                
                if(!acc.isEmpty()){
                    
                    DCCAActivityList[] activityLst = new List<DCCAActivityList>();
                    if(!acc[0].License_Activities__r.isEmpty()){
                        for(License_Activity__c act : acc[0].License_Activities__r){
                            DCCAActivityList a = new DCCAActivityList();
                            //a.activityCode = act.Activity__r.Activity_Code__c ;
                            a.activityCode = act?.Name;
                            a.activityName = act?.Activity_Name_Eng__c;
                            a.addDate      = String.valueOf(act?.Start_Date__c); 
                            a.cancelDate   = String.valueOf(act?.End_Date__c);
                            activityLst.add(a);
                        }
                        ObjRes.activityList = activityLst;
                    }else{
                        ObjRes.activityList = activityLst;
                        
                    }
                    
                    //ObjRes.cancelDate = ;
                    //ObjRes.cocAuth = ;
                    //ObjRes.cocNum = ;
                    //ObjRes.commerceNum = ;
                    ObjRes.expDate = String.valueOf(acc[0]?.Next_Renewal_Date__c);
                    ObjRes.issueAuthority = 'DIFC';
                    ObjRes.issueCity = 'DXB';
                    ObjRes.issueDate = String.valueOf(acc[0]?.ROC_reg_incorp_Date__c);
                    ObjRes.legalType = acc[0]?.Legal_Type_of_Entity__c;
                    ObjRes.licenCategory = 'Normal';
                    ObjRes.licenNum = acc[0]?.Registration_License_No__c;
                    ObjRes.licenType = acc[0]?.License_Type__c;
                    ObjRes.registryType = 'License';
                    ObjRes.tradeNameAr = acc[0]?.Trading_Name_Arabic__c;
                    ObjRes.tradeNameEn = acc[0]?.Trade_Name__c;
                    ObjRes.tradeSerialNum = acc[0]?.Unified_License_Number__c;
                    ObjRes.status = 'success';
                    ObjRes.message = 'ok';
                    
                }else{
                    ObjRes.status = 'error';
                    ObjRes.message='No data available or invalid registration number.';
                }
                
            }else{
                ObjRes.status = 'error';
                ObjRes.message='Please pass valid registration number.';
                
            }
            
        }
        catch(DmlException e)
        {
            
            ObjRes.status='error';
            ObjRes.message=e.getMessage();
            
        }
        
       /// RestContext.response.responseBody = Blob.valueOf('ss');
        
        return ObjRes;
        
        //    return  '';
    }
    
    
    
    

    
    global class  DCCAInputRequestWrapper
    {
        public string registrationNumber { get; set; }
        
        global DCCAInputRequestWrapper(String regNum){
            this.registrationNumber = regNum;
        }
    }
    
    
    
    global class DCCATradeLicenseResponseWrapper {
        public List<DCCAActivityList> activityList;
        public String cancelDate;
        public String cocAuth;
        public String cocNum;
        public String commerceNum;
        public String expDate;
        public String issueAuthority;
        public String issueCity;
        public String issueDate;
        public String legalType;
        public String licenCategory;
        public String licenNum;
        public String licenType;
        public String registryType;
        public String status;
        public String tradeNameAr;
        public String tradeNameEn;
        public String tradeSerialNum;
        public String message;
        
    }
    
    public class DCCAActivityList {
        public String activityCode;
        public String activityName;
        public String addDate;
        public String cancelDate;
    }
    
    
    
    
    
    
}