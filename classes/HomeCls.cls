/*****************************************************************
Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date           Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    15-Dec-2015    Rajil        Added the communityUserRoles to enable/disable the services for DIFC mobile portal.
 V1.2    10-Dec-2015    Swati        Hide Compliance for Fit-Out
 V1.3    03-05-2016     Swati        Added custom code for fine process
 V1.4    28-07-2016     Swati        Changes to show fine on portal for manually added fine.
 V1.5    08-09-2016     Sravan        Added Custom code to display notifications on the portal Home Page
 V1.6    23-11-2016     Claude       Added text change for CONTRACTOR-ONLY users
 V1.7    20-12-2016     Swati        as per #3788
 V1.8   18/01/2017  Swati       Modified logic as per # 3842
 V1.9    06-12-2017     M.Prem       Changed the view on portal home page for Non DIFC Client portal users - #1516
 V1.10    11/12/2017    Arun         Added Code for VAT 
 V1.11   24-12-2017     Danish       Condition to access VAT
 *****************************************************************/
public without sharing class HomeCls {
    
    public list<NotificationWrapper> Notifications {get;set;}
    string CustomerId;
    public list<FineDetails> Fines {get;set;}
    public string ServiceRequest_Prefix {set;get;}
    public string SRId {get;set;}
    public string objectionId {get;set;} 
    
    public SR_Doc__c ComplianceDoc {get;set;}
    public string Type {get;set;}
    public string RocStatus {get;set;} 
    public string communityUserRoles {get;set;}
    
    public boolean isCompany {get; set;} //V1.2
    public boolean isFOorProperty {get; set;} //V1.2
    Public List<Portal_Notification__c> portalNotifications{get;set;} // V1.5
    
    public Boolean isFitOutOnly     {get; set;} // V1.6
    
    public Boolean canAccessVATInfo {get;set;}
    
    public string strDisplayService {get; set;} //V1.9 - #1516
    
    public HomeCls(){
        init();
    }
    
    //VAT
    public string AccountTax{get;set;}
    public string AccountTaxPopup{get;set;}
    public void UpdateTax()
    {
        Account ObjAccount=new Account(Id=CustomerId);
        ObjAccount.Tax_Registration_Number_TRN__c=AccountTax;
        update ObjAccount;
        AccountTaxPopup='alert("Tax Registration Number (TRN) information is successfully updated in your Company Info.");';
    
    }
    public void UpdateTaxNA()
    {
        Account ObjAccount=new Account(Id=CustomerId);
        ObjAccount.Tax_Registration_Number_TRN__c='Not Applicable';
        update ObjAccount;
         AccountTaxPopup='';
        
    
    }
    
    //End VAT
    
    
    public void init(){
    strDisplayService ='';
        ServiceRequest_Prefix = Cls_FieldElementId_Util.getObjectPrefix('Service_Request__c');  
        Notifications = new list<NotificationWrapper>();
        
        isCompany=false;
        isFOorProperty=false;
        canAccessVATInfo  = false;
        
        for(User objUser : [select Id,Can_Access_VAT_Info__c,Tax_Registration_Number_TRN__c,AccountId,Contact.Account.RecordType.DeveloperName,Account_ID__c,Contact.Account.ROC_Status__c,Community_User_Role__c from User where Id=:Userinfo.getUserId()]){
             AccountTax=objUser.Tax_Registration_Number_TRN__c;
            CustomerId = objUser.AccountId;
            
            system.debug('test' + CustomerId );
            RocStatus = objUser.Contact.Account.ROC_Status__c;
            if(objUser.Community_User_Role__c!=null && objUser.Community_User_Role__c!=''){
               communityUserRoles = objUser.Community_User_Role__c;
               isFitOutOnly = String.isNotBlank(objUser.Contact.Account.RecordType.DeveloperName) && objUser.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account'); // V1.6
               canAccessVATInfo  =  objUser.Can_Access_VAT_Info__c; // 1.11
           }
           /*
           
           V1.9 - #1516 - M.Prem - 06-12-2017 - to change the view of home page for Non DIFC Client portal users
           Below is the condition to find Non DIFC client portal users
           START
           
           if(objUser.AccountId!=null)
           {
           if(objUser.Contact.Account.RecordType.DeveloperName.equals('RORP_Account') && objUser.Contact.Account.ROC_Status__c == null )
           
                strDisplayService = 'none';
           
           else
                strDisplayService = 'block';
             
           }
           
           */
        }
        for(Compliance__c objComp : [select Id,Name,Account__c,Start_Date__c,End_Date__c,Exception_Date__c,Fine__c from Compliance__c where Account__c=:CustomerId AND (Status__c = 'Open' OR Status__c='Defaulted') order by End_Date__c]){
            NotificationWrapper objNotificationWrapper = new NotificationWrapper();
            objNotificationWrapper.Compliance = objComp;
            objNotificationWrapper.ComplianceName = objComp.Name;
            objNotificationWrapper.StartDate = objComp.Start_Date__c;
            objNotificationWrapper.EndDate = objComp.End_Date__c;
            objNotificationWrapper.ExceptionDate = objComp.Exception_Date__c;
            Notifications.add(objNotificationWrapper);
        }
        
        //VAT
        
        if(string.isblank(AccountTax) && canAccessVATInfo)
        {
                AccountTaxPopup='pageLoad();';
        }
        
        if(communityUserRoles.contains('Company Services')){
            isCompany = true;   
            
   
        }
        
        if(communityUserRoles.contains('Fit-Out Services') || communityUserRoles.contains('Property Services')){
            isFOorProperty = true;  
        }
        
        if(communityUserRoles.contains('Company Services') && communityUserRoles.contains('Property Services')){
            isCompany = true;  
            isFOorProperty = false;
        }
        
        //Fines Info - Display the fines which are issued in last 3 months
        Fines = new list<FineDetails>();
        Date d = system.today().addMonths(-3);
        
        //V1.3, // V1.8
        for(Service_Request__c objSR : [select Id,Type_of_Request__c,Name,Customer__c,External_Status_Code__c,Compliance__c,Compliance__r.Name,date__c,Compliance__r.six_Fine_issued__c,
                                        (select Id,Price__c,SRPriceLine_Text__c from SR_Price_Items1__r where Status__c=:'Added' ),
                                        (select Id,External_Status_Name__c from Service_Requests1__r where Record_Type_Name__c='Objection_SR' and Customer__c =: CustomerId AND Customer__c != null) 
                                        from Service_Request__c 
                                        where Customer__c =: CustomerId 
                                        AND Record_Type_Name__c='Issue_Fine' and External_Status_Name__c=:'Issued'
                                        AND CreatedDate >=: d /*AND Fee_Exemption__c=false*/ order by CreatedDate desc]){//V1.4
            
            FineDetails objFineDetails = new FineDetails();
            if(objSR.SR_Price_Items1__r!=null){
                for(SR_Price_Item__c objtemp : objSR.SR_Price_Items1__r){
                    if(objtemp.SRPriceLine_Text__c == 'Failure to renew Data Protection (non financial/Retail)'){
                        objFineDetails.FineName =   'Failure to renew notification of personal data operations';
                    }
                    else if(objtemp.SRPriceLine_Text__c == 'Failure to renew Data Protection Fine (financial)'){
                         objFineDetails.FineName = 'Failure to renew notification of personal data operations';
                    }
                    else{
                        objFineDetails.FineName = objtemp.SRPriceLine_Text__c;
                    }
                    objFineDetails.FineAmt = (objFineDetails.FineAmt + objtemp.Price__c).setscale(2);
                }
            }
            
            if(objSR.Type_of_Request__c == 'Other'){ //V1.7
                objFineDetails.isOther = true;  
            }
            objFineDetails.Status = objSR.External_Status_Code__c;
            objFineDetails.SRId = objSR.Id;
            if(objSR.Service_Requests1__r!=null && !objSR.Service_Requests1__r.isEmpty()){
                if(objSR.Service_Requests1__r[0].External_Status_Name__c == 'Submitted'){
                     objFineDetails.ObjectionSRId = objSR.Service_Requests1__r[0].id;
                     objFineDetails.objectionRejected = false;
                }
                else if(objSR.Service_Requests1__r[0].External_Status_Name__c == 'Rejected'){
                    objFineDetails.objectionRejected = true;
                }
               
            }
            if(objSR.date__c>=system.today()){
                objFineDetails.datePassed = false;          
            }
            else if(objSR.date__c<system.today()){
                objFineDetails.datePassed = true;   
            }
            if(objSR.Compliance__r.six_Fine_issued__c==true){
                objFineDetails.fineNotPaid = true;
            }
            
            Fines.add(objFineDetails); 
        }
        
              
        //Complince Document informaiton added on 20th Nov
        ComplianceDoc = new SR_Doc__c();
        Type = 'Constructor';
        ComplianceCalendarInfo();     
        
        PortalNotifications(); // V1.5  
    }
    
    public void ReGenerateCalendar(){
        if(ComplianceDoc != null && ComplianceDoc.Id != null){
            ComplianceDoc.Doc_ID__c = '';
            ComplianceDoc.Generate_Document__c = true;
            update ComplianceDoc;
            ComplianceCalendarInfo();
        }
    }
    
    public void ComplianceCalendarInfo(){
        for(SR_Doc__c objSRDoc : [select Id,Name,Preview_Document__c,Generate_Document__c,Doc_ID__c from SR_Doc__c where Service_Request__r.Customer__c=:CustomerId AND Name='Compliance Calendar']){
            ComplianceDoc = objSRDoc;
        }
        system.debug('Type is : '+Type);
        if((ComplianceDoc == null || ComplianceDoc.Id == null) && Type != 'Constructor'){
            //Create a new SR Doc for Compliance Calendar and Create a Dummy SR
            //Check for the DM SR if exist alright otherwise create a new DM SR
            Service_Request__c objDMSR = new Service_Request__c();
            for(Service_Request__c obj : [select Id from Service_Request__c where Customer__c != null AND Customer__c =:CustomerId AND Record_Type_Name__c='DM_SR'])
                objDMSR = new Service_Request__c(Id=obj.Id);
            if(objDMSR.Id == null){ 
                for(RecordType objRT : [select Id from RecordType where sObjectType='Service_Request__c' AND DeveloperName='DM_SR']){
                    objDMSR.Customer__c = CustomerId;
                    objDMSR.RecordTypeId = objRT.Id;
                }
                objDMSR.Send_SMS_To_Mobile__c = '+971602345678';
                insert objDMSR;
            }
            SR_Doc__c objSRDoc = new SR_Doc__c();
            objSRDoc.Service_Request__c = objDMSR.Id;
            objSRDoc.Name = 'Compliance Calendar';
            objSRDoc.Document_Master__c = [select Id from Document_Master__c where Name='Compliance Calendar' limit 1].Id;
            objSRDoc.Generate_Document__c = true;
            objSRDoc.Status__c = 'Generate';
            objSRDoc.Unique_SR_Doc__c = 'Compliance_Calendar_Upload_for_DM_SR_'+objDMSR.Id;
            objSRDoc.Letter_Template__c = [select Id from Loop__DDP_Integration_Option__c where Name='Compliance Calendar' limit 1].Id;
            //objSRDoc.Sys_IsGenerated_Doc__c = true;
            insert objSRDoc;
        }
    }
    
    public Class NotificationWrapper{
        public Compliance__c Compliance {get;set;}
        public string ComplianceName {get;set;}
        public Date StartDate {get;set;}
        public Date EndDate {get;set;}
        public Date ExceptionDate {get;set;}
    }
    
    public Class FineDetails{
        public String FineName {get;set;}
        public decimal FineAmt {get;set;}
        public String Status {get;set;}
        public string ObjectionSRId {get;set;}
        public String SRId {get;set;}
        public boolean datePassed {get;set;}
        public boolean objectionRejected {get;set;}
        public boolean fineNotPaid {get;set;} 
        public boolean isOther {get;set;} //V1.7
        FineDetails(){
            FineAmt = 0;
        }
    }
    
    //  V1.5
    
    Public void PortalNotifications()
    {
        portalNotifications = new list<Portal_Notification__c>();
        for(Portal_Notification__c pn : Portal_Notification__c.getAll().Values())
        {
            if(pn.Active__c)
               portalNotifications.add(pn);
        }
        
    }
    
    
}