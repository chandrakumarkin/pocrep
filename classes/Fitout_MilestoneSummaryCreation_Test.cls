@isTest
public class Fitout_MilestoneSummaryCreation_Test {
	public static testMethod void testMilestoneSummaryCreation() 
    {
        Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
        insert objAccount;
        
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.RF_Valid_To__c = system.today().addDays(10);
        objLease.RF_Valid_From__c = system.today();
        objLease.Lease_Types__c = 'Retail Lease';
        insert objLease;
        
        list<Building__c> lstBuilding = new list<Building__c>();
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP On Plan';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '2300';
        objBuilding.SAP_Building_No__c = '123456';
        lstBuilding.add(objBuilding);
        insert lstBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c =  lstBuilding[0].Id;
        objUnit.Building_Name__c = 'RORP On Plan';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'office';
        lstUnits.add(objUnit);
        
        List<Lease_Partner__c> ListLP=new List<Lease_Partner__c>();
        Lease_Partner__c objLP = new Lease_Partner__c();
        objLP.Lease__c = objLease.Id;
        objLP.Account__c = objAccount.Id;
        objLP.Unit__c = lstUnits[0].Id;
        objLP.status__c = 'Draft';
        objLP.Type_of_Lease__c = 'Leased';
        objLP.Is_License_to_Occupy__c = true;
        objLP.Is_6_Series__c = false;
        ListLP.add(objLP);
        insert ListLP;
        Test.startTest();
        	Fitout_MilestoneSummaryCreation abs= new Fitout_MilestoneSummaryCreation();
	    	String cronExpr = '0 0 0 15 3 ? 2099';
			String jobId = System.schedule('myJobTestJobName', cronExpr, abs);
        	//abs.createLogRecord(Database.Error err)
        Test.stopTest();
    }
}