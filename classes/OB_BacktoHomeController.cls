public without sharing class OB_BacktoHomeController {
    
    @AuraEnabled
    public static RespondWrap getSrInfo(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        if(reqWrap.srId != null){
            for (HexaBPM__Service_Request__c srOjb : [select id ,HexaBPM__Internal_Status_Name__c, recordtype.Developername,HexaBPM__Record_Type_Name__c FRom HexaBPM__Service_Request__c WHere id =: reqWrap.srId]){
                respWrap.srObj = srOjb;
            }
            respWrap.cpLInk = OB_AgentEntityUtils.getCPREnewalLInk();
            respWrap.cpLInk += '&srId='+respWrap.srObj.id;
        }
        
        return respWrap;
    }
        
    
    // ------------ Wrapper List ----------- //
        
    public class RequestWrap{
    
        @AuraEnabled public String srId; 
    }
        
    public class RespondWrap{
    
        @AuraEnabled public HexaBPM__Service_Request__c srObj;
        @AuraEnabled public string errorMessage;
        @AuraEnabled public string cpLInk;
    }
}