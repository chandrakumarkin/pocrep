/*************************************************************************************************
 *  Name        : MobileForgotPasswordController
 *  Author      : Diana
 *  Company     : NSI JLT
 *  Date        : 2015-11-13     
 *  Purpose     : This class is to reset password
 * 
 * /

/**
 * An apex page controller that exposes the site forgot password functionality
 */
public with sharing class MobileForgotPasswordController {
    public String username {get; set;}   
    public MobileForgotPasswordController() {}
    public boolean success = false;
    
    public PageReference forgotPassword() {
       
        if(null!=username && username!=''){
         success = Site.forgotPassword(username);
        }
        else{
            
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter value in username field');//Same old drama 
		   ApexPages.addMessage(myMsg);
        
        }
    PageReference pr = Page.MobileForgotPasswordConfirm;
    pr.setRedirect(true);
    if(success){ 
     return pr;
    }
    return null;
   }
    
    public PageReference navigateBack(){
        
		  PageReference pr = Page.MobileCustomLogin;
    	  pr.setRedirect(true); 
          return pr;
    }
    
}