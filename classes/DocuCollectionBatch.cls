/*
* DocuCollectionBatch Class
* Author      : Hussain Fazal Ullah 
* Date        : 2019-08-28     
* Modification History:
*****************************************************************************************************************
* Version       Date                Author                  Remarks                 Description     
* 1.0           2019-08-28          Hussain Fazal Ullah     Created The Class       Created the batch class to send 
                                                                                    email reminder if the documents 
                                                                                    are not collected
*****************************************************************************************************************/


global class DocuCollectionBatch implements Database.Batchable<sObject>
{
        
    global database.querylocator start(database.batchablecontext bc)
    {
        String stepName = 'Collected';
        String stepStatus = 'Awaiting Review';
       
        Date clsDate = Null;
        //String str = '%' + '=' + '%';
        String str = 'Non_DIFC_Sponsorship_Visa_Renewal';
        String recType = System.Label.GS_Send_Reminder_Record_Types;
        List<String> recTypeList = new list<string>();
        if(recType.contains(',') )
        {
            for( String recTypeName : recType.split(','))
            {
                recTypeList.add(recTypeName);
                system.debug('@@@@@RecTypeList'+recTypeList);
            }
        }
        String stepRecs = 'SELECT Id,Step_Name__c,Applicant_Email__c,SR__r.ID,Closed_Date__c,Step_Status__c from Step__c where Step_Name__c=:stepName and Step_Status__c=:stepStatus and CreatedDate <= Last_N_Days:2 and Closed_Date__C=:clsDate and SR__r.RecordType.DeveloperName IN : recTypeList';
        System.debug('!!!!!!!'+stepRecs);
        return database.getquerylocator(stepRecs);
      
       
    }
    
    global void execute(database.batchablecontext bc,List<Step__c> lstSteps)
    {
      
        SET<Id> setStepId = new SET<Id>();
        List<String> lstEmailId = new List<String>();
         String targetObjId = System.Label.GS_Set_Target_Object_Id;
        
        for(Step__c eachStep:lstSteps)
        {
            //setStepId.add(eachStep.Id);
            //lstEmailId.add(eachStep.Applicant_Email__c);
            //system.debug('!!!!!!lstEmail' +lstEmailId);
            
        }
        
        EmailTemplate emailTemplate = [select Id from EmailTemplate where DeveloperName = 'GS_Document_Collection_Reminder'];
        
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'portal@difc.ae'];
        Map<Id,List<String>> mapDemo = new Map<Id,List<String>>();
      
        for(Step__c eachStep:lstSteps){
            if(eachStep.Applicant_Email__c!=null)
            {
                mapDemo.put(eachStep.Id,new List<String>{eachStep.Applicant_Email__c});
            }
                       
            
        }
        
        for(Step__c eachStep:lstSteps)
        {
          if(mapDemo.get(eachStep.Id)!=Null)
          {
          // lstEmailId = new List<String>();
            mail = new Messaging.SingleEmailMessage();
            if ( owea.size() > 0 ) 
            {
             mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }
            mail.setSaveAsActivity(false);
            mail.setTemplateId(emailTemplate.ID);
            system.debug('@@@@eachstep'+eachstep.SR__c);
            mail.setWhatId(eachstep.Id);
            mail.setTargetObjectId(targetObjId);
            mail.setTreatTargetObjectAsRecipient(false);
            lstEmailId.add(eachStep.Applicant_Email__c);
            mail.setToAddresses(mapDemo.get(eachStep.Id));  //add other emails here.
            system.debug('11111111111111' +lstEmailId);
            emails.add(mail);
          }
            
                  
        }
        system.debug('@@@@@@@@' +emails);
        if(emails.size()>0)
        {
            system.debug('inside@@@@@@@@');
            Messaging.sendEmail(emails); 
        }
    }       
    
    global void finish(database.batchablecontext bc)
    {
        
    }
}