/**
*Author : Merul Shah
*Description : Save client comment to DB.
**/ 
public without sharing class OB_ClientCommentOnStepController
{
     @AuraEnabled   
    public static ResponseWrapper  loadStepDB(String reqWrapPram)
    {
        //reqest wrpper
        RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
        
        //response wrpper
        ResponseWrapper respWrap = new ResponseWrapper();
        HexaBPM__Step__c stepObj;
        String stepId = reqWrap.stepId; 
        
      for(HexaBPM__Step__c step : [SELECT id,
                                            HexaBPM__SR_Step__c,
                                            HexaBPM__Status__c,
                                            Step_Template_Code__c,
                                            HexaBPM__SR__c ,
                                            HexaBPM__Step_Notes__c,
                                            HexaBPM__Step_Template__r.Name,
                                            HexaBPM__Step_Template__r.Id,
                                            HexaBPM__SR__r.Name,
                                            HexaBPM__SR__r.RecordType.Name
                                            
                                       FROM HexaBPM__Step__c
                                      WHERE id =:stepId  
                                        AND Step_Template_Code__c = 'AWAITING_INFORMATION_FROM_ENTITY'
                                        AND HexaBPM__Status_Type__c !='End'
                                      LIMIT 1
                                     ]
           )
        {
            respWrap.stepObj = step;
            
        }
        
        if( respWrap.stepObj == NULL )
        {
            respWrap.errorMessage = 'No Pending Action.';
        }
        
        return respWrap;
    }
    
    
    @AuraEnabled   
    public static ResponseWrapper  onStepSaveDB(String reqWrapPram)
    {
        //reqest wrpper
        RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
        
        //response wrpper
        ResponseWrapper respWrap = new ResponseWrapper();
         
        String stepId = reqWrap.stepId; 
        
        HexaBPM__Step__c stepObj = reqWrap.stepObj;
        String srId = reqWrap.stepObj.HexaBPM__SR__c ;
        system.debug('########## stepId  '+stepId );
        
        try
        {
               
              
                if( stepObj != NULL)
                {    
                    HexaBPM__Step_Transition__c SRstpTran;
                    String srInternalStatusID;
                    String srExternalStatusID;
                    for(HexaBPM__Step_Transition__c SRstpTranTemp : [
                                                                     SELECT id,
                                                                            HexaBPM__Transition__c,
                                                                            HexaBPM__Transition__r.HexaBPM__From__c,
                                                                            HexaBPM__Transition__r.HexaBPM__To__c,
                                                                            HexaBPM__SR_Step__c,
                                                                            HexaBPM__SR_Status_External__c,
                                                                            HexaBPM__SR_Status_Internal__c
                                                                            
                                                                       FROM HexaBPM__Step_Transition__c
                                                                      WHERE HexaBPM__SR_Step__c =:stepObj.HexaBPM__SR_Step__c 
                                                                        AND HexaBPM__Transition__r.HexaBPM__From__c =: stepObj.HexaBPM__Status__c 
                                                                      LIMIT 1    
                                                                    ])
                    {
                        system.debug('########## stepObj '+stepObj );
                        stepObj.HexaBPM__Status__c = SRstpTranTemp.HexaBPM__Transition__r.HexaBPM__To__c;
                        if(string.isNotEmpty(SRstpTranTemp.HexaBPM__SR_Status_Internal__c)){
                            srInternalStatusID = SRstpTranTemp.HexaBPM__SR_Status_Internal__c;
                            srExternalStatusID = SRstpTranTemp.HexaBPM__SR_Status_External__c;
                        }
                        
                        
                    }
                    
                    // Error catching remaining
                    if( !String.isBlank( srId ) )
                    {
                        system.debug('########## before stepObj '+stepObj );
                        update stepObj;
                        
                        HexaBPM__Service_Request__c srObj = new HexaBPM__Service_Request__c(id = srId );
                        if(string.isNotEmpty(srInternalStatusID)){
                            srObj.HexaBPM__Internal_SR_Status__c  = srInternalStatusID;
                            srObj.HexaBPM__External_SR_Status__c  = srExternalStatusID; 
                        }
                       
                        
                        
                        system.debug('########## before srObj '+srObj );
                        update srObj; 
                        
                    }
                              
                }
                
                
        }
        catch(DMLException e)
        {
             respWrap.errorMessage = OB_QueryUtilityClass.getMessageFromDMLException(e);
        }
        return respWrap;
    }
    
    /*
    public class SRWrapper 
    {
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public List<OB_EntityDirectorContainerController.AmendmentWrapper> amedWrapLst { get; set; }
        
        public SRWrapper()
        {}
    }
    
    public class AmendmentWrapper 
    {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public Id amedID { get; set; }
        @AuraEnabled public Boolean isIndividual { get; set; }
        @AuraEnabled public Boolean isBodyCorporate { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        
        //lookupLabel
        
        public AmendmentWrapper()
        {}
    }
    
    */
    
    
    public class RequestWrapper 
    {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public String stepId {get;set;}
        @AuraEnabled public HexaBPM__Step__c stepObj {get;set;}
      
        public RequestWrapper()
        {}
    }

    public class ResponseWrapper 
    {
        @AuraEnabled public String errorMessage {get;set;}
        @AuraEnabled public HexaBPM__Step__c stepObj {get;set;}
        
        public ResponseWrapper()
        {}
    }

}