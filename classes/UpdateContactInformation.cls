/**
 * @description       : 
 * @author            : shoaib tariq
 * @group             : 
 * @last modified on  : 02-22-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-21-2021   shoaib tariq   Initial Version
**/
public without sharing class UpdateContactInformation {
    @AuraEnabled(cacheable=true)
    public static Contact getContactInfo(string recordId) {
        return [SELECT Id, email,Phone
                 FROM Contact
                 WHERE Id =: recordId];
    }

    @AuraEnabled
    public static String updateContact(string recordId,Boolean isCommunityEvents,Boolean isDIFCAcademy,string newemail
                                         ,Boolean isDIFCFintechHive,Boolean isGeneralAnnouncements,string newphone) {
        Contact thisContact = [SELECT Id, email,Phone,isCommunityEvents__c,isDIFCAcademy__c,isDIFCFintechHive__c,isGeneralAnnouncements__c FROM Contact WHERE Id =: recordId];
        if(string.isNotBlank(newemail)) thisContact.email = newemail;
        if(string.isNotBlank(newphone)) thisContact.Phone = newphone;
        thisContact.isCommunityEvents__c = isCommunityEvents;
        thisContact.isDIFCAcademy__c     = isDIFCAcademy;
        thisContact.isDIFCFintechHive__c = isDIFCFintechHive;
        thisContact.isGeneralAnnouncements__c = isGeneralAnnouncements;
        try {
            update thisContact;
            return 'Details Updated';
        } catch (Exception ex) {
            return ex.getMessage();
        }
    } 
}