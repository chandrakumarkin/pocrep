/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestSRHistoryDetails {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.BP_No__c = '000400034';
        objAccount.Financial_Year_End__c = 'March-31';
    	insert objAccount;
    	
    	Account_Share_Detail__c objASD = new Account_Share_Detail__c();
		objASD.Account__c = objAccount.Id;
		objASD.No_of_shares_per_class__c = 1000;
		objASD.Nominal_Value__c = 1000;
		objASD.Start_Date__c = system.today();
		objASD.Status__c = 'Active';
		insert objASD;
    	
    	Contact objContact = new Contact();
    	objContact.FirstName = 'Test';
    	objContact.LastName = 'Babu';
    	objContact.Email = 'test@test.ae';
    	objContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Contact').getRecordTypeId();
    	insert objContact;
    	
    	list<Relationship__c> lstRel = new list<Relationship__c>();
    	Relationship__c objRel = new Relationship__c();
    	objRel.Subject_Account__c = objAccount.Id;
    	objRel.Object_Account__c = objAccount.Id;
    	objRel.Object_Contact__c = objContact.Id;
    	objRel.Relationship_Type__c = 'Has Auditor';
    	objRel.Active__c =true;
    	objRel.Push_to_SAP__c = true;
    	lstRel.add(objRel);
    	objRel = new Relationship__c();
    	objRel.Subject_Account__c = objAccount.Id;
    	objRel.Object_Account__c = objAccount.Id;
    	objRel.Object_Contact__c = objContact.Id;
    	objRel.Relationship_Type__c = 'Has Auditor';
    	objRel.Active__c =true;
    	objRel.Push_to_SAP__c = true;
    	objRel.Start_Date__c = system.today();
    	objRel.End_Date__c = system.today().addYears(1);
    	lstRel.add(objRel);
    	objRel = new Relationship__c();
    	objRel.Subject_Account__c = objAccount.Id;
    	objRel.Object_Contact__c = objContact.Id;
    	objRel.Relationship_Type__c = 'Has Company Secretary';
    	objRel.Active__c =true;
    	objRel.Push_to_SAP__c = true;
    	objRel.Start_Date__c = system.today();
    	objRel.End_Date__c = system.today().addYears(1);
    	lstRel.add(objRel);
    	objRel = new Relationship__c();
    	objRel.Subject_Account__c = objAccount.Id;
    	objRel.Object_Contact__c = objContact.Id;
    	objRel.Relationship_Type__c = 'Has Director';
    	objRel.Active__c =true;
    	objRel.Push_to_SAP__c = true;
    	lstRel.add(objRel);
    	objRel = new Relationship__c();
    	objRel.Subject_Account__c = objAccount.Id;
    	objRel.Object_Contact__c = objContact.Id;
    	objRel.Relationship_Type__c = 'Beneficiary Owner';
    	objRel.Active__c =true;
    	objRel.Push_to_SAP__c = true;
    	lstRel.add(objRel);
    	objRel = new Relationship__c();
    	objRel.Subject_Account__c = objAccount.Id;
    	objRel.Object_Contact__c = objContact.Id;
    	objRel.Relationship_Type__c = 'Has Personal Assistant';
    	objRel.Active__c =true;
    	objRel.Push_to_SAP__c = true;
    	lstRel.add(objRel);
    	objRel = new Relationship__c();
    	objRel.Subject_Account__c = objAccount.Id;
    	objRel.Object_Contact__c = objContact.Id;
    	objRel.Relationship_Type__c = 'Is Shareholder Of';
    	objRel.Active__c =true;
    	objRel.Push_to_SAP__c = true;
    	lstRel.add(objRel);
    	Relationship__c objRel1 = new Relationship__c();
    	objRel1.Subject_Account__c = objAccount.Id;
    	objRel1.Object_Account__c = objAccount.Id;
    	objRel1.Relationship_Type__c = 'Is Shareholder Of';
    	objRel1.Active__c =true;
    	objRel1.Push_to_SAP__c = true;
    	lstRel.add(objRel1);
    	insert lstRel;
    	
    	Relationship__c objRelSH = new Relationship__c();
    	objRelSH.Subject_Account__c = objAccount.Id;
    	objRelSH.Object_Contact__c = objContact.Id;
    	objRelSH.Relationship_Type__c = 'Is Member LLC Of';
    	objRelSH.Active__c =true;
    	objRelSH.Push_to_SAP__c = true;
    	insert objRelSH;
    	
    	list<Shareholder_Detail__c> lstSD = new list<Shareholder_Detail__c>();
    	Shareholder_Detail__c objSD = new Shareholder_Detail__c();
		objSD.Relationship__c = objRelSH.Id;
		objSD.Account__c = objAccount.Id;
		objSD.Account_Share__c = objASD.Id;
		objSD.No_of_Shares__c = 13;
		objSD.Status__c = 'Active';
		lstSD.add(objSD);
		objSD = new Shareholder_Detail__c();
		objSD.Relationship__c = objRel1.Id;
		objSD.Account__c = objAccount.Id;
		objSD.Account_Share__c = objASD.Id;
		objSD.No_of_Shares__c = 13;
		objSD.Status__c = 'Active';
		lstSD.add(objSD);		
		insert lstSD;
		
		map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Change_Details_Add_or_Remove_Director')]){
        	mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
		}
		
		Service_Request__c objSR = new Service_Request__c();
		objSR.Customer__c = objAccount.Id;
		insert objSR;
		
		test.startTest();
			SRHistoryDetails.TrackHistory(objSR);
			SRHistoryDetails.TrackHistory(objSR);
			string sHistory = SRHistoryDetails.getAuditorDetails(objSR);
			SRHistoryDetails.parseHistory(sHistory);
			sHistory = SRHistoryDetails.getAuthorizedSharesNominalValueDetails(objSR);
			SRHistoryDetails.parseHistory(sHistory);
			sHistory = SRHistoryDetails.getClientInformation(objSR);
			SRHistoryDetails.parseHistory(sHistory);
			sHistory = SRHistoryDetails.getShareDetails(objSR);
			SRHistoryDetails.parseHistory(sHistory);
			sHistory =SRHistoryDetails.getUBODetails(objSR);
			SRHistoryDetails.parseHistory(sHistory);
			objSR.RecordTypeId = mapRecordTypeIds.get('Change_Details_Add_or_Remove_Director');
			update objSR;
			for(Service_Request__c obj :[select Id,Name,Customer__c,Record_Type_Name__c,Account_Legal_Entity__c from Service_Request__c where Id=:objSR.Id]){
				objSR = obj;
			}
			sHistory = SRHistoryDetails.getIndividualDetails(objSR);
			SRHistoryDetails.parseHistory(sHistory);
			
			SRHistoryDetails objSRHistoryDetails = new SRHistoryDetails();
			set<string> HistoryRecordTypes = objSRHistoryDetails.HistoryRecordTypes;
			
		test.stopTest();
    }
    
    static testMethod void myUnitTest1() {
 		Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.BP_No__c = '000400034';
        objAccount.Financial_Year_End__c = 'March-31';
        objAccount.Phone = '+97112345678';
        objAccount.PO_Box__c = '123456';
        objAccount.Sector_Classification__c = 'test';
    	insert objAccount;
    	
    	Unit__c objUnit = new Unit__c();
    	objUnit.Name = '12345';
    	objUnit.Floor__c = '12';
    	objUnit.SAP_Unit_No__c ='12345';
    	objUnit.Unit_Square_Feet__c  = 12345;
    	insert objUnit;
    	
    	Operating_Location__c objOL = new Operating_Location__c();
    	objOL.IsRegistered__c = true;
    	objOL.Unit__c = objUnit.Id;
    	objOL.Status__c = 'Active';
    	objOL.Account__c = objAccount.Id;
    	insert objOL;
    	
    	Account_Share_Detail__c objASD = new Account_Share_Detail__c();
		objASD.Account__c = objAccount.Id;
		objASD.No_of_shares_per_class__c = 1000;
		objASD.Nominal_Value__c = 1000;
		objASD.Start_Date__c = system.today();
		objASD.Status__c = 'Active';
		insert objASD;
    	
    	Contact objContact = new Contact();
    	objContact.FirstName = 'Test';
    	objContact.LastName = 'Babu';
    	objContact.Email = 'test@test.ae';
    	objContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Contact').getRecordTypeId();
    	insert objContact;
    	
 		map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Change_of_Address_Details','Application_of_Registration')]){
        	mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
		}
		
		Service_Request__c objSR = new Service_Request__c();
		objSR.Customer__c = objAccount.Id;
		objSR.RecordTypeId = mapRecordTypeIds.get('Change_of_Address_Details');
		insert objSR;
		
		for(Service_Request__c obj :[select Id,Name,Customer__c,Record_Type_Name__c,Account_Legal_Entity__c from Service_Request__c where Id=:objSR.Id]){
			objSR = obj;
		}
		string sHistory = SRHistoryDetails.getOperatingLocations(objSR);
		SRHistoryDetails.RecordTypeName = 'Change_of_Address_Details';
		SRHistoryDetails.parseHistory(sHistory);
		
		objSR.RecordTypeId = mapRecordTypeIds.get('Application_of_Registration');
		update objSR;
		
		list<Amendment__c> lstAmendments = new list<Amendment__c>();
		Amendment__c objAmd = new Amendment__c();
		objAmd.ServiceRequest__c = objSR.Id;
		objAmd.Customer__c = objAccount.Id;
		objAmd.Relationship_Type__c = 'Shareholder';
		objAmd.Company_Name__c = 'Test Company';
		objAmd.Registration_Date__c = system.today().addDays(-10);
		objAmd.Permanent_Native_Country__c = 'UAE';
		objAmd.Place_of_Registration__c = 'Dubai';
		objAmd.Amendment_Type__c = 'Body Corporate';
		objAmd.Title_new__c = '';
		objAmd.Family_Name__c = '';
		objAmd.Middle_Name__c = '';
		objAmd.Given_Name__c = '';
		objAmd.Passport_No__c = '';
		objAmd.Nationality_list__c = '';
		lstAmendments.add(objAmd);
		
		objAmd = new Amendment__c();
		objAmd.ServiceRequest__c = objSR.Id;
		objAmd.Customer__c = objAccount.Id;
		objAmd.Relationship_Type__c = 'Shareholder';
		objAmd.Company_Name__c = 'Test Company';
		objAmd.Registration_Date__c = system.today().addDays(-10);
		objAmd.Permanent_Native_Country__c = 'UAE';
		objAmd.Place_of_Registration__c = 'Dubai';
		objAmd.Amendment_Type__c = 'Individual';
		objAmd.Title_new__c = 'Mr.';
		objAmd.Family_Name__c = 'RB';
		objAmd.Middle_Name__c = 'Chowdary';
		objAmd.Given_Name__c = 'Nagaboina';
		objAmd.Passport_No__c = '123456';
		objAmd.Nationality_list__c = 'India';
		lstAmendments.add(objAmd);
		
		insert lstAmendments;
		
		list<Shareholder_Detail__c> lstSD = new list<Shareholder_Detail__c>();
    	Shareholder_Detail__c objSD = new Shareholder_Detail__c();
		objSD.Account__c = objAccount.Id;
		objSD.Account_Share__c = objASD.Id;
		objSD.No_of_Shares__c = 13;
		objSD.Status__c = 'Draft';
		objSD.Amendment__c = lstAmendments[0].Id;
		lstSD.add(objSD);
		objSD = new Shareholder_Detail__c();
		objSD.Account__c = objAccount.Id;
		objSD.Account_Share__c = objASD.Id;
		objSD.No_of_Shares__c = 13;
		objSD.Status__c = 'Draft';
		objSD.Amendment__c = lstAmendments[1].Id;
		lstSD.add(objSD);		
		insert lstSD;
		
		for(Service_Request__c obj :[select Id,Name,Customer__c,Record_Type_Name__c,Account_Legal_Entity__c from Service_Request__c where Id=:objSR.Id]){
			objSR = obj;
		}
		sHistory = SRHistoryDetails.getAORData(objSR);
		SRHistoryDetails.RecordTypeName = 'Application_of_Registration';
		SRHistoryDetails.parseHistory(sHistory);
		
		License_Activity_Master__c LAM= new License_Activity_Master__c();
        LAM.name='Test Activity';
        LAM.Type__c='License Activity';
        LAM.Sector_Classification__c='test';
        LAM.Enabled__c=true;
        insert LAM;
        
        License_Activity__c LA= new License_Activity__c();
        LA.Account__c = objAccount.id;
        LA.Activity__c=LAM.id;  
        LA.Sector_Classification__c='test';      
        insert LA;
        
        sHistory = SRHistoryDetails.getBusinessActivities(objSR);
		SRHistoryDetails.parseHistory(sHistory);
        
    }
}