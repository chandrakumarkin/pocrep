@isTest
public with sharing class CLS_BatchCalculateCaseAgeTest {
    
    public static void init(){
        
        Case caseObj = new Case(
        GN_Type_of_Feedback__c = 'Enquiry',
        Status = 'In Progress',
        Origin = 'Phone',
        CreatedDate = System.now(),
        Full_Name__c='Raghav');
        
        insert caseObj;
    }
    
    @isTest static void test(){
        
        init();
        Test.startTest(); 
        CLS_BatchCalculateCaseAge t1= new CLS_BatchCalculateCaseAge();
        Database.executeBatch(t1); 
        Test.stopTest();
    }
}