/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 04-14-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   04-11-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public class AccountTriggerHelper{

    public static void handleAfterUpdateAction(Map < Id, Account > accountMap) {
        List < Account > accountRecords = [Select id, name, recordTypeId,
            (Select Contractor__c,Email__c, Nationality__c, Nationality_list__c, Passport_Number__c, Place_of_Birth__c, First_Name__c, Last_Name__c, Nationality_AR__c, Email_Address__c, id, name, Customer__c, Company_Type_list__c, Service_type__c, recordType.Developername from Service_Requests__r where recordType.Developername = 'Contractor_Registration' or recordType.Developername='Event_Service_Request'),
            (Select id, Role__c,name, firstname, lastname, Email, Nationality_Lookup__c, Nationality__c, Passport_No__c, Place_of_Birth__c from Contacts where recordType.DeveloperName = 'Portal_User')
            from Account where Id IN: accountMap.keyset()
        ];
        String errorMessage ='';
        Id accountId = null;
        for(Account accRec : accountRecords){
            for (Service_Request__c serviceRequestRec: accRec.Service_Requests__r) {
                if(serviceRequestRec.Contractor__c == null && serviceRequestRec.recordType.Developername == 'Event_Service_Request'){
                    errorMessage = 'Please update the Contractor details on the service request '+serviceRequestRec.Name;
                    accountId = accRec.id;
                    break;
                }
                for (Contact contactRec: accRec.Contacts) {
                    if (contactRec.FirstName == serviceRequestRec.First_Name__c && contactRec.LastName == serviceRequestRec.Last_Name__c &&
                        contactRec.Email == serviceRequestRec.Email_Address__c && contactRec.Passport_No__c == serviceRequestRec.Passport_Number__c && contactRec.Role__c !='Event Services'
                        && serviceRequestRec.recordType.Developername == 'Contractor_Registration') {
                            errorMessage = 'Please update the Contact role to Event Services for the contact '+contactRec.Name;
                            accountId = accRec.id;
                            break;
                    }
                }
            }
        }
        if(errorMessage != ''){
            accountMap.get(accountId).addError(errorMessage);
        }
    }
    public static void updatePendingLandlordNocStep(Set<id> accountIds){
        if(accountIds != Null && accountIds.size() > 0){
            //Get all related fit out requests with Issue Landlord NOC step
            List<Service_Request__c> serviceReqList = new List<Service_Request__c>(); 
            serviceReqList = [Select id,Name,(Select id,name,Status__c,Step_Name__c from Steps_SR__r where Step_Name__c='Landlord NOC Issued' AND Status__r.Name='Pending') from Service_Request__c where service_type__c ='Fit - Out Service Request' AND Customer__c in : accountIds];
           
            List<Status__c> stepStatus = new List<Status__c>();
            stepStatus = [Select id from Status__c where Name='Completed' limit 1];
            
            List<Step__c> stepList =  new List<Step__c>();
            for(Service_Request__c servReq : serviceReqList){
                for(Step__c step : servReq.Steps_SR__r){ 
                    //system.assertEquals(null, step);
                    step.Status__c = stepStatus[0].id;
                    stepList.add(step);
                }
            }
            if(stepList !=Null && stepList.size() > 0) update stepList;
        }
    }
    
    
    public static void createaBankAccount(List<Account> lstAccounts){
         
        RecursiveControlCls.isDataSenttoMashreq = true;
        Set<Id> setAccountID = new Set<Id>();
        List<String> lstEmailAddress = new List<String>();
        String portalUserID = '';
        List<Messaging.SingleEmailMessage> emails = new List<Messaging.SingleEmailMessage>();
        EmailTemplate eTemplate= new EmailTemplate();
        eTemplate = [Select id from EmailTemplate where name=:Label.Email_Template_Name_to_Open_Bank_Account];
        HexaBPM__Service_Request__c serviceRequest = new HexaBPM__Service_Request__c();
        system.debug('!!@@##'+lstAccounts);
        String AccountID;
        
        for(Account eachAccount:lstAccounts){
            system.debug('!!@##');
            setAccountID.add(eachAccount.ID);
            AccountID = eachAccount.ID;
        }
        system.debug('!!@@##'+serviceRequest);
        
        serviceRequest = new HexaBPM__Service_Request__c();
        serviceRequest.RecordtypeID = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get(Label.Open_a_Bank_Account).getRecordTypeId();   
        serviceRequest.HexaBPM__Customer__c = AccountID;
        serviceRequest.HexaBPM__Auto_Submit__c = true;
        serviceRequest.HexaBPM__Submitted_Date__c = system.today();
        serviceRequest.HexaBPM__Submitted_DateTime__c = system.now();
        serviceRequest.HexaBPM__FinalizeAmendmentFlg__c = true;
        INSERT serviceRequest;
        
       for(AccountContactRelation thisRelation :[SELECT AccountId,ContactId,Contact.Email FROM AccountContactRelation where AccountId IN:setAccountID AND 
                           Contact.Recordtype.DeveloperName ='Portal_User' AND Roles INCLUDES ('Company Services')]){
            lstEmailAddress.add(thisRelation.Contact.Email);
            portalUserID = thisRelation.ContactId;
        }
       
        OrgWideEmailAddress emailAddress = new OrgWideEmailAddress();
        emailAddress = [select id from OrgWideEmailAddress where Address=:Label.ORGIdforMashreq limit 1];
        
        system.debug('!!@##@@'+portalUserID);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSaveAsActivity(false);
        mail.setTargetObjectId(portalUserID);
        mail.setTemplateId(eTemplate.Id);
        mail.setToAddresses(lstEmailAddress);  //add other emails here.
        mail.setOrgWideEmailAddressId(emailAddress.ID);
        system.debug('!!@##@@'+mail);
        emails.add(mail);
        Messaging.sendEmail(emails);  
    }
     
    public static void changeRelationshipManageronAOR(Map<Id, Account> newAccountMap,Map<Id, Account> oldAccountMap){

        Set<Id> accountids                = new Set<Id>();
        List<HexaBPM__Step__c> actionList = new List<HexaBPM__Step__c>();
        Map<Id,Account> accountMap        = new  Map<Id,Account>();

        User integrationUserId = new user();

        if ([SELECT Id  FROM User Where userName =: system.label.UserName LIMIT 1].size() > 0 ){ 
        
            integrationUserId =  [SELECT Id 
                                     FROM User 
                                     Where userName =: system.label.UserName LIMIT 1];

            for(Account thisAccount : newAccountMap.values()){

                if(thisAccount.ownerId != oldAccountMap.get(thisAccount.Id).ownerId 
                && oldAccountMap.get(thisAccount.Id).ownerId == integrationUserId.Id){
                    
                    accountids.add(thisAccount.Id);
                    accountMap.put(thisAccount.Id,thisAccount);
               }
        }
        }

        if(!accountids.isEmpty()){
          for(HexaBPM__Step__c thisAction : [SELECT Id,
                                                    OwnerId,
                                                    HexaBPM__SR__r.HexaBPM__Customer__c
                                                    FROM HexaBPM__Step__c 
                                                    WHERE HexaBPM__SR__r.HexaBPM__Customer__c IN :accountids ]){
            
            if(thisAction.OwnerId == integrationUserId.Id){
               thisAction.ownerId =  accountMap.get(thisAction.HexaBPM__SR__r.HexaBPM__Customer__c).OwnerId;
               actionList.add(thisAction);
            }
        }

         if(!actionList.isEmpty()){
              Update actionList;
          }
        }
    }
}