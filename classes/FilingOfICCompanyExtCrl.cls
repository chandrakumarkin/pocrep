/*
Created By  : Suchita - DIFC on 23 May 2021 
Description : This Class is extension class for FilingOfICCompany visualforce page
--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No        Date                Updated By          Description
v1.0      23 May 2021          Suchita Sharma,       This Class is extension class for FilingOfICCompany visualforce page

----------------------------------------------------------------------------------------
*/
public class FilingOfICCompanyExtCrl{
    public Service_Request__c SRData { get; set;}
    public string RecordTypeId;
    public map < string, string > mapParameters;
    public List<Shareholder_Detail__c> ListShareholder { get; set; }
    public Attachment ObjAttachment { get;set; }
    public boolean SRisValid { get;set; }
    public Account ObjAccount { get;set; }
    public List<String> SuffixList = new List<String>{'Ltd','Limited','PLC'};
    public boolean IsSuffixAvailable{get;set;}
    public string EntityName{get;set;}
    public List<SelectOption> suffixOption{set;}
	public String selectedSuffix{get;set;}
    public boolean IsSuffixAvailableTrade{get;set;}
    public string TradeName{get;set;}
    public String selectedSuffixTrade{get;set;}
    public String selectedICType{get;set;}
    private string Rtype; //sR Record in URL 
    public String selectedCompanyId{get; set;}
  	public String selectedCompanyName{get; set;}
    public String LicenseActivityType;
    Public string conversionValidation = 'Conversion to an Incorporated Cell is only allowed for entities conducting Fund or Insurance business.';
    public FilingOfICCompanyExtCrl (ApexPages.StandardController controller) {
        
        SRisValid = false;
        SRData = (Service_Request__c) controller.getRecord();
        
        Service_Request__c SRDataObj;
        if( SRData.Id != null ){
            SRDataObj = [SELECT Record_Type_Name__c, RecordTypeId,Entity_Name__c,Proposed_Trading_Name_1__c,Usage_Type__c,Title__c,Transfer_to_account__c FROM Service_Request__c WHERE Id =: SRData.Id];
        	IsSuffixAvailable = false;
            selectedCompanyName=SRDataObj.Title__c;
            selectedCompanyId=SRDataObj.Transfer_to_account__c;
            if(SRDataObj.Entity_Name__c!=null){
                String EntityNameWithSuffix = SRDataObj.Entity_Name__c;
                
                EntityName= EntityNameWithSuffix;
                IsSuffixAvailable = false;
                for(String suffixString:SuffixList){
                    if(EntityNameWithSuffix.endsWithIgnoreCase(suffixString)){
                        IsSuffixAvailable = true;
                        EntityName = EntityNameWithSuffix.removeEndIgnoreCase(suffixString);
                        break;
                    }
                }
            }
            IsSuffixAvailableTrade = false;
            If(SRDataObj.Proposed_Trading_Name_1__c!=null){
               String TradeNameWithSuffix = SRDataObj.Proposed_Trading_Name_1__c;
                TradeName= TradeNameWithSuffix;
                IsSuffixAvailableTrade = false;
                for(String suffixStringTrade :SuffixList){
                    if(TradeNameWithSuffix.endsWithIgnoreCase(suffixStringTrade)){
                        IsSuffixAvailableTrade = true;
                        TradeName = TradeNameWithSuffix.removeEndIgnoreCase(suffixStringTrade);
                        break;
                    }
                }   
            }
        }
        
        ObjAccount = new account();
        ObjAttachment = new Attachment();
        if (apexpages.currentPage().getParameters() != null)
            mapParameters = apexpages.currentPage().getParameters();
        if (mapParameters.get('RecordType') != null)
            RecordTypeId = mapParameters.get('RecordType');
        
        if (mapParameters.get('type') != null){
            Rtype = mapParameters.get('type');
        }else if( SRDataObj != null){
            Rtype = SRDataObj.Record_Type_Name__c;
            RecordTypeId = SRDataObj.RecordTypeId;
        }
        PopulateInitialData();  
        Boolean activityValidation=checkValidActivity();
        if(activityValidation){           
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.SEVERITY.FATAL, conversionValidation);
            ApexPages.addMessage(myMsg);
        }else{SRisValid = true;}      
    }
     public List<SelectOption> getICTypeOption() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new Selectoption('','--Select Type--'));
        if(LicenseActivityType=='Investment Fund'){
            options.add(new SelectOption('Open-ended Incorporated Cell','Open-ended Incorporated Cell'));
        }
        options.add(new SelectOption('Closed-ended Incorporated Cell','Closed-ended Incorporated Cell'));
        return options;
    }
    public List<SelectOption> getsuffixOption () {
        List<SelectOption> options = new List<SelectOption>();
        if(selectedICType!=null){
            options.add(new SelectOption(selectedICType,selectedICType));
        }
        return options;
    }
    public void ICTypeOptionChange(){
        //Call
    }
    public void setICCCompanydetails(){
      Account selectedAccount = [SELECT Id, Name, Active_License__r.License_Issue_Date__c, Registration_License_No__c FROM Account WHERE Id = :selectedCompanyId];
   	  selectedCompanyName = selectedAccount.Name;
      selectedCompanyId = '';
    } 
    Public boolean checkValidActivity(){
        
        Boolean activityValidation= true;
        for(License_Activity__c act:[select id,Activity_Name_Eng__c from License_Activity__c where Account__c=:SRData.Customer__c and Activity_Name_Eng__c in('Investment Fund','Insurance Management','Insurance Intermediation')]){
            activityValidation=false;
            LicenseActivityType=act.Activity_Name_Eng__c;
        }
        
        return activityValidation;
    }
    public PageReference SaveRecord() {
        
        Date dt = System.today();//current date
     
        try {
            if(IsSuffixAvailable){
                SRData.Entity_Name__c =  EntityName+' '+selectedICType;
                //SRData.Legal_entity_of_company_before_transfer__c = selectedSuffix;
            }
            if(IsSuffixAvailableTrade){
                SRData.Proposed_Trading_Name_1__c =  TradeName+' '+selectedICType;
                //SRData.Legal_entity_of_company_before_transfer__c = selectedSuffixTrade;
            }
            if(selectedICType!=null){
                SRData.Relation__c=selectedICType;
            }
            if(selectedICType!=null){
                SRData.Title__c=selectedCompanyName;
            }
            upsert SRData;
            PageReference acctPage = new ApexPages.StandardController(SRData).view();
            acctPage.setRedirect(true);
            return acctPage;
        } catch (Exception e) {
            ApexPages.Message myMsg =new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getDmlMessage(0));
            ApexPages.addMessage(myMsg);
            return NULL;
        }
   
    }
    
    public void PopulateInitialData(){
        for (User objUsr: [SELECT id, ContactId, Email, Contact.Account.Legal_Type_of_Entity__c, Phone, Contact.AccountId, 
                           Contact.Account.Name,Contact.Account.Trading_Name_Arabic__c,Contact.Account.Trade_Name__c,Contact.Account.Arabic_Name__c,Contact.Account.OB_Sector_Classification__c 
                           FROM User 
                           WHERE Id =: userinfo.getUserId() ]) {
                               
                               if (SRData.id == null) {
                                   
                                   SRData.Customer__c = objUsr.Contact.AccountId;
                                   SRData.RecordTypeId = RecordTypeId;
                                   SRData.Email__c = objUsr.Email;
                                   SRData.Legal_Structures__c = objUsr.Contact.Account.Legal_Type_of_Entity__c;
                                   SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                                   SRData.Entity_Name__c = objUsr.Contact.Account.Name;
                                   IsSuffixAvailable = false;
                                   if(objUsr.Contact.Account.Name!=null){
                                       String EntityNameWithSuffix = objUsr.Contact.Account.Name;
                                       EntityName= EntityNameWithSuffix;
                                       for(String suffixString:SuffixList){
                                           if(EntityNameWithSuffix.endsWithIgnoreCase(suffixString)){
                                               IsSuffixAvailable = true;
                                               EntityName = EntityNameWithSuffix.removeEndIgnoreCase(suffixString);
                                               break;
                                            }
                                       }
                                   }
                                   IsSuffixAvailableTrade = false;
                                   if(objUsr.Contact.Account.Trade_Name__c!=null){
                                       String TradeNameWithSuffix = objUsr.Contact.Account.Trade_Name__c;
                                       TradeName= TradeNameWithSuffix;
                                       for(String suffixStringTrade:SuffixList){
                                           if(TradeNameWithSuffix.endsWithIgnoreCase(suffixStringTrade)){
                                               IsSuffixAvailableTrade = true;
                                               TradeName = TradeNameWithSuffix.removeEndIgnoreCase(suffixStringTrade);
                                               break;
                                            }
                                       }
                                   }
                                   SRData.Previous_Entity_Name__c = objUsr.Contact.Account.Name;
                                   SRData.Pre_Entity_Name_Arabic__c= objUsr.Contact.Account.Arabic_Name__c;
                                   SRData.Pro_Entity_Name_Arabic__c=objUsr.Contact.Account.Arabic_Name__c;
                                   SRData.Previous_Trading_Name_1__c = objUsr.Contact.Account.Trade_Name__c;
                                   SRData.Proposed_Trading_Name_1__c = objUsr.Contact.Account.Trade_Name__c;
                                   SRData.Pre_Trade_Name_1_in_Arab__c=objUsr.Contact.Account.Trading_Name_Arabic__c;
                                   SRData.pro_trade_name_1_in_arab__c=objUsr.Contact.Account.Trading_Name_Arabic__c;
                                   SRData.Usage_Type__c=objUsr.Contact.Account.OB_Sector_Classification__c;
                                   
                                   ObjAccount = objUsr.Contact.Account;
                               }
                           }
    }
}