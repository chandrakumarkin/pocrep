/******************************************************************************************
 *  BatchVisaExceptionUpdate
 
 *  Author      : Veera
 *  Company     : techCarrot
 *  Date        : 2018-07-23    
 *  Modification 
        History :
----------------------------------------------------------------------
   Version  Date         Author             Remarks                                                 
  =======   ==========   =============  ==================================
  
*******************************************************************************************/


global with sharing class BatchVisaExceptionUpdate implements Database.Batchable <sObject>,Schedulable  {

   global void execute(SchedulableContext ctx) {
    Database.executeBatch(new BatchVisaExceptionUpdate(), 100);
  }

 global List <Visa_Exception__c> start(Database.BatchableContext BC) {

  List < Visa_Exception__c> lstVisaExc = [select id,Account__c,Exception_Visa_Quota__c,Visa_Exception_Review_Date__c from Visa_Exception__c where Visa_Exception_Review_Date__c = YESTERDAY ];
  return lstVisaExc ;
 }

 global void execute(Database.BatchableContext BC, list < Visa_Exception__c> lstVisaExc ) {
  List <Account> lstVisasToUpdate = new List < Account> (); 
  
  List<string> AccountIds = new List<string>();
  
   map<id,Account> accountMap = new Map<id,Account>();
  
  try {  
      
      for(Visa_Exception__c  visaEx : lstVisaExc ){         
        AccountIds.add(visaEx.Account__c) ;     
      }
      
      if(AccountIds.size()>0) {
      
        for(Account acc: [select id,Exception_Visa_Quota__c from Account where id in:AccountIds]){      
          accountMap.put(acc.id,acc);      
        }
      }
      if(accountMap.size()>0){
      
       Account acc = new Account();
      
        for(Visa_Exception__c  visaEx : lstVisaExc){
         
            acc.id = visaEx.Account__c;
            acc.Exception_Visa_Quota__c  =  accountMap.get(visaEx.Account__c).Exception_Visa_Quota__c  - visaEx.Exception_Visa_Quota__c ;
            lstVisasToUpdate.add(acc); 
           
        }
        
        if(lstVisasToUpdate.size()>0)update lstVisasToUpdate;
      
      }
     
  }catch (Exception ex) {
   Log__c objLog = new Log__c();
   objLog.Type__c = 'Schedule Batch Visa Exception status Update ';
   objLog.Description__c = 'Exceptio is : ' + ex.getMessage() + '\nLine # ' + ex.getLineNumber();
   insert objLog;
  }

 }


 global void finish(Database.BatchableContext BC) {}
}