public class CarParking_ADCB_PayementConfirm {
    public  Map<String,String> Parameter{get;set;}
    string req_reference_number{get;set;}
    string transaction_id{get;set;}
    public boolean IsAdded{get;set;}
    public static String cardUID{get;set;}
    public CarParking_ADCB_PayementConfirm (){  
        Parameter= ApexPages.currentPage().getParameters();
        /*System.debug('before: '+Parameter.get('req_reference_number'));
        string referenceNumber = ApexPages.currentPage().getHeaders().get('referer');
        ApexPages.PageReference ref = new ApexPages.PageReference(referenceNumber);
        Parameter = ref.getParameters();
        System.debug('after: '+Parameter.get('req_reference_number'));*/
        
        IsAdded=false;
    }
    public PageReference updateReceipt(){
        Id sobjectRecId;
        String reqRefNumber;
        Boolean payStatus = false;
        if(Parameter.get('req_reference_number')!=null){
            reqRefNumber = Parameter.get('req_reference_number');
            sobjectRecId = ( reqRefNumber.length() > 18 ? reqRefNumber.split('_')[0] : reqRefNumber );
            String sobjectType = sobjectRecId.getSObjectType().getDescribe().getName();            
            if(Parameter.get('decision')=='ACCEPT'){
                payStatus = true;
                If( reqRefNumber.length() >= 18 && sobjectType == 'Car_Parking_Details_Visitor_Valet__c'){
                    updateReceiptAndCarParkDet(reqRefNumber.split('_')[1],reqRefNumber.split('_')[0],Parameter);
                }
            }
            else{
                payStatus = false;
                LogCreate(string.valueof(Parameter),'Car Parking Payment','');
            }
        }
        PageReference pageRef;
        if(reqRefNumber != null && reqRefNumber.length() > 18){     
            pageRef = new PageReference(Label.ADCB_Portal_URL+'/s/car-parking-payment-result?s='+(payStatus ? 'y' :'n')+'&rid='+EncodingUtil.base64Encode(Blob.valueOf(reqRefNumber.split('_')[1])));
        }else{
            pageRef = new PageReference(Label.Community_URL+'/'+Parameter.get('req_reference_number')); 
        }
        return pageRef;
    }
    public void updateReceiptAndCarParkDet(string receiptId,String carParkDetId,Map<String,String> tempParameter){
        try{            
            Car_Parking_Receipt__c ObjRec = new Car_Parking_Receipt__c( Id = receiptId );
            ObjRec.PG_Error_Message__c=tempParameter.get('message');
            if(tempParameter.get('decision')=='ACCEPT')         
                ObjRec.Payment_Status__c='Success';
            else
                ObjRec.Payment_Status__c='Failure';
            
            if(tempParameter.get('score_card_scheme')!=null)
                ObjRec.Card_Type__c=tempParameter.get('score_card_scheme');
            if(tempParameter.get('transaction_id')!=null){
                ObjRec.Payment_Gateway_Ref_No__c = tempParameter.get('transaction_id'); 
                ObjRec.Bank_Ref_No__c            = tempParameter.get('transaction_id');
            }
            if(tempParameter.get('card_type_name') != null){
                ObjRec.Card_Type__c = tempParameter.get('card_type_name');
            }            
            if(String.isNotBlank(ObjRec.Id)){
                update ObjRec;
            }
            Car_Parking_Details_Visitor_Valet__c obj = new Car_Parking_Details_Visitor_Valet__c();
            if(String.isNotBlank(carParkDetId)){
                obj = [Select Id, Exit_Time__c, Type__c
                       from Car_Parking_Details_Visitor_Valet__c
                       where id =:carParkDetId limit 1];//new Car_Parking_Details_Visitor_Valet__c();
                obj.Id = carParkDetId;
                if(obj.Type__c == 'Visitor'){
                    obj.Exit_Time__c = System.now();
                    update obj;
                }//check if it applies only to visitor or both
            }
            System.debug('decision: '+tempParameter.get('decision')=='ACCEPT');
            System.debug('isblank: '+String.isNotBlank(carParkDetId));
            if(tempParameter.get('decision')=='ACCEPT' && String.isNotBlank(carParkDetId)){
                System.debug('Entered in if');
                if(obj.Type__c == 'Visitor'){
                    updateDeignaWithPaymentDetails(carParkDetId);
                }
                else{
                    updateSparkWithPaymentDetails(carParkDetId);
                }
            }
        }
        catch(Exception e){
            LogCreate(e.getMessage(),'Visitor/Valet Parking Payment','');
            /*Log__c ObjLog         = new Log__c();
            ObjLog.Description__c = e.getMessage();
            ObjLog.Type__c        = 'Topup Recharge Payment.';
            insert ObjLog;*/
        }
    }
    @future(callout=true)
    public static void updateSparkWithPaymentDetails(String carParkDetId){
        try{
            Car_Parking_Details_Visitor_Valet__c carParkDet = [Select Id, Amount__c, Email__c, Mail_Invoice__c, 
                                                               Vehicle_Number__c, Ticket_Number__c,Type__c, 
                                                               Vehicle_Series__c, Emirates__c, Card_Number__c, 
                                                               Exit_Time__c, Entry_Time__c, Location__c, 
                                                               Valet_Transaction_Id__c, Hours__c, Plate_Number__c, 
                                                               Requested_DateTime__c
                                                               from Car_Parking_Details_Visitor_Valet__c
                                                               where id =: carParkDetId];
            CarParking_Visitors_Valet_Controller.JSONResponseWrapper obj = new 
                CarParking_Visitors_Valet_Controller.JSONResponseWrapper();
            obj.DIFC_TID = '1';
            obj.SPARK_TID = carParkDet.Valet_Transaction_Id__c;
            obj.Card_Number = carParkDet.Card_Number__c;
            obj.Entity_ID = '3';
            obj.Location_ID = 4;
            obj.RDateTime = carParkDet.Requested_DateTime__c.formatGMT('yyyy-MM-dd\'T\'HH:mm:ss');
            obj.Amount = Integer.valueOf(carParkDet.Amount__c);
            obj.Currency_Z = 'AED';
            obj.Payment_Status = true;
            String reqBody = JSON.serialize(obj, true);
            reqBody = reqBody.contains('"Currency_Z"')? reqBody.replace('"Currency_Z"', '"Currency"') : reqBody;
            reqBody = reqBody.contains('"Payment_Status"')? reqBody.replace('"Payment_Status"', '"Payment Status"') : reqBody;
            HttpResponse response = CarParking_Visitors_Valet_Controller.fetchDetailsFromSpark(reqBody, 'https://sparkappae.tech:3443/DIFC_Spark_SetPaymentStatus', carParkDet);
            if(response != null){
                if(response.getStatusCode() ==201 || response.getStatusCode() == 200){
                    System.debug('response: '+response.getBody());
                    string resBody = response.getBody();
                    resBody = resBody.contains('"Currency"') ? resBody.replace('"Currency"','"Currency_Z"') : resBody;
                    resBody = resBody.contains('"Payment Status"') ? resBody.replace('"Payment Status"','"Payment_Status"') : resBody;
                    CarParking_Visitors_Valet_Controller.JSONResponseWrapper respWrapObj = (CarParking_Visitors_Valet_Controller.JSONResponseWrapper)
                        JSON.deserialize(resBody, CarParking_Visitors_Valet_Controller.JSONResponseWrapper.class);
                    if(respWrapObj.ResponseStatus == 200){
                        carParkDet.Updated_ThirdParty_with_Payment_Details__c = 'Success';
                        update carParkDet;
                    }
                    else{
                        carParkDet.Updated_ThirdParty_with_Payment_Details__c = 'Failed';
                        update carParkDet;
                    }
                }
                else{
                    carParkDet.Updated_ThirdParty_with_Payment_Details__c = 'Failed';
                    update carParkDet;
                }
            }
        }
        catch(Exception e){
            LogCreate(e.getMessage(),'Update Payment details in Spark.','');
        } 
    }
    @future(callout=true)
    public static void updateDeignaWithPaymentDetails(String carParkDetId){
        try{            
            List<Car_Parking_API_Service_fields__mdt> serviceFields = 
                [SELECT Request_Name__c, Sequence__c, Namespace__c, 
                 Parent_Node__c, Type__c, Xml_Label__c, Source_Object__c, 
                 Field_Name__c FROM Car_Parking_API_Service_fields__mdt WHERE 
                 Request_Name__c = 'setCardSettlement' ORDER BY Sequence__c];
            if(serviceFields.size()>0){
                Car_Parking_Details_Visitor_Valet__c carParkDet = [Select Id, Amount__c, Email__c, Mail_Invoice__c, 
                                                                   Vehicle_Number__c, Ticket_Number__c,
                                                                   Type__c, Vehicle_Series__c, Emirates__c, 
                                                                   Card_Number__c, Exit_Time__c, Entry_Time__c, 
                                                                   Location__c, Valet_Transaction_Id__c, 
                                                                   Hours__c, Plate_Number__c from 
                                                                   Car_Parking_Details_Visitor_Valet__c
                                                                   where id =: carParkDetId];
                Dom.XMLNode rootEle = CarParking_Visitors_Valet_Controller.generatexmlRequest(serviceFields, carParkDet,'setCardSettlement');
                if(rootEle != null){
                    processSecondResXML(rootEle, carParkDet.Ticket_Number__c);
                    if(cardUID != null && cardUID != ''){
                        System.debug('Updated Designa with payment details');
                        carParkDet.CardUID__c = cardUID;
                        carParkDet.Updated_ThirdParty_with_Payment_Details__c = 'Success';
                        update carParkDet;
                    }
                    else{
                        carParkDet.Updated_ThirdParty_with_Payment_Details__c = 'Failed';
                        update carParkDet;
                    }
                }
                else{
                    carParkDet.Updated_ThirdParty_with_Payment_Details__c = 'Failed';
                    update carParkDet;
                }
            }
        }
        catch(Exception e){
            LogCreate(e.getMessage(),'Update Payment details in Designa.','');
        }
    }
    public static void processSecondResXML(Dom.XmlNode inputNode, String CardNumber){
        String currentNodeName;
        for(Dom.XmlNode childNode :inputNode.getChildElements()){
            //System.debug('childNode: '+childNode);
            currentNodeName = childNode.getName();
            if(currentNodeName == 'setCardSettlementResult'){
                //System.debug('cardUID: '+cardUID);
                if(childNode.getChildElement('CardUId', 'http://www.designa.de/') != null){                    
                    cardUID = childNode.getChildElement('CardUId', 'http://www.designa.de/').getText();                    
                }
            }
            else{
                processSecondResXML(childNode, CardNumber);
            }
        }
    }
    public static void LogCreate(string Description,string type,string ReceiptId)
    {
        Log__c ObjLog = new Log__c();
        ObjLog.Description__c=Description; 
        ObjLog.Type__c=type; 
        if(ReceiptId!='')
            ObjLog.Receipt__c=ReceiptId;
        insert ObjLog;
        
    }
}