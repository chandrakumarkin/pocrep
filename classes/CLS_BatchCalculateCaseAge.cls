/*************************************************************************************************
 *  Name        : CLS_BatchCalculateCaseAge 
 *  Author      : Kaavya Raghuram
 *  Company     : PWC
 *  Date        : 5-Mar-2018    
 *  Purpose     : This class is to calculate the case age in business days for all open cases   
 ---------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              

**************************************************************************************************/
global class CLS_BatchCalculateCaseAge implements Database.Batchable<sObject>{

    global Database.QueryLocator start(Database.BatchableContext info){ 
        string query = 'select id,CreatedDate,GN_Case_Age__c,isclosed,GN_Case_Age_str__c,(select id, From__c, Owner__c, Business_Hours_Id__c, Until__c from SLA__r WHERE GN_Is_Current_Case_Sys_Queue__c = true AND Business_Hours_Id__c <> null LIMIT 1) from Case where isclosed=false and createddate = TODAY';
        return Database.getQueryLocator(query);
    } 
    global void execute(Database.BatchableContext info, List<Case> scope){
        for(Case cs: scope){
            DateTime CurrentDateTime = DateTime.now();
            Decimal ageInMilliSeconds =  BusinessHours.diff(Label.Business_Hours_Id,cs.CreatedDate,CurrentDateTime); 
            Long CaseAgeMillis =  BusinessHours.diff(Label.Business_Hours_Id,cs.CreatedDate,CurrentDateTime); 
            system.debug('Agein miliseconds==>'+ageInMilliSeconds );  
            system.debug('@@CS ID: '+cs.Id);  
            system.debug('@@CS SLA: '+cs.SLA__r); 
            
            if(!cs.isClosed){             
                cs.GN_Case_Age__c = ageInMilliSeconds.divide((1000 * 60 * 60 * 8),3); 
                if(cs.GN_Case_Age__c > 1){
                    cs.GN_Case_Age_str__c = String.valueOf(ageInMilliSeconds.divide((1000 * 60 * 60 * 8),3)).substringBefore('.') + ' days';
                } else{
                    //cs.GN_Case_Age_str__c = String.valueOf(Math.floor(Math.MOD(CaseAgeMillis,(1000 * 60 * 60 * 8))/(1000 * 60 * 60))) + ' hours';
                    cs.GN_Case_Age_str__c = String.valueOf(Math.MOD(Integer.valueOf(ageInMilliSeconds),(1000 * 60 * 60 * 8))/(1000 * 60 * 60)) + ' hours';
                }
                
                if(!cs.SLA__r.isEmpty()){
                    
                    Decimal ageInMilliSecondsSLA;
                    if(cs.SLA__r[0].Until__c == null){
                        ageInMilliSecondsSLA = BusinessHours.diff(cs.SLA__r[0].Business_Hours_Id__c,cs.SLA__r[0].From__c,CurrentDateTime);
                    } else{
                        ageInMilliSecondsSLA = BusinessHours.diff(cs.SLA__r[0].Business_Hours_Id__c,cs.SLA__r[0].From__c,cs.SLA__r[0].Until__c); 
                    }
                    
                    Decimal ageinMilliSLA = ageInMilliSecondsSLA.divide((1000 * 60 * 60 * 8),3); 
                    cs.GN_Case_Age_SLA__c = ageinMilliSLA;
                    /*if(ageinMilliSLA > 1){
                        cs.GN_SLA_Case_Age__c = String.valueOf(ageInMilliSecondsSLA.divide((1000 * 60 * 60 * 8),3)).substringBefore('.') + ' days';
                        //cs.GN_SLA_Case_Age__c = String.valueOf(ageInMilliSecondsSLA);
                    } else{
                        cs.GN_SLA_Case_Age__c = String.valueOf(Math.MOD(Integer.valueOf(ageInMilliSecondsSLA),(1000 * 60 * 60 * 8))/(1000 * 60 * 60)) + ' hours';
                    }*/
                }
            }
        }
        update scope;  
    }
    global void finish(Database.BatchableContext info){     
    } 
}