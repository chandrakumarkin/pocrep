//test class name : ExemptedUBORegulations_Edit_Test 

public with sharing class ExemptedUBORegulations_Edit{

public Service_Request__c SRData{get;set;}

 public string RecordTypeId;
 public map<string,string> mapParameters;
 public UBO_Data__c ObjUBO{get;set;}
 public Attachment ObjAttachment{get;set;}
 public boolean SRisValid{get;set;}
 private Map<string,id> RecordTypeIdsUBO;
  
 public Account ObjAccount{get;set;}
     public ExemptedUBORegulations_Edit(ApexPages.StandardController controller)
    {
    
        RecordTypeIdsUBO=new Map<string,id>();
          for(RecordType objRT:[select Id,Name,DeveloperName from RecordType where  SobjectType='UBO_Data__c'])
          {
              RecordTypeIdsUBO.put(objRT.DeveloperName,objRT.id);
          }
          
            SRisValid=true;
            SRData=(Service_Request__c )controller.getRecord();
            ObjAccount=new account();
            ObjAttachment=new Attachment();  
            if(apexpages.currentPage().getParameters()!=null)
            mapParameters = apexpages.currentPage().getParameters();
            if(mapParameters.get('RecordType')!=null) 
            RecordTypeId= mapParameters.get('RecordType');
       
         ObjUBO=new UBO_Data__c ();
    
       for(User objUsr:[select id,ContactId,Email,Contact.Account.Exempted_from_UBO_Regulations__c,Phone,Contact.AccountId,Contact.Account.Company_Type__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  from User where Id=:userinfo.getUserId()])
            {
            
            if(objUsr.Contact.Account.Exempted_from_UBO_Regulations__c==true)
            {
            SRisValid=false;// company is execmpted company this SR not valid 
             ObjUBO.I_am_Exempt_entities__c='No';
            
            }
            else
             ObjUBO.I_am_Exempt_entities__c='Yes';
            
             if(SRData.id==null)
             {
             
             
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
               // SRData.I_am_Exempt_entities__c='Yes';
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                ObjAccount= objUsr.Contact.Account;
                SRData.Customer__r=ObjAccount;
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                ObjUBO.RecordTypeId=RecordTypeIdsUBO.get('Full_Exempted_UBO_Regulations');

                    
             }
             else
             {
             
                ObjUBO=[select id,Name_of_Regulator_Exchange_Govt__c ,I_am_Exempt_entities__c,Country__c,Reason_for_exemption__c,Select_Regulator_Exchange_Govt__c,Service_Request__c from
                UBO_Data__c where Service_Request__c=:SRData.id];
             }
             
               
               
    }
        
    }
   
public  PageReference RemoveExemptedSaveRecord()
{
    try
    {

        upsert SRData;
        ObjUBO.I_am_Exempt_entities__c='No';
        ObjUBO.Service_Request__c=SRData.id;
        upsert ObjUBO;
        PageReference acctPage = new ApexPages.StandardController(SRData).view();
        acctPage.setRedirect(true);
        return acctPage; 
    }

    catch (Exception e)
    {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
        ApexPages.addMessage(myMsg);

    }
    return null;

}   
 
public  PageReference SaveRecord()
{

try
{

    if((ObjAttachment==null || ObjAttachment.Name==null) && SRData.id==null )
    {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload Evidence.');
        ApexPages.addMessage(myMsg);
        
    }
    else
    {

    upsert SRData;
    ObjUBO.Service_Request__c=SRData.id;
    upsert ObjUBO;

    List<SR_Doc__c> ListofDocs=[select id from SR_Doc__c where Service_Request__c=:SRData.id];

    if(!ListofDocs.isEmpty())
    {
        SR_Doc__c ObjDoc=ListofDocs[0];
        ObjAttachment.Parentid=ObjDoc.id;
        insert ObjAttachment;
        ObjDoc.Status__c = 'Uploaded';
        update ObjDoc;

    }

    PageReference acctPage = new ApexPages.StandardController(SRData).view();
    acctPage.setRedirect(true);
    return acctPage; 
    }   

}

catch (Exception e)
{
    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
    ApexPages.addMessage(myMsg);

}
return null;
      

}


}