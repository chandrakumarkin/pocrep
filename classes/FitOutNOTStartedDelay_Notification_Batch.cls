global with sharing class FitOutNOTStartedDelay_Notification_Batch implements Database.Batchable < sObject > , Schedulable {
	global void execute(SchedulableContext ctx) {
        FitOutNOTStartedDelay_Notification_Batch bachable = new FitOutNOTStartedDelay_Notification_Batch();
        database.executeBatch(bachable);
    }
	
    String query = '';
    String unitsWithoutProjectsSOQL ='';
    global Database.QueryLocator start(Database.BatchableContext bc) {
    	FitOut_SOQL_Queries_MDT soqlMDT = new FitOut_SOQL_Queries_MDT();
    	query = soqlMDT.FitOut_OngoingProjectsDelayNotification;  
    	//unitsWithoutProjectsSOQL = soqlMDT.Units_With_out_Projects;
        //query = 'Select id,Progress__c ,service_type__c from service_request__c Limit 2';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Service_Request__c > scope) {
    	FitOut_SOQL_Queries_MDT soqlMDT = new FitOut_SOQL_Queries_MDT();
    	unitsWithoutProjectsSOQL = soqlMDT.Units_With_out_Projects;
        List<String> customersIds = new List<String>();
    	for(Service_request__c servRecDetails : (List<Service_request__c>) scope){
           customersIds.add('\'' + servRecDetails.Customer__c + '\'');
           //system.assertEquals(null,customersIds);
        }
        //Now fetch all the Rent Free units where client License issue date is lessthan a year 
        Map<id,Service_request__c> fitoutRequests = new Map<Id,Service_request__c>();
    	//string templatename ='FitOut_Project_Delay_Notification_After_Request_Raised';
    	unitsWithoutProjectsSOQL = unitsWithoutProjectsSOQL.replace('filters', ' AND Account__c NOT IN (' +String.join(customersIds,',')+')');
    	List<Lease_Partner__c> leasePartnerList = new List<Lease_Partner__c>();
        system.debug('unitsWithoutProjectsSOQL----------'+unitsWithoutProjectsSOQL);
        leasePartnerList = Database.query(unitsWithoutProjectsSOQL);
        //Now get the accounts where notifications needs to be sent:
        Set<id> sentNotificationsToCustomersIds = new Set<id>();
        Map<id,String> laseTypeAndCustomerMap = new Map<id,String>();
        for(Lease_Partner__c leasePartnerRec : leasePartnerList){
        	sentNotificationsToCustomersIds.add(leasePartnerRec.Account__c);
        	laseTypeAndCustomerMap.put(leasePartnerRec.Account__c,leasePartnerRec.Unit__r.Unit_Type_DDP__c);
        }
        if(sentNotificationsToCustomersIds.size() > 0){
        	List<Messaging.SingleEmailMessage> messageList = new List<Messaging.SingleEmailMessage>();
        	string templatename ='FitOut_Project_Delay_Notification_No_Request_Raised';
        	String leasingNotificationTemp ='FitOut_No_Customer_Reply_Notification_Leasing';
	        EmailTemplate emailTemp=[Select Id, Name, Subject, Body,HTMLValue from EmailTemplate where developerName =:templatename limit 1];
	        EmailTemplate emailTempLeaseing=[Select Id, Name, Subject, Body,HTMLValue from EmailTemplate where developerName =:leasingNotificationTemp limit 1];
	        List<String> sendingTo = new List<String>();
	        List<String> sendingToCC = new List<String>();
	        List<String> sendingToBCC = new List<String>();
        	//Now get the one contact per account
	        Map<id,Account> accountMap = new Map<Id,Account>([Select id,name,FitOut_Notification_Sent_On__c,(Select id,name,email from contacts where Role__c Like '%Company Services%' order by createddate desc limit 1) from Account where id in :sentNotificationsToCustomersIds]);
	        for(Account accRec : accountMap.values()){
	        	Integer delayedDays = 0;
	        	if(accRec.FitOut_Notification_Sent_On__c != Null){ delayedDays = accRec.FitOut_Notification_Sent_On__c.daysBetween(system.today());accountMap.remove(accRec.id);}
	        	else{accRec.FitOut_Notification_Sent_On__c = system.today(); accountMap.put(accRec.id,accRec);
	        		if(laseTypeAndCustomerMap.get(accRec.id) != Null && laseTypeAndCustomerMap.get(accRec.id).contains('commercial')){sendingToCC = system.label.ccFitOutDelayNotificationList_Office.split(';'); }else{sendingToCC = system.label.ccFitOutDelayNotificationList.split(';');}
		        	for(Contact con : accRec.contacts){
		        		if(delayedDays == 0 ||delayedDays == 7 ||delayedDays == 14 ||delayedDays == 21){
		        			if(delayedDays == 21){
		        				sendingTo.addAll(system.label.ccFitOutDelayNotificationList.split(';'));
				        		Messaging.SingleEmailMessage semail = createEmailMessage(sendingTo,sendingToCC,sendingToBCC,emailTempLeaseing,accRec.Name,delayedDays);
				        		messageList.add(semail);
				        		system.debug('Leasing email ---'+semail);
		        			}else{
			        			sendingTo.add(con.email);
				        		Messaging.SingleEmailMessage semail = createEmailMessage(sendingTo,sendingToCC,sendingToBCC,emailTemp,accRec.Name,delayedDays);
				        		messageList.add(semail);
				        		system.debug('Client Email ---'+semail);
		        			}
		        		}
		        	}
	        	}
	        }
	        if(messageList.size()> 0)Messaging.sendEmail(messageList);
	        if(accountMap.size() > 0)update accountMap.values();
        }
    }
    global void finish(Database.BatchableContext BC) {
		
    }
    public Messaging.SingleEmailMessage createEmailMessage(List<String> toaddresses,List<String> toCCaddresses,List<String> toBCCaddresses,EmailTemplate emailTemp,String clientName,Integer delayedDays) {
		Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();system.debug('toaddresses----------'+toaddresses);semail.setToAddresses(toaddresses);semail.setCcAddresses(toCCaddresses);semail.templateid = emailTemp.id;emailTemp.Subject = emailTemp.Subject.replace('{!clientName}',clientName);semail.setSubject(emailTemp.Subject);emailTemp.Body = emailTemp.Body.replace('{!clientName}',clientName);semail.setPlainTextBody(emailTemp.Body); return semail;
	}
}