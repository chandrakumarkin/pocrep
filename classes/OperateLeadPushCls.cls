/*
        Author      :   Shabbir
        Description :   This class is used to pass the lead data to Operate system
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    30-06-2018  Shabbir         Created
*/    
public without sharing class OperateLeadPushCls {
    
    /**
        * Creates a lead in the operate system once condition met in SF
        * account records
        * @params      ids                 List of Lead Ids
    */
    @InvocableMethod(label='Create Lead in Operate' description='Creates a lead in the operate system once condition met in SF')
    public static void createLeadInOperate(list<Id> listLeadIds){
        OperateIntegrationCls.pushSFLeadtoOperate(listLeadIds);
    }   
    
 }