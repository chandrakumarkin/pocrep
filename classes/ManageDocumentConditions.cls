public with sharing class ManageDocumentConditions {
    
    public List<conditionWrapper> ConditionWrapperLst {get;set;}
    string PRId = Apexpages.currentPage().getParameters().get('id');
    list<Condition__c> BRConditionLst = new list<Condition__c>();
    list<Condition__c> conditionLst = new list<Condition__c>();
    conditionWrapper CW;
    public Integer selectedRowIndex{get;set;}
    public String BRCondition{get;set;}
    map<string,list<string>> mapObjectFields;
    list<SelectOption> lstCSObjects;
    map<string,list<SelectOption>> mapPicklistFlds = new map<string,list<SelectOption>>();
    map<string,string> mapFldType = new map<string,string>();
    public string FilterCondition{get;set;}
    map<string,list<selectoption>> mapClassMethods;
    public integer iSelectedRow{get;set;}
    public ManageDocumentConditions(){
        mapClassMethods = new map<string,list<selectoption>>();
        mapObjectFields = new map<string,list<string>>();
        lstCSObjects = new list<SelectOption>();
        FilterCondition='';
        map<string,BR_Objects__c> mapBRCS = new map<string,BR_Objects__c>();
        if(BR_Objects__c.getAll()!=null){
            mapBRCS = BR_Objects__c.getAll();
        }
        system.debug('mapBRCS==>'+mapBRCS);
        lstCSObjects.add(new SelectOption('','--None--'));
        if(mapBRCS!=null && mapBRCS.size()>0){
            list<string> listmethods = new list<string>();
            for(BR_Objects__c objCS:mapBRCS.values()){
                if(!objCS.Class_Method__c && objCS.sObject_Name__c!=null){
                    lstCSObjects.add(new SelectOption(objCS.sObject_Name__c,objCS.Name));
                    if(getsObjectField(objCS.sObject_Name__c)!=null && getsObjectField(objCS.sObject_Name__c).size()>0){
                        mapObjectFields.put(objCS.sObject_Name__c,getsObjectField(objCS.sObject_Name__c));
                    }
                }else{
                    if(objCS.Class_Name__c!=null && objCS.Method_Name__c!=null){
                        list<selectoption> lstClsMthds;
                        if(mapClassMethods.get(objCS.Class_Name__c)!=null){
                            lstClsMthds = mapClassMethods.get(objCS.Class_Name__c);
                            lstClsMthds.add(new selectoption(objCS.Method_Name__c,objCS.Method_Name__c));
                            mapClassMethods.put(objCS.Class_Name__c,lstClsMthds);
                        }else{
                            lstClsMthds = new list<selectoption>();
                            lstClsMthds.add(new selectoption('','--None--'));
                            lstClsMthds.add(new selectoption(objCS.Method_Name__c,objCS.Method_Name__c));
                            mapClassMethods.put(objCS.Class_Name__c,lstClsMthds);
                        }
                        listmethods.add(objCS.Method_Name__c);
                    }
                }
            }
            //mapObjectFields.put('cls_executeAction',listmethods);
        }
        lstCSObjects.add(new SelectOption('Custom Code','Custom Code'));
        system.debug('mapObjectFields==>'+mapObjectFields);
        ConditionWrapperLst = new list<conditionWrapper>();
        BRCondition='';
        getConditions();
    }
    public void getConditions(){
        BRConditionLst = [select Id,Name,S_No__c,Field_Type__c,Field_Name__c,Object_Name__c,Class_Name__c,Open_Bracket__c,Conditional_Operator__c,Close_Bracket__c,SR_Template_Docs__c,SR_Template_Docs__r.Conditions__c,Operator__c,value__c from Condition__c where SR_Template_Docs__c=:PRId order by S_No__c];
        if(BRConditionLst.isEmpty()){
            Condition__c objCondition = new Condition__c();
            CW = new conditionWrapper(objCondition,ConditionWrapperLst.size());
            CW.lstObjects = lstCSObjects;
            CW.lstFields = new list<SelectOption>();
            CW.lstFields.add(new SelectOption('','--None--'));
            //CW.lstClasses = new list<selectoption>();
            for(string strClass:mapClassMethods.keyset()){
                CW.lstClasses.add(new selectoption(strClass,strClass));
            }
            CW.rowIndex = ConditionWrapperLst.size();
            ConditionWrapperLst.add(CW);
        }else{
            for(Condition__c objCondition : BRConditionLst){
                FilterCondition = objCondition.SR_Template_Docs__r.Conditions__c;
                CW = new conditionWrapper(objCondition,ConditionWrapperLst.size());
                CW.lstObjects = lstCSObjects;
                //CW.lstClasses = new list<selectoption>();
                for(string strClass:mapClassMethods.keyset()){
                    CW.lstClasses.add(new selectoption(strClass,strClass));
                }
                list<SelectOption> lstFldOptns = new list<SelectOption>();
                lstFldOptns.add(new SelectOption('','--None--'));
                if(objCondition.Object_Name__c!=null && objCondition.Object_Name__c!='Custom Code' && mapObjectFields.get(objCondition.Object_Name__c)!=null){
                    for(String strFld:mapObjectFields.get(objCondition.Object_Name__c)){
                        lstFldOptns.add(new SelectOption(strFld,strFld));
                    }
                }
                if(objCondition.Object_Name__c=='Custom Code' && objCondition.Class_Name__c!=null){
                    if(mapClassMethods.get(objCondition.Class_Name__c)!=null)
                        lstFldOptns = mapClassMethods.get(objCondition.Class_Name__c);
                }
                CW.lstPickList = new list<SelectOption>();
                if(objCondition.Field_Type__c!=null && objCondition.Field_Type__c.toLowerCase()=='picklist' && objCondition.Object_Name__c!=null && objCondition.Field_Name__c!=null){
                    if(getPicklistValues(objCondition.Object_Name__c,objCondition.Field_Name__c)!=null && getPicklistValues(objCondition.Object_Name__c,objCondition.Field_Name__c).size()>0)
                        CW.lstPickList = getPicklistValues(objCondition.Object_Name__c,objCondition.Field_Name__c);
                    //if(mapPicklistFlds.get(objCondition.Object_Name__c+'.'+objCondition.Field_Name__c)!=null && mapPicklistFlds.get(objCondition.Object_Name__c+'.'+objCondition.Field_Name__c).size()>0)
                        //CW.lstPickList = mapPicklistFlds.get(objCondition.Object_Name__c+'.'+objCondition.Field_Name__c);
                }
                /*
                if(mapPicklistFlds.get(objCondition.Object_Name__c+'.'+objCondition.Field_Name__c)!=null && mapPicklistFlds.get(objCondition.Object_Name__c+'.'+objCondition.Field_Name__c).size()>0)
                    CW.lstPickList = mapPicklistFlds.get(objCondition.Object_Name__c+'.'+objCondition.Field_Name__c);
                */
                if(mapFldType!=null && mapFldType.get(objCondition.Object_Name__c+'.'+objCondition.Field_Name__c)!=null)
                    CW.FieldType = mapFldType.get(objCondition.Object_Name__c+'.'+objCondition.Field_Name__c);
                CW.lstFields = lstFldOptns;
                CW.rowIndex = ConditionWrapperLst.size();
                ConditionWrapperLst.add(CW);
                //BRCondition=objCondition.BRCondition__c;
            }
        }
    }
    public void ChangesObjectField(){
        system.debug('selectedRowIndex=>'+selectedRowIndex);
        if(selectedRowIndex!=null && ConditionWrapperLst!=null && ConditionWrapperLst.size()>selectedRowIndex){
            system.debug('object=>'+ConditionWrapperLst[selectedRowIndex].objCondition.Object_Name__c);
            system.debug('Field=>'+ConditionWrapperLst[selectedRowIndex].objCondition.Field_Name__c);
            ConditionWrapperLst[selectedRowIndex].objCondition.Value__c='';
            ConditionWrapperLst[selectedRowIndex].lstPickList = new list<selectoption>();
            if(ConditionWrapperLst[selectedRowIndex].objCondition.Object_Name__c=='Custom Code'){
                ConditionWrapperLst[selectedRowIndex].FieldType = 'TEXT';
            }
            if(mapFldType.get(ConditionWrapperLst[selectedRowIndex].objCondition.Object_Name__c+'.'+ConditionWrapperLst[selectedRowIndex].objCondition.Field_Name__c)!=null)
                ConditionWrapperLst[selectedRowIndex].FieldType = mapFldType.get(ConditionWrapperLst[selectedRowIndex].objCondition.Object_Name__c+'.'+ConditionWrapperLst[selectedRowIndex].objCondition.Field_Name__c);
            if(ConditionWrapperLst[selectedRowIndex].FieldType=='PICKLIST'){
                /*if(mapPicklistFlds.get(ConditionWrapperLst[selectedRowIndex].objCondition.Object_Name__c+'.'+ConditionWrapperLst[selectedRowIndex].objCondition.Field_Name__c)!=null)
                    ConditionWrapperLst[selectedRowIndex].lstPickList = mapPicklistFlds.get(ConditionWrapperLst[selectedRowIndex].objCondition.Object_Name__c+'.'+ConditionWrapperLst[selectedRowIndex].objCondition.Field_Name__c);
                    */
                if(ConditionWrapperLst[selectedRowIndex].objCondition.Object_Name__c!=null && ConditionWrapperLst[selectedRowIndex].objCondition.Field_Name__c!=null && 
                getPicklistValues(ConditionWrapperLst[selectedRowIndex].objCondition.Object_Name__c,ConditionWrapperLst[selectedRowIndex].objCondition.Field_Name__c)!=null && 
                getPicklistValues(ConditionWrapperLst[selectedRowIndex].objCondition.Object_Name__c,ConditionWrapperLst[selectedRowIndex].objCondition.Field_Name__c).size()>0){
                    system.debug('***** In Condition ******');
                    ConditionWrapperLst[selectedRowIndex].lstPickList = getPicklistValues(ConditionWrapperLst[selectedRowIndex].objCondition.Object_Name__c,ConditionWrapperLst[selectedRowIndex].objCondition.Field_Name__c);
                }
            }
        }
    }
    public PageReference saveConditions(){
        conditionLst = new list<Condition__c>();
        string strFilterCondition = '';
        if(FilterCondition!=null && FilterCondition!='' && FilterCondition.trim()!=''){
            string trimmedFilterCond = FilterCondition.replaceAll('\\s+','');
            trimmedFilterCond = trimmedFilterCond.toUpperCase();
            string actOpen = '(';
            string actClose = ')';
            string open = '#(#';
            string closed = '#)#';
            if(trimmedFilterCond.indexOf(actOpen)>-1)
                trimmedFilterCond = trimmedFilterCond.replace(actOpen,open);
            if(trimmedFilterCond.indexOf(actClose)>-1)
                trimmedFilterCond = trimmedFilterCond.replace(actClose,closed);
            if(trimmedFilterCond.indexOf('AND')>-1)
                trimmedFilterCond = trimmedFilterCond.replaceAll('AND','#'+'AND'+'#');
            if(trimmedFilterCond.indexOf('OR')>-1)
                trimmedFilterCond = trimmedFilterCond.replaceAll('OR','#'+'OR'+'#');
            
            Condition__c objCondition;
            for(conditionWrapper objWrap : ConditionWrapperLst){
                objCondition=objWrap.objCondition;
                objCondition.SR_Template_Docs__c = PRId;
                integer Row = objWrap.rowIndex+1;
                objCondition.S_No__c = objWrap.rowIndex+1;
                objCondition.Field_Type__c = objWrap.FieldType;
                string strrow = Row+'';
                string fldcon;
                if(objCondition.Object_Name__c!='Custom Code'){
                    fldcon = objWrap.objCondition.Object_Name__c+'->'+objWrap.objCondition.Field_Name__c+'#'+objWrap.objCondition.Operator__c+'#'+objWrap.objCondition.Value__c;
                }else{
                    fldcon = objWrap.objCondition.Class_Name__c+'->'+objWrap.objCondition.Field_Name__c+'#'+objWrap.objCondition.Operator__c+'#'+objWrap.objCondition.Value__c;
                }
                if(trimmedFilterCond!=null && trimmedFilterCond!='' && trimmedFilterCond.indexOf(strrow)>-1){
                    trimmedFilterCond = trimmedFilterCond.replaceAll(strrow,fldcon);
                }
                conditionLst.add(objCondition);
            }
            strFilterCondition = trimmedFilterCond;
        }else{
            Condition__c objCondition;
            FilterCondition = '';
            strFilterCondition = '';
            for(conditionWrapper objWrap : ConditionWrapperLst){
                objCondition=objWrap.objCondition;
                objCondition.Open_Bracket__c = '';
                objCondition.Close_Bracket__c = '';
                objCondition.Field_Type__c = objWrap.FieldType;
                objCondition.SR_Template_Docs__c = PRId;
                objCondition.S_No__c = objWrap.rowIndex+1;
                if(objWrap.rowIndex!=0){
                    FilterCondition = FilterCondition+' AND '+string.valueOf(objWrap.rowIndex+1);
                    objCondition.Conditional_Operator__c = 'AND';
                    strFilterCondition += '#'+objCondition.Conditional_Operator__c+'#';
                    if(objCondition.Object_Name__c!='Custom Code'){
                        strFilterCondition += objWrap.objCondition.Object_Name__c+'->'+objWrap.objCondition.Field_Name__c;
                    }else{
                        strFilterCondition += objWrap.objCondition.Class_Name__c+'->'+objWrap.objCondition.Field_Name__c;
                    }
                    strFilterCondition += '#'+objWrap.objCondition.Operator__c+'#'+objWrap.objCondition.Value__c;
                    conditionLst.add(objCondition);
                }else{
                     FilterCondition = string.valueOf(objWrap.rowIndex+1);
                     if(objCondition.Object_Name__c!='Custom Code'){
                        strFilterCondition = objWrap.objCondition.Object_Name__c+'->'+objWrap.objCondition.Field_Name__c+'#'+objWrap.objCondition.Operator__c+'#'+objWrap.objCondition.Value__c;
                     }else{
                        strFilterCondition = objWrap.objCondition.Class_Name__c+'->'+objWrap.objCondition.Field_Name__c+'#'+objWrap.objCondition.Operator__c+'#'+objWrap.objCondition.Value__c;
                     }
                     conditionLst.add(objCondition);
                }
            }
        }
        list<string> lstVals = new list<string>();
        lstVals = strFilterCondition.split('#');
        system.debug('lstVals==>'+lstVals);
        system.debug('lstVals size==>'+lstVals.size());
        PageReference returnPage = new PageReference('/' + PRId);
        returnPage.setRedirect(true);
        if(conditionLst!=null && conditionLst.size()>0){
            try{
                upsert conditionLst;
                if(conditionLst.size()==1){
                    FilterCondition = '';
                }
                SR_Template_Docs__c updateBR = new SR_Template_Docs__c(id=PRId,SR_Template_Docs_Condition__c=strFilterCondition,Conditions__c=FilterCondition);
                update updateBR;
                return returnPage;
            }catch(Exception e){
                return null;
            }
        }else{
          if(PRId!=null && PRId!=''){
            SR_Template_Docs__c updateBR = new SR_Template_Docs__c(id=PRId,SR_Template_Docs_Condition__c=null,Conditions__c=null);
              update updateBR;
          }
            return returnPage;
        }
        return null;
    }
    
    public PageReference addRow() {
        String condition='';
        boolean bErrorFlag=false;
        if(ConditionWrapperLst!=null && ConditionWrapperLst.size()>0){
            for(conditionWrapper CWrap:ConditionWrapperLst){
                if(CWrap.objCondition.Object_Name__c==null || CWrap.objCondition.Object_Name__c==''){
                    bErrorFlag = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You must select an Object.'));
                    return null;
                }
                if(CWrap.objCondition.Field_Name__c==null || CWrap.objCondition.Field_Name__c==''){
                    bErrorFlag = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You must select an Field.'));
                    return null;
                }
                if(CWrap.objCondition.Operator__c==null || CWrap.objCondition.Operator__c==''){
                    bErrorFlag = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You must select an Operator.'));
                    return null;
                }
                if(CWrap.objCondition.Object_Name__c=='cls_executeAction' && (CWrap.objCondition.Operator__c!='=' && CWrap.objCondition.Operator__c!='!=')){
                    bErrorFlag = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You must select either "=" or "!=" operator for "Custom Code"'));
                    return null;
                }
                
                if(CWrap.objCondition.Object_Name__c=='cls_executeAction' && (CWrap.objCondition.Value__c==null || CWrap.objCondition.Value__c=='')){
                    bErrorFlag = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You must Enter an Value.'));
                    return null;
                }
                if(CWrap.objCondition.Object_Name__c=='cls_executeAction' && (CWrap.objCondition.Value__c!=null && CWrap.objCondition.Value__c!='' && !CWrap.objCondition.Value__c.equalsIgnoreCase('true') && !CWrap.objCondition.Value__c.equalsIgnoreCase('false'))){
                    bErrorFlag = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Value must be either True or False'));
                    return null;
                }
                if((CWrap.objCondition.Operator__c!='=' && CWrap.objCondition.Operator__c!='!=')&& (CWrap.objCondition.Value__c==null || (CWrap.objCondition.Value__c!=null && CWrap.objCondition.Value__c.trim()==''))){
                    bErrorFlag = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You must Enter an Value.'));
                    return null;
                }
            }
        }
        if(!bErrorFlag){
            Condition__c objCondition = new Condition__c();
            /*
            for(conditionWrapper CW : ConditionWrapperLst){
                if(CW.objCondition.Conditional_Operator__c!=NULL && CW.objCondition.Conditional_Operator__c!=''){
                    condition+=CW.objCondition.Conditional_Operator__c+' ';
                }
                if(CW.objCondition.Open_Bracket__c!=NULL && CW.objCondition.Open_Bracket__c!=''){
                    condition+=CW.objCondition.Open_Bracket__c+' ';
                }
                if(CW.objCondition.Object_Name__c!=NULL && CW.objCondition.Object_Name__c!='' && CW.objCondition.Field_Name__c!=NULL && CW.objCondition.Field_Name__c!=''){
                    condition+=CW.objCondition.Object_Name__c+'.'+CW.objCondition.Field_Name__c+' ';
                }
                if(CW.objCondition.Operator__c!=NULL && CW.objCondition.Operator__c!=''){
                    condition+=CW.objCondition.Operator__c+' ';
                }
                if(CW.objCondition.value__c!=NULL && CW.objCondition.value__c!=''){
                    condition+=CW.objCondition.value__c+' ';
                }
                if(CW.objCondition.Close_Bracket__c!=NULL && CW.objCondition.Close_Bracket__c!=''){
                    condition+=CW.objCondition.Close_Bracket__c+' ';
                }       
            }
            */
            
            CW = new conditionWrapper(objCondition,ConditionWrapperLst.size());
            //CW.lstClasses = new list<selectoption>();
            for(string strClass:mapClassMethods.keyset()){
                CW.lstClasses.add(new selectoption(strClass,strClass));
            }
            CW.lstObjects = lstCSObjects;
            CW.lstFields = new list<SelectOption>();
            CW.lstFields.add(new SelectOption('','--None--'));
            CW.rowIndex = ConditionWrapperLst.size();
            CW.lstPickList = new list<SelectOption>();
            ConditionWrapperLst.add(CW);
            BRCondition=condition;
        }
        return null;
    }
    
    public PageReference cancelCondition() {
        PageReference returnPage = new PageReference('/' + PRId);
        returnPage.setRedirect(true);
        return returnPage;
    }
    
    public PageReference DeleteCondition() {
        Condition__c DelCondition = new Condition__c();
        BRCondition='';
        String condition='';
        system.debug('ConditionWrapperLst=>'+ConditionWrapperLst);
        system.debug('selectedRowIndex=>'+selectedRowIndex);
        
        if(selectedRowIndex!=null && selectedRowIndex<ConditionWrapperLst.size()){
          if(ConditionWrapperLst[selectedRowIndex].objCondition!=null && ConditionWrapperLst[selectedRowIndex].objCondition.Id!=null){
            DelCondition = ConditionWrapperLst[selectedRowIndex].objCondition;
            try{
               delete DelCondition;
            }catch(Exception e){
            
            }
          }
          ConditionWrapperLst.remove(selectedRowIndex);
        }
        
        if(ConditionWrapperLst!=null && ConditionWrapperLst.size()>0){
          for(conditionWrapper CWrap:ConditionWrapperLst){
              if(CWrap.rowIndex>selectedRowIndex){
                  CWrap.rowIndex = CWrap.rowIndex-1;
              }
              if(CWrap.conditionNo>selectedRowIndex){
                  CWrap.conditionNo = CWrap.conditionNo-1;
              }
              if(CWrap.objCondition.Conditional_Operator__c!=NULL && CWrap.objCondition.Conditional_Operator__c!=''){
                  condition+=CWrap.objCondition.Conditional_Operator__c+' ';
              }
              if(CWrap.objCondition.Open_Bracket__c!=NULL && CWrap.objCondition.Open_Bracket__c!=''){
                  condition+=CWrap.objCondition.Open_Bracket__c+' ';
              }
              if(CWrap.objCondition.Object_Name__c!=NULL && CWrap.objCondition.Object_Name__c!='' && CWrap.objCondition.Field_Name__c!=NULL && CWrap.objCondition.Field_Name__c!=''){
                  condition+=CWrap.objCondition.Object_Name__c+'.'+CWrap.objCondition.Field_Name__c+' ';
              }
              if(CWrap.objCondition.Operator__c!=NULL && CWrap.objCondition.Operator__c!=''){
                  condition+=CWrap.objCondition.Operator__c+' ';
              }
              if(CWrap.objCondition.value__c!=NULL && CWrap.objCondition.value__c!=''){
                  condition+=CWrap.objCondition.value__c+' ';
              }
              if(CWrap.objCondition.Close_Bracket__c!=NULL && CWrap.objCondition.Close_Bracket__c!=''){
                  condition+=CWrap.objCondition.Close_Bracket__c+' ';
              }       
          }
        }
        //updateBRCondition();
        return null;
    }
    
    public void updateBRCondition(){
        String codeCondition='';
        String condition='';
        list<Condition__c> updateBRConditionLst = new list<Condition__c>();
        updateBRConditionLst = [select Id,Name,Field_Type__c,Field_Name__c,Object_Name__c,Open_Bracket__c,Conditional_Operator__c,Close_Bracket__c,SR_Template_Docs__c,Operator__c,value__c from Condition__c where SR_Template_Docs__c=:PRId order by Id];
        for(Condition__c objCon : updateBRConditionLst){
            
            if(objCon.Conditional_Operator__c!=NULL && objCon.Conditional_Operator__c!=''){
                codeCondition+=objCon.Conditional_Operator__c+'#';
                condition+=objCon.Conditional_Operator__c+' ';
            }
            if(objCon.Open_Bracket__c!=NULL && objCon.Open_Bracket__c!=''){
                codeCondition+=objCon.Open_Bracket__c+'#';
                condition+=objCon.Open_Bracket__c+' ';
            }
            if(objCon.Object_Name__c!=NULL && objCon.Object_Name__c!='' && objCon.Field_Name__c!=NULL && objCon.Field_Name__c!=''){
                codeCondition+=objCon.Object_Name__c+'.'+objCon.Field_Name__c+'#';
                condition+=objCon.Object_Name__c+'.'+objCon.Field_Name__c+' ';
            }
            if(objCon.Operator__c!=NULL && objCon.Operator__c!=''){
                codeCondition+=objCon.Operator__c+'#';
                condition+=objCon.Operator__c+' ';
            }
            if(objCon.value__c!=NULL && objCon.value__c!=''){
                codeCondition+=objCon.value__c+'#';
                condition+=objCon.value__c+' ';
            }
            if(objCon.Close_Bracket__c!=NULL && objCon.Close_Bracket__c!=''){
                codeCondition+=objCon.Close_Bracket__c+'#';
                condition+=objCon.Close_Bracket__c+' ';
            }       
        }
        if(codeCondition!=null && codeCondition.length()>3){
            if(codeCondition.substring(0,2)=='OR'){
                codeCondition = codeCondition.substring(2, codeCondition.length());
            }else if(codeCondition.substring(0,3)=='AND'){
                codeCondition = codeCondition.substring(3, codeCondition.length());
            }
        }
        if(condition!=null && condition.length()>3){
            if(condition.substring(0,2)=='OR'){
                condition = condition.substring(2, condition.length()-1);
            }else if(condition.substring(0,3)=='AND'){
                condition = condition.substring(3, condition.length()-1);
            }
        }
        BRCondition=condition;
        SR_Template_Docs__c updateBR = new SR_Template_Docs__c(id=PRId,SR_Template_Docs_Condition__c=codeCondition,Conditions__c=condition);
        Update updateBR;
    }
    public list<string> getsObjectField(string sObjName){
        system.debug('sObjName=======>'+sObjName);
        map<String, Schema.SObjectType>  m = Schema.getGlobalDescribe();
        list<string> listFields = new list<string>();
        SObjectType  objtype;
        DescribeSObjectResult objDef1;
        map<String, SObjectField> fieldmap;
        if(m.get(sObjName)!= null)
            objtype = m.get(sObjName);
        if(objtype!= null)
           objDef1 =  objtype.getDescribe();
        if(objDef1!= null)
           fieldmap =  objDef1.fields.getmap();
        if(fieldmap!=null){
           for(string fld:fieldmap.keySet()){
                DescribeFieldResult selectedSRField = fieldmap.get(fld).getDescribe();
                if(!string.valueOf(selectedSRField.getName()).toLowerCase().startsWith('ora') && selectedSRField.isAccessible() && (selectedSRField.getType()+'' == 'STRING' || selectedSRField.getType()+'' == 'INTEGER' || selectedSRField.getType()+'' == 'DOUBLE' || selectedSRField.getType()+'' == 'PICKLIST' || selectedSRField.getType()+'' == 'MULTIPICKLIST' || selectedSRField.getType()+'' == 'BOOLEAN' || selectedSRField.getType()+'' == 'DATE')){
                    mapFldType.put(sObjName+'.'+selectedSRField.getName(),selectedSRField.getType()+'');
                    listFields.add(selectedSRField.getName());
                    if(selectedSRField.getType()+'' == 'PICKLIST' && mapPicklistFlds.get(sObjName+'.'+selectedSRField.getName())==null){
                        /*
                        list<selectoption> lstPickListFlds = new list<selectOption>();
                        lstPickListFlds.add(new selectoption('','--None--'));
                        List<Schema.PicklistEntry> ple = selectedSRField.getPicklistValues();
                        if(ple!=null && ple.size()>0){
                            for(Schema.PicklistEntry f : ple){
                                lstPickListFlds.add(new SelectOption(f.getValue(),f.getLabel()));
                            }    
                        }
                        mapPicklistFlds.put(sObjName+'.'+selectedSRField.getName(),lstPickListFlds);
                        */
                        /*
                        list<selectoption> lstPickListFlds = new list<selectOption>();
                        lstPickListFlds.add(new selectoption('','--None--'));
                        mapPicklistFlds.put(sObjName+'.'+selectedSRField.getName(),lstPickListFlds);
                        */
                        list<selectoption> lstPickListFlds = new list<selectOption>();
                        lstPickListFlds.add(new selectoption('','--None--'));
                        mapPicklistFlds.put(sObjName+'.'+selectedSRField.getName(),lstPickListFlds);
                    }
                }
           }
        }
        system.debug('sObjName=======>'+sObjName);
        system.debug('listFields=======>'+listFields);
        listFields.sort();
        return listFields;
    }
    public void getSelectedObjectFlds(){
        system.debug('iSelectedRow===>'+iSelectedRow);
        if(iSelectedRow!=null && ConditionWrapperLst!=null && ConditionWrapperLst.size()>iSelectedRow){
            list<SelectOption> lstFldOpts = new list<SelectOption>();
            lstFldOpts.add(new SelectOption('','--None--'));
            if(ConditionWrapperLst[iSelectedRow].objCondition.Object_Name__c!=null && ConditionWrapperLst[iSelectedRow].objCondition.Object_Name__c!='Custom Code' && mapObjectFields.get(ConditionWrapperLst[iSelectedRow].objCondition.Object_Name__c)!=null){
                ConditionWrapperLst[iSelectedRow].objCondition.Class_Name__c = '';
                for(String strFld:mapObjectFields.get(ConditionWrapperLst[iSelectedRow].objCondition.Object_Name__c)){
                    lstFldOpts.add(new SelectOption(strFld,strFld));
                }
            }
            if(ConditionWrapperLst[iSelectedRow].objCondition.Object_Name__c=='Custom Code' && ConditionWrapperLst[iSelectedRow].objCondition.Class_Name__c!=null && ConditionWrapperLst[iSelectedRow].objCondition.Class_Name__c!=''){
                if(mapClassMethods.get(ConditionWrapperLst[iSelectedRow].objCondition.Class_Name__c)!=null)
                    lstFldOpts = mapClassMethods.get(ConditionWrapperLst[iSelectedRow].objCondition.Class_Name__c);
            }
            ConditionWrapperLst[iSelectedRow].lstFields = lstFldOpts;
        }
    }
    public list<selectoption> getPicklistValues(String ObjectApi_name,String Field_name){ 
        system.debug('ObjectApi_name==>'+ObjectApi_name);
        system.debug('Field_name==>'+Field_name);
        List<selectoption> lstPickvals = new List<selectoption>();
        system.debug('mapPicklistFlds==>'+mapPicklistFlds);
        system.debug('mapPicklistFlds==>'+mapPicklistFlds.size());
        if(mapPicklistFlds!=null && mapPicklistFlds.get(ObjectApi_name+'.'+Field_name).size()==1){
            Schema.SObjectType targetType = Schema.getGlobalDescribe().get(ObjectApi_name);//From the Object Api name retrieving the SObject
            Sobject Object_name = targetType.newSObject();
            Schema.sObjectType sobject_type = Object_name.getSObjectType(); //grab the sobject that was passed
            Schema.DescribeSObjectResult sobject_describe = sobject_type.getDescribe(); //describe the sobject
            Map<String, Schema.SObjectField> field_map = sobject_describe.fields.getMap(); //get a map of fields for the passed sobject
            List<Schema.PicklistEntry> pick_list_values = field_map.get(Field_name).getDescribe().getPickListValues(); //grab the list of picklist values for the passed field on the sobject
            lstPickvals.add(new selectoption('','--None--'));
            for (Schema.PicklistEntry a : pick_list_values) { //for all values in the picklist list
                lstPickvals.add(new selectoption(a.getValue(),a.getValue()));//add the value  to our final list
            }
            mapPicklistFlds.put(ObjectApi_name+'.'+Field_name,lstPickvals);
        }else{
            if(mapPicklistFlds.get(ObjectApi_name+'.'+Field_name)!=null && mapPicklistFlds.get(ObjectApi_name+'.'+Field_name).size()>0){
                lstPickvals = mapPicklistFlds.get(ObjectApi_name+'.'+Field_name);
            }
        }
        system.debug('lstPickvals==>'+lstPickvals);
        return lstPickvals;
    }
    public class conditionWrapper{
        public Integer conditionNo {get;set;} 
        public Condition__c objCondition{get;set;} 
        public list<SelectOption> lstPickList{get;set;}
        public list<SelectOption> lstObjects{get;set;}
        public list<SelectOption> lstClasses{get;set;}
        public list<SelectOption> lstFields{get;set;}
        public string strObjectName{get;set;}
        public string strFieldName{get;set;}
        public integer rowIndex{get;set;}
        public string FieldType{get;set;}
        
        public conditionWrapper(Condition__c condition,Integer intCount){
            objCondition=condition;
            conditionNo=intCount;
            lstPickList = new list<SelectOption>();
            lstClasses = new list<SelectOption>();
            lstClasses.add(new selectoption('','--None--'));
        }
    }
}