/******************************************************************************************
 *  Test Class Name : Test class for ThompsonReutersValidation class 
 *  Date     : 01/May/2019
 *  Description : 
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.0   24/SEP/2019                     
*******************************************************************************************/
@isTest
private class TestThompsonReutersValidation{
    
    public static Account objAccount;
    public static SR_Template__c objTemplate;
    public static Contact objContact;
    public static Service_Request__c objSR;
    public static Service_Request__c objSR2;
     public static Service_Request__c objSR3;
    public static Amendment__c objAmd;
    public static Amendment__c objAmd2;
    public static TR_Sanctioned_Nationality__c trNationality;
    public static List<Step__c> objStep2;
    public static Dom.XMLNode address;
    public static TR_Contact__c trContact;
    public static void testData(){
        
        
        objAccount = new Account();
        objStep2 = new List<Step__c>();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        SR_Template__c objTemplate2 = new SR_Template__c();
        objTemplate2.Name = 'Change Shareholder Details';
        objTemplate2.SR_RecordType_API_Name__c = 'Change_Shareholder_Details';
        objTemplate2.Menutext__c = 'Change_Shareholder_Details';
        objTemplate2.Available_for_menu__c = true;
        objTemplate2.Template_Sequence_No__c = 124;
        objTemplate2.Menu__c = 'Company';
        objTemplate2.Active__c = true;
        insert objTemplate2;
        
        
        SR_Template__c objTemplate3 = new SR_Template__c();
        objTemplate3.Name = 'Update details, Add or Remove Ultimate Beneficial Owner';
        objTemplate3.SR_RecordType_API_Name__c = 'Change_Add_or_Remove_Beneficial_or_Ultimate_Owner';
        objTemplate3.Menutext__c = 'Update Details, Add or Remove Ultimate Beneficial Owner';
        objTemplate3.Available_for_menu__c = true;
        objTemplate3.Template_Sequence_No__c = 125;
        objTemplate3.Menu__c = 'Company';
        objTemplate3.Active__c = true;
        insert objTemplate3;
        
        objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        insert objContact;
        
        objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Email__c = 'testsr@difc.com';
       // objSR.Contact__c = objContact.Id;
        objSR.SR_Template__c = objTemplate.Id;
        insert objSR;
        
        objSR2 = new Service_Request__c();
        objSR2.Customer__c = objAccount.Id;
        objSR2.Email__c = 'testsr@difc.com';
       // objSR.Contact__c = objContact.Id;
        objSR2.SR_Template__c = objTemplate2.Id;
        insert objSR2;
        
        objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Email__c = 'testsr@difc.com';
       // objSR.Contact__c = objContact.Id;
        objSR3.SR_Template__c = objTemplate3.Id;
        insert objSR3;
        
        Lookup__c objlkp = new Lookup__c();
        objlkp.Name = 'Pakistan';
        objlkp.Type__c = 'Nationality';
        insert objlkp;
        
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.Amendment_Type__c = 'Individual';
        objAmd.PO_Box__c = '12345';
        objAmd.First_Name__c = 'Abduk';
        objAmd.Last_Name__c = 'Kader';
        objAmd.sys_create__c = false;
        objAmd.Status__c = 'New';
        objAmd.Nationality__c = objlkp.Id;
        objAmd.is_Updated__c=true;
        Insert objAmd;
        
        objAmd2 = new Amendment__c();
        objAmd2.ServiceRequest__c = objSR2.Id;
        objAmd2.Amendment_Type__c = 'Individual';
        objAmd2.Shareholder_Company__c = objAccount.id;
        objAmd2.PO_Box__c = '12345';
        objAmd2.First_Name__c = 'Abduk';
        objAmd2.Last_Name__c = 'Kader';
        objAmd2.sys_create__c = false;
        objAmd2.Status__c = 'New';
        objAmd2.Nationality__c = objlkp.Id;
        objAmd.is_Updated__c=true;
        Insert objAmd2;
        
        trNationality = new TR_Sanctioned_Nationality__c();
        trNationality.Name = 'India';
        INSERT trNationality;
        
        List<UBO_Data__c> uboList = new List<UBO_Data__c>();
        map<string,string> mapRecordType1 = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where SObjectType = 'UBO_Data__c' and DeveloperName IN ('Individual')]){
            mapRecordType1.put(objRT.DeveloperName,objRT.Id);
        }        
        UBO_Data__c uboData = new UBO_Data__c(
        Nationality_New__c='India',
        Date_of_Cessation__c= system.today(),
        Status__c='Draft',
        Passport_Number_New__c='+971543343234',
        Email_New__c='ss@gmail.com',
        Last_Name_New__c='MOhammed',
        First_Name_New__c = 'Ashik',
        RecordTypeid= mapRecordType1.get('Individual'),
        Type__c='Direct',
        Date_of_Birth__c=system.today().addyears(-20),
        Gender__c='Male',
        Passport_Expiry_Date__c=system.today().addyears(3),
        Passport_Number__c='FGHFJ3242',
        Email__c='ss@gmail.com',
        Place_of_Birth__c='Chennai',
        Title__c='Mr.',
        Date_Of_Becoming_a_UBO__c=system.today(),
        First_Name__c='Poonkuzhali',
        Last_Name__c='Senthamil Selvi',
        Passport_Data_of_Issuance__c=system.today().addyears(-1),
        Entity_Name__c='ABC Corporation',
        Date_Of_Registration__c=system.today().addyears(-1),
        Name_of_Regulator_Exchange_Govt__c='UAE',
        Select_Regulator_Exchange_Govt__c='Dubai',
        Reason_for_exemption__c='Regulated by a recognised financial service provider other than DFSA',
        Entity_Type__c='The individual has the right to appoint or remove the majority of the Directors of the Company',
        Phone_Number__c='+971543343234',
        Apartment_or_Villa_Number__c='3223',
        Emirate_State_Province__c='Dubai',
        Nationality__c='India',
        Building_Name__c='Raga',
        Street_Address__c='business bay',
        City_Town__c='Dubai',
        Country__c='India',
        PO_Box__c='23432432',
        Post_Code__c='234243',
        Service_Request__c = objSR3.id
        );

        insert uboData;
        
        
        
        
    }
    static testMethod void myUnitTest(){
        
        testData();
        Test.startTest();
        
        status__c stepStatus = new Status__c();//Code__c='VERIFIED',Type__c='End',Name='Verified'
        stepStatus.Type__c='End';
        stepStatus.Name='VERIFIED';
        stepStatus.Code__c='VERIFIED';
        upsert stepStatus;
        
        status__c stepStatus2 = new Status__c();//Code__c='VERIFIED',Type__c='End',Name='Verified'
        stepStatus2.Type__c='Start';
        stepStatus2.Name='Approved';
        stepStatus2.Code__c='Approved';
        upsert stepStatus2;
        
        status__c stepStatus1 = new Status__c();//Code__c='VERIFIED',Type__c='End',Name='Verified'
        stepStatus1.Type__c='Start';
        stepStatus1.Name='Pending Review';
        stepStatus1.Code__c='Pending_Review';
        upsert stepStatus1;
        
        List<step_Template__c> stpTemp = new List<step_Template__c>();
        step_Template__c stpTemp1 = new Step_template__c(Name='Verification of Application',Step_RecordType_API_Name__c='Verification_of_Application',Code__c='VERIFICATION_OF_APPLICATION');
        insert stpTemp1;
        
        step_Template__c stpTemp2 = new Step_template__c(Name='AML_APPROVAL',Step_RecordType_API_Name__c='Verification_of_Application',Code__c='AML_APPROVAL');
        insert stpTemp2;
        
        objStep2 = new List<step__c>();
        step__c verfStep = new step__c();
        verfStep.sr__c = objSR.id;
        verfStep.Step_Template__c = stpTemp1.id;
        verfStep.status__c = stepStatus.id;
        objStep2.add(verfStep);
        
        step__c verfStep1 = new step__c();
        verfStep1.sr__c = objSR.id;
        verfStep1.Step_Template__c = stpTemp1.id;
        verfStep1.status__c = stepStatus1.id;
        objStep2.add(verfStep1);
        
        step__c verfStep2 = new step__c();
        verfStep2.sr__c = objSR2.id;
        verfStep2.Step_Template__c = stpTemp1.id;
        verfStep2.status__c = stepStatus1.id;
        
        objStep2.add(verfStep2);
        
        step__c verfStep3 = new step__c();
        verfStep3.sr__c = objSR2.id;
        verfStep3.Step_Template__c = stpTemp2.id;
        verfStep3.status__c = stepStatus1.id;
        
        objStep2.add(verfStep3);
        
        step__c verfStep4 = new step__c();
        verfStep4.sr__c = objSR.id;
        verfStep4.Step_Template__c = stpTemp2.id;
        verfStep4.status__c = stepStatus1.id;
        
        objStep2.add(verfStep4);
        
        insert objStep2;
    
        trContact = new TR_Contact__c();
        trContact.Given_Name__c = 'David';
        trContact.Gender__c = 'Male';
        trContact.Date_of_Birth__c = system.today()-36500;
        trContact.Company_Name__c = 'DIFC';
        trContact.Registration_Incorporation_Date__c  = system.today()-365;
        trContact.Nationality_list__c = 'India';
        trContact.Place_of_Registration__c = 'India';
        trContact.Amendment__c =objAmd.Id; 
        trContact.ServiceRequest__c = objSR.Id;
        INSERT trContact;
        
        Thompson_Reuters_Check__c TRCheckObject = new Thompson_Reuters_Check__c();
        TRCheckObject.TRCase_No__c = '202000003520';
        TRCheckObject.TR_Case_Status__c = 'Validation';
        TRCheckObject.TR_Check_Result__c = 'TR Check Successful: Match Found';
        TRCheckObject.Name__c = 'David';
        TRCheckObject.Score__c ='90';
        TRCheckObject.Step__c = objStep2[0].Id;
        TRCheckObject.ServiceRequest__c = objSR.Id;
        insert TRCheckObject;
        
        List<Id> trContId = new List<Id>();
        trContId.add(trContact.id);
        
        String body = '{"DIFC":"<?xml version="1.0" encoding="UTF-8" standalone="yes"?><sdResultsSets><resultSets><sdResultSet><caseCode>20200603093658975312</caseCode><caseNum>202000003520</caseNum><clientCode>TRC-24006</clientCode><clientFeedSourceSystem>-</clientFeedSourceSystem><indexDate>2020-06-02T17:12:31.823+04:00</indexDate><sdQuery><clientID>TRC-24006</clientID><clientType>CUSTOMER</clientType><createCaseOnMatches>CREATECASE</createCaseOnMatches><types><type>INDIVIDUAL</type></types><indDob>1981-01-28T00:00:00+04:00</indDob><name>George Davida</name><nationalityName>United Arab Emirates</nationalityName><ruleId>RULE151</ruleId></sdQuery><sdResults><sdResult><categories><category>ec_4</category></categories><entityId>e_tr_wci_3086840</entityId><modifiedDate>2020-02-28T04:00:00+04:00</modifiedDate><name>George DAVID</name><originalScript>George DAVID</originalScript><score>93.46293210983276</score><secondaryAddrInfo>Lagos, Lagos, NIGERIA</secondaryAddrInfo><secondaryCityInfo>Lagos</secondaryCityInfo><secondaryCountryInfo>NIGERIA</secondaryCountryInfo><secondaryDateOfBirthInfo>1937-06-10</secondaryDateOfBirthInfo><secondaryGenderInfo>MALE</secondaryGenderInfo><secondaryNationalityInfo>CY/CYPRUS</secondaryNationalityInfo><secondaryRuleInfo>Name Partial Match, Secondary: -6.0</secondaryRuleInfo><secondaryScore>-6.0</secondaryScore><sources><source>b_trwc_1</source></sources><totalScore>87.46293210983276</totalScore><type>INDIVIDUAL</type></sdResult></sdResults></sdResultSet></resultSets></sdResultsSets>"}';
        
        Test.setMock(HttpCalloutMock.class, new ThompsonReutersValidationMockTest(trContact.Name));
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint('https://trsd.difc.ae/transwatchwebapp/webresources/sdqueryservice/searches');
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setBody(body);
        ThompsonReutersValidationMockTest obj = new ThompsonReutersValidationMockTest('test');
        HttpResponse res = obj.respond(request);
        Dom.Document doc = res.getBodyDocument();
        DOM.XmlNode rootNode = doc.getRootElement();
        ThompsonReutersValidation.parsechildXMLResponse(rootNode,string.valueof(objSR.Id),string.valueof(objStep2[0].Id)); 
        ThompsonReutersValidation.parsechildXMLResponse(rootNode,string.valueof(objSR2.Id),string.valueof(objStep2[1].Id));
        ThompsonReutersValidation.parsechildXMLResponse(rootNode,string.valueof(objSR3.Id),string.valueof(objStep2[2].Id)); 
        ThompsonReutersValidation.CaseStatusResponseXML(rootNode,'202000003520');
        ThompsonReutersValidation.initiateThompsonReutersCheck(objSR.Id ,'Step',objStep2[0].Id); 
        ThompsonReutersValidation.initiateThompsonReutersCheck(objSR2.Id,'Step',objStep2[1].Id);
        ThompsonReutersValidation.initiateThompsonReutersCheck(objSR3.Id ,'Step',objStep2[2].Id);  
         
        Integer code= 200;
        String status= 'pass';
        Map<String, String> responseHeaders= new Map<String, String>();
        
        ThompsonReutersValidation.caseNumber='250000';
        ThompsonReutersValidation.TRCheckObject= TRCheckObject;
        ThompsonReutersValidation.checkTRCaseStatus('250000');  
        Test.StopTest();
    }
    static testMethod void myUnitTest2(){
        
        testData();
        Test.startTest();
        TRCaseStatusbatch obj = new TRCaseStatusbatch();
        DataBase.executeBatch(obj);
        Test.StopTest();
    }
    static testMethod void myUnitTest3(){
        
        testData();
        Test.startTest();
        
        status__c stepStatus = new Status__c();//Code__c='VERIFIED',Type__c='End',Name='Verified'
        stepStatus.Type__c='End';
        stepStatus.Name='VERIFIED';
        stepStatus.Code__c='VERIFIED';
        upsert stepStatus;
        
        status__c stepStatus1 = new Status__c();//Code__c='VERIFIED',Type__c='End',Name='Verified'
        stepStatus1.Type__c='Start';
        stepStatus1.Name='Pending Review';
        stepStatus1.Code__c='Pending_Review';
        upsert stepStatus1;
        
        status__c stepStatus2 = new Status__c();//Code__c='VERIFIED',Type__c='End',Name='Verified'
        stepStatus2.Type__c='Start';
        stepStatus2.Name='Approved';
        stepStatus2.Code__c='Approved';
        upsert stepStatus2;
        
        
        List<step_Template__c> stpTemp = new List<step_Template__c>();
        step_Template__c stpTemp1 = new Step_template__c(Name='Verification of Application',Step_RecordType_API_Name__c='Verification_of_Application',Code__c='VERIFICATION_OF_APPLICATION');
        insert stpTemp1;
        step_Template__c stpTemp2 = new Step_template__c(Name='AML_APPROVAL',Step_RecordType_API_Name__c='Verification_of_Application',Code__c='AML_APPROVAL');
        insert stpTemp2;
        
        objStep2 = new List<step__c>();
        step__c verfStep = new step__c();
        verfStep.sr__c = objSR.id;
        verfStep.Step_Template__c = stpTemp1.id;
        verfStep.status__c = stepStatus.id;
        objStep2.add(verfStep);
        
        step__c verfStep1 = new step__c();
        verfStep1.sr__c = objSR.id;
        verfStep1.Step_Template__c = stpTemp1.id;
        verfStep1.status__c = stepStatus1.id;
        objStep2.add(verfStep1);
        
        step__c verfStep2 = new step__c();
        verfStep2.sr__c = objSR2.id;
        verfStep2.Step_Template__c = stpTemp1.id;
        verfStep2.status__c = stepStatus1.id;
        
        objStep2.add(verfStep2);
        
        step__c verfStep3 = new step__c();
        verfStep3.sr__c = objSR2.id;
        verfStep3.Step_Template__c = stpTemp2.id;
        verfStep3.status__c = stepStatus1.id;
        
        objStep2.add(verfStep3);
        
        step__c verfStep4 = new step__c();
        verfStep4.sr__c = objSR.id;
        verfStep4.Step_Template__c = stpTemp2.id;
        verfStep4.status__c = stepStatus1.id;
        
        objStep2.add(verfStep4);
        
        insert objStep2;
        
        trContact = new TR_Contact__c();
        trContact.Given_Name__c = 'David';
        trContact.Gender__c = 'Male';
        trContact.Date_of_Birth__c = system.today()-36500;
        trContact.Company_Name__c = 'DIFC';
        trContact.Registration_Incorporation_Date__c  = system.today()-365;
        trContact.Nationality_list__c = 'India';
        trContact.Place_of_Registration__c = 'India';
        trContact.Amendment__c =objAmd.Id; 
        trContact.ServiceRequest__c = objSR.Id;
        INSERT trContact;
        
        Thompson_Reuters_Check__c TRCheckObject = new Thompson_Reuters_Check__c();
        TRCheckObject.TRCase_No__c = '250000';
        TRCheckObject.TR_Case_Status__c = 'Validation';
        TRCheckObject.TR_Check_Result__c = 'TR Check Successful: Match Found';
        TRCheckObject.Name__c = 'David';
        TRCheckObject.Score__c ='90';
        TRCheckObject.Step__c = objStep2[0].Id;
        TRCheckObject.ServiceRequest__c = objSR.Id;
        insert TRCheckObject;
        
        ThompsonReutersValidation trValidation = new ThompsonReutersValidation();
        ThompsonReutersValidation.initiateThompsonReutersCheck(objSR.Id,'Step',objStep2[0].Id);    
        ThompsonReutersValidation.checkTRCaseStatus('250000');  
        List<Thompson_Reuters_Check__c> trCaseClosedList = [select id,TRCase_No__c,TR_Case_Status__c from Thompson_Reuters_Check__c where ServiceRequest__c =:objSR.Id];
         for(Thompson_Reuters_Check__c itr:trCaseClosedList){
             itr.TR_Case_Status__c = 'CloseCase';
             itr.TR_case_comments__c='test';
         }
        update trCaseClosedList;
        ThompsonReutersValidation.checkexistingAmendmentList(new List<Amendment__c>{objAmd,objAmd2},new List<TR_Contact__c>{trContact});
        Test.StopTest();
    }
}