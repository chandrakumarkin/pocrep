/***************************************************************************************************************************
    Name        :   DIFCLandingController
    Author      :   Bilal
    Date        :   21-12-2014
    Description :   An apex page controller that takes the user to the right start page based on credentials or lack thereof
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    01-09-2016  Kaavya        Updated the controller to allow direct login to different page
    
*****************************************************************************************************************************/
 
public with sharing class DIFCLandingController {

    // Code we will invoke on page load.
    public PageReference forwardToCustomAuthPage() {
        
        PageReference landingPage = null;
        if(UserInfo.getUserType() == 'Guest'){
            /*return new PageReference( Label.Community_URL + '/signin');*/               
            String userAgent = System.currentPageReference().getHeaders().get('User-Agent');
            System.debug('userAgent: '+userAgent );      
            landingPage = new PageReference(Site.getBaseSecureUrl() + '/signin');
        }
        else{
            /*return new PageReference(Label.Community_URL + '/home'); */
            //landingPage = new PageReference(Site.getBaseSecureUrl() + Label.PortalLandingPage); /** V1.0 commenting out to put inside if**
            //V1.0 start
            
            String addUrl = System.currentPageReference().getParameters().get('add'); 
            if(addUrl==null)
                landingPage = new PageReference(Site.getBaseSecureUrl() + Label.PortalLandingPage); 
            else
                landingPage = new PageReference(Site.getBaseSecureUrl() + addUrl); 
            
            //V1.0 end
        }
        landingPage.setRedirect(true);
        return landingPage;        
    }

    public DIFCLandingController() {}
}