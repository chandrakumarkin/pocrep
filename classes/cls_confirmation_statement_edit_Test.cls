@isTest
public class cls_confirmation_statement_edit_Test {
    @testsetup
    static void Init(){
        Test.startTest();
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'acctest@testtesttest.com';
        objAccount.BP_No__c = '991234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.BD_Sector__c = 'Wealth Management (WM)';
        objAccount.OB_Sector_Classification__c = 'Wealth Management';
        objAccount.Subsector__c = 'Asset Management;Other';
        objAccount.Other_SubSector__c = 'TestOtherSubSecVal';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = 'Yes';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact1';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'testcon@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testcommuser@difcportal.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactId=objContact.Id, Community_User_Role__c='Company Services',
                                TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        
        Test.stopTest();
    }

    static testmethod void myTest1(){
        Test.startTest();   
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = 'Yes';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordTypeIds.get('License_Renewal');
        objSR.Service_Category__c = 'New';
        objSR.I_agree__c=true;
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        objSR.Financial_Year_End_mm_dd__c ='Test';
        objSR.Agreement_Date__c=date.today();
        insert objSR;
        
        Apexpages.currentPage().getParameters().put('RecordType',mapRecordTypeIds.get('Confirmation_Statement'));
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
        cls_confirmation_statement_edit confirmation = new cls_confirmation_statement_edit(con);
        
        confirmation.AnnualTurnover();
        confirmation.FEYValue(date.today());
        confirmation.FEYValue(date.today()+1);
        confirmation.FEYValue(date.today()+2);
        confirmation.FEYValue(date.today()+3);
        confirmation.FEYValue(date.today()+4);
        confirmation.FEYValue(date.today()+5);
        
        Test.stopTest();           
    }
    
    static testmethod void myTest2(){
        Test.startTest();
        Service_Request__c objSR = new Service_Request__c();
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
        cls_confirmation_statement_edit confirmation = new cls_confirmation_statement_edit(con);
        confirmation.AnnualTurnover();
        Test.stopTest();
    }
    
    static testmethod void myTest3(){
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = 'Yes';
        objAccount.Contact_Details_Provided__c = FALSE;
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordTypeIds.get('License_Renewal');
        objSR.Service_Category__c = 'New';
        objSR.I_agree__c=true;
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        objSR.Financial_Year_End_mm_dd__c ='Test';
        objSR.Change_Activity__c = TRUE;
        insert objSR;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactId=objContact.Id, Community_User_Role__c='Company Services',
                                TimeZoneSidKey='America/Los_Angeles', UserName='newuser1@testorg.com');
        insert objUser;
        
        system.runAs(objUser){
            Test.startTest();
            Apexpages.currentPage().getParameters().put('RecordType',mapRecordTypeIds.get('License_Renewal'));
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            
            cls_confirmation_statement_edit confirmation = new cls_confirmation_statement_edit(con);
            confirmation.isUBOExempted = true;
            confirmation.AnnualTurnover();
            confirmation.SaveConfirmation();
            Test.stopTest();
        }
    }
    
    
    static testmethod void myTest4(){
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = 'Yes';
        objAccount.Contact_Details_Provided__c = FALSE;
        objAccount.Exempted_from_UBO_Regulations__c =true;
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordTypeIds.get('License_Renewal');
        objSR.Service_Category__c = 'New';
        objSR.I_agree__c=true;
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        objSR.Financial_Year_End_mm_dd__c ='Test';
        objSR.Change_Activity__c = TRUE;
        objSR.Entity_Name_checkbox__c = true;
        objSR.Agreement_Date__c=Date.today()+21;
        objSR.Business_Activity__c = true;
        insert objSR;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser2@difcportal.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactId=objContact.Id, Community_User_Role__c='Company Services',
                                TimeZoneSidKey='America/Los_Angeles', UserName='newuser2@testorg.com');
        insert objUser;
        
        system.runAs(objUser){
            
            Test.startTest();
            Apexpages.currentPage().getParameters().put('RecordType',mapRecordTypeIds.get('License_Renewal'));
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            
            cls_confirmation_statement_edit confirmation = new cls_confirmation_statement_edit(con);
            confirmation.isUBOExempted = true;
            confirmation.AnnualTurnover();
            confirmation.SaveConfirmation();
            
            Test.stopTest();
        }
    }
    
    @isTest
    static void testNewMethod(){ 
        
        User comUsr = [SELECT Id,Name,ContactId,Contact.AccountId From User WHERE Email ='testcommuser@difcportal.com'];
        
        System.runAs(comUsr){
            
            Test.startTest();
            

            
            
            Service_Request__c objSR = new Service_Request__c();
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            cls_confirmation_statement_edit co = new cls_confirmation_statement_edit(con);
            co.rightvalues = new Set<String>{'Brokerage','Other'};
            co.gSelect();
            co.gDeselect();
            co.getDeselectedValues();
            co.rightvalues = new Set<String>{'Brokerage','Other'};
            co.getSelectedValues();//rightvalues
            co.SaveConfirmation();
            
            Test.stopTest();	   
        }
        
        
    }

    
    
    @isTest
    static void testNewMethod2(){ 
        
        User comUsr = [SELECT Id,Name,ContactId,Contact.AccountId From User WHERE Email ='testcommuser@difcportal.com'];
        
        System.runAs(comUsr){
            
            Test.startTest();
            
            
            map<string,string> mapRecordTypeIds = new map<string,string>();
            for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Confirmation_Statement','License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
                mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
            }
            
            Service_Request__c objSR = new Service_Request__c();
            objSR.Customer__c = comUsr.Contact.AccountId;
            objSR.Date_of_Declaration__c = Date.today();
            objSR.Email__c = 'test333@test333.com';
            objSR.RecordTypeId = mapRecordTypeIds.get('Confirmation_Statement');
            objSR.Service_Category__c = 'New';
            objSR.I_agree__c=true;
            objSR.Type_of_Lease__c = 'Lease';
            objSR.Rental_Amount__c = 1234;
            objSR.Financial_Year_End_mm_dd__c ='Test';
            objSR.Change_Activity__c = TRUE;
            objSR.Entity_Name_checkbox__c = true;
            objSR.Agreement_Date__c=Date.today()+21;
            objSR.Business_Activity__c = true;
            objSR.Declaration_Signatory_Name__c = 'Test';
            objSR.Declaration_Signatory_Capacity__c = 'Tdst2';
            objSR.Current_Registered_Building__c = 'TestSUbsectorNew';
            insert objSR;
            
            
            Apexpages.currentPage().getParameters().put('RecordType',mapRecordTypeIds.get('Confirmation_Statement'));            
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            cls_confirmation_statement_edit co = new cls_confirmation_statement_edit(con);
            co.gSelect();
            co.rightvalues = new Set<String>{'Asset Management','Other'};
            co.getSelectedValues();
            co.SaveConfirmation();
            
            Test.stopTest();	   
        }
        
        
    } 
    
    @isTest
    static void testNewMethod3(){ 
        
        User comUsr = [SELECT Id,Name,ContactId,Contact.AccountId From User WHERE Email ='testcommuser@difcportal.com'];
        
        System.runAs(comUsr){
            
            Test.startTest();
            
            
            map<string,string> mapRecordTypeIds = new map<string,string>();
            for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Confirmation_Statement','License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
                mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
            }
            
            Service_Request__c objSR = new Service_Request__c();
            objSR.Customer__c = comUsr.Contact.AccountId;
            objSR.Date_of_Declaration__c = Date.today();
            objSR.Email__c = 'test333@test333.com';
            objSR.RecordTypeId = mapRecordTypeIds.get('Confirmation_Statement');
            objSR.Service_Category__c = 'New';
            objSR.I_agree__c=true;
            objSR.Type_of_Lease__c = 'Lease';
            objSR.Rental_Amount__c = 1234;
            objSR.Financial_Year_End_mm_dd__c ='Test';
            objSR.Change_Activity__c = TRUE;
            objSR.Entity_Name_checkbox__c = true;
            objSR.Agreement_Date__c=Date.today()+21;
            objSR.Business_Activity__c = true;
            objSR.Declaration_Signatory_Name__c = 'Test';
            objSR.Declaration_Signatory_Capacity__c = 'Tdst2';
            objSR.Current_Registered_Building__c = 'TestSUbsectorNew';
            insert objSR;
            
            
            Apexpages.currentPage().getParameters().put('RecordType',mapRecordTypeIds.get('Confirmation_Statement'));            
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            cls_confirmation_statement_edit co = new cls_confirmation_statement_edit(con);
            co.gSelect();
            co.rightvalues = new Set<String>{'Brokerage'};
            co.getSelectedValues();
            co.SaveConfirmation();
            
            Test.stopTest();	   
        }
        
        
    }
    
    @isTest
    static void testNewMethod4AllBlank(){ 
        
        User comUsr = [SELECT Id,Name,ContactId,Contact.AccountId From User WHERE Email ='testcommuser@difcportal.com'];
        
        System.runAs(comUsr){
            
            Test.startTest();
            
            
            map<string,string> mapRecordTypeIds = new map<string,string>();
            for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Confirmation_Statement','License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
                mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
            }
            
            Service_Request__c objSR = new Service_Request__c();
            objSR.Customer__c = comUsr.Contact.AccountId;
            objSR.Date_of_Declaration__c = Date.today();
            objSR.Email__c = 'test333@test333.com';
            objSR.RecordTypeId = mapRecordTypeIds.get('Confirmation_Statement');
            objSR.Service_Category__c = 'New';
            objSR.I_agree__c=true;
            objSR.Type_of_Lease__c = 'Lease';
            objSR.Rental_Amount__c = 1234;
            objSR.Financial_Year_End_mm_dd__c ='Test';
            objSR.Change_Activity__c = TRUE;
            objSR.Entity_Name_checkbox__c = true;
            objSR.Agreement_Date__c=Date.today()+21;
            objSR.Business_Activity__c = true;
            objSR.Declaration_Signatory_Name__c = 'Test';
            objSR.Declaration_Signatory_Capacity__c = 'Tdst2';
            //objSR.Current_Registered_Building__c = 'TestSUbsectorNew';
            insert objSR;
            
            
            Apexpages.currentPage().getParameters().put('RecordType',mapRecordTypeIds.get('Confirmation_Statement'));            
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
            cls_confirmation_statement_edit co = new cls_confirmation_statement_edit(con);
            //co.gSelect();
            //co.rightvalues = new Set<String>{'Asset Management','Other'};
            //co.getSelectedValues();
            co.SaveConfirmation();
            
            Test.stopTest();	   
        }
        
        
    }
    
}