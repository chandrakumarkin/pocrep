@isTest
public class OB_SetupObjectDataHelperTest {
    @isTest
    static void createTestData() {
    	UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
		insert r;
    	Profile adminProfile = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    	//User admin = [SELECT Id, Username, UserRoleId FROM User WHERE Profile.Name = 'System Administrator' AND UserRoleId = :userRole_1.Id LIMIT 1];
    	
    	User admin = new User(Alias = 'tstuassr', Email='testu12r@difc.portal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = adminProfile.Id,
                        Community_User_Role__c='Employee Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser12@testorg.com',UserRoleId = r.Id);
        insert admin;
         
    	system.runAs(admin){
    		
	    	Group g1 = new Group(Name='group name', type='Queue');
	        insert g1;
	        QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Lead');
	        insert q1;
	        
	        //Create Parent Group
	       Group grp = new Group();
	        grp.name = 'Test Group1';
	        grp.Type = 'Regular'; 
	        Insert grp; 
	 
	        //Create Group Member
	        GroupMember grpMem1 = new GroupMember();
	        grpMem1.UserOrGroupId = UserInfo.getUserId();
	        grpMem1.GroupId = grp.Id;
	        Insert grpMem1;
    	
	    	test.starttest();
	    	OB_SetupObjectDataHelper.getsObjectQueues('Lead');
	    	OB_SetupObjectDataHelper.getEmailTemplates();
	    	OB_SetupObjectDataHelper.GetDelegatedUsers(admin.Id);
	    	OB_SetupObjectDataHelper.GetDelegatedUsers(grp.Id);
	    	test.stoptest();
    	}
    }
}