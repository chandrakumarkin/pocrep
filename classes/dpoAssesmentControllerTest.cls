/*
   
*/
@isTest
public class dpoAssesmentControllerTest 
{
    
    private static testmethod void onAmedSaveDBTest()
    {
        
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;
        
        insertNewAccounts[0].ROC_Status__c = 'Account Created';
        update insertNewAccounts[0];
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom']; 
         
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',contactid = insertNewContacts[0].Id);
        insert u;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'DPO_Annual_Assessment'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('DPO_Annual_Assessment', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Identify ultimate beneficial owners';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;

        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        Recordtype rt =  [Select Id, DeveloperName FROM RecordType WHERE SObjectType = 'HexaBPM__Service_Request__c' and DeveloperName ='DPO_Annual_Assessment' limit 1];
        
        HexaBPM__Service_Request__c newSR = new HexaBPM__Service_Request__c();
        newSR.RecordtypeId = rt.Id ;  
        newSR.Last_Name__c = 'test DPO';
        newSR.HexaBPM__Customer__c = insertNewAccounts[0].id  ;
        //newSR.Entity_Type__c =  listEntityType.size() >= i ? listEntityType[i-1] : listEntityType[0];
        //newSR.Business_Sector__c =  listBusiness_Sector.size() >= i ? listBusiness_Sector[i-1] : listBusiness_Sector[0];
        //newSR.legal_structures__c =  listLegalStructures.size() >= i ? listLegalStructures[i-1] : listLegalStructures[0];
        //newSR.Type_of_Entity__c =  listTypeofEntity.size() >= i ? listTypeofEntity[i-1] : listTypeofEntity[0];
        insert newSR;
        
        Annual_Assessment__c annualAsses = new Annual_Assessment__c();
        annualAsses.Identify_the_objectives__c = 'test';
        annualAsses.Collect__c = 'test';
        annualAsses.Use__c = 'test';
        annualAsses.Delete_Retention__c = 'test';
        annualAsses.Store__c = 'test';
        annualAsses.Personal_data_with_Internal_or_External__c = 'Internal - within the group of companies or between departments';
        annualAsses.Internal_and_External_stakeholders_Notes__c = 'test';
        annualAsses.What_is_the_source_of_the_data__c = 'Data subject directly  ';
        annualAsses.Source_of_the_data_Risk_Notes__c = 'test';
        annualAsses.Provide_a_flow_diagram_or_describing_dat__c = 'test';
        annualAsses.Types_of_processing_in_your_operations__c = 'a material amount of Special Categories of Personal Data is to be Processed    ';
        annualAsses.Does_the_collected_data_include_special__c = 'No';
        annualAsses.Does_the_collected_data_include_criminal__c = 'Yes';
        annualAsses.Service_Request__c = newSR.Id;
               
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        test.startTest();
        system.runAs(u){
       
            dpoAssesmentController.RequestWrapper reqWrapper = new dpoAssesmentController.RequestWrapper();
            reqWrapper.flowId = listPageFlow[0].Id;
            reqWrapper.pageId = listPage[0].Id;
            reqWrapper.srId = newSR.Id;
            dpoAssesmentController.getDataController(JSON.serialize(reqWrapper));
        
            dpoAssesmentController.RequestWrapper1 reqWrapper1 = new dpoAssesmentController.RequestWrapper1();
            reqWrapper1.annualAssesment = annualAsses;
            dpoAssesmentController.onAmedSaveDB1(JSON.serialize(reqWrapper1));
     
            dpoAssesmentController.RequestWrap reqWrap = new dpoAssesmentController.RequestWrap();
            reqWrap.srId = newSR.Id;
            dpoAssesmentController.fetchSRObjFields(JSON.serialize(reqWrap));
            dpoAssesmentController.createAssessmentSRs('DPO_Annual_Assessment');
            
            dpoAssesmentController.getButtonAction(newSR.Id,'','');
            
            dpoAssesmentController.RequestWrapper2 reqWrapper2 = new dpoAssesmentController.RequestWrapper2();
            reqWrapper2.annualAssesment = new List<Annual_Assessment_Risk_and_Matrix__c>();
            reqWrapper2.amRec = annualAsses;
            dpoAssesmentController.onAmedSaveDB2(JSON.serialize(reqWrapper2));
            reqWrapper.srId = newSR.Id;
            dpoAssesmentController.getDataController(JSON.serialize(reqWrapper));
            dpoAssesmentController.fieldNameAndLableWrap  fieldNameAndLableWrap = new dpoAssesmentController.fieldNameAndLableWrap();
            dpoAssesmentController.secDetailWrap fieldNameAndLableWrapNew = new dpoAssesmentController.secDetailWrap();               
    
        }
        
        HexaBPM__Step__c hx = new HexaBPM__Step__c(HexaBPM__SR__c =newSR.Id);
        insert hx;
        DPOAssessmentApprovalBatchSch sh1= new DPOAssessmentApprovalBatchSch();
        String sch = '0 0 23 * * ?';
        system.schedule('Test check', sch, sh1);
        
        test.stopTest();
          
    }
    
    private static testmethod void onAmedSaveDBTest1()
    {
        
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;
        
        insertNewAccounts[0].ROC_Status__c = 'Account Created';
        update insertNewAccounts[0];
        
        
        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom']; 
         
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',contactid = insertNewContacts[0].Id);
        insert u;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'DPO_Annual_Assessment'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('DPO_Annual_Assessment', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Identify ultimate beneficial owners';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;

        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        Recordtype rt =  [Select Id, DeveloperName FROM RecordType WHERE SObjectType = 'HexaBPM__Service_Request__c' and DeveloperName ='DPO_Annual_Assessment' limit 1];
    
               
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        test.startTest();
        system.runAs(u){
            try{
            dpoAssesmentController.createAssessmentSRs('DPO_Annual_Assessment');
            }catch(exception e){
            
            }
                
        }
        
        
        test.stopTest();
          
    }
}