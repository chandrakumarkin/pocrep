@isTest
public class CC_AutoCloseNameCheckTest {
    public static testmethod void test1(){
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        //insertNewAccounts[0].Next_Renewal_Date__c = Date.today();
        insert insertNewAccounts;
        
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 
        
        License__c lic = new License__c(name='12345',Account__c=insertNewAccounts[0].id, License_Expiry_Date__c = Date.today());
        insert lic;
        
        insertNewAccounts[0].Active_License__c = lic.id;
        update insertNewAccounts;
        
        
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
         List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].Passport_No__c = 'A12345';
        insertNewSRs[0].Nationality__c = 'India';
        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
        insertNewSRs[0].Gender__c = 'Male';
        insertNewSRs[0].Place_of_Birth__c = 'Dubai';
        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
        insertNewSRs[0].Last_Name__c = 'test';
        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Business_Sector__c = 'Hotel';
        insertNewSRs[0].entity_name__c = 'Tentity';
        insertNewSRs[0].Progress_Completion__c = 10.0;
        insertNewSRs[1].License_Application__c = lic.id;
        insertNewSRs[0].HexaBPM__Contact__c = con.id;
       // insertNewSRs[0].Passport_Expiry_Date__c = Date.today().addmonths(20);
       // insertNewSRs[0].Place_of_Issue__c = 'Dubai';
        insert insertNewSRs;
        
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs[0].id;
        insertNewSRs[0].HexaBPM__Parent_SR__c = insertNewSRs[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        update insertNewSRs;
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'REGISTRAR_REVIEW'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
        listSRSteps[0].HexaBPM__Step_No__c = 1;
        //listSRSteps[0].HexaBPM__Step_Template_Code__c = 'CLO_REVIEW';
        insert listSRSteps;
        
       
        
        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Awaiting Verification',HexaBPM__Code__c='AWAITING_VERIFICATION',HexaBPM__Type__c='Start');
        
        HexaBPM__Status__c status1 = new HexaBPM__Status__c
        (Name='Re-Upload Document',HexaBPM__Code__c='REUPLOAD_DOCUMENT',HexaBPM__Type__c='End');
        insert status1;
        
         List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
    insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        insertNewSteps[0].HexaBPM__Status__c = status.id;
       // insertNewSteps[0].Step_Template_Code__c = 'CLO_REVIEW';
        insertNewSteps[0].Relationship_Manager__c = userinfo.getUserId();
        insertNewSteps[1].Relationship_Manager__c = userinfo.getUserId();
        insert insertNewSteps;
        
        HexaBPM__Transition__c transition = new HexaBPM__Transition__c(HexaBPM__From__c=status.id,HexaBPM__To__c=status1.id);
        insert transition;
        
        HexaBPM__SR_Status__c srStatus = new HexaBPM__SR_Status__c(Name='Awaiting Re-Upload',HexaBPM__Code__c='AWAITING_RE-UPLOAD');
        insert srStatus;
        
        HexaBPM__Step_Transition__c stepTransition = new HexaBPM__Step_Transition__c
        (HexaBPM__SR_Step__c=listSRSteps[0].id,HexaBPM__Transition__c=transition.id,HexaBPM__SR_Status_Internal__c=listSRStatus[1].id,HexaBPM__SR_Status_External__c=listSRStatus[1].id);
        insert stepTransition;
        
        Company_Name__c companyName = new Company_Name__c(Application__c = insertNewSRs[1].id,Status__c='Approved' , Entity_Name__c = insertNewAccounts[0].id,OB_Is_Trade_Name_Allowed__c='Yes');
        insert companyName;
        
         // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'GENERAL'});
        listDocMaster[0].HexaBPM__Code__c='NOC_SHARING';
        insert listDocMaster;
        // create SR Template doc
        

        
        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        listSRTemplateDoc[0].HexaBPM__Added_through_Code__c = true;
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].id;
        insert listSRTemplateDoc;
        
        //----------------------------------------------------------------------------------------------------
        Test.startTest();
            CC_AutoCloseNameCheck CC_AutoCloseNameCheckObj = new CC_AutoCloseNameCheck();
            CC_AutoCloseNameCheckObj.EvaluateCustomCode(insertNewSRs[0], insertNewSteps[0]);
            
            CC_AutoCloseStep CC_AutoCloseStepObj = new CC_AutoCloseStep();
            CC_AutoCloseStepObj.EvaluateCustomCode(insertNewSRs[0], insertNewSteps[0]);
          
          HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c,HexaBPM__SR__r.Progress_Completion__c,HexaBPM__SR__r.HexaBPM__Contact__c,
                                     HexaBPM__SR__r.Sharing_Space_with_Affiliated_Entity__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[0].Id  LIMIT 1];
        
          HexaBPM__Step__c step1 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[1].Id  LIMIT 1];
        
          CC_SecurityStepApproval CC_SecurityStepApprovalObj = new CC_SecurityStepApproval();
          CC_SecurityStepApprovalObj.EvaluateCustomCode(insertNewSRs[0], step);
          
          CC_CreateCompliance CC_CreateComplianceObj = new CC_CreateCompliance();
            CC_CreateComplianceObj.EvaluateCustomCode(insertNewSRs[0], step);
        
        
        
          CC_Create_AOR_Financial CC_Create_AOR_FinancialObj = new CC_Create_AOR_Financial();
          CC_Create_AOR_FinancialObj.EvaluateCustomCode(insertNewSRs[0], step);
        
          CC_Create_AOR_NF_Retail CC_Create_AOR_NF_RetailObj = new CC_Create_AOR_NF_Retail();
          CC_Create_AOR_NF_RetailObj.EvaluateCustomCode(insertNewSRs[0], step);
        
          CC_Create_SR_On_AOR_Rejection CC_Create_SR_On_AOR_RejectionObj = new CC_Create_SR_On_AOR_Rejection();
          CC_Create_SR_On_AOR_RejectionObj.EvaluateCustomCode(insertNewSRs[0], step);
        
          CC_EntityNameSRValidation CC_EntityNameSRValidationObj = new CC_EntityNameSRValidation();
          CC_EntityNameSRValidationObj.EvaluateCustomCode(insertNewSRs[0], step);
        
        
        /*  for(HexaBPM__SR_Template_Docs__c SRTmpDoc:[Select Id,HexaBPM__SR_Template__c,HexaBPM__Document_Master__r.HexaBPM__Code__c,HexaBPM__Document_Master__c,HexaBPM__Group_No__c,HexaBPM__Document_Description__c,HexaBPM__Document_Master__r.Name,HexaBPM__Document_Description_External__c,
                                                       HexaBPM__Generate_Document__c,HexaBPM__Added_through_Code__c,HexaBPM__Document_Master__r.HexaBPM__LetterTemplate__c from HexaBPM__SR_Template_Docs__c]){
        system.debug('SRTmpDoc==>'+ SRTmpDoc);
                system.debug(' HexaBPM__SR_Template__c =='+  SRTmpDoc.HexaBPM__SR_Template__c);
                system.debug(' HexaBPM__Added_through_Code__ =='+  SRTmpDoc.HexaBPM__Added_through_Code__c);
                system.debug('  HexaBPM__Document_Master__r.HexaBPM__Code__c =='+  SRTmpDoc.HexaBPM__Document_Master__r.HexaBPM__Code__c);
                system.debug('  :stp.HexaBPM__SR__r.HexaBPM__SR_Template__c =='+  step.HexaBPM__SR__r.HexaBPM__SR_Template__c);
            } */
          listSRTemplateDoc[0].HexaBPM__SR_Template__c = step.HexaBPM__SR__r.HexaBPM__SR_Template__c;
          update listSRTemplateDoc;
          CC_GenerateOfficeSharingNOC CC_GenerateOfficeSharingNOCObj = new CC_GenerateOfficeSharingNOC();
          CC_GenerateOfficeSharingNOCObj.EvaluateCustomCode(insertNewSRs[0], step);
          
        
          CC_HasGeneralPartner CC_HasGeneralPartnerObj = new CC_HasGeneralPartner();
          CC_HasGeneralPartnerObj.EvaluateCustomCode(insertNewSRs[0], step);
          CC_HasGeneralPartnerObj.EvaluateCustomCode(insertNewSRs[1], step1);
        
          CC_HasInvestmentFund CC_HasInvestmentFundObj = new CC_HasInvestmentFund();
          CC_HasInvestmentFundObj.EvaluateCustomCode(insertNewSRs[0], step);
          CC_HasInvestmentFundObj.EvaluateCustomCode(insertNewSRs[1], step);
        
          CC_HasOECActivity CC_HasOECActivityObj = new CC_HasOECActivity();
          CC_HasOECActivityObj.EvaluateCustomCode(insertNewSRs[0], step);
          CC_HasOECActivityObj.EvaluateCustomCode(new HexaBPM__Service_Request__c(HexaBPM__Parent_SR__c = insertNewSRs[0].id), step);
        
          CC_HasOperatingRepOffice CC_HasOperatingRepOfficeObj = new CC_HasOperatingRepOffice();
          CC_HasOperatingRepOfficeObj.EvaluateCustomCode(insertNewSRs[0], step);
          CC_HasOperatingRepOfficeObj.EvaluateCustomCode(insertNewSRs[1], step);
        
          CC_HasRBActivity CC_HasRBActivityObj = new CC_HasRBActivity();
          CC_HasRBActivityObj.EvaluateCustomCode(insertNewSRs[0], step);
          CC_HasRBActivityObj.EvaluateCustomCode(new HexaBPM__Service_Request__c(HexaBPM__Parent_SR__c = insertNewSRs[0].id), step);
        
          CC_LM_Assignment CC_LM_AssignmentObj = new CC_LM_Assignment();
          CC_LM_AssignmentObj.EvaluateCustomCode(insertNewSRs[0], step);
        
          CC_ShareholderCount CC_ShareholderCountObj = new CC_ShareholderCount();
          CC_ShareholderCountObj.EvaluateCustomCode(insertNewSRs[0], step);
        
          CC_Step_Assignment CC_Step_AssignmentObj = new CC_Step_Assignment();
          CC_Step_AssignmentObj.Assign_Step_Owner_SR_Submit(new Map<String, HexaBPM__Service_Request__c>(), new list < HexaBPM__Step__c >(), new Map < String, HexaBPM__SR_Steps__c >());
          CC_Step_AssignmentObj.Assign_Step_Owner_Step_Closure(new Map < String, HexaBPM__Step__c >(), new list < HexaBPM__Step__c >(), new  Map < String, HexaBPM__SR_Steps__c >());
          
          //HexaBPM__Action__c action = new HexaBPM__Action__c(HexaBPM__Action_Type__c='HexaBPM__Service_Request__c',HexaBPM__Field_Name__c='Address_Line_1__c',HexaBPM__Field_Type__c='Text',HexaBPM__Value_or_Field__c='Value');
          CC_Update_sObject CC_Update_sObjectObj = new CC_Update_sObject();
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='HexaBPM__Service_Request__c',HexaBPM__Field_Name__c='Address_Line_1__c',HexaBPM__Field_Type__c='Text',HexaBPM__Value_or_Field__c='Value'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='HexaBPM__Service_Request__c',HexaBPM__Field_Name__c='Progress_Completion__c',HexaBPM__Field_Type__c='Double',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='5'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='HexaBPM__Service_Request__c',HexaBPM__Field_Name__c='Date_of_Issue__c',HexaBPM__Field_Type__c='DATE',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='01/01/2020'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='HexaBPM__Service_Request__c',HexaBPM__Field_Name__c='DFSA_DNFBP_Approval__c',HexaBPM__Field_Type__c='BOOLEAN',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='true'), step);
        
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='HexaBPM__Step__c',HexaBPM__Field_Name__c='HexaBPM__Rejection_Reason__c',HexaBPM__Field_Type__c='Text',HexaBPM__Value_or_Field__c='Value'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='HexaBPM__Step__c',HexaBPM__Field_Name__c='HexaBPM__Step_No__c',HexaBPM__Field_Type__c='Double',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='5'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='HexaBPM__Step__c',HexaBPM__Field_Name__c='HexaBPM__Closed_Date__c',HexaBPM__Field_Type__c='DATE',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='01/01/2020'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='HexaBPM__Step__c',HexaBPM__Field_Name__c='Returned__c',HexaBPM__Field_Type__c='BOOLEAN',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='true'), step);
        
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='Account',HexaBPM__Field_Name__c='Name',HexaBPM__Field_Type__c='Text',HexaBPM__Value_or_Field__c='Value'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='Account',HexaBPM__Field_Name__c='Calculated_Visa_Quota__c',HexaBPM__Field_Type__c='Double',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='5'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='Account',HexaBPM__Field_Name__c='Blacklisted_Until__c',HexaBPM__Field_Type__c='DATE',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='01/01/2020'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='Account',HexaBPM__Field_Name__c='IsPartner',HexaBPM__Field_Type__c='BOOLEAN',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='true'), step);
        
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='Contact',HexaBPM__Field_Name__c='FirstName',HexaBPM__Field_Type__c='Text',HexaBPM__Value_or_Field__c='Value'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='Contact',HexaBPM__Field_Name__c='Monthly_Salary__c',HexaBPM__Field_Type__c='Double',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='5'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='Contact',HexaBPM__Field_Name__c='Date_of_Issue__c',HexaBPM__Field_Type__c='DATE',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='01/01/2020'), step);
          CC_Update_sObjectObj.UpdatesObject(new HexaBPM__Action__c(HexaBPM__Action_Type__c='Contact',HexaBPM__Field_Name__c='Recitfy_visa__c',HexaBPM__Field_Type__c='BOOLEAN',HexaBPM__Value_or_Field__c='Value',HexaBPM__Field_Value__c='true'), step);
        
        
          CC_AOR_RS_ManagerValidation CC_AOR_RS_ManagerValidationObj = new CC_AOR_RS_ManagerValidation();
          CC_AOR_RS_ManagerValidationObj.EvaluateCustomCode(insertNewSRs[0], step);
        CC_AOR_RS_ManagerValidationObj.EvaluateCustomCode(null, null);
        system.debug('@@@@@@@@@@@ srRec ');
        
        
        
        insertNewSRs[0].Authorized_Signatory_Combinations__c = 'filler text';
        insertNewSRs[0].Authorized_Signatory_Combinations_Arabic__c = 'filler text';
        update insertNewSRs[0];
        
        CC_AOR_RS_ManagerValidationObj.EvaluateCustomCode(insertNewSRs[0], step);
          //-------------------------------------------------------------------------------------------------------
          Id p = [select id from profile where name='DIFC Customer Community User Custom'].id;      
             
                      
            User user = new User(alias = 'test123', email='test123@noemail.com',
                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                    localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                    ContactId = con.Id,
                    timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
           
            insert user;
            //system.runAs(user)
            {
                CC_UpdateConvertedLeadSRContact CC_UpdateConvertedLeadSRContactobj = new CC_UpdateConvertedLeadSRContact();
                CC_UpdateConvertedLeadSRContactobj.EvaluateCustomCode(insertNewSRs[0], step);
                
                CC_AutoSubmit_SR CC_AutoSubmit_SRObj = new CC_AutoSubmit_SR();
                CC_AutoSubmit_SRObj.AutoSubmit_SR(new Set<Id>{insertNewSRs[0].id}, String.valueof(status.id));
                
                CC_CheckInPrincipleApproval CC_CheckInPrincipleApprovalObj = new CC_CheckInPrincipleApproval();
                CC_CheckInPrincipleApprovalObj.EvaluateCustomCode(insertNewSRs[0], step);
                
                CC_CloneRejectedSR CC_CloneRejectedSRobj = new CC_CloneRejectedSR();
                CC_CloneRejectedSRobj.EvaluateCustomCode(insertNewSRs[0], step);
                
                
                insertNewSRs[1].Entity_Type__c = null;
                insertNewSRs[1].HexaBPM__Parent_SR__c = null;
                insertNewSRs[1].Last_Name__c = 'test';
                insertNewSRs[1].HexaBPM__Email__c = 'teset@test.com';
                //insertNewSRs[1].Entity_Type__c = 'Retail';
                update insertNewSRs;
                HexaBPM__Step__c step2 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[1].Id  LIMIT 1];
                
                CC_CopyApprovedNameToAccount CC_CopyApprovedNameToAccountObj = new CC_CopyApprovedNameToAccount();
                CC_CopyApprovedNameToAccountObj.EvaluateCustomCode(insertNewSRs[0], step);
                CC_CopyApprovedNameToAccountObj.EvaluateCustomCode(insertNewSRs[0], step2);
                
                CC_CreatePortalUser CC_CreatePortalUserObj = new CC_CreatePortalUser();
                CC_CreatePortalUserObj.EvaluateCustomCode(insertNewSRs[0], step);
                CC_CreatePortalUserObj.EvaluateCustomCode(insertNewSRs[0], step2); 
                
                insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('New_User_Registration').getRecordTypeId();
                insertNewSRs[1].Entity_Type__c = 'Financial - related';
                insertNewSRs[1].Business_Sector__c ='Investment Fund';
                update insertNewSRs;
                
                HexaBPM__Step__c step3 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[1].Id  LIMIT 1];
                
                CC_CreatePortalUserObj.EvaluateCustomCode(insertNewSRs[0], step3);
                
            }
        Test.stopTest();
    }
    public static testMethod void CC_SendCP_Email() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
      
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'Commercial_Permission';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New Commercial Permission').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
        HexaBPM__Document_Master__c docmaster = new HexaBPM__Document_Master__c ();
        docmaster.HexaBPM__Code__c ='Commercial_Permission';
        insert docmaster;
        
         HexaBPM__Document_Master__c docmaster2 = new HexaBPM__Document_Master__c ();
        docmaster2.HexaBPM__Code__c ='Commercial_Permission_Dual_License';
        insert docmaster2;
        
        HexaBPM__SR_Template_Docs__c objSRTempdoc = new HexaBPM__SR_Template_Docs__c();
        objSRTempdoc.Visible_to_GS__c = true;
        objSRTempdoc.HexaBPM__Document_Master__c = docmaster.id;
        insert objSRTempdoc;
        
        HexaBPM__SR_Doc__c objSrDoc = new HexaBPM__SR_Doc__c();
        objSrDoc.HexaBPM__Service_Request__c = objHexaSR.Id;
        objsrDoc.HexaBPM__Document_Master__c = docmaster.id;
        objSRDoc.HexaBPM__SR_Template_Doc__c = objSRTempdoc.Id;
         objSrDoc.HexaBPM__Doc_ID__c = '123';
        insert objSRDoc;
        
         HexaBPM__SR_Doc__c objSrDoc2 = new HexaBPM__SR_Doc__c();
        objSrDoc2.HexaBPM__Service_Request__c = objHexaSR.Id;
        objsrDoc2.HexaBPM__Document_Master__c = docmaster2.id;
        objSRDoc2.HexaBPM__SR_Template_Doc__c = objSRTempdoc.Id;
         objSrDoc.HexaBPM__Doc_ID__c = '123';
        insert objSRDoc2;
        
        ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true
            );
            insert contentVersionInsert;
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            
            //create ContentDocumentLink  record 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = objSrDoc.id;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.shareType = 'V';
            insert cdl;
        
        objSrDoc.HexaBPM__Doc_ID__c = cdl.id;
        update objSrDoc;
       
       Blob b = Blob.valueOf('Test Data');
        
        Attachment attachment = new Attachment();
        attachment.ParentId = objSRDoc.Id;
        attachment.Name = 'Commercial Permission Letter';
        attachment.Body = b;
        insert(attachment);
        
        Test.startTest();
         HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Account_Owner_Email__c   
                                  ,HexaBPM__SR__r.Type_of_commercial_permission__c                            
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_CreateCompliance tester = new CC_CreateCompliance();
        tester.EvaluateCustomCode(objHexaSR,step); 
        
        Id SRRecId3 = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New Commercial Permission').getRecordTypeId();
        objHexaSR.RecordtypeId = SRRecId3;
        objHexaSR.Type_of_commercial_permission__c = 'Dual license of DED licensed firms with an affiliate in DIFC';
        update objHexaSR;
         HexaBPM__Step__c step2 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Account_Owner_Email__c   
                                  ,HexaBPM__SR__r.Type_of_commercial_permission__c   ,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c                          
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
          tester.EvaluateCustomCode(objHexaSR,step2); 
        Test.stopTest();
       
    }
}