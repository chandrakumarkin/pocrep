/*
Author		:	Durga Prasad
Description	:	Trigger Handler for Lead Object Triger

v1.0   Merul  					Addition of createCP.
v1.1   prateek  				Addition of createSrsForExistingContact.
V1.2   salma.rasheed@pwc.com	Added Type of Commercial Permission in the Query to map the Type of CP from Account to SR
*/
public without sharing class OB_LeadTriggerHandler
{

public static boolean firstRun = true;

public class exsistingConAccWrapp {
	
	public String contactId;
	public contact relConObj;
	public account relAccObj;        
	
}
		
public static void createSrsForExistingContact( List<Lead> TriggerNew){

	if(firstRun){
	
	firstRun = false;
	Map<Id,id> relConAccIdMap = new Map<Id,id>();
	list<exsistingConAccWrapp> exsistingConAccWrappList = new list<exsistingConAccWrapp>();

	Map<Id,account> mapConCPSr = new Map<Id,account>();
	Map<Id,account> mapConSrFinancial = new Map<Id,account>();
	Map<Id,account> mapConSrNonFinancial = new Map<Id,account>();

	list<AccountContactRelation> relObjList = new list<AccountContactRelation>();

	for(Lead leadObj : TriggerNew){
		if( leadObj.IsConverted ){
			relConAccIdMap.put(leadobj.ConvertedContactId,leadobj.ConvertedAccountId);
		}
		
	}

	if(!relConAccIdMap.isEmpty()){
		Map<Id,contact> relConMap = new Map<Id,contact>([
															SELECT id,Send_Portal_Login_Link__c,createdDate,
															OB_Self_Registered_Contact__c,accountId
															FROM Contact
															WHERE id IN: relConAccIdMap.keySet()
															]);

	  Map<Id,account> relAccMap = new Map<Id,account>([
															SELECT id,Company_Type__c,Phone,Name,OB_Is_Multi_Structure_Application__c,
															Is_Commercial_Permission__c ,OB_Sector_Classification__c,OB_Type_of_commercial_permission__c,
															ROC_Status__c,createdDate from Account WHERE id IN: relConAccIdMap.values()
															]);	//V1.2 - Added Type of Commercial Permission in the Query
		
		for(AccountContactRelation relObj : [select id,account.Is_Commercial_Permission__c from AccountContactRelation where contactid in: relConAccIdMap.keySet() 
		AND accountid in: relConAccIdMap.values() ]){
			if(relObj.account.Is_Commercial_Permission__c == 'yes'){
				relObj.roles='Company services;Property Services';
			}else{
				relObj.roles='Company services;Property Services;Super User';
			}
			
			relObj.Access_Level__c= '	Read/Write';
			relObjList.add(relObj);
		}		
		
		if(relObjList.size()  > 0){
			
			update relObjList;
		}

		for(Id relConId : relConMap.keySet()){
				Id relAccId = relConAccIdMap.get(relConId);
				Contact relContact = relConMap.get(relConId);
				if(relAccId != null){
					Account relAccount = relAccMap.get(relAccId);
					system.debug('$$$$relAccount createdDate ' +  relAccount.createdDate);
					system.debug('$$$$relContact createdDate ' +  relContact.createdDate);
					if(relAccount != null && relContact.accountId != null &&
					   relAccount.createdDate != null && relAccount.createdDate > relContact.createdDate){
						exsistingConAccWrapp wrppObj = new exsistingConAccWrapp();
						wrppObj.contactId = relConId;
						wrppObj.relAccObj = relAccount;
						wrppObj.relConObj = relContact;
						exsistingConAccWrappList.add(wrppObj);
					}
				}
		}													
	}
system.debug('$$$$exsistingConAccWrappList ' + exsistingConAccWrappList);
	if(exsistingConAccWrappList.size() > 0 ){
		for(Lead leadObj : TriggerNew)
		{
			
				exsistingConAccWrapp wrapObj = new exsistingConAccWrapp();
				for(exsistingConAccWrapp wrppObjItr : exsistingConAccWrappList){
					if (wrppObjItr.contactId == leadObj.ConvertedContactId){
						wrapObj = wrppObjItr;
					}
				}
				
				system.debug('$$$$current wrapper ' +  wrapObj);
				contact relConObj = wrapObj.relConObj;
				account relAccObj = wrapObj.relAccObj;
				

				if( relAccObj.Is_Commercial_Permission__c == 'Yes' && leadObj.Send_Portal_Login_Link__c == 'Yes' ){
					mapConCPSr.put(leadObj.ConvertedContactId ,relAccObj );	

			 }else if(leadObj.Send_Portal_Login_Link__c == 'Yes' && 
								 ((relAccObj.Company_Type__c == 'Non - financial' && relAccObj.OB_Sector_Classification__c == 'Investment Fund')
								 || relAccObj.Company_Type__c == 'Financial - related')
			 					&& (relAccObj.ROC_Status__c == 'Account Created' || relAccObj.ROC_Status__c == 'Under Formation')){

					mapConSrFinancial.put(leadObj.ConvertedContactId ,relAccObj);	

			 }else if(leadObj.Send_Portal_Login_Link__c == 'Yes'  &&
								 ((relAccObj.Company_Type__c == 'Non - financial' && relAccObj.OB_Sector_Classification__c != 'Investment Fund')
								 || relAccObj.Company_Type__c == 'Retail')
			 					&& (relAccObj.ROC_Status__c == 'Account Created' || relAccObj.ROC_Status__c == 'Under Formation')){

					mapConSrNonFinancial.put(leadObj.ConvertedContactId ,relAccObj);	

			 }

			}

	}
	system.debug('$$$$ cpMap ' + mapConCPSr);			
	
	if(!mapConCPSr.isEmpty()){
		OB_CreateCP.CreateCP(mapConCPSr);
	}
	if(!mapConSrFinancial.isEmpty()){
		OB_CreateDraftSR.createSRForExstCon(mapConSrFinancial);
	}
	if(!mapConSrNonFinancial.isEmpty()){
		OB_CreateNonFinancialSR.createNonFinancialSRForExstCon(mapConSrNonFinancial);
	}

	}
	
	
	

}

//Addition of createCP.
public static void createCP( List<Lead> TriggerNew,
								Map<Id,Lead> TriggerOldMap)
{
	
	/*
		Map<Id,Id> mapConIdAccId = new Map<Id,Id>();
		set<Id> accIdSet = new set<Id>();

		for(Lead leadObj : TriggerNew)
		{
			accIdSet.add(leadobj.ConvertedAccountId);
		}

		Map<Id,Account> mapAccId = new Map<Id,Account>(
															[
																SELECT id,
																		Is_Commercial_Permission__c 
																FROM Account
																WHERE id IN:accIdSet 
															]
													);

		

		for(Lead leadObj : TriggerNew)
		{
			system.debug('@@@@@@@@@@@ leadObj '+leadObj.IsConverted );
			// IsConverted 
			if( 
				leadObj.IsConverted 
						&& mapAccId.containsKey( leadObj.ConvertedAccountId ) 
			)
			{
				if( mapAccId.get( leadObj.ConvertedAccountId ).Is_Commercial_Permission__c == 'Yes' )
				{
					mapConIdAccId.put(leadObj.ConvertedContactId ,leadobj.ConvertedAccountId );	
				}
				
			}

		}
	
		if( mapConIdAccId.size() > 0)
		{
			OB_CreateCP.CreateCP(mapConIdAccId);
		}

	*/
	
	
}
    
    public static void createNonFinancialSRForExstConHelper(Map<Id,account> mapConAccId){

        list<contact> lstContact = new list<contact>();
        list<HexaBPM__Service_Request__c> lstSR = new list<HexaBPM__Service_Request__c>();
        list<string> lstContactId = new list<string>();
        string lookupValue;
        Contact contactRecord;
        list<string> lstLookUpField = new list<string>();
        map<string,Contact> mapContact = new map<string,Contact>();
        map<string,boolean> mapCommercialPermission = new map<string,boolean>();
        String commaSepratedFields = '';
        string soql;

        /* for(Contact contObj : lstContact)
            lstContactId.add(contObj.id); */
        for(id contId : mapConAccId.keySet()){
            lstContactId.add(contId);
        }   

		set<string> setQueryFields = new set<string>();
		for(OB_User_Registration_SR_Map__c settingObj : OB_User_Registration_SR_Map__c.getAll().values()) {
           
        }
        setQueryFields.add('account.is_commercial_permission__c');
        for(string strField : setQueryFields) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = strField;
            }else{
                
            }
        }
        set<string> setAccountId = new set<string>();
        soql = 'select ' +commaSepratedFields+',OB_Block_Homepage__c from Contact where id in :lstContactId';
        for(Contact contactObj : Database.query(soql)){
            mapContact.put(contactObj.id,contactObj);
           /*  if(contactObj.account.is_commercial_permission__c=='Yes')
            	mapCommercialPermission.put(contactObj.Id,true);
            else
            	mapCommercialPermission.put(contactObj.Id,false);
            	
            if(contactObj.AccountId!=null)
                setAccountId.add(contactObj.AccountId); */

            lstContact.add(contactObj);

            if(mapConAccId.get(contactObj.id) != null && mapConAccId.get(contactObj.id).is_commercial_permission__c=='Yes'){
                
            }
            	
            else
            	mapCommercialPermission.put(contactObj.Id,false);
            	
            if(mapConAccId.get(contactObj.id).id !=null)
                setAccountId.add(mapConAccId.get(contactObj.id).id);
            
        }
		
		//Code to check SR Exists or not
        map<string,string> MapAccountUserRegApplication = new map<string,string>();
        map<string,string> MapAccountInPrincpApplication = new map<string,string>();
        map<string,string> MapOpportunities = new map<string,string>();//v1.1
        set<string> setRecordTypeIds = new set<string>{Label.OB_USER_REG_RECORDTYPEID,Label.OB_IN_PRINCIPLE_ID};
        if(setAccountId.size()>0){
        	for(Opportunity Opp:[Select Id,AccountId from Opportunity where AccountId IN:setAccountId order by CreatedDate]){//v1.1
        		MapOpportunities.put(Opp.AccountId,Opp.Id);
        	}
	        for(HexaBPM__Service_Request__c objSR:[Select Id,HexaBPM__Customer__c,HexaBPM__Record_Type_Name__c from HexaBPM__Service_Request__c where HexaBPM__Customer__c IN:setAccountId and RecordTypeId IN:setRecordTypeIds and HexaBPM__IsCancelled__c=false and HexaBPM__Is_Rejected__c=false]){
	        	
	        }
        }
		
        for(Contact contactObj : lstContact){
            if(contactObj.AccountId!=null && MapAccountUserRegApplication.get(contactObj.AccountId)==null && !mapCommercialPermission.get(contactObj.Id)){
                
            }
        }
    }


public static void Execute_AU(list<Lead> TriggerNew,map<Id,Lead> TriggerOldMap)
{
		CheckUserExists(TriggerNew);
		createSrsForExistingContact(TriggerNew);
}

public static void CheckUserExists(list<Lead> TriggerNew)
{
		set<string> setEmails = new set<string>();
		map<string,string> MapUserEmail = new map<string,string>();
		set<string> ContactIds = new set<string>();
	for(Lead ld:TriggerNew){
		if(ld.Email!=null && ld.IsConverted && ld.Send_Portal_Login_Link__c=='Yes' && ld.ConvertedContactId!=null){
			setEmails.add(ld.Email);
			ContactIds.add(ld.ConvertedContactId);
		}
	}
	map<string,Contact> MapConvertedContacts = new map<string,Contact>();
	if(ContactIds.size()>0){
		for(Contact obj:[Select Id,AccountId,CreatedDate from Contact where Id IN:ContactIds]){
			MapConvertedContacts.put(obj.Id,obj);
		}
	}
	if(setEmails.size()>0){
		for(User usr:[Select Id,Email,username from User where Email IN:setEmails and IsActive=true and ContactId!=null]){
			MapUserEmail.put(usr.Email.tolowercase(),usr.UserName);
		}
	}
	for(Lead ld:TriggerNew){
		if(ld.Email!=null && ld.IsConverted && ld.Send_Portal_Login_Link__c=='Yes' && MapUserEmail.get(ld.Email.tolowercase())!=null){
			system.debug('ContactCreatedDate==>'+MapConvertedContacts.get(ld.ConvertedContactId).CreatedDate);
			system.debug('LastModifiedDate==>'+ld.LastModifiedDate);
			
			DateTime ConvertedDateTime = ld.LastModifiedDate.addSeconds(-15);
			system.debug('ConvertedDateTime==>'+ConvertedDateTime);
			
			system.debug('ConvertedContactId==>'+ld.ConvertedContactId);
			//MapConvertedContacts.get(ld.ConvertedContactId).CreatedDate <= ConvertedDateTime
			if(ld.ConvertedContactId!=null && MapConvertedContacts.get(ld.ConvertedContactId).CreatedDate >= ConvertedDateTime){
				Trigger.New[0].addError('Portal user already exists with this email. Please select Choose Existing Contact');
			}
		}
	}
}

}