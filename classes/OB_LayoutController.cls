/*
 * Description : This controller is used to show the entity name in the home page of community.
 *
 * ****************************************************************************************
 * History :
 * [31.OCT.2019] Prateek Kadkol - Code Creation
 * [24.APRL.2020] Prateek Kadkol - CP Logic
 */
public without sharing class OB_LayoutController 
{
    @AuraEnabled
    public static User getUserInfo() {
        user loggedInUser = new user();
        /* for(User usr :[select id, Contact.Account.Name, Contact.Account.ROC_Status__c FROM User Where Id = :userInfo.getUserId() and ContactId != null]) {
            loggedInUser = usr;
        } */
        for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) 
        {
            if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                loggedInUser = usrObj;
            }
        }
        return loggedInUser;
    }


    

    @AuraEnabled
    public static RespondWrap difcTabsGetInfo(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        
        user loggedInUser = new user();
        for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) 
        {
            if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                loggedInUser = usrObj;
                respWrap.loggedInUser = usrObj;
            }
        }
        respWrap.isFitOut = OB_AgentEntityUtils.hasSpecifiedRole(loggedInUser.ContactId,loggedInUser.contact.AccountId,'Fit-Out Services');

        return respWrap;
    }

    @AuraEnabled
    public static RespondWrap getRelation()  {
    
        //declaration of wrapper
        RespondWrap respWrap =  new RespondWrap();
        
        
        user loggedInUser = new user();
        for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) 
        {
            if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                loggedInUser = usrObj;
                respWrap.loggedInUser =usrObj;
                
            }
        }

        if(loggedInUser.ContactId != null && loggedInUser.contact.AccountId != null){
            for(AccountContactRelation relObj : [SELECT id , Roles, isActive FROM AccountContactRelation WHERE AccountId =:loggedInUser.contact.AccountId
             AND ContactId =:loggedInUser.ContactId   ]){
               
                respWrap.relObj = relObj;
            }
        }

        

        return respWrap;
    }
        


    
    @AuraEnabled
    public static RespondWrap fetchPageFlowWrapper(string requestWrapParam) {
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap = new RespondWrap();
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        if(reqWrap.pageflowId != null) {
            for(HexaBPM__Page_Flow__c pgf :[select id, name, HexaBPM__Flow_Description__c FROM HexaBPM__Page_Flow__c Where id = :reqWrap.pageflowId LIMIT 1]) {
                respWrap.currentPageFlowObj = pgf;
            }
        }
        if(reqWrap.srId != null) {
            string pageTracker = '';
            for(HexaBPM__Service_Request__c objSR :[SELECT Id, Progress_Completion__c, Page_Tracker__c FROM HexaBPM__Service_Request__c WHERE Id = :reqWrap.srId]) {
                /*     if(objSR.Progress_Completion__c <= 100) {
                 respWrap.progressCompletion = objSR.Progress_Completion__c;
                 } else {
                 respWrap.progressCompletion = 100;
                 } */
                pageTracker = objSR.Page_Tracker__c;
            }
            if(pageTracker != null) {
                decimal pageFilledPersentage = 0;
                list<string> pageTrackerList = pageTracker.split(',');

                for(HexaBPM__Page__c pagesObj :[SELECT id, Completion_Weightage__c from HexaBPM__Page__c where id IN :pageTrackerList]) {
                    if(pagesObj.Completion_Weightage__c != null) {
                        pageFilledPersentage += pagesObj.Completion_Weightage__c;
                    }

                }
                if(pageFilledPersentage <= 100) {
                    respWrap.progressCompletion = pageFilledPersentage;
                } else {
                    respWrap.progressCompletion = 100;
                }
            } else {
                respWrap.progressCompletion = 0;
            }
        }
        return respWrap;
    }

    public class RequestWrap {
        @AuraEnabled
        public String pageflowId { get; set; }
        @AuraEnabled
        public String srId { get; set; }
        public RequestWrap() {
        }
    }
    public class RespondWrap {
        @AuraEnabled
        public HexaBPM__Page_Flow__c currentPageFlowObj { get; set; }
        @AuraEnabled
        public AccountContactRelation relObj { get; set; }
        @AuraEnabled
        public user loggedInUser { get; set; }
        @AuraEnabled
        public boolean isFitOut;
        @AuraEnabled
        public Decimal progressCompletion { get; set; }
        public RespondWrap() {
            progressCompletion = 0;
        }
    }

    
}