/*********************************************************************************************************************
*  Author   : Shikha
*  Date     : 04/08/2021
*  Description : Generic class to list all the methods to bypass sharing settings
**********************************************************************************************************************/
public without sharing class DIFC_SharingBypass {
	
    public static List<UBO_Secure__c> fetchUBOSecureRecords(String bpNo){
        List<UBO_Secure__c> uboSecureRecords = [SELECT Id, BP_no__c FROM UBO_Secure__c WHERE BP_no__c =:bpNo];
        return uboSecureRecords;
    }
}