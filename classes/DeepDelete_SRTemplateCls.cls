global with sharing class DeepDelete_SRTemplateCls {
	webservice static string DeepDelete(String SRTemplateId){
		system.debug('SRTemplateId==>'+SRTemplateId);
		list<SR_Steps__c> lstSRSteps = [select id from SR_Steps__c where SR_Template__c=:SRTemplateId]; 
		list<SR_Template_Item__c> lstTemplItem = [select id from SR_Template_Item__c where SR_Template__c=:SRTemplateId];
		list<SR_Template_Docs__c> lstTemplDocs = [select id from SR_Template_Docs__c where SR_Template__c=:SRTemplateId];
		set<id> setSRStepIds = new set<id>();
		set<id> setBRIds = new set<id>();
		set<id> setTemplateDocs = new set<id>();
		if(lstTemplDocs!=null && lstTemplDocs.size()>0){
			for(SR_Template_Docs__c TemplDoc:lstTemplDocs){
				setTemplateDocs.add(TemplDoc.Id);
			}
		}
		if(lstSRSteps!=null && lstSRSteps.size()>0){
			for(SR_Steps__c srstp:lstSRSteps){
				setSRStepIds.add(srstp.id);
			}
		}
		if(setTemplateDocs!=null && setTemplateDocs.size()>0){
			list<Condition__c> lstCond = [select id from Condition__c where SR_Template_Docs__c IN:setTemplateDocs];
			if(lstCond!=null && lstCond.size()>0){
				try{
					delete lstCond;
					delete lstTemplDocs;
				}catch(Exception e){
					
				}
			}
		}
		if(setSRStepIds!=null && setSRStepIds.size()>0){
			list<Business_Rule__c> lstBrs = new list<Business_Rule__c>();
			for(Business_Rule__c br:[select id from business_rule__c where SR_Steps__c IN:setSRStepIds]){
				lstBrs.add(br);
				setBRIds.add(br.id);
			}
			list<Step_Transition__c> lstStpTransitions = [select id from Step_Transition__c where SR_Step__c IN:setSRStepIds];
			if(lstStpTransitions!=null && lstStpTransitions.size()>0){
				delete lstStpTransitions;
			}
			if(setBRIds!=null && setBRIds.size()>0){
				list<Action__c> lstActions = [select id from Action__c where Business_rule__c IN:setBRIds];
				list<Condition__c> lstCond = [select id from Condition__c where Business_rule__c IN:setBRIds];
				if(lstCond!=null && lstCond.size()>0){
					delete lstCond;
				}
				if(lstActions!=null && lstActions.size()>0){
					delete lstActions;
				}
				delete lstBrs;
			}
			delete lstSRSteps;
		}
		if(lstTemplItem!=null && lstTemplItem.size()>0){
			delete lstTemplItem;
		}
		list<SR_Template__c> lstStrTempl = [select id from SR_Template__c where id=:SRTemplateId];
		if(lstStrTempl!=null && lstStrTempl.size()>0){
			try{
				delete lstStrTempl;
			}catch(Exception e){
				
			}
		}
		return 'Success';
	}
}