/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_cls_DocumentAuthentication {
	
	/**
     * Initializes the test data for the entire test class
     * @author		Claude Manahan
     * @date		6-6-2016
     */
    @testSetup static void setTestData() {
    	
    	//Test_FitoutServiceRequestUtils.init();
    	Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
		
		insert objAccount;
		
		Contact objContact = Test_CC_FitOutCustomCode_DataFactory.getTestContact(objAccount.Id);
        
        insert objContact;
		
		System.runAs(new User(Id = Userinfo.getUserId(), ContactId = objContact.Id)){
			
			Account objContractor = Test_CC_FitOutCustomCode_DataFactory.getTestContractor();
			Account objContractor2 = Test_CC_FitOutCustomCode_DataFactory.getTestContractor();
			
			objContractor2.Name = 'Test Empty Violations COntractor';
			objContractor2.Bp_No__c = 'BP12345678';
			
			List<Lookup__c> testLookups = Test_CC_FitOutCustomCode_DataFactory.getTestLookupRecords();
			List<SR_Status__c> testSRStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSrStatuses();
			List<CountryCodes__c> countryCodesTestList = Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
			
			insert new List<Account>{objContractor,objContractor2};
			insert countryCodesTestList;
			insert testLookups;
		}
	}

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Loop__DDP__c testDdp = new Loop__DDP__c();

	    testDdp.Name = 'Test DDP';
	    testDdp.Loop__Type__c = 'Letter';
	    testDdp.Loop__Output_Filename__c = 'Test Letter';
	    
	    insert testDdp;
	    
	    Loop__DDP_Integration_Option__c testTemplate = new Loop__DDP_Integration_Option__c();
	    
	    testTemplate.Name = 'Test TEmplate';
	    testTemplate.Loop__Output__c = 'PDF';
	    testTemplate.Loop__Attach_As__c = 'Attachment';
	    testTemplate.Loop__DDP__c = testDdp.Id;
	    
	    insert testTemplate;
	    
	    /* Create a Document master */
	    Document_Master__c testDocumentMaster = new Document_Master__c();
	    
	    testDocumentMaster.Name = 'Test Document Master';
	    testDocumentMaster.Description__c  = 'Test Document Master';
	    testDocumentMaster.DMS_Document_Index__c = '1';
	    testDocumentMaster.Doc_ID__c = '123456789012345678';
	    testDocumentMaster.DMS_Document_Section__c = 'Test';
	    testDocumentMaster.DMS_Document_Category__c = 'Test';
	    testDocumentMaster.LetterTemplate__c = testTemplate.Id;
	    
	    insert testDocumentMaster;
	    
	    SR_Template__c testFitoutTemplate 
				= Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Test','Test','Fit-Out & Events');
				
		insert testFitoutTemplate; // Create an event site visit report template
	    
	    /* Create an SR Template Doc */
	    SR_Template_Docs__c testSRTemplateDoc = new SR_Template_Docs__c();
	    
	    testSRTemplateDoc.SR_Template__c = testFitoutTemplate.Id;
	    testSRTemplateDoc.Document_Master__c = testDocumentMaster.Id;
	    testSRTemplateDoc.Added_through_Code__c = false;
	    testSrtemplateDoc.Requirement__c = 'Copy Required';
	    //testSrtemplateDoc.SR_Template_Docs_Condition__c = 'Account->Company_Type__c#=#Financial - related#AND#Service_Request__c->Legal_Structures__c#!=#LTD IC';
	    testSrtemplateDoc.SR_Template_Docs_Condition__c = '';
	    testSrtemplateDoc.Conditions__c = '1 AND 2';
	    testSrTemplateDoc.Generate_Document__c = true;
	    testSrTemplateDoc.Document_Description_External__c = 'Test';
	    testSrtemplateDoc.Group_No__c = 1;
	    testSrtemplateDoc.Document_Name_for_SR__c = 'Test 1';
	    testSrTemplateDoc.On_Submit__c = false;
	    
	    insert testSrTemplateDoc;
	    
        Service_Request__c testSr = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        
        insert testSr;
        
        SR_Doc__c testFineGeneratedSRDoc 
				= Test_CC_FitOutCustomCode_DataFactory.getTestSrDoc('Fine Generated/Signed Document',testSr.Id);
				
		testFineGeneratedSRDoc.SR_Template_Doc__c = testSRTemplateDoc.Id;
    
    	insert testFineGeneratedSRDoc;
    	
    	Attachment testAttachment = new Attachment();
    	
    	testAttachment.ContentType = 'application/pdf';
    	testAttachment.body = Blob.valueOf('--some random text--');
    	testAttachment.ParentId = testFineGeneratedSRDoc.Id;
    	testAttachment.Name = 'random_file.pdf';
    	
    	insert testAttachment;
    	
    	Test.startTest();
    	
    	String srDocNumber = [SELECT Document_No__c FROM SR_Doc__c WHERE Id = :testFineGeneratedSRDoc.Id].Document_No__c;
    	
    	/* Set Page */
		Test.setCurrentPage(Page.DocumentAuthentication);
		
		/* Set Page Parameters */
		ApexPages.CurrentPage().GetParameters().put('ref',srDocNumber);
    	
    	Cls_DocumentAuthentication docAuthInst = new Cls_DocumentAuthentication();
    	
    	System.debug(docAuthInst.AttachValBase64 );
    	System.debug(docAuthInst.AttachBlobVal);
    	System.debug(docAuthInst.AtchContentType);
    	System.debug(docAuthInst.searchDoc);
    	
    	Test.stopTest();
    	    
    }
    
}