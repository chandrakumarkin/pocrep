/**
 * @description       : 
 * @author            : Veera
 * @group             : 
 * @last modified on  : 31-05-2021
 * @last modified by  : Veera
 * Modifications Log 
 * Ver   Date          Author                               Modification
 * 1.0  31-05-2021     Veera                                 Initial Version
 **/

public without sharing class GSGDRFARemovePending {

  @InvocableMethod(Label = 'GSGDRFARemovePending' description = 'GSGDRFARemovePending')
  public static void GSGDRFARemovePendingMethod(List < id > ids) {

    List < step__c > stepList = new List < step__c > ();

    List < string > stepNames = new List < string > {
      'Cancellation Form Typed',
      'Entry Permit Form is Typed',
      'Online Entry Permit is Typed',
      'Visa Stamping Form is Typed',
      'Renewal Form is Typed'
    };

    for (step__C step: [select id, Step_Name__c, SR_Record_Type_Text__c, Step_Status__c, SR_verified_by_the_Typist__c,sr__r.Reason_for_Cancellations__c from step__c where SR__C =: ids[0] and Step_Status__c != 'closed'
        and step_name__C in: stepNames
      ]) {

      system.debug('====================' + step);

      if (step.SR_Record_Type_Text__c == 'DIFC_Sponsorship_Visa_New') {
        if (step.Step_Name__c == 'Entry Permit Form is Typed' || step.Step_Name__c == 'Online Entry Permit is Typed') {

          if ((step.Step_Status__c == 'Awaiting Review' || step.Step_Status__c == 'Repush to GDRFA') && step.SR_verified_by_the_Typist__c) {

            Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
            WebServiceGdrfaDocumentCompressClass.GDRFAImageCompressorCallout(ids[0]);
            WebServIntegrationGdrfaEntryPermClass.GDRFAEntryPermitAppCreate(ids[0], StatusReference.Id);
          }

        } else if (step.Step_Name__c == 'Visa Stamping Form is Typed') {

          if ((step.Step_Status__c == 'Awaiting Review' || step.Step_Status__c == 'Repush to GDRFA') && step.SR_verified_by_the_Typist__c) {

            Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
            WebServIntegrationGdrfaSubResClass.GDRFASubmitResidenceCallout(ids[0], StatusReference.Id);
          }
        }

      }

      if (step.SR_Record_Type_Text__c == 'DIFC_Sponsorship_Visa_Renewal') {

        if (step.Step_Name__c == 'Renewal Form is Typed') {

          if (step.Step_Status__c == 'Awaiting Review' || step.Step_Status__c == 'Repush to GDRFA') {
            Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
            //WebServiceGdrfaDocumentCompressClass.GDRFAImageCompressorCallout(ids[0]);
            WebServIntegrationGdrfaResRenewalClass.GDRFARenewResidenceCallout(ids[0], StatusReference.Id);
          }

        }

      }

      if (step.SR_Record_Type_Text__c == 'DIFC_Sponsorship_Visa_Cancellation') {

        if (step.Step_Name__c == 'Cancellation Form Typed' && step.sr__r.Reason_for_Cancellations__c !='Medically unfit') {
      
          if (step.Step_Status__c == 'Awaiting Review' || step.Step_Status__c == 'Repush to GDRFA') {
            Status__c StatusReference = [SELECT Id FROM Status__c WHERE Code__c = 'DNRD_GS_Review'];
            WebServiceGdrfaDocumentCompressClass.GDRFAImageCompressorCallout(ids[0]);
            WebServIntegrationGdrfaSubCancelClass.GDRFASubmitCancelCallout(ids[0], StatusReference.Id);
          }

        }
      }

    }

  }

}