/*
        Author      :   Arun 
        Description :   Class to suport Objection requests 
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    29-06-2020 Arun Singh  Created
    
*/    

public with sharing class cls_PSA_Termination_Objection {

public Service_Request__c SRData{get;set;}
public Account ObjAccount{get;set;}
public boolean Isvalid{get;set;}
Step__c ObjStepToClose;

public string RequestName{get;set;}
    public cls_PSA_Termination_Objection(ApexPages.StandardController controller) 
    {
      List<String> fields = new List<String> {'Type_of_Request__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        SRData=(Service_Request__c )controller.getRecord();
        
      map<string,string> mapParameters;
      string RecordTypeId;
      Isvalid=true;
       mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();              
       
           RecordTypeId=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('PSA_Termination_Objection').getRecordTypeId();
           
       
      
         for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                             Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                             Contact.Account.Qualifying_Type__c,Contact.Account.Qualifying_Purpose_Type__c,Contact.Account.Contact_Details_Provided__c,
                             Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  
                             from User where Id=:userinfo.getUserId() and AccountId!=null])
            {
              
             if(SRData.id==null)
             {
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                SRData.Expiry_Date__c=objUsr.Contact.Account.Next_Renewal_Date__c;
                ObjAccount= objUsr.Contact.Account;
                SRData.Would_you_like_to_opt_our_free_couri__c ='No';
                
                SRData.Type_of_Request__c=mapParameters.get('Request');
                
                //List<Service_Request__c> ListSr=[select id from Service_Request__c where Customer__c=:SRData.Customer__c and Completed_Date_Time__c !=null and  RecordType.DeveloperName='PSA_Termination' order by Completed_Date_Time__c DESC ];
                
                List<Step__c> ListStep=[select SR__c,Status__c,id,Name,Closed_Date_Time__c from Step__c where id=:mapParameters.get('stp') and Closed_Date_Time__c=null];
                if(!ListStep.isEmpty())
                {
                    
                    SRData.Linked_SR__c=ListStep[0].SR__c;
                    ObjStepToClose=ListStep[0];
                    ObjStepToClose.Status__c=[select id,Name from Status__c where Code__c='COMPLETED'].id;
                    ObjStepToClose.Closed_Date_Time__c=datetime.now();
                }
                
                else
                Isvalid=false;

              }
            }
     

        RequestName=SRData.Type_of_Request__c;
                
                
    }

public  PageReference SaveRecord()
{
    
      try     
        {  
            upsert SRData;
            if(ObjStepToClose!=null)
            update ObjStepToClose;
            PageReference acctPage = new ApexPages.StandardController(SRData).view();        
            acctPage.setRedirect(true);        
            return acctPage;     
        }catch (Exception e)     
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
            ApexPages.addMessage(myMsg);    
        }  
         return null;     
        
}
    
    

}