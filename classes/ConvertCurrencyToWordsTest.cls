@isTest
Public class ConvertCurrencyToWordsTest
{
    static testmethod  void ConvertCurrencyToWordsTest()
    {
        ConvertCurrencyToWords ctw = new ConvertCurrencyToWords();
        ctw.NumberToConv  = 10000.20;        
        
        system.assertEquals('Ten Thousand dirhams and Twenty fils',ctw.getWord());
        
        ctw.NumberToConv  = 1.20;
         system.assertEquals('One dirhams and Twenty fils',ctw.getWord());
         
         
         ctw.NumberToConv  = 1000000.02;
         system.assertEquals('One Million dirhams and Two fils',ctw.getWord());
         
         ctw.NumberToConv  = 100;
        // system.assertEquals('One Hundred dirhams',ctw.getWord());
        
        ctw.NumberToConv  = 10;
        system.assertEquals('Ten dirhams',ctw.getWord());
        
        ctw.NumberToConv  = 1000000000000000.00;
         system.assertEquals('-Not Defined- dirhams',ctw.getWord());
        
        
        
        
        
        
        
    }

}