/*
        Author      :   Shabbir
        Description :   This class is used to create tenancies , lease partner and operating location 
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    29-08-2018  Shabbir         Created
    V1.1    07-07-2020  Arun            Replaced Available_for_CoWorking__c  with unit type 
*/    
public without sharing class OperateLeaseCreateRecordCls {
    
    /**
        * Creates tenancies , lease partner and operating location once condition met in SF
        * account records
        * @params      ids                 List of Lease Ids
    */
    
    list<Id> listLeaseIds = new list<Id>();

    @InvocableMethod(label='Create Records from Lease' description='Creates tenancies , lease partner and operating location once condition met in SF')
    public static void createRecordsfromLease(list<Id> listLeaseIds){
      pushRecordsfromLease(listLeaseIds);
    }
    
    @future(callout=true)
    public static void pushRecordsfromLease(list<Id> listLeaseIds){ 
    list<Tenancy__c> tenncylist = new list<Tenancy__c>();
    list<Lease_Partner__c> lPList = new list<Lease_Partner__c>();
    
   // list<Unit__c> unitlist = [select id,Name,Unit_Square_Feet__c from Unit__c where Available_for_CoWorking__c = True];
    

    list<string> LeaseTypes=new list<string> ();
    
    Map<id,Lease__c> Maplspartner = new Map<id,Lease__c>([select id,Name,Account__r.Id, Account__r.BP_No__c,Start_Date__c,End_Date__c,Square_Feet__c,Lease_Types__c,SAP_Lease_Number__c,Type__c from Lease__c where Id IN:listLeaseIds]);
    
    //Get lease types 
     for(Lease__c lease :Maplspartner.values())
     {
         if(lease.Lease_Types__c!=null)
         LeaseTypes.add(lease.Lease_Types__c);
     }
     
     //get units based on select lease type 
     Map<string,list<Unit__c>> MapUnits=new Map<string,list<Unit__c>>();
     for(Unit__c ObjUnit:[select id,Co_Working_Type__c,Name,Unit_Square_Feet__c from Unit__c where Co_Working_Type__c in :  LeaseTypes])
     {
         list<Unit__c> TempUnit=new  list<Unit__c>();
         if(MapUnits.ContainsKey(ObjUnit.Co_Working_Type__c))
         {
             TempUnit=MapUnits.get(ObjUnit.Co_Working_Type__c);
         }
         
         TempUnit.add(ObjUnit);
         MapUnits.put(ObjUnit.Co_Working_Type__c,TempUnit);
             
     }
     
    //Create Linked records 
        for(Lease__c lease :Maplspartner.values()){
            
            for(Unit__c unit : MapUnits.get(lease.Lease_Types__c)){
                
                // Tenancy creation
                Tenancy__c tn = new Tenancy__c();
                tn.Unit__c = unit.id;
                tn.Start_Date__c = lease.Start_Date__c;
                tn.End_Date__c = lease.End_Date__c;
                tn.Lease__c = lease.Id;
                tn.SAP_Tenancy_No__c = lease.Name + unit.Name;
                tn.Square_Feet__c = unit.Unit_Square_Feet__c;
                tenncylist.add(tn);
           system.debug('tenncylist'+tenncylist);     
                //Lease Partner Creation
                Lease_Partner__c lp = new Lease_Partner__c();
                lp.Name = unit.Name;
                lp.Unit__c = unit.id;
                lp.Account__c = lease.Account__r.id;
                lp.Lease__c = lease.Id;
                lp.Unique_Number__c = lease.Name + unit.Name + lease.Account__r.BP_No__c;
                lp.SAP_PARTNER__c = lease.Account__r.BP_No__c;
                lp.Square_Feet__c = lease.Square_Feet__c;
                lp.Lease_Types__c = lease.Lease_Types__c;
                lp.Start_date__c = lease.Start_date__c;
                lp.End_Date__c = lease.End_Date__c;
               // lp.SAP_Lease_Number__c = lease.SAP_Lease_Number__c;
                lp.Type_of_Lease__c = lease.Type__c;
                lPList.add(lp);
           system.debug('lPList'+lPList);                
            }
        
        }
           

  try{
      if(tenncylist.size()>0){
           upsert tenncylist SAP_Tenancy_No__c;
         }
      
      if(lPList.size()>0){
          upsert lPList Unique_Number__c;
         }
      
     }      
    catch(Exception e) {
       insert LogDetails.CreateLog(null, 'OperateLeaseCreateRecordCls/pushRecordsfromLease', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
         }
           
    }     
    
 }