/**********************************************************************************************************************
    Author      :   Syeda Firdous Fatima
    Class Name  :   delayedMedicalResults
    Description :   This class is extension for the DelayedMedicalResults page used by DHA team.
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    17-12-2019  Syeda Firdous Created
    
**********************************************************************************************************************/

public class delayedMedicalResults{
    @testvisible
    List<Appointment__c> PendingMedicalResultsList;
    public List<Appointment__c> delayedMedicalResultsList {get; set;}
    public boolean showErrorOnly {get; set;}
    
     List<String> SrStatus = new List<string>();
     set<string> ExcludeSRStatus = new set<string>();
    
//  {'The applicant is medically unfit','Cancelled','Process Completed','Cancellation Approved'};
    
    public delayedMedicalResults(ApexPages.StandardController std){
        delayedMedicalResultsList = new List<Appointment__c>();
        PendingMedicalResultsList = new List<Appointment__c>();
        
            SrStatus = Label.Exclude_Delayed_Medical_Results.split(',');
            ExcludeSRStatus.addall(SrStatus);
            
            
        // Needs to change below list of delayedMedicalResultsList  to PendingMedicalResultsList after calculation
        PendingMedicalResultsList = [select id, Name, DHA_Reference_Number__c, Step__r.Medical_Fitness__c,
                                    Service_Request__c, Service_Request__r.Name, Step__c, Service_Request__r.Customer__c, Service_Request__r.Customer__r.Name, Applicant_s_Name__c, 
                                    Medical_Process_Completed__c, Service_Type__c, Service_Request__r.SR_Menu_Text__c, SR_Status__c, Step_Status__c, 
                                    Step__r.Medical_Completed_On__c, Step__r.Completed_hours_by_WF__c, Appointment_Details__c,
                                    Step__r.Second_Medical_Done__c, Step__r.Third_Medical_Done__c,Step__r.Status_Code__c
                                    from Appointment__c
                                    where (Step__r.Step_Name__c ='Medical Fitness Test Scheduled' 
                                    and SR_Status__c not In :ExcludeSRStatus 
                                    /*(SR_Status__c = 'Medical Fitness Test Is Scheduled' OR SR_Status__c = 'Application is pending')*/
                                    and (Step_Status__c = 'Re-Scheduled' or Step_Status__c = 'Scheduled' )
                                    and Medical_Process_Completed__c = true                                    
                                    and Step__r.Application_Reference__c != NULL
                                    and Step__r.Medical_Completed_On__c != NULL
                                    and Step__r.Medical_Fitness__c != 'Yes'
                                    and Step__r.Medical_Fitness__c != 'No'                                    
                                    )                                  
                                    ];
                                        
        if(AppointmentDetails()){                                                
            CompletedHoursCalculation();
        }
    
    }
    @testvisible
    void CompletedHoursCalculation(){
        if(PendingMedicalResultsList.size() > 0){
         ID BHid = id.valueOf(Label.Business_Hours_Id); // fetching Business Hours Id based on Label
         // ID businessHoursId = getBusinessHoursId();
         string businessHourName = 'GS';
         ID businessHoursId = String.isNotBlank(businessHourName) && Business_Hours__c.getAll().containsKey(businessHourName) ?  Id.valueOf(Business_Hours__c.getInstance(businessHourName).Business_Hours_Id__c) : Id.ValueOf( [SELECT Id FROM BusinessHours WHERE Name =: businessHourName limit 1].Id);
         BHId = businessHoursId ; // String.isBlank(businessHoursId) ? id.valueOf(Label.Business_Hours_Id) : businessHoursId;
         Decimal businessHourDecimal;
         Decimal CompleteDurationDifference;
         
         long BUSINESS_HOUR_MILLISECONDS = 3600000; 
         
         
           for(Appointment__c apt : PendingMedicalResultsList){           
               if(apt.Step__r.Medical_Completed_On__c != null && apt.Medical_Process_Completed__c == true){
                    
              
              /* To get the exact hours with floating values, 0.0 is added */
                    businessHourDecimal = BUSINESS_HOUR_MILLISECONDS + 0.0;
                    CompleteDurationDifference = BusinessHours.diff(BHId,apt.Step__r.Medical_Completed_On__c,System.Now()) + 0.0;
                    CompleteDurationDifference = CompleteDurationDifference != null ? (CompleteDurationDifference /businessHourDecimal).setScale(2) : 0.0;
                    apt.Step__r.Completed_hours_by_WF__c = CompleteDurationDifference ;                     
                    if(CompleteDurationDifference != 0 && CompleteDurationDifference != null){
                    if(apt.Service_Type__c == 'Normal' && CompleteDurationDifference >= 8.0){ delayedMedicalResultsList.add(apt);}
                       if(apt.Service_Type__c == 'Express' && CompleteDurationDifference >= 4.0){delayedMedicalResultsList.add(apt);}
                        
                    }
                    
               }
           } 
        }
    }
    
    boolean AppointmentDetails(){
        
        //Getting use information from labels
        Id CurrentUserId = UserInfo.getUserId();
        Id GroupId = Id.ValueOf( Label.DHA_Team_Public_Group );
        
        // Checking membership of the user from the group.
        List<GroupMember> ListGroupMember = [SELECT Id, Group.Name, Group.DeveloperName FROM GroupMember WHERE UserOrGroupId =: CurrentUserId AND GroupId =: GroupId];
        
        // if user is part of the Group show the list view else show the message.
        if( ListGroupMember.size() > 0 && !test.isRunningTest()){ return true;}
        
        
        else{
            // ShowMessage = true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.DHA_Team_Message ));
            showErrorOnly = true;
            return false;
        }
    }
 
        
}