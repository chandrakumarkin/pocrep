/*********************************************************************************************************************
 *  Name        : CC_RefundAmount
 *  Author      : Shikha Khanuja
 *  Company     : DIFC
 *  Date        : 18-04-2021  
 * *******************************************************************************************************************/
global without sharing class CC_RefundAmount implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        
        string strResult = 'Success';
        list<HexaBPM__SR_Price_Item__c> lstItems = new list<HexaBPM__SR_Price_Item__c>();
        Decimal PortalBalance = 0;
        String accountId = '';
        for(HexaBPM__SR_Price_Item__c objItem : [select Id,HexaBPM__Status__c, HexaBPM__ServiceRequest__r.HexaBPM__Customer__c, Total_Price_AED__c from HexaBPM__SR_Price_Item__c where HexaBPM__ServiceRequest__c =: stp.HexaBPM__SR__c]){
            accountId = objItem.HexaBPM__ServiceRequest__r.HexaBPM__Customer__c;
            objItem.HexaBPM__Status__c = 'Cancelled';
            PortalBalance = PortalBalance + objItem.Total_Price_AED__c;
            lstItems.add(objItem);
        }
        system.debug('### lstItems '+lstItems);
        if(!lstItems.isEmpty())
            update lstItems;
        if(accountId != null){
   
        }
        return strResult;
    }
}