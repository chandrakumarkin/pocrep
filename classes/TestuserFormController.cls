/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 11-Sep-2014   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class TestuserFormController {

 public static Attachment testPassport;
 static Boolean flag;
 static String str;


 //static Service_Request__c sUsr = new Service_Request__c();
 

    public static testmethod void test() {
        
        String contInd;
        String conRT1;
        PageReference pageRef = Page.User_Access_Form;
        Account acc = new Account(Name = 'TestAccount',BP_No__c='0000008978');
        insert acc;
        SR_Status__c srStatus = new SR_Status__c(name='Submitted',Code__c='SUBMITTED');
        SR_Template__c srTemp = new SR_Template__c(name = 'User Access Form',SR_RecordType_API_Name__c='User Access Form');
        Lookup__c nat = new Lookup__c(Type__c='Nationality',Name='United Arab Emirates');
        License__c lic = new License__c(status__c='Active',Account__c=acc.id);
  
        insert srTemp;
        insert srStatus;
        insert nat;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        License__c lic123 = new License__c(status__c='Active',Account__c=acc.id);
        lic123.Name = '001';
        for(RecordType conInd : [select Id,Name,DeveloperName from RecordType where DeveloperName='Individual' AND SobjectType='Contact'])
                   contInd = conInd.Id;
      for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true])
                  conRT1 = objRT.Id;
        list<Contact> listcon = new list<Contact>();
        Contact cont = new Contact(firstname='Tim',lastname='Khan',accountId=acc.id,Email='test10202020220@test1212121212.com',RecordTypeId=conRT1,Passport_No__c='ASDD2322');
        Contact cont1 = new Contact(firstname='Timmy',lastname='John',Passport_No__c='AD00023',accountId=acc.id,Email='test12356@test23434.com',RecordTypeId=contInd);
        Contact cont2 = new Contact(firstname='Timmy',lastname='John',Passport_No__c='ASD00023',accountId=acc.id,Email='test126@test234234.com',RecordTypeId=contInd);
       // Contact cont3 = new Contact(firstname='Timmy',lastname='John',Passport_No__c='ASD00023',accountId=acc.id,Email='test126@test234234.com',RecordTypeId=conRT1);
        insert lic123;
        listcon.add(cont);
        listcon.add(cont1);
        listcon.add(cont2);
        insert listcon;
        //insert cont1;
        list<Relationship__c> lstrel = new list<Relationship__c>();
        Relationship__c RelUser = new Relationship__c();
        RelUser.Object_Contact__c = cont.id;
        RelUser.Subject_Account__c = acc.id;
        RelUser.Relationship_Type__c = 'Has Principal User';
        RelUser.Start_Date__c = Date.today(); 
        RelUser.Push_to_SAP__c = true;
        RelUser.Active__c = true;
        lstrel.add(RelUser);
        Relationship__c RelUsr1 = new Relationship__c();
        RelUsr1.Object_Contact__c = cont1.id;
        RelUsr1.Subject_Account__c = acc.id;
        RelUsr1.Relationship_Type__c = 'Has Principal User';
        RelUsr1.Start_Date__c = Date.today(); 
        RelUsr1.Push_to_SAP__c = true;
        RelUsr1.Active__c = true;
        lstrel.add(RelUsr1);
        insert lstrel;
        Service_Request__c SR = new Service_Request__c(Username__c='test@test.com',License_Number__c =lic.id); 
        SR.Customer__c = acc.Id;
        SR.SR_Template__c = srTemp.Id;
        SR.Submitted_DateTime__c = DateTime.now();
        Date subDate = Date.today();
        SR.Submitted_Date__c = subDate;
        SR.Title__c = 'Mrs.';
        SR.First_Name__c = 'Finley';
        SR.Send_SMS_To_Mobile__c = '+971562345234';
        SR.Middle_Name__c = 'Ashton';
        SR.License_Number__c = '001';
        SR.Last_Name__c = 'Kate';
        SR.Position__c = 'Engineer';
        SR.Place_of_Birth__c = 'USA';
        SR.Nationality_list__c  = nat.Name;
        SR.Date_of_Birth__c = Date.newinstance(1980,10,17);
        SR.Email__c = 'test@test.com';
        SR.Roles__c = 'Company Services';
        SR.Nationality__c = nat.id;
        SR.Passport_Number__c = 'ASD00023';
        insert SR;
       
        Attachment PassportCopy=new Attachment();    
        PassportCopy.Name='PassportCopy.png';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        PassportCopy.body=bodyBlob;
        PassportCopy.parentId=SR.Id;
        insert PassportCopy;
       
        List <License__c> tmp = [Select Account__r.Name, Account__c from License__c where name = :SR.License_Number__c];
        List <SR_Status__c> status = [Select Id from SR_Status__c where name = 'Submitted'];
        List <SR_Template__c> template = [Select Id from SR_Template__c where name  = 'User Access Form'];
        List <License__c> lstlic = [Select id,Name from License__c where Id=:lic123.Id];
        string LicenseName = lstlic[0].Name;
        for(Integer l=lstlic[0].Name.length();l<4;l++)
                        LicenseName = '0'+LicenseName;
        
        system.Test.startTest();
        Test.setCurrentPageReference(pageRef);
      
        ApexPages.CurrentPage().getparameters().put('BPno', '08978');
        ApexPages.StandardController sc = new ApexPages.standardController(SR);
        userFormCls userform = new userFormCls(sc);
        
        
        userFormCls userformCls1 = new userFormCls();
        
        //userform.ComName='demo';
        userform.propServices=true;
        
        pageRef = userform.saveUpdateSR();
        userform.PassportCopy = PassportCopy;
        pageRef = userform.getAccount();
        pageRef = userform.saveInfo();
        
        userForm.sUser.License_Number__c = '001';
        
        
        pageRef = userform.saveRevokeSR();
        pageRef = userform.saveUpdateSR();
        userform.compServices = false;
        userform.BPNo = '08978';
        pageRef = userform.saveInfo();
        userform.sUser.License_Number__c = 'as23';
        pageRef = userform.saveInfo();
        //bpno null
        ApexPages.StandardController scc = new ApexPages.standardController(SR);
        userFormCls userformCls2 = new userFormCls(scc);
        userformCls2.isConsent = true;
        userformCls2.compServices=true;
        userformCls2.empServices=false;
        userformCls2.PassportCopy = PassportCopy;
        pageRef = userformCls2.getAccount();
        pageRef = userformCls2.saveInfo();
        userformCls2.sUser.License_Number__c = '001';
        pageRef = userformCls2.saveUpdateSR();
        pageRef = userformCls2.saveRevokeSR();
        //userformCls2.BPNo='';
        SR.License_Number__c='';
        SR.Username__c = '';
        userformCls2.strSelVal='Create New User';
        update SR;
        userformCls2.sUser = new Service_Request__c(Customer__c=acc.Id);
        userformCls2.compServices=false;
        userformCls2.empServices=true;
        userformCls2.sUser.License_Number__c=LicenseName;//lstlic[0].Name;
        userformCls2.sUser.Title__c = 'Mrs.';
        userformCls2.sUser.Nationality_list__c  ='USA';
        userformCls2.sUser.First_Name__c = 'Finley';
        userformCls2.sUser.Send_SMS_To_Mobile__c = '+971562345234';
        userformCls2.sUser.Middle_Name__c = 'Ashton';
        userformCls2.sUser.Last_Name__c = 'Kate';
        userformCls2.sUser.Position__c = 'Engineer';
        userformCls2.sUser.Place_of_Birth__c = 'USA';
        userformCls2.sUser.Nationality_list__c  = nat.Name;
        userformCls2.sUser.Date_of_Birth__c = Date.newinstance(1980,10,17);
        userformCls2.sUser.Email__c = 'test@test.com';
        userformCls2.sUser.Roles__c = 'Company Services';
        userformCls2.sUser.Gender__c = 'Male';
        userformCls2.sUser.Passport_Date_of_Issue__c = system.today() -2;
        userformCls2.sUser.Passport_Expiry_Previous_Sponsor__c = system.today()+365;
        userformCls2.sUser.Passport_Country_of_Issue__c = 'India';
        userformCls2.sUser.Nationality__c = nat.id;
        userformCls2.sUser.Passport_Number__c = 'ASD00023';
        userformCls2.sUser.Username__c = 'saima.hidayat@difcportal2.ae';
        
        //userformCls2.sUser = SR;
        userformCls2.saveInfo();
        SR.Username__c = 'saima.hidayat@difcportal2.ae';
        SR.License_Number__c = lic123.Name;
        update SR;
        
        //ApexPages.CurrentPage().getparameters().put('id', SR.id);      
        
        
        /*system.test.startTest();
        User usr = new User();
        usr.IsActive = true;
        usr.CompanyName = acc.id;
        usr.username = 'saima.hidayat@difcportal.ae';
        usr.contactId=cont2.Id;
    usr.firstname='Test';
    usr.lastname='Test';
    usr.email=SR.Email__c;
    usr.phone = SR.Send_SMS_To_Mobile__c;
    usr.Title = 'Mr.';
        usr.communityNickname = (SR.Last_Name__c +string.valueof(Math.random()).substring(4,9));
    usr.alias = string.valueof(SR.Last_Name__c.substring(0,1) + string.valueof(Math.random()).substring(4,9));            
    usr.profileid = Label.Profile_ID;
    usr.emailencodingkey='UTF-8';
    usr.languagelocalekey='en_US';
    usr.localesidkey='en_GB';
    usr.timezonesidkey='Asia/Dubai';
    usr.Community_User_Role__c = SR.Roles__c;
        insert usr;
        system.test.stopTest();
        userFormCls.updateUser(SR.id);
        Step__c stp  = new Step__c();
        stp.SR__c = SR.id;
        insert stp;
        userFormCls.createUser(stp);
        userFormCls.revokeUser(SR.id);
        */
        
        ApexPages.CurrentPage().getparameters().put('BPno', '08978');
        pageRef = userformCls2.getAccount();
        pageRef = userform.saveInfo();
        pageRef = userform.saveRevokeSR();
        userformCls2.compServices=true;
        userformCls2.empServices=true;
        pageRef = userformCls2.getAccount();
        pageRef = userformCls2.saveInfo();
        userformCls2.disablename1();
        userformCls2.enablename1();
        userformCls2.disableopt();
        userformCls2.enableopt();
        testPassport = userformCls2.getPassportCopy();
        flag = userformCls2.getFlag();
        userform.setFlag(flag);
        flag = userform.getdisplayAcc();
        userform.setdisplayAcc(flag);
        str = userform.getaccountName();
        userform.setaccountName(str);
        pageRef = userformCls2.saveUpdateSR();
        pageRef = userformCls2.saveRevokeSR();
        pageRef = userform.ChangeOption();
        userform.compServices=true;
        userform.empServices=false;
        //userform.sUser.License_Number__c=lstlic[0].Name;
        //userform.sUser.Username__c = '';
        //pageRef = userform.saveInfo();
        userform.compServices=false;
        userform.empServices=true;
        pageRef = userform.getAccount();
        userform.sUser.License_Number__c=lstlic[0].Name;
        userform.getPassportCopy();
        userform.disableopt();
        userform.RePrepare();
        userform.compServices=false;
        userform.empServices=true;
        pageRef = userform.saveInfo();
        userform.compServices=true;
        userform.empServices=true;
        pageRef = userform.getAccount();
       // pageRef = userform.saveInfo();
        //userform.compServices=false;
        //userform.empServices=false;  
        //pageRef = userform.getAccount();
        //pageRef = userform.saveInfo();
        userform.disablename1();
        userform.enablename1();
        userform.disableopt();
        userform.enableopt();
        userform.setFlag(flag);
        flag = userform.getdisplayAcc();
        userform.setdisplayAcc(flag);
        str = userform.getaccountName();
        userform.setaccountName(str);
        ApexPages.CurrentPage().getparameters().put('BPno',null);
        ApexPages.CurrentPage().getparameters().put('id', SR.id); 
        
        pageRef = userform.getAccount();
        pageRef = userform.saveInfo();
        
             
        
        
        
    }
    
    public static testmethod void testUSR(){
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = 'dotnetcodex@gmail.com' + System.now().millisecond() ,
        Alias = 'sfdc',
        Email='dotnetcodex@gmail.com',
        EmailEncodingKey='UTF-8',
        Firstname='Dhanik',
        Lastname='Sahni',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago',
        IsActive = true
        );
        Database.insert(portalAccountOwner1);
        system.runas(portalAccountOwner1){
                PageReference pageRef = Page.User_Access_Form;
                Account acc = new Account(Name = 'TestAccount',BP_No__c='0000008978');
                acc.ROC_Status__c = 'Active';
                insert acc;
                String conRT1;
                for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true])
                          conRT1 = objRT.Id;
                Contact cont = new Contact(firstname='Tim',lastname='Khan',accountId=acc.id,Email='test@test.com',RecordTypeId=conRT1,Passport_No__c='ASD12322');
                insert cont;
                SR_Template__c srTemp = new SR_Template__c(name = 'User Access Form',SR_RecordType_API_Name__c='User Access Form');
                insert srTemp;
               //Lookup__c nat = new Lookup__c(Type__c='Nationality',Name='USA');
               // insert nat;
                License__c lic = new License__c(status__c='Active',Account__c=acc.id);
                lic.Name = '001';
                insert lic;
                CountryCodes__c objCC = new CountryCodes__c();
                objCC.Name = '971';
                insert objCC;
                ApexPages.CurrentPage().getparameters().put('BPno', '08978');
                userFormCls objusr = new userFormCls();
                objusr.compServices=false;
                objusr.empServices=false;  
                pageRef = objusr.getAccount();
                pageRef = objusr.saveInfo();
                objusr.sUser = new Service_Request__c(Customer__c = acc.id);
                objusr.sUser.License_Number__c=lic.Name;
                objusr.sUser.Username__c = '';
                pageRef = objusr.saveInfo();
                objusr.sUser = new Service_Request__c();
                objusr.BPNo ='';
               // objusr.sUser.License_Number__c ='2002';
                objusr.getAccount();
                objusr.saveInfo();
                
                Service_Request__c SR = new Service_Request__c(Username__c='xyz@test.com',License_Number__c =lic.id); 
                SR.Customer__c = acc.Id;
                SR.SR_Template__c = srTemp.Id;
                SR.Submitted_DateTime__c = DateTime.now();
                Date subDate = Date.today();
                SR.Submitted_Date__c = subDate;
                SR.Title__c = 'Mrs.';
                SR.First_Name__c = 'Finley';
                SR.Send_SMS_To_Mobile__c = '+971562345234';
                SR.Middle_Name__c = 'Ashton';
                SR.Last_Name__c = 'Kate';
                SR.Position__c = 'Engineer';
                SR.Place_of_Birth__c = 'USA';
               // SR.Nationality_list__c  = nat.Name;
                SR.Date_of_Birth__c = subDate;
                SR.Email__c = 'test@test.com';
                SR.Roles__c = 'Company Services;Employee Services';
                //SR.Nationality__c = nat.id;
                SR.Passport_Number__c = 'ASD00023';
                SR.License_Number__c = lic.Name;
                SR.Gender__c = 'FeMale';
                SR.Passport_Date_of_Issue__c = system.today()-2;
                SR.Passport_Expiry_Previous_Sponsor__c = system.today()+365;
                SR.Passport_Country_of_Issue__c = 'India';
                insert SR;
             
               list<Relationship__c> lstRel = new list<Relationship__c>();
               
                Contact objContact = new Contact();
                objContact.LastName = 'Last RB';
                objContact.Email = 'xyz@test.com';
                objContact.AccountId = acc.Id;
                objContact.FirstName = 'Nagaboina';
                insert objContact;
                
                Contact objContact1 = new Contact();
                objContact1.LastName = 'Last RB';
                objContact1.Email = 'xyz@test.com';
                objContact1.AccountId = acc.Id;
                objContact1.FirstName = 'Nagaboina';
                insert objContact1;
               
               
                Relationship__c RelUser = new Relationship__c();
                //RelUser.Object_Contact__c = cont.id;
                RelUser.Subject_Account__c = acc.id;
                RelUser.Relationship_Type__c = 'Has Principal User';
                RelUser.Start_Date__c = Date.today(); 
                RelUser.Active__c = true;
                RelUser.Push_to_SAP__c = true;
                lstRel.add(RelUser);
                
                Relationship__c RelUser1 = new Relationship__c();
                RelUser1.Object_Contact__c = objContact.id;
                RelUser1.Subject_Account__c = acc.id;
                RelUser1.Relationship_Type__c = 'GS Portal Contact';
                RelUser1.Start_Date__c = Date.today(); 
                RelUser1.Active__c = true;
                RelUser1.Push_to_SAP__c = true;
                lstRel.add(RelUser1);
                
                insert lstRel;
                objusr.sUser.Date_of_Birth__c = Date.Today().AddYears(-2);
                objusr.saveInfo();
                
                Attachment PassportCopy=new Attachment();    
                PassportCopy.Name='PassportCopy.png';
                Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                PassportCopy.body=bodyBlob;
                PassportCopy.parentId=SR.Id;
                insert PassportCopy;
                
                License__c licNew = new License__c();
                licNew.Account__c = acc.id;
                licNew.Name = '001';
                insert licNew;
                SR.License_Number__c = licNew.name;
                update SR;
                objusr.BPNo = '0000008978';
                objusr.saveInfo();
                objusr.sUser.License_Number__c = '001';
                objusr.BPNo = null;
                objusr.PassportCopy = PassportCopy;
                objusr.sUser.Date_of_Birth__c = Date.Today().AddYears(-26);
                objusr.saveInfo();
                objusr.sUser.Username__c='xyz@test.com';
                objusr.sUser.License_Number__c = '001';
                
                Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
                
                /* Account objAccount = new Account();
                objAccount.Name = 'Test Custoer 1';
                //objAccount.E_mail__c = 'test@test.com';
                objAccount.BP_No__c = '001234';
                objAccount.Company_Type__c = 'Financial - related';
                objAccount.Sector_Classification__c = 'Authorised Market Institution';
                insert objAccount;*/
                
               
                
                 User userObj = new User(Alias = 'tstusr', Email='xyz@test.com', 
                                EmailEncodingKey='UTF-8', FirstName='abc', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactID = objContact.ID, CompanyName = acc.ID,
                                TimeZoneSidKey='America/Los_Angeles', UserName='xyz@test.com',
                                IsActive = true);
                userObj.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services;Employee Services';
                insert userObj;
                
                 User userObj1 = new User(Alias = 'tstusr', Email='xyz@test.com', 
                                EmailEncodingKey='UTF-8', FirstName='abc', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactID = objContact1.ID, CompanyName = acc.ID,
                                TimeZoneSidKey='America/Los_Angeles', UserName='xyz1@test.com',
                                IsActive = true);
                userObj1.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services;Employee Services'; 
                
                insert userObj1;
                
                test.starttest();
                
                
                
                objusr.sUser.ID = NULL;
                
                SR_Status__c srStatus = new SR_Status__c(name='Submitted',Code__c='SUBMITTED');
                insert srStatus;
                objUsr.saveUpdateSR();
                objusr.saveRevokeSR();
              
                Step__c stp  = new Step__c();
                stp.SR__c = SR.id;
                insert stp;
                String str1;
                
                
                str1= userFormCls.createUser(stp);
                userFormCls.updateUser(stp.SR__c);
                userFormCls.revokeUser(stp.SR__c);
                userFormCls.remUser(stp.SR__c);
                test.stoptest();
                }
    }
    
     public static testmethod void testUSR1(){
        
               Account acc = new Account(Name = 'TestAccount',BP_No__c='0000008978');
               acc.ROC_Status__c = 'Active';
               insert acc;
               
                Contact objContact = new Contact();
                objContact.LastName = 'Last RB';
                objContact.AccountId = acc.Id;
                objContact.Email = 'xyz@test.com';
                objContact.AccountId = acc.Id;
                objContact.FirstName = 'Nagaboina';
                insert objContact;
                
                
               Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
         
               User userObj = new User(Alias = 'tstusr', Email='xyz@test.com', 
                                EmailEncodingKey='UTF-8', FirstName='abc', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactID = objContact.ID, CompanyName = acc.ID,
                                TimeZoneSidKey='America/Los_Angeles', UserName='xyz@test.com',
                                IsActive = true);
         
                userObj.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services;Employee Services';
                insert userObj;
                
                test.startTest();
                  userFormCls thisclass = new userFormCls();
                  thisclass.userName = userObj.UserName;
               //   thisclass.populateRelatedAccounts();
                test.stoptest();
             
                }
    
     public static testmethod void testUSR2(){
        
               Account acc = new Account(Name = 'TestAccount',BP_No__c='0000008978');
               acc.ROC_Status__c = 'Active';
               acc.Registration_License_No__c = 'Test';
               insert acc;
               
                Contact objContact = new Contact();
                objContact.LastName = 'Last RB';
                //objContact.AccountId = acc.Id;
                objContact.Email = 'xyz@test.com';
                objContact.AccountId = acc.Id;
                objContact.FirstName = 'Nagaboina';
                insert objContact;
               
               AccountContactRelation acctcr = new AccountContactRelation(Id= [SELECT Id FROM AccountContactRelation WHERE AccountId =: acc.Id LIMIT 1].Id,AccountId = acc.id, ContactId = objContact.id, Roles = 'Super User');
               update acctcr;

   
               
                Relationship__c RelUser = new Relationship__c();
                RelUser.Subject_Account__c = acc.id;
                RelUser.Relationship_Type__c = 'Is Authorized Signatory for';
                RelUser.Start_Date__c = Date.today(); 
                RelUser.Active__c = true;
                RelUser.Push_to_SAP__c = true;
                insert RelUser;
               
               Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
         
               User userObj = new User(Alias = 'tstusr', Email='xyz@test.com',
                                EmailEncodingKey='UTF-8', FirstName='abc', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactID = objContact.ID, CompanyName = acc.ID,
                                TimeZoneSidKey='America/Los_Angeles', UserName='xyz@test.com',
                                IsActive = true);
         
                userObj.Community_User_Role__c = 'Company Services;Property Services;Employee Services';
                insert userObj;
         
                HexaBPM__SR_Template__c newTemplate = new HexaBPM__SR_Template__c();
                newTemplate.Name = 'Super User Authorization';
                newTemplate.HexaBPM__SR_RecordType_API_Name__c ='Superuser_Authorization';
                insert newTemplate;
                
                HexaBPM__SR_Status__c srStatus = new HexaBPM__SR_Status__c();
                srStatus.HexaBPM__Code__c='draft';
                insert srStatus;
                
                  
                HexaBPM__SR_Status__c srStatus2 = new HexaBPM__SR_Status__c();
                srStatus2.HexaBPM__Code__c='Submitted';
                insert srStatus2;
         
                test.startTest();
                  userFormCls thisclass = new userFormCls();
                  thisclass.sUser.License_Number__c ='Test';
              //    thisclass.eachUser = userObj;
                  thisclass.selectedAccount = acc.ID;
                //  thisclass.populateASignatoriesNew();
               //   thisclass.populateASignatories();
         
                 // thisclass.lstWrapper.add(new userFormCls.SuperUserWrapper(RelUser));
         
               /*   for(userFormCls.SuperUserWrapper eachAmendment:thisclass.lstWrapper){
                      eachAmendment.isSelect = true;
                  }*/
                 //   thisclass.getAccount();
                 system.runAs(userObj){
                  thisclass.saveSuperUser();
                 }
                test.stoptest();
             
                }
     public static testmethod void testUSR3(){
        
               Account acc = new Account(Name = 'TestAccount',BP_No__c='0000008978');
               acc.ROC_Status__c = 'Active';
               acc.Registration_License_No__c = 'Test';
               insert acc;
               
                Contact objContact = new Contact();
                objContact.LastName = 'Last RB';
                //objContact.AccountId = acc.Id;
                objContact.Email = 'xyz@test.com';
                objContact.AccountId = acc.Id;
                objContact.FirstName = 'Nagaboina';
                insert objContact;
               
            
               
                Relationship__c RelUser = new Relationship__c();
                RelUser.Subject_Account__c = acc.id;
                RelUser.Relationship_Type__c = 'Is Authorized Signatory for';
                RelUser.Start_Date__c = Date.today(); 
                RelUser.Active__c = true;
                RelUser.Push_to_SAP__c = true;
                insert RelUser;
               
               Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
         
               User userObj = new User(Alias = 'tstusr', Email='xyz@test.com',
                                EmailEncodingKey='UTF-8', FirstName='abc', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactID = objContact.ID, CompanyName = acc.ID,
                                TimeZoneSidKey='America/Los_Angeles', UserName='xyz@test.com',
                                IsActive = true);
         
                userObj.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services;Employee Services';
                insert userObj;
                
                test.startTest();
                  userFormCls thisclass = new userFormCls();
                  thisclass.sUser.License_Number__c ='Test';
                 /* thisclass.eachUser = userObj;
                  thisclass.selectedAccount = acc.ID;
                  thisclass.populateASignatoriesNew();
                  thisclass.populateASignatories();*/
                test.stoptest();
             
                }
}