/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class ContractorBlackPointsUpdateBatch_Test {
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test 
        Id contractorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Contractor_Account').getRecordTypeId();
        Account acc = new Account();
        acc.name = 'test';
        acc.recordTypeId=contractorRecordTypeId;
        insert acc;
        Service_request__c serv = new Service_Request__c(Contractor__c =acc.id,Date_and_Time_of_Work__c=system.Now());
        insert serv;
        Violation__c violation = new Violation__C(Contractor__c = acc.id,Related_Request__c =serv.id);
        insert violation;
        
        Test.startTest();        
        ContractorBlackPointsUpdateBatch blackPointCalculation1 = new ContractorBlackPointsUpdateBatch();
        String sch1 = '20 30 8 10 2 ?';
        String jobID1 = System.schedule('Contractor blackpoints1', sch1, blackPointCalculation1);
        
        Set<Id> contractorIds = new Set<Id>();
        contractorIds.add(acc.Id);
        ContractorBlackPointsUpdateBatch blackPointCalculation2 = new ContractorBlackPointsUpdateBatch(contractorIds);
        String sch2 = '20 30 8 10 2 ?';
        String jobID2 = System.schedule('Contractor blackpoints2', sch2, blackPointCalculation2);
        Test.stopTest();
    }
}