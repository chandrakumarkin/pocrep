/**
    *Author      : Rajil
    *CreatedDate : 10-11-2019
    *Description : This apex class is used to retrieve the QuickLink details - Compliance Calendar to be shown on the Home Page.
    --------------------------------------------------------------------------------------------------------------------------
     Modification History
     ----------------------------------------------------------------------------------------
     V.No   Date            Updated By      Description
     ----------------------------------------------------------------------------------------
     1.0   17 Feb 2020     Leeba            For Main_Entity__c changes on contact
     1.1   04 March 2020   Leeba            For Reverting Main_Entity__c changes on contact
     1.2   20-May-2020     Mudasir          Added the Fitout and Event Manual download links for the contractor and event organizer
     1.3   24-Feb-2021     Shikha           Added logic to fetch legal type of entity Ticket #13241
     1.4   31-May-2021     Arun             Added to pass Account Id 
     ----------------------------------------------------------------------------------------
**/
public without sharing class OB_QuickLinkController {

    @AuraEnabled
    public static string createComplianceCalendar(){
        
        //string CustomerId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        //1.0
        string CustomerId;
        List<User> currentUserLst =  OB_AgentEntityUtils.getUserById(UserInfo.getUserId());
        if( currentUserLst != NULL && currentUserLst.size() > 0)
        {
            //1.1
            if(currentUserLst[0].contactid != NULL && currentUserLst[0].contact.AccountId !=NULL)
            {
                CustomerId = currentUserLst[0].contact.AccountId;
            }
        }
        string conditionText;
        map<string,string> complianceCalendarMap = new map<string,string>();
        for(Compliance_Calendar__mdt calendarSettingRecord : [select Condition__c,Share_with_Link__c from Compliance_Calendar__mdt]){
            if(String.isNotBlank(calendarSettingRecord.Condition__c)){
                //conditionText = calendarSettingRecord.Condition__c.replaceAll( '\\s+', '');
                complianceCalendarMap.put(calendarSettingRecord.Condition__c,calendarSettingRecord.Share_with_Link__c);
            }
        }
        string docLink;
        for(Account acctRecord : [select Legal_Type_of_Entity__c,
                                    License_Activity__c 
                                    from Account where id=:CustomerId]){
               system.debug('!!@##@@'+acctRecord.License_Activity__c+'^^^^'+acctRecord.Legal_Type_of_Entity__c);                        
            if(String.isNotBlank(acctRecord.License_Activity__c)){
                docLink = complianceCalendarMap.get('License_Activity__c = '+acctRecord.License_Activity__c);
            }
            if(String.isBlank(docLink)){
                docLink = complianceCalendarMap.get('Legal_Type_of_Entity__c = '+acctRecord.Legal_Type_of_Entity__c);
            }
        }
        system.debug('!!@##@@'+docLink);
        return docLink;

    }
    @AuraEnabled
    public static Entity viewAccountInfo(){
        Entity entityObj = new Entity();
        string acctId;
        string userRole;
        /*
        for(User usr:[select AccountId,contact.Role__c,Community_User_Role__c from User where id=: Userinfo.getUserId()]){
            acctId = usr.AccountId;
            if(String.isNotBlank(usr.contact.Role__c)){
                userRole = usr.contact.Role__c;
            }else if(String.isNotBlank(usr.Community_User_Role__c)){
                userRole = usr.Community_User_Role__c;
            }
        }
        */
        //1.0
        for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
            //1.1
            entityObj.userObj = usrObj;
            if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                acctId = usrObj.contact.AccountId;
            }
            if(String.isNotBlank(usrObj.contact.Role__c)){
                userRole = usrObj.contact.Role__c;
            }else if(String.isNotBlank(usrObj.Community_User_Role__c)){
                userRole = usrObj.Community_User_Role__c;
            }
            //v1.2
            if(usrObj.ContactId!=null && usrObj.contact.Contractor__c!=null) {
                if(usrObj.contact.Contractor__r.RecordType.DeveloperName=='Contractor_Account'){
                    entityObj.isContractorLogin = true;
                }else if(usrObj.contact.Contractor__r.RecordType.DeveloperName=='Event_Account'){
                    entityObj.isEventOrganizerLogin = true;
                }
            }
        }
        //1.4 
        entityObj.Accountid=acctId;
        entityObj.role = userRole;
        if(String.isNotBlank(userRole) && userRole.contains('Company Services'))
            entityObj.isCompanyService = true;
        //v1.3 
        for(Account account : [select Company_Type__c,GIIN__c,FATCA_Eligibility__c,FATCA_Eligibility_F__c,ROC_Status__c,Legal_Type_of_Entity__c from Account where id = :acctId]){
            entityObj.type = account.Company_Type__c;
            entityObj.rocStatus = account.ROC_Status__c;
            entityObj.fatcaEligibility = account.FATCA_Eligibility_F__c;
            //v1.3 start
            entityObj.showShareCertificate = (account.Legal_Type_of_Entity__c == 'LTD' || account.Legal_Type_of_Entity__c == 'Public Company') ? true : false;
            //v1.3 end
            if(String.isNotBlank(account.GIIN__c))
                entityObj.isGIIN = true;
        }

        return entityObj;
    }
    
     @AuraEnabled
    public static Boolean DFSAPortalDetails(){
        
        Boolean isEmailSent;
        string CustomerId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        List<DFSA_Security_Users__c> lstDFSASecurity = new List<DFSA_Security_Users__c>();
        lstDFSASecurity = [SELECT Id FROM DFSA_Security_Users__c where Customer__c=:CustomerId];
        
        if(lstDFSASecurity.size()>0){
            isEmailSent = true;
            
        }
        else {
            isEmailSent = false;
        }
        return isEmailSent;
    }
    
    public class Entity{
        @AuraEnabled
        public string type {get;set;}
        @AuraEnabled
        public string rocStatus {get;set;}
        @AuraEnabled
        public string role {get;set;}
        @AuraEnabled
        public boolean isGIIN {get;set;}
        @AuraEnabled
        public boolean isCompanyService {get;set;}
        @AuraEnabled
        public boolean fatcaEligibility {get;set;}
        @AuraEnabled
        public boolean isContractorLogin {get;set;}
        @AuraEnabled
        public boolean isEventOrganizerLogin {get;set;}
        //v1.3
        @AuraEnabled
        public boolean showShareCertificate {get;set;}

        @AuraEnabled
        public user userObj {get;set;}
        
        //1.4 
         @AuraEnabled
        public string Accountid{get;set;}

        public Entity(){

            isGIIN = false;
            isCompanyService = false;
            fatcaEligibility= false;
            showShareCertificate = false;//v1.3
        }
    }

}