/*****************************************************************************************************************************
Author      :   Mudasir Wani	
Date        :   08/12/2019
Description :   This class contains the Custom codes for the Government Services processes.
--------------------------------------------------------------------------------------------------------------------------
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date            Updated By      Description
--------------------------------------------------------------------------------------------------------------------------             
V1.1    08/12/2019      Mudasir         Created a new class for the GS related functionalities
*****************************************************************************************************************************/
public class GS_NewHelperClass {
	public static void processActions(Step__c stp){
    	if(stp.step_name__c =='Mobile Number Update'){
    		mobileNumberUpdate(stp.id);
    	}
    	else if(stp.step_name__c =='Original Passport and Entry Permit are Received'){
    		checkIfMobileUpdateStepIsClosed(stp);
    	}
    }
    
    public static void mobileNumberUpdate(Id stepId){
  		string res = 'Success';
  		Step__c objStp = [select id,name,mobile_No__c,status__c,step_name__c,SR__c from Step__c where id =:stepId order by createddate DESC limit 1];
  		try {
        	List<Service_request__c> servList = new List<Service_request__c>();
			for(Service_request__c servReq : [Select id,Mobile_number__c,Would_you_like_to_opt_our_free_couri__c from service_request__c where id=:objStp.SR__c]){
				servReq.Mobile_number__c = objStp.mobile_No__c != Null ? objStp.mobile_No__c : Null;
				servList.add(servReq);
			}
			update servList;
			objStp.status__c = System.Label.closedStepStatus;
			update objStp;
        } catch (exception e) { res = e.getMessage();}
    }
    
    public static string checkIfMobileUpdateStepIsClosed(step__c objStp) {
        string res = 'Success';
        try {
        	if([Select count() from service_Request__c where id=:objStp.SR__c and Mobile_Number__c = Null] > 0){
        		res =  'UAE mobile number is mandatory in order to proceed further';
        	}
			
        } catch (exception e) { res = e.getMessage(); }

        return res;

    } 
}