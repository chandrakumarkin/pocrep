@isTest(seeAllData=true)
public class PackageSupport_Test 
{
    @isTest
    private static void init()
    {
    
           Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        objAccount.Roc_status__c= 'Active';
        insert objAccount;
        
      //  Account ObjExAccount=[select id,Name,Roc_status__c,Company_type__c from account where Roc_status__c='Active' limit 1];
        Package__c ObjPackage=[select id,Package_Code__c from Package__c where Package_Code__c='Innovation Market' limit 1];
        
        Package__c ObjDefaultPackage=[select id,Package_Code__c from Package__c where Package_Code__c='Innovation Standard' limit 1];
        
        List<ID> ids=new List<ID>();
        //ids.add(ObjExAccount.id);
        ids.add(objAccount.id);
        
        
        
        Company_package__c ObjPack=new Company_package__c();
        ObjPack.Account__c=objAccount.id;
        ObjPack.Package__c=ObjPackage.id;
        ObjPack.Status__c='Approved';
        ObjPack.Start_Date__c=Date.today();
        ObjPack.End_Date__c=Date.today().addDays(365);
        insert ObjPack;
        
        /*
        Company_package__c ObjPackd=new Company_package__c();
        ObjPackd.Account__c=objAccount.id;
        ObjPackd.Package__c=ObjDefaultPackage.id;
        ObjPackd.Status__c='Default';
        ObjPackd.Start_Date__c=Date.today();
        ObjPackd.End_Date__c=Date.today().addDays(365);
        insert ObjPackd;
        */
        
        
       
        
        
        //PackageSupportCls.
        
        PackageSupportCls.PackageData ObjPackageNew=new PackageSupportCls.PackageData();
        ObjPackageNew.AccountId=objAccount.id;
        ObjPackageNew.AccountStatus=objAccount.ROC_Status__c;
        ObjPackageNew.CompanyType=objAccount.Company_type__c ;
        ObjPackageNew.CurrentAccountPackageCode=ObjPackage.Package_Code__c;
        ObjPackageNew.PackageCode='Innovation Standard';
        ObjPackageNew.PackageStatus='Default';
        
        
        List<PackageSupportCls.PackageData> ListNEwPacks=new List<PackageSupportCls.PackageData>();
        ListNEwPacks.add(ObjPackageNew);
        
        PackageSupportCls.DefaultPackage(ListNEwPacks);
        
        
         ExpiredPackageSupportCls.AssignActivePackage(ids);
        
        
        ObjPack.Status__c='Expired';
        
        update ObjPack;
        
        
        
    
    }
 }