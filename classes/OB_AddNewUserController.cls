/**
 * Description : Controller for OB_manageUsers Component 
 *
 * ****************************************************************************************
 * History :
 * [06.FEB.2020] Prateek Kadkol - Code Creation
 */
public without sharing class OB_AddNewUserController {
   
    @AuraEnabled
    public static RespondWrap initContact()  {
        
        //declaration of wrapper
    
        RespondWrap respWrap =  new RespondWrap();
        
        string recordId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();

        contact conObj = new contact();
        conOBj.RecordTypeId = recordId;
        respWrap.initContact = conObj;

        //get role picklist value
		
		Schema.DescribeFieldResult userAccsessSchema = AccountContactRelation.Access_Level__c.getDescribe();
		List<Schema.PicklistEntry> userAccsessSchemaVal = userAccsessSchema.getPicklistValues();
		for( Schema.PicklistEntry access : userAccsessSchemaVal){
			respWrap.userAccsessValue.add(access.getValue());
        }  

        // get declaration
        Map<String,String> fieldDeclarationMap= new Map<String,String>();
        for(Declarations__mdt declare: [Select ID,Declaration_Text__c,Application_Type__c,Field_API_Name__c 
                                                From Declarations__mdt 
                                                Where 	Object_API_Name__c = 'AccountContactRelation' LIMIT 1])
                {
                    
                    fieldDeclarationMap.put(declare.Field_API_Name__c,declare.Declaration_Text__c);
                }
        
                if(fieldDeclarationMap != null && !fieldDeclarationMap.isEmpty()){
                    respWrap.declarationObj = fieldDeclarationMap;
                }
        return respWrap;

            	
    }

    @AuraEnabled
    public static RespondWrap getConAccess(String requestWrapParam)  {
        
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        
        

        for(AccountContactRelation relObj : [SELECT id, Roles, Access_Level__c FROM AccountContactRelation WHERE id =: reqWrap.relId]){   
            respWrap.updatedRelObj = relObj;
        }

         
        
        //get role picklist value
		
		Schema.DescribeFieldResult userAccsessSchema = AccountContactRelation.Access_Level__c.getDescribe();
		List<Schema.PicklistEntry> userAccsessSchemaVal = userAccsessSchema.getPicklistValues();
		for( Schema.PicklistEntry access : userAccsessSchemaVal){
			respWrap.userAccsessValue.add(access.getValue());
        } 

        
        return respWrap;

            	
    }

    @AuraEnabled
    public static RespondWrap insertNewCont(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

        //check for duplicate 

        boolean isDuplicate = false;
        list<User> dupUserList = new list<User>();
           

       //company and propRoleCheck
       reqWrap.RolesSelected = OB_AgentEntityUtils.compAndPropRoleCheck(reqWrap.RolesSelected);
        
        for(user uerObj : [SELECT id,firstName,LastName,contactId FROM user where email =: reqWrap.contToInsert.email AND ContactId != NULL 
        AND IsActive = true ]){
            isDuplicate = true;
            dupUserList.add(uerObj);           
        }

        if(isDuplicate == false){
            

            try{
                
                contact ConToInsert = new Contact();
                ConToInsert = reqWrap.contToInsert; 
                ConToInsert.OB_Roles__c = reqWrap.RolesSelected;
                ConToInsert.OB_Access_Level__c = reqWrap.AccessLevel;
                ConToInsert.Send_Portal_Login_Link__c = 'yes';
                ConToInsert.OB_Block_Homepage__c = true;
                ConToInsert.AccountId = reqWrap.accIdToRelate;
                insert ConToInsert;

                if(ConToInsert.Id != null){
                    respWrap.insertedContact = ConToInsert;
                respWrap.updatedRelObj = updateRelationshipRecord(ConToInsert.Id,reqWrap.accIdToRelate,
                reqWrap.RolesSelected,reqWrap.AccessLevel);

                //create compleate your profile sr

                creteCompleateProfileSr(ConToInsert);
                }
                
                
            }catch(DMLException e) {
                string DMLError = e.getdmlMessage(0) + '';
                if(DMLError == null) {
                    DMLError = e.getMessage() + '';
                }
                respWrap.errorMessage = DMLError;

                
            }
        }else{
            try{
                set<String> dupUserId = new set<String>();
                for(user dupuser : dupUserList){
                    dupUserId.add(dupuser.contactId);
                }

                 
            //boolean hasRel = relCheck(dupUserId,reqWrap.accIdToRelate);
            relCheckWrap relWrap = relCheck(dupUserId,reqWrap.accIdToRelate);
            if(relWrap.relExist == true){
                respWrap.duplicateContact =  dupUserList[0];  
                if(relWrap.relRec.isActive == true){
                    respWrap.errorMessage = 'The user with this email address is already linked to this entity';
                }else{
                    respWrap.relToAct = relWrap.relRec.Id;
                    respWrap.errorMessage = 'Deativated User';
                }
                 
                
            }else{
                
                respWrap.duplicateContact = dupUserList[0];
                respWrap.errorMessage = 'Dup User';
                //respWrap.updatedRelObj = updateRelationshipRecord(contactId,reqWrap.accIdToRelate,
                //reqWrap.RolesSelected,reqWrap.AccessLevel);
            } 
            }catch(DMLException e) {
                string DMLError = e.getdmlMessage(0) + '';
                if(DMLError == null) {
                    DMLError = e.getMessage() + '';
                }
                respWrap.errorMessage = DMLError;
            }
                     
        }
               
        return respWrap;

    }

    @AuraEnabled
    public static RespondWrap addDupContact(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
      
            try{
               
                respWrap.updatedRelObj = updateRelationshipRecord(reqWrap.duplicateContact.Contactid,reqWrap.accIdToRelate,
                reqWrap.RolesSelected,reqWrap.AccessLevel);
            
            }catch(DMLException e) {
                string DMLError = e.getdmlMessage(0) + '';
                if(DMLError == null) {
                    DMLError = e.getMessage() + '';
                }
                respWrap.errorMessage = DMLError;
            }
                     
        
               
        return respWrap;

    }

    public static relCheckWrap relCheck(set<String> contactID, String relAccountID)  {

        relCheckWrap respWrap =new relCheckWrap();
        respWrap.relExist = false;
        for(AccountContactRelation relObj : [SELECT id, Roles, isActive FROM AccountContactRelation WHERE ContactId IN :contactID
        AND AccountId =: relAccountID AND isActive = true LIMIT 1]){
            respWrap.relExist = true;
            respWrap.relRec = relObj;
        }
        
        return respWrap;
        
    }

    public static void creteCompleateProfileSr(contact relCont)  {

        string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('Converted User Registration').getRecordTypeId();
		string srTemplateId = '';
				
		for(HexaBPM__SR_Template__c srTempObj : [SELECT id,name from HexaBPM__SR_Template__c WHERE Name = 'Complete your profile'
		AND HexaBPM__SR_RecordType_API_Name__c = 'Converted_User_Registration' AND HexaBPM__Active__c = true]){
			srTemplateId = srTempObj.Id;
		}

		if( srTemplateId!= null && recordId !=null){

			HexaBPM__Service_Request__c srObj = new HexaBPM__Service_Request__c();
			srObj.recordTypeId = recordId;
			srObj.HexaBPM__SR_Template__c = srTemplateId;
            srObj.HexaBPM__Customer__c = relCont.AccountId;
            srObj.HexaBPM__Contact__c = relCont.Id;
            srObj.HexaBPM__Email__c = relCont.email ;
            srObj.first_name__c = relCont.firstName;
            srObj.last_name__c = relCont.lastName;
            srObj.hexabpm__send_sms_to_mobile__c = relCont.phone ;
            srObj.Created_By_Agent__c = true;
            insert srObj;
        }

        
    }
    
    public static AccountContactRelation updateRelationshipRecord(String insertedContactID, String relAccountID, string rolesToSet,string accsesslevel)  {

        AccountContactRelation relOBjToUPdate = new AccountContactRelation();
        boolean relExist = false;
        for(AccountContactRelation relObj : [SELECT id, Roles, Access_Level__c FROM AccountContactRelation WHERE ContactId =:insertedContactID
        AND AccountId =: relAccountID  LIMIT 1]){
            relExist = true;
            relOBjToUPdate = relObj;
        }

        if(relExist == false){
            relOBjToUPdate.AccountId = relAccountID;
        relOBjToUPdate.ContactId = insertedContactID;
        }
        relOBjToUPdate.isActive = true; 
        relOBjToUPdate.I_agree_superuser__c = true;
        relOBjToUPdate.Roles = rolesToSet;
        relOBjToUPdate.Access_Level__c = accsesslevel;
        upsert relOBjToUPdate;
        return relOBjToUPdate;
        
    }


    @AuraEnabled
    public static RespondWrap updateRelationship(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

        //company and propRoleCheck
       reqWrap.RolesSelected = OB_AgentEntityUtils.compAndPropRoleCheck(reqWrap.RolesSelected);
       
        AccountContactRelation relOBjToUPdate = new AccountContactRelation();
        for(AccountContactRelation relObj : [SELECT id, Roles, Access_Level__c FROM AccountContactRelation WHERE id =: reqWrap.relId]){
            relOBjToUPdate = relObj;
        }


        if(reqWrap.toDeactivate == true){

            relOBjToUPdate.isActive = false;

        }else if(reqWrap.toActivate == true){

            relOBjToUPdate.isActive = true;

        }else{
            relOBjToUPdate.Roles = reqWrap.RolesSelected;
        relOBjToUPdate.Access_Level__c = reqWrap.AccessLevel;
        }

        try{
        
         update relOBjToUPdate;
         respWrap.updatedRelObj =relOBjToUPdate;
         
        }catch(DMLException e) {
        
         string DMLError = e.getdmlMessage(0) + '';
         if(DMLError == null) {
         DMLError = e.getMessage() + '';
         
         }
         respWrap.errorMessage = DMLError;
        }
        
        
        return respWrap;

    }

   
        
        // ------------ Wrapper List ----------- //
        
        public class RequestWrap{

            @AuraEnabled public contact contToInsert;
            @AuraEnabled public string AccessLevel;
            @AuraEnabled public string RolesSelected;
            @AuraEnabled public string accIdToRelate;
            @AuraEnabled public string relId;
            @AuraEnabled public boolean toDeactivate;
            @AuraEnabled public boolean toActivate;
            @AuraEnabled public user duplicateContact;
                                                                                                                                                                                                                                                  public RequestWrap(){}
        }

        public class relCheckWrap{
            @AuraEnabled public boolean relExist;
            @AuraEnabled public boolean isDeact; 
            @AuraEnabled public AccountContactRelation relRec;                                                                                                                                                                                                                                                 
        }
        
        public class RespondWrap{
    
            @AuraEnabled public contact initContact;
            @AuraEnabled public List<String> userAccsessValue = new List<String>();
            @AuraEnabled public string errorMessage;
            @AuraEnabled public string customError;
            @AuraEnabled public contact insertedContact;
            @AuraEnabled public AccountContactRelation updatedRelObj;
            @AuraEnabled public string relToAct;
            @AuraEnabled public object declarationObj;
            @AuraEnabled public user duplicateContact;

        
        }
}