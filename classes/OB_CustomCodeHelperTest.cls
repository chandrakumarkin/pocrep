@isTest

public class OB_CustomCodeHelperTest {

    public static testmethod void test1(){

         // create account

        List<Account> insertNewAccounts = new List<Account>();

        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);

        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';

        //insertNewAccounts[0].Next_Renewal_Date__c = Date.today();

        insert insertNewAccounts;

       

        License__c lic = new License__c(name='12345',Account__c=insertNewAccounts[0].id, License_Expiry_Date__c = Date.today());

        insert lic;

       

        insertNewAccounts[0].Active_License__c = lic.id;

        update insertNewAccounts;

       

        

        //create lead

        List<Lead> insertNewLeads = new List<Lead>();

        insertNewLeads = OB_TestDataFactory.createLead(2);

        insert insertNewLeads;

        //create License Activity Master

        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();

        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});

        insert listLicenseActivityMaster;

       

        //create lead activity

        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();

        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);

        insert listLeadActivity;

       

        //create createSRTemplate

            List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();

        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});

        createSRTemplateList[0].ownerid = userinfo.getUserId();

        insert createSRTemplateList;

       

        system.debug('============='+ createSRTemplateList[0].ownerid);

        //create page flow

        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;

        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);

        insert listPageFlow;

       

        //create page

        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;

        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);

        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';

        listPage[0].Community_Page__c = 'ob-searchentityname';

        insert listPage;

       

        //create SR status

        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;

        listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});

        insert listSRStatus;

        //create SR

        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();

        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts,

                                                   new List<string>{'Non - financial','Retail'},

                                                   new List<string>{'Foundation','Services'},

                                                   new List<string>{'Foundation','Company'},

                                                   new List<string>{'Foundation','Recognized Company'});

        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);

        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);

        insertNewSRs[1].Setting_Up__c='Branch';

        insertNewSRs[0].Setting_Up__c='Branch';

        insertNewSRs[1].Page_Tracker__c = listPage[0].id;

        insertNewSRs[0].Page_Tracker__c = listPage[0].id;

        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';

        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';

        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();

        insertNewSRs[0].Passport_No__c = 'A12345';

        insertNewSRs[0].Nationality__c = 'India';

        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);

        insertNewSRs[0].Gender__c = 'Male';

        insertNewSRs[0].Place_of_Birth__c = 'Dubai';

        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);

        insertNewSRs[0].Last_Name__c = 'test';

        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';

        insertNewSRs[0].Entity_Type__c = 'Retail';

        insertNewSRs[0].Business_Sector__c = 'Hotel';

        insertNewSRs[0].entity_name__c = 'Tentity';

        insertNewSRs[1].License_Application__c = lic.id;

        insertNewSRs[0].Apply_for_Establishment_Card__c = 'Yes';

       // insertNewSRs[0].Place_of_Issue__c = 'Dubai';

        insert insertNewSRs;

       

           
        //create step template

        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();

        listStepTemplate = OB_TestDataFactory.createStepTemplate(5, new List<string> {'RM_Assignment'}, new List<String> {'REGISTRAR_REVIEW','BD_DETAILED_REVIEW','CLO_REVIEW','COMPLIANCE_REVIEW','CFO_REVIEW'}

                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});

        insert listStepTemplate;

        //create SR Step

        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();

        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);

        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';

        listSRSteps[0].HexaBPM__Step_No__c = 1;

        listSRSteps[0].ownerId = userinfo.getUserId();

        //listSRSteps[0].HexaBPM__Step_Template_Code__c = 'CLO_REVIEW';

        insert listSRSteps;

       

       

        

        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Awaiting Verification',HexaBPM__Code__c='AWAITING_VERIFICATION',HexaBPM__Type__c='Start',HexaBPM__Reupload_Document__c=true);

        insert status;

        HexaBPM__Status__c status1 = new HexaBPM__Status__c

        (Name='Re-Upload Document',HexaBPM__Code__c='REUPLOAD_DOCUMENT',HexaBPM__Type__c='End',HexaBPM__Reupload_Document__c=true);

        insert status1;

       

         List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();

                        insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);

        insertNewSteps[0].HexaBPM__Status__c = status.id;

       // insertNewSteps[0].Step_Template_Code__c = 'CLO_REVIEW';

        insertNewSteps[0].Relationship_Manager__c = userinfo.getUserId();

        insertNewSteps[1].Relationship_Manager__c = userinfo.getUserId();

        insertNewSteps[1].HexaBPM__Sys_Step_Loop_No__c ='A_5';

        insert insertNewSteps;

       

        HexaBPM__Transition__c transition = new HexaBPM__Transition__c(HexaBPM__From__c=status.id,HexaBPM__To__c=status1.id);

        insert transition;

       

        HexaBPM__SR_Status__c srStatus = new HexaBPM__SR_Status__c(Name='Awaiting Re-Upload',HexaBPM__Code__c='AWAITING_RE-UPLOAD');

        insert srStatus;

       

        HexaBPM__Step_Transition__c stepTransition = new HexaBPM__Step_Transition__c

        (HexaBPM__SR_Step__c=listSRSteps[0].id,HexaBPM__Transition__c=transition.id,HexaBPM__SR_Status_Internal__c=listSRStatus[1].id,HexaBPM__SR_Status_External__c=listSRStatus[1].id);

        insert stepTransition;

       

        Company_Name__c companyName = new Company_Name__c(Application__c = insertNewSRs[1].id,Status__c='Approved' , OB_Is_Trade_Name_Allowed__c='Yes',Entity_Name__c = insertNewAccounts[0].id);

        insert companyName;

       

         // create document master

        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();

        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'GENERAL'});

        listDocMaster[0].HexaBPM__Code__c='NOC_SHARING';

        insert listDocMaster;

       

        Group testGroup = new Group(Name='Client', Type='Queue');

                        insert testGroup;

       

        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();

        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);

        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;

        insert listSRTemplateDoc;

       

        // create SR Doc

        List<HexaBPM__SR_Doc__c> listSRDoc = new List<HexaBPM__SR_Doc__c>();

        listSRDoc = OB_TestDataFactory.createSRDoc(3, insertNewSRs, listDocMaster, listSRTemplateDoc);

        listSRDoc[0].HexaBPM__Status__c = 'Uploaded';

        listSRDoc[1].HexaBPM__Status__c = 'Uploaded';

        listSRDoc[2].HexaBPM__Status__c = 'Uploaded';

        listSRDoc[0].HexaBPM__Is_Not_Required__c = true;

        listSRDoc[1].HexaBPM__Is_Not_Required__c = true;

        listSRDoc[2].HexaBPM__Is_Not_Required__c = true;
        listSRDoc[0].HexaBPM__Doc_ID__c ='123';

        insert listSRDoc;

       

        HexaBPM__Section__c section = new HexaBPM__Section__c();

        section.HexaBPM__Default_Rendering__c=false;

        section.HexaBPM__Section_Type__c='CommandButtonSection';

        section.HexaBPM__Section_Description__c='PageBlockSection';

        section.HexaBPM__layout__c='2';

        section.HexaBPM__Page__c =listPage[0].id;

        insert section;

 

        section.HexaBPM__Default_Rendering__c=true;

        update section;

       

        

        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();

        sec.HexaBPM__Section__c=section.id;

        sec.HexaBPM__Component_Type__c='Input Field';

        sec.HexaBPM__Field_API_Name__c='first_name__c';

        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';

        sec.HexaBPM__Default_Value__c='0';

        sec.HexaBPM__Mark_it_as_Required__c=true;

        sec.HexaBPM__Field_Description__c = 'desc';

        sec.HexaBPM__Render_By_Default__c=false;

        insert sec;

        

        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();

        objAmendment1 = new HexaBPM_Amendment__c();

        objAmendment1.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');

        objAmendment1.Nationality_list__c = 'India';

        objAmendment1.Passport_No__c = '1234';

        objAmendment1.First_Name__c = '1234';

        objAmendment1.Last_Name__c = '1234';

        objAmendment1.Role__c = 'Director;Qualified Applicant';

        objAmendment1.Gender__c = 'Male';

        objAmendment1.DI_Mobile__c = '+97124567882';

        objAmendment1.ServiceRequest__c = insertNewSRs[0].Id;

        objAmendment1.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';

        insert objAmendment1;

       

        insertNewSRs[0].Select_who_will_be_signing_the_PSA__c = objAmendment1.id;

       // insertNewSRs[0].Pa

        update insertNewSRs;

         RecordType rt =  [Select Id From RecordType where sobjecttype = 'Opportunity' and developername = 'NFS'];

         Opportunity opp = new Opportunity();

    opp.Name = 'DIFC Financial';

    opp.CloseDate = System.today();

    opp.AccountId = insertNewAccounts[0].Id;

    opp.Lead_Email__c = 'abc@abc.com';

    opp.CC_Emails__c = 'abc@abc.com';

    opp.Lead_Id__c = insertNewLeads[0].Id;

   // opp.StageName = 'Submitted to Leasing Committee';

    opp.StageName = 'Application Submitted to DFSA';

    opp.RecordTypeId = rt.id;

    insert opp;

        

        Test.startTest();

            OB_CustomCodeHelper.CreateSR(insertNewSRs[0].id,'AOR_NF_R');

            OB_CustomCodeHelper.SRDeepClone(insertNewSRs[0].id,opp.id);
        OB_CustomCodeHelper.CreateSR(opp.id,'AOR_NF_R');

        Test.stopTest();

    }

}