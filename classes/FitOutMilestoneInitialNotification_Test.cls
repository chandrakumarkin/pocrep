@isTest
public class FitOutMilestoneInitialNotification_Test {
	public static testMethod void testFitOutMilestoneInitialNotification() 
    {
    	Account accRec = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
        insert accRec;
        
        Fitout_Milestone_Penalty__c fitoutPenSumRec 			= 	new Fitout_Milestone_Penalty__c();        
        fitoutPenSumRec.Account__c 								= 	accRec.id;
        fitoutPenSumRec.Unit_Handover_Date__c 					= 	system.today();
        fitoutPenSumRec.Additional_Rent_Free_Period_Days__c		= 	1;
        fitoutPenSumRec.Additional_Tenant_email__c				=	'mudasir.saleem@gmail.com';
        fitoutPenSumRec.Next_Conceptual_Penalty_Date__c			=	system.today()+4;
        fitoutPenSumRec.Next_Detail_Design_Penalty_Date__c		=	system.today()+4;
        fitoutPenSumRec.Next_First_Inspection_Penalty_Date__c	=	system.today()+4;
        fitoutPenSumRec.Next_Final_Inspection_Penalty_Date__c	=	system.today()+4;
        fitoutPenSumRec.Building__c								=	'Building';
        fitoutPenSumRec.Floor__c								=	'Floor';
        fitoutPenSumRec.Unit__c									=	'Unit';
        
        insert fitoutPenSumRec;
        Test.startTest();
        	FitOutMilestoneInitialNotification_Batch abs= new FitOutMilestoneInitialNotification_Batch();
	    	String cronExpr = '0 0 0 15 3 ? 2099';
			String jobId = System.schedule('myJobTestJobName', cronExpr, abs);
        	//abs.createLogRecord(Database.Error err)
        Test.stopTest();
    }
}