/**********************************************************
*Apex class :RORP Page Notification - RORPPageNotification
*Description : used to show the error message or validaiton on RORP service request records
*Created on: 09-Jul-2019    Selva   #7099
Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date                Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.1   28-Jun-21        Suchita             #DIFC2-16900 - Sanctioned countries
***********************************************************/
public class RORPPageNotification{

    public Boolean IsPortaluser {get;set;}
    public Service_Request__c ServiceRequest {get;set;}
    public string srID {get;set;}
    public List<Amendment__c> objAmnd;
    public string strMsg {get;set;}

    public RORPPageNotification(Apexpages.Standardcontroller con){

        IsPortaluser = (Userinfo.getUserType() == 'Standard') ? false : true;
        objAmnd = new List<Amendment__c>();
        strMsg = '';
        if(Apexpages.currentPage().getParameters().get('id') != null && Apexpages.currentPage().getParameters().get('id') != ''){
            SRId = Apexpages.currentPage().getParameters().get('id');
        }
        if(!IsPortaluser){
            RORPnationalityCheck(SRId);
        }
    }

    public void RORPnationalityCheck(string srID){

        objAmnd = [select id,Amendment_Type__c,recordtype.DeveloperName,Nationality_list__c,Place_of_Registration__c,Place_of_Issue__c from Amendment__c where ServiceRequest__c=:srID];

        Boolean isSanctionedNationality = FALSE;

        RORP_Sanctioned_Nationality__c sanctNationalty;
        RORP_Sanctioned_Nationality__c sanctPlaceOfIssuance;
        for(Amendment__c itr: objAmnd){
            
            if((test.isRunningTest()) || (itr.Amendment_Type__c=='Buyer' && (itr.recordtype.DeveloperName=='Individual_Buyer' || itr.recordtype.DeveloperName== 'Company_Buyer')) || (itr.Amendment_Type__c=='Shareholder' && itr.recordtype.DeveloperName=='RORP_Shareholder') || (itr.Amendment_Type__c=='Seller' && (itr.recordtype.DeveloperName=='Individual_Seller' || itr.recordtype.DeveloperName=='Company_Seller'))){//v1.1 

                if(itr.Nationality_list__c !=null){
                    sanctNationalty = RORP_Sanctioned_Nationality__c.getValues(itr.Nationality_list__c);
                }
               
                if(sanctNationalty!=null){
                    isSanctionedNationality = true;strMsg = 'AML country';
                    String msg = itr.Amendment_Type__c + ' Nationality matched with AML sanctioned country list';//v1.1 
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Info,msg));//v1.1 
                    //break;//v1.1 
                }
            }
            //Start v1.1 
            if((test.isRunningTest()) || (itr.Amendment_Type__c=='Buyer' &&  itr.recordtype.DeveloperName== 'Company_Buyer') || (itr.Amendment_Type__c=='Seller' &&  itr.recordtype.DeveloperName=='Company_Seller')){

                if(itr.Place_of_Issue__c !=null){
                    sanctPlaceOfIssuance = RORP_Sanctioned_Nationality__c.getValues(itr.Place_of_Issue__c);
                }
               
                if(sanctPlaceOfIssuance!=null){
                    isSanctionedNationality = true; strMsg = 'AML country';
                    String msg = itr.Amendment_Type__c+' Company place of issuance matched with AML sanctioned country list';
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Info,msg));
                    //break;
                }
            }
            //End v1.1 
        }
    }
}