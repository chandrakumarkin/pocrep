/**
*Author : Merul Shah
*Description : Data provided for Register address module..
Merul : 19 Jan 2020 : #1635 : registered address should have any addresses till sharing of office is approved.

**/
public without sharing class OB_RegisteredAddressContainerController {

    //static final String OL_NOT_FOUND_ERROR  = 'No address was found, please apply for the lease registration.';
    static final String OL_NOT_FOUND_ERROR  = '</br><b>No address was found. Please follow the steps outlined on this page to register an address</b></br></br>';
// Commented below line as per tkt #1474 
//'- If you have leased a property in the DIFC, then please apply for Lease Registration. For more details on Lease Registration, please email rorp@difc.ae or visit us at DIFC Services.</br></br>'+
//'- If you own the unit from which the entity will operate, then please apply for License to Occupy. For more details on Licence to Occupy, please email rorp@difc.ae or visit us at DIFC Services.</br></br>'+
//'- If the proposed entity will be sharing office space with an affiliate that was not submitted as part of the initial approval form, then please contact your assigned Relationship Manager.';
    
    @AuraEnabled
    public static ResponseWrapper removeOprLocAmed(String reqWrapPram) 
    {
        //reqest wrpper
        RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);

        //response wrpper
        ResponseWrapper respWrap = new ResponseWrapper();

        try{
            AmendmentWrapper amedWrap = reqWrap.amedWrap;
            amedWrap.amedObj.Status__c = 'Inactive';
            update amedWrap.amedObj;
        } 
        catch(Exception e) 
        {
            
            system.debug('#########exception Msg ' + e.getMessage());
            respWrap.errorMessage = '' + e.getMessage();
        }

        return respWrap;


    }

    @AuraEnabled
    public static ResponseWrapper onAmedSaveToDB(String reqWrapPram) {
        system.debug('########## reqWrapPram '+reqWrapPram);
        //reqest wrpper
        RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);

        //response wrpper
        ResponseWrapper respWrap = new ResponseWrapper();

        List<RecordType> lstRecTyp = new List<RecordType>();
        lstRecTyp = [SELECT id, 
                             DeveloperName, 
                             SobjectType
                     FROM RecordType
                     WHERE DeveloperName = 'Operating_Location'
                     AND SobjectType = 'HexaBPM_Amendment__c'];


        Operating_Location__c  objOL = reqWrap.oprLocWrap.oprLocObj;
        
        HexaBPM__Service_Request__c srObj = reqWrap.srWrap.srObj;

        if(reqWrap.srWrap.amedWrapLst != NULL && reqWrap.srWrap.amedWrapLst.size() > 0) {
            respWrap.errorMessage = 'Please remove the registered address';
            return respWrap;
        }

        if(srObj != NULL) {
            update srObj;
		}

        if(objOL != NULL) 
        {
            HexaBPM_Amendment__c objAmd = new HexaBPM_Amendment__c();
            objAmd.Customer__c = objOL.Account__c;
            objAmd.ServiceRequest__c = srObj.Id;
            objAmd.Amendment_Type__c = 'Operating Location';
            //objAmd.IsRegistered__c = objOL.IsRegistered__c;
            objAmd.IsRegistered__c = true;
            objAmd.Unit__c = objOL.Unit__c;
            objAmd.Hosting_Company__c = objOL.Hosting_Company__c;
            objAmd.Operating_Type__c = objOL.Type__c;
            objAmd.Operating_Location__c = objOL.Id;
            objAmd.Operating_Location_Status__c = objOL.Status__c;
            //objAmd.Building__c = objOL.Building__c;
            objAmd.Status__c = 'Active';
            objAmd.recordTypeId = (lstRecTyp != NULL ? lstRecTyp [0].Id :NULL);

            if(objAmd.Role__c == NULL) {
                objAmd.Role__c = 'Operating Location';
            } 
            else if(objAmd.Role__c.indexOf('Operating Location') ==-1) {
                objAmd.Role__c = objAmd.Role__c + ';Operating Location';
            }
            upsert objAmd;
        }
        return respWrap;

    }


    @AuraEnabled
    public static ResponseWrapper getOperatingLocationsForCSP(String reqWrapPram) 
    {
        //reqest wrpper
        RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);

        //response wrpper
        ResponseWrapper respWrap = new ResponseWrapper();
        respWrap.lstOprLocWrap = new List<OperationLocationWrapper>();
        respWrap.srWrap = new SRWrapper();

        List<Operating_Location__c> lstFinalOptLoc = new List<Operating_Location__c>();
        Set<String> selectedAmedOptLoc = new  Set<String>(); 
         //Request
        SRWrapper srWrap = reqWrap.srWrap;
        respWrap.srWrap = reqWrap.srWrap;
        respWrap.srWrap.amedWrapLst = new List<AmendmentWrapper>();
        
        //respWrap.srWrap.amedWrapLst = new List<AmendmentWrapper>();
        system.debug('@@@@@@@@@@ reqWrap.srWrap.srObj.Id '+reqWrap.srWrap.srObj.Id);
        
        
        //For existing opertating Location Amendment
        for(HexaBPM_Amendment__c amedObj : getExistingAmendment(reqWrap.srWrap.srObj.Id,'Operating Location')) 
        {
            AmendmentWrapper amedWrap = new AmendmentWrapper();
            amedWrap.amedObj = amedObj;
            selectedAmedOptLoc.add(amedObj.Operating_Location__c);
            respWrap.srWrap.amedWrapLst.add(amedWrap);
        }
        

        if(srWrap != NULL) 
        {
            if(lstFinalOptLoc.size() == 0 && srWrap.srObj.Corporate_Service_Provider_Name__c != NULL) 
            {
                //CSP
                lstFinalOptLoc.addAll(getActiveAndRegisteredOpertingLocation(srWrap.srObj.Corporate_Service_Provider_Name__c));

            }
        }

        // wrapping
        if(lstFinalOptLoc.size() > 0 && respWrap.errorMessage == NULL) 
        {
            
            for(Operating_Location__c oprLoc :lstFinalOptLoc) 
            {
                if( !selectedAmedOptLoc.contains(oprLoc.Id ) )
                {
                        OperationLocationWrapper oprLocWrap = new OperationLocationWrapper();
                        oprLocWrap.oprLocObj = oprLoc;
                        respWrap.lstOprLocWrap.add(oprLocWrap);
                }
               
            }
        } 
        else if(lstFinalOptLoc.size() == 0) 
        {
            respWrap.errorMessage = OL_NOT_FOUND_ERROR  ;

        }
        return respWrap;


    }


    @AuraEnabled
    public static ResponseWrapper getOperatingLocations(String reqWrapPram) 
    {
    
        //reqest wrpper
        RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);

        //response wrpper
        ResponseWrapper respWrap = new ResponseWrapper();
        respWrap.lstOprLocWrap = new List<OperationLocationWrapper>();
        
        system.debug('@@@@@@@@@@ reqWrap.srId '+reqWrap.srId);
        
        Set<String> selectedAmedOptLoc = new Set<String>();
        List<Operating_Location__c> lstFinalOptLoc = new List<Operating_Location__c>();
        String srId = reqWrap.srId;
        respWrap.srWrap = new SRWrapper();
        respWrap.srWrap.amedWrapLst = new List<AmendmentWrapper>();
        
       // system.debug('@@@@@@@@@@ getExistingAmendment  '+getExistingAmendment(srId,'Operating Location') );
         

         //For existing opertating Location Amendment
        for(HexaBPM_Amendment__c amedObj : getExistingAmendment(srId,'Operating Location')) 
        {
            AmendmentWrapper amedWrap = new AmendmentWrapper();
            amedWrap.amedObj = amedObj;
            selectedAmedOptLoc.add(amedObj.Operating_Location__c);
            respWrap.srWrap.amedWrapLst.add(amedWrap);
        }
        system.debug('@@@@@@@@@@ respWrap.srWrap.amedWrapLst before '+respWrap.srWrap.amedWrapLst );
        system.debug('######## srId  ' + srId);
        for(HexaBPM__Service_Request__c serReq :[SELECT id, 
                                                         HexaBPM__Customer__c, 
                                                         Business_Sector__c, 
                                                         Name_of_the_Hosting_Affiliate__c, 
                                                         Registered_Agent__c, 
                                                         Corporate_Service_Provider_Name__c, 
                                                         Corporate_Service_Provider_Name__r.Name, 
                                                         Co_working_Facility__c, 
                                                         Fund_Administrator_Name__c, 
                                                         Fund_Manager__c, 
                                                         Location_of_records_and_registers__c, 
                                                         Records_and_registers_held_by__c, 
                                                         HexaBPM__External_SR_Status__c, 
                                                         HexaBPM__External_SR_Status__r.Name, 
                                                         HexaBPM__Internal_SR_Status__c, 
                                                         HexaBPM__Internal_Status_Name__c,
                                                 		 HexaBPM__Record_Type_Name__c/*,
														 (
                                                             SELECT id,
																	Step_Template_Code__c,
																	Current_Step_Status__c 
															   FROM HexaBPM__Steps_SR__r  
                                                         )*/
                                                 FROM HexaBPM__Service_Request__c
                                                 WHERE id = :srId
                                                 LIMIT 1
            ]) 
            
        {


            //respWrap.srWrap = new SRWrapper();
            String flowId = reqWrap.flowId;
            
            respWrap.srWrap.srObj = serReq;
            respWrap.srWrap.lookupLabel = serReq.Corporate_Service_Provider_Name__r.Name;

            String customerId = serReq.HexaBPM__Customer__c;
            List<Operating_Location__c> lstOwnOptLoc = new List<Operating_Location__c>();
           

            lstOwnOptLoc = getOwnOpertingLoaction(customerId);
            system.debug('######## lstOwnOptLoc  ' + lstOwnOptLoc.size());
            
            
            //Quering related Step.
			List<HexaBPM__Step__c> lstStp = new List<HexaBPM__Step__c>();
            lstStp = [ 
					   SELECT id,
                      		  Step_Template_Code__c,
                              Current_Step_Status__c 
                         FROM HexaBPM__Step__c
					    WHERE HexaBPM__SR__c =:srId
					];
			

            if(serReq.Business_Sector__c == 'Prescribed Companies') 
            {

                system.debug('######## Prescribed Companies ');
                if(serReq.Name_of_the_Hosting_Affiliate__c != NULL || serReq.Registered_Agent__c != NULL) 
                {
                  if(serReq.Name_of_the_Hosting_Affiliate__c != NULL) 
                {
                    // registered address should have any addresses till sharing of office is approved.
                    Boolean loadOpLoc = ( serReq.HexaBPM__Record_Type_Name__c == 'AOR_Financial' ? isSharingOfOfficeApproved(serReq,lstStp) : true);
                    if(loadOpLoc)
                    {
                        lstFinalOptLoc.addAll(getActiveAndRegisteredOpertingLocation(serReq.Name_of_the_Hosting_Affiliate__c));
                    }
                }

                if(serReq.Registered_Agent__c != NULL) {
                    lstFinalOptLoc.addAll(getActiveAndRegisteredOpertingLocation(serReq.Registered_Agent__c));
                }
                }

                if(serReq.HexaBPM__Customer__c != NULL && lstOwnOptLoc.size() > 0) {

                    lstFinalOptLoc.addAll(lstOwnOptLoc);
                }


                if(lstFinalOptLoc.size() == 0) {
                    //CSP
                    //lstFinalOptLoc.addAll( getActiveAndRegisteredOpertingLocation(serReq.Corporate_Service_Provider_Name__c ) );
                    respWrap.askForCSP = true;
                }

            } 
            else if(serReq.Name_of_the_Hosting_Affiliate__c != NULL || serReq.Registered_Agent__c != NULL) 
            {

                system.debug('######## Hosting ');
				if(serReq.Name_of_the_Hosting_Affiliate__c != NULL) 
                {
                    // registered address should have any addresses till sharing of office is approved.
                    Boolean loadOpLoc = ( serReq.HexaBPM__Record_Type_Name__c == 'AOR_Financial' ? isSharingOfOfficeApproved(serReq,lstStp) : true);
                    if(loadOpLoc)
                    {
                        lstFinalOptLoc.addAll(getActiveAndRegisteredOpertingLocation(serReq.Name_of_the_Hosting_Affiliate__c));
                    }
                }

                if(serReq.Registered_Agent__c != NULL) {
                    lstFinalOptLoc.addAll(getActiveAndRegisteredOpertingLocation(serReq.Registered_Agent__c));
                }

                if(serReq.HexaBPM__Customer__c != NULL && lstOwnOptLoc.size() > 0) {

                    lstFinalOptLoc.addAll(lstOwnOptLoc);
                }

                if(lstFinalOptLoc.size() == 0) {
                    respWrap.errorMessage = OL_NOT_FOUND_ERROR;
                }


            } 
            else if(serReq.Co_working_Facility__c != NULL && serReq.Co_working_Facility__c == 'Yes') 
            {

                system.debug('######## Co working');
                if(serReq.HexaBPM__Customer__c != NULL && lstOwnOptLoc.size() > 0) 
                {
                    lstFinalOptLoc.addAll(lstOwnOptLoc);
                }


                if(lstFinalOptLoc.size() == 0) 
                {
                    respWrap.errorMessage = 'No address was found, please apply for the lease on coworking system.';
                }
            } 
            else if(serReq.Fund_Administrator_Name__c != NULL || serReq.Fund_Manager__c != NULL) 
            {
                    system.debug('######## Fund Admin');
                    if(serReq.Fund_Administrator_Name__c != NULL) 
                    {
                       lstFinalOptLoc.addAll(getActiveAndRegisteredOpertingLocation(serReq.Fund_Administrator_Name__c));
                    }

                    if(serReq.Fund_Manager__c != NULL) 
                    {
                        lstFinalOptLoc.addAll(getActiveAndRegisteredOpertingLocation(serReq.Fund_Manager__c));
                    }
    
                    if(lstFinalOptLoc.size() == 0) 
                    {
                        respWrap.errorMessage = OL_NOT_FOUND_ERROR;
                    }

            } 
            else if(lstOwnOptLoc.size() > 0)
            {

                    lstFinalOptLoc.addAll(lstOwnOptLoc);
                    if(lstFinalOptLoc.size() == 0) 
                    {
                        respWrap.errorMessage = OL_NOT_FOUND_ERROR;
                    }
             }


                system.debug('######## respWrap.errorMessage ' + respWrap.errorMessage);
                // wrapping
                if(lstFinalOptLoc.size() > 0 && respWrap.errorMessage == NULL) 
                {
                    system.debug('######## lstOwnOptLoc');
                    for(Operating_Location__c oprLoc :lstFinalOptLoc) 
                    {
                        if( !selectedAmedOptLoc.contains(oprLoc.Id ) )
                        {
                                OperationLocationWrapper oprLocWrap = new OperationLocationWrapper();
                                oprLocWrap.oprLocObj = oprLoc;
                                respWrap.lstOprLocWrap.add(oprLocWrap);
                        }
                        
                    }
                }
                else if(lstFinalOptLoc.size() == 0) 
                {
                    respWrap.errorMessage = OL_NOT_FOUND_ERROR;
                }
            
            
        }
        
        for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
                                                    AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
            respWrap.ButtonSection = sectionObj;
        }

        system.debug('@@@@@@@@@@ respWrap.srWrap.amedWrapLst end '+respWrap.srWrap.amedWrapLst );
        return respWrap;

    }
    
    
    //registered address should have any addresses till sharing of office is approved.
    public static Boolean isSharingOfOfficeApproved(HexaBPM__Service_Request__c serReq,
                                                    List<HexaBPM__Step__c> lstStp )
    {
        
        //lstStp
        Boolean isSharingOfOfficeApproved = false;
        //for(HexaBPM__Step__c  stpTemp : serReq.HexaBPM__Steps_SR__r )
        for(HexaBPM__Step__c  stpTemp : lstStp )
        {
            if( stpTemp.Step_Template_Code__c  == 'SHARING_OF_OFFICE_APPROVE'
				 && stpTemp.Current_Step_Status__c == 'Approved'  )
            {
                isSharingOfOfficeApproved = true; 
            }
        }
        return isSharingOfOfficeApproved;
        
       // return true;
    }
    
    
    public static List<HexaBPM_Amendment__c> getExistingAmendment(String srId,String amedType)
    {
        return [SELECT id, 
                       IsRegistered__c , 
                       Unit__c , 
                       Hosting_Company__c , 
                       Operating_Type__c , 
                       Operating_Location__c , 
                       Operating_Location_Status__c, 
                       Unit__r.Name, 
                       Unit__r.Floor__c, 
                       Unit__r.Building__r.Name
                 FROM HexaBPM_Amendment__c
                WHERE ServiceRequest__c = :srId
                  AND Amendment_Type__c = :amedType 
                  AND Status__c != 'Inactive'];
                  // 'Operating Location'
        
    }


   

    public static List<Operating_Location__c> getActiveAndRegisteredOpertingLocation(String accId) 
    {
        List<Operating_Location__c> lstActvRegOptLoc = new List<Operating_Location__c>();
        for(Operating_Location__c oprLoc :getOpertingLoactionBasedOnAccId(accId)) 
        {
            if(oprLoc.Status__c == 'Active' && (oprLoc.Type__c == 'Purchased' || oprLoc.Type__c == 'Leased') ) 
            {
                lstActvRegOptLoc.add(oprLoc);
            }

        }
        return lstActvRegOptLoc;

    }


    public static List<Operating_Location__c> getOwnOpertingLoaction(String accId) 
    {
        List<Operating_Location__c> ownOprLoactionLst = new List<Operating_Location__c>();

        system.debug('######## getOpertingLoactionBasedOnAccId(accId) ' + getOpertingLoactionBasedOnAccId(accId));
        for(Operating_Location__c oprLoc :getOpertingLoactionBasedOnAccId(accId)) {
            system.debug('######## oprLoc ' + oprLoc);
            if(oprLoc.Status__c == 'Active' && (oprLoc.Type__c == 'Purchased' || oprLoc.Type__c == 'Leased')) {
                ownOprLoactionLst.add(oprLoc);
            }

        }
        return ownOprLoactionLst;

    }

    public static List<Operating_Location__c> getOpertingLoactionBasedOnAccId(String accId) {


        return [SELECT id, 
                        Account__c, 
                        Account_Office__c, 
                        Account_Unit_Unique__c, 
                        Area__c, 
                        Area_WF__c, 
                        Building_Name__c, 
                        Floor__c, 
                        Hosting_Company__c, 
                        Is_License_to_Occupy__c, 
                        Lease__c, 
                        Lease_Partner__c, 
                        Lease_Types__c, 
                        IsRegistered__c, 
                        Status__c, 
                        Type__c, 
                        Unique_Id__c, 
                        Unit__c, 
                        Unit__r.Name, 
                        Unit_Usage_Type__c
                FROM Operating_Location__c
                WHERE Account__c = :accId];

    }




    public class SRWrapper {
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        @AuraEnabled public List<AmendmentWrapper> amedWrapLst { get; set; }
        @AuraEnabled public String viewSRURL { get; set; }
        @AuraEnabled public Boolean isDraft { get; set; }


        public SRWrapper() {
        }
    }

    public class AmendmentWrapper {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public Id amedID { get; set; }

        //lookupLabel

        public AmendmentWrapper() {
        }
    }

    //Operating_Location__c
    public class OperationLocationWrapper {
        @AuraEnabled public Operating_Location__c oprLocObj { get; set; }

        //lookupLabel

        public OperationLocationWrapper() {
        }
    }

    public class RequestWrapper {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId { get; set; }
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public SRWrapper srWrap { get; set; }
        @AuraEnabled public OperationLocationWrapper oprLocWrap { get; set; }

        @AuraEnabled public AmendmentWrapper amedWrap { get; set; }


        public RequestWrapper() {
        }
    }

    public class ResponseWrapper {
        @AuraEnabled public List<OperationLocationWrapper> lstOprLocWrap { get; set; }
        @AuraEnabled public SRWrapper srWrap { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public Boolean askForCSP { get; set; }
        @AuraEnabled public HexaBPM__Section_Detail__c ButtonSection { get; set; }


        public ResponseWrapper() {
        }
    }

    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID, 
                                                        string ButtonId, 
                                                        string pageId,
                                                     SRWrapper srWrap
                                                       ) 
    {


        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        HexaBPM__Service_Request__c objRequestParam = ( srWrap != NULL ? srWrap.srObj : NULL) ;
        
        
        HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
        PageFlowControllerHelper.objSR = objRequest;
        //system.debug('@@@@@@@  objRequestParam.Location_of_records_and_registers__c '+ objRequestParam.Location_of_records_and_registers__c);
        
        //overriding the UI with the DB one.
        objRequest.Location_of_records_and_registers__c = (objRequestParam != NULL && objRequestParam.Location_of_records_and_registers__c != NULL  ? objRequestParam.Location_of_records_and_registers__c : objRequest.Location_of_records_and_registers__c);
        objRequest.Records_and_registers_held_by__c = (objRequestParam != NULL && objRequestParam.Records_and_registers_held_by__c != NULL  ? objRequestParam.Records_and_registers_held_by__c : objRequest.Records_and_registers_held_by__c);
        
        system.debug('@@@@@@@  objRequest.Location_of_records_and_registers__c '+ objRequest.Location_of_records_and_registers__c);
        
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();

        objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
        upsert objRequest;
        OB_RiskMatrixHelper.CalculateRisk(SRID);

        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;

        system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
        return respWrap;
    }
    
   
    
    
    public class ButtonResponseWrapper 
    {
        @AuraEnabled public String pageActionName { get; set; }
        @AuraEnabled public string communityName { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public string CommunityPageName { get; set; }
        @AuraEnabled public string sitePageName { get; set; }
        @AuraEnabled public string strBaseUrl { get; set; }
        @AuraEnabled public string srId { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public boolean isPublicSite { get; set; }

    }

}