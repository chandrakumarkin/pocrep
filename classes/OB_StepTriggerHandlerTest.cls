@isTest
public class OB_StepTriggerHandlerTest {
	@testSetup
    static void createTestData() {
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        insert listPage;
        
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[0].id;
        insert insertNewSRs;
        
        
    }
    
   
    
    static testMethod void stepTriggerTest() 
    {
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = [select id from HexaBPM__Service_Request__c];
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = [select id from HexaBPM__SR_Steps__c];
        HexaBPM__Status__c statusVal = new HexaBPM__Status__c(Name = 'TEST',HexaBPM__Code__c = 'TEST',HexaBPM__Type__c = 'Start');
    	insert statusVal;
        
        HexaBPM__Status__c statusVal1 = new HexaBPM__Status__c(Name = 'TEST',HexaBPM__Code__c = 'REQUEST_REJECTED',HexaBPM__Type__c = 'Start',HexaBPM__Rejection__c=true);
    	insert statusVal1;
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insert insertNewSteps;
        List<HexaBPM__Step__c> insertNewSteps1 = new List<HexaBPM__Step__c>();
		insertNewSteps1 = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
		insertNewSteps1[0].HexaBPM__Status__c = statusVal.Id;
		insertNewSteps1[0].HexaBPM__SR__c = insertNewSRs[0].Id;
		insertNewSteps1[1].HexaBPM__Status__c = statusVal.Id;
		insertNewSteps1[1].HexaBPM__SR__c = insertNewSRs[0].Id;
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insert insertNewSteps1;
        OB_StepTriggerHelper.CloseOpenStepsonRejection(insertNewSRs[0].Id,insertNewSteps[0].Id);
    }


    static testMethod void calculateDueDateTest() 
    {
        
        String userId = UserInfo.getUserId();
        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];

        OBBusinessHour__c bussHrMD = new OBBusinessHour__c();
        bussHrMD.Name = 'Test';
        bussHrMD.Business_Hour_Id__c = bhs[0].Id;
        bussHrMD.Queue_Id__c = userId;
        insert bussHrMD;
        
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = [select id from HexaBPM__Service_Request__c];
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = [select id from HexaBPM__SR_Steps__c];
        listSRSteps[0].HexaBPM__Estimated_Hours__c=2;
        update listSRSteps[0];
       
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(1, insertNewSRs, listSRSteps);
        insertNewSteps[0].ownerId = userId;
        insert insertNewSteps;
        
         insertNewSteps[0].HexaBPM__Closed_Date_Time__c = system.now();
         update insertNewSteps[0];
        //Map<Id,HexaBPM__Step__c> mapStepDB = new Map<Id,HexaBPM__Step__c>(insertNewSteps);
        //OB_StepTriggerHandler.calculateDueDate(insertNewSteps,mapStepDB);
    }



}