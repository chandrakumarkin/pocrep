/******************************************************************************************************
 *  Author   : Kumar Utkarsh
 *  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* Mock Class to fetch Application Status details.
*******************************************************************************************************/
@isTest
global class WebServiceGdrfaAppStatusPrintVisaMock implements HttpCalloutMock{
   //Implement http mock callout here
   // Implement this interface method
   global HTTPResponse respond(HTTPRequest request){
       // Create a fake response
       HttpResponse response = new HttpResponse();
       response.setStatusCode(200);
       response.setBody('{"DocumentsList": [{ "documentTypeId": "50f41f94-b59b-4482-871e-2eff765aa5ba","document":"62728","IsToBeReplaced":true,"IsMandatory": true,"MimeType": "2","documentNameEn": "Sponsored Passport page 2","documentNameAr": "جواز المكفول صفحة 2","Reason": "Document name is missing","IsNew": true}],"IssueDate": "2018-05-09","ExpiryDate": "2021-05-09","errorCode": null,"fileName": "20000000000007_eVisa.pdf","FileType":"1","reportContentAsBase64String": "Base64Content","ApplicationNumber": "20000000000007", "StatesCode": "ADDITIONALDOCREQUIRED", "PaidOn": "2021-02-03T11:20:37Z", "RefernceNumber": "S2PXZ1612351237336", "TransactionId": "40425", "ApplicantName": "Test Name", "CurrentStatus": "Approved", "ServiceName": "New Work Entry Permit", "FileNo": "2013610090909", "IsSuccess": true, "access_token": "eyJc0MDU1RjVFMTBDQjRFRTI0QjZDMUQiLg","expires_in": 1,"token_type": "Bearer","IsValid": "False", "errorMsg": [ { "messageEn": "Fail to submit the application, please make sure that the application has been paid and all of the mandatory documents have been uploaded","messageAr": "فشل في تقديم الطلب ، يرجى التأكد من أنه قد تم دفع الطلب وتم تحميل جميع المستندات الإلزامية"}]}');     
       return response;
   }

}