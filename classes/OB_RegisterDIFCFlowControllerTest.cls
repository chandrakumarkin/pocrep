@isTest
public class OB_RegisterDIFCFlowControllerTest{
    public static testmethod void test1(){
        
        // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'GENERAL'});
        insert listDocMaster;
        // create SR Template doc
        
         //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle','AOR_Financial'});
        insert createSRTemplateList;
        
        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        insert listSRTemplateDoc;
        
         ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        
        // create account
        list<Account> insertNewAccounts = new list<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
       /* List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;*/
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        listPage[0].HexaBPM__Page_Flow__c = listPageFlow[0].id;
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1 = new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        //sec1.Submit_Request__c = true;
        insert sec1;
        
        sec1 = new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='Last_Name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=true;
        //sec1.Submit_Request__c = true;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Non - financial';
        insertNewSRs[1].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule.HexaBPM__Section_Detail__c = sec.id;
        objPgNavRule.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule.HexaBPM__Section__c=section.id;
        objPgNavRule.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule1 = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule1.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule1.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule1.HexaBPM__Section_Detail__c = sec1.id;
        objPgNavRule1.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule1.HexaBPM__Section__c=section.id;
        objPgNavRule1.HexaBPM__Rule_Type__c='Navigation Rule';
        insert ObjPgNavRule1;
       
        HexaBPM__Page_Flow_Condition__c ObjPgFlowCond = new HexaBPM__Page_Flow_Condition__c();
        ObjPgFlowCond.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        ObjPgFlowCond.HexaBPM__Field_Name__c = 'Id';
        ObjPgFlowCond.HexaBPM__Operator__c = '!='; 
        ObjPgFlowCond.HexaBPM__Value__c = 'null';
        ObjPgFlowCond.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id; 
        insert ObjPgFlowCond;
        
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id;
        insert ObjPgFlowAction;
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction1 = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction1.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule1.id;
        ObjPgFlowAction1.HexaBPM__Page_Flow__c = listPageFlow[0].id;
        ObjPgFlowAction1.HexaBPM__Page__c = listPage[0].id;
        insert ObjPgFlowAction1;
        
        // create SR Doc
        //List<HexaBPM__SR_Doc__c> listSRDoc = new List<HexaBPM__SR_Doc__c>();
        //listSRDoc = OB_TestDataFactory.createSRDoc(1, insertNewSRs, listDocMaster, listSRTemplateDoc);
        //insert listSRDoc;
        
        test.startTest();
            OB_RegisterDIFCFlowController.RequestWrapper reqWrapper = new OB_RegisterDIFCFlowController.RequestWrapper();
            reqWrapper.flowId = listPageFlow[0].Id;
            reqWrapper.pageId = listPage[0].Id;
            reqWrapper.srId = insertNewSRs[0].Id;
            String requestString = JSON.serialize(reqWrapper);
            OB_RegisterDIFCFlowController.getFlowPages(requestString);
        
        //-----------------------------------------------------------------------------------------------------------------------
        
            OB_RegisterDIFCPagesController.RequestWrapper reqWrapperObj = new OB_RegisterDIFCPagesController.RequestWrapper();
            reqWrapperObj.cmdActionId = sec1.id;
            //reqWrapperObj.docMasterContentDocMap
            //reqWrapperObj.documentId
            //reqWrapperObj.fieldApi
            //reqWrapperObj.fieldVal
            reqWrapperObj.flowId = listPageFlow[0].Id;
           // reqWrapperObj.objRequest
            //reqWrapperObj.objRequestMap
            reqWrapperObj.pageId = listPage[0].Id;
           // reqWrapperObj.RequestWrapper()
           // reqWrapperObj.secObjID
            reqWrapperObj.srId = insertNewSRs[0].Id;
        	reqWrapperObj.docMasterContentDocMap = new Map<String, String>{'GENERAL'=>documents[0].Id};
            String reqString = JSON.serialize(reqWrapperObj);
            OB_RegisterDIFCPagesController.getFlowContent(reqString);
            //OB_RegisterDIFCPagesController.getFlowPageContent(reqString);
            OB_RegisterDIFCPagesController.typeCastFlds('Entity_Type__c','Financial - related',insertNewSRs[0]);
            OB_RegisterDIFCPagesController.dynamicButtonAction(reqString);
            OB_RegisterDIFCPagesController.reparentToSRDocs(reqWrapperObj);
            OB_RegisterDIFCPagesController.getForm(reqString);
            OB_RegisterDIFCPagesController.evaluateOnChange(reqString);
        //---------------------------------------------------------------------------------------------------------------------
            OB_RegisterWithDIFCController.getServiceEmailAddress(insertNewSRs[0].Id);
            OB_RegisterWithDIFCController.saveService(insertNewSRs[0]);
        //------------------------------------------------------------------------------------------------------------
            
        test.stopTest();
    }
    public static testmethod void PageFlowControllerHelper(){
    	
    	list<Account> insertNewAccounts = new list<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
    	//create createSRTemplate
        list<HexaBPM__SR_Template__c> createSRTemplateList = new list<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].HexaBPM__Render_by_Default__c = true;
        listPage[0].Community_Page__c = 'test';
        listPage[0].HexaBPM__Page_Flow__c = listPageFlow[0].id;
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1 = new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='first';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        //sec1.Submit_Request__c = true;
        insert sec1;
        
        HexaBPM__Section_Detail__c sec2 = new HexaBPM__Section_Detail__c();
        sec2.HexaBPM__Section__c=section1.id;
        sec2.HexaBPM__Component_Type__c='Input Field';
        sec2.HexaBPM__Field_API_Name__c='Last_Name__c';
        sec2.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec2.HexaBPM__Default_Value__c='Last';
        sec2.HexaBPM__Mark_it_as_Required__c=true;
        sec2.HexaBPM__Field_Description__c = 'desc';
        sec2.HexaBPM__Render_By_Default__c=true;
        //sec2.Submit_Request__c = true;
        insert sec2;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_NF_R', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule.HexaBPM__Section_Detail__c = sec.id;
        objPgNavRule.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule.HexaBPM__Section__c=section.id;
        objPgNavRule.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule1 = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule1.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule1.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule1.HexaBPM__Section_Detail__c = sec1.id;
        objPgNavRule1.HexaBPM__Rule_Text_Condition__c='#(#HexaBPM__Service_Request__c->Id#!=#Null#)##OR##(#HexaBPM__Service_Request__c->Id#!=#Null#AND#HexaBPM__Service_Request__c->Id#!=#Null#)#';  
        objPgNavRule1.HexaBPM__Section__c=section.id;
        objPgNavRule1.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule1;
       
        HexaBPM__Page_Flow_Condition__c ObjPgFlowCond = new HexaBPM__Page_Flow_Condition__c();
        ObjPgFlowCond.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        ObjPgFlowCond.HexaBPM__Field_Name__c = 'Id';
        ObjPgFlowCond.HexaBPM__Operator__c = '!='; 
        ObjPgFlowCond.HexaBPM__Value__c = 'null';
        ObjPgFlowCond.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id; 
        insert ObjPgFlowCond;
        
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id;
        insert ObjPgFlowAction;
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction1 = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction1.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule1.id;
        ObjPgFlowAction1.HexaBPM__Page_Flow__c = listPageFlow[0].id;
        ObjPgFlowAction1.HexaBPM__Page__c = listPage[0].id;
        insert ObjPgFlowAction1;
        
        PageFlowControllerHelper helperObj = new PageFlowControllerHelper();
        PageFlowControllerHelper.objSR = insertNewSRs[0]; 
        helperObj.getReviewConfirmContent(listPageFlow[0].Id,'AOR_NF_R');
        PageFlowControllerHelper.getDisplayPageIdsMap(listPageFlow[0].Id, insertNewSRs[0]);
        PageFlowControllerHelper.getHiddenPageIdsMap(listPageFlow[0].Id, insertNewSRs[0]);
        helperObj.getLightningButtonAction(sec1.Id);
        PageFlowControllerHelper.getBaseUrl();
        PageFlowControllerHelper.communityURLFormation(insertNewSRs[0],listPageFlow[0].Id,'test');
        PageFlowControllerHelper.communityURLFormation(insertNewSRs[0],listPageFlow[0].Id,listPage[0].Id,'test');
        helperObj.getSideBarReference(listPage[0].Id);
        PageFlowControllerHelper.ConvertToDecimal('1234.56');
        PageFlowControllerHelper.ConvertToDate('25/12/2018');
        PageFlowControllerHelper.ConvertToDateTime('11/20/2010 12:00:00');
        PageFlowControllerHelper.ConvertToBoolean('true');
        PageFlowControllerHelper.ConvertToBoolean('false');
        
    }
    public static testmethod void PageFlowControllerHelper2(){
    	
    	list<Account> insertNewAccounts = new list<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
    	//create createSRTemplate
        list<HexaBPM__SR_Template__c> createSRTemplateList = new list<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        listPage[0].HexaBPM__Page_Flow__c = listPageFlow[0].id;
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1 = new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='first';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        //sec1.Submit_Request__c = true;
        insert sec1;
        
        HexaBPM__Section_Detail__c sec2 = new HexaBPM__Section_Detail__c();
        sec2.HexaBPM__Section__c=section1.id;
        sec2.HexaBPM__Component_Type__c='Input Field';
        sec2.HexaBPM__Field_API_Name__c='Last_Name__c';
        sec2.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec2.HexaBPM__Default_Value__c='Last';
        sec2.HexaBPM__Mark_it_as_Required__c=true;
        sec2.HexaBPM__Field_Description__c = 'desc';
        sec2.HexaBPM__Render_By_Default__c=true;
        //sec2.Submit_Request__c = true;
        insert sec2;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_NF_R', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Non - financial';
        insertNewSRs[1].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get(Label.Self_Registration_Record_Type).getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
         
        insert insertNewSRs;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule.HexaBPM__Section_Detail__c = sec.id;
        objPgNavRule.HexaBPM__Rule_Text_Condition__c='HexaBPM__Service_Request__c->Id#!=#null';  
        objPgNavRule.HexaBPM__Section__c=section.id;
        objPgNavRule.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule;
        
        HexaBPM__Page_Navigation_Rule__c objPgNavRule1 = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule1.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule1.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule1.HexaBPM__Section_Detail__c = sec1.id;
        objPgNavRule1.HexaBPM__Rule_Text_Condition__c='#(#HexaBPM__Service_Request__c->Id#!=#Null#)##OR##(#HexaBPM__Service_Request__c->Id#NOT CONTAINS#Null#AND#HexaBPM__Service_Request__c->Id#!=#Null#)#';  
        objPgNavRule1.HexaBPM__Section__c=section.id;
        objPgNavRule1.HexaBPM__Rule_Type__c='Render Rule';
        insert ObjPgNavRule1;
        
        objPgNavRule1 = new HexaBPM__Page_Navigation_Rule__c();
        objPgNavRule1.HexaBPM__Rule_Name__c = 'Page Rule';
        objPgNavRule1.HexaBPM__Page__c = listPage[0].id;
        objPgNavRule1.HexaBPM__Section_Detail__c = sec1.id;
        objPgNavRule1.HexaBPM__Rule_Text_Condition__c='#(#HexaBPM__Service_Request__c->Id#!=#Null#)##OR##(#HexaBPM__Service_Request__c->Id#NOT CONTAINS#Null#AND#HexaBPM__Service_Request__c->Id#!=#Null#)#';  
        objPgNavRule1.HexaBPM__Section__c=section.id;
        objPgNavRule1.HexaBPM__Rule_Type__c='Navigation Rule';
        insert ObjPgNavRule1;
       
        HexaBPM__Page_Flow_Condition__c ObjPgFlowCond = new HexaBPM__Page_Flow_Condition__c();
        ObjPgFlowCond.HexaBPM__Object_Name__c = 'HexaBPM__Service_Request__c';
        ObjPgFlowCond.HexaBPM__Field_Name__c = 'Id';
        ObjPgFlowCond.HexaBPM__Operator__c = '!='; 
        ObjPgFlowCond.HexaBPM__Value__c = 'null';
        ObjPgFlowCond.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id; 
        insert ObjPgFlowCond;
        
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule.id;
        insert ObjPgFlowAction;
        HexaBPM__Page_Flow_Action__c ObjPgFlowAction1 = new HexaBPM__Page_Flow_Action__c();
        ObjPgFlowAction1.HexaBPM__Page_Navigation_Rule__c = ObjPgNavRule1.id;
        ObjPgFlowAction1.HexaBPM__Page_Flow__c = listPageFlow[0].id;
        ObjPgFlowAction1.HexaBPM__Page__c = listPage[0].id;
        insert ObjPgFlowAction1;
        
        PageFlowControllerHelper helperObj = new PageFlowControllerHelper();
        PageFlowControllerHelper.objSR = insertNewSRs[0]; 
        helperObj.getReviewConfirmContent(listPageFlow[0].Id,'AOR_NF_R');
        PageFlowControllerHelper.getDisplayPageIdsMap(listPageFlow[0].Id, insertNewSRs[0]);
        PageFlowControllerHelper.getHiddenPageIdsMap(listPageFlow[0].Id, insertNewSRs[0]);
        helperObj.getLightningButtonAction(sec1.Id);
        PageFlowControllerHelper.getBaseUrl();
        PageFlowControllerHelper.communityURLFormation(insertNewSRs[0],listPageFlow[0].Id,'test');
        PageFlowControllerHelper.communityURLFormation(insertNewSRs[0],listPageFlow[0].Id,listPage[0].Id,'test');
        helperObj.getSideBarReference(listPage[0].Id);
        PageFlowControllerHelper.ConvertToDecimal('1234.56');
        PageFlowControllerHelper.ConvertToDate('25/12/2018');
        PageFlowControllerHelper.ConvertToDateTime('11/20/2010 12:00:00');
        PageFlowControllerHelper.ConvertToBoolean('true');
        PageFlowControllerHelper.ConvertToBoolean('false');
        
        PageFlowControllerHelper.parseEachCondition('10','=','10');
        PageFlowControllerHelper.parseEachCondition('10','!=','10');
        PageFlowControllerHelper.parseEachCondition('10','>=','10');
        PageFlowControllerHelper.parseEachCondition('10','CONTAINS','10');
        
        PageFlowControllerHelper.responseWrapper objRespWrap = new PageFlowControllerHelper.responseWrapper();
        objRespWrap.communityName = '';
        objRespWrap.CommunityPageName = '';
        objRespWrap.sitePageName = '';
        objRespWrap.strBaseUrl = '';
        objRespWrap.srId = '';
        objRespWrap.flowId = '';
        objRespWrap.pageId = '';
        objRespWrap.pg = '';
        objRespWrap.isPublicSite = true;
        
    }
}