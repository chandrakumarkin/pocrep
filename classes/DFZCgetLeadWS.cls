@RestResource(urlMapping='/api/GetLead/*')
global with sharing class DFZCgetLeadWS{

     
    @HttpPost
    global static DFZCCreateResponce doPost() 
    {
    
        RestRequest req = RestContext.request;
        
        
        System.debug('===============doPost=============================');
        System.debug('=====PushedLeads=>'+req.requestBody.toString());
        string postBody=req.requestBody.toString();
        
          List<string> LeadIds=new List<string>();
        
        List<LeadsStatus> tempLeadsStatus= (List<LeadsStatus>)JSON.deserialize(postBody,List<LeadsStatus>.class);
        
        for(LeadsStatus TempObj:tempLeadsStatus)
          LeadIds.add(TempObj.leadId);
          
         List<DFZCLead> ListTempLead=new List<DFZCLead>();
         
         System.debug('==LeadIds=='+LeadIds);
         
         DFZCCreateResponce ObjDFZCCreateResponce =new DFZCCreateResponce ();
         ObjDFZCCreateResponce.isSuccess=true;
         ObjDFZCCreateResponce.errorMessage='';
         
         List<lead> ListLeads=[select Status,Milestone_ID__c,gdprConsent__c,ConvertedAccountId,SubZone__c,Sector_Classification__c,Company_Type__c,Place_of_Trip__c,FirstName,LastName,MobilePhone,Email,Preference__c,Company,Your_Enquiry__c,CreatedDate,LastModifiedDate
        from lead where Milestone_ID__c in :LeadIds];
        
       // Map<id,Account> MapAccounts=new Map<id,Account>();
        Set<Id> ListAccountIds=new Set<Id>();
        
        for(Lead ObjLead:ListLeads)
        {
            if(ObjLead.ConvertedAccountId!=null)
            ListAccountIds.add(ObjLead.ConvertedAccountId);
        }
        
        Map<id,Account> MapAccounts=new Map<id,Account>([select id,Name,ROC_Status__c from account where id in: ListAccountIds and ROC_Status__c='Active' ]);
         
         for(Lead ObjLead:ListLeads)
        {
            DFZCLead TempLead=new DFZCLead();
            TempLead.leadId=ObjLead.Milestone_ID__c;
            TempLead.firstName=ObjLead.FirstName;
            TempLead.lastName=ObjLead.LastName;
            TempLead.phoneNumber=ObjLead.MobilePhone;
            TempLead.email=ObjLead.Email;
            TempLead.contactPreference=ObjLead.Preference__c;
            if(!string.isblank(ObjLead.Company))
            TempLead.companyName=ObjLead.Company;
            TempLead.additionalComments=ObjLead.Your_Enquiry__c;
            
            if(ObjLead.Sector_Classification__c!=null)
            TempLead.areaOfInterest=ObjLead.Sector_Classification__c.split(',');
            
            TempLead.ipAddress=ObjLead.Place_of_Trip__c;
            
             TempLead.subZoneName=ObjLead.SubZone__c;
             
             TempLead.submittedOn=ObjLead.CreatedDate;
            
            TempLead.gdprConsent= ObjLead.gdprConsent__c;
             

            TempLead.ipAddress=ObjLead.Place_of_Trip__c;
            
            if(ObjLead.ConvertedAccountId!=null)
            {
                if(MapAccounts.ContainsKey(ObjLead.ConvertedAccountId))
                TempLead.status='Qualified';
                else
                TempLead.status='In Progress';
            }
        
        else if(ObjLead.Status.startsWith('No') || ObjLead.Status=='Invalid' ||ObjLead.Status=='Rejected')
            TempLead.status='Disqualified';
        else if(ObjLead.Status!='Open')
                TempLead.status='In Progress';
        else        
            TempLead.status=ObjLead.Status;
        
        
            TempLead.updatedOn=ObjLead.LastModifiedDate;
           
           ListTempLead.add(TempLead);
            
            
            }
  
         
       ObjDFZCCreateResponce.Leads=ListTempLead;
     return ObjDFZCCreateResponce;
    }
    
 
 public  class  LeadsStatus
{
    String leadId{get;set;}
}
 global class  DFZCCreateResponce
{
    boolean isSuccess{get;set;}
    string errorMessage{get;set;}
    List<DFZCLead> Leads{get;set;}
    
}



global  class  DFZCLead
{
    string leadId{get;set;}
    string firstName{get;set;}
    string lastName{get;set;}
    string phoneNumber{get;set;}
    string email{get;set;}
    List<string> areaOfInterest{get;set;}
    string subZoneName{get;set;}
    string ipAddress{get;set;}
    string contactPreference{get;set;}
    string companyName{get;set;}
    string additionalComments{get;set;}
    Datetime submittedOn{get;set;}
    
    //Only use in case of get Lead details
    string status{get;set;}
    Datetime updatedOn{get;set;}
boolean gdprConsent{get;set;} 
    
    
}
    
}