global without sharing class ROS_CreateRelationshipsOnApproval implements HexaBPM.iCustomCodeExecutable {
    
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR,HexaBPM__Step__c stp) {
        string strResult = 'Success';
        try{
            system.debug('stp.HexaBPM__SR__c'+stp.HexaBPM__SR__c);
            String ServiceRequestId= stp.HexaBPM__SR__c;
            string querySoql = UtilitySoqlHelper.AllFields('Approved_Form__c');
            querySoql += ' Where Service_Request__c =: ServiceRequestId order by Createddate desc limit 1';
            List<Approved_Form__c> alList = Database.query(querySoql);
            system.debug('alList::'+alList);
            List<ROS_Relationship__c> newRelList = new List<ROS_Relationship__c>();
            map<String, ROS_Relationship__c> rosRelMap = new map<String, ROS_Relationship__c>();
            for(ROS_Relationship__c RosRelObj :[Select Id, Account__c,Amendment_Id__c,Financial_Statement_Number__c,Original_Service_Request__c, Active__c, City__c,Shareholder_Type__c, Country__c, Description__c, End_Date__c, Jurisdiction_Of_Organization__c, Mailing_Address__c,Organizational_ID__c,Organization_Name__c,Reference_Service_Request__c, Start_Date__c, Status__c,Type__c, Type_Of_Organization__c, Value_of_Collateral__c,No_Change_in_Collateral_Value__c
                                                from ROS_Relationship__c where Original_Service_Request__c =:alList[0].Initial_Financial_Statement__c AND Active__c = true]){
                                                    if(RosRelObj.Original_Service_Request__c != null){
                                                        rosRelMap.put(RosRelObj.Id, RosRelObj);
                                                    }
                                                }
            system.debug('rosRelMap::'+rosRelMap);
            for(Debtor_Secured_Party__c dbtObj :[SELECT id,Approved_Form__c,ROS_Relationship__c, ROS_Relationship__r.Id, Approved_Form__r.Service_Request__c, Approved_Form__r.Initial_Financial_Statement__r.id,Approved_Form__r.SR_Statement_File_No__c, Approved_Form__r.Service_Request__r.Id,Amendment_Id__c, Approved_Form__r.Initial_Financial_Statement__c,City__c, Country__c, First_Name__c,Last_Name__c, Is_Existing__c, Jurisdiction_Of_Organization__c, Middle_Name__c, Mailing_Address__c,
                                                 Organizational_ID__c,Approved_Form__r.Service_Request__r.HexaBPM__Customer__c, Organization_Name__c, Service_Type__c, Shareholder_Type__c, Status__c, Suffix__c, Type_Of_Organization__c, Value_of_Security__c, No_Change_in_Collateral_Value__c from Debtor_Secured_Party__c
                                                 Where Approved_Form__r.Service_Request__c=:stp.HexaBPM__SR__c]){
                                                     System.debug('Test::'+dbtObj);
                                                     //if(rosRelMap != null && rosRelMap.size() > 0){
                                                     if(rosRelMap.get(dbtObj.ROS_Relationship__r.Id) != null){
                                                         system.debug('testhjkj');
                                                         ROS_Relationship__c relObj =  rosRelMap.get(dbtObj.ROS_Relationship__r.Id);
                                                         if(dbtObj.Status__c == 'Removed'){
                                                             relObj.Status__c =  'Removed';
                                                             relObj.Active__c = false;
                                                             relObj.No_Change_in_Collateral_Value__c = dbtObj.No_Change_in_Collateral_Value__c;
                                                             relObj.Account__c = dbtObj.Approved_Form__r.Service_Request__r.HexaBPM__Customer__c;
                                                             relObj.End_Date__c = date.today();
                                                             if(dbtObj.Approved_Form__r.Initial_Financial_Statement__r != null){
                                                                 relObj.Original_Service_Request__c = dbtObj.Approved_Form__r.Initial_Financial_Statement__c;}
                                                             if(dbtObj.Approved_Form__r.Service_Request__c != null){
                                                                 relObj.Reference_Service_Request__c = dbtObj.Approved_Form__r.Service_Request__c;}
                                                             if(dbtObj.Approved_Form__r.SR_Statement_File_No__c != null){
                                                                 relObj.Financial_Statement_Number__c = dbtObj.Approved_Form__r.SR_Statement_File_No__c;}
                                                             newRelList.add(relObj);
                                                         }if(dbtObj.Status__c == 'Updated'){
                                                             relObj.Status__c =  'Updated';
                                                             relObj.Active__c = true;
                                                             relObj.No_Change_in_Collateral_Value__c = dbtObj.No_Change_in_Collateral_Value__c;
                                                             relObj.Start_Date__c = date.today(); 
                                                             if(dbtObj.Approved_Form__r.Initial_Financial_Statement__r != null){
                                                                 relObj.Original_Service_Request__c = dbtObj.Approved_Form__r.Initial_Financial_Statement__c;}
                                                             if(dbtObj.Approved_Form__r.Service_Request__c != null){
                                                                 relObj.Reference_Service_Request__c = dbtObj.Approved_Form__r.Service_Request__c;}
                                                             newRelList.add(relObj);
                                                         }
                                                         
                                                         //} 
                                                         
                                                     }else if(rosRelMap.get(dbtObj.ROS_Relationship__r.Id) == null){
                                                         ROS_Relationship__c rosRel = new ROS_Relationship__c();
                                                         system.debug('testhjkj9879');
                                                         if(dbtObj.Approved_Form__r.Service_Request__r.HexaBPM__Customer__c != null){
                                                             rosRel.Account__c = dbtObj.Approved_Form__r.Service_Request__r.HexaBPM__Customer__c;}
                                                         rosRel.Active__c = true;
                                                         rosRel.Amendment_Id__c = dbtObj.Id;
                                                         if(dbtObj.City__c != '' && dbtObj.City__c != null){
                                                             rosRel.City__c = dbtObj.City__c;}
                                                         if(dbtObj.Country__c != '' && dbtObj.Country__c != null){
                                                             rosRel.Country__c = dbtObj.Country__c;}
                                                         if(dbtObj.First_Name__c != '' && dbtObj.First_Name__c != null){
                                                             rosRel.first_name__c = dbtObj.First_Name__c;}
                                                         if(dbtObj.last_name__c != '' && dbtObj.last_name__c != null){
                                                             rosRel.last_name__c = dbtObj.Last_name__c;}
                                                         if(dbtObj.Approved_Form__c != null){
                                                             rosRel.Reference_Service_Request__c = dbtObj.Approved_Form__r.Service_Request__r.Id;}
                                                         if(dbtObj.Approved_Form__c != null && dbtObj.Approved_Form__r.Initial_Financial_Statement__c != null){
                                                             system.debug('testABC');
                                                             rosRel.Original_Service_Request__c = dbtObj.Approved_Form__r.Initial_Financial_Statement__r.id;}
                                                         if(dbtObj.Approved_Form__c != null && dbtObj.Approved_Form__r.Initial_Financial_Statement__c == null){
                                                             system.debug('testABCD');
                                                             rosRel.Original_Service_Request__c = dbtObj.Approved_Form__r.Service_Request__r.id;
                                                         }
                                                         if(dbtObj.Approved_Form__r.SR_Statement_File_No__c != null){
                                                             rosRel.Financial_Statement_Number__c = dbtObj.Approved_Form__r.SR_Statement_File_No__c;}
                                                         if(dbtObj.No_Change_in_Collateral_Value__c != null){
                                                             rosRel.No_Change_in_Collateral_Value__c = dbtObj.No_Change_in_Collateral_Value__c;}
                                                         if(dbtObj.Organizational_ID__c != null && dbtObj.Organizational_ID__c != ''){
                                                             rosRel.Organizational_ID__c = dbtObj.Organizational_ID__c;}
                                                         if(dbtObj.Organization_Name__c != null && dbtObj.Organization_Name__c != ''){
                                                             rosRel.Organization_Name__c = dbtObj.Organization_Name__c;}
                                                         if(dbtObj.Shareholder_Type__c != null  && dbtObj.Shareholder_Type__c != ''){
                                                             rosRel.Shareholder_Type__c = dbtObj.Shareholder_Type__c;}
                                                         if(dbtObj.Service_Type__c != null && dbtObj.Service_Type__c != ''){
                                                             rosRel.Type__c = dbtObj.Service_Type__c;}
                                                         if(dbtObj.Status__c != null && dbtObj.Status__c != ''){
                                                             rosRel.Status__c = dbtObj.Status__c;}
                                                         if(dbtObj.Suffix__c != null && dbtObj.Suffix__c != ''){
                                                             rosRel.Suffix__c = dbtObj.Suffix__c;}
                                                         if(dbtObj.Type_Of_Organization__c != null && dbtObj.Type_Of_Organization__c != ''){
                                                             rosRel.Type_Of_Organization__c = dbtObj.Type_Of_Organization__c;}
                                                         rosRel.Active__c = true;
                                                         rosRel.Status__c = 'Active';
                                                         rosRel.Start_Date__c = date.today();
                                                         //if(dbtObj.Value_of_Collateral__c != null && dbtObj.Value_of_Collateral__c != ''){
                                                         //rosRel.Value_of_Collateral__c = dbtObj.Value_of_Collateral__c;}
                                                         newRelList.add(rosRel);
                                                     }
                                                     
                                                 }
            try{
                system.debug('newRelList::::'+newRelList);
                Upsert newRelList;
                system.debug('newRelList::'+newRelList);
            }catch (DMLexception e){
                System.debug('message:'+e.getCause());
                strResult = e.getMessage()+'';
            Log__c log = new Log__c();
             log.Description__c = 'ROS2'+ e.getMessage()+'lnumber'+e.getLineNumber()+'cause'+e.getCause()+'type'+e.getTypeName();
            //log.SR_Step__c = stp.Id;
            insert log;
            }
            
            for(Collateral_Detail__c dbtObj :[SELECT id,Approved_Form__c, ROS_Relationship__c, ROS_Relationship__r.Id,Approved_Form__r.SR_Statement_File_No__c, Approved_Form__r.Service_Request__c,Approved_Form__r.Service_Request__r.HexaBPM__Customer__c, Approved_Form__r.Initial_Financial_Statement__r.id, Approved_Form__r.Service_Request__r.Id,Amendment_Id__c, Approved_Form__r.Initial_Financial_Statement__c, Is_Existing__c,
                                              Status__c, Description__c, Collateral_Value__c, No_Change_in_Collateral_Value__c from Collateral_Detail__c
                                              Where Approved_Form__r.Service_Request__c=:stp.HexaBPM__SR__c]){
                                                  
                                                  //if(rosRelMap != null && rosRelMap.size() > 0){
                                                  if(rosRelMap.get(dbtObj.ROS_Relationship__r.Id) != null){
                                                      ROS_Relationship__c relObj =  rosRelMap.get(dbtObj.ROS_Relationship__r.Id);
                                                      if(dbtObj.Status__c == 'Removed'){
                                                          relObj.Status__c =  'Removed';
                                                          relObj.Active__c = false;
                                                          relObj.End_Date__c = date.today();
                                                          if(dbtObj.No_Change_in_Collateral_Value__c != null){
                                                              relObj.No_Change_in_Collateral_Value__c = dbtObj.No_Change_in_Collateral_Value__c;}
                                                          if(dbtObj.Approved_Form__r.SR_Statement_File_No__c != null){
                                                              relObj.Financial_Statement_Number__c = dbtObj.Approved_Form__r.SR_Statement_File_No__c;}
                                                      }if(dbtObj.Status__c == 'Updated'){
                                                          relObj.Status__c =  'Updated';
                                                          relObj.Active__c = true;
                                                          relObj.Start_Date__c = date.today();  
                                                          if(dbtObj.No_Change_in_Collateral_Value__c != null){
                                                              relObj.No_Change_in_Collateral_Value__c = dbtObj.No_Change_in_Collateral_Value__c;}
                                                          if(dbtObj.Approved_Form__r.SR_Statement_File_No__c != null){
                                                              relObj.Financial_Statement_Number__c = dbtObj.Approved_Form__r.SR_Statement_File_No__c;}
                                                      }
                                                      if(relObj != null){
                                                          newRelList.add(relObj);
                                                      }
                                                  } 
                                                  //}
                                                  // else if(rosRelMap == null || rosRelMap.isEmpty()){
                                                  else if((rosRelMap.get(dbtObj.ROS_Relationship__r.Id) == null)){
                                                      ROS_Relationship__c rosRel = new ROS_Relationship__c();
                                                      if(dbtObj.Approved_Form__r.Service_Request__r.HexaBPM__Customer__c != null){
                                                          rosRel.Account__c = dbtObj.Approved_Form__r.Service_Request__r.HexaBPM__Customer__c;}
                                                      rosRel.Active__c = true;
                                                      rosRel.Amendment_Id__c = dbtObj.Id;
                                                      if(dbtObj.Approved_Form__c != null){
                                                          system.debug('teskjktABCDER');
                                                          rosRel.Reference_Service_Request__c = dbtObj.Approved_Form__r.Service_Request__r.Id;}
                                                      if(dbtObj.Approved_Form__r.SR_Statement_File_No__c != null){
                                                          rosRel.Financial_Statement_Number__c = dbtObj.Approved_Form__r.SR_Statement_File_No__c;}
                                                      if(dbtObj.Approved_Form__c != null && dbtObj.Approved_Form__r.Initial_Financial_Statement__c != null){
                                                          system.debug('testABCDER');
                                                          rosRel.Original_Service_Request__c = dbtObj.Approved_Form__r.Initial_Financial_Statement__r.id;}
                                                      if(dbtObj.Approved_Form__c != null && dbtObj.Approved_Form__r.Initial_Financial_Statement__c == null){
                                                          system.debug('testABCDEFG');
                                                          rosRel.Original_Service_Request__c = dbtObj.Approved_Form__r.Service_Request__r.id;
                                                      }
                                                      rosRel.Type__c = 'COLLATERAL';
                                                      if(dbtObj.Status__c != null && dbtObj.Status__c != ''){
                                                          rosRel.Status__c = dbtObj.Status__c;}
                                                      if(dbtObj.Description__c != ''){
                                                          rosRel.Description__c = dbtObj.Description__c;}
                                                      if(dbtObj.Collateral_Value__c != null){
                                                          rosRel.Value_of_Collateral__c = dbtObj.Collateral_Value__c;}
                                                      rosRel.Active__c = true;
                                                      rosRel.Status__c = 'Active';
                                                      rosRel.Start_Date__c = date.today();
                                                      
                                                      if(dbtObj.No_Change_in_Collateral_Value__c != null){
                                                          rosRel.No_Change_in_Collateral_Value__c = dbtObj.No_Change_in_Collateral_Value__c;}
                                                      newRelList.add(rosRel);
                                                  }
                                                  
                                              }
            
            system.debug('newRelList::::'+newRelList);
            Upsert newRelList;
            system.debug('newRelList:'+newRelList);
        }catch (exception e){
            strResult = e.getMessage()+'';
            Log__c log = new Log__c();
            log.Description__c = 'ROS'+ e.getMessage()+'lnumber'+e.getLineNumber()+'cause'+e.getCause()+'type'+e.getTypeName();
            //log.SR_Step__c = stp.Id;
            insert log;
        }
        return strResult;
        
    }
    
}