@isTest
public class VoluntaryStrikeOffBatchTest {
    public static Service_Request__c ServiceRequests{get;set;}
    
    public static void testData(){
        
        string conRT;
        for (RecordType objRT: [select Id, Name, DeveloperName from RecordType where DeveloperName = 'Request_for_Voluntary_Strike_off' AND SobjectType = 'Service_Request__c']){
           conRT = objRT.Id;
        }
            
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        ObjAccount.ROC_status__c='Dissolved - Voluntary Strike off';
        insert objAccount;
        
        SR_Status__c StatusObj = new SR_Status__c();
        StatusObj.Code__c='PENDINGVOLUNTARY';
        StatusObj.Name='Pending Voluntary strike off';
       	insert StatusObj;
        
        SR_Status__c StatusObj2 = new SR_Status__c();
        StatusObj2.Code__c='Approved';
        StatusObj2.Name='Approved';
        insert StatusObj2;
               
        ServiceRequests = new Service_Request__c();
        ServiceRequests.Customer__c = objAccount.Id;
        ServiceRequests.Email__c = 'testsr123@difc.com';
        ServiceRequests.In_accordance_with_Article_10_1__c = true;
        ServiceRequests.In_Accordance_with_Article_11__c = true;
        ServiceRequests.In_Accordance_with_Article_12_1a__c = true;
        ServiceRequests.In_Accordance_with_Article_12_1bj__c = true;
        ServiceRequests.Express_Service__c = false;
        ServiceRequests.RecordtypeId = conRT;
        ServiceRequests.Internal_SR_Status__c = StatusObj.id;
        ServiceRequests.External_SR_Status__c = StatusObj.id;
        insert ServiceRequests;
        
       
       
        Step__c objStep  = new Step__c();
        objStep.SR__c = ServiceRequests.Id;
        insert objStep;
        
    }
    
    static testmethod void positiveTest(){
      testData();
      Test.startTest();
   			try{
                //VoluntaryStrikeOffBatch myBatchObject = new VoluntaryStrikeOffBatch(); 
				//Id batchId = Database.executeBatch(myBatchObject);
				 
                Datetime dt = system.now().addMinutes(1);
                string str = dt.second()+' '+(dt.minute() )+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' '+' ? '+ dt.year();
                system.schedule('ScheduleVoluntaryStrikeOffBatch',str,new ScheduleVoluntaryStrikeOffBatch());
   			}catch(Exception e){
   				system.debug(e.getMessage());
   			}
   		Test.stopTest(); 
    }

}