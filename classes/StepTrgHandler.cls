/******************************************************************************************
 *  StepTrgHandler
 *  Author   : Swati Sehrawat
 *  Company  : NSI JLT
 *  Date     : 21-Jun-2016                         
 ----------------------------------------------------------------------
   Version  Date         Author             Remarks                                                 
  =======   ==========   =============  ==================================
  v1.0      21-Jun-2016  Swati              ticket 2521
  v1.1      28-Jun-2016  Swati              ticket 2521 - added pending step condition
  v1.2      24-Jul-2016  Swati              ticket 2521 - added PO Box Master Condition
  v1.3      23-Feb-2017  Swati              ticket 3732
  V1.4      13-June-2017 Sravan             Create Custom Attachment for Secupass
  V1.5      06-June-2017 Sravan   			Update Arabic fields for Secupass
*******************************************************************************************/


public without sharing class StepTrgHandler implements  TriggerFactoryInterface{

    public void executeBeforeInsertTrigger(list<sObject> lstNewRecords){
        
    }
    
    public void executeBeforeUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
        map<Id,Step__c> mapOldStepRecords = new map<Id,Step__c>();
        mapOldStepRecords = (map<Id,Step__c>)trigger.newMap;
        pendingGSServices(lstNewRecords,mapOldStepRecords);        
    }
    
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
        map<Id,Step__c> mapOldStepRecords = new map<Id,Step__c>();
        if(mapOldRecords != null){
            mapOldStepRecords = (map<Id,Step__c>)mapOldRecords;
            updatePortalUsers(lstNewRecords,mapOldStepRecords); //v1.3 
            
        }
    } 
    
    public void executeAfterInsertTrigger(list<sObject> lstNewRecords){
    }
    
    public void executeAfterUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
    	list<step__c> newRecords = (List<step__c>)lstNewRecords;
    	map<id,step__c> oldRecords = (map<id,step__c>)mapOldRecords; 
    	createCustomAttachment(newRecords,oldRecords);        // V1.4
    	updateArabicFields(newRecords,oldRecords); // V1.5
    }
    
    public void executeAfterInsertUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
    }   
    
    
    //Ticket 2521
    
    public void pendingGSServices(list<Step__c> lstNewRecords,map<Id,Step__c> mapOldRecords){
        list<id> accountIdList = new list<id>();
        
        map<id,string> accountMap = new map<id,string>();
        map<id,integer> POBoxMap = new map<id,integer>();
        map<id,integer> pipelineEmployeeMap = new map<id,integer>();
        map<id,integer> currentEmployeeMap = new map<id,integer>();
        
        for(step__c tempObj : lstNewRecords){
            if((tempObj.SR_Record_Type__c == 'Transfer_from_DIFC' ||  tempObj.SR_Record_Type__c == 'Dissolution' || tempObj.SR_Record_Type__c == 'De_Registration')
                && tempObj.Step_Name__c == 'Clearance from all the Departments' && tempObj.Government_Services__c == true && tempObj.Status_Code__c == 'PENDING_CLEARANCE'){//V1.1
                
                accountIdList.add(tempObj.Customer_Name__c);
            }
        }   
        
        if(accountIdList!=null && !accountIdList.isEmpty()){
            list<account> tempAccountList = new list<account>([select id,Index_Card_Status__c ,
                                                                (select id from PO_Box_Masters__r where Current_Owner__c NOT IN : accountIdList), //  v1.2
                                                                (select Id,AccountId from Contacts where Id IN 
                                                                    (select Object_Contact__c from Relationship__c 
                                                                     where Subject_Account__c in : accountIdList AND Active__c = true AND Document_Active__c = true 
                                                                     AND End_Date__c > Today AND (Relationship_Type__c = 'Has DIFC Seconded' 
                                                                     OR Relationship_Type__c = 'Has DIFC Local /GCC Employees' 
                                                                     OR Relationship_Type__c = 'Has Temporary Employee') ) 
                                                                     AND RecordType.DeveloperName = 'GS_Contact'),
                                                                (select Id,Customer__c from Service_Requests__r where Customer__c in : accountIdList 
                                                                    AND (Record_Type_Name__c='DIFC_Sponsorship_Visa_New' OR Record_Type_Name__c='Company_Temporary_Work_Permit_Request_Letter' 
                                                                    OR Record_Type_Name__c='Employment_Visa_from_DIFC_to_DIFC' OR Record_Type_Name__c='Employment_Visa_Govt_to_DIFC' 
                                                                    OR Record_Type_Name__c='Visa_from_Individual_sponsor_to_DIFC' OR (Record_Type_Name__c = 'Access_Card' 
                                                                    AND (Service_Category__c ='New Seconded Employee Card' OR Service_Category__c = 'New Employee Card for UAE / GCC Nationals'))) 
                                                                    AND isClosedStatus__c != true AND Is_Rejected__c != true AND External_Status_Name__c != 'Rejected' 
                                                                    AND External_Status_Name__c != 'Cancelled' AND External_Status_Name__c != 'Draft') 
                                                                 from Account where id IN : accountIdList]);
            for(Account tempObj : tempAccountList){
                accountMap.put(tempObj.id,tempObj.Index_Card_Status__c);
                POBoxMap.put(tempObj.id,tempObj.Customer_PO_Boxes__r.size());
                pipelineEmployeeMap.put(tempObj.id,tempObj.Service_Requests__r.size());
                currentEmployeeMap.put(tempObj.id,tempObj.Contacts.size());
            }
            
            
            if(accountMap!=null){
                for(step__c tempObj : lstNewRecords){
                    if(accountMap.get(tempObj.Customer_Name__c) != 'Cancelled' && accountMap.get(tempObj.Customer_Name__c) != 'Not Available')  {
                        tempObj.addError('Please Contact Government Services, Activities are still pending for the Client.');
                    }
                }       
            }
            
            if(pipelineEmployeeMap!=null){
                for(step__c tempObj : lstNewRecords){
                    if(pipelineEmployeeMap.get(tempObj.Customer_Name__c) > 0)   {
                        tempObj.addError('Please Contact Government Services, Activities are still pending for the Client.');
                    }
                }       
            }
            
            if(currentEmployeeMap!=null){
                for(step__c tempObj : lstNewRecords){
                    if(currentEmployeeMap.get(tempObj.Customer_Name__c) > 0)    {
                        tempObj.addError('Please Contact Government Services, Activities are still pending for the Client.');
                    }
                }       
            }
            
            if(POBoxMap!=null){
                for(step__c tempObj : lstNewRecords){
                    if(POBoxMap.get(tempObj.Customer_Name__c) > 0)  {
                        tempObj.addError('Please contact Government Services, activities are still pending for the client.');
                    }
                }       
            }
        }
    }
    
    //v1.3 
    public void updatePortalUsers(list<Step__c> lstNewRecords,map<Id,Step__c> mapOldRecords){
        set<id> accountSet = new set<id>();
        if(lstNewRecords!=null && lstNewRecords.size()>0){
            for(step__c tempObj : lstNewRecords){
                if(tempObj.Sys_SR_Group__c == 'GS' && mapOldRecords!=null && tempObj.Status__c!=mapOldRecords.get(tempObj.Id).Status__c){
                    accountSet.add(tempObj.Customer_Name__c);   
                }
            }   
        }
        if(accountSet!=null && !accountSet.isEmpty()){
            list<Contact> lstContacts = [select Id,accountId from Contact where AccountId IN : accountSet AND RecordType.DeveloperName='Portal_User' AND Id IN (select ContactId from User where Contact.AccountId IN : accountSet AND IsActive=true AND Community_User_Role__c INCLUDES ('Employee Services'))];   
            
            Map<String,List<Contact>> mapAccountWiseContact = new Map<String,List<Contact>>();
            for(Contact cont : lstContacts){
                if(mapAccountWiseContact.containsKey(cont.accountId)){
                    List<Contact> lstCont = mapAccountWiseContact.get(cont.accountId);
                    lstCont.add(cont);
                }
                else{
                    List<Contact> lstCont = new List<Contact>();
                    lstCont.add(cont);
                    mapAccountWiseContact.put(cont.accountId,lstCont);
                }
            }
            if(mapAccountWiseContact!=null && !mapAccountWiseContact.isEmpty() && lstNewRecords!=null && !lstNewRecords.isEmpty()){
                for(step__c obj : lstNewRecords){
                    if(mapAccountWiseContact.get(obj.Customer_Name__c)!=null && mapAccountWiseContact.get(obj.Customer_Name__c).size()>0){
                        if(mapAccountWiseContact.get(obj.Customer_Name__c).size() > 0)
                            obj.Principal_User_1__c = mapAccountWiseContact.get(obj.Customer_Name__c)[0].Id;
                        if(mapAccountWiseContact.get(obj.Customer_Name__c).size() > 1)
                            obj.Principal_User_2__c = mapAccountWiseContact.get(obj.Customer_Name__c)[1].Id;
                        if(mapAccountWiseContact.get(obj.Customer_Name__c).size() > 2)
                            obj.Principal_User_3__c = mapAccountWiseContact.get(obj.Customer_Name__c)[2].Id;
                        if(mapAccountWiseContact.get(obj.Customer_Name__c).size() > 3)
                            obj.Principal_User_4__c = mapAccountWiseContact.get(obj.Customer_Name__c)[3].Id;
                        if(mapAccountWiseContact.get(obj.Customer_Name__c).size() > 4)
                            obj.Principal_User_5__c = mapAccountWiseContact.get(obj.Customer_Name__c)[4].Id;        
                    }   
                }   
            }
        
        }
    }
    
    
    // V1.4 start
    Public void createCustomAttachment(list<Step__c> lstNewRecords,map<Id,Step__c> mapOldRecords){
    	
    	set<id> srIDs = new set<id>();
        Map<string,Service_Request__c> searchContactMap = new  Map<string,Service_Request__c>();
        List<Service_Request__c> serviceListUpdate = new List<Service_Request__c>();
        
        set<String> passportIds                   = new set<String>();
    	map<id,id> validAttachmentIDs             = new map<id,id>();
    	map<id,List<string>> docDetailsMap        = new map<id,List<string>>(); // Doc Id - Key // SR No,Document Name,Customer ID, Customer Name - Values
    	List<Log__c> logList                      = new List<Log__c>();
    	
    	for(step__c stp : lstNewRecords){
            
    		if(((stp.closed_Date__c !=null 
                     && mapOldRecords.get(stp.id).closed_Date__c ==null 
                     && stp.closed_Date__c !=mapOldRecords.get(stp.id).closed_Date__c)
                ||   (stp.Status_Code__c == 'CLOSED'
                     && stp.Status_Code__c != mapOldRecords.get(stp.id).Status_Code__c))
                &&   stp.create_picture__c )
                
    			srIDs.add(stp.sr__c);
            
    	}
        if(!srIDs.isEmpty()){
        	for(Service_Request__c thisSR : [SELECT ID ,Contact__c,Submitted_DateTime__c,Date_of_Birth__c,Passport_Number__c 
                                            FROM Service_Request__c 
                                            WHERE ID IN : srIDs LIMIT 1]){
                                                
            	if(String.isBlank(thisSR.Contact__c) 
                  || thisSR.Submitted_DateTime__c == null) {
                      
                	searchContactMap.put(thisSR.Passport_Number__c+'-'+thisSR.Date_of_Birth__c,thisSR);
	                passportIds.add(thisSR.Passport_Number__c);
            	}
        	}
        }
        
        if(!searchContactMap.isEmpty()){
            
            for(Contact thisContact : [SELECT Id,Birthdate,Passport_No__c 
                                            FROM Contact
                                            where RecordType.DeveloperName='GS_Contact' 
                                            AND Passport_No__c IN :passportIds LIMIT 1]){
                   
                 if(searchContactMap.containsKey(thisContact.Passport_No__c+'-'+thisContact.Birthdate)){
                     
                    Service_Request__c thisSR  = searchContactMap.get(thisContact.Passport_No__c+'-'+thisContact.Birthdate);
                   
                    if(String.isBlank(thisSR.Contact__c)){
                      thisSR.Contact__c= thisContact.Id;
                    }
                   
                    if(thisSR.Submitted_DateTime__c == null){
                      thisSR.Submitted_DateTime__c= System.now();
                    }
                    serviceListUpdate.add(thisSR);
                }
            }
        }
        
		if(srIDs !=null && srIDs.size()>0){
			for(sr_Doc__c srdoc : [select id,Doc_ID__c,Name,Service_Request__c,Service_Request__r.customer__r.Name,Service_Request__r.customer__c,Service_Request__r.Name from SR_Doc__c where Service_Request__c in :srIDs and Name ='Coloured Photo' and Doc_ID__c !=null and status__c = 'Uploaded' and Sent_Picture_to_Secupass__c = 0]){
				docDetailsMap.put(srdoc.id,new list<string>{srdoc.Service_Request__c,srdoc.Name,srdoc.Service_Request__r.Name,srdoc.Service_Request__r.customer__c,srdoc.Service_Request__r.customer__r.Name});    			
				validAttachmentIDs.put(srdoc.id,srdoc.Doc_ID__c);
			}
		}
    	List<Custom_Attachment__c> cAttachToUpdate = new List<Custom_Attachment__c>();
    	Custom_Attachment__c ca;
    	long noOfloops;
		integer maxSizeOfText = 131072;
		string attachmentBodybase64;
		if(docDetailsMap !=null && docDetailsMap.size()>0){
			for(Attachment att :[select id,Name,Body,parentID from attachment where parentID in :docDetailsMap.keySet() and id in:validAttachmentIDs.values()]){
				attachmentBodybase64 = string.valueOf(EncodingUtil.base64Encode(att.Body));    		
				ca = new Custom_Attachment__c(SR_Doc__c =att.parentID ,Service_Request__c =docDetailsMap.get(att.ParentID)[0],SRDoc_EXT__c =att.parentID);
				ca.Picture_Length__c =  attachmentBodybase64.length();
				noOfloops = (ca.Picture_Length__c/maxSizeOfText).round(System.RoundingMode.CEILING);
				if(noOfloops <= 12){ 
					for(integer i = 1;i <=12;i++){
						if(i < noOfloops)    
							ca.put('Picture_in_Text'+i+'__c',attachmentBodybase64.substring(maxSizeOfText*(i-1), maxSizeOfText*i));
						else if(i == noOfloops)	
							ca.put('Picture_in_Text'+i+'__c',attachmentBodybase64.substring(maxSizeOfText*(i-1),attachmentBodybase64.length()));
						else if(i>noOfloops)
							ca.put('Picture_in_Text'+i+'__c','');
							
					}
						cAttachToUpdate.add(ca);
				}else{
						Log__c binaryattLog = new Log__c();
						binaryattLog.Account__c = docDetailsMap.get(att.ParentID)[3];
						binaryattLog.Description__c =  'Unable to convert picture to binary format.Reached maximum limit SR No - '+docDetailsMap.get(att.ParentID)[2]+' SR Doc -'+docDetailsMap.get(att.ParentID)[1];
						binaryattLog.Type__c	 = 'Failure - Converting Attachment  to Binary format';
						logList.add(binaryattLog); 
				}
			}
		}	
        	if(cAttachToUpdate !=null && !cAttachToUpdate.isEmpty()) Upsert cAttachToUpdate SRDoc_EXT__c;
        	if(logList !=null && logList.size()>0) insert logList;
            if(serviceListUpdate !=null && serviceListUpdate.size()>0) update serviceListUpdate;
        
       }
    	
    // End V1.4
    
    
    // V1.5  start
    public void updateArabicFields(list<Step__c> lstNewRecords,map<Id,Step__c> mapOldRecords){
    	map<id,map<string,string>> srContactMap = new map<id,map<string,string>>();
    	for(Step__c objStp : lstNewRecords){
    		if(objStp.SR__c !=null && objStp.Update_Arabic_Names__c ==true && objStp.closed_Date__c !=null && mapOldRecords.get(objStp.id).closed_Date__c == null && objStp.closed_Date__c != mapOldRecords.get(objStp.id).closed_Date__c)
        		srContactMap.put(objStp.SR__c,null);
    	}
    	
    	map<id,id> srContactIDMap;
		List<Contact> conListToUpdate;
		if(srContactMap!=null && srContactMap.size()>0){
			srContactIDMap = new map<id,id>();
			conListToUpdate = new List<Contact>();
	        for(service_Request__c sr :[select id,contact__c,contact__r.First_Name_Arabic__c,contact__r.Last_Name_Arabic__c from service_Request__c where id in : srContactMap.keySet() and contact__c !=null]){
	        		srContactIDMap.put(sr.id,sr.contact__c);
	        		srContactMap.put(sr.id,new map<string,string>());
	        		srContactMap.get(sr.id).put('firstname',sr.contact__r.First_Name_Arabic__c);
	        		srContactMap.get(sr.id).put('lastname',sr.contact__r.last_Name_Arabic__c);        		
	        }
		}		
		
		for(Step__c objStp : lstNewRecords){
			if(srContactMap.containsKey(objStp.sr__c) && srContactMap.get(objStp.sr__c) !=null && (objStp.First_Name_Arabic__c !=srContactMap.get(objStp.sr__c).get('firstname') || objStp.Last_Name_Arabic__c !=srContactMap.get(objStp.sr__c).get('lastname')) ){
	        	conListToUpdate.add(new contact(id=srContactIDMap.get(objStp.sr__c),First_Name_Arabic__c=objStp.First_Name_Arabic__c,Last_Name_Arabic__c=objStp.Last_Name_Arabic__c,Arabic_fields_updated_on__c=system.Now()));             
        }
        
        if(conListToUpdate!=null && !conListToUpdate.isEmpty())
           update conListToUpdate;       
    	
    }
    
    
	}
	// V1.5  End
}