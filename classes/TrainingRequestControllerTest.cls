@IsTest
private class TrainingRequestControllerTest {
    static testMethod void validateTrainingRequest() {
        User portalAccountOwner = createPortalAccountOwner();  
        User communityUser = createCommunityUser(portalAccountOwner); 
        //inserting training master.
        Training_Master__c trainingMaster;
        System.runAs (portalAccountOwner){
            trainingMaster = new Training_Master__c();
            trainingMaster.Name = 'Employee Service training';
            trainingMaster.Category__c = 'Employee Services';
            trainingMaster.Available_Seats__c = 10;
            trainingMaster.Status__c = 'Approved by HOD';
            trainingMaster.Start_DateTime__c = DateTime.now().addDays(1);
            trainingMaster.To_DateTime__c = DateTime.now().addDays(2);
            insert trainingMaster;
        }
        System.runAs ( communityUser ) {
            PageReference pageRef = Page.TrainingRequest;
            pageRef.getParameters().put('category', 'Employee Services');
            Test.setCurrentPage(pageRef);
            TrainingRequestController trControllerObj = new TrainingRequestController();
            trControllerObj.selectedValue = trainingMaster.id;
            trControllerObj.save();
            trControllerObj.doCancel();
            trControllerObj.populateDetail();
            trControllerObj.validateRequestLimit();
        }

    }
    private static User createPortalAccountOwner() {
        UserRole portalRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role', PortalType = 'None');
        insert portalRole;
        System.debug('portalRole is ' + portalRole);
        Profile sysAdminProfile = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner = new User(
            UserRoleId = portalRole.Id,
            ProfileId = sysAdminProfile.Id,
            Username = 'portalOwner' + System.currentTimeMillis() + '@test.com',
            Alias = 'Alias',
            Email = 'portal.owner@test.com',
            EmailEncodingKey = 'UTF-8',
            Firstname = 'Portal',
            Lastname = 'Owner',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles'
        );
        Database.insert(portalAccountOwner);
        return portalAccountOwner;
    }
    private static User createCommunityUser(User portalAccountOwner) {
         User communityUser;
        System.runAs(portalAccountOwner) {
            //Create account  
            Account portalAccount = new Account(
                Name = 'portalAccount',
                OwnerId = portalAccountOwner.Id
            );
            Database.insert(portalAccount);
            //Create contact  
            Contact portalContact = new Contact(
                FirstName = 'portalContactFirst',
                Lastname = 'portalContactLast',
                AccountId = portalAccount.Id,
                Email = 'portalContact' + System.currentTimeMillis() + '@test.com'
            );
            Database.insert(portalContact);
            Set<String> customerUserTypes = new Set<String> {'CspLitePortal'};
            communityUser = new User(
                ProfileId = [SELECT id FROM profile where usertype in :customerUserTypes limit 1].Id,
                FirstName = 'CommunityUserFirst',
                LastName = 'CommunityUserLast',
                Email = 'community.user@test.com',
                Username = 'community.user.' + System.currentTimeMillis() + '@test.com',
                Title = 'Title',
                Alias = 'Alias',
                TimeZoneSidKey = 'America/Los_Angeles',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                LocaleSidKey = 'en_US',
                ContactId = portalContact.id
            );
            Database.insert(communityUser);
        }
        return communityUser;
    }
}