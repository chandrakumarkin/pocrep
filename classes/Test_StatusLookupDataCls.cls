/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=true)
private class Test_StatusLookupDataCls { 

     static testMethod void myUnitUBOTest1(){
         list<Step__c> lstStep = [select id,SR_Record_Type_Text__c,SR__c,Step_Name__c,SR__r.SR_Template__c,Status__c,SR__r.Customer__c,SR__r.Record_Type_Name__c,SR__r.Summary__c,SR_Step__c,Rejection_Reason__c,Status__r.Name,Status__r.Code__c,SR__r.SR_Template__r.Menu__c,SAP_Seq__c,SR_Group__c from Step__c where SR_Step__c!=null and SR__c!=null and SR_Record_Type_Text__c IN('Change_Details_Add_or_Remove_Designated_Member') and SR_Status__c IN ('Under Verification') order by createddate desc limit 2];
        test.starttest();
            for(Step__c itr:lstStep ){
             Apexpages.currentPage().getParameters().put('id',itr.Id);
            StatusLookupDataCls objStatusLookupDataCls = new StatusLookupDataCls();
            objStatusLookupDataCls.checkAwaitingOrginalDepntSRS();
            objStatusLookupDataCls.hasRejectionReasonVar = 'Test Rejection REason';
            objStatusLookupDataCls.changeReasonVisibility();  
            objStatusLookupDataCls.AccountBalance();
            }
        test.stoptest();
     }
     static testMethod void myUnitUBOTest2(){
         list<Step__c> lstStep = [select id,SR_Record_Type_Text__c,SR__c,Step_Name__c,SR__r.SR_Template__c,Status__c,SR__r.Customer__c,SR__r.Record_Type_Name__c,SR__r.Summary__c,SR_Step__c,Rejection_Reason__c,Status__r.Name,Status__r.Code__c,SR__r.SR_Template__r.Menu__c,SAP_Seq__c,SR_Group__c from Step__c where SR_Step__c!=null and SR__c!=null and SR_Record_Type_Text__c IN('Change_Details_Add_or_Remove_Director') and SR_Status__c IN ('Under Verification') order by createddate desc limit 2];
        test.starttest();
            for(Step__c itr:lstStep ){
             Apexpages.currentPage().getParameters().put('id',itr.Id);
            StatusLookupDataCls objStatusLookupDataCls = new StatusLookupDataCls();
            objStatusLookupDataCls.checkAwaitingOrginalDepntSRS();
            objStatusLookupDataCls.hasRejectionReasonVar = 'Test Rejection REason';
            objStatusLookupDataCls.changeReasonVisibility();  
            objStatusLookupDataCls.AccountBalance();
            }
        test.stoptest();
     }
     static testMethod void myUnitUBOTest3(){
         list<Step__c> lstStep = [select id,SR_Record_Type_Text__c,SR__c,Step_Name__c,SR__r.SR_Template__c,Status__c,SR__r.Customer__c,SR__r.Record_Type_Name__c,SR__r.Summary__c,SR_Step__c,Rejection_Reason__c,Status__r.Name,Status__r.Code__c,SR__r.SR_Template__r.Menu__c,SAP_Seq__c,SR_Group__c from Step__c where SR_Step__c!=null and SR__c!=null and SR_Record_Type_Text__c IN('Freehold_Transfer_Registration') and SR_Status__c IN ('Under Verification') order by createddate desc limit 1];
        test.starttest();
            for(Step__c itr:lstStep ){
             Apexpages.currentPage().getParameters().put('id',itr.Id);
            StatusLookupDataCls objStatusLookupDataCls = new StatusLookupDataCls();
            objStatusLookupDataCls.checkAwaitingOrginalDepntSRS();
            objStatusLookupDataCls.hasRejectionReasonVar = 'Test Rejection REason';
            objStatusLookupDataCls.changeReasonVisibility();  
            objStatusLookupDataCls.AccountBalance();
            }
        test.stoptest();
     }
     static testMethod void myUnitUBOTest4(){
         list<Step__c> lstStep = [select id,SR_Record_Type_Text__c,SR__c,Step_Name__c,SR__r.SR_Template__c,Status__c,SR__r.Customer__c,SR__r.Record_Type_Name__c,SR__r.Summary__c,SR_Step__c,Rejection_Reason__c,Status__r.Name,Status__r.Code__c,SR__r.SR_Template__r.Menu__c,SAP_Seq__c,SR_Group__c from Step__c where SR_Step__c!=null and SR__c!=null and SR_Record_Type_Text__c ='Shareholder_holding_shares_on_trust' and SR_Status__c ='Under Verification' limit 1];
        test.starttest();
            for(Step__c itr:lstStep ){
             Apexpages.currentPage().getParameters().put('id',itr.Id);
            StatusLookupDataCls objStatusLookupDataCls = new StatusLookupDataCls();
            objStatusLookupDataCls.checkAwaitingOrginalDepntSRS();
            objStatusLookupDataCls.hasRejectionReasonVar = 'Test Rejection REason';
            objStatusLookupDataCls.changeReasonVisibility();  
            objStatusLookupDataCls.AccountBalance();
            }
        test.stoptest();
     }
     static testMethod void myUnitUBOTest5(){
         list<Step__c> lstStep = [select id,SR_Record_Type_Text__c,SR__c,Step_Name__c,SR__r.SR_Template__c,Status__c,SR__r.Customer__c,SR__r.Record_Type_Name__c,SR__r.Summary__c,SR_Step__c,Rejection_Reason__c,Status__r.Name,Status__r.Code__c,SR__r.SR_Template__r.Menu__c,SAP_Seq__c,SR_Group__c from Step__c where SR_Step__c!=null and SR__c!=null and SR_Record_Type_Text__c ='Application_of_Registration' and SR_Status__c ='Under Verification' limit 1];
        test.starttest();
            for(Step__c itr:lstStep ){
             Apexpages.currentPage().getParameters().put('id',itr.Id);
            StatusLookupDataCls objStatusLookupDataCls = new StatusLookupDataCls();
            objStatusLookupDataCls.checkAwaitingOrginalDepntSRS();
            objStatusLookupDataCls.hasRejectionReasonVar = 'Test Rejection REason';
            objStatusLookupDataCls.changeReasonVisibility();  
            objStatusLookupDataCls.AccountBalance();
            }
        test.stoptest();
     }
     static testMethod void myUnitUBOTest6(){
         list<Step__c> lstStep = [select id,SR_Record_Type_Text__c,SR__c,Step_Name__c,SR__r.SR_Template__c,Status__c,SR__r.Customer__c,SR__r.Record_Type_Name__c,SR__r.Summary__c,SR_Step__c,Rejection_Reason__c,Status__r.Name,Status__r.Code__c,SR__r.SR_Template__r.Menu__c,SAP_Seq__c,SR_Group__c from Step__c where SR_Step__c!=null and SR__c!=null and SR_Record_Type_Text__c ='Change_Add_or_Remove_Beneficial_or_Ultimate_Owner' and SR_Status__c ='Under Verification' limit 1];
        test.starttest();
            for(Step__c itr:lstStep ){
             Apexpages.currentPage().getParameters().put('id',itr.Id);
            StatusLookupDataCls objStatusLookupDataCls = new StatusLookupDataCls();
            objStatusLookupDataCls.checkAwaitingOrginalDepntSRS();
            objStatusLookupDataCls.hasRejectionReasonVar = 'Test Rejection REason';
            objStatusLookupDataCls.changeReasonVisibility();  
            objStatusLookupDataCls.AccountBalance();
            }
        test.stoptest();
     }
     static testMethod void myUnitUBOTest7(){
         list<Step__c> lstStep = [select Id,SAP_Seq__c,Step_Template__r.SR_Status__c from Step__c where SAP_Seq__c != null AND Step_Status__c != 'Closed' AND Step_Status__c != 'Cancelled' AND Step_Status__c != 'Rejected' AND SAP_Seq__c != '0010' limit 1];
        test.starttest();
            for(Step__c itr:lstStep ){
             Apexpages.currentPage().getParameters().put('id',itr.Id);
            StatusLookupDataCls objStatusLookupDataCls = new StatusLookupDataCls();
            objStatusLookupDataCls.checkAwaitingOrginalDepntSRS();
            objStatusLookupDataCls.hasRejectionReasonVar = 'Test Rejection REason';
            objStatusLookupDataCls.changeReasonVisibility();  
            objStatusLookupDataCls.GsStepProcess();
            StatusLookupDataCls.fakeMethod();
            }
        test.stoptest();
     }
}