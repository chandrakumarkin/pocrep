/*************************************************************************************************
 *  Name        : MobileSavedRequestController
 *  Author      : Diana
 *  Company     : NSI JLT
 *  Date        : 10/11/2015     
 *  Purpose     : This class is to retrieve the Pending requests for DIFC Mobile    
  ---------------------------------------------------------------------------------------------------------------------
  
**************************************************************************************************/
public without sharing class MobilePendingRequestController {

    public class CustomSettingsException extends Exception{}
    
    public string accId{get;set;}

    public MobilePendingRequestController() {
     
       string userId = userinfo.getUserId();
       for(User usr:[select id,Account_Id__c,Community_User_Role__c from User where id=:userId]){
           accId = usr.Account_Id__c;
           if(usr.Community_User_Role__c!=null && usr.Community_User_Role__c!=''){
              
               CommunityUserRoles = usr.Community_User_Role__c;
           }
                
       }

        LoadRequests();     
    }    

    // Ravi modified
    public list<SavedRequests> AllRequests {get;set;} 
    public Integer iPagesize = 20;
    public string CommunityUserRoles {get;set;}
    public Integer RowIndex {get;set;}
    
    public void LoadRequests(){
        
      
        AllRequests = new list<SavedRequests>();
        SavedRequests objSavedRequests;
        list<string> lstSRTypes = new list<string>{'ROC','GS','RORP','IT','BC' , 'Marketing'}; //v1.1 Added Marketing in type of request
        map<string,list<Service_Request__c>> mapSRs = new map<string,list<Service_Request__c>>();
        map<string,list<string>> mapSRIds = new map<string,list<string>>();
        list<Service_Request__c> lstSRs;
        list<string> lstSRIds;
        
         //Diana : Modified the query
         //Previous query : for(Service_Request__c objSR :[SELECT id, Name,SR_Group__c, SR_Menu_Text__c, SR_Template__r.Menu__c,SR_Template__r.Sub_menu__c,External_Status_Name__c, CreatedDate, Portal_Service_Request_Name__c,CreatedBy.Name
                        //from Service_Request__c where External_Status_Name__c!='Draft' AND External_Status_Name__c!='Approved' AND External_Status_Name__c!='Rejected' AND External_Status_Name__c!='Return for More Information' AND Record_Type_Name__c!='DM_SR' AND Customer__c =:accId AND Pre_GoLive__c != true ORDER BY CreatedDate ASC]){
           
        for(Service_Request__c objSR :[SELECT id, Name,SR_Group__c, SR_Menu_Text__c, SR_Template__r.Menu__c,SR_Template__r.Sub_menu__c,External_Status_Name__c, CreatedDate, Portal_Service_Request_Name__c,CreatedBy.Name
                        from Service_Request__c where External_Status_Name__c!='Draft' AND Record_Type_Name__c!='DM_SR' AND Customer__c =:accId AND Pre_GoLive__c != true ORDER BY CreatedDate ASC]){
           
            lstSRs = mapSRs.containsKey(objSR.SR_Group__c) ? mapSRs.get(objSR.SR_Group__c) : new list<Service_Request__c>();
                            
            System.debug('mobile'+lstSRs.size());
            if(lstSRs.size() < iPagesize)
                lstSRs.add(objSR);
            mapSRs.put(objSR.SR_Group__c,lstSRs);
            
            lstSRIds = mapSRIds.containsKey(objSR.SR_Group__c) ? mapSRIds.get(objSR.SR_Group__c) : new list<string>();
            lstSRIds.add(objSR.Id);
            mapSRIds.put(objSR.SR_Group__c,lstSRIds);
        }
        if(!mapSRIds.isEmpty()){
            lstSRIds = new list<string>();
            Integer iC = 1;
            map<Integer,list<string>> mapTempIds;
            for(string SRGroup : lstSRTypes){
                Boolean bFlg = false;
                if(CommunityUserRoles.contains('Company Services') && (SRGroup == 'ROC' || SRGroup == 'RORP' || SRGroup == 'BC' || SRGroup == 'Marketing')){ //v1.1 Added Marketing condition
                    bFlg = true;
                }else if(CommunityUserRoles.contains('Company Services') == false && CommunityUserRoles.contains('Property Services') && SRGroup == 'RORP'){
                    bFlg = true;
                }else if((SRGroup == 'GS' && CommunityUserRoles.contains('Employee Services')) || 
                    (SRGroup == 'IT' && CommunityUserRoles.contains('IT Services')) || (SRGroup == 'Marketing' && CommunityUserRoles.contains('Marketing Services')) ){ //v1.1 Added Marketing condition
                    bFlg = true;
                }
                /*else if((SRGroup == 'ROC' && CommunityUserRoles.contains('Company Services')) || (SRGroup == 'GS' && CommunityUserRoles.contains('Employee Services')) || 
                    (SRGroup == 'IT' && CommunityUserRoles.contains('IT Services')) || ((SRGroup == 'RORP' || SRGroup == 'BC' )&& CommunityUserRoles.contains('Property Services')) )
                    bFlg = true;
                */  
                if(mapSRIds.containsKey(SRGroup) && bFlg){
                    objSavedRequests = new SavedRequests();
                    objSavedRequests.SRType = SRGroup;
                    objSavedRequests.RequestIds = mapSRIds.get(SRGroup);
                    objSavedRequests.CurrentPageNo = 1;
                    objSavedRequests.Requests = mapSRs.get(SRGroup);
                    objSavedRequests.Index = AllRequests.size();
                    
                    mapTempIds = new map<Integer,list<string>>();
                    for(string srId : objSavedRequests.RequestIds){
                        if(lstSRIds.size() < iPagesize)
                            lstSRIds.add(srId);
                        else{
                            mapTempIds.put(iC,lstSRIds);
                            lstSRIds = new list<string>();
                            iC++;
                            lstSRIds.add(srId);
                        }
                    }
                    if(!lstSRIds.isEmpty()){
                        mapTempIds.put(iC,lstSRIds);
                    }
                    objSavedRequests.mapPageSRIds = mapTempIds;
                    objSavedRequests.TotalPages =  mapTempIds.size();
                   
                    //v1.1 Added Marketing condition below
                    objSavedRequests.RoleType = (SRGroup == 'ROC') ? 'Company Service' : (SRGroup == 'GS') ? 'Employee Service' : (SRGroup == 'RORP') ? 'Property Service' : (SRGroup == 'IT') ? 'IT Services' : (SRGroup == 'Marketing') ? 'Marketing Services' : (SRGroup == 'BC') ? 'Business Centre Service' : '';  
                    AllRequests.add(objSavedRequests);
					
                }
            }
        }
        
    }
    
    public void getRequests(Integer PageNo){
        if(RowIndex != null && AllRequests != null && RowIndex <= AllRequests.size()){
            list<string> lstIds = AllRequests[RowIndex].mapPageSRIds.get(PageNo);
            if(lstIds != null && !lstIds.isEmpty()){
                AllRequests[RowIndex].Requests = new list<Service_Request__c>();
                AllRequests[RowIndex].Requests = [SELECT id, Name,SR_Group__c, SR_Menu_Text__c, SR_Template__r.Menu__c,External_Status_Name__c, CreatedDate, Portal_Service_Request_Name__c,CreatedBy.Name,SR_Template__r.Sub_menu__c 
                         from Service_Request__c where Id IN : lstIds AND Pre_GoLive__c != true ORDER BY CreatedDate ASC];
                AllRequests[RowIndex].CurrentPageNo = PageNo;
            }
        }
    }
    
    public void getNextRecords(){
        
         System.debug('entered');
        if(RowIndex != null && AllRequests != null && RowIndex <= AllRequests.size()){
            getRequests((AllRequests[RowIndex].CurrentPageNo+1));
            
            System.debug('entered');
        }
    }
    public void getPreviousRecords(){
        if(RowIndex != null && AllRequests != null && RowIndex <= AllRequests.size()){
            getRequests((AllRequests[RowIndex].CurrentPageNo-1));
        }
    }
    public void getFirstRecords(){
        if(RowIndex != null && AllRequests != null && RowIndex <= AllRequests.size()){
            getRequests(1);
        }
    }
    public void getLastRecords(){
        if(RowIndex != null && AllRequests != null && RowIndex <= AllRequests.size()){
            getRequests((AllRequests[RowIndex].TotalPages.intValue()));
        }
    }
    
    public class SavedRequests{
        public string SRType {get;set;}
        public string RoleType {get;set;}
        
        public Integer CurrentPageNo {get;set;}
        public decimal TotalPages {get;set;}
        public list<Service_Request__c> Requests {get;set;}
        public list<string> RequestIds {get;set;}
        public Integer Index {get;set;}
        public map<Integer,list<string>> mapPageSRIds {get;set;}
    }
      
}