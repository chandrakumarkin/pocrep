/*
    Author      : Leeba
    Date        : 2-April-2020
    Description : Test class for CC_PromoteToSuperUser 
    --------------------------------------------------------------------------------------
*/
@isTest
public class CC_PromoteToSuperUserTest{

       public static testMethod void CC_PromoteToSuperUser() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       Account acc1 = new Account();
       acc1.name = 'test1';
       insert acc1;
       
       Contact con = new Contact();
       con.LastName = 'test';
       con.AccountId = acc1.id;      
       insert con;
       
       AccountContactRelation acccon = new AccountContactRelation();
       acccon.AccountId = acc.id;
       acccon.ContactId = con.id;
       acccon.IsActive = true;
       acccon.Roles = 'Community User';
       insert acccon;
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Contact__c = con.id;
        insert objHexaSR;
        
        HexaBPM__Status__c objhexastatus = new HexaBPM__Status__c();
        objhexastatus.HexaBPM__Type__c = 'Intermediate';
        objhexastatus.HexaBPM__Code__c = 'REQUEST_WITHDRAWN';
        insert objhexastatus;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        objHexastep.HexaBPM__Status__c = objhexastatus.id;
        insert objHexastep;
        
        Test.startTest();
        HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.HexaBPM__Contact__c
                                  from HexaBPM__Step__c where Id=:objHexastep.Id];
        CC_PromoteToSuperUser CC_PromoteToSuperUserObj = new CC_PromoteToSuperUser();
        CC_PromoteToSuperUserObj.EvaluateCustomCode(objHexaSR,step); 
        CC_PromoteToSuperUser.UpdateUserRole(new List<Id>(),new List<Id>());
        Test.stopTest();
    }

}