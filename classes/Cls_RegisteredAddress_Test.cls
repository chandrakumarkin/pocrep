@isTest

public class Cls_RegisteredAddress_Test{

  static testmethod void TestMethod1(){
  
  
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Under Formation';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        insert objContact;
        
        User usr = new User();
        usr.IsActive = true;
        usr.CompanyName = objAccount.id;
        usr.username = 'testDIFC@difcportal.ae';
        usr.contactId=objContact.Id;
        usr.firstname='Test';
        usr.lastname='Test';
        usr.email='test@difc.ae';
        //usr.phone = SR.Send_SMS_To_Mobile__c;
        usr.Title = 'Mr.';
        usr.communityNickname = (usr.firstname +string.valueof(Math.random()).substring(4,9));
        usr.alias = string.valueof(usr.firstname.substring(0,1) + string.valueof(Math.random()).substring(4,9));            
        usr.profileid = Label.Profile_ID;
        usr.emailencodingkey='UTF-8';
        usr.languagelocalekey='en_US';
        usr.localesidkey='en_GB';
        usr.timezonesidkey='Asia/Dubai';
        usr.Community_User_Role__c = 'Employee Services';
        insert usr;
        
        
        Service_request__c sr = new Service_request__c ();
        sr.Customer__c =objAccount.id;
        sr.Declaration_Signatory_Name__c='test';
        sr.Declaration_Signatory_Capacity__c ='test';
        sr.Email__c ='c-veera.narayana@difc.ae';
        sr.Send_SMS_To_Mobile__c ='+971589602235';
        insert sr;
        
        Records_kept__c rk= new Records_kept__c();
        rk.Account__c = objAccount.id;
        rk.Status__c ='Active';
        insert rk;
        
    
  
       Cls_RegisteredAddress RegAdd = new Cls_RegisteredAddress();
       regAdd.objSr = sr;   
       regAdd.strSRId =sr.id;   
       RegAdd.RecordskeptLoad();
  
  
  
  
  }

}