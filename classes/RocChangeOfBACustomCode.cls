/******************************************************************************************
 *  Author   : Ravindra Babu Nagaboina
 *  Company  : NSI JLT
 *  Date     : 26th Oct 2014   
 *  Description : This class will be used to process the Change of Business Activity Information                       
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.1  21/Dec/2015      Shabbir             Added the logic for company type change per Assembla 743
 V1.2  14/Apr/2016      Swati             	Added the logic for End date as per Assembla 2634
*******************************************************************************************/

public without sharing class RocChangeOfBACustomCode {
    public static string UpdateBusinessActivities(Step__c objStep){
        
        list<License_Activity__c> lstLicenseActivities = new list<License_Activity__c>();
        list<License_Activity__c> lstLicenseActivitiesInsert = new list<License_Activity__c>();
        list<Amendment__c> lstAmendments = new list<Amendment__c>();
        
        map<string,string> mapLicenseIds = new map<string,string>();
        
        Account objAccount;// = new Account(Id=objStep.SR__r.Customer__c);
        Amendment__c MainAmendment = new Amendment__c();
        
        for(Amendment__c objAmendment : [select Id,Name,Company_Type__c,Sector_Classification__c,Company_Type_Changed__c,Sector_Classification_Changed__c,Existing_Contact_Company__c,
                End_Date__c,Registration_Date__c,License_Activity_Master__c,ServiceRequest__c,Capacity__c,Amendment_Type__c,License_Activity_Master__r.Sector_Classification__c
                from Amendment__c where ServiceRequest__c=:objStep.SR__c AND (Amendment_Type__c='Change of Business Activity' OR Amendment_Type__c='License Activity' OR Amendment_Type__c='Business Activity')]){
            if(objAmendment.Amendment_Type__c == 'Change of Business Activity'){
                MainAmendment = objAmendment;
            }else{
                lstAmendments.add(objAmendment);
            }
        }
        // v1.1 - uncommented the company type and sector classification logic change.
        if(MainAmendment.Company_Type_Changed__c || MainAmendment.Sector_Classification_Changed__c){
            objAccount = new Account(Id=objStep.SR__r.Customer__c);
            if(MainAmendment.Company_Type_Changed__c)
                objAccount.Company_Type__c = MainAmendment.Company_Type__c;
            if(MainAmendment.Sector_Classification_Changed__c)
                objAccount.Sector_Classification__c = MainAmendment.Sector_Classification__c;
        }
        
        if(!lstAmendments.isEmpty()){
            for(Amendment__c objAmd : lstAmendments){
                License_Activity__c objLA;
                system.debug('---objAmd.End_Date__c---'+objAmd.End_Date__c);
                if(objAmd.Capacity__c != null && objAmd.End_Date__c != null){
                    objLA = new License_Activity__c(Id=objAmd.Capacity__c);                    
                    //objLA.End_Date__c = objAmd.End_Date__c;
                    //To maintain in sync with other add and remove SR keeping end date as system.today()
                    //Would change in future to date of cessation once business confirms
                    //Done by swati - v1.2
                    objLA.End_Date__c = objAmd.End_Date__c;
                    objLA.License_Activity_Unique_Id__c = '';
                    lstLicenseActivities.add(objLA);                    
                }else if(objAmd.Capacity__c == null && (objAmd.End_Date__c == null || objAmd.End_Date__c >= system.today())){
                    objLA = new License_Activity__c();
                    objLA.Account__c = objStep.SR__r.Customer__c;
                    objLA.Activity__c = objAmd.License_Activity_Master__c;
                    objLA.Start_Date__c = system.today();
                    objLA.Sector_Classification__c = objAmd.License_Activity_Master__r.Sector_Classification__c;
                    lstLicenseActivitiesInsert.add(objLA);                    	
                }   
            }
        }
        
        if(objAccount != null)
            update objAccount;
        if(!lstLicenseActivities.isEmpty())
        	update lstLicenseActivities;
        if(!lstLicenseActivitiesInsert.isEmpty())
        	insert lstLicenseActivitiesInsert;
        return 'Success';
    }
}