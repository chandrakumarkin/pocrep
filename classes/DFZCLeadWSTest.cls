@isTest
public class DFZCLeadWSTest {
    @isTest static void testCreateLead() {
        
		String reqBody = '[{"leadId":"ABC123","firstName":"Test Lead DFZC","areaOfInterest":["S1","S2"],"lastName":"WS","phoneNumber":"+971512512512","email":"test@testtesttesttest.com","subZoneName":"Zone1","ipAddress":"192.168.0.0","contactPreference":"contactprefeerence","companyName":"Test Lead Company","additionalComments":"Additional Commnet","submittedOn":"2020-12-17T11:49:58","gdprConsent":true,"status":"new","updatedOn":"2020-12-17T11:49:58"}]';
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        System.debug('Base URL: ' + sfdcBaseURL );
        
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = sfdcBaseURL+'/services/apexrest/api/CreateLead/';
        System.debug('ZZZ requestUri-->'+request.requestUri);   
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(reqBody);
        RestContext.request = request;
       
        DFZCLeadWS.DFZCCreateResponce resp = DFZCLeadWS.doPost();
        System.debug('Resp-->'+resp);
        
        //System.assert(true,resp.isSuccess);
        
        Integer count  = [Select Count() From Lead Where firstName = 'Test Lead DFZC' ];
        System.assertEquals(1,count);
        
        Test.stopTest();
        
    }
    
    //BlhmDPTjROhBfR1zJrUo5WGg0ilQg05sWYLZvkIMkShzlaAXsXCqxsU4lLBFzV9JBJVy2JhRIgYSaldskPCwWy0W6INj0zbA6Ok0
    //
   
    
      @isTest static void testFailedLead() {
        
        //this will throw exception cause "ipaddress" is morethan 80 characters
		String reqBody = '[{"leadId":"ABC123","firstName":"Test Lead DFZC","areaOfInterest":["S1","S2"],"lastName":"WS","phoneNumber":"+971512512512","email":"test@testtesttesttest.com","subZoneName":"Zone1","ipAddress":"BlhmDPTjROhBfR1zJrUo5WGg0ilQg05sWYLZvkIMkShzlaAXsXCqxsU4lLBFzV9JBJVy2JhRIgYSaldskPCwWy0W6INj0zbA6Ok0","contactPreference":"contactprefeerence","companyName":"Test Lead Company","additionalComments":"Additional Commnet","submittedOn":"2020-12-17T11:49:58","gdprConsent":true,"status":"new","updatedOn":"2020-12-17T11:49:58"}]';
        String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        System.debug('Base URL: ' + sfdcBaseURL );
        
        Test.startTest();
        
        RestRequest request = new RestRequest();
        request.requestUri = sfdcBaseURL+'/services/apexrest/api/CreateLead/';
        System.debug('ZZZ requestUri-->'+request.requestUri);   
        request.httpMethod = 'POST';
        request.requestBody = Blob.valueOf(reqBody);
        RestContext.request = request;
       
        DFZCLeadWS.DFZCCreateResponce resp = DFZCLeadWS.doPost();
        System.debug('Failed Resp-->'+resp);
        
        //System.assert(true,resp.isSuccess);

        
        Test.stopTest();
        
    }
}