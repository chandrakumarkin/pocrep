@isTest
global class sapComDocumentSapSoapFunctionsMcSMock implements WebServiceMock {
    
     public static sapComDocumentSapSoapFunctionsMcS.ZsfRefAttachment_element ttRefundOp = new sapComDocumentSapSoapFunctionsMcS.ZsfRefAttachment_element();
      	
      global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) 
       {
      	
      	
        sapComDocumentSapSoapFunctionsMcS.ZsfRefAttachmentResponse_element ttRefundOps = new sapComDocumentSapSoapFunctionsMcS.ZsfRefAttachmentResponse_element();
     
       	response.put('response_x', ttRefundOps); 
   	  }

}