/**
*Author : Merul Shah
*Description : This class creates the new  Ind/BC of role other location.
**/
public class OB_RegAddOthrLocNewController
{
    
    
    @AuraEnabled   
    public static ResponseWrapper initAmendmentDB(String reqWrapPram)
    {
        
        
        //reqest wrpper
        OB_RegAddOthrLocNewController.RequestWrapper reqWrap = (OB_RegAddOthrLocNewController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_RegAddOthrLocNewController.RequestWrapper.class);
        
        //response wrpper
        OB_RegAddOthrLocNewController.ResponseWrapper respWrap = new OB_RegAddOthrLocNewController.ResponseWrapper();
        String srId = reqWrap.srId; 
        String amedtype = reqWrap.amedtype;
        
        List<RecordType> lstRecTyp = [SELECT id,
                                             DeveloperName,
                                             SobjectType 
                                        FROM RecordType 
                                       WHERE DeveloperName =:amedtype  
                                         AND SobjectType = 'HexaBPM_Amendment__c'];
        
            
        
        OB_RegAddOthrLocNewController.AmendmentWrapper amedWrap = new OB_RegAddOthrLocNewController.AmendmentWrapper();
        HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c();
        amedObj.ServiceRequest__c = srId;
        amedObj.recordTypeId = lstRecTyp[0].Id;
        //amedObj.Is_Director__c = true;
        
        if(amedObj.Role__c == NULL )
        {
            amedObj.Role__c = 'Director';
        }
        else if(amedObj.Role__c.indexOf('Director') == -1)
        {
             amedObj.Role__c = amedObj.Role__c +';Director';
        }
        
        
        amedWrap.isIndividual = ( amedtype == 'Individual'  ? true : false);
        amedWrap.isBodyCorporate =  ( amedtype == 'Body_Corporate' ? true : false );
        amedWrap.amedObj = amedObj;
        respWrap.amedWrap = amedWrap;
        
        return respWrap;
    }
    

    
    
    public class AmendmentWrapper 
    {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public Boolean isIndividual { get; set; }
        @AuraEnabled public Boolean isBodyCorporate { get; set; }
        
        public AmendmentWrapper()
        {}
    }
    
    
    public class RequestWrapper 
    {
        //@AuraEnabled public Id flowId { get; set; }
        //@AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public String amedtype { get; set; }
        //@AuraEnabled public AmendmentWrapper amedWrap{get;set;}
       
      
        public RequestWrapper()
        {}
    }

    public class ResponseWrapper 
    {
        //@AuraEnabled public SRWrapper  srWrap{get;set;}
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        
        public ResponseWrapper()
        {}
    }

}