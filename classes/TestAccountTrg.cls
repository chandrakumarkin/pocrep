/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestAccountTrg {
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        
        History_Tracking__c objHT = new History_Tracking__c();
        objHT.Name = 'Name';
        objHT.Field_Label__c = 'Send Portal Email';
        objHT.Field_Name__c = 'Send_Portal_Email__c';
        objHT.Object_Name__c = 'Account';
        insert objHT;
        
        License_Activity_Master__c objLAM = new License_Activity_Master__c();
        objLAM.Name = 'Test DIFC Activity';
        objLAM.Sector_Classification__c = 'Authorised Market Institution';
        objLAM.Type__c = 'License Activity';
        objLAM.Sys_Code__c = '048';
        objLAM.Enabled__c = true;
        insert objLAM;
        
        Profile sysAdProfile = [SELECT ID FROM Profile WHERE Name = 'System Administrator'];
    
        Test.startTest();
        
        System.runAs(new User(Id = UserInfo.getUserId(), ProfileId = sysAdProfile.Id)){
            
            Account objAccount = new Account();
            objAccount.Name = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1';
            //objAccount.E_mail__c = 'test@test.com';
            objAccount.Trade_Name__c = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1';
            objAccount.BP_No__c = '12345';
            objAccount.Company_Type__c = 'Financial - related';
            objAccount.Sector_Classification__c = 'Authorised Market Institution';
            objAccount.Send_Portal_Email__c = false;
            objAccount.ROC_Status__c = '';
            insert objAccount;
            
            License_Activity__c objLA = new License_Activity__c();
            objLA.Account__c = objAccount.Id;
            objLA.Activity__c = objLAM.Id;
            objLA.Sector_Classification__c = 'Authorised Market Institution';        
            insert objLA;
            
            Contact objContact = new Contact();
            objContact.LastName = 'Last RB';
            objContact.Email = 'test@difcportal.com';
            objContact.AccountId = objAccount.Id;
            objContact.FirstName = 'Nagaboina';
            objContact.RecordTypeId = [SELECT ID FROM RecordType WHERE DeveloperName = 'Portal_User' AND SobjectType = 'Contact'].ID;
            insert objContact;
            
            objAccount.Send_Portal_Email__c = true;
            objAccount.Contractor_License_Expiry_Date__c = system.today()+1;
            objAccount.ROC_Status__c = 'Active';
            objAccount.Registration_License_No__c = '1234';
            update objAccount;
            
            Compliance__c comp = new Compliance__c();
            comp.Account__c = objAccount.ID;
            comp.status__c = 'Open';
            insert comp;
            
            try { 
            
                objAccount.Send_Portal_Email__c = false;
                
                update objAccount;
                
                objAccount.bp_no__c = '';
                objAccount.Send_Portal_Email__c = true;
                
                update objAccount;
            
            } catch (Exception e){
                
                System.debug(e);
            }
            
        }
               
        Test.stopTest();
        
    }
    
    static testMethod void myUnitTest2(){
        
        try { 
            
            Account objAccount2 = new Account();
            objAccount2.Name = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1';
            //objAccount.E_mail__c = 'test@test.com';
            objAccount2.Trade_Name__c = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1';
            objAccount2.BP_No__c = '12345';
            objAccount2.Company_Type__c = 'Financial - related';
            objAccount2.Sector_Classification__c = 'Authorised Market Institution';
            objAccount2.Send_Portal_Email__c = false;
            objAccount2.ROC_Status__c = '';
        
            insert objAccount2;
            
            objAccount2.Sector_Classification__c = '';
            objAccount2.Send_Portal_Email__c = true;
            
            update objAccount2;
        
        } catch (Exception e){
            
            System.debug(e);
            
        }
        
    }
    
    static testMethod void myUnitTest3(){
        
        History_Tracking__c objHT = new History_Tracking__c();
        objHT.Name = 'Account.Sector_Classification__c';
        objHT.Field_Label__c = 'Sector Classification';
        objHT.Field_Name__c = 'Sector_Classification__c';
        objHT.Object_Name__c = 'Account';
        insert objHT;
        
        try { 
            
            Account objAccount = new Account();
            objAccount.Name = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1';
            //objAccount.E_mail__c = 'test@test.com';
            objAccount.Trade_Name__c = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1';
            objAccount.BP_No__c = '12345';
            objAccount.Company_Type__c = 'Financial - related';
            objAccount.Sector_Classification__c = 'Authorised Market Institution';
            objAccount.Send_Portal_Email__c = false;
            objAccount.ROC_Status__c = '';
            insert objAccount;
            
            Contact objContact = new Contact();
            objContact.LastName = 'Last RB';
            objContact.Email = 'test@difcportal.com';
            objContact.AccountId = objAccount.Id;
            objContact.FirstName = 'Nagaboina';
            objContact.RecordTypeId = [SELECT ID FROM RecordType WHERE DeveloperName = 'Portal_User' AND SobjectType = 'Contact'].ID;
            insert objContact;
            
            objAccount.Send_Portal_Email__c = true;
            objAccount.Sector_Classification__c = 'Authorised Market Institution Test';
            update objAccount;
        
        } catch (Exception e){
            
            System.debug(e);
            
        }
        
    }
    
    static testMethod void myUnitTest4(){
        
        History_Tracking__c objHT = new History_Tracking__c();
        objHT.Name = 'Account.Name';
        objHT.Field_Label__c = 'Name';
        objHT.Field_Name__c = 'Name';
        objHT.Object_Name__c = 'Account';
        insert objHT;
        
        try { 
            
            Account objAccount = new Account();
            objAccount.Name = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1';
            //objAccount.E_mail__c = 'test@test.com';
            objAccount.Trade_Name__c = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1';
            objAccount.BP_No__c = '12345';
            objAccount.Company_Type__c = 'Financial - related';
            objAccount.Sector_Classification__c = 'Authorised Market Institution';
            objAccount.Send_Portal_Email__c = false;
            objAccount.Registration_License_No__c = '123456';
            objAccount.ROC_Status__c = '';
            insert objAccount;
            
            objAccount.Name = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 2';
            objAccount.Send_Portal_Email__c = true;
            objAccount.Sector_Classification__c = 'Authorised Market Institution Test';
            update objAccount;
        
        } catch (Exception e){
            
            System.debug(e);
            
        }
        
    }
    
    static testmethod void myUnitTest5(){
        
        List<Account> accountsToInsert = new List<Account>();
        Account objAccount = new Account();
            objAccount.Name = 'Ares Administrative Services (DIFC) Limited';
            //objAccount.E_mail__c = 'test@test1.com';
            objAccount.Trade_Name__c = 'Ares Administrative Services (DIFC) Limited';
            objAccount.BP_No__c = '123456';
            objAccount.Company_Type__c = 'Financial - related';
            objAccount.Sector_Classification__c = 'Authorised Market Institution';
            objAccount.Send_Portal_Email__c = false;
            objAccount.Registration_License_No__c = '1234567';
            objAccount.ROC_Status__c = '';
            accountsToInsert.add(objAccount);
            
            Account objAccount1 = new Account();
            objAccount1.Name = 'Ares Administrative';
            //objAccount.E_mail__c = 'test@test2.com';
            objAccount1.Trade_Name__c = 'Ares Administrative';
            objAccount1.BP_No__c = '123458';
            objAccount1.Company_Type__c = 'Financial - related';
            objAccount1.Sector_Classification__c = 'Authorised Market Institution';
            objAccount1.Send_Portal_Email__c = false;
            objAccount1.Registration_License_No__c = '1234568';
            objAccount1.ROC_Status__c = '';
            accountsToInsert.add(objAccount1);
            
            
            Account objAccount2 = new Account();
            objAccount2.Name = 'Ares Administrative 12345678912345678912345678912345678912345678998765432189123456789123456789123456789987654321 89123456789123456789123456789987654321';
            //objAccount.E_mail__c = 'test@test3.com';
            objAccount2.Trade_Name__c = 'Ares Administrative 123456789 123456789 123456789 123456789 123456789987654321 8912345678912345678912345678998765432189123456789123456789123456789987654321 89123456789123456789123456789987654321';
            objAccount2.BP_No__c = '123459';
            objAccount2.Company_Type__c = 'Financial - related';
            objAccount2.Sector_Classification__c = 'Authorised Market Institution';
            objAccount2.Send_Portal_Email__c = false;
            objAccount2.Registration_License_No__c = '1234569';
            objAccount2.ROC_Status__c = '';
            accountsToInsert.add(objAccount2);
            
            
            
            insert accountsToInsert;
        
    }
}