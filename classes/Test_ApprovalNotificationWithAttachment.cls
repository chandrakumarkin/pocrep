@IsTest(SeeAllData=false)
public class Test_ApprovalNotificationWithAttachment {
    
  static testMethod void TestMethod_EmailService(){
  
        //Test_FitoutServiceRequestUtils.useDefaultUser();
        //Test_FitoutServiceRequestUtils.setToInternalUser();
        //Test_FitoutServiceRequestUtils.prepareTestUser(true);
        
        List<Send_Email_with_Docs__c> sendEmailSetting = new List<Send_Email_with_Docs__c>();
        
        Send_Email_with_Docs__c setting = new Send_Email_with_Docs__c();
        setting.Name = 'AoR';
        setting.Document_type__c = 'E-Certificate of Continuation,E-Certificate Generic New Registration,E-Certificate New Incorporation-Generic,E-Certificate New Incorporation-NPIO,E-Certificate New Incorporation-LTD SPC,E-Certificate of Name Change';
        setting.Document_Type_2__c = 'E-Certificate of Continuation';
        setting.Document_Type_3__c = 'E-Certificate of Continuation';       
        setting.Email_Template_Name__c = 'Application_Approval_Notification_AoR';
        setting.Record_Type_Name__c = 'Application_of_Registration';
        setting.Applicable_Legal_Structure__c= 'LTD,LTD SPC,LTD PCC,LTD IC,LTD National,LTD Supra National,LLC,LLP,RLLP,FRC,GP,RP,RLP,LP,NPIO,Foundation'; 
        sendEmailSetting.add(setting);
        
        Send_Email_with_Docs__c setting1 = new Send_Email_with_Docs__c();
        setting1.Name = 'Freehold_Transfer_Registration';
        setting1.Document_type__c = 'Transfer instruments - Off Plan';
        setting1.Document_Type_3__c = '';       
        setting1.Email_Template_Name__c = 'Application_Approval_Notification_AoR';
        setting1.Record_Type_Name__c = 'Freehold_Transfer_Registration';
        setting1.Applicable_Legal_Structure__c= 'LTD,LTD SPC,LTD PCC,LTD IC,LTD National,LTD Supra National,LLC,LLP,RLLP,FRC,GP,RP,RLP,LP,NPIO,Foundation'; 
        sendEmailSetting.add(setting1); 
        
        insert sendEmailSetting;     
        
        User objUser = Test_FitoutServiceRequestUtils.getTestUser();
        objUser.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services';
        update objUser;
            
         Account objAccount = new Account();
         objAccount.Name = 'Test Account';
         objAccount.Phone = '1234567890';
         objAccount.Sector_Classification__c = 'Investment Fund';
         objAccount.Legal_Type_of_Entity__c  = 'LTD';   
         insert objAccount;

        
        SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        insert objSRStep;
        
        SR_Status__c srSubmittedStatus = new SR_Status__c ();
        srSubmittedStatus.name  = 'Submitted';
        srSubmittedStatus.Code__c = 'Submitted';
        insert srSubmittedStatus;

        SR_Status__c srApprovedStatus = new SR_Status__c ();
        srApprovedStatus.name  = 'Approved';
        srApprovedStatus.Code__c = 'Approved';
        insert srApprovedStatus;
        
        system.debug('srStatus'+srSubmittedStatus);     
        List<Document_Master__c> docMaster = new List<Document_Master__c>();
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'E-Certificate Generic New Registration';
        objDocMaster.code__c='E-Certificate Generic New Registration';
        objDocMaster.DMS_Document_Index__c='abd';
        docMaster.add(objDocMaster);
        
        Document_Master__c objDocMaster1 = new Document_Master__c();
        objDocMaster1.Name = 'Transfer instruments - Off Plan';
        objDocMaster1.code__c='Transfer instruments - Off Plan';
        objDocMaster1.DMS_Document_Index__c='abd1';
        docMaster.add(objDocMaster1);
        
        insert docMaster;
        List<SR_Template_Docs__c> srTempDoc  = new List<SR_Template_Docs__c>();
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.On_Submit__c=true;
        objTempDocs.Document_Master__c = docMaster[0].Id;
        objTempDocs.Validate_In_Code_at_Step_No__c = objSRStep.Id;
        srTempDoc.add(objTempDocs);
        
        SR_Template_Docs__c objTempDocs1 = new SR_Template_Docs__c();
        objTempDocs1.On_Submit__c=true;
        objTempDocs1.Document_Master__c = docMaster[1].Id;
        objTempDocs1.Validate_In_Code_at_Step_No__c = objSRStep.Id;
        srTempDoc.add(objTempDocs1);
        
        insert srTempDoc;
        
        
        system.runAs(objUser) {

           List<Id> strRecType = new List<Id>();
           
            
            for(RecordType rectyp:[select id from RecordType where DeveloperName IN ('Application_of_Registration','Freehold_Transfer_Registration') and sObjectType='Service_Request__c']){
                strRecType.add(rectyp.Id);
            }
            
            
            Service_Request__c objSR = new Service_Request__c ();
            objSR.Customer__c = objAccount.Id;
            //objSR.Customer__c = objUser.Contact.AccountId;
            //objSR.Date_of_Declaration__c = Date.today();
            objSR.Email__c = 'gsyadav19@gmail.com';
            objSR.Entity_Name__c = 'LTD';
            
            
            
            if(strRecType[0]!=null)
                objSR.RecordTypeId = strRecType[0];
            
            objSR.Internal_SR_Status__c = srSubmittedStatus.Id;
            objSR.External_SR_Status__c = srSubmittedStatus.Id;
            
            insert objSR;
            
            Service_Request__c objSR1 = new Service_Request__c ();
            objSR1.Customer__c = objAccount.Id;
            //objSR.Customer__c = objUser.Contact.AccountId;
            //objSR.Date_of_Declaration__c = Date.today();
            objSR1.Email__c = 'gsyadav19@gmail.com';
            objSR1.Entity_Name__c = 'LTD';
            
            
            
            if(strRecType[1]!=null)
                objSR1.RecordTypeId = strRecType[1];
            
            objSR1.Internal_SR_Status__c = srSubmittedStatus.Id;
            objSR1.External_SR_Status__c = srSubmittedStatus.Id;
            
            insert objSR1;     
            
            
            List<Step__c> objStep = new List<Step__c>();
            Step__c objStep1 = new Step__c();
            objStep1.SR__c = objSR.Id;
            objStep1.Client_Name__c = 'abc';
            
            Step__c objStep2 = new Step__c();
            objStep2.SR__c = objSR1.Id;
            objStep2.Client_Name__c = 'abc';
            
            objStep.add(objStep1);
            objStep.add(objStep2); 
          
            insert objStep;
            
            list<SR_Doc__c> lstSR_Docs = new list<SR_Doc__c>();
            list<Attachment> lstAttachments = new list<Attachment>();
            
            SR_Doc__c objSRDoc = new SR_Doc__c();
            objSRDoc.Service_Request__c = objSR.Id;
            objSrDoc.Name = 'E-Certificate of Continuation';
            objSrDoc.Document_Master__c = objDocMaster.id;
            
            SR_Doc__c objSRDoc1 = new SR_Doc__c();
            objSRDoc1.Service_Request__c = objSR1.Id;
            objSrDoc1.Name = 'E-Certificate of Continuation';
            objSrDoc1.Document_Master__c = objDocMaster.id;
            
            lstSR_Docs.add( objSRDoc );
             lstSR_Docs.add( objSRDoc1 );
            
            insert lstSR_Docs;
            
            Attachment objAttachment1 = new Attachment();
            objAttachment1.Name = 'Test Document';
            objAttachment1.Body = blob.valueOf('test');
            objAttachment1.ParentId = lstSR_Docs[0].Id;
            lstAttachments.add(objAttachment1); 
            
            Attachment objAttachment2 = new Attachment();
            objAttachment2.Name = 'Test Document';
            objAttachment2.Body = blob.valueOf('test');
            objAttachment2.ParentId = lstSR_Docs[1].Id;
            lstAttachments.add(objAttachment2);
            
            insert lstAttachments;
            Test.StartTest();
                
            objSR.Internal_SR_Status__c = srApprovedStatus.Id;
            objSR.External_SR_Status__c = srApprovedStatus.Id;
            
            //update objSR;    

            list<Id> lstId = new list<Id>();
            lstId.add(objSR.Id);
            lstId.add(objSR1.Id);     
            cls_ApprovalNotificationWithAttachment.SendApprovalEmailWithAttachment(lstId);           
            cls_ApprovalNotificationWithAttachment.callfuturetoSendEmail(lstId);
            cls_ApprovalNotificationWithAttachment.sendLeaseCertificate(objSR.Id);
            cls_ApprovalNotificationWithAttachment.sendLeaseCertificate(objSR1.Id);
          
            Test.stopTest();
        }
  
           
  }
  static testMethod void TestMethod_EmailService_test2(){
  
        //Test_FitoutServiceRequestUtils.useDefaultUser();
        //Test_FitoutServiceRequestUtils.setToInternalUser();
        //Test_FitoutServiceRequestUtils.prepareTestUser(true);
        
        
       List<Send_Email_with_Docs__c> sendEmailSetting = new List<Send_Email_with_Docs__c>();
        
        Send_Email_with_Docs__c setting = new Send_Email_with_Docs__c();
        setting.Name = 'Lease_Registration';
        setting.Document_type__c = 'Lease_Certificate_Developer';
        setting.Document_Type_2__c = '';
        setting.Document_Type_3__c = '';       
        setting.Email_Template_Name__c = 'Application_Approval_Notification_AoR';
        setting.Record_Type_Name__c = 'Lease_Registration';
        setting.Applicable_Legal_Structure__c= 'LTD,LTD SPC,LTD PCC,LTD IC,LTD National,LTD Supra National,LLC,LLP,RLLP,FRC,GP,RP,RLP,LP,NPIO,Foundation'; 
        sendEmailSetting.add(setting);
        
        Send_Email_with_Docs__c setting1 = new Send_Email_with_Docs__c();
        setting1.Name = 'Freehold_Transfer_Registration';
        setting1.Document_type__c = 'Transfer instruments - Off Plan';
        setting1.Document_Type_3__c = '';       
        setting1.Email_Template_Name__c = 'Application_Approval_Notification_AoR';
        setting1.Record_Type_Name__c = 'Freehold_Transfer_Registration';
        setting1.Applicable_Legal_Structure__c= 'LTD,LTD SPC,LTD PCC,LTD IC,LTD National,LTD Supra National,LLC,LLP,RLLP,FRC,GP,RP,RLP,LP,NPIO,Foundation'; 
        sendEmailSetting.add(setting1); 
        
        insert sendEmailSetting;     
        
        User objUser = Test_FitoutServiceRequestUtils.getTestUser();
        objUser.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services';
        update objUser;
            
         Account objAccount = new Account();
         objAccount.Name = 'Test Account';
         objAccount.Phone = '1234567890';
         objAccount.Sector_Classification__c = 'Investment Fund';
         objAccount.Legal_Type_of_Entity__c  = 'LTD';   
         insert objAccount;

        
        SR_Steps__c objSRStep = new SR_Steps__c();
        objSRStep.Step_RecordType_API_Name__c = 'General';
        objSRStep.Summary__c = 'Approve Request';
        objSRStep.Step_No__c = 1.0;
        insert objSRStep;
        
        SR_Status__c srSubmittedStatus = new SR_Status__c ();
        srSubmittedStatus.name  = 'Submitted';
        srSubmittedStatus.Code__c = 'Submitted';
        insert srSubmittedStatus;

        SR_Status__c srApprovedStatus = new SR_Status__c ();
        srApprovedStatus.name  = 'Approved';
        srApprovedStatus.Code__c = 'Approved';
        insert srApprovedStatus;
        
        system.debug('srStatus'+srSubmittedStatus);     
        List<Document_Master__c> docMaster = new List<Document_Master__c>();
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Lease_Certificate_Developer';
        objDocMaster.code__c='Lease_Certificate_Developer';
        objDocMaster.DMS_Document_Index__c='abd';
        docMaster.add(objDocMaster);
        
        Document_Master__c objDocMaster1 = new Document_Master__c();
        objDocMaster1.Name = 'Transfer instruments - Off Plan';
        objDocMaster1.code__c='Transfer instruments - Off Plan';
        objDocMaster1.DMS_Document_Index__c='abd1';
        docMaster.add(objDocMaster1);
        
        insert docMaster;
        List<SR_Template_Docs__c> srTempDoc  = new List<SR_Template_Docs__c>();
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.On_Submit__c=true;
        objTempDocs.Document_Master__c = docMaster[0].Id;
        objTempDocs.Validate_In_Code_at_Step_No__c = objSRStep.Id;
        srTempDoc.add(objTempDocs);
        
        SR_Template_Docs__c objTempDocs1 = new SR_Template_Docs__c();
        objTempDocs1.On_Submit__c=true;
        objTempDocs1.Document_Master__c = docMaster[1].Id;
        objTempDocs1.Validate_In_Code_at_Step_No__c = objSRStep.Id;
        srTempDoc.add(objTempDocs1);
        
        insert srTempDoc;
        
        
        system.runAs(objUser) {

           List<Id> strRecType = new List<Id>();
           
            
            for(RecordType rectyp:[select id from RecordType where DeveloperName IN ('Freehold_Transfer_Registration','Lease_Registration') and sObjectType='Service_Request__c']){
                strRecType.add(rectyp.Id);
            }
            
            
            Service_Request__c objSR = new Service_Request__c ();
            objSR.Customer__c = objAccount.Id;
            //objSR.Customer__c = objUser.Contact.AccountId;
            //objSR.Date_of_Declaration__c = Date.today();
            objSR.Email__c = 'gsyadav19@gmail.com';
            objSR.Entity_Name__c = 'LTD';
            
            
            
            if(strRecType[0]!=null)
                objSR.RecordTypeId = strRecType[0];
            
            objSR.Internal_SR_Status__c = srSubmittedStatus.Id;
            objSR.External_SR_Status__c = srSubmittedStatus.Id;
            
            insert objSR;
            
            Service_Request__c objSR1 = new Service_Request__c ();
            objSR1.Customer__c = objAccount.Id;
         
            objSR1.Email__c = 'gsyadav19@gmail.com';
            objSR1.Entity_Name__c = 'LTD';
            
            
            
            if(strRecType[1]!=null)
                objSR1.RecordTypeId = strRecType[1];
            
            objSR1.Internal_SR_Status__c = srSubmittedStatus.Id;
            objSR1.External_SR_Status__c = srSubmittedStatus.Id;
            
            insert objSR1;     
            
            
            
              
            
            List<Step__c> objStep = new List<Step__c>();
            Step__c objStep1 = new Step__c();
            objStep1.SR__c = objSR.Id;
            objStep1.Client_Name__c = 'abc';
            
            Step__c objStep2 = new Step__c();
            objStep2.SR__c = objSR1.Id;
            objStep2.Client_Name__c = 'abc';
            
           
            
            objStep.add(objStep1);
            objStep.add(objStep2); 
          
            insert objStep;
            
            list<SR_Doc__c> lstSR_Docs = new list<SR_Doc__c>();
            list<Attachment> lstAttachments = new list<Attachment>();
            
            SR_Doc__c objSRDoc = new SR_Doc__c();
            objSRDoc.Service_Request__c = objSR.Id;
            objSrDoc.Name = 'E-Certificate of Continuation';
            objSrDoc.Document_Master__c = objDocMaster.id;
            
            SR_Doc__c objSRDoc1 = new SR_Doc__c();
            objSRDoc1.Service_Request__c = objSR1.Id;
            objSrDoc1.Name = 'E-Certificate of Continuation';
            objSrDoc1.Document_Master__c = objDocMaster.id;
            
            
            
            lstSR_Docs.add( objSRDoc );
             lstSR_Docs.add( objSRDoc1 );
         
            insert lstSR_Docs;
            
            Attachment objAttachment1 = new Attachment();
            objAttachment1.Name = 'Test Document';
            objAttachment1.Body = blob.valueOf('test');
            objAttachment1.ParentId = lstSR_Docs[0].Id;
            lstAttachments.add(objAttachment1); 
            
            Attachment objAttachment2 = new Attachment();
            objAttachment2.Name = 'Test Document';
            objAttachment2.Body = blob.valueOf('test');
            objAttachment2.ParentId = lstSR_Docs[1].Id;
            lstAttachments.add(objAttachment2);
           
            
            insert lstAttachments;
            Test.StartTest();
                
            objSR.Internal_SR_Status__c = srApprovedStatus.Id;
            objSR.External_SR_Status__c = srApprovedStatus.Id;
            
            //update objSR;    

            list<Id> lstId = new list<Id>();
            lstId.add(objSR.Id);
            lstId.add(objSR1.Id);
              
            //cls_ApprovalNotificationWithAttachment.SendApprovalEmailWithAttachment(lstId);           
            //cls_ApprovalNotificationWithAttachment.callfuturetoSendEmail(lstId);
            cls_ApprovalNotificationWithAttachment.sendLeaseCertificate(objSR.Id);
            cls_ApprovalNotificationWithAttachment.sendLeaseCertificate(objSR1.Id);
           
            Test.stopTest();
        }
  
           
  }

}