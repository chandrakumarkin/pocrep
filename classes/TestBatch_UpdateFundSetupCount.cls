@istest
public class TestBatch_UpdateFundSetupCount {
    
    @istest
    public static void testBatchUpdate(){
        Account acc  = new Account();
        acc.name = 'test';  
        acc.Is_Commercial_Permission__c = 'Yes';
        acc.Company_Type__c = 'Financial-Related';
        acc.ROC_Status__c = 'Active';
        acc.Sector_Classification__c = 'Authorized Firm';       
        insert acc;
                
        Account acc1  = new Account();
        acc1.name = 'test';  
        acc1.Is_Commercial_Permission__c = 'Yes';
        acc1.Company_Type__c = 'Financial-Related';
        acc1.ROC_Status__c = 'Active';
        acc1.Sector_Classification__c = 'Investment Fund';   
        acc1.Hosting_Company__c = acc.Id;
        insert acc1;
        
        Test.startTest();
        Database.executeBatch(new Batch_UpdateFundSetupCount());
        Test.stopTest();
        Account hostingAcc = [SELECT Id, FundSetupCount__c FROM Account WHERE Id = :acc.Id];
        system.assertEquals(1, hostingAcc.FundSetupCount__c);
        
    }
}