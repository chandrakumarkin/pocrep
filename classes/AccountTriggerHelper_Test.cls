/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AccountTriggerHelper_Test{

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        objAcc.RecordTypeId = [select Id,Name,DeveloperName from RecordType where DeveloperName ='Contractor_Account' AND SobjectType='Account' limit 1].id;
        insert objAcc;
        
        License__c objLic = new License__c();
        objLic.Account__c = objAcc.Id;
        objLic.License_Issue_Date__c = system.today();
        objLic.License_Expiry_Date__c = system.today().addDays(365);
        objLic.Status__c = 'Active';
        insert objLic;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAcc.Id;
        objSR.recordTypeid = [select Id,Name,DeveloperName from RecordType where DeveloperName='Event_Service_Request' AND SobjectType='Service_Request__c'].id;
        insert objSR;
        
        Contact con = new Contact(lastname ='TestEventService', AccountId=objAcc.id);
        con.recordTypeId=[select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact'].id;
        insert con;
        Map < Id, Account > accountMap = new Map < Id, Account >();
        accountMap.put(objAcc.id,objAcc);
        test.startTest();
	        objAcc.RecordTypeId = [select Id,Name,DeveloperName from RecordType where DeveloperName ='Event_Account' AND SobjectType='Account' limit 1].id;
	        try{
	       	//update objAcc;
	       	AccountTriggerHelper.handleAfterUpdateAction(accountMap);
	       }catch(Exception ex){
	       	system.debug(ex.getMessage());
	       }
        test.stopTest();
    }
    static testMethod void myUnitTestTwo() {
        // TO DO: implement unit test
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        License__c objLic = new License__c();
        objLic.Account__c = objAcc.Id;
        objLic.License_Issue_Date__c = system.today();
        objLic.License_Expiry_Date__c = system.today().addDays(365);
        insert objLic;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAcc.Id;
        objSR.Contractor__c = objAcc.id;
        objSR.recordTypeid = [select Id,Name,DeveloperName from RecordType where DeveloperName ='Contractor_Registration' AND SobjectType='Service_Request__c' limit 1].id;
        objLic.Status__c = 'Active';
        objSr.First_Name__c =  'firstName';
        objSR.last_Name__c = 'Lastname';
        objSR.Email_Address__c = 'Lastname@gmail.com';
        objSR.Passport_Number__c = 'Passpot1234';
        insert objSR;
        
        Contact con = new Contact(lastname ='TestEventService', AccountId=objAcc.id);
        con.recordTypeId=[select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact'].id;
        con.FirstName = 'firstName';
        con.LastName = 'Lastname';
        con.email =  'Lastname@gmail.com';
        con.Passport_No__c = 'Passpot1234';
        insert con;
        
        Map < Id, Account > accountMap = new Map < Id, Account >();
        accountMap.put(objAcc.id,objAcc);
        test.startTest();
        	objAcc.RecordTypeId = [select Id,Name,DeveloperName from RecordType where DeveloperName ='Event_Account' AND SobjectType='Account' limit 1].id;
	       try{
	       	AccountTriggerHelper.handleAfterUpdateAction(accountMap);
	       	//update objAcc;
	       }catch(Exception ex){
	       	system.debug(ex.getMessage());
	       }
        test.stopTest();
    }
    
    
   static testMethod void myUnitTest1(){
   
   List<Account> lstAccount = new List<Account>();
   Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        lstAccount.add(objAccount);
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
   
     map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
   
      
        
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'Fit - Out Service Request';
        objTemplate1.SR_RecordType_API_Name__c = 'Fit - Out Service Request';
        objTemplate1.Active__c = true;
        insert objTemplate1;
        
        
        
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Landlord NOC Issued';
        objStepType1.Code__c = 'Landlord NOC Issued';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Landlord NOC Issued';
        insert objStepType1;
        
        
    Service_Request__c objSR3 = new Service_Request__c();
    objSR3.Customer__c = objAccount.Id;
    objSR3.Email__c = 'testsr@difc.com';    
    objSR3.Contact__c = objContact.Id;
    objSR3.Express_Service__c = false;
    objSR3.SR_Template__c = objTemplate1.Id;
    
    insert objSR3;
    
    Status__c objStatus = new Status__c();
    objStatus.Name = 'Pending';
    objStatus.Type__c = 'Pending';
    objStatus.Code__c = 'Pending';
    insert objStatus;
        Status__c objStatus1 = new Status__c();
    objStatus1.Name = 'Completed';
    objStatus1.Type__c = 'Completed';
    objStatus1.Code__c = 'Completed';
    insert objStatus1;
        
        step__c objStep = new Step__c();
        objStep.SR__c = objSR3.Id;
        //objStep2.OwnerId = mapNameOwnerId.get('Customer');
        //objStep2.Biometrics_Required__c = 'No';
        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        //objStep.Step_Name__c='Landlord NOC Issued';
        objStep.status__c = objStatus.id;
        insert objStep;
        //Step__c stepRecord = [Select id,step_Name__c from Step__c where id=:objStep.id];
        // system.assertEquals(null, stepRecord);
        AccountTriggerHelper.updatePendingLandlordNocStep(new Set<id>{objAccount.Id});
        
         Profile profile1 = [Select Id from Profile where name = 'DIFC Customer Community User Custom'];
        User portalAccountOwner1 = new User(ProfileId = profile1.Id,Username = System.now().millisecond() + 'test2@test.com',ContactId=objContact.Id,
                                            Alias = 'TestDIFC',Email='test123@Difc.com',EmailEncodingKey='UTF-8',Firstname='Test',Lastname='DIFC',
                                            LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Chicago',isActive=true,Community_User_Role__c='Company Services');
        Database.insert(portalAccountOwner1);
         
        system.runas(portalAccountOwner1){
        	try{
        AccountTriggerHelper.createaBankAccount(lstAccount);
        	}
        	catch(exception ex){
        		
        	}
        }
        
   }
    
    static testMethod void myUnitTest3() {
        // TO DO: implement unit test
        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community Plus User Custom']; 
        
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;
        
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='integration@difc.com.uatfullx',contactid = insertNewContacts[0].Id);
        
      
        
        system.runAs(u){
        	Account objAcc = new Account();
        	objAcc.Name = 'Test Acc';
        	objAcc.RecordTypeId = [select Id,Name,DeveloperName from RecordType where DeveloperName ='Contractor_Account' AND SobjectType='Account' limit 1].id;
        	insert objAcc;
        }
        
        test.startTest();
        	Account thisAccount = [SELECT Id,ownerId FROM Account WHERE Name='Test Acc'];
        	thisAccount.OwnerId = userinfo.getUserId();
        	update thisAccount;
        test.stopTest();
    }
}