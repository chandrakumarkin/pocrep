/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 14-Aug-2014   
    ---------------------------------------------------------------------------------------------------------------------
    Modification History
    ---------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    ---------------------------------------------------------------------------------------------------------------------             
    V1.0    27-08-2015  Saima         RORP User Access Form page changes
    V1.1    07-01-2016  Saima         Property Listing User Form changes
    V1.2    25-01-2016  Swati         Event Access Form
    V1.3    15-09-2016  Claude        Ticket #3336 && #3383 - Added Event Request Access as part of record type list for rendering messages
    V1.4    28-12-2016  Sravan        Ticket# 3216 - added flag to identifyunderformation account
*******************************************************************************************/
//Class used to display PDF form with New User Access Details.

global without sharing class FormPreviewController {

public String DocUrl{get;set;}
String SRId;
public Boolean isRender{get;set;}
public boolean isUFAcc{get;set;} // V1.4

public FormPreviewController(){
    SRId=ApexPages.currentPage().getParameters().get('SRNo');
    list<Service_Request__c> srlist = [Select id,name,Record_Type_Name__c,recordtype.DeveloperName,Customer__r.ROC_Status__c from Service_Request__c where id=:SRId];
    if(srlist.size()>0 && srlist[0].Record_Type_Name__c =='RORP_User_Access_Form' || 
        srlist[0].Record_Type_Name__c =='Property_Listing_User_Access_Form' || //V1.0 - RORP User Access Form , V1.1 - Property Listing User Form 
        srList[0].Record_Type_Name__c =='Request_Access_For_Events') // V1.3 - Claude - Added 
        isRender = false;
    else{
        isRender = true;
        if(srList[0].Customer__r.ROC_Status__c == 'Under Formation')
            isUFAcc = true;  //V1.4
    }
}

/*
public void FetchDoc() {
        SRId=ApexPages.currentPage().getParameters().get('SRNo');
        System.debug('SRRRRID==='+SRId);
        List <SR_Doc__c> SRDoc = [Select Doc_ID__c, Preview_Document__c from SR_Doc__c where (Service_Request__c = :SRId AND Name = 'User Access Form')];
        DocUrl = SRDoc[0].Doc_ID__c;
         System.debug(DocUrl);
        
            
    }
    */
   
 public void loadPDF(){

        SRId=ApexPages.currentPage().getParameters().get('SRNo');
        System.debug('SRRRRID==='+SRId);
        List <SR_Doc__c> SRDoc = [Select Doc_ID__c, Preview_Document__c from SR_Doc__c where (Service_Request__c = :SRId AND Name like '%User%' AND Doc_ID__c != Null)];
        if(SRDoc != Null && SRDoc.isEmpty()==false)
        {
            isRender = false;
            DocUrl = SRDoc[0].Doc_ID__c;        
        }
 }


  public void loadDoc(){

        SRId=ApexPages.currentPage().getParameters().get('UserSRNo');
        System.debug('SRRRRID==='+SRId);
        List<Attachment> userDoc = [Select Id,parentId,Name from Attachment where parentId =:SRId and Name='New User Access Form.pdf'];
        //List <SR_Doc__c> SRDoc = [Select Doc_ID__c, Preview_Document__c from SR_Doc__c where (Service_Request__c = :SRId AND Name like '%User%' AND Doc_ID__c != Null)];
        if(userDoc!= Null && userDoc.isEmpty()==false)
        {
            isRender = false;
            DocUrl =userDoc[0].Id;        
        }
 }
 
 
   
}