@istest
public class TestROC_Utility {
    @istest
    public static void updatePriceItem(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Account';
        objAcc.Phone = '1234567890';
        objAcc.Sector_Classification__c = 'Authorized Firm';
        insert objAcc;
        
        contact con = new contact();
        con.Account =objAcc;
        con.role__c ='Company Services';   
        con.firstname ='test';  
        con.lastname ='test';
        insert con;
        
        SR_Status__c srStatus = new SR_Status__c ();
        srStatus.name  = 'Draft';
        srStatus.Code__c = 'DRAFT';
        insert srStatus;
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Client_Information','Lease_Registration','Surrender_Termination_of_Lease_and_Sublease','Freehold_Transfer_Registration','Company_buyer','Developer','Individual_Buyer','Company_Seller','RORP_Authorized_Signatory','RORP_Shareholder','RORP_Shareholder_Company')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        Service_Request__c objSR3= new Service_Request__c();
        objSR3.Customer__c = objAcc.Id;
        objSR3.Date_of_Declaration__c = Date.today();
        objSR3.Email__c = 'test@test.com';
        objSR3.RecordTypeId = mapRecordType.get('Client_Information');    
        objSR3.Internal_SR_Status__c = srStatus.Id;
        objSR3.Express_Service__c = false;
        objSR3.Service_Category__c = 'New';        
        //objSR3.Unit__c = lstUnits[0].Id;
        objSR3.Type_of_Lease__c = 'Lease';
        objSR3.Rental_Amount__c = 1234;
        insert objSR3;  
        
        SR_Price_Item__c priceItm = new SR_Price_Item__c();
        priceItm.ServiceRequest__c = objSR3.Id;
        priceItm.Price__c = 123;
        priceItm.Status__c = 'Added';
        insert priceItm;
        
        priceItm = new SR_Price_Item__c();
        priceItm.ServiceRequest__c = objSR3.Id;
        priceItm.Price__c = 28;
        priceItm.Status__c = 'Added';
        insert priceItm;
        
        Test.startTest();
        ROC_Utility.updatePricesSR(New List<Service_Request__c> {objSR3});
        ROC_Utility.Incumbency_Validate(objSR3);
        objSR3.Doyouwishtoaddpassportnumbers__c = 'Yes';
        update objSR3;
        ROC_Utility.Incumbency_Validate(objSR3);
        Test.stopTest();
    }
     @istest
    public static void updateICCCompanyTest(){
        Account objAcc = new Account();
        objAcc.Name = 'Test Account';
        objAcc.Phone = '1234567890';
        objAcc.Sector_Classification__c = 'Authorized Firm';
        insert objAcc;
        
        contact con = new contact();
        con.Account =objAcc;
        con.role__c ='Company Services';   
        con.firstname ='test';  
        con.lastname ='test';
        insert con;
        
        SR_Status__c srStatus = new SR_Status__c ();
        srStatus.name  = 'Draft';
        srStatus.Code__c = 'DRAFT';
        insert srStatus;
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Filing_of_IC_company')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        Service_Request__c objSR3= new Service_Request__c();
        objSR3.Customer__c = objAcc.Id;
        objSR3.Date_of_Declaration__c = Date.today();
        objSR3.Email__c = 'test@test.com';
        objSR3.RecordTypeId = mapRecordType.get('Filing_of_IC_company');    
        objSR3.Internal_SR_Status__c = srStatus.Id;
        objSR3.Express_Service__c = false;
        objSR3.Service_Category__c = 'New';        
        //objSR3.Unit__c = lstUnits[0].Id;
        objSR3.Type_of_Lease__c = 'Lease';
        objSR3.Rental_Amount__c = 1234;
        ObjSR3.Transfer_to_account__c=objAcc.id;
        insert objSR3;  
        
        Step__c stpObj = new Step__c();
        stpObj.SR__c=objSR3.id;
        Insert stpObj;
        SR_Price_Item__c priceItm = new SR_Price_Item__c();
        priceItm.ServiceRequest__c = objSR3.Id;
        priceItm.Price__c = 123;
        priceItm.Status__c = 'Added';
        insert priceItm;
        
        priceItm = new SR_Price_Item__c();
        priceItm.ServiceRequest__c = objSR3.Id;
        priceItm.Price__c = 28;
        priceItm.Status__c = 'Added';
        insert priceItm;
        Relationship__c relObj= new Relationship__c();
        relObj.Subject_Account__c=objAcc.id;
        relObj.Active__c=true;
        relObj.Relationship_Type__c = 'Incorporated Cell Company';
        insert relObj;
        Test.startTest();
        String success=ROC_Utility.updateICCCompany(stpObj.ID);
        ROC_Utility.CreateICCRelationship(objAcc.id,objAcc.id);
        //ROC_Utility.CreateAddressChangeSR(objSR3.ID);
        Test.stopTest();
    }
}