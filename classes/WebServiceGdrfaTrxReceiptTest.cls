/******************************************************************************************************
 *  Author   : Kumar Utkarsh
 *  Date     : 02-May-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    02-May-2021  Utkarsh         Created
* This is a Test class for WebServiceGdrfaFetchTrxReceiptTest and WebServiceGdrfaFetchTrxReceiptScheduler
*******************************************************************************************************/
@isTest
public class WebServiceGdrfaTrxReceiptTest {
    
    @testSetup static void testSetupdata(){
        Service_Request__c testSr = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        Id RecordTypeIdSr = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-Renewal').getRecordTypeId();
        testSr.RecordTypeId = RecordTypeIdSr;
        testSr.Mobile_Number__c = '';
        testSr.Date_of_Birth__c = Date.newInstance(2001, 02, 03);
        insert testSr;
        Status__c status = new Status__c();
        status.Code__c = 'AWAITING_REVIEW';
        insert status;
        List<Step_Template__c> stepTemplateList = new List<Step_Template__c>();
        List<Step__c> stepList = new List<Step__c>();
        Step_Template__c stepTemplate3 = new Step_Template__c();
        stepTemplate3.Name = 'Renewal form is typed';
        stepTemplate3.Code__c = 'Renewal form is typed';
        stepTemplate3.Step_RecordType_API_Name__c = 'General';
        stepTemplateList.add(stepTemplate3);
        Status__c status1 = new Status__c();
        status1.Code__c = 'CLOSED';
        status1.Name = 'closed';
        insert status1;
        Step__c step5 = new Step__c();
        step5.Step_Template__c = stepTemplate3.Id;
        step5.Transaction_Number__c = '1232421786';
        step5.DNRD_Receipt_No__c = '1234561321192';
        step5.Payment_Date__c = Date.newInstance(2021, 02, 03);
        step5.Status__c = status1.Id;
        step5.SR__c = testSr.Id;
        step5.Is_Transaction_Receipt__c = False;
        stepList.add(step5);
        insert stepList;
        
        Document_Master__c docMaster = new Document_Master__c();
        docMaster.code__c = 'Transaction_Receipt_Document';
        insert docMaster;
        
        SR_Doc__c srDoc = new SR_Doc__c();
        srDoc.Service_Request__c = testSr.Id;
        srDoc.Document_Master__c = docMaster.Id;
        srDoc.Name = 'Transaction Receipt Document';
        Insert srDoc;
    }
    
     static testMethod void webServiceGdrfaTxReceiptTest(){
        WebServiceGdrfaFetchTrxReceiptBatch mbc = new WebServiceGdrfaFetchTrxReceiptBatch();
        List<Step__c> scope = [SELECT Id, DNRD_Receipt_No__c, SR__c FROM Step__c];
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new webServiceGdrfaAppFeeMock());
        mbc.start(null);
        mbc.execute(null, scope);
        mbc.finish(null);
        Test.stopTest();
    }
}