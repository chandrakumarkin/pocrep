/*
* Used in Lightning component: OB_ManageGuardians
* Guardians Screen 
 */
public without sharing class OB_ManageGuardiansController {
	//reqest wrpper
	public static OB_ManageGuardiansController.RequestWrapper reqWrap;
	//response wrpper
	public static OB_ManageGuardiansController.ResponseWrapper respWrap;

	/*
    * Display existing SR Doc of that amendment record
    */
	@AuraEnabled
	public static map<string, map<string, string>> viewSRDocs(string srId, string amendmentId) {
		//Preparing the SRDocs for the amendment
		map<string, map<string, string>> mapSRDocs = new map<string, map<string, string>>();
		map<string, string> mapIdName = new map<string, string>();
		/*if(String.isNotBlank(amendmentId)) {
			for(HexaBPM__SR_Doc__c srDoc :[select id, File_Name__c, HexaBPM__Document_Master__r.HexaBPM__Code__c from HexaBPM__SR_Doc__c
										    where HexaBPM__Service_Request__c = :srId and HexaBPM_Amendment__c = :amendmentId
										    and HexaBPM__Document_Master__r.HexaBPM__Code__c in('Passport_Copy_Individual')]) {

				mapIdName.put('Id', srDoc.id);
				mapIdName.put('FileName', srDoc.File_Name__c);
				mapSRDocs.put(srDoc.HexaBPM__Document_Master__r.HexaBPM__Code__c, mapIdName);

			}
		}*/
		return mapSRDocs;
	}
	
	 /*
    * File reader
    */
	@AuraEnabled
	public static Id saveChunkBlob(Id parentId, String srId, String fileName, String base64Data, String contentType, String fileId, string documentMasterCode, string strDocumentId) {
		//Create SR Doc and set that id as parent Id.
		/*try{

			Id srDocumentId;
			if(String.isBlank(fileId)) {
				if(String.isBlank(strDocumentId))
					srDocumentId = OB_QueryUtilityClass.createSRDoc(srId, parentId, documentMasterCode);
				else {
					srDocumentId = Id.valueOf(strDocumentId);
					System.debug('srDocumentId>' + srDocumentId);
				}
				//Saving the file to SRDocs under amendment.
				if(srDocumentId != null)
					fileId = OB_QueryUtilityClass.saveTheFile(srDocumentId, fileName, base64Data, contentType);
			} else {
				OB_QueryUtilityClass.appendToFile(fileId, base64Data);
			}

			system.debug(fileId);
			return Id.valueOf(fileId);

		} 
		catch(Exception ex) {
			return null;
		}*/
		return null;
	}




	/*
	 * Lightning component: OB_ManageGuardians
	 * On load will fetch existing list of Guardians amendment of Service Request
	 */
	@AuraEnabled
	public static ResponseWrapper getExistingAmendment(String reqWrapPram) {

		system.debug('##########getExistingAmendment##########');
		//reqest wrpper
		reqWrap = (OB_ManageGuardiansController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_ManageGuardiansController.RequestWrapper.class);
		//response wrpper
		respWrap = new OB_ManageGuardiansController.ResponseWrapper();

		String srId = reqWrap.srId;

		respWrap = getAmendmentList(srId, respWrap,reqWrap);
		for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
												    AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
			respWrap.ButtonSection = sectionObj;
		}
		return respWrap;

	}
	/*
	* Query SR and amendment list to be display on screen 
	* show existing amendment
	* query declaration from metadata
	*/
	public static ResponseWrapper getAmendmentList( String srId, 
													ResponseWrapper respWrap, 
													RequestWrapper reqWrap) 
	{
		String flowId = reqWrap.flowId;
		String declarationIndividualText = '';
		String declarationCorporateText = '';

		String indRecTyp = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Individual');
		String  bodyRecTyp = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Body_Corporate');

		for(Declarations__mdt declare :[Select ID, Declaration_Text__c, Application_Type__c, Field_API_Name__c, Record_Type_API_Name__c, Object_API_Name__c
									    From Declarations__mdt
									    Where (Record_Type_API_Name__c = 'Individual' OR Record_Type_API_Name__c = 'Body_Corporate')
									    AND Field_API_Name__c = 'I_Agree_Guardian__c'
									    AND Object_API_Name__c = 'HexaBPM_Amendment__c']) {

			if(String.IsNotBlank(declare.Record_Type_API_Name__c) && declare.Record_Type_API_Name__c.EqualsIgnorecase('Individual')) {
				declarationIndividualText = declare.Declaration_Text__c;
			} else {
				declarationCorporateText = declare.Declaration_Text__c;
			}

		}



		//* v1.0 Merul  26 march 2020  Libary implementation
		Map<Id,AccountContactRelation> mapRelatedAccForAmed = new Map<Id,AccountContactRelation>();
		Map<Id,AccountContactRelation> mapRelatedAccForRelshp = new Map<Id,AccountContactRelation>();
		
		 //* v1.0 Merul  26 march 2020  Libary implementation
		 // Moving out the declarations
		OB_ManageGuardiansController.SRWrapper srWrap = new OB_ManageGuardiansController.SRWrapper();
		srWrap.amedWrapLst = new List<OB_ManageGuardiansController.AmendmentWrapper>();
		//srWrap.amedShrHldrWrapLst = new List<OB_ManageGuardiansController.AmendmentWrapper>();

		//* v1.0 Merul  26 march 2020  Libary implementation
		HexaBPM__Service_Request__c currentSR = new HexaBPM__Service_Request__c();
		
		// * v1.0 Merul  26 march 2020  Libary implementation
		Map<String,AmendmentWrapper> mapExistingAmed = new  Map<String,AmendmentWrapper>();
		Map<String,AmendmentWrapper> mapShareholder = new  Map<String,AmendmentWrapper>();
		Map<String,AmendmentWrapper> mapRelShpToAmedWrap = new  Map<String,AmendmentWrapper>();
		Map<String,AmendmentWrapper> mapShareholderCombine = new  Map<String,AmendmentWrapper>();



		
            // getting all related account based on current loggedin user.
            User loggedInUser = new User();
            String contactId;
       
            for(User UserObj : [
                                    SELECT id,
                                        contactid,
                                        Contact.AccountId,
                                        ProfileId 
                                    FROM User 
                                    WHERE Id = :UserInfo.getUserId() 
                                    LIMIT 1
                                ]
                ) 
            {
                loggedInUser =  UserObj;
                contactId = UserObj.contactid;
			}
			
			
			if( string.isNotBlank(contactId) )
			{

					for( 
						AccountContactRelation accConRel :  [
																SELECT Id,
																		AccountId,
																		ContactId,
																		Roles,
																		Access_Level__c,
																		Account.ROC_Status__c
																FROM AccountContactRelation
																WHERE ContactId =:contactId
																AND Roles includes ('Company Services')
														
															]
					)
					{
						if( accConRel.Account.ROC_Status__c == 'Under Formation' 
									|| accConRel.Account.ROC_Status__c == 'Account Created' )    
									{
										mapRelatedAccForAmed.put( accConRel.AccountId , accConRel);
									}
						
						if( accConRel.Account.ROC_Status__c == 'Active' 
									|| accConRel.Account.ROC_Status__c == 'Not Renewed' )    
									{
										mapRelatedAccForRelshp.put( accConRel.AccountId , accConRel);
									}
					}

					reqWrap.mapRelatedAccForRelshp = mapRelatedAccForRelshp;
					system.debug('@@@@@@@@@@ mapRelatedAccForRelshp '+ mapRelatedAccForRelshp);
	
	
					reqWrap.mapRelatedAccForAmed = mapRelatedAccForAmed;
					system.debug('@@@@@@@@@@ mapRelatedAcc '+ mapRelatedAccForAmed);

					// getting relationship
                	List<Relationship__c> lstRel = OB_libraryHelper.getRelationShip( mapRelatedAccForRelshp );
					for(Relationship__c relShip : lstRel)                           
					{
						system.debug('############## relShip '+relShip);
						OB_ManageGuardiansController.AmendmentWrapper amedWrap = new OB_ManageGuardiansController.AmendmentWrapper();
						amedWrap.amedObj = new HexaBPM_Amendment__c();
						if(relShip.Amendment_ID__c != null){
							amedWrap.relatedAmendID = relShip.Amendment_ID__c;
						}else{
							amedWrap.relatedAmendID = relShip.Sys_Amendment_Id__c;
						}
						 amedWrap.RelatedRelId = relShip.id;
						// amedWrap.lookupLabel = (amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name :'');
						if( relShip.Object_Contact__c != NULL )
						{
							
							amedWrap.amedObj = OB_libraryHelper.setAmedFromObjContact(amedWrap.amedObj,relShip);
							amedWrap.isIndividual = true;
							amedWrap.displayName = (String.isNotBlank( amedWrap.amedObj.First_Name__c) ? amedWrap.amedObj.First_Name__c : '') +' '+  (String.isNotBlank( amedWrap.amedObj.Last_Name__c) ? amedWrap.amedObj.Last_Name__c : '');
							//amedWrap.amedObj.recordTypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Individual');
							amedWrap.amedObj.recordTypeId = indRecTyp;
							
							system.debug('@@@@@@@@@@@   amedWrap.amedObj.recordTypeId  '+ amedWrap.amedObj.recordTypeId );
	
							String passport = ( String.isNotBlank( amedWrap.amedObj.Passport_No__c )  ? amedWrap.amedObj.Passport_No__c : '' );
							String nationality = ( String.isNotBlank( amedWrap.amedObj.Nationality_list__c )  ? amedWrap.amedObj.Nationality_list__c : '' ); 
							String uniqKey = passport + nationality;
							if( String.isNotBlank( uniqKey  ) )
							{
								mapRelShpToAmedWrap.put( uniqKey.toLowercase() , amedWrap);
							}
												  
	
						}
						else if( relShip.Object_Account__c != NULL )
						{
							
							amedWrap.amedObj = OB_libraryHelper.setAmedFromObjAccount(amedWrap.amedObj,relShip);
							amedWrap.isBodyCorporate = true;
							amedWrap.displayName = amedWrap.amedObj.Company_Name__c;
							//amedWrap.amedObj.recordTypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Body_Corporate');
							amedWrap.amedObj.recordTypeId = bodyRecTyp;
	
							if( String.isNotBlank( amedWrap.amedObj.Registration_No__c  ) )
							{
								mapRelShpToAmedWrap.put( amedWrap.amedObj.Registration_No__c.toLowercase(), amedWrap);
							}
							
						}
	
					}
					system.debug('@@@@@@@@@@ mapRelShpToAmedWrap '+ mapRelShpToAmedWrap);



			}
			




		system.debug('=====declarationIndividualText=====' + declarationIndividualText);
		system.debug('=====declarationCorporateText=====' + declarationCorporateText);
		//OB_ManageGuardiansController.SRWrapper srWrap = new OB_ManageGuardiansController.SRWrapper();

		/*
		for(HexaBPM__Service_Request__c serObj :[SELECT id, HexaBPM__External_SR_Status__c, HexaBPM__External_SR_Status__r.Name, 
														HexaBPM__Internal_SR_Status__c, HexaBPM__Internal_Status_Name__c, 
														(SELECT id, Name__c, 
																First_Name__c, 
																Last_Name__c, 
																Middle_Name__c, 
																Nationality_list__c, 
																Date_of_Birth__c, 
																Address__c, 
																Passport_No__c, 
																RecordType.DeveloperName, 
																Please_select_if_applicable__c, 
																Type_of_Ownership_or_Control__c, 
																Title__c, Passport_Issue_Date__c, Date_Of_Becoming_a_UBO__c, 
																Are_you_a_resident_in_the_U_A_E__c, Gender__c, 
																Former_Name__c, Place_of_Birth__c, 
																Passport_Expiry_Date__c, Place_of_Issue__c, 
																Email__c, Mobile__c, PO_Box__c, 
																Post_Code__c, Emirate_State_Province__c, 
																Apartment_or_Villa_Number_c__c, Building_Name__c, 
																Permanent_Native_City__c, Permanent_Native_Country__c, 
																Is_this_individual_a_PEP__c, U_I_D_No__c, 
																Relationship_Type__c, Role__c, 
																Company_Name__c, Registration_No__c, Registration_Date__c, 
																Telephone_No__c, 
																DI_Permanent_Native_Country__c, DI_Email__c, DI_Emirate_State_Province__c, 
																DI_Address__c, DI_Apartment_or_Villa_Number_c__c, 
																DI_Building_Name__c, DI_Permanent_Native_City__c, DI_First_Name__c, DI_Last_Name__c, 
																DI_PO_Box__c, DI_Post_Code__c, DI_Mobile__c, Certified_passport_copy__c, 
																Is_this_Entity_registered_with_DIFC__c, Entity_Name__c, Entity_Name__r.Name, 
																Country_of_Registration__c, Place_of_Registration__c, Date_of_Registration__c, 
																Individual_Corporate_Name__c, I_Agree_Guardian__c
													FROM Amendments__r
													WHERE  (RecordType.DeveloperName = :OB_ConstantUtility.AMENDMENT_CORPORATE_RT.developerName
													OR RecordType.DeveloperName = :OB_ConstantUtility.AMENDMENT_INDIVIDUAL_RT.developerName)
													AND Role__c != null)
											      FROM HexaBPM__Service_Request__c
											      WHERE id = :srId
												  Limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())
												]
		) 
		*/

		List<HexaBPM__Service_Request__c> lstSerReq =   OB_libraryHelper.getSR(srId,mapRelatedAccForAmed);
        for(HexaBPM__Service_Request__c serObj :  lstSerReq) 
		{
				if( serObj.Id == srId )
				{
						system.debug('===inside service request===');
						srWrap.srObj = serObj;
						srWrap.amedWrapLst = new List<OB_ManageGuardiansController.AmendmentWrapper>();
						srWrap.shareholderAmedWrapLst = new List<OB_ManageGuardiansController.AmendmentWrapper>();
						srWrap.declarationIndividualText = declarationIndividualText;
						srWrap.declarationCorporateText = declarationCorporateText;
						for(HexaBPM_Amendment__c amed :serObj.Amendments__r) 
						{
							
							
								system.debug('===inside amendment===');
								OB_ManageGuardiansController.AmendmentWrapper amedWrap = new OB_ManageGuardiansController.AmendmentWrapper();
								amedWrap.amedObj = amed;
								amedWrap.lookupLabel = (amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name :'');


								if(  amed.RecordType.DeveloperName == 'Individual'  )
								{
									
										String passport = ( String.isNotBlank( amed.Passport_No__c )  ? amed.Passport_No__c : '' );
										String nationality = ( String.isNotBlank( amed.Nationality_list__c )  ? amed.Nationality_list__c : '' ); 
										String uniqKey = passport + nationality;
										// srWrap.amedShrHldrWrapLst.add(amedWrap);
										amedWrap.displayName = (String.isNotBlank( amedWrap.amedObj.First_Name__c) ? amedWrap.amedObj.First_Name__c : '') +' '+ (String.isNotBlank( amedWrap.amedObj.Last_Name__c) ? amedWrap.amedObj.Last_Name__c : '');
                                                
										if( String.isNotBlank( uniqKey ) )
										{
											mapExistingAmed.put(uniqKey.toLowercase(),amedWrap);
										}
								}
								else if( amed.RecordType.DeveloperName == 'Body_Corporate'  )
								{
											amedWrap.displayName = amedWrap.amedObj.Company_Name__c;
											if(amedWrap.displayName == null){
												amedWrap.displayName = amedWrap.amedObj.Individual_Corporate_Name__c;  
										}
										if( String.isNotBlank(  amed.Registration_No__c ) )
										{
											mapExistingAmed.put( amed.Registration_No__c.toLowercase() ,amedWrap);
										}
								}


								if(String.IsNotBlank(amed.Role__c) && amed.Role__c.containsIgnoreCase('Guardian')) 
								{
									srWrap.amedWrapLst.add(amedWrap);
								} 
								else 
								{
									srWrap.shareholderAmedWrapLst.add(amedWrap);
								}
								
							
						}
				}

		}

		
		 // for amedment libary from all other SR.
		 for(HexaBPM__Service_Request__c serObj :  lstSerReq) 
		 {  
			 
			 for(HexaBPM_Amendment__c amed : serObj.Amendments__r) 
			 {
				 
				 
					 OB_ManageGuardiansController.AmendmentWrapper amedWrap = new OB_ManageGuardiansController.AmendmentWrapper();
					 amedWrap.amedObj = amed;
					 amedWrap.relatedAmendID = amed.Id;
                    if(serObj.Id != srId){
                        amedWrap.amedObj.Id = null;
                    }
					 amedWrap.isIndividual = (amed.RecordType.DeveloperName == 'Individual' ? true :false);
					 amedWrap.isBodyCorporate = (amed.RecordType.DeveloperName == 'Body_Corporate' ? true :false);
					 amedWrap.lookupLabel = (amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name :'');
					 
				   
					 if( String.isNotBlank(amed.Role__c) )
					 {
						 
							 if(amed.RecordType.DeveloperName == 'Body_Corporate')
							 {
								 System.debug('@@@@@@@@@@@@@@@@@  amed.Registration_No__c '+  amed.Registration_No__c);
								 if( String.isNotBlank(  amed.Registration_No__c ) )
								 {
									 amedWrap.displayName = amedWrap.amedObj.Company_Name__c;
									 if(amedWrap.displayName == null){
                                                    amedWrap.displayName = amedWrap.amedObj.Individual_Corporate_Name__c;  
                                                }
									 mapShareholder.put( amed.Registration_No__c.toLowercase() ,amedWrap);
								 }
							 }
							 
							 if(amed.RecordType.DeveloperName == 'Individual')
							 {
								 String passport = ( String.isNotBlank( amed.Passport_No__c )  ? amed.Passport_No__c : '' );
								 String nationality = ( String.isNotBlank( amed.Nationality_list__c )  ? amed.Nationality_list__c : '' ); 
								 String uniqKey = passport + nationality;
								 System.debug('@@@@@@@@@@@@@@@@@ uniqKey '+ uniqKey);
								 // srWrap.amedShrHldrWrapLst.add(amedWrap);
								 //if( String.isNotBlank( uniqKey ) )
								 if( String.isNotBlank( uniqKey  ) )
								 {
									 amedWrap.displayName = (String.isNotBlank( amedWrap.amedObj.First_Name__c) ? amedWrap.amedObj.First_Name__c : '') +' '+ (String.isNotBlank( amedWrap.amedObj.Last_Name__c) ? amedWrap.amedObj.Last_Name__c : '');
									 mapShareholder.put(uniqKey.toLowercase(),amedWrap);
								 }
							 }

					 }
			 }
		 }
		
		 System.debug('@@@@@@@@@@@@@@@@@ mapShareholder '+ mapShareholder.keySet());
                    

		 system.debug('@@@@@@@@@@ mapExistingAmed '+ mapShareholder);
		 system.debug('@@@@@@@@@@ mapExistingAmed.size() '+ mapShareholder.size());
		 system.debug('@@@@@@@@@@ mapRelShpToAmedWrap '+ mapRelShpToAmedWrap);
		 system.debug('@@@@@@@@@@ mapRelShpToAmedWrap.size() '+ mapRelShpToAmedWrap.size());

		 //mapShareholderCombine
		 for(String key : mapShareholder.keySet() )
		 {
			 if(  !mapExistingAmed.containsKey( key.toLowercase()) )
			 {
				 mapShareholderCombine.put( key , mapShareholder.get(key) );
			 }
				 
		 }

		 for(String key : mapRelShpToAmedWrap.keySet() )
		 {
			 if(  !mapExistingAmed.containsKey( key.toLowercase())  )
			 {
				 mapShareholderCombine.put( key , mapRelShpToAmedWrap.get(key) );
			 }
			
		 }
		 system.debug('@@@@@@@@@@ mapShareholderCombine '+ mapShareholderCombine);
		 system.debug('@@@@@@@@@@ mapShareholderCombine.size() '+ mapShareholderCombine.size());

		 
		 if( mapShareholderCombine.size() > 0 )
		 {
			 srWrap.shareholderAmedWrapLst.addAll( mapShareholderCombine.values() );
			 srWrap.shareholderAmedWrapLst.sort();
		 }
		 System.debug('@@@@@@@@@@@@@@@@@ srWrap.amedShrHldrWrapLst  '+ srWrap.shareholderAmedWrapLst );
		 
		
		respWrap.srWrap = srWrap;
		system.debug('=====amend====' + respWrap);

		return respWrap;
	}

	/*
	 * Lightning component: OB_ManageGuradinasDetail
	 * on click of "Confirm" button will call upsert the amendment record in database
	 */
	@AuraEnabled
	public static ResponseWrapper onAmedSaveDB(String reqWrapPram) {
		system.debug('Response---->' + reqWrapPram);
		//reqest wrpper
		reqWrap = (OB_ManageGuardiansController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_ManageGuardiansController.RequestWrapper.class);

		//response wrpper
		respWrap = new OB_ManageGuardiansController.ResponseWrapper();
		String RecordTypename = reqWrap.recordtypename;
		// service request ID
		String srId = reqWrap.srId;
		String amendID = '';
		Boolean isDuplicate = false;

		Boolean enforceGuardianRole =  false;
		
		Map<String,String> docMasterContentDocMap = reqWrap.docMasterContentDocMap;
		system.debug('@@@@@  docMasterContentDocMap '+docMasterContentDocMap);
		
		system.debug('@@@@@@@ reqWrap.amedWrap ' + reqWrap.amedWrap);


		try{
			if(String.IsnotBlank(srId)) 
			{
				if(reqWrap.amedWrap.amedObj != null) 
				{
					/*
						if(reqWrap.amedWrap.amedObj.Id != null) 
						{
							amendID = reqWrap.amedWrap.amedObj.Id;
							RecordTypename = reqWrap.amedWrap.amedObj.recordtype.developername;
						}
					*/

					amendID = ( reqWrap.amedWrap.amedObj.ServiceRequest__c != srId ? NULL : reqWrap.amedWrap.amedObj.Id);
					enforceGuardianRole = reqWrap.amedWrap.amedObj.ServiceRequest__c != srId;
					reqWrap.amedWrap.amedObj.ServiceRequest__c = srId;
					
					System.debug('----------RecordTypename----' + RecordTypename);
					// check duplicate individual amendment based on nationality and passport
					// query existing amendment record of that service request.
					if(String.IsNotBlank(RecordTypename) && RecordTypename.equalsIgnoreCase(OB_ConstantUtility.AMENDMENT_INDIVIDUAL_RT.developerName)) 
					{
						String passport = String.isNotBlank(reqWrap.amedWrap.amedObj.Passport_No__c) ? reqWrap.amedWrap.amedObj.Passport_No__c :'';
						String nationality = String.IsNotBlank(reqWrap.amedWrap.amedObj.Nationality_list__c) ? reqWrap.amedWrap.amedObj.Nationality_list__c.toLowerCase() :'';
						String passportNationlity = (String.isNotBlank(passport) ? passport + '_' :'') + (String.IsNotBlank(nationality) ? nationality :'');
						system.debug('===passportNationlity==' + passportNationlity);

						for(HexaBPM_Amendment__c amd :[Select ID, First_Name__c, Passport_No__c, Nationality_list__c, 
													    recordtype.developername, Role__c
													    From HexaBPM_Amendment__c
													    Where ServiceRequest__c = :srId
													    AND Id != :amendID
													    AND recordtype.developername = :OB_ConstantUtility.AMENDMENT_INDIVIDUAL_RT.developerName
													    AND Passport_No__c = :passport AND Nationality_list__c = :nationality
													    Limit 1]) {
							system.debug('===duplicate==');
							//duplicateAmend = amd;
							reqWrap.amedWrap.amedObj.Id = amd.Id;
							reqWrap.amedWrap.amedObj.Role__c = amd.Role__c;
							amendID = amd.Id;
							Boolean isDirector = String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Director')?true:false;
							
							if((String.IsNotBlank(amd.Role__c) 
								&& amd.Role__c.containsIgnorecase('Guardian')) || isDirector) {
								isDuplicate = true;
								break;
							}
							
						}
					} else if(String.IsNotBlank(RecordTypename) && RecordTypename.equalsIgnoreCase('Body_Corporate')) 
					{
						String registrationNo = String.isNotBlank(reqWrap.amedWrap.amedObj.Registration_No__c) ? reqWrap.amedWrap.amedObj.Registration_No__c :'';
						system.debug(amendID + '====registrationNo=====' + registrationNo);
						for(HexaBPM_Amendment__c amd :[Select ID, Registration_No__c, 
													    recordtype.developername, Role__c
													    From HexaBPM_Amendment__c
													    Where ServiceRequest__c = :srId
													    AND (recordtype.developername = 'Body_Corporate' AND Id != :amendID)
													    AND Registration_No__c = :registrationNo
													    Limit 1]) {
							system.debug('==registrationNo=duplicate==');
							reqWrap.amedWrap.amedObj.Id = amd.Id;
							reqWrap.amedWrap.amedObj.Role__c = amd.Role__c;
							amendID = amd.Id;
							if(String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Guardian')) {
								isDuplicate = true;
							}
							break;
						}
					}
					system.debug(isDuplicate + '====isDuplicate=amendID====' + amendID);
					system.debug(reqWrap.amedWrap.amedObj.Role__c + '==registrationNo=duplicate==' + RecordTypename);
					// duplicate record throw an error
					if(isDuplicate) 
					{
						respWrap.errorMessage = 'This person/corporate already exist';
					}
					else 
					{

						// add guardian role

						if( enforceGuardianRole )
						{
							reqWrap.amedWrap.amedObj.Role__c = 'Guardian';
						}
						else 
						{
							if(String.IsNotBlank(reqWrap.amedWrap.amedObj.Role__c))
								reqWrap.amedWrap.amedObj.Role__c = !reqWrap.amedWrap.amedObj.Role__c.containsIgnoreCase('Guardian') ? reqWrap.amedWrap.amedObj.Role__c + ';Guardian' :reqWrap.amedWrap.amedObj.Role__c;
							else 
								reqWrap.amedWrap.amedObj.Role__c = 'Guardian';	
						}
						

						// for new Shareholder assign Shareholder record type
						if(reqWrap.amedWrap.amedObj.Id == null) {
							// query Shareholder record type
							Id recordTypeID = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', RecordTypename);
							if(String.IsNotBlank(recordTypeID)) {
								reqWrap.amedWrap.amedObj.recordtypeId = recordTypeID;
							}
						}
						reqWrap.amedWrap.amedObj.I_Agree_Guardian__c = true;
						upsert reqWrap.amedWrap.amedObj;


						system.debug('=======reqWrap.amedWrap.amedObj====' + reqWrap.amedWrap.amedObj);
						//reparenting logic comes here....
                        OB_AmendmentSRDocHelper.RequestWrapper srDocReq = new OB_AmendmentSRDocHelper.RequestWrapper(); 
                        OB_AmendmentSRDocHelper.ResponseWrapper srDocResp = new OB_AmendmentSRDocHelper.ResponseWrapper(); 
                        srDocReq.amedId = reqWrap.amedWrap.amedObj.Id;
                        srDocReq.srId = srId;
                        srDocReq.docMasterContentDocMap = docMasterContentDocMap;
                        
                        srDocResp = OB_AmendmentSRDocHelper.reparentSRDocsToAmendment(srDocReq);
                        
						if(String.IsNotBlank(srId)) { 
							// initialize the list again
							//calculate risk
							OB_RiskMatrixHelper.CalculateRisk(srId);
							respWrap = getAmendmentList(srId, respWrap,reqWrap);
						}
					}
				}

				respWrap.amedWrap = reqWrap.amedWrap;
			} else {
				respWrap.errorMessage = 'Service Request is null';
			}

		} catch(Exception e) {
			string DMLError = e.getdmlMessage(0) + '';
			if(DMLError == null) {
				DMLError = e.getMessage() + '';
			}
			respWrap.errorMessage = DMLError;
			system.debug('##########DMLError ' + DMLError);
			//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
			return respWrap;
		}
		return respWrap;
	}
	
	/*
	* Process OCR logic 
	*/
	//revised
    @AuraEnabled
    public static ResponseWrapper processOCR(String reqWrapPram) 
    {
    	
        
        //reqest wrpper
        RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
        /*String fileId = reqWrap.fileId;
        
        //OCR Wrapper
        OB_OCRHelper.RequestWrapper ocrReqWrap = new OB_OCRHelper.RequestWrapper();
        OB_OCRHelper.ResponseWrapper ocrRespWrap  = new OB_OCRHelper.ResponseWrapper();
        
        
        //Current class wrapper.
        //RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
        ResponseWrapper respWrap = new ResponseWrapper();
        
        // preparing request.
        ocrReqWrap.fileId = fileId; 
        ocrRespWrap = OB_OCRHelper.processOCR(ocrReqWrap);
        
        if( ocrRespWrap.errorMessage == NULL)
        {
            respWrap.mapFieldApiObject  = ocrRespWrap.mapFieldApiObject;
        }
        else if( ocrRespWrap.errorMessage != NULL )
        {
            respWrap.errorMessage = ocrRespWrap.errorMessage;
        }*/
        
        
        return respWrap;
        
    
    }

	/*
	 * Lightning component: OB_manageGuardiansNew
	 */
	@AuraEnabled
	public static ResponseWrapper initAmendmentDB(String reqWrapPram) {
		//reqest wrpper
		reqWrap = (OB_ManageGuardiansController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_ManageGuardiansController.RequestWrapper.class);

		//response wrpper
		respWrap = new OB_ManageGuardiansController.ResponseWrapper();
		String srId = reqWrap.srId;
		String recordtypename = reqWrap.recordtypename;
		OB_ManageGuardiansController.AmendmentWrapper amedWrap = new OB_ManageGuardiansController.AmendmentWrapper();
		HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c();
		amedObj.ServiceRequest__c = srId;
		amedObj.Relationship_Type__c = 'Guardian';
		// query Shareholder record type
		Id recordTypeID = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', recordtypename);
		if(String.IsNotBlank(recordTypeID)) {
			amedObj.recordtypeId = recordTypeID;
		}
		//amedObj.recordtype.developername=recordtypename;
		amedWrap.amedObj = amedObj;

		respWrap.amedWrap = amedWrap;

		return respWrap;
	}


	/*
	 * Lightning component: OB_ManageGuardiansExist
	 * remove existing Guardian
	 */
	@AuraEnabled
	public static ResponseWrapper removeAmendment(String reqWrapPram) {
		//reqest wrpper
		reqWrap = (OB_ManageGuardiansController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_ManageGuardiansController.RequestWrapper.class);
		//response wrpper
		respWrap = new OB_ManageGuardiansController.ResponseWrapper();
		try{
			String amendmentID = reqWrap.amendmentID;
			String srId = reqWrap.srId;
			AmendmentWrapper amedWrap = reqWrap.amedWrap;

			if(String.IsNotBlank(amendmentID) && amedWrap != null && amedWrap.amedObj != null && amedWrap.amedObj.Id != null) {
				String role = amedWrap.amedObj.Role__c;
				if(String.IsNotBlank(role) && role.equalsIgnoreCase('Guardian')) {
					delete amedWrap.amedObj;
				} else {
					amedWrap.amedObj.Role__c = role.replace('Guardian', '');
					amedWrap.amedObj.I_Agree_Guardian__c = false;
					update amedWrap.amedObj;
				}
				if(String.IsNotBlank(srId)) { 
					// initialize the list again
					//calculate risk
					OB_RiskMatrixHelper.CalculateRisk(srId);
					respWrap = getAmendmentList(srId, respWrap,reqWrap);
				}
			} else {
				respWrap.errorMessage = 'No record found';
			}
		} catch(Exception e) {
			string DMLError = e.getdmlMessage(0) + '';
			if(DMLError == null) {
				DMLError = e.getMessage() + '';
			}
			respWrap.errorMessage = DMLError;
			system.debug('##########DMLError ' + DMLError);
			//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
			return respWrap;
		}
		return respWrap;
	}



	/////////////////////////////////Wrapper  classes//////////////////////////////////////
	/*
	 * data will come from client side
	 */
	public class RequestWrapper{
		@AuraEnabled public Id amendmentID { get; set; }
		@AuraEnabled public Id srId { get; set; }
		@AuraEnabled public AmendmentWrapper amedWrap { get; set; }
		@AuraEnabled public String recordtypename { get; set; }
		@AuraEnabled public string pageID { get; set; }
		@AuraEnabled public string flowId { get; set; }
		@AuraEnabled public Map<String,String> docMasterContentDocMap { get; set; }
		@AuraEnabled public String fileId { get; set; }

		
		@AuraEnabled public Map<Id,AccountContactRelation> mapRelatedAccForRelshp { get; set; }
        @AuraEnabled public Map<Id,AccountContactRelation> mapRelatedAccForAmed { get; set; }
		
		public RequestWrapper() {
		}
	}

	/*
	 * store detail of service request with list of amendment
	 */
	public class SRWrapper{
		@AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
		@AuraEnabled public List<OB_ManageGuardiansController.AmendmentWrapper> amedWrapLst { get; set; }
		@AuraEnabled public List<OB_ManageGuardiansController.AmendmentWrapper> shareholderAmedWrapLst { get; set; }
		@AuraEnabled public String declarationIndividualText { get; set; }
		@AuraEnabled public String declarationCorporateText { get; set; }
		@AuraEnabled public Boolean isDraft { get; set; }
		@AuraEnabled public string viewSRURL { get; set; }
		public SRWrapper() {

		}
	}


	/*
	 * store detail of amendment object
	 */
	public class AmendmentWrapper implements Comparable
	{
		@AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
		@AuraEnabled public Boolean isIndividual { get; set; }
        @AuraEnabled public Boolean isBodyCorporate { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
		@AuraEnabled public String displayName { get; set; }
		@AuraEnabled public String relatedAmendID { get; set; }
		@AuraEnabled public String relatedRelId { get; set; }

		 // Implement the compareTo() method
		 public Integer compareTo(Object compareTo) 
		 {
			 AmendmentWrapper compareToEmp = (AmendmentWrapper)compareTo;
			 if ( displayName == compareToEmp.displayName ) return 0;
			 if ( displayName > compareToEmp.displayName) return 1;
			 return -1;        
		 }
		
		public AmendmentWrapper() { 
		}
	}

	/*
	 * Result will send to client side
	 */
	public class ResponseWrapper
	{
		@AuraEnabled public SRWrapper srWrap { get; set; }
		@AuraEnabled public AmendmentWrapper amedWrap { get; set; }
		@AuraEnabled public String errorMessage { get; set; }
		@AuraEnabled public HexaBPM__Section_Detail__c ButtonSection { get; set; }
		// For OCR
        @AuraEnabled public  Map<String,Object> mapFieldApiObject {get;set;}
		public ResponseWrapper() { 
		}
	}

	@AuraEnabled
	public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId) {


		ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
		HexaBPM__Service_Request__c objRequest = new HexaBPM__Service_Request__c(Id = SRID);


		//if(OB_QueryUtilityClass.minRecValidationCheck(SRID, 'Guardian')) {
			objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
			upsert objRequest;
		//}



		//calculate risk
		OB_RiskMatrixHelper.CalculateRisk(objRequest.Id);
		 objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
		PageFlowControllerHelper.objSR = objRequest;
		PageFlowControllerHelper objPB = new PageFlowControllerHelper();

		PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
		system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
		respWrap.pageActionName = responseNextPage.pg;
		respWrap.communityName = responseNextPage.communityName;
		respWrap.CommunityPageName = responseNextPage.CommunityPageName;
		respWrap.sitePageName = responseNextPage.sitePageName;
		respWrap.strBaseUrl = responseNextPage.strBaseUrl;
		respWrap.srId = objRequest.Id;
		respWrap.flowId = responseNextPage.flowId;
		respWrap.pageId = responseNextPage.pageId;
		respWrap.isPublicSite = responseNextPage.isPublicSite;

		system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
		return respWrap;
	}
	public class ButtonResponseWrapper {
		@AuraEnabled public String pageActionName { get; set; }
		@AuraEnabled public string communityName { get; set; }
		@AuraEnabled public String errorMessage { get; set; }
		@AuraEnabled public string CommunityPageName { get; set; }
		@AuraEnabled public string sitePageName { get; set; }
		@AuraEnabled public string strBaseUrl { get; set; }
		@AuraEnabled public string srId { get; set; }
		@AuraEnabled public string flowId { get; set; }
		@AuraEnabled public string pageId { get; set; }
		@AuraEnabled public boolean isPublicSite { get; set; }

	}
}