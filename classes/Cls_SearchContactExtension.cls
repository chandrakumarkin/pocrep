/******************************************************************************************
 *  Name        : Cls_SearchContactExtension 
 *  Author      : Danish Farooq
 *  Date        : 2018-06-04
 *  Description : Extension Class for searching Active Clients
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        	Updated By    	Description
 v1.1	 23 Dec 2020	Shikha Khanuja	Passed requestType,serviceType parameters and modified logic to display clients as per parameters passed
*******************************************************************************************/
public virtual class Cls_SearchContactExtension {
  
  public String currentAccountId            {get;set;}
  public String ClientName {get;set;}
  
  public String accountStatus              {get;set;}
  
  public Boolean searchWithLicenseNo          {get;set;}
  public Boolean isSameClient              {get;set;}
  
  public list<HostingWrapper> HostingCompanies     {get;set;}
  
  //Shikha - added parameters to control display of members
  public String serviceTypeValue {get;set;}
  public String requestTypeValue {get;set;}
    public String OutputText{get;set;}
  
  public Cls_SearchContactExtension (){
    
    searchWithLicenseNo = false;
    isSameClient = false;
    //Shikha - added to clear list on invoke of component
     HostingCompanies = new List<HostingWrapper>();
      clearList();
      system.debug('### constructor invoked');
      system.debug('### Hosting Companies '+HostingCompanies);
  }
  
  public void clearList(){
    
    HostingCompanies.clear();
    OutputText = '';
  }
  
  public virtual void SearchContact(){
    	
       // HostingCompanies = new list<HostingWrapper>();
        clearList();
        ClientName  = String.isNotBlank(ClientName) ? ClientName.trim() : '';
        system.debug('%%%%%%%' +ClientName  );
          system.debug('%%%%%%%' +currentAccountId );
        HostingWrapper objHostingWrapper;
      	system.debug('### type '+serviceTypeValue);
      system.debug('### request Type '+requestTypeValue);
      List<Relationship__c> lstRel = new List<Relationship__c>();
      Map<String,Set<Id>> membershipContactMap = new Map<String,Set<Id>>();
      Map<String,String> contactStatusMap = new Map<String,String>();
      List<Membership__c> accountMembershipList = new List<Membership__c>();
      /*if(requestTypeValue == 'Tenants'){
          accountMembershipList = [SELECT Id, RecordType.Name, MembershipStatus__c, Contact__c, Contact__r.Account.Name 
                                                   FROM Membership__c 
                                                   WHERE Contact__r.AccountId =:currentAccountId AND MembershipStatus__c = 'Active'];  
      }
      else{*/
          /*accountMembershipList = [SELECT Id, RecordType.Name, MembershipStatus__c, Contact__c, Contact__r.Account.Name 
                                                   FROM Membership__c 
                                                   WHERE Contact__r.AccountId =:currentAccountId AND MembershipStatus__c NOT IN ('Expired','Cancelled')];  */
      //} 
      
      
      if(String.isNotBlank(serviceTypeValue) && (serviceTypeValue.contains(GlobalConstants.REMOVE_EMP_ACCESS) || serviceTypeValue.contains(GlobalConstants.CANCELLATION))){
          lstRel = [Select Id,Object_Contact__c,Object_Contact__r.FirstName,
                                         Object_Contact__r.LastName from Relationship__c where
                                         Subject_Account__c =: currentAccountId AND
                                         Object_Contact__r.RecordType.DeveloperName = 'GS_Contact' AND (
                                         Object_Contact__r.LastName like :'%'+ClientName  +'%' OR
                                         Object_Contact__r.FirstName like :'%'+ClientName  +'%' )];
      }
      else{
         lstRel = [Select Id,Object_Contact__c,Object_Contact__r.FirstName,
                                         Object_Contact__r.LastName from Relationship__c where
                                         Subject_Account__c =: currentAccountId AND
                                         Object_Contact__r.RecordType.DeveloperName = 'GS_Contact' AND
                                         Active__c = true AND (
                                         Object_Contact__r.LastName like :'%'+ClientName  +'%' OR
                                         Object_Contact__r.FirstName like :'%'+ClientName  +'%' )];
                                         
      }
      //Shikha - adding membership logic
      Set<Id> contactIds = new Set<Id>();
      for(Relationship__c objRelation : lstRel){
          contactIds.add(objRelation.Object_Contact__c);
      }
      system.debug('### contactIds '+contactIds);
      if(serviceTypeValue == GlobalConstants.MEMBERSHIP_RENEWAL){	
          accountMembershipList = [SELECT Id, RecordType.Name, MembershipStatus__c, Contact__c, Contact__r.Account.Name 	
                               FROM Membership__c 	
                               WHERE Contact__c =:contactIds AND MembershipStatus__c NOT IN ('Cancelled') ORDER BY LastModifiedDate ASC]; 	
      }	
      else{	
          accountMembershipList = [SELECT Id, RecordType.Name, MembershipStatus__c, Contact__c, Contact__r.Account.Name 	
                               FROM Membership__c 	
                               WHERE Contact__c =:contactIds AND MembershipStatus__c NOT IN ('Expired','Cancelled','Pending Renewal')]; 	
      }	
      if(serviceTypeValue == GlobalConstants.MEMBERSHIP_RENEWAL){	
      	//List<Membership__c> renewalList = new List<Membership__c>();  	
        Map<Id,Membership__c> renewalMembershipMap = new Map<Id,Membership__c>();	
          for(Membership__c objMember : accountMembershipList){	
              if(objMember.MembershipStatus__c == GlobalConstants.STAT_ACTIVE){	
                  renewalMembershipMap.put(objMember.Contact__c,objMember);	
              }	
              else if(objMember.MembershipStatus__c != GlobalConstants.STAT_ACTIVE && !renewalMembershipMap.containsKey(objMember.Contact__c)){	
                  renewalMembershipMap.put(objMember.Contact__c,objMember);	
              }	
          }	
          accountMembershipList = renewalMembershipMap.values();	
      }      	
      for(Membership__c objMem : accountMembershipList){
          if(membershipContactMap.containsKey(objMem.RecordType.Name)){
              membershipContactMap.get(objMem.RecordType.Name).add(objMem.Contact__c);
          }
          else{
              membershipContactMap.put(objMem.RecordType.Name,new Set<Id> {objMem.Contact__c});
          }
          contactStatusMap.put(objMem.RecordType.Name+','+objMem.Contact__c,objMem.MembershipStatus__c);
      }
      system.debug('### membershipContactMap '+membershipContactMap);
      system.debug('### contactStatusMap '+contactStatusMap);
      system.debug('### lstRel '+lstRel);
      List<Relationship__c> filteredRelationshipList = new List<Relationship__c>();
      if(String.isNotBlank(requestTypeValue) && String.isNotBlank(serviceTypeValue)){
          for(Relationship__c objRelation : lstRel){
              if(!membershipContactMap.isEmpty() ){
                  if(requestTypeValue == GlobalConstants.TENANTS && serviceTypeValue == GlobalConstants.VEHICLE_REG && membershipContactMap.containsKey(GlobalConstants.MEMBERSHIP) 
                     && !membershipContactMap.get(GlobalConstants.MEMBERSHIP).contains(objRelation.Object_Contact__c)){
                         system.debug('### vehicle registration satisfied');
                         filteredRelationshipList.add(objRelation);                         
                     }
                  else if(requestTypeValue == GlobalConstants.MEMBERSHIP && serviceTypeValue == GlobalConstants.MEMBERSHIP_NEW && membershipContactMap.containsKey(GlobalConstants.TENANTS) 
                     && !membershipContactMap.get(GlobalConstants.TENANTS).contains(objRelation.Object_Contact__c)){
                         system.debug('### monthly membership satisfied');
                         filteredRelationshipList.add(objRelation);
                     }
                  else if(serviceTypeValue == GlobalConstants.MEMBERSHIP_NEW  && !membershipContactMap.containsKey(GlobalConstants.TENANTS)){
                      system.debug('### membership without tenants');
                      filteredRelationshipList.add(objRelation);
                  }
                  else if(serviceTypeValue == GlobalConstants.VEHICLE_REG && !membershipContactMap.containsKey(GlobalConstants.MEMBERSHIP)){
                      system.debug('### tenants without membership');
                      filteredRelationshipList.add(objRelation);
                  }
                  else if(serviceTypeValue == GlobalConstants.MEMBERSHIP_RENEWAL && membershipContactMap.containsKey(requestTypeValue) && membershipContactMap.get(requestTypeValue).contains(objRelation.Object_Contact__c)
                        && contactStatusMap.containsKey(requestTypeValue+','+objRelation.Object_Contact__c) && (contactStatusMap.get(requestTypeValue+','+objRelation.Object_Contact__c) == GlobalConstants.STAT_ACTIVE || contactStatusMap.get(requestTypeValue+','+objRelation.Object_Contact__c) == GlobalConstants.STAT_EXP)){	
                              filteredRelationshipList.add(objRelation);	
                  }
                  else if(serviceTypeValue != GlobalConstants.MEMBERSHIP_NEW && serviceTypeValue != GlobalConstants.VEHICLE_REG && membershipContactMap.containsKey(requestTypeValue) 
                          && membershipContactMap.get(requestTypeValue).contains(objRelation.Object_Contact__c) && contactStatusMap.containsKey(requestTypeValue+','+objRelation.Object_Contact__c) 
                          && contactStatusMap.get(requestTypeValue+','+objRelation.Object_Contact__c) == GlobalConstants.STAT_ACTIVE){
                      system.debug('### rest of the values with membership');
                      filteredRelationshipList.add(objRelation);
                  }
              }
              /*else{
                  system.debug('### if no membership satisfied');
                  filteredRelationshipList.add(objRelation);
              } */            
          }
      }
      else{
          system.debug('### in else no membership contact map');
          filteredRelationshipList = lstRel;
      }
      system.debug('### accountMembershipList '+accountMembershipList);
      if(accountMembershipList.isEmpty() && (serviceTypeValue == GlobalConstants.MEMBERSHIP_NEW || serviceTypeValue == GlobalConstants.VEHICLE_REG)){
          filteredRelationshipList = lstRel;
      }
      if(filteredRelationshipList.isEmpty() && serviceTypeValue != GlobalConstants.MEMBERSHIP_NEW && serviceTypeValue != GlobalConstants.VEHICLE_REG){
          OutputText = 'No active registration for any employees. Please raise request either for New Vehicle Registration or New Monthly Membership';
      }
      else{
          OutputText = '';
      }
      //Filtering duplicate contact Ids if present in relationship      
      Map<Id,Relationship__c> contactVsRelationship = new Map<Id,Relationship__c>();
      for(Relationship__c objRelation : filteredRelationshipList){
          contactVsRelationship.put(objRelation.Object_Contact__c,objRelation);
      }
      filteredRelationshipList = contactVsRelationship.values();
      system.debug('### filteredRelationshipList '+filteredRelationshipList);
      
        for(Relationship__c objCon : filteredRelationshipList ){ // V1.9 - Claude - new query        	
            objHostingWrapper = new HostingWrapper();
            objHostingWrapper.ClientName = (objCon.Object_Contact__r.FirstName != NULL) ?  objCon.Object_Contact__r.FirstName+' '+objCon.Object_Contact__r.LastName : objCon.Object_Contact__r.LastName;
            objHostingWrapper.ClientID = objCon.Object_Contact__C;
            HostingCompanies.add(objHostingWrapper);
        }
    }
    
    public class HostingWrapper{
        
        public string ClientID {get;set;}
        public string ClientName {get;set;}
        public String RegistrationNumber   {get;set;}
    }
    
}