/**
 * @description       : 
 * @author            : shoaib tariq
 * @group             : 
 * @last modified on  : 02-22-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   02-22-2021   shoaib tariq   Initial Version
**/
@isTest(seeAllData = false)
public with sharing class UpdateContactInformationTest {
    static testMethod void myUnitTest() {
        list<Account> insertNewAccounts = new list<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
         //create contact
        list<Contact> insertNewContacts = new list<Contact>();
        insertNewContacts = OB_TestDataFactory.createContacts(1, insertNewAccounts);
        insertNewContacts[0].important__c  = true;
        for(RecordType RC:[Select Id from RecordType where DeveloperName='Business_Development' and sobjectType='Contact' and IsActive=true]){
        	insertNewContacts[0].RecordTypeId = RC.Id;
        }
        insert insertNewContacts;

        test.startTest();
        UpdateContactInformation.getContactInfo(insertNewContacts[0].Id);
        UpdateContactInformation.updateContact(insertNewContacts[0].Id,true,true,'shoibjooo@gmail.com',true,true,'+9871523830186');
        Test.stopTest();
        
    }
}