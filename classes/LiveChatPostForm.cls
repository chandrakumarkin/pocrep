public class LiveChatPostForm 
{
    public string CaseID{get;set;}
    public string Reating{get;set;}
    public string Comments{get;set;}
    public boolean submitted{get;set;}
    public Live_Chat_Feedback__c ObjFeed{get;set;}
    
 
    public LiveChatPostForm(ApexPages.StandardController controller) 
    {
        ObjFeed=new Live_Chat_Feedback__c();
        submitted=false;
    
        if(ApexPages.currentPage().getParameters().get('attachedRecords')!=null)
      {
          CaseID= ApexPages.currentPage().getParameters().get('attachedRecords');
          if(CaseID!=null)
          {
               string[] TempRecord=CaseID.split(':');
                System.debug(TempRecord.size());
                if(TempRecord.size()>1)
                {
                    CaseID=TempRecord[1].ReplaceAll('}','').ReplaceAll('"','');
                    if(!Test.isrunningtest())
                     ObjFeed.Case__c=CaseID;
                }
            }
      }
      
      
    }
    public void  SubmitRequest()
    {

        ObjFeed.Rating__c=Reating;
        insert ObjFeed;
        submitted =true;
    
    }
    
   




}