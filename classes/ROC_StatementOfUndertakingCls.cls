/***********************************************************************************
 *  Author   : Claude Manahan
 *  Company  : DIFC
 *  Date     : 08/05/2017
 *  Purpose  : Custom Controller for statement of undertaking page 
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date        Updated By  Description
 ----------------------------------------------------------------------------------------              
V1.0	08/05/2017	Claude		Created
**********************************************************************************************/
public class ROC_StatementOfUndertakingCls extends Cls_ProcessFlowComponents {
	
	public ROC_StatementOfUndertakingCls(){}
	
	public override PageReference commitRecord(){
		
		if(objSr.Statement_of_Undertaking__c && isNext()) saveRecord();
		
		if(objSr.Statement_of_Undertaking__c || !isNext()) return DynamicButtonAction();
		
		return setAgreementCheckboxMessage();
	}
	
	/**
	 * Redirects the user to the designated
	 * page
	 * @return 			pageRef			The reference to the selected page
	 */
    public override PageReference goTopage(){
    	
        if(objSr.Statement_of_Undertaking__c && isNext()) saveRecord();
           
        if(objSr.Statement_of_Undertaking__c || !isNext() ) return getNextPage();
        
        return setAgreementCheckboxMessage();
    }
    
    private PageReference setAgreementCheckboxMessage(){
    	
    	setMessage('Kindly agree to the Terms and Conditions by clicking on the "I agree" checkbox.');
    	
    	return null;
    }
}