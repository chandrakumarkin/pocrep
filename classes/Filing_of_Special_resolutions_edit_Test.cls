/*
    Author      :   Sai
    Description :   Test class of Filing_of_Special_resolutions_edit class
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    23-07-2015  Ravi            Sai Kalyan

*/

@isTest
private class Filing_of_Special_resolutions_edit_Test {
	
	public static List<Service_Request__c> lstSRData;
	
    public static void testData(){
    	
    	lstSRData = TestUtilityClass.createServiceRequestData(1,true);
    	
    }
    
    static testmethod void positiveTest(){ 
    	testData();
    	
    	TEST.STARTTEST();
    		System.currentPageReference().getParameters().put('RecordType', '0121j0000008eVUAAY'); 
    		ApexPages.StandardController srRecord = new ApexPages.StandardController(lstSRData[0]);
        	Filing_of_Special_resolutions_edit resolutions = new Filing_of_Special_resolutions_edit(srRecord);
        	system.assertEquals(lstSRData[0].Service_Category__c,'New');
        	resolutions.SaveConfirmation();
        TEST.STOPTEST();
    }
    
    static testmethod void positiveTest1(){ 
    	testData();
    	TEST.STARTTEST();
    		System.currentPageReference().getParameters().put('RecordType', '0121j0000008eVUAAY'); 
    		Service_Request__c request = new Service_Request__c();
    		ApexPages.StandardController srRecord = new ApexPages.StandardController(request);
        	Filing_of_Special_resolutions_edit resolutions = new Filing_of_Special_resolutions_edit(srRecord);
        	resolutions.SaveConfirmation();
        TEST.STOPTEST();
    }
    
    static testmethod void negativeTest(){ 
    	testData();
    	TEST.STARTTEST();
    		Service_Request__c request = new Service_Request__c();
    		ApexPages.StandardController srRecord = new ApexPages.StandardController(request);
        	Filing_of_Special_resolutions_edit resolutions = new Filing_of_Special_resolutions_edit(srRecord);
        	resolutions.SaveConfirmation();
        TEST.STOPTEST();
    }
}