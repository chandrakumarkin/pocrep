/******************************************************************************
* Class : OfferProcessUtilityTest
* Description :
* Utility Class : OfferProcessUtility and Apex class LeadOfferUnitPDFTemplateUtility
*******************************************************************************/
@isTest
private class OfferProcessUtilityTest{
    public static Offer_Process__c op;
     public static Offer_Process__c op2;
    public static List<Offer_Process__c> opList;
    public static Lead testCrmLead;
    public static List<Unit_Offer__c> uoList;
    
    public static void createData(){
        
        Building__c b = new Building__c();
        b.Company_Code__c = '5300';
        b.DIFC_FitOut__c = true;
        insert b;
        
        Unit__c unit = new Unit__c();
        unit.Building__c = b.ID;
        unit.Name = 'Gate Village Building 3';
        unit.Unit_Square_Feet__c = 100;
        unit.Measurement_Type__c='Office Space';
        insert unit;
        
        Unit__c unit1 = new Unit__c();
        unit1.Building__c = b.ID;
        unit1.Name = 'Gate Village Building 2';
        unit1.Unit_Square_Feet__c = 100;
        unit1.Measurement_Type__c='Office Space - External';
        insert unit1;
        
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;

        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        
        
        insert testCrmLead; // Create a test lead   
        
        op = new Offer_Process__c();
        op.OfferValidityTill__c = Date.Today();
        op.isActive__c = true;
        op.Units__c = unit.Id;
        op.IsCutomerSelected__c = false;
        op.Lead__c= testCrmLead.id;
        op.Leasing_Terms_Year__c =2;
        op.Rent_Free_Period__c = 45;
        op.Leasing_Terms_Months__c = 1;
        op.FirstYearBaseRent__c = 100;
        op.SecondYearBaseRent__c = 100;
        op.ThirdyearBaseRent__c = 100;
        op.FourthYearBaseRent__c = 100;
        op.FifthYearBaseRent__c = 100;
        op.X2ndYrEBR__c =100;
        op.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Offer_Process__c' AND DeveloperName = 'Multiple_Unit_Offer'].Id;
        op.X2ndYrUtilities__c=100;
        op.BaseRentEscalationPercentage__c = 5;
        op.BaseRentEscalationType__c =  'Amount';
        op.FirstYearBaseRent__c = 100;
        op.FirstYearBaseRent__c=10;
        op.firstyearServC__c=20;
        op.X1stYrUtilities__c=10;
        op.X1stYrEBR__c=10;
        op.SCEscalationType__c='Amount';
        op.SecondyearServiceCharge__c = 10;
        op.ThirdyearServiceCharges__c  = 5;
        op.FourthyearServiceCharges__c = 10;
        op.FifthyearServiceCharges__c = 10;
        op.SCEscalation__c = 10;
        op.ThirdyearServiceCharges__c = 100;
        op.X3rdYrUtilities__c = 100;
        op.X3rdYrEBR__c = 100;
        op.FourthyearServiceCharges__c = 100;
        op.X4thYrUtilities__c = 100;
        op.X4thYrEBR__c = 100;
        op.FifthyearServiceCharges__c = 100;
        op.X5thYrUtilities__c = 100;
        op.X5thYrEBR__c = 100;
        op.UtilitiesEscalation__c =10;
        op.ExternalBaseEscalation__c = 10;
        op.UtilitiesEscalationType__c='Percentage';
        op.ExternalBaseRentEscalationType__c ='Percentage';
        insert op;
        
        op2 = new Offer_Process__c();
        op2.OfferValidityTill__c = Date.Today();
        op2.isActive__c = false;
        op2.Units__c = unit.Id;
        op2.IsCutomerSelected__c = false;
        op2.Lead__c= testCrmLead.id;
        op2.Leasing_Terms_Year__c =2;
        op2.Rent_Free_Period__c = 40000;
        op2.Leasing_Terms_Months__c = 2;
        op2.FirstYearBaseRent__c = 100;
        op2.BaseRentEscalationPercentage__c = 5;
        op2.BaseRentEscalationType__c =  'Percentage';
        op2.FirstYearBaseRent__c = 100;
        op2.FirstYearBaseRent__c=10;
        op2.firstyearServC__c=20;
        op2.X1stYrUtilities__c=10;
        op2.X1stYrEBR__c=10;
        op2.SCEscalationType__c='Percentage';
        op2.SecondyearServiceCharge__c = 10;
        op2.ThirdyearServiceCharges__c  = 5;
        op2.FourthyearServiceCharges__c = 10;
        op2.FifthyearServiceCharges__c = 10;
        op2.SCEscalation__c = 10;
        op2.SecondYearBaseRent__c = 100;
        op2.ThirdyearBaseRent__c = 100;
        op2.FourthYearBaseRent__c = 100;
        op2.FifthYearBaseRent__c = 100;
        op2.X2ndYrEBR__c =100;
        op2.X2ndYrUtilities__c=100;
        op2.ThirdyearServiceCharges__c = 100;
        op2.X3rdYrUtilities__c = 100;
        op2.X3rdYrEBR__c = 100;
        op2.FourthyearServiceCharges__c = 100;
        op2.X4thYrUtilities__c = 100;
        op2.X4thYrEBR__c = 100;
        op2.FifthyearServiceCharges__c = 100;
        op2.X5thYrUtilities__c = 100;
        op2.X5thYrEBR__c = 100;
        op2.UtilitiesEscalation__c =10;
        op2.ExternalBaseEscalation__c = 10;
        op2.UtilitiesEscalationType__c='Amount';
        op2.ExternalBaseRentEscalationType__c ='Amount';
        insert op2;
        
        opList = new List<Offer_Process__c>();
        opList.add(op);
        opList.add(op2);
        
        uoList = new List<Unit_Offer__c>();
        Unit_Offer__c uo = new Unit_Offer__c();
        uo.OfferName__c = op.ID;
        uo.Unit__c = unit.Id;
        uo.Area__c = 100;
        uoList.add(uo);
        
        Unit_Offer__c uo1 = new Unit_Offer__c();
        uo1.OfferName__c = op.ID;
        uo1.Unit__c = unit1.Id;
        uo1.Area__c = 100;
        
        uoList.add(uo1);
        
        Insert uoList;
        
        PageReference pageRef = Page.InLineOfferProcessPage;
        Offer_Process__c op = new Offer_Process__c ();
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',null);
        pageRef.getParameters().put('CF00N0J00000A2LjS_lkid',testCrmLead.id);
        ApexPages.StandardController sc = new ApexPages.standardController(op);
        OfferProcessUtility  ctrl = new OfferProcessUtility(sc);
        ctrl.bcrentCalculation();
        ctrl.recCompId = testCrmLead.id;
         ctrl.offerProcessEdit();
    }
    static testMethod void TestMethod1(){
        createData();
        ApexPages.StandardController sc = new ApexPages.StandardController(op);
        OfferProcessUtility nrtCtrlr = new OfferProcessUtility(sc);
        nrtCtrlr.externalBaseRentCalc();
        nrtCtrlr.UtilitieChargeCalc();
        nrtCtrlr.offerProcessEdit();
    }
   static testMethod void TestMethod10(){
        
        Building__c b = new Building__c();
        b.Company_Code__c = '5300';
        b.DIFC_FitOut__c = true;
        insert b;
        
        Unit__c unit = new Unit__c();
        unit.Building__c = b.ID;
        unit.Name = 'Gate Village Building 3';
        unit.Unit_Square_Feet__c = 100;
        unit.Measurement_Type__c='Office Space';
        insert unit;
        
        Unit__c unit1 = new Unit__c();
        unit1.Building__c = b.ID;
        unit1.Name = 'Gate Village Building 2';
        unit1.Unit_Square_Feet__c = 100;
        unit1.Measurement_Type__c='Office Space - External';
        insert unit1;
        
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'BusinessCentre'].Id;
        testCrmLead.Company_Type__c='Retail';
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        
        
        insert testCrmLead; // Create a test lead   
        
        op = new Offer_Process__c();
        op.OfferValidityTill__c = Date.Today();
        op.isActive__c = true;
        op.Units__c = unit.Id;
        op.IsCutomerSelected__c = false;
        op.Lead__c= testCrmLead.id;
        op.Leasing_Terms_Year__c =2;
        op.Rent_Free_Period__c = 45;
        op.Leasing_Terms_Months__c = 1;
        op.FirstYearBaseRent__c = 100;
        op.SecondYearBaseRent__c = 100;
        op.ThirdyearBaseRent__c = 100;
        op.FourthYearBaseRent__c = 100;
        op.FifthYearBaseRent__c = 100;
        op.X2ndYrEBR__c =100;
        op.X2ndYrUtilities__c=100;
        op.BaseRentEscalationPercentage__c = 5;
        op.BaseRentEscalationType__c =  'Amount';
        op.FirstYearBaseRent__c = 100;
        op.FirstYearBaseRent__c=10;
        op.firstyearServC__c=20;
        op.X1stYrUtilities__c=10;
        op.X1stYrEBR__c=10;
        op.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Offer_Process__c' AND DeveloperName = 'Business_Centre'].Id;
        op.SCEscalationType__c='Amount';
        op.SecondyearServiceCharge__c = 10;
        op.ThirdyearServiceCharges__c  = 5;
        op.FourthyearServiceCharges__c = 10;
        op.FifthyearServiceCharges__c = 10;
        op.SCEscalation__c = 10;
        op.ThirdyearServiceCharges__c = 100;
        op.X3rdYrUtilities__c = 100;
        op.X3rdYrEBR__c = 100;
        op.FourthyearServiceCharges__c = 100;
        op.X4thYrUtilities__c = 100;
        op.X4thYrEBR__c = 100;
        op.FifthyearServiceCharges__c = 100;
        op.X5thYrUtilities__c = 100;
        op.X5thYrEBR__c = 100;
        op.UtilitiesEscalation__c =10;
        op.ExternalBaseEscalation__c = 10;
        op.UtilitiesEscalationType__c='Percentage';
        op.ExternalBaseRentEscalationType__c ='Percentage';
        insert op;
       
        
        op2 = new Offer_Process__c();
        op2.OfferValidityTill__c = Date.Today();
        op2.isActive__c = false;
        op2.Units__c = unit.Id;
        op2.IsCutomerSelected__c = false;
        op2.Lead__c= testCrmLead.id;
        op2.Leasing_Terms_Year__c =2;
        op2.Rent_Free_Period__c = 40000;
        op2.Leasing_Terms_Months__c = 2;
        op2.FirstYearBaseRent__c = 100;
        op2.UnreseverdParkingcost__c =100;
        op2.No_ofCarParks__c = 2;
        op2.BaseRentEscalationPercentage__c = 5;
        op2.BaseRentEscalationType__c =  'Percentage';
        op2.FirstYearBaseRent__c = 100;
        op2.FirstYearBaseRent__c=10;
        op2.firstyearServC__c=20;
        op2.X1stYrUtilities__c=10;
        op2.X1stYrEBR__c=10;
        op2.SCEscalationType__c='Percentage';
        op2.SecondyearServiceCharge__c = 10;
        op2.ThirdyearServiceCharges__c  = 5;
        op2.FourthyearServiceCharges__c = 10;
        op2.FifthyearServiceCharges__c = 10;
        op2.SCEscalation__c = 10;
        op2.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Offer_Process__c' AND DeveloperName = 'Business_Centre'].Id;
        op2.SecondYearBaseRent__c = 100;
        op2.ThirdyearBaseRent__c = 100;
        op2.FourthYearBaseRent__c = 100;
        op2.FifthYearBaseRent__c = 100;
        op2.X2ndYrEBR__c =100;
        op2.X2ndYrUtilities__c=100;
        op2.ThirdyearServiceCharges__c = 100;
        op2.X3rdYrUtilities__c = 100;
        op2.X3rdYrEBR__c = 100;
        op2.FourthyearServiceCharges__c = 100;
        op2.X4thYrUtilities__c = 100;
        op2.X4thYrEBR__c = 100;
        op2.FifthyearServiceCharges__c = 100;
        op2.X5thYrUtilities__c = 100;
        op2.X5thYrEBR__c = 100;
        op2.UtilitiesEscalation__c =10;
        op2.ExternalBaseEscalation__c = 10;
        op2.UtilitiesEscalationType__c='Amount';
        op2.ExternalBaseRentEscalationType__c ='Amount';
        insert op2;
       
        uoList = new List<Unit_Offer__c>();
        Unit_Offer__c uo = new Unit_Offer__c();
        uo.OfferName__c = op.ID;
        uo.Unit__c = unit.Id;
        uo.Area__c = 100;
        uoList.add(uo);
        
        Unit_Offer__c uo1 = new Unit_Offer__c();
        uo1.OfferName__c = op2.ID;
        uo1.Unit__c = unit1.Id;
        uo1.Area__c = 100;
        
        uoList.add(uo1);
        
        Insert uoList;
        
        PageReference pageRef2 = Page.InLineOfferProcessPage;
        Offer_Process__c op2 = new Offer_Process__c ();
        Test.setCurrentPage(pageRef2);
        //pageRef2.getParameters().put('id',op.ID);
        pageRef2.getParameters().put('rowIndex','0');
        pageRef2.getParameters().put('CF00N0J00000A2LjS_lkid',testCrmLead.id);
        ApexPages.StandardController sc2 = new ApexPages.standardController(op);
        OfferProcessUtility  ops2= new OfferProcessUtility(sc2);
        ops2.bcrentCalculation();
        ops2.recCompId = testCrmLead.id;
        ops2.fetchunitOffer();
        ops2.addRow();
        ops2.addunitRow();
        ops2.cancelOfferUnit();
        ops2.UtilitieChargeCalc();
        ops2.externalBaseRentCalc();
        ops2.areaCalc();
        ops2.rowIndex=1;
        ops2.deleteRow();
        
        PageReference pageRef = Page.InLineOfferProcessPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',op2.ID);
        pageRef.getParameters().put('rowIndex','0');
        pageRef.getParameters().put('CF00N0J00000A2LjS_lkid',testCrmLead.id);
        ApexPages.StandardController sc = new ApexPages.standardController(op2);
        OfferProcessUtility  ops= new OfferProcessUtility(sc);
        ops.bcrentCalculation();
        ops.recCompId = testCrmLead.id;
        ops.cancelOfferUnit();
        ops.fetchunitOffer();
        ops.addRow();
        ops.addunitRow();
        ops.areaCalc();
        ops.validationCond();
        ops.savefromUnit();
        ops.saveOfferDetails();
        ops.rentEscalation();
        ops.totalVal();
        ops.SendOfferEmail();
        ops.serviceChargeCalc();
        ops.UtilitieChargeCalc();
        ops.externalBaseRentCalc();
        ops.offerProcessEdit();
        ops.UnitOfferList  = uoList ;
        ops.rowIndex=1;
        ops.deleteRow();

       
    }
    static testMethod  void createData9(){
        
        Building__c b = new Building__c();
        b.Company_Code__c = '5300';
        b.DIFC_FitOut__c = true;
        insert b;
        
        Unit__c unit = new Unit__c();
        unit.Building__c = b.ID;
        unit.Name = 'Gate Village Building 3';
        unit.Unit_Square_Feet__c = 100;
        unit.Measurement_Type__c='Office Space';
        insert unit;
        
        Unit__c unit1 = new Unit__c();
        unit1.Building__c = b.ID;
        unit1.Name = 'Gate Village Building 2';
        unit1.Unit_Square_Feet__c = 100;
        unit1.Measurement_Type__c='Office Space - External';
        insert unit1;
        
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Retail'].Id;
        testCrmLead.Company_Type__c='Retail';
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        
        
        insert testCrmLead; // Create a test lead   
        
        op = new Offer_Process__c();
        op.OfferValidityTill__c = Date.Today();
        op.isActive__c = true;
        op.Units__c = unit.Id;
        op.IsCutomerSelected__c = false;
        op.Lead__c= testCrmLead.id;
        op.Leasing_Terms_Year__c =2;
        op.Rent_Free_Period__c = 45;
        op.Leasing_Terms_Months__c = 1;
        op.FirstYearBaseRent__c = 100;
        op.SecondYearBaseRent__c = 100;
        op.ThirdyearBaseRent__c = 100;
        op.FourthYearBaseRent__c = 100;
        op.FifthYearBaseRent__c = 100;
        op.X2ndYrEBR__c =100;
        op.X2ndYrUtilities__c=100;
        op.BaseRentEscalationPercentage__c = 5;
        op.BaseRentEscalationType__c =  'Amount';
        op.FirstYearBaseRent__c = 100;
        op.FirstYearBaseRent__c=10;
        op.firstyearServC__c=20;
        op.X1stYrUtilities__c=10;
        op.X1stYrEBR__c=10;
        op.SCEscalationType__c='Amount';
        op.SecondyearServiceCharge__c = 10;
        op.ThirdyearServiceCharges__c  = 5;
        op.FourthyearServiceCharges__c = 10;
        op.FifthyearServiceCharges__c = 10;
        op.SCEscalation__c = 10;
        op.ThirdyearServiceCharges__c = 100;
        op.X3rdYrUtilities__c = 100;
        op.X3rdYrEBR__c = 100;
        op.FourthyearServiceCharges__c = 100;
        op.X4thYrUtilities__c = 100;
        op.X4thYrEBR__c = 100;
        op.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Offer_Process__c' AND DeveloperName = 'Retail_Offer'].Id;
        op.FifthyearServiceCharges__c = 100;
        op.X5thYrUtilities__c = 100;
        op.X5thYrEBR__c = 100;
        op.UtilitiesEscalation__c =10;
        op.ExternalBaseEscalation__c = 10;
        op.UtilitiesEscalationType__c='Percentage';
        op.ExternalBaseRentEscalationType__c ='Percentage';
        insert op;
        
        op2 = new Offer_Process__c();
        op2.OfferValidityTill__c = Date.Today();
        op2.isActive__c = false;
        op2.Units__c = unit.Id;
        op2.IsCutomerSelected__c = false;
        op2.Lead__c= testCrmLead.id;
        op2.Leasing_Terms_Year__c =2;
        op2.Rent_Free_Period__c = 40000;
        op2.Leasing_Terms_Months__c = 2;
        op2.FirstYearBaseRent__c = 100;
        op2.BaseRentEscalationPercentage__c = 5;
        op2.BaseRentEscalationType__c =  'Percentage';
        op2.FirstYearBaseRent__c = 100;
        op2.FirstYearBaseRent__c=10;
        op2.firstyearServC__c=20;
        op2.X1stYrUtilities__c=10;
        op2.X1stYrEBR__c=10;
        op2.SCEscalationType__c='Percentage';
        op2.SecondyearServiceCharge__c = 10;
        op2.ThirdyearServiceCharges__c  = 5;
        op2.FourthyearServiceCharges__c = 10;
        op2.FifthyearServiceCharges__c = 10;
        op2.SCEscalation__c = 10;
        op2.SecondYearBaseRent__c = 100;
        op2.ThirdyearBaseRent__c = 100;
        op2.FourthYearBaseRent__c = 100;
        op2.FifthYearBaseRent__c = 100;
        op2.X2ndYrEBR__c =100;
        op2.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Offer_Process__c' AND DeveloperName = 'Retail_Offer'].Id;
        op2.X2ndYrUtilities__c=100;
        op2.ThirdyearServiceCharges__c = 100;
        op2.X3rdYrUtilities__c = 100;
        op2.X3rdYrEBR__c = 100;
        op2.FourthyearServiceCharges__c = 100;
        op2.X4thYrUtilities__c = 100;
        op2.X4thYrEBR__c = 100;
        op2.FifthyearServiceCharges__c = 100;
        op2.X5thYrUtilities__c = 100;
        op2.X5thYrEBR__c = 100;
        op2.UtilitiesEscalation__c =10;
        op2.ExternalBaseEscalation__c = 10;
        op2.UtilitiesEscalationType__c='Percentage';
        op2.ExternalBaseRentEscalationType__c ='Percentage';
        insert op2;
        
        uoList = new List<Unit_Offer__c>();
        Unit_Offer__c uo = new Unit_Offer__c();
        uo.OfferName__c = op.ID;
        uo.Unit__c = unit.Id;
        uo.Area__c = 100;
        uoList.add(uo);
        
        Unit_Offer__c uo1 = new Unit_Offer__c();
        uo1.OfferName__c = op.ID;
        uo1.Unit__c = unit1.Id;
        uo1.Area__c = 100;
        
        Unit_Offer__c uo2 = new Unit_Offer__c();
        uo2.OfferName__c = op.ID;
        uo2.Unit__c = unit1.Id;
        uo2.Area__c = 100;
        
        uoList.add(uo1);
        
        Insert uoList;
        
        PageReference pageRef = Page.InLineOfferProcessPage;
        Offer_Process__c op = new Offer_Process__c ();
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',null);
        pageRef.getParameters().put('CF00N0J00000A2LjS_lkid',testCrmLead.id);
        ApexPages.StandardController sc = new ApexPages.standardController(op);
        OfferProcessUtility  ctrl = new OfferProcessUtility(sc);
        ctrl.fetchunitOffer();
        ctrl.bcrentCalculation();
        ctrl.recCompId = testCrmLead.id;
        ctrl.bcrentCalculation();
        ctrl.recCompId = testCrmLead.id;
        ctrl.cancelOfferUnit();
        ctrl.addRow();
        ctrl.addunitRow();
        ctrl.areaCalc();
        ctrl.validationCond();
       
        ctrl.savefromUnit();
        ctrl.saveOfferDetails();
        ctrl.rentEscalation();
        ctrl.delUnitList = uoList ;
        ctrl.totalVal();
        ctrl.SendOfferEmail();
        ctrl.serviceChargeCalc();
        ctrl.UtilitieChargeCalc();
        ctrl.externalBaseRentCalc();
        ctrl.UnitOfferList  = uoList ;
        ctrl.rowIndex=0;
        ctrl.deleteRow();
        ctrl.offerProcessEdit();
        
    }
    
    static testMethod void createData11(){
        
        Building__c b = new Building__c();
        b.Company_Code__c = '5300';
        b.DIFC_FitOut__c = true;
        insert b;
        
        Unit__c unit = new Unit__c();
        unit.Building__c = b.ID;
        unit.Name = 'Gate Village Building 3';
        unit.Unit_Square_Feet__c = 100;
        unit.Measurement_Type__c='Office Space';
        insert unit;
        
        Unit__c unit1 = new Unit__c();
        unit1.Building__c = b.ID;
        unit1.Name = 'Gate Village Building 2';
        unit1.Unit_Square_Feet__c = 100;
        unit1.Measurement_Type__c='Office Space - External';
        insert unit1;
        
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Special_Leasing'].Id;
        testCrmLead.Company_Type__c='Retail';
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        
        
        insert testCrmLead; // Create a test lead   
        
        op = new Offer_Process__c();
        op.OfferValidityTill__c = Date.Today();
        op.isActive__c = true;
        op.Units__c = unit.Id;
        op.IsCutomerSelected__c = false;
        op.Lead__c= testCrmLead.id;
        op.Leasing_Terms_Year__c =2;
        op.Rent_Free_Period__c = 45;
        op.Leasing_Terms_Months__c = 1;
        op.FirstYearBaseRent__c = 100;
        op.SecondYearBaseRent__c = 100;
        op.ThirdyearBaseRent__c = 100;
        op.FourthYearBaseRent__c = 100;
        op.FifthYearBaseRent__c = 100;
        op.X2ndYrEBR__c =100;
        op.X2ndYrUtilities__c=100;
        op.BaseRentEscalationPercentage__c = 5;
        op.BaseRentEscalationType__c =  'Amount';
        op.FirstYearBaseRent__c = 100;
        op.FirstYearBaseRent__c=10;
        op.firstyearServC__c=20;
        op.X1stYrUtilities__c=10;
        op.X1stYrEBR__c=10;
        op.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Offer_Process__c' AND DeveloperName = 'Specialty_Leasing'].Id;
        op.SCEscalationType__c='Amount';
        op.SecondyearServiceCharge__c = 10;
        op.ThirdyearServiceCharges__c  = 5;
        op.FourthyearServiceCharges__c = 10;
        op.FifthyearServiceCharges__c = 10;
        op.SCEscalation__c = 10;
        op.ThirdyearServiceCharges__c = 100;
        op.X3rdYrUtilities__c = 100;
        op.X3rdYrEBR__c = 100;
        op.FourthyearServiceCharges__c = 100;
        op.X4thYrUtilities__c = 100;
        op.X4thYrEBR__c = 100;
        op.FifthyearServiceCharges__c = 100;
        op.X5thYrUtilities__c = 100;
        op.X5thYrEBR__c = 100;
        op.UtilitiesEscalation__c =10;
        op.ExternalBaseEscalation__c = 10;
        op.UtilitiesEscalationType__c='Percentage';
        op.ExternalBaseRentEscalationType__c ='Percentage';
        insert op;
        
        uoList = new List<Unit_Offer__c>();
        Unit_Offer__c uo = new Unit_Offer__c();
        uo.OfferName__c = op.ID;
        uo.Unit__c = unit.Id;
        uo.Area__c = 100;
        uoList.add(uo);
        
        Unit_Offer__c uo1 = new Unit_Offer__c();
        uo1.OfferName__c = op.ID;
        uo1.Unit__c = unit1.Id;
        uo1.Area__c = 100;
        
        uoList.add(uo1);
        
        Insert uoList;

        
        PageReference pageRef = Page.InLineOfferProcessPage;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('id',op.Id);
        pageRef.getParameters().put('rowIndex','1');
        pageRef.getParameters().put('CF00N0J00000A2LjS_lkid',testCrmLead.id);
        ApexPages.StandardController sc = new ApexPages.standardController(op);
        OfferProcessUtility  ops= new OfferProcessUtility(sc);
        ops.bcrentCalculation();
        ops.recCompId = testCrmLead.id;
        ops.cancelOfferUnit();
        ops.offerProcessEdit();
        ops.fetchunitOffer();
        ops.rentEscalation();
        ops.addRow();
        ops.addunitRow();
        ops.areaCalc();
        ops.validationCond();
        ops.savefromUnit();
        ops.saveOfferDetails();
       
        ops.totalVal();
        ops.SendOfferEmail();
        ops.serviceChargeCalc();
        ops.UtilitieChargeCalc();
        ops.externalBaseRentCalc();
        ops.UnitOfferList  = uoList ;
        ops.rowIndex=1;
        ops.deleteRow();
        ops.offerProcessEdit();
        ops.bcrentCalculation();
       
    }
    static testMethod void TestOpportunityMethod6(){
        test.startTest();
      
        Account testAccount = Test_cls_Crm_Utils.getTestCrmAccount(Test_cls_Crm_Utils.CRM_DEFAULT_CUSTOMERNAME,Test_cls_Crm_Utils.CRM_DEFAULT_COMPANYTYPE);
        testAccount.Lead_Reference_Number__c = '123';
        insert testAccount;
        
        Opportunity testOpportunity = Test_cls_Crm_Utils.getTestOpportunity(Test_cls_Crm_Utils.CRM_DEFAULT_OPPORTUNITYNAME,testAccount.Id,'Confirmed Unit');
        testOpportunity.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND DeveloperName = 'Offer_Process'].Id;
        testOpportunity.Oppty_Rec_Type__c = 'Leasing OfferProcess';
        insert testOpportunity;
        
       
        
        Building__c b = new Building__c();
        b.Company_Code__c = '5300';
        b.DIFC_FitOut__c = true;
        insert b;
        
        Unit__c unit = new Unit__c();
        unit.Building__c = b.ID;
        unit.Name = 'Gate Village Building 3';
        unit.Unit_Square_Feet__c = 100;
        unit.Measurement_Type__c='Office Space';
        insert unit;
        
        List<lead> leadlst = new List<lead>();
        lead testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Company_Type__c='Retail';
        testCrmLead.Activities__c = 'ATM';
        
        leadlst.add(testCrmLead);
        
        lead testCrmLead1  = new Lead();
        testCrmLead1 = Test_cls_Crm_Utils.getTestLead();
        testCrmLead1.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Retail'].Id;
        testCrmLead1.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead1.Activities__c = 'ATM';
        testCrmLead1.Company_Type__c='Retail';
        leadlst.add(testCrmLead1);
        
        lead testCrmLead3  = new Lead();
        testCrmLead3 = Test_cls_Crm_Utils.getTestLead();
        testCrmLead3.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Special_Leasing'].Id;
        testCrmLead3.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead3.Activities__c = 'ATM';
         testCrmLead3.Company_Type__c='Retail';
        leadlst.add(testCrmLead3);
        
        insert leadlst;
        
        List<Opportunity> opportunityLst = new List<Opportunity>();
        Opportunity testOpportunity1 = Test_cls_Crm_Utils.getTestOpportunity(Test_cls_Crm_Utils.CRM_DEFAULT_OPPORTUNITYNAME,testAccount.Id,'Confirmed Unit');
        testOpportunity1.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND DeveloperName = 'Offer_Process'].Id;
        testOpportunity1.Oppty_Rec_Type__c = 'Leasing OfferProcess';
        testOpportunity1.Lead_Id__c = leadlst[0].id;
        opportunityLst.add(testOpportunity1);
        
        Opportunity testOpportunity2 = Test_cls_Crm_Utils.getTestOpportunity(Test_cls_Crm_Utils.CRM_DEFAULT_OPPORTUNITYNAME,testAccount.Id,'Confirmed Unit');
        testOpportunity2.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND DeveloperName = 'BD_Retail'].Id;
        testOpportunity2.Oppty_Rec_Type__c = 'Retail';
        testOpportunity2.Lead_Id__c = leadlst[1].id;
        opportunityLst.add(testOpportunity2);
        
        Opportunity testOpportunity3 = Test_cls_Crm_Utils.getTestOpportunity(Test_cls_Crm_Utils.CRM_DEFAULT_OPPORTUNITYNAME,testAccount.Id,'Confirmed Unit');
        testOpportunity3.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND DeveloperName = 'BD_Specialty'].Id;
        testOpportunity3.Oppty_Rec_Type__c = 'Special Leasing';
       testOpportunity3.Lead_Id__c = leadlst[2].id;
        opportunityLst.add(testOpportunity3);
        
        insert opportunityLst;
        
        List<Offer_Process__c> offerList = new List<Offer_Process__c>();
        Offer_Process__c op = new Offer_Process__c();
        op.OfferValidityTill__c = Date.Today();
        op.isActive__c = true;
        op.Units__c = unit.Id;
        op.IsCutomerSelected__c = false;
        op.Lead__c= leadlst[0].id;
        op.Leasing_Terms_Year__c =2;
        op.Rent_Free_Period__c = 45;
        op.Leasing_Terms_Months__c = 1;
        op.FirstYearBaseRent__c = 100;
        op.SecondYearBaseRent__c = 100;
        op.ThirdyearBaseRent__c = 100;
        op.FourthYearBaseRent__c = 100;
        op.FifthYearBaseRent__c = 100;
        op.X2ndYrEBR__c =100;
        op.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Offer_Process__c' AND DeveloperName = 'Multiple_Unit_Offer'].Id;
        op.X2ndYrUtilities__c=100;
        op.BaseRentEscalationPercentage__c = 5;
        op.BaseRentEscalationType__c =  'Amount';
        op.FirstYearBaseRent__c = 100;
        op.FirstYearBaseRent__c=10;
        op.firstyearServC__c=20;
        op.X1stYrUtilities__c=10;
        op.X1stYrEBR__c=10;
        op.SCEscalationType__c='Amount';
        op.SecondyearServiceCharge__c = 10;
        op.ThirdyearServiceCharges__c  = 5;
        op.FourthyearServiceCharges__c = 10;
        op.FifthyearServiceCharges__c = 10;
        op.SCEscalation__c = 10;
        op.ThirdyearServiceCharges__c = 100;
        op.X3rdYrUtilities__c = 100;
        op.X3rdYrEBR__c = 100;
        op.FourthyearServiceCharges__c = 100;
        op.X4thYrUtilities__c = 100;
        op.X4thYrEBR__c = 100;
        op.FifthyearServiceCharges__c = 100;
        op.X5thYrUtilities__c = 100;
        op.X5thYrEBR__c = 100;
        op.UtilitiesEscalation__c =10;
        op.ExternalBaseEscalation__c = 10;
        op.UtilitiesEscalationType__c='Percentage';
        op.ExternalBaseRentEscalationType__c ='Percentage';
        offerList.add(op);
        
        Offer_Process__c op2 = new Offer_Process__c();
        op2.OfferValidityTill__c = Date.Today();
        op2.isActive__c = false;
        op2.Units__c = unit.Id;
        op2.IsCutomerSelected__c = false;
        op2.Lead__c= leadlst[1].id;
        op2.Leasing_Terms_Year__c =2;
        op2.Rent_Free_Period__c = 40000;
        op2.Leasing_Terms_Months__c = 2;
        op2.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Offer_Process__c' AND DeveloperName = 'Retail_Offer'].Id;
        op2.FirstYearBaseRent__c = 100;
        op2.BaseRentEscalationPercentage__c = 5;
        op2.BaseRentEscalationType__c =  'Percentage';
        op2.FirstYearBaseRent__c = 100;
        op2.FirstYearBaseRent__c=10;
        op2.firstyearServC__c=20;
        op2.X1stYrUtilities__c=10;
        op2.X1stYrEBR__c=10;
        op2.SCEscalationType__c='Percentage';
        op2.SecondyearServiceCharge__c = 10;
        op2.ThirdyearServiceCharges__c  = 5;
        op2.FourthyearServiceCharges__c = 10;
        op2.FifthyearServiceCharges__c = 10;
        op2.SCEscalation__c = 10;
        op2.SecondYearBaseRent__c = 100;
        op2.ThirdyearBaseRent__c = 100;
        op2.FourthYearBaseRent__c = 100;
        op2.FifthYearBaseRent__c = 100;
        op2.X2ndYrEBR__c =100;
        op2.X2ndYrUtilities__c=100;
        op2.ThirdyearServiceCharges__c = 100;
        op2.X3rdYrUtilities__c = 100;
        op2.X3rdYrEBR__c = 100;
        op2.FourthyearServiceCharges__c = 100;
        op2.X4thYrUtilities__c = 100;
        op2.X4thYrEBR__c = 100;
        op2.FifthyearServiceCharges__c = 100;
        op2.X5thYrUtilities__c = 100;
        op2.X5thYrEBR__c = 100;
        op2.UtilitiesEscalation__c =10;
        op2.ExternalBaseEscalation__c = 10;
        op2.UtilitiesEscalationType__c='Amount';
        op2.ExternalBaseRentEscalationType__c ='Amount';
        offerList.add(op2);
        
        insert offerList; // Create a test lead   
        
      for(Offer_Process__c offe : offerList){
        for(opportunity oppl: opportunityLst){
            offe.Opportunity__c = oppl.id;
            oppl.Lead_Id__c = offe.Lead__c;
        }
      }
      update offerList;
      update opportunityLst;
        
       
        
        Map<Id,string> leadId = new Map<Id,string>();
        leadId.put(testOpportunity.Lead_Id__c,testOpportunity.Id);
        
        List<Attachment> attList = new List<Attachment>();
        
        for(Integer i= 0;i<10;i++){
            Attachment iAtt = new Attachment();
            iAtt.parentid = testOpportunity.ID;
            iAtt.Name = 'testABC';
            iAtt.Body = Blob.valueOf('Unit Test Attachment Body');
            attList.add(iAtt);
        }
        
        Insert attList;
        
        List<Opportunity> oppList =  new List<Opportunity>();
        oppList.add(testOpportunity);
        
        OpportunityTriggerUtility opp = new OpportunityTriggerUtility(); 
        OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
        OpportunityTriggerUtility.isAttched(oppList);
        test.stopTest();
    }     
}