/******************************************************************************************
 *  Author  	: Durga Prasad
 *  Company 	: NSI JLT
 *  Date    	: 04-Aug-2014 
 *  Description	: This class is used to form dynamic queries with all the fields for 
 				  different objects                         
*******************************************************************************************/
global without sharing class Context{
    public Service_Request__c serviceRequest;
    public Step__c SRStep;
      
    public map<string,string> StepFieldMap;
    public map<string,string> SRFieldMap;
    public map<string,string> AccntFieldMap;
    public map<string,string> RegistrationMap;
    public map<string,string> LicenseFieldMap;
    public map<string,string> ContactFieldMap;
      
    public map<string,string> PL_StepFieldMap;
    public map<string,string> PL_SRFieldMap;
    public map<string,string> PL_AccntFieldMap;
    public map<string,string> PL_RegistrationMap;
    public map<string,string> PL_LicenseFieldMap;
    public map<string,string> PL_ContactFieldMap;
    
    public map<string,string> Document_StepFieldMap;
    public map<string,string> Document_SRFieldMap;
    public map<string,string> Document_AccntFieldMap;
    public map<string,string> Document_RegistrationMap;
    public map<string,string> Document_LicenseFieldMap;
    public map<string,string> Document_ContactFieldMap;
    
    public map<string,string> MapSRFieldType;
    public map<string,string> MapStepFullFields;
    public map<string,string> MapSRFullFields;
    public map<String, SObjectField> MapPropLocFullFields;
    public map<string,string> MapLeaseFullFields;
    public map<string,string> MapTenancyFullFields;
    public map<string,string> MapLPTFullFields;
    public map<string,Service_Request__c> mapSRRecords;//map to hold the SR Id as key and SR as value value contains relational fields also.
    map<string,Step__c> mapStepRecords;//map to hold the Step Id as key and Step as value value contains relational fields also.
    //map<string,Property_Location__c> mapPropLocRecords;//map to hold the Property Location Id as key and value as Property Location record with relational fields values also
    map<string,Lease__c> mapLeaseRecords;
    set<string> setLeaseFlds = new set<string>{};
     // Constructor
    public Context(){ 
        serviceRequest = new Service_Request__c();
        SRStep = new Step__c();
        MapStepFullFields = new map<string,string>();
        MapSRFullFields = new map<string,string>();
        MapPropLocFullFields = new map<String, SObjectField>();
        MapLeaseFullFields = new map<string,string>();
        MapTenancyFullFields = new map<string,string>();
        MapLPTFullFields = new map<string,string>();
        
        //mapPropLocRecords = new map<string,Property_Location__c>();
        mapLeaseRecords = new map<string,Lease__c>();
        mapSRRecords = new map<string,Service_Request__c>();
        mapStepRecords = new map<string,Step__c>();
        
        MapSRFieldType = new map<string,string>();
        
        SRFieldMap = new map<string,string>();
        SRFieldMap.put('submitted_date__c','submitted_date__c');
        
        AccntFieldMap = new map<string,string>();
        StepFieldMap = new map<string,string>();
        RegistrationMap = new map<string,string>();
        ContactFieldMap = new map<string,string>();
        LicenseFieldMap = new map<string,string>();
        
        PL_StepFieldMap = new map<string,string>();
        PL_SRFieldMap = new map<string,string>();
        PL_AccntFieldMap = new map<string,string>();
        PL_RegistrationMap = new map<string,string>();
        PL_LicenseFieldMap = new map<string,string>();
        PL_ContactFieldMap = new map<string,string>();
        
        Document_SRFieldMap = new map<string,string>();
        Document_AccntFieldMap = new map<string,string>();
        Document_RegistrationMap = new map<string,string>();
        Document_StepFieldMap = new map<string,string>();
        Document_LicenseFieldMap = new map<string,string>();
        Document_ContactFieldMap = new map<string,string>();
        
        map<String, Schema.SObjectType>  m = Schema.getGlobalDescribe();
        SObjectType  objtype;
        DescribeSObjectResult objDef1;
        map<String, SObjectField> fieldmap;
        if(m.get('Step__c')!= null)
            objtype = m.get('Step__c');
        if(objtype != null)
            objDef1 =  objtype.getDescribe();
        if(objDef1 != null)
            fieldmap =  objDef1.fields.getmap();
        if(fieldmap!=null){
            for(Schema.SObjectField strFld:fieldmap.values()){
              Schema.DescribeFieldResult fd = strFld.getDescribe();
              if(fd.isCustom() && !string.valueOf(strFld).toLowerCase().startsWith('ora_')){
                  MapStepFullFields.put(string.valueOf(strFld).toLowerCase(),string.valueOf(strFld).toLowerCase());
              }
            }
            MapStepFullFields.put('id','id');
            MapStepFullFields.put('ownerid','ownerid');
            MapStepFullFields.put('name','name');
            MapStepFullFields.put('recordtypeid','recordtypeid');
            MapStepFullFields.put('createdbyid','createdbyid');
        }
        //'contact_picture__c'=>'contact_picture__c',
        
        Map<String, String> Map_SR_Rich_LongText_Fields = new Map<String, String>{};
        /*
        Map<String, String> Map_SR_Rich_LongText_Fields = new Map<String, String>{'kp_additional_remarks__c'=>'kp_additional_remarks__c','termination_reason__c'=>'termination_reason__c','additional_terms_1__c'=>'additional_terms_1__c',
        'additional_terms_1_arabic__c'=>'additional_terms_1_arabic__c','additional_terms_2_arabic__c'=>'additional_terms_2_arabic__c','additional_terms_3_arabic__c'=>'additional_terms_3_arabic__c',
        'additional_terms_2__c'=>'additional_terms_2__c','additional_terms_3__c'=>'additional_terms_3__c','changed_unit_address__c'=>'changed_unit_address__c','disclaimer__c'=>'disclaimer__c'};
        */
        if(m.get('Service_Request__c')!= null)
            objtype = m.get('Service_Request__c');
        if(objtype != null)
            objDef1 =  objtype.getDescribe();
        if(objDef1 != null)
            fieldmap =  objDef1.fields.getmap();
        if(fieldmap!=null){
            for(Schema.SObjectField strFld:fieldmap.values()){
              Schema.DescribeFieldResult fd = strFld.getDescribe();
              // we are excluding rich text field from the main sr select 
              if(fd.isCustom() && Map_SR_Rich_LongText_Fields!=null && Map_SR_Rich_LongText_Fields.get(string.valueOf(strFld).toLowerCase())==null){
                  MapSRFullFields.put(string.valueOf(strFld).toLowerCase(),string.valueOf(strFld).toLowerCase());
                  SRFieldMap.put(string.valueOf(strFld).toLowerCase(),string.valueOf(strFld).toLowerCase());
                  PL_SRFieldMap.put(string.valueOf(strFld).toLowerCase(),string.valueOf(strFld).toLowerCase());
                  Document_SRFieldMap.put(string.valueOf(strFld).toLowerCase(),string.valueOf(strFld).toLowerCase());
              }
            }
            MapSRFullFields.put('id','id');
            SRFieldMap.put('id','id');
            PL_SRFieldMap.put('id','id');
            Document_SRFieldMap.put('id','id');
            
            MapSRFullFields.put('name','name');
            SRFieldMap.put('name','name');
            PL_SRFieldMap.put('name','name');
            Document_SRFieldMap.put('name','name');
            
            MapSRFullFields.put('ownerid','ownerid');
            SRFieldMap.put('ownerid','ownerid');
            PL_SRFieldMap.put('ownerid','ownerid');
            Document_SRFieldMap.put('ownerid','ownerid');
            
            MapSRFullFields.put('createddate','createddate');
            SRFieldMap.put('createddate','createddate');
            PL_SRFieldMap.put('createddate','createddate');
            Document_SRFieldMap.put('createddate','createddate');
            
            MapSRFullFields.put('recordtypeid','recordtypeid');
            SRFieldMap.put('recordtypeid','recordtypeid');
            PL_SRFieldMap.put('recordtypeid','recordtypeid');
            Document_SRFieldMap.put('recordtypeid','recordtypeid');
            
        }
        //system.debug('MapSRFullFields===>'+MapSRFullFields);
    }
     
    
     /*
        Method Name : PrepareDocumentConFields
        Description : Method prepares maps according to the objects in the document condition
     */ 
    public void PrepareDocumentConFields(set<id> setTemplDocIds){
        //Document_SRFieldMap = new map<string,string>();
        Document_AccntFieldMap = new map<string,string>();
        Document_RegistrationMap = new map<string,string>();
        Document_StepFieldMap = new map<string,string>();
        Document_LicenseFieldMap = new map<string,string>();
        Document_ContactFieldMap = new map<string,string>();
        
        if(setTemplDocIds!=null && setTemplDocIds.size()>0){
             for(Condition__c DC:[select id,Name,Class_Name__c,Close_Bracket__c,Conditional_Operator__c,Field_Name__c,Object_Name__c,Open_Bracket__c,Operator__c,S_No__c,Value__c from Condition__c where SR_Template_Docs__c IN:setTemplDocIds]){
                if(DC.Object_Name__c.toLowercase()=='account'){
                    Document_AccntFieldMap.put(DC.Field_Name__c,DC.Field_Name__c);
                }
                if(DC.Object_Name__c.toLowercase()=='service_request__c'){
                    //Document_SRFieldMap.put(DC.Field_Name__c,DC.Field_Name__c);
                }
                if(DC.Object_Name__c.toLowercase()=='contact'){
                    Document_ContactFieldMap.put(DC.Field_Name__c,DC.Field_Name__c);
                }
                if(DC.Object_Name__c.toLowercase()=='lease__c'){
                   setLeaseFlds.add(DC.Field_Name__c);
                }
                if(DC.Object_Name__c.toLowercase()=='license__c'){
                    Document_LicenseFieldMap.put(DC.Field_Name__c,DC.Field_Name__c);
                }
                if(DC.Object_Name__c.toLowercase()=='Step__c'){
                    Document_StepFieldMap.put(DC.Field_Name__c,DC.Field_Name__c);
                }
             }
        }
    }
    
     /*
        Method Name : PreparePricingFieldList
        Description : Method prepares maps according to the objects in the price line condition
     */
    public void PreparePricingFieldList(set<id> SetPLIds){
        system.debug('SetPLIds===>'+SetPLIds);
        PL_AccntFieldMap = new map<string,string>();
        PL_RegistrationMap = new map<string,string>();
        PL_LicenseFieldMap = new map<string,string>();
        PL_ContactFieldMap = new map<string,string>();
        set<string> setSRFlds = new set<string>{'Quantity__c','CreatedDate'};
        if(SetPLIds!=null && SetPLIds.size()>0){
            for(Condition__c objCon:[select Object_Name__c,Field_Name__c,Pricing_Line__c from Condition__c where Object_Name__c!=null and Field_Name__c!=null and Pricing_Line__c!=null and Pricing_Line__c IN:SetPLIds]){
                if(objCon.Object_Name__c.toLowercase()=='account'){
                    PL_AccntFieldMap.put(objCon.Field_Name__c,objCon.Field_Name__c);
                }
                if(objCon.Object_Name__c.toLowercase()=='contact'){
                    PL_ContactFieldMap.put(objCon.Field_Name__c,objCon.Field_Name__c);
                }
                if(objCon.Object_Name__c.toLowercase()=='lease__c'){
                   setLeaseFlds.add(objCon.Field_Name__c);
                }
                if(objCon.Object_Name__c.toLowercase()=='license__c'){
                    PL_LicenseFieldMap.put(objCon.Field_Name__c,objCon.Field_Name__c);
                }
            }
        }
        for(string srfld:setSRFlds){
            PL_SRFieldMap.put(srfld,srfld);
        }
    }
    
    /*
        Method Name : PreparePricingSR
        Description : 
     */
    public void PreparePricingSR(set<id> setSRIds){
        list<Service_Request__c> listpricingSR = new list<Service_Request__c>();
        mapSRRecords = new map<string,Service_Request__c>();
        
        if(Document_SRFieldMap!=null && Document_SRFieldMap.size()>0){
            for(string str:Document_SRFieldMap.keyset()){
                PL_SRFieldMap.put(str,str);
            }
        }
        if(Document_AccntFieldMap!=null && Document_AccntFieldMap.size()>0){
            for(string str:Document_AccntFieldMap.keyset()){
                PL_AccntFieldMap.put(str,str);
            }
        }
        if(Document_ContactFieldMap!=null && Document_ContactFieldMap.size()>0){
            for(string str:Document_ContactFieldMap.keyset()){
                PL_ContactFieldMap.put(str,str);
            }
        }
        if(Document_LicenseFieldMap!=null && Document_LicenseFieldMap.size()>0){
            for(string str:Document_LicenseFieldMap.keyset()){
                PL_LicenseFieldMap.put(str,str);
            }
        }
        string SRqry =  'Select Customer__r.Registration_License_No__c';
        //Adding all SR fields to the query
        if(MapSRFullFields!=null && MapSRFullFields.size()>0){
            for(string srField : MapSRFullFields.keyset()){
				SRqry += ','+srField;
            }
            SRqry += ','+'Parent_SR__r.Required_Docs_not_Uploaded__c';
            if(setLeaseFlds!= null && setLeaseFlds.size()>0){
                for(string leasefield:setLeaseFlds){
                  SRqry += ',Lease__r.'+leasefield;
                }
            }
        }
       
        //Adding all SR's Customers fields to the query
        if(PL_AccntFieldMap!=null && PL_AccntFieldMap.size()>0){
            for(string accntField : PL_AccntFieldMap.keyset()){
                SRqry += ','+'Customer__r.'+accntField;
            }
        }
        
        //Adding all SR's Contact fields to the query
        if(PL_ContactFieldMap!=null && PL_ContactFieldMap.size()>0){
            for(string contactField : PL_ContactFieldMap.keyset()){
                SRqry += ','+'Contact__r.'+contactField;
            }
        }
        
        //Adding all SR's License fields to the query
        if(PL_LicenseFieldMap!=null && PL_LicenseFieldMap.size()>0){
            for(string licenseField : PL_LicenseFieldMap.keyset()){
                SRqry += ','+'License__r.'+licenseField;
            }
        }
       
        SRqry += ' From Service_Request__c where Id IN:setSRIds';       
        system.debug('SRqry=>'+SRqry);
        
        //Executing query with the prepared query string
        listpricingSR = database.query(SRqry);
        for(Service_Request__c sr:listpricingSR){
            mapSRRecords.put(string.valueOf(sr.id),sr);
        }
    }
    
    /*
        Method Name : getSRFullRecord
        Description : Method returns a Service Request Record from the Map mapSRRecords
     */
    public Service_Request__c getSRFullRecord(string SRid){
        if(mapSRRecords!=null && mapSRRecords.get(SRid)!=null && mapSRRecords.get(SRid).id!=null){
            system.debug('mapSRRecords.get(SRid)===>'+mapSRRecords.get(SRid));
            return mapSRRecords.get(SRid);
        }else{
            return null;
        }
    }
    
     /*
        Method Name : PreparePropertyLocation
        Description : Method forms and fires the query to return the whole Property Location records, up to the 2nd level self reference fields 
     */
    public void PreparePropertyLocation(set<id> setPLocIds)
    {
    	/*
        MapPropLocFullFields = Schema.getGlobalDescribe().get('Property_Location__c').getDescribe().fields.getMap();
    
        string strPLocQry = 'SELECT RecordType.Name, RecordType.DeveloperName,Parent_Location__r.RecordType.Name, Parent_Location__r.RecordType.DeveloperName, Parent_Location__r.Parent_Location__r.RecordType.Name, Parent_Location__r.Parent_Location__r.RecordType.DeveloperName ';
        set<Id> PropLocId = new set<Id>();
        PropLocId.addAll(setPLocIds);
        if(MapPropLocFullFields!=null && MapPropLocFullFields.size()>0)
        {
            for(string plField : MapPropLocFullFields.keyset())
            {   
                strPLocQry += ','+plField;
                strPLocQry += ','+'Parent_Location__r.'+plField;
                strPLocQry += ','+'Parent_Location__r.Parent_Location__r.'+plField;
            }
    
        }
    
        strPLocQry += ' FROM Property_Location__c WHERE Id IN:PropLocId';
    	
        list<Property_Location__c> lstPropLoc = new list<Property_Location__c>();
        lstPropLoc = database.query(strPLocQry);
        
        for(Property_Location__c pl:lstPropLoc)
        {
            mapPropLocRecords.put(string.valueOf(pl.id),pl);
        }
    	*/
    }
    
    /*
        Method Name : getPropLocFullRecord
        Description : Method returns a Property Location Record from the Map mapPropLocRecords
    */
    /*
    public Property_Location__c getPropLocFullRecord(string PropLocId){       
        if(mapPropLocRecords!=null && mapPropLocRecords.get(PropLocId)!=null && mapPropLocRecords.get(PropLocId).id!=null)          
            return mapPropLocRecords.get(PropLocId);        
        else
            return null;
    }
    */
    
 	/*
        Method Name : Prepare_StepQuery
        Description : Method forms the dynamic query string for the step 
     */
    public void Prepare_StepQuery(set<id> setBRIds,set<id> set_Step_Ids){
        system.debug('setBRIds =>'+setBRIds);
        system.debug('set_Step_Ids =>'+set_Step_Ids);
        if(set_Step_Ids!=null && set_Step_Ids.size()>0){
            set<string> setRegFlds = new set<string>();
            set<string> setAccountFlds = new set<string>();
            set<string> setContactFlds = new set<string>();
            set<string> setSysContactFlds = new set<string>();
            setContactFlds.add('AccountId');
            setSysContactFlds.add('AccountId');
            set<string> setLicenseFlds = new set<string>();
            set<string> setSRFlds = new set<string>();
            set<string> setStepFlds = new set<string>();
           
            //Preparing step field map 
            if(PL_StepFieldMap!=null && PL_StepFieldMap.size()>0){
                for(string str:PL_StepFieldMap.keyset()){
                    //setStepFlds.add(str);
                    StepFieldMap.put(str,str);
                }
            }
            
            //Preparing SR field map 
            if(PL_SRFieldMap!=null && PL_SRFieldMap.size()>0){ 
                for(string str:PL_SRFieldMap.keyset()){
                    //setSRFlds.add(str);
                    SRFieldMap.put(str,str);
                }
            }
            
            //Preparing Account field set 
            if(PL_AccntFieldMap!=null && PL_AccntFieldMap.size()>0){
                for(string str:PL_AccntFieldMap.keyset()){
                    setAccountFlds.add(str);
                    AccntFieldMap.put(str,str);
                }
            }
            
            //Preparing Contact field set
            if(PL_ContactFieldMap!=null && PL_ContactFieldMap.size()>0){
                for(string str:PL_ContactFieldMap.keyset()){
                    setContactFlds.add(str);
                    ContactFieldMap.put(str,str);
                }
            }
            
            //Preparing License field set
            if(PL_LicenseFieldMap!=null && PL_LicenseFieldMap.size()>0){
                for(string str:PL_LicenseFieldMap.keyset()){
                    setLicenseFlds.add(str);
                    LicenseFieldMap.put(str,str);
                }
            }
            
            
            if(Document_SRFieldMap!=null && Document_SRFieldMap.size()>0){
                for(string str:Document_SRFieldMap.keyset()){
                    setSRFlds.add(str);
                    SRFieldMap.put(str,str);
                }
            }
            if(Document_StepFieldMap!=null && Document_StepFieldMap.size()>0){
                for(string str:Document_StepFieldMap.keyset()){
                    setStepFlds.add(str);
                    StepFieldMap.put(str,str);
                }
            }
            if(Document_AccntFieldMap!=null && Document_AccntFieldMap.size()>0){
                for(string str:Document_AccntFieldMap.keyset()){
                    AccntFieldMap.put(str,str);
                    setAccountFlds.add(str);
                }
            }
            if(Document_ContactFieldMap!=null && Document_ContactFieldMap.size()>0){
                for(string str:Document_ContactFieldMap.keyset()){
                    ContactFieldMap.put(str,str);
                    setContactFlds.add(str);
                }
            }
            if(Document_LicenseFieldMap!=null && Document_LicenseFieldMap.size()>0){
                for(string str:Document_LicenseFieldMap.keyset()){
                    LicenseFieldMap.put(str,str);
                    setLicenseFlds.add(str);
                }
            }
            
            system.debug('inside=>');
            system.debug('setBRIds=>'+setBRIds);
            if(setBRIds!=null && setBRIds.size()>0){
              for(Condition__c con:[select Id,Object_Name__c,Field_Name__c from Condition__c where Object_Name__c!=null and Field_Name__c!=null and Business_Rule__c!=null and Business_Rule__c IN:setBRIds and Object_Name__c!='Service_Request__c' and Object_Name__c!='Step__c']){
                  system.debug('con=>'+con);
                  if(con.Object_Name__c.toLowercase()=='account'){
                      setAccountFlds.add(con.Field_Name__c);
                      AccntFieldMap.put(con.Field_Name__c,con.Field_Name__c);
                  }
                  
                  if(con.Object_Name__c.toLowercase()=='service_request__c'){
                      system.debug('inside=>1');
                      setSRFlds.add(con.Field_Name__c);
                      SRFieldMap.put(con.Field_Name__c,con.Field_Name__c);
                  }
                  if(con.Object_Name__c.toLowercase()=='step__c'){
                      setStepFlds.add(con.Field_Name__c);
                      StepFieldMap.put(con.Field_Name__c,con.Field_Name__c);
                  }
                  if(con.Object_Name__c.toLowercase()=='lease__c'){
                     setLeaseFlds.add(con.Field_Name__c);
                  }
                  if(con.Object_Name__c.toLowercase()=='contact'){
                      setContactFlds.add(con.Field_Name__c);
                      ContactFieldMap.put(con.Field_Name__c,con.Field_Name__c);
                  }
                  if(con.Object_Name__c.toLowercase()=='license__c'){
                      setLicenseFlds.add(con.Field_Name__c);
                      LicenseFieldMap.put(con.Field_Name__c,con.Field_Name__c);
                  }
              }
            }
            system.debug('SRFieldMap=>'+SRFieldMap);
            system.debug('StepFieldMap=>'+StepFieldMap);
            system.debug('AccntFieldMap=>'+AccntFieldMap);
            system.debug('ContactMap=>'+ContactFieldMap);
            system.debug('LicenseMap=>'+LicenseFieldMap);
            
            //Preparing step query - adding all step fields
            string Stepqry =  'Select SR__r.Customer__r.Registration_License_No__c';
            if(MapStepFullFields!=null && MapStepFullFields.size()>0){
                for(string stepField : MapStepFullFields.keyset()){
                      Stepqry += ','+stepField;
                }
            }
            
            //Preparing step query - adding all SR related fields
            if(MapSRFullFields!=null && MapSRFullFields.size()>0){
               for(string SRField :MapSRFullFields.keyset()){
                    Stepqry += ','+'SR__r.'+SRField;
               }
               Stepqry += ','+'SR__r.Parent_SR__r.Required_Docs_not_Uploaded__c';
               if(setLeaseFlds!= null && setLeaseFlds.size()>0){
                  for(string leasefield:setLeaseFlds){
                    Stepqry += ',SR__r.Lease__r.'+leasefield;
                  }
               }
            }
            
            //Preparing step query - adding all SR's Customer fields
            if(setAccountFlds!=null && setAccountFlds.size()>0){
                for(string accntField : setAccountFlds){
                    Stepqry += ','+'SR__r.Customer__r.'+accntField;
                }
            }
            
            //Preparing step query - adding all SR's Contact fields
            if(setContactFlds!=null && setContactFlds.size()>0){
                for(string contctField : setContactFlds){
                    Stepqry += ','+'SR__r.Contact__r.'+contctField;
                }
            }
           
            //Preparing step query - adding all SR's License fields
            if(setLicenseFlds!=null && setLicenseFlds.size()>0){
                for(string licenseField : setLicenseFlds){
                    Stepqry += ','+'SR__r.License__r.'+licenseField;
                }
            }
            
            Stepqry += ' From Step__c where Id IN:set_Step_Ids';
            system.debug('Stepqry=>'+Stepqry);
            
            //Executing step query using prepared string and preparing mapStepRecords map
            for(Step__c stp:database.query(Stepqry)){
                if(string.valueof(stp.id).length()==18){
                    string FifteenDigitId = string.valueOf(stp.id).substring(0,15);
                    mapStepRecords.put(FifteenDigitId,stp);
                }else{
                    mapStepRecords.put(string.valueof(stp.id),stp);
                }
            }
            system.debug('mapStepRecords=>'+mapStepRecords);
        }
    }
    
    /*
        Method Name : ExecuteStepcontext
        Description : Method returns the step record from the mapStepRecords map
     */
    public Step__c ExecuteStepcontext(Step__c step){
        Step__c stp = new Step__c();
        if(step!=null && step.id!=null){
            string fifteendigitid = '';
            if(string.valueof(step.id).length()==18){
                fifteendigitid = string.valueof(step.id).substring(0,15);
            }else if(string.valueof(step.id).length()==15){
                fifteendigitid = string.valueof(step.id);
            }
            if(mapStepRecords.get(fifteendigitid)!=null){
                SRStep = mapStepRecords.get(fifteendigitid);
                stp = mapStepRecords.get(fifteendigitid);
            }
            serviceRequest = null;
        }
        system.debug('SRStep==>'+SRStep);
        return stp;
    }
}