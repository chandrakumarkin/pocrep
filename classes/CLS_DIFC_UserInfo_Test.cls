@isTest
public Class CLS_DIFC_UserInfo_Test{
    
    private static testmethod void createCommunityUser(){ 
        
        System.runAs(new User(Id=UserInfo.getUserId())){
            Id portalRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
            Id profileId = [Select Id From Profile Where Name ='DIFC Customer Community Plus User Custom'].Id;
            Account acc= new Account(Name='Test');
            insert acc;
            Contact con = new Contact(FirstName='Test', LastName='Community Contact', Email='test@gmail.com', accountId=acc.Id, RecordTypeId=portalRecordTypeId);
            insert con;
            User newUser = new User(alias = 'test123', email='test123@noemail.com',
                                    emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                    localesidkey='en_US', profileid = profileId, country='United States',IsActive =true,
                                    ContactId = con.Id,timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert newUser;
            System.Test.startTest();
            RestRequest req = new RestRequest(); 
            req.requestURI = '/services/apexrest/DIFCUserInfo'; 
            req.httpMethod = 'POST';
            CLS_DIFC_UserInfo.getUserInfo(newUser.Id);
            
            RestRequest req1 = new RestRequest(); 
            req1.requestURI = '/services/apexrest/DIFCUserInfo';
            req1.httpMethod = 'POST';
            CLS_DIFC_UserInfo.getUserInfo('');
            System.Test.stopTest();
        }      
    }
}