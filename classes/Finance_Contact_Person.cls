/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-08-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   09-15-2020   Mudasir                              Initial Version
**/
public without sharing class Finance_Contact_Person {


    public string RecordTypeId;
    public map<string,string> mapParameters;
    public String CustomerId;
    public Account ObjAccount{get;set;}
    public Service_Request__c SRData{get;set;}
    public boolean ValidTosubmit;
    public integer rowNumberToEdit{get;set;}
    
    public List<Amendment__c> ListAmend {get;set;}

    
    public Finance_Contact_Person(ApexPages.StandardController controller) {
        
        
        
        List<String> fields = new List<String> {'Customer__c','Legal_Structures__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        
        SRData=(Service_Request__c)controller.getRecord();
        ValidTosubmit=true;
        
        ListAmend=new List<Amendment__c>();
    
        mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();              
        if(mapParameters.get('RecordType')!=null) 
        RecordTypeId= mapParameters.get('RecordType');    
        else
        RecordTypeId=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Finance_Contact_Person').getRecordTypeId();
            
        
            for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                                Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                                Contact.Account.Qualifying_Type__c,Contact.Account.Restrict_certificate_of_good_standing__c,Contact.Account.Contact_Details_Provided__c,
                                Contact.Account.Index_Card__c,Contact.Account.Index_Card_Status__c,Contact.Account.Is_Foundation_Activity__c,  
                                Contact.Name,Contact.Occupation__c from User where Id=:userinfo.getUserId()])
                {
                    CustomerId = objUsr.Contact.AccountId;
                    ObjAccount= objUsr.Contact.Account;
                    system.debug('ObjAccount==>'+ObjAccount);
                    
                    if(SRData.id==null)
                    {
                    SRData.Customer__c = objUsr.Contact.AccountId;
                    SRData.RecordTypeId=RecordTypeId;
                    SRData.Email__c = objUsr.Email;
                    SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                    SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                    SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                    SRData.Sponsor_Mother_Full_Name__c=SRData.Legal_Structures__c;
                    SRData.Declaration_Signatory_Name__c = objUsr.Contact.Name;
                    SRData.Declaration_Signatory_Capacity__c = objUsr.Contact.Occupation__c;
                    }
                    
                    
                
                }
                ExistingRecords();
                    
                
    
    
        
    
    }
    
    //********************************************* UBO ***************************
    
    public  void ExistingRecords()
    {
        system.debug('SRData---'+SRData);
        if(SRData.id==null)
        {
            for(Relationship__c ObjRel:[select Object_Contact__r.Office_Unit_Number__c,Object_Contact__r.ADDRESS_LINE1__c,Object_Contact__r.ADDRESS_LINE2__c,Object_Contact__r.Emirate_State_Province__c,
            Object_Contact__r.City__c,Object_Contact__r.Country__c,Object_Contact__r.Postal_Code__c
            ,Object_Contact__c,Object_Contact__r.Occupation__c,Object_Contact__r.Salutation,Object_Contact__r.Nationality__c,Object_Contact__r.Passport_No__c,Object_Contact__r.FirstName,Object_Contact__r.LastName,Object_Contact__r.Email,Object_Contact__r.MobilePhone,Object_Contact__r.Phone,Object_Contact__r.id from Relationship__c where Relationship_Type__c='Finance Contact Person'
            and Subject_Account__c=:SRData.Customer__c and Active__c=true and Subject_Account__c!=null])
            {
                Amendment__c ObjAnd=new Amendment__c();
                
                ObjAnd.Contact__c=ObjRel.Object_Contact__c;
                ObjAnd.Title_new__c=ObjRel.Object_Contact__r.Salutation;
                ObjAnd.Family_Name__c=ObjRel.Object_Contact__r.FirstName;
                ObjAnd.Given_Name__c=ObjRel.Object_Contact__r.LastName;
                
                ObjAnd.Person_Email__c=ObjRel.Object_Contact__r.Email;
                ObjAnd.Mobile__c=ObjRel.Object_Contact__r.MobilePhone;
                ObjAnd.Phone__c=ObjRel.Object_Contact__r.Phone;
                
                ObjAnd.Nationality_list__c=ObjRel.Object_Contact__r.Nationality__c;
                ObjAnd.Passport_No__c=ObjRel.Object_Contact__r.Passport_No__c;
                ObjAnd.Occupation__c=ObjRel.Object_Contact__r.Occupation__c;
                
                //Address details 
                ObjAnd.Apt_or_Villa_No__c=ObjRel.Object_Contact__r.Office_Unit_Number__c;
                ObjAnd.Building_Name__c=ObjRel.Object_Contact__r.ADDRESS_LINE1__c;
                ObjAnd.Street__c=ObjRel.Object_Contact__r.ADDRESS_LINE2__c;
                ObjAnd.Emirate_State__c=ObjRel.Object_Contact__r.Emirate_State_Province__c;
                ObjAnd.Permanent_Native_City__c=ObjRel.Object_Contact__r.City__c;
                ObjAnd.Permanent_Native_Country__c=ObjRel.Object_Contact__r.Country__c;
                ObjAnd.Post_Code__c=ObjRel.Object_Contact__r.Postal_Code__c;
                
                
                
                
                ObjAnd.Amendment_Type__c='Individual';
                ObjAnd.Status__c='Active';
                ObjAnd.Relationship_Type__c='Finance Contact Person';
                ObjAnd.RecordTypeId=Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
                
                ObjAnd.ServiceRequest__c=SRData.id;
                ObjAnd.Customer__c=SRData.Customer__c;
                
                ListAmend.add(ObjAnd);
                
            }
        }
        else
        {
            ListAmend=[select id,Contact__c,Title_new__c,Family_Name__c,Given_Name__c,Permanent_Native_Country__c from Amendment__c where ServiceRequest__c=:SRData.id ];
        }
    }
    
    
    public  List<Amendment__c> getListofNewAmend()
    {
        if(SRData.id!=null) //Existing SR retrun all existing Amd 
        return [select id,Contact__c,Permanent_Native_Country__c,Apt_or_Villa_No__c,Permanent_Native_City__c,Building_Name__c,Street__c,Emirate_State__c,PO_Box__c,Title_new__c,Occupation__c,Family_Name__c,Given_Name__c,Job_Title__c,Passport_No__c,Person_Email__c,Nationality_list__c,Mobile__c,Phone__c from Amendment__c where ServiceRequest__c=:SRData.id and (Status__c='Active' or Status__c='Draft') order by createddate];
        else
            return ListAmend;
        
    }
        
    //When user click on Add new Amd    
    public Amendment__c TempObjAnd{get;set;}
    public  void AddAndRecord()
    {
        TempObjAnd=new Amendment__c();
        TempObjAnd.RecordTypeId=Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
        TempObjAnd.Status__c='Draft';
        TempObjAnd.ServiceRequest__c=SRData.id;
        TempObjAnd.Amendment_Type__c='Individual';
        TempObjAnd.Relationship_Type__c='Finance Contact Person';
            
    }
    //When user click on Edit Amd
        public  void EditUBORecord()
    {
        //system.assertEquals(null, rowNumberToEdit);
        TempObjAnd=new Amendment__c();
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        /*if(getListofNewAmend().size()>=param)
            {
            TempObjAnd=getListofNewAmend()[param];
            }*/
        if(getListofNewAmend().size()>=rowNumberToEdit)
            {
            TempObjAnd=getListofNewAmend()[rowNumberToEdit];
            }
        
            
    }
    //When user Click Cancel 
    public  void CancelSaveRow()
    {
        TempObjAnd=null;
    }
    
    public  void SaveRecordAndAdd()
    {
        SaveAmdRow();
        AddAndRecord();
    }
    
    public  void SaveAmdRow()
    {
        try     
            { 
    
            upsert SRData;
            
            
            //ListAmend
                if(TempObjAnd.id != NULL && ListAmend.size() > rowNumberToEdit)
                {
                   ListAmend[rowNumberToEdit] =  TempObjAnd;
                }
                else{
                    ListAmend.add(TempObjAnd);
                }
            for(Amendment__c ObjAnd:ListAmend)
            ObjAnd.ServiceRequest__c=SRData.id;
            
                upsert ListAmend;
            
            CancelSaveRow();
            }catch (Exception e)     
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
                ApexPages.addMessage(myMsg);    
            }  
        
        
        
    }
    
        public void removingUBORow()
    {
            
            //system.assertEquals(ListAmend.size(), rowNumberToEdit);
            Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
            if(ListAmend.size()>=param && ListAmend.size() > 1)
            {
            //Amendment__c TempObjPro=ListAmend[param];
            //ListAmend.remove(param);
            Amendment__c TempObjPro=ListAmend[rowNumberToEdit];
            ListAmend.remove(rowNumberToEdit);
            //system.assertEquals(Null, TempObjPro);
            //system.assertNotEquals(Null, TempObjPro);
            //system.assertEquals(rowNumberToEdit, ListAmend.size());
            if(TempObjPro.Contact__c!=null)
                TempObjPro.Status__c='Removed';
            else if(TempObjPro.id!=null)
                delete TempObjPro;
        
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Attt leaset one Finance contact person is required');         
                ApexPages.addMessage(myMsg);   
            }
    }
    
    
    
    //****************************************************************************************************************************
    
public string  validationRule()
{
    
    string Errormessage='';
    return Errormessage;
    
}   
    public  PageReference SaveRecord()
    {
    
    string Errormessage=validationRule();
    boolean isvalid=String.isBlank(Errormessage);
    if(isvalid) 
        {       
        
            try     
            {  
                upsert SRData;
                PageReference acctPage = new ApexPages.StandardController(SRData).view();        
                acctPage.setRedirect(true);        
                return acctPage;     
            }catch (Exception e)     
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
                ApexPages.addMessage(myMsg);    
            }  
            }
            else
            {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Errormessage);         
                ApexPages.addMessage(myMsg); 
            }
    
    return null;
    
        
    }
    
}