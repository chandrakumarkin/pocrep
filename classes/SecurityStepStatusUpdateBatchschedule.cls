/*********************************************************************************************************************
*  Name     : SecurityStepStatusUpdateBatchschedule 
*  Author   : Sai Kalyan
*  Purpose  : This class used to schedule SecurityStepStatusUpdateBatch Batch class
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
 V1.0          Sai
**/

global class SecurityStepStatusUpdateBatchschedule implements schedulable{
	
	global void execute(schedulablecontext sc){
		
		SecurityStepStatusUpdateBatch sc1 = new SecurityStepStatusUpdateBatch(); 
		database.executebatch(sc1,1);
	}
}