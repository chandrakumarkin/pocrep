@isTest(seeAllData=true)
  public class PassportViewerController_test{
  
     static testMethod void TestData(){
     
     map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
          mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }     
     Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
      
      Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_New');
        objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+971589602235';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+919843834346';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
       // objSR.Internal_SR_Status__c = lstSRStatus[0].Id;
       // objSR.External_SR_Status__c = lstSRStatus[0].Id;
        objSR.SAP_Unique_No__c = '123456789012345';
        insert objSR;
        
        
        SR_doc__c srDoc = new SR_doc__c();
        SrDoc.Service_Request__c  = objSR.id;
        SrDoc.status__c = 'Uploaded';
        SrDoc.name = 'Passport';
        
        insert srDoc ;
        
        Attachment att = new Attachment ();
        att.parentId = srDoc.id;
        att.name ='test';
        att.body =Blob.valueOf('Unit Test Attachment Body');
        
        insert att;
        
        
        List<attachment> atts = new List<attachment>();
        atts.add(att);
        
        List<id> aIds = new List<id>();
        
        for(attachment attachment : atts){
            aIds.add(attachment.id); 
        }
        
      
        test.startTest();
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
        PassportViewerController pvc = new PassportViewerController(con);
        pvc.getAttachments();
        
        test.stopTest();
    
     
      
      }
  }