@istest
public class OB_LockAndUnlockSRHelperTest 
{

    public static testmethod void testmethod1()
    {
       
        
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];

        // create User
        
        User u = new User();

        u.FirstName = 'A1';
        u.LastName = 'S1';
        u.Email = 'test67@test.com';
        u.Username = 'astest7645@test.com';
        u.Alias = 'astest';
        u.ProfileId = p1.Id;
        u.TimeZoneSidKey    = 'America/Denver';
        u.LocaleSidKey      = 'en_US';
        u.EmailEncodingKey  = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';

        insert u;
        system.debug('u contains ' + u);

       
        
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-reviewandconfirm';
        listPage[0].HexaBPM__Is_Custom_Component__c = true;
        listPage[0].HexaBPM__Page_Flow__c = listPageFlow[0].Id;
        
        //listPage[1].HexaBPM__VF_Page_API_Name__c = 'Shareholder';
        listPage[1].Community_Page__c = 'ob-manageshareholders';
        listPage[1].HexaBPM__Is_Custom_Component__c = true;
        listPage[1].HexaBPM__Page_Flow__c = listPageFlow[0].Id; 
        
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        system.debug('####### listSRStatus '+listSRStatus[2].Id);
       
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(1, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[2].id ; // submitted
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[2].id ; // submitted
        insertNewSRs[0].HexaBPM__Auto_Submit__c = true;
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
         system.debug('$$$$$$$$$$ before insertNewSRs[0].HexaBPM__EXternal_Status_Name__c '+insertNewSRs[0].HexaBPM__External_SR_Status__c);
        insert insertNewSRs;
        system.debug('$$$$$$$$$$ insertNewSRs '+[SELECT id, 
                                                                 HexaBPM__Internal_SR_Status__c, 
                                                                 HexaBPM__External_SR_Status__c, 
                                                                 HexaBPM__Internal_Status_Name__c, 
                                                                 HexaBPM__EXternal_Status_Name__c
                                                          FROM HexaBPM__Service_Request__c
                                                         WHERE id IN : insertNewSRs]);
        system.debug('$$$$$$$$$$ after insertNewSRs[0].HexaBPM__EXternal_Status_Name__c '+insertNewSRs[0].HexaBPM__External_SR_Status__c);
        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1,new List<String>{'Low'},new List<Integer>{1}, new List<String>{'Low'});
        insert listCS;
        //create amendments
        list<HexaBPM_Amendment__c> listAmendments = new list<HexaBPM_Amendment__c>() ;
        listAmendments = OB_TestDataFactory.createApplicationAmendment(1,listCS,insertNewSRs);
        insert listAmendments;
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'AWAITING_CHANGES'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].Pages_to_Unlock__c = 'ob-manageshareholders';
        insert listSRSteps;
        
        
        System.runas(u)
        {

             //Queue
            Group g1 = new Group(Name='ClientStep', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'HexaBPM__Step__c');
            insert q1;
            
             List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
            insertNewSteps = OB_TestDataFactory.createSteps(1, insertNewSRs, listSRSteps);
            insertNewSteps[0].ownerId = g1.Id;
            insertNewSteps[0].HexaBPM__SR_Step__c = listSRSteps[0].Id;
            insert insertNewSteps;
        }
        
       
        //---------------------------------------------------------------------------------------------------
        
       
        test.startTest();
        	system.debug('$$$$$$$$$$ insertNewSRs[0].HexaBPM__EXternal_Status_Name__c '+insertNewSRs[0].HexaBPM__External_SR_Status__c);
            OB_LockAndUnlockSRHelper locHlpr = new OB_LockAndUnlockSRHelper();
            OB_LockAndUnlockSRHelper.RequestWrapper reqWrapPram = new  OB_LockAndUnlockSRHelper.RequestWrapper();
            reqWrapPram.currentCommunityPageId = listPage[1].Id; // of shareholder.
            reqWrapPram.flowId = listPageFlow[0].Id;
            reqWrapPram.communityReviewPageName = 'ob-reviewandconfirm';
            reqWrapPram.srId = insertNewSRs[0].Id;
            
        
            OB_LockAndUnlockSRHelper.lockAndUnlockSRById(reqWrapPram);
             OB_LockAndUnlockSRHelper.GetPageLockDetails(reqWrapPram.flowId,reqWrapPram.srId);
        
        test.stopTest();
        
        
    }
    public static testmethod void testmethod2()
    {
       
        
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];

        // create User
        User u = new User();

        u.FirstName = 'A9';
        u.LastName = 'S9';
        u.Email = 'test478@test.com';
        u.Username = 'astest5656@test.com';
        u.Alias = 'astest';
        u.ProfileId = p1.Id;
        u.TimeZoneSidKey    = 'America/Denver';
        u.LocaleSidKey      = 'en_US';
        u.EmailEncodingKey  = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';

        insert u;
        system.debug('u contains ' + u);

       
        
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].HexaBPM__Is_Custom_Component__c = true;
        listPage[0].Community_Page__c = 'ob-reviewandconfirm';
        listPage[0].HexaBPM__Page_Flow__c = listPageFlow[0].Id;
        
        //listPage[1].HexaBPM__VF_Page_API_Name__c = 'Shareholder';
        listPage[1].Community_Page__c = 'ob-manageshareholders';
        listPage[1].HexaBPM__Is_Custom_Component__c = true;
        listPage[1].HexaBPM__Page_Flow__c = listPageFlow[0].Id; 
        
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        system.debug('####### listSRStatus '+listSRStatus[2].Id);
       
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(1, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        //insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[2].id ; // submitted
        //insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[2].id ; // submitted
        //insertNewSRs[0].HexaBPM__Auto_Submit__c = true;
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
         system.debug('$$$$$$$$$$ before insertNewSRs[0].HexaBPM__EXternal_Status_Name__c '+insertNewSRs[0].HexaBPM__External_SR_Status__c);
        insert insertNewSRs;
        system.debug('$$$$$$$$$$ insertNewSRs '+[SELECT id, 
                                                                 HexaBPM__Internal_SR_Status__c, 
                                                                 HexaBPM__External_SR_Status__c, 
                                                                 HexaBPM__Internal_Status_Name__c, 
                                                                 HexaBPM__EXternal_Status_Name__c
                                                          FROM HexaBPM__Service_Request__c
                                                         WHERE id IN : insertNewSRs]);
        system.debug('$$$$$$$$$$ after insertNewSRs[0].HexaBPM__EXternal_Status_Name__c '+insertNewSRs[0].HexaBPM__External_SR_Status__c);
        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1,new List<String>{'Low'},new List<Integer>{1}, new List<String>{'Low'});
        insert listCS;
        //create amendments
        list<HexaBPM_Amendment__c> listAmendments = new list<HexaBPM_Amendment__c>() ;
        listAmendments = OB_TestDataFactory.createApplicationAmendment(1,listCS,insertNewSRs);
        insert listAmendments;
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'AWAITING_CHANGES'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].Pages_to_Unlock__c = 'ob-manageshareholders';
        insert listSRSteps;
        
        
        System.runas(u)
        {

             //Queue
            Group g1 = new Group(Name='ClientStep', type='Queue');
            insert g1;
            QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'HexaBPM__Step__c');
            insert q1;
            
             List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
            insertNewSteps = OB_TestDataFactory.createSteps(1, insertNewSRs, listSRSteps);
            insertNewSteps[0].ownerId = g1.Id;
            insertNewSteps[0].HexaBPM__SR_Step__c = listSRSteps[0].Id;
            insert insertNewSteps;
        }
        
       
        //---------------------------------------------------------------------------------------------------
        
       
        test.startTest();
        	system.debug('$$$$$$$$$$ insertNewSRs[0].HexaBPM__EXternal_Status_Name__c '+insertNewSRs[0].HexaBPM__External_SR_Status__c);
            OB_LockAndUnlockSRHelper locHlpr = new OB_LockAndUnlockSRHelper();
            OB_LockAndUnlockSRHelper.RequestWrapper reqWrapPram = new  OB_LockAndUnlockSRHelper.RequestWrapper();
            reqWrapPram.currentCommunityPageId = listPage[1].Id; // of shareholder.
            reqWrapPram.flowId = listPageFlow[0].Id;
            reqWrapPram.communityReviewPageName = 'ob-reviewandconfirm';
            reqWrapPram.srId = insertNewSRs[0].Id;
            
        
            OB_LockAndUnlockSRHelper.lockAndUnlockSRById(reqWrapPram);
             OB_LockAndUnlockSRHelper.GetPageLockDetails(reqWrapPram.flowId,reqWrapPram.srId);
        
        test.stopTest();
        
        
    }
    // for negative usecase
     public static testmethod void testmethod3()
    {
       
        
        
        Profile p1 = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];

        // create User
        User u = new User();

        u.FirstName = 'A';
        u.LastName = 'S';
        u.Email = 'test9090889@test.com';
        u.Username = 'astest98080@test.com';
        u.Alias = 'astest';
        u.ProfileId = p1.Id;
        u.TimeZoneSidKey    = 'America/Denver';
        u.LocaleSidKey      = 'en_US';
        u.EmailEncodingKey  = 'UTF-8';
        u.LanguageLocaleKey = 'en_US';

        insert u;
        system.debug('u contains ' + u);

       
        
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].HexaBPM__Is_Custom_Component__c = true;
        listPage[0].Community_Page__c = 'ob-reviewandconfirm';
        listPage[0].HexaBPM__Page_Flow__c = listPageFlow[0].Id;
        
        //listPage[1].HexaBPM__VF_Page_API_Name__c = 'Shareholder';
        listPage[1].Community_Page__c = 'ob-manageshareholders';
        listPage[1].HexaBPM__Is_Custom_Component__c = true;
        listPage[1].HexaBPM__Page_Flow__c = listPageFlow[0].Id; 
        
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        system.debug('####### listSRStatus '+listSRStatus[2].Id);
       
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(1, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[2].id ; // submitted
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[2].id ; // submitted
        insertNewSRs[0].HexaBPM__Auto_Submit__c = true;
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
         system.debug('$$$$$$$$$$ before insertNewSRs[0].HexaBPM__EXternal_Status_Name__c '+insertNewSRs[0].HexaBPM__External_SR_Status__c);
        insert insertNewSRs;
        system.debug('$$$$$$$$$$ insertNewSRs '+[SELECT id, 
                                                                 HexaBPM__Internal_SR_Status__c, 
                                                                 HexaBPM__External_SR_Status__c, 
                                                                 HexaBPM__Internal_Status_Name__c, 
                                                                 HexaBPM__EXternal_Status_Name__c
                                                          FROM HexaBPM__Service_Request__c
                                                         WHERE id IN : insertNewSRs]);
        system.debug('$$$$$$$$$$ after insertNewSRs[0].HexaBPM__EXternal_Status_Name__c '+insertNewSRs[0].HexaBPM__External_SR_Status__c);
        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1,new List<String>{'Low'},new List<Integer>{1}, new List<String>{'Low'});
        insert listCS;
        //create amendments
        list<HexaBPM_Amendment__c> listAmendments = new list<HexaBPM_Amendment__c>() ;
        listAmendments = OB_TestDataFactory.createApplicationAmendment(1,listCS,insertNewSRs);
        insert listAmendments;
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'AWAITING_CHANGES'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        //listSRSteps[0].Pages_to_Unlock__c = 'ob-manageshareholders1';  // negative use case
        insert listSRSteps;
        
        
        
        
       
        //---------------------------------------------------------------------------------------------------
        
       
        test.startTest();
        	system.debug('$$$$$$$$$$ insertNewSRs[0].HexaBPM__EXternal_Status_Name__c '+insertNewSRs[0].HexaBPM__External_SR_Status__c);
            OB_LockAndUnlockSRHelper locHlpr = new OB_LockAndUnlockSRHelper();
            OB_LockAndUnlockSRHelper.RequestWrapper reqWrapPram = new  OB_LockAndUnlockSRHelper.RequestWrapper();
            reqWrapPram.currentCommunityPageId = listPage[1].Id; // of shareholder.
            reqWrapPram.flowId = listPageFlow[0].Id;
            reqWrapPram.communityReviewPageName = 'ob-reviewandconfirm';
            reqWrapPram.srId = insertNewSRs[0].Id;
        
            reqWrapPram.communityName = 'test';
            reqWrapPram.sitePageName = 'test';
            reqWrapPram.strBaseUrl = 'test';
            reqWrapPram.pageId = 'test';
            
        
            OB_LockAndUnlockSRHelper.lockAndUnlockSRById(reqWrapPram);
             OB_LockAndUnlockSRHelper.GetPageLockDetails(reqWrapPram.flowId,reqWrapPram.srId);
        
        test.stopTest();
        
        
    }
    
}