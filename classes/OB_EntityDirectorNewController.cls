/**
*Author : Merul Shah
*Description : This compoents creates the new  Ind/BC directors.
**/

public class OB_EntityDirectorNewController
{
    
    
    @AuraEnabled   
    public static ResponseWrapper initAmendmentDB(String reqWrapPram)
    {
        
        
        //reqest wrpper
        OB_EntityDirectorNewController.RequestWrapper reqWrap = (OB_EntityDirectorNewController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_EntityDirectorNewController.RequestWrapper.class);
        
        //response wrpper
        OB_EntityDirectorNewController.ResponseWrapper respWrap = new OB_EntityDirectorNewController.ResponseWrapper();
        String srId = reqWrap.srId; 
        String amedtype = reqWrap.amedtype;
        
        List<RecordType> lstRecTyp = [SELECT id,
                                             DeveloperName,
                                             SobjectType 
                                        FROM RecordType 
                                       WHERE DeveloperName =:amedtype  
                                         AND SobjectType = 'HexaBPM_Amendment__c'];
        
            
        
        OB_EntityDirectorNewController.AmendmentWrapper amedWrap = new OB_EntityDirectorNewController.AmendmentWrapper();
        HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c();
        amedObj.ServiceRequest__c = srId;
        amedObj.recordTypeId = lstRecTyp[0].Id;
        system.debug('######## amedObj.recordTypeId   '+amedObj.recordTypeId);
        //amedObj.Is_Director__c = true;
        
        if(amedObj.Role__c == NULL )
        {
            amedObj.Role__c = 'Director';
        }
        else if(amedObj.Role__c.indexOf('Director') == -1)
        {
             amedObj.Role__c = amedObj.Role__c +';Director';
        }
        
        
        amedWrap.isIndividual = ( amedtype == 'Individual'  ? true : false);
        amedWrap.isBodyCorporate =  ( amedtype == 'Body_Corporate' ? true : false );
        amedWrap.amedObj = amedObj;
        respWrap.amedWrap = amedWrap;
        
        return respWrap;
    }
    

    /*
    public class SRWrapper 
    {
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public List<OB_EntityDirectorContainerController.AmendmentWrapper> amedWrapLst { get; set; }
        
        public SRWrapper()
        {}
    }
    */
    public class AmendmentWrapper 
    {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public Boolean isIndividual { get; set; }
        @AuraEnabled public Boolean isBodyCorporate { get; set; }
        
        public AmendmentWrapper()
        {}
    }
    
    
    public class RequestWrapper 
    {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public String amedtype { get; set; }
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
       
      
        public RequestWrapper()
        {}
    }

    public class ResponseWrapper 
    {
        //@AuraEnabled public SRWrapper  srWrap{get;set;}
        @AuraEnabled public AmendmentWrapper amedWrap{get;set;}
        
        public ResponseWrapper()
        {}
    }

}