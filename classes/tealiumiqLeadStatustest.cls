@isTest
private class tealiumiqLeadStatustest{

    static testMethod void myTestMethod() {        
         test.starttest();
         
         Lead ObjLead=new Lead();
         ObjLead.LeadSource='Company Website';
         ObjLead.LastName='ss';
         ObjLead.Email='asd@cs.com';
         ObjLead.Milestone_ID__c='ssss2222222s';
         ObjLead.Company='kkkk';
         
         insert ObjLead;
         
         tealiumiqLeadStatus myClass = new tealiumiqLeadStatus ();   
         String chron = '0 0 23 * * ?';        
         system.schedule('Test Sched', chron, myClass);
         test.stopTest();
    }
}