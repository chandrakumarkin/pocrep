/************************************************************************************************************************
    Author      :   Durga Prasad
    Class Name  :   CC_Validations
    Description :   Custom code which is used for the Custom validations those are not possible through standard validations.
    
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date           Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    16-09-2015     Saima         Added ROC status for Name validation
 V1.2    27-12-2015     Ravi          Added logic as per #1428 to validate the Lease for Change of Entity Name & Change of Authorized Signatory
 V1.3    05-Feb-16      Shabbir       Add logic for future lease Assembla# 2122
************************************************************************************************************************/
public without sharing class CC_Validations {
    public static string validateOpenSR(Step__c stp){
        string strResult = 'Success';
        String SRName='';
        if(stp!=null && stp.SR__c!=null && stp.SR__r.Customer__c!=null){
            for(Service_Request__c SR :[SELECT id,name,Customer__c,Record_Type_Name__c,isClosedStatus__c,Is_Rejected__c,IsCancelled__c FROM Service_Request__c WHERE Id!=:stp.SR__c and RecordTypeId=:stp.SR__r.RecordTypeId and (isClosedStatus__c=FALSE AND Is_Rejected__c=FALSE AND IsCancelled__c=FALSE) AND Customer__c=:stp.SR__r.Customer__c]){
                SRName = SR.name;
            }
        }
        if(SRName!=null && SRName!=''){
            strResult = 'Please cancel the request no '+ SRName + ' which is similar in order to proceed.';
        }
        return strResult;
    }
    public static string Validate_Duplicate_CompanyName(Step__c stp){
        string strResult = 'Success';
        if(stp!=null && stp.SR__c!=null && stp.SR__r.Entity_Name__c!=null){
            for(Account ObjAcc:[select id,Name,ROC_Status__c from Account where Name=:stp.SR__r.Entity_Name__c and Id!=:stp.SR__r.Customer__c and ROC_Status__c!=null limit 1]){
                strResult = 'There already exists a company with the same name as '+stp.SR__r.Entity_Name__c+'. Kindly select some other name.';
            }
        }
        return strResult;
    }
    public static string CheckOpenSR(string SRID,string SRTemplateId,string strRecordTypeName,string CustomerId){
        string strResult = 'Success';
        
        system.debug('SRID==>'+SRID);
        system.debug('strRecordTypeName==>'+strRecordTypeName);
        system.debug('SRTemplateId==>'+SRTemplateId);
        system.debug('CustomerId==>'+CustomerId);
        
        string SRName='';
        string TemplateName = '';
        if(SRTemplateId!=null && CustomerId!=null){
            if(SRID!=null){
                for(Service_Request__c SR :[SELECT id,name,SR_Template__c,SR_Template__r.Name,Customer__c,Record_Type_Name__c,isClosedStatus__c,Is_Rejected__c,IsCancelled__c FROM Service_Request__c WHERE SR_Template__c=:SRTemplateId and SR_Template__r.Allow_Multiple_SR_Submission__c=false and Customer__c=:CustomerId and isClosedStatus__c=false AND Is_Rejected__c=false AND IsCancelled__c=false and Id!=:SRID limit 1]){
                    SRName = SR.name; TemplateName = SR.SR_Template__r.Name;
                }
            }else{
                for(Service_Request__c SR :[SELECT id,name,SR_Template__c,SR_Template__r.Name,Customer__c,Record_Type_Name__c,isClosedStatus__c,Is_Rejected__c,IsCancelled__c FROM Service_Request__c WHERE SR_Template__c=:SRTemplateId  and SR_Template__r.Allow_Multiple_SR_Submission__c=false and Customer__c=:CustomerId and isClosedStatus__c=false AND Is_Rejected__c=false AND IsCancelled__c=false limit 1]){ 
                    SRName = SR.name;TemplateName = SR.SR_Template__r.Name;
                }
            }
        }
        if(SRName!=null && SRName!=''){
            //strResult = 'There is a request already submitted/in draft for '+TemplateName+'. Please check request no : '+SRName;
            
            strResult = 'There is a similar request (SR '+SRName+') raised through the portal. Please check under Service Requests.';
            
        }
        return strResult;
    }
    
    public static string CheckActiveLease(Step__c Step){
        string res = 'Success';
        Boolean isLease = false;
        set<string> setLeaseStatus = new set<string>{'Active' , 'Future Lease'}; //v1.3 
        for(Lease__c obj : [select Id,Status__c from Lease__c where Account__c =: Step.SR__r.Customer__c AND  Status__c IN:setLeaseStatus AND (Type__c = 'Leased' OR Is_License_to_Occupy__c = true) limit 1]){ //v1.3
            isLease = true;
        }
        if(isLease == false){ //check for sharing
            for(Lease__c obj : [select Id,Status__c from Lease__c where Account__c IN (select Hosting_Company__c from Account where Id=:Step.SR__r.Customer__c) AND Status__c IN:setLeaseStatus AND (Type__c = 'Leased' OR Is_License_to_Occupy__c = true) limit 1]){ //v1.3
                isLease = true;
            }
        }
        if(isLease == false)
            res = 'There is no active lease for the client.';
        return res;
    }
    
}