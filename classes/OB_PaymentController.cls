/**
*Author : Rajil Ravindran
*Description : This controller is used to process the payment and generate receipts related to Name Reservation and Self Registration.  
*Created Date : 16.10.2019
Merul:  v 1.0  changes for namerenewal in viewProductInformation
Merul:  v 2.0  changes for namerenewal in updateSRLines
Merul:  v 3.0  changes for namerenewal in processNameRenewalSR
Maruf:  v 4.0  Changes for removing Non Deductible items in updateSRLines
**/ 
public without sharing class OB_PaymentController 
{
    
    //Merul: This method closes Name renewal SR and it related step.Also it updates the dates on company name.  
    //Merul:  v 3.0  changes for namerenewal in processNameRenewalSR
    @AuraEnabled
    public static void processNameRenewalSR( String requestWrapParam )
    {
        system.debug('$$$$$$$ String reqWrapPram '+requestWrapParam);
        RequestWrapper reqWrap = (OB_PaymentController.RequestWrapper) JSON.deserializeStrict(requestWrapParam, OB_PaymentController.RequestWrapper.class);
        HexaBPM__Status__c stepClosedStatus;
        HexaBPM__SR_Status__c srClosedStatus;
        
        List<HexaBPM__Service_Request__c> lstSRToUpdate = new List<HexaBPM__Service_Request__c>();
        List<HexaBPM__Step__c> lstStepToUpdate = new List<HexaBPM__Step__c>();
        List<Company_Name__c> lstCompNameToUpdate = new List<Company_Name__c>();
        Set<Id> setInPrinciple = new Set<Id>();
        
        
        if(reqWrap.srId != NULL)
        {
            
            // get closed HexaBPM__Status__c
            for(HexaBPM__Status__c stepStatusTemp : [
                SELECT id,
                HexaBPM__Code__c 
                FROM HexaBPM__Status__c
                WHERE HexaBPM__Code__c = 'CLOSED'
                LIMIT 1
            ]
               )
            {
                stepClosedStatus = stepStatusTemp;
            }
            system.debug('$$$$$$$ String stepClosedStatus  '+stepClosedStatus);
            
            // get closed HexaBPM__SR_Status__c
            for(HexaBPM__SR_Status__c srStatusTemp : [
                SELECT id,
                HexaBPM__Code__c 
                FROM HexaBPM__SR_Status__c
                WHERE HexaBPM__Code__c = 'CLOSED'
                LIMIT 1
            ]
               )
            {
                srClosedStatus = srStatusTemp;
            }
            system.debug('$$$$$$$ String srClosedStatus  '+srClosedStatus);
            
            //filters remaining... this Name renewal SR.    
            for(HexaBPM__Service_Request__c sr :  [
                SELECT id, 
                HexaBPM__Parent_SR__c,
                (SELECT id 
                 FROM HexaBPM__Steps_SR__r)
                FROM HexaBPM__Service_Request__c
                WHERE id =:reqWrap.srId 
                AND HexaBPM__Record_Type_Name__c = 'Name_Reservation_Renewal'
                LIMIT 1
            ]
               )
            {
                system.debug('$$$$$$$ String sr  '+sr);
                sr.HexaBPM__External_SR_Status__c = srClosedStatus.Id;
                sr.HexaBPM__Internal_SR_Status__c = srClosedStatus.Id;
                lstSRToUpdate.add(sr);
                if( sr.HexaBPM__Parent_SR__c != NULL )
                {
                    setInPrinciple.add(sr.HexaBPM__Parent_SR__c);
                }
                system.debug('$$$$$$$ setInPrinciple  '+setInPrinciple);
                
                if( sr.HexaBPM__Steps_SR__r != NULL 
                   && sr.HexaBPM__Steps_SR__r.size() > 0)
                {
                    for( HexaBPM__Step__c step : sr.HexaBPM__Steps_SR__r)
                    {
                        step.HexaBPM__Status__c = stepClosedStatus.Id;    
                        lstStepToUpdate.add(step);
                    }
                    
                }
                system.debug('$$$$$$$ lstStepToUpdate  '+lstStepToUpdate);
                
            }
            
            
            if(lstSRToUpdate.size() > 0)
            {
                update lstSRToUpdate;
            }
            
            if(lstStepToUpdate.size() > 0)
            {
                update lstStepToUpdate;
            }
            
            // push compay name date to 90 days.
            // only one can be Approved and reserved at a time.
            for( Company_Name__c cmpName : [
                SELECT Id,
                Name,
                Date_of_Expiry__c,
                Date_of_Approval__c,
                Application__c,
                Application__r.HexaBPM__Customer__c 
                FROM Company_Name__c 
                WHERE Application__c IN:setInPrinciple 
                AND Date_of_Expiry__c != NULL
                AND Status__c = 'Approved'
                AND Reserved_Name__c = true
                
            ]
               )
            {
                // sr.HexaBPM__Parent_SR__c
                if( cmpName.Date_of_Approval__c != NULL )
                {
                    cmpName.Date_of_Approval__c = cmpName.Date_of_Approval__c + 90;
                    cmpName.Reserved_Name__c = false;
                    lstCompNameToUpdate.add(cmpName);
                }
            }
            
            if(lstCompNameToUpdate.size() > 0)
            {
                update lstCompNameToUpdate;
            }
            
            
        }
        
    }
    
    
    @AuraEnabled
    public static Receipt updateReceipt(String reqWrapPram ) {
        Receipt receiptObj = new Receipt();
        for(User usr : [Select Contact.Account.Name from User where Id =: UserInfo.getUserId()]){
            receiptObj.accountName = usr.Contact.Account.Name;
        }
        RequestWrapper reqWrap = (OB_PaymentController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_PaymentController.RequestWrapper.class);
        system.debug('==reqWrap===='+reqWrap);
        receiptObj.receiptRef = OB_PaymentController.updateReceiptRecord(reqWrap);
        return receiptObj;
    }
    public static string updateReceiptRecord(RequestWrapper reqWrap){
        system.debug('=====reqWrap===='+reqWrap);
        
        //Updates the receipt record
        Receipt__c receiptObj = new Receipt__c(Id = reqWrap.receiptId);
        receiptObj.Amount__c=Decimal.valueof(reqWrap.amount); 
        receiptObj.Transaction_Date__c=system.now(); 
        receiptObj.Checkout_Payment_ID__c=reqWrap.checkoutPaymentID;
        receiptObj.Payment_Gateway_Ref_No__c=reqWrap.checkoutPaymentID;
        receiptObj.Payment_Status__c=reqWrap.receiptStatus;
        receiptObj.Card_Type__c=reqWrap.cardType;
        receiptObj.Bank_Ref_No__c = reqWrap.bankReferenceNumber;
        update receiptObj; 
        string receiptReference;
        for(Receipt__c receipt : [select name from Receipt__c where id = :receiptObj.Id])
            receiptReference = receipt.name;
        
        return receiptReference;
        
    }
    @AuraEnabled
    public static ProductInfo viewProductInformation(string srId, string category){
        
        ProductInfo prod = new ProductInfo();
        decimal totalAmt = 0;
        decimal totalPriceInAED = 0;
        string accountId;
        for(User usr : [Select contactid,Contact.AccountId,Contact.Account.Name from User where Id =: UserInfo.getUserId()]){
            accountId = usr.Contact.AccountID;
            prod.accountName = usr.Contact.Account.Name;
        }
        if(String.isNotBlank(srId))
        {
            
            // Merul:  v 1.0  changes for namerenewal in viewProductInformation
            // adding  category == 'namerenewal'
            if(String.isNotBlank(category) && ( category == 'namereserve' ||  category == 'namerenewal' ) )
                //if(String.isNotBlank(category)  && category == 'namereserve')
            {
                for(HexaBPM__SR_Price_Item__c srPriceItem : [select HexaBPM__Product__r.Name,Pricing_Line_Name__c,Total_Price_USD__c,Total_Price_AED__c  from HexaBPM__SR_Price_Item__c 
                                                             where HexaBPM__ServiceRequest__c = :srId  
                                                             //and HexaBPM__Pricing_Line__r.Non_Deductible__c=false
                                                             and HexaBPM__ServiceRequest__r.HexaBPM__Customer__c = :accountId 
                                                             and HexaBPM__Product__r.ProductCode = 'OB Name Reservation' 
                                                             and HexaBPM__Status__c = 'Blocked' limit 1]){
                                                                 prod.lstPriceItem.add(srPriceItem);
                                                                 totalAmt = totalAmt + srPriceItem.Total_Price_USD__c;
                                                                 totalPriceInAED = totalPriceInAED + srPriceItem.Total_Price_AED__c;                               
                                                             }
            }
            else
            {
                for(HexaBPM__SR_Price_Item__c srPriceItem : [select HexaBPM__Product__r.Name,Pricing_Line_Name__c,Total_Price_USD__c,Total_Price_AED__c from HexaBPM__SR_Price_Item__c
                                                             where HexaBPM__ServiceRequest__c = :srId
                                                             //and HexaBPM__Pricing_Line__r.Non_Deductible__c=false 
                                                             and HexaBPM__ServiceRequest__r.HexaBPM__Customer__c = :accountId 
                                                             and HexaBPM__Product__r.ProductCode != 'OB Name Reservation'
                                                             and HexaBPM__Status__c = 'Blocked']){
                                                                 prod.lstPriceItem.add(srPriceItem);
                                                                 totalAmt = totalAmt + srPriceItem.Total_Price_USD__c;
                                                                 totalPriceInAED = totalPriceInAED + srPriceItem.Total_Price_AED__c;                                
                                                             }
            }
            prod.total = totalAmt;
            prod.totalInAED = totalPriceInAED;
        }
        
        return prod;
    }
    /*
public static string createReceiptRecord(RequestWrapper reqWrap){
system.debug('=====reqWrap===='+reqWrap);
string accountId;
boolean isFitOutContractor = false;
for(User usr : [Select Contact.AccountId,contact.Contractor__c,Contact.Account.RecordType.DeveloperName from User where Id =:UserInfo.getUserId()]){
//accountId = usr.Contact.AccountID;
if(usr.contact.Contractor__c != null){
isFitOutContractor = (String.isNotBlank(usr.Contact.Contractor__c) && !usr.Contact.Contractor__c.equals(usr.Contact.AccountId) ) || 
usr.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account');
}
accountId = isFitOutContractor && String.isNotBlank(usr.Contact.Contractor__c) ? usr.Contact.Contractor__c : usr.Contact.AccountId;
}
Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get(reqWrap.paymentType).getRecordTypeId();
//Create a receipt record with values entered by the customer
Receipt__c receiptObj = new Receipt__c();
receiptObj.Customer__c=accountId;
if(String.isNotBlank(reqWrap.srId))
receiptObj.OB_Application__c = reqWrap.srId;
receiptObj.Amount__c=Decimal.valueof(reqWrap.amount); 
receiptObj.Receipt_Type__c=reqWrap.paymentType;
receiptObj.Transaction_Date__c=system.now(); 
receiptObj.RecordTypeId=rt;
receiptObj.Checkout_Payment_ID__c=reqWrap.checkoutPaymentID;
receiptObj.Payment_Gateway_Ref_No__c=reqWrap.checkoutPaymentID;
receiptObj.Payment_Status__c=reqWrap.receiptStatus;
receiptObj.Card_Type__c=reqWrap.cardType;
receiptObj.Bank_Ref_No__c = reqWrap.bankReferenceNumber;
insert receiptObj; 
string receiptReference;
for(Receipt__c receipt : [select name from Receipt__c where id = :receiptObj.Id])
receiptReference = receipt.name;

return receiptReference;

}
*/
    @AuraEnabled
    public static List <String> getselectOptions(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allOpts = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        System.debug('objType is' + objType);
        // Describe the SObject using its object type.
        
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        System.debug('objDescribe is' + objDescribe);
        System.debug('Result is' + objObject.getSObjectType().getDescribe().fields.getMap());
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        System.debug('fieldMap is' + fieldMap);
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values = fieldMap.get(fld).getDescribe().getPickListValues();
        System.debug('values are '+values);
        // Add these values to the select option list.
        for (Schema.PicklistEntry a: values) {
            allOpts.add(a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        allOpts.sort();
        return allOpts;
    }
    //Submitting for Name reservation approval process   
    public static void submitForNameApproval(String transactionId,String srId){
        
        List<Company_Name__c> lstCompNames = new List<Company_Name__c>();
        String submittedNameRefNumbers='';
        string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        for(Company_Name__c cn: [select id,Entity_Name__c,OwnerId,Ends_With__c,
                                 Name,Stage__c,Status__c,Reserved_Name__c
                                 from Company_Name__c where Application__c =:srId
                                 and Application__r.HexaBPM__Customer__c = :accountId]){
                                     cn.Stage__c='Submitted';
                                     cn.Status__c='In Progress'; 
                                     cn.Reserved_Name__c=true;
                                     lstCompNames.add(cn);
                                 }
        if(lstCompNames != null && lstCompNames.size() > 0)
            update lstCompNames;
    }
    //This method is invoked from OB_CheckoutPayment Component once the credit card payment is approved.
    @AuraEnabled
    public static ProductInfo createNameReservationSR(string srId){
        
        ProductInfo productObj = new ProductInfo(); 
        try{
            string recordTypeName;
            string pageId;
            string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
            //Create Sub SR
            OB_NamingConventionHelper.CreateNameApprovalSR(srId);
            
            //Submit CompanyNames for Approval
            OB_NamingConventionHelper.submitCompanyNameForApproval(srId);
            
            //Updating the Price Item to Blocked
            list<HexaBPM__SR_Price_Item__c> lstSRPriceItem = new list<HexaBPM__SR_Price_Item__c>();
            for(HexaBPM__SR_Price_Item__c srPriceItem : [select Id,HexaBPM__Product__r.Name,Pricing_Line_Name__c,HexaBPM__Status__c,Total_Price_USD__c,Total_Price_AED__c  
                                                         from HexaBPM__SR_Price_Item__c
                                                         where HexaBPM__ServiceRequest__r.HexaBPM__Customer__c = :accountId 
                                                         and HexaBPM__Product__r.ProductCode = 'OB Name Reservation'
                                                         and HexaBPM__Status__c = 'Added' 
                                                         and HexaBPM__ServiceRequest__c = :srId limit 1])
            {
                srPriceItem.HexaBPM__Status__c = 'Blocked'; 
                lstSRPriceItem.add(srPriceItem);
                productObj.lstPriceItem.add(srPriceItem);
                productObj.total =  srPriceItem.Total_Price_USD__c;
                productObj.totalInAED =  srPriceItem.Total_Price_AED__c;
                productObj.name = srPriceItem.HexaBPM__Product__r.Name;
                
            }
            if(lstSRPriceItem != null && lstSRPriceItem.size() > 0)
                update lstSRPriceItem;
            
            OB_NamingConventionHelper.setPageTrackerAfterPayment(srId,'ob-searchentityname');
        } 
        catch (DMLException e)
        {
            productObj.isError = true;
            string DMLError = e.getdmlMessage(0) + '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
            productObj.errorMessage = DMLError;
            System.debug('=====>'+ e.getMessage());
        }
        return productObj;
    }
    // Update the SR_Price_items to blocked(exclude Name Reservation Price Item) when the payment is done.  
    // Update the SR Status to Submitted  
    //The below method is invoked from OB_CheckoutPayment Component and also from OB_PaymentModeController class
    // v 2.0  changes for namerenewal in updateSRLines
    @AuraEnabled
    public static ProductInfo updateSRLines(String srId){
        //SRPayment paymentObj = new SRPayment();
        ProductInfo productObj = new ProductInfo();
        decimal totalAmt = 0;
        decimal totalAmtInAED = 0;
        boolean isSuccess = false;
        boolean IsAllowedToSubmit = false;
        boolean ChangeSRStatus = true;
        string accountId = OB_QueryUtilityClass.getAccountId(UserInfo.getUserId());
        
        Savepoint Stat_svpoint = Database.setSavepoint();
        
        try{
            HexaBPM__Service_Request__c objRequest = new HexaBPM__Service_Request__c(Id = srId);
            
            //v 2.0  changes for namerenewal in updateSRLines
            /*
for(HexaBPM__Service_Request__c SR : [select Id from HexaBPM__Service_Request__c where id = :srId
and HexaBPM__Customer__c = :accountId 
and HexaBPM__Internal_SR_Status__r.Name='Draft'])
{
IsAllowedToSubmit = true;
}
*/
            for(HexaBPM__Service_Request__c SR : [select Id,HexaBPM__Internal_SR_Status__r.Name from HexaBPM__Service_Request__c where id = :srId and HexaBPM__Customer__c = :accountId]){
                if(SR.HexaBPM__Internal_SR_Status__r.Name=='Draft'){
                    IsAllowedToSubmit = true;
                }else if(SR.HexaBPM__Internal_SR_Status__r.Name.equalsIgnoreCase('Submitted')){
                    IsAllowedToSubmit = true;
                    ChangeSRStatus = false;
                }   
            }
            if(IsAllowedToSubmit){
                
                //Updating the Price LineItems to "Blocked"
                list<HexaBPM__SR_Price_Item__c> srPriceItems = new list<HexaBPM__SR_Price_Item__c>();
                list<HexaBPM__SR_Price_Item__c> srPriceItemsTBCancelled = new list<HexaBPM__SR_Price_Item__c>();
                //v 2.0  changes for namerenewal in updateSRLines
                /* 
                for(HexaBPM__SR_Price_Item__c srPriceItem : [SELECT id,HexaBPM__Status__c,HexaBPM__Product__r.Name,Pricing_Line_Name__c,
                Total_Price_USD__c,Total_Price_AED__c,HexaBPM__Pricing_Line__r.Non_Deductible__c  
                FROM HexaBPM__SR_Price_Item__c 
                WHERE HexaBPM__ServiceRequest__c =: srId 
                //and HexaBPM__Pricing_Line__r.Non_Deductible__c=false
                and HexaBPM__Product__r.ProductCode != 'OB Name Reservation'
                and HexaBPM__Status__c='Added'])
                */
                //  v 2.0  changes for namerenewal in updateSRLines
                //  v 4.0 added Non_Deductible__c = flase condition in query
                for(HexaBPM__SR_Price_Item__c srPriceItem : [SELECT id,HexaBPM__Status__c,HexaBPM__Product__r.Name,Pricing_Line_Name__c,HexaBPM__Product__r.ProductCode,
                                                             Total_Price_USD__c,Total_Price_AED__c,HexaBPM__Pricing_Line__r.Non_Deductible__c  
                                                             FROM HexaBPM__SR_Price_Item__c 
                                                             WHERE HexaBPM__ServiceRequest__c =: srId 
                                                             //and HexaBPM__Product__r.ProductCode != 'OB Name Reservation'
                                                             and HexaBPM__Status__c='Added']){
                                                                 if(!srPriceItem.HexaBPM__Pricing_Line__r.Non_Deductible__c || srPriceItem.Total_Price_USD__c < 0){
                                                                     if(srPriceItem.Pricing_Line_Name__c!='Reservation of Name (Retail/Non-Retail)'){
                                                                         if(!srPriceItem.HexaBPM__Pricing_Line__r.Non_Deductible__c || srPriceItem.Pricing_Line_Name__c=='Self Registration Advance'){
                                                                             srPriceItem.HexaBPM__Status__c = 'Blocked';
                                                                         }else{
                                                                             srPriceItem.HexaBPM__Status__c = 'Cancelled';
                                                                         }
                                                                         srPriceItems.add(srPriceItem);
                                                                         productObj.lstPriceItem.add(srPriceItem);
                                                                         totalAmt = totalAmt + srPriceItem.Total_Price_USD__c;   
                                                                         totalAmtInAED = totalAmtInAED + srPriceItem.Total_Price_AED__c;
                                                                     }else{
                                                                         srPriceItem.HexaBPM__Status__c = 'Cancelled';
                                                                         srPriceItemsTBCancelled.add(srPriceItem);
                                                                     }
                                                                 }else if(srPriceItem.HexaBPM__Pricing_Line__r.Non_Deductible__c && srPriceItem.Total_Price_USD__c >= 0){
                                                                        srPriceItem.HexaBPM__Status__c = 'Cancelled';
                                                                        srPriceItemsTBCancelled.add(srPriceItem);
                                                                 }
                                                             }
                
                if(srPriceItems.size() > 0)
                    update srPriceItems;
                
                if(srPriceItemsTBCancelled.size()>0)
                    update srPriceItemsTBCancelled;
                productObj.total = totalAmt;
                productObj.totalInAED = totalAmtInAED;
                
                //Updating status to submitted only for the Draft SR's
                if(ChangeSRStatus){
                    for(HexaBPM__SR_Status__c status:[select id,Name from HexaBPM__SR_Status__c where (Name='Submitted' or Name= 'submitted') limit 1]){
                        objRequest.HexaBPM__Internal_SR_Status__c = status.id;
                        objRequest.HexaBPM__External_SR_Status__c = status.id;
                    }
                    objRequest.HexaBPM__Submitted_Date__c = system.today(); 
                    objRequest.HexaBPM__Submitted_DateTime__c = system.now(); 
                    objRequest.HexaBPM__FinalizeAmendmentFlg__c = true;
                    update objRequest;
                }
            }
            OB_NamingConventionHelper.setPageTrackerAfterPayment(srId,'ob-reviewandconfirm');
            isSuccess = true;
        }catch (DMLException e){
            productObj.isError = true;
            string DMLError = e.getdmlMessage(0) + '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
            productObj.errorMessage = DMLError;
            System.debug('=====>'+ e.getMessage());
            Database.rollback(Stat_svpoint);
        }
        
        return productObj;
    }
    /**********************************Wrapper classes*********************************************************/    
    public class ProductInfo{
        @AuraEnabled public decimal total{get;set;}
        @AuraEnabled public decimal totalInAED{get;set;}
        @AuraEnabled public list<HexaBPM__SR_Price_Item__c> lstPriceItem {get;set;}
        @AuraEnabled public string name {get;set;}
        @AuraEnabled public string accountName {get;set;}
        @AuraEnabled public boolean isError {get;set;}
        @AuraEnabled public string errorMessage {get;set;}
        public ProductInfo(){
            lstPriceItem = new list<HexaBPM__SR_Price_Item__c>();
            total = 0; //USD
            totalInAED = 0;
            isError = false;
        }
    }   
    
    public class RequestWrapper{
        @AuraEnabled public String amount                   {get;set;}
        @AuraEnabled public String paymentType              {get;set;}
        @AuraEnabled public String checkoutPaymentID        {get;set;}
        @AuraEnabled public String receiptStatus            {get;set;}
        @AuraEnabled public String cardType                 {get;set;}
        @AuraEnabled public String bankReferenceNumber      {get;set;}
        @AuraEnabled public String srId                     {get;set;}
        @AuraEnabled public String receiptId                     {get;set;}
    }
    public class Receipt{
        @AuraEnabled public String accountName                   {get;set;}
        @AuraEnabled public String receiptRef                   {get;set;}
    }
}