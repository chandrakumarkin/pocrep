/**
*Author : Merul Shah
*Description : This displays all the Flows based of configurable screen..
**/
public without sharing class OB_RegisterDIFCFlowController
{
    @AuraEnabled   
    public static ResponseWrapper  getFlowPages(String reqWrapPram)
    {
        system.debug('@@@@@@@@ reqWrapPram '+reqWrapPram);
        RequestWrapper reqWrap = (OB_RegisterDIFCFlowController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_RegisterDIFCFlowController.RequestWrapper.class);
        
        OB_RegisterDIFCFlowController.ResponseWrapper  respWrap = new OB_RegisterDIFCFlowController.ResponseWrapper();
        respWrap.pgFlwWrapLst = new List<OB_RegisterDIFCFlowController.PageFlowWrapper>();    
        
        Id flowId = reqWrap.flowId;
        Id pageId = reqWrap.pageId;
        String srid = reqWrap.srId;
        
        //Locking and Unlocking logic
        if(!String.isBlank(srId)) 
        {
          OB_LockAndUnlockSRHelper.RequestWrapper reqWarpLocUnLoc = new OB_LockAndUnlockSRHelper.RequestWrapper();
          reqWarpLocUnLoc.srId = srId;
          reqWarpLocUnLoc.currentCommunityPageId = pageId;
          reqWarpLocUnLoc.flowId = flowId;
          reqWarpLocUnLoc.communityReviewPageName = 'ob-reviewandconfirm';
          //system.debug('$$$$$$$$$ ' + OB_LockAndUnlockSRHelper.lockAndUnlockSRById(reqWarpLocUnLoc));
    
          OB_LockAndUnlockSRHelper.ResponseWrapper respWarpLocUnLoc = new OB_LockAndUnlockSRHelper.ResponseWrapper();
          respWarpLocUnLoc = OB_LockAndUnlockSRHelper.lockAndUnlockSRById(reqWarpLocUnLoc);
          respWrap.isSRLocked = respWarpLocUnLoc.isSRLocked;
          respWrap.communityReviewPageURL = respWarpLocUnLoc.communityReviewPageURL;
        }
       
       
        HexaBPM__Service_Request__c objrequest = new HexaBPM__Service_Request__c();
        
        for(HexaBPM__Page_Flow__c pgFlow : [SELECT Id,
                                                   Display_as_Index__c,
                                                   HexaBPM__Extension_Object__c,
                                                   HexaBPM__Flow_Description__c,
                                                   HexaBPM__Master_Object__c,
                                                   HexaBPM__Preview_Flow__c,
                                                   HexaBPM__Record_Type_API_Name__c,
                                                   Seq__c,
                                                   SLA_Display__c,
                                                   (SELECT id,Name,
                                                           Community_Page__c,
                                                           Completion_Weightage__c,
                                                           HexaBPM__Is_Custom_Component__c,
                                                           HexaBPM__Deployment_Code__c,
                                                           HexaBPM__Edit_Layout__c,
                                                           HexaBPM__Page_Description__c,
                                                           HexaBPM__Page_Flow__c,
                                                           HexaBPM__Page_Order__c,
                                                           HexaBPM__Preview_Page__c,
                                                           HexaBPM__Render_by_Default__c,
                                                           HexaBPM__Sys_Page_Id__c,
                                                           HexaBPM__What_Id__c 
                                                      FROM HexaBPM__Pages__r 
                                                      ORDER BY HexaBPM__Page_Order__c) 
                                              FROM HexaBPM__Page_Flow__c
                                             WHERE id =: flowId ])
        {
            
            OB_RegisterDIFCFlowController.PageFlowWrapper pgFlwWrap = new OB_RegisterDIFCFlowController.PageFlowWrapper();
            pgFlwWrap.pageFlowObj = pgFlow;
            
            List<OB_RegisterDIFCFlowController.PageWrapper> pgWrapLst = new List<OB_RegisterDIFCFlowController.PageWrapper>();
            for(HexaBPM__Page__c page : pgFlow.HexaBPM__Pages__r )
            {
                OB_RegisterDIFCFlowController.PageWrapper pgWrap = new OB_RegisterDIFCFlowController.PageWrapper();
           		pgWrap.pageObj = page;
                pgWrapLst.add(pgWrap);
                
            }
			pgFlwWrap.pages	= pgWrapLst;   
            respWrap.pgFlwWrapLst.add(pgFlwWrap);
        
        }
        return respWrap;
    
    }
    
    public class PageFlowWrapper {
        @AuraEnabled public List<PageWrapper> pages { get; set; }
        @AuraEnabled public String pageFlowLabel {get;set;}
        @AuraEnabled public Id pageFlowId { get; set; }
        @AuraEnabled public HexaBPM__Page_Flow__c pageFlowObj { get; set; }
        
        public PageFlowWrapper()
        {}
    }
    
    public class PageWrapper 
    {
        //@AuraEnabled public List<SectionWrapper> sections { get; set; }
        @AuraEnabled public Id pageId { get; set; }
        @AuraEnabled public String pageLabel {get;set;}
        @AuraEnabled public HexaBPM__Page__c pageObj { get; set; }
        @AuraEnabled public Boolean isDefaultRendered {get;set;} 
        public PageWrapper()
        {
        }
    }
    
    public class RequestWrapper 
    {
        @AuraEnabled public Id flowId { get; set; }
        @AuraEnabled public Id pageId {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public String fieldApi { get; set; }
        @AuraEnabled public String fieldVal { get; set; }
        @AuraEnabled public String cmdActionId { get; set; }
        @AuraEnabled public HexaBPM__Service_Request__c objRequest{get;set;}
        @AuraEnabled public Map<String,String> objRequestMap { get; set; }
      
        public RequestWrapper()
        {}
    }

    public class ResponseWrapper 
    {
        @AuraEnabled public List<OB_RegisterDIFCFlowController.PageFlowWrapper> pgFlwWrapLst {get;set;}
        @AuraEnabled public boolean isSRLocked { get; set; }
    	@AuraEnabled public string communityReviewPageURL { get; set; }
        
        public ResponseWrapper()
        {}
    }


}