/*
    Author      : Durga Prasad
    Date        : 22-Nov-2019
    Description : Custom code to update the Contact Passport details with SR 
    --------------------------------------------------------------------------------------
    v1.1          Leeba     19-03-2020     Code to copy the passport copy from Service Request to Contact
    v1.2          Abbas     13-02-2021     update valid gender value on Contact from SR (#13204 & #13205)
*/
global without sharing class CC_UpdateConvertedLeadSRContact implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
            try{
                string ContactId;
                for(User usr:[select Id,ContactId from User where Id=:userinfo.getUserId() and ContactId!=null]){
                    ContactId = usr.ContactId;
                }
                if(ContactId!=null){
                    Contact objcon = new Contact(Id=ContactId);
                    
                    if(stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c!=null){
                        objcon.MobilePhone = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;
                        objcon.Phone = stp.HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c;
                    }
                    
                    if(stp.HexaBPM__SR__r.Passport_No__c!=null)
                        objcon.Passport_No__c = stp.HexaBPM__SR__r.Passport_No__c;
                    if(stp.HexaBPM__SR__r.Nationality__c!=null)
                        objcon.Nationality__c = stp.HexaBPM__SR__r.Nationality__c;
                    if(stp.HexaBPM__SR__r.Date_of_Birth__c!=null)
                        objcon.Birthdate = stp.HexaBPM__SR__r.Date_of_Birth__c;
                    if(stp.HexaBPM__SR__r.Place_of_Birth__c!=null)
                        objcon.Place_of_Birth__c = stp.HexaBPM__SR__r.Place_of_Birth__c;
                    if(stp.HexaBPM__SR__r.Date_of_Issue__c!=null)
                        objcon.Date_of_Issue__c = stp.HexaBPM__SR__r.Date_of_Issue__c;
                    if(stp.HexaBPM__SR__r.Date_of_Expiry__c!=null)
                        objcon.Passport_Expiry_Date__c = stp.HexaBPM__SR__r.Date_of_Expiry__c;
                    if(stp.HexaBPM__SR__r.Place_of_Issuance__c!=null)
                        objcon.Place_of_Issue__c = stp.HexaBPM__SR__r.Place_of_Issuance__c;
                    
                    //v1.2
                    if(stp.HexaBPM__SR__r.Gender__c!=null){
                        //objcon.Gender__c = stp.HexaBPM__SR__r.Gender__c;
                        String srGender = '';
                        srGender = ( String.isNotBlank(stp.HexaBPM__SR__r.Gender__c) && stp.HexaBPM__SR__r.Gender__c == 'M' ? 'Male' : String.isNotBlank(stp.HexaBPM__SR__r.Gender__c) && stp.HexaBPM__SR__r.Gender__c == 'F' ? 'Female' : '');
                        System.debug('ZZZ CC_UpdateConvertedLeadSRContact.cls->EvaluateCustomCode_M-->gender-->'+srGender);
                        objcon.Gender__c = srGender;   
                    }

                    
                    
                    update objcon;
                    
                    //v1.1  
                    OB_AgentEntityUtils.setPassportFileUnderContact(objcon,stp.HexaBPM__SR__c);
                    
                }
            }catch(DMLException e){
                string DMLError = e.getdmlMessage(0)+'';
                if(DMLError==null){
                    DMLError = e.getMessage()+'';
                }
                strResult = DMLError;
            }
        }
        return strResult;
    }   
}