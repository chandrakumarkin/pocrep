/*********************************************************************************************************************
Author: Azfer Pervaiz
Purpose: Lite version of  CLS_CreatePRJSON_NanoController
-------------------------------------------------------------------------------------------------------------------------
Modification History 
----------------------------------------------------------------------------------------
V.No    Date           Updated By      Description
----------------------------------------------------------------------------------------              
V1.0    10-Feb-2019    Azfer           Created the Class.
V1.1    23-May-2019    Sai             Updated Class  
**********************************************************************************************************************/
@RestResource(urlMapping = '/DIFCClientDirectoryLiteAPI/*')
global class CLS_CreatePRJSON_NanoLiteController{

    @Httpget 
    global static void jsonResponse(){
    	
    	String rStartNumber ='';
    	String Mode = '';
    	if(!test.isRunningTest()){
    		RestContext.response.addHeader('Content-Type', 'application/json');
    	}
    	
    	
    	if(RestContext.request !=null && RestContext.request.params !=null){
    		
    		if(RestContext.request.params.get('Registration_Number') !=null && RestContext.request.params.get('Registration_Number') !=''){
    			rStartNumber = RestContext.request.params.get('Registration_Number');
    		}
    		if(RestContext.request.params.get('Mode') !=null && RestContext.request.params.get('Registration_Number') !='' && RestContext.request.params.get('Mode') == 'List'){
    			Mode = 'List';
    		}
    	}
    	
    	try{
    		RestContext.response.responseBody = Blob.valueOf(WayFindingJSONClass.jsonParser(rStartNumber,mode));
    	}
    	catch(Exception ex){
    		
    	}
    }
}