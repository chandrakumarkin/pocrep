global without sharing class GsEntryPermitUploadHandler {

Private static string  decryptedSAPPassWrd = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456' ;
@future(callout=true)
public static void EntryPermitStatusInSAP(String srId,String StepSeqNo,dateTime DocUploadedDtTime ){

 Service_request__c objSR =[select id,Name,SAP_SGUID__c,SAP_Unique_No__c,SAP_MATNR__c,SAP_UMATN__c from service_request__c where id=:srId limit 1];
        string result = 'Success';
        SAPGSWebServices.ZSF_S_GS_ACTIVITY item;
        list<SAPGSWebServices.ZSF_S_GS_ACTIVITY> items = new list<SAPGSWebServices.ZSF_S_GS_ACTIVITY>();
            
        item = new SAPGSWebServices.ZSF_S_GS_ACTIVITY();
        item.WSTYP = 'EPUP';
        item.SSTOP = 'X';
        item.RENUM = objSR.Name;//string.valueOf(objSRItem.Id).substring(0,15);
        item.SGUID = objSR.SAP_SGUID__c;
        item.UNQNO = objSR.SAP_Unique_No__c;
        //item.BUKRS = '5000';//objSRItem.Company_Code__c;        
        item.BUKRS = System.Label.CompanyCode5000;
        item.MATNR = objSR.SAP_MATNR__c;
        item.UMATN = objSR.SAP_UMATN__c;
        item.SEQNR = StepSeqNo;
        item.POSNR = '00010';
        item.ERNAM = WebService_Details__c.getAll().get('Credentials').SAP_User_Id__c;
        item.ERDAT = GsPendingInfo.SAPDateFormat(system.now());
        item.ERTIM = GsPendingInfo.SAPTimeFormat(system.now());
        
        item.DDATE = GsPendingInfo.SAPDateFormat(DocUploadedDtTime);
        item.DTIME = GsPendingInfo.SAPTimeFormat(DocUploadedDtTime);
        
        items.add(item);
        
        if(items != null && !items.isEmpty()){
            SAPGSWebServices.ZSF_TT_GS_ACTIVITY objActivityData = new SAPGSWebServices.ZSF_TT_GS_ACTIVITY();
            objActivityData.item = items;
            
            SAPGSWebServices.serv_actv objSFData = new SAPGSWebServices.serv_actv();        
            objSFData.timeout_x = 120000;
            objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
            objSFData.inputHttpHeaders_x= new Map<String, String>();
         // Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c);  Commented for V1.5
            Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+GsEntryPermitUploadHandler.decryptedSAPPassWrd); 
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
            
            system.debug('request $$$ : '+objActivityData);
            
            SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element response = objSFData.Z_SF_GS_ACTIVITY(objActivityData,null);
            
            list<Log__c> lstLogs = new list<Log__c>();
            
            system.debug('response $$$ : '+response);
            
          /* if(response.EX_GS_SERV_OUT != null && response.EX_GS_SERV_OUT.item != null){
                for(SAPGSWebServices.ZSF_S_GS_SERV_OP objStepRes : response.EX_GS_SERV_OUT.item){
                    if(objStepRes.ACCTP != 'P'){
                        result = 'Fail - '+objStepRes.SFMSG;
                        Log__c objLog = new Log__c();objLog.Type__c = 'Entry permit and cancellation  SR# '+objStepRes.RENUM;objLog.Description__c = objStepRes.SFMSG;lstLogs.add(objLog);
                    }
                }
            }
            if(!lstLogs.isEmpty())
                insert lstLogs; 
                */
        }
       
    }
}