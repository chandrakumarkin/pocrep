/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-21-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   10-21-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest(SeeAllData=false)
private class StepProcessBuilderProcessesTest {
    static testmethod void stepProcessBuilderStepNameTest()
    {
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.Menutext__c = 'New Employment Visa Package';
        testSrTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.SR_Group__c = 'GS';
        testSrTemplate.Active__c = true;
        insert testSrTemplate;
        
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('DIFC_Sponsorship_Visa_New');
        SR.Customer__c = objAccount.Id;
        SR.Mobile_Number__c ='+971500000000';
        insert SR;
        
        Status__c status = Test_CC_FitOutCustomCode_DataFactory.getTestStepStatus('Pending Review', 'Pending_Review');        
        insert status;
        
        Step_Template__c testStepTemplates = new Step_Template__c(Code__c = 'Medical Fitness Test Scheduled',Name='Medical Fitness Test Scheduled',Step_RecordType_API_Name__c = 'General');
        insert testStepTemplates;
        
        SR_Steps__c srstep = new SR_Steps__c();
        srstep.Step_Template__c = testStepTemplates.id;
        insert srstep;
        
        
        step__c step = Test_CC_FitOutCustomCode_DataFactory.getTestStep(SR.id, status.Id, objAccount.Name);
        step.Step_Template__c = testStepTemplates.Id;
        step.SR_Step__c = srstep.id;
        step.mobile_No__c ='+971500000000';
        insert step;
        
        
        Test.startTest();
            StepProcessBuilderProcesses.stepProcessBuilderStepName(new list<id>{step.id});
        Test.stopTest();
    }
}