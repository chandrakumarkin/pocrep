@isTest
public class CC_CreatePortalUserTest{
     public static testmethod void test1(){
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        //insertNewAccounts[0].Next_Renewal_Date__c = Date.today();
        insert insertNewAccounts;
        
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
            insert con; 
        
        License__c lic = new License__c(name='12345',Account__c=insertNewAccounts[0].id, License_Expiry_Date__c = Date.today());
        insert lic;
        
        insertNewAccounts[0].Active_License__c = lic.id;
        update insertNewAccounts;
        
        
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        
        
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'New_User_Registration','In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        //insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].Passport_No__c = 'A12345';
        insertNewSRs[0].Nationality__c = 'India';
        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
        insertNewSRs[0].Gender__c = 'Male';
        insertNewSRs[0].Place_of_Birth__c = 'Dubai';
        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
        insertNewSRs[0].Last_Name__c = 'test';
        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Business_Sector__c = 'Hotel';
        insertNewSRs[0].entity_name__c = 'Tentity';
         insertNewSRs[0].Gender__c = 'M';
        insertNewSRs[0].Progress_Completion__c = 10.0;
        insertNewSRs[0].HexaBPM__Contact__c = con.id;
       // insertNewSRs[0].Passport_Expiry_Date__c = Date.today().addmonths(20);
       // insertNewSRs[0].Place_of_Issue__c = 'Dubai';
        insert insertNewSRs;
        
         
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'REGISTRAR_REVIEW'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
        listSRSteps[0].HexaBPM__Step_No__c = 1;
        //listSRSteps[0].HexaBPM__Step_Template_Code__c = 'CLO_REVIEW';
        insert listSRSteps;
        
       
        
        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Awaiting Verification',HexaBPM__Code__c='AWAITING_VERIFICATION',HexaBPM__Type__c='Start');
        
        HexaBPM__Status__c status1 = new HexaBPM__Status__c
        (Name='Re-Upload Document',HexaBPM__Code__c='REUPLOAD_DOCUMENT',HexaBPM__Type__c='End');
        insert status1;
        
         List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
        insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        insertNewSteps[0].HexaBPM__Status__c = status.id;
       // insertNewSteps[0].Step_Template_Code__c = 'CLO_REVIEW';
        insertNewSteps[0].Relationship_Manager__c = userinfo.getUserId();
        insertNewSteps[1].Relationship_Manager__c = userinfo.getUserId();
        insert insertNewSteps;
        
      
        
        //----------------------------------------------------------------------------------------------------
        Test.startTest();
            HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c,HexaBPM__SR__r.Progress_Completion__c,HexaBPM__SR__r.HexaBPM__Contact__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[0].Id  LIMIT 1];
        
            HexaBPM__Step__c step1 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[1].Id  LIMIT 1];
        
            
        
       
            //-------------------------------------------------------------------------------------------------------
            
               
                insertNewSRs[1].entity_name__c ='test';
                insertNewSRs[1].Entity_Type__c = null;
                insertNewSRs[1].HexaBPM__Parent_SR__c = null;
                insertNewSRs[1].Last_Name__c = 'test';
                insertNewSRs[1].HexaBPM__Email__c = 'teset@test.com';
                //insertNewSRs[1].Entity_Type__c = 'Retail';
                update insertNewSRs;
                HexaBPM__Step__c step2 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[1].Id  LIMIT 1];
                
                CC_CreatePortalUser CC_CreatePortalUserObj = new CC_CreatePortalUser();
                CC_CreatePortalUserObj.EvaluateCustomCode(insertNewSRs[0], step);
                CC_CreatePortalUserObj.EvaluateCustomCode(insertNewSRs[0], step2); 
                
               
                insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('New_User_Registration').getRecordTypeId();
                insertNewSRs[1].Entity_Type__c = 'Financial - related';
                insertNewSRs[1].Business_Sector__c ='Investment Fund';
                update insertNewSRs;
               
                HexaBPM__Step__c step3 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[1].Id  LIMIT 1];
                
                CC_CreatePortalUserObj.EvaluateCustomCode(insertNewSRs[0], step3); 
            
        Test.stopTest();
    }
    public static testmethod void test2(){
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        //insertNewAccounts[0].Next_Renewal_Date__c = Date.today();
        insert insertNewAccounts;
        
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 
        
        License__c lic = new License__c(name='12345',Account__c=insertNewAccounts[0].id, License_Expiry_Date__c = Date.today());
        insert lic;
        
        insertNewAccounts[0].Active_License__c = lic.id;
        update insertNewAccounts;
        
        
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        
        
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].Passport_No__c = 'A12345';
        insertNewSRs[0].Nationality__c = 'India';
        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
        insertNewSRs[0].Gender__c = 'Male';
        insertNewSRs[0].Place_of_Birth__c = 'Dubai';
        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
        insertNewSRs[0].Last_Name__c = 'test';
        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Business_Sector__c = 'Hotel';
        insertNewSRs[0].entity_name__c = 'Tentity';
        insertNewSRs[0].HexaBPM__Customer__c = null;
        insertNewSRs[0].Progress_Completion__c = 10.0;
        insert insertNewSRs;
        
         
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'REGISTRAR_REVIEW'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
        listSRSteps[0].HexaBPM__Step_No__c = 1;
        //listSRSteps[0].HexaBPM__Step_Template_Code__c = 'CLO_REVIEW';
        insert listSRSteps;
        
       
        
        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Awaiting Verification',HexaBPM__Code__c='AWAITING_VERIFICATION',HexaBPM__Type__c='Start');
        
        HexaBPM__Status__c status1 = new HexaBPM__Status__c
        (Name='Re-Upload Document',HexaBPM__Code__c='REUPLOAD_DOCUMENT',HexaBPM__Type__c='End');
        insert status1;
        
         List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
        insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        insertNewSteps[0].HexaBPM__Status__c = status.id;
       // insertNewSteps[0].Step_Template_Code__c = 'CLO_REVIEW';
        insertNewSteps[0].Relationship_Manager__c = userinfo.getUserId();
        insertNewSteps[1].Relationship_Manager__c = userinfo.getUserId();
        insert insertNewSteps;
        
      
        // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'Passport Copy'});
        insert listDocMaster;
        
         List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        insert listSRTemplateDoc; 
        
        
        // create SR Doc
        List<HexaBPM__SR_Doc__c> listSRDoc = new List<HexaBPM__SR_Doc__c>();
        listSRDoc = OB_TestDataFactory.createSRDoc(1, insertNewSRs, listDocMaster, listSRTemplateDoc);
        listSRDoc[0].Name = 'Passport Copy';
        listSRDoc[0].File_Name__c = 'Passport Copy';
        listSRDoc[0].HexaBPM__Doc_ID__c = 'Passport Copy';
        insert listSRDoc;
        
        
        //----------------------------------------------------------------------------------------------------
        Test.startTest();
            HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.relationship_with_entity__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c,HexaBPM__SR__r.Progress_Completion__c,HexaBPM__SR__r.HexaBPM__Contact__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[0].Id  LIMIT 1];
        
            HexaBPM__Step__c step1 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.relationship_with_entity__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[1].Id  LIMIT 1];
        
            
        
       
            //-------------------------------------------------------------------------------------------------------
            
               
                insertNewSRs[1].entity_name__c ='test';
                insertNewSRs[1].Entity_Type__c = null;
                insertNewSRs[1].HexaBPM__Parent_SR__c = null;
                insertNewSRs[1].Last_Name__c = 'test';
                insertNewSRs[1].HexaBPM__Email__c = 'teset@test.com';
                //insertNewSRs[1].Entity_Type__c = 'Retail';
                update insertNewSRs;
                HexaBPM__Step__c step2 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[1].Id  LIMIT 1];
                
                CC_CreatePortalUser CC_CreatePortalUserObj = new CC_CreatePortalUser();
                CC_CreatePortalUserObj.EvaluateCustomCode(insertNewSRs[0], step);
                CC_CreatePortalUserObj.EvaluateCustomCode(insertNewSRs[0], step2); 
                
               
                insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('New_User_Registration').getRecordTypeId();
                insertNewSRs[1].Entity_Type__c = 'Financial - related';
                insertNewSRs[1].Business_Sector__c ='Investment Fund';
                update insertNewSRs;
               
                HexaBPM__Step__c step3 = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                     HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                     HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                     HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                     HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c
                                     from HexaBPM__Step__c where Id=:insertNewSteps[1].Id  LIMIT 1];
                
                CC_CreatePortalUserObj.EvaluateCustomCode(insertNewSRs[0], step3); 
                CC_CreatePortalUser.dummytest();
        Test.stopTest();
    }
}