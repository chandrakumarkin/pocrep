public class SRTriggerBL {
   
    
     @future
    public static void updateChildAddresses(Id requestIds) 
    {
        updateChangeOfAddressChildAccounts(requestIds);
    }
    
	public static void updateChangeOfAddressChildAccounts(Id requestIds){
        Map <Id,Service_Request__c> recordMap = new Map<Id,Service_Request__c>([SELECT Id,RecordTypeId,External_Status_Name__c,Customer__c,Customer__r.Hosting_Company__c from Service_Request__c where Id =:requestIds ]);
        Set<Id> approved_serviceIds = new Set<Id>();
        Map<Id,Id> serviceAccountMap = new Map<Id,Id>();
        Id coa_recordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Change of Address Details').getRecordTypeId();
        //Get all service request which are approved and belongs to recordtype = COA
        for(Id requestId : recordMap.keyset()){
            Service_Request__c request = recordMap.get(requestId);
            if(request.RecordTypeId==coa_recordTypeId &&
               request.External_Status_Name__c!=null && 
               request.External_Status_Name__c.tolowercase() == 'approved'){
                //Only if Service request is attached to Parent Account   
                if(request.Customer__r.Hosting_Company__c==null && request.Customer__c!=null)   
                serviceAccountMap.put(request.Id,request.Customer__c);
            }
        }
        
        if(serviceAccountMap.size() > 0){ 
            
            Map<Id,Account> parentAccountMap = new Map<Id,Account>([SELECT Id, PO_Box__c,Building_Name__c,Sys_Office__c,Street__c,City__c,Postal_Code__c,Country__c from Account where Id in:serviceAccountMap.values()]);
            Map<Id, List<Account>> childAccountMap = new Map<Id, List<Account>>();
            for (Account childaccount: [SELECT ID,Hosting_Company__c from Account where Hosting_Company__c in: serviceAccountMap.values()]){
                if(childAccountMap.get(childaccount.Hosting_Company__c)==null)
                    childAccountMap.put(childaccount.Hosting_Company__c,new List<Account>{childaccount});
                else
                    childAccountMap.get(childaccount.Hosting_Company__c).add(childaccount);
            }
            
            List<Account> updateChildAccountList = new List<Account>();
            for(Id requestId:serviceAccountMap.keyset()){
                string parentCustomer = serviceAccountMap.get(requestId);
                if(childAccountMap.get(parentCustomer)!=null){
                    for(Account childaccount: childAccountMap.get(parentCustomer)){
                        childaccount.PO_Box__c = parentAccountMap.get(parentCustomer).PO_Box__c;
                        Childaccount.Building_Name__c=parentAccountMap.get(parentCustomer).Building_Name__c;
                         Childaccount.Sys_Office__c=parentAccountMap.get(parentCustomer).Sys_Office__c;
                         Childaccount.Street__c=parentAccountMap.get(parentCustomer).Street__c;
                         Childaccount.City__c=parentAccountMap.get(parentCustomer).City__c;
                         Childaccount.Postal_Code__c=parentAccountMap.get(parentCustomer).Postal_Code__c;
                        Childaccount.Country__c=parentAccountMap.get(parentCustomer).Country__c;
                        updateChildAccountList.add(childaccount);
                    }
                }
            }
            if(updateChildAccountList.size()>0)
                update updateChildAccountList;
        }
        
    }
}