@isTest
global class NorBlocIntegrationControllerMockTest implements HttpCalloutMock {

    global HTTPResponse respond(HTTPRequest req){
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"access_token":"7d6619723480f33bd5b8f2b3fcf529c6ffb191d77625d4fca77529f32cae4d8f","token_type":"Bearer","expires_in":600,"scope":"GET POST PUT","refresh_token":"1313bf746c105dc99fc1af1414fdeb341a7c439e58b0e23c5f3ecdb9945897e"}');
        res.setStatusCode(200);
        return res;
	}
}