/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 06-09-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   05-20-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public without sharing class UserUploadFormController {
    @AuraEnabled
    public static string SaveFile(Id parentId, String fileName, String base64Data, String contentType) {

        SR_Doc__c SRDoc = new SR_Doc__c(Name = 'Authorized Signatory Form	', Service_Request__c = parentId,Status__c='Uploaded');
        insert SRDoc; 

        base64Data                  = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment attach           = new Attachment();
        attach.parentId             = SRDoc.Id;
        attach.Body                 = EncodingUtil.base64Decode(base64Data);
        attach.Name                 = fileName;
        attach.ContentType          = contentType;

        try{

             if(!test.isRunningTest()) insert attach;
             return 'success';
        }
        catch(exception ex){
             return 'Error';
        }
    }
}