/*
        Author      :   Shabbir
        Description :   This class is used to pass the push lead/account into Operate system
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    20-06-2018  Shabbir         Created
    v1.1    16-08-2018  selva           Assembla #5639 - added account Id
    V1.2    26-08-2018  Arun            Added Code for contact push      
*/    
public without sharing class OperateIntegrationCls {
        
    public static string opeateLogin() {
        
        string returnMessage = '';
        string clientId = 'kUGEjJoemUgqdE5k';
        string clientSecret = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(Operate_Details__c.getAll().get('Credentials').Client_Secret__c) :'123456789'; 
       // string username = test.isrunningTest()==false ? EncodingUtil.urlEncode(Operate_Details__c.getAll().get('Credentials').User_Name__c, 'UTF-8') : 'dfdf';
       // string password = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(Operate_Details__c.getAll().get('Credentials').Password__c) :'1234567890';
      //  password = EncodingUtil.urlEncode(password, 'UTF-8');
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        //String reqUrl = Label.Operate_Login_URL;
        
        String reqUrl = 'https://identity.officernd.com/oauth/token';
        
        request.setEndpoint(reqUrl);
        request.setMethod('POST');
        request.setHeader('Content-Type','application/x-www-form-urlencoded');
    
        String postBody = 'grant_type=client_credentials'
                + '&client_id=' + clientId
                + '&client_secret=' + clientSecret
                + '&scope=officernd.api.read officernd.api.write';
                
                
        try{
            request.setBody(postBody);
            String access_token = '';
            if(!Test.isRunningTest()){
                System.HttpResponse response = new System.Http().send(request);
                system.debug('Response ' +response.getBody());
                map<String, String> values = (map<String, String>) JSON.deserialize(response.getBody(), map<String, String>.class);
                access_token = values.get('access_token');
            }else{
                access_token = 'dfdf';
            }
            returnMessage = access_token;

        } catch (Exception e) {
            insert LogDetails.CreateLog(null, 'OperateIntegrationCls/opeateLogin', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
            
        }
        return returnMessage;
    }
    
    @Future(callout=true)
    public static void pushSFLeadtoOperate(list<Id> listLeadIds) {
        
       
        
            
    }
    

    @Future(callout=true)
    public static void pushSFAccounttoOperate(list<Id> listAccountIds) {
        
        if(!listAccountIds.isEmpty()){
            string authorizationToken = OperateIntegrationCls.opeateLogin();
            system.debug('authorizationToken ' + authorizationToken);
            if(string.isNotBlank(authorizationToken))
            {
                
                list<Account> lstAccount = new list<Account>([select Id, Name, BP_No__c, Lead_Id__c,Fintech_Email__c,
                BD_Sector__c,Company_Type__c from Account where Id =:listAccountIds ]);
                
                list<Lead> lstLead = new list<Lead>([Select Id from Lead where ConvertedAccountId=:listAccountIds ]);
                
                if(!lstAccount.isEmpty())
                {  
                    JSONGenerator jsn = JSON.createGenerator(true);
                    jsn.writeStartObject();
                    
                    jsn.writeStringField('name',lstAccount[0].Name);
                    
                    jsn.writeStringField('url','www.difc.ae');
                    jsn.writeStringField('description','aa');
                    jsn.writeStringField('organization','5d0ddb6f18d6cb0113e4af5a');
                    jsn.writeBooleanField('isTeam',true);
                     if(string.isNotBlank(lstAccount[0].Fintech_Email__c))
                        jsn.writeStringField('email',lstAccount[0].Fintech_Email__c);                
                    else
                        jsn.writeStringField('email','SF@difc.ae');                
                    
                    jsn.writeFieldName('properties');
                    jsn.writeStartObject();
                    jsn.writeObjectField('BPNumber', lstAccount[0].BP_No__c);
                    jsn.writeObjectField('AccountIdSF', lstAccount[0].Id);
                    jsn.writeEndObject();


                     
                    
                    jsn.writeEndObject();
                    
                   // Http http = new Http();
                    HttpRequest request = new HttpRequest();
                    String reqUrl = 'https://app.officernd.com/api/v1/organizations/fintechhive/teams';
                    request.setEndpoint(reqUrl);
                    request.setMethod('POST');
                    request.setHeader('Authorization','Bearer '+authorizationToken);
                    request.setHeader('cache-control', 'no-cache');
                    request.setHeader('scope', 'officernd.api.write');
                    request.setHeader('content-type', 'application/json');
                    
                    string data =jsn.getAsString();
                    
                    System.debug('data==>'+data);
                    System.debug('authorizationToken==>'+authorizationToken);
                    
                    string responseBody = '';
                    request.setBody(data);
                    
                      if(!Test.isRunningTest()){
                      
                    System.HttpResponse response1 = new System.Http().send(request); 
                       responseBody = response1.getBody();
                   }
                   else
                   {
                   responseBody ='[{"tags":[],"details":{},"calculatedStatus":"inactive","calculatedStage":"","_id":"5eadba84310113012427ff24","name":"Daman Real Estate Capital Partners Limited","url":"www.difc.ae","description":"aa","organization":"5d0ddb6f18d6cb0113e4af5a","email":"sf@difc.ae","properties":{"BPNumber":"0000415923","AccountIdSF":"0012000001H3yr5AAB"},"office":"5de8fb16e6aca0056595c298","twitterInfo":{"description":"","imageUrl":null},"image":null,"createdAt":"2020-05-02T18:23:00.149Z","modifiedAt":"2020-05-02T18:23:00.149Z","createdBy":"5ea13840b8fd3900696119dc","modifiedBy":"5ea13840b8fd3900696119dc","billing":[],"isTeam":true}]';
                   }
                   
                   system.debug('account push response '+responseBody);
                    String accountIdToUpdate= responseBody.substring(responseBody.indexOf('"_id":"')+7,responseBody.indexOf('"_id":"')+31);
                    System.debug('accountIdToUpdate==>'+accountIdToUpdate);

                    
                    
 //V1.2    
                  List<contact>  ListContact=[select firstname,Name,LastName,email,Phone,Title,RecordTypeId from contact where  Accountid=:lstAccount[0].id and 
                  FinTech_Contact__c=true limit 1];
                 if(!ListContact.isEmpty() && string.isNotBlank(accountIdToUpdate))   
                 {
                     //Start for Push contact 
                    
                    JSONGenerator jsncontact = JSON.createGenerator(true);
                    jsncontact.writeStartObject();
                    
                     if(string.isNotBlank(ListContact[0].Name))
                        jsncontact.writeStringField('name',ListContact[0].Name);
                     if(string.isNotBlank(ListContact[0].email))
                        jsncontact.writeStringField('email',ListContact[0].email);
                    jsncontact.writeStringField('team',accountIdToUpdate);
                   
                    
                     
                    jsncontact.writeEndObject();
                    
                    //Http httpContact = new Http();
                    HttpRequest requestContact = new HttpRequest();
                    String reqUrlContact = 'https://app.officernd.com/api/v1/organizations/fintechhive/members';
                    requestContact.setEndpoint(reqUrlContact);
                    requestContact.setMethod('POST');
                    requestContact.setHeader('Authorization','Bearer '+authorizationToken);
                    requestContact.setHeader('cache-control', 'no-cache');
                    requestContact.setHeader('scope', 'officernd.api.write');
                    requestContact.setHeader('content-type', 'application/json');
                    
                    System.debug('===jsncontact=>'+jsncontact.getAsString());
                    
                    string dataContact = jsncontact.getAsString();
                    string responseBodyContact = '';
                    requestContact.setBody(dataContact);
                    if(!Test.isRunningTest())
                    {
                        System.HttpResponse response = new System.http().send(requestContact); 
                        responseBodyContact = response.getBody();
                    }else{
                        responseBodyContact = '{"response":{"id":"94d2597b-78a1-402e-8772-b8630aa8d334","accounts":[{"accountid":"1051","outputvalue_webtolead":"False"}]}}';
                    }
                    system.debug('Contact push response '+responseBodyContact);

                }
                    //End Push Contact 
                    
 //End V1.2                    
                       if(string.isNotBlank(accountIdToUpdate))
                    {
                        Account objAccount = new Account(Id = lstAccount[0].Id);
                        objAccount.Operate_Account_Id__c = accountIdToUpdate;
                        update objAccount;                
                    }
                    
                                 
                }
                
            }
        }
            
    }    

}