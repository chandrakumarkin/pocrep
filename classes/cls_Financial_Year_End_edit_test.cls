@isTest
public class cls_Financial_Year_End_edit_test {
    private static testmethod  void testme(){
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
                
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = '31st December';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Custoer 2';
        objAccount1.E_mail__c = 'test@test.com';
        objAccount1.BP_No__c = '001235';
        objAccount1.Company_Type__c = 'Retail';
        objAccount1.Sector_Classification__c = 'Retail';
        objAccount1.Legal_Type_of_Entity__c = 'LTD';
        objAccount1.ROC_Status__c = 'Active';
        insert objAccount1;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
           
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'Submitted';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);  
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'Approved';
        lstSRStatus.add(objSRStatus);
             
        insert lstSRStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordTypeIds.get('License_Renewal');
        //objSR.Building__c = objBuilding.Id;
        //objSR.Unit__c = lstUnits[0].Id;
        objSR.Service_Category__c = 'New';
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        objSR.Financial_Year_End_mm_dd__c ='31st December';
        insert objSR;
        Relationship__c relBus = new Relationship__c();
        relBus.Object_Contact__c = objContact.id;
        relBus.Subject_Account__c =  objAccount.Id;
        //relBus.Relationship_Type__c = 'Beneficiary Owner';
        relBus.Relationship_Type__c ='Is Shareholder Of';
        relBus.Active__c= true;
        insert relBus;
        
        Attachment ObjAttachment = new Attachment();
        ObjAttachment.name ='test';
        ObjAttachment.Body = Blob.valueOf('test body');
        ObjAttachment.ParentId = objSR.id;
        insert ObjAttachment;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Sample_Test_RecType';
        objTemplate.SR_RecordType_API_Name__c = 'Sample_Test_RecType';
        objTemplate.ORA_Process_Identification__c = 'Sample_Test_RecType';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Board Resolution';
        objDocMaster.Code__c = 'Board Resolution';
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;        
        objTempDocs.Generate_Document__c = true;
        insert objTempDocs;
        
        test.startTest();
            Apexpages.currentPage().getParameters().put('RecordType',mapRecordTypeIds.get('License_Renewal'));
        	Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
        	cls_Financial_Year_End_edit cont = new cls_Financial_Year_End_edit(con);
        	cont.ObjAttachment = ObjAttachment;
        	cont.SaveConfirmation();
        	//cont.AnnualTurnover();
        	//cont.IsFEY_Provided = true;
        Test.stopTest();
    }
    private static testmethod  void testme1(){
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
                
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = '31st December';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Custoer 2';
        objAccount1.E_mail__c = 'test@test.com';
        objAccount1.BP_No__c = '001235';
        objAccount1.Company_Type__c = 'Retail';
        objAccount1.Sector_Classification__c = 'Retail';
        objAccount1.Legal_Type_of_Entity__c = 'LTD';
        objAccount1.ROC_Status__c = 'Active';
        insert objAccount1;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
           
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'Submitted';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);  
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'Approved';
        lstSRStatus.add(objSRStatus);
             
        insert lstSRStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordTypeIds.get('License_Renewal');
        //objSR.Building__c = objBuilding.Id;
        //objSR.Unit__c = lstUnits[0].Id;
        objSR.Service_Category__c = 'New';
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        //insert objSR;
        Relationship__c relBus = new Relationship__c();
        relBus.Object_Contact__c = objContact.id;
        relBus.Subject_Account__c =  objAccount.Id;
        //relBus.Relationship_Type__c = 'Beneficiary Owner';
        relBus.Relationship_Type__c ='Is Shareholder Of';
        relBus.Active__c= true;
        insert relBus;
        test.startTest();
        	Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
        	cls_Financial_Year_End_edit cont = new cls_Financial_Year_End_edit(con);
        cont.SaveConfirmation();
        Test.stopTest();
    }
}