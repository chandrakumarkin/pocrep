/*****************************************************************************************************************************
Author      :   Swati Sehrawat
Date        :   3rd May 2016
Description :   This class contains the Custom codes for Fine Process
--------------------------------------------------------------------------------------------------------------------------
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date            Updated By    Description
V1.0    11th Aug 2016   Swati           Ticket 3159
V1.1    15/01/2017      Sravan          Added filter to update SR dates only if there is no compliance linked to the SR.
V1.2    13/06/2019      Sai             Logic updated in “AllowSRSubmit” method to allow user to submit the SR for UBO fine
V1.3    04/July/2019    Sai             #6901
V1.4    07/July/2019    Sai             #7082
v1.5    28/july/2019    Sai             #7145
V1.6    05/Oct/2020     Arun            #11507 Changed for other type of fines 
v1.7    29/Dec/2020     Arun            #13045 No change Dates for  Preliminary Notice of Fines and Decision Notice 
v1.8    12/July/2021    Utkarsh         #16969 Added the PN/DN changes method 
--------------------------------------------------------------------------------------------------------------------------             
*****************************************************************************************************************************/

public without sharing class CC_FineProcess {
    
    public static string uploadSignedDocument(Step__c stp){
        string status = 'Success';
        if(stp != null){
            try{
                string strDocuments = '';
                for(SR_Doc__c srdoc:[select id,Doc_ID__c,SR_Template_Doc__c,Document_Name__c from SR_Doc__c where (SR_Template_Doc__r.Document_Master_Code__c=:'Fine Generated/Signed Document' OR SR_Template_Doc__r.Document_Master_Code__c=:'No Objection letter Generated/Signed') and Doc_ID__c=null and Service_Request__c =: stp.SR__c and Is_Not_Required__c=false]){
                    if(strDocuments=='')
                        strDocuments = SRDOC.id;
                    else
                        strDocuments = strDocuments +' , '+SRDOC.id;
                }
                if(strDocuments!=null && strDocuments!=''){
                    Status = 'Please upload Signed copy Generated Document in order to proceed.';
                }
            }catch(Exception e){
                insert LogDetails.CreateLog(null, 'CC_FineProcess/uploadSignedDocument', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
                return e.getMessage();
            }
        }
        return status;
    }
    
    public static string AllowSRSubmit (service_request__c objSR){
        
        
        try{
            string status = 'Success';
            if(objSR!=null){
                service_request__c SRRequest = new service_request__c();
                SRRequest = [SELECT Record_Type_Name__c,customer__c FROM service_request__c  where ID=:objSR.ID];
                
                for(service_request__c eachRequest:[select id,Type_of_Request__c,Record_Type_Name__c from service_request__c where customer__c =: objSR.customer__c and record_type_name__c=:'Issue_Fine' and External_Status_Name__c=:'Issued'])
                    
                {
                    System.debug('eachRequest--'+eachRequest);
                    if(eachRequest.Type_of_Request__c == 'UBO' &&
                       (SRRequest.Record_Type_Name__c == 'Change_Add_or_Remove_Beneficial_or_Ultimate_Owner' || 
                        SRRequest.Record_Type_Name__c == 'Full_Exempted_UBO_Regulations'))
                    {   // V1.5
                        status = 'The entity has an outstanding fine (s). You must pay the fine before you can submit this service request.';
                    }  
                    else if(SRRequest.Record_Type_Name__c == 'License_Renewal')
                    { //V1.4
                        if(eachRequest.Type_of_Request__c == 'Inactive Lease' ||
                           eachRequest.Type_of_Request__c == 'File Audited Accounts'||
                           eachRequest.Type_of_Request__c == 'Filing of audited accounts and annual directors' )
                            status = 'The entity has an outstanding fine (s). You must pay the fine before you can submit this service request.'; 
                        
                    }
                    else
                    {
                        status = 'The entity has an outstanding fine (s). You must pay the fine before you can submit this service request.'; 
                    }
                    
                    System.debug('status---->>'+status);      
                }
                
                
                if(SRRequest.Record_Type_Name__c == 'License_Renewal' && status =='Success')
                { 
                    List<Compliance__c> ListComp=[select Id,Name from Compliance__c where Status__c='Defaulted' and (Name='Filing of audited accounts and annual directors' 
                                                                                                                     OR Name='File Audited Accounts') and Exception_Date__c>=LAST_90_DAYS and Account__c=:SRRequest.customer__c];
                    
                    if(!ListComp.isEmpty())
                    {
                        status = 'You must complete the filing requirements as per the compliance (s) before you can submit this service request. ';
                        /*  
for(Compliance__c ObjCom:ListComp)
{
status+=' '+ObjCom.Name+'; \r\n ';
}
*/
                    }
                    
                }
                
            }
            System.debug('objSR---->>'+objSR);
            
            
            // System.assertEquals('status' , status);
            return status;  
        }
        catch(Exception ex){
            return null;
        }
        
    }
    
    public static string updateFineSRtoWaived(Step__c stp){
        string status = 'Success';
        if(stp != null){
            try{
                list<service_request__c> tempListOfSR = [select id,compliance__c from service_request__c where id =:stp.SR__r.linked_SR__c and record_type_name__c=:'Issue_Fine'];  
                system.debug('---tempListOfSR--'+tempListOfSR);
                if(tempListOfSR!=null && !tempListOfSR.isEmpty()){
                    List<SR_Status__c> waivedStatus = [select id,name from SR_Status__c where Name='Waived' limit 1];   
                    tempListOfSR[0].Internal_SR_Status__c = waivedStatus[0].id;   tempListOfSR[0].External_SR_Status__c = waivedStatus[0].id; update tempListOfSR;
                    
                    list<Compliance__c> tempListCompliance = [select id, Objection_Approved__c from Compliance__c where id=:tempListOfSR[0].compliance__c];
                    if(tempListCompliance!=null && !tempListCompliance.isEmpty()){
                        tempListCompliance[0].Objection_Approved__c = true;
                        update tempListCompliance;
                    }
                    
                    list<SR_Price_Item__c> tempPriceItemList = [select id, ServiceRequest__c, Status__c from SR_Price_Item__c where ServiceRequest__c=:tempListOfSR[0].id];
                    if(tempPriceItemList!=null && !tempPriceItemList.isEmpty()){
                        for(SR_Price_Item__c tempObj : tempPriceItemList){
                            tempObj.Status__c = 'Waived';   
                        }
                        update  tempPriceItemList;
                    }
                }
            }catch(Exception e){
                insert LogDetails.CreateLog(null, 'CC_FineProcess/updateFineSRtoWaived', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
                return e.getMessage();
            }
        }
        return status;
    }
    
    public static string sendMailforOtherFine(Step__c stp){
        //List<Contact> relatedContactList = new List<Contact>();
        system.debug('stp.SR__r.customer__r.ownerId::'+stp.SR__r.customer__c);
        string status = 'Success';
        if(stp != null){
            try{
                list<service_request__c> tempListOfSR = [select id,submitted_date__c,Type_of_Request__c,date__c,end_date__c,Date_of_written_notice__c from service_request__c where id =:stp.SR__c and Compliance__c =null];  //   V1.1
                if(tempListOfSR!=null && !tempListOfSR.isEmpty()){
                    
                    //1.7 V
                    if(tempListOfSR[0].Type_of_Request__c=='Preliminary Notice of Fines' || tempListOfSR[0].Type_of_Request__c=='Decision Notice of Fines')
                    {
                        tempListOfSR[0].Date__c = tempListOfSR[0].submitted_date__c+15;
                    }                       
                    else
                    {
                        tempListOfSR[0].Date_of_Written_Notice__c = tempListOfSR[0].submitted_date__c; tempListOfSR[0].Date__c = tempListOfSR[0].submitted_date__c+15;tempListOfSR[0].End_Date__c = tempListOfSR[0].submitted_date__c+30; 
                        update tempListOfSR;                    
                    }
                }
                List<String> emailList = new List<String>();
                //List<String> relatedContactEmailList = new List<String>();
                list<id> accountIdlist = new list<id>();
                //list<String> rmEmailList = new List<String>();
                map<string,string> mapUserEmails = new map<string,string>();
                if(stp.SR__r.customer__c!=null){
                    //relatedContactList = CC_ROCCodecls.CompanyServicesPortalContacts(stp.SR__r.customer__c, 'ROC');
                    accountIdlist.add(stp.SR__r.customer__c); 
                }  
                /*if(stp.SR__r.customer__c!=null){
rmEmailList.add(stp.SR__r.customer__r.owner.email); 
}*/
                if(accountIdlist!=null && !accountIdlist.isEmpty()){
                    
                    Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
                    /*for(User objUser : [select Id,Email,ContactId from User where IsActive=true AND Contact.AccountId IN : accountIdlist AND Community_User_Role__c INCLUDES ('Company Services') AND contact.recordTypeId=:portalUserId]){
                        mapUserEmails.put(objUser.ContactId,objUser.Email);
                    }*/
                       for(Contact objUser :CC_ROCCodecls.CompanyServicesPortalContacts(stp.SR__r.customer__c, 'ROC'))
                       {
                      	  mapUserEmails.put(objUser.id,objUser.Email);
                        }
                    for(Account acc:[SELECT Id, Owner.Email from Account where Id=:accountIdlist]){
                        emailList.add(acc.Owner.Email);
                    }
                }
                system.debug('---mapUserEmails---'+mapUserEmails);
                system.debug('---emailList---'+emailList);
                
                if(mapUserEmails!=null && !mapUserEmails.isEmpty()){
                    OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address = 'portal@difc.ae'];
                    string strTemplateId;
                    
                    if(stp.SR__r.type_of_request__c == 'Other')
                    {
                        for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='Notification_of_Other_Fines'])
                            strTemplateId = objET.Id;
                    }
                    // V1.6   
                    else if(stp.SR__r.type_of_request__c == 'Preliminary Notice of Fines' || stp.SR__r.type_of_request__c == 'Decision Notice of Fines')
                    {
                        for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='Notification_of_notice'])  strTemplateId = objET.Id;
                    }
                    else if(stp.SR__r.type_of_request__c == 'UBO' && stp.Step_Status__c =='Document Generated'){
                        for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='Notification_of_Other_Fines_UBO'])
                            strTemplateId = objET.Id;
                    }
                    else if(stp.SR__r.type_of_request__c == 'UBO' && stp.Step_Status__c =='Document Ready'){ //V1.3
                        for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='UBO_fine_Notification'])
                            strTemplateId = objET.Id;
                    }
                    else if(stp.SR__r.type_of_request__c == 'Inactive Lease' && stp.Step_Status__c =='Document Generated'){
                        for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='Notification_of_Other_Fines_Inactive_Lease'])
                            strTemplateId = objET.Id;
                    }
                    else if(stp.SR__r.type_of_request__c == 'Inactive Lease' && stp.Step_Status__c =='Document Ready'){ //V1.3
                        for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='InactiveLease_fine_Notification'])
                            strTemplateId = objET.Id;
                    }
                    else if(stp.SR__r.type_of_request__c == 'Notification Renewal'){
                        for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='Document_Ready_DP_Client'])
                            strTemplateId = objET.Id;
                    }
                    else if(stp.SR__r.type_of_request__c != 'Notification Renewal' && stp.SR__r.type_of_request__c != 'Other'){
                        for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='Document_Ready_Client'])
                            strTemplateId = objET.Id;
                    }
                    
                    list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                    
                    for(string UserId : mapUserEmails.keySet()){
                        mail = new Messaging.SingleEmailMessage();
                        mail.setReplyTo('portal@difc.ae');
                        if(strTemplateId!=null && strTemplateId!=''){
                            mail.setWhatid(stp.SR__c);
                            mail.setTemplateId(strTemplateId);  
                            mail.setSaveAsActivity(false);
                            if(lstOrgEA!=null && lstOrgEA.size()>0)
                                mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
                            mail.setTargetObjectId(UserId);
                            if(!emailList.isEmpty()){
                                mail.setToAddresses(emailList);
                            }
                            if(stp.SR__r.customer__c != null){
                                //mail.setTargetObjectId(stp.SR__r.customer__r.ownerId);
                            }
                            mails.add(mail);
                        }  
                    } 
                    
                    system.debug('---mapUserEmails---'+mails);
                    system.debug('---mails---'+mails);
                    if(mails != null && !mails.isEmpty()){
                        if(!test.isRunningTest()){
                            Messaging.sendEmail(mails);
                        }
                    }
                }   
            }catch(Exception e){
                system.debug('---1---'+e.getMessage());
                system.debug('---2---'+e.getLineNumber());
                insert LogDetails.CreateLog(null, 'CC_FineProcess/uploadSignedDocument', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
                return e.getMessage();
            }
        }
        return status;
    }
    
    public static string updateObjectionRaised (Step__c stp){
        string status = 'Success';
        if(stp!=null && stp.SR__c!=null && stp.SR__r.Compliance__c!=null){
            list<compliance__c> tempList = [select id, objection_raised__c from compliance__c where id=:stp.SR__r.Compliance__c];
            if(tempList!=null && !tempList.isEmpty()){
                tempList[0].objection_raised__c = true;
                update tempList;
            }   
        }
        return status;  
    }
//v1.8 Start
    public static string updateIssueFineStatus(String SrId){
        string status = IssueFineObjectionWithdraw.updateIssueFine(SrId);
        return status;
    }
    
    public static string withdrawIssueFineStatus(String SrId){
        string status = IssueFineObjectionWithdraw.withdrawIssueFine(SrId);
        return status;
    }
    //v1.8 End
}