global class SearchAndReplaceAccounts implements Database.Batchable < sObject > {

    global final String Query;
    global List<sObject> passedRecords= new List<sObject>();

    global SearchAndReplaceAccounts(String q,List<sObject> listofRecords) {

        Query = q;
        passedRecords = listofRecords;
        system.debug('Query---'+Query);
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {

        if(query != '')return Database.getQueryLocator(query);
        else return DataBase.getQueryLocator([select id,Phone,Mobile_Number__c,Previous_Registered_Phone__c,Residence_Phone__c,Work_Phone__c,E_mail__c,Email_Address__c,Email_Address_to_send_Portal_Link__c,Fintech_Email__c,Initial_Contact_Email__c from Account WHERE Id IN: passedRecords and Id != null]);
    }

    global void execute(Database.BatchableContext BC, List < Account > scope) {
        
        for (Account ObjContact: scope) {

            ObjContact.Phone = '+971569803616';
            ObjContact.Mobile_Number__c = '+971569803616';
            ObjContact.Previous_Registered_Phone__c = '+971569803616';
            ObjContact.Residence_Phone__c = '+971569803616';
            ObjContact.Work_Phone__c = '+971569803616';

            ObjContact.E_mail__c = 'test@difc.ae.invalid';
            ObjContact.Email_Address__c = 'test@difc.ae.invalid';
            ObjContact.Email_Address_to_send_Portal_Link__c = 'test@difc.ae.invalid';
            ObjContact.Fintech_Email__c = 'test@difc.ae.invalid';
            ObjContact.Initial_Contact_Email__c = 'test@difc.ae.invalid';

        }
        system.debug('scope---'+scope);
        
        Database.SaveResult[] srList = Database.update(scope, false);
        List<Log__c> logRecords = new List<Log__c>();
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            }else {
                for(Database.Error err : sr.getErrors()) {
                    string errorresult = err.getMessage();
                    insert LogDetails.CreateLog(null, 'SearchAndReplaceAccounts', 'Exception is : '+errorresult+'=Account Id==>'+sr.getId());
                    logRecords.add(new Log__c(
                      Type__c = 'SearchAndReplaceAccounts',
                      Description__c = 'Exception is : '+errorresult+'=Contact Id==>'+sr.getId()
                    ));
                    System.debug('The following error has occurred.');                    
                }
            }
        }
        if(logRecords.size()>0)insert logRecords;
    }

    global void finish(Database.BatchableContext BC) {}
}