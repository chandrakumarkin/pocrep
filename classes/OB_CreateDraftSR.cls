/******************************************************************************************
 *  Author      : Rajil Ravindran
 *  Date        : 19-Jan-2020
 *  Description : Controller to create SR - Complete Profile SR and AOR Application - invoked from
                  process builder : Create draft SR for lead conversion and contact creation
    V 1.0   Merul	populating the contact on SR if contact(OB_Block_Homepage__c) is checked
    v 1.1   Durga	Code to populate Opportunity on Creation of the Application  
 ********************************************************************************************/
public without sharing class OB_CreateDraftSR {
    
    @InvocableMethod
    public static void createSR(List<Contact> lstContact){
        list<HexaBPM__Service_Request__c> lstSR = new list<HexaBPM__Service_Request__c>();
        list<string> lstContactId = new list<string>();
        string lookupValue;
        Contact contactRecord;
        list<string> lstLookUpField = new list<string>();
        map<string,Contact> mapContact = new map<string,Contact>();
        map<string,boolean> mapCommercialPermission = new map<string,boolean>();
        String commaSepratedFields = '';
        string soql;

        for(Contact contObj : lstContact)
            lstContactId.add(contObj.id);
		
		set<string> setQueryFields = new set<string>();
		for(OB_Complete_Profile_Map__c settingObj : OB_Complete_Profile_Map__c.getAll().values()) {
            if(settingObj.SourceObjectField__c!=null)
            	setQueryFields.add(settingObj.SourceObjectField__c.toLowerCase());
        }
		setQueryFields.add('account.is_commercial_permission__c');
        for(string strField : setQueryFields) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = strField;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + strField;
            }
        }
        /*
        for(OB_Complete_Profile_Map__c settingObj : OB_Complete_Profile_Map__c.getAll().values()) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = settingObj.SourceObjectField__c;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + settingObj.SourceObjectField__c;
            }
        }
        */
        
        soql = 'select ' +commaSepratedFields+',OB_Block_Homepage__c from Contact where Id in :lstContactId';
        
        set<string> setAccountId = new set<string>();
        for(Contact contactObj : Database.query(soql)){
        	if(contactObj.account.is_commercial_permission__c=='Yes')
            	mapCommercialPermission.put(contactObj.Id,true);
            else
            	mapCommercialPermission.put(contactObj.Id,false);
            mapContact.put(contactObj.Id,contactObj);
            if(contactObj.AccountId!=null)
            	setAccountId.add(contactObj.AccountId);
        }
        
        //Code to check SR Exists or not
        map<string,string> MapAccountUserRegApplication = new map<string,string>();
        map<string,string> MapAccountAORFApplication = new map<string,string>();
        map<string,string> MapOpportunities = new map<string,string>();//v1.1
        set<string> setRecordTypeIds = new set<string>{Label.OB_USER_REG_RECORDTYPEID,Label.OB_AOR_FINANCIAL_RECORDTYPEID};
        if(setAccountId.size()>0){
        	for(Opportunity Opp:[Select Id,AccountId from Opportunity where AccountId IN:setAccountId order by CreatedDate]){//v1.1
        		MapOpportunities.put(Opp.AccountId,Opp.Id);
        	}
	        for(HexaBPM__Service_Request__c objSR:[Select Id,HexaBPM__Customer__c,HexaBPM__Record_Type_Name__c from HexaBPM__Service_Request__c where HexaBPM__Customer__c IN:setAccountId and RecordTypeId IN:setRecordTypeIds and HexaBPM__IsCancelled__c=false and HexaBPM__Is_Rejected__c=false]){
	        	if(objSR.HexaBPM__Record_Type_Name__c=='Converted_User_Registration')
	        		MapAccountUserRegApplication.put(objSR.HexaBPM__Customer__c,objSR.Id);
	        	else if(objSR.HexaBPM__Record_Type_Name__c=='AOR_Financial')
	        		MapAccountAORFApplication.put(objSR.HexaBPM__Customer__c,objSR.Id);
	        }
        }
        system.debug('MapAccountUserRegApplication==>'+MapAccountUserRegApplication);
        for(Contact contactObj : lstContact){
        	system.debug('MapAccountUserRegApplication.get(contactObj.AccountId)==>'+MapAccountUserRegApplication.get(contactObj.AccountId));
            if(contactObj.AccountId!=null && MapAccountUserRegApplication.get(contactObj.AccountId)==null && !mapCommercialPermission.get(contactObj.Id)){
	            HexaBPM__Service_Request__c completeProfileSR = new HexaBPM__Service_Request__c();
	            if(MapOpportunities.get(contactObj.AccountId)!=null)//v1.1
	            	completeProfileSR.Opportunity__c = MapOpportunities.get(contactObj.AccountId);//v1.1
	            completeProfileSR.recordTypeId = Label.OB_USER_REG_RECORDTYPEID;
	            contactRecord = mapContact.get(contactObj.Id);
                for(OB_Complete_Profile_Map__c settingObj : OB_Complete_Profile_Map__c.getAll().values()){
	                lookupValue = '';
	                if(settingObj.SourceObjectField__c != null && settingObj.SourceObjectField__c.indexOf('.') > -1){
	                    lstLookUpField = settingObj.SourceObjectField__c.split('\\.');
	                    if(lstLookUpField != null && lstLookUpField.size() > 0 && lstLookUpField.size() < 3){
	                        if(contactRecord.getSobject(lstLookUpField[0]) != null)
	                            lookupValue = (String) contactRecord.getSobject(lstLookUpField[0]).get(lstLookUpField[1]);
	                        if(String.isNotBlank(lookupValue))
	                            completeProfileSR.put(settingObj.TargetObjectField__c,lookupValue);
	                    }
	                }   
	                else 
	                    completeProfileSR.put(settingObj.TargetObjectField__c,contactRecord.get(settingObj.SourceObjectField__c));
                }
                // V1.0   Merul populating the contact on SR if contact(OB_Block_Homepage__c) is checked
                if(contactRecord.OB_Block_Homepage__c)
                    completeProfileSR.put('HexaBPM__Contact__c',contactObj.id);  
	            lstSR.add(completeProfileSR);
        	}
        } 
        if(lstSR.size() > 0)
            insert lstSR;
        
        map<string,string> mapLinkedSR = new map<string,string>();
        for(HexaBPM__Service_Request__c sr : lstSR)
            mapLinkedSR.put(sr.HexaBPM__Contact__c,sr.id);

        //Creating AOR Application
        commaSepratedFields = '';
        mapContact = new map<string,Contact>();
        for(OB_AOR_SR_Map__c settingObj : OB_AOR_SR_Map__c.getAll().values()) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = settingObj.SourceObjectField__c;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + settingObj.SourceObjectField__c;
            }
        }
        
        soql = 'select '+commaSepratedFields+',OB_Block_Homepage__c from Contact where id in :lstContactId';
        for(Contact contactObj : Database.query(soql)){
            mapContact.put(contactObj.id,contactObj);
        }
        list<HexaBPM__Service_Request__c> lstAORSR = new list<HexaBPM__Service_Request__c>();
        for(Contact contactObj : lstContact){
        	if(contactObj.AccountId!=null && MapAccountAORFApplication.get(contactObj.AccountId)==null && !mapCommercialPermission.get(contactObj.Id)){
	            HexaBPM__Service_Request__c AORSR = new HexaBPM__Service_Request__c();
	            if(MapOpportunities.get(contactObj.AccountId)!=null)//v1.1
	            	AORSR.Opportunity__c = MapOpportunities.get(contactObj.AccountId);//v1.1
	            AORSR.recordTypeId = Label.OB_AOR_FINANCIAL_RECORDTYPEID;
	            AORSR.HexaBPM__SR_Template__c =  Label.OB_AORFSRID;
	            contactRecord = mapContact.get(contactObj.Id);
                for(OB_AOR_SR_Map__c settingObj : OB_AOR_SR_Map__c.getAll().values()){
	                lookupValue = '';
	                if(settingObj.SourceObjectField__c != null && settingObj.SourceObjectField__c.indexOf('.') > -1){
	                    lstLookUpField = settingObj.SourceObjectField__c.split('\\.');
	                    if(lstLookUpField != null && lstLookUpField.size() > 0 && lstLookUpField.size() < 3){
	                        if(contactRecord.getSobject(lstLookUpField[0]) != null)
	                            lookupValue = (String) contactRecord.getSobject(lstLookUpField[0]).get(lstLookUpField[1]);
	                        if(String.isNotBlank(lookupValue))
	                            AORSR.put(settingObj.TargetObjectField__c,lookupValue);
	                    }
	                }   
	                else 
	                    AORSR.put(settingObj.TargetObjectField__c,contactRecord.get(settingObj.SourceObjectField__c));
	
	                //Populating the Linked SR Field
	                if(mapLinkedSR.get(contactObj.id) != null)
                        AORSR.put('HexaBPM__Linked_SR__c',mapLinkedSR.get(contactObj.id));
                        
                     
                }
                 //  V 1.0   Merul populating the contact on SR if contact(OB_Block_Homepage__c) is checked
                 if(contactRecord.OB_Block_Homepage__c){
                     AORSR.put('HexaBPM__Contact__c',contactRecord.id);  
                 }  
                 
	            lstAORSR.add(AORSR);
        	}
        } 
        if(lstAORSR != null && lstAORSR.size() > 0)
            insert lstAORSR;

    }

    public static void createSRForExstCon(Map<Id,account> mapConAccID){
        list<contact> lstContact = new list<contact>();
        list<HexaBPM__Service_Request__c> lstSR = new list<HexaBPM__Service_Request__c>();
        list<string> lstContactId = new list<string>();
        string lookupValue;
        Contact contactRecord;
        list<string> lstLookUpField = new list<string>();
        map<string,Contact> mapContact = new map<string,Contact>();
        map<string,boolean> mapCommercialPermission = new map<string,boolean>();
        String commaSepratedFields = '';
        string soql;

        /* for(Contact contObj : lstContact)
            lstContactId.add(contObj.id); */
        
        for(id contId : mapConAccId.keySet()){
            lstContactId.add(contId);
        } 
		
		set<string> setQueryFields = new set<string>();
		for(OB_Complete_Profile_Map__c settingObj : OB_Complete_Profile_Map__c.getAll().values()) {
            if(settingObj.SourceObjectField__c!=null)
            	setQueryFields.add(settingObj.SourceObjectField__c.toLowerCase());
        }
		setQueryFields.add('account.is_commercial_permission__c');
        for(string strField : setQueryFields) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = strField;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + strField;
            }
        }
        /*
        for(OB_Complete_Profile_Map__c settingObj : OB_Complete_Profile_Map__c.getAll().values()) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = settingObj.SourceObjectField__c;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + settingObj.SourceObjectField__c;
            }
        }
        */
        
        soql = 'select ' +commaSepratedFields+',OB_Block_Homepage__c from Contact where Id in :lstContactId';
        
        set<string> setAccountId = new set<string>();
        for(Contact contactObj : Database.query(soql)){
        	/* if(contactObj.account.is_commercial_permission__c=='Yes')
            	mapCommercialPermission.put(contactObj.Id,true);
            else
            	mapCommercialPermission.put(contactObj.Id,false);
            mapContact.put(contactObj.Id,contactObj);
            if(contactObj.AccountId!=null)
                setAccountId.add(contactObj.AccountId); */
              
                lstContact.add(contactObj);

             if(mapConAccId.get(contactObj.id) != null && mapConAccId.get(contactObj.id).is_commercial_permission__c=='Yes')
            	mapCommercialPermission.put(contactObj.Id,true);
            else
            	mapCommercialPermission.put(contactObj.Id,false);
            	
            if(mapConAccId.get(contactObj.id).id !=null)
                setAccountId.add(mapConAccId.get(contactObj.id).id);
        }

        system.debug('$$$$$$SETacc ID' + setAccountId);
        
        //Code to check SR Exists or not
        map<string,string> MapAccountUserRegApplication = new map<string,string>();
        map<string,string> MapAccountAORFApplication = new map<string,string>();
        map<string,string> MapOpportunities = new map<string,string>();//v1.1
        set<string> setRecordTypeIds = new set<string>{Label.OB_USER_REG_RECORDTYPEID,Label.OB_AOR_FINANCIAL_RECORDTYPEID};
        if(setAccountId.size()>0){
        	for(Opportunity Opp:[Select Id,AccountId from Opportunity where AccountId IN:setAccountId order by CreatedDate]){//v1.1
        		MapOpportunities.put(Opp.AccountId,Opp.Id);
        	}
	        for(HexaBPM__Service_Request__c objSR:[Select Id,HexaBPM__Customer__c,HexaBPM__Record_Type_Name__c from HexaBPM__Service_Request__c where HexaBPM__Customer__c IN:setAccountId and RecordTypeId IN:setRecordTypeIds and HexaBPM__IsCancelled__c=false and HexaBPM__Is_Rejected__c=false]){
	        	if(objSR.HexaBPM__Record_Type_Name__c=='Converted_User_Registration')
	        		MapAccountUserRegApplication.put(objSR.HexaBPM__Customer__c,objSR.Id);
	        	else if(objSR.HexaBPM__Record_Type_Name__c=='AOR_Financial')
	        		MapAccountAORFApplication.put(objSR.HexaBPM__Customer__c,objSR.Id);
	        }
        }
        system.debug('MapAccountUserRegApplication==>'+MapAccountUserRegApplication);
        for(Contact contactObj : lstContact){
        	system.debug('MapAccountUserRegApplication.get(contactObj.AccountId)==>'+MapAccountUserRegApplication.get(contactObj.AccountId));
            if(contactObj.AccountId!=null && MapAccountUserRegApplication.get(contactObj.AccountId)==null && !mapCommercialPermission.get(contactObj.Id)){
	            HexaBPM__Service_Request__c completeProfileSR = new HexaBPM__Service_Request__c();
	            
	            completeProfileSR.recordTypeId = Label.OB_USER_REG_RECORDTYPEID;
                
                account accountObj = mapConAccId.get(contactObj.id);
                if(MapOpportunities.get(accountObj.id)!=null)//v1.1
	            	completeProfileSR.Opportunity__c = MapOpportunities.get(accountObj.id);//v1.1
                /* for(OB_Complete_Profile_Map__c settingObj : OB_Complete_Profile_Map__c.getAll().values()){
	                lookupValue = '';
	                if(settingObj.SourceObjectField__c != null && settingObj.SourceObjectField__c.indexOf('.') > -1){
	                    lstLookUpField = settingObj.SourceObjectField__c.split('\\.');
	                    if(lstLookUpField != null && lstLookUpField.size() > 0 && lstLookUpField.size() < 3){
	                        if(contactRecord.getSobject(lstLookUpField[0]) != null)
	                            lookupValue = (String) contactRecord.getSobject(lstLookUpField[0]).get(lstLookUpField[1]);
	                        if(String.isNotBlank(lookupValue))
	                            completeProfileSR.put(settingObj.TargetObjectField__c,lookupValue);
	                    }
	                }   
	                else 
	                    completeProfileSR.put(settingObj.TargetObjectField__c,contactRecord.get(settingObj.SourceObjectField__c));
                } */
                completeProfileSR.put('HexaBPM__Contact__c',contactObj.id);
                completeProfileSR.put('HexaBPM__Customer__c',accountObj.id);
                completeProfileSR.put('HexaBPM__Email__c',contactObj.Email);
                completeProfileSR.put('Entity_Name__c',accountObj.Name);
                completeProfileSR.put('Entity_Type__c',accountObj.Company_Type__c);
                completeProfileSR.put('first_name__c',contactObj.FirstName);
                completeProfileSR.put('OB_Is_Multi_Structure_Application__c',accountObj.OB_Is_Multi_Structure_Application__c);
                completeProfileSR.put('last_name__c',contactObj.LastName);
                completeProfileSR.put('HexaBPM__Send_SMS_to_Mobile__c',accountObj.Phone);
                completeProfileSR.put('Business_Sector__c',accountObj.OB_Sector_Classification__c);

                // V1.0   Merul populating the contact on SR if contact(OB_Block_Homepage__c) is checked
                if(contactObj.OB_Block_Homepage__c)
                    completeProfileSR.put('HexaBPM__Contact__c',contactObj.id);  
	            lstSR.add(completeProfileSR);
        	}
        } 
        if(lstSR.size() > 0)
            insert lstSR;
        
        map<string,string> mapLinkedSR = new map<string,string>();
        for(HexaBPM__Service_Request__c sr : lstSR)
            mapLinkedSR.put(sr.HexaBPM__Contact__c,sr.id);

        //Creating AOR Application
        commaSepratedFields = '';
        mapContact = new map<string,Contact>();
        for(OB_AOR_SR_Map__c settingObj : OB_AOR_SR_Map__c.getAll().values()) {
            if(commaSepratedFields == null || commaSepratedFields == ''){
                commaSepratedFields = settingObj.SourceObjectField__c;
            }else{
                commaSepratedFields = commaSepratedFields + ', ' + settingObj.SourceObjectField__c;
            }
        }
        
        soql = 'select '+commaSepratedFields+',OB_Block_Homepage__c from Contact where id in :lstContactId';
        for(Contact contactObj : Database.query(soql)){
            mapContact.put(contactObj.id,contactObj);
        }
        list<HexaBPM__Service_Request__c> lstAORSR = new list<HexaBPM__Service_Request__c>();
        for(Contact contactObj : lstContact){
        	if(contactObj.AccountId!=null && MapAccountAORFApplication.get(contactObj.AccountId)==null && !mapCommercialPermission.get(contactObj.Id)){
	            HexaBPM__Service_Request__c AORSR = new HexaBPM__Service_Request__c();
	            
	            AORSR.recordTypeId = Label.OB_AOR_FINANCIAL_RECORDTYPEID;
	            AORSR.HexaBPM__SR_Template__c =  Label.OB_AORFSRID;
                contactRecord = mapContact.get(contactObj.Id);
                account accountObj = mapConAccId.get(contactObj.id);

                if(MapOpportunities.get(accountObj.id)!=null)//v1.1
                    AORSR.Opportunity__c = MapOpportunities.get(accountObj.id);//v1.1
                    
               /*  for(OB_AOR_SR_Map__c settingObj : OB_AOR_SR_Map__c.getAll().values()){
	                lookupValue = '';
	                if(settingObj.SourceObjectField__c != null && settingObj.SourceObjectField__c.indexOf('.') > -1){
	                    lstLookUpField = settingObj.SourceObjectField__c.split('\\.');
	                    if(lstLookUpField != null && lstLookUpField.size() > 0 && lstLookUpField.size() < 3){
	                        if(contactRecord.getSobject(lstLookUpField[0]) != null)
	                            lookupValue = (String) contactRecord.getSobject(lstLookUpField[0]).get(lstLookUpField[1]);
	                        if(String.isNotBlank(lookupValue))
	                            AORSR.put(settingObj.TargetObjectField__c,lookupValue);
	                    }
	                }   
	                else 
	                    AORSR.put(settingObj.TargetObjectField__c,contactRecord.get(settingObj.SourceObjectField__c));
	
	                //Populating the Linked SR Field
	                if(mapLinkedSR.get(contactObj.id) != null)
                        AORSR.put('HexaBPM__Linked_SR__c',mapLinkedSR.get(contactObj.id));
                        
                     
                } */

                AORSR.put('Business_Sector__c',accountObj.OB_Sector_Classification__c);
                AORSR.put('HexaBPM__Contact__c',contactRecord.id);
                AORSR.put('HexaBPM__Customer__c',accountObj.id);
                AORSR.put('HexaBPM__Email__c',contactRecord.Email);
                AORSR.put('Entity_Name__c',accountObj.Name);
                AORSR.put('Entity_Type__c',accountObj.Company_Type__c);
                AORSR.put('first_name__c',contactRecord.FirstName);
                AORSR.put('OB_Is_Multi_Structure_Application__c',accountObj.OB_Is_Multi_Structure_Application__c);
                AORSR.put('last_name__c',contactRecord.LastName);
                AORSR.put('HexaBPM__Send_SMS_to_Mobile__c',accountObj.Phone);
                if(mapLinkedSR.get(contactObj.id) != null)
                        AORSR.put('HexaBPM__Linked_SR__c',mapLinkedSR.get(contactObj.id));

                 //  V 1.0   Merul populating the contact on SR if contact(OB_Block_Homepage__c) is checked
                 if(contactRecord.OB_Block_Homepage__c){
                     AORSR.put('HexaBPM__Contact__c',contactRecord.id);  
                 }  
                 
	            lstAORSR.add(AORSR);
        	}
        } 
        if(lstAORSR != null && lstAORSR.size() > 0)
            insert lstAORSR;

    }

    
}