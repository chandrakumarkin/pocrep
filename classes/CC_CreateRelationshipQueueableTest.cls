@isTest
public class CC_CreateRelationshipQueueableTest {
    
    @isTest
    private static void testBulkMethod(){  
    	// create account
    	// create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(2);
        insertNewAccounts[0].Registration_License_No__c = '1234';  
        insert insertNewAccounts;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Id GSUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(2,insertNewAccounts);
        insertNewContacts[0].Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Previous_Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Country_of_Birth__c  = 'Russian Federation';
        insertNewContacts[0].Country__c  = 'Russian Federation';
        insertNewContacts[0].Issued_Country__c  = 'Russian Federation';
        insertNewContacts[0].Gender__c  = 'Male';
        insertNewContacts[0].UID_Number__c  = '9712';
        insertNewContacts[0].MobilePhone  = '+97123576565';
        insertNewContacts[0].Passport_No__c  = '+97123576565';
        insertNewContacts[0].recordTypeId = portalUserId;
        // non portal
        insertNewContacts[1].Nationality__c  = 'India';
        insertNewContacts[1].Previous_Nationality__c  = 'India';
        insertNewContacts[1].Country_of_Birth__c  = 'India';
        insertNewContacts[1].Country__c  = 'India';
        insertNewContacts[1].Gender__c  = 'Female';
        insertNewContacts[1].UID_Number__c  = '9712';
        //insertNewContacts[1].MobilePhone  = '+97123576565';
        insertNewContacts[1].Passport_No__c = '12345';
        insert insertNewContacts;
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle','New_User_Registration'});
        insert createSRTemplateList;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        License__c ll =new License__c();
        ll.Account__c = insertNewAccounts[0].Id;
        insert ll;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'New_User_Registration', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].HexaBPM__Contact__c=insertNewContacts[0].id;
        insertNewSRs[1].HexaBPM__Contact__c=insertNewContacts[0].id;
        insertNewSRs[0].Foreign_Entity_Name__c='Test Forien';
        insertNewSRs[0].Place_of_Registration_parent_company__c='Russian Federation';
        insertNewSRs[0].License_Application__c=ll.Id;
        insertNewSRs[1].License_Application__c=ll.Id;
        
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Non - financial';
        insertNewSRs[1].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','In_Principle');
        insertNewSRs[1].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','AOR_Financial');
        insert insertNewSRs;
        
        HexaBPM__Status__c srStatus = new HexaBPM__Status__c();
        srStatus.Name = 'In Progress';
        srStatus.HexaBPM__Code__c = 'In Progress';
		insert srStatus;
        
        List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(1,
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'In_Principle','AOR_Financial'},
                                                        new List<String>{'Test'});

        insert stepTemplate;
        
        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
        objSRSteps.HexaBPM__Active__c = true;
       // objSRSteps.HexaBPM__Status__c = srStatus[0].Id;
        insert objSRSteps;
        
        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(2,insertNewSRs,new List<HexaBPM__SR_Steps__c>{objSRSteps});
		stepList[0].HexaBPM__Status__c = srStatus.Id;
		stepList[1].HexaBPM__Status__c = srStatus.Id;
        stepList[0].RecordTypeId = Schema.SObjectType.HexaBPM__Step__c.getRecordTypeInfosByName().get('General').getRecordTypeId();
        stepList[1].RecordTypeId = Schema.SObjectType.HexaBPM__Step__c.getRecordTypeInfosByName().get('General').getRecordTypeId();
		insert stepList;
        
        
        HexaBPM__Document_Master__c doc= new HexaBPM__Document_Master__c();
        doc.HexaBPM__Available_to_client__c = true;
        doc.HexaBPM__Code__c = 'UBO_FOUNDATION';
        doc.Download_URL__c = 'test';
        insert doc;
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Role__c = 'Shareholder;Company Secretary;Director';
        objAmendment.Passport_No__c = '12345';
        objAmendment.First_Name__c = '12345';
        objAmendment.Last_Name__c = '12345';
        objAmendment.Gender__c = 'Male';
        objAmendment.Mobile__c = '+9712346572';
        objAmendment.DI_Mobile__c = '+97124567882';
        objAmendment.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment);
        HexaBPM_Amendment__c objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment3.Role__c = 'DPO';
        objAmendment3.First_Name__c = 'DPO';
        objAmendment3.Last_Name__c = 'DPO';
        objAmendment3.Gender__c = 'Female';
        objAmendment3.Mobile__c = '+9712346572';
        objAmendment3.DI_Mobile__c = '+97124567882';
        objAmendment3.ServiceRequest__c = insertNewSRs[0].Id;
        listAmendment.add(objAmendment3);

        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment1.Nationality_list__c = 'India';
        objAmendment1.Passport_No__c = '1234';
        objAmendment1.First_Name__c = '1234';
        objAmendment1.Last_Name__c = '1234';
        objAmendment1.Role__c = 'Director;Qualified Applicant;Signatory';
        objAmendment1.Gender__c = 'Male';
        objAmendment1.DI_Mobile__c = '+97124567882';
        objAmendment1.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment1.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment1);

        HexaBPM_Amendment__c objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        objAmendment2.Address__c = 'India';
        objAmendment2.Apartment_or_Villa_Number_c__c = 'India';
        objAmendment2.Country_of_Registration__c = 'India';
        objAmendment2.Place_of_Registration__c = 'India';
        objAmendment2.Permanent_Native_City__c = 'India';
        objAmendment2.Emirate_State_Province__c = 'India';
        objAmendment2.Permanent_Native_Country__c = 'India';
        objAmendment2.Country_of_source_of_income__c = 'India';
        objAmendment2.Email__c = 'test.test@test.com';
		objAmendment2.Reason_for_exemption_under_DIFC_UBO__c = 'Government entity wholly owned by a government entity';
        objAmendment2.PO_Box__c = '1234';
        objAmendment2.Registration_No__c = '1234';
        objAmendment2.Role__c = 'Shareholder;Director;Exempt Entity';
        objAmendment2.ServiceRequest__c = insertNewSRs[0].Id;
        objAmendment2.DI_Mobile__c = '+97124567882';
        objAmendment2.Is_this_member_a_designated_Member__c = 'No';
        objAmendment2.Company_Name__c = '+97124567882';
        objAmendment2.Former_Name__c = '+97124567882';
        objAmendment2.Telephone_No__c = '+97124567882';
        objAmendment2.Mobile__c = '+97124567882';
        objAmendment2.Trading_Name__c = '+97124567882';
        objAmendment2.Decree_No__c = 'ACC123';
        objAmendment2.Type_of_Ownership_or_Control__c='The individual(s) who exercises significant control or influence over the Foundation or its Council';
        listAmendment.add(objAmendment2);
        insert listAmendment;
        
        Currency__c currencyVal = new Currency__c();
        currencyVal.Name = 'AED';
        currencyVal.Active__c = true;
        currencyVal.Currency_Code__c = 'AED';
		insert  currencyVal;  
		     
        Account_Share_Detail__c shareClass= new Account_Share_Detail__c();
        shareClass.Account__c = insertNewAccounts[0].Id;
        shareClass.No_of_shares_per_class__c = 23;
        shareClass.Currency__c = currencyVal.Id; 
        shareClass.Nominal_Value__c =100; 
        insert shareClass;
        
        Shareholder_Detail__c sd = new Shareholder_Detail__c();
        sd.Account__c= insertNewAccounts[0].Id;
        sd.Account_Share__c= shareClass.Id;
        sd.OB_Amendment__c= listAmendment[0].Id;
        insert sd;
        
        HexaBPM__SR_Doc__c srDoc = new HexaBPM__SR_Doc__c();
        srDoc.HexaBPM_Amendment__c = listAmendment[0].Id;
        srDoc.HexaBPM__Doc_ID__c = listAmendment[0].Id;
        srDoc.Name = 'Passport';
        srDoc.HexaBPM__Service_Request__c=insertNewSRs[0].Id;
        insert srDoc;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = srDoc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        List<Product2>  prodList = OB_TestDataFactory.createProduct(new list<String>{'TEST'});
        insert prodList;
        
        HexaBPM__Pricing_Line__c lineItem = OB_TestDataFactory.createSRPriceItem();
        insert lineItem;
        
        List<HexaBPM__SR_Price_Item__c> pricList = OB_TestDataFactory.createSRPriceItem(prodList,insertNewSRs[0].Id,lineItem.Id);
        pricList[0].HexaBPM__Status__c='test';
        pricList[0].HexaBPM__Price__c=2;
        pricList[0].HexaBPM__ServiceRequest__c=insertNewSRs[0].Id;
        insert pricList;
        
        List<HexaBPM__SR_Price_Item__c> pricList1 = OB_TestDataFactory.createSRPriceItem(prodList,insertNewSRs[1].Id,lineItem.Id);
        pricList1[0].HexaBPM__Status__c='test';
        pricList1[0].HexaBPM__Price__c=2;
        pricList1[0].HexaBPM__ServiceRequest__c=insertNewSRs[1].Id;
        insert pricList1;
        WebService_Details__c wed = new WebService_Details__c();
        wed.Name ='Credentials';
        wed.Password__c ='test';
        wed.SAP_User_Id__c ='SFIUSER';
        wed.Username__c ='SFIUSER';
        insert wed;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://eccdev.com/sampledata';
        insert objEP;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        
        
        Map<Id,HexaBPM_Amendment__c> amndMp = new Map<Id,HexaBPM_Amendment__c>();
        Map<id,list<Shareholder_Detail__c>> sharholderExistingMp = new Map<id,list<Shareholder_Detail__c>>();
        Map<String,String> passportNationalityMp = new Map<String,String>();
        Map<string,sObject> accountContactNewMp = new Map<string,sObject>();
        
        
        
        set<string> amdIdset = new set<String>{listAmendment[0].Id,listAmendment[1].Id,listAmendment[2].Id,listAmendment[3].Id};
        listAmendment = [Select Id,Roles__c,Individual_Corporate_Name__c,Registered_with_DIFC__c, 
                                      ServiceRequest__c,ServiceRequest__r.HexaBPM__Customer__c,
                                      Recordtype.developername,Registration_No__c,Reason_for_exemption_under_DIFC_UBO__c,
                                      UBO_Country_of_Registration__c,Decree_No__c,Country_of_Registration__c,
                                      Contact__c,customer__c,Partner_Type__c,ServiceRequest__r.HexaBPM__Parent_SR__c,
                                      Title__c,DI_First_Name__c,DI_Last_Name__c,Middle_Name__c,DI_Email__c,Mobile__c,Last_Name__c,First_Name__c,Date_of_Birth__c,Place_of_Birth__c,Passport_No__c,
                                      Nationality_list__c,Email__c,Apartment_or_Villa_Number_c__c,Address__c,
                                      PO_Box__c,Permanent_Native_City__c,Emirate_State_Province__c,Permanent_Native_Country__c,
                                      Passport_Expiry_Date__c,Gender__c,Country__c,Place_of_Issue__c,
                                      Passport_Issue_Date__c,Are_you_a_resident_in_the_U_A_E__c,Is_this_individual_a_PEP__c,
                                      Is_this_member_a_designated_Member__c,Company_Name__c,
                                      Entity_Name__r.Name,
                                      Former_Name__c,Date_Of_Registration__c,
                                      Telephone_No__c,Place_of_Registration__c,Entity_Name__c,
                                      Country_of_source_of_income__c,E_I_D_no__c,U_I_D_No__c,Emirate__c,
                                      Source_and_origin_of_funds_for_entity__c,Source_of_income_wealth__c,
                                      Type_of_Authority__c,Please_provide_the_type_of_authority__c,
                                      Financial_Service_Regulator__c,
                                      Trading_Name__c,Select_the_Qualified_Applicant_Type__c,
                                      Recognised_Exchange__c,Is_Shareholder_Qualifying_Applicant__c,Please_state_the_name_of_the_Regulator__c,
                                      Name_of_the_Regulated_Firm__c,Name_of_the_exchange__c,Jurisdiction__c,
                                      (Select Id,Status__c,Account__c,
                                       Account_Share__c,Account_Share__r.Status__c,
                                       Shareholder_Account__c,
                                       Shareholder_Contact__c,OB_amendment__c,
                                       OB_amendment__r.Other_contribution_type__c,
                                       OB_amendment__r.Type_of_Contribution__c,
                                       Sys_Proposed_No_of_Shares__c,No_of_Shares__c  
                                       From Shareholder_Details__r
                                       )
                                      From HexaBPM_Amendment__c
                                      Where Id IN :amdIdset
                        			];
        
        
        
        


		amndMp.put(listAmendment[0].Id,listAmendment[0]);
        amndMp.put(listAmendment[1].Id,listAmendment[1]);
        amndMp.put(listAmendment[2].Id,listAmendment[2]);
        amndMp.put(listAmendment[3].Id,listAmendment[3]);
        
        list<Shareholder_Detail__c> sdLst = new list<Shareholder_Detail__c>();
        sdLst.add(sd);
        sharholderExistingMp.put(sd.Id,sdLst);
        
        
        String passport = String.isNotBlank(listAmendment[0].Passport_No__c) ? listAmendment[0].Passport_No__c.toLowerCase():'';
        String nationality = String.IsNotBlank(listAmendment[0].Nationality_list__c) ? listAmendment[0].Nationality_list__c.toLowerCase() :'';
        String passportNationlity = (String.isNotBlank(passport) ? passport + '_' :'') + (String.IsNotBlank(nationality) ? nationality :'');
        
        String passport2 = String.isNotBlank(listAmendment[2].Passport_No__c) ? listAmendment[2].Passport_No__c.toLowerCase():'';
        String nationality2 = String.IsNotBlank(listAmendment[2].Nationality_list__c) ? listAmendment[2].Nationality_list__c.toLowerCase() :'';
        String passportNationlity2 = (String.isNotBlank(passport2) ? passport2 + '_' :'') + (String.IsNotBlank(nationality2) ? nationality2 :'');
        
        passportNationalityMp.put(passport,passportNationlity);
        passportNationalityMp.put(passport2,passportNationlity2);
        CC_CreateRelationshipQueueable tt = new CC_CreateRelationshipQueueable(amndMp, sharholderExistingMp, passportNationalityMp, accountContactNewMp);
        
        test.starttest();
        
        
        
        System.enqueueJob(tt);
        
    	test.stopTest();
    }

}