@isTest
private class Test_cls_InspectionChecklist {
	
	 @testSetup static void setTestData() {
    	
    	Test_FitoutServiceRequestUtils.init();
    	
    	Test_FitoutServiceRequestUtils.prepareSetup();
	}
	
	/* TODO: Put all HSE Tests into one method, grouped into lists of SRs */

	static TestMethod void testEventInspection(){
		
		Test_FitoutServiceRequestUtils.useDefaultUser();
		Test_FitoutServiceRequestUtils.testEventInspection();
	}
	
	static TestMethod void testFinalInspection(){
		
		Test_FitoutServiceRequestUtils.useDefaultUser();
		Test_FitoutServiceRequestUtils.testFinalInspection();
	}
	
	static TestMethod void testFirstFixInspection(){
		
		Test_FitoutServiceRequestUtils.useDefaultUser();
		Test_FitoutServiceRequestUtils.testFirstFixInspection();
	}
	
	static TestMethod void testHSEInspectionForHotWork(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
		Test_FitoutServiceRequestUtils.testHSEInspectionForHotWork();
    }
    
    static TestMethod void testHSEInspectionForExcavation(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
		Test_FitoutServiceRequestUtils.testHSEInspectionForExcavation();
    }
    
    static TestMethod void testHSEInspectionForWorkAtHeight(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
		Test_FitoutServiceRequestUtils.testHSEInspectionForWorkAtHeight();
    }
    
    static TestMethod void testHSEInspectionForCraneHoist(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
		Test_FitoutServiceRequestUtils.testHSEInspectionForCraneHoist();
    }
    
    static TestMethod void testHSEInspectionForConfinedSpaceEntry(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
		Test_FitoutServiceRequestUtils.testHSEInspectionForConfinedSpaceEntry();
    }
    
    static TestMethod void testHSEInspectionForOnNear(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
		Test_FitoutServiceRequestUtils.testHSEInspectionForOnNear();
    }
    
    static TestMethod void testHSEInspectionForIsolation(){
        Test_FitoutServiceRequestUtils.useDefaultUser();
		Test_FitoutServiceRequestUtils.testHSEInspectionForIsolation();
    }
}