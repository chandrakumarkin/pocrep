/***********************************************************************************************
*  Author    : Suchita  
*  Company   : DIFC
*  Date      : 24th Feb 2021
*  Purpose   : This class is used to update the status of Voluntary Strike off SR to closed 
*             after 90 days 
* Test Class : VoluntaryStrikeOffBatchTest
-----------------------------------------------------------------------------------------------
Modification History 
-----------------------------------------------------------------------------------------------
V.No    Date               Updated By      Description
-----------------------------------------------------------------------------------------------              
V1.0   24th Feb 2021     Suchita Sharma     intial Version
**********************************************************************************************/

global class VoluntaryStrikeOffBatch implements Database.Batchable<sObject>,Database.Stateful ,Database.AllowsCallouts{ 
    public final String Query;
    public VoluntaryStrikeOffBatch(){
        Query='Select Id from Service_Request__c where Customer__r.ROC_status__c=\'Dissolved - Voluntary Strike off\' and External_Status_Name__c= \'Pending Voluntary Strike off\'';
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC,List<Service_Request__c> scope){
        List<Service_Request__c	> srToUpdate = new List<Service_Request__c>();	
        Id approveSRStatus = [Select Id,name from SR_Status__c where name = 'Approved' Limit 1].Id;
        for(Service_Request__c SRRec: scope){
            SRRec.External_SR_Status__c	= approveSRStatus;
            SRRec.Internal_SR_Status__c	= approveSRStatus;
            srToUpdate.add(SRRec);
        }
        
        try{
            if(!srToUpdate.isEmpty()){
                update srToUpdate;
            }
        }catch(Exception e){
            insert LogDetails.CreateLog(null, 'BatchVoluntaryStrikeOff', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
        }
    }
    
    public void finish(Database.BatchableContext BC){
        
    }
}