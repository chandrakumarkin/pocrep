/**************************************************************************************************
* Name               : CC_CPPerformFinalUpdates                                                               
* Description        : CC Code to update Authorised Signatory Combination on the Account                                        
* Created Date       : 15th December 2020                                                            
* Created By         : Salma Rasheed (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version    Author                  Date               Comment                                                     
  1          salma.rasheed@pwc.com   15/12/2020        Initial Draft
**************************************************************************************************/

global without sharing class CC_CPPerformFinalUpdates implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
         if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null && String.IsNotBlank(stp.HexaBPM__SR__r.Authorized_Signatory_Combinations__c)){
            try{
                Account objAccount = new Account(id=stp.HexaBPM__SR__r.HexaBPM__Customer__c,Authorized_Signatory_Combinations__c=stp.HexaBPM__SR__r.Authorized_Signatory_Combinations__c);
                update objAccount;
            }catch(Exception e){    strResult = e.getMessage()+'';  }
        }
        return strResult;
    } 
}