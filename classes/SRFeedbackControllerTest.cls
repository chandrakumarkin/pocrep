@IsTest
private class SRFeedbackControllerTest {
    static testMethod void validatefeedback(){
        Service_Request__c srObj = new Service_Request__c();
        srObj.Send_SMS_To_Mobile__c = '+971526767777';
        insert srObj;
        PageReference pageRef = Page.SRFeedback;
        pageRef.getParameters().put('id', String.valueOf(srObj.Id));
        Test.setCurrentPage(pageRef);
        SRFeedbackController srController = new SRFeedbackController(); //Instantiate the Class
        srController.doSubmit();
    }
}