global class exceptionDateWrapper{
        public id recordId {get;set;}
        public boolean selectRecord {get;set;}
        public string name {get;set;}
        public string client {get;set;}
        public date exceptionDate {get;set;}
        public date startDate {get;set;}
        public date endDate {get;set;}
        public date defaultedDate {get;set;}
        
        public exceptionDateWrapper(compliance__c a){
            name = '';
            client = '';
            recordId = null;
            selectRecord = false;
            exceptionDate = null;
            startDate = null;
            endDate = null;
            defaultedDate = null;
        }
    }