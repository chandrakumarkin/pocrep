@isTest
public class New_Ser_MainClass_Test {
    
    static testmethod void myTest1(){
            
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = 'Yes';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordTypeIds.get('License_Renewal');
        objSR.Service_Category__c = 'New';
        objSR.I_agree__c=true;
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        objSR.Financial_Year_End_mm_dd__c ='Test';
        objSR.Agreement_Date__c=date.today();
        insert objSR;
        
        Apexpages.currentPage().getParameters().put('RecordType',mapRecordTypeIds.get('Confirmation_Statement'));
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
        New_Ser_MainClass confirmation = new New_Ser_MainClass(con);
        Apexpages.currentPage().getParameters().put('field_V','Arun');
        Apexpages.currentPage().getParameters().put('field_F','Entity_Name__c');
        confirmation.RowValueChange();
        confirmation.getYear();
        
        confirmation.getRegisteredAuditors();
        
        confirmation.SaveConfirmation();
        
        
       
       
       
        
       
        }
        }