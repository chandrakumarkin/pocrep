@isTest
public without sharing class CC_CreateRSManagerOnAORNFTest 
{
     public static testmethod void  CC_CreateRSManagerOnAORNFTest() 
    {
           // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(2);
        insertNewAccounts[0].Registration_License_No__c = '1234';  
        insert insertNewAccounts;
        
	        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
	        Id GSUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
	        
	        //create contact
	        List<Contact> insertNewContacts = new List<Contact>();
	        insertNewContacts =  OB_TestDataFactory.createContacts(2,insertNewAccounts);
	        insertNewContacts[0].Nationality__c  = 'Russian Federation';
	        insertNewContacts[0].Previous_Nationality__c  = 'Russian Federation';
	        insertNewContacts[0].Country_of_Birth__c  = 'Russian Federation';
	        insertNewContacts[0].Country__c  = 'Russian Federation';
	        insertNewContacts[0].Issued_Country__c  = 'Russian Federation';
	        insertNewContacts[0].Gender__c  = 'Male';
	        insertNewContacts[0].UID_Number__c  = '9712';
	        insertNewContacts[0].MobilePhone  = '+97123576565';
	        insertNewContacts[0].Passport_No__c  = '+97123576565';
	        insertNewContacts[0].recordTypeId = portalUserId;
	        // non portal
	        insertNewContacts[1].Nationality__c  = 'India';
	        insertNewContacts[1].Previous_Nationality__c  = 'India';
	        insertNewContacts[1].Country_of_Birth__c  = 'India';
	        insertNewContacts[1].Country__c  = 'India';
	        insertNewContacts[1].Gender__c  = 'Female';
	        insertNewContacts[1].UID_Number__c  = '9712';
	        //insertNewContacts[1].MobilePhone  = '+97123576565';
	        insertNewContacts[1].Passport_No__c = '12345';
	        insert insertNewContacts;
        
            //create createSRTemplate
            List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
            createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_NF_R','In_Principle'});
            createSRTemplateList[0].ownerid = userinfo.getUserId();
            insert createSRTemplateList;

            //create SR status
            HexaBPM__SR_Status__c srStatus = new HexaBPM__SR_Status__c() ;
            srStatus.Name = 'In Progress';
            srStatus.HexaBPM__Code__c = 'IN_PROGRESS';
            srStatus.HexaBPM__Type__c = 'Start';
            insert srStatus;
            
            //create SR status
	        /*list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
	        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','In Progress','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
	        insert listSRStatus;*/
        
            //create SR
	        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
	        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'New_User_Registration', 'In_Principle'}, insertNewAccounts, 
	                                                   new List<string>{'Non - financial','Retail'}, 
	                                                   new List<string>{'General Partner for a Limited Partnership Fund','Services'}, 
	                                                   new List<string>{'Partnership','Company'}, 
	                                                   new List<string>{'Recognized Limited Partnership (RLP)','Recognized Company'});
	        insertNewSRs[1].Setting_Up__c='Branch';
	        insertNewSRs[0].Setting_Up__c='Branch';
	        insertNewSRs[0].HexaBPM__Customer__c=insertNewAccounts[0].Id;
	        insertNewSRs[1].HexaBPM__Customer__c=insertNewAccounts[0].Id;
	        insertNewSRs[0].HexaBPM__Contact__c=insertNewContacts[0].id;
	        insertNewSRs[1].HexaBPM__Contact__c=insertNewContacts[0].id;
	        insertNewSRs[0].Foreign_Entity_Name__c='Test Forien';
	        insertNewSRs[0].Place_of_Registration_parent_company__c='Russian Federation';
	        
	        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
	        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
	        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = srStatus.id;
	        insertNewSRs[0].HexaBPM__External_SR_Status__c = srStatus.id;
	        insertNewSRs[1].HexaBPM__External_SR_Status__c = srStatus.id;
	        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = srStatus.id;
	        insertNewSRs[0].Entity_Type__c = 'Non - financial';
	        insertNewSRs[1].Entity_Type__c = 'Non - financial';
	        insertNewSRs[0].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','In_Principle');
	        insertNewSRs[1].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','AOR_NF_R');
	        insert insertNewSRs;
			
			insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs[0].Id;
	        upsert insertNewSRs[1];
	        
			List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(4,
                                                        new List<String>{'RS_OFFICER_REVIEW','RS_MANAGER_REVIEW','ENTITY_SIGNING','COMPLIANCE_REVIEW'},
                                                        new List<String>{'RS_OFFICER_REVIEW','RS_MANAGER_REVIEW','ENTITY_SIGNING','COMPLIANCE_REVIEW'},
                                                        new List<String>{'AOR_NF_R','AOR_NF_R','AOR_NF_R','AOR_NF_R'},
                                                        new List<String>{'Test','Test','Test','Test'});

	        stepTemplate[0].HexaBPM__Code__c = 'RS_OFFICER_REVIEW';
	        stepTemplate[1].HexaBPM__Code__c = 'RS_MANAGER_REVIEW';
	        stepTemplate[2].HexaBPM__Code__c = 'ENTITY_SIGNING';
	        stepTemplate[3].HexaBPM__Code__c = 'COMPLIANCE_REVIEW';
	        insert stepTemplate;
	        
	        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
	        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
	        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
	        objSRSteps.HexaBPM__Active__c = true;
	       // objSRSteps.HexaBPM__Status__c = srStatus[0].Id;
	        insert objSRSteps;
	        
	        HexaBPM__SR_Steps__c objSRSteps1 = new HexaBPM__SR_Steps__c();
	        objSRSteps1.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
	        objSRSteps1.HexaBPM__Step_Template__c = stepTemplate[1].Id;
	        objSRSteps1.HexaBPM__Active__c = true;
	       // objSRSteps.HexaBPM__Status__c = srStatus[0].Id;
	        system.debug('inert objSRSteps1-----'+objSRSteps1);
	        insert objSRSteps1;
	        system.debug('inert objSRSteps2----'+objSRSteps1);
	        
	        HexaBPM__Status__c srStatus1 = new HexaBPM__Status__c();
	        srStatus1.Name = 'In Progress';
	        srStatus1.HexaBPM__Code__c = 'In Progress';
			insert srStatus1;
	        
	        HexaBPM__Status__c srStatus2 = new HexaBPM__Status__c();
	        srStatus2.Name = 'APPROVED';
	        srStatus2.HexaBPM__Code__c = 'APPROVED';
			insert srStatus2;
	        
	        HexaBPM__Status__c srStatus3 = new HexaBPM__Status__c();
	        srStatus3.Name = 'AML_SUBMITTED';
	        srStatus3.HexaBPM__Code__c = 'AML_SUBMITTED';
			insert srStatus3;
	        
	        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(1,new List<HexaBPM__Service_Request__c>{insertNewSRs[0]},new List<HexaBPM__SR_Steps__c>{objSRSteps});
	        
			stepList[0].HexaBPM__Status__c = srStatus1.Id;
			insert stepList;
			
			List<HexaBPM__Step__c> step1List = OB_TestDataFactory.createSteps(4,new List<HexaBPM__Service_Request__c>{insertNewSRs[1]},new List<HexaBPM__SR_Steps__c>{objSRSteps1});
			
			step1List[0].HexaBPM__Status__c = srStatus2.Id;// RS Manager
			step1List[0].HexaBPM__Step_Template__c = stepTemplate[0].Id;
			
			step1List[1].HexaBPM__Status__c = srStatus2.Id;//RS officer
			step1List[1].HexaBPM__Step_Template__c = stepTemplate[1].Id;
			
			step1List[2].HexaBPM__Status__c = srStatus2.Id;//RS officer
			step1List[2].HexaBPM__Step_Template__c = stepTemplate[2].Id;
			
			step1List[3].HexaBPM__Status__c = srStatus3.Id;//RS officer
			step1List[3].HexaBPM__Step_Template__c = stepTemplate[3].Id;
			
			insert step1List;
        

		test.starttest();
        CC_CreateRSManagerOnAORNF createRel= new CC_CreateRSManagerOnAORNF();
        for(HexaBPM__SR_Steps__c obj :[Select Id,HexaBPM__SR_Template__r.HexaBPM__SR_RecordType_API_Name__c,
	        								HexaBPM__Step_Template__r.HexaBPM__Code__c from HexaBPM__SR_Steps__c]){
	        	system.debug('=======sr rec========'+obj.HexaBPM__SR_Template__r.HexaBPM__SR_RecordType_API_Name__c);
	        	system.debug('=======sr code========'+obj.HexaBPM__Step_Template__r.HexaBPM__Code__c);
	        }
        
        for(HexaBPM__Service_Request__c srty : [SELECT id,HexaBPM__Parent_SR__c,HexaBPM__External_Status_Name__c,
        											HexaBPM__Record_Type_Name__c,HexaBPM__Customer__c
        											 FROM HexaBPM__Service_Request__c])
                {
                	system.debug('===============record Id============='+srty.Id);
                	system.debug('===============record tyep============='+srty.HexaBPM__Record_Type_Name__c);
                	system.debug('===============record parent============='+srty.HexaBPM__Parent_SR__c);
                	system.debug('===============record HexaBPM__External_Status_Name__c============='+srty.HexaBPM__External_Status_Name__c);
                	system.debug('===============record HexaBPM__Customer__c============='+srty.HexaBPM__Customer__c);
                }
        List<HexaBPM__Step__c> stepNewList  = [select id,HexaBPM__SR__c,
			        HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Type_of_entity__c,
			        HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.Authorized_Signatory_Combinations__c,
			        HexaBPM__SR__r.Authorized_Signatory_Combinations_Arabic__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,
			        HexaBPM__Step_Template__r.HexaBPM__Code__c,
        			HexaBPM__SR__r.License_Application__c from HexaBPM__Step__c];
        for(HexaBPM__Step__c stp: stepNewList){
        	system.debug('===stepId===='+stp.Id);
        	system.debug('======='+stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c);
        	system.debug('==external====='+stp.HexaBPM__Step_Template__r.HexaBPM__Code__c);
        }
        system.debug(insertNewSRs[0].Id+'==sr Id======stepId==='+stepNewList[0].Id);
        createRel.EvaluateCustomCode(insertNewSRs[0],stepNewList[0]);
        
    	test.stopTest();

    }


    
}