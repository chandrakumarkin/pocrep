@isTest(SeeAllData=true) 
public with sharing class DIFC_APIFieldSelectionsControllerTest
{
    static testMethod void  Test_DIFC_APIFieldSelectionsController() 
    {
        String fieldName = 'Member.ConfirmCessation', flag = '0',update1,flag1 = '1',fieldName1 = 'RegisteredNumber';
        DIFC_APIFieldSelectionsController difc = new DIFC_APIFieldSelectionsController();
        
        DIFC_Field_Selection__c  df = new DIFC_Field_Selection__c (Name = 'Member.ConfirmCessation',Selected__c = true);  
        insert df;
        DIFC_Field_Selection__c  df1 = new DIFC_Field_Selection__c (Name = 'Member.EndDate',Selected__c = true);  
        insert df1;
        DIFC_Field_Selection__c  df2 = new DIFC_Field_Selection__c (Name = 'Member.ManagerName',Selected__c = true);  
        insert df2;
        DIFC_Field_Selection__c  df3 = new DIFC_Field_Selection__c (Name = 'Member.StartDate',Selected__c = true);  
        insert df3;
        DIFC_Field_Selection__c  df4 = new DIFC_Field_Selection__c (Name = 'TradingName.RegisteredNumber',Selected__c = true);  
        insert df4;
        DIFC_Field_Selection__c  df5 = new DIFC_Field_Selection__c (Name = 'DateOfRegistration',Selected__c = true);  
        insert df5;
        DIFC_Field_Selection__c  df6 = new DIFC_Field_Selection__c (Name = 'LicencedActivity.RegisteredNumber',Selected__c = true);  
        insert df6;
        DIFC_Field_Selection__c  df7 = new DIFC_Field_Selection__c (Name = 'Manager.RegisteredNumber',Selected__c = true);  
        insert df7;
        DIFC_Field_Selection__c  df8 = new DIFC_Field_Selection__c (Name = 'TypeOfLicense',Selected__c = true);  
        insert df8;
        
        DIFC_Field_Selection__c  df9 = new DIFC_Field_Selection__c (Name = 'CompanyId',Selected__c = false);  
        insert df9;
        DIFC_Field_Selection__c  df10 = new DIFC_Field_Selection__c (Name = 'RegisteredNumber',Selected__c = false);  
        insert df10;
        DIFC_Field_Selection__c  df11 = new DIFC_Field_Selection__c (Name = 'RegulatedStatus',Selected__c = false);  
        insert df11;
        DIFC_Field_Selection__c  df12 = new DIFC_Field_Selection__c (Name = 'EntityStatus',Selected__c = false);  
        insert df12;
        DIFC_Field_Selection__c  df13 = new DIFC_Field_Selection__c (Name = 'DNFBP',Selected__c = false);  
        insert df13;
        DIFC_Field_Selection__c  df14 = new DIFC_Field_Selection__c (Name = 'DateOfIncorporation',Selected__c = false);  
        insert df14;
        DIFC_Field_Selection__c  df15 = new DIFC_Field_Selection__c (Name = 'DFSALicense',Selected__c = false);  
        insert df15;
        DIFC_Field_Selection__c  df16 = new DIFC_Field_Selection__c (Name = 'TypeOfEntity',Selected__c = false);  
        insert df16;
        difc.fetchSelectedFields();
        
        update1 =  DIFC_APIFieldSelectionsController.updateField(fieldName,flag);
        update1 =  DIFC_APIFieldSelectionsController.updateField(fieldName,flag1);
                
    }    
}