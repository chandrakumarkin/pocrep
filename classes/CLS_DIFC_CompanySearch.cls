@RestResource(urlMapping = '/CompanySearch/*')
global class CLS_DIFC_CompanySearch{

    @Httpget 
    global static  void jsonResponse()
    {
        //Name from URL
          String Mode =  RestContext.request.params.get('q_search');
          if(!test.isRunningTest())
          {
                RestContext.response.addHeader('Content-Type', 'application/json');
          }
      
      try
      {
       
        RestContext.response.responseBody = Blob.valueOf(jsonResString(Mode));
      }
      catch(Exception ex)
      {
        
      }
      
      
    }
    
    public static string jsonResString(string Mode)
    {
     //Dubai_Data__c ObjSQL=Dubai_Data__c.getInstance(Mode );
     
     String j= Mode+'%';
     //List<Dubai_Data_Meta__mdt> DataList= [SELECT SQL_Values__c   FROM Dubai_Data_Meta__mdt   WHERE DeveloperName =:Mode];
       List<Account> ListAccounts=[select Name,Registration_License_No__c,Account_ID_18_Report__c from account where  Registration_License_No__c!=null AND ROC_Status__c  in('Active','Not Renewed')  and Name Like:j limit 10];                                                        
     if(ListAccounts.Size()>0)     
    return      JSON.serialize( ListAccounts);
    else
    return  '';
    
    
    }
    
 }