/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CC_CreateConditions {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Service_Request__c objSR = new Service_Request__c();
        insert objSR;
        
        CC_CreateConditions.Check_Condition('Check_ShareHolderIndExists',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_MemberIndExists',objSR.Id);
        CC_CreateConditions.Check_Condition('General_Partner_Ind_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('General_Partner_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Limited_Partner_Ind_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Limited_Partner_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Founding_Member_Ind_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Founding_Member_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_Individual_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_BodyCorporate_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Director_BodyCorporate_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_Member_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_Foundation_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_ShareHolder_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('DesignatedMember_Ind_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('DesignatedMember_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('CalculateClearance',objSR.Id);
        CC_CreateConditions.Check_Condition('hasFineAmount',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_ShareHolder_Ind_or_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_Member_Ind_or_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Founding_Member_Ind_or_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('General_Partner_Ind_or_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('Limited_Partner_Ind_or_BC_Exists',objSR.Id);
        CC_CreateConditions.Check_Condition('DesignatedMember_Ind_or_BC_Exists',objSR.Id);
        
        CC_CreateConditions.Check_Condition('checkConceptualApprovalByFOSP',objSR.Id);
        CC_CreateConditions.Check_Condition('checkVerifyAndApproveByFOSP',objSR.Id);
        CC_CreateConditions.Check_Condition('checkDetailedDesignApprovalByFOSP',objSR.Id);
        CC_CreateConditions.Check_Condition('doesNotContainsWithoutReplacement',objSR.Id);
        CC_CreateConditions.Check_Condition('doesNotContainUser',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_Shrhldr_BdyCrp_Mbr_GnrlPrtnr_LtdPrtnr_DsgntdMbr',objSR.Id);
        CC_CreateConditions.Check_Condition('isChangedAmendment',objSR.Id);
        
        //The below code has been added on 13-11-2017 - M.Prem
        CC_CreateConditions.Check_Condition('RestrictEncashmentCreation',objSR.Id);//#4210
        CC_CreateConditions.Check_Condition('UBO_Ind_or_BC_Exists',objSR.Id);//#3736
        CC_CreateConditions.Check_Condition('Check_Share_Or_Member_Ind_or_BC_Exists',objSR.Id);//#3736
        CC_CreateConditions.Check_Condition('Check_IS_Add_in_SR_Amendment',objSR.Id);
        CC_CreateConditions.Check_Condition('Founding_Member_Ind_ExistsAML',objSR.Id);
        CC_CreateConditions.Check_Condition('CheckChangedAmendment',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_IS_Changein_SR_Amendment',objSR.Id);//#3736
        CC_CreateConditions.Check_Condition('checkEncashmentStep',objSR.Id);
        CC_CreateConditions.Check_Condition('checkDpSection',objSR.Id);
        CC_CreateConditions.Check_Condition('CheckUBODataUpdate',objSR.Id);
        CC_CreateConditions.Check_Condition('CheckUBODataRemove',objSR.Id);
        CC_CreateConditions.Check_Condition('CheckUBODataAdded',objSR.Id);
        CC_CreateConditions.Check_Condition('Check_IS_Removed_SR_Amendment',objSR.Id); 
        CC_CreateConditions.Check_Condition('checkRelationshipTypeforAML',objSR.Id);         
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.id;
        objStep.Step_No__c = 1.0;
        objStep.Sys_Step_Loop_No__c = '1.0_1.0';
        insert objStep;
        List<Step__c> stepList = new List<Step__c>();
        stepList.add(objStep);
        CC_CreateConditions.Check_Condition('getMostRecentStep',objSR.Id);
        
         Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Amendment__c iAmm = new Amendment__c();
        iAmm.Status__c = 'Active';
        iAmm.No_of_shares_per_class__c = 1;
        iAmm.Sys_Proposed_Nominal_Value__c = 1;
        iAmm.Nominal_Value__c = 1;
        iAmm.Sys_Proposed_No_of_Shares_per_Class__c = 1;
        iAmm.Shareholder_Company__c = objAccount.ID;
        iAmm.ServiceRequest__c = objSR.id;
        iAmm.Class_Name__c ='A';
        iAmm.Shareholder_Company_val__c = objAccount.id;
        iAmm.Amendment_Type__c='Type';
        insert iAmm;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        
        insert objContact;
        
        Relationship__c relBus = new Relationship__c();
        relBus.Object_Contact__c = objContact.id;
        relBus.Subject_Account__c =  objAccount.Id;
        relBus.Relationship_Type__c ='Beneficiary Owner';
        relBus.Active__c= true;
        relBus.Primary__c = true;
        insert relBus;
        
        CC_CreateConditions.isAgeBelowEighteen(objSR.Id);
        CC_CreateConditions.getMostRecentStep(stepList,false);
        CC_CreateConditions.checkDpSection(objSR.Id);
        cc_CreateConditions.isParent_RequestType_As_C_Major(objSR.Id);
        cc_CreateConditions.isNameChangedInAmendment(objSR.Id);
        cc_CreateConditions.CheckChangedAmendment(objSR.Id);
    }
    
    static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        Service_Request__c objSR = new Service_Request__c();
        objSR.Type_of_Request__c = 'Type A';
        insert objSR;
        Status__c stepStatus = new Status__c(Name = 'Passed');
        SR_Steps__c srStep = new Sr_Steps__c(Step_No__c=85.0);
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.id;
        objStep.Step_No__c = 85.0;
        objStep.Sys_Step_Loop_No__c = '85.0';
        objStep.Status__c = stepStatus.id;
        insert objStep;
        List<Step__c> stepList = new List<Step__c>();
        stepList.add(objStep);
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        
        insert objContact;
        
        Relationship__c relBus = new Relationship__c();
        relBus.Object_Contact__c = objContact.id;
        relBus.Subject_Account__c =  objAccount.Id;
        relBus.Relationship_Type__c ='Beneficiary Owner';
        relBus.Active__c= true;
        relBus.Primary__c = true;
        insert relBus;
        
        Amendment__c iAmm = new Amendment__c();
        iAmm.Status__c = 'Active';
        iAmm.No_of_shares_per_class__c = 1;
        iAmm.Sys_Proposed_Nominal_Value__c = 1;
        iAmm.Nominal_Value__c = 1;
        iAmm.Sys_Proposed_No_of_Shares_per_Class__c = 1;
        iAmm.Shareholder_Company__c = objAccount.ID;
        iAmm.ServiceRequest__c = objSR.id;
        iAmm.Class_Name__c ='A';
        iAmm.Shareholder_Company_val__c = objAccount.id;
        iAmm.Amendment_Type__c='Type';
        insert iAmm;
        
        CC_CreateConditions.isVisaApplicable(objSR.Id);
        CC_CreateConditions.ApprovalForFitToOccupyCertificateStep(objSR.Id);
        CC_CreateConditions.CheckUBODataUpdate(objSR.Id);
        CC_CreateConditions.CheckUBODataRemove(objSR.Id);
        CC_CreateConditions.CheckUBODataAdded(objSR.Id);
        CC_CreateConditions.Check_IS_Removed_SR_Amendment(objSR.Id);
        cc_CreateConditions.isNameChangedInAmendment(objSR.Id);
        cc_CreateConditions.CheckChangedAmendment(objSR.Id);
        cc_CreateConditions.IS_AddOrRemoved_SR_Amendment(objSR.Id);
        cc_CreateConditions.checkDeRegistration(objSR.Id);
        cc_CreateConditions.ApplicantOutsideUAEWithNoMobile(objSR.Id);
        //cc_CreateConditions.checkDeRegistration(objSR.Id);
        //   cc_CreateConditions.checkDeRegistration(objSR.Id);
        
        
        
    }
}