/*
    Created By  : Shabbir - DIFC on 30 June,2018
    Description : Test class for OperateIntegrationCls, OperateAccountPushCls, OperateLeadPushCls
--------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------              
 ----------------------------------------------------------------------------------------
*/
@isTest
private class TestOperateIntegrationCls {

    static testMethod void myUnitTestLead() {
        
        lead objLead = new Lead();
        objLead.Company = 'johnSmith';
        objLead.LastName = 'john';
        objLead.firstname = 'Ghan';
        objLead.Email = 'gsyadav19@gmail.com';
        objLead.LeadSource = 'Call centre';
        objLead.MobilePhone = '8019773451';
        objLead.Description = 'Description';
        objLead.FinTech_Category__c = 'Crowdfunding';
        objLead.Country__c = 'India';
        objLead.Website = 'www.salesforcebyshyam.blogspot.com';
        objLead.Title = 'Mr. Shyam';
        objLead.BD_Sector_Classification__c = 'Banks';
        objLead.Company_Type__c = 'Financial - related';
        //objLead.ConvertedAccountId = objAccount.Id;
        insert objLead;
        
      //  Database.LeadConvert lc = new database.LeadConvert();
      //  lc.setLeadId(objLead.Id);
      //  Database.LeadConvertResult lcr = Database.convertLead(lc);
        
        list<Id> listLeadIds = new list<Id>();
        listLeadIds.add(objLead.Id);
        OperateLeadPushCls.createLeadInOperate(listLeadIds);        
        
    }
    
    static testMethod void myUnitTestAccount() {
        
        Account objAccount = new Account();
        objAccount.Name = 'johnSmith';
        objAccount.BP_No__c = '143343434';
        objAccount.BD_Sector__c = 'Banks';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Fintech_Email__c = 'gsyadav19@gmail.com';
        insert objAccount;
        
      Id RecordTypeIdContact = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Development').getRecordTypeId();

        
        Contact con = new Contact();
        con.firstname = 'Shyam';
        con.LastName = 'yadav';
        con.email = 'gsyadav19@gmail.com';
        con.AccountId = objAccount.Id;
        con.Phone = '8019773451';
        con.Title = 'Title';
        con.RecordTypeId = RecordTypeIdContact;
        insert con;
        
        list<Id> listAccountIds = new list<Id>();
        listAccountIds.add(objAccount.Id);
        OperateAccountPushCls.createAccountInOperate(listAccountIds);       
        
    }   
    
}