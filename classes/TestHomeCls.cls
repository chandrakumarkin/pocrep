/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class TestHomeCls {
    
    @testSetup static void setTestData() {
        
        Loop__DDP__c testDdp = new Loop__DDP__c(); 
        Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
        
        /*
         * Create the required SR and step Statuses for the SRs
         */
        List<SR_Status__c> testSrStatusList = new List<SR_Status__c>(); 
        
        testSrStatusList.add(new SR_Status__c(Name = 'Draft',Code__c =  'DRAFT'));
        testSrStatusList.add(new SR_Status__c(Name = 'Submitted',Code__c =  'SUBMITTED'));
        testSrStatusList.add(new SR_Status__c(Name = 'Approved',Code__c =   'APPROVED'));
        testSrStatusList.add(new SR_Status__c(Name = 'Issued',Code__c = 'ISSUED'));
        
        Status__c testSubmittedStatus = new Status__c(Name = 'Submitted', Code__c = 'SUBMITTED');
        Status__c testDraftStatus = new Status__c(Name = 'Draft',Code__c = 'DRAFT');
        Status__c testApprovedStatus = new Status__c(Name = 'Approved',Code__c = 'APPROVED');
        
        /* Get the default country Codes: 971 & 376 */
        List<CountryCodes__c> testCountryCodes = Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
        
        /* Create a lookup for UAE */
        Lookup__c uaeLookup = new Lookup__c(DNRD_Code__c='101',Name='United Arab Emirates',Type__c='Nationality',Code__c='AE');
        
        /* Create the required DDP records */
        testDdp.Name = 'Test DDP';
        testDdp.Loop__Type__c = 'Letter';
        testDdp.Loop__Output_Filename__c = 'Test Letter';
        
        insert testDdp;
        
        Loop__DDP_Integration_Option__c testTemplate = new Loop__DDP_Integration_Option__c();
            
        testTemplate.Name = 'Compliance Calendar';
        testTemplate.Loop__Output__c = 'PDF';
        testTemplate.Loop__Attach_As__c = 'Attachment';
        testTemplate.Loop__DDP__c = testDdp.Id;
        
        insert testTemplate;
        
        /* Create the remaining primary records */
        insert uaeLookup;
        insert objAccount;
        insert testCountryCodes;
        insert testSrStatusList;
        insert new List<Status__C>{testSubmittedStatus,testDraftStatus,testApprovedStatus};
        
        /* Create a contact to be associated to the new user */
        Contact objContact = Test_CC_FitOutCustomCode_DataFactory.getTestContact(objAccount.Id);
        
        insert objContact;
         
        User objUser = new User(id = UserInfo.getUserId());
        
        objUser.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services';
     //   objUser.Contactid = objContact.ID;
        update objUser;
        
        Test_FitoutServiceRequestUtils.prepareSetup();
    }

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        User objUser = new User(id = UserInfo.getUserId());
        
        objUser.Community_User_Role__c = 'Company Services;Fit-Out Services;Property Services';
      //  objUser.Contact = objContact.ID;
        update objUser;
        
        system.runAs(objUser) {
            
            Document_Master__c objDocMaster = new Document_Master__c();
            objDocMaster.Name = 'Compliance Calendar';
            insert objDocMaster;
             
            list<Compliance__c> lstCompliance = new list<Compliance__c>();
            Compliance__c objCompliance = new Compliance__c();
            objCompliance = new Compliance__c();
            objCompliance.Name = 'License Renewal';
            objCompliance.Start_Date__c = system.today().addDays(-7).addDays(-7);
            objCompliance.End_Date__c = system.today().addDays(-1);
            objCompliance.Exception_Date__c = objCompliance.End_Date__c;
            objCompliance.Status__c = 'Open';
            lstCompliance.add(objCompliance);
            
            objCompliance = new Compliance__c();
            objCompliance.Name = 'Notification Renewal';
            objCompliance.Start_Date__c = system.today().addDays(-7);
            objCompliance.End_Date__c = system.today().addDays(-1);
            objCompliance.Exception_Date__c = objCompliance.End_Date__c;
            objCompliance.Status__c = 'Open';
            lstCompliance.add(objCompliance);
            insert lstCompliance;
            
            FineSRDetailsCls.IssueFine(lstCompliance[0].Id);
            FineSRDetailsCls.IssueFine(lstCompliance[1].Id);
            
            Product2 testFineProduct = Test_CC_FitOutCustomCode_DataFactory.getTestProduct('Fine','ROC');
            
            insert testFineProduct; // create the test fine product
            
            Pricing_Line__c testPricingLine = Test_CC_FitOutCustomCode_DataFactory.getTestPricingLine(testFineProduct.Id,'Other');
            
            insert testPricingLine; // create a test pricing line 
            
            /* Create an objection SR */
            
            system.test.startTest();
            
            Service_Request__c testIssueFineRequest 
                = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
            
            testIssueFineRequest.Customer__c = null;
           // testIssueFineRequest.Compliance__c = lstCompliance[0].Id;
            testIssueFineRequest.Type_Of_Request__c = 'Other';
            testIssueFineRequest.Comments__c = 'Test';
            testIssueFineRequest.Fine_Amount_AED__c = 1000;
            testIssueFineRequest.Email__c = 'test.difc@test.org';
            testIssueFineRequest.Is_From_Back_Office__c = true;
            testIssueFineRequest.Building__c = null;
                
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(testIssueFineRequest,'Issue_Fine');
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestStatus(testIssueFineRequest,'Draft',true);
            
            insert testIssueFineRequest;
            
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestStatus(testIssueFineRequest,'Issued',true);
            
            update testIssueFineRequest;
            
            SR_Doc__c objSrDocComplianceDoc = new SR_Doc__c();
            
            objSrDocComplianceDoc.Name='Compliance Calendar';
            objSrDocComplianceDoc.Service_Request__c = testIssueFineRequest.Id;
            
            insert objSrDocComplianceDoc;
            
            Service_Request__c dmSrTest = testIssueFineRequest.clone(false,true,false,false);
            
            dmSrTest.Customer__c = null;
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(dmSrTest,'DM_SR');
            
            Service_Request__c testObjectionSr = testIssueFineRequest.clone(false,true,false,false);
                
            testObjectionSr.Customer__c = null;
            testObjectionSr.Linked_SR__c = testIssueFineRequest.Id;
            Test_CC_FitOutCustomCode_DataFactory.setServiceRequestRecordType(testObjectionSr,'Objection_SR');
            
            insert new List<Service_Request__c>{testObjectionSr,dmSrTest}; // Create an objection SR
            
            /* Create an SR price item */
            SR_Price_Item__c testSrPriceLineItem 
                = Test_CC_FitOutCustomCode_DataFactory.getTestSrPriceItem(testIssueFineRequest.Id,testPricingLine.Id,testFineProduct.Id);
            
            insert testSrPriceLineItem; 
            
            HomeCls objHomeCls = new HomeCls();
            
            objHomeCls.Type = 'Page';
            
            objHomeCls.SRId = testIssueFineRequest.Id;
            
            try {
                objHomeCls.ComplianceCalendarInfo();
            } catch (Exception e){
                System.debug('Error occured >> ' + e.getMessage());
            }
            
          
            
            /*
             * REMOVE AFTER FIT-OUT DEPLOYMENT
             */
            //START OF TEST METHOD TO BE REMOVED - CLAUDE
            try {
                //objHomeCls.RaisObjectionSR();   
            } catch (Exception e){
                System.debug('Error occured >> ' + e.getMessage());
            }
            //END OF TEST METHOD TO BE REMOVED - CLAUDE
            
            objHomeCls.ReGenerateCalendar();    
            
            delete objSrDocComplianceDoc;
            
            testComplianceCalendarInfo(testIssueFineRequest.Id);
            
            system.Test.stopTest();
        }
    }
    
    @future 
    private static void testComplianceCalendarInfo(String srId){
        
        HomeCls objHomeCls = new HomeCls();
      
            
        objHomeCls.Type = 'Page';
        
        objHomeCls.SRId = srId;
        
        try {
            objHomeCls.ComplianceCalendarInfo();    
            objHomeCls.UpdateTax();
            objHomeCls.UpdateTaxNA();
            
              VatController vatCtrl = new VatController();
            
            vatCtrl.UpdateTax();
         
            
        } catch (Exception e){
            System.debug('Error occured >> ' + e.getMessage());
        }
        
    } 
}