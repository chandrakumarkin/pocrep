/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class TestRefundRequestUtils {
    
    
    @testSetUp
    static void PrepareTestData(){
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'Refund';
        objEP.URL__c = 'http://accbal.com/sampledata';
        insert objEP;
        
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'AccountBal';
        objEP.URL__c = 'http://accbal.com/sampledata';
        insert objEP;
        
        Block_Trigger__c objCS = new Block_Trigger__c();
        objCS.Block_Web_Services__c = false;
        objCS.Name = 'Block';
        insert objCS;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        List<SR_Template__c> srTemplateList = new List<SR_Template__c>();
        SR_Template__c srTemplate = new SR_Template__c();
        srTemplate.Name = 'Request a Refund';
        srTemplate.SR_RecordType_API_Name__c = 'Refund_Request';
        srTemplate.SR_Group__c = 'Other';
        srTemplate.Menu__c = 'otherServices';
        srTemplate.sub_Menu__c = 'Administrative Services';
        srTemplateList.add(srTemplate);
        
        SR_Template__c srTemplate1 = new SR_Template__c();
        srTemplate1.Name = 'Request a Refund';
        srTemplate1.SR_RecordType_API_Name__c = 'Request_for_Contractor_Wallet_Refund';
        srTemplate1.SR_Group__c = 'Other';
        srTemplate1.Menu__c = 'otherServices';
        srTemplate1.sub_Menu__c = 'Administrative Services';
        srTemplate1.Menutext__c = 'Request for Wallet Refund';
        srTemplateList.add(srTemplate1);
        
        SR_Template__c srTemplate2 = new SR_Template__c();
        srTemplate2.Name = 'Refund of part of the Security Deposit';
        srTemplate2.SR_RecordType_API_Name__c = 'Refund_of_The_Security_Deposit';
        srTemplate2.SR_Group__c = 'Other';
        srTemplate2.Menu__c = 'otherServices';
        srTemplate2.sub_Menu__c = 'Lease Registration';
        srTemplate2.Menutext__c = 'Lease Registration';
        srTemplateList.add(srTemplate2);
        
        SR_Template__c srTemplate3 = new SR_Template__c();
        srTemplate3.Name = 'Refund of part of the Security Deposit';
        srTemplate3.SR_RecordType_API_Name__c = 'Refund_of_The_Security_Deposit';
        srTemplate3.SR_Group__c = 'Other';
        srTemplate3.Menu__c = 'otherServices';
        srTemplate3.sub_Menu__c = 'Release / Refund of Security Deposit';
        srTemplate3.Menutext__c = 'Release / Refund of Security Deposit';
        srTemplateList.add(srTemplate3);
        insert srTemplateList;
        
        List<Account> accList = new List<Account>();
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 354545';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '1234';
        accList.add(objAccount);
        
        objAccount = new Account();
        objAccount.Name = 'Test Custoer 354544';
        objAccount.E_mail__c = 'test@tests.com';
        objAccount.BP_No__c = '5678';
        accList.add(objAccount);
        
        insert accList;
        List<Service_Request__c> SrList = new List<Service_Request__c>();
        Service_Request__c srobj = new Service_Request__c();
        srobj.Type_of_refund__c ='Portal Balance Refund';
        srobj.Mode_of_refund__c = 'Cheque in Favour of the Entity';
        srobj.Refund_Amount__c = 10000;
        srobj.I_agree__c = true;
        srobj.Customer__c = [select id from account limit 1].id;
        srObj.SR_template__c = srTemplate.id;
        srObj.RecordTypeID = [select id from RecordType where DeveloperName = 'Refund_Request' and SobjectType = 'Service_Request__c' limit 1].id;
        SrList.add(srObj); 
        
        Service_Request__c srobj1 = new Service_Request__c();
        //srobj1.Type_of_refund__c ='Refund';
        srobj1.Mode_of_refund__c = 'Wire Transfer to the entity account';
        srobj1.Refund_Amount__c = 10000;
        srobj1.I_agree__c = true; 
        srobj1.Customer__c = [select id from account limit 1].id;
        srobj1.SR_template__c = srTemplate1.id;
        srobj1.Payment_Method__c = 'Cheque';
        srobj1.Declaration_Signatory_Name__c = 'Test Bank';
        srobj1.Address_Details__c =  'Test Address';
        srobj1.SAP_SGUID__c = 'TestName';
        srobj1.SAP_OSGUID__c = 'Test123';
        srobj1.SAP_GSTIM__c = 'Test456';
        srobj1.SAP_GSDAT__c = 'Test789';
        srobj1.RecordTypeID = [select id from RecordType where DeveloperName = 'Request_for_Contractor_Wallet_Refund' and SobjectType = 'Service_Request__c' limit 1].id;
        SrList.add(srObj1); 
        
        Lookup__c lookup = new Lookup__c();
        lookup.Name = 'test';
        lookup.Code__c = '0001';
        lookup.Type__c = 'Developer';
        insert lookup;
        
        Building__c building = new Building__c();
        building.Name = 'Central Park';
        building.SAP_Building_No__c = '00000001';
        building.Developer_Lookup__c = lookup.Id;
        insert building;
        
        Unit__c Unit = new Unit__c();
        Unit.Name = '24';
        Unit.Floor__c = '2';
        Unit.SAP_Unit_No__c = '00000356';
        Unit.Building__c = building.Id;
        Insert Unit;
        
        Service_Request__c srobj2 = new Service_Request__c();
        //srobj2.Type_of_refund__c ='Refund';
        srobj2.Mode_of_refund__c = 'Wire Transfer to the entity account';
        srobj2.Refund_Amount__c = 10000;
        srobj2.I_agree__c = true; 
        srobj2.Customer__c = [select id from account limit 1].id;
        srobj2.SR_template__c = srTemplate2.id;
        srobj2.Payment_Method__c = 'Cheque';
        srobj2.Declaration_Signatory_Name__c = 'Test Bank';
        srobj2.Address_Details__c =  'Test Address';
        srobj2.SAP_SGUID__c = 'TestName';
        srobj2.SAP_OSGUID__c = 'Test123';
        srobj2.SAP_GSTIM__c = 'Test456';
        srobj2.SAP_GSDAT__c = 'Test789';
        srObj2.Share_Value__c = 1000;
        srObj2.Type_of_Request__c = 'Refund of part of the Security Deposit';
        srObj2.Lease_Contract_No__c = 'Test';
        srObj2.Middle_Name_Previous_Sponsor__c = 'Test Middle Name';
        srObj2.Address_Details__c = 'Test Address';
        srObj2.Authority_Name__c = 'Test Authority';
        srObj2.Tax_Registration_Number_TRN__c = '123';
        srObj2.Security_Deposit_AED__c = 1000;
        srObj2.Sponsor_Street__c = 'Test Street';
        srObj2.Cage_No__c = 'Test Cage Number';
        srObj2.Bank_Address__c = 'Bank Address';
        srObj2.IBAN_Number__c = '12345';
        //srObj2.Unit__c = Unit.Id;
        srobj2.RecordTypeID = [select id from RecordType where DeveloperName = 'Refund_of_The_Security_Deposit' and SobjectType = 'Service_Request__c' limit 1].id;
        SrList.add(srobj2);
        
        Service_Request__c srobj3 = new Service_Request__c();
        //srobj3.Type_of_refund__c ='Refund';
        srobj3.Mode_of_refund__c = 'Wire Transfer to the entity account';
        srobj3.Refund_Amount__c = 10000;
        srobj3.I_agree__c = true; 
        srobj3.Customer__c = [select id from account limit 1].id;
        srobj3.SR_template__c = srTemplate3.id;
        srobj3.Payment_Method__c = 'Cash';
        srobj3.Declaration_Signatory_Name__c = 'Test Bank';
        srobj3.Address_Details__c =  'Test Address';
        srobj3.SAP_SGUID__c = 'TestName';
        srobj3.SAP_OSGUID__c = 'Test123';
        srobj3.SAP_GSTIM__c = 'Test456';
        srobj3.SAP_GSDAT__c = 'Test789';
        srobj3.Share_Value__c = 1000;
        srobj3.Type_of_Request__c = 'Release of Security Deposit';
        srobj3.Lease_Contract_No__c = 'Test';
        srobj3.Middle_Name_Previous_Sponsor__c = 'Test Middle Name';
        srobj3.Address_Details__c = 'Test Address';
        srobj3.Authority_Name__c = 'Test Authority';
        srobj3.Tax_Registration_Number_TRN__c = '123';
        srobj3.Security_Deposit_AED__c = 1000;
        srobj3.Sponsor_Street__c = 'Test Street';
        srobj3.Cage_No__c = 'Test Cage Number';
        srobj3.Bank_Address__c = 'Bank Address';
        srobj3.IBAN_Number__c = '12345';
        //srobj3.Unit__c = Unit.Id;
        srobj3.RecordTypeID = [select id from RecordType where DeveloperName = 'Refund_Request' and SobjectType = 'Service_Request__c' limit 1].id;
        SrList.add(srobj3);
        Insert SrList;
        
        Amendment__c amd = new Amendment__c();
        amd.ServiceRequest__c = srobj2.Id;
        amd.Amendment_Type__c = 'Unit';
        amd.Unit__c = Unit.Id;
        amd.Address1__c = 'Test 1234';
        Insert amd;
        
        Amendment__c amd1 = new Amendment__c();
        amd1.ServiceRequest__c = srobj3.Id;
        amd1.Amendment_Type__c = 'Unit';
        amd1.Unit__c = Unit.Id;
        amd1.Address1__c = 'Test 1234';
        Insert amd1;
        
        Step_Template__c stpTemp = new Step_Template__c();
        stpTemp.Name = 'HOD Review';
        stpTemp.Code__c = 'HOD_Review';
        //stpTemp.SR_Template__c =  srTemplate.id;
        stpTemp.Step_RecordType_API_Name__c = 'General';
        insert stpTemp;
        
        List<Step__c> listStep = new List<Step__c>();
        Step__c stepSr = new Step__c();
        stepSr.SR__c = SrObj2.Id;
        stepSr.Step_Template__c = stpTemp.id;
        listStep.add(stepSr);
        
        SR_Steps__c srStp = new SR_Steps__c();
        srStp.SR_Template__c = srTemplate.id;
        srStp.Step_Template__c = stpTemp.id;
        insert srStp;
        Status__c stts = new Status__c();
        stts.Code__c = 'CLOSED';
        stts.Name = 'closed';
        insert stts;
        
        status__c st = new status__c();
        st.Name ='Approved';
        st.Code__c = 'Approved';
        insert st;
        
        SR_Status__c srStatus = new SR_Status__c();
        srStatus.Name = 'Submitted';
        srStatus.Code__c= 'SUBMITTED';
        insert srStatus;
        
        step__c stp = new step__c();
        stp.sr__c = srObj.id;
        //  stp.step_Name__c = 'HOD Review';
        stp.Step_Template__c = stpTemp.id;
        stp.Applicant_Email__c = 'c-sravan.Booragadda@difc.ae';
        stp.status__c = st.id;
        stp.Closed_date_time__c = system.now();
        listStep.add(stp);
        insert listStep;
        
        Document_Master__c dm = new Document_Master__c();
        dm.Name = 'Commercial License';
        dm.Code__c = 'Commercial Licence';
        insert dm;
        
        
        SR_Doc__c srd = new SR_Doc__c();
        srd.Name = 'Commercial License';
        srd.Document_Master__c = dm.id;
        srd.Service_Request__c = srobj.id;
        insert srd;
        
        attachment att = new attachment();
        att.Name = 'test';
        att.body = blob.valueOf('dumy text');
        att.parentId = srd.id;
        insert att;
        
        
        Email_Addresses__c ea = new Email_Addresses__c();
        ea.Name = 'Refund Emails To Finance';
        ea.To_Addresses__c = 'test@test.com;test1@test.com';
        insert ea;
        
        /* OrgWideEmailAddress  owea = new OrgWideEmailAddress();
owea.Address = 'portal@difc.ae';
insert owea; */
        
        st = new status__c();
        st.Name ='ERRORED_FROM_SAP';
        st.Code__c = 'ERRORED_FROM_SAP';
        insert st;     
        
        
    }
    
    
    
    static testMethod void myUnitTest1() {
        
        test.startTest(); 
        service_request__c srObj = [select id,Name,Service_Type__c,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c
                                    from service_request__c Limit 1];
        
        step__c stp = [select id,Name,SR__r.Name,SR__r.Type_of_Refund__c,SR__r.Refund_Amount__c,SR__r.Mode_of_Refund__c,SR__r.Customer__r.BP_No__c,SR__r.Bank_Name__c,
                       SR__r.Bank_Address__c,SR__r.SAP_SGUID__c,SR__r.SAP_OSGUID__c,SR__r.IBAN_Number__c,SR__r.SAP_GSTIM__c,SR__r.first_Name__c,SR__r.Last_Name__c,SR__r.SAP_Unique_No__c,
                       SR__r.Contact__r.BP_No__c,SR__r.SAP_MATNR__c,SR__r.Transfer_to_Account__r.BP_No__c,SR__r.Transfer_to_Account__c from step__c where sr__c =: srObj.id];                                     
        
        string stpSerialized = JSon.Serialize(stp);
        
        Test.setMock(WebServiceMock.class, new TestSAPRefundRequestServiceMock());
        SAPPortalBalanceRefundService.ZSF_S_REFUND_OP ZSFRefundOp = new SAPPortalBalanceRefundService.ZSF_S_REFUND_OP();
        ZSFRefundOp.RENUM = srObj.Name;
        ZSFRefundOp.SGUID = srObj.id;
        ZSFRefundOp.RFTYP = 'PBAL';       
        ZSFRefundOp.RFMOD = 'CHCO';
        ZSFRefundOp.RFAMT = srObj.Refund_Amount__c.toPlainString();  // Refund Amount
        ZSFRefundOp.KUNNR = srObj.customer__r.BP_No__c;
        ZSFRefundOp.RSTAT ='S';
        ZSFRefundOp.SFMSG = '';
        TestSAPRefundRequestServiceMock.items.add(ZSFRefundOp);
        Test.StopTest();
        
        RefundRequestUtils.PushToSAP(srObj,srObj.Type_of_Refund__c);
        
        
        srObj.Type_of_Refund__c = 'PSA Refund';
        RefundRequestUtils.PushToSAP(srObj,null);
        
        RefundRequestUtils.getPSAAmount(srObj.customer__c);
        RefundRequestUtils.getRefundAmount(srObj.customer__c);
        
        srObj.Type_of_Refund__c = 'PSA Transfer';
        RefundRequestUtils.PushToSAP(srObj, srObj.Type_of_Refund__c);
        
        srObj.Type_of_Refund__c = 'Portal Balance Refund';
        srobj.Mode_of_refund__c ='Credit to the portal Account';
        RefundRequestUtils.PushToSAP(srObj, srObj.Type_of_Refund__c);
        
        srObj.Type_of_Refund__c = 'Portal Balance Refund';
        srobj.Mode_of_refund__c ='Cheque in favour of another Individual';
        RefundRequestUtils.PushToSAP(srObj, srObj.Type_of_Refund__c);
        
        srObj.Type_of_Refund__c = 'Portal Balance Refund';
        srobj.Mode_of_refund__c ='Cheque in favour of another Individual';
        RefundRequestUtils.PushToSAP(srObj, srObj.Type_of_Refund__c);
        
        srObj.Type_of_Refund__c = 'Portal Balance Refund';
        srobj.Mode_of_refund__c ='Cheque in favour of another Individual';
        RefundRequestUtils.PushToSAP(srObj, srObj.Type_of_Refund__c);
        
        srObj.Type_of_Refund__c = 'Portal Balance Refund';
        srobj.Mode_of_refund__c ='Cheque in favour of another Entity';
        RefundRequestUtils.PushToSAP(srObj, srObj.Type_of_Refund__c);
        
        srObj.Type_of_Refund__c = 'Portal Balance Refund';
        srobj.Mode_of_refund__c ='Wire Transfer to the entity account';
        RefundRequestUtils.PushToSAP(srObj, srObj.Type_of_Refund__c);
        
        srObj.Type_of_Refund__c = 'Portal Balance Refund';
        srobj.Mode_of_refund__c ='Wire Transfer to the account of another entity';
        RefundRequestUtils.PushToSAP(srObj, srObj.Type_of_Refund__c);
        
        
        srObj.Type_of_Refund__c = 'Portal Balance Refund';
        srobj.Mode_of_refund__c ='Wire Transfer to the account of another Individual';
        RefundRequestUtils.PushToSAP(srObj, srObj.Type_of_Refund__c);
        
        
        
        srObj.Type_of_Refund__c = 'PSA Refund';
        srobj.Mode_of_refund__c ='Credit to the portal Account';
        RefundRequestUtils.PushToSAP(srObj,null);
        
        srObj.Type_of_Refund__c = 'PSA Refund';
        srobj.Mode_of_refund__c ='Cheque in favour of another Individual';
        RefundRequestUtils.PushToSAP(srObj,null);
        
        srObj.Type_of_Refund__c = 'PSA Refund';
        srobj.Mode_of_refund__c ='Cheque in favour of another Individual';
        RefundRequestUtils.PushToSAP(srObj,null);
        
        srObj.Type_of_Refund__c = 'PSA Refund';
        srobj.Mode_of_refund__c ='Cheque in favour of another Individual';
        RefundRequestUtils.PushToSAP(srObj,null);
        
        srObj.Type_of_Refund__c = 'PSA Refund';
        srobj.Mode_of_refund__c ='Cheque in favour of another Entity';
        RefundRequestUtils.PushToSAP(srObj, null);
        
        srObj.Type_of_Refund__c = 'PSA Refund';
        srobj.Mode_of_refund__c ='Wire Transfer to the entity account';
        RefundRequestUtils.PushToSAP(srObj,null);
        
        srObj.Type_of_Refund__c = 'PSA Refund';
        srobj.Mode_of_refund__c ='Wire Transfer to the account of another entity';
        RefundRequestUtils.PushToSAP(srObj,null);
        
        
        srObj.Type_of_Refund__c = 'PSA Refund';
        srobj.Mode_of_refund__c ='Wire Transfer to the account of another Individual';
        RefundRequestUtils.PushToSAP(srObj,null);       
        
        
        srObj.Type_of_Refund__c = 'PSA Transfer';
        // srobj.Mode_of_refund__c ='Wire Transfer to the account of another Individual';
        RefundRequestUtils.PushToSAP(srObj,null);       
        
        
        srObj.Type_of_Refund__c = 'Portal Balance Transfer';
        // srobj.Mode_of_refund__c ='Wire Transfer to the account of another Individual';
        RefundRequestUtils.PushToSAP(srObj,null); 
        
        

    }
    
    
    
    static testMethod void MyTestMethod2(){
        test.startTest(); 
        service_request__c srObj = [select id,Name,Service_Type__c,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c
                                    from service_request__c Limit 1];
        
        step__c stp = [select id,Name,SR__r.Name,SR__r.Type_of_Refund__c,SR__r.Refund_Amount__c,SR__r.Mode_of_Refund__c,SR__r.Customer__r.BP_No__c,SR__r.Bank_Name__c,
                       SR__r.Bank_Address__c,SR__r.SAP_SGUID__c,SR__r.SAP_OSGUID__c,SR__r.IBAN_Number__c,SR__r.SAP_GSTIM__c,SR__r.first_Name__c,SR__r.Last_Name__c,SR__r.SAP_Unique_No__c,
                       SR__r.Contact__r.BP_No__c,SR__r.SAP_MATNR__c,SR__r.Transfer_to_Account__r.BP_No__c,SR__r.Transfer_to_Account__c from step__c where sr__c =: srObj.id];                                     
        
        string stpSerialized = JSon.Serialize(stp);
        
        Test.setMock(WebServiceMock.class, new TestSAPRefundRequestServiceMock());
        SAPPortalBalanceRefundService.ZSF_S_REFUND_OP ZSFRefundOp = new SAPPortalBalanceRefundService.ZSF_S_REFUND_OP();
        ZSFRefundOp.RENUM = srObj.Name;
        ZSFRefundOp.SGUID = srObj.id;
        ZSFRefundOp.RFTYP = 'PBAL';       
        ZSFRefundOp.RFMOD = 'CHCO';
        ZSFRefundOp.RFAMT = srObj.Refund_Amount__c.toPlainString();  // Refund Amount
        ZSFRefundOp.KUNNR = srObj.customer__r.BP_No__c;
        ZSFRefundOp.RSTAT ='S';
        ZSFRefundOp.SFMSG = '';
        TestSAPRefundRequestServiceMock.items.add(ZSFRefundOp);
        
        HodPreCheckRefund_QC Qc = new HodPreCheckRefund_QC(srObj.Id,stp .Id,'HOD Review');
        System.enqueueJob(Qc);
        Test.StopTest();
        
        RefundRequestUtils.PushToSAP(srObj,srObj.Type_of_Refund__c);
        string val = label.PSA_Refund_Logic_Check;
        
        srObj.Type_of_Refund__c = 'PSA Refund';
        RefundRequestUtils.PushToSAP(srObj,null);
        RefundRequestUtils.PushToSAPAsync(stpSerialized,null);
        RefundRequestUtils.getPSAAmount(srObj.customer__c);
        RefundRequestUtils.getRefundAmount(srObj.customer__c);
        
        srObj.Type_of_Refund__c = 'PSA Transfer';
        RefundRequestUtils.PushToSAPAsync(stpSerialized,null);
        
        srObj.Type_of_Refund__c = 'Portal Balance Refund';
        srobj.Mode_of_refund__c ='Credit to the portal Account';
        RefundRequestUtils.PushToSAPAsync(stpSerialized,null);
        
        //   RefundRequestUtils.sendEmailToFinance(stpSerialized,'Portal Balance Refund');
        
        
        
        
        
        
    }
    
    
    static testmethod void myunittest3(){
        
        
        test.startTest(); 
        service_request__c srObj = [select id,Service_Type__c,Name,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c
                                    from service_request__c Limit 1];
        
        
        
        
        Test.setMock(WebServiceMock.class, new TestSAPRefundRequestServiceMock());
        SAPPortalBalanceRefundService.ZSF_S_REFUND_OP ZSFRefundOp = new SAPPortalBalanceRefundService.ZSF_S_REFUND_OP();
        ZSFRefundOp.RENUM = srObj.Name;
        ZSFRefundOp.SGUID = srObj.id;
        ZSFRefundOp.RFTYP = 'PBAL';       
        ZSFRefundOp.RFMOD = 'CHCO';
        ZSFRefundOp.RFAMT = srObj.Refund_Amount__c.toPlainString();  // Refund Amount
        ZSFRefundOp.KUNNR = srObj.customer__r.BP_No__c;
        ZSFRefundOp.RSTAT ='S';
        ZSFRefundOp.SFMSG = '';
        TestSAPRefundRequestServiceMock.items.add(ZSFRefundOp);
        Test.StopTest();
        RefundRequestUtils.createLineManagerForRejection(new List<id>{srObj.id});
        
        
        
    }
    
    static testmethod void MyUnitTest4(){
        
        test.startTest();
        Test.setMock(WebServiceMock.class, new TestAccountBalanceServiceCls());
        list<AccountBalenseService.ZSF_S_ACC_BAL> resActBals = new list<AccountBalenseService.ZSF_S_ACC_BAL>();
        AccountBalenseService.ZSF_S_ACC_BAL objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '1234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new AccountBalenseService.ZSF_S_ACC_BAL();
        resActBals.add(objActBal);
        
        TestAccountBalanceServiceCls.resActBals = resActBals;       
        test.StopTest();
        
        
        service_request__c srObj = [select id,Name,Service_Type__c,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c
                                    from service_request__c Limit 1];
        
        
        apexpages.currentPage().getParameters().put('id',srObj.id);
        apexpages.currentPage().getParameters().put('typ',srObj.Type_of_Refund__c);
        EmailToFinance_Refund er = new EmailToFinance_Refund();
        er.fetchsrinfO();
        
        
        
    }
    
    
    static testmethod Void MyTestMethod4(){
        AsyncSAPPortalBalanceRefundService asyncObj = new AsyncSAPPortalBalanceRefundService();
        AsyncSAPPortalBalanceRefundService.Asyncrefund asyncRef = new AsyncSAPPortalBalanceRefundService.Asyncrefund();
        AsyncSAPPortalBalanceRefundService.Z_SF_REFUND_PROCESSResponse_elementFuture fut = new AsyncSAPPortalBalanceRefundService.Z_SF_REFUND_PROCESSResponse_elementFuture(); 
        try{
            System.Continuation continuation;
            SAPPortalBalanceRefundService.ZSF_TT_REFUND rf = new SAPPortalBalanceRefundService.ZSF_TT_REFUND();
            asyncRef.beginZ_SF_REFUND_PROCESS(continuation,rf); 
        }catch(exception e){
            
            
        }
        
    }
    
    static testmethod Void MyTestMethod5(){
        SAPPortalBalanceRefundService rf = new SAPPortalBalanceRefundService ();
        SAPPortalBalanceRefundService.ZSF_S_REFUND_OP rfndop = new SAPPortalBalanceRefundService.ZSF_S_REFUND_OP();
        SAPPortalBalanceRefundService.ZSF_TT_REFUND  rfndop1 = new SAPPortalBalanceRefundService.ZSF_TT_REFUND();
        SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP rfndop2 = new SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP();
        SAPPortalBalanceRefundService.Z_SF_REFUND_PROCESS_element  rfndop3 = new SAPPortalBalanceRefundService.Z_SF_REFUND_PROCESS_element();
        SAPPortalBalanceRefundService.ZSF_S_REFUND rfndop4  = new SAPPortalBalanceRefundService.ZSF_S_REFUND();
        SAPPortalBalanceRefundService.Z_SF_REFUND_PROCESSResponse_element rfndop5 = new SAPPortalBalanceRefundService.Z_SF_REFUND_PROCESSResponse_element();   
        
    }
    
    static testmethod void MyUnitTest6(){
        service_request__c srObj = [select id,Service_Type__c,Name,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c
                                    from service_request__c Limit 1];  
        
        test.startTest();     
        
        Test.setMock(WebServiceMock.class, new TestSAPRefundRequestServiceMock());
        SAPPortalBalanceRefundService.ZSF_S_REFUND_OP ZSFRefundOp = new SAPPortalBalanceRefundService.ZSF_S_REFUND_OP();
        ZSFRefundOp.RENUM = srObj.Name;
        ZSFRefundOp.SGUID = srObj.id;
        ZSFRefundOp.RFTYP = 'PBAL';       
        ZSFRefundOp.RFMOD = 'CHCO';
        ZSFRefundOp.RFAMT = srObj.Refund_Amount__c.toPlainString();  // Refund Amount
        ZSFRefundOp.KUNNR = srObj.customer__r.BP_No__c;
        ZSFRefundOp.RSTAT ='E';
        ZSFRefundOp.SFMSG = '';
        TestSAPRefundRequestServiceMock.items.add(ZSFRefundOp);
        
        
        
        database.ExecuteBatch(new RefundrequestsFailedWebserviceBatch());
        Test.StopTest();
        
        
    }
    
    
    static testmethod void MyUnitTest7(){
        
        
        
        
        
        service_request__c srObj = [select id,Name,Service_Type__c,Mobile_Number__c,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c
                                    from service_request__c Limit 1];
        
        
        srObj.RecordTypeID = [select id from RecordType where DeveloperName = 'DIFC_Sponsorship_Visa_New' and SobjectType = 'Service_Request__c' limit 1].id;  
        srObj.Event_Name__c = 'Cancellation';
        update srObj;
        test.startTest();     
        
        Test.setMock(WebServiceMock.class, new TestSAPRefundRequestServiceMock());
        SAPPortalBalanceRefundService.ZSF_S_REFUND_OP ZSFRefundOp = new SAPPortalBalanceRefundService.ZSF_S_REFUND_OP();
        ZSFRefundOp.RENUM = srObj.Name;
        ZSFRefundOp.SGUID = srObj.id;
        ZSFRefundOp.RFTYP = 'PBAL';       
        ZSFRefundOp.RFMOD = 'CHCO';
        ZSFRefundOp.RFAMT = srObj.Refund_Amount__c.toPlainString();  // Refund Amount
        ZSFRefundOp.KUNNR = srObj.customer__r.BP_No__c;
        ZSFRefundOp.RSTAT ='S';
        ZSFRefundOp.SFMSG = '';
        TestSAPRefundRequestServiceMock.items.add(ZSFRefundOp);
        
        
        
        database.ExecuteBatch(new RefundrequestsFailedWebserviceBatch());
        
        
        Test.StopTest();   
        
        
        
        
    }
    
    static testMethod void myUnitTest8() {
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new TestSAPRefundRequestServiceMock());
        
        service_request__c srObj = [select id,Name,Service_Type__c,Type_of_Refund__c,Refund_Amount__c,Declaration_Signatory_Name__c,Address_Details__c,Payment_Method__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,SAP_GSDAT__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c
                                    from service_request__c WHERE Service_Type__c ='Request for Wallet Refund' Limit 1];
        
        SAPPortalBalanceRefundService.ZSF_S_REFUND_OP ZSFRefundOp = new SAPPortalBalanceRefundService.ZSF_S_REFUND_OP();
        ZSFRefundOp.RENUM = srObj.Name;
        ZSFRefundOp.SGUID = srObj.id;
        ZSFRefundOp.RFTYP = 'PBAL';       
        ZSFRefundOp.RFMOD = 'CHCO';
        ZSFRefundOp.RFAMT = srObj.Refund_Amount__c.toPlainString();  // Refund Amount
        ZSFRefundOp.KUNNR = srObj.customer__r.BP_No__c;
        ZSFRefundOp.RSTAT ='S';
        ZSFRefundOp.SFMSG = '';
        TestSAPRefundRequestServiceMock.items.add(ZSFRefundOp);
        Test.StopTest();
        RefundRequestUtils.PushToSAP(srObj, 'FCWR');
        RefundRequestUtils.updateSRStatus('SUBMITTED', srObj);
        
    }
    
        static testMethod void myUnitTest9() {
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new TestSAPRefundRequestServiceMock());
        
        service_request__c srObj = [select id,Name,Service_Type__c,Type_of_Refund__c,Refund_Amount__c,Declaration_Signatory_Name__c,Address_Details__c,Payment_Method__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,SAP_GSDAT__c,Type_of_Request__c, Authority_Name__c, Tax_Registration_Number_TRN__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Share_Value__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c,Unit__r.Parent_Unit__c,Unit__r.Parent_Unit__r.SAP_Unit_No__c,Unit__r.SAP_Unit_No__c,Sponsor_Street__c,
                                    Lease_Contract_No__c,Middle_Name_Previous_Sponsor__c, Cage_No__c, Sponsor_Last_Name__c, Post_Code__c, Security_Deposit_AED__c,Unit__r.Name,Building__r.Name,Building__r.Developer_Lookup__r.Code__c, Building__r.SAP_Building_No__c,
                                    (select Id, Floor__c, Unit__r.Name, Unit__r.Building__r.name, Unit__r.Building__r.SAP_Building_No__c, Address1__c,Unit__r.Building__r.Developer_Lookup__r.Code__c, Unit__r.Parent_Unit__r.SAP_Unit_No__c, Unit__r.SAP_Unit_No__c from Amendments__r where Amendment_Type__c = 'Unit' and Unit__r.Building__r.name != null)
                                    from service_request__c WHERE Service_Type__c ='Lease Registration' Limit 1];
        
        SAPPortalBalanceRefundService.ZSF_S_REFUND_OP ZSFRefundOp = new SAPPortalBalanceRefundService.ZSF_S_REFUND_OP();
        ZSFRefundOp.RENUM = srObj.Name;
        ZSFRefundOp.SGUID = srObj.id;
        ZSFRefundOp.RFTYP = 'PBAL';       
        ZSFRefundOp.RFMOD = 'CHCO';
        ZSFRefundOp.RFAMT = srObj.Refund_Amount__c.toPlainString();  // Refund Amount
        ZSFRefundOp.KUNNR = srObj.customer__r.BP_No__c;
        ZSFRefundOp.RSTAT ='S';
        ZSFRefundOp.SFMSG = '';
        TestSAPRefundRequestServiceMock.items.add(ZSFRefundOp);
        Test.StopTest();
        WebServiceToWalletRefund.refundWalletRorpExpiry(srObj.Id);
        WebServiceToWalletRefund.prepareSAPData(srObj, 'RSDR', 'Tenant');
        WebServiceToWalletRefund.prepareSAPData(srObj, 'RSDR', 'Landlord');
        
    }
    
    static testMethod void myUnitTest10() {
        
        Test.startTest();
        Test.setMock(WebServiceMock.class, new TestSAPRefundRequestServiceMock());
        
        service_request__c srObj = [select id,Name,Service_Type__c,Type_of_Refund__c,Refund_Amount__c,Declaration_Signatory_Name__c,Address_Details__c,Payment_Method__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,SAP_GSDAT__c,Type_of_Request__c, Authority_Name__c, Tax_Registration_Number_TRN__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Share_Value__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c,Unit__r.Parent_Unit__c,Unit__r.Parent_Unit__r.SAP_Unit_No__c,Unit__r.SAP_Unit_No__c,Sponsor_Street__c,
                                    Lease_Contract_No__c,Middle_Name_Previous_Sponsor__c, Cage_No__c, Sponsor_Last_Name__c, Post_Code__c, Security_Deposit_AED__c,Unit__r.Name,Building__r.Name,Building__r.Developer_Lookup__r.Code__c, Building__r.SAP_Building_No__c,
                                    (select Id, Floor__c, Unit__r.Name, Unit__r.Building__r.name, Unit__r.Building__r.SAP_Building_No__c, Address1__c,Unit__r.Building__r.Developer_Lookup__r.Code__c, Unit__r.Parent_Unit__r.SAP_Unit_No__c, Unit__r.SAP_Unit_No__c from Amendments__r where Amendment_Type__c = 'Unit' and Unit__r.Building__r.name != null)
                                    from service_request__c WHERE Service_Type__c = 'Release / Refund of Security Deposit' Limit 1];
        
        SAPPortalBalanceRefundService.ZSF_S_REFUND_OP ZSFRefundOp = new SAPPortalBalanceRefundService.ZSF_S_REFUND_OP();
        ZSFRefundOp.RENUM = srObj.Name;
        ZSFRefundOp.SGUID = srObj.id;
        ZSFRefundOp.RFTYP = 'PBAL';       
        ZSFRefundOp.RFMOD = 'CHCO';
        ZSFRefundOp.RFAMT = srObj.Refund_Amount__c.toPlainString();  // Refund Amount
        ZSFRefundOp.KUNNR = srObj.customer__r.BP_No__c;
        ZSFRefundOp.RSTAT ='S';
        ZSFRefundOp.SFMSG = '';
        TestSAPRefundRequestServiceMock.items.add(ZSFRefundOp);
        Test.StopTest();
        WebServiceToWalletRefund.refundWalletRorpExpiry(srObj.Id);
        WebServiceToWalletRefund.prepareSAPData(srObj, 'RSDR', 'Tenant');
        WebServiceToWalletRefund.prepareSAPData(srObj, 'RSDR', 'Landlord');
        
    }
  
}