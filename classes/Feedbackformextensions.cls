public  class Feedbackformextensions {

    string SRID{get;set;}
    public Feedbackformextensions(ApexPages.StandardController controller) 
    {
    
    
        ObjFeedback=new Feedback__c ();
         Submited=false;
         SRID=apexpages.currentpage().getparameters().get('sr');
         if(string.isblank(SRID))
         {
              Submited=true;
         }
         else
         {
         List<Feedback__c> ListFeed=[select id from Feedback__c where Service_Request__r.Name=:SRID];
         if(!ListFeed.isEmpty())
         {
              Submited=true;
         }
         
         }
         
    }
    
    public boolean Submited{get;set;}
    
    public Feedback__c ObjFeedback{get;set;}
    
   //insert Feedback record  
    public PageReference Submit()
    {
    ObjFeedback.SR_Number__c=SRID;
    List<Service_Request__c> SRList=[select id,Customer__c from Service_Request__c where name=:SRID limit 1];
    if(!SRList.isEmpty())
    {
        ObjFeedback.Account__c=SRList[0].Customer__c ;
        ObjFeedback.Service_Request__c=SRList[0].id;
        insert ObjFeedback;
    }
    
    Submited=true;
    return null;
    
    }
    

}