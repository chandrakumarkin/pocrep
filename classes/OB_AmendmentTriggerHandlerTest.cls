@isTest
public class OB_AmendmentTriggerHandlerTest {
    @testSetup
    static  void createTestData() {
        // create Country_Risk_Scoring__c
        List<Country_Risk_Scoring__c> listCountryRiskScoring = new List<Country_Risk_Scoring__c>();
        listCountryRiskScoring = OB_TestDataFactory.createCountryRiskScorings(1, new List<string> {'Medium'}, new List<Integer> {10}, new List<string> {'Medium'});
        insert listCountryRiskScoring;
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(1, new List<string> {'New_User_Registration'});
        insert createSRTemplateList;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        
        HexaBPM__SR_Status__c objDraftStatus = new HexaBPM__SR_Status__c(Name='Draft',HexaBPM__Code__c='DRAFT');
        insert objDraftStatus;
        
        
        insertNewSRs = OB_TestDataFactory.createSR(9, new List<string> {'New_User_Registration'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Non - financial','Non - financial','Non - financial','Non - financial','Retail','Non - financial','Non - financial','Non - financial'}, 
                                                   new List<string>{'Foundation', 'Audit and Accounting', 'Audit and Accounting', 'Audit and Accounting','Non Profit Incorporated Organisation (NPIO)','Services','Audit and Accounting','Audit and Accounting','Audit and Accounting'}, 
                                                   new List<string>{'Foundation', 'Partnership', 'Partnership', 'Partnership', 'Non Profit Incorporated Organisation (NPIO)','Company','Partnership','Partnership','Partnership'}, 
                                                   new List<string>{'Foundation','General Partnership (GP)','Limited Partnership (LP)', 'Limited Liability Partnership (LLP)','NPIO','Recognized Company','Recognized Limited Partnership (RLP)','Recognized Limited Liability Partnership (RLLP)','Recognized Partnership (RP)'});
        
        insert insertNewSRs;
        system.debug('---->'+insertNewSRs);
        
    }
    
    static testMethod void amendmentTriggerTest() {
    	History_Tracking__c objHT = new History_Tracking__c();
    	objHT.Name = 'Place of Registration';
    	objHT.Field_Label__c = 'Place of Registration';
    	objHT.Object_Name__c = 'HexaBPM_Amendment__c';
    	objHT.Field_Name__c = 'Place_of_Registration__c';
    	insert objHT;
    	
    	
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = [select id from HexaBPM__Service_Request__c];
        
        list<Country_Risk_Scoring__c> listCountryRiskScoring = new list<Country_Risk_Scoring__c>();
        listCountryRiskScoring = [select id from Country_Risk_Scoring__c];
        test.startTest();
        //create HexaBPM_Amendment__c
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        Account acc = [SELECT id FROM Account LIMIT 1]; 
       
        for(HexaBPM_Amendment__c amedTemp :  OB_TestDataFactory.createApplicationAmendment(9, listCountryRiskScoring, insertNewSRs)){
            amedTemp.Entity_Name__c = acc.Id;
            amedTemp.OB_Linked_Multistructure_Applications__c = insertNewSRs[0].Id+','+insertNewSRs[0].Id;
            amedTemp.Place_of_Registration__c =  'test';
            listAmendment.add(amedTemp);
        }
        insert listAmendment;
        
        listAmendment[0].OB_Linked_Multistructure_Applications__c = insertNewSRs[0].Id+','+insertNewSRs[0].Id+','+insertNewSRs[0].Id;
        listAmendment[0].Place_of_Registration__c = 'Dubai';
        update listAmendment;
        delete listAmendment;
        test.stopTest();
    }
    
}