/********************************************************************************************************************************************************
 *  Author   	: Mudasir Wani
 *  Date     	: 04-April-2020
 *  Description	: This class will have the common database functioalities across the DIFC organization
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date       			Updated By    		Description
---------------------------------------------------------------------------------------------------------------------  
V1.0	04-April-2020		Mudasir 			Created this class 
*********************************************************************************************************************************************************/

public without sharing class DIFCDataServiceUtils {
    
	 
	
	/***********************************************************************
	*	Parameter 		:	String value of SObject		: SObjectName
						:	String of fields to query 	: SOQLFieldsToQuery
						:	String of filters			: SOQLFilters 
	*	Return 			:	SOQL in string format 
	*	Created By		:	Mudasir Wani
	*	Created Date	:	31-March-2020
	*************************************************************************/
    /*public static boolean getUserRecordAccess(String SObjectName , String objSOQLFieldsToQuery){
    	String SOQLQueryString = 'Select '+objSOQLFieldsToQuery+' from '+SObjectName+' where UserId =\''+UserInfo.getUserId()+'\'';
    	system.debug('SOQLQueryString---------'+SOQLQueryString);
    	return true;
    }*/
    
    
	/***********************************************************************
	*	Parameter 		:	String value of SObject		: SObjectName
						:	String of fields to query 	: SOQLFieldsToQuery
						:	String of filters			: SOQLFilters 
	*	Return 			:	SOQL in string format 
	*	Created By		:	Mudasir Wani
	*	Created Date	:	31-March-2020
	*************************************************************************/
    public static String prepareQuery(String SObjectName , String objSOQLFieldsToQuery , String onjSOQLFilters){
    	//Consider the case of no where condition 
    	String SOQLQueryString = 'Select '+objSOQLFieldsToQuery+' from '+SObjectName+' where '+onjSOQLFilters;
    	system.debug('SOQLQueryString---------'+SOQLQueryString);
    	return SOQLQueryString;
    }
    /***********************************************************************
	*	Parameter 		:	String value of SObject		: SObjectName
						:	String of fields to query 	: SOQLFieldsToQuery
						:	String of filters			: SOQLFilters 
	*	Return 			:	SOQL in string format 
	*	Created By		:	Mudasir Wani
	*	Created Date	:	31-March-2020
	*************************************************************************/
    public static String prepareQuery(String SObjectName , String objSOQLFieldsToQuery , String onjSOQLFilters , String orderByFields , Integer recordlimit){
    	//Consider the case of no where condition 
    	String SOQLQueryString = 'Select '+objSOQLFieldsToQuery+' from '+SObjectName+' where '+onjSOQLFilters;
    	if(orderByFields != ''){
    		SOQLQueryString = SOQLQueryString + ' order by '+ orderByFields;
    	}
    	if(recordlimit > 0){
    		SOQLQueryString = SOQLQueryString + ' Limit '+ recordlimit;
    	}
    	system.debug('SOQLQueryString---------'+SOQLQueryString);
    	return SOQLQueryString;
    }
    
	/***********************************************************************
	*	Parameter 		:	String of fields to query 	: SOQLFieldsToQuery
						:	String of filters			: SOQLFilters 
	*	Return 			:	List<Relationship__c> 
	*	Created By		:	Mudasir Wani
	*	Created Date	:	31-March-2020 
	************************************************************************/
    public static List<Relationship__c> getRelationshipList(String objSOQLFieldsToQuery , String onjSOQLFilters){
    	String SOQLQueryString = prepareQuery('Relationship__c',objSOQLFieldsToQuery,onjSOQLFilters);
    	List<Relationship__c> relationshipRecords = Database.Query(SOQLQueryString);
    	return relationshipRecords;
    }
    
    /***********************************************************************
	*	Parameter 		:	String of fields to query 	: SOQLFieldsToQuery
						:	String of filters			: SOQLFilters 
	*	Return 			:	List<Service_Request__c> 
	*	Created By		:	Mudasir Wani
	*	Created Date	:	31-March-2020
	************************************************************************/
    public static List<Service_Request__c> getServiceRequestList(String objSOQLFieldsToQuery , String objSOQLFilters){
    	String SOQLQueryString = prepareQuery('Service_Request__c',objSOQLFieldsToQuery,objSOQLFilters);
    	List<Service_Request__c> relationshipRecords = Database.Query(SOQLQueryString);
    	return relationshipRecords;
    }
    /***********************************************************************
	*	Parameter 		:	String of fields to query 	: SOQLFieldsToQuery
						:	String of filters			: SOQLFilters 
	*	Return 			:	List<User> 
	*	Created By		:	Mudasir Wani
	*	Created Date	:	31-March-2020
	************************************************************************/
    /*public static List<User> getUserList(String objSOQLFieldsToQuery , String onjSOQLFilters){
    	String SOQLQueryString = prepareQuery('User',objSOQLFieldsToQuery,onjSOQLFilters);
    	List<User> relationshipRecords = Database.Query(SOQLQueryString);
    	return relationshipRecords;
    }*/
    
    /***********************************************************************
	*	Parameter 		:	String of fields to query 	: SOQLFieldsToQuery
						:	String of filters			: SOQLFilters 
	*	Return 			:	List<Contact> 
	*	Created By		:	Mudasir Wani
	*	Created Date	:	31-March-2020
	************************************************************************/
    /*public static List<Contact> getContactList(String objSOQLFieldsToQuery , String onjSOQLFilters){
    	String SOQLQueryString = prepareQuery('Contact',objSOQLFieldsToQuery,onjSOQLFilters);
    	List<Contact> contactList = Database.Query(SOQLQueryString);
    	return contactList;
    }*/
    /***********************************************************************
	*	Parameter 		:	String of fields to query 	: SOQLFieldsToQuery
						:	String of filters			: SOQLFilters 
	*	Return 			:	List<Step> 
	*	Created By		:	Mudasir Wani
	*	Created Date	:	26-June-2020
	************************************************************************/
    public static List<Step__c> getStepList(String objSOQLFieldsToQuery , String onjSOQLFilters ,String orderByFields ,Integer recordlimit){
    	String SOQLQueryString = prepareQuery('Step__c',objSOQLFieldsToQuery,onjSOQLFilters,orderByFields,recordlimit);
    	List<Step__c> stepList = Database.Query(SOQLQueryString);
    	return stepList;
    }
    
}