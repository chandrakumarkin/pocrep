/*
    Date        : 1-Jan-2020
    Description : Check validation for TR check object
* 				  if case number id != null and case status is not  close then wont allow to approve the compliance step
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_ThomsonReutersCheckValidation implements HexaBPM.iCustomCodeExecutable {
	
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        Boolean isTRStatusOpen = false;
        if(stp !=null && stp.HexaBPM__SR__c != null ){
        	for(Thompson_Reuters_Check__c TRchk:[Select ID from Thompson_Reuters_Check__c 
        										Where HexaBPM_ServiceRequest__c =: stp.HexaBPM__SR__c
        										AND TRCase_No__c != null
        										AND TR_Case_Status__c !=: system.label.TR_Case_Status 
        										order by createddate
        										Limit 1]){
        		isTRStatusOpen = true;
        	}
        }
        if(isTRStatusOpen){
        	strResult = 'Please close all open TR cases before approval';
        }
        return strResult;
    }
}