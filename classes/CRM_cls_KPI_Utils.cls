/******************************************************************************************
*  Name        : CRM_cls_KPI_Utils
*  Author      : Claude Manahan
*  Company     : NSI JLT
*  Date        : 2016-11-2
*  Description : Utility class for KPI Module of CRM
----------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
----------------------------------------------------------------------------------------
V1.0   02-11-2016   Claude        Created
V1.1   25-01-2017   Claude        Added new counter for RM Meetings (#3858)
V1.2   16-03-2017   Claude        Moved Task/Event update logic to code
V1.3   26-03-2017   Claude        Moved KPI Logic from other classes to KPI Utils Class; Added logic for checking activities assigned to multiple users
V1.4   17-05-2017   Claude        Added initialization of year and KPI counters for new accounts
V1.5   15-06-2017   Claude        Changed mapping from Sector Classification to BD Sector on Account
V1.6   08-08-2017   Claude        Added check for record types for email tasks
*******************************************************************************************/
public without sharing class CRM_cls_KPI_Utils {
    
    /**
     * Insert/Updates the KPI Records
     * @params      kpiRecords      The KPI records
     */
    public static Set<String> updateKpiRecords(List<KPI__c> kpiRecords){
        
        upsert kpiRecords;
        
        Set<String> kpiIds = new Set<String>();
        
        for(Kpi__c k : kpiRecords) kpiIds.add(k.Id);
        
        List<KPI__c> kpiRecordsIndividual = [SELECT Id, Is_KPI_Met__c, Year__c FROM KPI__c WHERE Id in: kpiIds];
        
        for(KPI__c k : kpiRecordsIndividual) k.Is_Current_KPI__c = (k.Year__c == System.Today().year() && k.Is_KPI_Met__c) ? 'TRUE' : 'FALSE';
        
        update kpiRecordsIndividual;
        
        return kpiIds;
    }
    
    /**
     * Updates the affiliate KPI records
     */ 
    public static void updateAffiliateKPI(Set<String> kpiIdsBatched){
        
        /* Create a map for each account per KPI */
        Map<Id,Boolean> kpiMetMap = new Map<Id,Boolean>();
        Map<id,KPI__c> kpiMap = new Map<id,Kpi__c>();
        
        List<KPI__c> kpiRecords = [SELECT Id, Related_Account__c, Is_Current_KPI__c, Is_KPI_Met__c, Is_Affiliate_KPI_Met__c, Is_Any_KPI_Met__c, Year__c FROM KPI__c WHERE id in :kpiIdsBatched];
        
        /* This will store the KPI records from other years */
        List<KPI__c> kpisOfPrevYears = new List<KPI__c>();
        Set<String> kpiIds = new Set<String>();
        
        for(KPI__c k : kpiRecords){
            kpiMap.put(k.Related_Account__c,k);
            kpiIds.add(k.Id);
        }
        
        /* Get the parent affiliations per account */
        //V1.1 - Claude
        //List<Relationship__c> lstRelationships = [SELECT ID, Object_Account__C, Subject_Account__c FROM Relationship__c WHERE Object_Account__C IN :kpiMetMap.keySet() AND Active__c = TRUE AND Relationship_Type__c = 'Has Affiliates with Same Management'];
        List<Relationship__c> lstRelationships = [SELECT ID, Active__c, Object_Account__C, Subject_Account__c FROM Relationship__c WHERE (Object_Account__C IN :kpiMap.keySet() OR Subject_Account__c IN :kpiMap.keySet() ) AND Relationship_Type__c = 'Has Affiliates with Same Management']; // V1.1 - Claude - Removed ACTIVE = TRUE filter
        
        /* We have to get all the KPI records, so we need to use both the subject and object account lookups */
        Set<Id> idOfAccounts = new Set<Id>();
        
        for(Relationship__c r : lstRelationships){
            idOfAccounts.add(r.Object_Account__c);
            idOfAccounts.add(r.Subject_Account__c);
        }
        
        List<KPI__c> additionalKPI = [SELECT Id, Related_Account__c, Is_Current_KPI__c, Is_KPI_Met__c, Is_Affiliate_KPI_Met__c, Is_Any_KPI_Met__c, Year__c FROM KPI__c WHERE Related_Account__c in :idOfAccounts AND Id NOT in: kpiIds];
        
        if(!additionalKPI.isEmpty()){
            kpiRecords.addAll(additionalKPI);
        }
        
        for(KPI__c k : kpiRecords){
            
            if(k.Year__c == System.Today().year()){
                kpiMetMap.put(k.Related_Account__c,k.Is_KPI_Met__c || (String.isNotBlank(k.Is_Current_KPI__c) && k.Is_Current_kpi__c.equals('TRUE')) || k.Is_Affiliate_KPI_Met__c);
                kpiMap.put(k.Related_Account__c,k);
            } else {
                kpisOfPrevYears.add(k);
            }
        }
        
        //V1.1 - Claude
        /* Create a map of the relationships per parent account, mapped for each child account */
        Map<String,Map<String,Boolean>> relationshipMap = new Map<String,Map<String,Boolean>>();
        
        /* Create a map of the relationships per child account, mapped for each parent account */
        Map<String,Map<String,Boolean>> relationshipMapParents = new Map<String,Map<String,Boolean>>();  
        //V1.1 - Claude
        
        Map<id,Set<Id>> mapOFParents = new Map<Id,Set<Id>>();
        
        /* Map each of the parents according to their children; this will only apply for KPIs/Tasks/Events under children */
        for(Relationship__c r : lstRelationships){
            
            Set<Id> parentIds = mapOFParents.containsKey(r.Object_Account__C) ? mapOFParents.get(r.Object_Account__C) : new Set<Id>();
            
            parentIds.add(r.Subject_Account__c);
            
            mapOFParents.put(r.Object_account__c,parentIds);
            
            // V1.1 - Claude
            Map<String,Boolean> mappedParentRelStatus = relationshipMap.containsKey(r.Object_Account__c) ? relationshipMap.get(r.Object_Account__c) : new Map<String,Boolean>();
            
            mappedParentRelStatus.put(r.Subject_Account__c,r.Active__c);
            
            relationshipMap.put(r.Object_Account__c,mappedParentRelStatus); 
            // V1.1 - Claude
        }
        
        for(Id i : mapOFParents.keySet()){
            
            Boolean kpiAffiliateMet = false;
            
            Map<String,Boolean> mappedParentRelStatus = relationshipMap.get(i);
            
            /* Check their parent if their KPI is met */
            for(Id i2 : mapOFParents.get(i)){
                
                if(kpiMetMap.containsKey(i2) && mappedParentRelStatus.get(i2) && kpiMetMap.get(i2) && !kpiAffiliateMet) kpiAffiliateMet = true;
                
                if(kpiMap.containsKey(i2)) kpiMap.get(i2).Is_Affiliate_KPI_Met__c = kpiAffiliateMet;
                
            }
            
            if(kpiMap.containsKey(i)) kpiMap.get(i).Is_Affiliate_KPI_Met__c = kpiAffiliateMet;
        }
        
        //V1.1 - Claude
        /* A separate logic has been made for the parents */
        Map<id,Set<Id>> mapOFChildren = new Map<Id,Set<Id>>();
        
        for(Relationship__c r : lstRelationships){
            
            Set<Id> childIds = relationshipMapParents.containsKey(r.Subject_Account__c) ? mapOFParents.get(r.Subject_Account__c) : new Set<Id>();
            
            childIds.add(r.Object_Account__c);
            
            mapOFChildren.put(r.Subject_Account__c,childIds);
            
            Map<String,Boolean> mappedChildrenRelStatus = relationshipMap.containsKey(r.Subject_Account__c) ? relationshipMap.get(r.Subject_Account__c) : new Map<String,Boolean>();
            
            mappedChildrenRelStatus.put(r.Object_Account__c,r.Active__c);
            
            relationshipMap.put(r.Subject_Account__c,mappedChildrenRelStatus);
        }
        
        for(Id i : mapOFChildren.keySet()){
            
            Boolean kpiAffiliateMet = false;
            
            Map<String,Boolean> mappedChildrenRelStatus = relationshipMap.get(i);
            
            /* Check their parent if their KPI is met */
            for(Id i2 : mapOFChildren.get(i)){
                
                if(kpiMetMap.containsKey(i2) && mappedChildrenRelStatus.get(i2) && kpiMetMap.get(i2) && !kpiAffiliateMet) kpiAffiliateMet = true;
                
                if(kpiMap.containsKey(i2)) kpiMap.get(i2).Is_Affiliate_KPI_Met__c = kpiAffiliateMet;
                
            }
            
            if(kpiMap.containsKey(i)) kpiMap.get(i).Is_Affiliate_KPI_Met__c = kpiAffiliateMet;
        }
        
        //V1.1 - Claude
        
        kpiRecords.clear();
        
        kpiRecords.addAll(kpiMap.values());
        kpiRecords.addAll(kpisOfPrevYears);          
        
        for(KPI__c k : kpiRecords) k.Is_Current_KPI__c = (k.Year__c == System.Today().year() && (k.Is_Affiliate_KPI_Met__c || k.Is_KPI_Met__c)) ? 'TRUE' : 'FALSE';
        
        Database.executeBatch(new CRM_cls_AffiliateBatchKPIHandler(kpiRecords));
        
    }
    
   /**
    * Creates the KPI records for newly created
    * account records
    * @params      ids                 List of Account Ids
    */
    @InvocableMethod(label='Create KPI Record' description='Creates a KPI record for a new Account')
    public static void createKPIRecords(List<ID> ids) {
        
        List<Relationship__c> rels = [SELECT Id, Subject_account__c, object_account__c from relationship__c where Active__c = TRUE AND (subject_account__c IN : ids OR object_account__c IN :ids) AND Relationship_Type__c = 'Has Affiliates with Same Management'];
        
        Set<Id> relAccIds = new Set<Id>();
        
        for(Relationship__c r : rels){
            
            if(String.isNotBlank(r.Subject_account__c)) relAccIds.Add(r.Subject_account__c);
            
            if(String.isNotBlank(r.object_account__c)) relAccIds.Add(r.object_account__c);
            
        }
        
        for(Id i : ids) relAccIds.add(i);
        
        List<Id> newIds = new List<Id>();
        
        newIds.addAll(relAccIds);
        
        List<KPI__c> kpiRecords;
        
        /* Get a list of KPI records */
        if(!newIds.isEmpty()) kpiRecords = getKPIRecords(newIds);
            
        if(!kpiRecords.isEmpty()) ID jobID = System.enqueueJob(new QueuableKPIHandler(kpiRecords));
        
    }
    
    /**
    * Updates the Task/Event Details based
    * on its respective Lead/Account
    * account records
    * @params      listOfActivities        List of activities
    * @params      isTask                  Checks if the process has been invoked by task records
    * @params      isBefore                Checks if the process has been executed before the end of a DML
    * @params      isInsert                Checks if the process has been executed during an insert operation
    */
    public static void updateActivityDetails(List<sObject> listOfActivities, Boolean isTask, Boolean isBefore, Boolean isInsert){
        
        try {
        
            Set<Id> outlookTasks = new Set<Id>();
            
            List<sObject> activitiesToUpdate = new List<sObject>();
            
            Id whoId;
            Id whatId;
            Id accountId;
            
            Task taskRecord;
            Event eventRecord;
            
            Boolean runUpdate;
            
            Map<String,Id> activityRecordTypeIds = new Map<String,Id>();
            
            for(RecordType r : [SELECT Id, Name FROM RecordType WHERE Name IN ('Meeting','Email') AND sObjectType = 'Task']){
                activityRecordTypeIds.put(r.Name,r.Id);
            }
            
            for(sObject activityRecord : listOfActivities){
                
                taskRecord = new Task();
                eventRecord = new Event();
                
                if(isTask) taskRecord = (Task) activityRecord;
                else eventRecord = (Event) activityRecord;
                
                if(isTask && String.isNotBlank(taskRecord.Subject) && taskRecord.Subject.contains('Email:') && String.isNotBlank(taskRecord.RecordTypeId) && taskRecord.RecordTypeId.equals(activityRecordTypeIds.get('Meeting'))){
                    taskRecord.RecordTypeId = activityRecordTypeIds.get('Email');
                }
                
                whoId = isTask ? taskRecord.whoId : eventRecord.whoId;
                whatId = isTask ? taskRecord.whatId : eventRecord.whatId;
                accountId = isTask ? taskRecord.accountId : eventRecord.accountId;
                
                if(String.isBlank(whoId) && String.isBlank(whatId) && String.isBlank(accountId)) outlookTasks.add(activityRecord.Id);
                
                runUpdate = isTask ? taskRecord.Run_Trigger_Update__c || taskRecord.Status.equals('Open') : eventRecord.Run_Trigger_Update__c;
                
                if( isBefore && (isInsert || runUpdate) ) activitiesToUpdate.add(activityRecord); 
            }
            
            if(!activitiesToUpdate.isEmpty()) executeActivityDetailsUpdate(activitiesToUpdate,isTask,false);
            
            if(!outlookTasks.isEmpty() && !System.isFuture()) updateActivityDetails(outlookTasks,isTask);
            
            if(!isInsert && !isBefore){
                
                Set<Id> retailEmailIds = new Set<Id>();
                
                for(sObject activityRecord : listOfActivities){
                    
                    taskRecord = new Task();
                    
                    if(isTask) taskRecord = (Task) activityRecord;
                    
                    if(isTask && 
                        taskRecord != null && 
                        String.isNotBlank(taskRecord.RecordTypeId) &&                            // V1.6 - Claude - Added null check
                        taskRecord.RecordTypeId.equals(activityRecordTypeIds.get('Email')) && 
                        !taskRecord.Run_Trigger_Update__c) retailEmailIds.add(taskRecord.Id);
                }
                
                if(!retailEmailIds.isEmpty() && !System.isFuture() && !System.isBatch()) updateRetailEmails(retailEmailIds);
                
            }
        
        } catch(Exception e) {
            
            logKpiError(e);
        }
    }
    
    @future(callout = true)
    private static void updateRetailEmails(Set<Id> recordIds){
        
        List<Task> retailEmails = new list<Task>();
       
        for(Id i : recordIds){
                
            retailEmails.add(new Task(Id = i, Run_Trigger_Update__c = true));
            
        }
        
        if(!test.isRunningTest())update retailEmails;
        
    }
    
    /**
    * Updates the Task/Event Details based
    * on its respective Lead/Account
    * account records
    * @params      listOfActivities        List of activities
    * @params      isTask                  Checks if the process has been invoked by task records
    * @params      isFuture                Checks if the update will be done after all immediate actions has been executed
    */
    private static void executeActivityDetailsUpdate(List<sObject> listOfActivities, Boolean isTask, Boolean isFuture){
        
        Id whoId;
        Id accountId;
        
        Task taskRecord;
        Event eventRecord;
        
        Map<Id,Id> oppAccountMap = new Map<Id,Id>();
        Map<Id,Id> contactLeadMap = new Map<Id,Id>();
        Map<Id,Id> contactAccountMap = new Map<Id,Id>();
        
        Set<Id> oppIds = new Set<Id>();
        Set<Id> leadIds = new Set<Id>();
        Set<Id> contactIds = new Set<Id>();
        Set<Id> accountIds = new Set<Id>();
        
        for(sObject activityRecord : listOfActivities){
            
            taskRecord = new Task();
            eventRecord = new Event();
            
            if(isTask) taskRecord = (Task) activityRecord;
            else eventRecord = (Event) activityRecord;
            
            whoId = isTask ? taskRecord.whoId : eventRecord.whoId;
            
            if(String.isNotBlank(whoId) && whoId.getSobjectType() == Schema.Contact.SObjectType) contactIds.add(whoId);
        }
       
        if(!contactIds.isEmpty()){
            
            for(Contact c : [SELECT Accountid, Related_Lead__c FROM Contact WHERE Id IN :contactIds]){
                
                if(String.isNotBlank(c.Related_Lead__c)){
                    
                    leadIds.add(c.Related_Lead__c);
                    contactLeadMap.put(c.Id,c.Related_Lead__c);
                    
                } else {
                    
                    accountIds.add(c.Accountid);
                    contactAccountMap.put(c.Id,c.AccountId);
                    
                }
            }
            
        }
        
        for(sObject activityRecord : listOfActivities){
            
            taskRecord = new Task();
            eventRecord = new Event();
            
            if(isTask) taskRecord = (Task) activityRecord;
            else eventRecord = (Event) activityRecord;
            
            whoId = isTask ? taskRecord.whoId : eventRecord.whoId;
            accountId = isTask ? (String.isBlank(taskRecord.accountId) ? taskRecord.WhatId : taskRecord.accountId) : (String.isBlank(taskRecord.accountId) ? eventRecord.whatId : eventRecord.accountId);
            
            if(String.isNotBlank(whoId) && whoId.getSobjectType() == Schema.Lead.SObjectType) leadIds.add(whoId);
            else if(String.isNotBlank(accountId) && accountId.getSobjectType() == Schema.Account.SObjectType) accountIds.add(accountId);
            else if(String.isNotBlank(accountId) && accountId.getSobjectType() == Schema.Opportunity.SObjectType) oppIds.add(accountId);
            
        }
        
        if(!oppIds.isEmpty()){
            
            for(Opportunity o : [SELECT AccountId FROM Opportunity WHERE Id in :oppIds]){
                
                accountIds.add(o.AccountId);
                oppAccountMap.put(o.Id,o.AccountId);
                
            }
            
        }
        
        Map<Id,Lead> mappedLeads = new Map<Id,Lead>([SELECT Company_Type__c, BD_Sector_Classification__c, Priority_to_DIFC__c FROM Lead WHERE ID in :leadIds]);
        
        /* V1.5 - Claude - Changed from sector classification to bd sector */
        Map<Id,Account> mappedAccounts = new Map<Id,Account>([SELECT ROC_Status__c, BD_Sector__c, Company_Type__c, Account.RecordType.DeveloperName, Priority_to_DIFC__c FROM Account WHERE Id in :accountIds]);
        
        String priorityToDifc;
        String typeOfAccount;
        String companyType;
        String sectorClassification; 
        
        /* V1.3 - Claude - This will check if the activity is assigned to multiple users, mapped by subject + accountId + dateTimeFrom */ 
        Set<String> activityKeySet = new Set<String>();  
        
        for(sObject activityRecord : listOfActivities){
            
            companyType = '';
            typeOfAccount = '';
            priorityToDifc = '';
            
            taskRecord = new Task();
            eventRecord = new Event();
            
            if(isTask) taskRecord = (Task) activityRecord;
            else eventRecord = (Event) activityRecord;
            
            whoId = isTask ? taskRecord.whoId : eventRecord.whoId;
            
            accountId = null;
            
            if(String.isNotBlank(taskRecord.accountId)) accountId = taskRecord.accountId;
            
            else if(String.isNotBlank(eventRecord.accountId)) accountId = eventRecord.accountId;
            
            else if(contactAccountMap.containsKey(whoId)) accountId = contactAccountMap.get(whoId);
            
            else if(oppAccountMap.containsKey(taskRecord.WhatId)) accountId = oppAccountMap.get(taskRecord.WhatId); 
            
            else if(oppAccountMap.containsKey(eventRecord.WhatId)) accountId = oppAccountMap.get(eventRecord.WhatId);
            
            else if(String.isNotBlank(taskRecord.WhatId) && taskRecord.WhatId.getSobjectType() == Schema.Account.SObjectType) accountId = taskRecord.WhatId;
            
            else if(String.isNotBlank(eventRecord.WhatId) && eventRecord.WhatId.getSobjectType() == Schema.Account.SObjectType) accountId = eventRecord.WhatId;
            
            if(String.isNotBlank(accountId) && accountId.getSobjectType() == Schema.Account.SObjectType && mappedAccounts.containsKey(accountId)){
                
                //V1.3 - Claude - Start
                String subjectKey = isTask && String.isNotBlank(taskRecord.Subject) ? taskRecord.Subject : String.isNotBlank(eventRecord.Subject) ? eventRecord.Subject : 'Blank Subject';
                String dateTimeKey = String.valueOf(isTask ? (taskRecord.Date_Time_From__c == null ? System.Today() : taskRecord.Date_Time_From__c.date()) : (eventRecord.EndDateTime == null ? System.Today() : eventRecord.EndDateTime.date())); 
                String activityKey = subjectKey + accountId + dateTimeKey; 
                
                if(activityKeySet.contains(activityKey) && isTask) taskRecord.Has_Similar_Activity__c = true;
                else if(activityKeySet.contains(activityKey)) eventRecord.Has_Similar_Activity__c = true;
                else activityKeySet.add(activityKey);
                
                //V1.3 - Claude - End
                
                typeOfAccount = String.isNotBlank(mappedAccounts.get(accountId).Roc_Status__c) ? (mappedAccounts.get(accountId).Roc_Status__c.equals('Active') ? 'Client' : mappedAccounts.get(accountId).RecordType.DeveloperName.equals('Event_Customer') ? 'Event Customer' : 'Prospect') : '';
                companyType = String.isNotBlank(mappedAccounts.get(accountId).Company_Type__c) ? mappedAccounts.get(accountId).Company_Type__c : '';
                priorityToDifc = String.isNotBlank(mappedAccounts.get(accountId).Priority_to_DIFC__c) ? mappedAccounts.get(accountId).Priority_to_DIFC__c : '';
                
                //V1.5 - Claude - Start
                //sectorClassification = String.isNotBlank(mappedAccounts.get(accountId).Sector_Classification__c) ? mappedAccounts.get(accountId).Sector_Classification__c : '';
                sectorClassification = String.isNotBlank(mappedAccounts.get(accountId).BD_Sector__c) ? mappedAccounts.get(accountId).BD_Sector__c : '';
                //V1.5 - Claude - End
                
            } else if(String.isNotBlank(whoId)){
                
                whoId = contactLeadMap.containsKey(whoId) ? contactLeadMap.get(whoId) : whoId;
                
                if(mappedLeads.containsKey(whoId)){
                    
                    typeOfAccount = 'Prospect';
                    companyType =  String.isNotBlank(mappedLeads.get(whoId).Company_Type__c) ? mappedLeads.get(whoId).Company_Type__c : '';
                    priorityToDifc = String.isNotBlank(mappedLeads.get(whoId).Priority_to_DIFC__c) ? mappedLeads.get(whoId).Priority_to_DIFC__c : '';
                    sectorClassification = String.isNotBlank(mappedLeads.get(whoId).BD_Sector_Classification__c) ? mappedLeads.get(whoId).BD_Sector_Classification__c : '';
                    
                }
                
            }
            
            if(isTask){
                
                taskRecord.Sector_Classification__c = sectorClassification;
                taskRecord.Type_of_Account__c = typeOfAccount;
                taskRecord.Priority_to_DIFC__c = priorityToDifc;
                taskRecord.Company_Type__c = companyType;
                taskRecord.Run_Trigger_Update__c = false;
                
            } else {
                
                eventRecord.Sector_Classification__c = sectorClassification;
                eventRecord.Type_of_Account__c = typeOfAccount;
                eventRecord.Priority_to_DIFC__c = priorityToDifc;
                eventRecord.Company_Type__c = companyType;
                eventRecord.Run_Trigger_Update__c = false;
            }
            
            
        }
        
        if(isFuture){
            
            if(isTask) update (List<Task>) listOfActivities;
            
            else update (List<Event>) listOfActivities;
            
        }
        
    }
    
    /**
    * V1.2
    * Updates the Task/Event Details based
    * on its respective Lead/Account
    * account records
    * @params      recordIds               Set of activity IDs
    * @params      isTask                  Checks if the process has been invoked by task records
    */
    @future(callout=true) 
    public static void updateActivityDetails(Set<Id> recordIds, Boolean isTask){
        
        List<sObject> listOfActivities = new List<sObject>();
        
        if(isTask){
            
            for(Task t : [SELECT Id, Subject, Sector_Classification__c, Date_Time_From__c, Has_Similar_Activity__c, whoId, whatId, accountId FROM Task WHERE Id in :recordIds]){
                
                listOfActivities.add( (sObject) t );
                
            }
            
        } else {
            
            for(Event t : [SELECT Id, Subject, Sector_Classification__c, Date_Time_From__c, EndDateTime, Has_Similar_Activity__c, whoId, whatId, accountId FROM Event WHERE Id in :recordIds]){
                
                listOfActivities.add( (sObject) t );
                
            }
            
        }
        
        executeActivityDetailsUpdate(listOfActivities,isTask,true);
    }
    
    /**
    * Creates the KPI records for newly created
    * account records
    */
    public static List<KPI__c> getKPIRecords(){
        
        List<Account> relatedAccts = [SELECT Id, ROC_Status__c, (SELECT Id, Year__c, Is_KPI_Met__c FROM KPIs__r WHERE Year__c =: System.Today().year()) FROM Account WHERE ROC_Status__c != '' AND ROC_Status__c != NULL ];
        
        return getKPIRecords(relatedAccts);
    }
    
   /**
    * Creates the KPI records for newly created
    * account records
    * @params      ids             List of Account Ids
    * @return      kpiRecords      List of Account KPI Records
    */
    public static List<KPI__c> getKPIRecords(List<Id> ids){
        
        List<Account> relatedAccts = [SELECT Id, ROC_Status__c, (SELECT Id, Year__c, Is_KPI_Met__c FROM KPIs__r WHERE Year__c =: System.Today().year()) FROM Account WHERE Id in:ids ];
        
        return getKPIRecords(relatedAccts);
    }
    
   /**
    * Creates the KPI records for newly created
    * account records
    * @params      relatedAccts    List of Accounts
    * @return      kpiRecords      List of Account KPI Records
    */
    public static List<KPI__c> getKPIRecords(List<account> relatedAccts){
        
        /* This will store the list of KPI records */
        List<KPI__c> kpiRecords = new List<KPI__c>();
        
        for(Account a : relatedAccts){
            
            /* If the ROC Status is not blank, create a KPI record for that account */
            if(a.KPIs__r.isEmpty()){
                kpiRecords.add(new KPI__c(Related_Account__c = a.Id,Year__c = System.Today().year()));
            } else {
                kpiRecords.addAll(a.KPIs__r);
            }
            
        }
        
        return kpiRecords;
    }
    
    /**
    * Creates task records from previous years,
    * and creates the corresponding KPI records
    * if non-existent.
    *
    * NOTE:
    * ONLY EXECUTE THIS METHOD
    * IF THE KPI RECORDS NEED UPDATING.
    * @params       yearRange       The year as to which the KPI needs to be corrected
    */
    public static void createKPIForPrevYears(Integer yearRange){
        upsertKpiRecords(null,yearRange);
    }
    
    /**
     * Upserts the KPI records of each Account 
     * @params      accountIds      The account Ids as associated to the activitiy
     * @params      yearRange       The year of the KPI is being computed
     */
    public static void upsertKpiRecords(List<Id> ids, Integer yearRange){
        
        /* Query all accounts */
        Map<Id,Account> relatedAccts = ids == null || ids.isEmpty() ? new Map<id,Account>([SELECT Id, ROC_Status__c FROM Account WHERE ROC_Status__c != '' AND ROC_Status__c != NULL ]) :
                                            new Map<id,Account>([SELECT Id, ROC_Status__c FROM Account WHERE ROC_Status__c != '' AND ROC_Status__c != NULL  AND Id in :ids]);
        
        /* This will store all the KPI Records */
        List<KPI__c> kpiRecords = new List<KPI__c>();
        
        DateTime startDate = DateTime.newInstance(yearRange-1, 12, 31, 23, 59, 59);
        DateTime endDate = DateTime.newInstance(yearRange+1, 1, 1, 0, 0, 0);
        
        /* Query the tasks from the previous years and this year */
        List<Task> oldTasks = [SELECT Id, 
                                      Status, 
                                      WhatId, 
                                      OwnerId,
                                      AccountID,  
                                      Has_Similar_Activity__c,
                                      Date_Time_From__c, 
                                      Category__c FROM Task 
                                      WHERE Date_Time_From__c > :startDate AND Date_Time_From__c <= :endDate AND (Category__c = 'DIFC Mgmt MTG CEO/HQ' OR Category__c = 'MTG RM with CEO' OR Category__c = 'Sector L–Firm CEO/HQ' OR Category__c = 'Sector L-Firm CEO/HQ') AND Category__c != '' AND Category__c != NULL AND (WhatId in :relatedAccts.keyset() OR AccountID in :relatedAccts.keySet()) AND Date_Time_From__c != NULL AND Status = 'Completed'];
         
        /* NOW, check the Events */
        List<Event> relatedEvents = [SELECT Id, 
                                            Category__c, 
                                            WhatId, 
                                            AccountId, 
                                            StartDateTime, 
                                            OwnerId,
                                            EndDateTime, 
                                            Has_Similar_Activity__c,
                                            Date_Time_From__c, 
                                            Date_Time_To__c FROM Event 
                                            WHERE EndDateTime > :startDate AND EndDateTime <= :endDate AND EndDateTime <= TODAY AND (Category__c = 'DIFC Mgmt MTG CEO/HQ' OR Category__c = 'MTG RM with CEO' OR Category__c = 'Sector L–Firm CEO/HQ' OR Category__c = 'Sector L-Firm CEO/HQ') AND Category__c != '' AND Category__c != NULL AND (WhatId in :relatedAccts.keyset() OR AccountID in :relatedAccts.keySet())];
        
        /* This will store the Account Id who has activities */
        Set<String> accountIdSet = new Set<String>();
        
        /* Check all Account-exclusive tasks */
        for(Task t : oldTasks) 
            if(String.isNotBlank(t.AccountID)) accountIdSet.add(t.AccountID);
            else if(String.isNotBlank(t.WhatId) && t.WhatId.getSobjectType() == Schema.Account.SObjectType) accountIdSet.add(t.WhatId);
        
        /* Check all Account-exclusive tasks */
        for(Event t : relatedEvents) 
            if(String.isNotBlank(t.AccountID)) accountIdSet.add(t.AccountID);
            else if(String.isNotBlank(t.WhatId) && t.WhatId.getSobjectType() == Schema.Account.SObjectType) accountIdSet.add(t.WhatId);
        
        /* Get the account IDS that do not contain in any map */
        Set<String> accountIdsWithNoActivities = new Set<String>();
        
        for(String k : relatedAccts.keySet()) if(!accountIdSet.contains(k)) accountIdsWithNoActivities.add(k);
        
        /* Query any existing KPI Records */
        List<KPI__c> existingKPIs = [SELECT Id, 
                                            Is_Affiliate_KPI_Met__c, 
                                            Is_KPI_Met__c, 
                                            Is_Current_KPI__c, 
                                            Related_Account__c, 
                                            Related_Account__r.OwnerId, // Added owner ID for checking if the attendee is the account owner
                                            Year__c FROM KPI__c 
                                            WHERE Related_Account__c IN :relatedAccts.keySet()];
        
        Map<String,KPI__c> kpisToBeProcessed = new Map<String,KPI__c>();
        
        Set<String> accountsWithKpi = new Set<String>();
        
        if(!existingKPIs.isEmpty()){
            
            for(KPI__c k : existingKPIs){
                
                k.No_of_DIFC_Management_Meetings__c = 0;
                k.No_of_RM_Meetings__c = 0;
                k.No_Of_Sector_Lead_Meetings__c = 0;
                k.No_of_Attended_RM_Meetings__c = 0;
                
                kpisToBeProcessed.put(k.Related_Account__c,k);
                accountsWithKpi.add(k.Related_Account__c);
            } 
            
        } 
        
        for(String i : accountIdSet) if(!accountsWithKpi.contains(i)) kpisToBeProcessed.put( i, new KPI__c(Related_Account__c = i, No_of_DIFC_Management_Meetings__c = 0, No_of_RM_Meetings__c = 0, No_Of_Sector_Lead_Meetings__c = 0, No_of_Attended_RM_Meetings__c = 0, Year__c = yearRange) ); // V1.4 - Claude - Initialized KPI 
        
        for(Task t : oldTasks) if(!t.Has_Similar_Activity__c) checkKpi(t.OwnerId,t.Category__c,kpisToBeProcessed.get(t.AccountId));
        
        for(Event t : relatedEvents) if(!t.Has_Similar_Activity__c) checkKpi(t.OwnerId,t.Category__c,kpisToBeProcessed.get(t.AccountId));
        
        /* Create new KPI records for empty accounts for the current YEAR */
        for(String k : accountIdsWithNoActivities) if(!kpisToBeProcessed.containsKey(k)) kpisToBeProcessed.put(k, new KPI__c(Related_Account__c = k, Year__c = yearRange)); // V1.04 - Added year for Accounts who have no KPI's for the year

        System.debug(kpisToBeProcessed.values());
        
        ID jobID = System.enqueueJob(new QueuableKPIHandler(kpisToBeProcessed.values()));       
    }
    
    /**
     * Calculates the KPI of the account
     * @params      ownerId             The owner of the activity
     * @params      activityCategory    The category of the activity
     * @params      kpiRecord           The KPI record to hold the values
     */
    private static void checkKpi(String ownerId, String activityCategory, KPI__c kpiRecord){
        
        if(activityCategory.equals('DIFC Mgmt MTG CEO/HQ')) kpiRecord.No_of_DIFC_Management_Meetings__c++;
        else if(activityCategory.equals('MTG RM with CEO')) kpiRecord.No_of_RM_Meetings__c++;
        else if(activityCategory.equals('Sector L–Firm CEO/HQ') || activityCategory.equals('Sector L-Firm CEO/HQ')) kpiRecord.No_Of_Sector_Lead_Meetings__c++;
        
        if(String.isNotBlank(ownerId) && String.isNotBlank(kpiRecord.Related_Account__r.OwnerId) && (activityCategory.equals('DIFC Mgmt MTG CEO/HQ') || 
            activityCategory.equals('Sector L–Firm CEO/HQ') || 
            activityCategory.equals('Sector L-Firm CEO/HQ')) && ownerId.equals(kpiRecord.Related_Account__r.OwnerId)){
            kpiRecord.No_of_Attended_RM_Meetings__c++;
        }   
        
    }
    
    /**
     * Logs an error to be sent to the admin
     * @params      e       The exception
     */
    private static void logKpiError(Exception e){
        
        CRM_cls_Utils crmUtils = new CRM_cls_Utils();
                
        crmUtils.setLogType('CRM KPI Utils : Task and Events');
        crmUtils.setErrorMessage('Exception Line Number : '+e.getLineNumber()+'\nException is : '+e.getMessage());
        
        crmUtils.logError();
        
    }
    
    /**
    * Queueable wrapper class for creating KPI records
    */
    public class QueuableKPIHandler implements Queueable {
        
        public List<KPI__c> kpiRecords;
        
        public QueuableKPIHandler(List<KPI__c> kpiRecords){
            this.kpiRecords = kpiRecords;
        }
        
        public void execute(QueueableContext context){
            
            CRM_cls_BatchKPIHandler kpiBatchHandler = new CRM_cls_BatchKPIHandler(kpiRecords);
            
            System.debug(Database.executeBatch(kpiBatchHandler));
            
        }
        
    }
    
}