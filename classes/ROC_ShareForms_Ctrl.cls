public class ROC_ShareForms_Ctrl{

     public string RecordTypeName {get;set;}
     
     public ROC_ShareForms_Ctrl(){
         
          string SRId = ApexPages.currentPage().getParameters().get('Id');
          RecordTypeName = [Select RecordType.DeveloperName from Service_Request__c
                              where ID =:SRId ].RecordType.DeveloperName;
     }

}