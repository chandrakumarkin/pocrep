/******************************************************************************************
 *  Class Name  : NorBlocIntegration
 *  Company     : DIFC
 *  Date        : 21 NOV 2019        
 *  Description :                   
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1    21 NOV 2019           Sai                Initial Version  
    v1.2    25-Nov-2020        selva            #11476 added turnover values and new parameters
*******************************************************************************************/
public class NorBlocReviewController {
    
    public static Account eachAccount;
    public static Integer turnoverRev;
    
    @AuraEnabled
    public static NorBlocReviewWrapper bankDetailstoReview (){
        
        eachAccount = new Account();
        User eachUser = new User(); 
        List<User> lstPortalUsers = new List<User>();
        List<HexaBPM__SR_Doc__c> lstDocumentsData = new List<HexaBPM__SR_Doc__c>();
        String activities  = '';
        List<String> lstRelationshipTypes = new List<String>();
        lstRelationshipTypes = Label.Relationship_Types_of_NorBloc.split(',');
        List<String> lstDocuments = new List<String>();
        lstDocuments = Label.DocumentstoDisplay.split(','); 
        List<Relationship__c> lstpartnersInfo = new List<Relationship__c>();
        List<Relationship__c> lstLP = new List<Relationship__c>();
        List<Relationship__c> lstRepresentative = new List<Relationship__c>();
        List<Relationship__c> lstDResponsible = new List<Relationship__c>();
        List<Relationship__c> lstGP = new List<Relationship__c>();
        List<Relationship__c> lstShareholders = new List<Relationship__c>();
        List<Relationship__c> lstDesignatedMember = new List<Relationship__c>();
        List<Relationship__c> lstFounder = new List<Relationship__c>();
        List<Relationship__c> lstFoundingMember = new List<Relationship__c>();
        List<Relationship__c> lstDirector = new List<Relationship__c>();
        List<Relationship__c> lstMembers = new List<Relationship__c>();
        List<Relationship__c> lstCouncilMember = new List<Relationship__c>();
        
        eachUser = [SELECT Contact.AccountID FROM User where ID=:userinfo.getUserId()];
        eachAccount = [SELECT Name,Registration_License_No__c,License_Issue_Date__c,Legal_Type_of_Entity__c,Place_of_Registration__c,
                       Reason_for_exemption__c,Next_Renewal_Date__c,Registration_Type__c,ROC_reg_incorp_Date__c,Tax_Registration_Number_TRN__c,
                       Building_Name__c,Office__c,Street__c,City__c,Lease_Types__c,BP_No__c FROM Account where Id=:eachUser.Contact.AccountID limit 1];
        
        for(License_Activity__c eachActivity: [SELECT Name FROM License_Activity__c where Account__c = :eachAccount.Id]){
                activities += eachActivity.Name + ',';
        }
        
        for(User portalusers: [Select Contact.Email,Contact.MobilePhone,Contact.Name,UserName 
                            from User where IsActive=True and Contact.AccountID=:eachAccount.ID and ContactID !=Null and 
                            Contact.Recordtype.DeveloperName ='Portal_User' AND Community_User_Role__c INCLUDES ('Company Services') AND Contact.MobilePhone !=null]){
            lstPortalUsers.add(portalusers);
                
        }
        system.debug('!!$$$$$@@#'+lstDocuments);
        
        system.debug('!!@@#'+lstDocumentsData);
        for(Relationship__c eachRelation:[SELECT Object_Contact__r.Name,
                                              Object_Contact__r.MobilePhone,Object_Contact__r.Email,Relationship_Type_formula__c,
                                              Object_Account__r.Name,Phone_No__c,Relationship_Type__c,Object_Contact_Email__c,Amendment_ID__c,Object_Contact__r.Passport_No__c,
                                              Object_Contact__r.Nationality__c,Object_Account__r.Registration_License_No__c, Object_Account__r.Registration_No__c,Object_Account__r.Place_of_Registration__c
                                              FROM Relationship__c where Subject_Account__c=:eachAccount.Id and Relationship_Type__c IN:lstRelationshipTypes AND Active__c=true AND
                                              (Relationship_Type__c!='GS Portal Contact' and Relationship_Type__c!='')]){
            
            system.debug('!!@@##@@'+eachRelation.Amendment_ID__c);
            if(eachRelation.Relationship_Type__c == 'Has Partner'){
                lstpartnersInfo.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'Has Limited Partner'){
                lstLP.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'Has Authorized Representative'){
                lstRepresentative.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'Has Service Document Responsible for'){
                lstDResponsible.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'Has General Partner'){
                lstGP.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'Is Shareholder Of'){
                lstShareholders.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'Has Designated Member'){
                lstDesignatedMember.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'has founder'){
                lstFounder.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'Has Founding Member'){
                lstFoundingMember.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'Has Director'){
                lstDirector.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'Has Member'){
                lstMembers.add(eachRelation);
            }
            if(eachRelation.Relationship_Type__c == 'has Council Member'){
                lstCouncilMember.add(eachRelation);
            }
        }
        system.debug('!!@@##'+lstShareholders);
        
        for(HexaBPM__SR_Doc__c eachDoc:[SELECT Name,Amendment_Name__c,HexaBPM__Status__c,HexaBPM__Preview_Download_Document__c,HexaBPM__Doc_ID__c FROM HexaBPM__SR_Doc__c
                                        where HexaBPM__Customer__c =:eachAccount.ID AND Name IN:lstDocuments]){
            lstDocumentsData.add(eachDoc);
        }
        
        NorBlocReviewWrapper wrapper = new NorBlocReviewWrapper();
        wrapper.activities = activities.removeEnd(',');
        wrapper.accountInformation = eachAccount;
        wrapper.portalUsers  = lstPortalUsers;
        wrapper.lstDocuments = lstDocumentsData;
        wrapper.lstpartners  = lstpartnersInfo;
        wrapper.lstlimitedPartner  = lstLP;
        wrapper.lstARepresentative  = lstRepresentative;
        wrapper.lstDSPusers  = lstDResponsible;
        wrapper.lstGP  = lstGP;
        wrapper.lstShareHolder  = lstShareholders;
        wrapper.lstDesignatedMember  = lstDesignatedMember;
        wrapper.lstfounder  = lstFounder;
        wrapper.lstFoundingMember  = lstFoundingMember;
        wrapper.lstDirector  = lstDirector;
        wrapper.lstMember  = lstMembers;
        wrapper.lstCouncilMember  = lstCouncilMember;
        wrapper.turnover=null;
        
        system.debug('!@@@#@'+wrapper);
        return wrapper;
    }
    
    @AuraEnabled
    public static boolean callOTPpage(integer turnover){ //v1.2 added 
        boolean isBankdetailsSenttoMashreq = false;
        turnoverRev =turnover;
        isBankdetailsSenttoMashreq  = otpPage();
        return isBankdetailsSenttoMashreq;
    }
   
   @AuraEnabled 
    public static boolean otpPage(){
        
        boolean isBankdetailsSenttoMashreq = false;
        try{
            
            User userDetails = new User();
            userDetails = [SELECT Contact.AccountID FROM User where ID=:userinfo.getUserId()];
            
            Account eachAccount = new Account();
            eachAccount = [SELECT is_Bank_Account_Created__c FROM Account where ID=:userDetails.Contact.AccountID];
            
            if(eachAccount.is_Bank_Account_Created__c == false){
                
                NorBlocIntegrationController.norBlocIntegration(userDetails.Contact.AccountID ,turnoverRev); //v1.2 added 
            }
            else {
                isBankdetailsSenttoMashreq = true;
            }
        }
        Catch(exception ex){
            
        }
        return isBankdetailsSenttoMashreq;
    }
    public class NorBlocReviewWrapper{
        
        @AuraEnabled public String activities {get;set;}
        @AuraEnabled public Integer turnover{get;set;}
        @AuraEnabled public Account accountInformation {get;set;}
        @AuraEnabled public List<User> portalUsers {get;set;}
        @AuraEnabled public List<HexaBPM__SR_Doc__c> lstDocuments {get;set;}
        @AuraEnabled public List<Relationship__c> lstpartners{get;set;}
        @AuraEnabled public List<Relationship__c> lstlimitedPartner{get;set;}
        @AuraEnabled public List<Relationship__c> lstARepresentative{get;set;}
        @AuraEnabled public List<Relationship__c> lstDSPusers{get;set;}
        @AuraEnabled public List<Relationship__c> lstGP{get;set;}
        @AuraEnabled public List<Relationship__c> lstShareHolder{get;set;}
        @AuraEnabled public List<Relationship__c> lstDesignatedMember{get;set;}
        @AuraEnabled public List<Relationship__c> lstfounder{get;set;}
        @AuraEnabled public List<Relationship__c> lstFoundingMember{get;set;}
        @AuraEnabled public List<Relationship__c> lstDirector{get;set;}
        @AuraEnabled public List<Relationship__c> lstMember{get;set;}
        @AuraEnabled public List<Relationship__c> lstCouncilMember{get;set;}
        
    }
    
}