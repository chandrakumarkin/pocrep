@isTest
global class sapComDocumentSapSoapFunctionsMcSRMDMock implements WebServiceMock {
    
     public static sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachment_element ttRefundOp = new sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachment_element();
        
      global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) 
       {
        
        
        sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachment_element ttRefundOps = new sapComDocumentSapSoapFunctionsMcSRMD.ZreOfrndAttachment_element();
     
         response.put('response_x', ttRefundOps); 
       }

}