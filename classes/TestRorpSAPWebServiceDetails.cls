/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRorpSAPWebServiceDetails {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'RORP CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        list<Relationship_Codes__c> lstCS1 = new list<Relationship_Codes__c>();
        lstCS1.add(new Relationship_Codes__c(Name='Is Point of contact for',Code__c='1234'));
        insert lstCS1;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Portal_User','RORP_Account','RORP_Contact')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Custoer 1';
        objAccount1.E_mail__c = 'test@test.com';
        //objAccount.BP_No__c = '001234';
        objAccount1.RecordTypeId = mapRecordTypeIds.get('RORP_Account');
        objAccount1.Country__c = 'India';
        insert objAccount1;
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Test Custoer 1';
        objAccount2.E_mail__c = 'test@test.com';
        //objAccount.BP_No__c = '001234';
        objAccount2.RecordTypeId = mapRecordTypeIds.get('RORP_Account');
        objAccount2.Country__c = 'India';
        insert objAccount2;
                
        Contact objCon = new Contact();
        objCon.FirstName = 'RB';
        objCon.LastName = 'Nagaboina';
        objCon.Country__c = 'India';
        objCon.Passport_No__c = '1234567';
        objCon.Nationality__c = 'India';
        objCon.RecordTypeId = mapRecordTypeIds.get('RORP_Contact');
        objCon.Birthdate = Date.newInstance(1991, 11, 19);
        objCon.RELIGION__c = 'Hindu';
        objCon.Gender__c = 'Female';
        objCon.Mother_Full_Name__c = 'test';
        objCon.Gross_Monthly_Salary__c = 123456;
        objCon.Email = 'test@test.test';
        insert objCon;
        
        Contact objPortCon = new Contact();
        objPortCon.FirstName = 'RB';
        objPortCon.LastName = 'Nagaboina';
        objPortCon.Country__c = 'India';
        objPortCon.Passport_No__c = '1234567';
        objPortCon.Nationality__c = 'India';
        objPortCon.RecordTypeId = mapRecordTypeIds.get('Portal_User');
        objPortCon.Birthdate = Date.newInstance(1991, 11, 19);
        objPortCon.RELIGION__c = 'Hindu';
        objPortCon.Gender__c = 'Female';
        objPortCon.Mother_Full_Name__c = 'test';
        objPortCon.Gross_Monthly_Salary__c = 123456;
        objPortCon.Email = 'test@test.test';
        objPortCon.Individual_Contact__c = objCon.id;
        objPortCon.AccountId = objAccount2.id;
        insert objPortCon;
        
        list<Document_Details__c> lstDocDt = new list<Document_Details__c>();
        
        Document_Details__c DocDt1 = new Document_Details__c(Contact__c=objCon.id,Document_Type__c='Passport',Document_Number__c='1234567');
        lstDocDt.add(DocDt1);
        Document_Details__c DocDt2 = new Document_Details__c(Contact__c=objCon.id,Document_Type__c='Emirates ID',Document_Number__c='1234567');
        lstDocDt.add(DocDt2);
        
        insert lstDocDt;
        
        Identification__c idnDoc = new Identification__c(name='1234567',Account__c=objAccount1.id,Identification_Type__c='Commercial License');
        insert idnDoc;
        
        list<Relationship__c> lstRel = new list<Relationship__c>();
        
        Relationship__c rel = new Relationship__c();
        rel.Relationship_Type__c = 'Is Point of contact for';
        rel.Subject_Account__c = objAccount.Id;
        rel.Start_Date__c = system.today();
        rel.Active__c = true;
        rel.Relationship_Group__c = 'RORP';
        rel.Object_Contact__c = objCon.Id;
        lstRel.add(rel);
        
        rel = new Relationship__c();
        rel.Relationship_Type__c = 'Is Point of contact for';
        rel.Subject_Account__c = objAccount.Id;
        rel.Start_Date__c = system.today();
        rel.Active__c = true;
        rel.Relationship_Group__c = 'RORP';
        rel.Object_Account__c = objAccount1.Id;
        lstRel.add(rel);
        insert lstRel;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        insert objSR;
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Price__c = 2134;
        insert objSRItem;
        
        list<RORP_CRM_SAPService.ZSF_IN_LOG> RorpPartnerInfo = new list<RORP_CRM_SAPService.ZSF_IN_LOG>();
        RORP_CRM_SAPService.ZSF_IN_LOG objData ;
        
        objData = new RORP_CRM_SAPService.ZSF_IN_LOG();        
        objData.STATUS = 'S';
        objData.PARTNER1 = '12345';
        objData.SFGUID = objCon.Id;
        RorpPartnerInfo.add(objData);
        
        objData = new RORP_CRM_SAPService.ZSF_IN_LOG();        
        objData.STATUS = 'S';
        objData.PARTNER1 = '12346';
        objData.SFGUID = objAccount1.Id;
        RorpPartnerInfo.add(objData);
        
        objData = new RORP_CRM_SAPService.ZSF_IN_LOG();        
        objData.STATUS = 'E';
        objData.PARTNER1 = '12346';
        objData.SFGUID = objAccount1.Id;
        RorpPartnerInfo.add(objData);
        
        Test.setMock(WebServiceMock.class, new TestRORPMockService());
        TestRORPMockService.RorpPartnerInfo = RorpPartnerInfo;
        
        RorpSAPWebServiceDetails.RorpAccountCreation(null, new list<string>{objAccount1.Id} , null);
        RorpSAPWebServiceDetails.RorpContactCreation(null, new list<string>{objCon.Id} , objSR.Id);
        RORPBPCreateWebService.RorpBPCreationNew(new list<string>{objCon.Id},objSR.Id,null);
        RORPBPCreateWebService.RorpBPCreationNew(new list<string>{objAccount1.Id},objSR.Id,null);
    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'RORP CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Relationship_Codes__c> lstCS1 = new list<Relationship_Codes__c>();
        lstCS1.add(new Relationship_Codes__c(Name='Is Point of contact for',Code__c='1234'));
        insert lstCS1;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('RORP_Account','RORP_Contact')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Custoer 1';
        objAccount1.E_mail__c = 'test@test.com';
        //objAccount.BP_No__c = '001234';
        objAccount1.RecordTypeId = mapRecordTypeIds.get('RORP_Account');
        objAccount1.Country__c = 'India';
        objAccount1.BP_No__c = '001235';
        insert objAccount1;
        
        Contact objCon = new Contact();
        objCon.FirstName = 'RB';
        objCon.LastName = 'Nagaboina';
        objCon.Country__c = 'India';
        objCon.Passport_No__c = '1234567';
        objCon.Nationality__c = 'India';
        objCon.RecordTypeId = mapRecordTypeIds.get('RORP_Contact');
        objCon.Birthdate = Date.newInstance(1991, 11, 19);
        objCon.RELIGION__c = 'Hindu';
        objCon.Gender__c = 'Female';
        objCon.Mother_Full_Name__c = 'test';
        objCon.Gross_Monthly_Salary__c = 123456;
        objCon.Email = 'test@test.test';
        objCon.BP_No__c = '001236';
        insert objCon;
        
        list<Relationship__c> lstRel = new list<Relationship__c>();
        
        Relationship__c rel = new Relationship__c();
        rel.Relationship_Type__c = 'Is Point of contact for';
        rel.Subject_Account__c = objAccount.Id;
        rel.Start_Date__c = system.today();
        rel.Active__c = true;
        rel.Relationship_Group__c = 'RORP';
        rel.Object_Contact__c = objCon.Id;
        lstRel.add(rel);
        
        rel = new Relationship__c();
        rel.Relationship_Type__c = 'Is Point of contact for';
        rel.Subject_Account__c = objAccount.Id;
        rel.Start_Date__c = system.today();
        rel.Active__c = true;
        rel.Relationship_Group__c = 'RORP';
        rel.Object_Account__c = objAccount1.Id;
        lstRel.add(rel);
        
        rel = new Relationship__c();
        rel.Relationship_Type__c = 'Is Point of contact for';
        rel.Object_Account__c = objAccount.Id;
        rel.Start_Date__c = system.today();
        rel.Active__c = true;
        rel.Relationship_Group__c = 'RORP';
        rel.Subject_Contact__c = objCon.Id;
        lstRel.add(rel);
        
        insert lstRel;
        
        list<RORP_CRM_SAPService.ZSF_IN_LOG> RorpPartnerInfo = new list<RORP_CRM_SAPService.ZSF_IN_LOG>();
        RORP_CRM_SAPService.ZSF_IN_LOG objData ;
        
        objData = new RORP_CRM_SAPService.ZSF_IN_LOG();        
        objData.STATUS = 'S';
        objData.PARTNER1 = '12345';
        objData.SFGUID = lstRel[0].Id;
        RorpPartnerInfo.add(objData);
        
        objData = new RORP_CRM_SAPService.ZSF_IN_LOG();        
        objData.STATUS = 'S';
        objData.PARTNER1 = '12346';
        objData.SFGUID = lstRel[1].Id;
        RorpPartnerInfo.add(objData);
        
        objData = new RORP_CRM_SAPService.ZSF_IN_LOG();        
        objData.STATUS = 'E';
        objData.PARTNER1 = '12346';
        objData.SFGUID = objAccount1.Id;
        RorpPartnerInfo.add(objData);
        
        objData = new RORP_CRM_SAPService.ZSF_IN_LOG();        
        objData.STATUS = 'E';
        objData.PARTNER1 = '12346';
        objData.SFGUID = objAccount1.Id;
        RorpPartnerInfo.add(objData);
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        insert objSR;
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Price__c = 2134;
        insert objSRItem;
        
        Test.setMock(WebServiceMock.class, new TestRORPMockService());
        TestRORPMockService.RorpPartnerInfo = RorpPartnerInfo;
        String str = objSR.id;
        RorpSAPWebServiceDetails.RorpPartnerCreationFuture(null, str, new list<string>{lstRel[0].Id,lstRel[1].Id,lstRel[2].id});
        RORPBPCreateWebService.RorpBPCreationNew(null, str, new list<string>{lstRel[0].Id,lstRel[1].Id,lstRel[2].id});
    }
}