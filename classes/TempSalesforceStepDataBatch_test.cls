/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TempSalesforceStepDataBatch_test {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Temp_Salesforce_Step_Data__c testRec = new Temp_Salesforce_Step_Data__c();
        testRec.Step_Id__c = 'testRec1';
        insert testRec;
        
         List<Step__c> testIdamaSteps = new List<Step__c>();
        
        Service_Request__c testSr = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        
        testSr.Sr_Group__c = 'Fit-Out & Events';
        testSr.Mobile_Number__c = '+9715000000000';
        
        //insert testSr;
        
        Temp_SAP_Step_Data__c testRec12 = new Temp_SAP_Step_Data__c();
        testRec12.Step_Id__c = 'testRec1';
        testRec12.SAP_Seq__c='testRec1';
        insert testRec12;
        
        Status__c testStatus = Test_CC_FitOutCustomCode_DataFactory.getTestStepStatus('Approved','APPROVED');
        
        insert testStatus;
				
		testIdamaSteps.add(Test_CC_FitOutCustomCode_DataFactory.getTestStep(testSR.Id,testStatus.Id,'Test'));
		/* Create an Step Template with a number greater than 81 */
		
		SR_Steps__c testSrStep = new SR_Steps__c();

		testSrStep.Step_No__c = 82;
		insert testSrStep;
		
		for(Step__c s : testIdamaSteps){
			s.SR_Step__c = testSrSTep.Id; 
            s.Step_Id__c = '__testRec1';
		}
		
		insert testIdamaSteps;
		
        test.startTest();
        	TempSalesforceStepDataBatch batch = new TempSalesforceStepDataBatch();
        	String cronExpr = '0 0 0 15 3 ? 2022';
        	String jobId = System.schedule('myJobTestJobName', cronExpr, batch); 
        test.stopTest();
        
    }
}