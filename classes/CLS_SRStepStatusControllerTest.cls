@isTest(SeeAllData=true) 
public with sharing class CLS_SRStepStatusControllerTest
{
    /*method if account returns data i.e. if response*/
    static testMethod void  Test_IfResponse() 
    {
        CLS_SRStepStatusController.jsonRes('0190764',  'RD3851785');

         
         List<Service_Request__c> ListSR=[select id,Passport_Number__c,Name from Service_Request__c where Passport_Number__c!=null and External_Status_Name__c='Under Verification' and  SR_Group__c='GS' limit 2];
         System.debug('===ListSR===>'+ListSR);
         if(ListSR.size()==2)
         {
         
             CLS_SRStepStatusController.jsonRes(ListSR[0].Name,  ListSR[0].Passport_Number__c);
              CLS_SRStepStatusController.jsonRes(ListSR[1].Name,  ListSR[1].Passport_Number__c);
         }
     
    }
 }