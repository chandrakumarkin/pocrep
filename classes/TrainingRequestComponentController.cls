public without sharing class TrainingRequestComponentController {
    public string serviceType {get;set;}
    public string strViewLink {get;set;}
    public boolean isTrainingAvailable  {
        get {
           return retrieveTrainingAvailable();
        }
        private set;
    }
    public TrainingRequestComponentController(){
        
    }
    public boolean retrieveTrainingAvailable(){
        boolean isAvailable = false;
        string keyPrefix = Training_Request__c.sObjectType.getDescribe().getKeyPrefix();
        for(ListView lstView : [SELECT Id FROM ListView WHERE SObjectType = 'Training_Request__c' and Name = :serviceType]){
            strViewLink = '../'+keyPrefix+'?fcf='+lstView.Id;
        }
        
        for(Training_Master__c TM : [select id,Name, Start_DateTime__c,To_DateTime__c,Description__c from Training_Master__c
                                     where Category__c = :serviceType and Start_DateTime__c > TODAY
                                     and Status__c = 'Approved by HOD' and Remaining_Seats__c > 0]){
        	isAvailable = true;
            break;
        }
        return isAvailable;

    }
}