/**********************************************************
*Apex Class : CertificateofIncumbency
*Description : Created to replace the standard layout
*Modified: #4979
v1.1    selva    added validation #12322
***********************************************************/
public with sharing class CertificateofIncumbency extends Cls_ProcessFlowComponents  {

public Service_Request__c SRData{get;set;}

public string RecordTypeId;
public map<string,string> mapParameters;
 
public Account ObjAccount{get;set;}

public string SRRecType {get;set;}
public Boolean isEditMode{get;set;}
public Boolean isDetail{get;set;}
public CurrentContext ContextObj {get;set;}

public string strPageId{get;set;}
public string isreview{get;set;}
public string srId;
public string Sr_Id{get;set;}  
public string legalType {get;set;}
public CertificateofIncumbency(){
        

        isEditMode = true;
        ObjAccount=new account();
        ContextObj = new CurrentContext();
        if(Apexpages.currentPage().getParameters().get('isDetail') != null && Apexpages.currentPage().getParameters().get('isDetail') == 'true'){
            ContextObj.IsDetail = true;
            isEditMode = false;
        }
        else{
            ContextObj.IsDetail = false;
        }
        
        if(Apexpages.currentPage().getParameters().get('id') != null){
            Sr_Id = Apexpages.currentPage().getParameters().get('id');
        }
        if(Sr_Id!=null){
            SRData = [select id,Confirm_Change_of_Entity_Name__c,Confirm_Change_of_Trading_Name__c,Customer__r.Legal_Type_of_Entity__c,Legal_Structures_Full__c,Customer__c,Financial_Year_End_mm_dd__c,Email__c,Send_SMS_To_Mobile__c,Declaration_Signatory_Name__c,Declaration_Signatory_Capacity__c,Name,Service_Type__c,External_Status_Name__c,Submitted_Date__c,Entity_Name__c,Doyouwishtoaddpassportnumbers__c,On_behalf_of_identical_name__c,Attestation_Required__c,Customer__r.Name from Service_Request__c where id=: Sr_Id];
            legalType  = SRData.Customer__r.Legal_Type_of_Entity__c;
        }else{
            mapParameters = new map<string,string>();

            if(apexpages.currentPage().getParameters()!=null){
            mapParameters = apexpages.currentPage().getParameters();
            }  
            
            SRRecType  ='Certificate of Incumbency';
           if(mapParameters.get('RecordType')!=null){ 
           RecordTypeId= mapParameters.get('RecordType');
           }
        }

        for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,Contact.Account.Contact_Details_Provided__c,Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  from User where Id=:userinfo.getUserId()])
        {
         if(SRData==null)
         {
            SRData = new Service_Request__c();
            SRData.Customer__c = objUsr.Contact.AccountId;
            SRData.RecordTypeId=RecordTypeId;
            SRData.Email__c = objUsr.Email;
            SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
            SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
            SRData.Entity_Name__c=objUsr.Contact.Account.Name;
            SRData.Expiry_Date__c=objUsr.Contact.Account.Next_Renewal_Date__c;
            ObjAccount= objUsr.Contact.Account;
            SRData.Financial_Year_End_mm_dd__c=objUsr.Contact.Account.Financial_Year_End__c;
            SRRecType  ='Certificate of Incumbency';
            legalType  = objUsr.Contact.Account.Legal_Type_of_Entity__c;

         }
          
        }
}

public pagereference SaveConfirmation(){ 
    
    boolean isvalid=true;
    isEditMode = false;
    //v1.1 added
    if((legalType=='LTD' || legalType=='Private Company' || legalType=='Public Company' || legalType=='Protected Cell Company' || legalType=='Investment Company') && SRData.Confirm_Change_of_Entity_Name__c==null){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please complete all fields.');         
        ApexPages.addMessage(myMsg); 
        isEditMode = true;
         return null;
    }
    if((legalType=='LTD' || legalType=='Private Company' || legalType=='Public Company' || legalType=='Protected Cell Company' || legalType=='Investment Company') && SRData.Confirm_Change_of_Entity_Name__c=='Yes' && SRData.Confirm_Change_of_Trading_Name__c==null){
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please complete all fields.');         
        ApexPages.addMessage(myMsg); 
         isEditMode = true;
         return null;
    }
    //v1.1 end
    if(SRData.Confirm_Change_of_Entity_Name__c=='No'){
        SRData.Confirm_Change_of_Trading_Name__c = null;
    }
    system.debug('----------'+ SRData.Confirm_Change_of_Entity_Name__c);
    upsert SRData;
    return null;

}
public pagereference EditSR(){
    SRData = [select id,Confirm_Change_of_Entity_Name__c,Confirm_Change_of_Trading_Name__c,Customer__r.Legal_Type_of_Entity__c,Legal_Structures_Full__c,Customer__c,Financial_Year_End_mm_dd__c,Email__c,Send_SMS_To_Mobile__c,Declaration_Signatory_Name__c,Declaration_Signatory_Capacity__c,Name,Service_Type__c,External_Status_Name__c,Submitted_Date__c,Entity_Name__c,Doyouwishtoaddpassportnumbers__c,On_behalf_of_identical_name__c,Attestation_Required__c,Customer__r.Name from Service_Request__c where id=: Sr_Id];
    legalType  = SRData.Customer__r.Legal_Type_of_Entity__c;
    isEditMode =true;
    return null;

}

     public class CurrentContext{
       
        public Boolean IsDetail {get;set;}
        public Boolean IsPortaluser {get;set;}
        
    }
    public PageReference CancelEco(){
        
        if(SRData.Id!=null){
            delete SRData;
        }
         pagereference pg = new Pagereference(Label.Page_Redirect);
         pg.setRedirect(true);
        
        return pg;
    }
    public override pagereference DynamicButtonAction(){
    system.debug('------'+isNext());
       if(isNext()==true){
           if((legalType=='LTD' || legalType=='Private Company' || legalType=='Public Company' || legalType=='Protected Cell Company' || legalType=='Investment Company') && SRData.Confirm_Change_of_Entity_Name__c==null){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please complete all fields.');         
                ApexPages.addMessage(myMsg); 
                isEditMode = true;
                 return null;
            }
            if((legalType=='LTD' || legalType=='Private Company' || legalType=='Public Company' || legalType=='Protected Cell Company' || legalType=='Investment Company') && SRData.Confirm_Change_of_Entity_Name__c=='Yes' && SRData.Confirm_Change_of_Trading_Name__c==null){
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please complete all fields.');         
                ApexPages.addMessage(myMsg); 
                 isEditMode = true;
                 return null;
            }
             
           SaveConfirmation();
       }
       //upsert SRData;
       return getButtonAction();
    }
}