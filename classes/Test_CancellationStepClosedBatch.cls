@isTest(SeeAllData=False)

public class Test_CancellationStepClosedBatch{

static testmethod void Test_Executebatchmethod(){

    User u = new User(
     ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
     FirstName = 'Ghanshyam',
     LastName = 'Yadav',
     Email = 'puser000@amamama.com',
     Username = 'puser000@amamama.com' + System.currentTimeMillis(),
     CompanyName = 'DIFC',
     Title = 'Mr',
     Alias = 'alias',
     TimeZoneSidKey = 'America/Los_Angeles',
     EmailEncodingKey = 'UTF-8',
     LanguageLocaleKey = 'en_US',
     LocaleSidKey = 'en_US');
     insert u;
    
     System.runAs(u){
    
    Currency__c c = new Currency__c();
    c.Name = 'US Dollar';
    c.End_Date__c = date.today() + 2;
    c.Start_Date__c = date.today();
    c.Exchange_Rate__c = 12;
    insert c;
    
    Account acc = new Account();
    acc.Name = '3 Capital Trading Limited';
    insert acc;
    
    SR_Status__c ss = new SR_Status__c();
    ss.Name = 'Awaiting Re-upload';
   // ss.CurrencyIsoCode = 'AED - UAE Dirham';
    ss.Code__c = 'AWAITING_RE_UPLOAD';
    insert ss;
         
    SR_Template__c stl = new SR_Template__c();
    stl.Name = 'Add Audited Accounts';
    stl.SR_RecordType_API_Name__c = 'Add_Audited_Accounts';
    stl.SR_Group__c = 'ROC';
    stl.Is_Letter__c = True;
    insert stl;     
         
    Status__c sc = new Status__c();
    sc.Name = 'The application is under process with GDRFA';
    sc.Code__c = 'CLOSED';
    insert sc;
    
    Service_Request__c sr = new Service_Request__c();
    sr.First_Name__c = 'Ghanshyam';
    sr.Last_Name__c = 'Yadav';
    sr.SAP_PR_No__c = '1000149742';
    sr.Customer__c = acc.Id;
    sr.Currency_Rate__c = c.Id;
    sr.Express_Service__c = False;
    sr.SR_Group__c = 'GS';
    sr.Completed_Date_Time__c = system.now().addMinutes(-6);
    sr.External_SR_Status__c = ss.Id;
    sr.SR_Template__c = stl.id;
    sr.Internal_SR_Status__c = ss.Id;
    //sr.External_SR_Status__c = sc.Id;
    insert sr;
    
    Step_Template__c sp = new Step_Template__c();
    sp.Name = 'Entry Permit Form is Typed';
    sp.Step_RecordType_API_Name__c = 'Reupload_Document_for_Clearance';
    sp.Code__c = 'REUPLOAD_DOCUMENT_FOR_CLEARANCE';
    insert sp;
    
         
  Map <id,Step__c> stepList = new Map <id,Step__c>();  
       
    Step__c st = new Step__c();
    st.OwnerId = u.Id;
    st.DNRD_Receipt_No__c = '20145220161409950';
    st.Step_Id__c = 'GO-VIP11_141607267850002016_0010';
    st.SR__c = sr.Id;
    st.Step_Template__c = sp.Id;
    st.Closed_Date_Time__c = system.now().addMinutes(-30);
    st.Sr_group__c = 'GS';
   // st.Step_Name__c = 'Entry Permit Form is Typed';
   st.Status__c = sc.Id;
         
    insert st;
    stepList.put(st.Id,st);    
         
list<Step__c> stlst = [Select Id, OwnerId,DNRD_Receipt_No__c,Step_Id__c,sr__r.name,Applicant_Name__c,Customer__c,Step_Name__c,SR_Status__c,SAP_PR_No__c,Closed_Date_Time__c,Name From step__c];     
    system.debug('Total step records'+stlst);
    for(Step__c s : stlst){
     stepList.put(s.Id,s); 
         } 
 system.debug('Total values in Map'+stepList); 
   
    test.startTest();    
        CancellationStepClosedBatch scb=new CancellationStepClosedBatch();
       ID batchprocessid = database.executebatch(scb);
         Database.QueryLocator ql = scb.start(null);
         scb.execute(null,stlst);
         scb.Finish(null);
   test.stopTest();
        
    }
}
}