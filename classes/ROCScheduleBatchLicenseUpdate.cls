/******************************************************************************************
 *  Name        : ROCScheduleBatchLicenseUpdate 
 *  Author      : Claude Manahan
 *  Company     : DIFC
 *  Date        : 2017-16-4
 *  Description : Schedulable class for ROC License Update for Government
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   16-04-2017   Claude        Created
 V1.1   18-03-2018   Shabbir       Updated logic to send activities 
 V1.2   29-Aug-2019  Arun           Updated and move logic to other class  
*******************************************************************************************/
public class ROCScheduleBatchLicenseUpdate implements Schedulable, Database.Batchable<sObject>, Database.AllowsCallouts {
    
    public ROCScheduleBatchLicenseUpdate(){}
       public  final boolean SendAll;

    public list<Account> start(Database.BatchableContext BC){

        if(SendAll==false)
        return [SELECT Id,
                      Name,                                                 // Company Name in English
                      Arabic_Name__c,                                       // Company Name in Arabic
                      Company_Type__c,                                      // Company Type
                      Office__c,                                            // Address - Office No.
                      Building_Name__c,                                     // Address - Building Name
                      Registration_License_No__c,                           // License No
                      Liquidated_Date__c,                                   // Liquidation Date - non-financial / retail
                      DFSA_Withdrawn_Date__c,                               // Withdrawn Date - financial
                      Legal_Type_of_Entity__c,                              // Legal entity (internal)
                      (select id, Type__c,Formula_Activity_Name_Eng__c FROM License_Activities__r where Include_Activity__c = true),        // Activities //v1.1
                      Active_License__r.License_Issue_Date__c,              // License Iddue Date
                      Active_License__r.License_Expiry_Date__c,             // License Expiry Date
                      Active_License__r.Status__c,                          // License Status 
                      Sector_Classification__c,                             // License Type
                      PO_Box__c,                                            // PO Box
                      Phone,                                                // COmpany Tel No
                      ROC_Status__c 
                      FROM Account WHERE Roc_Status__c != null AND Roc_Status__c != '' and Do_not_Push__c!='DSC' AND Legal_Type_of_Entity__c != null AND Registration_License_No__c != null AND Registration_License_No__c != '' and PR_Push_Date_F__c=today];
                      
            else
            {
              return [SELECT Id,
                      Name,                                                 // Company Name in English
                      Arabic_Name__c,                                       // Company Name in Arabic
                      Company_Type__c,                                      // Company Type
                      Office__c,                                            // Address - Office No.
                      Building_Name__c,                                     // Address - Building Name
                      Registration_License_No__c,                           // License No
                      Liquidated_Date__c,                                   // Liquidation Date - non-financial / retail
                      DFSA_Withdrawn_Date__c,                               // Withdrawn Date - financial
                      Legal_Type_of_Entity__c,                              // Legal entity (internal)
                      (select id, Type__c,Formula_Activity_Name_Eng__c FROM License_Activities__r where Include_Activity__c = true),        // Activities //v1.1
                      Active_License__r.License_Issue_Date__c,              // License Iddue Date
                      Active_License__r.License_Expiry_Date__c,             // License Expiry Date
                      Active_License__r.Status__c,                          // License Status 
                      Sector_Classification__c,                             // License Type
                      PO_Box__c,                                            // PO Box
                      Phone,                                                // COmpany Tel No
                      ROC_Status__c 
                      FROM Account WHERE Roc_Status__c != null AND Roc_Status__c != '' and Do_not_Push__c!='DSC' AND Legal_Type_of_Entity__c != null AND Registration_License_No__c != null AND Registration_License_No__c != '' ];
            }          
            
    }
        
    public void execute(Database.BatchableContext BC, list<Account> accountRecords){
        
        //sendAccountsToDsc(accountRecords);
        
        CC_DSC_DataShare ObjDSC=new CC_DSC_DataShare();
        ObjDSC.sendAccountsToDsc(accountRecords);
        
    }
    
    public void execute(SchedulableContext ctx) {
     
        database.executeBatch(this,20);
        
    }
    
    public void finish(Database.BatchableContext BC){
        
    }
    /*
    public void sendAccountsToDsc(List<Account> accountRecords){
        
        Set<Id> accountIds = new Set<Id>();
        
        for(Account a : accountRecords){
            accountIds.add(a.Id);
        }
        
        try {
            
            Map<Id,String> infoCountMap = getCurrentEmployeeCount(accountIds);
            
            list<servicesDscGovAeFreezonesintegratio.License> lstCompanyLicenses = new list<servicesDscGovAeFreezonesintegratio.License>();
            
            for(Account a : accountRecords){
    
                list<servicesDscGovAeFreezonesintegratio.EconomicActivity> lstEcnomicActivity = new list<servicesDscGovAeFreezonesintegratio.EconomicActivity>();
    
                if(!a.License_Activities__r.isEmpty()){
                    
                    for(License_Activity__c l : a.License_Activities__r){
                        
                        servicesDscGovAeFreezonesintegratio sc = new servicesDscGovAeFreezonesintegratio();
    
                        servicesDscGovAeFreezonesintegratio.EconomicActivity ecnomicActivity = new servicesDscGovAeFreezonesintegratio.EconomicActivity();
                        
                        ecnomicActivity.ActivityDescription = l.Formula_Activity_Name_Eng__c; // type of activity
                        //ecnomicActivity.ActivityCode = l.Type__c;
                        //ecnomicActivity.ActivityRevenue = '0';
                        
                        lstEcnomicActivity.add(ecnomicActivity);
                        
                    }
                    
                }
                
                list<servicesDscGovAeFreezonesintegratio.Shareholder> listShareHolders = new list<servicesDscGovAeFreezonesintegratio.Shareholder>();
                
                servicesDscGovAeFreezonesintegratio.License companyALicense = new servicesDscGovAeFreezonesintegratio.License();
                
                companyALicense.LicenseNo = a.Registration_License_No__c;
                companyALicense.LicenseNameAr = a.Arabic_Name__c;
                companyALicense.LicenseNameEn = a.Name;
                companyALicense.LicenseStatus = a.Active_License__r.Status__c;
                companyALicense.LicenseType = a.Sector_Classification__c;
                companyALicense.LegalEntity = a.Legal_Type_of_Entity__c;
                companyALicense.ZoneName = 'DIFC';
                companyALicense.EconomicActivities = lstEcnomicActivity;
                companyALicense.TotalEmployment = infoCountMap.get(a.Id); 
                
                companyALicense.LicenseCancelDate = a.Company_Type__c.equals('Financial - related') ? (a.DFSA_Withdrawn_Date__c != null ? String.valueOf(a.DFSA_Withdrawn_Date__c) : '') :
                                                    (a.Liquidated_Date__c != null ? String.valueOf(a.Liquidated_Date__c) : '');  
                 
                companyALicense.LicenseIssueDate = String.valueOf(a.Active_License__r.License_Issue_Date__c);
                companyALicense.LicenseExpiryDate = String.valueOf(a.Active_License__r.License_Expiry_Date__c);
                
                lstCompanyLicenses.add(companyALicense);
                
            }   
            
            servicesDscGovAeFreezonesintegratio.LicensesList_element liscenesList = new servicesDscGovAeFreezonesintegratio.LicensesList_element();
                
            liscenesList.Licenses = lstCompanyLicenses;
            liscenesList.Period = Datetime.valueOf(Label.DSCDIFC_Period); 
            liscenesList.Token = PasswordCryptoGraphy.DecryptPassword(Label.DSCDIFC_Password);
            
            tempuriOrg.BasicHttpBinding_IFZLicenseService tempURI = new tempuriOrg.BasicHttpBinding_IFZLicenseService();
        
            servicesDscGovAeFreezonesintegratio.ReplyMessage_element response_x = new servicesDscGovAeFreezonesintegratio.ReplyMessage_element();
            
            response_x = tempURI.UpdateLicenses(liscenesList);
            
            system.debug('response_x ' + response_x);
            
            insert LogDetails.CreateLog(null, 'ROC: Schedule Batch License Job', String.valueOf(response_x));
            
        } catch (Exception e){
            
            insert LogDetails.CreateLog(null, 'ROC: Schedule Batch License Job', 'Line Number : '+e.getLineNumber()+'\nException is : '+e);
                            
        }
    }
    
    
    private Map<Id,String> getCurrentEmployeeCount(set<Id> clientIds){
        
        Map<Id,String> infoCountMap = new Map<Id,String>();
        
        Map<Id,Integer> infoCountMapNum = new Map<Id,Integer>();
        
        for(Id clientId : clientIds){
            infoCountMapNum.put(clientId,0);
        } 
        
        for(Contact c : [select Id, (select Id, 
                                          Active__c,
                                          Object_Contact__c,
                                          Subject_Account__c,
                                          Relationship_Type__c from Secondary_Contact__r where 
                                          Subject_Account__c IN :clientIds AND Active__c = true AND 
                                          (Relationship_Type__c='Has DIFC Sponsored Employee' OR (Document_Active__c = true AND 
                                            (Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee') AND End_Date__c > TODAY ) ) AND 
                                             object_contact__c != NULL AND
                                             Object_Contact__r.RecordType.DeveloperName = 'GS_Contact') FROM Contact WHERE 
                                             RecordType.DeveloperName = 'GS_Contact' AND ID in 
                                             (select Object_Contact__c from Relationship__c where 
                                                      Subject_Account__c IN :clientIds AND Active__c = true AND 
                                                      (Relationship_Type__c='Has DIFC Sponsored Employee' OR (Document_Active__c = true AND 
                                                        (Relationship_Type__c = 'Has DIFC Local /GCC Employees' OR Relationship_Type__c = 'Has Temporary Employee') AND End_Date__c > TODAY ) ) AND 
                                                         object_contact__c != NULL AND
                                                         Object_Contact__r.RecordType.DeveloperName = 'GS_Contact')
                                             ]){
            
            if(!c.Secondary_Contact__r.isEmpty() &&
                infoCountMapNum.containsKey(c.Secondary_Contact__r[0].Subject_Account__c)){     
                infoCountMapNum.put(c.Secondary_Contact__r[0].Subject_Account__c,infoCountMapNum.get(c.Secondary_Contact__r[0].Subject_Account__c) + 1);
            }
        }
        
        for(Id i : clientIds) infoCountMap.put(i,String.valueOf(infoCountMapNum.get(i)));
        
        return infoCountMap;
    }
    */
}