@istest
public class Test_SAPWebServiceDetails {
	
    static testMethod void ECCServiceCallForScheduleTest(){
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
       Account acc = createAccount();
      Contact objContact = createContact(acc.Id);  
      Relationship__c objRel = createRelationship(acc.Id,objContact.Id); 
	  Service_Request__c sr = createSR(acc.Id,objContact.Id,GlobalConstants.TENANTS,GlobalConstants.VEHICLE_REG);             
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();      
      insert sr;           
      
      SR_Status__c srStatus = new SR_Status__c ();
      srStatus.name  = 'Submitted';
      srStatus.Code__c = 'Submitted';
      insert srStatus;
      
      sr.Submitted_Date__c = system.today();
      sr.Submitted_DateTime__c = system.now();
      sr.Internal_SR_Status__c = srStatus.ID;
      sr.External_SR_Status__c = srStatus.ID;
      sr.finalizeAmendmentFlg__c  = true;
      sr.Incorporated_Organization_Name__c = 'ABC123'; 
      sr.Activity_NOC_Required__c = false;
      update sr;           
      
      SR_Detail__c srDetail = createSRDetail(sr.Incorporated_Organization_Name__c,GlobalConstants.STAT_DRAFT,GlobalConstants.TENANTS, sr.Id); 
      insert srDetail;
      
      Step_Template__c  stpTemp = new Step_Template__c ();
      stpTemp.CODE__C  ='VERIFICATION_OF_APPLICATION';
      stpTemp.Step_RecordType_API_Name__c ='VERIFICATION_OF_APPLICATION';
      insert stpTemp;
      
      SR_Steps__c   stpTemplateCode = new SR_Steps__c   ();
      stpTemplateCode.Step_Template__c= stpTemp.ID;
      insert stpTemplateCode;
      
      Step__c step = new Step__c();
      step.SR__c = sr.ID;
      step.SR_Step__c = stpTemplateCode.ID;
      step.Step_Template__c  = stpTemp.ID;
      insert step;
      
      list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Car Parking Process';
        objTemplate.SR_RecordType_API_Name__c = 'Car_Parking';
        objTemplate.Menutext__c = 'DIFC Car Parking';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Other Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Car Parking';
        lstProducts.add(objProd);
        
        Product2 objPSAProd = new Product2();
        objPSAProd.Name = 'Car Parking Processing Fee';
        lstProducts.add(objPSAProd);                
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'New Parking Charges';
        objPL.DEV_Id__c = '1';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'PRK-004';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'Tag Charges';
        objPL.DEV_Id__c = '2';
        objPL.Product__c = lstProducts[1].Id;
        objPL.Material_Code__c = 'PRK-0001';
        objPL.Priority__c = 2;
        lstPLs.add(objPL);               
        
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            objDP.Unit_Price__c = 52.38;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = SR.Id;
        objItem.Price__c = 52.38;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        objItem.Status__c = 'Consumed';
        lstPriceItems.add(objItem);
        insert lstPriceItems;
        
		Test.setMock(WebServiceMock.class, new Test_SAPWebServiceMock());
        Test.setMock(HttpCalloutMock.class, new Test_SAPWebServiceMock());
        test.startTest();
        SAPWebServiceDetails.ECCServiceCallForSchedule(new List<String> {sr.Id},true);
        test.stopTest();
    }
    
    static testMethod void ECCServiceCallForScheduleTestMembership(){
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
       Account acc = createAccount();
      Contact objContact = createContact(acc.Id);  
      Relationship__c objRel = createRelationship(acc.Id,objContact.Id); 
	  Service_Request__c sr = createSR(acc.Id,objContact.Id,GlobalConstants.MEMBERSHIP,GlobalConstants.MEMBERSHIP_NEW);             
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();      
      insert sr;           
      
      SR_Status__c srStatus = new SR_Status__c ();
      srStatus.name  = 'Submitted';
      srStatus.Code__c = 'Submitted';
      insert srStatus;
      
      sr.Submitted_Date__c = system.today();
      sr.Submitted_DateTime__c = system.now();
      sr.Internal_SR_Status__c = srStatus.ID;
      sr.External_SR_Status__c = srStatus.ID;
      sr.finalizeAmendmentFlg__c  = true;
      sr.Incorporated_Organization_Name__c = 'ABC123'; 
      sr.Activity_NOC_Required__c = false;
      update sr;           
      
      SR_Detail__c srDetail = createSRDetail(sr.Incorporated_Organization_Name__c,GlobalConstants.STAT_DRAFT,GlobalConstants.TENANTS, sr.Id); 
      insert srDetail;
      
      Step_Template__c  stpTemp = new Step_Template__c ();
      stpTemp.CODE__C  ='VERIFICATION_OF_APPLICATION';
      stpTemp.Step_RecordType_API_Name__c ='VERIFICATION_OF_APPLICATION';
      insert stpTemp;
      
      SR_Steps__c   stpTemplateCode = new SR_Steps__c   ();
      stpTemplateCode.Step_Template__c= stpTemp.ID;
      insert stpTemplateCode;
      
      Step__c step = new Step__c();
      step.SR__c = sr.ID;
      step.SR_Step__c = stpTemplateCode.ID;
      step.Step_Template__c  = stpTemp.ID;
      insert step;
      
      list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Car Parking Process';
        objTemplate.SR_RecordType_API_Name__c = 'Car_Parking';
        objTemplate.Menutext__c = 'DIFC Car Parking';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Other Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Car Parking';
        lstProducts.add(objProd);
        
        Product2 objPSAProd = new Product2();
        objPSAProd.Name = 'Car Parking Processing Fee';
        lstProducts.add(objPSAProd);                
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'Membership Monthly';
        objPL.DEV_Id__c = '1';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'PRK-003';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'Membership Processing Fee';
        objPL.DEV_Id__c = '2';
        objPL.Product__c = lstProducts[1].Id;        
        objPL.Priority__c = 2;
        lstPLs.add(objPL);               
        
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            if(objP.Name != 'Membership Processing Fee'){
                objDP = new Dated_Pricing__c();
                objDP.Pricing_Line__c = objP.Id;
                objDP.Unit_Price__c = 1000;
                objDP.Date_From__c = system.today().addMonths(-1);
                objDP.Date_To__c = system.today().addYears(2);
                lstDP.add(objDP);
            }
            else{
                objDP = new Dated_Pricing__c();
                objDP.Pricing_Line__c = objP.Id;
                objDP.Unit_Price__c = 47.62;
                objDP.Date_From__c = system.today().addMonths(-1);
                objDP.Date_To__c = system.today().addYears(2);
                lstDP.add(objDP);
            }
        }
        insert lstDP;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = SR.Id;
        objItem.Price__c = 1000;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        objItem.Status__c = 'Consumed';
        lstPriceItems.add(objItem);
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = SR.Id;
        objItem.Price__c = 47.62;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        objItem.Status__c = 'Consumed';
        lstPriceItems.add(objItem);
        insert lstPriceItems;
        
		Test.setMock(WebServiceMock.class, new Test_SAPWebServiceMock());
        Test.setMock(HttpCalloutMock.class, new Test_SAPWebServiceMock());
        test.startTest();
        SAPWebServiceDetails.ECCServiceCallForSchedule(new List<String> {sr.Id},true);
        acc.Company_Code__c = '1100';
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = SR.Id;
        objItem.Price__c = 1000;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        objItem.Status__c = 'Consumed';
        insert objItem;
        
        SAPWebServiceDetails.ECCServiceCallToPushDPFuture(new List<String> {sr.Id},true);
        
        test.stopTest();
    }
    
    static testMethod void ECCServiceCallForScheduleTestMembershipRenewal(){
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
       Account acc = createAccount();
      Contact objContact = createContact(acc.Id);  
      Relationship__c objRel = createRelationship(acc.Id,objContact.Id); 
	  Service_Request__c sr = createSR(acc.Id,objContact.Id,GlobalConstants.MEMBERSHIP,GlobalConstants.MEMBERSHIP_RENEWAL);             
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();      
      insert sr;           
      
      SR_Status__c srStatus = new SR_Status__c ();
      srStatus.name  = 'Submitted';
      srStatus.Code__c = 'Submitted';
      insert srStatus;
      
      sr.Submitted_Date__c = system.today();
      sr.Submitted_DateTime__c = system.now();
      sr.Internal_SR_Status__c = srStatus.ID;
      sr.External_SR_Status__c = srStatus.ID;
      sr.finalizeAmendmentFlg__c  = true;
      sr.Incorporated_Organization_Name__c = 'ABC123'; 
      sr.Activity_NOC_Required__c = false;
      update sr;           
      
      SR_Detail__c srDetail = createSRDetail(sr.Incorporated_Organization_Name__c,GlobalConstants.STAT_DRAFT,GlobalConstants.TENANTS, sr.Id); 
      insert srDetail;
      
      Step_Template__c  stpTemp = new Step_Template__c ();
      stpTemp.CODE__C  ='VERIFICATION_OF_APPLICATION';
      stpTemp.Step_RecordType_API_Name__c ='VERIFICATION_OF_APPLICATION';
      insert stpTemp;
      
      SR_Steps__c   stpTemplateCode = new SR_Steps__c   ();
      stpTemplateCode.Step_Template__c= stpTemp.ID;
      insert stpTemplateCode;
      
      Step__c step = new Step__c();
      step.SR__c = sr.ID;
      step.SR_Step__c = stpTemplateCode.ID;
      step.Step_Template__c  = stpTemp.ID;
      insert step;
      
      list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Car Parking Process';
        objTemplate.SR_RecordType_API_Name__c = 'Car_Parking';
        objTemplate.Menutext__c = 'DIFC Car Parking';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Other Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Car Parking';
        lstProducts.add(objProd);
        
        Product2 objPSAProd = new Product2();
        objPSAProd.Name = 'Car Parking Processing Fee';
        lstProducts.add(objPSAProd);                
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'Membership Monthly';
        objPL.DEV_Id__c = '1';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'PRK-003';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'Renewal Fees';
        objPL.DEV_Id__c = '2';
        objPL.Product__c = lstProducts[1].Id;        
        objPL.Priority__c = 2;
        lstPLs.add(objPL);               
        
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            if(objP.Name != 'Renewal Fees'){
                objDP = new Dated_Pricing__c();
            	objDP.Pricing_Line__c = objP.Id;
            	objDP.Unit_Price__c = 1000;
            	objDP.Date_From__c = system.today().addMonths(-1);
            	objDP.Date_To__c = system.today().addYears(2);
            	lstDP.add(objDP);
            }
            else{
                objDP = new Dated_Pricing__c();
            	objDP.Pricing_Line__c = objP.Id;
            	objDP.Unit_Price__c = 4.76;
            	objDP.Date_From__c = system.today().addMonths(-1);
            	objDP.Date_To__c = system.today().addYears(2);
            	lstDP.add(objDP);
            }
        }
        insert lstDP;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = SR.Id;
        objItem.Price__c = 1000;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        objItem.Status__c = 'Consumed';
        lstPriceItems.add(objItem);
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = SR.Id;
        objItem.Price__c = 47.62;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        objItem.Status__c = 'Consumed';
        lstPriceItems.add(objItem);
        insert lstPriceItems;
        
		Test.setMock(WebServiceMock.class, new Test_SAPWebServiceMock());
        Test.setMock(HttpCalloutMock.class, new Test_SAPWebServiceMock());
        test.startTest();
        SAPWebServiceDetails.ECCServiceCallForSchedule(new List<String> {sr.Id},true);
        SAPWebServiceDetails.ECCServiceCall(new List<String> {sr.Id},true);
        test.stopTest();
    }
    
    static testMethod void ECCReceiptCallTest(){
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
       Account acc = createAccount();
      Contact objContact = createContact(acc.Id);  
      Relationship__c objRel = createRelationship(acc.Id,objContact.Id); 
	  Service_Request__c sr = createSR(acc.Id,objContact.Id,GlobalConstants.MEMBERSHIP,GlobalConstants.MEMBERSHIP_RENEWAL);             
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();      
      insert sr;           
      
      SR_Status__c srStatus = new SR_Status__c ();
      srStatus.name  = 'Submitted';
      srStatus.Code__c = 'Submitted';
      insert srStatus;
      
      sr.Submitted_Date__c = system.today();
      sr.Submitted_DateTime__c = system.now();
      sr.Internal_SR_Status__c = srStatus.ID;
      sr.External_SR_Status__c = srStatus.ID;
      sr.finalizeAmendmentFlg__c  = true;
      sr.Incorporated_Organization_Name__c = 'ABC123'; 
      sr.Activity_NOC_Required__c = false;
      update sr;           
      
      SR_Detail__c srDetail = createSRDetail(sr.Incorporated_Organization_Name__c,GlobalConstants.STAT_DRAFT,GlobalConstants.TENANTS, sr.Id); 
      insert srDetail;
      
      Step_Template__c  stpTemp = new Step_Template__c ();
      stpTemp.CODE__C  ='VERIFICATION_OF_APPLICATION';
      stpTemp.Step_RecordType_API_Name__c ='VERIFICATION_OF_APPLICATION';
      insert stpTemp;
      
      SR_Steps__c   stpTemplateCode = new SR_Steps__c   ();
      stpTemplateCode.Step_Template__c= stpTemp.ID;
      insert stpTemplateCode;
      
      Step__c step = new Step__c();
      step.SR__c = sr.ID;
      step.SR_Step__c = stpTemplateCode.ID;
      step.Step_Template__c  = stpTemp.ID;
      insert step;
      
      list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Car Parking Process';
        objTemplate.SR_RecordType_API_Name__c = 'Car_Parking';
        objTemplate.Menutext__c = 'DIFC Car Parking';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Other Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Car Parking';
        lstProducts.add(objProd);
        
        Product2 objPSAProd = new Product2();
        objPSAProd.Name = 'Car Parking Processing Fee';
        lstProducts.add(objPSAProd);                
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'Membership Monthly';
        objPL.DEV_Id__c = '1';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'PRK-003';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'Renewal Fees';
        objPL.DEV_Id__c = '2';
        objPL.Product__c = lstProducts[1].Id;        
        objPL.Priority__c = 2;
        lstPLs.add(objPL);               
        
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            if(objP.Name != 'Renewal Fees'){
                objDP = new Dated_Pricing__c();
            	objDP.Pricing_Line__c = objP.Id;
            	objDP.Unit_Price__c = 1000;
            	objDP.Date_From__c = system.today().addMonths(-1);
            	objDP.Date_To__c = system.today().addYears(2);
            	lstDP.add(objDP);
            }
            else{
                objDP = new Dated_Pricing__c();
            	objDP.Pricing_Line__c = objP.Id;
            	objDP.Unit_Price__c = 4.76;
            	objDP.Date_From__c = system.today().addMonths(-1);
            	objDP.Date_To__c = system.today().addYears(2);
            	lstDP.add(objDP);
            }
        }
        insert lstDP;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = SR.Id;
        objItem.Price__c = 1000;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        objItem.Status__c = 'Consumed';
        lstPriceItems.add(objItem);
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = SR.Id;
        objItem.Price__c = 47.62;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        objItem.Status__c = 'Consumed';
        lstPriceItems.add(objItem);
        insert lstPriceItems;
        
		Test.setMock(WebServiceMock.class, new Test_SAPWebServiceMock());
        Test.setMock(HttpCalloutMock.class, new Test_SAPWebServiceMock());
        List<Receipt__c> receiptList = new List<Receipt__c>();
        Receipt__c objReceipt = new Receipt__c();
        objReceipt.Customer__c = acc.id;
        objReceipt.Amount__c = 1050.00;
        objReceipt.Receipt_Type__c = 'Cash';
        objReceipt.Payment_Status__c = 'Success';
        insert objReceipt;
        test.startTest();
        SAPWebServiceDetails.ECCReceiptCall(new List<String> {objReceipt.Id},true);
        objReceipt = new Receipt__c();
        objReceipt.Customer__c = acc.id;
        objReceipt.Amount__c = 1050.00;
        objReceipt.Receipt_Type__c = 'Bank Transfer';
        objReceipt.Payment_Status__c = 'Success';
        insert objReceipt;
        SAPWebServiceDetails.ECCReceiptCall(new List<String> {objReceipt.Id},true);
        objReceipt = new Receipt__c();
        objReceipt.Customer__c = acc.id;
        objReceipt.Amount__c = 1050.00;
        objReceipt.Receipt_Type__c = 'Cheque';
        objReceipt.Payment_Status__c = 'Success';
        insert objReceipt;
        SAPWebServiceDetails.ECCReceiptCall(new List<String> {objReceipt.Id},true);
        test.stopTest();
    }
    
    static testmethod void CRMPartnerCallTest(){
         WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'CRM';
        objEP.URL__c = 'http://CRM.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
    	Account acc = createAccount();
        Contact objContact = createContact(acc.Id);
        Relationship__c objRelationship = createRelationship(acc.id,objContact.Id);
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        Test.setMock(WebServiceMock.class, new Test_SAPWebServiceMock());
        Test.setMock(HttpCalloutMock.class, new Test_SAPWebServiceMock());
        Test.startTest();
        Map<String,String> mapContactIds = new Map<String,String>();
        mapContactIds.put(objContact.Id,objContact.id);
        Map<String,String> mapAccountIds = new Map<String,String>();
        mapAccountIds.put(acc.Id,'OrganizerAccount');
        SAPWebServiceDetails.CRMPartnerCall(new List<String> {objRelationship.Id}, mapContactIds, mapAccountIds);
        Test.stopTest();
    }
    
    static testmethod void CRMPartnerCallConvertedAccTest(){
         WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'CRM';
        objEP.URL__c = 'http://CRM.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
    	Account acc = createAccount();
        acc.Company_Type__c = 'Non - financial';
        acc.Building_Name__c = 'Gate Village Building 5';
        acc.BD_Sector__c = 'Corporates and Service Provider';
        acc.Priority_to_DIFC__c = 'T3: No Global & low FTE';
        acc.Geographic_Region__c = 'GCC';       
        Contact objContact = createContact(acc.Id);
        Relationship__c objRelationship = createRelationship(acc.id,objContact.Id);
        objRelationship.Relationship_Type__c = 'Has General Partner';
        update objRelationship;
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        Test.setMock(WebServiceMock.class, new Test_SAPWebServiceMock());
        Test.setMock(HttpCalloutMock.class, new Test_SAPWebServiceMock());
        Test.startTest();
        Map<String,String> mapContactIds = new Map<String,String>();
        mapContactIds.put(objContact.Id,objContact.id);
        Map<String,String> mapAccountIds = new Map<String,String>();
        mapAccountIds.put(acc.Id,'ConvertedAccount');
        SAPWebServiceDetails.CRMPartnerCall(new List<String> {objRelationship.Id}, mapContactIds, mapAccountIds);
        SAPWebServiceDetails.CRMPartnerExtensionCall(mapAccountIds);
        Map<String,String> mapRelationShipPartners = new Map<String,String>();
        mapRelationShipPartners.put(acc.id,'78989');
        SAPWebServiceDetails.CRMCreateRelationship(new list<string> {objRelationship.Id},mapRelationShipPartners);
        Test.stopTest();
    }
    
    private static Account createAccount(){
 		 Account acc = new Account();
      	 acc.Name = 'Test Account';
      	 insert acc;
        return acc;
    }
    
    private static Contact createContact(String AccountId){
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = AccountId;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        return objContact;
    }
    
    private static Relationship__c createRelationship(String accountId, String contactId){
        Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = accountId;
        objRel.Object_Contact__c = contactId;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        return objRel;
    }
    
    private static Service_Request__c createSR(String accId, String conId, String reqType, String type){
        Service_Request__c sr = new Service_Request__c();
      	sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
      	sr.Customer__c = accId;
      	sr.Contact__c = conId;
      	sr.Commercial_Activity__c = reqType;
      	sr.Client_s_Representative_in_Charge__c = type;      
        return sr;
    }

	private static SR_Detail__c createSRDetail(String cardNumber,String status,String reqType, String srId){        
            SR_Detail__c amm2 = new SR_Detail__c ();
            amm2.Vehicle_Color__c = 'White';
            amm2.Vehicle_Manufacturer__c = 'Honda';
            amm2.Vehicle_Number__c = '1234';
            amm2.Vehicle_Type__c  = 'Sedan';           
            amm2.Status__c = status;
            amm2.Is_Card_Required__c = false;
            amm2.Registered_In__c = 'Dubai';            
            //Shikha - adding membership fields   
        	if(reqType == GlobalConstants.MEMBERSHIP){
            	amm2.GD_GV__c = 'GD';   
            	amm2.Paid__c = true;
            	amm2.NotCarOwner__c = false;
        	}                     
            amm2.CardNumber__c = cardNumber;            
        	amm2.Service_Request__c = srId;
        return amm2;
    }
     
    private static Membership__c createMembership(Service_Request__c objSR, String requestType, String status){        
        Membership__c objMem = new Membership__c();               
            objMem.Contact__c = objSR.Contact__c;
            objMem.FromDate__c = objSR.Mortgage_Start_Date__c;
            objMem.Effective_Start_Date__c = objSR.Agreement_Date__c;
            objMem.ToDate__c = objSR.Mortgage_End_Date__c;
            objMem.Period__c = objSR.Commercial_Activity_Previous_Sponsor__c;
            objMem.Tenure__c = objSR.Mortgage_Amount__c;
            objMem.Card_Number__c = objSR.Incorporated_Organization_Name__c;
        	objMem.MembershipStatus__c = status;           
            if(requestType == GlobalConstants.TENANTS){
                Id recordTypeId = Schema.SObjectType.Membership__c.getRecordTypeInfosByName().get(GlobalConstants.TENANTS).getRecordTypeId(); 
                objMem.RecordTypeId = recordTypeId;                
            }
            else{
                Id recordTypeId = Schema.SObjectType.Membership__c.getRecordTypeInfosByName().get(GlobalConstants.MEMBERSHIP).getRecordTypeId(); 
                objMem.RecordTypeId = recordTypeId;
            }
            system.debug('### membership object created '+objMem);
            insert objMem;
     		return objMem;   
    }
    
    /*static testMethod void ECCServiceCallToPushDPFutureTest(){
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
       Account acc = createAccount();
      Contact objContact = createContact(acc.Id);  
      Relationship__c objRel = createRelationship(acc.Id,objContact.Id); 
	  Service_Request__c sr = createSR(acc.Id,objContact.Id,GlobalConstants.TENANTS,GlobalConstants.VEHICLE_REG);             
      sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();      
      insert sr;           
      
      SR_Status__c srStatus = new SR_Status__c ();
      srStatus.name  = 'Submitted';
      srStatus.Code__c = 'Submitted';
      insert srStatus;
      
      sr.Submitted_Date__c = system.today();
      sr.Submitted_DateTime__c = system.now();
      sr.Internal_SR_Status__c = srStatus.ID;
      sr.External_SR_Status__c = srStatus.ID;
      sr.finalizeAmendmentFlg__c  = true;
      sr.Incorporated_Organization_Name__c = 'ABC123'; 
      sr.Activity_NOC_Required__c = false;
      update sr;           
      
      SR_Detail__c srDetail = createSRDetail(sr.Incorporated_Organization_Name__c,GlobalConstants.STAT_DRAFT,GlobalConstants.TENANTS, sr.Id); 
      insert srDetail;
      
      Step_Template__c  stpTemp = new Step_Template__c ();
      stpTemp.CODE__C  ='VERIFICATION_OF_APPLICATION';
      stpTemp.Step_RecordType_API_Name__c ='VERIFICATION_OF_APPLICATION';
      insert stpTemp;
      
      SR_Steps__c   stpTemplateCode = new SR_Steps__c   ();
      stpTemplateCode.Step_Template__c= stpTemp.ID;
      insert stpTemplateCode;
      
      Step__c step = new Step__c();
      step.SR__c = sr.ID;
      step.SR_Step__c = stpTemplateCode.ID;
      step.Step_Template__c  = stpTemp.ID;
      insert step;
      
      list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Car Parking Process';
        objTemplate.SR_RecordType_API_Name__c = 'Car_Parking';
        objTemplate.Menutext__c = 'DIFC Car Parking';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Other Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Car Parking';
        lstProducts.add(objProd);
        
        Product2 objPSAProd = new Product2();
        objPSAProd.Name = 'Car Parking Processing Fee';
        lstProducts.add(objPSAProd);                
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'New Parking Charges';
        objPL.DEV_Id__c = '1';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'PRK-004';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        
        objPL = new Pricing_Line__c();
        objPL.Name = 'Tag Charges';
        objPL.DEV_Id__c = '2';
        objPL.Product__c = lstProducts[1].Id;
        objPL.Material_Code__c = 'PRK-0001';
        objPL.Priority__c = 2;
        lstPLs.add(objPL);               
        
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            objDP.Unit_Price__c = 52.38;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = SR.Id;
        objItem.Price__c = 52.38;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        objItem.Status__c = 'Consumed';
        lstPriceItems.add(objItem);
        insert lstPriceItems;
        
		Test.setMock(WebServiceMock.class, new Test_SAPWebServiceMock());
        Test.setMock(HttpCalloutMock.class, new Test_SAPWebServiceMock());
        test.startTest();
        SAPWebServiceDetails.ECCServiceCallForSchedule(new List<String> {sr.Id},true);
        test.stopTest();
    }*/
}