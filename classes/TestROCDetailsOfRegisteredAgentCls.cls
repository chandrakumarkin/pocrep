@isTest(SeeAllData=false)
public class TestROCDetailsOfRegisteredAgentCls{
    
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_of_Address_Details')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Change Of Business Activity';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Application_of_Registration';
        insert objPageFlow;
        
        Page__c objPage = new Page__c();
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        insert objPage;
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.License_Activity__c  = '318';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        //objSR.Internal_SR_Status__c = lstStatus[0].Id;
        //objSR.External_SR_Status__c = lstStatus[0].Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('Application_of_Registration');
        insert objSR;
        
        Amendment__c amnd3 = new Amendment__c (ServiceRequest__c=objSR.id,Relationship_Type__c='Registered Agent',Amendment_Type__c='Body Corporate',Place_of_Registration__c = 'Test Location');
        insert amnd3;
        
          Apexpages.currentPage().getParameters().put('id',objSR.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('PageId',objPage.Id);
        
        ROCDetailsOfRegisteredAgent_Cls roc = new ROCDetailsOfRegisteredAgent_Cls();
        roc.SelectedType = objAccount.ID;
        roc.strNavigatePageId  = objPage.Id;
        ROC.GetSelectedAccountsDetail();
        roc.goTopage();
        roc.DynamicButtonAction();
        roc.BackendSRUpdate();
        roc.SaveAmnd();
        
     }

}