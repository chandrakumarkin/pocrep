@isTest
public with sharing class OB_SecurityStepStatusUpdateBatchTest {
	@isTest
    private static void initSecurityStepStatusUpdateBatch(){  
    	// create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Non - financial';
        insertNewSRs[1].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','In_Principle');
        insertNewSRs[1].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','AOR_Financial');
        insert insertNewSRs;
        
        HexaBPM__Status__c srStatus = new HexaBPM__Status__c();
        srStatus.Name = 'In Progress';
        srStatus.HexaBPM__Code__c = 'In Progress';
		insert srStatus;
        
        List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(1,
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'In_Principle','AOR_Financial'},
                                                        new List<String>{'Test'});

        insert stepTemplate;
        
        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
        objSRSteps.HexaBPM__Active__c = true;
       // objSRSteps.HexaBPM__Status__c = srStatus[0].Id;
        insert objSRSteps;
        
        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(2,insertNewSRs,new List<HexaBPM__SR_Steps__c>{objSRSteps});
		stepList[0].HexaBPM__Status__c = srStatus.Id;
		stepList[1].HexaBPM__Status__c = srStatus.Id;
		insert stepList;
		system.debug('==step===='+[SELECT Id,Name,Step_Template_Code__c FROM HexaBPM__Step__c]);
		test.starttest();
		
		Test.setMock(HttpCalloutMock.class, new OB_SecurityStepStatusMockTest());
		
		OB_SecurityStepStatusUpdateBatch obj = new OB_SecurityStepStatusUpdateBatch();
		
		OB_SecurityStepStatusUpdateBatch.testStatus='NEED_MORE_INFO';
        DataBase.executeBatch(obj); 
        
		/*OB_SecurityStepStatusUpdateBatch.testStatus='NEED_MORE_INFO';
        DataBase.executeBatch(obj); 
		OB_SecurityStepStatusUpdateBatch.testStatus='Rejected';
        DataBase.executeBatch(obj); */
		test.stoptest();
    } 
	@isTest
    private static void initSecurityStepStatusApproved(){  
    	  
    	// create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Non - financial';
        insertNewSRs[1].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','In_Principle');
        insertNewSRs[1].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','AOR_Financial');
        insert insertNewSRs;
        
        HexaBPM__Status__c srStatus = new HexaBPM__Status__c();
        srStatus.Name = 'In Progress';
        srStatus.HexaBPM__Code__c = 'In Progress';
		insert srStatus;
        
        List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(1,
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'In_Principle','AOR_Financial'},
                                                        new List<String>{'Test'});

        insert stepTemplate;
        
        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
        objSRSteps.HexaBPM__Active__c = true;
       // objSRSteps.HexaBPM__Status__c = srStatus[0].Id;
        insert objSRSteps;
        
        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(2,insertNewSRs,new List<HexaBPM__SR_Steps__c>{objSRSteps});
		stepList[0].HexaBPM__Status__c = srStatus.Id;
		stepList[1].HexaBPM__Status__c = srStatus.Id;
		insert stepList;
		system.debug('==step===='+[SELECT Id,Name,Step_Template_Code__c FROM HexaBPM__Step__c]);
		test.starttest();
		
		Test.setMock(HttpCalloutMock.class, new OB_SecurityStepStatusMockTest());
		
		OB_SecurityStepStatusUpdateBatch obj = new OB_SecurityStepStatusUpdateBatch();
		
		OB_SecurityStepStatusUpdateBatch.testStatus='APPROVED';
        DataBase.executeBatch(obj); 
        
		/*OB_SecurityStepStatusUpdateBatch.testStatus='NEED_MORE_INFO';
        DataBase.executeBatch(obj); 
		OB_SecurityStepStatusUpdateBatch.testStatus='Rejected';
        DataBase.executeBatch(obj); */
		test.stoptest();
    }
	@isTest
    private static void initSecurityStepStatusReject(){  
    	  
    	// create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Non - financial';
        insertNewSRs[1].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','In_Principle');
        insertNewSRs[1].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','AOR_Financial');
        insert insertNewSRs;
        
        HexaBPM__Status__c srStatus = new HexaBPM__Status__c();
        srStatus.Name = 'In Progress';
        srStatus.HexaBPM__Code__c = 'In Progress';
		insert srStatus;
        
        List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(1,
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'In_Principle','AOR_Financial'},
                                                        new List<String>{'Test'});

        insert stepTemplate;
        
        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
        objSRSteps.HexaBPM__Active__c = true;
       // objSRSteps.HexaBPM__Status__c = srStatus[0].Id;
        insert objSRSteps;
        
        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(2,insertNewSRs,new List<HexaBPM__SR_Steps__c>{objSRSteps});
		stepList[0].HexaBPM__Status__c = srStatus.Id;
		stepList[1].HexaBPM__Status__c = srStatus.Id;
		insert stepList;
		system.debug('==step===='+[SELECT Id,Name,Step_Template_Code__c FROM HexaBPM__Step__c]);
		test.starttest();
		
		Test.setMock(HttpCalloutMock.class, new OB_SecurityStepStatusMockTest());
		
		OB_SecurityStepStatusUpdateBatch obj = new OB_SecurityStepStatusUpdateBatch();
		
		
		OB_SecurityStepStatusUpdateBatch.testStatus='Rejected';
        DataBase.executeBatch(obj); 
		test.stoptest();
    }
	@isTest
    private static void initSecurityStepStatusSch(){  
    	// create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert insertNewContacts;

        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Non - financial';
        insertNewSRs[1].Entity_Type__c = 'Non - financial';
        insertNewSRs[0].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','In_Principle');
        insertNewSRs[1].recordtypeid = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','AOR_Financial');
        insert insertNewSRs;
        
        HexaBPM__Status__c srStatus = new HexaBPM__Status__c();
        srStatus.Name = 'In Progress';
        srStatus.HexaBPM__Code__c = 'In Progress';
		insert srStatus;
        
        List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(1,
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'In_Principle','AOR_Financial'},
                                                        new List<String>{'Test'});

        insert stepTemplate;
        
        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
        objSRSteps.HexaBPM__Active__c = true;
       // objSRSteps.HexaBPM__Status__c = srStatus[0].Id;
        insert objSRSteps;
        
        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(2,insertNewSRs,new List<HexaBPM__SR_Steps__c>{objSRSteps});
		stepList[0].HexaBPM__Status__c = srStatus.Id;
		stepList[1].HexaBPM__Status__c = srStatus.Id;
		insert stepList;
		system.debug('==step===='+[SELECT Id,Name,Step_Template_Code__c FROM HexaBPM__Step__c]);
		test.starttest();
		
		Test.setMock(HttpCalloutMock.class, new OB_SecurityStepStatusMockTest());
		
		OB_SecurityStepUpdateBatchschedule sh1 = new OB_SecurityStepUpdateBatchschedule();
		String sch ='0 0 0 * * ?'; 
		System.schedule('Test', sch,sh1);
		test.stoptest();
    } 
}