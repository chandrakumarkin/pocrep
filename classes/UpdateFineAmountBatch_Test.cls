/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 07-16-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   07-07-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
public class UpdateFineAmountBatch_Test {
    static testMethod void CreateAutomaticShipments(){
        
        Account objAccount        = new Account();
        objAccount.Name           = 'Test Custoer 1';
        objAccount.E_mail__c      = 'test@test.com';
        objAccount.BP_No__c       = '001234';
        objAccount.Company_Type__c          = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c  = 'LTD';
        objAccount.ROC_Status__c            = 'Active';
        objAccount.Financial_Year_End__c    = '31st December';
        insert objAccount;
        
        SR_Template__c srTemp            = new SR_Template__c (); 
        srTemp.Name                      = 'Establishment Card Application';
        srTemp.SR_RecordType_API_Name__c = 'Index_Card_Other_Services';
        srTemp.Menutext__c               = 'Establishment Card Application';
        insert srTemp;

        
         
        SR_Status__c srStatus = new SR_Status__c(Name ='Draft',Code__c='Draft');
        insert srStatus;
        
        Service_Request__c objSR1    = new Service_Request__c();
        objSR1.Customer__c           = objAccount.Id;
        objSR1.Submitted_Date__c     = Date.today();
        objSR1.SR_Template__c        = srTemp.Id;
        objSR1.External_SR_Status__c = srStatus.id;
        insert objSR1;

        Test.setCreatedDate(objSR1.Id, System.today().addDays(-40));  
        
        String expectedValue = [SELECT ID,Service_Type__c FROM Service_Request__c WHERE ID =: objSR1.Id].Service_Type__c;
        
        System.assertEquals('Establishment Card Application', expectedValue);
        System.assertEquals(srStatus.Id, [SELECT ID,External_SR_Status__c FROM Service_Request__c WHERE ID =:objSR1.Id].External_SR_Status__c);
        System.assertEquals(40, Date.valueOf([SELECT ID,CreatedDate FROM Service_Request__c WHERE ID =:objSR1.Id].CreatedDate).daysBetween(System.Today()));
       
        
        product2 prod                = new product2();
        prod.Name                    ='Penalty Charges';
        prod.ProductCode             ='Penalty Charges';
        prod.isActive                = true;
        prod.Family                  = 'GS' ;
        insert prod;
        
        Pricing_Line__c  prLine    = new Pricing_Line__c ();
        prLine.Priority__c         = 1;
        prLine.Material_Code__c    = 'GO_DNRD_Fine';       
        insert prLine;
        
        SR_Price_Item__c objSRItem   = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c  = objSR1.Id;
        objSRItem.Status__c          = 'Blocked';
        objSRItem.Pricing_Line__c    = prLine.Id;
        objSRItem.Price__c           = 100;
        insert objSRItem;
                  
      Test.startTest();
           SchedulableContext sc = null;
           UpdateFineAmountBatch obj = new UpdateFineAmountBatch();
           obj.execute(sc);
      Test.stopTest();
      System.assertEquals(200.00, [SELECT ID,Price__c FROM SR_Price_Item__c WHERE ID =:objSRItem.Id].Price__c);
    }

}