/**
    *Author      : Shikha
    *CreatedDate : 04-03-2021
    *Description : This apex class is used to view RM details on home page  
**/
@istest
public class Test_OB_RMDetailsController {
    
    @testsetup
    public static void setupData(){
        List<Account> acc = OB_TestDataFactory.createAccounts(1);
        insert acc;
        List<Contact> con = OB_TestDataFactory.createContacts(1, acc);
        insert con;
        system.debug('### con '+con);
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        system.debug('portalRole is ' + portalRole);
        
        //Profile profile1 = [Select Id from Profile where name = 'DIFC Customer Community Plus User Custom'];
        Profile profile2 = [Select Id from Profile where name = 'BD Financial Team'];
        
        User portalAccountOwner2 = new User(        
            ProfileId = profile2.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'      
        );
        List<user> users = new List<User>();
        //users.add(user1);
        users.add(portalAccountOwner2);
        Database.insert(users);
        system.debug('### users '+users);
        system.runAs(users[0]){
            acc[0].OwnerId = users[0].Id;
            update acc;
        }        
    }
    
    @istest
    public static void viewContactInfo(){
        // UserRole portalRole = [Select Id From UserRole Where PortalType = 'CustomerPortal' Limit 1];        
        /* List<Account> accountList = [SELECT Id, OwnerId, Name, Owner.Name, Owner.Title, Owner.Email,Owner.MobilePhone, Owner.Gender__c FROM Account];
UserRole r = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
insert r;
Profile p = [Select Id from Profile where name = 'DIFC Customer Community Plus User Custom'];
Contact c = [SELECT Id FROM Contact];
User pu = new User(profileId = p.id, username = 'testemail@test.com', email = 'testemail@test.com',
emailencodingkey = 'UTF-8', localesidkey = 'en_US',
languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
alias='cspu', lastname='lastname', contactId = c.id, userroleid=r.Id);
insert pu;*/
        //Create portal account owner
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = System.now().millisecond() + 'test2@test.com',
            Alias = 'batman',
            Email='bruce.wayne@wayneenterprises.com',
            EmailEncodingKey='UTF-8',
            Firstname='Bruce',
            Lastname='Wayne',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);
        
        //Create account
        system.runAs(portalAccountOwner1){
        Account portalAccount1 = new Account(
            Name = 'TestAccount',
            OwnerId = portalAccountOwner1.Id
        );
        Database.insert(portalAccount1);
        
        //Create contact
        Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'McTesty',
            AccountId = portalAccount1.Id,
            Email = System.now().millisecond() + 'test@test.com',
            OB_Block_Homepage__c = false
        );
        Database.insert(contact1);
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name LIKE 'DIFC Customer Community Plus User Custom' Limit 1];
        User user1 = new User(
            Username = System.now().millisecond() + 'test12345@test.com',
            ContactId = contact1.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
            system.runAs(user1){
                List<Account> accountList = [SELECT Id, OwnerId, Name, Owner.Name, Owner.Title, Owner.Email,Owner.MobilePhone, Owner.Gender__c FROM Account];
                Test.startTest();
                OB_RMDetailsController.viewContactInfo();
                Test.stopTest();
            }
    }
    }
    
}