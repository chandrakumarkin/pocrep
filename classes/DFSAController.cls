public class DFSAController {
	
	@AuraEnabled
	public static void insertNewSecurityUsers(DFSA_Security_Users__c securityUser){
			
		User eachUser = new User();
		eachUser = [SELECT Contact.AccountID FROM User where ID=:userinfo.getUserId()];
		securityUser.Customer__c = eachUser.Contact.AccountID;
		INSERT securityUser;
	}
    
}