/******************************************************************************************
*  Author   : Ravindra Babu Nagaboina
*  Company  : NSI DMCC
*  Date     : 09th Nov 2016   
*  Description : This class will be used as Lease Trigger Handler.                       
--------------------------------------------------------------------------------------------------------------------------
Modification History 
----------------------------------------------------------------------------------------
V.No   Date            Updated By          Description
----------------------------------------------------------------------------------------              
V1.0  09/Nov/2016      Ravi                Created

V1.2  24/Nov/2020      Abbas               Need to activate operating location when lease is reactivated.

V1.3  10/Feb/2020      Abbas               #13704
*******************************************************************************************/
public with sharing class LeaseTriggerHandler implements  TriggerFactoryInterface{
    
    public void executeBeforeInsertTrigger(list<sObject> lstNewRecords){
        
    }
    
    public void executeBeforeUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
        
    }
    
    public void executeBeforeInsertUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
        
    }
    
    public void executeAfterInsertTrigger(list<sObject> lstNewRecords){
        
    }
    
    public void executeAfterUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
        map<Id,Lease__c> mapOldLeaseRecords = new map<Id,Lease__c>();
        if(mapOldRecords != null)
            mapOldLeaseRecords = (map<Id,Lease__c>)mapOldRecords;
        UpdateOperatingLocationsFromLease(lstNewRecords,mapOldLeaseRecords);
    }
    
    public void executeAfterInsertUpdateTrigger(list<sObject> lstNewRecords,map<Id,sObject> mapOldRecords){
        
    }
    
    public void UpdateOperatingLocationsFromLease(list<Lease__c> lstNewLeases, map<Id,Lease__c> mapOldLeases){
        system.debug('ZZZ LeaseTH->In UpdateOperatingLocationsFromLease_M !!!');
        list<Operating_Location__c> lstOperatingLocations = new list<Operating_Location__c>();
        list<Id> lstLeaseIds = new list<Id>();
        list<Id> lstOLIds = new list<Id>();
        
        list<Id> lstFutureLeaseIds = new list<Id>();
        
        Set<String> reActivatedLeaseIdSet = new Set<String>();
        
        if(lstNewLeases != null){
            if(mapOldLeases != null){
                for(Lease__c objLease : lstNewLeases){
                    if(objLease.Status__c != 'Active' && mapOldLeases.get(objLease.Id).Status__c == 'Active' && (objLease.Type__c == 'Leased' || objLease.Is_License_to_Occupy__c == true ) ){
                        lstLeaseIds.add(objLease.Id);
                    }
                    system.debug('ZZZ LeaseTH->objLease.Status__c '+objLease.Status__c);
                    system.debug('ZZZ LeaseTH->mapOldLeases.get(objLease.Id).Status__c '+mapOldLeases.get(objLease.Id).Status__c);
                    if(objLease.Status__c == 'Active' && mapOldLeases.get(objLease.Id).Status__c == 'Future Lease' && (objLease.Type__c == 'Leased' || objLease.Is_License_to_Occupy__c == true ) ){
                        lstFutureLeaseIds.add(objLease.Id);
                    }
                    
                    //V1.2
                    if(objLease.Status__c == 'Active' && mapOldLeases.get(objLease.Id).Status__c != 'Active' && (objLease.Type__c == 'Leased' || objLease.Is_License_to_Occupy__c == true ) ){
                        reActivatedLeaseIdSet.add(objLease.Id);
                    }
                }
            }
            
            System.debug('ZZZ LeaseTH->lstLeaseIds->'+lstLeaseIds);
            System.debug('ZZZ LeaseTH->lstFutureLeaseIds->'+lstFutureLeaseIds);
            System.debug('ZZZ LeaseTH->reActivatedLeaseIdSet->'+reActivatedLeaseIdSet);
            
            
            if(!lstLeaseIds.isEmpty()){
                for(Operating_Location__c objOL : [select Id,Name,Status__c,IsRegistered__c,Hosting_Company__c from Operating_Location__c where Lease__c IN : lstLeaseIds AND Status__c = 'Active']){
                    objOL.Status__c = 'Inactive';
                    objOL.IsRegistered__c = false;
                    lstOperatingLocations.add(objOL);
                    lstOLIds.add(objOL.Id);
                }
                //v1.3 Added "Valid_for_Inactive__c" in Filter
                for(Operating_Location__c objOL : [select Id,Name,Status__c,IsRegistered__c,Hosting_Company__c from Operating_Location__c where Id NOT IN : lstOLIds AND Status__c = 'Active' and Valid_for_Inactive__c = true AND Unit__c IN (select Unit__c from Lease_Partner__c where Lease__c IN : lstLeaseIds)]){
                    objOL.Status__c = 'Inactive';
                    objOL.IsRegistered__c = false;
                    lstOperatingLocations.add(objOL);
                    lstOLIds.add(objOL.Id);
                }
            }
            
            /*if(!lstFutureLeaseIds.isEmpty()){
list<Lease_Partner__c> lstLPs = [select Id,Name,Lease__c,Unit__c,Account__c,Type_of_Lease__c,Is_License_to_Occupy__c,Is_6_Series__c from Lease_Partner__c where Lease__c IN : lstFutureLeaseIds AND (Type_of_Lease__c = 'Leased' OR Is_License_to_Occupy__c = true)];
LeasePartnerTriggerHandler objLPHandler = new LeasePartnerTriggerHandler();
objLPHandler.CreateOperatingLocaitonsFromLease(lstLPs);
}*/
        }
        
        System.debug('ZZZ LeaseTH->lstOperatingLocations SIZE->'+lstOperatingLocations.size());
        
        if(!lstOperatingLocations.isEmpty()){
            update lstOperatingLocations;
        }
        
        //V1.2
        if(!reActivatedLeaseIdSet.isEmpty()){
            activateOperatingLocation(reActivatedLeaseIdSet);
        }
    }
    
    //***START V1.2 **/
    public static void activateOperatingLocation(Set<String> leadIdSet){
        Map<Id,Operating_Location__c> operatingLocToActivateMap = new Map<Id,Operating_Location__c>();
        for(Operating_Location__c opLoc : [SELECT Id,Name,Status__c,IsRegistered__c,Hosting_Company__c 
                                           FROM Operating_Location__c 
                                           WHERE Lease__c IN :leadIdSet 
                                           AND Status__c = 'Inactive'
                                           Order By CreatedDate Desc
                                           LIMIT 1
                                          ]){
                                                           
            opLoc.Status__c = 'Active';
            //opLoc.IsRegistered__c = true;
            operatingLocToActivateMap.put(opLoc.Id,opLoc);
                                                           
            
        }
        
        
        System.debug('ZZZ LeaseTH->operatingLocToActivateMap ->'+operatingLocToActivateMap);
        
        if(operatingLocToActivateMap.size() > 0){
            update operatingLocToActivateMap.values();
        }
    }
    
    //*** END  V1.2 **/
    
}