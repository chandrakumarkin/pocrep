/*
    Author      : Durga Prasad
    Date        : 14-Dec-2019
    Description : Custom code to check user has selected Activity "Objects which are exclusively charitable"
    ---------------------------------------------------------------------------------------------------------
*/
global without sharing class CC_HasOECActivity  implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'false';
        if(SR!=null && SR.Id!=null){
            for(License_Activity__c LA:[Select Id from License_Activity__c where Application__c=:SR.Id and Activity__r.Activity_Code__c='OEC']){
                strResult = 'true';
            }
        }else if(SR!=null && SR.HexaBPM__Parent_SR__c!=null){
            for(License_Activity__c LA:[Select Id from License_Activity__c where Application__c=:SR.HexaBPM__Parent_SR__c and Activity__r.Activity_Code__c='OEC']){
                strResult = 'true';
            }
        }
        return strResult;
    }
}