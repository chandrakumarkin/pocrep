/**
 * Description : Controller for OB_ManageUsersSrPage component
 *
 * ****************************************************************************************
 * History :
 * [5.NOV.2019] Prateek Kadkol - Code Creation
 * v2 [5.MAY.2020] Prateek Kadkol - new fields added to query
 * 
 */
public without sharing class OB_ManageUsersSrPageController {
    @AuraEnabled
    public static RespondWrap loadPageContent(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        
        for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
												    AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
			respWrap.ButtonSection = sectionObj;
		}
        
        for(User usr:[Select Id,Contact.AccountId from user where Id=:userinfo.getuserid() and ContactId!=null]){
            respWrap.loggedInUser = usr;
        }

        respWrap.dropdownList = new list<Relationship__c>();
        respWrap.relAmendment = new list<HexaBPM_Amendment__c>();
        for(Relationship__c relObj:[Select Id,Contact_Name__c,Individual_First_Name__c,	Individual_Last_Name__c from Relationship__c where Relationship_Type_formula__c='Authorized Signatory' AND Subject_Account__c= :respWrap.loggedInUser.Contact.AccountId 
        AND Active__c=TRUE]){
            respWrap.dropdownList.add(relObj);
        }
        /* for(HexaBPM_Amendment__c relObj:[Select Id,Individual_Corporate_Name__c from HexaBPM_Amendment__c where ServiceRequest__c =: reqWrap.srId and 
        Role__c INCLUDES ('Authorised Signatory')]){
            respWrap.relAmendment.add(relObj);
        } */
        //v2 
        for(HexaBPM__Service_Request__c srObj:[Select Id,Step_Notes__c,HexaBPM__Contact__r.Name,Community_User_Roles__c,
        (select Id,First_Name__c,Last_Name__c,Individual_Corporate_Name__c from Amendments__r WHERE  Role__c INCLUDES ('Authorised Signatory')) from HexaBPM__Service_Request__c where id =: reqWrap.srId  ]){
            if(srObj.Step_Notes__c == 'Promote'){
                if(srObj.Community_User_Roles__c != null && srObj.Community_User_Roles__c.indexOf('Super User')==-1){
                    srObj.Community_User_Roles__c += ';Super User';
                }else if(srObj.Community_User_Roles__c == null){
                    srObj.Community_User_Roles__c = 'Super User';
                }
            }
            respWrap.srObj = srObj;
            for(HexaBPM_Amendment__c amendObj : srObj.Amendments__r ){
                respWrap.relAmendment.add(amendObj);
            }
            
        }

        return respWrap;
    }

    @AuraEnabled
    public static RespondWrap delAmendment(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

       
		HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c(id = reqWrap.amendIdToDel);
		
        try{
        
         delete amedObj;
        
         
        }catch(DMLException e) {
        
           string DMLError = e.getdmlMessage(0) + '';
           if(DMLError == null) {
             DMLError = e.getMessage() + '';
            }
           respWrap.errorMessage = DMLError;
        }

       
        
        return respWrap;
    }

    @AuraEnabled
    public static RespondWrap createAmendment(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
        
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

        string recordId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();

		HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c();
		amedObj.ServiceRequest__c = reqWrap.srId;
        amedObj.RecordTypeId = recordId;
        amedObj.First_Name__c=reqWrap.selectedRel.Individual_First_Name__c;
        amedObj.Last_Name__c=reqWrap.selectedRel.Individual_Last_Name__c;
        amedObj.Role__c = 'Authorised Signatory';
        
        try{
        
         insert amedObj;
         respWrap.debugger = amedObj;
         
        }catch(DMLException e) {
        
           string DMLError = e.getdmlMessage(0) + '';
           if(DMLError == null) {
             DMLError = e.getMessage() + '';
            }
           respWrap.errorMessage = DMLError;
        }

       
        
        return respWrap;
    }
        
    // dynamic button section
	@AuraEnabled
	public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId, HexaBPM__Service_Request__c srObj) {


        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        srObj.Community_User_Roles__c = OB_AgentEntityUtils.compAndPropRoleCheck(srObj.Community_User_Roles__c);
        update srObj;

		HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
		PageFlowControllerHelper.objSR = objRequest;
		PageFlowControllerHelper objPB = new PageFlowControllerHelper();

		objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
		upsert objRequest;

		PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
		system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
		respWrap.pageActionName = responseNextPage.pg;
		respWrap.communityName = responseNextPage.communityName;
		respWrap.CommunityPageName = responseNextPage.CommunityPageName;
		respWrap.sitePageName = responseNextPage.sitePageName;
		respWrap.strBaseUrl = responseNextPage.strBaseUrl;
		respWrap.srId = objRequest.Id;
		respWrap.flowId = responseNextPage.flowId;
		respWrap.pageId = responseNextPage.pageId;
		respWrap.isPublicSite = responseNextPage.isPublicSite;

		system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
		return respWrap;
	}
	public class ButtonResponseWrapper {
		@AuraEnabled public String pageActionName { get; set; }
		@AuraEnabled public string communityName { get; set; }
		@AuraEnabled public String errorMessage { get; set; }
		@AuraEnabled public string CommunityPageName { get; set; }
		@AuraEnabled public string sitePageName { get; set; }
		@AuraEnabled public string strBaseUrl { get; set; }
		@AuraEnabled public string srId { get; set; }
		@AuraEnabled public string flowId { get; set; }
		@AuraEnabled public string pageId { get; set; }
		@AuraEnabled public boolean isPublicSite { get; set; }
		@AuraEnabled public boolean tester { get; set; }

	}
        
    // ------------ Wrapper List ----------- //
        
    public class RequestWrap{
    
        @AuraEnabled public Relationship__c selectedRel;
        @AuraEnabled public string srId;
        @AuraEnabled public string pageId;  
        @AuraEnabled public string amendIdToDel ;                     
    }
        
    public class RespondWrap{
    
        @AuraEnabled public list<Relationship__c> dropdownList;
        @AuraEnabled public list<HexaBPM_Amendment__c> relAmendment;
        @AuraEnabled public User loggedInUser;
        @AuraEnabled public string errorMessage;
        @AuraEnabled public HexaBPM__Service_Request__c srObj;
        @AuraEnabled public HexaBPM__Section_Detail__c ButtonSection ;
        @AuraEnabled public object debugger;
        
    }
}