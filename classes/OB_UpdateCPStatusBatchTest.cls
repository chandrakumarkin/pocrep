@isTest
public class OB_UpdateCPStatusBatchTest {
    @testSetup
    static  void createTestData() {
        Id accountCP_RTId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Commercial Permission').getRecordTypeId();

        Account acc  = new Account();
        acc.Is_Commercial_Permission__c = 'Yes';
        acc.name = 'test';   
        acc.OB_Type_of_commercial_permission__c = 'Gate Avenue Pocket Shops';
        acc.RecordTypeId=accountCP_RTId;
        acc.Active_Leases__c = 0;
       insert acc;
         
       Compliance__c comprec = new Compliance__c();
        comprec.Account__c = acc.id;
        comprec.End_Date__c = system.today();
        comprec.Status__c ='open';
        insert comprec;
         
       
       contact con = new Contact();
       con.LastName = 'test';
       con.FirstName = 'test';
       con.Email = 'test@test.com';
       con.AccountId = acc.id;
       insert con;  
        
       Commercial_Permission__c CP = new Commercial_Permission__c();
       CP.Account__c = acc.id;
       CP.End_Date__c = system.today().addMonths(12);
       CP.Status__c ='Active';
       insert CP;
       
       Lease__c lse = new Lease__c();
       lse.Name = '234';
       lse.Account__c = acc.id;
       lse.End_Date__c  = system.today();
       insert lse; 
    }

    static testMethod void executeBatchTest1() {
        test.startTest();
        Database.executeBatch(new OB_UpdateCPStatusBatch());
        String sch ='0 48 * * * ?'; 
        System.schedule('Schedule to update CP', sch,new OB_UpdateCPStatusBatch());
        
        test.stopTest();
        List<Commercial_Permission__c>  CPList = [Select id,Status__c from Commercial_Permission__c];
        system.debug('Test CPLIst 2=>'+CPList);
        system.assertEquals(CPList[0].Status__c,'Expired');
    }

    static testMethod void executeBatchTest2() {
        List<Account> accList = [Select id,Active_Leases__c from Account];
        List<Commercial_Permission__c> CPList = [Select id,Status__c from Commercial_Permission__c];
        
        test.startTest();
        accList[0].Active_Leases__c = 1;
        update accList;
        
        CPList[0].Status__c = 'Expired';
        update CPList;
        Database.executeBatch(new OB_UpdateCPStatusBatch());

        String sch ='0 48 * * * ?'; 
        System.schedule('Schedule to update CP', sch,new OB_UpdateCPStatusBatch());
        
        test.stopTest();
        CPList = [Select id,Status__c from Commercial_Permission__c];
        system.assertEquals(CPList[0].Status__c,'Active');
    }
}