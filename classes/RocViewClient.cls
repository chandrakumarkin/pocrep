public without sharing class RocViewClient {
  public Account ObjAccount {get;set;}
  public Contact ObjContact {get;set;}
  
  Id conid;  
  
  public string viewType {get;set;}
  
  public string mode {get;set;}
  
  public RocViewClient(){
    viewType = Apexpages.currentPage().getParameters().get('type');
    mode=Apexpages.currentPage().getParameters().get('mode');
    ObjAccount = new Account();
    ObjContact = new Contact();
    conid= Apexpages.currentPage().getParameters().get('id');
    
    if(viewType == 'Account'){
          String query = 'SELECT ';
          for(Schema.FieldSetMember f : SObjectType.Account.FieldSets.Community_Account_Info.getFields()) {
              query += f.getFieldPath() + ',';
          }
          query += 'Id FROM Account where id=\''+Apexpages.currentPage().getParameters().get('id')+'\'';
          for(Account obj : Database.query(query)){
            ObjAccount = obj;
          }
    }else if(viewType == 'Contact'){
      String query = 'SELECT ';
          if(mode!='GS'){
              for(Schema.FieldSetMember f : SObjectType.Contact.FieldSets.Contact_Info.getFields()) {
                  query += f.getFieldPath() + ',';
              }
          }
          else{
              for(Schema.FieldSetMember f : SObjectType.Contact.FieldSets.GS_Contact_Info.getFields()) {
                  query += f.getFieldPath() + ',';
              }
          }
          query += 'Id FROM Contact where id=\''+Apexpages.currentPage().getParameters().get('id')+'\'';
          for(Contact obj : Database.query(query)){
            ObjContact = obj;
          }
    }
    
    
  }
  
  public List<Document_Details__c> getDoclist(){
      List<Document_Details__c> Doclist=[select id, name,Document_Number__c,Document_Type__c,ISSUE_DATE__c,EXPIRY_DATE__c 
                                          from Document_Details__c where Contact__c=:conid and SAP_CHGTYP__c != 'D'];
      return Doclist;
  }
  
  public List<Relationship__c> getDeplist(){
      List<Relationship__c> Deplist=[select id,Object_Contact__r.name,Object_Contact__r.Passport_No__c,Object_Contact__r.Nationality__c, Relationship_Type_formula__c from Relationship__c where Active__c = true AND Subject_Contact__c=:conid];
      return Deplist;
      
  }
  
}