@isTest
public class CaseFeedbackControllerTest {
    static testMethod void CaseFeedbackTest() {
        PageReference caseFeedbackPage = Page.CaseFeedback;
        Test.setCurrentPage(caseFeedbackPage);
        
        Case caseObj = new Case(
            GN_Type_of_Feedback__c = 'Complaint',
            Type = 'Parking',
            Status = 'In Progress',
            Origin = 'Phone',
            Subject = 'Testing',
            Full_Name__c='TEST',
            GN_Proposed_Due_Date__c = System.today() + 5
        );
        
        insert caseObj;
        // Put Id into the current page Parameters
        ApexPages.currentPage().getParameters().put('id',caseObj.Id);
        
        //ApexPages.StandardController sc = new ApexPages.StandardController(caseObj);
        CaseFeedbackController cc = new CaseFeedbackController();
        cc.CaseId = caseObj.Id;
        cc.doSubmit();
    }
}