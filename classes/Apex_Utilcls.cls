/*
    Author      :   Durga Prasad
    Class Name  :   Apex_Utilcls
    Description :   Utility class which will helpful to reduce the code redundancy
*/
public without sharing class Apex_Utilcls {
    public static set<string> getAllsObjectFields(String strObjectName){
        return null;
    }
    public static string getAllFieldsSOQL(String strObjectName){
        string strSOQL = 'Select Id,Name';
        map<String,Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(strObjectName.toLowerCase()).getDescribe().fields.getMap();
        if(fieldMap != null){
            for(Schema.SObjectField f : fieldMap.values()){
                Schema.DescribeFieldResult fd = f.getDescribe();
                if(fd.getName().toLowerCase()!='id' && fd.getName().toLowerCase()!='name'){//fd.isCustom()
                    strSOQL = strSOQL+','+fd.getName();
                }
            }
        }
        strSOQL = strSOQL+' from '+strObjectName;
        system.debug('strSOQL===>'+strSOQL);
        return strSOQL;
    }
    public static string getCustomFieldSOQL(String strObjectName){
        string strSOQL = 'Select Id,Name';
        map<String,Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(strObjectName.toLowerCase()).getDescribe().fields.getMap();
        if(fieldMap != null){
            for(Schema.SObjectField f : fieldMap.values()){
                Schema.DescribeFieldResult fd = f.getDescribe();
                if(fd.isCustom()){
                    strSOQL = strSOQL+','+fd.getName();
                }
            }
        }
        strSOQL = strSOQL+' from '+strObjectName;
        system.debug('strSOQL===>'+strSOQL);
        return strSOQL;
    }
    
    public static string getRandNo(integer len){
        String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        
        while (randStr.length() < len) {
             Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
             randStr += chars.substring(idx, idx+1);
        }
       
        return randStr;
    }
    
    public static string Amend_SOQL_withShareholders(String strObjectName){
        string strSHStatus = 'Draft';
        string strSOQL = 'Select Id,Related__r.Issuing_Authority_Other__c,Related__r.IssuingAuthority__c,Related__r.Amendment_Type__c,Related__r.Record_Type_Name__c,Related__r.Shareholder_Company__c,Related__r.Company_Name__c,Related__r.CL_Certificate_No__c,Contact__r.BP_No__c,Non_Reg_Company__r.Name,(select id,Account_Share__c,Status__c,Relationship__c,Amendment__c,Sys_Proposed_No_of_Shares__c,No_of_Shares__c from Shareholder_Details__r),Name'; // Added fields in query for RORP Freehold 
        map<String,Schema.SObjectField> fieldMap = Schema.getGlobalDescribe().get(strObjectName.toLowerCase()).getDescribe().fields.getMap();
        if(fieldMap != null){
            for(Schema.SObjectField f : fieldMap.values()){
                Schema.DescribeFieldResult fd = f.getDescribe();
                if(fd.getName().toLowerCase()!='id' && fd.getName().toLowerCase()!='name'){//fd.isCustom()
                    strSOQL = strSOQL+','+fd.getName();
                }
            }
        }
        strSOQL = strSOQL+' from '+strObjectName;
        system.debug('strSOQL===>'+strSOQL);
        return strSOQL;
    }
}