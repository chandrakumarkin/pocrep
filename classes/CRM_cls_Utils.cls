/******************************************************************************************
 *  Name        : CRM_cls_Utils 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2017-01-29
 *  Description : Utility Library for CRM
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   29-1-2016   Claude        Created 
 V1.1	20-08-2017	 Claude		   Added logic to copy all attachments from the record
*******************************************************************************************/
public without sharing class CRM_cls_Utils {
	
	/**
	 * Default transaction result
	 */
	public static final String SUCCESS_STATUS_CODE = 'Success';
    
    /**
	 * Holds any error message
	 */
	private String errorMessage;
	
	/**
	 * Stores the type of log to be used by the
	 * class
	 */
	private String logType;
	
	/* Class construcor */
	public CRM_cls_Utils(){
		init();
	}
	
	/**
	 * Executes starting methods
	 * and initializes class
	 * members
	 */
	private void init(){
		
		errorMessage = '';
		logType = '';
		
	}
	
	/**
	 * V1.1	
	 * Returns a list of attachments
	 * according to their parent 
	 * @params		relatedToId		The parent ID
	 * @return		attachments		The list of associated attachments
	 */
	public static List<Attachment> getAttachmentsByRecord(String relatedToId){
		
		list<Attachment> attachments = new list<Attachment>();
		
		for(Attachment a : [SELECT Id, Name, Body, ContentType FROM Attachment WHERE ParentId = :relatedToId]){
			
			attachments.add(
						
				new Attachment(
					ContentType = a.ContentType,
					Name = a.Name,
					Body = a.Body 
				)
			);
			
		}
		
		return attachments;
		
	}
	
	/**
     * Sets the error message of the transaction
     * @params			classLog		The type of log to be sent
     */
    public void setLogType(String classLog){
    	logType = classLog;
    }
	
	/**
     * Sets the error message of the transaction
     * @params			errMsg		The error message
     */
    public void setErrorMessage(String errMsg){
    	errorMessage = errMsg;
    }
    
    /**
     * Returns the error message
     */
    public String getErrorMessage(){
    	return errorMessage;
    }
    
    /**
     * Checks if an error had occurred
     * @params			hasError	Checks if there is an error
     */
    public Boolean hasError(){
    	return String.isNotBlank(errorMessage);
    } 
    
    /**
     * Createa an error log
     */
    public void logError(){
    	
      	insert LogDetails.CreateLog(null, logType, errorMessage);
    }
    
}