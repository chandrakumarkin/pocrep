/**
*Author : Merul Shah
*Description : Utility for locking and unlocking SR..

[6.MAY.2020] Prateek Kadkol - Edited to incorporate multistructure page 
**/
public without sharing class OB_LockAndUnlockSRHelper{

    public static map<string,boolean> GetPageLockDetails(string FlowId,string SRID){
        map<string,boolean> MapPageLockDetails = new map<string,boolean>();
        system.debug('FlowId===>'+FlowId);
        system.debug('SRID===>'+SRID);
        
        if(SRID!=null && FlowId!=null){
            list<string> PageNames = new list<string>();
            boolean isDraft = false;
            boolean isClosed = false;
            for(HexaBPM__Service_Request__c objSR:[Select Id,HexaBPM__IsClosedStatus__c,HexaBPM__IsCancelled__c,HexaBPM__Is_Rejected__c,HexaBPM__Internal_Status_Name__c,(SELECT Id,HexaBPM__SR_Step__r.Pages_to_Unlock__c from HexaBPM__Steps_SR__r where HexaBPM__Status_Type__c != 'End' AND Step_Template_Code__c = 'AWAITING_CHANGES' AND Is_Client_Step__c = true limit 1) from HexaBPM__Service_Request__c where Id=:SRID]){
                system.debug('objSR===>'+objSR);
                if(objSR.HexaBPM__Internal_Status_Name__c=='Draft'){
                    isDraft = true;
                }else if(objSR.HexaBPM__IsClosedStatus__c || objSR.HexaBPM__IsCancelled__c || objSR.HexaBPM__Is_Rejected__c){
                    isClosed = true;
                }else{
                    for(HexaBPM__Step__c stp:objSR.HexaBPM__Steps_SR__r){
                        system.debug('Pages===>'+stp.HexaBPM__SR_Step__r.Pages_to_Unlock__c);
                        if(stp.HexaBPM__SR_Step__r.Pages_to_Unlock__c!=null){
                             PageNames = stp.HexaBPM__SR_Step__r.Pages_to_Unlock__c.split(';');
                         }
                    }
                }
            }
            system.debug('PageNames===>'+PageNames);
            if(!isDraft && !isClosed){
                map<string,string> MapPagesToUnlock = new map<string,string>();
                for(string strPageName:PageNames){
                    MapPagesToUnlock.put(strPageName.tolowercase(),strPageName.tolowercase());
                }
                for(HexaBPM__Page__c pg:[Select Id,Name,HexaBPM__Is_Custom_Component__c,Community_Page__c from HexaBPM__Page__c where HexaBPM__Page_Flow__c=:FlowId]){
                    if(pg.Community_Page__c != 'ob-reviewandconfirm' && pg.Community_Page__c != 'ob-multistructure-reviewandconfirm'){
                        if(PageNames.size()>0){
                            if(pg.HexaBPM__Is_Custom_Component__c){
                                if(MapPagesToUnlock.get(pg.Community_Page__c.tolowercase())!=null)
                                    MapPageLockDetails.put(pg.Id,false);
                                else
                                    MapPageLockDetails.put(pg.Id,true);
                            }else{
                                if(MapPagesToUnlock.get(pg.Name.tolowercase())!=null)
                                    MapPageLockDetails.put(pg.Id,false);
                                else
                                    MapPageLockDetails.put(pg.Id,true);
                            }
                        }else{
                            MapPageLockDetails.put(pg.Id,true);
                        }
                    }else{
                        MapPageLockDetails.put(pg.Id,false);
                    }
                }
            }else if(isDraft){
                for(HexaBPM__Page__c pg:[Select Id,Name,HexaBPM__Is_Custom_Component__c,Community_Page__c from HexaBPM__Page__c where HexaBPM__Page_Flow__c=:FlowId]){
                    MapPageLockDetails.put(pg.Id,false);
                }
            }else{
                for(HexaBPM__Page__c pg:[Select Id,Name,HexaBPM__Is_Custom_Component__c,Community_Page__c from HexaBPM__Page__c where HexaBPM__Page_Flow__c=:FlowId]){
                    if(pg.Community_Page__c != 'ob-reviewandconfirm' && pg.Community_Page__c != 'ob-multistructure-reviewandconfirm')
                        MapPageLockDetails.put(pg.Id,true);
                    else
                        MapPageLockDetails.put(pg.Id,false);
                }
            }
        }
        return MapPageLockDetails;
    }
    
    public static ResponseWrapper lockAndUnlockSRById(OB_LockAndUnlockSRHelper.RequestWrapper reqWrapPram){
             system.debug('########### entered Lok inner  ' );
             system.debug('########### entered Lok inner  '+reqWrapPram);
             //reqest wrpper
             OB_LockAndUnlockSRHelper.RequestWrapper reqWrap = reqWrapPram;
            //response wrpper
             OB_LockAndUnlockSRHelper.ResponseWrapper respWrap = new OB_LockAndUnlockSRHelper.ResponseWrapper();
         
            String serObjId = reqWrap.srId; // param
            String currentCommunityPageId = reqWrap.currentCommunityPageId; //param
            String flowId =reqWrap.flowId; // param
            String communityReviewPageName = reqWrap.communityReviewPageName; //param
            
            
            system.debug('########### communityReviewPageName '+communityReviewPageName );
            String communityReviewPageURL;
            String currentPageUniqueName;
         
            Boolean isDraft = false;
            HexaBPM__Service_Request__c serObj = new HexaBPM__Service_Request__c();
            for(HexaBPM__Service_Request__c serObjTemp :[SELECT id, 
                                                                 HexaBPM__Internal_SR_Status__c, 
                                                                 HexaBPM__External_SR_Status__c, 
                                                                 HexaBPM__Internal_Status_Name__c, 
                                                                 HexaBPM__Record_Type_Name__c,	
                                                                 HexaBPM__EXternal_Status_Name__c
                                                                 FROM HexaBPM__Service_Request__c
                                                         WHERE id = :serObjId
                ]) 
                
            {
                serObj = serObjTemp;
            }
            system.debug('########### serObj ' );
            system.debug('########### serObj '+serObj );
            // if the SR status is draft the unlock the page.
            if((String.IsBlank(serObj.HexaBPM__Internal_Status_Name__c) || (String.IsNotBlank(serObj.HexaBPM__Internal_Status_Name__c)
               && serObj.HexaBPM__Internal_Status_Name__c.equalsIgnorecase('Draft')))
               || (String.IsBlank(serObj.HexaBPM__External_SR_Status__c) || (String.IsNotBlank(serObj.HexaBPM__External_SR_Status__c)
               && serObj.HexaBPM__EXternal_Status_Name__c.equalsIgnorecase('Draft')))
               || serObj.HexaBPM__Record_Type_Name__c == 'Superuser_Authorization') 
            {
                system.debug('########### drafyt chec ' );
                // draft service request
                isDraft = true;
                respWrap.isSRLocked = false;
                return respWrap;
            }
            
            // Quering Page.
            system.debug('########### page goinf ' );
            //Map for holding current page attribute.
            for( HexaBPM__Page__c page : [SELECT Id,Name,
                                                  HexaBPM__What_Id__c,
                                                  HexaBPM__Page_Flow__c,
                                                  HexaBPM__Is_Custom_Component__c,
                                                  Community_Page__c
                                           FROM HexaBPM__Page__c 
                                          WHERE HexaBPM__Page_Flow__c =:flowId 
                                          ])
            {
               
                // selecting curent comminity page attribute.
                /*
                   if custom page then label,
                   if standard the page.name
                */
                
                if( !String.isBlank( currentCommunityPageId ) && page.id == currentCommunityPageId )
                {
                    currentPageUniqueName = ( page.HexaBPM__Is_Custom_Component__c ? page.Community_Page__c  : page.Name  );
                    
                }
                
                //system.debug('@@@@@@@ page.Community_Page__c '+page.Community_Page__c);
                // for review page.
                if( communityReviewPageName != NULL 
                                && page.Community_Page__c == communityReviewPageName )
                {
                    communityReviewPageURL = '/'+system.Label.OB_Site_Prefix+'/s/'+page.Community_Page__c+'?srId='+serObjId+'&flowId='+flowId+'&pageId='+page.Id;
                }
               
               // system.debug('########### currentPageUniqueName ' +currentPageUniqueName);
               // system.debug('########### communityReviewPageURL ' +communityReviewPageURL);
                
            }
            
            if( String.isBlank( communityReviewPageURL  ) )
            {
                 communityReviewPageURL = '/'+system.Label.OB_Site_Prefix+'/s/';
                
            }
            system.debug('########### page config  ' );
            system.debug('########### currentPageUniqueName  '+currentPageUniqueName );
            system.debug('########### communityReviewPageURL '+communityReviewPageURL );
             
            // Query Steps.... there will be always 1 step with this condition.
            List<HexaBPM__Step__c> steplst = [
                                              SELECT id,
                                                     HexaBPM__SR__c,
                                                     HexaBPM__SR_Step__r.Pages_to_Unlock__c
                                                FROM HexaBPM__Step__c
                                               WHERE HexaBPM__SR__c =:serObj.Id
                                                 AND HexaBPM__Status_Type__c != 'End'
                                                 AND Step_Template_Code__c = 'AWAITING_CHANGES'
                                                 AND Is_Client_Step__c = true
                                                 
                                             ];
             
            if( steplst != NULL && steplst.size() > 0 ){
                HexaBPM__Step__c stepObj = steplst[0]; 
                
                // if stepObj.Pages_to_Unlock__c is null the lock it and review screen.
                if(stepObj.HexaBPM__SR_Step__r.Pages_to_Unlock__c == NULL || String.isBlank(stepObj.HexaBPM__SR_Step__r.Pages_to_Unlock__c)){
                    respWrap.isSRLocked = true;
                    // form review page link.
                    respWrap.communityReviewPageURL  = communityReviewPageURL ;
                    return respWrap;
                }
                
                // spliting configured pages.
                Set<String> unlockPageSet = new Set<String>(stepObj.HexaBPM__SR_Step__r.Pages_to_Unlock__c.split(';')); 
                system.debug('#################### unlockPageSet');
                 system.debug('#################### unlockPageSet'+unlockPageSet);
                 system.debug('#################### currentPageUniqueName '+currentPageUniqueName);
                if(unlockPageSet != NULL && currentPageUniqueName != NULL && unlockPageSet.contains(currentPageUniqueName)){
                     respWrap.isSRLocked = false;
                     return respWrap;
                }
                                             
            }
        
            // if nothing is founf the lock it.
            respWrap.isSRLocked = true;
            // form review page link.
            respWrap.communityReviewPageURL  = communityReviewPageURL ;
            
            //Querying  Page
            return respWrap;
           
    }
    public class RequestWrapper{
        public string communityName{get;set;}
        public string currentCommunityPageId {get;set;}
        public string communityReviewPageName {get;set;}
        public string sitePageName{get;set;}
        public string strBaseUrl{get;set;}
        public string srId{get;set;}
        public string flowId{get;set;}
        public string pageId{get;set;} 
        public RequestWrapper(){}
    }
    public class ResponseWrapper{
        public boolean isSRLocked{get;set;} 
        public string communityReviewPageURL{get;set;}
        public ResponseWrapper(){}
    }
}