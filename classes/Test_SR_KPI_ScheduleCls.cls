/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_SR_KPI_ScheduleCls{

    static testMethod void myUnitTest() {
        
    test.startTest();
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Test Account';
        insert objAccount2;
    
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@dmcc.com';
        insert objContact;
        
        Contact objContact2 = new Contact();
        objContact2.LastName = 'Test Contact2';
        objContact2.FirstName = 'Test Contact2';
        objContact2.AccountId = objAccount.Id;
        objContact2.Email = 'test@dmcc.com';
        insert objContact2;
    
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        String objRectype;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration') AND SobjectType='Service_Request__c']){
            objRectype = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
        //objSR.Record_Type_Name__c= 'Application_of_Registration';
        objSR.legal_structures__c = 'RLLP';
        objSR.Foreign_Registration_Name__c = 'Test FR Name';
        objSR.Foreign_Registration_Date__c = system.today();
        objSR.Foreign_Registration_Place__c = 'United Arab Emirates';
        objSR.Foreign_Registered_Building__c = 'Building1';
        objSR.Foreign_Registered_Street__c = 'Street1';
        objSR.Foreign_Registered_PO_Box__c = '13124';
        //objSR.Foreign_Registered_Phone__c = '+971 526241919';
        objSR.Foreign_Registered_Post__c = '124214';
        objSR.Foreign_registered_office_no__c = '124124';
        objSR.Foreign_Registered_Level__c = '12';
        objSR.Foreign_Registered_Country__c = 'United Arab Emirates';
        objSR.SR_Template__c = objTemplate.Id;
        objSR.Submitted_DateTime__c = system.today().addDays(-2);
        objSR.Entity_Name__c='Test';
        objSR.Proposed_Trading_Name_1__c='Test';
        objSR.Incorporated_Organization_Name__c='Test';
        objSR.Nature_of_business__c='Test';
        objSR.Nature_of_Business_Arabic__c='Test';
        objSR.Name_identical_to_another_company__c=true;
        objSR.Business_Name__c='Test';
        objSR.Identical_Business_Domicile__c='Test';
        objSR.Pro_Entity_Name_Arabic__c='Test';
        objSR.Pro_Trade_Name_1_in_Arab__c ='Test';
        objSR.Legal_Structures__c='RLLP';
        objSR.Registration_Type__c='Test';
        objSR.AOA_Format__c='Test';
        objSR.Standard_Charter_Format__c='Test';
        objSR.Current_Registered_Name__c='Test';
        objSR.Domicile_list__c='Test';
        objSR.Current_Registered_Office_No__c='Test';
        objSR.Current_Registered_Building__c='Test';
        objSR.Current_Registered_Level__c='Test';
        objSR.Current_Registered_Street__c='Test';
        objSR.Current_Registered_PO_Box__c='Test';
        objSR.Current_Address_Country__c='Test';
        objSR.Current_Registered_Postcode__c='Test';
        //objSR.Current_Registered_Phone__c='Test';
        objSR.Principal_Business_Activity__c='Test';
        objSR.Reasons_to_transfer_Foreign_Entity__c='Test';
        objSR.DFSA_Approval__c=true;
        objSR.DFSA_In_principle_Approval_Date__c=system.today();
        objSR.Completed_Date_Time__c = system.now();
        objSR.SR_Group__c = 'RORP';
        insert objSR;
        
        
        
        /*Set<String> QueueSet = new Set<String>{'Client Entry User','RoC Officer','Line Manager','Registrar','Data Protection Commissioner','Security'};
        list<QueueSobject> lstQueue  = new list<QueueSobject>();
        for(string qname :QueueSet){
        QueueSobject objQ = new QueueSobject();
        objQ.SobjectType = 'Step__c';
        //objQ.Queue.Name = 'Client Entry User';
        lstQueue.add(objQ);
        }
        insert lstQueue;
        */
        
        list<Step__c> lstStp = new list<Step__c>();
        Step_Template__c stpTemp = new Step_Template__c();
        stpTemp.Code__c = 'END_TASK';
        stpTemp.Name = 'End Task';
        stpTemp.Step_RecordType_API_Name__c = 'End_Task';
        insert stpTemp;
        
        Step__c objStp = new Step__c();
        objStp.SR__c = objSR.Id;
        objStp.Step_Template__c = stpTemp.id;
        objStp.SR_Group__c = 'RORP';
        lstStp.add(objStp);
        Integer i = 1;
        for(QueueSobject clientQ :[Select Id,QueueId,Queue.Name from QueueSobject where sObjectType = 'Step__c']){
                Step__c objStpKPI = new Step__c();
                objStpKPI.SR__c = objSR.Id;
                objStpKPI.OwnerId = clientQ.QueueId;
                objStpKPI.Closed_Date_Time__c = system.today().addDays(i);
                objStpKPI.SR_Group__c = 'RORP';
                lstStp.add(objStpKPI);
                i+=1;
             
        }
        
        insert lstStp;
        
        
        
       //  SR_KPI_ScheduleCls.schUpdate();
        String sch = '20 30 8 10 2 ?';
        
        System.schedule('KPI Scheduler',sch,new SR_KPI_ScheduleCls());
        
        Test.stopTest();
        }
 }