public without sharing class GS_Step_ProcessBuilder_Actions {
    @InvocableMethod(label='GS Step Process Builder' description='GS Step Process Builder initaiation') 
    public static void GsStepProcessbuilderActions(List<Id> stepIds){
  		for(Step__c objStp : [select id,name,mobile_No__c,status__c,step_name__c,SR__c from Step__c where id in : stepIds]){
  			GS_NewHelperClass.processActions(objStp);
  		}
    }
}