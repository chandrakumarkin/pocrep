/*********************************************************************************************************************
    Author      :   Durga Prasad
    Company     :   NSI
    Class Name  :   Cls_Review_and_Final_Validations
    Description :   This class will executes all the Amendment related validation when users finalises the SR from Review & Finalise Screen.

 --------------------------------------------------------------------------------------------------------------------------
Modification History
 --------------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By      Description
--------------------------------------------------------------------------------------------------------------------------             
V1.1    20-01-2015  Saima Hidayat   Check for Company Secretary & Designated Member (atleast one)    
V1.2    22-02-2015  Saima Hidayat   Validations for RelType==UBO modified for Amendment and AoR
V1.3    02-03-2015  Saima Hidayat   LTD IC issued shares can be of value $1.
V1.4    10-05-2016  Sravan          Account of type Finalnce-Related can have Issued Share Capital less than 50000$ as per TKT# 2708
V1.5    04-08-2016  Sravan          commented  validation condition error that ' Please add at least two Authorized Signatory to proceed." in the AoR as per TKT # 3197
V1.6    17-04-2017  Claude          Moved condition for LTD PCC (1091)
v1.7    13-02-2018  Danish          Create a field on account ' Min capital requirements' to allows submission if min requirement of 50,000 is not required  tkt # 4804  
v1.8    29-10-2018  selva        #5942 members not added and validation not firing
V1.9    11-11-2018  Arun            #5858 New company law   \
v2.0    23-03-2019  arun            Added ubo validation and removed old val
V2.1    04-09-2019  Arun            Disabled for LTD IC companies #7309
V2.2    04-09-2019  Arun            Disabled for LTD PCC companies #7341
v2.3	04-03-2021	Shikha			#7857 Add Validation to select one auditor based on ticket
**********************************************************************************************************************/

public without sharing class Cls_Review_and_Final_Validations {
    
    
    public static string Valid_Amendments_Foundation(String relType, String legalType ,Service_Request__c SR,list<Amendment__c> lstSRAmends){
        
         String msg = 'Success!';
          map<string,list<Amendment__c>> mapAmnd = new map<string,list<Amendment__c>>();
         for(Amendment__c amnd:lstSRAmends){
            list<Amendment__c> lstAmnd = new list<Amendment__c>();
            if(mapAmnd.get(amnd.Relationship_Type__c)!=null)
                lstAmnd = mapAmnd.get(amnd.Relationship_Type__c);
            lstAmnd.add(amnd);
            system.debug('amnd.Relationship_Type__c is : '+amnd.Relationship_Type__c);
            mapAmnd.put(amnd.Relationship_Type__c,lstAmnd);
        }
        
         
         if(relType == 'Council Member'){
            
            if(!mapAmnd.containsKey(relType) ||( mapAmnd.containsKey(relType) && mapAmnd.get(relType).size() < 2)){
              
                msg = 'A minimum of 2 '+relType+'s must be listed as per the DIFC Foundations Law';
                return msg;
            } 
         }
         
          if(relType == 'Guardian' && SR.Confirm_Change_of_Trading_Name__c == 'Yes'){
            if(!mapAmnd.containsKey(relType)){ msg = 'Please add at least one '+relType+' to proceed.';               return msg;
            }
         }
         
         if(relType == 'Founder'){
            
            if(!mapAmnd.containsKey(relType)){ msg = 'Please add at least one '+relType+' to proceed.';              return msg;         } 
         }
         
         if(relType == 'Registered Agent' && SR.Confirm_Change_of_Entity_Name__c== 'Yes'){
            
            if(!mapAmnd.containsKey(relType)){  msg = 'Please add at least one '+relType+' to proceed.';              return msg;
            } 
         }
         return msg;
    }
    public static string Valid_Amendments(String relType,String legalType,Service_Request__c SR,list<Amendment__c> lstSRAmends)
    {
    
  

    
        map<string,list<Amendment__c>> mapAmnd = new map<string,list<Amendment__c>>();
        
        set<string> setAmndTypes = new set<string>{'Individual','Body Corporate'};
        
        for(Amendment__c amnd:lstSRAmends){
            list<Amendment__c> lstAmnd = new list<Amendment__c>();
            if(mapAmnd.get(amnd.Relationship_Type__c)!=null)
                lstAmnd = mapAmnd.get(amnd.Relationship_Type__c);
            lstAmnd.add(amnd);
            system.debug('amnd.Relationship_Type__c is : '+amnd.Relationship_Type__c);
            mapAmnd.put(amnd.Relationship_Type__c,lstAmnd);
        }
        //System.debug('***&&&&&&'+lstSRAmends);
        string SMRel = '';
        if(relType=='Shareholder')
            SMRel = 'Share Capital';
        else
            SMRel = 'Membership Interest';
            
        Service_Request__c objSRFlow = new Service_Request__c();
        
        objSRFlow = SR;
        
         
 //V2.0 
 if(relType == 'UBO' && (objSRFlow.record_type_name__c == 'Application_of_Registration' || objSRFlow.record_type_name__c == 'Change_Add_or_Remove_Beneficial_or_Ultimate_Owner'))
    {
          List<UBO_Data__c> ListUBO=[select id from UBO_Data__c where Status__c!='Removed' and Service_Request__c=:SR.id];//V2.0 
            if(ListUBO.size()>0)
            return 'Success!';
            else
                return 'Please add at least one Beneficial/Ultimate Owner to proceed!';
    }
    
        
        String msg = 'Failure!';
        System.debug(legalType+'***&&&&&&');
        System.debug(relType+'***&&&&&&');
        System.debug(objSRFlow.record_type_name__c+'***&&&&&&');
        try{
          if(relType == 'Authorized Signatory' && legalType!='LTD SPC'){
            list<Amendment__c> amndAuth = new list<Amendment__c>();
            if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                amndAuth = mapAmnd.get(relType);
            //= [select id,Representation_Authority__c from Amendment__c where ServiceRequest__c=:SRid and Relationship_Type__c=:relType];
          
          /* Commented for  V1.5
            if(amndAuth.size()==1 && amndAuth[0].Representation_Authority__c=='Jointly'){
                msg = 'Please add atleast two '+relType+' to proceed.';
                return msg;
            }            
            */
            
            // V1.5 else if((amndAuth.size()==0) &&(objSRFlow.record_type_name__c =='Application_of_Registration'||objSRFlow.record_type_name__c=='Change_Add_or_Remove_Authorized_Signatory')){
            if((amndAuth.size()==0) &&(objSRFlow.record_type_name__c =='Application_of_Registration'||objSRFlow.record_type_name__c=='Change_Add_or_Remove_Authorized_Signatory')){ 
                msg = 'Please add at least one '+relType+' to proceed.';
                return msg;
            }
          }
          if(relType != 'UBO'&& relType!='Services Personnel')
          {
            /* if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                msg = 'Success!';
                */
                
        
             if(msg != 'Success!')
             {
                 if((relType=='Director') && (legalType=='LTD'||legalType=='LTD SPC'||legalType=='LTD National'||legalType=='LTD Supra National') && (objSRFlow.record_type_name__c =='Change_Details_Add_or_Remove_Director' || objSRFlow.record_type_name__c =='Application_of_Registration')){ // V1.6 - Claude - Moved LTD PCC to second condition
                     list<Amendment__c> amndDir = new list<Amendment__c>();
                     if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                        amndDir = mapAmnd.get(relType);
                     //V2.3 New company law 
                         if(amndDir.size()<2 & legalType!='LTD'){ msg = 'Please add at least two Directors to proceed.'; return msg;}
                         else if(amndDir.size()<1 & legalType=='LTD'){msg = 'Please add at least One Director to proceed.'; return msg;}                   
                     
                  }
                  
                  //Sai
                  else if(relType=='Council Member' && legalType=='Foundation' && objSRFlow.record_type_name__c =='Change_Details_Add_or_Remove_Council_Member'){ 
                     list<Amendment__c> amndDir = new list<Amendment__c>();
                     if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                        amndDir = mapAmnd.get(relType);
                     //V2.3 New company law 
                         if(amndDir.size()<2)
                         { 
                         	msg = 'Please add at least two Council Members to proceed.'; 
                         	return msg;
                         }
                  }
                  
                  else if(relType=='Founder' && legalType=='Foundation' && objSRFlow.record_type_name__c =='Change_Details_Add_or_Remove_Founder'){ 
                     list<Amendment__c> amndDir = new list<Amendment__c>();
                     if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                        amndDir = mapAmnd.get(relType);
                     //V2.3 New company law 
                         if(amndDir.size()<1)
                         { 
                         	msg = 'Please add at least one Founder to proceed.'; 
                         	return msg;
                         }
                  }
                  
                  //Sai
                  if((relType=='Director') && (legalType=='LTD'||legalType=='LTD SPC'||legalType=='LTD National'||legalType=='LTD Supra National') && (objSRFlow.record_type_name__c =='Change_Details_Add_or_Remove_Director' || objSRFlow.record_type_name__c =='Application_of_Registration')){ // V1.6 - Claude - Moved LTD PCC to second condition
                     list<Amendment__c> amndDir = new list<Amendment__c>();
                     if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                        amndDir = mapAmnd.get(relType);
                     //V2.3 New company law 
                         if(amndDir.size()<2 & legalType!='LTD'){ msg = 'Please add at least two Directors to proceed.'; return msg;}
                         else if(amndDir.size()<1 & legalType=='LTD'){msg = 'Please add at least One Director to proceed.'; return msg;}                   
                     
                  }
                  
                  else if((relType=='Director')&&(legalType=='LTD IC'||legalType=='FRC'||legalType=='LTD PCC') && (objSRFlow.record_type_name__c =='Change_Details_Add_or_Remove_Director' || objSRFlow.record_type_name__c =='Application_of_Registration')){
                    list<Amendment__c> amndDir = new list<Amendment__c>();
                    if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                        amndDir = mapAmnd.get(relType);
                        if(amndDir.size()==0){
                         System.debug('=====>>amndDir'+amndDir.size());
                         msg = 'Please add at least one '+relType+' to proceed.';                   return msg;
                        }
                  }else if((relType=='Auditor')&&(objSRFlow.record_type_name__c =='Add_or_Remove_Auditor')){//v2.3 started
                    list<Amendment__c> amndDir = new list<Amendment__c>();                      
                      if(GlobalConstants.LEGAL_ENTITY_AUDITOR_VAL.contains(objSRFlow.Customer__r.Legal_Type_of_Entity__c) || 
                       (objSRFlow.Customer__r.Legal_Type_of_Entity__c == 'LTD' && objSRFlow.Customer__r.Annual_Turnover_less_than_USD_5_million__c == 'No')){                           
                           if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0){
                               amndDir = mapAmnd.get(relType);
                               if(amndDir.size()==0){                         			
                         			msg = 'Please add at least one '+relType+' to proceed.';
                         			return msg;
                        		}
                           }
                           else if(mapAmnd.get(relType) == null){
                               msg = 'Please add at least one '+relType+' to proceed.';
                         	   return msg;
                           }
                       }
                      //v2.3 End
                    /*if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                        amndDir = mapAmnd.get(relType);
                        if(amndDir.size()==0){
                         //System.debug('=====>>amndDir'+amndDir.size());
                         msg = 'Please add at least one '+relType+' to proceed.';
                         return msg;
                        }*/
                  }else if((relType=='Authorized Representative')&&(objSRFlow.Sector_Classification__c=='Family Office')&&(objSRFlow.record_type_name__c =='Application_of_Registration'||objSRFlow.record_type_name__c =='Change_Add_or_Remove_an_Authorized_Representative')){
                    list<Amendment__c> amndDir = new list<Amendment__c>();
                    if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                        amndDir = mapAmnd.get(relType);
                        if(amndDir.size()==0){msg = 'Please add at least one '+relType+' to proceed.';    return msg;}
                  }else if(relType=='Designated Member' && (legalType=='RLLP'||legalType=='LLP') && (objSRFlow.record_type_name__c=='Application_of_Registration'||objSRFlow.record_type_name__c=='Change_Details_Add_or_Remove_Designated_Member')){
                        list<Amendment__c> amndAud = new list<Amendment__c>();
                        if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                            amndAud = mapAmnd.get(relType);
                            if(amndAud.size()==0){  
                                //msg = 'Success!';
                                msg = 'Please add at least one '+relType+' to proceed!';
                                return msg;
                            }
                 }else if((relType=='Manager') && (objSRFlow.record_type_name__c == 'Application_of_Registration'||objSRFlow.record_type_name__c=='Change_Details_Add_or_Remove_Manager')&&(legalType=='LLC')){// && objSRFlow.record_type_name__c == 'Add_or_Remove_Auditor'
                        list<Amendment__c> amndAud = new list<Amendment__c>();
                        if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                            amndAud = mapAmnd.get(relType);
                            if(amndAud.size()==0){  
                                //msg = 'Success!';
                                msg = 'Please add at least one '+relType+' to proceed!';
                                return msg;
                            }
                 }else if(relType=='Founding Member'&&legalType=='NPIO' && (objSRFlow.record_type_name__c =='Application_of_Registration' || objSRFlow.record_type_name__c =='Change_Details_Add_or_Remove_Founding_Member')){
                     list<Amendment__c> amndDir = new list<Amendment__c>();
                     if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                        amndDir = mapAmnd.get(relType);
                     if(amndDir.size()<3){
                         msg = 'Please add at least three '+relType+' to proceed.';
                         return msg;
                     }else{
                         msg = 'Success!';
                         return msg;
                     }
                  }
                  if(relType=='Shareholder' && legalType =='LTD SPC' && (objSRFlow.record_type_name__c == 'Application_of_Registration')){
                        //1.7
                        //Danish
                      if(!objSRFlow.Customer__r.Min_capital_requirements__c && objSRFlow.issued_share_capital_in_usd__c < 100){
                       
                            msg = 'The Issued Share Capital must not be less than $100!';
                            return msg;
                      }else{
                         msg = 'Success!';
                         return msg;
                      }
                  }else if(legalType=='GP' && relType=='General Partner' && (objSRFlow.record_type_name__c == 'Application_of_Registration' || objSRFlow.record_type_name__c == 'Change_Details_Add_or_Remove_Partner')){
                        list<Amendment__c> amndPP = new list<Amendment__c>();
                        if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                            amndPP = mapAmnd.get(relType);
                        if(amndPP.size()<2){
                            msg = 'Please add at least two Partners to proceed!';
                            return msg;
                        }else{
                             msg = 'Success!';
                             return msg;
                        }
                  }else if((legalType=='LP'||legalType=='RLP') && relType=='Limited Partner' && (objSRFlow.record_type_name__c == 'Application_of_Registration' || objSRFlow.record_type_name__c == 'Change_Details_Add_or_Remove_Partner')){
                        list<Amendment__c> amndPP = [select id from Amendment__c where ServiceRequest__c=:SR.Id and Relationship_Type__c LIKE '%Partner%'];
                        if(amndPP.size()<2){
                            msg = 'Please add at least two Partners to proceed!';
                            return msg;
                        }else{
                             msg = 'Success!';
                             return msg;
                        }
                  }else if((objSRFlow.record_type_name__c == 'Application_of_Registration') && relType=='Shareholder' && (legalType =='Public Company'|| legalType =='LTD PCC') && objSRflow.Company_Type_list__c !='Financial - related'){    // V1.4 Added condition to skip this check if Company Type is Financial - related 
                        System.debug('=========>>>'+objSRFlow.issued_share_capital_in_usd__c);
                        //1.7
                        //Danish
                        if(legalType=='Public Company')
                        {
                             if(!objSRFlow.customer__r.Min_capital_requirements__c && integer.valueof(objSRFlow.issued_share_capital_in_usd__c) < 100000)
                            {msg = 'The Issued '+SMRel+' must not be less than $100,000!' ;   return msg; }
                            else
                            {msg = 'Success!';return msg;}
                            
                            
                        }
                        else
                        {
                             if(!objSRFlow.customer__r.Min_capital_requirements__c && integer.valueof(objSRFlow.issued_share_capital_in_usd__c) < 50000)
                            {msg = 'The Issued '+SMRel+' must not be less than $50,000!' ; return msg;}
                            else
                            {msg = 'Success!';return msg;
                            }
                            
                        }
                       
                        
                  }else if((objSRFlow.record_type_name__c == 'Application_of_Registration') && relType=='Shareholder' && (legalType =='LTD IC'))
                  {
                        System.debug('=========>>>'+objSRFlow.issued_share_capital_in_usd__c);
                        
                        if(integer.valueof(objSRFlow.issued_share_capital_in_usd__c) < 1){
                       
                            msg = 'The Issued '+SMRel+' must not be less than $1!';
                            return msg;
                        }else{
                             msg = 'Success!';
                             return msg;
                        }
                  }
                  
                  /*
                  V1.8
                  else if((objSRFlow.record_type_name__c == 'Application_of_Registration')&& relType=='Member' && legalType=='LLC' && objSRflow.Company_Type_list__c !='Financial - related'){     // V1.4 Added condition to skip this check if Company Type is Financial - related // Commented for testing
                        System.debug('=========>>>'+objSRFlow.issued_share_capital_in_usd__c);
                          //1.7
                        //Danish
                        if(!objSRFlow.customer__r.Min_capital_requirements__c && integer.valueof(objSRFlow.issued_share_capital_in_usd__c) < 50000){
                        
                            msg = 'The Issued '+SMRel+' must not be less than $50,000!';
                            return msg;
                        }
                        //v1.8 added
                        else if(objSRFlow.customer__r.Min_capital_requirements__c && integer.valueof(objSRFlow.issued_share_capital_in_usd__c)==0){
                        
                            msg = 'Please add atleast one member to proceed.-validation added';
                            return msg;
                        }else{
                             msg = 'Success!';
                             return msg;
                        }
                  }*/
            }
            /*else if(relType=='Company Secretary' && legalType=='FRC'&& msg != 'Success!' && (objSRFlow.record_type_name__c == 'Application_of_Registration'||objSRFlow.record_type_name__c == 'Change_Details_Add_or_Remove_Company_Secretary')){
                msg = 'Success!';
                return msg;
            }*/
            /* V1.1 starts */
            
            //V1.9 disable for LTD 
            //V2.1 Disable for LTD IC
            //2.2 disabled for LTD PCC
            if(relType=='Company Secretary' && (legalType!='LTD' && legalType!='FRC'&&legalType!='LTD IC' && legalType!='LTD PCC' &&legalType!='LLP'&&legalType!='RLLP'&&legalType!='RP'&&legalType!='GP'&&legalType!='LP'&&legalType!='RLP') && msg != 'Success!' && (objSRFlow.record_type_name__c == 'Application_of_Registration'||objSRFlow.record_type_name__c == 'Change_Details_Add_or_Remove_Company_Secretary'))
            {
               list<Amendment__c> amndAud = new list<Amendment__c>();
               if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                    amndAud = mapAmnd.get(relType);
                    if(amndAud.size()==0){  
                        msg = 'Please add at least one '+relType+' to proceed!';
                        return msg;
                    }/* V1.1 Ends */
            }else{
                msg = 'Success!';
                //msg = 'Please add atleast one '+relType+' to proceed!';
                return msg;
            }
          }
          //2.0 disable UBO 
          /*else if(relType=='UBO' && relType!='Services Personnel'){
             list<Amendment__c> amndUBO = new list<Amendment__c>();
             if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0)
                amndUBO = mapAmnd.get(relType);
             
           // if(amndUBO.size()>0)
              //  msg = 'Success!';
             //
             if(msg!= 'Success!' && false){ 
               list<Amendment__c> amndDir  = new list<Amendment__c>();
               list<Relationship__c> RelDir = new list<Relationship__c>();
               if((legalType=='LTD'||legalType=='LTD SPC'||legalType=='LTD PCC'||legalType=='LTD IC')){
                   if(objSRFlow.record_type_name__c == 'Application_of_Registration'){
                       if(mapAmnd.containsKey('Shareholder')){
                           for(Amendment__c objMemBC:mapAmnd.get('Shareholder')){
                                if(objMemBC.Amendment_Type__c=='Body Corporate')
                                    amndDir.add(objMemBC);
                           }
                       }
                   }else if(objSRFlow.record_type_name__c == 'Change_Add_or_Remove_Beneficial_or_Ultimate_Owner'){
                        RelDir = [select id,Relationship_Type_formula__c from Relationship__c where subject_Account__c =:objSRFlow.Customer__c and Relationship_Type_formula__c='Shareholder' and Object_Account__c!=null and Active__c=true];
                   }
                   //[select id from Amendment__c where ServiceRequest__c=:SRid and Relationship_Type__c='Shareholder'and Amendment_Type__c='Body Corporate'];
                   if((amndDir.size()>0||RelDir.size()>0) && amndUBO.size()==0){
                     msg = 'Please add at least one Beneficial/Ultimate Owner to proceed!';
                     return msg;
                   }else{
                         msg = 'Success!';
                         return msg;
                   }
                }else if(legalType=='LLC'){
                   if(objSRFlow.record_type_name__c == 'Application_of_Registration'){
                       if(mapAmnd.containsKey('Member')){
                           for(Amendment__c objMemBC:mapAmnd.get('Member')){
                                if(objMemBC.Amendment_Type__c=='Body Corporate')
                                    amndDir.add(objMemBC);
                           }
                       }
                   }else if(objSRFlow.record_type_name__c == 'Change_Add_or_Remove_Beneficial_or_Ultimate_Owner'){                RelDir = [select id,Relationship_Type_formula__c from Relationship__c where subject_Account__c =:objSRFlow.Customer__c and Relationship_Type_formula__c='LLC Member' and Object_Account__c!=null and Active__c=true];
                   }
                   //[select id from Amendment__c where ServiceRequest__c=:SRid and Relationship_Type__c='Member'and Amendment_Type__c='Body Corporate'];
                   if((amndDir.size()>0||RelDir.size()>0) && amndUBO.size()==0){
                     msg = 'Please add at least one Beneficial/Ultimate Owner to proceed!';return msg;
                   }else{
                         msg = 'Success!';
                         return msg;
                   }
                }else if(legalType=='NPIO'){
                   if(objSRFlow.record_type_name__c == 'Application_of_Registration'){
                       if(mapAmnd.containsKey('Member')){
                           for(Amendment__c objMemBC:mapAmnd.get('Founding Member')){if(objMemBC.Amendment_Type__c=='Body Corporate')     amndDir.add(objMemBC);
                           }
                       }
                   }else if(objSRFlow.record_type_name__c == 'Change_Add_or_Remove_Beneficial_or_Ultimate_Owner'){ RelDir = [select id,Relationship_Type_formula__c from Relationship__c where subject_Account__c =:objSRFlow.Customer__c and Relationship_Type_formula__c='Founding Member' and Object_Account__c!=null and Active__c=true];  }
                   //[select id from Amendment__c where ServiceRequest__c=:SRid and Relationship_Type__c='Member'and Amendment_Type__c='Body Corporate'];
                   if((amndDir.size()>0||RelDir.size()>0) && amndUBO.size()==0){
                     msg = 'Please add at least one Beneficial/Ultimate Owner to proceed!';
                     return msg;
                   }else{
                         msg = 'Success!';
                         return msg;
                   }
                }else if(legalType=='GP'||legalType=='LP'||legalType=='RP'||legalType=='LLP'||legalType=='RLP'){
                   if(objSRFlow.record_type_name__c == 'Application_of_Registration'){
                        amndDir = [select id from Amendment__c where ServiceRequest__c=:SR.Id and Relationship_Type__c LIKE '%Partner%' and Amendment_Type__c='Body Corporate'];
                   }else if(objSRFlow.record_type_name__c == 'Change_Add_or_Remove_Beneficial_or_Ultimate_Owner'){
                        RelDir = [select id,Relationship_Type_formula__c from Relationship__c where subject_Account__c =:objSRFlow.Customer__c and Relationship_Type_formula__c LIKE '%Partner%' and Object_Account__c!=null and Active__c=true];
                   }
                   if((amndDir.size()>0||RelDir.size()>0) && amndUBO.size()==0){  msg = 'Please add at least one Ultimate Beneficial Owner to proceed!';  return msg;
                   }else{
                         msg = 'Success!';
                         return msg;
                   }
                }else if(legalType=='FRC' && (objSRFlow.record_type_name__c == 'Application_of_Registration' || objSRFlow.record_type_name__c == 'Change_Add_or_Remove_Beneficial_or_Ultimate_Owner')){
                   if(amndUBO.size()==0){
                     msg = 'Please add at least one Beneficial/Ultimate Owner to proceed!';
                     return msg;
                   }else{
                         msg = 'Success!';
                         return msg;
                   }
                }
              }else{                  msg = 'Success!';             return msg;
              }
          }
          */
          
          else if((legalType=='FRC'|| legalType=='RLLP' || legalType=='RP' || legalType=='RLP' ) && relType=='Services Personnel' && (objSRFlow.record_type_name__c == 'Application_of_Registration' || objSRFlow.record_type_name__c == 'Notice_of_Appointment_or_Cessation')){
              if(mapAmnd.get(relType)!=null && mapAmnd.get(relType).size()>0){
                msg = 'Success!';
              }else{
                msg = 'Please appoint at least one person to accept services.';
                return msg;
              }
              //System.debug('%%%%%%%'+msg);
         }else{
                msg = 'Success!';
        }
        }catch(Exception e){
                system.debug('Exceotion is : $$ line #'+e.getLineNumber()+' '+e.getMessage());
                return string.valueof(e);
        }
        msg = 'Success!';
        return msg;
    }
}