@isTest
public class ServiceRequestsAddressChangeBatch_Test {
    
    static testmethod void createUsersAccountContactData()
    {
        Test.startTest();
        List<UserRole> r = [select id,Name from UserRole where DeveloperName='DIFC_IT_Manager'];
        Id roleId = r[0].Id;
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = '31st December';
        //objAccount.Active_License__c = objLic.id;
        insert objAccount;
        
        License__c objLic = new License__c();
        objLic.License_Issue_Date__c = system.today();
        objLic.License_Expiry_Date__c = system.today().addDays(365);
        objLic.Status__c = 'Active';
        objLic.Account__c = objAccount.id;
        insert objLic;
        
        objAccount.Active_License__c = objLic.id;
        update objAccount;
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactId=objContact.Id, Community_User_Role__c='Company Services',
                                TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        //System.runAs(objUser) {
            Account a = new Account();
            
            a.Name = 'testScheduledApexFromTestMethod';
            a.ROC_Status__c= 'Active';
            a.Total_Meeting_Count__c=0;
            a.Total_Interaction_Count__c=0;
            a.License_Activity_Details__c ='text';
            a.Hosting_Company__c=null;
            
            insert a;
            
            Account a1 = new Account();
            
            a1.Name = 'testScheduledApexFromTestMethod';
            a1.ROC_Status__c= 'Active';
            a1.Total_Meeting_Count__c=0;
            a1.Total_Interaction_Count__c=0;
            a1.License_Activity_Details__c ='text';
            a1.Same_Management__c = a.id;
            a1.Hosting_Company__c=a.id;
            
            insert a1;
            
            contact con = new contact();
            con.accountId= a.id;
            con.firstName = 'test';
            con.LastName='faruk';
            
            insert con;
            
            contact con1 = new contact();
            con1.accountId= a1.id;
            con1.firstName = 'test';
            con1.LastName='faruk';
            con1.RecordTypeId = portalUserId;
            con1.Email = 'test@difcportal.com';
            insert con1;
            
            License_Activity_Master__c objLAM = new License_Activity_Master__c();
            objLAM.Name = 'Test DIFC Activity';
            objLAM.Sector_Classification__c = 'Authorised Market Institution';
            objLAM.Type__c = 'License Activity';
            objLAM.Sys_Code__c = '048';
            objLAM.Enabled__c = true;
            insert objLAM;
            
            License_Activity__c objLA = new License_Activity__c();
            objLA.Account__c = a.Id;
            objLA.Activity__c = objLAM.Id;
            objLA.Sector_Classification__c = 'Authorised Market Institution';        
            insert objLA;
            
            Relationship__c rel = new Relationship__c();
            rel.Relationship_Type__c ='Has Affiliates with Same Management';
            rel.Subject_Account__c= a.id;
            rel.Object_Account__c = a1.id;
            //rel.Object_Contact__c= con1.id;
            rel.Active__c = true;
            insert rel;
            
            List<Task> tskLIst = new List<Task>();
            
            Task t = new Task();
            t.OwnerId = UserInfo.getUserId();
            t.Subject='Donni';
            t.Status='Not Started';
            t.Priority='Normal';
            t.Whatid=a.id;
            t.Date_Time_From__c =system.now(); 
            t.Date_Time_To__c = system.now();
            t.recordTypeID = Schema.sObjectType.task.getRecordTypeInfosByName().get('Calls').getRecordTypeId();
            tskLIst.add(t);
            
            Task t1 = new Task();
            t1.OwnerId = UserInfo.getUserId();
            t1.Subject='Donni';
            t1.Status='Not Started';
            t1.Priority='Normal';
            t1.Whatid=a.id;
            t1.Date_Time_From__c =system.now(); 
            t1.Date_Time_To__c = system.now();
            t1.Description='Additional To: bassam.hassan@alsaconsumergroup.comCC: fitouts.DIFC@idama.aeAttachment: Subject: RE: Contractor access.    [ ref:_00D20nkZ5._5002017zI3K:ref ]Body:Dear BassamRequest you to kindly contact the fitouts team, copied to this mail for assistance on below.Thank youDIFC portal team--------------- Original Message ---------------From: BASSAM HASSAN [bassam.hassan@alsaconsumergroup.com]Sent: 05/11/2016 11:20To: portal@difc.aeSubject: Contractor access.Good Morning, Kindly note that we are  facing a problems in creating a contractor accessto the portal system,SO please your urgent actions will be required. Thanks.  Best Regards, Bassam HassanFacility Management & Quality Control Dept.  Alsa Consumer GroupP.O. Box 55106, Dubai, UAET: +971 4 2666262  <http://www.alsaconsumergroup.com/> www.alsaconsumergroup.com Alsa Consumer Group Logo This communication contains information which is proprietary to the DIFC, is (and is intended to remain) confidential, being provided for the exclusive use of the intended recipient, and may be legally privileged.  If you have reason to believe  you are not the intended recipient(s), disclosing, copying, disseminating or otherwise taking any action in connection with this communication or the information in it is prohibited and may be unlawful. If you have reason to believe you have received this communication in error, please notify the DIFC, comply with the foregoing warning and delete this communication from your system.Please consider the environment before printing this email.ref:_00D20nkZ5._5002017zI3K:ref';
            t1.recordTypeID = Schema.sObjectType.task.getRecordTypeInfosByName().get('Meeting').getRecordTypeId();
            
            tskLIst.add(t1);
            
            insert tskLIst;
            
            List<Event> eLst = new List<Event>();
            Event e =  new Event();
            e.Whatid=a.id;
            e.whoid=con.id;
            e.ActivityDate=date.today();
            e.ActivityDateTime = system.now();
            e.Date_Time_From__c=system.now();
            e.Date_Time_To__c=system.now();
            e.subject='Event';
            e.OwnerId = UserInfo.getUserId();
            e.DurationInMinutes=30;
            eLst.add(e);
            
            Event e1 =  new Event();
            e1.Whatid=a.id;
            e1.whoid=con.id;
            e1.ActivityDate=date.today();
            e1.ActivityDateTime = system.now();
            e1.Date_Time_From__c=system.now();
            e1.Date_Time_To__c=system.now();
            e1.subject='Event';
            e1.DurationInMinutes=30;
            e1.OwnerId = UserInfo.getUserId();
            eLst.add(e1);
            
            insert eLst;  
            
            createSRStatus();
            
            map<string,string> mapRecordTypeIds = new map<string,string>();
            for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA','Change_of_Address_Details')]){
                mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
            }
       
            
            Service_Request__c objSR = new Service_Request__c();
            objSR.Customer__c = a.Id;
            objSR.Date_of_Declaration__c = Date.today();
            objSR.Email__c = 'test@test.com';
            objSR.RecordTypeId = mapRecordTypeIds.get('Change_of_Address_Details');
            //objSR.Building__c = objBuilding.Id;
            //objSR.Unit__c = lstUnits[0].Id;
            objSR.Service_Category__c = 'New';
            objSR.Type_of_Lease__c = 'Lease';
            objSR.Rental_Amount__c = 1234;
            objSR.Financial_Year_End_mm_dd__c ='31st December';
            insert objSR;
            system.debug(objSR.Id);     
            
            Service_Request__c request = [SELECT Id, Name, RecordTypeId,External_Status_Name__c,Customer__c,Customer__r.Hosting_Company__c FROM Service_Request__c 
                                          where Recordtype.DeveloperName = 'Change_of_Address_Details'and Customer__r.Hosting_Company__c = null and createddate  = today];
            
            system.debug(request);             
             
            Attachment ObjAttachment = new Attachment();
            ObjAttachment.name ='test';
            ObjAttachment.Body = Blob.valueOf('test body');
            ObjAttachment.ParentId = objSR.id;
            insert ObjAttachment;
            
            SR_Template__c objTemplate = new SR_Template__c();
            objTemplate.Name = 'Company conversion Certificate for creation of Commercial License for Hosting co';
            objTemplate.SR_RecordType_API_Name__c = 'Company_conversion_Certificate_for_creation_of_Commercial_License_for_Hosting_co';
            objTemplate.ORA_Process_Identification__c = 'Company_conversion_Certificate_for_creation_of_Commercial_License_for_Hosting_co';
            objTemplate.Active__c = true;
            insert objTemplate;
            Document_Master__c objDocMaster = new Document_Master__c();
            objDocMaster.Name = 'Board Resolution';
            objDocMaster.Code__c = 'Board Resolution';
            insert objDocMaster;
            
            SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
            objTempDocs.SR_Template__c = objTemplate.Id;
            objTempDocs.Document_Master__c = objDocMaster.Id;
            objTempDocs.On_Submit__c = true;        
            objTempDocs.Generate_Document__c = true;
            insert objTempDocs;
            
            database.executeBatch(new ServiceRequestsAddressChangeBatch(), 1);
            
       
        //}
        
        Test.stopTest();
    }
    
    public static void createSRStatus(){
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c objSRStatus;
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Closed';
        objSRStatus.Code__c = 'CLOSED';
        objSRStatus.Type__c = 'End';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Rejected';
        objSRStatus.Code__c = 'REJECTED';
        objSRStatus.Type__c = 'Rejected';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Awaiting Approval';
        objSRStatus.Code__c = 'AWAITING_APPROVAL';
        objSRStatus.Type__c = 'Start';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Approved';
        objSRStatus.Code__c = 'APPROVED';
        objSRStatus.Type__c = 'Intermediate';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Submitted';
        objSRStatus.Code__c = 'SUBMITTED';
        lstSRStatus.add(objSRStatus);
        
        objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'DRAFT';
        lstSRStatus.add(objSRStatus);    
        
        if(lstSRStatus.size()>0)
            insert lstSRStatus;
    }
    
}