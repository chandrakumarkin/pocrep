/*
    Created By  : Ravi on 19th Oct, 2014
    Description : This class will invoke when user Issue the fine or objection the fine 
*/
// V1.0     24th May 2016       Swati       Fine Process, Apply discount method
// V1.1     2nd  June 2016       Swati      Fine Process, Reverse Fine method
// V1.2     October 15 2016     Sravan      Added to decrypt SAP Password
/**************************************************************************************************************************************************
**/
global without sharing class FineSRDetailsCls {
    
    webservice static string IssueFine(string ComplianceId){
        try{
            string RecordTypeId;
            Service_Request__c objSR = new Service_Request__c();
            
            for(RecordType objRT : [select Id,Name from RecordType where DeveloperName='Issue_Fine' AND SobjectType='Service_Request__c' AND IsActive=true])
                RecordTypeId = objRT.Id;
            
            for(Compliance__c objCompliance : [select Id,Account__c,Account__r.E_mail__c,Name,Principal_User__c,Principal_User__r.Email from Compliance__c where Id=:ComplianceId]){
                objSR.Customer__c = objCompliance.Account__c;
                objSR.Compliance__c = objCompliance.Id;
                objSR.RecordTypeId = RecordTypeId;
                objSR.Email__c = objCompliance.Principal_User__r.Email;//Account__r.E_mail__c;
            }
            insert objSR;
            
            Compliance__c objComp = new Compliance__c(Id=ComplianceId);
            objComp.Is_Fine_Issued__c = true;
            objComp.Fine_Issued_Date__c = system.today();
            update objComp;
            return objSR.Id;
        }catch(Exception ex){
            system.debug('Exception is : '+ex.getMessage());
            return 'Fail'+ex.getMessage();
        }
        return 'Success';
    }
    
    public static string FineObjectionInfo(Step__c objStep){
        PushObjectionSRtoSAP(new list<string>{objStep.SR__c});
        return 'Success';
    }
    
    @future(callout=true)
    public static void PushObjectionSRtoSAP(list<string> lstSRIDs){
        
        String Month = string.valueOf(system.today().month()).length()==1?'0'+system.today().month():''+system.today().month();
        String Day = string.valueOf(system.today().day()).length()==1?'0'+system.today().day():''+system.today().day();
        String Hours = string.valueOf(system.now().hour()).length()==1?'0'+system.now().hour():''+system.now().hour();
        String Minitues = string.valueOf(system.now().minute()).length()==1?'0'+system.now().minute():''+system.now().minute();
        String Seconds = string.valueOf(system.now().second()).length()==1?'0'+system.now().second():''+system.now().second();
        
        set<string> lstCustomerIds = new set<string>();
        
        list<SAPECCWebService.ZSF_S_RECE_SERV> items = new list<SAPECCWebService.ZSF_S_RECE_SERV>();
        map<string,string> mapSRCustomerIds = new map<string,string>();
        
        SAPECCWebService.ZSF_S_RECE_SERV item;
        try{
            for(SR_Price_Item__c objSRItem : [select Id,Name,Price__c,Transaction_Date__c,Company_Code__c,ServiceRequest__c,ServiceRequest__r.Record_Type_Name__c,ServiceRequest__r.Customer__c,ServiceRequest__r.Customer__r.BP_No__c,
                            Pricing_Line__c,Pricing_Line__r.Material_Code__c from SR_Price_Item__c where ServiceRequest__c IN : lstSRIDs]){
                item = new SAPECCWebService.ZSF_S_RECE_SERV();
                lstCustomerIds.add(objSRItem.ServiceRequest__r.Customer__c);
                    
                item.WSTYP = 'RFND';
                item.RENUM = objSRItem.Name;//string.valueOf(objSRItem.Id).substring(0,15);
                item.SGUID = string.valueOf(objSRItem.Id);
                item.BUKRS = objSRItem.Company_Code__c;
                item.KUNNR = objSRItem.ServiceRequest__r.Customer__r.BP_No__c;
                item.TDATE = system.today().year()+'-'+Month+'-'+Day;//'2014-09-16';
                item.GSPSN = '00';
                item.WAERS = 'AED';
                item.WRBTR = (objSRItem.Price__c != null)?string.valueOf(objSRItem.Price__c):'0';
                    
                item.MATNR = objSRItem.Pricing_Line__r.Material_Code__c;
                item.POSNR = '000010'; // Item # of SD Document, Always - 000010  
                item.MEINS = 'EA'; // Base unit of measure, always - EA
                item.KWMENG = '1.000'; //// Order Quantity, always 1
                //item.RUSER = '90029627'; // SAP Username
                item.RUSER = WebService_Details__c.getAll().get('Credentials').SAP_User_Id__c;
                item.RTIME = Hours+':'+Minitues+':'+Seconds;//'10:10:10';
                items.add(item);
                mapSRCustomerIds.put(objSRItem.Id,objSRItem.ServiceRequest__r.Customer__c);
            }
            
            SAPECCWebService.ZSF_TT_RECE_SERV objZSF_TT_RECE_SERV = new SAPECCWebService.ZSF_TT_RECE_SERV();
            objZSF_TT_RECE_SERV.item = items;
            
            SAPECCWebService.rece_serv objSFData = new SAPECCWebService.rece_serv();        
            objSFData.timeout_x = 100000;
            objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
            objSFData.inputHttpHeaders_x= new Map<String, String>();
            //Blob headerValue = Blob.valueOf('informatica:1234567');
            string decryptedSAPPassWrd  =  test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456789'; // Added for V1.2
            Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+decryptedSAPPassWrd);  // changed for V1.2 
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
            
            SAPECCWebService.Z_SF_RECEIPT_SERVICEResponse_element response = objSFData.Z_SF_RECEIPT_SERVICE(objZSF_TT_RECE_SERV);
            if(response != null){
                ServiceResponseResult objServiceResponseResult = ProcessSerrviceResponse(response.EX_SF_RS,mapSRCustomerIds);
                //list<SR_Price_Item__c> lstSRPriceItems = ProcessSerrviceResponse(response.EX_SF_RS,mapSRCustomerIds);
                list<SR_Price_Item__c> lstSRPriceItems = new list<SR_Price_Item__c>();
                if(objServiceResponseResult != null){
                    list<string> lstIds= new list<string>();
                    lstIds.addAll(lstCustomerIds);
                    //if(isFormSubmit == true){
                        //if(AccountBalanceInfoCls.getAccountBalance(lstIds, '5000') != 'Success'){
                        if(AccountBalanceInfoCls.getAccountBalance(lstIds, system.label.CompanyCode5000) != 'Success'){
                            //update objServiceResponseResult.PriceItems;
                        }
                        if(objServiceResponseResult.PriceItems != null)
                            update objServiceResponseResult.PriceItems;
                        if(objServiceResponseResult.Logs != null)
                            insert objServiceResponseResult.Logs;
                    /*}else{
                        if(objServiceResponseResult.PriceItems != null)
                            update objServiceResponseResult.PriceItems;
                    }*/
                }               
            }
        }catch(Exception ex){
            system.debug('Exception is : '+ex.getMessage());
            Log__c objLog = new Log__c();
            objLog.Description__c = ex.getMessage();
            objLog.Type__c = 'Webservice for Service Objection SR ';
            insert objLog;
        }
    }
    
    public static ServiceResponseResult ProcessSerrviceResponse(SAPECCWebService.ZSF_TT_RECE_SERV_OP Servicenfo,map<string,string> mapSRCustomerIds){
        try{
            if(Servicenfo != null && Servicenfo.item != null){
                list<SR_Price_Item__c> lstSRPriceItems = new list<SR_Price_Item__c>();
                list<Log__c> lstLogs = new list<Log__c>();
                SR_Price_Item__c objSRPriceItem;
                for(SAPECCWebService.ZSF_S_RECE_SERV_OP objPriceItemRes : Servicenfo.item){
                    if(objPriceItemRes.SGUID != null && objPriceItemRes.SGUID != ''){
                        //Update SR Price Items or Service Request
                        objSRPriceItem = new SR_Price_Item__c(Id=objPriceItemRes.SGUID);
                        if(objPriceItemRes.SFMSG=='Records are Inserted Successfully in the custom table'){
                            objSRPriceItem.Transaction_Id__c = objPriceItemRes.BELNR;
                            objSRPriceItem.Transaction_Number__c = objPriceItemRes.UNQNO;
                            objSRPriceItem.Status__c = 'Invoiced';
                        }else{
                            Log__c objLog = new Log__c();
                            objLog.Account__c = mapSRCustomerIds.get(objPriceItemRes.SGUID);
                            objLog.Type__c = 'Refund SR Price Item Service Process';
                            objLog.Description__c = objPriceItemRes.SFMSG;
                            objLog.SR_Price_Item__c = objSRPriceItem.Id;
                            lstLogs.add(objLog);
                        }
                        objSRPriceItem.Error_Status__c = objPriceItemRes.SFMSG;
                        lstSRPriceItems.add(objSRPriceItem);
                    }
                }
                ServiceResponseResult objServiceResponseResult = new ServiceResponseResult();
                objServiceResponseResult.PriceItems = lstSRPriceItems;
                objServiceResponseResult.Logs = lstLogs;
                return objServiceResponseResult;
            }
        }catch(Exception ex){
            system.debug('Exception is : '+ex.getMessage());
            Log__c objLog = new Log__c();
            objLog.Description__c = ex.getMessage();
            objLog.Type__c = 'Webservice Processing for Service Response';
            // insert objLog;
            return null;
        }
        return null;
    }
    
    public Class ServiceResponseResult {
        public list<SR_Price_Item__c> PriceItems {get;set;}
        public list<Log__c> Logs {get;set;}
    }   
    
    //V1.0 Used for Custom button to apply discount
    webservice static string applyDiscount(string SRId){
        try{
            string result = 'Success';
            List<service_request__c> tempList = [select id, refund_amount__c, (select id, Price__c from SR_Price_Items1__r) from service_request__c where id=:SRId];
            
            if(tempList!=null && !tempList.isEmpty()){
                if(tempList[0].SR_Price_Items1__r!=null && tempList[0].SR_Price_Items1__r.size()>0){
                    integer TotalPriceItem = tempList[0].SR_Price_Items1__r.size();
                    list<SR_Price_Item__c> tempPriceList = new list<SR_Price_Item__c>();
                    decimal newAmount = tempList[0].refund_amount__c/TotalPriceItem;
                    for(SR_Price_Item__c temp : tempList[0].SR_Price_Items1__r){
                        temp.Price__c = temp.Price__c-newAmount;  
                        tempPriceList.add(temp);
                    }   
                    if(tempPriceList!=null && !tempPriceList.isEmpty()){
                        update tempPriceList;
                        result = 'Success';
                    }   
                }
                else{
                    result = 'Price item does not exist.';
                }
                return result;
            }
        }catch(Exception e){
             insert LogDetails.CreateLog(null, 'cls_automatedFineProcess/initializeObjectionSR', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
        }
        return null;
    }
    
    //V1.1 Used for Custom button to reverse fine
    webservice static string reverseFine(string SRId){
        try{
            string result = 'Success';
            list<service_request__c> tempListOfSR = [select id,customer__c,type_of_request__c from service_request__c where id =:SRId and record_type_name__c=:'Issue_Fine'];   
            system.debug('---tempListOfSR--'+tempListOfSR);
            if(tempListOfSR!=null && !tempListOfSR.isEmpty()){
                List<SR_Status__c> ReversedStatus = [select id,name from SR_Status__c where Name='Reversed' limit 1];   
                tempListOfSR[0].Internal_SR_Status__c = ReversedStatus[0].id;
                tempListOfSR[0].External_SR_Status__c = ReversedStatus[0].id;
                update tempListOfSR;
                
                list<SR_Price_Item__c> tempPriceItemList = [select id, ServiceRequest__c, Status__c from SR_Price_Item__c where ServiceRequest__c=:tempListOfSR[0].id];
                if(tempPriceItemList!=null && !tempPriceItemList.isEmpty()){
                    for(SR_Price_Item__c tempObj : tempPriceItemList){
                        tempObj.Status__c = 'Reversed'; 
                    }
                    update  tempPriceItemList;
                }
                
                list<id> accountIdlist = new list<id>();
                map<string,string> mapUserEmails = new map<string,string>();
                system.debug('---tempListOfSR[0].customer__c---'+tempListOfSR[0].customer__c);
                if(tempListOfSR[0].customer__c!=null){
                    accountIdlist.add(tempListOfSR[0].customer__c); 
                }     
                system.debug('---accountIdlist---'+accountIdlist);  
                if(accountIdlist!=null && !accountIdlist.isEmpty()){
                    Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
                    for(User objUser : [select Id,Email,ContactId from User where IsActive=true AND Contact.AccountId IN : accountIdlist AND Community_User_Role__c INCLUDES ('Company Services') AND contact.recordTypeId=:portalUserId]){
                        system.debug('---objUser---'+objUser);  
                        mapUserEmails.put(objUser.ContactId,objUser.Email);
                    }
                }
                system.debug('---mapUserEmails---'+mapUserEmails);
                if(mapUserEmails!=null && !mapUserEmails.isEmpty()){
                    OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address = 'portal@difc.ae'];
                    string strTemplateId;
                    
                    if(tempListOfSR[0].type_of_request__c != 'Notification Renewal'){
                        for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='Fine_Reversed_Client'])
                            strTemplateId = objET.Id;
                    }
                    else{
                        for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='Fine_Reversed_DP_Client'])
                            strTemplateId = objET.Id;
                    }
                        
                    list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
                    
                    List<String> ccTo = new List<String>();
                    ccTo.add('nadine.chaar@difc.ae');
                    
                    for(string UserId : mapUserEmails.keySet()){
                        mail = new Messaging.SingleEmailMessage();
                        mail.setReplyTo('portal@difc.ae');
                        if(strTemplateId!=null && strTemplateId!=''){
                            mail.setWhatid(tempListOfSR[0].id);
                            mail.setTemplateId(strTemplateId);  
                            mail.setSaveAsActivity(false);
                            mail.setCcAddresses(ccTo);
                            if(lstOrgEA!=null && lstOrgEA.size()>0)
                                mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
                            mail.setTargetObjectId(UserId);
                            mails.add(mail);
                        }  
                    } 
                    
                    system.debug('---mails---'+mails);
                    if(mails != null && !mails.isEmpty()){
                        if(!test.isRunningTest()){
                            Messaging.sendEmail(mails);
                        }
                    }
                }
            }
            return result;
        }catch(Exception e){
             insert LogDetails.CreateLog(null, 'cls_automatedFineProcess/initializeObjectionSR', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
        }
        return null;
    }
}