/*
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    //V1.1    22-Feb-2021 Shoaib  
 
 */
global class closeCollectStepsBatch implements Database.Batchable<sObject>,Schedulable{
    
   global void execute(SchedulableContext ctx) {
        database.executeBatch(new closeCollectStepsBatch(), 1);
   }
    
   global List<Step__c> start(Database.BatchableContext bc) {
       
       Set<Id> stepIds = new Set<Id>();
       
       for(Step__c thisStep : [SELECT Id,Parent_Step__c,
                                     Parent_Step__r.Step_Name__c 
                                   FROM Step__c 
                                   WHERE Status_Code__c = 'DELIVERED'
                                   AND Step_Name__c = 'Courier Delivery'
                                   AND Parent_Step__r.Step_Name__c ='Collected' 
                                   AND Parent_Step__r.Status_Code__c != 'CLOSED'
                                   
                                   ]){
          stepIds.add(thisStep.Parent_Step__c);
           
       }
       
       return[SELECT ID,Status__c,Step_Name__c
                  FROM Step__c 
                  WHERE ID IN : stepIds 
                  ];
                
    }

    global void execute(Database.BatchableContext bc, List<Step__c> scope){
        
        List<Step__c> listToUpdate = new List<Step__c>();
        
        for(Step__c thisStep : scope ){
            if(!test.isRunningTest()){
            	thisStep.Status__c ='a1M20000001iL9Z';
            	listToUpdate.add(thisStep);
            }
        }
        
        update listToUpdate;
    }
    
    global void finish(Database.BatchableContext bc){
   		closeCollectStepsBatch objSchedule = new closeCollectStepsBatch();
        Datetime dt = DateCalculation(system.now(), 50);
        string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();
        for(CronTrigger obj : [select Id from CronTrigger where CronJobDetailId IN (SELECT Id FROM CronJobDetail where Name = 'Close Courier Steps')])
            system.abortJob(obj.Id);
        system.schedule('Close Courier Steps', sCornExp, objSchedule);
    } 
    
     public static DateTime DateCalculation(DateTime dt,Integer iFrequnacy){
		Datetime temp = dt.addMinutes(iFrequnacy);
		if(temp.hour() >= 18){
			temp = temp.addDays(1);
			string sDay = temp.format('EEEE');
			system.debug('sDay is : '+sDay);
			if(sDay == 'Friday'){
				temp = temp.addDays(2);
			}else if(sDay == 'Saturday'){
				temp = temp.addDays(1);
			}
			DateTime tempDate = Datetime.newInstance(temp.year(), temp.month(), temp.day(), 7, 30, 0);
			return tempDate;
		}
		return temp;
	}

}