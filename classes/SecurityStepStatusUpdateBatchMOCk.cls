/******************************************************************************************
 *  Class Name  : SecurityStepStatusUpdateBatchMOCk
 *  Author      : Sai kalyan Sanisetty 
 *  Company     : DIFC
 *  Date        : 14 April 2019        
 *  Description : Test Class for Step Helper                  
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1   14 April 2019         Sai                Initial Version  
*******************************************************************************************/

@isTest
global class SecurityStepStatusUpdateBatchMOCk implements HttpCalloutMock {
	
	global HTTPResponse respond(HTTPRequest req) {
		
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        res.setBody('{"DIFC":"DIFC"}');
        res.setStatusCode(200);
        return res;
	}
}