/**********************************************************************************************************************************************************
 *  Author   : Swati
 *  Company  : NSI
 *  Date     : 08/05/2016
 *  Purpose  : This class is to perform all automated fine process related tasks 
                - Update exception date methods
                - Remove or reduce fine
                - save objection SR
                - fine payment
  ---------------------------------------------------------------------------------------------------------------------------------------------------------
 Modification History 
 Date : 09/08/2018 : Added save and cancel button on popup to allow user to change the fine amount : Assembla no :5620
 ----------------------------------------------------------------------------------------------------------------------------------------------------------
V.No    Date            Updated By  Description
 ----------------------------------------------------------------------------------------------------------------------------------------------------------              

V 1.1  18-June- 2019     Sai        Updated the logic to add Type of request for "Fine objection "
 **********************************************************************************************************************************************************/

public without sharing class cls_automatedFineProcess {

    public service_request__c objSR {get; set;}
    public string typeOfSR {get; set;}
    public string RelatedRequest {get; set;}
    public string client {get; set;}
    public string Compliance {get; set;}
    public string complianceSelected {get; set;}
    public string objectionDescription {get; set;}
    public string priceItemToDelete {get; set;}
    
    public list<exceptionDateWrapper> lstSetController{get;set;}
    public list<exceptionDateWrapper> listOfCompliance {get;set;}
    public list<SR_Price_Item__c> listOfFine {get;set;}
    public list<SR_Price_Item__c> OldlistOfFine {get;set;}
    
    public boolean isObjection {get; set;}
    public boolean selectAll {get; set;}
    public boolean isAllowedToEdit {get; set;}
    
    public date exceptionDate {get;set;}
    
    public List<SelectOption> complianceType {get; set;}
    public Boolean isSendNotification {get;set;}
    public List<Account> accList;
    public List<contact> contList;
    public string accId;
    Map<string,SR_Price_Item__c> mapSRList;
    public String SRTypeofRequest;    //This Variable added by Sai to Populate Type of request for OBJECTION SR's
    CustomIterable objTemp;
    
/**********************************************************************************************************************************************************/
    //Constructor used for update exception date tab, create objection SR, pay fine
    
    public cls_automatedFineProcess(){
        typeOfSR = ApexPages.CurrentPage().GetParameters().Get('TypeOfSR');
        string fineId = ApexPages.CurrentPage().GetParameters().Get('fineId');
        string isObject = ApexPages.CurrentPage().GetParameters().Get('isObjection');  
        
        isObjection = false;
        isAllowedToEdit = false;
        isSendNotification = false;
        accList = new List<Account>();
        contList = new List<Contact>();
        mapSRList = new Map<string,SR_Price_Item__c>();
        if(string.isNotblank(isObject) && isObject == 'true'){
            isObjection = true; 
        }
        
        if(string.isNotBlank(typeOfSR) && string.isNotBlank(fineId)){
            objSR = new service_request__c();
            objSR = initializeObjectionSR(fineId, typeOfSR);
        }
        if(isObjection == false){
            complianceType = new List<SelectOption>();
            complianceType.add(new SelectOption('Open','Open'));
            complianceType.add(new SelectOption('Defaulted','Defaulted'));
            complianceType.add(new SelectOption('Created','Created'));
            complianceType.add(new SelectOption('Fulfilled','Fulfilled'));  
            complianceSelected = 'Open';        
            getAllRelatedCompliances(); 
        }
        
        OldlistOfFine = new list<SR_Price_Item__c>();
    }
    
/**********************************************************************************************************************************************************/
    //Constructor used for reducing price item in an issue fine SR    
    
    public cls_automatedFineProcess(ApexPages.StandardController controller){
        string strSRID = Apexpages.currentPage().getParameters().get('Id');
        objSR = new service_request__c();
        isAllowedToEdit = false;
        objSR = initializeObjectionSR(strSRID, 'reduceFine');
        if(objSR!=null){
            isAllowedToEdit = isAllowedToEdit();
            if(isAllowedToEdit == true){
                getRelatedPriceItem();
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Insufficient privileges. Please contact system administrator.'));
            }
        }
    }
    

/**********************************************************************************************************************************************************/    
    //get related SR on which action needs to be performed
    
    public service_request__c initializeObjectionSR(string fineId, string typeOfSR){
        try{
            string recordTypeName = '';
            if(typeOfSR=='Objection'){
                recordTypeName = 'Objection_SR';    
            }
            else if(typeOfSR=='Payment'){
                recordTypeName = 'Issue_Fine';
            }
            else if(typeOfSR=='reduceFine'){
                recordTypeName = 'Issue_Fine';
            }
            
            list<service_request__c> tempSRList = [select id, Customer__c,Type_of_Request__c, Linked_SR__c, Compliance__c, Reason_for_Request__c,
                                                   Customer__r.name, name,Compliance__r.name, Date_of_Written_Notice__c, date__c, End_Date__c
                                                   from service_request__c 
                                                   where id=:fineId and record_type_name__c=:'Issue_Fine' limit 1];
            
            if(tempSRList!=null && !tempSRList.isEmpty() && typeOfSR=='reduceFine'){
                service_request__c objSR = new service_request__c();
                objSR = tempSRList[0];
                return objSR;
            }
            SRTypeofRequest = tempSRList[0].Type_of_Request__c;
            
            if(tempSRList!=null && !tempSRList.isEmpty() && typeOfSR!='reduceFine'){
                service_request__c objSR = new service_request__c();
                objSR.customer__c = tempSRList[0].Customer__c;
                if(typeOfSR=='Objection'){
                    objSR.Linked_SR__c= tempSRList[0].id; 
                    objSR.Date_of_Written_Notice__c= tempSRList[0].Date_of_Written_Notice__c; 
                    objSR.date__c= tempSRList[0].date__c; 
                    objSR.End_Date__c= tempSRList[0].End_Date__c;  
                    list<SR_Template__c> listSRTemplate = [select id,SR_Description__c from SR_Template__c where SR_RecordType_API_Name__c=:'Objection_SR']; 
                    if(listSRTemplate!=null && !listSRTemplate.isEmpty() && listSRTemplate[0].SR_Description__c!=null){
                        objectionDescription = listSRTemplate[0].SR_Description__c;
                    }
                }
                objSR.Compliance__c = tempSRList[0].Compliance__c ;
                client = tempSRList[0].Customer__r.name;
                RelatedRequest  = tempSRList[0].name;
                Compliance = tempSRList[0].Compliance__r.name ;
                return objSR;
            }
        }catch(Exception e){
             insert LogDetails.CreateLog(null, 'cls_automatedFineProcess/initializeObjectionSR', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
        }
        return null;
    }

/**********************************************************************************************************************************************************/    
    //save the objection SR raised by client
    public pageReference saveObjectionSR(){
        try{
            pageReference redirectPage = null;
            Id objectionRecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Objection SR').getRecordTypeId();
            if(objSR!=null && objectionRecordTypeId!=null){
                objSR.recordTypeId = objectionRecordTypeId;
                if(SRTypeofRequest !=null){
                	objSR.type_of_request__c = SRTypeofRequest;
                }
                insert objSR;
                
                if(objSR.Linked_SR__c !=null){
                	service_request__c fineSRRequest = new service_request__c();
                	fineSRRequest = [SELECT Id,is_Objection_created__c FROM service_request__c where ID=:objSR.Linked_SR__c];
                	fineSRRequest.is_Objection_created__c = true;
                	UPDATE fineSRRequest;
                }
                
                redirectPage = new pagereference('https://portal.difc.ae/'+objSR.id);
            }
            redirectPage.setRedirect(true);
            return redirectPage;
        }catch(Exception e){
             insert LogDetails.CreateLog(null, 'cls_automatedFineProcess/saveObjectionSR', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
        }
        return null;
    }

/**********************************************************************************************************************************************************/    
    //redirect to sr submission page to deduct fine for the issue fine SR
    public pageReference finePaymentSR(){
        pageReference redirectPage = null; 
        redirectPage = new pagereference('/srsubmissionpage?id='+objSR.id);
        redirectPage.setRedirect(true);
        return redirectPage;
    }

/**********************************************************************************************************************************************************/    
    //all Methods related to removal of fine for an Issued Fine SR
    
    //is logged in user allowed to delete price item
    public boolean isAllowedToEdit(){
        boolean isAllowedToEdit = false;
        list<user> tempUserList = [select id,Allow_Fine_Process_Reduction__c from user where id=:userinfo.getUserid() and isactive=:true ];
        if(tempUserList!=null && !tempUserList.isEmpty() && tempUserList[0].Allow_Fine_Process_Reduction__c == true){
            isAllowedToEdit = true;         
        }
        return isAllowedToEdit;
    }
    
    //************************************************************************************************************************************************//
    
    //get all related price item associated to a fine for reduction
    public void getRelatedPriceItem(){
        try{
           
            listOfFine = [select id,Price__c,ServiceRequest__r.Customer__c,SRPriceLine_Text__c,SendNotification__c,status__c,createdDate,Reason__c from SR_Price_Item__c where ServiceRequest__c=:objSR.id and status__c =: 'Added'];   
            system.debug('---size--'+listoffine.size());
            isSendNotification  = true;
            accId = listOfFine[0].ServiceRequest__r.Customer__c;
           
            if(listOfFine!=null && !listOfFine.isEmpty()){
                OldlistOfFine = new list<SR_Price_Item__c>();
                OldlistOfFine  = listOfFine;
            }
            else{
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'No fine Exist to remove.'));
            }
        }catch(Exception e){
             insert LogDetails.CreateLog(null, 'cls_automatedFineProcess/updateExceptionDate', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
        }
    }
    
    //************************************************************************************************************************************************//
    
    //make price item 0 for selected fine
    public void deletePriceItem(){
        try{
            if(string.isNotBlank(priceItemToDelete)){
                list <SR_Price_Item__c> itemToDelete = [select id,Price__c,status__c from SR_Price_Item__c where id =: priceItemToDelete];
                if(itemToDelete!=null && !itemToDelete.isEmpty()){
                    itemToDelete[0].Price__c = 0;
                    itemToDelete[0].status__c = 'Cancelled';
                    update itemToDelete;
                    getRelatedPriceItem();  
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.info,'Fine removed successfully'));
                }
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Error in removing fine.'));
            insert LogDetails.CreateLog(null, 'cls_automatedFineProcess/updateExceptionDate', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
        }
    }
    
    //************************************************************************************************************************************************//
    
    //redirect back to SR once price is deleted
    public pageReference reDirectToMainSR(){
        pageReference redirectPage = null; 
        redirectPage = new pagereference('/'+objSR.id);
        redirectPage.setRedirect(true);
        system.debug('---redirectPage----'+redirectPage);
        return redirectPage;
    }
    
/**********************************************************************************************************************************************************/    
    //all Methods related to Update exception tab
    
    //get all annual return compliances to update exception date
    public pageReference getAllRelatedCompliances(){
        try{
            listOfCompliance = new list<exceptionDateWrapper>();
            lstSetController = new List<exceptionDateWrapper>();
            
            list <compliance__c> listARCompliance = new list <compliance__c>();
            listARCompliance = [select id, name, status__c, Defaulted_Date__c ,exception_date__c, start_date__c, 
                                       end_date__c, account__r.name 
                                from compliance__c 
                                where name='Annual Return' and status__c=:complianceSelected and account__c!=null];
            for(compliance__c tempObj : listARCompliance){
                exceptionDateWrapper obj = new exceptionDateWrapper(tempObj);
                obj.recordid = tempObj.id;
                obj.name = tempObj.name;
                obj.client = tempObj.account__r.name;
                obj.startDate = tempObj.start_Date__c;
                obj.endDate = tempObj.end_Date__c;
                obj.exceptionDate = tempObj.exception_Date__c;
                obj.defaultedDate = tempObj.defaulted_Date__c;
                listOfCompliance.add(obj);  
            }
            objTemp = new CustomIterable (listOfCompliance); 
            objTemp.setPageSize = 100;
            next(); 
        }catch(Exception e){
             insert LogDetails.CreateLog(null, 'cls_automatedFineProcess/getAllRelatedCompliances', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
        }
        return null;    
    }
    
    //*********************************************************************************************************************************************//
    
    //Update exception date on annual return compliance
    public pagereference updateExceptionDate(){
        try{
            list<compliance__c> templist = new list<compliance__c>();
            if(listOfCompliance!=null && !listOfCompliance.isEmpty()){
                for(exceptionDateWrapper obj : listOfCompliance){
                    if(obj.selectRecord == true && obj.exceptionDate!=null){
                        compliance__c objComp = new compliance__c();
                        objComp.id = obj.recordId;
                        objComp.exception_date__c = obj.exceptionDate;
                        templist.add(objComp);
                    }   
                }
                if(templist!=null && !templist.isEmpty()){
                    update templist;
                }
            }
        }catch(Exception e){
             insert LogDetails.CreateLog(null, 'cls_automatedFineProcess/updateExceptionDate', 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
        }
        return null;
    }    
    
    //*********************************************************************************************************************************************//
    
    //Method to select all records and update exception Date if Select all checkbox is selected
    public pageReference selectAll(){
        if(listOfCompliance!=null && !listOfCompliance.isEmpty() && selectAll==true){
            for(exceptionDateWrapper obj : listOfCompliance){
                obj.selectRecord = true;    
            }
        }
        
        if(listOfCompliance!=null && !listOfCompliance.isEmpty() && selectAll==false){
            for(exceptionDateWrapper obj : listOfCompliance){
                obj.selectRecord = false;   
            }
        }
        
        if(listOfCompliance!=null && !listOfCompliance.isEmpty() && exceptionDate!=null){
            for(exceptionDateWrapper obj : listOfCompliance){
                obj.exceptionDate = exceptionDate;  
            }
        }
        
        if(listOfCompliance!=null && !listOfCompliance.isEmpty() && exceptionDate==null){
            for(exceptionDateWrapper obj : listOfCompliance){
                obj.exceptionDate = exceptionDate;  
            }
        }
        return null;
    }
    
    //*********************************************************************************************************************************************//
    
    //Methods to handle pagination on page
   
    public Boolean hasNext { get { return objTemp.hasNext();} set;}
    
    public Boolean hasPrevious { get { return objTemp.hasPrevious();} set;}
    
    public void next(){
        lstSetController = objTemp.next(); 
    }
    
    public void previous(){
        lstSetController = objTemp.previous();
    }
    
    // Assembla : 5620
    public pagereference savevalue(){
        Boolean isChanged = false;
        mapSRList = new Map<string,SR_Price_Item__c>();
       OldlistOfFine = new list<SR_Price_Item__c>();
       try{
       OldlistOfFine  = [select id,Price__c,SRPriceLine_Text__c,SendNotification__c,status__c,createdDate,Reason__c from SR_Price_Item__c where ServiceRequest__c=:objSR.id and status__c =: 'Added'];
       
        for(SR_Price_Item__c itrOld:OldlistOfFine){
            for(SR_Price_Item__c itrNew : listOfFine){
                if(itrold.id ==itrNew.id && itrOld.Price__c != itrNew.Price__c && (itrNew.Reason__c=='' || itrNew.Reason__c==null)){
                    isChanged  = true;
                    system.debug('-----------'+ischanged);
                     ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'please fill the reason to change the amount'));
                     return null;
                }
                if(isSendNotification){
                    itrNew.SendNotification__c = true;
                     mapSRList.put(itrNew.id,itrNew);
                }
                if(!isSendNotification){
                    itrNew.SendNotification__c = false;
                }
            }
        }
        List<SR_Price_Item__c> rtnList = new List<SR_Price_Item__c>();
            if(listOfFine.size()>0){
                Boolean isContactnull = false;
                for(SR_Price_Item__c c:listOfFine){
                    if(c.Principal_User__c==null || c.Principal_User_2__c==null || c.Principal_User_3__c==null || c.Principal_User_4__c==null || c.Principal_User_5__c==null){
                        isContactnull = true;
                    }
                }
                
                if(mapSRList.size()>0 && isContactnull){
                    accList = [select id from account where id=:accId];
                    contList = [select Id from Contact where AccountId=:accList[0].Id AND RecordType.DeveloperName='Portal_User' AND Id IN (select ContactId from User where Contact.AccountId=:accList[0].Id AND IsActive=true)];
                    rtnList = PopulatePortalUsers('SR_Price_Item__c',contList,mapSRList);
                }
                if(rtnList.size()>0){
                    for(SR_Price_Item__c s:rtnList){
                        for(SR_Price_Item__c up : listOfFine){
                            if(s.id == up.id){
                                up.Principal_User__c = s.Principal_User__c;
                                up.Principal_User_2__c = s.Principal_User_2__c;
                                up.Principal_User_3__c = s.Principal_User_3__c;
                                up.Principal_User_4__c = s.Principal_User_4__c;
                                up.Principal_User_5__c = s.Principal_User_5__c;
                            }
                        }
                    }
                }
                update listOfFine;             
            }

        }catch(Exception e){
            system.debug('-----'+e);
        }
        
        pageReference redirectPage = null; 
        redirectPage = new pagereference('/'+objSR.id);
        redirectPage.setRedirect(true);
        return redirectPage;
    }
    
    // Assembla : 5620 Generic method to copy the portal user email id
    public static List<sobject> PopulatePortalUsers(string objName,list<Contact> lstContacts,Map<string,Sobject> sObjMaplst){
        
        List<sObject> sObjList = new List<sObject>();
        List<sObject> sObjList2 = new List<SObject>();
        SObjectType objType = Schema.getGlobalDescribe().get(objName);
        Map<String,Schema.SObjectField> mfields = objType.getDescribe().fields.getMap();
        string str='a0U2000000lGJIT';
        Set<string> keys = sObjMaplst.keySet(); 
        String queryStr='select id,Principal_User__c,Principal_User_2__c,Principal_User_3__c ,Principal_User_4__c ,Principal_User_5__c  from ' + objName +' where id=:keys';
        sObjList =Database.query(queryStr);
        
        for(Sobject sObj:sObjMaplst.values()){
           sObject addsObj = Schema.getGlobalDescribe().get(objName).newSObject() ;
            if(lstContacts != null && lstContacts.isEmpty() == false){
                if(lstContacts.size() > 0 && mfields.containsKey('Principal_User__c')){
                    addsObj.put(mfields.get('Principal_User__c'),lstContacts[0].Id);
                  }
                if(lstContacts.size() > 1 && mfields.keyset().contains('Principal_User_2__c')){
                    addsObj.put(mfields.get('Principal_User_2__c '),lstContacts[0].Id);
                   }
                if(lstContacts.size() > 2 && mfields.keyset().contains('Principal_User_3__c ')){
                    addsObj.put(mfields.get('Principal_User_3__c '),lstContacts[0].Id);
                   }
                if(lstContacts.size() > 3 && mfields.keyset().contains('Principal_User_4__c')){
                    addsObj.put(mfields.get('Principal_User_4__c '),lstContacts[0].Id);
                   }
                if(lstContacts.size() > 4 && mfields.keyset().contains('Principal_User_5__c ')){
                    addsObj.put(mfields.get('Principal_User_5__c '),lstContacts[0].Id);
                }
                  
                addsObj.put(mfields.get('id'),sObj.ID);
            }
            sObjList2.add(addsObj);
            system.debug('--------------'+sObjList2);
        }
        
        return sObjList2 ;
    }
/**********************************************************************************************************************************************************/        
}