/**
*  Author   : Swati Sehrawat
*  Company  : NSI JLT
*  Date     : 26-May-2016  
*  Test class for cls_AutomatedFineProcess
* 
*/

@isTest
private class Test_CC_FineProcess{
    
    static testMethod void testmethod1() {
        
        
        List<Account> accList = new List<Account>();
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        accList.add(objAccount);

        
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account';
        objAccount1.ROC_Status__c = 'Active';
        accList.add(objAccount1);
        insert accList;

        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        //Id portalUserId1 = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal_User').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact';
        objContact1.lastname = 'Test Contact1';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = portalUserId;
        objContact1.Email = 'test1@difcportal.com';
        insert objContact1;

        AccountContactRelation acRelation = new AccountContactRelation();
        acRelation.AccountId = objAccount1.Id;
        acRelation.ContactId = objContact1.Id;
        acRelation.Roles = 'Company Services';
        acRelation.IsActive = true;
        insert acRelation;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactId=objContact.Id, Community_User_Role__c='Company Services',
                                TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        String objRectype;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Issue_Fine' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        SR_Status__c objIssue = new SR_Status__c();
        objIssue.name = 'Issued';
        objIssue.Code__c = 'ISSUED';
        insert objIssue;
        
        compliance__c objComp = new compliance__c();
        objComp.name = 'License Renewal';
        objComp.status__c = 'Defaulted';
        objComp.account__r = objAccount;
        objComp.account__c = objAccount.id;
        objComp.start_date__c = system.today();
        objComp.end_date__c = system.today().addDays(15);
        objComp.defaulted_date__c = system.today().addDays(15);
        insert objComp;
        
        compliance__c objComp1 = new compliance__c();
        objComp1.name = 'License Renewal';
        objComp1.status__c = 'Defaulted';
        objComp1.account__r = objAccount1;
        objComp1.account__c = objAccount1.id;
        objComp1.start_date__c = system.today();
        objComp1.end_date__c = system.today().addDays(15);
        objComp1.defaulted_date__c = system.today().addDays(15);
        insert objComp1;
        
        List<Service_Request__c> srList = new List<Service_Request__c>();
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objIssue.id;
        objSR.External_SR_Status__c = objIssue.id;
        objSR.compliance__r = objComp;
        objSR.compliance__c = objComp.id;
        objSR.type_of_request__c = objComp.name;
        srList.add(objSR);
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__r = objAccount1;
        objSR1.Customer__c = objAccount1.id;
        objSR1.RecordTypeId = issueFineId;
        objSR1.submitted_date__c = date.today();
        objSR1.Internal_SR_Status__c = objIssue.id;
        objSR1.External_SR_Status__c = objIssue.id;
        objSR1.compliance__r = objComp1;
        objSR1.compliance__c = objComp1.id;
        objSR1.type_of_request__c = objComp1.name;
        srList.add(objSR1);
        insert srList;
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Status__c = 'Added';
        objSRItem.Price__c = 1000;
        insert objSRItem;
        
        string objectionId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Objection_SR' and sObjectType='Service_Request__c']){
            objectionId = rectyp.Id;
        }
        Service_Request__c objectionSR = new Service_Request__c();
        objectionSR.Customer__r = objAccount;
        objectionSR.RecordTypeId = objectionId;
        objectionSR.linked_SR__c = objSR.id;
        objectionSR.submitted_date__c = date.today();
        insert objectionSR; 
        
        
        string licenseRenewalId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='License_Renewal' and sObjectType='Service_Request__c']){
            licenseRenewalId = rectyp.Id;
        }
        Service_Request__c licenseRenewalSR = new Service_Request__c();
        licenseRenewalSR.Customer__r = objAccount;
        licenseRenewalSR.Customer__c = objAccount.id;
        licenseRenewalSR.RecordTypeId = licenseRenewalId;
        licenseRenewalSR.I_agree__c = true;
        licenseRenewalSR.submitted_date__c = date.today();
        insert licenseRenewalSR; 
        
        List<Step__c> liststep= new List<Step__c>();
        step__c objStep  = new Step__c();
        objStep.SR__r = objSR;
        objStep.SR__c = objSR.id;
        liststep.add(objStep);
        //insert objStep; 
        
        step__c objStep1  = new Step__c();
        objStep1.SR__r = objSR1;
        objStep1.SR__c = objSR1.id;
        liststep.add(objStep1);
        insert liststep; 
        
        step__c objectionStep  = new Step__c();
        objectionStep.SR__r = objectionSR;
        insert objectionStep;   
        
        SR_Status__c objWaived = new SR_Status__c();
        objWaived.name = 'Waived';
        objWaived.Code__c = 'WAIVED';
        insert objWaived;
        
        CC_FineProcess.uploadSignedDocument(objStep);
        CC_FineProcess.AllowSRSubmit(licenseRenewalSR);
        CC_FineProcess.updateFineSRtoWaived(objectionStep);
        
        
        system.debug('--1--'+objStep);
        system.debug('--2--'+objStep.SR__c);
        CC_FineProcess.sendMailforOtherFine(objStep);
        
        CC_FineProcess.sendMailforOtherFine(objStep1);
        CC_FineProcess.updateObjectionRaised(objStep);  
    }
    
    static testMethod void testmethod2() {
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactId=objContact.Id, Community_User_Role__c='Company Services',
                                TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        String objRectype;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Issue_Fine' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        SR_Status__c objIssue = new SR_Status__c();
        objIssue.name = 'Issued';
        objIssue.Code__c = 'ISSUED';
        insert objIssue;
        
        compliance__c objComp = new compliance__c();
        objComp.name = 'License Renewal';
        objComp.status__c = 'Defaulted';
        objComp.account__r = objAccount;
        objComp.account__c = objAccount.id;
        objComp.start_date__c = system.today();
        objComp.end_date__c = system.today().addDays(15);
        objComp.defaulted_date__c = system.today().addDays(15);
        insert objComp;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objIssue.id;
        objSR.External_SR_Status__c = objIssue.id;
        objSR.compliance__r = objComp;
        objSR.compliance__c = objComp.id;
        objSR.type_of_request__c = 'UBO';
        insert objSR; 
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Status__c = 'Added';
        objSRItem.Price__c = 1000;
        insert objSRItem;
        
        string objectionId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Objection_SR' and sObjectType='Service_Request__c']){
            objectionId = rectyp.Id;
        }
        Service_Request__c objectionSR = new Service_Request__c();
        objectionSR.Customer__r = objAccount;
        objectionSR.RecordTypeId = objectionId;
        objectionSR.linked_SR__c = objSR.id;
        objectionSR.submitted_date__c = date.today();
        insert objectionSR; 
        
        
        string licenseRenewalId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='License_Renewal' and sObjectType='Service_Request__c']){
            licenseRenewalId = rectyp.Id;
        }
        Service_Request__c licenseRenewalSR = new Service_Request__c();
        licenseRenewalSR.Customer__r = objAccount;
        licenseRenewalSR.Customer__c = objAccount.id;
        licenseRenewalSR.RecordTypeId = licenseRenewalId;
        licenseRenewalSR.I_agree__c = true;
        licenseRenewalSR.submitted_date__c = date.today();
        insert licenseRenewalSR; 
        
        step__c objStep  = new Step__c();
        objStep.SR__r = objSR;
        objStep.SR__c = objSR.id;
        insert objStep; 
        
        step__c objectionStep  = new Step__c();
        objectionStep.SR__r = objectionSR;
        insert objectionStep;   
        
        SR_Status__c objWaived = new SR_Status__c();
        objWaived.name = 'Waived';
        objWaived.Code__c = 'WAIVED';
        insert objWaived;
        
        Sr_Status__c srStatusCompleted = new Sr_Status__c();
        srStatusCompleted.Code__c = 'COMPLETED';
        srStatusCompleted.Name = 'Completed';
        insert srStatusCompleted;
    
        Sr_Status__c srStatusWithdraw = new Sr_Status__c();
        srStatusWithdraw.Code__c = 'Withdraw';
        srStatusWithdraw.Name = 'Withdraw';
        insert srStatusWithdraw;
        
        CC_FineProcess.uploadSignedDocument(objStep);
        CC_FineProcess.AllowSRSubmit(licenseRenewalSR);
        CC_FineProcess.updateFineSRtoWaived(objectionStep);
        
        
        system.debug('--1--'+objStep);
        system.debug('--2--'+objStep.SR__c);
        CC_FineProcess.sendMailforOtherFine(objStep);
        CC_FineProcess.updateObjectionRaised(objStep); 
        CC_FineProcess.updateIssueFineStatus(objStep.SR__c);
        CC_FineProcess.withdrawIssueFineStatus(objStep.SR__c);
    }
    
    static testMethod void testmethod3() {
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                                EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                                LocaleSidKey='en_US', ProfileId = objProfile.Id,
                                ContactId=objContact.Id, Community_User_Role__c='Company Services',
                                TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        String objRectype;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Issue_Fine' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        SR_Status__c objIssue = new SR_Status__c();
        objIssue.name = 'Issued';
        objIssue.Code__c = 'ISSUED';
        insert objIssue;
        
        compliance__c objComp = new compliance__c();
        objComp.name = 'License Renewal';
        objComp.status__c = 'Defaulted';
        objComp.account__r = objAccount;
        objComp.account__c = objAccount.id;
        objComp.start_date__c = system.today();
        objComp.end_date__c = system.today().addDays(15);
        objComp.defaulted_date__c = system.today().addDays(15);
        insert objComp;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objIssue.id;
        objSR.External_SR_Status__c = objIssue.id;
        objSR.Comments__c = 'Test';
        //objSR.compliance__c = objComp.id;
        objSR.type_of_request__c = 'Other';
        insert objSR; 
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Status__c = 'Added';
        objSRItem.Price__c = 1000;
        insert objSRItem;
        
        string objectionId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Objection_SR' and sObjectType='Service_Request__c']){
            objectionId = rectyp.Id;
        }
        Service_Request__c objectionSR = new Service_Request__c();
        objectionSR.Customer__r = objAccount;
        objectionSR.RecordTypeId = objectionId;
        objectionSR.linked_SR__c = objSR.id;
        objectionSR.submitted_date__c = date.today();
        insert objectionSR; 
        
        
        string licenseRenewalId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='License_Renewal' and sObjectType='Service_Request__c']){
            licenseRenewalId = rectyp.Id;
        }
        Service_Request__c licenseRenewalSR = new Service_Request__c();
        licenseRenewalSR.Customer__r = objAccount;
        licenseRenewalSR.Customer__c = objAccount.id;
        licenseRenewalSR.RecordTypeId = licenseRenewalId;
        licenseRenewalSR.I_agree__c = true;
        licenseRenewalSR.submitted_date__c = date.today();
        insert licenseRenewalSR; 
        
        step__c objStep  = new Step__c();
        objStep.SR__r = objSR;
        objStep.SR__c = objSR.id;
        insert objStep; 
        
        step__c objectionStep  = new Step__c();
        objectionStep.SR__r = objectionSR;
        insert objectionStep;   
        
        SR_Status__c objWaived = new SR_Status__c();
        objWaived.name = 'Waived';
        objWaived.Code__c = 'WAIVED';
        insert objWaived;
        
        CC_FineProcess.uploadSignedDocument(objStep);
        CC_FineProcess.AllowSRSubmit(licenseRenewalSR);
        CC_FineProcess.updateFineSRtoWaived(objectionStep);
        
        
        system.debug('--1--'+objStep);
        system.debug('--2--'+objStep.SR__c);
        CC_FineProcess.sendMailforOtherFine(objStep);
        CC_FineProcess.updateObjectionRaised(objStep);  
    }
}