/**
 * Created by bilalnazir on 10/9/16.
 */

public without sharing class PDFDocumentViewerCls_Captcha {

    public string documentNumber {get; set;}

    public transient string AttachValBase64{get;set;}
    public transient blob AttachBlobVal{get;set;}
    public string AtchContentType{get;set;}

    public boolean blobError {set; get;}

    public boolean isCaptchaEnabled {set; get;}
    public String debugMessage {get; set;}

    private Map<String,Google_Captcha_Keys__c> mapOfKeys;
    
    public PDFDocumentViewerCls_Captcha(){
        
        mapOfKeys = Google_Captcha_Keys__c.getAll();

        isCaptchaEnabled = true;

        if(isCaptchaEnabled){
            verified = false;
            success = false;
        }else{
            verified = true;
            success = true;
        }


        blobError = false;

        //documentNumber = '0082128-3296-0750582-';//Testing

        if(apexpages.currentPage().getparameters().get('ref')!=null && apexpages.currentPage().getparameters().get('ref')!=''){
            documentNumber = apexpages.currentPage().getparameters().get('ref');
            viewDocument();
        }

    }
    public string searchDoc{get;set;}

    public PageReference viewDocument(){

        this.verified = false;
        if(isCaptchaEnabled && !this.verified)
            validateCaptcha();

        if(this.verified){
            loadDocument(documentNumber);
        }
        return null;
    }
    
    public void loadDocument(String ReferenceNumber){
        if(ReferenceNumber!=null && ReferenceNumber!=''){

            try {
                if(ReferenceNumber.startsWith('SR') ){
                    List<HexaBPM__SR_Doc__c> srDoc = new List<HexaBPM__SR_Doc__c>();
                    srDoc = [select HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where Document_No_Id__c=:ReferenceNumber.trim()];
                    
                    for(ContentVersion eachVersion:[SELECT Id,VersionData,Application_Document__c,FileExtension FROM Contentversion where ContentDocumentID=:srDoc[0].HexaBPM__Doc_ID__c]){
                        AttachBlobVal = eachVersion.VersionData;        AtchContentType = eachVersion.FileExtension;            AttachValBase64 = EncodingUtil.Base64Encode(eachVersion.VersionData);
                    }
                }
                  else if(ReferenceNumber.startsWith('CWL') )
                 {//Co-Working Lease PDF
                     String RecordID=ReferenceNumber.removeStartIgnoreCase('CWL');
                     List<Office_RND_Membership__c> ListRND=[select id from Office_RND_Membership__c where membershipid__c=:RecordID];
                     if(ListRND.Size()==1)
                     {
                           PageReference pdf =  Page.QRViewFintech; pdf.getParameters().put('id',(String)ListRND[0].id);  pdf.setRedirect(true);  AttachBlobVal = pdf.getContent();   AtchContentType = 'application/pdf';AttachValBase64 = EncodingUtil.Base64Encode(pdf.getContent());
                            
                     }
                     
                     
                 }
                else{
                    list<SR_Doc__c> lstSRDoc = [select id,Name,Doc_ID__c,Status__c,Document_Validity__c from SR_Doc__c where Document_No_Id__c=:ReferenceNumber.trim() and Document_Validity__c = true];
                    if(lstSRDoc!= null && lstSRDoc.size() == 1){
                        for(Attachment attch:[Select Id,Body,ContentType from Attachment where Id=:lstSRDoc[0].Doc_ID__c]){
                            AttachBlobVal = attch.Body;                            AtchContentType = attch.ContentType;                            AttachValBase64 = EncodingUtil.Base64Encode(attch.body);
                        }
                    }
                }
                if(AttachValBase64 == null){
                    AttachValBase64 = '0';
                    blobError = true;
                }else {
                    blobError = false;
                }
                
            }catch(Exception ex){
                debugMessage = ex.getMessage();
                System.debug('Error occured:' + ex.getMessage());
            }
        }
    }
    /** Captcha Validation **/
/* Configuration */

    // The API endpoint for the reCAPTCHA service

    // The keys you get by signing up for reCAPTCHA for your domain
//    private static String baseUrl = 'https://www.google.com/recaptcha/api/siteverify';
//    private static String privateKey = '6LdHMwETAAAAABpdyutazGzBHYOi9OEptmgud2UQ';
//    public String publicKey {
//        get {return '6LdHMwETAAAAAKHlf30wDCDDv-ls5KXq8NnMm6W1';}
//    }

    /** Old reCaptcha 1.0 **/
    private static String baseUrl = 'https://www.google.com/recaptcha/api/siteverify';
    //private static String privateKey = '6LepEfwSAAAAAASNFNew1-039Uf-Seykvch5_FUu';
    
    public String publicKey {
        //get {return '6LepEfwSAAAAADUgZe2xPK8Kmmrn4B0RsCz827oo';}
        get{
            if(mapOfKeys.containsKey('Letter Authentication')){
                return PasswordCryptoGraphy.DecryptPassword(mapOfKeys.get('Letter Authentication').Public_Key__c);
            }
            
            return '';
        }
    }

    //Error message for invalid captcha entry
    public String captchaError { get; set; }

    public String response  {
        get {
            return ApexPages.currentPage().getParameters().get('g-recaptcha-response');
            //return ApexPages.currentPage().getParameters().get('recaptcha_response_field');
        }
    }

    public String challenge {
        get {
            return ApexPages.currentPage().getParameters().get('recaptcha_challenge_field');
        }
    }
    /*
    public String response  {
        get {
            return ApexPages.currentPage().getParameters().get('recaptcha_response_field');
        }
    }
*/
    // Whether the submission has passed reCAPTCHA validation or not
    public Boolean verified { get; private set; }
    public Boolean success{set;get;}

    // v1.1
    public void validateCaptcha(){

        system.debug('entered validate captcha');
        
        if(mapOfKeys.containsKey('Letter Authentication')){
        
            String privateKey = PasswordCryptoGraphy.DecryptPassword(mapOfKeys.get('Letter Authentication').Private_Key__c);
            
            // On first page load, form is empty, so no request to make yet
            if ( response == null ) {
                System.debug('reCAPTCHA verification attempt with empty form');
                this.captchaError = 'reCAPTCHA verification attempt with empty form';
    
            }
        
        
            HttpResponse r = makeRequest(baseUrl,
                    'secret=' + privateKey +
                            '&response='  + response +
                            '&remoteip='  + remoteHost
            );
    
    /*
            HttpResponse r = makeRequest(baseUrl,
                    'privatekey=' + privateKey +
                            '&remoteip='  + remoteHost +
                            '&challenge=' + challenge +
                            '&response='  + response
            );
    */
            //debugMessage = JSON.serialize(r.getBody());
            //debugMessage += '==>input Response:' + response;
            System.debug('input Response:===>' + response);
            System.debug('input r:===>' + r);
        
            if ( r!= null )
                this.verified = (r.getBody().contains('true'));
    
    
            if(this.verified) {
                // If they pass verification, do the lead submission
                this.success = true;
                this.captchaError = '';
            }
            else{
                this.captchaError = 'Invalid captcha: please retry';
            }
        }
    }


    /* Private helper methods */

    @TestVisible private static HttpResponse makeRequest(string url, string body)  {
        HttpResponse response = new HttpResponse();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody (body);
        try {
            Http http = new Http();
            response = http.send(req);
            System.debug('reCAPTCHA response: ' + response);
            System.debug('reCAPTCHA body: ' + response.getBody());
        } catch(System.Exception e) {
            System.debug('ERROR: ' + e);
        }
        return response;
    }

    @TestVisible private String remoteHost {
        get {
            String ret = '127.0.0.1';
            // also could use x-original-remote-host
            Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
            if (hdrs.get('x-original-remote-addr')!= null)
                ret =  hdrs.get('x-original-remote-addr');
            else if (hdrs.get('X-Salesforce-SIP')!= null)
                ret =  hdrs.get('X-Salesforce-SIP');
            return ret;
        }
    }

}