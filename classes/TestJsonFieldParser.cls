/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestJsonFieldParser {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Field_Details__c objCS = new Field_Details__c();
        objCS.Name = 'SR Field Id';
        objCS.Object_API_Name__c = 'Amendment__c';
        objCS.Field_API_Name__c = 'ServiceRequest__c';
        objCS.Field_Id__c = '123456789';
        insert objCS;
        
        JsonFieldParser.PrepareURL('Amendment__c', 'ServiceRequest__c', '123456789', 'test');
        JsonFieldParser.PrepareURL(null,null, '123456789', 'test');
        JsonFieldParser.TenantURL('Amendment__c', 'ServiceRequest__c', '123456789', 'test', '');
        JsonFieldParser.TenantURL(null,null, '123456789', 'test','');
        List<String> FieldList= new List<String>{'a--1d--2--e--2'};
        JsonFieldParser.ConstructURL (FieldList);
        JsonFieldParser.FieldURL('Amendment__c','ServiceRequest__c','123456789', 'test', '');
        
    }
    static testMethod void myUnitTest2() {
    	SR_Status__c objStat = new SR_Status__c();
    	objStat.Name = 'Draft';
    	objStat.Code__c = 'DRAFT';
    	insert objStat;
    	
        Account objAccount = new Account();
        objAccount.Name = 'Cancel SR Account';
        objAccount.AccountNumber = '12345';
        insert objAccount;
        RecordType  recTypeOne = [Select id,name,DeveloperName from  RecordType where DeveloperName='Site_Visit_Report'];
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = recTypeOne.id;
        insert objSR;
        RecordType  recType = [Select id,name,DeveloperName from  RecordType where DeveloperName='Request_for_security_deposit_encashment'];
        List<String> FieldList= new List<String>();
        FieldList.add(objAccount.id);
        FieldList.add(objSR.id);
        FieldList.add(recType.id);
        //JsonFieldParser.createSR(FieldList);
    }
}