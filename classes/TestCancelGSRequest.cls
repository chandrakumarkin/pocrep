@isTest(SeeAllData=false)
private class TestCancelGSRequest{
    static TestMethod void penaltyCalculatorTest()
    {
        
         map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_Renewal','Index_Card_Other_Services')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        test.startTest();
        
        CancelGSrequest.myString ='test';
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = label.RORPCompanyCode;
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        insert lstUnits;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        SR_Status__c SRStauts = new SR_Status__c(Name='Cancelled',Code__c='Cancelled');
        SR_Status__c SRStauts1 = new SR_Status__c(Name='Rejected',Code__c='REQUEST_REJECTED');
        lstSRStatus.add(SRStauts1); 
        lstSRStatus.add(SRStauts);        
        insert lstSRStatus;
        

    
   
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        insert objAccount;
        
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Date_of_Declaration__c = Date.today();
        objSR.Email__c = 'test@test.com';
        objSR.RecordTypeId = mapRecordType.get('DIFC_Sponsorship_Visa_Renewal');
        objSR.Building__c = objBuilding.Id;
        objSR.Unit__c = lstUnits[0].Id;
        objSR.Service_Category__c = 'New';
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Rental_Amount__c = 1234;
        objSr.Submitted_DateTime__c = system.today();
        objSR.Lease_Commencement_Date__c = Date.newInstance(2019,04,25);
        objSR.Lease_Expiry_Date__c = Date.newInstance(2030,04,25);
        objSR.Agreement_Date__c = Date.newInstance(2019,04,25);
        objSR.Submitted_Date__c = Date.newInstance(2019,04,29);
        //objSr.Penalty_Applicable__c = false;
        objSr.SR_Group__c = 'GS';
        insert objSR;
        
        
        Step_Template__c stepTemplate = new Step_Template__c();
        stepTemplate.name = 'Front Desk Review';
        stepTemplate.Code__c = 'Front Desk Review';
        stepTemplate.Step_RecordType_API_Name__c = 'Front Desk Review';
        insert stepTemplate;
        
        Status__c stepStatus = new Status__c();
        stepStatus.name = 'Pending';
        stepStatus.Code__c = 'PENDING';
        insert stepStatus;
        
        
        
         Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        objStep.Step_Template__c = stepTemplate.id;
        objStep.Status__c = stepStatus.id;
        objStep.Rejection_Reason__c = 'myString' ;
       
       // insert objStep;
        
       
       
        /**************************/
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Lease Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Lease_Registration';
        objTemplate.Menutext__c = 'Lease_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        objTemplate.SR_Group__c ='GS';
        
        insert objTemplate; 
        
        product2 prod= new product2();
        prod.Name='Penalty Charges';
        prod.ProductCode='Penalty Charges';
        prod.isActive=true;
        prod.Family= 'GS' ;
        insert prod;
        
        SR_Template_Item__c  srItem = new SR_Template_Item__c ();
        sritem.SR_Template__c  = objTemplate.id;
        sritem.Product__c = prod.id;       
        insert srItem;
        
        Pricing_Line__c  prLine = new Pricing_Line__c ();
        prLine.product__c= srItem.product__c;
        prLine.Priority__c =1;
        prLine.Name = 'Penalty Charges';       
        insert prLine;
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Status__c = 'Invoiced';
        objSRItem.Price__c = 1000;
        insert objSRItem;
        
        
      
               
        
        /* Dated_Pricing__c  dtprice = new Dated_Pricing__c ();
        dtprice.Unit_Price__c = 1200;
        dtprice.Pricing_Line__c  = prLine.id;
        insert dtprice;
        /**************************/
        
       // covering class without SR
       ApexPages.StandardController sc = new ApexPages.standardController(new Service_Request__c());
       CancelGSrequest csr = new CancelGSrequest(sc);
      //MyExtension extension = new MyExtension(controller);
       csr.autoRun();
       
       
       // covering class with SR
       sc = new ApexPages.standardController(objSR);
       csr = new CancelGSrequest(sc);
      //MyExtension extension = new MyExtension(controller);
       csr.autoRun();
       
       
       CancelPSATermrequestController  c = new CancelPSATermrequestController(SC);
       c.autoRun();
    }
    
    
    
}