global class SearchAndReplaceSteps implements Database.Batchable < sObject > {

global final String Query;
 global List<sObject> passedRecords= new List<sObject>();


global SearchAndReplaceSteps(String q,List<sObject> listofRecords) {

Query = q;
passedRecords = listofRecords;
}

global Database.QueryLocator start(Database.BatchableContext BC) {
if(query != '')return Database.getQueryLocator(query);
else return DataBase.getQueryLocator([Select id,Sys_Additional_Email__c,SAP_EMAIL__c,Contractor_email_for_fit_out__c,Client_Email_for_fit_out__c,Awaiting_Originals_Email_Text__c ,Applicant_Email__c ,Applicant_Mobile__c,Mobile_No__c,Telephone_Number__c from Step__c WHERE Id IN: passedRecords and Id != null]);
}

global void execute(Database.BatchableContext BC, List < Step__c > scope) {
    for (Step__c Sr: scope) {
    
        Sr.Sys_Additional_Email__c ='test@difc.ae.invalid';
        //Sr.SAP_EMAIL__c = 'test@difc.ae.invalid';
        Sr.Contractor_email_for_fit_out__c ='test@difc.ae.invalid';
        Sr.Client_Email_for_fit_out__c = 'test@difc.ae.invalid';
        Sr.Applicant_Email__c = 'test@difc.ae.invalid';
        Sr.Applicant_Mobile__c = '+971569803616';
        SR.Mobile_No__c = '+971569803616';
        SR.Telephone_Number__c = '+971569803616';
    }
    
    Database.SaveResult[] srList = Database.update(scope, false);
    List<Log__c> logRecords = new List<Log__c>();
    // Iterate through each returned result
    for (Database.SaveResult sr : srList) {
        if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
            System.debug('Successfully inserted account. Account ID: ' + sr.getId());
        }
        else {
            // Operation failed, so get all errors                
            for(Database.Error err : sr.getErrors()) {
                string errorresult = err.getMessage();
                logRecords.add(new Log__c(
                      Type__c = 'SearchAndReplaceSteps',
                      Description__c = 'Exception is : '+errorresult+'=StepId==>'+sr.getId()
                    ));
                System.debug('The following error has occurred.');                    
                //System.debug(err.getStatusCode() + ': ' + err.getMessage());
                //System.debug('Account fields that affected this error: ' + err.getFields());
            }
        }
    }
    if(logRecords.size()>0)insert logRecords;
        
}

global void finish(Database.BatchableContext BC) {}
}