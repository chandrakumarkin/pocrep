public with sharing class TrainingCancellationController {
    public Training_Master__c trainingMaster;
    public boolean submissionAllowed {get;set;}
    public string alertMessage {get;set;}
    
    public TrainingCancellationController(ApexPages.StandardController st){
        //allSeatsOccupied = false;
        submissionAllowed = true;
        if (!Test.isRunningTest())
        	st.addFields(new List<String>{'Status__c'});
        this.trainingMaster = (Training_Master__c) st.getRecord();
        if(trainingMaster.Status__c == 'Cancelled'){
            submissionAllowed = false;
            alertMessage = 'The Training is already cancelled.';
        }
        else if(trainingMaster.Status__c != 'Cancellation Approved'){
            submissionAllowed = false;
            alertMessage = 'Please submit for Approval to cancel the training.';
        }
    }
    public PageReference cancelTraining() {
        PageReference pr ;
        Pr = new PageReference('/'+trainingMaster.id);
        List<Training_Request__c> lstTrainingRequest = new List<Training_Request__c>(); //
        for(Training_Request__c trainingReqObj : [select Status__c from Training_Request__c 
                                                  where TrainingMaster__c = :trainingMaster.Id 
                                                  and Status__c in ('Submitted','Accepted','Invitation Sent','Rescheduled')]){
            trainingReqObj.Status__c = 'Cancelled';
            lstTrainingRequest.add(trainingReqObj);
        }
        try{
            if(lstTrainingRequest != null && lstTrainingRequest.size() > 0)
                update lstTrainingRequest;
            //Update the Training Master Status = Cancelled.
    		trainingMaster.Status__c = 'Cancelled';
            if(trainingMaster != null)
            	update trainingMaster;
            pr.setRedirect(true);
            return pr;
        }
        catch(Exception ex){
            ApexPages.addMessages(ex) ; 
        }
        return null;
    }
    public PageReference cancel(){
        PageReference pr ;
        Pr = new PageReference('/'+trainingMaster.id);
        pr.setRedirect(true);
        return pr;
    }
    /*
     @AuraEnabled
    public static string changeStatus(Id recordId) {
        List<Training_Request__c> lstTrainingRequest = new List<Training_Request__c>(); //
        for(Training_Request__c trainingReqObj : [select Status__c from Training_Request__c 
                                                  where TrainingMaster__c = :recordId and Status__c != 'Invalid']){
            trainingReqObj.Status__c = 'Confirmed';
            lstTrainingRequest.add(trainingReqObj);
        }
        try{
            if(lstTrainingRequest != null && lstTrainingRequest.size() > 0)
                update lstTrainingRequest;
        }
        catch(System.DmlException ex){
            return ex.getDmlMessage(0);
        }
        return 'The Training email is send successfully.';
    }
	*/
}