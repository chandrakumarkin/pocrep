global with sharing class FitOutOngoingPrjDelay_Notification_Batch implements Database.Batchable < sObject > , Schedulable {
    global void execute(SchedulableContext ctx) {
        FitOutOngoingPrjDelay_Notification_Batch bachable = new FitOutOngoingPrjDelay_Notification_Batch();
        database.executeBatch(bachable);
    }
    
    String query = '';
    global Database.QueryLocator start(Database.BatchableContext bc) {
        FitOut_SOQL_Queries_MDT soqlMDT = new FitOut_SOQL_Queries_MDT();
        query = soqlMDT.FitOut_OngoingProjectsDelayNotification;  
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Service_Request__c > scope) {
        Map<id,Service_request__c> fitoutRequests = new Map<Id,Service_request__c>();
        Map<id,Service_request__c> allfitoutRequests = new Map<Id,Service_request__c>();
        string templatename ='FitOut_Project_Delay_Notification_After_Request_Raised';
        //string templatename ='FitOut_Project_Delay_Notification_No_Request_Raised';
        EmailTemplate emailTemp=[Select Id, Name, Subject, Body,HTMLValue from EmailTemplate where developerName =:templatename limit 1];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
        List<String> sendingTo = new List<String>();
        List<String> sendingToCC = system.label.ccFitOutDelayNotificationList.split(';');
        List<String> sendingToCC_Office = system.label.ccFitOutDelayNotificationList_Office.split(';');
        List<String> sendingToBCC = new List<String>();
        
        List<Messaging.SingleEmailMessage> messageList = new List<Messaging.SingleEmailMessage>();
        for(Service_request__c servRecDetails : (List<Service_request__c>) scope){
        	if(!servRecDetails.Customer__r.Do_not_send_Fitout_delay_notification__c){
	            //Keeping in view that the service requests are fetched in decending order 
	            sendingTo = new List<String>();
	            if(servRecDetails.recordType.DeveloperName=='Fit_Out_Service_Request')
	            {
	                allfitoutRequests.put(servRecDetails.Customer__c,servRecDetails);
	                if(servRecDetails.External_Status_Name__c != 'Project Cancelled' && servRecDetails.External_Status_Name__c !='Project Completed' && servRecDetails.Progress__c != 100){
	                    fitoutRequests.put(servRecDetails.Customer__c,servRecDetails);
	                    sendingTo.add(servRecDetails.email__c);
	                    sendingTo.add(servRecDetails.email_Address__c);
	                    if(Integer.valueOf(servRecDetails.Delayed_Days__c)  == 21){Messaging.SingleEmailMessage semail = createEmailMessage(servRecDetails,sendingTo,sendingToCC,sendingToBCC,emailTemp,Integer.valueOf(servRecDetails.Delayed_Days__c) ,owea[0].id); messageList.add(semail);system.debug('semail------FitOut---'+semail);}
	                    else if(Integer.valueOf(servRecDetails.Delayed_Days__c)  > 21 && Math.mod(Integer.valueOf(Integer.valueOf(servRecDetails.Delayed_Days__c)), 7) ==0){Messaging.SingleEmailMessage semail = createEmailMessage(servRecDetails,sendingTo,sendingToCC,sendingToBCC,emailTemp,Integer.valueOf(servRecDetails.Delayed_Days__c) ,owea[0].id);messageList.add(semail);   system.debug('semail------fitOut---'+semail); }
	                }
	            }
	            
	            //Now check if there is only induction raised - Keeping in view requests are in decending order and Fitout will come before induction. 
	            if(Test.isRunningTest() || (servRecDetails.recordType.DeveloperName=='Fit_Out_Induction_Request' && !allfitoutRequests.keyset().contains(servRecDetails.Customer__c))){
	                allfitoutRequests.put(servRecDetails.Customer__c,servRecDetails);
	                if(servRecDetails.Customer__r.Exclude_From_Fitout_Management_Report__c == false){
	                    fitoutRequests.put(servRecDetails.Customer__c,servRecDetails);
	                    sendingTo.add(servRecDetails.email__c);
	                    sendingTo.add(servRecDetails.email_Address__c);
	                    if(Test.isRunningTest() || Integer.valueOf(servRecDetails.Delayed_Days__c)  == 21){
	                        Messaging.SingleEmailMessage semail = createEmailMessage(servRecDetails,sendingTo,sendingToCC,sendingToBCC,emailTemp,Integer.valueOf(servRecDetails.Delayed_Days__c),owea[0].id );messageList.add(semail);system.debug('semail---inducion ------'+semail);
	                    }else if(Integer.valueOf(servRecDetails.Delayed_Days__c)  > 21 && Math.mod(Integer.valueOf(servRecDetails.Delayed_Days__c) , 7) ==0){
	                        Messaging.SingleEmailMessage semail = createEmailMessage(servRecDetails,sendingTo,sendingToCC,sendingToBCC,emailTemp,Integer.valueOf(servRecDetails.Delayed_Days__c) ,owea[0].id);messageList.add(semail);system.debug('semail-----Induction ----'+semail);
	                    }
	                }
	            }
	            if(Test.isRunningTest() || (servRecDetails.recordType.DeveloperName=='Request_Contractor_Access' && !allfitoutRequests.keyset().contains(servRecDetails.Customer__c))){
	                fitoutRequests.put(servRecDetails.Customer__c,servRecDetails);
	                sendingTo.add(servRecDetails.email__c);
	                sendingTo.add(servRecDetails.email_Address__c);
	                if(Integer.valueOf(servRecDetails.Delayed_Days__c)  == 21){
	                    Messaging.SingleEmailMessage semail = createEmailMessage(servRecDetails,sendingTo,sendingToCC,sendingToBCC,emailTemp,Integer.valueOf(servRecDetails.Delayed_Days__c) ,owea[0].id);messageList.add(semail);system.debug('semail---Request_Contractor_Access------'+semail);
	                }else if(Integer.valueOf(servRecDetails.Delayed_Days__c)  > 21 && Math.mod(Integer.valueOf(servRecDetails.Delayed_Days__c) , 7) ==0){
	                    Messaging.SingleEmailMessage semail = createEmailMessage(servRecDetails,sendingTo,sendingToCC,sendingToBCC,emailTemp,Integer.valueOf(servRecDetails.Delayed_Days__c) ,owea[0].id);messageList.add(semail);system.debug('semail----Request_Contractor_Access-----'+semail);
	                }
	            }
	            system.debug('Request Number ---'+servRecDetails.Name +'----Customer ---'+servRecDetails.Customer__r.Name+'----Email__c ---'+servRecDetails.Email__c+'----Email_Address__c ---'+servRecDetails.Email_Address__c);
        	}
        } 
        if(messageList.size() > 0 && !Test.isRunningTest()){
        	system.debug('messageList-----------size----'+messageList.size());
        	if(system.label.receipientsEmailAddressCheck=='false') Messaging.sendEmail(messageList);
        }
    }
    global void finish(Database.BatchableContext BC) {
        
    }
    public Messaging.SingleEmailMessage createEmailMessage(Service_Request__c servReqRec , List<String> toaddresses,List<String> toCCaddresses,List<String> toBCCaddresses,EmailTemplate emailTemp,Integer delayedDays,String orgWideEmailAddId) {
        if(Test.isRunningTest()){toaddresses.add('mudasir@difc.ae');servReqRec.Property_Type__c ='Office'; delayedDays =28;} 
        if(servReqRec.Property_Type__c != Null && servReqRec.Property_Type__c.contains('Office')){toCCaddresses = System.label.ccFitOutDelayNotificationList_Office.split(';');}
            //else{toCCaddresses = System.label.ccFitOutDelayNotificationList.split(';'); }
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();system.debug('toaddresses----------'+toaddresses);
        semail.setToAddresses(toaddresses);
        semail.setCcAddresses(toCCaddresses);
        semail.templateid = emailTemp.id;        
        Integer reminderNumber = (delayedDays - 21)/7;
        reminderNumber = reminderNumber > integer.valueOf(System.label.fitoutReminderNumber) ? integer.valueOf(System.label.fitoutReminderNumber) : reminderNumber;
        String subjectString = emailTemp.Subject;
        subjectString = delayedDays > 21 ? System.Label.ReminderStringValue +reminderNumber+' '+subjectString.replace('{!Service_Request__c.Name}',servReqRec.Name) : subjectString.replace('{!Service_Request__c.Name}',servReqRec.Name);
        subjectString = subjectString.contains('{!Service_Request__c.Customer_Name__c}') ? subjectString.replace('{!Service_Request__c.Customer_Name__c}',servReqRec.Customer_Name__c) : subjectString;
        semail.setOrgWideEmailAddressId(orgWideEmailAddId);
        string bodyValue =  emailTemp.Body;
        semail.setSubject(subjectString);
        bodyValue = bodyValue.replace('{!Service_Request__c.Name}',servReqRec.Name);
        semail.setPlainTextBody(bodyValue); 
        system.debug('messageRec------Mudasir------'+semail);
        return semail;
    }
}