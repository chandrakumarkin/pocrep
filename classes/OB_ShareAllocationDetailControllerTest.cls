@isTest
public class OB_ShareAllocationDetailControllerTest 
{
    public static testmethod void addShareClassTest()
    {
       // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        Account hostingCompany = new Account(Name = 'hostingCompany');
        insert hostingCompany;
        
       
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(1, new List<string> {'In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        section.HexaBPM__Default_Rendering__c=true;
        insert section;

        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
      
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
       
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(9, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        
        // for pescribe company.
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].Id;
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[0].Business_Sector__c = 'Prescribed Companies';
        insertNewSRs[0].Type_of_Entity__c = 'Private';
        insertNewSRs[0].Name_of_the_Hosting_Affiliate__c = hostingCompany.Id;
        
            
        insert insertNewSRs;
        system.debug('######### insertNewSRs'+insertNewSRs);
        
        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1,new List<String>{'Low'},new List<Integer>{1}, new List<String>{'Low'});
        insert listCS;
        
       
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;
        /*
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(1, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insertNewSteps[0].HexaBPM__SR__c = insertNewSRs[0].Id;
        insert insertNewSteps;
        */
        
        
        //create amendments
        list<HexaBPM_Amendment__c> listAmendments = new list<HexaBPM_Amendment__c>() ;
        listAmendments = OB_TestDataFactory.createApplicationAmendment(2,listCS,insertNewSRs);
        listAmendments[0].ServiceRequest__c = insertNewSRs[0].Id;
        listAmendments[0].Amendment_Type__c = 'Operating Location';
        listAmendments[0].Status__c = 'Active';
        listAmendments[0].Role__c = 'Shareholder';
        listAmendments[0].recordTypeId  = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        insert listAmendments;
        
       Currency__c currencyVal = new Currency__c();
       currencyVal.Name = 'AED';
       currencyVal.Active__c = true;
       currencyVal.Currency_Code__c = 'AED';
	   insert  currencyVal;  
   
       Account_Share_Detail__c shareClass= new Account_Share_Detail__c();
       shareClass.Account__c = insertNewAccounts[0].Id;
       shareClass.No_of_shares_per_class__c = 23;
       shareClass.Currency__c = currencyVal.Id;
       shareClass.Nominal_Value__c =100;
       insert shareClass;
       /*
       Shareholder_Detail__c sd = new Shareholder_Detail__c();
       sd.Account__c= insertNewAccounts[0].Id;
       sd.Account_Share__c= shareClass.Id;
       sd.OB_Amendment__c= listAmendments[0].Id;
       sd.No_of_Shares_Allotted__c = 23;
       insert sd;
       */ 
        
          test.startTest();
             OB_ShareAllocationDetailController.RequestWrapper reqParam = new OB_ShareAllocationDetailController.RequestWrapper();
        	 OB_ShareAllocationDetailController.AccountShareDetailWrapper accShrDtl = new OB_ShareAllocationDetailController.AccountShareDetailWrapper();
        	 OB_ShareAllocationDetailController.ShareholderDetailWrapper shrDtlWrap = new OB_ShareAllocationDetailController.ShareholderDetailWrapper();
             OB_ShareAllocationDetailController.AmendmentWrapper amedWrap =  new OB_ShareAllocationDetailController.AmendmentWrapper();
             amedWrap.amedObj = listAmendments[0];
        	 amedWrap.isIndividual = false;
        	 amedWrap.isBodyCorporate = true;
             amedWrap.lookupLabel = '';
             amedWrap.shrDetailWrapLst = new List<OB_ShareAllocationDetailController.ShareholderDetailWrapper>();
             reqParam.flowId = listPageFlow[0].Id;
             reqParam.pageId = listPage[0].Id;
             reqParam.srId = insertNewSRs[0].Id;
             reqParam.flowId =  listPageFlow[0].Id;
        	 reqParam.amedWrap = amedWrap;
             reqParam.amendmentID='';
             //reqParam.stepId = insertNewSteps[0].Id;
             //reqParam.stepObj = insertNewSteps[0];
            
             String reqParamString = JSON.serialize(reqParam);    
             OB_ShareAllocationDetailController.addShareClass(reqParamString);
        
        	
        test.stopTest();
    }   

}