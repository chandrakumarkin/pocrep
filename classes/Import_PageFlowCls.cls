/*
    Author      : Durga Prasad
    Date        : 06-Oct-2014
    Description : This is an fetches the Page Flow Process records from another Org.

*/
public with sharing class Import_PageFlowCls {
	public static boolean isApexTest = false;//Variable used for CodeCoverage
    
    public string username{get;set;}
    public string password{get;set;}
    
    public string clientId = '';
    public string ConsumerKey = '';
    public string RestClsURL = '';
    public string accsToken = '';
    
    public string strTemplateName{get;set;}
    public FlowProcessWrap objProcess{get;set;}
    
    public boolean showPnl{get;set;}
    
    map<string,string> mapEmail_Sms_Templates = new map<string,string>();
    map<string,string> map_QueueName_Id = new map<string,string>();//to store the Queue Ids for Assigning the SR-Step Owner Ids
    public list<selectOption> lstSelectOption{get;set;}
    public string selSandBox{get;set;}
    
    public map<id,Process_Migration_Setup__c> mapLoginDetailsCS = new map<id,Process_Migration_Setup__c>();
    public Process_Migration_Setup__c objCS = new Process_Migration_Setup__c();
    public Import_PageFlowCls(){
        strTemplateName = '';
        username = '';
        password = '';
        showPnl = false;
        selSandBox = '';
        
        lstSelectOption = new list<selectoption>();
        lstSelectOption.add(new selectoption('','--None--'));
        
        for(EmailTemplate et:[select id,Name,Folder.Name from EmailTemplate limit 50000]){
            mapEmail_Sms_Templates.put(et.Name,et.Id);
        }
        // to verify whether queue is already available in destin. prepare this map
        for(QueueSobject objQueue:[Select Queue.Name,Queue.DeveloperName,SobjectType,QueueId From QueueSobject where sObjectType='SR_Steps__c']){
            map_QueueName_Id.put(objQueue.Queue.DeveloperName,objQueue.QueueId);
        }
        // get the user name password for connecting to source org from custom setting Process_Migration_Setup__c
        string UserName = '';
        string Password = '';
        string SecurityToken = '';
        for(Process_Migration_Setup__c Mig:[select Id,Name,Production_Environment__c,User_Name__c,Password__c,Security_Token__c,Instance_URL__c from Process_Migration_Setup__c where User_Name__c!=null and Password__c!=null]){
            UserName = Mig.User_Name__c;
            Password = Mig.Password__c;
            SecurityToken = Mig.Security_Token__c;
            lstSelectOption.add(new selectoption(Mig.Id,Mig.Name));
            mapLoginDetailsCS.put(Mig.Id,Mig);
        }
        objProcess = new FlowProcessWrap();
    }
    
    public void FetchAccesToken(){
    	if(selSandBox!=null && mapLoginDetailsCS.get(selSandBox)!=null){
    		objCS = mapLoginDetailsCS.get(selSandBox);
    		ConnectionError = '';
    		try{
            	accsToken = AcessToken(objCS.User_Name__c,objCS.Password__c,objCS.Security_Token__c,objCS.Production_Environment__c);
    		}catch(Exception e){
    			ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,string.valueOf(e.getMessage())));
    		}
            if(accsToken!=null && accsToken!=''){
            	system.debug('accsToken==>'+accsToken);
            	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Confirm,'Connection created Successfully, Please provide the SR RecordType API Name to proceed.'));
            }else{
            	if(ConnectionError!=null && ConnectionError!=''){
            		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'[ Unable to Connect '+objCS.Name+' ] : '+ConnectionError));
            	}else{
            		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Failed to create an Connection for '+objCS.Name+', Please try again.'));
            	}
            }
    	}
    }
    
    // click of fetch process button
    public void ImportProcess(){
        RestClsURL = objCS.Instance_URL__c+'/services/apexrest/Fetch_Flow_Process/GetFlowProcess';
        system.debug('RestClsURL===>'+RestClsURL);
        boolean templateExists = false;
        if(strTemplateName!=null && strTemplateName!=''){
            list<SR_Template__c> lstSRTemplate = [select id from SR_Template__c where SR_RecordType_API_Name__c=:strTemplateName];
            if(lstSRTemplate!=null && lstSRTemplate.size()>0){
                //templateExists = true;
            }
        }
        if(templateExists==true){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'SR-Template already exists in this Org. Please delete the SR-Template and related Child Records to process.'));
        }else if(templateExists==false && strTemplateName!=null && strTemplateName!=''){
            string postData='{\"SR_RecordType_API_Name\": \"'+strTemplateName+'\"}';
            system.debug('accsToken===========>'+accsToken);
            JSONParser parser;
            string responseString;
            HttpRequest req = new HttpRequest();
            req.setEndpoint(RestClsURL);
            req.setMethod('POST');
            req.setHeader('Authorization', 'OAuth ' + accsToken);
            req.setHeader('content-type','application/json');
            req.setBody(postData);
            Http http = new Http();
            if (!isApexTest){
                HTTPResponse res = http.send(req);
                responseString = string.valueOf(res.getBody());
                System.debug('responseString===>'+responseString);
            }else{
                responseString = '{"PageFlow":{"attributes":{"type":"Page_Flow__c","url":"/services/data/v29.0/sobjects/Page_Flow__c/a1011000000WETnAAO"},"Name":"Client Information","Record_Type_API_Name__c":"Client_Information","Id":"a1011000000WETnAAO","Master_Object__c":"Service_Request__c"},"lstSections":[{"attributes":{"type":"Section__c","url":"/services/data/v29.0/sobjects/Section__c/a0a11000001S5nnAAC"},"Name":"Client Information Buttons","Page__c":"a0Z110000014BCmEAM","Migration_Rec_Id__c":"a1C11000000oKInEAM","Id":"a0a11000001S5nnAAC","Section_Type__c":"CommandButtonSection","Default_Rendering__c":"true","Order__c":1,"Layout__c":"1"},{"attributes":{"type":"Section__c","url":"/services/data/v29.0/sobjects/Section__c/a0a11000001S5noAAC"},"Name":"Review & Finalize button Navigation Section - Client Information","Page__c":"a0Z110000014BCnEAM","Migration_Rec_Id__c":"a1C11000000oKIiEAM","Id":"a0a11000001S5noAAC","Section_Type__c":"CommandButtonSection","Default_Rendering__c":"true","Section_Title__c":"Review & Finalise","Order__c":1,"Layout__c":"1"},{"attributes":{"type":"Section__c","url":"/services/data/v29.0/sobjects/Section__c/a0a11000001S5npAAC"},"Name":"Request Details Buttons - Client Information","Page__c":"a0Z110000014BClEAM","Migration_Rec_Id__c":"a1C11000000oKIYEA2","Id":"a0a11000001S5npAAC","Section_Type__c":"CommandButtonSection","Default_Rendering__c":"true","Order__c":1,"Layout__c":"1"},{"attributes":{"type":"Section__c","url":"/services/data/v29.0/sobjects/Section__c/a0a11000001S5nqAAC"},"Name":"Customer Correspondence Details - Client Information","Page__c":"a0Z110000014BClEAM","Migration_Rec_Id__c":"a1C11000000oKIdEAM","Id":"a0a11000001S5nqAAC","Section_Type__c":"PageBlockSection","Default_Rendering__c":"true","Section_Title__c":"Correspondence Details","Order__c":2,"Layout__c":"1"}],"lstSecRules":[],"lstSecDetails":[{"attributes":{"type":"Section_Detail__c","url":"/services/data/v29.0/sobjects/Section_Detail__c/a0c11000000mV1WAAU"},"Cancel_Request__c":"false","hasOnChange__c":"false","Render_By_Default__c":"true","Order__c":1,"Component_Type__c":"Command Button","Name":"Buttons","Button_Location__c":"Top","Migration_Rec_Id__c":"a1D11000008AZEwEAO","Navigation_Directions__c":"Backward","Section__c":"a0a11000001S5nnAAC","Mark_it_as_Required__c":"false","Id":"a0c11000000mV1WAAU","Ignore_Required_Fields__c":"false","Button_Position__c":"Center","Component_Label__c":"Previous","Navigation_Direction__c":"Backward","Commit_the_Record__c":false},{"attributes":{"type":"Section_Detail__c","url":"/services/data/v29.0/sobjects/Section_Detail__c/a0c11000000mV1ZAAU"},"Cancel_Request__c":"false","hasOnChange__c":"false","Render_By_Default__c":"true","Order__c":1,"Component_Type__c":"Command Button","Name":"Buttons","Migration_Rec_Id__c":"a1D11000008AZENEA4","Section__c":"a0a11000001S5npAAC","Mark_it_as_Required__c":"false","Id":"a0c11000000mV1ZAAU","Ignore_Required_Fields__c":"false","Button_Position__c":"Center","Component_Label__c":"Save","Navigation_Direction__c":"Forward","Commit_the_Record__c":true},{"attributes":{"type":"Section_Detail__c","url":"/services/data/v29.0/sobjects/Section_Detail__c/a0c11000000mV1YAAU"},"Cancel_Request__c":"false","hasOnChange__c":"false","Render_By_Default__c":"true","Order__c":1,"Component_Type__c":"Command Button","Name":"Previous","Button_Location__c":"Top","Migration_Rec_Id__c":"a1D11000008AZEmEAO","Navigation_Directions__c":"Backward","Section__c":"a0a11000001S5noAAC","Mark_it_as_Required__c":"false","Id":"a0c11000000mV1YAAU","Ignore_Required_Fields__c":"true","Button_Position__c":"Center","Component_Label__c":"Previous","Navigation_Direction__c":"Backward","Commit_the_Record__c":false},{"attributes":{"type":"Section_Detail__c","url":"/services/data/v29.0/sobjects/Section_Detail__c/a0c11000000mV1XAAU"},"Cancel_Request__c":"false","Field_API_Name__c":"Customer__c","hasOnChange__c":"false","Render_By_Default__c":"true","Order__c":1,"Component_Type__c":"Input Field","Name":"Customer","Migration_Rec_Id__c":"a1D11000008AZEXEA4","Section__c":"a0a11000001S5nqAAC","Mark_it_as_Required__c":"true","Id":"a0c11000000mV1XAAU","Ignore_Required_Fields__c":"false","Component_Label__c":"Customer","Navigation_Direction__c":"Forward","Object_Name__c":"Service_Request__c","Commit_the_Record__c":false},{"attributes":{"type":"Section_Detail__c","url":"/services/data/v29.0/sobjects/Section_Detail__c/a0c11000000mV1aAAE"},"Cancel_Request__c":"false","hasOnChange__c":"false","Render_By_Default__c":"true","Order__c":2,"Component_Type__c":"Command Button","Name":"Buttons","Migration_Rec_Id__c":"a1D11000008AZESEA4","Navigation_Directions__c":"Forward","Section__c":"a0a11000001S5npAAC","Mark_it_as_Required__c":"false","Id":"a0c11000000mV1aAAE","Ignore_Required_Fields__c":"false","Button_Position__c":"Center","Component_Label__c":"Next","Navigation_Direction__c":"Forward","Commit_the_Record__c":false},{"attributes":{"type":"Section_Detail__c","url":"/services/data/v29.0/sobjects/Section_Detail__c/a0c11000000mV1dAAE"},"Cancel_Request__c":"false","hasOnChange__c":"false","Render_By_Default__c":"true","Order__c":2,"Component_Type__c":"Command Button","Name":"Buttons","Button_Location__c":"Top","Migration_Rec_Id__c":"a1D11000008AZF1EAO","Navigation_Directions__c":"Forward","Section__c":"a0a11000001S5nnAAC","Mark_it_as_Required__c":"false","Id":"a0c11000000mV1dAAE","Ignore_Required_Fields__c":"false","Button_Position__c":"Center","Component_Label__c":"Next","Navigation_Direction__c":"Forward","Commit_the_Record__c":false},{"attributes":{"type":"Section_Detail__c","url":"/services/data/v29.0/sobjects/Section_Detail__c/a0c11000000mV1cAAE"},"Cancel_Request__c":"false","Field_API_Name__c":"Email__c","hasOnChange__c":"false","Render_By_Default__c":"true","Order__c":2,"Component_Type__c":"Input Field","Name":"Email","Migration_Rec_Id__c":"a1D11000008AZEcEAO","Section__c":"a0a11000001S5nqAAC","Mark_it_as_Required__c":"true","Id":"a0c11000000mV1cAAE","Ignore_Required_Fields__c":"false","Component_Label__c":"Email","Navigation_Direction__c":"Forward","Object_Name__c":"Service_Request__c","Commit_the_Record__c":false},{"attributes":{"type":"Section_Detail__c","url":"/services/data/v29.0/sobjects/Section_Detail__c/a0c11000000mV1bAAE"},"Cancel_Request__c":"false","hasOnChange__c":"false","Render_By_Default__c":"true","Order__c":2,"Component_Type__c":"Command Button","Name":"Finalise & Proceed","Button_Location__c":"Top","Migration_Rec_Id__c":"a1D11000008AZErEAO","Navigation_Directions__c":"Forward","Section__c":"a0a11000001S5noAAC","Mark_it_as_Required__c":"false","Id":"a0c11000000mV1bAAE","Ignore_Required_Fields__c":"true","Button_Position__c":"Center","Component_Label__c":"Finalise & Proceed","Navigation_Direction__c":"Forward","Commit_the_Record__c":false},{"attributes":{"type":"Section_Detail__c","url":"/services/data/v29.0/sobjects/Section_Detail__c/a0c11000000mVogAAE"},"Cancel_Request__c":"true","hasOnChange__c":"false","Render_By_Default__c":"true","Order__c":3,"Component_Type__c":"Command Button","Name":"Cancel","Button_Location__c":"Top","Section__c":"a0a11000001S5noAAC","Mark_it_as_Required__c":"false","Id":"a0c11000000mVogAAE","Ignore_Required_Fields__c":"false","Button_Position__c":"Center","Component_Label__c":"Cancel SR","Navigation_Direction__c":"Forward","Commit_the_Record__c":false},{"attributes":{"type":"Section_Detail__c","url":"/services/data/v29.0/sobjects/Section_Detail__c/a0c11000000mV1eAAE"},"Cancel_Request__c":"false","Field_API_Name__c":"Send_SMS_To_Mobile__c","hasOnChange__c":"false","Render_By_Default__c":"true","Order__c":3,"Component_Type__c":"Input Field","Name":"Mobile","Migration_Rec_Id__c":"a1D11000008AZEhEAO","Section__c":"a0a11000001S5nqAAC","Mark_it_as_Required__c":"true","Id":"a0c11000000mV1eAAE","Ignore_Required_Fields__c":"false","Component_Label__c":"Mobile","Navigation_Direction__c":"Forward","Object_Name__c":"Service_Request__c","Commit_the_Record__c":false}],"lstSecDeatilRules":[{"attributes":{"type":"Page_Navigation_Rule__c","url":"/services/data/v29.0/sobjects/Page_Navigation_Rule__c/a1111000000nVHwAAM"},"Name":"PNR-000329","Migration_Rec_Id__c":"a1J110000008COYEA2","Rule_Name__c":"Next Button Navigation","Section_Detail__c":"a0c11000000mV1dAAE","Rule_Condition__c":"1","Id":"a1111000000nVHwAAM","Sys_Page_Flow_Id__c":"a1011000000WETn","Rule_Text_Condition__c":"Service_Request__c->Id#!=#null"},{"attributes":{"type":"Page_Navigation_Rule__c","url":"/services/data/v29.0/sobjects/Page_Navigation_Rule__c/a1111000000nVHxAAM"},"Name":"PNR-000330","Migration_Rec_Id__c":"a1J110000008COTEA2","Rule_Name__c":"Previous Page","Section_Detail__c":"a0c11000000mV1WAAU","Rule_Condition__c":"1","Id":"a1111000000nVHxAAM","Sys_Page_Flow_Id__c":"a1011000000WETn","Rule_Text_Condition__c":"Service_Request__c->Id#!=#null"}],"lstRuleConds":[{"attributes":{"type":"Page_Flow_Condition__c","url":"/services/data/v29.0/sobjects/Page_Flow_Condition__c/a0z11000000okKJAAY"},"Name":"Cond-000975","Page_Navigation_Rule__c":"a1111000000nVHxAAM","S_No__c":1,"Operator__c":"!=","Condition_Type__c":"Button Action","Field_Name__c":"Id","Id":"a0z11000000okKJAAY","Value__c":"null","Flow_Id__c":"a1011000000WETn","Object_Name__c":"Service_Request__c"},{"attributes":{"type":"Page_Flow_Condition__c","url":"/services/data/v29.0/sobjects/Page_Flow_Condition__c/a0z11000000okKKAAY"},"Name":"Cond-000976","Page_Navigation_Rule__c":"a1111000000nVHwAAM","S_No__c":1,"Operator__c":"!=","Condition_Type__c":"Button Action","Field_Name__c":"Id","Id":"a0z11000000okKKAAY","Value__c":"null","Flow_Id__c":"a1011000000WETn","Object_Name__c":"Service_Request__c"}],"lstRuleActions":[{"attributes":{"type":"Page_Flow_Action__c","url":"/services/data/v29.0/sobjects/Page_Flow_Action__c/a0y11000000qgxhAAA"},"Name":"Actn-000415","Page_Navigation_Rule__c":"a1111000000nVHxAAM","Page__c":"a0Z110000014BClEAM","S_No__c":1,"Is_Custom_Component__c":"false","Id":"a0y11000000qgxhAAA"},{"attributes":{"type":"Page_Flow_Action__c","url":"/services/data/v29.0/sobjects/Page_Flow_Action__c/a0y11000000qgxiAAA"},"Name":"Actn-000416","Page_Navigation_Rule__c":"a1111000000nVHwAAM","Page__c":"a0Z110000014BCnEAM","S_No__c":1,"Id":"a0y11000000qgxiAAA"}],"lstPgRules":[],"lstPages":[{"attributes":{"type":"Page__c","url":"/services/data/v29.0/sobjects/Page__c/a0Z110000014BClEAM"},"Name":"Request Details - Client Information","Page_Order__c":1,"Migration_Rec_Id__c":"a1B11000003pS7xEAE","No_Quick_navigation__c":"false","Page_Flow__c":"a1011000000WETnAAO","What_Id__c":"Service Request","Is_Custom_Component__c":"false","Id":"a0Z110000014BClEAM","Render_By_Default__c":"true","Page_Description__c":"This request is to ensure that DIFC communications are received by the relevant persons in the company. In this request you can update the details of the management, general communications, and IT. Just click on each of the sections shown on the left hand panel, complete it, save it, and press on Next. On Review and Finalize, make sure that you are okay with all the details of the completed sections and click on Finalize & Proceed. In this step, you can also cancel the request, and create a new one. Navigate to Submit Request to finalize the submission."},{"attributes":{"type":"Page__c","url":"/services/data/v29.0/sobjects/Page__c/a0Z110000014BCmEAM"},"Name":"Client Information","VF_Page_API_Name__c":"ClientInformation","Page_Order__c":2,"Migration_Rec_Id__c":"a1B11000003pS82EAE","No_Quick_navigation__c":"false","Page_Flow__c":"a1011000000WETnAAO","What_Id__c":"Service_Request__c","Id":"a0Z110000014BCmEAM","Render_By_Default__c":"true","Page_Description__c":"Client Information"},{"attributes":{"type":"Page__c","url":"/services/data/v29.0/sobjects/Page__c/a0Z110000014BCnEAM"},"Name":"Review & Finalize - Client Information","VF_Page_API_Name__c":"Review_Finalise","Page_Order__c":3,"Migration_Rec_Id__c":"a1B11000003pS87EAE","No_Quick_navigation__c":"false","Page_Flow__c":"a1011000000WETnAAO","What_Id__c":"Service_Request__c","Id":"a0Z110000014BCnEAM","Render_By_Default__c":"true","Page_Description__c":"Client Information"},{"attributes":{"type":"Page__c","url":"/services/data/v29.0/sobjects/Page__c/a0Z110000014BCoEAM"},"Name":"Submit Request - Client Information","VF_Page_API_Name__c":"SRSubmissionPage","Page_Order__c":4,"Migration_Rec_Id__c":"a1B11000003pS8CEAU","No_Quick_navigation__c":"true","Page_Flow__c":"a1011000000WETnAAO","What_Id__c":"Service_Request__c","Id":"a0Z110000014BCoEAM","Render_By_Default__c":"true","Page_Description__c":"Confirm the Service request."}],"lstNavigRules":[],"lstBtnActions":[{"attributes":{"type":"Page_Flow_Action__c","url":"/services/data/v29.0/sobjects/Page_Flow_Action__c/a0y11000000qgxjAAA"},"Name":"Actn-000417","Page__c":"a0Z110000014BCmEAM","Section_Detail__c":"a0c11000000mV1aAAE","Id":"a0y11000000qgxjAAA"},{"attributes":{"type":"Page_Flow_Action__c","url":"/services/data/v29.0/sobjects/Page_Flow_Action__c/a0y11000000qgxkAAA"},"Name":"Actn-000418","Page__c":"a0Z110000014BCnEAM","Section_Detail__c":"a0c11000000mV1dAAE","Id":"a0y11000000qgxkAAA"},{"attributes":{"type":"Page_Flow_Action__c","url":"/services/data/v29.0/sobjects/Page_Flow_Action__c/a0y11000000qgxlAAA"},"Name":"Actn-000419","Page__c":"a0Z110000014BCmEAM","Section_Detail__c":"a0c11000000mV1YAAU","Id":"a0y11000000qgxlAAA"},{"attributes":{"type":"Page_Flow_Action__c","url":"/services/data/v29.0/sobjects/Page_Flow_Action__c/a0y11000000qgxmAAA"},"Name":"Actn-000420","Page__c":"a0Z110000014BCoEAM","Section_Detail__c":"a0c11000000mV1bAAE","Id":"a0y11000000qgxmAAA"}]}';
            }
            objProcess = new FlowProcessWrap();
            if(responseString!=null && responseString!=''){
                objProcess = (FlowProcessWrap)JSON.deserialize(responseString,FlowProcessWrap.class);
            }
            if(objProcess!=null && objProcess.PageFlow!=null && objProcess.PageFlow.Id!=null){
                showPnl = true;
            }else{
            	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Info,'SR Template not found in '+objCS.Name +' environment with "'+strTemplateName+'" Record type API Name'));
            }
        }
    }
    
    public string strNewSRTemplId{get;set;}
    /*
        Method Name : ConfirmSave
        Description : This Method will saves the entire process into this Org.
    */
    public pagereference ConfirmSave(){
        if(objProcess!=null && objProcess.PageFlow!=null && objProcess.PageFlow.Id!=null){
        	/* Start of Preparing the Save Point */
            Savepoint Stat_svpoint = Database.setSavepoint();
            /* End of Preparing the Save Point */
            
            Page_Flow__c objFlow = new Page_Flow__c();
            objFlow = objProcess.PageFlow;
            objFlow.Id = null;
            objFlow.SR_Template__c = null;
            try{
            	insert objFlow;
            }catch(Exception e){
            	Database.rollback(Stat_svpoint);
                system.debug('===>'+e.getMessage());
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
                return null;
            }
            
            map<string,string> MapPageRecs = new map<string,string>();
            map<string,string> MapSecRecs = new map<string,string>();
            map<string,string> MapSecDetailRecs = new map<string,string>();
            map<string,string> MapNavigRuleRecs = new map<string,string>();
            
            if(objProcess.lstPages!=null && objProcess.lstPages.size()>0){
            	list<Page__c> lstPages_Insert = new list<Page__c>();
            	Page__c pgInsert;
            	for(Page__c objPg:objProcess.lstPages){
            		system.debug('objPg.Id==>'+objPg.Id);
            		system.debug('objPg==>'+objPg);
            		string strMigId = objPg.Id;
            		pgInsert = new Page__c();
            		pgInsert = objPg;
            		pgInsert.Id = null;
            		pgInsert.Migration_Rec_Id__c = strMigId;
            		system.debug('pgInsert.Migration_Rec_Id__c==>'+pgInsert.Migration_Rec_Id__c);
            		pgInsert.Page_Flow__c = objFlow.Id;
            		lstPages_Insert.add(pgInsert);
            	}
            	try{
					insert lstPages_Insert;
					for(Page__c pg:lstPages_Insert){
						MapPageRecs.put(pg.Migration_Rec_Id__c,pg.Id);
					}
            	}catch(Exception e){
            		Database.rollback(Stat_svpoint);
	                system.debug('===>'+e.getMessage());
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
	                return null;
            	}
            }
            system.debug('MapPageRecs==>'+MapPageRecs);
            if(objProcess.lstSections!=null && objProcess.lstSections.size()>0){
            	list<Section__c> lstSec_Insert = new list<Section__c>();
            	Section__c SecInsert;
            	for(Section__c objSec:objProcess.lstSections){
            		string strMigId = objSec.Id;
            		SecInsert = new Section__c();
            		SecInsert = objSec;
            		SecInsert.Id = null;
            		system.debug('objSec.Page__c==>'+objSec.Page__c);
            		system.debug('Mpa get==>'+MapPageRecs.get(objSec.Page__c));
            		SecInsert.Page__c = MapPageRecs.get(objSec.Page__c);
            		SecInsert.Migration_Rec_Id__c = strMigId;
            		lstSec_Insert.add(SecInsert);
            	}
            	try{
            		insert lstSec_Insert;
            		for(Section__c sec:lstSec_Insert){
						MapSecRecs.put(sec.Migration_Rec_Id__c,sec.Id);
					}
            	}catch(Exception e){
            		Database.rollback(Stat_svpoint);
	                system.debug('===>'+e.getMessage());
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
	                return null;
            	}
            }
            
            if(objProcess.lstSecDetails!=null && objProcess.lstSecDetails.size()>0){
            	list<Section_Detail__c> lstSecDet_Insert = new list<Section_Detail__c>();
            	Section_Detail__c SecDetInsert;
            	for(Section_Detail__c objSecDetail:objProcess.lstSecDetails){
            		string strMigId = objSecDetail.Id;
            		SecDetInsert = new Section_Detail__c();
            		SecDetInsert = objSecDetail;
            		SecDetInsert.Id = null;
            		SecDetInsert.Section__c = MapSecRecs.get(objSecDetail.Section__c);
            		SecDetInsert.Migration_Rec_Id__c = strMigId;
            		lstSecDet_Insert.add(SecDetInsert);
            	}
            	try{
            		insert lstSecDet_Insert;
            		for(Section_Detail__c secDet:lstSecDet_Insert){
						MapSecDetailRecs.put(secDet.Migration_Rec_Id__c,secDet.Id);
					}
            	}catch(Exception e){
            		Database.rollback(Stat_svpoint);
	                system.debug('===>'+e.getMessage());
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
	                return null;
            	}
            }
            
            if(objProcess.lstPgRules!=null && objProcess.lstPgRules.size()>0){
            	list<Page_Navigation_Rule__c> lstPgNavig_Insert = new list<Page_Navigation_Rule__c>();
            	Page_Navigation_Rule__c PgNavIns;
            	for(Page_Navigation_Rule__c pgrule:objProcess.lstPgRules){
            		string strMigId = pgrule.Id;
            		PgNavIns = new Page_Navigation_Rule__c();
            		PgNavIns = pgrule;
            		PgNavIns.Id = null;
            		PgNavIns.Page__c = MapPageRecs.get(pgrule.Page__c);
            		PgNavIns.Migration_Rec_Id__c = strMigId;
            		lstPgNavig_Insert.add(PgNavIns);
            	}
            	try{
            		insert lstPgNavig_Insert;
            		for(Page_Navigation_Rule__c pgrule:lstPgNavig_Insert){
						MapNavigRuleRecs.put(pgrule.Migration_Rec_Id__c,pgrule.Id);
					}
            	}catch(Exception e){
            		Database.rollback(Stat_svpoint);
	                system.debug('===>'+e.getMessage());
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
	                return null;
            	}
            }
            
            if(objProcess.lstSecRules!=null && objProcess.lstSecRules.size()>0){
            	list<Page_Navigation_Rule__c> lstSecRules_Insert = new list<Page_Navigation_Rule__c>();
            	Page_Navigation_Rule__c SecRule;
            	for(Page_Navigation_Rule__c sectRule:objProcess.lstSecRules){
            		string strMigId = sectRule.Id;
            		SecRule = new Page_Navigation_Rule__c();
            		SecRule = sectRule;
            		SecRule.Id = null;
            		SecRule.Section__c = MapSecRecs.get(sectRule.Section__c);
            		SecRule.Migration_Rec_Id__c = strMigId;
            		lstSecRules_Insert.add(SecRule);
            	}
            	try{
            		insert lstSecRules_Insert;
            		for(Page_Navigation_Rule__c sectionRule:lstSecRules_Insert){
						MapNavigRuleRecs.put(sectionRule.Migration_Rec_Id__c,sectionRule.Id);
					}
            	}catch(Exception e){
            		Database.rollback(Stat_svpoint);
	                system.debug('===>'+e.getMessage());
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
	                return null;
            	}
            }
            
            if(objProcess.lstSecDeatilRules!=null && objProcess.lstSecDeatilRules.size()>0){
            	list<Page_Navigation_Rule__c> lstSecDetRules_Insert = new list<Page_Navigation_Rule__c>();
            	Page_Navigation_Rule__c SecDetlsRule;
            	for(Page_Navigation_Rule__c secDetRule:objProcess.lstSecDeatilRules){
            		string strMigId = secDetRule.Id;
            		SecDetlsRule = new Page_Navigation_Rule__c();
            		SecDetlsRule = secDetRule;
            		SecDetlsRule.Id = null;
            		SecDetlsRule.Section_Detail__c = MapSecDetailRecs.get(secDetRule.Section_Detail__c);
            		SecDetlsRule.Migration_Rec_Id__c = strMigId;
            		lstSecDetRules_Insert.add(SecDetlsRule);
            	}
            	try{
            		insert lstSecDetRules_Insert;
            		for(Page_Navigation_Rule__c sectionDetRule:lstSecDetRules_Insert){
						MapNavigRuleRecs.put(sectionDetRule.Migration_Rec_Id__c,sectionDetRule.Id);
					}
            	}catch(Exception e){
            		Database.rollback(Stat_svpoint);
	                system.debug('===>'+e.getMessage());
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
	                return null;
            	}
            }
            
            if(objProcess.lstRuleConds!=null && objProcess.lstRuleConds.size()>0){
            	list<Page_Flow_Condition__c> lstRuleCond_Insert = new list<Page_Flow_Condition__c>();
            	Page_Flow_Condition__c objRuleCond;
            	for(Page_Flow_Condition__c RuleCnd:objProcess.lstRuleConds){
            		objRuleCond = new Page_Flow_Condition__c();
            		objRuleCond = RuleCnd;
            		objRuleCond.Id = null;
            		objRuleCond.Page_Navigation_Rule__c = MapNavigRuleRecs.get(RuleCnd.Page_Navigation_Rule__c);
            		lstRuleCond_Insert.add(objRuleCond);
            	}
            	try{
            		insert lstRuleCond_Insert;
            	}catch(Exception e){
            		Database.rollback(Stat_svpoint);
	                system.debug('===>'+e.getMessage());
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
	                return null;
            	}
            }
            
            if(objProcess.lstRuleActions!=null && objProcess.lstRuleActions.size()>0){
            	list<Page_Flow_Action__c> lstRuleAct_Insert = new list<Page_Flow_Action__c>();
            	Page_Flow_Action__c objRuleAct;
            	for(Page_Flow_Action__c RuleActn:objProcess.lstRuleActions){
            		objRuleAct = new Page_Flow_Action__c();
            		objRuleAct = RuleActn;
            		objRuleAct.Id = null;
            		objRuleAct.Page__c = MapPageRecs.get(RuleActn.Page__c);
            		objRuleAct.Page_Navigation_Rule__c = MapNavigRuleRecs.get(RuleActn.Page_Navigation_Rule__c);
            		lstRuleAct_Insert.add(objRuleAct);
            	}
            	try{
            		insert lstRuleAct_Insert;
            	}catch(Exception e){
            		Database.rollback(Stat_svpoint);
	                system.debug('===>'+e.getMessage());
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
	                return null;
            	}
            }
            
            if(objProcess.lstBtnActions!=null && objProcess.lstBtnActions.size()>0){
            	list<Page_Flow_Action__c> lstBtnAct_Insert = new list<Page_Flow_Action__c>();
            	Page_Flow_Action__c objBtnAct;
            	for(Page_Flow_Action__c BtnActn:objProcess.lstBtnActions){
            		objBtnAct = new Page_Flow_Action__c();
            		objBtnAct = BtnActn;
            		objBtnAct.Id = null;
            		objBtnAct.Page__c = MapPageRecs.get(BtnActn.Page__c);
            		objBtnAct.Section_Detail__c = MapSecDetailRecs.get(BtnActn.Section_Detail__c);
            		lstBtnAct_Insert.add(objBtnAct);
            	}
            	try{
            		insert lstBtnAct_Insert;
            	}catch(Exception e){
            		Database.rollback(Stat_svpoint);
	                system.debug('===>'+e.getMessage());
	                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,e.getMessage()));
	                return null;
            	}
            }
            
        }
        return null;
    }
    
    
    public string ConnectionError;
    /*
        Method Name : AcessToken
        Description : This Method Will Return the AccessToken by connecting with User Defined SFDC Org 
    */
    public string AcessToken(string userName,string Password,string SecurityToken,boolean isProd){
        system.debug('userName==>'+userName);
        system.debug('Password==>'+Password);
        string strInstance = 'test';
        if(isProd==true)
        	strInstance = 'login';
        system.debug('SecurityToken==>'+SecurityToken);
        string clientId = '3MVG98RqVesxRgQ6tItpjav2ozTf5.kqz3r5XRtGekSeU_hjbK7NoBwtN5G1Xi1PVWEfQXt9wV0.qKTX0OWmC';
        string ConsumerKey = '8890259184560227623';
        string URI = 'https://'+strInstance+'.salesforce.com/services/oauth2/token?grant_type=password&' + 'client_id=' + clientId + '&' + 'client_secret=' + ConsumerKey + '&' + 'username=' + username + '&' + 'password=' + password;
        JSONParser parser1;
        accsToken = '';
        HttpRequest req = new HttpRequest();
        req.setEndpoint(URI);
        req.setMethod('POST');
        Http http = new Http();
        if(!isApexTest){
            HTTPResponse res = http.send(req);
            accsToken = '';
            system.debug('AccessToken==>'+res.getBody());
            parser1 = JSON.createParser(res.getBody());
        }else{
             String result = '{"id":"https://test.salesforce.com/id/00Dg0000003L0MREA0/005g0000000vZSKAA2","issued_at":"1398932139767","token_type":"Bearer","instance_url":"https://cs17.salesforce.com","signature":"oQ7eOe8SIHMoqZw6XTX7qsoalf43WPMfJG9foXDhph8=","access_token":"00Dg0000003L0MR!AQUAQPMYBxWQE7EQj8U6q17.6IHneCQ0Fj.N2Reek.i5xA8Z.UFol_.v7iypTySMU1nKaEg0tciXmKyCwCGFPdZtuk_M5dQr"}';
             parser1 = JSON.createParser(result);
             return result;
        }
        if(parser1!=null){
            while(parser1.nextToken() != null) {
                if ((parser1.getCurrentToken() == JSONToken.FIELD_NAME) && (parser1.getText() == 'access_token')) {
                    system.debug('accsToken before nextToken'+parser1.getText());
                    parser1.nextToken();
                    system.debug('accsToken After nextToken'+parser1.getText());
                    accsToken = parser1.getText();
                }
                if ((parser1.getCurrentToken() == JSONToken.FIELD_NAME) && (parser1.getText() == 'error_description')) {
                    system.debug('error_description before nextToken'+parser1.getText());
                    parser1.nextToken();
                    system.debug('error_description After nextToken'+parser1.getText());
                    ConnectionError = parser1.getText();
                }
            }
        }
        system.debug('ConnectionError===>'+ConnectionError);
        return accsToken;
    }
    
    public class FlowProcessWrap{
    	public Page_Flow__c PageFlow{get;set;}
    	public list<Page__c> lstPages{get;set;}
    	public list<Section__c> lstSections{get;set;}
    	public list<Section_Detail__c> lstSecDetails{get;set;}
    	
    	public list<Page_Navigation_Rule__c> lstPgRules{get;set;}
    	public list<Page_Navigation_Rule__c> lstSecRules{get;set;}
    	public list<Page_Navigation_Rule__c> lstSecDeatilRules{get;set;}
    	public list<Page_Navigation_Rule__c> lstNavigRules{get;set;}
    	
    	public list<Page_Flow_Condition__c> lstRuleConds{get;set;}
    	
    	public list<Page_Flow_Action__c> lstRuleActions{get;set;}
    	public list<Page_Flow_Action__c> lstBtnActions{get;set;}
    	
    	public FlowProcessWrap(){
    		PageFlow = new Page_Flow__c();
    		lstPages = new list<Page__c>();
    		lstSections = new list<Section__c>();
    		lstSecDetails = new list<Section_Detail__c>();
    		
    		lstPgRules = new list<Page_Navigation_Rule__c>();
    		lstSecRules = new list<Page_Navigation_Rule__c>();
    		lstSecDeatilRules = new list<Page_Navigation_Rule__c>();
    		lstNavigRules = new list<Page_Navigation_Rule__c>();
    		
    		lstRuleConds = new list<Page_Flow_Condition__c>();
    		
    		lstRuleActions = new list<Page_Flow_Action__c>();
    		lstBtnActions = new list<Page_Flow_Action__c>();
    		
    	}
    }
}