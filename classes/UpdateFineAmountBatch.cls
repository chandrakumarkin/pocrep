/*********************************************************************************************************************
*  Name     : UpdateFineAmountBatch 
*  Author   : Shoaib Tariq
*  Purpose  : This class used to update fine amount calculation
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
 V1.0          Shoaib             07/05/20
**/
global class UpdateFineAmountBatch implements Database.Batchable<sObject>,Schedulable{

    global Database.QueryLocator start(Database.BatchableContext bc) {
       return Database.getQueryLocator(
            'SELECT Id,CreatedDate FROM Service_Request__c WHERE External_Status_Code__c = \'Draft\' and Service_Type__c = \'Establishment Card Application\'');
    }
        
    global void execute(Database.BatchableContext bc, List<Service_Request__c> records){
        Set<Id> srIds                        = new  Set<Id>();
        Map<Id,Service_Request__c> srMap     = new Map<Id,Service_Request__c>();
        List<SR_Price_Item__c> priceItemList = new List<SR_Price_Item__c> ();
        
        for(Service_Request__c thisSR : records){
            srIds.add(thisSR.Id);
            srMap.put(thisSR.Id, thisSR);
        }
        
        for(SR_Price_Item__c thisItem : [SELECT Id,
                                                Price__c,
                                                ServiceRequest__c
                                         		FROM SR_Price_Item__c 
                                         		WHERE ServiceRequest__c IN:srIds 
                                         AND Pricing_Line__r.Material_Code__c = 'GO_DNRD_Fine']){
              
         if(srMap.containsKey(thisItem.ServiceRequest__c) 
            			&& Date.valueOf(srMap.get(thisItem.ServiceRequest__c).CreatedDate).daysBetween(System.today())>30){
             
             Integer count = (Date.valueOf(srMap.get(thisItem.ServiceRequest__c).CreatedDate).daysBetween(System.today())/30) + 1;
             thisItem.Price__c = 100 * count;
             priceItemList.add(thisItem);
                                                        
           }                             
                                             
       }
        if(!priceItemList.isEmpty()){
            update priceItemList;
        }
      }
        
    global void finish(Database.BatchableContext bc){
        
    }
    
    global void execute(SchedulableContext sc) {
        UpdateFineAmountBatch b = new UpdateFineAmountBatch();
        database.executebatch(b,200);
   }

}