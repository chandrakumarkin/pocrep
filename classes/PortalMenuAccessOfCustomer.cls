/**
    *Author :
    *Description : 
    *
*/

public without sharing class PortalMenuAccessOfCustomer{
    
   @AuraEnabled public static List<PortalMenuWrapper> fetchPortalAccessList(String customerId){
    
        List<PortalMenuWrapper> portalWrapperList = new List<PortalMenuWrapper>();
        Map<string,Portal_Menu_Access_Of_Customer__c> portalMenuMap= new Map<string,Portal_Menu_Access_Of_Customer__c>();
        
        Set<string> activeSpecificSRs = new Set<string>();
        
        for(Portal_Specific_Customer_SR_Types__c pc : Portal_Specific_Customer_SR_Types__c.getall().values()){
            if(pc.Is_Active__c){
                activeSpecificSRs.add(pc.SR_Record_Type_API_Name__c);   
            }
        }
        
        List<SR_Template__c> srTempList = [
            select Id,SR_RecordType_API_Name__c,Menutext__c,Menu__c 
            from SR_Template__c 
            where SR_RecordType_API_Name__c IN:activeSpecificSRs 
            order by Menutext__c asc
        ];
        
        
        
        List<Account> customerList = [
            select Id,Name from Account where Id=:customerId limit 1
        ];
        
        if(srTempList.size() >0 && customerList.size() >0){
            
            List<Portal_Menu_Access_Of_Customer__c> portalMenuConfiguredList = [
                select Id,Customer__c,Active__c,SR_Template__c,SR_Template__r.SR_RecordType_API_Name__c
                from Portal_Menu_Access_Of_Customer__c
                where SR_Template__c IN:srTempList and Customer__c=:customerId
            ];
            
            for(Portal_Menu_Access_Of_Customer__c port : portalMenuConfiguredList){
                portalMenuMap.put(port.SR_Template__r.SR_RecordType_API_Name__c,port);
            }
            
            for(SR_Template__c sr : srTempList ){
                
                PortalMenuWrapper portal = new PortalMenuWrapper();
                portal.portalMenu = new Portal_Menu_Access_Of_Customer__c(Customer__c=customerId,SR_Template__c=sr.Id,Active__c = false);
                
                if(sr.SR_RecordType_API_Name__c != null &&
                    portalMenuMap.containskey(sr.SR_RecordType_API_Name__c) && 
                    portalMenuMap.get(sr.SR_RecordType_API_Name__c) != null
                ){
                    portal.portalMenu.Active__c = portalMenuMap.get(sr.SR_RecordType_API_Name__c).Active__c;
                    portal.portalMenu.Id= portalMenuMap.get(sr.SR_RecordType_API_Name__c).Id;
                }
                
                portal.customer = (customerList.size() > 0) ? customerList[0] :new account();
                portal.srTemplate = sr;
                
                portalWrapperList.add(portal);
            }
        }      
        return portalWrapperList;
    }

    @AuraEnabled
    public static List<PortalMenuWrapper> saveMenuList(List<PortalMenuWrapper> portalMenu,string accId){
        
        List<Portal_Menu_Access_Of_Customer__c> templist = new List<Portal_Menu_Access_Of_Customer__c>();

        if(portalMenu.size() >0){
            for(PortalMenuWrapper ptw : portalMenu){
                templist.add(ptw.portalMenu);
            }
        }
        
        if(templist.size()>0)upsert templist;  
        
        List<PortalMenuWrapper> menuList = fetchPortalAccessList(accId);
        return menuList;
    } 


    public class PortalMenuWrapper {
        
        @AuraEnabled public account customer { get; set; }
        @AuraEnabled public SR_Template__c srTemplate { get; set; }
        @AuraEnabled public Portal_Menu_Access_Of_Customer__c portalMenu { get; set; }
    }
}