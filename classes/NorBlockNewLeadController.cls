public class NorBlockNewLeadController {
	
	public static String finalJSON(String accountID){
		
		Account eachAccount = new Account();
		eachAccount = [SELECT Registration_License_No__c FROM Account where Id =:accountID];
		
		List<TargetOrgs> lstTargetOrgs = new List<TargetOrgs>();
		TargetOrgs targetORG = new TargetOrgs();
		targetORG.orgId = 2004;
		lstTargetOrgs.add(targetORG);
		
		customerInfo cinfo = new customerInfo();
		cinfo.categoryId = 'DIFC'+eachAccount.Registration_License_No__c;
		cinfo.category   = 'corporateCustomer';
		cinfo.countryIso2 = 'AE';
		
		jsonClass jsonvalue = new jsonClass();
		jsonvalue.customerInfo = cinfo;
		jsonvalue.targetOrgs = lstTargetOrgs;
		
		String finalJsonValue = JSON.serialize(jsonvalue); 
    	system.debug('!!@@@'+finalJsonValue);
        return finalJsonValue;
        
	}
	
    public class TargetOrgs {
        public Integer orgId;
    }

	public class jsonClass{
	    public CustomerInfo customerInfo;
	    public List<TargetOrgs> targetOrgs;
	}

    public class CustomerInfo {
        public String categoryId;
        public String category;
        public String countryIso2;
    }

    
    public static NorBlockNewLeadController parse(String json) {
        return (NorBlockNewLeadController) System.JSON.deserialize(json, NorBlockNewLeadController.class);
    }
}