/*
    Author      : Durga Prasad
    Date        : 16-Dec-2019
    Description : Custom code to get the count of Shareholders to evaluate the documents
    ---------------------------------------------------------------------------------------
*/
global without sharing class CC_ShareholderCount implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
    	string strResult = '0';
    	if(SR!=null && SR.Id!=null){
    		list<HexaBPM_Amendment__c> lstShareholders = [Select Id from HexaBPM_Amendment__c where ServiceRequest__c=:SR.Id AND Role__c Includes ('Shareholder')];
    		if(lstShareholders!=null && lstShareholders.size()>0)
    			strResult = lstShareholders.size()+'';
    	}
    	return strResult;
    }
}