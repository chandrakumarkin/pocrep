/**
* Author: Shoaib Tariq
* Date: 10/14/2020
* Description: Batch class to append email fields with .invalid
**/

global class SandboxPostRefresh_Batch implements Database.Batchable<sObject> {

    String query;
    String sObjectName;
    Map <String, List <String>> sObjectEmaiLFieldsMap = new Map <String, List <String>>();

    private SandboxPostRefresh_Batch(Map <String, List <String>> sObjectFieldsMap) {
        sObjectName = new List<String> (sObjectFieldsMap.keySet()).get(0);

        sObjectEmaiLFieldsMap = sObjectFieldsMap;
        query = SandboxPostRefreshHelper.getSOQLString(sObjectName, sObjectEmaiLFieldsMap.get(sObjectName));
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        system.debug('query'+query);
        return Database.getQueryLocator(query);
    }    
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        
        Map<String, Schema.SObjectType> gd                   = Schema.getGlobalDescribe();
        Schema.SObjectType ctype                             = gd.get(sObjectName); 
        Map<String, Schema.SobjectField> fmap                = ctype.getDescribe().fields.getMap();
        
        for(sObject thisRecord : scope) {
                
            for(String thisField : sObjectEmaiLFieldsMap.get(sObjectName)) {
                Schema.SObjectField field             = fmap.get(thisField);
                Schema.DisplayType FldType            = field.getDescribe().getType();
                
                String fieldToUpdate = (String) thisRecord.get(thisField);
                
                if(FldType == Schema.DisplayType.EMAIL ) {
                   thisRecord.put(thisField, fieldToUpdate + 'test@difc.ae.invalid'); 
                }
                if(FldType == Schema.DisplayType.PHONE ) {
                     thisRecord.put(thisField,'+971523830186');   
                }
             }
        }
        Database.update(scope, false); 
    }
    
    global void finish(Database.BatchableContext BC) {
        sObjectEmaiLFieldsMap.remove(sObjectName);
        executeBatch(sObjectEmaiLFieldsMap);
    }

    public static void executeBatch(Map <String, List <String>> sObjectFieldsMap) {
        if(!sObjectFieldsMap.isEmpty()) {
            Database.executeBatch(new SandboxPostRefresh_Batch(sObjectFieldsMap));
        }
    }
}