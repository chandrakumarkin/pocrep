/*
    Author      : Durga Prasad
    Date        : 09-Oct-2019
    Description : Custom code to check the Account has Active Commercial Permit or not

    v1: edited the condition
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_hasActiveCommercialPermit implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'False';
        if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null){
        	for(OB_Event_Contract__c objEC:[Select Id from OB_Event_Contract__c where Account__c=:stp.HexaBPM__SR__r.HexaBPM__Customer__c and Is_Active__c=true]){
        		strResult = 'True';
        	}
        }
		return strResult;   
    }
}