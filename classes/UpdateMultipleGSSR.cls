/*
Manully run in developer conlose 
UpdateMultipleGSSR  batchSch=new UpdateMultipleGSSR();
String sch='0 0 23 * * ?';
System.schedule('Update Multiple Dependent Visa Requests', sch , batchSch);

*/

//Upload Main Sr Status to Closed

global class UpdateMultipleGSSR implements Schedulable 
{
   global void execute(SchedulableContext sc) 
   {
   
    map<string,id> mapOfStatus = new map<string,id>();
       
          for(SR_Status__c obj : [select id,name from SR_Status__c where code__c!=null and name='Process Completed']) mapOfStatus.put(obj.name, obj.id);
          
    set<Id>  MainSrs=new set<id>();
 for(Service_Request__c ObjSr:[select id,Linked_SR__c from Service_Request__c where Completed_Date_Time__c=This_week and Linked_SR__c!=null and SR_Group__c='GS' And
 (Record_Type_Name__c='Non_DIFC_Sponsorship_Visa_Cancellation' Or Record_Type_Name__c='Non_DIFC_Sponsorship_Visa_Renewal' Or Record_Type_Name__c='Non_DIFC_Sponsorship_Visa_New') and Linked_SR__r.Completed_Date_Time__c=null ])
 {
    MainSrs.add(ObjSr.Linked_SR__c); 
 }
 List<Service_Request__c> mainSrUpdate=new List<Service_Request__c>();
 for(Service_Request__c MainSr:[select (select id from Service_Requests1__r where Completed_Date_Time__c=null) from Service_Request__c where External_SR_Status__c!=:mapOfStatus.get('Process Completed') and id in :MainSrs])
 {
     if(MainSr.Service_Requests1__r.size()==0)//no open SR;s
     {
        // Service_Request__c MainSr=new Service_Request__c(Id=StepSR.Linked_SR__c);
         MainSr.External_SR_Status__c= mapOfStatus.get('Process Completed');
         MainSr.Internal_SR_Status__c= mapOfStatus.get('Process Completed');
         MainSr.Completed_Date_Time__c=Datetime.Now();
         mainSrUpdate.add(MainSr);
         
     }
 }
   
   if(!mainSrUpdate.isempty())
        update   mainSrUpdate;

   }
}