@isTest
public class RocDataProtectionPermitVF_edit_Test {
    
    @testsetup static void InitData(){
       /* UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
            UserRoleId = portalRole.Id,
            ProfileId = profile1.Id,
            Username = 'dotnetcodex@gmail.com' + System.now().millisecond() ,
            Alias = 'sfdc',
            Email='dotnetcodex@gmail.com',
            EmailEncodingKey='UTF-8',
            Firstname='Dhanik',
            Lastname='Sahni',
            LanguageLocaleKey='en_US',
            LocaleSidKey='en_US',
            TimeZoneSidKey='America/Chicago'
        );
        Database.insert(portalAccountOwner1);*/
        

        Account act = new Account();
        act.Name = 'Test Custoer 1';
        act.E_mail__c = 'test@test.com';
        act.BP_No__c = '001234';
        act.Company_Type__c = 'Financial - related';
        act.Sector_Classification__c = 'Authorised Market Institution';
        act.Has_Permit_2__c = true;
        act.Has_Permit_3__c = false;
        Database.insert(act);
        
        
        //Create contact
        Contact contact1 = new Contact(
            FirstName = 'Test',
            Lastname = 'PortalUser1',
            AccountId = act.Id,
            Email = 'testportal@portaldifc.com'
        );
        Database.insert(contact1);
        
        
        Profile portalProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community Plus User Custom' Limit 1];
        User user1 = new User(
            UserName = contact1.Email,
            FirstName = contact1.FirstName,
            LastName = contact1.LastName,
            Alias = 'test123',
            email = contact1.Email,
            ContactId = contact1.Id,
            ProfileId = portalProfile.Id,
            EmailEncodingKey = 'UTF-8',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
        
    }

    @isTest static void testMethod1(){
        test.startTest();
        
        User portalUsr = [Select Id,name,ContactId,Account_ID__c From User Where UserName='testportal@portaldifc.com' LIMIT 1];
        System.debug('pUsr->Account_ID__c->'+portalUsr.Account_ID__c);
        System.debug('pUsr->Account_ID__c->'+portalUsr.Account_ID__c);
        System.runAs(portalUsr){
       		 map<string,string> mapRecordTypeIds = new map<string,string>();
        
         for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Notification_of_Personal_Data_Operations','User_Access_Form')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        /*Account objAccount = new Account();
        objAccount.Name = 'Test CustoMMMMer 1';
        objAccount.E_mail__c = 'test23323@test.com';
        objAccount.BP_No__c = '001236';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;*/
        
      
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = portalUsr.Account_ID__c;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        //objSR.RecordTypeId = mapRecordTypeIds.get('Notification_of_Personal_Data_Operations');
        objSR.RecordTypeId = mapRecordTypeIds.get('Notification_of_Personal_Data_Operations');
        objSR.Purpose_of_Notification__c = 'Prior to or immediately upon personal data processing';
        objSR.Transferring_Personal_Data_outside_DIFC__c = true;
        objSR.Processing_any_senstive_data__c = true;
        objSR.In_accordance_with_Article_10_1__c = false;
        objSR.Personal_Data_Processing_Procedure__c = '';
        objSR.Purpose_of_Processing_Data__c = '';
        objSR.Data_Subjects_Personal_Data__c = 'Other';
        objSR.In_Accordance_with_Article_11__c = false;
        objSR.Recognized_Jurisdictions__c = 'Indai;US Dept';
        objSR.In_Accordance_with_Article_12_1bj__c = false;
        objSR.In_Accordance_with_Article_12_1a__c = false;
        objSR.Other_Purpose_of_Processing_Data__c = '';
        objSR.Purpose_of_Notification__c  = 'Prior to or immediately upon personal data processing';
        insert objSR;
        
        
		//Notifications
        Permit__c p = new Permit__c();
        p.Name = 'Personal Data Processing Operations';
        p.Account__c = portalUsr.Account_ID__c;
        p.Date_To__c = System.today() + 1;
        P.Status__c = 'Active';
        insert p;
        p = [Select Id,Name,Date_To__c,Account__c,Active__c from  Permit__c Where Id = :p.Id];
            
        Data_Protection__c  dp = new Data_Protection__c ();
        dp.Permit__c = p.id;
        dp.RecordTypeId = Schema.SObjectType.Data_Protection__c.getRecordTypeInfosByName().get('Jurisdiction').getRecordTypeId();
        dp.Service_Request__c = objSR.Id;
        //dp.Name = 'Test Data Prot';
        insert dp;
            
            
            
            
        Amendment__c objAmendment = new Amendment__c();
        objAmendment.ServiceRequest__c = objSR.Id;
        objAmendment.First_Name__c = 'Test';
        objAmendment.Last_Name__c = 'Last Name';
        objAmendment.Address1__c = '14th Floor';
        objAmendment.Building_Name__c = 'The Gate';
        objAmendment.Address2__c = 'The Gate';
        objAmendment.Address3__c = 'Dubai';
        objAmendment.Emirate_State__c = 'Dubai';
        objAmendment.Country_Of_Issue__c = 'United Arab Emirates';
        //objAmendment.Phone__c = '+9712345678999';
        objAmendment.Fax__c = '+9711234567890';
        //objAmendment.Mobile__c = '+9711234567890';
        objAmendment.Person_Email__c = 'test@test.com';
        objAmendment.Passport_No__c = '123456';
            
        objAmendment.Amendment_Type__c = 'Individual';
        objAmendment.Relationship_Type__c = 'Data Controller';
        objAmendment.Shareholder_Company__c = portalUsr.Account_ID__c;
        insert objAmendment;
            
            
            
        Relationship__c rr = new Relationship__c();
        rr.Active__c  = true;
        rr.Object_Contact__c = portalUsr.ContactId;
        rr.Subject_Account__c = portalUsr.Account_ID__c;
        rr.Relationship_Type__c = 'Data Controller';
        insert rr;
            
        
            
                
            PageReference pageRef = Page.RocDataProtectionPermitVF_new; Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('RecordType','01220000000MbZbAAK');
            pageRef.getParameters().put('index','0');
            pageRef.getParameters().put('ValParam','Understood_the_purpose_of_notification__c');
            pageRef.getParameters().put('myParam','true');
            
        ApexPages.StandardController sc = new ApexPages.StandardController(objSR);    
        RocDataProtectionPermitVF_edit Con = new RocDataProtectionPermitVF_edit(sc); 
        
        Con.getAmendmentController();

        Con.AddAmendment();
         Con.SaveAmendment();
        
        Con.AddJurAmendment();
        Con.getAmendmentJurisdiction();
        Con.SaveJurAmendment();
        Con.getDataControllerRel();
        Con.FillSR();
        Con.SaveSR();
        
        Con.EditJurRow();
        Con.RemoveJurRow();
        Con.RowValueChange();  
            
        Con.CheckTransferringData();
        Con.RelationshipJurisdiction();
        Con.EditRow();
        
        PageReference newPageRef = Con.SaveConfirmation();  
        
            
            
        }
       

    }
    
    
    @isTest static void testMethod2(){
        test.startTest();
        
        User portalUsr = [Select Id,name,ContactId,Account_ID__c From User Where UserName='testportal@portaldifc.com' LIMIT 1];
        System.runAs(portalUsr){
            
            
            
            Relationship__c rr = new Relationship__c();
            rr.Active__c  = true;
            rr.Object_Contact__c = portalUsr.ContactId;
            rr.Subject_Account__c = portalUsr.Account_ID__c;
            rr.Relationship_Type__c = 'Data Controller';
            insert rr;
            
            Permit__c p = new Permit__c();
            p.Name = 'Personal Data Processing Operations';
            p.Account__c = portalUsr.Account_ID__c;
            p.Date_To__c = System.today() + 2;
            P.Status__c = 'Active';
            insert p;
            p = [Select Id,Name,Date_To__c,Account__c,Active__c from  Permit__c Where Id = :p.Id];
            System.debug('ZZZ Second perm--->'+p);
            Data_Protection__c  dp = new Data_Protection__c ();
            dp.Permit__c = p.id;
            dp.RecordTypeId = Schema.SObjectType.Data_Protection__c.getRecordTypeInfosByName().get('Jurisdiction').getRecordTypeId();
            //dp.Service_Request__c = objSR.Id;
            //dp.Name = 'Test Data Prot';
            insert dp;
            
            
            
            PageReference pageRef = Page.RocDataProtectionPermitVF_new; Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('RecordType','01220000000MbZbAAK');
            pageRef.getParameters().put('index','0');
            pageRef.getParameters().put('ValParam','Understood_the_purpose_of_notification__c');
            pageRef.getParameters().put('myParam','false');
            
            
            
            Service_Request__c objSR = new Service_Request__c();
            ApexPages.StandardController sc = new ApexPages.StandardController(objSR);    
            RocDataProtectionPermitVF_edit c = new RocDataProtectionPermitVF_edit(sc); 
            c.SaveSR();
            c.SaveConfirmation();
            c.RowValueChange();
            
            
        }

        test.stopTest();
    }
}