@isTest
public class closeCollectStepsBatchTest {
    
     static testMethod void myUnitTest() {
        
        List<Account> lstAccount = new List<Account>();
   		Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        lstAccount.add(objAccount);
        string conRT;
         
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
   
        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
       
         SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'Fit - Out Service Request';
        objTemplate1.SR_RecordType_API_Name__c = 'Fit - Out Service Request';
        objTemplate1.Active__c = true;
        insert objTemplate1;
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Courier Delivery';
        objStepType1.Code__c = 'Courier Delivery';
        objStepType1.Step_RecordType_API_Name__c = 'Courier Collection';
        objStepType1.Summary__c = 'Courier Delivery';
        insert objStepType1;
         
        Step_Template__c objStepType2 = new Step_Template__c();
        objStepType2.Name = 'Collected';
        objStepType2.Code__c = 'Collected';
        objStepType2.Step_RecordType_API_Name__c = 'Courier Collection';
        objStepType2.Summary__c = 'Collected';
        insert objStepType2;
       
        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Email__c = 'testsr@difc.com';    
        objSR3.Contact__c = objContact.Id;
        objSR3.Express_Service__c = false;
        objSR3.SR_Template__c = objTemplate1.Id;
    
        insert objSR3;
    
       Status__c objStatus = new Status__c();
       objStatus.Name = 'DELIVERED';
       objStatus.Type__c = 'DELIVERED';
       objStatus.Code__c = 'DELIVERED';
       insert objStatus;
         
       Status__c objStatus1 = new Status__c();
       objStatus1.Name = 'Pending Review';
       objStatus1.Type__c = 'Pending Review';
       objStatus1.Code__c = 'Pending Review';
       insert objStatus1;
         
       SR_Status__c objSRStatus = new SR_Status__c();
       objSRStatus.Name = 'AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
       objSRStatus.Code__c = 'AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
       insert objSRStatus;
       
       step__c objStep1 = new Step__c();
       objStep1.SR__c = objSR3.Id;
       objStep1.Step_Template__c = objStepType2.id ;
       objStep1.No_PR__c = false;
       objStep1.status__c = objStatus1.id;
       insert objStep1;
         
       step__c objStep = new Step__c();
       objStep.SR__c = objSR3.Id;
       objStep.Step_Template__c = objStepType1.id ;
       objStep.No_PR__c = false;
       objStep.status__c = objStatus.id;
       objStep.Parent_Step__c = objStep1.Id;
       insert objStep;
         
       
       Test.startTest();
         database.executeBatch(new closeCollectStepsBatch());
       Test.stopTest();
        	
     }

}