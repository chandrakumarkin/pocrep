@isTest(seeAllData=false)
private class TestRoCDeclarationCls{


    static testMethod void testDec() {
         
        PageReference pageRef = Page.RoCDeclaration;
        Test.setCurrentPageReference(pageRef);
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.Authorized_Share_Capital__c = 500000;
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        insert objAccount;
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
       // objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.legal_structures__c ='LLC';
        objSR.currency_list__c = 'US Dollar';
        objSR.Share_Capital_Membership_Interest__c = 50000;
        objSR.No_of_Authorized_Share_Membership_Int__c = 560;
        insert objSR;
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Change_in_Authorized_Shares_or_Nominal_Value';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Change_in_Authorized_Shares_or_Nominal_Value';
        insert objPageFlow;
        
        Page__c objPage = new Page__c();
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        insert objPage;
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Object_Name__c = 'Service_Request__c';
        objSD.Field_API_Name__c = 'Input Field';
        objSD.Name = 'Section Test';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        ApexPages.currentPage().getParameters().put('PageId',objPage.id);
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
   
    	RoCDeclarationCls rocDec = new RoCDeclarationCls();
        rocDec.objSRFlow.Id = objSR.id;
        rocDec.DynamicButtonAction();
        //rocDec.decSignatory[0].Service_Request__c = objSR.id;
        rocDec.decSignatory[0].Signatory_Name__c = 'Test';
        rocDec.decSignatory[0].Capacity__c = 'CEO';
        rocDec.save_close();
        rocDec.deleteRow();
        Declaration_Signatory__c decSign = new Declaration_Signatory__c(Service_Request__c = objSR.id);
        insert decSign;
        rocDec.decSignatory[0].Signatory_Name__c =null ;
        rocDec.decSignatory[0].Capacity__c = null;
        rocDec.save_close();
        rocDec.strNavigatePageId = objPage.Id;
        rocDec.strActionId = objSD.Id;
        rocDec.goTopage();
        rocDec.getDyncPgMainPB();
        rocDec.DynamicButtonAction();
        //rocDec.decSignatory[0].Service_Request__c = objSR.id;
        rocDec.decSignatory[0].Signatory_Name__c = 'Test';
        rocDec.decSignatory[0].Capacity__c = 'CEO';
        rocDec.save_close();
        rocDec.add_rows();
        rocDec.RowId = 0;
        rocDec.deleteRow();
        RoCDeclarationCls rocDec1 = new RoCDeclarationCls();
        
    }
    
    
 }