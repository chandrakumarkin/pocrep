/*********************************************************************************************************************
*  Name        : CC_AmendmentsCls
*  Author      : Durga Prasad
*  Company     : NSI JLT
*  Date        : 2014-10-20    
*  Purpose     : This is an custom code which will process the Amendments of all types of Legal Structures.
 --------------------------------------------------------------------------------------------------------------------------
Modification History
 --------------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
--------------------------------------------------------------------------------------------------------------------------             
V1.1    22-01-2015  Saima          Autofill method for SR fields added     
V1.2    12-02-2015  Saima          Rejection method added for SR on Step
V1.3    12-02-2016  Sravan          Added custom method that checks any changes to amendment Tkt # 3049.
V1.4    22-05-2017   Sravan        Filtering GSO Authorized Signatories form the query
v1.5    15-Nov-2020    selva       #12276 'Authorized Signatory Combinations' fields getting blank
**********************************************************************************************************************/

public without sharing class CC_AmendmentsCls {
    /*
        Method Name :   Process_Sale_Transfer_MembershipInterest
        Description :   This method will deal the sale/Transfer of MembershipInterest
    */
    public static string Process_Sale_Transfer_MembershipInterest(Step__c stp){
        string strResult = 'Success';
        system.debug('stp===>'+stp);
        if(stp!=null && stp.Id!=null && stp.SR__c!=null){
            list<Account_Share_Detail__c> lstAccShares_Old = new list<Account_Share_Detail__c>();
            list<Shareholder_Detail__c> lstShareholderDetails_Old = new list<Shareholder_Detail__c>();
            list<Relationship__c> lstRelationships_Old = new list<Relationship__c>();
            
            for(Account objAcc:[select id, 
            (select id,Active__c,Appointment_Date__c,End_Date__c,Object_Account__c,Object_Contact__c,Relationship_Type__c,
            Start_Date__c,Ter_Date__c,Title__c from Primary_Account__r),
            (select id,Available_for_Issue__c,No_of_shares_per_class__c,Nominal_Value__c,Status__c from Account_Share_Details__r),
            (select id,Account_Share__c,Amendment__c,Amount_of_Capital_Interest__c,No_of_Shares__c,Status__c from Account__r)
            from Account where Id=:stp.SR__r.Customer__c]){
                if(objAcc.Account_Share_Details__r!=null && objAcc.Account_Share_Details__r.size()>0)
                    lstAccShares_Old = objAcc.Account_Share_Details__r;
                if(objAcc.Account__r!=null && objAcc.Account__r.size()>0)
                    lstShareholderDetails_Old = objAcc.Account__r;
                if(objAcc.Primary_Account__r!=null && objAcc.Primary_Account__r.size()>0)
                    lstRelationships_Old = objAcc.Primary_Account__r;
            }
        }
        return strResult;
    }
    
    public static string Process_ChangeAmendments(Step__c stp){
        string strResult = 'Success';
        if(stp!=null && stp.Id!=null && stp.SR__c!=null){
            
        }
        return strResult;
    }
    
    
    
    /*V1.1 Authorized Signatory Autofill on SR fields */
    
    public static string AuthSignFill(Step__c stp){
        string strResult = 'Success';
        if(stp!=null && stp.Id!=null && stp.SR__c!=null){
            String AuthName=null;
            String AuthARName=null;
            Service_Request__c srNew = new Service_Request__c();
            srNew.id=stp.SR__c;
            List<Amendment__c> Rlist=[select id,Status__c,First_Name_Arabic__c,Title_new__c,Family_Name__c,Given_Name__c,ServiceRequest__r.Authorized_Signatory_Combinations__c,ServiceRequest__r.Authorized_Signatory_Combinations_Arabic__c from Amendment__c where Relationship_Type__c like '%Authorized Signatory%'and ServiceRequest__r.id=:stp.SR__c and Status__c!='Removed' and Relationship_Type__c !='GSO Authorized Signatory']; // V1.4 Added GSO Authorized Signatory
            if(Rlist.size()>0 && Rlist!=null &&(Rlist[0].ServiceRequest__r.Authorized_Signatory_Combinations__c==null || Rlist[0].ServiceRequest__r.Authorized_Signatory_Combinations__c=='')){
                for(Amendment__c R:Rlist){
                    AuthName+=R.Title_new__c+' '+R.Given_Name__c+' '+R.Family_Name__c+'\n';
                    AuthARName+=R.First_Name_Arabic__c+'\n';
                }
            }
            if(AuthName!=null){  srNew.Authorized_Signatory_Combinations__c = AuthName;}//v1.5
            if(AuthARName!=null){ srNew.Authorized_Signatory_Combinations_Arabic__c = AuthARName;}//v1.5 
            update srNew;
        }
        
        CheckChangedAmendment(stp.SR__c); //V1.3
        return strResult;
    }
    
    // Start V1.3
     Public static void CheckChangedAmendment(string strSRID){
        Map<id,Relationship__c> relationshipDetails = new map<id,Relationship__c>([select id,Name,Object_Contact__c,Object_Contact__r.FirstName,Object_Contact__r.LastName,Type_of_Authority__c from Relationship__c where Subject_Account__c = :[select Customer__c from Service_Request__c where id=:strSRID].Customer__c]);
        List<Amendment__c> amendment = new List<amendment__c>([select IS_Amend_Changed__c,id,status__c,sys_create__c,Relationship_Id__c,Given_Name__c,Family_Name__c,Representation_Authority__c from amendment__c where serviceRequest__c=:strSRID]);
        List<Amendment__c> updateAmendment = new list<Amendment__c>();
      if(amendment.size()>0){   
            for(Amendment__c a : amendment){                
                if((a.Relationship_Id__c !=null && relationshipDetails !=null && relationshipDetails.ContainSkey(a.Relationship_Id__c) && 
                   (relationshipDetails.get(a.Relationship_Id__c).Object_Contact__r.FirstName != a.Given_Name__c || relationshipDetails.get(a.Relationship_Id__c).Type_of_Authority__c !=a.Representation_Authority__c || relationshipDetails.get(a.Relationship_Id__c).Object_Contact__r.LastName != a.Family_Name__c) ||  a.sys_create__c==false || a.status__c=='Removed' )){
                    a.IS_Amend_Changed__c = true;
                     updateAmendment.add(a);            
                }
            }
      }
      
     if(updateAmendment.size()>0)
        update updateAmendment;
        
                
     }
    
    // End V1.3
    
    
    /*** V1.2   Rejection SR custom code***/
    public static string RejectSR(Step__c stp){
        string strResult = 'Success';
        if(stp!=null && stp.Id!=null && stp.SR__c!=null){
            list<Shareholder_Detail__c> lstupdateShare = new list<Shareholder_Detail__c>();
            list<Account_Share_Detail__c> lstupdateAcc = new list<Account_Share_Detail__c>();
            list<Shareholder_Detail__c> lstSharedetail= new list<Shareholder_Detail__c>();
            list<Account_Share_Detail__c> lstaccdetail = new list<Account_Share_Detail__c>();
            Set<String> amendIds= new Set<String>();
            list<Service_Request__c> lstSR = [select id,finalizeAmendmentFlg__c,Record_Type_Name__c,Internal_Status_Name__c,Customer__c from Service_Request__c where Id=:stp.SR__c];
            if(lstSR!=null && lstSR.size()>0){
                for(Amendment__c lstAmend: [Select id,ServiceRequest__c,Relationship_Type__c from Amendment__c where ServiceRequest__c =:stp.SR__c AND (Relationship_Type__c='Member' OR Relationship_Type__c='Shareholder')])
                    amendIds.add(lstAmend.id);
                System.debug('====>>>Amendlist to be deleted: '+amendIds);
                if(amendIds !=null &&amendIds.size()>0){
                    lstSharedetail = [Select id,Account__c,Amendment__c,Account_Share__c,Sys_Proposed_No_of_Shares__c,No_of_Shares__c,Status__c from Shareholder_Detail__c where Status__c='Draft' AND Amendment__c IN:amendIds];
                    for(Shareholder_Detail__c shrdtl:[Select id,Account__c,Amendment__c,Account_Share__c,Sys_Proposed_No_of_Shares__c,No_of_Shares__c,Status__c from Shareholder_Detail__c where Status__c='Active' AND Account__c=:lstSR[0].Customer__c]){
                        shrdtl.Sys_Proposed_No_of_Shares__c = shrdtl.No_of_Shares__c;
                        lstupdateShare.add(shrdtl);
                    }
                }
                
                lstaccdetail = [Select id,Account__c,Status__c from Account_Share_Detail__c where Account__c=:lstSR[0].Customer__c AND Status__c='Draft'];
                for(Account_Share_Detail__c accdtl:[Select id,No_of_shares_per_class__c,Sys_Proposed_No_of_Shares_per_Class__c,Sys_Proposed_Nominal_Value__c,Nominal_Value__c,Account__c,Status__c from Account_Share_Detail__c where Account__c=:lstSR[0].Customer__c AND Status__c='Active']){
                        accdtl.Sys_Proposed_No_of_Shares_per_Class__c = accdtl.No_of_shares_per_class__c;
                        accdtl.Sys_Proposed_Nominal_Value__c = accdtl.Nominal_Value__c;
                        lstupdateAcc.add(accdtl);
                }
                try{
                    if(lstupdateShare!=null && lstupdateShare.size()>0)
                        update lstupdateShare;
                    if(lstupdateAcc!=null && lstupdateAcc.size()>0)
                        update lstupdateAcc;
                    if(lstSharedetail!=null && lstSharedetail.size()>0)
                        delete lstSharedetail;
                    if(lstaccdetail!=null && lstaccdetail.size()>0)
                        delete lstaccdetail; 
                        
                }catch(DmlException e){
                    string DMLError = e.getdmlMessage(0)+'';
                    if(DMLError==null){
                        DMLError = e.getMessage()+'';
                    }
                    strResult = DMLError;
                }
            }else{
                
            }
        }
            return strResult;
    }
    
}