/*****************************************************************
Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date           Updated By           Description
 ----------------------------------------------------------------------------------------              
 V1.0    15-Dec-2015    Shabbir Ahmed        Schedule class to call BatchMassDeleteRecords class to delete all the record from any object 
*****************************************************************/
global without sharing class ScheduleMassDeleteRecords implements Schedulable {
    global void execute(SchedulableContext ctx) {
        String query = 'SELECT id FROM Company_Transaction_Detail__c';
        BatchMassDeleteRecords batchApex = new BatchMassDeleteRecords(query );
        ID batchprocessid = Database.executeBatch(batchApex);
    }
}