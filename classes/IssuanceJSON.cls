/******************************************************************************************
 *  Class Name  : IssuanceJSON
 *  Author      : Sai kalyan Sanisetty 
 *  Company     : DIFC
 *  Date        : 18 AUG 2019        
 *  Description :                
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1    18 AUG 2019         Sai                Initial Version  
*******************************************************************************************/
public with sharing class IssuanceJSON{
    
    public static String requestId;
    public static set<id> setAttachIds;
    public Static String companyNameArabic;
    
    public static string issuanceJSONBODY(String StepID){
        
        STEP__c stepRecord = new STEP__c();
        Contact contactRecord = new Contact();
        List<String> lstMobileNumbers;
        List<String> lstAmdType = new List<String>();
        lstAmdType = Label.Amendment_Type.split(',');
        List<String> lstAmdRelType = new List<String>();
        lstAmdRelType = Label.Relationship_Type.split(',');
        List<String> lstIban = new List<String>();
        
        RequestCommonData rCommonData = new RequestCommonData();
        RepresentativeIndividual rIndividual = new RepresentativeIndividual();
        List<ShareholderIndividual> lstshIndividual = new List<ShareholderIndividual>();
        ShareholderIndividual shIndividual = new ShareholderIndividual();
        BuildIssuanceDto IssuanceDto  = new BuildIssuanceDto();
        List<DirectorBoard> lstDirectors = new List<DirectorBoard>();
        List<DirectorBoard> lstParentDirectors = new List<DirectorBoard>();
        DirectorBoard directors = new DirectorBoard();
        List<SR_Doc__c> srDocsAvailable = new List<SR_Doc__c>();
        map<string,SR_Doc__c> individualPassportCopies = new map<string,SR_Doc__c>();
        map<string,SR_Doc__c> BodyCorporateCopies = new map<string,SR_Doc__c>();
        setAttachIds = new set<id>();      
        String SrDocID;  
        Attachment eachAttachment = new Attachment();
         
        if(String.isNotBlank(StepID)){
            stepRecord = [SELECT Name,SR__r.CreatedBYID,SR_Number_Step__c,SR__c,SR__r.Foreign_Registration_Name__c,SR__r.Foreign_Registration_Date__c,
            			  SR__r.Foreign_Registration_Place__c,SR__r.Customer__c,SR__r.Legal_Structures__c,SR__r.Place_of_Registration_parent_company__c FROM STEP__c where ID=:StepID];
        }
       
        if(stepRecord !=null && String.isNotBlank(stepRecord.Name)){
            rCommonData.requestSourceId = stepRecord.SR_Number_Step__c;
            requestId = stepRecord.SR_Number_Step__c;
        }
        
        if(String.isNotBlank(stepRecord.SR__r.CreatedBYID)){
            
            User userRecord = new User();
            userRecord = [SELECT ContactID FROM USER where ID=:stepRecord.SR__r.CreatedBYID];
            
            if(String.isNotBlank(userRecord.ContactID)){
                contactRecord  = [SELECT Name,Account.Name,Account.Arabic_Name__c,Account.Legal_Type_of_Entity__c,Previous_Nationality__c,Gender__c,
                                  Birthdate,Country_of_Birth__c,Place_of_Birth__c,MobilePhone,Area__c,DOCUMENT_TYPE__c,Passport_No__c,Nationality__c,
                                  UID_Number__c,Passport_Nationality__c,Work_Phone__c,Account.E_mail__c,Account.Work_Phone__c,Account.Country__c,
                                  Address__c,Date_of_Issue__c,Portal_Username__c,Issued_Country__c,Passport_Expiry_Date__c,Account.Registration_Type__c,Account.Home_Country__c,Account.ParentID,
                                  Place_of_Issue__c
                                  FROM Contact where ID=:userRecord.ContactID];
            }
        }
        if(String.isNotBlank(contactRecord.Portal_Username__c)){
            
            for(SR_Doc__c eachDoc:[SELECT Id,Name,Doc_ID__c FROM SR_Doc__c where Service_Request__r.Username__c=:contactRecord.Portal_Username__c 
                                   AND Service_Request__r.Record_Type_Name__c='User_Access_Form']){
                if(eachDoc.Name.contains('Passport')){
                    setAttachIds.add(eachDoc.Doc_ID__c);
                    SrDocID = eachDoc.Id;
               }
            }
            
        }
        if(contactRecord.Account.Name!=null && !test.isRunningTest()){
            String accountName = contactRecord.Account.Name.replaceAll(' ', '%20');
            companyNameArabic =  TranslatiorClass.translatorMethod(contactRecord.Account.Name);
        }
        rIndividual.companyNameAR = returnNOTNull(companyNameArabic);
        rIndividual.companyNameEN = returnNOTNull(contactRecord.Account.Name);
        if(contactRecord.Nationality__c !=null && contactRecord.Nationality__c !='Russian Federation'){
        	rIndividual.currentNationality= returnNOTNull(contactRecord.Nationality__c);
        }
        else if(contactRecord.Nationality__c =='Russian Federation' ){
        	rIndividual.currentNationality= 'Russia';
        }
        if(contactRecord.Previous_Nationality__c !=null && contactRecord.Previous_Nationality__c !='' && contactRecord.Previous_Nationality__c !='Russian Federation'){
        	rIndividual.previousNationality= contactRecord.Previous_Nationality__c;
        }
        else if(contactRecord.Previous_Nationality__c =='Russian Federation' ){
        	rIndividual.previousNationality  = 'Russia';
        }
        if(contactRecord.Gender__c !=null && contactRecord.Gender__c =='Male'){
            rIndividual.gender= 'M';
        }
        else if(contactRecord.Gender__c !=null && contactRecord.Gender__c !='Male'){
            rIndividual.gender= 'F';
        }
        else{
            rIndividual.gender= '';
        }
        rIndividual.birthDate= returnNOTNull(String.valueOf(contactRecord.Birthdate));
        if(contactRecord.Country_of_Birth__c !=null && contactRecord.Country_of_Birth__c !='Russian Federation'){
        	rIndividual.birthCountry= returnNOTNull(contactRecord.Country_of_Birth__c);
        }
        else if(contactRecord.Country_of_Birth__c =='Russian Federation'){
        	rIndividual.birthCountry= 'Russia';
        }
        rIndividual.birthCity= returnNOTNull(contactRecord.Place_of_Birth__c);
        if(String.isNotBlank(contactRecord.MobilePhone)){
            
            String mobileNumber = contactRecord.MobilePhone.replace('+','');
            lstMobileNumbers = new List<String>{mobileNumber};
            rIndividual.mobileNumber= lstMobileNumbers;
        }
        else{
            lstMobileNumbers = new List<String>();
            rIndividual.mobileNumber= lstMobileNumbers;
        }
        rIndividual.area= 'DIFC';
        if(String.isNotBlank(contactRecord.UID_Number__c)){
            rIndividual.udbNumber= Integer.valueOf(returnNOTNull(contactRecord.UID_Number__c)); 
        }
        else{
            rIndividual.udbNumber= 0;
        }
        rIndividual.documentType= 'PASSPORT';
        rIndividual.documentNumber= returnNOTNull(contactRecord.Passport_No__c); 
        if(contactRecord.Issued_Country__c !=null && contactRecord.Issued_Country__c !='Russian Federation'){
        	rIndividual.documentIssuePlace= returnNOTNull(contactRecord.Issued_Country__c);
        }  
        else if(contactRecord.Issued_Country__c =='Russian Federation'){
        	rIndividual.documentIssuePlace= 'Russia';
        }
        rIndividual.documentIssueDate= returnNOTNull(String.valueOf(contactRecord.Date_of_Issue__c));   
        rIndividual.documentExpiryDate= returnNOTNull(String.valueOf(contactRecord.Passport_Expiry_Date__c)); 
        if(SrDocID !=null){
            rIndividual.documentCopyFileId= SrDocID;         
        }
        else
        {
            rIndividual.documentCopyFileId = 'NA';
        }
        rIndividual.residenceCopyFileId= 'NA';
        rIndividual.address = Label.DIFC_Address;
        
        for(SR_Doc__c objSR_Doc:[select id,Doc_ID__c,Name,Document_Master__c,Document_Master__r.Code__c,Amendment__c,Amendment__r.Amendment_Type__c,
                                 Amendment__r.Passport_No__c,Amendment__r.Relationship_Type__c,Amendment__r.Customer__r.name,
                                 Amendment__r.Customer__r.Company_Type__c,Amendment__r.ServiceRequest__r.Legal_Structures__c,
                                 amendment__r.Name_of_incorporating_Shareholder__c,amendment__r.Registration_No__c from SR_Doc__c where Doc_ID__c!=null 
                                 and Service_Request__c=:stepRecord.SR__c and (Amendment__r.Amendment_Type__c ='Individual' 
                                 or Amendment__r.Amendment_Type__c ='Body Corporate')]){
            srDocsAvailable.add(objSR_Doc);
            if(objSR_Doc.Amendment__r.Amendment_Type__c == 'Individual' && objSR_Doc.Amendment__r.Passport_No__c!=null && objSR_Doc.Name.contains('Passport')){
                individualPassportCopies.put(objSR_Doc.Amendment__r.Passport_No__c,objSR_Doc);
            }
        }
        
        for(SR_Doc__c objSR_Doc: srDocsAvailable)
        {
            if(objSR_Doc.Amendment__r.Amendment_Type__c == 'Body Corporate' && objSR_Doc.Amendment__c !=null)
            { 
                if(objSR_Doc.Name.contains('Certificate of Incorporation'))
                {
                  //setAttachIds.add(objSR_Doc.Doc_ID__c);
                  BodyCorporateCopies.put(objSR_Doc.Amendment__r.Registration_No__c,objSR_Doc);
                }
             } 
         }
        
        for(Amendment__c eachAmendment:[SELECT Company_Name__c,Nationality_list__c,Gender__c,Date_of_Birth__c,Permanent_Native_Country__c,
                                        Permanent_Native_City__c,Mobile__c,Area_in_sq_m__c,Passport_No__c,Nationality__c,Passport_Issue_Date__c,
                                        Passport_Expiry_Date__c,Contact__c,Customer__r.Country__c,Customer__r.Registration_Type__c,Address__c,
                                        New_No_of_Shares__c,Old_Share_Value__c,Customer__r.Arabic_Name__c,Registration_Date__c,Amendment_Type__c,Customer__r.Name,Customer__r.ROC_Number__c,
                                        Place_of_Issue__c,Relationship_Type__c,Ownership_Percentage__c,Registration_No__c FROM Amendment__c where ServiceRequest__c=:stepRecord.SR__c AND Amendment_Type__c IN:lstAmdType AND 
                                        Relationship_Type__c IN:lstAmdRelType ORDER BY CreatedDate asc]){
                shIndividual = new ShareholderIndividual();   
                if(eachAmendment.Amendment_Type__c == 'Individual'){
                    
                    set<string> IndShareholdertype=new Set<string>{'Designated Member','Shareholder','Member','Limited Partner','General Partner','Founding Member','Founder'};
                    if(eachAmendment.Amendment_Type__c == 'Individual' && IndShareholdertype.contains(eachAmendment.Relationship_Type__c) && individualPassportCopies.containsKey(eachAmendment.Passport_No__c))
                    {
                        setAttachIds.add(individualPassportCopies.get(eachAmendment.Passport_No__c).Doc_ID__c);
                    }
       
                    shIndividual.companyNameAR = returnNOTNull(companyNameArabic);
                    shIndividual.companyNameEN = returnNOTNull(eachAmendment.Customer__r.Name);
                    if(eachAmendment.Nationality_list__c !=null && eachAmendment.Nationality_list__c != 'Russian Federation'){
                    	shIndividual.currentNationality = returnNOTNull(eachAmendment.Nationality_list__c);
                    }
                    else{
                    	shIndividual.currentNationality = 'Russia';
                    }
                    if(eachAmendment.Gender__c !=null && eachAmendment.Gender__c =='Male'){
                        shIndividual.gender= 'M';
                    }
                    else if(eachAmendment.Gender__c !=null && eachAmendment.Gender__c !='Male'){
                        shIndividual.gender= 'F';
                    }
                    else{
                        shIndividual.gender= 'M';
                    }
        
                    shIndividual.birthDate = returnNOTNull(String.valueOf(eachAmendment.Date_of_Birth__c));
                    if(eachAmendment.Permanent_Native_Country__c !=null && eachAmendment.Permanent_Native_Country__c !='Russian Federation'){
                    	shIndividual.birthCountry = returnNOTNull(eachAmendment.Permanent_Native_Country__c);
                    }
                    else if(eachAmendment.Permanent_Native_Country__c =='Russian Federation'){
                    	shIndividual.birthCountry = 'Russia';
                    }
                    shIndividual.birthCity = returnNOTNull(eachAmendment.Permanent_Native_City__c);
                    if(String.isNotBlank(eachAmendment.Mobile__c)){
                        
                        String mobileNumber = eachAmendment.Mobile__c.replace('+','');
                        lstMobileNumbers = new List<String>{mobileNumber};
                        shIndividual.mobileNumber = lstMobileNumbers;
                    }
                    else{
                        lstMobileNumbers = new List<String>();
                        shIndividual.mobileNumber = lstMobileNumbers;
                    }
                    shIndividual.area = 'DIFC';
                    shIndividual.shareholderIndividualId = eachAmendment.ID;
                    shIndividual.udbNumber = 0;      
                    shIndividual.documentType = Label.PASSPORT;    
                    shIndividual.documentNumber = returnNOTNull(String.valueOf(eachAmendment.Passport_No__c));
                    shIndividual.documentIssuePlace = returnNOTNull(eachAmendment.Place_of_Issue__c);    
                    shIndividual.documentIssueDate = returnNOTNull(String.valueOf(eachAmendment.Passport_Issue_Date__c));     
                    shIndividual.documentExpiryDate = returnNOTNull(String.valueOf(eachAmendment.Passport_Expiry_Date__c));
                    if(eachAmendment.Passport_No__c !=null && individualPassportCopies !=null && individualPassportCopies.get(eachAmendment.Passport_No__c) !=null){
                        shIndividual.documentCopyFileId = individualPassportCopies.get(eachAmendment.Passport_No__c).Id; 
                    }
                    else{
                        shIndividual.documentCopyFileId = '';
                    }
                    shIndividual.residenceCopyFileId = 'NA'; 
                    shIndividual.address = Label.DIFC_Address;
                    if(eachAmendment.Ownership_Percentage__c !=null){
	                    String ownership = eachAmendment.Ownership_Percentage__c.replace('%','');
	                    shIndividual.ownershipPercent = Integer.valueof(ownership ); 
                    }
                    else{
                        shIndividual.ownershipPercent = 1;
                    }
                    shIndividual.sharesNumber = 1;
                    shIndividual.sharesValue = 1;
                    if(individualPassportCopies.containsKey(eachAmendment.Passport_No__c)){
                        setAttachIds.add(individualPassportCopies.get(eachAmendment.Passport_No__c).Doc_ID__c);
                    }
                }   
                else{
                    
                    if(eachAmendment.Amendment_Type__c == 'Body Corporate' && BodyCorporateCopies.containsKey(eachAmendment.Registration_No__c))
                    {
                        setAttachIds.add(BodyCorporateCopies.get(eachAmendment.Registration_No__c).Doc_ID__c);
                    }
                    shIndividual.shareholderIndividualId = eachAmendment.ID;
                    shIndividual.companyNameAR = returnNOTNull(companyNameArabic);
                    shIndividual.companyNameEN = returnNOTNull(eachAmendment.Company_Name__c);
                    if(eachAmendment.Nationality_list__c !=null && eachAmendment.Nationality_list__c != 'Russian Federation'){
                    	shIndividual.currentNationality = returnNOTNull(eachAmendment.Nationality_list__c);
                    }
                    else if(eachAmendment.Nationality_list__c == 'Russian Federation'){
                    	shIndividual.currentNationality = 'Russia';
                    }
                    shIndividual.gender= 'M';
                    shIndividual.birthDate = returnNOTNull(String.valueOf(eachAmendment.Date_of_Birth__c));
                    
                    if(eachAmendment.Permanent_Native_Country__c !=null && eachAmendment.Permanent_Native_Country__c !='Russian Federation'){
                    	shIndividual.birthCountry = returnNOTNull(eachAmendment.Permanent_Native_Country__c);
                    }
                    else if(eachAmendment.Permanent_Native_Country__c =='Russian Federation'){
                    	shIndividual.birthCountry = 'Russia';
                    }
                    shIndividual.birthCity = returnNOTNull(eachAmendment.Permanent_Native_City__c);
                    shIndividual.area = 'DIFC';
                    if(String.isNotBlank(eachAmendment.Mobile__c)){
                        
                        String mobileNumber =eachAmendment.Mobile__c.replace('+','');
                        lstMobileNumbers = new List<String>{mobileNumber};
                        shIndividual.mobileNumber = lstMobileNumbers;
                    }
                    else{
                        lstMobileNumbers = new List<String>();
                        shIndividual.mobileNumber = lstMobileNumbers;
                    }
                    
                    
                    shIndividual.documentType = 'Certificate of Incorporation';    
                    shIndividual.documentNumber = returnNOTNull(String.valueOf(eachAmendment.Registration_No__c));
                    shIndividual.documentIssuePlace = 'United Arab Emirates';   
                    shIndividual.documentIssueDate = returnNOTNull(String.valueOf(eachAmendment.Registration_Date__c)); 
                    if(BodyCorporateCopies.get(eachAmendment.Registration_No__c) !=null){ 
                    	shIndividual.documentCopyFileId = BodyCorporateCopies.get(eachAmendment.Registration_No__c).Id;  
                    }
                    shIndividual.residenceCopyFileId = 'NA'; 
                    shIndividual.address = returnNOTNull(eachAmendment.Address__c);
                    if(eachAmendment.Ownership_Percentage__c !=null){
                        shIndividual.ownershipPercent = Integer.valueof(eachAmendment.Ownership_Percentage__c.replace('%',''));
                    }
                    else{
                        shIndividual.ownershipPercent = 1;
                    }
                    
                    if(eachAmendment.New_No_of_Shares__c !=null){
                    	shIndividual.sharesNumber = Integer.valueOf(eachAmendment.New_No_of_Shares__c);
                    }
                    else
                    {
                    	shIndividual.sharesNumber = 1;
                    }
                    shIndividual.sharesValue = 1;
                } 
                lstshIndividual.add(shIndividual);              
        }
        
        List<Amendment__c> lstAmendmentDirectors = new List<Amendment__c>();
        lstAmendmentDirectors = [select Nationality_list__c,Given_Name__c,Family_Name__c,Amendment_Type__c,Company_Name__c,Place_of_Registration__c FROM Amendment__c where ServiceRequest__c=:stepRecord.SR__c AND Relationship_Type__c='Director'];
        
        for(Amendment__c eachDirector:lstAmendmentDirectors)
        {	
            directors = new DirectorBoard();
            if(eachDirector.Amendment_Type__c == 'Individual'){
	            String Name  = eachDirector.Given_Name__c+' '+eachDirector.Family_Name__c;
	            if(String.isNotBlank(Name)){
	                directors.Name = returnNOTNull(Name);
	                if(eachDirector.Nationality_list__c !=null && eachDirector.Nationality_list__c !='Russian Federation'){
	                	directors.nationality = returnNOTNull(eachDirector.Nationality_list__c);
	                }
	                else if(eachDirector.Nationality_list__c =='Russian Federation'){
	                	directors.nationality = 'Russia';
	                }
	            }
            }
            else if(eachDirector.Amendment_Type__c == '	Body Corporate'){
            	
            	if(String.isNotBlank(eachDirector.Company_Name__c)){
	                directors.Name = returnNOTNull(eachDirector.Company_Name__c);
	                if(eachDirector.Place_of_Registration__c !=null && eachDirector.Place_of_Registration__c !='Russian Federation'){
	                	directors.nationality = returnNOTNull(eachDirector.Place_of_Registration__c);
	                }
	                else if(eachDirector.Place_of_Registration__c =='Russian Federation'){
	                	directors.nationality = 'Russia';
	                }
	            }
	            
            }
            lstDirectors.add(directors);
        }
        if(lstAmendmentDirectors !=null && lstAmendmentDirectors.isEmpty()){
        	  directors = new DirectorBoard();
        	  directors.Name = 'NA';   
        	  directors.nationality = 'United Arab Emirates';
        	  lstDirectors.add(directors);     	
        }   
        
        List<ParentCompanyData> lstParentData = new List<ParentCompanyData>();
        
        Account parentAccount = new Account();
        ParentCompanyData parentData = new ParentCompanyData();
        if(String.isNotBlank(stepRecord.SR__r.Foreign_Registration_Name__c) && !test.isRunningTest()){
        	parentData.companyNameAR = TranslatiorClass.translatorMethod(stepRecord.SR__r.Foreign_Registration_Name__c);
        }
        else{
        	parentData.companyNameAR = 'NA';
        }
        parentData.companyNameEN = returnNOTNull(stepRecord.SR__r.Foreign_Registration_Name__c);
        if(stepRecord.SR__r.Foreign_Registration_Name__c !=null){
        	 directors = new DirectorBoard();
	    	  directors.Name = 'NA';   
	    	  directors.nationality = 'United Arab Emirates';
	    	  lstDirectors.add(directors); 
        	parentData.directorBoard = lstDirectors;
        }
        parentData.WorkArea = 'DIFC';
        
        String legalStructer = '';
        if(stepRecord.SR__r.Legal_Structures__c =='LTD'){
        	legalStructer = 'Private Company';
        }
        else if(stepRecord.SR__r.Legal_Structures__c =='FRC'){
        	legalStructer = 'FRC ';
        }
        else if(stepRecord.SR__r.Legal_Structures__c !='FRC'){
        	legalStructer = returnNOTNull(stepRecord.SR__r.Legal_Structures__c);
        }
        
        parentData.registrationType = legalStructer;
        parentData.companyPercentage ='100';
        if(stepRecord.SR__r.Foreign_Registration_Place__c !=null){
        	
        	if(stepRecord.SR__r.Foreign_Registration_Place__c !=null && stepRecord.SR__r.Foreign_Registration_Place__c != 'Russian Federation'){
        		parentData.originCountry = stepRecord.SR__r.Foreign_Registration_Place__c;
        	}
        	else if(stepRecord.SR__r.Foreign_Registration_Place__c == 'Russian Federation'){
        		parentData.originCountry = 'Russia';
        	}
        	
        }
        else{
        	parentData.originCountry = 'United Arab Emirates';
        }
         
        parentData.location  = 'DIFC';
        parentData.position = 'Inside the country';
        parentData.employeesNumber = '1';
        parentData.EstablishmentNumber = '1';
        lstParentData.add(parentData);   
        
        IssuanceDto.registrationType = legalStructer;
        IssuanceDto.companyNameEN = returnNOTNull(contactRecord.Account.Name);
        IssuanceDto.companyNameAR = returnNOTNull(companyNameArabic);
        IssuanceDto.workArea = 'DIFC';
       
        IssuanceDto.companyPercentage = 100;    
        
       
           
        IssuanceDto.requestCommonData = rCommonData;
        IssuanceDto.representativeIndividual = rIndividual;
        IssuanceDto.shareholderIndividual = lstshIndividual;
        IssuanceDto.workArea = 'DIFC';
        IssuanceDto.directorBoard = lstDirectors;
        
        if(contactRecord.Account.Country__c !=null && contactRecord.Account.Country__c != 'Russian Federation'){
        	IssuanceDto.originCountry = returnNOTNull(contactRecord.Account.Country__c); 
        }
        else if(contactRecord.Account.Country__c == 'Russian Federation'){
        	IssuanceDto.originCountry = 'Russia'; 
        }
        IssuanceDto.location = 'DIFC';
        IssuanceDto.position = 'Inside';
        IssuanceDto.representativeName = returnNOTNull(contactRecord.Name);
        if(contactRecord.Nationality__c !=null && contactRecord.Nationality__c != 'Russian Federation'){
        	IssuanceDto.representativeNationality = returnNOTNull(contactRecord.Nationality__c);
        }
        else if(contactRecord.Nationality__c == 'Russian Federation'){
        	IssuanceDto.representativeNationality = 'Russia';
        }
        IssuanceDto.representativeId = returnNOTNull(contactRecord.UID_Number__c);
        if(contactRecord.Work_Phone__c !=null){
        	IssuanceDto.representativeContactNumber = contactRecord.Work_Phone__c.replace('+','');
        }
        else{
        	IssuanceDto.representativeContactNumber = 'NA';
        }
        IssuanceDto.employeesNumber = 1;
        IssuanceDto.establishmentNumber = contactRecord.Id;
        IssuanceDto.establishmentNumberImmSys = 'NA';
        IssuanceDto.iBan = lstIban;
        IssuanceDto.bankStatementFileId = 'NA';
        IssuanceDto.shareholderType = Label.Individual;
        IssuanceDto.representativeType = Label.Individual;
        IssuanceDto.parentCompanyData = lstParentData;
        IssuanceDto issuanceFinal = new IssuanceDto();
        issuanceFinal.IssuanceDto = IssuanceDto;
        String finalJson = JSON.serialize(issuanceFinal); 
        return finalJson;
    }

   public static string returnNOTNull(String value){
        
        String finalString = '';
        if(String.isNotBlank(value)){
            finalString = value;
        }
        else{
            finalString = 'NA';
        }
        return finalString;
    }
    
    //Wrapper Class to Build JSON
     
     public class RequestCommonData {
        public String requestSourceId;
     }
     
     public class RepresentativeIndividual {

        public String companyNameAR;
        public String companyNameEN;
        public String currentNationality;
        public String previousNationality;
        public String gender;
        public String birthDate;
        public String birthCountry;
        public String birthCity;
        public List<String> mobileNumber;
        public String area;
        public Integer udbNumber;
        public String documentType;
        public String documentNumber;
        public String documentIssuePlace;
        public String documentIssueDate;
        public String documentExpiryDate;
        public String documentCopyFileId;
        public String residenceCopyFileId;
        public String address;
    }
    
    public class ShareholderIndividual {
        
        public string shareholderIndividualId;
        public String companyNameAR;
        public String companyNameEN;
        public String currentNationality;
        public String previousNationality;
        public String gender;
        public String birthDate;
        public String birthCountry;
        public String birthCity;
        public List<String> mobileNumber;
        public String area;
        public Integer udbNumber;
        public String documentType;
        public String documentNumber;
        public String documentIssuePlace;
        public String documentIssueDate;
        public String documentExpiryDate;
        public String documentCopyFileId;
        public String residenceCopyFileId;
        public String address;
        public Integer ownershipPercent;
        public Integer sharesNumber;
        public Integer sharesValue;
    }
    
    public class ParentCompanyData{
        
        public String companyNameAR;
        public String companyNameEN;
        public String workArea;
        public String registrationType ;
        public String companyPercentage ;
        public List<DirectorBoard> directorBoard;
        public String originCountry;
        public String location;
        public String position;
        public String representativeName;
        public Integer representativeNationality;
        public String representativeId;
        public String representativeContactNumber;
        public String employeesNumber;
        public String establishmentNumber;
        public String establishmentNumberImmSys;
        public String AccountNumberMultiplefields;
        public String Bankstatement;
        public String Offshorecontract;
        public Integer Establishmentcontract;
    
    }
    
    public class DirectorBoard {
        public String name;
        public String nationality;
    }
    
    
     public class BuildIssuanceDto {
        
        public RequestCommonData requestCommonData;
        public RepresentativeIndividual representativeIndividual;
        public List<ShareholderIndividual> shareholderIndividual;
        public String companyNameEN;
        public String companyNameAR;
        public String workArea;
        public String registrationType;
        public Integer companyPercentage;
        public List<DirectorBoard> directorBoard;
        public String originCountry;
        public String location;
        public String position;
        public String representativeName;
        public String representativeNationality;
        public String representativeId;
        public String representativeContactNumber;
        public Integer employeesNumber;
        public String establishmentNumber;
        public String establishmentNumberImmSys;
        public List<String> iBan;
        public String bankStatementFileId;
        public String shareholderType;
        public String representativeType;
        public List<ParentCompanyData> parentCompanyData;
     }
    
     public class IssuanceDto {
        public BuildIssuanceDto issuanceDto;
     }
}