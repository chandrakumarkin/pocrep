@isTest
private class CC_UpdateConvertedLeadSRContactTest {
    
    static testMethod void myUnitTest() {
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        //insertNewAccounts[0].Next_Renewal_Date__c = Date.today();
        insert insertNewAccounts;
        
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 
        
        License__c lic = new License__c(name='12345',Account__c=insertNewAccounts[0].id, License_Expiry_Date__c = Date.today());
        insert lic;
        
        insertNewAccounts[0].Active_License__c = lic.id;
        update insertNewAccounts;
        
        
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(3);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(3,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(3, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(4, new List<string> {'End','Draft','Submitted','AOR_REJECTED'}, new List<string> {'END','DRAFT','SUBMITTED','AOR_REJECTED'}, new List<string> {'End','','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(3, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[2].Lead_Id__c = string.valueof(insertNewLeads[2].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[2].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[2].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[2].Foreign_entity_registered_number__c = '3237';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].Passport_No__c = 'A12345';
        insertNewSRs[0].Nationality__c = 'India';
        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
        insertNewSRs[0].Gender__c = 'Male';
        insertNewSRs[0].Place_of_Birth__c = 'Dubai';
        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
        insertNewSRs[0].Last_Name__c = 'test';
        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Business_Sector__c = 'Hotel';
        insertNewSRs[0].entity_name__c = 'Tentity';
        insertNewSRs[0].Progress_Completion__c = 10.0;
        insertNewSRs[1].License_Application__c = lic.id;
        insertNewSRs[0].HexaBPM__Contact__c = con.id;
        insertNewSRs[0].HexaBPM__Send_SMS_to_Mobile__c = '+971512512512';
        // insertNewSRs[0].Passport_Expiry_Date__c = Date.today().addmonths(20);
        // insertNewSRs[0].Place_of_Issue__c = 'Dubai';
        insert insertNewSRs;
        
        insertNewSRs[0].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].HexaBPM__Customer__c = insertNewAccounts[0].id;
        insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs[0].id;
        insertNewSRs[0].HexaBPM__Parent_SR__c = insertNewSRs[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        insertNewSRs[2].HexaBPM__Parent_SR__c = insertNewSRs[1].id;
        insertNewSRs[2].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        update insertNewSRs;
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'REGISTRAR_REVIEW'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
        listSRSteps[0].HexaBPM__Step_No__c = 1;
        //listSRSteps[0].HexaBPM__Step_Template_Code__c = 'CLO_REVIEW';
        insert listSRSteps;
        
        
        
        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Awaiting Verification',HexaBPM__Code__c='AWAITING_VERIFICATION',HexaBPM__Type__c='Start');
        
        HexaBPM__Status__c status1 = new HexaBPM__Status__c
            (Name='Re-Upload Document',HexaBPM__Code__c='REUPLOAD_DOCUMENT',HexaBPM__Type__c='End');
        insert status1;
        
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
        insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        insertNewSteps[0].HexaBPM__Status__c = status.id;
        insertNewSteps[1].HexaBPM__Status__c = status1.id;
        // insertNewSteps[0].Step_Template_Code__c = 'CLO_REVIEW';
        insertNewSteps[0].Relationship_Manager__c = userinfo.getUserId();
        insertNewSteps[1].Relationship_Manager__c = userinfo.getUserId();
        insert insertNewSteps;
        
        
        HexaBPM__Transition__c transition = new HexaBPM__Transition__c(HexaBPM__From__c=status.id,HexaBPM__To__c=status1.id);
        insert transition;
        
        HexaBPM__SR_Status__c srStatus = new HexaBPM__SR_Status__c(Name='Awaiting Re-Upload',HexaBPM__Code__c='AWAITING_RE-UPLOAD');
        insert srStatus;
        
        HexaBPM__Step_Transition__c stepTransition = new HexaBPM__Step_Transition__c
            (HexaBPM__SR_Step__c=listSRSteps[0].id,HexaBPM__Transition__c=transition.id,HexaBPM__SR_Status_Internal__c=listSRStatus[1].id,HexaBPM__SR_Status_External__c=listSRStatus[1].id);
        insert stepTransition;
        
        Company_Name__c companyName = new Company_Name__c(Application__c = insertNewSRs[1].id,Status__c='Approved' , Entity_Name__c = insertNewAccounts[0].id,OB_Is_Trade_Name_Allowed__c='Yes');
        insert companyName;
        
        // create document master
        //List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        //listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'GENERAL'});
        //listDocMaster[0].HexaBPM__Code__c='NOC_SHARING';
        //insert listDocMaster;
        
        
        HexaBPM__Step__c step = [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Passport_No__c,HexaBPM__SR__r.Nationality__c,HexaBPM__SR__r.Date_of_Birth__c,
                                 HexaBPM__SR__r.Gender__c,HexaBPM__SR__r.Place_of_Birth__c,HexaBPM__SR__r.Date_of_Issue__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,HexaBPM__SR__r.HexaBPM__Parent_SR__c,
                                 HexaBPM__SR__r.Last_Name__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Business_Sector__c,HexaBPM__SR__r.entity_name__c,HexaBPM__SR__r.Setting_Up__c,
                                 HexaBPM__SR__r.First_Name__c,HexaBPM__SR__r.HexaBPM__Send_SMS_to_Mobile__c,HexaBPM__SR__r.Place_of_Issuance__c,HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__SR_Template__c,Relationship_Manager__c,
                                 HexaBPM__SR__r.Name_of_the_Hosting_Affiliate__c,HexaBPM__SR__r.Progress_Completion__c,HexaBPM__SR__r.HexaBPM__Contact__c,
                                 HexaBPM__SR__r.Sharing_Space_with_Affiliated_Entity__c,HexaBPM__SR_Step__c,HexaBPM__Status__c
                                 from HexaBPM__Step__c where Id=:insertNewSteps[0].Id  LIMIT 1];   
        
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile Where Name='DIFC Customer Community Plus User Custom' Limit 1];
        User user1 = new User(
            Username = System.now().millisecond() + 'test12345@test.com',
            ContactId = con.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'test12345@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'McTesty',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
        
        System.runAs(user1){
            CC_UpdateConvertedLeadSRContact CC_AutoCloseStepObj = new CC_UpdateConvertedLeadSRContact();
            CC_AutoCloseStepObj.EvaluateCustomCode(insertNewSRs[0], step); 
            
 
        }
        
        
        
        
        
    }
    
}