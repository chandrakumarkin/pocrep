@isTest
public class OB_RiskMatrixHelperTest 
{

     public static testmethod void calculateRiskTest()
     {
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(2);
        insert insertNewAccounts;
        
         //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        
         //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
       
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        //insertNewSRs[0].Entity_Type__c = 'Financial - related';
        //insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('In_Principle').getRecordTypeId();
        insert insertNewSRs;
        
        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1,new List<String>{'Low'},new List<Integer>{1}, new List<String>{'Low'});
        insert listCS;
        //create amendments
        list<HexaBPM_Amendment__c> listAmendments = new list<HexaBPM_Amendment__c>() ;
        listAmendments = OB_TestDataFactory.createApplicationAmendment(1,listCS,insertNewSRs);
        insert listAmendments;
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;
        
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insert insertNewSteps;
        //---------------------------------------------------------------------------------------------------
      
		 
         
          //create license
        License__c licenseObj = new License__c();
        licenseObj.Account__c = insertNewAccounts[0].Id;
        insert licenseObj;
         
         License_Activity__c testAct = new License_Activity__c();
         testAct.Activity__c = listLicenseActivityMaster[0].id;
         testAct.Account__c = insertNewAccounts[1].id;
         testAct.License__c = licenseObj.Id;
          insert testAct;
         
         list<License_Activity__c> testActList = new list<License_Activity__c>();
         testActList.add(testAct);
        
         
         
          //testAct.Activity__r.OB_DNFBP__c = 'yes';
         //update testAct;
         listLicenseActivityMaster[0].OB_Sell_or_Buy_Real_Estate__c = 'yes';
         update listLicenseActivityMaster[0];
         
         
             
       
         
         List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
         /*
        HexaBPM_Amendment__c objAmendment = new HexaBPM_Amendment__c();
        objAmendment = new HexaBPM_Amendment__c();
        objAmendment.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment.Nationality_list__c = 'India';
        objAmendment.Role__c = 'Shareholder';
         objAmendment.Type_of_Ownership_or_Control__c = 'The individual members of the Council who are deemed to be a UBO';
        objAmendment.Passport_No__c = '12345';
        objAmendment.ServiceRequest__c = insertNewSRs[1].Id;
        listAmendment.add(objAmendment);
         
         HexaBPM_Amendment__c objAmendment4 = new HexaBPM_Amendment__c();
        objAmendment4.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment4.Nationality_list__c = 'India';
        objAmendment4.Role__c = 'Director';
         objAmendment4.Type_of_Ownership_or_Control__c = 'The individual members of the Council who are deemed to be a UBO';
        objAmendment4.Passport_No__c = '12345';
        objAmendment4.ServiceRequest__c = insertNewSRs[1].Id;
        listAmendment.add(objAmendment4);
         
         HexaBPM_Amendment__c objAmendment3 = new HexaBPM_Amendment__c();
        objAmendment3.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment3.Nationality_list__c = 'India';
        objAmendment3.Role__c = 'Guardian';
        objAmendment3.Passport_No__c = '12345';
        objAmendment3.ServiceRequest__c = insertNewSRs[1].Id;
        listAmendment.add(objAmendment3);
        
         HexaBPM_Amendment__c objAmendment2 = new HexaBPM_Amendment__c();
        objAmendment2.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Individual');
        objAmendment2.Nationality_list__c = 'India';
        objAmendment2.Role__c = 'Authorised Signatory';
        objAmendment2.Passport_No__c = '12345';
         objAmendment2.Type_of_Authority__c = 'Jointly';
        objAmendment2.ServiceRequest__c = insertNewSRs[1].Id;
        listAmendment.add(objAmendment2);
        
		*/
         
        HexaBPM_Amendment__c objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1 = new HexaBPM_Amendment__c();
        objAmendment1.recordtypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
        objAmendment1.Nationality_list__c = 'India';
        objAmendment1.Registration_No__c = '1234';
        //objAmendment1.Role__c = 'Qualified Applicant';
        objAmendment1.Status__c = 'Active';
        objAmendment1.Is_this_Entity_registered_with_DIFC__c = 'Yes';
        objAmendment1.ServiceRequest__c = insertNewSRs[1].Id;
        listAmendment.add(objAmendment1);
        insert listAmendment;
         
          
         test.startTest();
         	OB_RiskMatrixHelper rskHelper = new OB_RiskMatrixHelper();
         	OB_RiskMatrixHelper.CalculateRisk(insertNewSRs[1].Id);
         
         	insertNewSRs[1].Entity_Type__c = 'Financial - related';
            update insertNewSRs[1];
         	
         	OB_RiskMatrixHelper.PrimaryChecks(insertNewSRs[1].Id);
         
            insertNewSRs[1].Ownership_Control_Structures_Include__c = 'test'; 
            insertNewSRs[1].Distribution_Channel_Countries__c = 'India';
            insertNewSRs[1].Parent_or_Entity_Business_Activities__c = 'test';
            //insertNewSRs[1].Distribution_Channel_Countries__c = 'test';
            update insertNewSRs[1];
         	OB_RiskMatrixHelper.OtherRiskRateTotal(insertNewSRs[1].Id);
         
           
         
         
         test.stopTest();
         
          
         
    }
    
    
}