@istest
public class Test_BatchTEODiscount {
	@testsetup
    public static void testsetupData (){
        List<Account> accList = new List<Account>();
        Account objAccount = new Account();
        objAccount.Name = 'TEO Company';
        objAccount.Executive_Company__c = true;
        objAccount.TEO_Discount__c = true;
        objAccount.ROC_reg_incorp_Date__c = Date.newInstance(2017, 11, 20);        
        accList.add(objAccount);
        
        objAccount = new Account();
        objAccount.Name = 'TEO Company1';
        objAccount.Executive_Company__c = true;
        objAccount.TEO_Discount__c = true;
        objAccount.ROC_reg_incorp_Date__c = Date.newInstance(2018, 11, 20);        
        accList.add(objAccount);
        
        insert accList;
    }
    
    @istest
    public static void testBatchTEOUpdate(){
        List<Account> accList = [SELECT Id, Name, Executive_Company__c, ROC_reg_incorp_Date__c, TEO_Discount__c FROM Account];
        test.startTest();
        Database.executeBatch(new BatchTEODiscount_3Yrs());
        String sch ='0 48 * * * ?'; 
        System.schedule('newJob', sch, new BatchTEODiscount_3Yrs());
        //System.enqueueJob(new BatchTEODiscount_3Yrs());
        test.stopTest();
    }
    
   @istest
    public static void testUpdateTEODiscountFlag(){
        List<Account> accList = [SELECT Id, Name, Executive_Company__c, ROC_reg_incorp_Date__c, TEO_Discount__c FROM Account];
        test.startTest();
        BatchTEODiscount_3Yrs.updateTEODiscount();
        test.stopTest();
    }
}