/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData = false)
global class Test_CC_CourierCls implements WebServiceMock {

    global void doInvoke(
        Object stub,
        Object request,
        Map < String, Object > response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {

        if (endpoint != null && endpoint.contains('tracking')) {
            WS_CourierTracking.TrackingResult TrackingResult = new WS_CourierTracking.TrackingResult();
            TrackingResult.WaybillNumber = '123556';
            TrackingResult.UpdateCode = '1212';
            TrackingResult.UpdateDescription = 'Delivered';
            TrackingResult.UpdateLocation = 'Test location';
            TrackingResult.Comments = 'Test Comments';
            TrackingResult.ProblemCode = 'No Problem';
            TrackingResult.UpdateDateTime = system.now();

            list < WS_CourierTracking.TrackingResult > lstTrackingElem = new list < WS_CourierTracking.TrackingResult > ();
            lstTrackingElem.add(TrackingResult);
            WS_CourierTracking.ArrayOfTrackingResult arrayoftrackingElem = new WS_CourierTracking.ArrayOfTrackingResult();
            arrayoftrackingElem.TrackingResult = lstTrackingElem;
            WS_CourierTracking.KeyValueOfstringArrayOfTrackingResultmFAkxlpY_element keyElement = new WS_CourierTracking.KeyValueOfstringArrayOfTrackingResultmFAkxlpY_element();
            keyElement.Key = '123456';
            keyElement.Value = arrayoftrackingElem;
            list < WS_CourierTracking.KeyValueOfstringArrayOfTrackingResultmFAkxlpY_element > arrayofKeyElem = new list < WS_CourierTracking.KeyValueOfstringArrayOfTrackingResultmFAkxlpY_element > ();
            arrayofKeyElem.add(keyElement);
            WS_CourierTracking.ArrayOfKeyValueOfstringArrayOfTrackingResultmFAkxlpY arrayTrackingElem = new WS_CourierTracking.ArrayOfKeyValueOfstringArrayOfTrackingResultmFAkxlpY();
            arrayTrackingElem.KeyValueOfstringArrayOfTrackingResultmFAkxlpY = arrayofKeyElem;

            WS_CourierTracking.ShipmentTrackingResponse_element Resp = new WS_CourierTracking.ShipmentTrackingResponse_element();
            Resp.hasErrors = false;
            Resp.Transaction_x = null;
            Resp.TrackingResults = arrayTrackingElem;
            response.put('response_x', Resp);

        } else if (endpoint != null && endpoint.contains('Shipping')) {
            WS_Courier.ProcessedShipment processedShipment = new WS_Courier.ProcessedShipment();
            processedShipment.id = 'ABC00011';
            processedShipment.ForeignHAWB = 'shp-000111';
            processedShipment.HasErrors = false;
            List < WS_Courier.ProcessedShipment > lstProcessedShp = new List < WS_Courier.ProcessedShipment > ();
            lstProcessedShp.add(processedShipment);

            WS_Courier.ArrayOfProcessedShipment arrayofShp = new WS_Courier.ArrayOfProcessedShipment();
            arrayofShp.ProcessedShipment = lstProcessedShp;

            WS_Courier.ShipmentCreationResponse_element Resp = new WS_Courier.ShipmentCreationResponse_element();
            Resp.Shipments = arrayofShp;
            Resp.Transaction_x = null;
            Resp.hasErrors = false;
            response.put('response_x', Resp);
        }


    }

    static testMethod void myUnitTest() {

        list < Endpoint_URLs__c > lstCS = new list < Endpoint_URLs__c > ();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'Aramex';
        objEP.URL__c = 'https://ws.aramex.net/ShippingAPI/Shipping/Service_1_0.svc';
        lstCS.add(objEP);
        insert lstCS;

        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;

        Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;

        Status__c sts = new Status__c();
        sts.Name = 'Delivered';
        sts.Code__c = 'DELIVERED';
        sts.Type__c = 'End';
        insert sts;

        SR_Status__c SRsts = new SR_Status__c();
        SRsts.Name = 'Delivered';
        SRsts.Code__c = 'Delivered';
        SRsts.Type__c = 'End';
        insert SRsts;

        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c = 'Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;

        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;

        Step_Template__c stpTem = new Step_Template__c();
        stpTem.Name = 'Courier Delivery';
        stpTem.Code__c = 'Courier_Delivery';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Delivery';
        stpTem.Courier_Type__c = 'Delivery';
        insert stpTem;

        map < string, string > mapRecordType = new map < string, string > ();
        for (RecordType objRT: [select Id, DeveloperName from RecordType where DeveloperName IN('Application_of_Registration', 'Change_Shareholder_Details', 'Sale_or_Transfer_of_Shares_or_Membership_Interest', 'Allotment_of_Shares_Membership_Interest')]) {
            mapRecordType.put(objRT.DeveloperName, objRT.Id);
        }

        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.legal_structures__c = 'LTD SPC';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c = 90;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR1.RecordTypeId = mapRecordType.get('Application_of_Registration');
        objSR1.Consignee_FName__c = 'Test';
        objSR1.Consignee_LName__c = 'Test';
        //objSR1.Send_SMS_To_Mobile__c = '+97112345670';
        objSR1.Use_Registered_Address__c = false;
        objSR1.Apt_or_Villa_No__c = 'testing123';
        objSR1.Email__c = 'test@test.com';
        objSR1.Avail_Courier_Services__c = 'Yes';
        objSR1.Courier_Cell_Phone__c = '+97112345670';
        objSR1.Courier_Mobile_Number__c = '+97112345670';
        insert objSR1;

        Step__c objStp1 = new Step__c();
        objStp1.SR__c = objSR1.Id;
        objStp1.Step_Template__c = stpTem.id;
        objStp1.SR_Group__c = 'ROC';
        insert objStp1;

        Shipment__c shp = new Shipment__c();
        shp.status__c = 'Ready';
        shp.Courier_Type__c = 'Delivery';
        shp.Airway_Bill_No__c = '123456';
        shp.Step__c = objStp1.id;
        shp.Express_Service__c = true;
        //insert shp;

        Step__c stp1 = [select id, Name, SR__c, SR_Group__c, SR__r.Courier_Cell_Phone__c, SR__r.Courier_Mobile_Number__c, SR__r.Consignee_FName__c, SR__r.Consignee_LName__c, SR__r.Avail_Courier_Services__c, SR__r.Send_SMS_To_Mobile__c, SR__r.Apt_or_Villa_No__c, SR__r.Customer__r.Office__c, SR__r.Customer__r.Building_Name__c, SR__r.Customer__r.Name, SR__r.Email__c, SR__r.Customer__c, SR__r.Use_Registered_Address__c, SR__r.Express_Service__c, Step_Template__r.Courier_Type__c from Step__c where Id =: objStp1.Id];

        test.startTest();
        Test.setMock(WebServiceMock.class, new Test_CC_CourierCls());
        CC_CourierCls.CreateShipment(objStp1.id);
        CC_CourierCls.CreateShipmentNormal(objStp1.id);
        //database.executeBatch(new CourierTracking_Cls(), 1);
        //CourierTracking_Cls.TrackShipment();
        test.stopTest();

    }

    static testMethod void myUnitTestGS() {

        list < Endpoint_URLs__c > lstCS = new list < Endpoint_URLs__c > ();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;

        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;

        Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;

        Status__c sts = new Status__c();
        sts.Name = 'Delivered';
        sts.Code__c = 'DELIVERED';
        sts.Type__c = 'End';
        insert sts;

        SR_Status__c SRsts = new SR_Status__c();
        SRsts.Name = 'Delivered';
        SRsts.Code__c = 'Delivered';
        SRsts.Type__c = 'End';
        insert SRsts;

        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c = 'Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;

        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;

        Step_Template__c stpTem = new Step_Template__c();
        stpTem.Name = 'Courier Collection';
        stpTem.Code__c = 'Courier Collection';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Collection';
        stpTem.Courier_Type__c = 'Collection';
        insert stpTem;

        map < string, string > mapRecordType = new map < string, string > ();
        for (RecordType objRT: [select Id, DeveloperName from RecordType where DeveloperName IN('Application_of_Registration', 'Change_Shareholder_Details', 'Sale_or_Transfer_of_Shares_or_Membership_Interest', 'Allotment_of_Shares_Membership_Interest')]) {
            mapRecordType.put(objRT.DeveloperName, objRT.Id);
        }

        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.legal_structures__c = 'LTD SPC';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c = 90;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR1.RecordTypeId = mapRecordType.get('Application_of_Registration');
        objSR1.Consignee_FName__c = 'Test';
        objSR1.Consignee_LName__c = 'Test';
        //objSR1.Send_SMS_To_Mobile__c = '+97112345670';
        objSR1.Use_Registered_Address__c = false;
        objSR1.Apt_or_Villa_No__c = 'testing123';
        objSR1.Email__c = 'test@test.com';
        objSR1.Avail_Courier_Services__c = 'Yes';
        objSR1.Courier_Cell_Phone__c = '+97112345670';
        objSR1.Courier_Mobile_Number__c = '+97112345670';
        insert objSR1;

        Step__c objStp1 = new Step__c();
        objStp1.SR__c = objSR1.Id;
        objStp1.Step_Template__c = stpTem.id;
        objStp1.SR_Group__c = 'GS';
        insert objStp1;
        
        pending__c pen = new pending__c();
        pen.Type_Of_Courier__c ='Delivery';
        pen.Is_Courier_Related__c = true;
        pen.End_Date__c= null;
        insert pen;

        Shipment__c shp = new Shipment__c();
        shp.status__c = 'Ready';
        shp.Courier_Type__c = 'Delivery';
        shp.Airway_Bill_No__c = '123456';
        shp.Step__c = objStp1.id;
        shp.Express_Service__c = true;
        //insert shp;

        Step__c stp1 = [select id, Name, SR__c, SR_Group__c, SR__r.Courier_Cell_Phone__c, SR__r.Courier_Mobile_Number__c, SR__r.Consignee_FName__c, SR__r.Consignee_LName__c, SR__r.Avail_Courier_Services__c, SR__r.Send_SMS_To_Mobile__c, SR__r.Apt_or_Villa_No__c, SR__r.Customer__r.Office__c, SR__r.Customer__r.Building_Name__c, SR__r.Customer__r.Name, SR__r.Email__c, SR__r.Customer__c, SR__r.Use_Registered_Address__c, SR__r.Express_Service__c, Step_Template__r.Courier_Type__c from Step__c where Id =: objStp1.Id];

        test.startTest();
        Test.setMock(WebServiceMock.class, new Test_CC_CourierCls());
        CC_CourierCls.CreateShipment(objStp1.id);
        CC_CourierCls.CreateShipmentNormal(objStp1.id);
        //database.executeBatch(new CourierTracking_Cls(), 1);
        //CourierTracking_Cls.TrackShipment();
        test.stopTest();

    }

    static testMethod void myUnitTest1() {

        Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;

        Status__c sts = new Status__c();
        sts.Name = 'Delivered';
        sts.Code__c = 'DELIVERED';
        sts.Type__c = 'End';
        insert sts;

        SR_Status__c SRsts = new SR_Status__c();
        SRsts.Name = 'Delivered';
        SRsts.Code__c = 'Delivered';
        SRsts.Type__c = 'End';
        insert SRsts;

        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c = 'Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;

        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;

        Step_Template__c stpTem = new Step_Template__c();
        stpTem.Name = 'Courier Delivery';
        stpTem.Code__c = 'Courier_Delivery';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Delivery';
        stpTem.Courier_Type__c = 'Delivery';
        insert stpTem;

        map < string, string > mapRecordType = new map < string, string > ();
        for (RecordType objRT: [select Id, DeveloperName from RecordType where DeveloperName IN('Application_of_Registration', 'Change_Shareholder_Details', 'Sale_or_Transfer_of_Shares_or_Membership_Interest', 'Allotment_of_Shares_Membership_Interest')]) {
            mapRecordType.put(objRT.DeveloperName, objRT.Id);
        }

        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.legal_structures__c = 'LTD SPC';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c = 90;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR1.RecordTypeId = mapRecordType.get('Application_of_Registration');
        objSR1.Consignee_FName__c = 'Test';
        objSR1.Consignee_LName__c = 'Test';
        //objSR1.Send_SMS_To_Mobile__c = '+97112345670';
        objSR1.Use_Registered_Address__c = false;
        objSR1.Apt_or_Villa_No__c = 'testing123';
        objSR1.Email__c = 'test@test.com';
        objSR1.Avail_Courier_Services__c = 'Yes';
        objSR1.Courier_Cell_Phone__c = '+97112345670';
        objSR1.Courier_Mobile_Number__c = '+97112345670';
        insert objSR1;

        Step__c objStp1 = new Step__c();
        objStp1.SR__c = objSR1.Id;
        objStp1.Step_Template__c = stpTem.id;
        objStp1.SR_Group__c = 'ROC';
        insert objStp1;

        Shipment__c shp = new Shipment__c();
        shp.status__c = 'Ready';
        shp.Courier_Type__c = 'Delivery';
        shp.Airway_Bill_No__c = '123456';
        shp.Step__c = objStp1.id;
        shp.Express_Service__c = true;
        insert shp;

        Step__c stp1 = [select id, Name, SR__c, SR_Group__c, SR__r.Courier_Cell_Phone__c, SR__r.Courier_Mobile_Number__c, SR__r.Consignee_FName__c, SR__r.Consignee_LName__c, SR__r.Avail_Courier_Services__c, SR__r.Send_SMS_To_Mobile__c, SR__r.Apt_or_Villa_No__c, SR__r.Customer__r.Office__c, SR__r.Customer__r.Building_Name__c, SR__r.Customer__r.Name, SR__r.Email__c, SR__r.Customer__c, SR__r.Use_Registered_Address__c, SR__r.Express_Service__c, Step_Template__r.Courier_Type__c from Step__c where Id =: objStp1.Id];

        test.startTest();
        Test.setMock(WebServiceMock.class, new Test_CC_CourierCls());
        database.executeBatch(new CourierTracking_Cls(), 1);
        test.stopTest();

    }
    static testMethod void myUnitTest2() {

        Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;

        Status__c sts = new Status__c();
        sts.Name = 'Delivered';
        sts.Code__c = 'DELIVERED';
        sts.Type__c = 'End';
        insert sts;

        SR_Status__c SRsts = new SR_Status__c();
        SRsts.Name = 'Delivered';
        SRsts.Code__c = 'Delivered';
        SRsts.Type__c = 'End';
        insert SRsts;

        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c = 'Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;

        map < string, string > mapRecordType = new map < string, string > ();
        for (RecordType objRT: [select Id, DeveloperName from RecordType where DeveloperName IN('Application_of_Registration', 'Change_Shareholder_Details', 'Sale_or_Transfer_of_Shares_or_Membership_Interest', 'Allotment_of_Shares_Membership_Interest')]) {
            mapRecordType.put(objRT.DeveloperName, objRT.Id);
        }

        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.legal_structures__c = 'LTD SPC';
        objSR.currency_list__c = 'US Dollar';
        objSR.Share_Capital_Membership_Interest__c = 90;
        objSR.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR.Consignee_FName__c = 'Test';
        objSR.Consignee_LName__c = 'Test';
        //objSR.Send_SMS_To_Mobile__c = '+97112345670';
        objSR.Use_Registered_Address__c = false;
        objSR.Apt_or_Villa_No__c = 'testing123';
        objSR.Email__c = 'test@test.com';
        objSR.Avail_Courier_Services__c = 'Yes';
        objSR.Courier_Cell_Phone__c = '+97112345670';
        objSR.Courier_Mobile_Number__c = '+97112345670';
        objSR.SR_Group__c = 'GS';
        insert objSR;

        Step__c objStp = new Step__c();
        objStp.SR__c = objSR.Id;
        objStp.SR_Group__c = 'GS';
        insert objStp;

        Shipment__c shp1 = new Shipment__c();
        shp1.status__c = 'Ready';
        shp1.Courier_Type__c = 'Delivery';
        shp1.Airway_Bill_No__c = '123456';
        shp1.Step__c = objStp.id;
        insert shp1;

        test.startTest();
        Test.setMock(WebServiceMock.class, new Test_CC_CourierCls());
        database.executeBatch(new CourierTracking_Cls(), 1);
        test.stopTest();

    }

    static testMethod void myUnitTest3() {
        list < Endpoint_URLs__c > lstCS = new list < Endpoint_URLs__c > ();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;

        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;

        Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;

        list < Status__c > lstStatus = new list < Status__c > ();

        Status__c sts = new Status__c();
        sts.Name = 'PENDING_COLLECTION';
        sts.Code__c = 'PENDING_COLLECTION';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'PENDING_DELIVERY';
        sts.Code__c = 'PENDING_DELIVERY';
        lstStatus.add(sts);
        insert lstStatus;

        SR_Status__c SRsts = new SR_Status__c();
        SRsts.Name = 'Delivered';
        SRsts.Code__c = 'Delivered';
        SRsts.Type__c = 'End';
        insert SRsts;

        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c = 'Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;

        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;

        list < Step_Template__c > lstStepTemplates = new list < Step_Template__c > ();

        Step_Template__c stpTem = new Step_Template__c();
        stpTem.Name = 'GS_COURIER_COLLECTION';
        stpTem.Code__c = 'GS_COURIER_COLLECTION';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Delivery';
        stpTem.Courier_Type__c = 'Collection';
        lstStepTemplates.add(stpTem);
        stpTem = new Step_Template__c();
        stpTem.Name = 'GS_COURIER_DELIVERY';
        stpTem.Code__c = 'GS_COURIER_DELIVERY';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Delivery';
        stpTem.Courier_Type__c = 'Collection';
        lstStepTemplates.add(stpTem);
        insert lstStepTemplates;

        map < string, string > mapRecordType = new map < string, string > ();
        for (RecordType objRT: [select Id, DeveloperName from RecordType where DeveloperName IN('Application_of_Registration', 'Change_Shareholder_Details', 'Sale_or_Transfer_of_Shares_or_Membership_Interest', 'Allotment_of_Shares_Membership_Interest')]) {
            mapRecordType.put(objRT.DeveloperName, objRT.Id);
        }

        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.legal_structures__c = 'LTD SPC';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c = 90;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR1.Consignee_FName__c = 'Test';
        objSR1.Consignee_LName__c = 'Test';
        //objSR1.Send_SMS_To_Mobile__c = '+97112345670';
        objSR1.Use_Registered_Address__c = false;
        objSR1.Apt_or_Villa_No__c = 'testing123';
        objSR1.Email__c = 'test@test.com';
        objSR1.Avail_Courier_Services__c = 'Yes';
        objSR1.Courier_Cell_Phone__c = '+97112345670';
        objSR1.Courier_Mobile_Number__c = '+97112345670';
        objSR1.SR_Group__c = 'GS';
        objSR1.SAP_Unique_No__c = '1234567890';
        insert objSR1;

        Step__c objStp1 = new Step__c();
        objStp1.SR__c = objSR1.Id;
        //objStp1.Step_Template__c = lstStepTemplates[1].id;
        objStp1.SR_Group__c = 'GS';
        insert objStp1;

        Step__c objStp2 = new Step__c();
        objStp2.SR__c = objSR1.Id;
        objStp2.Step_Template__c = lstStepTemplates[1].id;
        objStp2.SR_Group__c = 'GS';
        objStp2.Parent_Step__c = objStp1.Id;
        objStp2.SAP_CTEXT__c ='Test';
        insert objStp2;
        
        pending__C pen = new Pending__C();
        pen.step__c = objStp2.id;
        pen.Is_Courier_Related__c = true;
        pen.Service_Request__c = objSR1.id;
        pen.Type_Of_Courier__c = 'Delivery';
        
        insert pen;

        Shipment__c shp = new Shipment__c();
        shp.status__c = 'Ready';
        shp.Courier_Type__c = 'Delivery';
        shp.Airway_Bill_No__c = '123456';
        shp.Step__c = objStp1.id;
        shp.Express_Service__c = true;
        shp.Consignee_Name__c ='Veera';
        shp.Shipper_Name__c ='DIFC';
        insert shp;
        shp.Status__c = 'Delivered';
        shp.Created_On__c = system.now();
        shp.Courier_Status_Updated_on__c = system.now();
        test.startTest();
        //Test.setMock(WebServiceMock.class, new Test_CC_CourierCls());
        Test.setMock(WebServiceMock.class, new TestGsCRMServiceMock());
        CC_CourierCls.ShipmentInfo objShipmentInfo = new CC_CourierCls.ShipmentInfo();
        objShipmentInfo.Steps = new list < Step__c > ();
        objShipmentInfo.Steps.add(objStp2);

        objShipmentInfo.Shipments = new list < Shipment__c > ();
        objShipmentInfo.Shipments.add(shp);

        list < SAPGSWebServices.ZSF_S_GS_SERV_OP > lstGSItems = new list < SAPGSWebServices.ZSF_S_GS_SERV_OP > ();
        SAPGSWebServices.ZSF_S_GS_SERV_OP objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'N';
        objGSItem.SOPRP = 'N';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        //objGSItem.SGUID = SRItemId;
        lstGSItems.add(objGSItem);
        objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'P';
        objGSItem.SOPRP = 'N';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        //objGSItem.SGUID = SRItemId;
        lstGSItems.add(objGSItem);
        objGSItem = new SAPGSWebServices.ZSF_S_GS_SERV_OP();
        objGSItem.ACCTP = 'P';
        objGSItem.SOPRP = 'P';
        objGSItem.BANFN = '50001234';
        objGSItem.VBELN = '05001234';
        objGSItem.BELNR = '1234567';
        objGSItem.WSTYP = 'SERV';
        objGSItem.MATNR = 'GO-00001';
        objGSItem.UNQNO = '12345675000215';
        //objGSItem.SGUID = SRItemId;
        lstGSItems.add(objGSItem);

        TestGsCRMServiceMock.lstGSPriceItems = lstGSItems;

        CC_CourierCls.PushShipmentToSAP(objShipmentInfo, 'Change');
        CC_CourierCls.PushShipmentToSAP(objShipmentInfo, 'Close');
        CC_CourierCls.Run();
        test.stopTest();

    }


    static testmethod void myUnitTest4() {

        WS_Courier.Attachment attch = new WS_Courier.Attachment();
        WS_Courier.ArrayOfAttachment arayofAttach = new WS_Courier.ArrayOfAttachment();
        WS_Courier.ShipmentLabel shpLabel = new WS_Courier.ShipmentLabel();
        WS_Courier.ArrayOfNotification arayofNot = new WS_Courier.ArrayOfNotification();
        WS_Courier.ArrayOfDimensions arayOfDimen = new WS_Courier.ArrayOfDimensions();
        WS_Courier.ShipmentItem shp = new WS_Courier.ShipmentItem();
        WS_Courier.Money mon = new WS_Courier.Money();
        WS_Courier.Volume vol = new WS_Courier.Volume();
        WS_Courier.ArrayOfProcessedShipmentAttachment arayofProc = new WS_Courier.ArrayOfProcessedShipmentAttachment();
        WS_Courier.ScheduledDelivery schd = new WS_Courier.ScheduledDelivery();
        WS_Courier.ProcessedShipmentAttachment procShip = new WS_Courier.ProcessedShipmentAttachment();


    }

}