@isTest
public class EnterGIINController_Test {
    
    static testMethod void TestEnterGIINController(){
        
          Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        objAccount.ROC_Status__c = 'Active';
        objAccount.Financial_Year_End__c = '31st December';
        //objAccount.Active_License__c = objLic.id;
        objAccount.GIIN__c = '12345';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
            
         Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
         User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com'
                        );
        insert objUser;
        
        System.runAs(objUser){
            EnterGIINController thisclass = new EnterGIINController();
            thisclass.save();
        }
    }

}