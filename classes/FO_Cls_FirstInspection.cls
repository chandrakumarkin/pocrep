/******************************************************************************************
 *  Author   : Swati Sehrawat
 *  Company  : NSI JLT
 *  Date     : 05-Jan-2016
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    01/05/2016  Swati 		  Created
V1.1	03/22/2016  Claude		  Added logic to check if Contractor is applicable
V1.2	03/29/2016  Claude		  Added logic to get Contractor fields from the main SR
V1.3	04/04/2016	Claude		  Added logic to check if an existing inspection exists
V1.4	04/05/2016  Claude		  Moved SR Doc generation to CC_cls_FitOutandEventCustomCode
V1.5	09/06/2016	Claude		  Added logic to get checklist information
V1.6    15/08/2016	Claude		  Made revisions for displaying formatted date; Fixed bug on retrieving old checklist values; code refactoring
V1.7	19/09/2016	Claude		  Added sorting logic for lookup question
****************************************************************************************************************/
//Class to inspect checklist for first fit out inspection in DIFC
public without sharing class FO_Cls_FirstInspection {
    
    /**
     * This will store the service request
     * the step came from
     */
    public Service_Request__c srObj 								{get;set;}
    
    /**
     * This will store the actual step
     */
    public step__c stepObj 											{get;set;}
    
    /**
     * This will store the fit-out inspection
     * checklist records
     */
    public Fit_Out_Inspection__c foObj 								{get;set;}
    
    /**
     * This will store the questions to be 
     * displayed in the page
     */
    public list<questionWrapper> questionsList 						{get;set;}
    
    /**
     * This will store the checklist type
     */
    public string checkListType 									{get;set;}
    
    /**
     * This will store the service request ID
     */
    public string serviceRequestId 									{get;set;}
    
    /**
     * This will store the step ID for the
     * checklist
     */
    public string stepId 											{get;set;}
    
    /**
     * This will the type of service request
     */
    public string typeOfSR 											{get;set;}
    
    /**
     * This will render the form components 
     * whether in EDIT mode or READ-ONLY
     * mode
     */
    public string showresult 										{get;set;}
    
    /**
     * This will store the name of the 
     * current contractor
     */
    public string contractor 										{get; private set;}
    
    /* Inspection Checklist checkboxes */
    public boolean readjustment 									{get;set;}
    public boolean accepted 										{get;set;}
    public boolean reinspection 									{get;set;}
    
    public boolean setup 											{get;set;}
    public boolean handOver 										{get;set;}
    
    /**
     * This will store the selected date 
     * in the form
     */
    public dateTime inspectionDate 									{get;set;}
    
    /**
     * This will store a formatted version
     * of the date
     */
    public String inspectionDateFormatted							{get; private set;} // V1.6 <- Claude - Added new String field for instant formatting		
    
    /* V1.5 - Claude - Checklist information values */ 
    public String refNum											{get;set;}
    public String updatedDate										{get;set;}
    public String checkListHeader									{get;set;}
    
    /**
     * This will store the previous steps
     * with the same type
     */
    private List<Step__c> previousStep;
    
    /**
     * This will store the checklist version 
     */
    private String checklistVersion;
    
    /**
     * This will store the current inspection checklist
     * values
     */
    private List<Fit_Out_Inspection__c> currentList;
    
    /**
     * This will store the step ID to be used for the
     * queries
     */
    private String stepIdQuery;
    
    /* Class constructor */ 
    public FO_Cls_FirstInspection(){
        init();
    }
    
    /**
     * Sets the inspection date whenever the value is changed
     * @author	Claude Manahan. NSI-DMCC
     * @date	06/02/2016
     */
    public void setInspectionDate(){
    	foObj.Inspection_Date__c = inspectionDate == null ? System.Now() : inspectionDate; 
    }
    
    /**
     * Initializes class variables and collections
     * @author	Claude Manahan. NSI-DMCC
     * @date	06/02/2016
     */
    private void init(){
    	
    	try{
            
            initializeValues();	// Initialize the global and inner variables
            
            /* Initialize values for processing */
            initializeSR();
            initializeCheckList();
            initializeSRType();
            initializeStep();
            getUrlParameters();
            
            setChecklist();		// Set the checklist
            
            setChecklistInfo(); // V1.5
            
        } catch(exception ex){
            system.debug('---line---'+ex.getLineNumber());
            System.debug('Error: ' + ex.getMessage());
        }
    }
    
    /**
     * Sets the cheklist
     * @author	Claude Manahan. NSI-DMCC
     * @date	06/02/2016
     */
    private void setChecklist(){
    	
    	/* Check the version */
    	System.debug('Current Version: ' + checklistVersion);
        System.debug('Is Current Version blank? ' + checklistVersion == null);
        
        initializeCurrentChecklist();
        
        if(isReadOnly()){
            setChecklistResult();
        } else if(!currentList.isEmpty()){
        	setCurrentChecklistValues();
        } else if(hasPreviousStep() && currentList.isEmpty()){
            setPreviousValues();
        } 
        
        setChecklistDetails();
    }
    
    /**
     * Sets the other details of the checklist
     */
    private void setChecklistDetails(){
    	
    	System.debug('Is there a current list? ' + !currentList.isEmpty());
    	
    	if(!currentList.isEmpty()){
    		
	      foObj.idama_comments__c = currentList[0].idama_comments__c;
	      foObj.IDAMA_HSE_name__c = currentList[0].IDAMA_HSE_name__c;
	      foObj.IDAMA_FO_Mechanical__c = currentList[0].IDAMA_FO_Mechanical__c;
	      foObj.IDAMA_FO_Civil__c = currentList[0].IDAMA_FO_Civil__c;
	      foObj.IDAMA_FO_Electrical__c = currentList[0].IDAMA_FO_Electrical__c;
	      foObj.Client_Name__c = currentList[0].Client_Name__c;
	      foObj.Contractor_Name__c = currentList[0].Contractor_Name__c;
	      foObj.Inspection_Date__c = currentList[0].Inspection_Date__c;
	      foObj.IDAMA_FO_Name__c = currentList[0].IDAMA_FO_Name__c;
	      foObj.IDAMA_HSE_Position__c = currentList[0].IDAMA_HSE_Position__c;
	      foObj.IDAMA_FO_Position__c = currentList[0].IDAMA_FO_Position__c;
	      
	      if(currentList[0].Inspection_Status__c == 'Adjustment Required'){
	          readjustment = true;
	      }
	      if(currentList[0].Inspection_Status__c == 'Inspection Passed'){
	          accepted = true;
	      }
	      if(currentList[0].Inspection_Status__c == 'Re-inspection Required'){
	          reinspection = true;
	      }
	      
	      if(foObj.Inspection_Date__c == null){
	        foObj.Inspection_Date__c = System.now();  
	      } 
	      
	      inspectionDate = foObj.Inspection_Date__c;
	      inspectionDateFormatted = foObj.Inspection_Date__c.format('dd/MM/YYYY hh:mm a','Asia/Dubai');
	      
		} 
    }
    
    /**
     * Sets the checklist informatiom
     * @author		Claude Manahan. NSI-DMCC
     * @date		06/09/2016
     */
    private void setChecklistInfo(){
    	
    	Checklist_Information__c checkListInfo = FO_cls_InspectionChecklist.getChecklistInformation(checkListType);
    	
    	if(checkListInfo != null){ 
    		refNum = checkListInfo.Reference_Number__c;
    		updatedDate = checkListInfo.Update_Date__c.format();
    		checkListHeader = checkListInfo.Checklist_Header__c;
    	}
    }
    
    /**
     * Sets the checklist with the most recent values
     * has been submitted
     * @author	Claude Manahan. NSI-DMCC
     * @date	06/02/2016
     */
    private void setCurrentChecklistValues(){
    	
    	System.debug('Getting current values:');
        initializeInspectionList(); // V1.3 - Claude - gets any currently existing values saved for the checklist
    }
    
    /**
     * Sets the checklist as a READ-ONLY
     * has been submitted
     * @author	Claude Manahan. NSI-DMCC
     * @date	06/02/2016
     */
    private void setChecklistResult(){
    	
    	System.debug('--Viewing Checklist in READ-ONLY Mode--');
        currentList = relatedInspectionList(checklistVersion);
        
        if(!currentList.isEmpty()){
        	initializeInspectionList();	
        }
    }
    
    /**
     * Checks if the checklist is submitted and set to READ-ONLY
     * has been submitted
     * @author	Claude Manahan. NSI-DMCC
     * @date	06/02/2016
     * @return	isReadOnly		flag to check if the checklist is read-only
     */
    private Boolean isReadOnly(){
    	return string.isNotBlank(ShowResult) && ShowResult.contains('true');
    }
    
    /**
     * Checks if the checklist has no previous version
     * has been submitted
     * @author	Claude Manahan. NSI-DMCC
     * @date	06/02/2016
     * @return	hasNoPreviousVersion		flag to check if the checklist has no previous versions
     */
    private Boolean hasNoPreviousVersion(){
    	return String.isBlank(checklistVersion);
    }
    
    /**
     * Gets the current values of the checklist before it 
     * has been submitted
     * @author	Claude Manahan. NSI-DMCC
     * @date	06/02/2016
     */
    private void initializeCurrentChecklist(){
    	
    	currentList = new List<Fit_Out_Inspection__c>();
    	
    	String checklistVer = hasNoPreviousVersion() ? '' : checklistVersion;
    	
    	System.debug('Initializing latest version -->' + checklistVer);
    	currentList = relatedInspectionList(checklistVer);
		
		if(currentList.isEmpty()){
        	getPreviousStep(); // V1.3 - Query a previous step, if any	
        }
    }
    
    /**
     * Initializes the class members accessed in the page
     * @author	Claude Manahan. NSI-DMCC
     * @date	06/02/2016
     */
    private void initializeValues(){
    	
    	/* Instantiate objects and collections */
    	foObj =  new Fit_Out_Inspection__c();
    	previousStep = new List<Step__c>();
        questionsList = new list<questionWrapper>();
        currentList = new List<Fit_Out_Inspection__c>();
        
    	//V1.5 - Claude - initialized info strings
    	refNum = '';
    	updatedDate = '';
    	checkListHeader = '';
    	contractor = '';
        checklistVersion = '';
        stepIdQuery = '';
    	
    	//V1.6 - Claude - initialze boolean flags
    	setup = false;
    	handover = false;
    	
    	inspectionDate = system.now();
    	foObj.Inspection_Date__c = System.now();
    	
    }
    
    /**
     * Gets the URL Paramters passed to the page
     * @author	Claude Manahan. NSI-DMCC
     * @date	06/02/2016
     */
    private void getUrlParameters(){
    	
    	ShowResult = ApexPages.CurrentPage().GetParameters().Get('ShowResult');
        checklistVersion = ApexPages.CurrentPage().GetParameters().Get('version');
    }
    
    /**
     * //V1.3 - Claude - new method
     * Sets the checklist values from a previous step
     * @author	Claude Manahan. NSI-DMCC
     * @date	04/04/2016
     */
    private void setPreviousValues(){
    	
    	System.debug('Getting previous values');
    	System.debug('Retrieving previous step. Reference >> ' + previousStep);
    	
        currentList = getPreviousStepChecklist();
        
        for(questionWrapper question : questionsList){
        	
        	for(Fit_Out_Inspection__c inspectionQuestionPrev : currentList){
        		
        		if(inspectionQuestionPrev.description__c.equals(question.lookupOBj.Description__c)){
        			
        			question.yes = inspectionQuestionPrev.Options__c.equals('Yes');
        			question.no = inspectionQuestionPrev.Options__c.equals('No');
        			question.na =  inspectionQuestionPrev.Options__c.equals('N/A');
        		}
        		
        	}
        	
        }
    }
    
    /**
     * //V1.3 - Claude - new method
     * Get the checklist values of the previous step
     * @author	Claude Manahan. NSI-DMCC
     * @date	04/04/2016
     * @return	previousStepChecklist
     */
    private list<Fit_Out_Inspection__c> getPreviousStepChecklist(){		
    	
   		return relatedInspectionList(previousStep[0].Collection_Batch_Number__c);
    }
    
    /**
     * //V1.3 - Claude - new method
     * Get the previous steps
     * @author	Claude Manahan. NSI-DMCC
     * @date	04/04/2016
     */
    private void getPreviousStep(){
    	System.debug('Getting Previous Step >>');
    	previousStep = new List<Step__c>(
    					[SELECT Id, 
						        Step_Name__c, 
						        units__c,
						        Collection_Batch_Number__c
								FROM Step__c WHERE
								ID != :stepId AND 
								SR_Step__c = :stepObj.SR_Step__c AND
								SR__C = :serviceRequestId ORDER BY Name DESC]);
    }
    
    /**
     * //V1.3 - Claude - new method
     * Checks if the inspection checklist step has a previous similar step
     * @author	Claude Manahan. NSI-DMCC
     * @date	04/04/2016
     * @return  hasPreviousStep		flag if the step has a previous related step
     */
    private Boolean hasPreviousStep(){
    	return !previousStep.isEmpty();
    }
    
    /**
     * Submits the current inspection values
     */
    public pageReference submitInspection(){
    	
    	/* Create a savepoint */
        Savepoint sp = Database.setSavepoint();
        
        try{
        	
        	/* This will store the version of the checklist to be saved */
        	String version = '';
        	
        	/* Prepare a pagereference instance */
            pageReference redirectPage = null;
            
            /* This will store the inspection checklist values to be stored */
            list<Fit_Out_Inspection__c> quesToInsert = new list<Fit_Out_Inspection__c>();
            
            /* This will store the prefix based on the type of service request */
            String versionPrefix = stepObj.SR_Type_Step__c.equals('Event Service Request') ? 'EI' : 'FI';
            
            /* Set the version number to be appended to the version text */
            Integer versionNumber = Integer.valueOf((stepObj.units__c!=null) ? (stepObj.units__c + 1) : 
            		(hasPreviousStep() ? previousStep[0].units__c + 1 : 1));
            
            /* Store the version of the checklist */
            stepObj.Collection_Batch_Number__c = versionPrefix + versionNumber;
            stepObj.units__c = versionNumber;
            version = stepObj.Collection_Batch_Number__c;
            
            if(!questionsList.isEmpty()){
            	
            	/* Loop through each question wrapper to get the questions and their corresponding values */
                for(questionWrapper tempObj : questionsList){
                	
                	/* Create a new Fit-out Inspection instance */
                    Fit_Out_Inspection__c fitOutObj = new Fit_Out_Inspection__c();
                    
                    /* If values came from a previous step, do NOT add record ID */
                    if(tempObj.recordid!=null && !hasPreviousStep()){
                        fitOutObj.id = tempObj.recordid;
                    } else{
                        fitOutObj.Service_Request__c = srObj.id;
                    }
                    
                    /* Get the question/comments and other details filled up in the checklist */
                    fitOutObj.description__c = tempObj.lookUpObj.description__c;
                    
                    /* Get the referenced lookup record of the inspection record 
                     * NOTE: A Lookup record is used to store the questions for
                     *		 each item. To change the questions as a whole, go 
                     *		 to the Lookup records
                     */
                    if(tempObj.lookUpObj!=null){
                        fitOutObj.Lookup__c = tempObj.lookUpObj.id;
                        fitOutObj.name = tempObj.lookUpObj.name;
                    }
                    
                    /* Assign the step ID for the fit-out inspection instance */
                    if(stepObj!=null){
                        fitOutObj.step__c = stepId;
                    }
                    
                    /* If all the values are not filled-up, set the value to 'N/A' */
                    if(tempObj.yes == false && tempObj.no == false && tempObj.na == false){
                        fitOutObj.Options__c = 'N/A';
                    }
                    
                    /* Copy the comments */
                    fitOutObj.Comments__c = tempObj.comments;
                    
                    /* Copy the remaining values */
                    if(tempObj.lookUpObj == null && tempObj.description!=null){
                        fitOutObj.description__c = tempObj.description;
                    }
                    
                    if(tempObj.yes == true){
                        fitOutObj.Options__c = 'Yes';
                        fitOutObj.done_by_IDAMA__c = true;
                    }
                    
                    if(tempObj.no == true){
                        fitOutObj.Options__c = 'No';
                        fitOutObj.done_by_IDAMA__c = true;
                    }
                    
                    if(tempObj.na == true){
                        fitOutObj.Options__c = 'N/A';
                        fitOutObj.done_by_IDAMA__c = true;
                    }
                    
                    /* Assign the version */
                    fitOutObj.version__c = stepObj.Collection_Batch_Number__c;
                    
                    if(foObj!=null){
                    	
                    	if(foObj.Inspection_Date__c > System.Now()){
                    		
                    		/* Validation rule to check if the checklist inspection date is greater than TODAY */
                    		Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Inspection Date cannot be a future date.'));
                    		Database.rollback(sp);
            				return null;
            				
                    	} else {
                    		
                    		/* Copy IDAMA values (if any) */
                    		fitOutObj.idama_comments__c = foObj.idama_comments__c;
	                        fitOutObj.IDAMA_HSE_name__c = foObj.IDAMA_HSE_name__c;
	                        fitOutObj.IDAMA_FO_Mechanical__c = foObj.IDAMA_FO_Mechanical__c;
	                        fitOutObj.IDAMA_FO_Civil__c = foObj.IDAMA_FO_Civil__c;
	                        fitOutObj.IDAMA_FO_Electrical__c = foObj.IDAMA_FO_Electrical__c;
	                        
	                        // Claude - Start of missing event FO fields 
	                        fitOutObj.IDAMA_FO_Name__c = foObj.IDAMA_FO_Name__c;
	                        fitOutObj.IDAMA_HSE_Position__c = foObj.IDAMA_HSE_Position__c;
	                        fitOutObj.IDAMA_FO_Position__c = foObj.IDAMA_FO_Position__c;
	                        // End of missing event FO fields
	                        
	                        fitOutObj.Client_Name__c = foObj.Client_Name__c;
	                        fitOutObj.Contractor_Name__c = foObj.Contractor_Name__c;
	                        
		                    fitOutObj.Inspection_Date__c = inspectionDate == null ? System.Now() : inspectionDate;	
	                        
	                        if(setup == true){
	                            fitOutObj.Event_Premise_Status__c = 'Set-Up';
	                        }
	                        if(handover == true){
	                            fitOutObj.Event_Premise_Status__c = 'Final Handover';
	                        }
	                        if(readjustment == true){
	                            fitOutObj.Inspection_Status__c = 'Adjustment Required';
	                        }
	                        if(accepted == true){
	                            fitOutObj.Inspection_Status__c = 'Inspection Passed';
	                        }
	                        if(reinspection == true){
	                            fitOutObj.Inspection_Status__c = 'Re-inspection Required';
	                        }
                    	}
                    }
                    
                    quesToInsert.add(fitOutObj);
                }
                
                if(!quesToInsert.isEmpty() && quesToInsert!=null){
                    upsert quesToInsert;
                }
            }
            if(stepObj!=null){
                update stepObj;
                redirectPage = new pagereference('/'+stepObj.id);
            }
            
            redirectPage.setRedirect(true);
            return redirectPage;
            
        } catch(Exception ex){
        	
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,ex.getMessage()));
            system.debug('---ex------'+ex.getLineNumber());
            Database.rollback(sp);
        } 
        
        return null;
    } 
    
    /**
     * Redirects the user back to the step 
     * record or service request record
     * detail page
     */
    public pageReference cancelInspection(){
        
        try{
            pageReference redirectPage = null;
            
            if(stepObj!=null){
                redirectPage = new pagereference('/'+stepObj.id);
            } else{
                redirectPage = new pagereference('/'+srObj.id);
            }
            
            redirectPage.setRedirect(true);
            return redirectPage;
            
        } catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,ex.getMessage()));
        } 
        return null;
    }
    
    /**
     * Retrieves the service request ID and
     * queries the service request record 
     * using that ID
     */
    public void initializeSR(){
        
        serviceRequestId = Apexpages.currentPage().getParameters().get('srId');
        
        if(string.isNotBlank(serviceRequestId)){
            srObj = relatedServiceRequest();
        } else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Service Request Missing'));
        }
    }
    
    /**
     * Retrieves the checklist type and
     * gets the related questions for that 
     * type 
     */
    public void initializeCheckList(){
    	
        checkListType = Apexpages.currentPage().getParameters().get('CLType');
        
        if(string.isNotBlank(checkListType) &&  // V1.6 - Claude - Replaced null check with String.isNotBlank() method 
        	(checkListType.equals('Final Inspection Checklist') || //V1.6 - Claude - Replaced '==' with String.equals() method 
        	checkListType.equals('First Fix Inspection Checklist') || 
        	checkListType.equals('Event Inspection Checklist'))){
            questionsList = relatedQuestionList();
        } else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'CheckList Type is Missing'));
        }
    }
    
    /**
     * Retrieves the step ID and
     * queries the step record 
     * using that ID
     */
    public void initializeStep(){
    	
		stepId = Apexpages.currentPage().getParameters().get('stepId'); // V1.6 - Moved variable assignment above    	
    	
        if(String.isNotBlank(stepId)){ //V1.6 - Added step ID check immediately to reduce conditions
            stepObj = relatedStep();
        } else {
        	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Step is missing'));
        }
    }
    
    /**
     * Retrieves the service request type
     */
    public void initializeSRType(){
        
        typeOfSR = Apexpages.currentPage().getParameters().get('srType');
        
        if(string.isBlank(typeOfSR)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Type of Service Request is Missing'));
        }
    }
    
    /**
     * Retrieves the service request details
     * @return 		relatedServiceRequest	The service request record
     */
    private service_request__c relatedServiceRequest(){
    	
    	// V1.1		Added Contractor fields for checking
        List<Service_Request__c> tempSrList = [select id,
        											  linked_sr__r.linked_sr__r.Sponsor_First_Name__c,
        											  linked_sr__r.linked_sr__r.Sponsor_Last_Name__c,
                                                      linked_sr__r.linked_sr__r.First_Name__c,
                                                      linked_sr__r.linked_sr__r.Last_Name__c,
                                                      first_name__c,
                                                      last_name__c,
                                                      Event_Name__c,
                                                      location__c,
                                                      Customer__r.name,
                                                      Customer__r.RORP_License_No__c,
                                                      License_Number__c,
                                                      linked_sr__r.Event_Name__c,
                                                      Company_Name__c, 
                                                      Date__c,Position__c,
                                                      contractor__r.name,
                                                      type_of_request__c,
                                                      linked_sr__r.contractor__r.name,
                                                      linked_sr__r.type_of_request__c,
                                                      Registered_Address__c,
                                                      Approving_Authority__c,
                                                      Sponsor_Visa_Expiry_Date__c,
                                                      Sponsor_Establishment_No__c,
                                                      Sponsor_First_Name__c,
                                                      Sponsor_Last_Name__c,
                                                      Email_Address__c,
                                                      Title_Sponsor__c,
                                                      Sponsor_Office_Telephone__c,
                                                      Sponsor_Passport_No__c,
                                                      Sponsor_P_O_Box__c,
                                                      Sponsor_Nationality__c,
                                                      Sponsor_Mobile_No__c,
                                                      Sponsor_Visa_No__c,
                                                      Sponsor_Middle_Name__c,
                                                      Current_Registered_Name__c,
                                                      CreatedBy.Contact.Contractor__r.Name,
                                                      CreatedBy.isClient__c,
                                                      Record_Type_Name__c
                                                   	  from Service_Request__c where id=:serviceRequestId];
                                                   	  
        if(!tempSrList.isEmpty()){
            
            srObj = tempSrList[0];
            
            /* Set the contractor name */
            contractor = srObj.Record_Type_Name__c.equals('Event_Service_Request') && !srOBj.CreatedBy.isClient__c ?  
            					srOBj.CreatedBy.Contact.Contractor__r.Name :  
            					srObj.contractor__r.name;
           
            return srObj;
        }
        
        return null;
    }
    
    /**
     * Retrieves the step details
     * @return 		relatedStep		The step record
     */
    private step__c relatedStep(){
    	
    	System.debug('Getting related step >>');
        List<step__c> tempStepList = [select id,
        									 name,
        									 Collection_Batch_Number__c,
        									 units__c,
        									 comments__c,
        									 Step_Name__c,
        									 Event_Premise_Status__c,
        									 Inspection_Status__c,
                                             Schedule_Date__c,
                                             name__c,
                                             name1__c,
                                             Name_2__c,
                                             name_3__c,
                                             Date_and_Time_of_Inspection__c,
                                             position__c,
                                             position1__c, 
                                             Status_Code__c, 
                                             SR_Step__c, 
                                             SR_Type_Step__c 
                                      		 from step__c where id=:stepId]; //V1.3 - Claude - Added 'SR_Step__c' field
        if(!tempStepList.isEmpty()){
            
            step__c stepRecord = tempStepList[0];
            
            if(stepRecord.Step_Name__c == 'Inspection' || stepRecord.Step_Name__c == 'First Fix Inspection'){
                setup = true;
            } else if(stepRecord.Step_Name__c == 'Final Inspection'){
                handOver = true;
            }
            
            System.debug('The Step >> ' + stepRecord);
            
            return stepRecord;
        }
        
        return null;
    }
    
    /**
     * Retrieves the questions for the checklist
     * @return 		relatedQuestionList		A list of questions based on the checklist type
     */
    private list<questionWrapper> relatedQuestionList(){
    	
        list<questionWrapper> questionsList = new list<questionWrapper>();
        
        /* V3.17 - Claude - Start */
        
        /* Query the questions */
        for(lookup__c a : [select id,name,Description__c ,Check_List_classification__c,Question_Order__c,Check_List_Type__c from lookup__c 
                         where Check_List_Type__c =: checkListType order by Question_Order__c ASC]){
            questionsList.add(new questionWrapper(a));
        }
        
        return questionsList;
    }
    
    /**
     * Retrieves the checklist responses based on the version specified
     * @return 		relatedInspectionList		The checklist answers
     */
    private list<Fit_Out_Inspection__c> relatedInspectionList(string currentVersion){
        
        list<Fit_Out_Inspection__c> tempList;
        
		System.debug('Is Not Null: ' + stepObj!=null);
        System.debug('Is Setup: ' + setup);
        System.debug('Is Handover: ' + handOver);
        
        if(stepObj!=null){
        	
        	stepIdQuery = hasPreviousStep() ? previousStep[0].Id : stepId;
        	
        	/* Query any existing version of the checklist responses */
        	String existingVersion = getExistingVersion();
        	
        	System.debug('Existing Version: ' + existingVersion);
        	
        	currentVersion = String.isBlank(currentVersion) ? existingVersion : currentVersion;
        	
        	System.debug('Current Version: ' + currentVersion);
        	
        	tempList = setup ? getFirstInspectionResponses(currentVersion) : getFinalInspectionResponses(currentVersion);
        }
        
        return tempList;
    }
    
    /**
	 * //V1.6 - Claude - Added new method  
	 * Returns first inspection checklist 
	 * responses based on the version
	 * specified
	 * @params		version							The version of the checklist to be retrieved
	 * @return 		firstInspectionResponses		The final inspection checklist responses
	 */
	private list<Fit_Out_Inspection__c> getFirstInspectionResponses(String version){
		return getResponses(version,false);
	}
	
	/**
	 * //V1.6 - Claude - Added new method  
	 * Returns first inspection checklist 
	 * responses based on the version
	 * specified
	 * @params		version							The version of the checklist to be retrieved
	 * @return 		finalInspectionResponses		The final inspection checklist responses
	 */
	private list<Fit_Out_Inspection__c> getFinalInspectionResponses(String version){
		return getResponses(version,true);
	}
    
    /**
	 * //V1.6 - Claude - Added new method  
	 * Returns first inspection checklist 
	 * responses based on the version
	 * specified
	 * @params		version				The version of the checklist to be retrieved
	 * @params		isFinalInspection	Tells whether the step is a Final Inspection checklist
	 * @return 		responses			The checklist responses
	 */
	private list<Fit_Out_Inspection__c> getResponses(String version, Boolean isFinalInspection){

		String filters = ' WHERE step__c = \''+stepIdQuery+'\' AND service_request__c = \'' + serviceRequestId + '\' AND version__c = \''+version+'\' AND ';		
		String checklistTypeVar = setup == null || setup == false ? 'step__r.step_name__c = \'Final Inspection\'' : '(step__r.step_name__c = \'Inspection\' or step__r.step_name__c = \'First Fix Inspection\')';
		String queryFields = 'SELECT id,other__c,service_request__c,name,lookup__c,lookup__r.description__c,options__c,contractor_checklist__c,lookup__r.Question_Order__C,lookup__r.Check_List_Type__c, lookup__r.Check_List_classification__c, Comments__c,description__c, done_by_idama__c,done_By_contractor__c, Client_Name__c, Client_Position__c,Contractor_Name__c,IDAMA_Comments__c,IDAMA_FO_Civil__c,IDAMA_FO_Electrical__c,IDAMA_FO_Mechanical__c,IDAMA_FO_Name__c,IDAMA_FO_Position__c,IDAMA_HSE_Name__c,IDAMA_HSE_Position__c,Inspection_Status__c,Event_Premise_Status__c,Inspection_Date__c FROM Fit_Out_Inspection__c';
		
		/* Build the query string, and retrieve the records */
		return Database.query(queryFields + filters + checklistTypeVar);
	}
	
    /**
     * Gets any existing version of the 
     * checklist responses
     * @return 		existingVersion		The most recent version of the checklist
     */
    private String getExistingVersion(){
    	
    	String existingVersion = '';
    	
    	String checklistTypeVar = setup == null || setup == false ? 'step__r.step_name__c = \'Final Inspection\'' : '(step__r.step_name__c = \'Inspection\' or step__r.step_name__c = \'First Fix Inspection\')';
    	String versionQuery = 'SELECT COUNT(ID), version__c FROM Fit_Out_Inspection__c where service_request__c = \''+serviceRequestId+'\' AND Step__c = \''+stepIdQuery+'\' AND ' + checklistTypeVar + ' group by version__c';
    	
    	AggregateResult[] versionResult = Database.query(versionQuery);
		
		if(!versionResult.isEmpty()){
			System.debug('-- Checking version --');
			List<Integer> versionList = new List<Integer>();
			
			String versionPrefix = String.valueOf(versionResult[0].get('version__c')).contains('FI') ? 'FI' : 'EI';
		
			for(AggregateResult a : versionResult){
				System.debug('Agg. result: ' + a);
				versionList.add(Integer.valueOf(String.valueOf(a.get('version__c')).replaceAll('FI','').replaceAll('EI','')));
			}
			
			versionList.sort();
			existingVersion = versionPrefix + String.valueOf(versionList.get(versionList.size()-1));
			System.debug('Existing Version: ' + existingVersion);
		} 
		
		return existingVersion;
    }
    
    /**
     * Sets the initial values of the checklist
     */
    private void initializeInspectionList(){
        
        questionsList = new list<questionWrapper>();
        
        if(currentList!=null && !currentList.isEmpty()){
            
            for(Fit_Out_Inspection__c obj : currentList){
            	
                questionWrapper tempObj = new questionWrapper(null); 
                tempObj = relatedQuestion(obj);
                
                tempObj.lookUpObj.description__c = tempObj.lookUpObj.description__c.replaceAll('�','\'');
                
                questionsList.add(tempObj);
            }
        }
        
        /* Sort the question list */
        List<questionWrapper> questionListFormatted = new List<questionWrapper>();
        
        Map<Integer,questionWrapper> questionMap = new Map<Integer,questionWrapper>();
        
        for(questionWrapper q : questionsList){
        	questionMap.put(Integer.valueOf(q.lookUpObj.Question_Order__c),q);
        }
        
        List<Integer> questionKeyList = new List<Integer>(questionMap.keySet());
        
        questionKeyList.sort();
        
        for(Integer i : questionKeyList){
        	questionListFormatted.add(questionMap.get(i));
        }
        
        questionsList = new List<questionWrapper>(questionListFormatted);
    }
    
    /**
     * Retrieves the checklist question
     * @params		obj					The fit-out inspection response
     * @return 		relatedQuestion		The question related to that response
     */
    private questionWrapper relatedQuestion(Fit_Out_Inspection__c obj){
        
        questionWrapper tempObj = new questionWrapper(null); 
       
        if(obj.lookup__c!=null){
            tempObj.lookupObj = new lookup__c();
            tempObj.lookupObj.id = obj.lookup__c;
            tempObj.lookupObj.Check_List_classification__c = obj.lookup__r.Check_List_classification__c;
            tempObj.lookupObj.Check_List_Type__c = obj.lookup__r.Check_List_Type__c;
            tempObj.lookupObj.description__c = obj.lookup__r.description__c;
            tempObj.lookupObj.Question_Order__c = obj.lookup__r.Question_Order__c;
        }
        
        tempObj.comments = obj.Comments__c;
        tempObj.name = obj.Name;
        tempObj.description = obj.description__c;
        
        if(obj.Options__c == 'Yes'){
            tempObj.yes = true;
        }
        
        if(obj.Options__c == 'No'){
            tempObj.no = true;
        }
       
        if(obj.Options__c == 'N/A'){
            tempObj.na = true;
        }
        
        tempObj.recordId = obj.id;
       
        return tempObj;
    }
    
    /* Custom wrapper for the inspection questions and answers */
    public class questionWrapper{
        
        public lookup__c lookUpObj{get; set;}
        public boolean yes {get;set;}
        public boolean no {get;set;}
        public boolean na {get;set;}
        public string comments {get;set;}
        public string name {get;set;}
        public id recordId {get;set;}
        public string description {get;set;}
        
        public questionWrapper(lookup__c a){
            lookUpObj = a;
            yes = false;
            no = false;
            na = false;
            comments = '';
            name = '';
            recordId = null;
            description = '';
        }
    }             
}