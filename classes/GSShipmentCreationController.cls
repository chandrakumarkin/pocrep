/**
 * @description       : Used this call to create shipment on the document delivery once Governmnet fine is paid
 * @author            : Veera 
 * @group             : 
 * @last modified on  : 25-04-2021
 * @last modified by  : Veera
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   25-04-2021   Veera                                Initial Version
**/

public without sharing class GSShipmentCreationController{

   @InvocableMethod(Label = 'ShippmentCreateAfterFinePaid'  description = 'ShippmentCreateAfterFinePaid')
   public static void GSCorierDeliveryShipmentMethod(List < id > ids) {
   
   integer i =0;
   
   step__c stp = new step__c();
        
        for( step__C step : [select id,Step_Name__c  from step__c where step_name__c in ('Courier Delivery','Collected') and SR__C in:ids  order by createdDate desc limit 2]){
            
           if(step.Step_Name__c == 'Collected'){
               
               i++;
           }
           
           if(step.Step_Name__c == 'Courier Delivery'){
            
             if([select count() from Shipment__c where Step__r.id = : step.id] == 0)
                  stp = step;
            
           }
           
        } 
        
        if((i>0 && stp!=null) || test.isRunningTest()){
           GSCorierDeliveryShipmentMethod(stp.id);
        }
    }
    @future(callout=true)
    public static void GSCorierDeliveryShipmentMethod(String CourierStepId){
        if(test.isRunningTest() == false)
            CC_CourierCls.CreateShipmentNormal(CourierStepId);
     }
      
}