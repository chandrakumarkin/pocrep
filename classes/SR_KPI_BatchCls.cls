/*************************************************************************************************
 *  Name        : SR_KPI_BatchCls
 *  Author      : Sravan Booragadda
 *  Company     : NSI JLT
 *  Date        : 08-03-2016    
 *  Purpose     : This class is to calculate RoC and Client Step durations on SR in Batches     
  
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By          Description
---------------------------------------------------------------------------------------------------------------------             
v1.1    24-Oct-16   Shabbir Ahmed       Updated ROC time calculation for Awaiting Original  
v1.2    07-Jun-17   Claude              Revised KPI query string to APEX Utils; Added logic for KPI for RORP Team (4035) 
    
***************************************************************************************************/
Global without sharing class SR_KPI_BatchCls implements Database.Batchable<sobject>
{
    
    Private String SrQueryString;
    
    Global SR_KPI_BatchCls()
    {
        SrQueryString = 'Select id,SR_Group__c,Completed_Date_Time__c,Submitted_DateTime__c from Service_Request__c where Completed_Date_Time__c!=null  AND Pre_GoLive__c=false  AND Completed_Date_Time__c = TODAY';
    }
        
    
    
    Global Database.QueryLocator start(Database.BatchableContext BC)
    {
        return Database.getQueryLocator(SrQueryString);         
        
    }
    
    Global Void execute(Database.BatchableContext BC,list<Service_Request__c> SRBatchList)
    {
        if(SRBatchList !=null && SRBatchList.size() > 0)
        {
                //ROC KPI
            decimal tot_user_hrs = 0;
            decimal tot_roc_hrs = 0;
            decimal tot_reg_time = 0;
            decimal tot_rorp_reg_time = 0;
            decimal tot_line_time = 0;
            decimal tot_dp_time = 0 ;
            decimal tot_security_time = 0;
            decimal tot_reviewer_time = 0;
            //GSO KPI
            decimal tot_GSBO_time = 0;
            decimal tot_GSFDO_time = 0;
            decimal tot_GSLM_time=0;
            decimal tot_GST_time=0;
            //decimal BO_prep_time=0;
            //decimal med_schedule_time=0;
            //decimal med_time=0;
            
            //RORP KPI
            decimal tot_rorp_hrs = 0;
            
            decimal Immigration_time=0;
            boolean rescheduled = false;
            string SRNo='';
            
            boolean isRORP = false;
           
            list<Service_Request__c> srlist = new list<Service_Request__c>();
            map<string,list<Step__c>> map_stepOwner = new map<string,list<Step__c>>();
            map<string,list<Step__c>> map_stepSR = new map<string,list<Step__c>>();
            map<string,string> map_Queue_id = new map<string,string>();
            map<string,datetime> map_end_date = new map<string,datetime>();
            map<string,datetime> map_submitted_date = new map<string,datetime>();
            map<string,BusinessHours> map_businessHour = new map<string,BusinessHours>();
            map<string,string> map_SRGroup = new map<string,string>();
            Service_Request__c SR = new Service_Request__c();
            
            try{
            for(BusinessHours bh : [Select Id,name from BusinessHours where IsActive=true]){
                map_businessHour.put(bh.name,bh);
            }
            
            for(QueueSobject clientQ :[Select Id,QueueId,Queue.Name from QueueSobject where sObjectType = 'Step__c']){
                map_Queue_id.put(clientQ.Queue.Name,clientQ.QueueId);
            }
            
            //Group clientQ = [Select id,name from Group where Name='Client Entry User'];
            Set<String> SRid = new Set<String>();
            
            for(Service_Request__c lstSR :SRBatchList){
                SRid.add(lstSR.id);
                map_end_date.put(lstSR.id,lstSR.Completed_Date_Time__c);
                map_submitted_date.put(lstSR.id,lstSR.Submitted_DateTime__c);
                map_SRGroup.put(lstSR.id,lstSR.SR_Group__c);
            }
            /*list<Step__c> endtkStp =[Select SR__c,SR__r.Submitted_DateTime__c,id,Name,createdDate,Step_Name__c,Step_Template__c from Step__c where Step_Template__r.Code__c='END_TASK' and createddate=TODAY and SR__c!=null ];
            //to hold all SR ids for the day
            for(Step__c stpSR : endtkStp){
                SRid.add(stpSR.SR__c);
                map_end_date.put(stpSR.SR__c,stpSR.createdDate);
                map_submitted_date.put(stpSR.SR__c,stpSR.SR__r.Submitted_DateTime__c);
            }*/
            //To get all the steps related to these SR ids
            //V1.2 - Claude - Start
            list<Step__c> userstp = [Select id,SR_Group__c,Sys_Step_OwnerId__c,Is_KPI_Applicable__c,
                                            OwnerId,
                                            SAP_RESCH__c,
                                            CreatedDate,
                                            SR__c,
                                            SR__r.Submitted_DateTime__c,
                                            Closed_Date_Time__c,
                                            Closed_Date__c,
                                            Originals_Awaited_Date__c,
                                            Step_Template__r.KPI_Step_Type__c,
                                            Put_on_hold__c,     // V1.2
                                            Paused_Date__c,     // V1.2
                                            Resumption_Date__c  // V1.2 
                                            from Step__c where SR__c IN :SRid AND Is_KPI_Applicable__c = true];
            //V1.2 - Claude - End
            
            for(Step__c stp:userstp){
                list<Step__c> lstSR = map_stepSR.containsKey(stp.SR__c) ? map_stepSR.get(stp.SR__c) : new list<step__c>();
                lstSR.add(stp);
                map_stepSR.put(stp.SR__c,lstSR);//Mapping SR with Step list
                
            }
            for(string tempSR :SRid){
                map_stepOwner = new map<string,list<Step__c>>();
                //ROC KPI
                tot_user_hrs = 0;
                tot_roc_hrs = 0;
                tot_rorp_hrs = 0;
                tot_reg_time = 0;
                tot_rorp_reg_time = 0;
                tot_line_time = 0;
                tot_dp_time = 0 ;
                tot_security_time = 0;
                tot_reviewer_time = 0;
                //GSO KPI
                tot_GSBO_time = 0;
                tot_GSFDO_time = 0;
                tot_GSLM_time=0;
                tot_GST_time=0; 
                //BO_prep_time=0;
                //med_schedule_time=0;
                //med_time=0;
                Immigration_time=0;
                rescheduled = false;
                SRNo = tempSR;
                
                if(tempSR !=null && map_stepSR.ContainsKey(tempSR))
                {
                    for(Step__c SRSteps : map_stepSR.get(tempSR)){
                        list<step__c> lst = map_stepOwner.containsKey(SRSteps.Sys_Step_OwnerId__c) ? map_stepOwner.get(SRSteps.Sys_Step_OwnerId__c) : new list<step__c>();
                        lst.add(SRSteps);
                        map_stepOwner.put(SRSteps.Sys_Step_OwnerId__c,lst);
                    }
                }
                
                //Client User Entry Steps
                if(map_stepOwner.get(map_Queue_id.get('Client Entry User'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('Client Entry User'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                        
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                tot_user_hrs = tot_user_hrs+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                                
                            }
                        }
                    }
                }
                //RoC Officer Steps
                if(map_stepOwner.get(map_Queue_id.get('RoC Officer'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('RoC Officer'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                        
                            BusinessHours bh;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                //v1.1 start
                                // tot_roc_hrs = tot_roc_hrs+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));      ----- Commented out orignal condition v1.1
                                if(stp.CreatedDate!=null && stp.Originals_Awaited_Date__c!=null)
                                    tot_roc_hrs = tot_roc_hrs+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Originals_Awaited_Date__c)/3600000));                                   
                                else
                                      tot_roc_hrs = calculatePausedHours(tot_roc_hrs,stp,bh); //veera added
                                    //tot_roc_hrs = tot_roc_hrs+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));        -- commented by   veera calculate based on hold                        
                                //v1.1 end
                            }
                        }
                    }
                }
                //Line Manager Steps
                if(map_stepOwner.get(map_Queue_id.get('Line Manager'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('Line Manager'))){
                        
                        BusinessHours bh ;
                        for(string obj : map_businessHour.keyset()){
                            if(stp.SR_Group__c == obj){
                                bh = map_businessHour.get(stp.SR_Group__c);
                            }   
                        }
                        if(bh!=null){
                            tot_line_time = tot_line_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                        }
                    }
                }
                //Registrar Steps
                if(map_stepOwner.get(map_Queue_id.get('Registrar'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('Registrar'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                        
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                tot_reg_time = tot_reg_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                            }
                        }
                    }
                }
                //Data Protection Commissioner Steps
                if(map_stepOwner.get(map_Queue_id.get('Data Protection Commissioner'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('Data Protection Commissioner'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                        
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                tot_dp_time = tot_dp_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                            }
                        }
                    }
                }
                //Security Steps
                if(map_stepOwner.get(map_Queue_id.get('Security'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('Security'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                        
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                tot_security_time = tot_security_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                            }
                        }
                    }
                }
                //Reviewer Steps
                if(map_stepOwner.get(map_Queue_id.get('Reviewers'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('Reviewers'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                        
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                               // tot_reviewer_time = tot_reviewer_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                               tot_reviewer_time = calculatePausedHours(tot_roc_hrs,stp,bh); 
                            }
                        }
                    }
                }
                //GS Back Office Steps
                if(map_stepOwner.get(map_Queue_id.get('GS Back Office'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('GS Back Office'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                if(stp.Step_Template__r.KPI_Step_Type__c!=null && stp.Step_Template__r.KPI_Step_Type__c=='Client')
                                    tot_user_hrs = tot_user_hrs+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                                else if(stp.Step_Template__r.KPI_Step_Type__c!=null && stp.Step_Template__r.KPI_Step_Type__c=='Medical Start'){
                                    if(stp.SAP_RESCH__c=='X')
                                        rescheduled = true;
                                }else if (stp.Step_Template__r.KPI_Step_Type__c!=null && stp.Step_Template__r.KPI_Step_Type__c=='Immigration')
                                    Immigration_time = Immigration_time + ((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                                else if(stp.Step_Template__r.KPI_Step_Type__c==null || stp.Step_Template__r.KPI_Step_Type__c==''){
                                    tot_GSBO_time = tot_GSBO_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                                }
                            }
                        }
                    }
                }
                //GS Front Desk Officer Steps
                if(map_stepOwner.get(map_Queue_id.get('GS Front Desk Officer'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('GS Front Desk Officer'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                        
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                if(stp.Step_Template__r.KPI_Step_Type__c!=null && stp.Step_Template__r.KPI_Step_Type__c=='Client')
                                    tot_user_hrs = tot_user_hrs+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                                else if (stp.Step_Template__r.KPI_Step_Type__c!=null && stp.Step_Template__r.KPI_Step_Type__c=='Immigration')
                                    Immigration_time = Immigration_time + ((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                                else if(stp.Step_Template__r.KPI_Step_Type__c==null || stp.Step_Template__r.KPI_Step_Type__c==''){
                                    if(stp.CreatedDate!=null && stp.Originals_Awaited_Date__c!=null)
                                        tot_GSFDO_time = tot_GSFDO_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Originals_Awaited_Date__c)/3600000));
                                    else
                                        tot_GSFDO_time = tot_GSFDO_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                                }
                            }
                        }
                    }
                }
                //GS Line Manager Steps
                if(map_stepOwner.get(map_Queue_id.get('GS Line Manager'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('GS Line Manager'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                        
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                tot_GSLM_time = tot_GSLM_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                            }
                        }
                    }
                }
                //GS Typist Steps
                if(map_stepOwner.get(map_Queue_id.get('GS Typist'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('GS Typist'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                tot_GST_time = tot_GST_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                            }
                        }
                    }
                }
                
                //RORP Queue Steps
                if(map_stepOwner.get(map_Queue_id.get('RORP Front Desk Officer'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('RORP Front Desk Officer'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                        
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                 system.debug('######' + stp.SR_Group__c);
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                //V1.2 - Claude - Start
                                //tot_rorp_hrs = tot_rorp_hrs+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                                tot_rorp_reg_time = tot_rorp_reg_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                                tot_rorp_hrs = calculatePausedHours(tot_rorp_hrs,stp,bh);
                                //V1.2 - Claude - End
                            }
                        }
                    }
                }
                
                //RORP Registrar Steps
                if(map_stepOwner.get(map_Queue_id.get('RORP Registrar'))!=null){
                    for(Step__c stp:map_stepOwner.get(map_Queue_id.get('RORP Registrar'))){
                        if(stp.Closed_Date_Time__c!=null && stp.CreatedDate!=null){
                        
                            BusinessHours bh ;
                            for(string obj : map_businessHour.keyset()){
                                system.debug('######' + stp.SR_Group__c);
                                if(stp.SR_Group__c == obj){
                                    bh = map_businessHour.get(stp.SR_Group__c);
                                }   
                            }
                            if(bh!=null){
                                //V1.2 - Claude - Start
                                tot_rorp_reg_time = tot_rorp_reg_time+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000));
                                tot_rorp_hrs = calculatePausedHours(tot_rorp_hrs,stp,bh);               
                                //V1.2 - Claude - end
                            }
                        }
                    }
                }
                
                SR = new Service_Request__c(id=tempSR);
                SR.Client_Time__c = tot_user_hrs;
                SR.RoC_Time__c = tot_roc_hrs+tot_reviewer_time;
                SR.RORP_Time__c = tot_rorp_hrs;
                SR.Line_Manager_Time__c = tot_line_time;
                SR.Reg_Time__c = tot_reg_time;
                SR.Security_Time__c = tot_security_time;
                SR.DP_Time__c = tot_dp_time;
                SR.GS_Back_Office_Time__c = tot_GSBO_time;
                SR.GS_Front_Desk_Officer_Time__c = tot_GSFDO_time;
                SR.GS_Line_Manager_Time__c = tot_GSLM_time;
                SR.GS_Typist__c = tot_GST_time;
                SR.Immigration_Time__c = Immigration_time;
                SR.Medical_Rescheduled__c = rescheduled;
                if(map_SRGroup!=null && map_SRGroup.get(tempSR)=='RORP'){
                    SR.Reg_Time__c = tot_rorp_reg_time;
                }
                
                if(map_submitted_date.get(tempSR)!= null && map_end_date.get(tempSR)!=null){
                        
                    BusinessHours bh ;
                    for(string obj : map_businessHour.keyset()){
                        if(map_SRGroup!=null && map_SRGroup.get(tempSR) == obj){
                            bh = map_businessHour.get(map_SRGroup.get(tempSR));
                        }   
                    }
                    if(bh!=null){        
                        SR.Total_SR_Time__c = ((decimal)((decimal)system.Businesshours.diff(bh.id,map_submitted_date.get(tempSR),map_end_date.get(tempSR))/3600000));
                    }
                }
                srlist.add(SR);
            }
            //Update all SRs for the day
            //V1.2 update srlist;
            //v1.2 - new changes
            Database.SaveResult[] lstUpdateSRs = Database.update(srlist, false);
            String sStr = '';
            for(Database.SaveResult objResult : lstUpdateSRs){
                if(objResult.isSuccess() == false){
                    sStr += 'SR Id - '+objResult.getId()+' Error : '+objResult.getErrors()+'\n';
                }
            }
            if(sStr != ''){
                insert LogDetails.CreateLog(null, 'Schedule for SR total time failed', sStr);
            }
        }catch(Exception ex){
            system.debug('Exception is : '+ex.getMessage());
            /*Log__c objLog = new Log__c();
            objLog.Description__c ='Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage()+'----RequestID===>>'+SRNo;
            objLog.Type__c = 'Schedule for SR total time failed';
            insert objLog;        */
            insert LogDetails.CreateLog(null, 'Schedule for SR total time failed', 'Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage()+'----RequestID===>>'+SRNo);
        }
            
        }
        
    }
    
    /**
     * V1.2
     * Calculates the KPI hours for RORP Steps
     * @author      Claude Manahan  
     * @date        07-06-2017  
     * @params      rorpHours           The number of hours calculated
     * @params      stp                 The step record
     * @params      bh                  The business hours instance
     * @return      calculatedRorpHours The calculated RORP Hours
     */
    private Decimal calculatePausedHours(Decimal rorphours, Step__c stp, BusinessHours bh){
        
        return !stp.Put_on_Hold__c ? ((stp.Paused_Date__c != null && stp.Resumption_Date__c != null) ? 
                    rorphours+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Paused_Date__c)/3600000 + (decimal)system.Businesshours.diff(bh.id,stp.Resumption_Date__c,stp.Closed_Date_Time__c)/3600000)) :
                    rorphours+((decimal)((decimal)system.Businesshours.diff(bh.id,stp.CreatedDate,stp.Closed_Date_Time__c)/3600000))) : rorphours;
        
    }
    
    
    Global void finish(Database.BatchableContext BC)
    {
        
    }
    
}