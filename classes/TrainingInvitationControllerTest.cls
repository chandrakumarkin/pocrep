@IsTest
private class TrainingInvitationControllerTest {
    static testMethod void validateTrainingRequest() {
        Training_Master__c trainingMaster = new Training_Master__c();
        trainingMaster.Name = 'Employee Service training';
        trainingMaster.Category__c = 'Employee Services';
        trainingMaster.Available_Seats__c = 10;
        trainingMaster.Status__c = 'Approved by HOD';
        trainingMaster.Start_DateTime__c = DateTime.now().addDays(1);
        trainingMaster.To_DateTime__c = DateTime.now().addDays(2);
        insert trainingMaster;

        //inserting training request.
        Training_Request__c trObj = new Training_Request__c();
        trObj.First_Name__c = 'Sam';
        trObj.Last_Name__c = 'S';
        trObj.TrainingMaster__c = trainingMaster.id;
        trObj.Status__c = 'Submitted';
        insert trObj;

        PageReference pageRef = Page.TrainingInvitation;
        pageRef.getParameters().put('id', String.valueOf(trObj.Id));
        Test.setCurrentPage(pageRef);
        TrainingInvitationController trainingInvitationObj = new TrainingInvitationController(); //Instantiate the Class
		trainingInvitationObj.warningMessage = 'test';
        trainingInvitationObj.status = 'Accepted';
        trainingInvitationObj.confirmAction();
    }
}