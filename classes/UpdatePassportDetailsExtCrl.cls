/*
    Created By  : Suchita - DIFC on 23 Feb 2021
    Description : This Class is extension class for UpdatePassportDetails visualforce page
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    ----------------------------------------------------------------------------------------
    V.No        Date                Updated By          Description
	v1.0      	18 Feb 2021         Suchita Sharma,     This Class is extension class for UpdatePassportDetails visualforce page
    v1.1		22 Jun 2021			Ananya Acharya	 	This is to check whether user/portal contact has shareholder details or not.
    
	test Class - UpdatePassportDetailsExtCrlTest
    
   ----------------------------------------------------------------------------------------
    */
    public class UpdatePassportDetailsExtCrl {
        public Boolean isActiveShareHolder{get;set;}
        public Service_Request__c SRData { get; set;}
        public string RecordTypeId{get;set;}
        public map < string, string > mapParameters;
        //Constructor
        public UpdatePassportDetailsExtCrl (ApexPages.StandardController controller) {
            
            SRData = (Service_Request__c) controller.getRecord();
            
            if (apexpages.currentPage().getParameters() != null)
                mapParameters = apexpages.currentPage().getParameters();
            if (mapParameters.get('RecordType') != null)
                RecordTypeId = mapParameters.get('RecordType');
            
            PopulateInitialData(); 
            
        }
        
        public void PopulateInitialData(){
            for (User objUsr: [SELECT id, ContactId, Email, Contact.Account.Legal_Type_of_Entity__c, Phone, Contact.AccountId, 
                               Contact.Account.Name, Contact.Passport_No__c,Contact.FirstName,Contact.LastName,Contact.Nationality__c,
                               Contact.Place_of_Birth__c,Contact.Birthdate,Contact.Gender__c,Contact.Issued_Country__c,
                               Contact.Date_of_Issue__c,Contact.Passport_Expiry_Date__c,Contact.Country_of_Birth__c,Contact.Address_Line_2_HC__c,
                               Contact.Address_Line_1_HC__c,Contact.CITY1__c,Contact.Emirate_State_Province__c,Contact.PO_Box__c,Contact.COUNTRY_ENGLISH2__c,
                               Contact.Place_of_Issue__c,Contact.Does_the_individual_have_a_valid_UAE_res__c,Contact.Does_the_individual_reside_in_the_UAE__c,
                               Contact.Unique_Key__c FROM User 
                               WHERE Id =: userinfo.getUserId() ]) {
                                   if (SRData.id == null) {
                                       SRData.Customer__c= objUsr.Contact.AccountId;
                                       SRData.RecordTypeId = RecordTypeId;
                                       SRData.Email__c = objUsr.Email;
                                       SRData.Legal_Structures__c = objUsr.Contact.Account.Legal_Type_of_Entity__c;
                                       SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                                       SRData.Declaration_Signatory_Name__c = objUsr.ContactId; // Contact Id of Logged In user
                                       SRData.Sponsor_Middle_Name__c = objUsr.Contact.Unique_Key__c; // Unique Id of Passport+Nationality
                                       SRData.Passport_Number__c=objUsr.Contact.Passport_No__c;
                                       SRData.Last_Name__c=objUsr.Contact.LastName;
                                       SRData.First_Name__c=objUsr.Contact.FirstName;
                                       SRData.Nationality_list__c = objUsr.Contact.Nationality__c;
                                       SRData.Place_of_Birth__c = objUsr.Contact.Place_of_Birth__c;
                                       SRData.Date_of_Birth__c = objUsr.Contact.Birthdate;
                                       SRData.Gender__c = objUsr.Contact.Gender__c;
                                       SRData.Passport_Place_of_Issue__c = objUsr.Contact.Place_of_Issue__c;
                                       SRData.Passport_Date_of_Issue__c = objUsr.Contact.Date_of_Issue__c;
                                       SRData.Passport_Date_of_Expiry__c = objUsr.Contact.Passport_Expiry_Date__c;
                                       SRData.Street_Address__c = objUsr.Contact.Address_Line_1_HC__c;
                                       SRData.Name_of_Addressee__c = objUsr.Contact.Address_Line_2_HC__c;
                                       SRData.City_Town__c = objUsr.Contact.CITY1__c;
                                       SRData.Emirate_State__c = objUsr.Contact.Emirate_State_Province__c;
                                       SRData.PO_BOX__c = objUsr.Contact.PO_Box__c;
                                       SRData.Name_of_the_Country_GS__c=ObjUsr.Contact.COUNTRY_ENGLISH2__c;
                                       SRData.Confirm_Change_of_Entity_Name__c=ObjUsr.Contact.Does_the_individual_have_a_valid_UAE_res__c;
                                       SRData.Confirm_Change_of_Trading_Name__c=ObjUsr.Contact.Does_the_individual_reside_in_the_UAE__c;
                                   }
                               }
        }
        
        //v1.1 ANANYA ACHARYA TICKET - 16154
        public void checkShareHolder(){
            string current_portal_user_Id = UserInfo.getUserId();
            List<User> communityportaluser = [SELECT Id, Name,ContactId from User where Id =:current_portal_user_Id and Profile.UserLicense.Name Like 'Customer Community%' limit 1];
            if(communityportaluser.isEmpty())
            	isActiveShareHolder = false;
            else{
                //current user is portal user
                For(Contact con: [SELECT Id,(SELECT Id from Secondary_Contact__r where Active__c = true and
                                 Relationship_Type__c in('Has Founding Member','Has General Partner','Has Limited Partner','Has Member','Is Shareholder Of','has founder','Foreign Entity Shareholder')) 
                                  from Contact where Id  =:communityportaluser[0].ContactId limit 1]){
                                      if(con.Secondary_Contact__r.size()>0){
                                          isActiveShareHolder = true;
                        break;
                    }
                }
            }
            system.debug('isActiveShareHolder: '+isActiveShareHolder);
        }
        
        public string  validationRule(){
            string Errormessage='';
            if((SRData.Nationality_list__c=='Kuwait' || SRData.Nationality_list__c=='Saudi Arabia' || SRData.Nationality_list__c=='Bahrain'
                || SRData.Nationality_list__c=='Qatar'  || SRData.Nationality_list__c=='Oman') && SRData.Confirm_Change_of_Trading_Name__c ==null){
               Errormessage='Does the individual reside in the UAE?: You must enter a value';
            }
            if(SRData.Nationality_list__c !='' && (SRData.Nationality_list__c !='Kuwait' &&  SRData.Nationality_list__c!='United Arab Emirates' 
                                                   && SRData.Nationality_list__c!='Saudi Arabia'  && SRData.Nationality_list__c !='Bahrain'  
                                                   && SRData.Nationality_list__c !='Qatar'  && SRData.Nationality_list__c!='Oman')
               && SRData.Confirm_Change_of_Entity_Name__c ==null){
               Errormessage='Does the individual have a valid UAE residence visa?: You must enter a value';
            }
            return Errormessage;
        }
        
        public PageReference SaveRecord() {
            string Errormessage=validationRule();
            boolean isvalid=String.isBlank(Errormessage);
            if(isvalid || Test.isRunningTest()){   
                try{
                    upsert SRData ;
                    createAmendmentforSR(SRData);
                    PageReference acctPage = new ApexPages.StandardController(SRData).view();
                    acctPage.setRedirect(true);
                    return acctPage;
                }catch (Exception e) {
                    ApexPages.Message myMsg =new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getDmlMessage(0));
                    ApexPages.addMessage(myMsg);
                    return NULL;
                }
            }else{
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,Errormessage);         
                ApexPages.addMessage(myMsg); 
            }
            return null;
        }
        
        public static string autoApproveSR(String stepId){
            string status = 'Success';
            try{
                List<Step__c> ListStep=[select SR__r.External_SR_Status__c,SR__r.Internal_SR_Status__c from Step__c where id=:stepId limit 1];
                if(!ListStep.IsEmpty()){
                    List<SR_Status__c> SRStatus = [Select Id,name from SR_Status__c where name = 'Approved'];
                    Service_Request__c objSRStatusUpdate = new Service_Request__c(Id = ListStep[0].SR__c);
                    objSRStatusUpdate.External_SR_Status__c = SRStatus[0].Id;
                    objSRStatusUpdate.Internal_SR_Status__c = SRStatus[0].Id;
                    update objSRStatusUpdate;
                }
            }catch(Exception ex){Log__c objLog = new Log__c();objLog.Description__c = ex.getMessage() +'   '+ex.getLineNumber();objLog.Type__c = 'auto approve of SR';insert objLog; } 
            return status;
        }
        public static string updatePassportDetailOnContact(String SRId){
            Service_Request__c SRObj = [select id,Declaration_Signatory_Name__c,Passport_Number__c,Nationality_list__c,Place_of_Birth__c,Date_of_Birth__c,
                                        Gender__c,Passport_Place_of_Issue__c,Passport_Date_of_Issue__c,Passport_Date_of_Expiry__c,
                                        Street_Address__c,Name_of_Addressee__c,City_Town__c,Emirate_State__c,PO_BOX__c,Send_SMS_To_Mobile__c,Email__c,Name_of_the_Country_GS__c,Confirm_Change_of_Entity_Name__c,Confirm_Change_of_Trading_Name__c,Sponsor_Middle_Name__c,Customer__c from Service_Request__c where id=:SRId limit 1];
            string status = 'Success';
            try{
                //Record type Check
                Id PortalrecordTypeId =Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Portal_User').getRecordTypeId();
                Id individualrecordTypeId =Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
                Set<ID> RelContactSet = new set<Id>();
               	for(Relationship__c rel : [Select id,Object_Contact__c from Relationship__c where Status__c='Active' and Object_Contact__r.Unique_Key__c=:SRObj.Sponsor_Middle_Name__c and Subject_Account__c=:SRObj.Customer__c and Object_Contact__r.RecordTypeId=:individualrecordTypeId ]){
         		   RelContactSet.add(rel.Object_Contact__c);
        		}
                
                List<Contact> contactUpdateList = new List<Contact>();
                List<User> userUpdateList = new List<User>(); 
                for(contact conObj: [select Nationality__c,Passport_No__c,Place_of_Birth__c,Birthdate,Gender__c,Issued_Country__c,Date_of_Issue__c,Passport_Expiry_Date__c,
                                     Country_of_Birth__c,Address_Line_2_HC__c,Address_Line_1_HC__c,CITY1__c,Emirate_State_Province__c,PO_Box__c,MobilePhone,Email,Does_the_individual_have_a_valid_UAE_res__c,Does_the_individual_reside_in_the_UAE__c,COUNTRY_ENGLISH2__c,Place_of_Issue__c  
                                     from Contact where (Id=:SRObj.Declaration_Signatory_Name__c  AND AccountId=:SRObj.Customer__c AND RecordTypeId=:PortalrecordTypeId) OR (Id=:RelContactSet and RecordTypeId=:individualrecordTypeId) ]){
                                         conObj.Passport_No__c = SRObj.Passport_Number__c;
                                         conObj.Nationality__c=SRObj.Nationality_list__c;
                                         conObj.Place_of_Birth__c=SRObj.Place_of_Birth__c;
                                         conObj.Birthdate=SRObj.Date_of_Birth__c;
                                         conObj.Gender__c=SRObj.Gender__c;
                                         conObj.Place_of_Issue__c=SRObj.Passport_Place_of_Issue__c;
                                         conObj.Date_of_Issue__c=SRObj.Passport_Date_of_Issue__c;
                                         conObj.Passport_Expiry_Date__c=SRObj.Passport_Date_of_Expiry__c;
                                         conObj.Address_Line_1_HC__c=SRObj.Street_Address__c;
                                         conObj.Address_Line_2_HC__c=SRObj.Name_of_Addressee__c;
                                         conObj.CITY1__c=SRObj.City_Town__c;
                                         conObj.Emirate_State_Province__c=SRObj.Emirate_State__c;
                                         conObj.PO_Box__c=SRObj.PO_BOX__c;
                                         conObj.MobilePhone=SRObj.Send_SMS_To_Mobile__c;
                                         conObj.Email=SRObj.Email__c;
                                         conObj.COUNTRY_ENGLISH2__c=SRObj.Name_of_the_Country_GS__c;
                                         if(SRObj.Confirm_Change_of_Entity_Name__c!=null){
                                             conObj.Does_the_individual_have_a_valid_UAE_res__c=SRObj.Confirm_Change_of_Entity_Name__c;
                                         }
                                         if(SRObj.Confirm_Change_of_Trading_Name__c!=null){
                                         conObj.Does_the_individual_reside_in_the_UAE__c=SRObj.Confirm_Change_of_Trading_Name__c;
                                         }
                                         contactUpdateList.add(conObj);
                                     }
               
                if(!contactUpdateList.isEmpty()){
                    update contactUpdateList;
                }
                for (User objUsr: [SELECT id, Email,Phone FROM User WHERE ContactId=:SRObj.Declaration_Signatory_Name__c  limit 1 ]){
                                        objUsr.phone= SRObj.Send_SMS_To_Mobile__c;
                                        objUsr.Email=SRObj.Email__c;
                                        userUpdateList.add(objUsr);
                                    }
                if(!userUpdateList.isEmpty()){
                    Update userUpdateList;
                }
            }catch(Exception ex){Log__c objLog = new Log__c();objLog.Description__c = ex.getMessage() +'   '+ex.getLineNumber();objLog.Type__c = 'UpdatePassportDetailsExtCrl';insert objLog; } 
            return status;
        }
        
        
        // Create Amendment
        public static void createAmendmentforSR(Service_Request__c SRObj){
			Service_Request__c SR = [select id,Declaration_Signatory_Name__c,Passport_Number__c,Nationality_list__c,Place_of_Birth__c,Date_of_Birth__c,
                                        Gender__c,Passport_Place_of_Issue__c,Passport_Date_of_Issue__c,Passport_Date_of_Expiry__c,
                                        Street_Address__c,Name_of_Addressee__c,City_Town__c,Emirate_State__c,PO_BOX__c,Send_SMS_To_Mobile__c,Email__c,
                                        Name_of_the_Country_GS__c,Confirm_Change_of_Entity_Name__c,Confirm_Change_of_Trading_Name__c,Sponsor_Middle_Name__c,
                                        Customer__c,First_Name__c,Last_Name__c from Service_Request__c where id=:SRObj.id limit 1];
                                        
            Amendment__c AmdObj = new Amendment__c();
            AmdObj.ServiceRequest__c = SR.id;
            AmdObj.Customer__c = SR.Customer__c;
            AmdObj.Contact__c = SR.Declaration_Signatory_Name__c;
            AmdObj.Given_Name__c = SR.First_Name__c;
            AmdObj.Family_Name__c = SR.Last_Name__c;
            AmdObj.Person_Email__c = SR.Email__c;
            AmdObj.Mobile__c = SR.Send_SMS_To_Mobile__c;
            AmdObj.Phone__c = SR.Send_SMS_To_Mobile__c;
            AmdObj.Office_Phone_Number__c = SR.Send_SMS_To_Mobile__c;
            AmdObj.Gender__c=SR.Gender__c;
            AmdObj.Date_of_Birth__c=SR.Date_of_Birth__c;
            AmdObj.Place_of_Birth__c=SR.Place_of_Birth__c;
            AmdObj.Nationality_list__c=SR.Nationality_list__c;
            AmdObj.Passport_No__c= SR.Passport_Number__c;
            AmdObj.Passport_Expiry_Date__c =SR.Passport_Date_of_Expiry__c;
            AmdObj.Apt_or_Villa_No__c=SR.Street_Address__c;
            AmdObj.Building_Name__c=SR.Name_of_Addressee__c;
            AmdObj.Emirate_State__c=SR.Emirate_State__c;
            AmdObj.Permanent_Native_City__c=SR.City_Town__c;
            AmdObj.Emirate__c=SR.Name_of_the_Country_GS__c;
            AmdObj.PO_Box__c=SR.PO_BOX__c;
            AmdObj.Status__c='Draft';
            AmdObj.Amendment_Type__c='Individual';
            AmdObj.Permanent_Native_Country__c=SR.Name_of_the_Country_GS__c;
            AmdObj.RecordTypeId =  Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Individual').getRecordTypeId();
            AmdObj.Amendment_Unique_Id__c=SR.id;
            try{
                upsert AmdObj Amendment_Unique_Id__c;  
            }catch (DmlException e) {
                System.debug(e.getMessage());
            }	
        }
		 
        
    }