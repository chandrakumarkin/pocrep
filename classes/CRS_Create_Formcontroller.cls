public without sharing class CRS_Create_Formcontroller {

public  CRS_Form__c crsform{get;set;}


    public CRS_Create_Formcontroller(ApexPages.StandardController controller) 
    {
        this.crsform= (CRS_Form__c)controller.getRecord();

    }
     public PageReference submitNull() 
     {
       ID contactId = [Select contactid from User where id =: Userinfo.getUserid()].contactId;
       
       if(contactId !=null)
       {
          Contact AccID  = [Select AccountID,Account.Name,Account.Registration_License_No__c from Contact where id =: contactId ];
           System.debug('AccID ===>'+AccID);
           crsform.CRS_Year__c=label.CRS_Year_ID;
           crsform.Account__c=AccID.AccountID;
           crsform.Official_FI_Legal_Name__c=AccID.Account.Name;
           crsform.FI_ID__c=AccID.Account.Registration_License_No__c;
           crsform.Status__c='Submitted';
           crsform.Is_Null_return__c='YES';
           upsert crsform;
           return new PageReference('/'+crsform.id) ;
       
       
       }
     return null;
     }
     public PageReference submit() 
     {
    
       ID contactId = [Select contactid from User where id =: Userinfo.getUserid()].contactId;
       
       if(contactId !=null)
       {
      
       Contact AccID  = [Select AccountID,Account.Name,Account.Registration_License_No__c from Contact where id =: contactId ];
       System.debug('AccID ===>'+AccID);
       
     //  List<CRS_Form__c> ListObj=[select ID from CRS_Form__c where Account__c=:AccID.AccountID and Status__c!='Cancelled' and Tax_Residence_Country_new__c=:crsform.Tax_Residence_Country_new__c and CRS_Year__c=:label.CRS_Year_ID];
   //    System.debug('Obj===>'+ListObj);
       
       //[select ID from CRS_Form__c where Account__c='0012000001ZUn7XAAT' and CRS_Year__c='a2j3E000000vMnh'];
       
     //   if(ListObj.isEmpty())
       {
           
           crsform.CRS_Year__c=label.CRS_Year_ID;
           crsform.Account__c=AccID.AccountID;
           crsform.Official_FI_Legal_Name__c=AccID.Account.Name;
           crsform.FI_ID__c=AccID.Account.Registration_License_No__c;
           
           upsert crsform;
           return new PageReference('/'+crsform.id+'/e?retURL=/'+crsform.id) ;
       }
       /*
       else
       {
           crsform=ListObj[0];
           return new PageReference('/'+crsform.id) ;
       }
      */
        
       // PageReference acctPage = new ApexPages.StandardController(Obj).View();
        //acctPage.setRedirect(true);
        
       
       
        
          
       }
     
return null;
  
  }
    

}