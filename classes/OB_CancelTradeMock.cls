@isTest
global class OB_CancelTradeMock implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        OB_INF_CancelTradeName_WS.CancelTradeNameResponse_element res = new OB_INF_CancelTradeName_WS.CancelTradeNameResponse_element();
        res.P_OP_TransactionNumber = 12345;
        res.P_OP_Success = 'true';
        res.P_OP_ArabicMessage = 'abc';
        res.P_OP_EnglishMessage = 'abc';
        res.P_OP_LogID = 'ert';
        res.P_OP_Code = 'abc';       
        response.put('response_x', res); 
   }
}