@isTest(seeAllData=false)
global class Test_SAPWebServiceMock implements WebServiceMock,HttpCalloutMock{
    public static List<SAPECCWebService.ZSF_S_RECE_SERV> lstWebServResponse = new List<SAPECCWebService.ZSF_S_RECE_SERV>();
    public static List<SAPCRMWebService.ZSF_IN_LOG > lstWebServResponse1 = new List<SAPCRMWebService.ZSF_IN_LOG >();
    
    global void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType){
        if(endpoint != null && (endpoint.contains('ECC') || endpoint.contains('/sap/bc/srt') || endpoint.contains('eqa') || endpoint.contains('CRM'))){
	        if(endpoint == 'http://ECC.com/sampledata'){
	        	SAPECCWebService.Z_SF_RECEIPT_SERVICEResponse_element respElement = new SAPECCWebService.Z_SF_RECEIPT_SERVICEResponse_element();
		        SAPECCWebService.ZSF_TT_RECE_SERV_OP EX_SF_RS = new SAPECCWebService.ZSF_TT_RECE_SERV_OP();
		        SAPECCWebService.ZSF_TT_ACC_BAL objActBal = new SAPECCWebService.ZSF_TT_ACC_BAL();
		        
		        EX_SF_RS.item = TestGsCRMServiceMock.lstPSAItems;
		        respElement.EX_SF_RS = EX_SF_RS;
		        respElement.EX_SF_RS_ACC = objActBal;
		        response.put('response_x',respElement);
			}
			else if(endpoint == 'http://eqa.com/sampledata'){
				SAPPortalBalanceRefundService.Z_SF_REFUND_PROCESSResponse_element respElement = new SAPPortalBalanceRefundService.Z_SF_REFUND_PROCESSResponse_element();
				SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP IM_SF_RP = new SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP();
				IM_SF_RP.item = TestGsCRMServiceMock.lstNextrefund;
				respElement.EX_SF_RP = IM_SF_RP;
				response.put('response_x',respElement);

			}
            else if(endpoint == 'http://CRM.com/sampledata'){
                SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element respElement = new SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element();
            }
			else{
	        	SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element resElement = new SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element();
	        	SAPGSWebServices.ZSF_TT_GS_NEXT_ACT EX_GS_NEXT_ACT = new SAPGSWebServices.ZSF_TT_GS_NEXT_ACT();
		        SAPGSWebServices.ZSF_TT_ACC_BAL EX_GS_SERV_ACC = new SAPGSWebServices.ZSF_TT_ACC_BAL();
		        SAPGSWebServices.ZSF_TT_GS_SERV_OP EX_GS_SERV_OUT = new SAPGSWebServices.ZSF_TT_GS_SERV_OP();
				
				EX_GS_NEXT_ACT.item = TestGsCRMServiceMock.lstNextActivity;
				EX_GS_SERV_ACC.item = TestGsCRMServiceMock.lstAccountBal;
				EX_GS_SERV_OUT.item = TestGsCRMServiceMock.lstGSPriceItems;
				
				resElement.EX_GS_NEXT_ACT = EX_GS_NEXT_ACT;
				resElement.EX_GS_SERV_ACC = EX_GS_SERV_ACC;
				resElement.EX_GS_SERV_OUT = EX_GS_SERV_OUT;
				response.put('response_x',resElement);
	        }
        }else{
        	GsSAPCRMService.Z_SF_GS_PROCESSResponse_element respElement = new GsSAPCRMService.Z_SF_GS_PROCESSResponse_element();
	        GsSAPCRMService.ZSF_IN_LOGT EX_GS_OUT = new GsSAPCRMService.ZSF_IN_LOGT(); 
	        
	        /*GsSAPCRMService.ZSF_IN_LOG objOut = new GsSAPCRMService.ZSF_IN_LOG();
	        
	        objOut.ACTION = 'P';
	        objOut.SFREFNO = '0031100000j2ri6';
	        objOut.SFGUID = '0031100000j2ri6';
	        objOut.PARTNER1 = '001234';
	        objOut.PARTNER2 = '001239';
	        objOut.MSG = 'Partner Created Successfully RC';
			TestGsCRMServiceMock.lstCRMResponse.add(objOut);
			*/
	        
	        EX_GS_OUT.item = TestGsCRMServiceMock.lstCRMResponse;
	        respElement.EX_GS_OUT = EX_GS_OUT;
	        response.put('response_x',respElement);
        }
	}
    
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody('<xml><EncodedData>TestBarCodeservice</EncodedData></xml>');
        res.setStatusCode(200);
        return res;
    }
}