@isTest
public with sharing class AttachmentTriggerHandlerTest{

    public static Case caseobj;
    
    @isTest static void test(){
        caseObj = new Case(
        GN_Type_of_Feedback__c = 'Complaint',
        Type = 'Parking',
        Status = 'In Progress',
        Origin = 'Phone',
        Subject = 'Testing',
        GN_Proposed_Due_Date__c = System.today(),
        Full_Name__c='TEST');
        
        insert caseObj;

        
        test.startTest();
            Attachment objAtt = new Attachment();
            objAtt.Name = 'test.exe';
            objAtt.Body = Blob.valueOf('TEST');
            objAtt.ParentId = caseObj.Id;
            try{
                insert objAtt;
            } catch(Exception e){
                Boolean expectedExceptionThrown =  e.getMessage().contains('These file extensions are not alllowed: .exe .bat') ? true : false;
                System.AssertEquals(true, true);
            }
        test.stopTest();
    }
    //V1.1: Test Coverage for creating content document for attachments created by drawloop fo Onboarding
    @isTest static void obAttachmentUnitTest(){

        // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'GENERAL'});
        insert listDocMaster;
        // create SR Template doc
        
         //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle','AOR_Financial'});
        insert createSRTemplateList;
        
        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        insert listSRTemplateDoc;
        
        
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
       
        
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(1, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        
        insertNewSRs[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        insert insertNewSRs;
        
        
        // create SR Doc
        List<HexaBPM__SR_Doc__c> listSRDoc = new List<HexaBPM__SR_Doc__c>();
        listSRDoc = OB_TestDataFactory.createSRDoc(1, insertNewSRs, listDocMaster, listSRTemplateDoc);
        insert listSRDoc;

        listSRDoc[0].HexaBPM__Sys_IsGenerated_Doc__c = true;
        update listSRDoc;


        test.startTest();
            Attachment objAtt = new Attachment();
            objAtt.Name = 'test.txt';
            objAtt.Body = Blob.valueOf('TEST');
            objAtt.ParentId = listSRDoc[0].Id;
            insert objAtt;
        test.stopTest();
    }
        
}