public without sharing class OB_EntityUBOContainerHelper 
{
    
     

   //* v1.0 Merul  26 march 2020  Libary implementation
   public static List<Relationship__c> getRelationShip( OB_EntityUBOContainerController.RequestWrapper reqWrap )
   {
       
       List<Relationship__c> relLst = new  List<Relationship__c>();
      /*
       if( reqWrap.mapRelatedAccForRelshp != NULL 
               && reqWrap.mapRelatedAccForRelshp.size() > 0)
       {
       */
           relLst = [SELECT id,
                               Object_Contact__c,
                               Object_Account__c,
                               Subject_Account__c,
                               Relationship_Type__c,
                               
                               // Contact
                               Object_Contact__r.Salutation,
                               Object_Contact__r.LastName,
                               Object_Contact__r.FirstName,
                               Object_Contact__r.Birthdate,
                               Object_Contact__r.Place_of_Birth__c,
                               Object_Contact__r.Passport_No__c,
                               Object_Contact__r.Nationality__c,
                               Object_Contact__r.Phone,
                               Object_Contact__r.Email,
                               Object_Contact__r.Office_Unit_Number__c,
                               Object_Contact__r.ADDRESS_LINE1__c,
                               Object_Contact__r.PO_Box_HC__c,
                               Object_Contact__r.CITY1__c,
                               Object_Contact__r.Emirate_State_Province__c,
                               Object_Contact__r.Country__c,
                               Object_Contact__r.Passport_Expiry_Date__c,
                               Object_Contact__r.Gender__c,
                               Object_Contact__r.Issued_Country__c,
                               Object_Contact__r.Place_of_Issue__c,
                               Object_Contact__r.MobilePhone,
                               Object_Contact__r.Are_you_a_resident_in_the_U_A_E__c,
                               Object_Contact__r.Is_this_individual_a_PEP__c,
                               Object_Contact__r.Country_Income_Source__c,
                               Object_Contact__r.UID_Number__c,
                               Object_Contact__r.Emirate__c,
                               Object_Contact__r.Details_on_source_of_income__c,
                               Object_Contact__r.source_of_income_wealth__c,
                               Object_Contact__r.Type_of_Authority__c,
                               Object_Contact__r.Type_of_Authority_Other__c,
                               Object_Contact__r.Date_of_Issue__c,

                               
                               //Account
                               Object_Account__r.Is_Designated_Member__c,
                               Object_Account__r.Name,
                               Object_Account__r.Former_Name__c,
                               Object_Account__r.Phone,
                               Object_Account__r.Place_of_Registration_Text__c,
                               Object_Account__r.Office__c,
                               Object_Account__r.Building_Name__c,
                               Object_Account__r.PO_Box__c,
                               Object_Account__r.City__c,
                               Object_Account__r.Emirate_State__c,
                               Object_Account__r.Country__c,
                               Object_Account__r.Source_of_Income_and_Origin_of_Wealth__c,
                               Object_Account__r.Email_Address__c,
                               Object_Account__r.Emirate__c,
                               Object_Account__r.Jurisdiction__c,
                               Object_Account__r.Mobile_Number__c,
                               Object_Account__r.Nationality__c,
                               Object_Account__r.Source_of_income_wealth__c,
                               Object_Account__r.Please_state_the_source_of_income_weal__c,
                               Object_Account__r.Trade_Name__c,
                               Object_Account__r.Qualifying_Type__c,
                               Object_Account__r.ROC_reg_incorp_Date__c,
                               Object_Account__r.Name_of_Regulator_Exchange_Govt__c

                        FROM  Relationship__c
                        WHERE Status__c = 'Active'
                        //AND Subject_Account__c IN: reqWrap.mapRelatedAccForRelshp.keySet() 
                           
                        
                   ];
       // }
       
       return relLst;
   } 


   //* v1.0 Merul  26 march 2020  Libary implementation
   public static OB_EntityUBOContainerController.AmendmentWrapper  setAmedFromObjAccount(  
                                                                                            OB_EntityUBOContainerController.AmendmentWrapper amedWrap, 
                                                                                            Relationship__c relShip
                                                                                        )
{
        
        amedWrap.amedObj.Company_Name__c = relShip.Object_Account__r.Name;
        amedWrap.amedObj.Is_this_member_a_designated_Member__c =( relShip.Object_Account__r.Is_Designated_Member__c ? 'Yes' : 'No');
        amedWrap.amedObj.Former_Name__c = relShip.Object_Account__r.Former_Name__c;
        amedWrap.amedObj.Telephone_No__c = relShip.Object_Account__r.Phone;
        amedWrap.amedObj.Place_of_Registration__c = relShip.Object_Account__r.Place_of_Registration_Text__c; 
        amedWrap.amedObj.Apartment_or_Villa_Number_c__c = relShip.Object_Account__r.Office__c; 
        amedWrap.amedObj.Address__c = relShip.Object_Account__r.Building_Name__c; 
        amedWrap.amedObj.PO_Box__c = relShip.Object_Account__r.PO_Box__c; 
        amedWrap.amedObj.Permanent_Native_City__c = relShip.Object_Account__r.City__c; 
        amedWrap.amedObj.Emirate_State_Province__c = relShip.Object_Account__r.Emirate_State__c; 
        amedWrap.amedObj.Permanent_Native_Country__c = relShip.Object_Account__r.Country__c;  
        amedWrap.amedObj.Country_of_source_of_income__c = relShip.Object_Account__r.Source_of_Income_and_Origin_of_Wealth__c; 
        amedWrap.amedObj.Email__c = relShip.Object_Account__r.Email_Address__c;    
        amedWrap.amedObj.Emirate__c = relShip.Object_Account__r.Emirate__c;   
        amedWrap.amedObj.Jurisdiction__c = relShip.Object_Account__r.Jurisdiction__c;  
        amedWrap.amedObj.Mobile__c = relShip.Object_Account__r.Mobile_Number__c;  
        amedWrap.amedObj.Nationality_list__c = relShip.Object_Account__r.Nationality__c;  
        amedWrap.amedObj.Source_and_origin_of_funds_for_entity__c = relShip.Object_Account__r.Source_of_income_wealth__c;  
        amedWrap.amedObj.Source_of_income_wealth__c = relShip.Object_Account__r.Please_state_the_source_of_income_weal__c;  
        amedWrap.amedObj.Trading_Name__c = relShip.Object_Account__r.Trade_Name__c;  
        amedWrap.amedObj.Select_the_Qualified_Applicant_Type__c = relShip.Object_Account__r.Qualifying_Type__c;  
        amedWrap.amedObj.Financial_Service_Regulator__c = relShip.Object_Account__r.Name_of_Regulator_Exchange_Govt__c;  
        amedWrap.amedObj.Date_of_Registration__c = relShip.Object_Account__r.ROC_reg_incorp_Date__c;  
        
        return amedWrap;
}


//* v1.0 Merul  26 march 2020  Libary implementation
public static OB_EntityUBOContainerController.AmendmentWrapper  setAmedFromObjContact(  
                                                                                            OB_EntityUBOContainerController.AmendmentWrapper amedWrap, 
                                                                                            Relationship__c relShip
                                                                                          )
{
    
        amedWrap.amedObj.Title__c = relShip.Object_Contact__r.Salutation;
        amedWrap.amedObj.Last_Name__c = relShip.Object_Contact__r.LastName;
        amedWrap.amedObj.First_Name__c = relShip.Object_Contact__r.FirstName;
        amedWrap.amedObj.Date_of_Birth__c = relShip.Object_Contact__r.Birthdate;
        amedWrap.amedObj.Place_of_Birth__c = relShip.Object_Contact__r.Place_of_Birth__c ;
        amedWrap.amedObj.Passport_No__c = relShip.Object_Contact__r.Passport_No__c ;
        amedWrap.amedObj.Nationality_list__c = relShip.Object_Contact__r.Nationality__c ;
        amedWrap.amedObj.Mobile__c = relShip.Object_Contact__r.Phone ;
        amedWrap.amedObj.Email__c = relShip.Object_Contact__r.Email ;
        amedWrap.amedObj.Apartment_or_Villa_Number_c__c = relShip.Object_Contact__r.Office_Unit_Number__c;
        amedWrap.amedObj.Address__c = relShip.Object_Contact__r.ADDRESS_LINE1__c;
        amedWrap.amedObj.PO_Box__c = relShip.Object_Contact__r.PO_Box_HC__c;
        amedWrap.amedObj.Permanent_Native_City__c = relShip.Object_Contact__r.CITY1__c;
        amedWrap.amedObj.Emirate_State_Province__c = relShip.Object_Contact__r.Emirate_State_Province__c;
        amedWrap.amedObj.Permanent_Native_Country__c = relShip.Object_Contact__r.Country__c;
        amedWrap.amedObj.Passport_Expiry_Date__c = relShip.Object_Contact__r.Passport_Expiry_Date__c;
        amedWrap.amedObj.Gender__c = relShip.Object_Contact__r.Gender__c;
        amedWrap.amedObj.Country__c = relShip.Object_Contact__r.Issued_Country__c;
        amedWrap.amedObj.Place_of_Issue__c = relShip.Object_Contact__r.Place_of_Issue__c;
        amedWrap.amedObj.Mobile__c = relShip.Object_Contact__r.MobilePhone;
        amedWrap.amedObj.Passport_Issue_Date__c = relShip.Object_Contact__r.Date_of_Issue__c;
        amedWrap.amedObj.Are_you_a_resident_in_the_U_A_E__c = relShip.Object_Contact__r.Are_you_a_resident_in_the_U_A_E__c;
        amedWrap.amedObj.Is_this_individual_a_PEP__c = relShip.Object_Contact__r.Is_this_individual_a_PEP__c;
        amedWrap.amedObj.Country_of_source_of_income__c = relShip.Object_Contact__r.Country_Income_Source__c;
        amedWrap.amedObj.U_I_D_No__c = relShip.Object_Contact__r.UID_Number__c;
        amedWrap.amedObj.Emirate__c = relShip.Object_Contact__r.Emirate__c;
        amedWrap.amedObj.Source_and_origin_of_funds_for_entity__c = relShip.Object_Contact__r.Details_on_source_of_income__c;
        amedWrap.amedObj.Source_of_income_wealth__c = relShip.Object_Contact__r.source_of_income_wealth__c;
        amedWrap.amedObj.Type_of_Authority__c = relShip.Object_Contact__r.Type_of_Authority__c;
        amedWrap.amedObj.Please_provide_the_type_of_authority__c = relShip.Object_Contact__r.Type_of_Authority_Other__c;
   
        return amedWrap;
}

}