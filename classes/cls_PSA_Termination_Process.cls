/*
        Author      :   Arun 
        Description :   Class to suport SR PSA Termination Service request 
        
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    29-06-2020 Arun Singh  Created
    
*/    

public with sharing class cls_PSA_Termination_Process{

    public Service_Request__c SRData{get;set;}
    public map<string,string> mapParameters;
    string RecordTypeId;

    public cls_PSA_Termination_Process(ApexPages.StandardController controller) 
    {
        List<String> fields = new List<String> {'No_of_documents_lost__c','Additional_Details__c','Expiry_Date__c','Customer__c','Customer__r.Registration_License_No__c','Customer__r.Index_Card_Status__c','Customer__r.BP_No__c','Customer__r.Index_Card__r.Name','Customer__r.Sponsored_Employee__c','Customer__r.ROC_Status__c','Reason_for_Renewal__c','Quantity__c','Entity_Name__c','Customer__r.Name','Court_Order__c'};
        
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
              SRData=(Service_Request__c )controller.getRecord();
        
        mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();              
       if(mapParameters.get('RecordType')!=null) 
       RecordTypeId= mapParameters.get('RecordType'); 
        RecordTypeId=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('PSA_Termination').getRecordTypeId();
           
       
        if(SRData.id==null)
        {
           SRData.RecordTypeId=RecordTypeId;
        } 
        else if(SRData.Entity_Name__c==null)
        {
            
            SRData.Email__c = UserInfo.getUserEmail();
            SRData.Entity_Name__c=SRData.Customer__r.Name;
        }   
        
        SRData.Would_you_like_to_opt_our_free_couri__c ='No';
       
    }
    
     //Reporting Financial Institution type
    public  void RecordvalueChange()
    {
        string fieldChanged= Apexpages.currentPage().getParameters().get('field_name');
        string fieldValue= Apexpages.currentPage().getParameters().get('field_value');
         
         
        System.debug('fieldValue===>'+fieldValue);
        System.debug('fieldChanged===>'+fieldChanged);
        
        if(!string.isblank(fieldChanged) && !string.isblank(fieldValue))
        {
      Account  ObjAccount;
        List<Account> ListAccount=[select id,Name,Registration_License_No__c,BP_No__c,Index_Card_Status__c,Index_Card__r.Name,Sponsored_Employee__c,ROC_Status__c from account where id=:fieldValue];
        if(!ListAccount.isEmpty())
        {
                ObjAccount=ListAccount[0];
                SRData.Customer__r=ObjAccount;
                 SRData.Entity_Name__c=ObjAccount.Name;
                SRData.put(fieldChanged,fieldValue);
        }

        
         List<user> ListContact=[select ContactId,Email,Phone from User where Contact.AccountId=:fieldValue AND IsActive=true AND Community_User_Role__c INCLUDES ('Employee Services') order by createdDate limit 1];
         
         if(!ListContact.isempty() && ObjAccount!=null)
         {
             SRData.Email__c = ListContact[0].Email;
             SRData.Send_SMS_To_Mobile__c = ListContact[0].Phone;
             SRData.Entity_Name__c=ObjAccount.Name;
         }
         
         
         
        
        
        
        }
        
        
    }
    
    
    public  PageReference SaveRecord()
    {
    
     try     
        {  
            
            string Status=SRData.Customer__r.ROC_Status__c;
            if(SRData.Customer__r.ROC_Status__c!='Active')
            Status='inactive';
            

            
            
            SRData.Commercial_Activity__c=SRData.No_of_documents_lost__c+'-'+Status;
          upsert SRData;
            PageReference acctPage = new ApexPages.StandardController(SRData).view();        
            acctPage.setRedirect(true);        
            return acctPage;     
            
        }
         catch (Exception e)     
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
            ApexPages.addMessage(myMsg);    
        }  
        
        return null;
    }
    

}