@isTest
public class DCCATradeLicenseInfoWSTest {
    
    @isTest
    static void testmethod1(){
        Test.startTest();
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insertNewAccounts[0].Registration_License_No__c = '1234';
        
        insertNewAccounts[0].ROC_reg_incorp_Date__c = System.today().addMonths(3);
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Private' ;
        
        insertNewAccounts[0].Trading_Name_Arabic__c = 'ذا ستيك بار';
        insertNewAccounts[0].Trade_Name__c = 'Abc corp123';
        
        insert insertNewAccounts;
        
        
        License_Activity_Master__c lm = new License_Activity_Master__c();
        lm.Name = 'Restaurant';
        lm.Activity_Code__c = 'RESTAURANT';
        lm.Enabled__c = true;
        lm.Sys_Code__c  = '255';
        lm.Type__c = 'Business Activity';
        insert lm;
        
        
        License_Activity__c act = new License_Activity__c();
        act.Account__c  = insertNewAccounts[0].Id;
        act.Activity__c = lm.Id;
        act.Start_Date__c = System.today();
        act.End_Date__c   = System.today() + 90;
        insert act;
        
        
        RestRequest request = new RestRequest();
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        
        request.requestUri = baseUrl+'/services/apexrest/difccomdata/';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"registrationNumber": "1234"}');
        RestContext.request = request;
        
        DCCATradeLicenseInfoWS.DCCATradeLicenseResponseWrapper resp = DCCATradeLicenseInfoWS.jsonResponse();
        
        Test.stopTest();

    }
    
    
    @isTest
    static void testmethod2(){
        Test.startTest();
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insertNewAccounts[0].Registration_License_No__c = '5678';
        
        insertNewAccounts[0].ROC_reg_incorp_Date__c = System.today().addMonths(3);
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Private' ;
        
        insertNewAccounts[0].Trading_Name_Arabic__c = 'ذا ستيك بار';
        insertNewAccounts[0].Trade_Name__c = 'Abc corp123';
        
        insert insertNewAccounts;
        
        

        
        
        RestRequest request = new RestRequest();
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        
        request.requestUri = baseUrl+'/services/apexrest/difccomdata/';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"registrationNumber": "5678"}');
        RestContext.request = request;
        
        DCCATradeLicenseInfoWS.DCCATradeLicenseResponseWrapper resp = DCCATradeLicenseInfoWS.jsonResponse();
        
        Test.stopTest();

    }
    
    
        @isTest
    static void testmethod3(){
        Test.startTest();
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        insertNewAccounts[0].Registration_License_No__c = '5678';
        
        insertNewAccounts[0].ROC_reg_incorp_Date__c = System.today().addMonths(3);
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Private' ;
        
        insertNewAccounts[0].Trading_Name_Arabic__c = 'ذا ستيك بار';
        insertNewAccounts[0].Trade_Name__c = 'Abc corp123';
        
        insert insertNewAccounts;
        
        

        
        
        RestRequest request = new RestRequest();
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        
        request.requestUri = baseUrl+'/services/apexrest/difccomdata/';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"registrationNumber": "7777"}');
        RestContext.request = request;
        
        DCCATradeLicenseInfoWS.DCCATradeLicenseResponseWrapper resp = DCCATradeLicenseInfoWS.jsonResponse();
        
        Test.stopTest();

    }
    
    
            @isTest
    static void testmethod4(){
        Test.startTest();
        
        
        RestRequest request = new RestRequest();
        String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        
        request.requestUri = baseUrl+'/services/apexrest/difccomdata/';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"registrationNumber": ""}');
        RestContext.request = request;
        
        DCCATradeLicenseInfoWS.DCCATradeLicenseResponseWrapper resp = DCCATradeLicenseInfoWS.jsonResponse();
        
        Test.stopTest();

    }
    
}