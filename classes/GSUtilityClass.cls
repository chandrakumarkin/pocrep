/*
* GSUtilityClass Class
* Author      : Azfer Pervaiz
* Date        : 2019-03-24     
* Modification History:
*****************************************************************************************************************
* Version       Date                Author              Remarks                                                 
* 1.0           2019-03-24          Azfer Pervaiz       Created The Class.
* 1.0           2019-03-24          Azfer Pervaiz       Added Method : CheckServiceRequestForFine
* 1.1           19th June 2019      Mudasir             Updated filter and changed the query to count query
* 1.2           19th August 2019    Fazal               Added the Invocable method for ticket number 7198
* 1.3           21 - 04-2-21        Veera               Addded to get Marital Status  
*****************************************************************************************************************/
public class GSUtilityClass
{
    public static Boolean CheckServiceRequestForFine(Id paramSRId, Decimal paramFineAmount)
    {
        if([SELECT count() FROM Service_Request__c WHERE Linked_SR__c =: paramSRId AND Fine_Amount_AED__c =: paramFineAmount AND External_Status_Name__c='Approved' AND Record_Type_Name__c = 'Over_Stay_Fine' AND SAP_Unique_No__c != null AND Id != null] == 0)
        {
            return true;
        }else
        {return false;}
    }
    
    //1.3 -- start
    public static string getMaritalStatusCode (string MaritalStatus){
    
       if(MaritalStatus == 'Single') return '0';
       if(MaritalStatus == 'Married') return '1';
       if(MaritalStatus == 'Widow') return '2';
       if(MaritalStatus == 'Divorced') return '3';       
       if(MaritalStatus == 'Unspecific') return '6';
       if(MaritalStatus == 'Child') return '7';
       return null;
      
       
    }
    //1.3 -- end
      
@InvocableMethod 
public static void CertificateUploadedGS(List<ID> paramListId)
{
    List <Step__c> listSteps = new List <Step__c>();
    GS_Medical_Certificate_Upload__mdt GSMedicalCertificateUpload = new GS_Medical_Certificate_Upload__mdt();
    GSMedicalCertificateUpload = [SELECT Step_names__c from GS_Medical_Certificate_Upload__mdt where label = 'Step_Name' ];
    List<String> ListStepName = new list<string>();
    if( GSMedicalCertificateUpload.Step_names__c.contains(',') ){
     for( String Stepname : GSMedicalCertificateUpload.Step_names__c.split(',')){
        ListStepName.add(Stepname);}}
     else
     {ListStepName.add( GSMedicalCertificateUpload.Step_names__c );}
     system.debug('liststepname!!!!!!'+ListStepName);
     for( Step__c StepObj : [select Name, id,Step_Name__c,Health_Insurance_Uploaded__c from Step__c where SR__c in: paramListId and Step_Name__c in: ListStepName])
     {
         StepObj.Health_Insurance_Uploaded__c = true;ListSteps.add(StepObj);
     }
 }
    }