public class CC_CreateRelationshipQueueable implements Queueable{
    
    private Map<Id,HexaBPM_Amendment__c> amndMap = new Map<Id,HexaBPM_Amendment__c>();
    
    // list of shareholder record per amendment
    private Map<id,list<Shareholder_Detail__c>> sharholderExistingMap = new Map<id,list<Shareholder_Detail__c>>();
    
    // set account registration number for checking existing in system
    private Set<String> registrationNumberSet = new Set<String>();
    
    // map of passport and passpport-Nationality 
    private Map<String,String> passportNationalityMap = new Map<String,String>();
    
    
    // store existing passport number  and contact ID 
    private Map<String,string> passportContactIdMap = new Map<String,string>(); 
    
    
    // insert new account and contact detail
    private Map<string,sObject> accountContactNewMap = new Map<string,sObject>();
    
    
    public CC_CreateRelationshipQueueable (Map<Id,HexaBPM_Amendment__c> amndMp,Map<id,list<Shareholder_Detail__c>> sharholderExistingMp, Map<String,String> passportNationalityMp,Map<string,sObject> accountContactNewMp){
        
        this.amndMap = amndMp;
        this.sharholderExistingMap = sharholderExistingMp;
        this.passportNationalityMap = passportNationalityMp;
        this.accountContactNewMap = accountContactNewMp;
    }
    
    
    
    public void execute(QueueableContext qc){
        CC_CreateRelationship.createRelatioshipBulk(amndMap, sharholderExistingMap, passportNationalityMap, accountContactNewMap);
    }
}