/**
 * Created by Mudasir on 17 October 2019
 * For the Fitout related deployments below test classes were used 
 * Test_CC_Cls_FitOutCustomCode_Addl,Test_Cls_EventRequestAccessForm,Test_CustomCode_UtilCls,Test_FO_SRValidations,Test_FO_cls_DocumentViewer,Test_SRTrigger,Test_SRValidation,Test_StepTrigger,Test_cls_FitOutandEventCustomCode,Test_cls_InspectionChecklist,Test_cls_fitout_phase_two
 * In addition I have incorporated this test class
 */
@isTest
private class CC_cls_FitOutandEventCustomCode_Test {

    static testMethod void lookAndFeelIsRequiredAtStepTest() {
        Account accRec = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
        insert accRec;
        
        Map<String , String> mapOfServCategoryStepNames = new Map<String , String>();
        mapOfServCategoryStepNames.put('Hoarding and or Graphics','Upload Detailed Design Documents');
        mapOfServCategoryStepNames.put('Shop Front','Request for final inspection');
        List<look_And_Feel_Service_Required__mdt> lookAndFeelServiceRequiredMDT = Test_CC_FitOutCustomCode_DataFactory.getTestLookAndFeelServiceRequiredMDT(mapOfServCategoryStepNames);
        
        Service_Request__c servRec = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        SR_Status__c srStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSRStatus('Approved', 'Approved');
        servRec.Customer__c = accRec.id;
        servRec.Internal_SR_Status__c = srStatus.id;
        servRec.External_SR_Status__c = srStatus.id;
        servRec.service_category__c = lookAndFeelServiceRequiredMDT[0].Service_Category__c;
        system.debug('servRec---'+servRec);
        insert servRec;
        SR_Template__c srTemp = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Look and Feel Approval Request','Look_Feel_Approval_Request','Fit - Out & Events');
        insert srTemp;
        Service_Request__c servRecLook = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        servRecLook.service_category__c = lookAndFeelServiceRequiredMDT[0].Service_Category__c;
        servRecLook.Linked_SR__c =servRec.id;
        servRecLook.SR_Template__c = srTemp.id;
        servRecLook.External_SR_Status__c = srStatus.id;
        insert servRecLook;
        
        
        Status__c status = Test_CC_FitOutCustomCode_DataFactory.getTestStepStatus('Pending Review', 'Pending_Review');        
        insert status;
        Step_Template__c testStepTemplates = new Step_Template__c(Code__c = 'UPLOAD_DETAILED_DESIGN_DOCUMENTS',Name='Upload Detailed Design Documents',Step_RecordType_API_Name__c = 'Upload_Detailed_Design_Documents');
        insert testStepTemplates;
        step__c step = Test_CC_FitOutCustomCode_DataFactory.getTestStep(servRec.id, status.Id, accRec.Name);
        step.Step_Template__c = testStepTemplates.Id;
        insert step;
        //database.insert(lookAndFeelServiceRequiredMDT, false);  
        CC_cls_FitOutandEventCustomCode.lookAndFeelIsRequiredAtStep(step);
    }
}