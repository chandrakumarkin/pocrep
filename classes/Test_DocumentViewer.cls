/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_DocumentViewer {

     static testMethod void DocumentViewerTest(){
     	
      Account objAccount = new Account();
	  objAccount.Name = 'Test Account';
	  objAccount.Phone = '1234567890';
	  insert objAccount;
      
      Service_Request__c objSR = new Service_Request__c();
      objSR.Customer__c = objAccount.Id;
      objSR.SR_Group__c = 'ROC';
      insert objSR;
      
      SR_Doc__c objSRDoc = new SR_Doc__c();
      objSRDoc.Service_Request__c = objSR.Id;
      insert objSRDoc;
      
         //Shikha
      Product2 objProd = new Product2();
      objProd.Name = 'Dummy';
      objProd.ProductCode = 'Dummy';
      objProd.Family = 'ROC';
      objProd.IsActive = true;
      insert objProd;
         
      SR_Price_Item__c ojPriceItm = new SR_Price_Item__c();
      ojPriceItm.Product__c = objProd.Id;
      ojPriceItm.Price__c = 2100;
      ojPriceItm.ServiceRequest__c = objSR.Id;
      insert ojPriceItm;
      
      Document_Master__c objDocMaster = new Document_Master__c();
      objDocMaster.Name = 'NoticeToPay';
      objDocMaster.Code__c = 'NOTICE_TO_PAY';
      objDocMaster.Available_to_client__c = true;
      insert objDocMaster;
         
      SR_Doc__c objSRDoc1 = new SR_Doc__c();
      objSRDoc1.Service_Request__c = objSR.Id;
      objSRDoc1.Document_Master__c = objDocMaster.Id;
      objSRDoc1.Sys_IsGenerated_Doc__c = true;
      objSRDoc1.Status__c = 'Generated';
      insert objSRDoc1;
         
      Attachment objAttachment = new Attachment();
      objAttachment.Name = 'Test Document';
      objAttachment.Body = blob.valueOf('test');
      objAttachment.ParentId = objAccount.Id;
      //insert objAttachment;
      
      Attachment objAttachment1 = new Attachment();
      objAttachment1.Name = 'Test Document';
      objAttachment1.Body = blob.valueOf('test');
      objAttachment1.ParentId = objSRDoc.Id;
      insert objAttachment1;
      
      Page_Flow__c objPF = new Page_Flow__c();
      objPF.Name = 'Test Flow';
      objPF.Master_Object__c = 'Service_Request__c';
      objPF.Flow_Description__c = 'Test';
      objPF.Record_Type_API_Name__c = 'Application_Of_Registration';
      insert objPF;
        
      Page__c objPg = new Page__c();
      objPg.Name = 'Page1';
      objPg.Page_Description__c = 'Page1';
      objPg.Is_Custom_Component__c = false;
      objPg.Page_Order__c = 1;
      objPg.What_Id__c = 'Service_Request__c';
      objPg.Render_By_Default__c = true;
      objPg.No_Quick_navigation__c = false;
      objPg.VF_Page_API_Name__c = 'Process_Flow';
      objPg.Page_Flow__c = objPF.Id;
      insert objPg;
      
      Section__c ButtonSec = new Section__c();
      ButtonSec.Page__c = objPg.Id;
      ButtonSec.Name = 'Page1 Section2';
      ButtonSec.Section_Description__c = 'Test';
      ButtonSec.Default_Rendering__c = true;
      ButtonSec.Layout__c = '1';
      ButtonSec.Order__c = 2;
      ButtonSec.Section_Title__c = 'Test Type';
      ButtonSec.Section_Type__c = 'CommandButtonSection';
	  insert ButtonSec;
      
      Section_Detail__c NextBtn = new Section_Detail__c();
	  NextBtn.Section__c = ButtonSec.Id;
	  NextBtn.Order__c = 2;
	  NextBtn.Render_By_Default__c = true;
	  NextBtn.Component_Type__c = 'Command Button';
	  NextBtn.Navigation_Directions__c = 'Forward';
	  NextBtn.Button_Location__c = 'Top';
	  NextBtn.Button_Position__c = 'Left';
	  NextBtn.Component_Label__c = 'Client';
	  insert NextBtn;
      
      Apexpages.currentPage().getParameters().put('id',objSR.Id);
      Apexpages.currentPage().getParameters().put('type','upgrade');
      Apexpages.currentPage().getParameters().put('FlowId',objPF.Id);
      Apexpages.currentPage().getParameters().put('PageId',objPg.Id);
      DocumentViewer objDocumentViewer = new DocumentViewer();
      objDocumentViewer.strActionId = NextBtn.id;
      objDocumentViewer.strObjectId = objSR.Id;
      objDocumentViewer.strdifcComments = 'test';
      objDocumentViewer.strExternalComments = 'test';
      objDocumentViewer.setstrObjectName('Service_Request__c');
         //Shikha
         Test.startTest();
      objDocumentViewer.regenerateNOP();
         Test.stopTest();
      objDocumentViewer.getstrObjectName();
      objDocumentViewer.getblnDisableControls();
      objDocumentViewer.setblnDisableControls(true);
      objDocumentViewer.setdisplayTable('True');
      objDocumentViewer.getdisplayTable();
      objDocumentViewer.setFrameDisplay('Account');
      objDocumentViewer.getFrameDisplay();
      objDocumentViewer.setImgDisplay('test');
      objDocumentViewer.getImgDisplay();
      objDocumentViewer.setDocumentID(objAttachment.Id);
      objDocumentViewer.getDocumentID();
      objDocumentViewer.setImagesrc('/'+objAttachment.Id);
      objDocumentViewer.getImagesrc();
      //objDocumentViewer.setremids(objAttachment.Id);
      //objDocumentViewer.getremids();
      objDocumentViewer.getlststrdoc();
      objDocumentViewer.getdvDocsVisibility();
      //objDocumentViewer.setdvDocsVisibility('test');
      //objDocumentViewer.getdvNewDocVisibility();
      //objDocumentViewer.setdvNewDocVisibility('true');
      //objDocumentViewer.getPageId();
      objDocumentViewer.strObjectId = objSR.Id;
      objDocumentViewer.FileName = 'Test Doc.pdf';
      objDocumentViewer.document = objAttachment;
      objDocumentViewer.Cancel();
      objDocumentViewer.strSRDocId = objSRDoc.Id;
      objDocumentViewer.document = objAttachment;
      objDocumentViewer.Upload();
      //objDocumentViewer.remDoc();
      objDocumentViewer.AgrDocId = objSRDoc.Id;
      objDocumentViewer.EditDoc();
      objDocumentViewer.SaveDoc();
      objDocumentViewer.Back_To_SR();
      DocumentViewer.checkGSValidationMethod(objSRDoc.Id);
      Pagereference pg = objDocumentViewer.DynamicButtonAction();
     
    }
}