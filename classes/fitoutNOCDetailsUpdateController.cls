/******************************************************************************************
 *  Version 	 Author       		 Date 		  Description
 * -----------------------------------------------------------------------------------------
 *  V1.1       Vinod Jetti        06-07-2021	  #DIFC2-16822 - updated the response_code__c in Contractor Wallet SR.
*********************************************************************************************/
public class fitoutNOCDetailsUpdateController {
    @AuraEnabled 
    public static  Service_request__c  getRequestData(String stepId)
    {  
        system.debug('stepId---'+stepId);
        Step__c stepRecord = [Select id,SR__c from step__c where id=:stepId];
        Service_request__c serviceRequestRec
            =[
                Select id,name,Additional_Details__c,Building_Name__c,Sys_Registered_Address_CL__c,foreign_registered_level__c,Linked_SR__c,Linked_SR__r.Linked_SR__c 
              	from service_request__c 
              	where id=:stepRecord.SR__c
             ];
        return serviceRequestRec;
    }
    
    @AuraEnabled 
    public static  Service_request__c  saveNOCDetails(Service_request__c serviceRequestRec)
    {  
        Service_request__c serviceRequests
            =[
                Select id,name,Building_Name__c,Sys_Registered_Address_CL__c,foreign_registered_level__c,Linked_SR__c,Linked_SR__r.Linked_SR__c,Additional_Details__c 
              	from service_request__c 
              	where id=:serviceRequestRec.id
             ];

       // V1.1 - response_code__c added by Vinod and update the same in Date_validation_for_fit_out_requests validation rule
        Service_Request__c contractprPortalAccess = new Service_request__c (id=serviceRequests.Linked_SR__r.Linked_SR__c ,foreign_registered_level__c=serviceRequestRec.foreign_registered_level__c,Sys_Registered_Address_CL__c = serviceRequestRec.Sys_Registered_Address_CL__c);
        contractprPortalAccess.response_code__c = 'update'; //V1.1
        update contractprPortalAccess;
				
        Service_Request__c inductionSR =  new Service_request__c (id=serviceRequests.Linked_SR__c ,Additional_Details__c=serviceRequestRec.Additional_Details__c,Building_Name__c= serviceRequestRec.Building_Name__c,foreign_registered_level__c=serviceRequestRec.foreign_registered_level__c,Sys_Registered_Address_CL__c = serviceRequestRec.Sys_Registered_Address_CL__c);
        update inductionSR;
       
        update serviceRequestRec;
        return serviceRequestRec;
    }
}