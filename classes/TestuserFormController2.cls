/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 11-Sep-2014   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class TestuserFormController2 {

    public static Attachment testPassport;
    static Boolean flag;
    static String str;
    Public static user ObjUser;
    public static Account  acc;
    public static SR_Template__c srTemp;
    public static Lookup__c nat;
    public static License__c lic123;
    public static Relationship__c RelUser;
    //public static
    public static void prepareData(){
        String contInd;
        String conRT1;
        
        acc = new Account(Name = 'TestAccount',BP_No__c='0000009999');
        acc.Roc_status__c='Under Formation';
        acc.Registration_License_No__c = '1004';
        insert acc;
        
        SR_Status__c srStatus = new SR_Status__c(name='Submitted',Code__c='SUBMITTED');
        srTemp = new SR_Template__c(name = 'User Access Form',SR_RecordType_API_Name__c='User Access Form');
        nat = new Lookup__c(Type__c='Nationality',Name='United Arab Emirates');
        // License__c lic = new License__c(status__c='Active',Account__c=acc.id);
        
        insert srTemp;
        insert srStatus;
        insert nat;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        lic123 = new License__c(status__c='Active',Account__c=acc.id);
        insert lic123;
        
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='Portal_User' AND SobjectType='Contact' AND IsActive=true]){
              conRT1 = objRT.Id;
        }        
        
        Contact cont = new Contact(firstname='Tim',lastname='sususu',accountId=acc.id,Email='test@mxmanto.com',RecordTypeId=conRT1,Passport_No__c='IN2929292');
        insert cont;
        
        RelUser = new Relationship__c();
        RelUser.Object_Contact__c = cont.id;
        RelUser.Subject_Account__c = acc.id;
        RelUser.Relationship_Type__c = 'Has Principal User';
        RelUser.Start_Date__c = Date.today(); 
        RelUser.Push_to_SAP__c = true;
        RelUser.Active__c = true;
        insert RelUser;
        
        //user ObjUser=[select username,Id from user limit 1];
        
        //ObjUser=[select username,Id,Entity_License_No__c,Community_User_Role__c from user where isActive=true and Accountid!=null and Community_User_Role__c='Company Services; Employee Services; Property Services' and contactID!=null limit 1];
        
        profile testProfile = [SELECT Id 
                               FROM profile
                               WHERE Name = 'DIFC Customer Community User Custom' 
                               LIMIT 1];
        ObjUser= new User(LastName = 'test2', 
                                 Username = 'test@test.com', 
                                 Email = 'test_unique20169@test.com', 
                                 Alias = 'testu1', 
                                 TimeZoneSidKey = 'GMT', 
                                 LocaleSidKey = 'en_GB', 
                                 EmailEncodingKey = 'ISO-8859-1', 
                                 ProfileId = testProfile.Id, 
                                 ContactId = cont.Id,
                                 Community_User_Role__c='Company Services'+';'+'IT Services',
                                 CompanyName = acc.Id,
                                 LanguageLocaleKey = 'en_US',IsActive=true); 
        insert ObjUser;
    
    }
     // 
     public static testmethod void testUnderFormation() {
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = 'dotnetcodex@gmail.com' + System.now().millisecond() ,
        Alias = 'sfdc',
        Email='dotnetcodex@gmail.com',
        EmailEncodingKey='UTF-8',
        Firstname='Dhanik',
        Lastname='Sahni',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago',
        IsActive = true
        );
        Database.insert(portalAccountOwner1);
        system.runas(portalAccountOwner1){
        prepareData();
        test.startTest();
            Service_Request__c SR = new Service_Request__c(Username__c=ObjUser.username,License_Number__c =ObjUser.Entity_License_No__c); 
            SR.Customer__c = acc.Id;
            SR.SR_Template__c = srTemp.Id;
            SR.Submitted_DateTime__c = DateTime.now();
            Date subDate = Date.today();
            SR.Submitted_Date__c = subDate;
            SR.Title__c = 'Mrs.';
            SR.First_Name__c = 'Finley';
            SR.Send_SMS_To_Mobile__c = '+971562345234';
            SR.Middle_Name__c = 'Ashton';
            SR.Last_Name__c = 'Kate';
            SR.Position__c = 'Engineer';
            SR.Place_of_Birth__c = 'USA';
            SR.Nationality_list__c  = nat.Name;
            SR.Date_of_Birth__c = subDate;
            SR.Email__c = 'test@test.com';
            SR.Roles__c = 'Company Services';
            SR.Nationality__c = nat.id;
            SR.Passport_Number__c = 'ASD00023';
            SR.License_Number__c = '002';
            insert SR;
            Step__c stp  = new Step__c();
            stp.SR__c = SR.id;
            insert stp;
            
            PageReference pageRef = Page.User_Access_Form;
            
            Test.setCurrentPageReference(pageRef);
            ApexPages.CurrentPage().getparameters().put('BPno', '000009999');
            Service_Request__c SR2 = new Service_Request__c();
            ApexPages.StandardController sc = new ApexPages.standardController(SR2);
            userFormCls userform = new userFormCls(sc);
            userFormCls userform2 = new userFormCls();
            
            
            userform.RePrepare();
            userform.getFlag();
            userform.getdisplayAcc();
            userform.getaccountName();
            userform.RePrepare();
            userform.disablename1();
            userform.enablename1();
            userform.enableopt();
            userform.getPassportCopy();
            userform.getAccount();
            userform.isConsent = true;
            
            Service_Request__c sUser=userform.sUser;
            sUser.User_Form_Action__c ='Create New User';
            
            sUser.Title__c ='Mr.';
            sUser.First_Name__c ='Create New User';
            sUser.Position__c ='Create New User';
            sUser.Nationality_list__c ='USA';
            sUser.Middle_Name__c ='Create New User';
            sUser.Passport_Number__c ='Create New User';
            sUser.Last_Name__c ='Create New User';
            sUser.Email__c ='CXXrrr@difc.ae';
            sUser.Place_of_Birth__c ='Create New User';
            sUser.Send_SMS_To_Mobile__c ='+971569803616';
            sUser.Date_of_Birth__c =Date.newinstance(1980,10,17);
            sUser.License_Number__c =lic123.Name;
            sUser.Username__c = 'test@test.com';
            userform.compServices=true;
            userform.empServices=true;
            userform.propServices=true;
            
            Attachment PassportCopy=new Attachment();    
            PassportCopy.Name='PassportCopy.png';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            PassportCopy.body=bodyBlob;
            //  PassportCopy.parentId=SR.Id;
            userform.PassportCopy=PassportCopy;
            
            userFormCls.remUser('test@test.com');
            
            sUser.Username__c='test@test.com';
            sUser.License_Number__c='1004';
            userform.saveInfo();
            //userform.saveRevokeSR();
            //userform.saveUpdateSR();
            
            userform.BPNo = Null;
            userform.compServices = false;
            userform.setdisplayAcc(false);
            userform.isConsent = true;
            
            String str1= userFormCls.createUser(stp);
            userFormCls.updateUser(stp.SR__c);
            userFormCls.revokeUser(stp.SR__c);
            userFormCls.remUser(stp.SR__c);
            
            userform.saveInfo();
            
            ApexPages.CurrentPage().getparameters().put('BPno', '000#$9999');
            userform.getAccount();
            
            RelUser.Object_Contact__c = null;
            update RelUser;
            
            userFormCls.updateUser(stp.SR__c);
            
        test.stopTest(); 
        }
     }
    public static testmethod void testUnderFormation2() {
    
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = 'dotnetcodex@gmail.com' + System.now().millisecond() ,
        Alias = 'sfdc',
        Email='dotnetcodex@gmail.com',
        EmailEncodingKey='UTF-8',
        Firstname='Dhanik',
        Lastname='Sahni',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago',
        IsActive = true
        );
        Database.insert(portalAccountOwner1);
        system.runas(portalAccountOwner1){
            prepareData();
            test.startTest();
                Service_Request__c SR = new Service_Request__c(Username__c=ObjUser.username,License_Number__c =ObjUser.Entity_License_No__c); 
                SR.Customer__c = acc.Id;
                SR.SR_Template__c = srTemp.Id;
                SR.Submitted_DateTime__c = DateTime.now();
                Date subDate = Date.today();
                SR.Submitted_Date__c = subDate;
                SR.Title__c = 'Mrs.';
                SR.First_Name__c = 'Finley';
                SR.Send_SMS_To_Mobile__c = '+971562345234';
                SR.Middle_Name__c = 'Ashton';
                SR.Last_Name__c = 'Kate';
                SR.Position__c = 'Engineer';
                SR.Place_of_Birth__c = 'USA';
                SR.Nationality_list__c  = nat.Name;
                SR.Date_of_Birth__c = subDate;
                SR.Email__c = 'test@test.com';
                SR.Roles__c = 'Company Services';
                SR.Nationality__c = nat.id;
                SR.Passport_Number__c = 'ASD00023';
                SR.License_Number__c = '002';
                insert SR;
                Step__c stp  = new Step__c();
                stp.SR__c = SR.id;
                insert stp;
                
                
                PageReference pageRef = Page.User_Access_Form;
                
                Test.setCurrentPageReference(pageRef);
                ApexPages.CurrentPage().getparameters().put('BPno', '000009999');
                Service_Request__c SR2 = new Service_Request__c();
                ApexPages.StandardController sc = new ApexPages.standardController(SR2);
                userFormCls userform = new userFormCls(sc);
                userFormCls userform2 = new userFormCls();
                
                
                userform.RePrepare();
                userform.getFlag();
                userform.getdisplayAcc();
                //userform.getaccountName();
                userform.RePrepare();
                userform.disablename1();
                userform.enablename1();
                userform.enableopt();
                userform.getPassportCopy();
                //userform.getAccount();
                userform.isConsent = true;
                
                Service_Request__c sUser=userform.sUser;
                sUser.User_Form_Action__c ='Create New User';
                
                sUser.Title__c ='Mr.';
                sUser.First_Name__c ='Create New User';
                sUser.Position__c ='Create New User';
                sUser.Nationality_list__c ='USA';
                sUser.Middle_Name__c ='Create New User';
                sUser.Passport_Number__c ='Create New User';
                sUser.Last_Name__c ='Create New User';
                sUser.Email__c ='CXXrrr@difc.ae';
                sUser.Place_of_Birth__c ='Create New User';
                sUser.Send_SMS_To_Mobile__c ='+971569803616';
                sUser.Date_of_Birth__c =Date.newinstance(1980,10,17);
                sUser.License_Number__c ='100';
                sUser.Username__c = 'test@test.com';
                userform.compServices=true;
                userform.empServices=true;
                userform.propServices=true;
                
                Attachment PassportCopy=new Attachment();    
                PassportCopy.Name='PassportCopy.png';
                Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
                PassportCopy.body=bodyBlob;
                //  PassportCopy.parentId=SR.Id;
                userform.PassportCopy=PassportCopy;
                
                userFormCls.remUser('test@test.com');//ObjUser.username
                userform.saveUpdateSR();
                sUser.Username__c='test@test.com';
                sUser.License_Number__c='1004';
                userform.saveInfo();
                userform.saveRevokeSR();
                
                RelUser.Object_Contact__c = null;
                update RelUser;
                
                userFormCls.updateUser(stp.SR__c);
                
                userform.BPNo = Null;
                userform.compServices = false;
                userform.setdisplayAcc(false);
                userform.isConsent = true;
                
                String str1= userFormCls.createUser(stp);
                userFormCls.updateUser(stp.SR__c);
                userFormCls.revokeUser(stp.SR__c);
                userFormCls.remUser(stp.SR__c);
                
                userform.saveUpdateSR();
                userform.ChangeOption();
                sUser.License_Number__c ='100';
                userform.saveRevokeSR();
                
                ApexPages.CurrentPage().getparameters().put('BPno', '000#$9999');
                //userform.getAccount();
                
            test.stopTest(); 
        }
     }
     public static testmethod void testUnderFormation3() {
        
        UserRole portalRole = [Select Id From UserRole Where PortalType = 'None' Limit 1];
        Profile profile1 = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner1 = new User(
        UserRoleId = portalRole.Id,
        ProfileId = profile1.Id,
        Username = 'dotnetcodex@gmail.com' + System.now().millisecond() ,
        Alias = 'sfdc',
        Email='dotnetcodex@gmail.com',
        EmailEncodingKey='UTF-8',
        Firstname='Dhanik',
        Lastname='Sahni',
        LanguageLocaleKey='en_US',
        LocaleSidKey='en_US',
        TimeZoneSidKey='America/Chicago',
        IsActive = true
        );
        Database.insert(portalAccountOwner1);
        system.runas(portalAccountOwner1){
        prepareData();
        test.startTest();
            
            PageReference pageRef = Page.User_Access_Form;
            
            Test.setCurrentPageReference(pageRef);
            ApexPages.CurrentPage().getparameters().put('BPno', '00009999');
            Service_Request__c SR2 = new Service_Request__c();
            ApexPages.StandardController sc = new ApexPages.standardController(SR2);
            userFormCls userform = new userFormCls(sc);
            userFormCls userform2 = new userFormCls();
            
            userform.RePrepare();
            userform.getFlag();
            userform.getdisplayAcc();
            userform.getaccountName();
            userform.RePrepare();
            userform.disablename1();
            userform.enablename1();
            userform.enableopt();
            userform.getPassportCopy();
            userform.getAccount();
            userform.isConsent = true;
            
            
            
            Service_Request__c sUser=userform.sUser;
            sUser.User_Form_Action__c ='Create New User';
            
            sUser.Title__c ='Mr.';
            sUser.First_Name__c ='Create New User';
            sUser.Position__c ='Create New User';
            sUser.Nationality_list__c ='USA';
            sUser.Middle_Name__c ='Create New User';
            sUser.Passport_Number__c ='Create New User';
            sUser.Last_Name__c ='Create New User';
            sUser.Email__c ='CXXrrr@difc.ae';
            sUser.Place_of_Birth__c ='Create New User';
            sUser.Send_SMS_To_Mobile__c ='00#569803616';
            sUser.Date_of_Birth__c =Date.newinstance(1980,10,17);
            sUser.License_Number__c =lic123.Name;
            userform.compServices=true;
            userform.empServices=true;
            userform.propServices=true;
            sUser.Username__c = 'test@test.com';
            Attachment PassportCopy=new Attachment();    
            PassportCopy.Name='PassportCopy.png';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            PassportCopy.body=bodyBlob;
            //  PassportCopy.parentId=SR.Id;
            userform.PassportCopy=PassportCopy;
            
            userFormCls.remUser('test@test.com');
            
            sUser.Username__c='test@test.com';//ObjUser.username
            sUser.License_Number__c='1004';
            userform.saveInfo();
            
        test.stopTest(); 
        }
     }
    
}