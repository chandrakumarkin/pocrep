/********************************************************************************************************************************************************
 *  Author      : Mudasir Wani
 *  Date        : 31-March-2020
 *  Description : This class will have the common functioalities used for the GS related operations across the DIFC organization    
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date                Updated By          Description
---------------------------------------------------------------------------------------------------------------------  
V1.0    31-March-2020       Mudasir             Created this class 
*********************************************************************************************************************************************************/
public without sharing class DIFCApexCodeUtility {
    
    /********************************************
    *   Parameter       :   AccountId 
    *   Data Type       :   String
    *   Return          :   Boolean  
    *   Created By      :   Mudasir Wani
    *   Created Date    :   31-March-2020
    *********************************************/
    public static Boolean hasExpiredEmployee(String accountId){
        Boolean result = false;
        if(accountId != NULL){
            Map<id , boolean> accountWithViolationMap = getCompanyEmployeeViolationDetails(new Set<id>{accountId});
            result = accountWithViolationMap.get(accountId);
        }
        return result;
    } 
    
    /***********************************************************************************************************
    *   Method          :   getCompanyEmployeeViolationDetails
    *   Input Parameter :   Accepts the set of account ids Set<id>
    *                   :   
    *   Return          :   Map<id , boolean> , Map of Account id and respective indicator of violation
                            If value associated with account Id is true that means the account has a violation
                              
    *   Created By      :   Mudasir Wani
    *   Created Date    :   19-April-2020
    ************************************************************************************************************/
    public static Map<id , boolean> getCompanyEmployeeViolationDetails(Set<id> accountIds){
        //Construct the Map intially all accounts with no violation
        Map<id , boolean> accountWithViolationMap = new Map<id , boolean>();
        for(Id idValue : accountIds){
            accountWithViolationMap.put(idValue , false);
        }
        
        Boolean result = false;
        
        List<Relationship__c> ListOfRelationships;
        Map<id , Set<id>> mapOfAccountIdAndSetOfContactIds;
        Map<id , Set<id>> accountContactIdsOfInProgressRequests;
        
        if(accountIds != NULL && accountIds.size() >0){
            //First get the expired relationships Map
            ListOfRelationships = DIFCBusinessLogicUtils.getListOfRelationships(accountIds);  
            //Get the contacts associated with each account, this will be later user to identify the violation against each account
            if(ListOfRelationships!= NULL && ListOfRelationships.size() > 0){
                mapOfAccountIdAndSetOfContactIds = DIFCBusinessLogicUtils.getMapOfAccountIdAndSetOfContactIds(ListOfRelationships);
            }
            //Find the service requests for the associated contacts and get the set of contactids where service request is in progress
            if(mapOfAccountIdAndSetOfContactIds != NULL && mapOfAccountIdAndSetOfContactIds.size() >0){
                Set<id> contactIdSet = new Set<id>();
                for(String accountId : mapOfAccountIdAndSetOfContactIds.keyset()){
                    contactIdSet.addAll(mapOfAccountIdAndSetOfContactIds.get(accountId));
                }
                accountContactIdsOfInProgressRequests = DIFCBusinessLogicUtils.getContactIdsOfInProgressRequests(contactIdSet);  
            }
            //Now we have two Maps, mapOfAccountIdAndSetOfContactIds which contains all the accounts with contacts 
            //and accountContactIdsOfInProgressRequests which contains account contact ids of in progress requests
            if(accountContactIdsOfInProgressRequests != NULL && accountContactIdsOfInProgressRequests.size() >0){
                if(mapOfAccountIdAndSetOfContactIds.size() > 0){
                    for(String accountId : mapOfAccountIdAndSetOfContactIds.keyset()){
                        //Now check the size of the contacts and in progress sr request contacts
                        if(mapOfAccountIdAndSetOfContactIds.get(accountId) != NULL && accountContactIdsOfInProgressRequests.get(accountId)!= NULL &&  mapOfAccountIdAndSetOfContactIds.get(accountId).size() > accountContactIdsOfInProgressRequests.get(accountId).size()){
                            accountWithViolationMap.put(accountId,true);
                        }else{
                            for(String contactId : mapOfAccountIdAndSetOfContactIds.get(accountId)){
                                //Check if the current contact has any renewal or cancellation raised 
                                if(accountContactIdsOfInProgressRequests.get(accountId) != NULL && accountContactIdsOfInProgressRequests.get(accountId).contains(contactId)){
                                    continue;
                                }else{
                                    //If any violation is found against any account exit the loop and mark the account as violater
                                    system.debug('Violation is found agains the contact with id ---'+contactId);
                                    accountWithViolationMap.put(accountId,true);
                                    break;
                                }
                            }
                        }
                    }
                }
            }else{
                //If no request is found then mark all the accounts with expired visa as violater
                if(mapOfAccountIdAndSetOfContactIds != Null && mapOfAccountIdAndSetOfContactIds.size() > 0){
                    for(String accountId : mapOfAccountIdAndSetOfContactIds.keyset()){
                        accountWithViolationMap.put(accountId,true);
                    }
                }
            }
        }
        return accountWithViolationMap;
    } 
     
    /********************************************
    *   Parameter       :   AccountId 
    *   Data Type       :   String
    *   Return          :   Boolean   
    *   Created By      :   Mudasir Wani
    *   Created Date    :   31-March-2020
    *********************************************/
    public static List<User> getActivePortalUsers(String AccountId, String userFieldsToQuery, string objSOQLFilters){
        Set<id> contactIdSet = new Set<id>();
        String contactIdsStr ='';
        for(AccountContactRelation  relshipRecord : [Select ContactId from AccountContactRelation where AccountId =:AccountId]){
            contactIdSet.add(relshipRecord.ContactId);
            contactIdsStr = contactIdsStr != '' ? contactIdsStr +','+ '\''+relshipRecord.ContactId+'\'' : '\''+relshipRecord.ContactId+'\'';
        }
        if(contactIdSet.size() >0){
            String userSOQL = 'Select '+ userFieldsToQuery+' from User where isActive = true AND ContactId in ('+contactIdsStr+')';
            return Database.Query(userSOQL);
        }
        return Null;
    }
    
    /********************************************
    *   Parameter       :   AccountId 
    *   Data Type       :   String
    *   Return          :   Boolean   
    *   Created By      :   Mudasir Wani
    *   Created Date    :   31-March-2020
    *********************************************/
    
    public static AccountContactRelation prepareAccountContactRelation(string contactIdVal , string accountIdVal,String Access_Level ,boolean isActive , String rolesToBeAssigned){
        //Check if the relation already exist activate it
        Boolean statusOfContactRelation = false;
        List<AccountContactRelation> accConRelList = new List<AccountContactRelation>();
        //system.assertEquals(contactIdVal,accountIdVal);
        accConRelList = [select id,AccountId,ContactId,Access_Level__c,Roles,IsActive from AccountContactRelation where ContactId =:contactIdVal and AccountId =:accountIdVal];
        //system.assertEquals(null,accConRelList.size());
        AccountContactRelation accConRel= new AccountContactRelation();
        if(accConRelList.size() > 0){
            accConRel.IsActive = true;
            accConRel.Access_Level__c =Access_Level;
            accConRel.Roles = rolesToBeAssigned;//Assuming the we pass all the new roles
        }else{accConRel.accountId =accountIdVal;accConRel.contactId = contactIdVal;accConRel.IsActive = isActive;accConRel.Access_Level__c =Access_Level;accConRel.Roles = rolesToBeAssigned;}
        return accConRel;
    }
    
    /********************************************
    *   Parameter       :   List of Ids 
    *   Data Type       :   List <id>
    *   Created By      :   Mudasir Wani
    *   Created Date    :   27-April-2020
    *********************************************/
    public static void logWebServiceLoad(String className , String recordId , 
                                                String departmentName , String webServiceRequest,
                                                String webServiceResponse, String serviceRequestNumber){
        system.debug('className---'+className+'---recordId---'+recordId+'--departmentName--'+departmentName+'---webServiceRequest---'+webServiceRequest+'---webServiceResponse---'+webServiceResponse+'---serviceRequestNumber---'+serviceRequestNumber);                                                   
        Web_Service_Load__c webServceLoadRec = DIFCBusinessLogicUtils.prepareWebServiceLoad(className , recordId ,departmentName , webServiceRequest,webServiceResponse, serviceRequestNumber);
        DIFCDMLUtils.insertWebServiceLoad(webServceLoadRec);
    }
    /********************************************
    *   Parameter       :   List of Ids 
    *   Data Type       :   List <id>
    *   Return          :   Boolean   
    *   Created By      :   Mudasir Wani
    *   Created Date    :   27-May-2020
    *********************************************/
    public static boolean checkIfSRStatusToUpdate(List <id> stepIds){
        //Fetch the step name details 
        Set<id> servReqIds = new Set<id>();
        Set<String> stepNames = new Set<String>();
        for(Step__c stepRec : [Select id,step_Name__c,SR__c from Step__c where id in : stepIds]){
            servReqIds.add(stepRec.SR__c);
            stepNames.add(stepRec.step_Name__c);
        }
        /*List<Step__c> stepList = [Select id,step_Name__c,SR__c from Step__c where id in : stepIds];
        List<SR_Price_Item__c> pricingLines = new List<SR_Price_Item__c>();
        pricingLines = [Select id,name from SR_Price_Item__c where ServiceRequest__c =:stepList[0].SR__c 
                                    and Pricing_Line__r.Update_SR_Status__c =:stepList[0].Step_Name__c];
        if(pricingLines.size() > 0){
            return true;
        }*/
        //We don't have any such scanerio where we will pass multiple steps but we need to enhance the functionality for the good
        if([Select count() from SR_Price_Item__c where ServiceRequest__c IN : servReqIds and Pricing_Line__r.Update_SR_Status__c in : stepNames] > 0){
            return true;
        }
        return false;
    }
   
    /********************************************
    *   Parameter       :   Service Request 
    *   Data Type       :   Service_Request__c
    *   Return          :   Boolean   
    *   Created By      :   Mudasir Wani
    *   Created Date    :   02-June-2020
    *********************************************/
   public static boolean CheckIfMasterDataUpdateIsApplicable(Service_Request__c serviceRequestRec){
        if([Select count() from SR_Price_Item__c where ServiceRequest__c =: serviceRequestRec.id and Pricing_Line__r.Update_Master_Data__c=true] > 0){
            return true;
        }
        return false;
    }
    
    /********************************************
    *   Parameter       :   Step ids
    *   Data Type       :   List<Id>
    *   Return          :   Service_Request__c   
    *   Created By      :   Mudasir Wani
    *   Created Date    :   25-June-2020
    *********************************************/
   public static Service_request__c serviceRequestStatusUpdateOnStep(Step__c step){
        //Get the step and status details from the 
        system.debug('step------'+step);
        //Step__c step = [Select id,sr__c,Step_Template__r.Internal_SR_Status__c,Step_Template__r.External_SR_Status__c,SR__r.Internal_SR_Status__c,SR__r.External_SR_Status__r.Do_Not_Change_SR_Status__c,SR__r.External_SR_Status__c from step__c where id in : stepIds];
        if(step.SR__r.External_SR_Status__c != NULL && step.SR__r.External_SR_Status__r.Do_Not_Change_SR_Status__c ==false){
            Service_request__c serviceRequestRec = new Service_request__c();
            serviceRequestRec.id = step.sr__c;
            if(step.Step_Template__r.External_SR_Status__c != NULL) serviceRequestRec.External_SR_Status__c = step.Step_Template__r.External_SR_Status__c;
            if(step.Step_Template__r.Internal_SR_Status__c != NULL) serviceRequestRec.Internal_SR_Status__c = step.Step_Template__r.Internal_SR_Status__c;
            if(step.Step_Template__r.External_SR_Status__c != NULL || step.Step_Template__r.Internal_SR_Status__c != NULL){
                return serviceRequestRec;
            }
        }
        return null;
    } 
    /********************************************
    *   Parameter       :   Step ids
    *   Data Type       :   List<Id>
    *   Return          :   Service_Request__c   
    *   Created By      :   Mudasir Wani
    *   Created Date    :   25-June-2020
    *********************************************/
   public static Step__c prepareStep(boolean isInsertOperation, Step__c stepRec,string stepStatus,string stepTemplateName,String serviceRequestId){
        //Create new Step
        if(isInsertOperation){
            SR_Steps__c srStep =[SELECT Id,OwnerId,Start_Status__c,Step_Template__c,Summary__c FROM SR_Steps__c where Step_Template__r.Name=: stepTemplateName];
            Step__c step            =   new Step__c();
            step.step_template__c   =   srStep.Step_Template__c;
            step.status__c          =   srStep.Start_Status__c;
            step.SR_Step__c         =   srStep.id;
            step.ownerId            =   srStep.OwnerId;
            step.SR__c              =   serviceRequestId;
            step.Summary__c         =   srStep.Summary__c;
            
            return step;
        }
        else{
            stepRec.status__c = [select id from Status__c where name=:stepStatus limit 1].id;
            return stepRec;
        }
    } 
    
}