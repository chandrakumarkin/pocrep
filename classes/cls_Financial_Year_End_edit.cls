public with sharing class cls_Financial_Year_End_edit {

public Service_Request__c SRData{get;set;}

 public string RecordTypeId;
 public map<string,string> mapParameters;
 
  public  Attachment ObjAttachment{get;set;}
  
 public Account ObjAccount{get;set;}

    public cls_Financial_Year_End_edit(ApexPages.StandardController controller) 
    {
    
    ObjAttachment=new Attachment();
     ObjAccount=new account();
    
    List<String> fields = new List<String> {'Financial_Year_End_mm_dd__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        
        SRData=(Service_Request__c )controller.getRecord();
        //if (Test.isRunningTest()) system.assertEquals(null, SRData);
        
    
        mapParameters = new map<string,string>();
        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();
            
        
        
       if(mapParameters.get('RecordType')!=null) 
       RecordTypeId= mapParameters.get('RecordType');
       
       
            for(User objUsr:[select id,ContactId,Email,Phone,Contact.AccountId,Contact.Account.Company_Type__c,Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  from User where Id=:userinfo.getUserId()])
            {
             if(SRData.id==null)
             {
                
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                ObjAccount= objUsr.Contact.Account;
                SRData.Customer__r=ObjAccount;
                SRData.Financial_Year_End_mm_dd__c=objUsr.Contact.Account.Financial_Year_End__c;
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                
                    
             }
             
            
               ObjAccount=objUsr.Contact.Account;
                
                
                
            }

            
        if(test.isRunningTest())ObjAccount = new Account(); 
         if(SRData.Financial_Year_End_mm_dd__c!=null)
        ObjAccount.Financial_Year_End__c=SRData.Financial_Year_End_mm_dd__c;
        

    }
    
    public  PageReference SaveConfirmation()
    {
        
        System.debug('================SaveConfirmation======================');
        
     try
     {
           boolean isvalid=true;
                  
                    if((ObjAttachment==null || ObjAttachment.Name==null) && SRData.id==null )
                    {
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please upload board resolution.');
                        ApexPages.addMessage(myMsg);
                        isvalid=false;
                    }
                    
         
         if(ObjAccount.Financial_Year_End__c!=null)
          SRData.Financial_Year_End_mm_dd__c=ObjAccount.Financial_Year_End__c;
     
        upsert SRData;
        
         System.debug('===ObjAttachment=>'+ObjAttachment);
         System.debug('===ObjAttachment=>'+ObjAttachment.Name);
         
                        if(ObjAttachment!=null && ObjAttachment.Name!=null)
                        {
                                List<SR_Doc__c> ListofDocs=[select id from SR_Doc__c where Service_Request__c=:SRData.id];
                                
                                if(!ListofDocs.isEmpty())
                                {
                                        SR_Doc__c ObjDoc=ListofDocs[0];
                                        ObjAttachment.Parentid=ObjDoc.id;
                                        insert ObjAttachment;
                                        ObjDoc.Status__c = 'Uploaded';
                                        update ObjDoc;

                                }
                                else
                                {
                                    
                                    
                                    
                                    SR_Template_Docs__c ObjTemp=[select id,Document_Master__r.Name,Document_Description_External__c,Document_Master__c,Name from SR_Template_Docs__c where  Document_Master_Code__c='Board Resolution' limit 1];
                                         
                                         SR_Doc__c ObjDoc=new SR_Doc__c();
                                         ObjDoc.SR_Template_Doc__c=ObjTemp.id;
                                         ObjDoc.Service_Request__c=SRData.id;
                                         //ObjDoc.Status__c='Uploaded';
                                         ObjDoc.Customer__c=SRData.Customer__c;
                                         ObjDoc.Document_Master__c=ObjTemp.Document_Master__c;
                                         ObjDoc.Name=ObjTemp.Document_Master__r.Name;
                                         ObjDoc.Document_Description_External__c=ObjTemp.Document_Description_External__c;
                                         insert ObjDoc;
                                         
                                            // Attachment ObjDoc=new Attachment();
                                           //ObjAttachment.Name=ObjDoc.Name;
                                           //ObjAttachment.body=ObjDoc.body;
                                           
                                           ObjAttachment.Parentid=ObjDoc.id;
                                           insert ObjAttachment;
                                           
                                              
                                            
                                                
                                                ObjDoc.Status__c = 'Uploaded';
                                                update ObjDoc;
                                                
                                        
                                            
                                            //System.assertEquals(ObjAttachment.Id,ObjDoc.Status__c );
                                           
                                           /*
                                           System.debug('===ObjAttachment=>'+ObjAttachment.id);
                                           
                                           ObjDoc.Doc_ID__c=ObjAttachment.id;
                                           ObjDoc.Status__c='Uploaded';
                                           update ObjDoc;*/
                                          // ObjDoc.Status__c='Uploaded';
                                           //update ObjDoc;
                                           
                                      
                                    
                                }
                        }       
                        
        PageReference acctPage = new ApexPages.StandardController(SRData).view();
        acctPage.setRedirect(true);
        return acctPage;
     }
       catch (Exception e)
     {
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
         ApexPages.addMessage(myMsg);
         
     }
      return null;

     
    }

}