/*
* Used on Define Share Class screen
* Can add new share class for an account in Draft status
* All share class be of one currency only. Cann't be of different currency
*/
public without sharing class OB_AddShareClassController {
    //reqest wrpper
    public static OB_AddShareClassController.RequestWrapper reqWrap;
    //response wrpper
    public static OB_AddShareClassController.ResponseWrapper respWrap;
    
    /*
    * Lightning component: OB_ADDShareClassContainer 
    * 
    */
    @AuraEnabled   
    public static ResponseWrapper getShareClassList(String reqWrapPram)
    {
    	system.debug('##########getShareClassList##########'+reqWrapPram);
    	//reqest wrpper
		reqWrap = (OB_AddShareClassController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_AddShareClassController.RequestWrapper.class);
     	//response wrpper
        respWrap = new OB_AddShareClassController.ResponseWrapper();
        String accountID = reqWrap.accountID;
        String srId = reqWrap.srId;
        
        if(String.IsNotBlank(srId) ){
        	
        	respWrap = getShareClassList( srId, accountID, respWrap);
            for(HexaBPM__Section_Detail__c sectionObj: [SELECT id,HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection' 
														AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
			 respWrap.ButtonSection =  sectionObj;                                            
														}
        }else{
       		respWrap.errorMessage = 'Account/Service Request cannot be null';
        }
        
        return respWrap;
    	
    }
    
    public static ResponseWrapper getShareClassList(String srId,String accountID,ResponseWrapper respWrap){
        system.debug('-------------------->'+accountID);
        system.debug(String.IsBlank(accountID));
        OB_AddShareClassController.SRWrapper srWrap = new OB_AddShareClassController.SRWrapper();
        String currencyName = '';
        String currencyId = '';
        String flowId = reqWrap.flowId;
        HexaBPM__Service_Request__c serObj = new HexaBPM__Service_Request__c(Id = srId);
        
        	if(String.IsBlank(accountID)){
	            for(HexaBPM__Service_Request__c sr: [Select Id,HexaBPM__Customer__c,
	            									HexaBPM__External_SR_Status__c,HexaBPM__External_SR_Status__r.Name,
    												HexaBPM__Internal_SR_Status__c,HexaBPM__Internal_Status_Name__c 
	            									From HexaBPM__Service_Request__c 
	            										Where Id =: srId]){
	            	accountID = sr.HexaBPM__Customer__c;// get account id to get the share class list
	            	serObj = sr;// assign service request
	            }  
        	}
        	
        	srWrap.srObj = serObj;
	    	srWrap.shareClassWrapLst = new List<OB_AddShareClassController.shareClassWrapper>();
			// set service request status
	    	/*if(srWrap.srObj != null && srWrap.srObj.Id != null){
		    		srWrap.isDraft = OB_QueryUtilityClass.checkSRStatusDraft(srWrap.srObj);
		    		// if SR status is not draft then redirect to ob-reviewandconfirm lightning page
			    	if(!srWrap.isDraft){
			    		system.debug('=====flowId===='+flowId);
			    		srWrap.viewSRURL = PageFlowControllerHelper.communityURLFormation(srWrap.srObj,flowId,'ob-reviewandconfirm');
			    		system.debug('=====viewSRURL===='+srWrap.viewSRURL);
			    	}
	    	}else{
	    		srWrap.isDraft = false;
	    	} */     	
        	if(String.IsNotBlank(accountID) ){                                  
				for(Account_Share_Detail__c shrcls : [Select Id,Name,CurrencyIsoCode,No_of_shares_per_class__c,
														Nominal_Value__c,Account__c,Currency__c,Currency__r.Name,
														Status__c 
													From Account_Share_Detail__c
													Where Account__c =: accountID
													AND (Status__c = 'Draft' OR Status__c = 'Active')
													Limit : (Limits.getLimitQueryRows()-Limits.getQueryRows())
													]){
					OB_AddShareClassController.shareClassWrapper shrclsWrap = new OB_AddShareClassController.shareClassWrapper();
                	shrclsWrap.shareClassObj = shrcls;
                	shrclsWrap.lookupLabel = shrcls.Currency__r.Name;
                	
                	if(String.IsBlank(currencyName)){
                		currencyId= shrcls.Currency__c;
                		currencyName= shrcls.Currency__r.Name;
                	}
                	
                	srWrap.shareClassWrapLst.add(shrclsWrap);
				}
        	}else{
       			respWrap.errorMessage = 'Account/Service Request cannot be null';
        	}
        	//Always one SR so can be in loop.
        	srWrap.currencyName = currencyName; 
        	srWrap.currencyId = currencyId; 
            respWrap.srWrap = srWrap;
            respWrap.accountID = accountID;// assign account ID
        	return respWrap;
    }
    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID,string ButtonId, string pageId){
        
        
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
        PageFlowControllerHelper.objSR = objRequest;
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();

        objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
        upsert objRequest;
        OB_RiskMatrixHelper.CalculateRisk(SRID);
        
        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage '+responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;
        
        system.debug('@@@@@@@@2 respWrap.pageActionName '+respWrap.pageActionName);
        return respWrap;
    }
    
    /*
    * Lightning component: OB_ShareClassNew- to be remove
    * on click of "Add Share Class" 
    */
    @AuraEnabled   
    public static ResponseWrapper initAmendmentDB(String reqWrapPram)
    {
        //reqest wrpper
        reqWrap = (OB_AddShareClassController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_AddShareClassController.RequestWrapper.class);
        
        //response wrpper
        respWrap = new OB_AddShareClassController.ResponseWrapper();
        String srId = reqWrap.srId; 
        String accountID = reqWrap.accountID; 
        system.debug('===accountID====='+accountID);
        
        OB_AddShareClassController.shareClassWrapper shrclsWrap = new OB_AddShareClassController.shareClassWrapper();
        Account_Share_Detail__c shareObj = new Account_Share_Detail__c();
        shareObj.Account__c = accountID;
        shareObj.Status__c = 'Draft';
        shareObj.Start_Date__c = system.today();
        shrclsWrap.shareClassObj = shareObj;
        
        respWrap.shareClassWrap = shrclsWrap;
        system.debug('===shrclsWrap====='+shrclsWrap);
        return respWrap;
    }
    
    
    /*
    * Lightning component: OB_ShareClassDetail
    * on click of "Confirm" button will call upsert the amendment record in database 
    */
    @AuraEnabled   
    public static ResponseWrapper onSaveDB(String reqWrapPram)
    {
        //reqest wrpper
        reqWrap = (OB_AddShareClassController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_AddShareClassController.RequestWrapper.class);
        
        //response wrpper
        respWrap = new OB_AddShareClassController.ResponseWrapper();
        //system.debug('@@@@@@@ reqWrap.amedWrap '+reqWrap.amedWrap);
        
        //service request ID
        String srId = reqWrap.srId;
        String accountID = reqWrap.accountID;
        try{
        	if(String.IsnotBlank(srId)){
        		
	        	if(reqWrap.shareClassWrap.shareClassObj != null){
	        		
        			upsert reqWrap.shareClassWrap.shareClassObj;
        			//calculate risk
        			OB_RiskMatrixHelper.CalculateRisk(srId);
        			// re-intialize the wrapper to display correct data on form
        			// initialize the list again
    				respWrap = getShareClassList( srId, accountID, respWrap);
	        	}
	        	system.debug('=====respWrap===='+respWrap.srWrap);
	        	respWrap.shareClassWrap = reqWrap.shareClassWrap;
        	}else{
        		respWrap.errorMessage = 'Service Request is null';
        	}
        
        }catch(dmlException e){
            //string DMLError = e.getMessage();
          	//respWrap.errorMessage = DMLError;
          	string DMLError = e.getdmlMessage(0)+'';
            if(DMLError==null){
                DMLError = e.getMessage()+'';
            }
            respWrap.errorMessage = DMLError;
          	system.debug('##########DMLError '+DMLError);
          	//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
          	return respWrap;
        }
        return respWrap;
    }
    
    
     /*
    * Lightning component: OB_ShareClassExisting
    * Delete 
    */
    @AuraEnabled   
    public static ResponseWrapper  removeShareClass(String reqWrapPram)
    {
    	system.debug('===removeShareClass=======');
    	//reqest wrpper
		reqWrap = (OB_AddShareClassController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_AddShareClassController.RequestWrapper.class);
     	//response wrpper
        respWrap = new OB_AddShareClassController.ResponseWrapper();
        //service request ID
        String srId = reqWrap.srId;
        String accountID = reqWrap.accountID;
        try{
	     	String delShareClassID = reqWrap.delShareClassID;
	     	
	     	system.debug('===delShareClassID======='+delShareClassID);
	     	
	     	if(String.IsNotBlank(delShareClassID)){
	     		system.debug('======delShareClassID inside=====');
	     		Account_Share_Detail__c shareObj = new Account_Share_Detail__c(Id=delShareClassID);
     			delete shareObj;
     			//calculate risk
        		OB_RiskMatrixHelper.CalculateRisk(srId);
		     	// re-intialize the wrapper to display correct data on form
    			// initialize the list again
				respWrap = getShareClassList( srId, accountID, respWrap);
	     		
	     	}else{
	     		respWrap.errorMessage = 'Share Class record is not present';
	     	}
        }catch(dmlException e){
            //string DMLError = e.getMessage();
          	//respWrap.errorMessage = DMLError;
          	string DMLError = e.getdmlMessage(0)+'';
            if(DMLError==null){
                DMLError = e.getMessage()+'';
            }
            respWrap.errorMessage = DMLError;
          	system.debug('##########DMLError '+DMLError);
          	//ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
          	return respWrap;
        }
        return respWrap;
    }
    
    /////////////////////////////////Wrapper  classes//////////////////////////////////////
    
    /*
    * data will come from client side 
    */
    public class RequestWrapper{
    	
        @AuraEnabled public Id accountID {get;set;}
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public string pageID { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string delShareClassID { get; set; }
        @AuraEnabled public OB_AddShareClassController.shareClassWrapper shareClassWrap { get; set; }
        public RequestWrapper(){}
    }
    
    /*
    * store detail of service request with list of amendment
    */
    public class SRWrapper 
    {
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public String currencyName { get; set; }
        @AuraEnabled public String currencyId { get; set; }
        @AuraEnabled public List<OB_AddShareClassController.shareClassWrapper> shareClassWrapLst { get; set; }// UBO list and individual as UBO
        @AuraEnabled public Boolean isDraft { get; set; }
        @AuraEnabled public string viewSRURL { get; set; }
        public SRWrapper(){}
    }
    
    /*
    * store detail of share class
    */
    public class shareClassWrapper 
    {
        @AuraEnabled public Account_Share_Detail__c shareClassObj { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        public shareClassWrapper(){}
    }
    
    /*
    * Result will send to client side 
    */
    public class ResponseWrapper 
    {
        @AuraEnabled public SRWrapper srWrap{get;set;}
        @AuraEnabled public Id accountID {get;set;}
        @AuraEnabled public OB_AddShareClassController.shareClassWrapper shareClassWrap {get;set;}
        @AuraEnabled public String errorMessage{get;set;}
        @AuraEnabled public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        public ResponseWrapper()
        {}
    }
    
    /*
    *
    */
    public class ButtonResponseWrapper{
        @AuraEnabled public String pageActionName{get;set;}
        @AuraEnabled public string communityName{get;set;}
        @AuraEnabled public String errorMessage{get;set;}
        @AuraEnabled public string CommunityPageName{get;set;}
        @AuraEnabled public string sitePageName{get;set;}
        @AuraEnabled public string strBaseUrl{get;set;}
        @AuraEnabled public string srId{get;set;}
        @AuraEnabled public string flowId{get;set;}
        @AuraEnabled public string  pageId{get;set;}
        @AuraEnabled public boolean isPublicSite{get;set;}
        
    }
}