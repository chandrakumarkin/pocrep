@isTest
public without sharing class OB_AgentEntityUtilsTest {
    
    public static testmethod void testMethod1(){
            
            /* create test data here */ 
    
            // create account
            List<Account> insertNewAccounts = new List<Account>();
            insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
            insert insertNewAccounts;
        
        Compliance__c  be = new Compliance__c ();
        be.Status__c = 'Open';
        be.Name = 'Commercial Permission Renewal';
        be.account__c = insertNewAccounts[0].Id;
        insert be;
    
            //create contact
            Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
            upsert con; 
        
        AccountContactRelation relObj = new AccountContactRelation();
        for(AccountContactRelation relationObj : [SELECT id , Roles FROM AccountContactRelation WHERE AccountId =:insertNewAccounts[0].Id AND ContactId =:con.Id  ]){
            relObj = relationObj;
        }
 
        relObj.Roles = 'Super User';
        update relObj;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('Commercial_Permission_Renewal', 1);
        insert listPageFlow;
        
        list<HexaBPM__Page_Flow__c> listPageFlow2 = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow2 = OB_TestDataFactory.createPageFlow('Superuser_Authorization', 1);
        insert listPageFlow2;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Identify ultimate beneficial owners';
        listPage[0].HexaBPM__Render_by_Default__c = false;
        listPage[0].Community_Page__c = 'test';
        listPage[0].HexaBPM__Page_Order__c = 1;
        insert listPage;
        
        list<HexaBPM__Page__c> listPage2 = new list<HexaBPM__Page__c>() ;
        listPage2 = OB_TestDataFactory.createPageRecords(listPageFlow2);
        listPage2[0].HexaBPM__VF_Page_API_Name__c = 'Upgrade_to_Super_User';
        listPage2[0].HexaBPM__Render_by_Default__c = false;
        listPage2[0].Community_Page__c = 'test';
        listPage2[0].HexaBPM__Page_Order__c = 1;
        insert listPage2;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        sec1.Submit_Request__c = true;
        insert sec1;
        
        
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Rejected','Draft','Submitted'}, new List<string> {'REJECTED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        
        
        
         // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'Passport Copy'});
        insert listDocMaster;


//create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(3, new List<string> {'In_Principle','AOR_Financial','Commercial_Permission_Renewal'});
        createSRTemplateList[2].name = 'Commercial Permission Renewal';
        insert createSRTemplateList;
        
        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        insert listSRTemplateDoc;        
        
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'Converted_User_Registration', 'Converted_User_Registration'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].HexaBPM__customer__c=insertNewAccounts[0].Id;
        insertNewSRs[0].HexaBPM__customer__c=insertNewAccounts[0].Id;
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        insert insertNewSRs;
        insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs[0].id;
        //insertNewSRs[0].HexaBPM__Customer__c  = insertNewAccounts[0].Id;
        update insertNewSRs;
        
        
        // create SR Doc
        List<HexaBPM__SR_Doc__c> listSRDoc = new List<HexaBPM__SR_Doc__c>();
        listSRDoc = OB_TestDataFactory.createSRDoc(1, insertNewSRs, listDocMaster, listSRTemplateDoc);
        listSRDoc[0].Name = 'Passport Copy';
        insert listSRDoc;
        
        
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        listLicenseActivityMaster[0].Name = 'tester';
        listLicenseActivityMaster[0].Enabled__c = true;
        insert listLicenseActivityMaster;
        
        //create license
        License__c licenseObj = new License__c();
        licenseObj.Account__c = insertNewAccounts[0].Id;
        insert licenseObj;
        
         License_Activity__c actObj = new License_Activity__c();
            actObj.Activity__c = listLicenseActivityMaster[0].Id;
            actObj.Account__c  = insertNewAccounts[0].Id;
          actObj.License__c = licenseObj.Id;
            insert actObj;
        
        ContentVersion contentVersionInsert = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true
            );
            insert contentVersionInsert;
            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
            
            //create ContentDocumentLink  record 
            ContentDocumentLink cdl = New ContentDocumentLink();
            cdl.LinkedEntityId = listSRDoc[0].id;
            cdl.ContentDocumentId = documents[0].Id;
            cdl.shareType = 'V';
            insert cdl;
        
        ContentVersion contentVersionInsert2 = new ContentVersion(
                Title = 'Test',
                PathOnClient = 'Test.jpg',
                VersionData = Blob.valueOf('Test Content Data'),
                IsMajorVersion = true
                );
                insert contentVersionInsert2;
                List<ContentDocument> documentsw = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
                
                //create ContentDocumentLink  record 
                ContentDocumentLink cdl1 = New ContentDocumentLink();
                cdl1.LinkedEntityId = con.id;
                cdl1.ContentDocumentId = documentsw[0].Id;
                cdl1.shareType = 'V';
                insert cdl1;
        
        
    
            test.startTest();
            
            
            Id profileToTestWith = [select id from profile where name='DIFC Customer Community User Custom'].id;      
            
            
            User user = new User(alias = 'test123', email='test123@noemail.com',
                                 emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                                 localesidkey='en_US', profileid = profileToTestWith, country='United States',IsActive =true,
                                 ContactId = con.Id,
                                 timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
            
            insert user;
            system.runAs(user)
            {
                
                OB_AgentEntityUtils.RequestWrap reqWrapper = new OB_AgentEntityUtils.RequestWrap();
                OB_AgentEntityUtils.RespondWrap repWrapper = new OB_AgentEntityUtils.RespondWrap();
                repWrapper.relatedAccounts = new list<Account> ();
                repWrapper.errorMessage = 'error';
                
                /*String requestString = JSON.serialize(reqWrapper);
                
                repWrapper = nameOfClassToTest.nameOfMethodToTest(requestString); */
                
                OB_AgentEntityUtils.getRelatedAccounts(con.id);
                OB_AgentEntityUtils.compAndPropRoleCheck('Company Services');
                OB_AgentEntityUtils.compAndPropRoleCheck(null);
                OB_AgentEntityUtils.hasSpecifiedRole(con.id , insertNewAccounts[0].Id , 'Super User');
                OB_AgentEntityUtils.getUserById(user.Id);
                
                
        
                
                OB_AgentEntityUtils.setPassportFileUnderContact(con,insertNewSRs[0].id);
                OB_AgentEntityUtils.getHEXASrLink();
                OB_AgentEntityUtils.pageFlowQueryCheck(insertNewAccounts[0].Id);
                OB_AgentEntityUtils.hasOpenSr(new set<string>{insertNewAccounts[0].Id});
                
                
                Relationship__c rel = new Relationship__c();
        rel.Subject_Account__c = insertNewAccounts[0].id;
        rel.Object_Contact__c = con.Id;
        rel.Object_Account__c = insertNewAccounts[0].id;
                rel.Relationship_Type__c = 'Has Holding Company';
                rel.Active__c =true;
                
                insertNewAccounts[0].Sys_Office__c ='ttt,rr,edd';
                update insertNewAccounts[0];
        insert rel;
                
                
                insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('Commercial_Permission_Renewal').getRecordTypeId();
                //update insertNewSRs[0];
                
                try{
                  OB_AgentEntityUtils.createNewCpRenewalSr(con.id,'update' );  
                    
                    OB_AgentEntityUtils.createNewCpRenewalSr(con.id,'update' );  
                    
                   
                    

                }catch(Exception e){
                    
                }
                
                try{
                    
                     insertNewSRs[0].HexaBPM__Customer__c  = insertNewAccounts[0].Id;
        update insertNewSRs[0];
                    
                    OB_AgentEntityUtils.createNewCpRenewalSr(con.id,'update' );  
                }catch(Exception e){
                    
                }
                
                 try{
                    
                    OB_AgentEntityUtils.getCPREnewalLInk();
                }catch(Exception e){
                    
                }
                
                string srID = insertNewSRs[0].id;
                OB_AgentEntityUtils.setAmedFromObjAccount(rel,srID);
                OB_AgentEntityUtils.setAmedFromObjContact(rel,srID);
                OB_AgentEntityUtils.checkForOpenCompliance(insertNewAccounts[0].id);
                OB_AgentEntityUtils.checkForSubmittedCPRenwal(srID);
                
                insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('New_User_Registration').getRecordTypeId();
                update insertNewSRs[1];
                OB_AgentEntityUtils.pageFlowQueryCheck(insertNewAccounts[0].Id);
                
                
            }
            
            
            
        }
}