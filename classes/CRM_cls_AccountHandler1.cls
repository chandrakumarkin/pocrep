/******************************************************************************************
 *  Name        : CRM_cls_AccountHandler 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2017-25-1
 *  Description : Utility class for Account updates for CRM via Process Builder
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   25-01-2017   Claude        Created
 V1.1   29-01-2017   Claude        Added log for errors for multipe opportunities and/or no opportunity
 V1.2   09-02-2017   Claude        Added Closed Date for ROC Registered Accounts
 V1.3   12-07-2017   Claude        Added logic for funds vehicle
 V1.4   30-07-2017   Claude        Added logic to create license activities for Retail
*******************************************************************************************/
global without sharing class CRM_cls_AccountHandler1 {
    
    private static CRM_cls_Utils crmUtils;
    
    /**
     * V1.4
     * Creates license activities for newly converted accounts
     * @params      acctActivityMap         The list of activities mapped per account id
     * @params      mappedAccountRecords    Map of newly converted accounts
     */ 
    /** 
     * V1.3
     * Dedicated method for javascript-invoked actions
     * @params      accountId       The account's record Id
     * @params      methodname      The method called in the javascript
     * @params      params          The parameters passed in JSON format
     * @return      result          The result of the transaction
     */
    @AuraEnabled
    webservice static String processAccountRecords(String accountId, String methodName, String params){
      system.debug('accountId----ghan' +accountId);
     system.debug('methodName----ghan'+ methodName); 
      system.debug('params----ghan'+ params); 
          List<GroupMember> gpmr = [SELECT GroupId,UserOrGroupId FROM GroupMember where GroupId ='00G3E000000kTgc' and UserOrGroupId = :userinfo.getuserid()]; 
        // if(gpmr != Null){
	  
        if( String.isNotBlank(methodName) &&  String.isNotBlank(accountId) && gpmr.size()>0){
                    
            crmUtils = new CRM_cls_Utils();
            
            system.debug('You are welcome');
            if(methodName.equals('createFundAccount') &&  params != Null /* && functionParams.containsKey('relationshipType') */){
                
                return createFundAccount(accountId,params);                
            }            
            
                
        }
		
		 else{
            return 'you are not authorized to generate fund records';
         }
        
        return 'Nothing defined during method callout.';
        
    }
    
    /**
     * V1.3
     * Creates a fund vehicle account
     * related to its respective 
     * financial account
     * @params      accountId           The account's record Id
     * @params      params              The parameters passed in the javascript
     * @return      result              The result of the transaction 
     */
    @AuraEnabled
    public static string createFundAccount(string accountId, string params){
        
        Savepoint spdata = Database.setSavepoint();
            
        try {
            
            List<Account> accountRecords = getAccountRecords(new List<Id>{accountId});
            Id fundRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('BD Fund').getRecordTypeId();
             
            if(!accountRecords.isEmpty()){
                if (!Test.isRunningTest()) {
                crmUtils.setLogType('CRM: Creating Fund Account');
                }
                Account fundAccount = new Account();
                
                fundAccount.Name = accountRecords[0].Name + ' - Fund';
                fundAccount.Priority_to_DIFC__c = 'Standard';
                fundAccount.Company_Type__c = 'Non - financial';
                fundAccount.BD_Sector__c = 'Wealth Management (WM)';
                
                fundAccount.ownerId = accountRecords[0].ownerId;
                fundAccount.Geographic_Region__c = accountRecords[0].Geographic_Region__c;
                fundAccount.Financial_Sector_Activities__c = accountRecords[0].Financial_Sector_Activities__c;
                fundAccount.BD_Financial_Services_Activities__c = accountRecords[0].BD_Financial_Services_Activities__c;
                fundAccount.recordTypeId = fundRecordTypeId ;
              
                insert fundAccount;
                system.debug('fundAccount--Ghan'+fundAccount);
                Relationship__c fundRelationship = new Relationship__c();

              string relType = String.valueOf(params);
                system.debug('relType---'+relType);
                fundRelationship.Subject_Account__c= accountId;
                fundRelationship.Object_Account__c = fundAccount.Id;  
                fundRelationship.Active__c = true;
                fundRelationship.Relationship_Type__c = relType;      // TODO: To be determined     
                //fundRelationship.Relationship_Type_formula__c= relType;                   
                fundRelationship.Start_Date__c = system.today();
                fundRelationship.Push_to_SAP__c = true;
                
                insert fundRelationship;
                system.debug('fundRelationship--'+fundRelationship);
                Map<String,String> mappedContactIds = new Map<String,String>();
                
                SAPWebServiceDetails.CRMPartnerCall(new List<String>{fundRelationship.Id},null,new Map<String,String>{fundAccount.Id => 'ConvertedAccount'}); // TODO: Change account param if needed
                
                insert new Opportunity(
                    AccountId = fundAccount.Id,
                    Name = fundAccount.Name + ' - Fund',
                    StageName = 'Documents submitted to DFSA',
                    CloseDate = System.Today() + 30,
                    RecordTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Opportunity' AND DeveloperName = 'BD_Financial_Fund'].Id
                );
                system.debug('fundAccount.Id----'+fundAccount.Id);
               // return fundAccount.Id;
               return 'Fund account successfully generated!';
            }
            
           
            
     //   }

        
      //  else{
       //     return 'you are not authorized to generate fund records';
      //  }
             return 'Account not found.';
    }        
        
   catch (DMLException e){
            
            return getErrorMessage(e,spdata);
        }
        
    }
    
    /**
     * Executes any related action for Account records for CRM
     * account records
     * @params      ids     The account IDs
     */
    @InvocableMethod(label='Execute Account Update Processes' description='Executes any related processes when an Account is updated')
    public static void processAccountRecords(List<ID> ids) {
        
        List<Account> accountsToBeUpdated = getAccountRecords(ids);
        
        /*  
         *  NOTE: To avoid any SOQL limits during the transaction, use the 
         *  @future annotation. If the method will call another
         *  method that has the @future annotation, use Queueable
         *  classes instead.
         */
            
        // #3857 - Start
        updateRelatedRocOppsToRegistered(accountsToBeUpdated);
        // #3857 - End
        
    }
    
    /**
     * V1.3
     * Gets the account records
     * @params      ids             The account record IDs
     * @return      accountRecords  The account records
     */
    private static List<Account> getAccountRecords(List<Id> ids){
        
        return [SELECT Id, Name, ROC_Status__c, (SELECT id, StageName FROM Opportunities),
                                                    OwnerId,                                    // V1.3 - New Fields - Start 
                                                    Financial_Sector_Activities__c,       
                                                    BD_Financial_Services_Activities__c,
                                                    Geographic_Region__c                        // V1.3 - New Fields - End
                                                    from Account WHERE Id = :ids];
        
    }
    
    /**
     * Updates an Account record's opportunity
     * to 'ROC Registered'
     * account records
     * @params      accountsToBeUpdated     The accounts to be updated
     */
    private static void updateRelatedRocOppsToRegistered(List<Account> accountsToBeUpdated){

        Set<Id> oppIds = new Set<Id>();
        
        crmUtils = new CRM_cls_Utils();
        
        crmUtils.setLogType('CRM: ROC Status Update');
            
        for(Account a : accountsToBeUpdated){
                
            /* Add the opportunity records */
            if(!a.Opportunities.isEmpty() && 
                String.isNotBlank(a.ROC_Status__c) && 
                a.ROC_Status__c.equals('Active') &&
                a.Opportunities.size() == 1){
                
                // V1.1
                for(Opportunity o : a.Opportunities){
                    oppIds.add(o.Id);   
                }
                // V1.1
                
            } else if(a.Opportunities.isEmpty()) {
                crmUtils.setErrorMessage('No opportunity was found for this account: ' + a.Name + ' with Record ID: ' + a.Id);
            } else if(a.Opportunities.size() > 1){
                crmUtils.setErrorMessage('Multiple opportunities were found for this account: ' + a.Name + ' with Record ID: ' + a.Id);
            }
            
            if(crmUtils.hasError()){ 
                crmUtils.logError();    
                break;
            }
        }
            
        if(!oppIds.isEmpty() && !crmUtils.hasError()) updateRelatedOpportunitiesStage(oppIds,'ROC Registered');    
    }
    
    /**
     * Updates an Account record's opportunity
     * account records
     * @params      oppIds          Set of opportunity record IDs
     * @params      stageName       The name of the stage
     */
    private static void updateRelatedOpportunitiesStage(Set<Id> oppIds, String stageName){
        
        List<Opportunity> listOpps = new List<Opportunity>();
        
        for(Id i : oppIds){
            listOpps.add(new Opportunity(id = i,StageName = stageName,CloseDate = System.Today())); // V1.2 - Claude - Added Closed Date
        }
        
      //  System.debug('The queued job >>' + System.enqueueJob(new QueuableAccountHandler(listOpps)));
    } 
    
    /**
     * Returns the error thrown by 
     * the transaction
     * @params      e               The exception instance
     * @params      spdata          The savepoint
     * @return      errorMessage    The error message
     */
    @TestVisible
    private static String getErrorMessage(Exception e, Savepoint spdata){
        
        if(Test.isRunningTest()){
        
             crmUtils = new CRM_cls_Utils();
             
             crmUtils.setLogType('CRM: Test Fund Account');
        }
        
        crmUtils.setErrorMessage('Error occurred at ' + e.getLineNumber() + '. The error: ' + e.getMessage() );
                
        crmUtils.logError();
        
        Database.rollback(spdata);
        
        return crmUtils.getErrorMessage();
        
    } 
    
}