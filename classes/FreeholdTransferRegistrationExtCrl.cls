/*
Created By  : Suchita - DIFC on 18 Feb 2021
Description : This Class is extension class for FreeholdTransferRegistration visualforce page
--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No        Date                Updated By          Description
v1.0      18 Feb 2021         Suchita Sharma,       This Class is extension class for FreeholdTransferRegistration visualforce page

----------------------------------------------------------------------------------------
*/
public class FreeholdTransferRegistrationExtCrl {
    
    public Service_Request__c SRData { get; set;}
    public string RecordTypeId{get;set;}
    public map < string, string > mapParameters;
    //Constructor
    public FreeholdTransferRegistrationExtCrl (ApexPages.StandardController controller) {
        
        SRData = (Service_Request__c) controller.getRecord();
        
        if (apexpages.currentPage().getParameters() != null)
            mapParameters = apexpages.currentPage().getParameters();
        if (mapParameters.get('RecordType') != null)
            RecordTypeId = mapParameters.get('RecordType');
        
        PopulateInitialData(); 
        
    }
    
    public void PopulateInitialData(){
        for (User objUsr: [SELECT id, ContactId, Email, Contact.Account.Legal_Type_of_Entity__c, Phone, Contact.AccountId, 
                           Contact.Account.Name 
                           FROM User 
                           WHERE Id =: userinfo.getUserId() ]) {
                           if (SRData.id == null) {
                                   SRData.Customer__c= objUsr.Contact.AccountId;
                                   SRData.RecordTypeId = RecordTypeId;
                                   SRData.Email__c = objUsr.Email;
                                   SRData.Legal_Structures__c = objUsr.Contact.Account.Legal_Type_of_Entity__c;
                                   SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                               }
                           }
    }
    public PageReference SaveRecord() {
            try{
            upsert SRData ;
            PageReference acctPage = new ApexPages.StandardController(SRData).view();
            acctPage.setRedirect(true);
            return acctPage;
            }catch (Exception e) {
                 ApexPages.Message myMsg =new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getDmlMessage(0));
                 ApexPages.addMessage(myMsg);
                 return NULL;
                }
        }
}