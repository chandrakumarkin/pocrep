@isTest 
public class CreateLeadControllerTestClass 
{
    static testMethod void testMethod1() 
    {
        Case testCase = new Case();
        testCase.Company_Name__c='Test Case1' ;
        testCase.GN_Are_you_sure_you_want_to_create_lead__c = 'Yes';
        testCase.Origin = 'Phone';
        insert testCase;
        
        Task objTask = new Task();
        objTask.Subject = 'test';
        objTask.WhatId = testCase.Id;
        insert objTask;
        
        Test.StartTest(); 
        system.currentPageReference().getParameters().put('Id', testCase.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
        CreateLeadController testCaseController = new CreateLeadController(sc);
        testCaseController.CancelAction(); 
        testCaseController.createLead();
        Test.StopTest();
    }
    static testMethod void testMethod2() 
    {
        
        Case testCase = new Case();
        testCase.Company_Name__c='Test Case1' ;
        testCase.GN_Are_you_sure_you_want_to_create_lead__c = 'Yes';
        insert testCase;
        
        Test.StartTest(); 
        system.currentPageReference().getParameters().put('Id', testCase.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(testCase);
        CreateLeadController testCaseController = new CreateLeadController(sc);
        testCaseController.CancelAction(); 

        testCase.Company_Name__c='===================================================================================================Some string which is longer than 255 characters================================================================================================================';
        testCaseController.objCase = testCase;
        testCaseController.createLead();
        Test.StopTest();
    }
}