/*
Created By  : Suchita - DIFC on 9 June 2021
Description : This Class is extension class for SurrenderTerminationOfLeaseSublease visualforce page
--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No        Date                Updated By          Description
v1.0      9 June 2021         Suchita Sharma,       This Class is extension class for SurrenderTerminationOfLeaseSublease visualforce page

----------------------------------------------------------------------------------------
*/
public class SurrenderTerminationOfLeaseSublease {
    public Service_Request__c SRData { get; set;}
    public string RecordTypeId{get;set;}
    public boolean showSecuritySec{get;set;}
    public string unitId{get;set;}
    public map < string, string > mapParameters;    
    public Service_Request__c ServiceRequest {get;set;}
    public string SelectedUnit{get;set;}
    public Id SRID{get;set;}
    public String unitUsageTypeStr{get;set;}
    //Constructor
    public SurrenderTerminationOfLeaseSublease (ApexPages.StandardController controller){
        
        ServiceRequest = (Service_Request__c) controller.getRecord();
      	
        if (apexpages.currentPage().getParameters() != null)
            mapParameters = apexpages.currentPage().getParameters();
        if (mapParameters.get('RecordType') != null)
            RecordTypeId = mapParameters.get('RecordType');
        if(mapParameters.get('id') != null)
            SRID = mapParameters.get('id');
        
        showSecuritySec=false;   
        PopulateInitialData(); 
        ServiceRequest = getSRDetails(ServiceRequest);
        
    }
    
    public void PopulateInitialData(){
        for (User objUsr: [SELECT id, ContactId, Email, Contact.Account.Legal_Type_of_Entity__c, Phone, Contact.AccountId, 
                           Contact.Account.Name 
                           FROM User 
                           WHERE Id =: userinfo.getUserId() ]) {
                           if (ServiceRequest.id == null) {
                                ServiceRequest.Customer__c= objUsr.Contact.AccountId;
                                ServiceRequest.RecordTypeId = RecordTypeId;
                                ServiceRequest.Email__c = objUsr.Email;
                                ServiceRequest.Legal_Structures__c = objUsr.Contact.Account.Legal_Type_of_Entity__c;
                                ServiceRequest.Send_SMS_To_Mobile__c = objUsr.Phone;
                               }
                           }
    }
    public PageReference SaveRecord() {
            try{
            if(ServiceRequest.id!=null){
                update ServiceRequest;  
                /*if(ServiceRequest.Unit__c!=null){
                    clsRefund_of_the_Security_Deposit cls = new clsRefund_of_the_Security_Deposit();
                    cls.getBPNumberforUnit(String.valueOf(ServiceRequest.Unit__c),String.valueOf(ServiceRequest.id));
                }*/
            }
            else{insert ServiceRequest;  
            } 
            PageReference acctPage = new ApexPages.StandardController(ServiceRequest).view();
            acctPage.setRedirect(true);
            return acctPage;
            }catch (Exception e) {
                 ApexPages.Message myMsg =new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getDmlMessage(0));
                 ApexPages.addMessage(myMsg);
                 return NULL;
                }
    }
    public PageReference securitySection() {        
       System.debug('Method Called securitySection***');
        for(Unit__c u:[select id,Unit_Type_DDP__c from Unit__c where id=:SelectedUnit]){
            if(u.Unit_Type_DDP__c=='Residential'){
                unitUsageTypeStr='Residential';
                showSecuritySec=true;  
            }
        }
        System.debug('showSecuritySec***'+showSecuritySec);
        return null;
    } 
    public Service_Request__c getSRDetails(Service_Request__c objSR){
       
        objSR = (objSR != null) ? objSR : new Service_Request__c();
        
        if(SRID != null){
            String sQry = 'select Id,Name,Probation_Period__c ,Customer__c,Submitted_Date__c,Email__c,Send_SMS_To_Mobile__c,Type_of_Lease_Cancellation__c,Justification__c,Confirm_Change_of_Entity_Name__c,Feature__c,Street_Previous_Sponsor__c,Sponsor_P_O_Box__c'; 
            sQry += ',Agreement_Date__c,Lease_Expiry_Date__c,Lease_Commencement_Date__c,Type_of_Lease__c,Rental_Amount__c,Building__c,Unit__c,Unit__r.Unit_Type_DDP__c,Type_of_Request__c,Annual_Amount_AED__c';
            
            sQry += ',Security_Deposit_AED__c,Cage_No__c,Share_Value__c,Middle_Name_Previous_Sponsor__c,SAP_SGUID__c,Bank_Name__c,SAP_OSGUID__c,SAP_GSTIM__c,IBAN_Number__c,Bank_Address__c,Sys_Estimated_Share_Capital__c,Authority_Name__c,Sponsor_Middle_Name__c,Tax_Registration_Number_TRN__c,Post_Code__c,SAP_GSDAT__c,Address_Details__c,Activity_NOC_Required__c,Downsizing_Applicable__c,is_RM_Rejected__c,RecordTypeId,Confirm_Change_of_Trading_Name__c,Business_Name__c,Commercial_Activity_Previous_Sponsor__c,Name_Address_DOB_of_Shareholders__c,Name_Address_DOB_of_Directors__c,Entity_Name_checkbox__c';
            
            sQry += ' From Service_Request__c where Id=\''+SRID+'\' ';
          
            for(Service_Request__c obj : database.query(sQry)){
                objSR = obj;
                showSecuritySec= obj.Unit__r.Unit_Type_DDP__c=='Residential'?true:false;
            }
        }
        return objSR;
    }
}