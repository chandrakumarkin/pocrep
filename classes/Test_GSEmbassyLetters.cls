/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_GSEmbassyLetters {

    static testMethod void myUnitTest1() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        service_request__c objSR = new service_request__c();
        
        System.runAs(objUser){
       		Test.setCurrentPage(Page.GSEmbassyLetter);
       		Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
       		cls_GSEmbassyLetter clsObj1 = new cls_GSEmbassyLetter(con);
       		clsObj1.recordTypeName = 'Consulate_or_Embassy_Letter';
       		clsObj1.IsDetailPage = true;
       		clsObj1.cancelRequest();
       	}
    }
    
    static testMethod void myUnitTest2() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        service_request__c objSR = new service_request__c();
        
        System.runAs(objUser){
       		Test.setCurrentPage(Page.GSEmbassyLetter);
       		Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
       		cls_GSEmbassyLetter clsObj1 = new cls_GSEmbassyLetter(con);
       		cls_GSEmbassyLetter clsObj2 = new cls_GSEmbassyLetter();
       		clsObj2.recordTypeName = 'Consulate_or_Embassy_Letter';
       		clsObj2.cancelRequest();
       		clsObj2.showCountryMethod();
       	}
    }
    
    static testMethod void myUnitTest3() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Consulate_or_Embassy_Letter' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.name = 'Draft';
        objStatus.Code__c = 'Draft';
        insert objStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objStatus.id;
        objSR.External_SR_Status__c = objStatus.id;
		objSR.Country__c = 'India; ';
		objSR.Country__c = objSR.Country__c + 'Afghanistan; ';
		objSR.Country__c = objSR.Country__c + 'Pakistan; ';
		objSR.contact__c = objContact1.id;
		insert objSR;
		
		Id recordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Consulate or Embassy Letter').getRecordTypeId();
		amendment__c amendObj = new amendment__c();
		amendObj.Letter_Format__c = 'Arabic';
		amendObj.Letter_To_Consulate_Embassy__c = 'Embassy of';
		amendObj.Purpose_of_Visit__c = 'Business';
		amendObj.Type_of_Visit_Visa__c = 'Single Entry';
		amendObj.emirate__c = 'Dubai';
		amendObj.ServiceRequest__c = objSR.id;
		amendObj.Permanent_Native_Country__c = 'India';	
		amendObj.ServiceRequest__c = objSR.id;
		amendObj.recordTypeId = recordTypeId;
		amendObj.customer__c = objSR.customer__c;
		amendObj.Contact__c = objSR.contact__c;
		insert amendObj;
		
		
        System.runAs(objUser){
       		Test.setCurrentPage(Page.GSEmbassyLetter);
       		Apexpages.currentPage().getParameters().put('id',objSR.id);
       		Apexpages.currentPage().getParameters().put('RecordType',issueFineId);
			Apexpages.currentPage().getParameters().put('RecordTypeName','Consulate_or_Embassy_Letter');
			Apexpages.currentPage().getParameters().put('isDetail','true');
       		Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
       		cls_GSEmbassyLetter clsObj1 = new cls_GSEmbassyLetter(con);
       		cls_GSEmbassyLetter clsObj2 = new cls_GSEmbassyLetter();
       		clsObj1.recordTypeName = 'Consulate_or_Embassy_Letter';
       		clsObj1.objSRWrapList = new map<string,cls_SRWrapperClassForFields> ();
       		cls_SRWrapperClassForFields tempObj = new cls_SRWrapperClassForFields('Consulate_or_Embassy_Letter');
       		tempObj.LetterFormat = 'Arabic';
			tempObj.LetterToConsulate = 'Embassy of';
			tempObj.PurposeofVisit = 'Business';
			tempObj.typeOfVisitVisa = 'Single Entry';
			tempObj.EmirateEmbassy = 'Dubai';
			clsObj1.objSRWrapList.put('India',tempObj);
       		clsObj1.confirmUpgrade();
       		clsObj1.submitRequest();
       		clsObj1.showCountryMethod();
       	}
    }
    
    static testMethod void myUnitTest4() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Consulate_or_Embassy_Letter' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.name = 'Draft';
        objStatus.Code__c = 'Draft';
        insert objStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objStatus.id;
        objSR.External_SR_Status__c = objStatus.id;
		objSR.Country__c = 'India; ';
		objSR.Country__c = objSR.Country__c + 'Afghanistan; ';
		objSR.Country__c = objSR.Country__c + 'China; ';
		objSR.Country__c = objSR.Country__c + 'Pakistan; ';
		objSR.Country__c = objSR.Country__c + 'Algeria; ';
		objSR.Country__c = objSR.Country__c + 'Australia; ';
		objSR.contact__c = objContact1.id;
		insert objSR;
		
        System.runAs(objUser){
       		Test.setCurrentPage(Page.GSEmbassyLetter);
       		Apexpages.currentPage().getParameters().put('id',objSR.id);
       		Apexpages.currentPage().getParameters().put('RecordType',issueFineId);
			Apexpages.currentPage().getParameters().put('RecordTypeName','Consulate_or_Embassy_Letter');
       		Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
       		cls_GSEmbassyLetter clsObj1 = new cls_GSEmbassyLetter(con);
       		cls_GSEmbassyLetter clsObj2 = new cls_GSEmbassyLetter();
       		clsObj1.showCountryMethod();
       	}
    }
    
    static testMethod void myUnitTest5() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Consulate_or_Embassy_Letter' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Supporting letter to travel for Business/Personal – DIFC Sponsored Employee';
        objTemplate.SR_RecordType_API_Name__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.name = 'Draft';
        objStatus.Code__c = 'Draft';
        insert objStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objStatus.id;
        objSR.External_SR_Status__c = objStatus.id;
        objSR.Country__c = 'India; ';
		objSR.Country__c = objSR.Country__c + 'Afghanistan; ';
		objSR.Country__c = objSR.Country__c + 'China; ';
		objSR.contact__c = objContact1.id;
        objSR.SR_Template__c = objTemplate.id;
		insert objSR;
		
        System.runAs(objUser){
       		Test.setCurrentPage(Page.GSEmbassyLetter);
       		Apexpages.currentPage().getParameters().put('id',objSR.id);
       		Apexpages.currentPage().getParameters().put('RecordType',issueFineId);
			Apexpages.currentPage().getParameters().put('RecordTypeName','Consulate_or_Embassy_Letter');
       		Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
       		cls_GSEmbassyLetter clsObj1 = new cls_GSEmbassyLetter(con);
       		cls_GSEmbassyLetter clsObj2 = new cls_GSEmbassyLetter();
       		clsObj1.objSRWrapList = new map<string,cls_SRWrapperClassForFields> ();
       		cls_SRWrapperClassForFields tempObj = new cls_SRWrapperClassForFields('Consulate_or_Embassy_Letter');
       		tempObj.LetterFormat = 'Arabic';
			tempObj.LetterToConsulate = 'Embassy of';
			tempObj.PurposeofVisit = 'Business';
			tempObj.typeOfVisitVisa = 'Single Entry';
			tempObj.EmirateEmbassy = 'Dubai';
			clsObj1.objSRWrapList.put('India',tempObj);
       		clsObj1.saveRequest();
       	}
    }
    
    static testMethod void myUnitTest6() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Consulate_or_Embassy_Letter' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Supporting letter to travel for Business/Personal – DIFC Sponsored Employee';
        objTemplate.SR_RecordType_API_Name__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        SR_Status__c objStatus = new SR_Status__c();
        objStatus.name = 'Draft';
        objStatus.Code__c = 'Draft';
        insert objStatus;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objStatus.id;
        objSR.External_SR_Status__c = objStatus.id;
		objSR.Country__c = 'India; ';
		objSR.Country__c = objSR.Country__c + 'Afghanistan; ';
		objSR.Country__c = objSR.Country__c + 'China; ';
		objSR.contact__c = objContact1.id;
		objSR.SR_Template__c = objTemplate.id;
		insert objSR;
		
		Id recordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Consulate or Embassy Letter').getRecordTypeId();
		amendment__c amendObj = new amendment__c();
		amendObj.Letter_Format__c = 'Arabic';
		amendObj.Letter_To_Consulate_Embassy__c = 'Embassy of';
		amendObj.Purpose_of_Visit__c = 'Business';
		amendObj.Type_of_Visit_Visa__c = 'Single Entry';
		amendObj.emirate__c = 'Dubai';
		amendObj.ServiceRequest__c = objSR.id;
		amendObj.Permanent_Native_Country__c = 'India';	
		amendObj.ServiceRequest__c = objSR.id;
		amendObj.recordTypeId = recordTypeId;
		amendObj.customer__c = objSR.customer__c;
		amendObj.Contact__c = objSR.contact__c;
		insert amendObj;
		
        System.runAs(objUser){
       		Test.setCurrentPage(Page.GSEmbassyLetter);
       		Apexpages.currentPage().getParameters().put('id',objSR.id);
       		Apexpages.currentPage().getParameters().put('RecordType',issueFineId);
			Apexpages.currentPage().getParameters().put('RecordTypeName','Consulate_or_Embassy_Letter');
       		Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR);
       		cls_GSEmbassyLetter clsObj1 = new cls_GSEmbassyLetter(con);
       		cls_GSEmbassyLetter clsObj2 = new cls_GSEmbassyLetter();
       		clsObj1.objSRWrapList = new map<string,cls_SRWrapperClassForFields> ();
       		cls_SRWrapperClassForFields tempObj = new cls_SRWrapperClassForFields('Consulate_or_Embassy_Letter');
       		tempObj.LetterFormat = 'Arabic';
			tempObj.LetterToConsulate = 'Embassy of';
			tempObj.PurposeofVisit = 'Business';
			tempObj.typeOfVisitVisa = 'Single Entry';
			tempObj.EmirateEmbassy = 'Dubai';
			clsObj1.objSRWrapList.put('India',tempObj);	
       		clsObj1.showCountryMethod();
       		clsObj1.editRequest();
       		clsObj1.saveRequest();
       	}
    }
    
}