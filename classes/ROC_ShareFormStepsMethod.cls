/****************************************************************************************************************
 * Modification History
 * ----------------------------------------------------------------------------------------------------
 *  Version       Date                Author              Comments                                                 
 * ----------------------------------------------------------------------------------------------------
 *  v1.0		28/04/2021			  Shikha			Tkt #7075 - Inactivate relationship when no of shares are 0
 ****************************************************************************************************************/
public class ROC_ShareFormStepsMethod{
    /*
Using for both redemption and cancellation of shares
*/
    
    public static string CancellationOfShares(Step__c stp){
        
        string strResult = 'Success';
        try{
            
            List<Amendment__c> lstAmnd = [Select ID, Status__c, Class_Name__c,No_of_shares_per_class__c,Sys_Proposed_Nominal_Value__c,
                                          Class_of_Shares__c,
                                          Nominal_Value__c,Sys_Proposed_No_of_Shares_per_Class__c,Shareholder_Company_val__c,
                                          Total_Issued__c, ServiceRequest__c,Share_holder_company_txt__c
                                          FROM Amendment__c WHERE ServiceRequest__c = :stp.SR__c];
            
            
            if(lstAmnd.isEmpty()) return null;
            
            Map<string,Amendment__c> mapOfAmnd = new Map<string,Amendment__c>();
            
            for(Amendment__c iAmm : lstAmnd)
                mapOfAmnd.put(iAmm.Shareholder_Company_val__c+'-'+iAmm.Class_Name__c,iAmm);
            
            
            List<Shareholder_Detail__c> lstShareholderDetail = [Select ID, Account_Share__c, Account_Share__r.Name,
                                                                No_of_Shares__c,Shareholder_Account__c,
                                                                Shareholder_Contact__c,Status__c,Sys_Proposed_No_of_Shares__c
                                                                from Shareholder_Detail__c
                                                                where Account__c =: stp.SR__r.Customer__c
                                                                AND Status__c = 'Active'];
            
            if(lstShareholderDetail.isEmpty())  return null;
            
            List<Shareholder_Detail__c> updateLstShareHolderDetail = new List<Shareholder_Detail__c>();
            
            for(Shareholder_Detail__c iSh : lstShareholderDetail){
                
                if(string.isNotBlank(iSh.Shareholder_Account__c) && mapOfAmnd.ContainsKey(iSh.Shareholder_Account__c+'-'+iSh.Account_Share__r.Name)){
                    
                    Amendment__c amm = mapOfAmnd.get(iSh.Shareholder_Account__c+'-'+iSh.Account_Share__r.Name);
                    iSh.No_of_Shares__c -= amm.Sys_Proposed_No_of_Shares_per_Class__c;
                    iSh.Sys_Proposed_No_of_Shares__c = iSh.No_of_Shares__c; 
                    if(iSh.No_of_Shares__c == 0)
                    {
                        iSh.Status__c = 'Inactive';
                        
                    }
                    updateLstShareHolderDetail.add(iSh);
                }
                else if(string.isNotBlank(iSh.Shareholder_Contact__c) && mapOfAmnd.ContainsKey(iSh.Shareholder_Contact__c+'-'+iSh.Account_Share__r.Name)){
                    
                    Amendment__c amm = mapOfAmnd.get(iSh.Shareholder_Contact__c+'-'+iSh.Account_Share__r.Name);
                    iSh.No_of_Shares__c -= amm.Sys_Proposed_No_of_Shares_per_Class__c;
                    iSh.Sys_Proposed_No_of_Shares__c = iSh.No_of_Shares__c; 
                    if(iSh.No_of_Shares__c == 0)
                    {
                        iSh.Status__c = 'Inactive';
                        
                    }
                    updateLstShareHolderDetail.add(iSh);
                }
            }
            
            
            if(updateLstShareHolderDetail.isEmpty()) return null;
            
            update updateLstShareHolderDetail;  
            
            return strResult;
        }
        
        catch(Exception ex){
            return ex.getMessage();
        }
    }
    
    public static string PurchaseOwnOfShares(Step__c stp){
        
        string strResult = 'Success';
        //try{
        
        string typeOfPurchase = stp.SR__r.Commercial_Activity__c;
        
        List<Amendment__c> lstAmnd = [Select ID, Status__c, Class_Name__c,No_of_shares_per_class__c,Sys_Proposed_Nominal_Value__c,
                                      Class_of_Shares__c,
                                      Nominal_Value__c,Sys_Proposed_No_of_Shares_per_Class__c,Shareholder_Company_val__c,
                                      Total_Issued__c, ServiceRequest__c,Share_holder_company_txt__c
                                      FROM Amendment__c WHERE ServiceRequest__c = :stp.SR__c];
        
        if(lstAmnd.isEmpty()) return null;
        
        Map<string,Amendment__c> mapOfAmnd = new Map<string,Amendment__c>();
        
        for(Amendment__c iAmm : lstAmnd)
            mapOfAmnd.put(iAmm.Shareholder_Company_val__c+'-'+iAmm.Class_Name__c,iAmm);
        
        
        if(typeOfPurchase != NULL && typeOfPurchase.contains('treasury')){
            
            string companyName = [Select Name from Account where ID =: stp.SR__r.Customer__c].Name;
            string trAccountName = companyName + '(Treasury Account)';
            Map<string,Shareholder_Detail__c> className = new Map<string,Shareholder_Detail__c>();
            
            List<Shareholder_Detail__c> lstShareholderDetail = new List<Shareholder_Detail__c>();
            List<Shareholder_Detail__c> treasuryShareDetail = new List<Shareholder_Detail__c>();
            
            List<Account> lstAcc = [Select Id, name from Account where ParentId =: stp.SR__r.Customer__c];
            //v1.0 start
            Set<Id> bodyCorpAccountSet = new Set<Id>();
            Set<Id> relationContactSet = new Set<Id>();
            //v1.0 end
            //create an account
            if(lstAcc.isEmpty()){
                
                //account details
                Account parAcc = new Account();
                parAcc.Name = trAccountName;
                parAcc.ROC_reg_incorp_Date__c = stp.SR__r.Foreign_Registration_Date__c;
                parAcc.Place_of_Registration__c = stp.SR__r.Foreign_Registration_Place__c;
                parAcc.Building_Name__c = stp.SR__r.Foreign_Registered_Building__c;
                parAcc.Street__c = stp.SR__r.Foreign_Registered_Street__c;
                parAcc.PO_Box__c = stp.SR__r.Foreign_Registered_PO_Box__c;
                parAcc.Phone = stp.SR__r.Foreign_Registered_Phone__c;
                parAcc.Postal_Code__c = stp.SR__r.Foreign_Registered_Post__c;
                parAcc.Office__c = stp.SR__r.Foreign_registered_office_no__c;
                parAcc.Level__c = stp.SR__r.Foreign_Registered_Level__c;
                parAcc.Country__c = stp.SR__r.Foreign_Registered_Country__c;
                
                
                //Creating Relationship Type
                Relationship__c relAcc = new Relationship__c();
                relAcc.Subject_Account__c = stp.SR__r.Customer__c;
                relAcc.Relationship_Type__c = 'Has Holding Company';
                relAcc.Active__c = true;
                relAcc.Start_Date__c = system.today();
                
                parAcc.ParentId = stp.SR__r.Customer__c;
                insert parAcc;
                
                relAcc.Object_Account__c = parAcc.Id;
                
                insert relAcc; 
                
                lstAcc.add(parAcc);
                
            }
            
            List<Shareholder_Detail__c> updateLstShareHolderDetail = new List<Shareholder_Detail__c>();         
            List<Shareholder_Detail__c> existingShareHolderDetial = [Select ID, Account_Share__c, Account_Share__r.Name,
                                                                     No_of_Shares__c,Shareholder_Account__c,
                                                                     Shareholder_Account__r.Name,
                                                                     Shareholder_Contact__c,Status__c,
                                                                     Sys_Proposed_No_of_Shares__c
                                                                     from Shareholder_Detail__c
                                                                     where Account__c =: stp.SR__r.Customer__c
                                                                     AND Status__c = 'Active'];
            
            Map<string,Shareholder_Detail__c> mapOfTreasury = new Map<string,Shareholder_Detail__c>();
            
            
            for(Shareholder_Detail__c iSh : existingShareHolderDetial){
                
                if(string.isNotBlank(iSh.Shareholder_Account__c)){
                    if(mapOfAmnd.ContainsKey(iSh.Shareholder_Account__c+'-'+iSh.Account_Share__r.Name) 
                       && !iSh.Shareholder_Account__r.Name.contains('Treasury')){
                           
                           Amendment__c amm = mapOfAmnd.get(iSh.Shareholder_Account__c+'-'+iSh.Account_Share__r.Name);
                           iSh.No_of_Shares__c -= amm.Sys_Proposed_No_of_Shares_per_Class__c;
                           iSh.Sys_Proposed_No_of_Shares__c = iSh.No_of_Shares__c; 
                           if(iSh.No_of_Shares__c == 0){
                               iSh.Status__c = 'Inactive';
                               bodyCorpAccountSet.add(iSh.Shareholder_Account__c);//v1.0
                           }
                           
                           updateLstShareHolderDetail.add(iSh);
                           
                           //create new share holder detail for treasury
                           Shareholder_Detail__c sh;
                           sh = new Shareholder_Detail__c();
                           sh.No_of_Shares__c = amm.Sys_Proposed_No_of_Shares_per_Class__c;
                           sh.Account__c = stp.SR__r.Customer__c;
                           sh.Shareholder_Account__c  = lstAcc[0].ID;
                           sh.Status__c = 'Active';
                           sh.Account_Share__c = iSh.Account_Share__c;
                           sh.Total_Nominal_Value__c = iSh.Total_Nominal_Value__c;
                           
                           
                           if(mapOfTreasury.containsKey(iSh.Account_Share__r.Name)){
                               mapOfTreasury.get(iSh.Account_Share__r.Name).No_of_Shares__c += sh.No_of_Shares__c;
                           }
                           else
                               mapOfTreasury.put(iSh.Account_Share__r.Name , sh);
                           
                       }
                    
                }
                else if(string.isNotBlank(iSh.Shareholder_Contact__c)){
                    if(mapOfAmnd.ContainsKey(iSh.Shareholder_Contact__c+'-'+iSh.Account_Share__r.Name)){
                        
                        Amendment__c amm = mapOfAmnd.get(iSh.Shareholder_Contact__c+'-'+iSh.Account_Share__r.Name);
                        iSh.No_of_Shares__c -= amm.Sys_Proposed_No_of_Shares_per_Class__c;
                        iSh.Sys_Proposed_No_of_Shares__c = iSh.No_of_Shares__c; 
                        if(iSh.No_of_Shares__c == 0){
                            iSh.Status__c = 'Inactive';
                            relationContactSet.add(iSh.Shareholder_Contact__c);//v1.0
                        }
                        
                        updateLstShareHolderDetail.add(iSh);
                        
                        Shareholder_Detail__c sh;
                        sh = new Shareholder_Detail__c();
                        sh.No_of_Shares__c = amm.Sys_Proposed_No_of_Shares_per_Class__c;
                        sh.Account__c = stp.SR__r.Customer__c;
                        sh.Shareholder_Account__c  = lstAcc[0].ID;
                        sh.Status__c = 'Active';
                        sh.Account_Share__c = iSh.Account_Share__c;
                        sh.Total_Nominal_Value__c = iSh.Total_Nominal_Value__c;
                        
                        // mapOfTreasury.put(iSh.Account_Share__r.Name , sh);
                        
                        if(mapOfTreasury.containsKey(iSh.Account_Share__r.Name)){
                            mapOfTreasury.get(iSh.Account_Share__r.Name).No_of_Shares__c += sh.No_of_Shares__c;
                        }
                        else
                            mapOfTreasury.put(iSh.Account_Share__r.Name , sh);
                    }
                    
                }   
            }
            
            
            for(Shareholder_Detail__c iSh : existingShareHolderDetial){
                
                if(iSh.Shareholder_Account__c != NULL && iSh.Shareholder_Account__r.Name.contains('Treasury')){
                    if(mapOfTreasury.containsKey(iSh.Account_Share__r.Name)){
                        
                        Shareholder_Detail__c newSH = mapOfTreasury.get(iSh.Account_Share__r.Name);
                        iSh.No_of_Shares__c += newSH.No_of_Shares__c;
                        updateLstShareHolderDetail.add(iSh);
                        mapOfTreasury.remove(iSh.Account_Share__r.Name);
                    }
                    
                    
                }
                
            }
            
            if(!mapOfTreasury.isEmpty())
                insert mapOfTreasury.values();
            
            if(!updateLstShareHolderDetail.isEmpty())
                update updateLstShareHolderDetail;
            //v1.0
            InactivateRelationship(bodyCorpAccountSet,relationContactSet,stp.SR__r.Customer__c);
            return strResult;
            
        }           
        else{
            
            List<Shareholder_Detail__c> lstShareholderDetail = [Select ID, Account_Share__c, Account_Share__r.Name,
                                                                No_of_Shares__c,Shareholder_Account__c,
                                                                Shareholder_Contact__c,Status__c,
                                                                Sys_Proposed_No_of_Shares__c
                                                                from Shareholder_Detail__c
                                                                where Account__c =: stp.SR__r.Customer__c
                                                                AND Status__c = 'Active'];
            //v1.0 Start
            Set<Id> bodyCorpAccountSet = new Set<Id>();
            Set<Id> relationContactSet = new Set<Id>();
            //v1.0 End
            
            if(lstShareholderDetail.isEmpty())  return null;
            
            List<Shareholder_Detail__c> updateLstShareHolderDetail = new List<Shareholder_Detail__c>();
            
            for(Shareholder_Detail__c iSh : lstShareholderDetail){
                
                if(string.isNotBlank(iSh.Shareholder_Account__c) && mapOfAmnd.ContainsKey(iSh.Shareholder_Account__c+'-'+iSh.Account_Share__r.Name)){
                    
                    Amendment__c amm = mapOfAmnd.get(iSh.Shareholder_Account__c+'-'+iSh.Account_Share__r.Name);
                    iSh.No_of_Shares__c -= amm.Sys_Proposed_No_of_Shares_per_Class__c;
                    iSh.Sys_Proposed_No_of_Shares__c = iSh.No_of_Shares__c; 
                    if(iSh.No_of_Shares__c == 0)
                    {
                        iSh.Status__c = 'Inactive';
                        bodyCorpAccountSet.add(iSh.Shareholder_Account__c);//v1.0
                    }
                    
                    updateLstShareHolderDetail.add(iSh);
                }
                else if(string.isNotBlank(iSh.Shareholder_Contact__c) && mapOfAmnd.ContainsKey(iSh.Shareholder_Contact__c+'-'+iSh.Account_Share__r.Name)){
                    
                    Amendment__c amm = mapOfAmnd.get(iSh.Shareholder_Contact__c+'-'+iSh.Account_Share__r.Name);
                    iSh.No_of_Shares__c -= amm.Sys_Proposed_No_of_Shares_per_Class__c;
                    iSh.Sys_Proposed_No_of_Shares__c = iSh.No_of_Shares__c; 
                    if(iSh.No_of_Shares__c == 0)
                    {
                        iSh.Status__c = 'Inactive';
                        relationContactSet.add(iSh.Shareholder_Contact__c);//v1.0
                    }
                    
                    updateLstShareHolderDetail.add(iSh);
                }
            }
            
            if(updateLstShareHolderDetail.isEmpty()) return null;
            
            update updateLstShareHolderDetail; 
            //v1.0
            InactivateRelationship(bodyCorpAccountSet,relationContactSet,stp.SR__r.Customer__c);                            
            return strResult;
        }
        
        // }
        
        /*  catch(Exception ex){
return ex.getMessage();
}*/
    }
    
    //v1.0 Start
    public static void InactivateRelationship(Set<Id> bodyCorpSet, Set<Id> relationConSet, String accountId){
        List<Relationship__c> updatedRelationshipList = new List<Relationship__c>();
        if(!bodyCorpSet.isEmpty()){
            for(Relationship__c objRelation : [SELECT Id, Name, End_Date__c, object_Account__c, Status__c, Active__c FROM Relationship__c 
                                               WHERE Object_Account__c IN :bodyCorpSet AND Subject_Account__c =:accountId AND Relationship_Type_formula__c = 'Shareholder']){                        
                                                   objRelation.Active__c = false;
                                                   objRelation.End_Date__c = System.today();
                                                   updatedRelationshipList.add(objRelation);
                                               }
        }
        if(!relationConSet.isEmpty()){
            for(Relationship__c objRelation : [SELECT Id, Name, End_Date__c, Object_contact__C, Status__c, Active__c FROM Relationship__c 
                                               WHERE Object_Contact__c IN :relationConSet AND Subject_Account__c =:accountId AND Relationship_Type_formula__c = 'Shareholder']){                        
                                                   objRelation.Active__c = false;
                                                   objRelation.End_Date__c = System.today();
                                                   updatedRelationshipList.add(objRelation);
                                               }
        }
        if(!updatedRelationshipList.isEmpty()){
            update updatedRelationshipList;
        }
    }
    //v1.0 End
    
    public static string ConsolidateSubDivdeShares(Step__c stp){
        
        try{
            string strResult = 'Success';
            List<Amendment__c> lstAmnd = [Select ID, Class_Name__c,No_of_shares_per_class__c,
                                          Class_of_Shares__c,
                                          Nominal_Value__c,Sys_Proposed_Nominal_Value__c,Sys_Proposed_No_of_Shares_per_Class__c,
                                          Currency__r.Name,Shareholder_Company_val__c,Total_Issued_New__c,
                                          Total_Issued__c, ServiceRequest__c FROM Amendment__c WHERE ServiceRequest__c =: stp.SR__c
                                          ORDER BY ID];
            
            if(lstAmnd.isEmpty()) return null;
            
            Map<string, Amendment__c> mapOfAmnd = new Map<string,Amendment__c>();
            Map<string,Account_Share_Detail__c> mapOfAcocuntShare = new Map<string,Account_Share_Detail__c>();
            
            for(Amendment__c iAmm : lstAmnd)
                mapOfAmnd.put(iAmm.Class_Name__c, iAmm);
            
            for( Account_Share_Detail__c iAccountShareDetail : [Select ID, Account__c,Name,Nominal_value_txt__c,
                                                                Sys_Proposed_No_of_Shares_per_Class__c,
                                                                Sys_Proposed_Nominal_Value__c,
                                                                Nominal_Value__c,No_of_shares_per_class__c
                                                                FROM Account_Share_Detail__c WHERE Account__c =: stp.SR__r.Customer__c ]){
                                                                    
                                                                    if(mapOfAmnd.containsKey(iAccountShareDetail.Name)){
                                                                        iAccountShareDetail.Nominal_Value__c = mapOfAmnd.get(iAccountShareDetail.Name).Sys_Proposed_Nominal_Value__c;
                                                                        iAccountShareDetail.No_of_shares_per_class__c = mapOfAmnd.get(iAccountShareDetail.Name).Sys_Proposed_No_of_Shares_per_Class__c;
                                                                        iAccountShareDetail.Nominal_value_txt__c = string.ValueOf(iAccountShareDetail.Nominal_Value__c);
                                                                        iAccountShareDetail.Sys_Proposed_Nominal_Value__c = iAccountShareDetail.Nominal_Value__c;
                                                                        iAccountShareDetail.Sys_Proposed_No_of_Shares_per_Class__c = iAccountShareDetail.No_of_shares_per_class__c;
                                                                        mapOfAcocuntShare.put(iAccountShareDetail.ID,iAccountShareDetail);
                                                                    }
                                                                }
            
            if(mapOfAcocuntShare.isEmpty()) return strResult;
            
            List<Shareholder_Detail__c> lstOfNewShareHolderDetail = new List<Shareholder_Detail__c>();
            
            for(Shareholder_Detail__c iShd : [Select ID, Account_Share__c,Account_Share__r.Name, No_of_Shares__c,
                                              Sys_Proposed_No_of_Shares__c
                                              FROM Shareholder_Detail__c WHERE Account_Share__c IN : mapOfAcocuntShare.keySet()]){
                                                  
                                                  if(mapOfAcocuntShare.containsKey(iShd.Account_Share__c)){
                                                      
                                                      Account_Share_Detail__c accD = mapOfAcocuntShare.get(iShd.Account_Share__c);
                                                      Amendment__c iAmmOld = mapOfAmnd.get(iShd.Account_Share__r.Name);
                                                      iShd.No_of_Shares__c = (iShd.No_of_Shares__c / iAmmOld.Total_Issued__c) * accD.No_of_shares_per_class__c;
                                                      iShd.Sys_Proposed_No_of_Shares__c = iShd.No_of_Shares__c;
                                                      
                                                      lstOfNewShareHolderDetail.add(iShd);
                                                  }
                                              }
            
            if(!lstOfNewShareHolderDetail.isEmpty()){
                update lstOfNewShareHolderDetail;
                update mapOfAcocuntShare.values();
            }
            
            
            return strResult;
        }
        catch(Exception ex){
            return ex.getMessage();
        }       
        
    }
    
    /*------------------------------Signage SR -------------------------------------------------------*/
    
    static List<Operating_Location__c> getOperatingLocation(string aAccID){
        
        List<Operating_Location__c> lstOpt = new List<Operating_Location__c>();
        
        lstOpt = [Select ID,Unit__c,Unit__r.Name,Lease__c,Lease__r.Start_date__c,
                  Unit__r.Unit_Type_DDP__c,Unit__r.Unit_Usage_Type__c,
                  Hosting_Company__c,Building_Name__c,Type__c,
                  Unit__r.Is_Business_Center__c
                  from
                  Operating_Location__c where Account__c =: aAccID
                  AND IsRegistered__c = true AND Unit__r.Third_Party__c = false
                  AND Status__c = 'Active'];
        if(lstOpt.isEmpty()){
            insert LogDetails.CreateLog(null, 'Signage: GetOperatingLocation', 'Line Number : 387\nException is : NO lease Operating location exist for this account : '+aAccID);  
        }
        return lstOpt;
    }
    
    static Account getAccount(string aAccID){
        
        return [Select  Name ,Trade_Name__c,Company_Type__c,Legal_Type_of_Entity__c from 
                Account where ID =: aAccID];
    }
    
    @future
    public static void CreateSignageSRforEntityName(string aJsonStep){
        
        step__c stp = (step__c)Json.deserialize(aJsonStep,step__c.class);
        Account acc =  getAccount(stp.SR__r.Customer__c);
        
        Service_Request__c serviceReq = [Select ID ,Name,
                                         Entity_Name__c,Proposed_Trading_Name_1__c FROM Service_Request__c
                                         WHERE ID =: stp.SR__c];
        
        List<Operating_Location__c> lstOperatingLoc = getOperatingLocation(acc.ID);
        
        system.debug('###lstOperatingLoc' + lstOperatingLoc.size());
        if(lstOperatingLoc.size() == 0) 
            return; // if lease is for third party
        
        string usageType = lstOperatingLoc[0].Unit__r.Unit_Usage_Type__c;
        system.debug('$$$$$$' + usageType );
        if(usageType.contains('Retail') && string.isBlank(serviceReq.Proposed_Trading_Name_1__c)){
            insert LogDetails.CreateLog(null, 'Signage: CreateSignageSRforEntityName', 'Line Number : 417\nException is : trading name is blank for retail unit please fill trading name : '+acc.ID);  
            return; 
        }
        
        if(string.isBlank(serviceReq.Entity_Name__c) && (usageType.contains('Commercial') || usageType.contains('Business'))){
            insert LogDetails.CreateLog(null, 'Signage: CreateSignageSRforEntityName', 'Line Number : 417\nException is : Entiy name is blank for business/commercial unit please fill trading name : '+acc.ID); 
            return;
        }
        
        
        CreateSignageSR(stp,acc,lstOperatingLoc,false,false,false,false,true);
        
    }
    
    @future
    public static void CreateSignageSRforAddress(string aJsonStep){
        
        step__c stp = (step__c)Json.deserialize(aJsonStep,step__c.class);
        Account acc =  getAccount(stp.SR__r.Customer__c);
        List<Operating_Location__c> lstOperatingLoc = getOperatingLocation(acc.ID);
        
        system.debug('###lstOperatingLoc' + lstOperatingLoc.size());
        if(lstOperatingLoc.size() == 0) 
            return; // if lease is for third party
        
        CreateSignageSR(stp,acc,lstOperatingLoc,false,false,true,false,false);
        
    }
    
    
    
    @future
    public static void UpdateSignageSRforAOR(string aJsonStep){
        
        step__c stp = (step__c)Json.deserialize(aJsonStep,step__c.class);
        
        Account acc =  getAccount(stp.SR__r.Customer__c);
        
        List<Service_Request__c> lstServiceRequest = [Select id ,Domestic_Helper__c from Service_Request__c
                                                      where Customer__c =: acc.ID
                                                      AND External_Status_Code__c = 'Submitted'
                                                      AND RecordType.developerName = 'Signage'];
        
        
        
        if(!lstServiceRequest.isEmpty()){
            
            lstServiceRequest[0].Domestic_Helper__c = true;
            lstServiceRequest[0].Position__c = 'Stamp Signage';
            update lstServiceRequest;
            
            Step__c step = new Step__c();
            step.SR__c = lstServiceRequest[0].ID;
            step.SR_Step__c = [Select ID from SR_Steps__c WHERE Step_Template_Code__c = 'STAMP_SIGNAGE' limit 1].ID;
            step.Step_Template__c = [Select ID from Step_Template__c WHERE CODE__C = 'STAMP_SIGNAGE' limit 1].ID;
            step.Status__c = [select id,name from Status__c where Code__C  = 'PENDING_REVIEW'].id;   
            step.Step_No__c = 30;
            step.Due_Date__c= DateTime.Now().AddDays(1);
            insert step;
            
            
        }
        
    }
    
     @future
    public static void CopyExistingAddress(string aJsonStep){
        
          
         step__c stp = (step__c)Json.deserialize(aJsonStep,step__c.class);
         Account acc = [Select ID,Office__c, Building_Name__c,Street__c,City__c 
                        FROM Account where ID =: stp.SR__r.Customer__c];
        
        Service_Request__c sr = new Service_Request__c();
        sr.ID = stp.SR__c;
        sr.Description__c = acc.Office__c+', '+acc.Building_Name__c +', '+acc.Street__c +', '+acc.City__c;
        update sr;
          
    }    
    
    @future
    public static void CreateSignageSRForDissolution(string aJsonStep){
        
        step__c stp = (step__c)Json.deserialize(aJsonStep,step__c.class);
        
        Account acc =  getAccount(stp.SR__r.Customer__c);
        
        SR_Template__c srTemplate = [Select ID, Estimated_Hours__c from SR_Template__c where Name = 'Signage'];
        
        List<Operating_Location__c> lstOperatingLoc = getOperatingLocation(acc.ID);
        
        system.debug('###lstOperatingLoc' + lstOperatingLoc.size());
        if(lstOperatingLoc.size() == 0) 
            return; // if lease is for third party
        
        CreateSignageSR(stp,acc,lstOperatingLoc,false,false,false,true,false);
        
        
    }
    
    
    @future
    public static void CreateSignageSRforAOR(string aJsonStep){
        
        step__c stp = (step__c)Json.deserialize(aJsonStep,step__c.class);
        
        Account acc =  getAccount(stp.SR__r.Customer__c);
        
        SR_Template__c srTemplate = [Select ID, Estimated_Hours__c from SR_Template__c where Name = 'Signage'];
        
        List<Operating_Location__c> lstOperatingLoc = getOperatingLocation(acc.ID);
        
        if(lstOperatingLoc.size() == 0) 
            return; // if lease is for third party
        
        
        boolean isBusinessCenter = lstOperatingLoc[0].Unit__r.Is_Business_Center__c;
        boolean isNotSharing = false;
        
        isNotSharing = (string.isBlank(lstOperatingLoc[0].Hosting_Company__c)) ? true : false;
        
        if(isBusinessCenter && isNotSharing == false){
            insert LogDetails.CreateLog(null, 'Signage: CreateSignageSRforAOR', 'Line Number : 545\nException is : Cannot create signage for Business and sharing lease  : '+acc.ID); 
            return;
        }
        
        
        
        CreateSignageSR(stp,acc,lstOperatingLoc,isNotSharing,true,false,false,false);
        
    }
    
    
    
    static void CreateSignageSR(Step__c aStp, Account aAcc, List<Operating_Location__c> alstOperatingLoc, boolean aFintec,boolean aIsAOR, boolean aIsAddressChanged, boolean aIsDisolution ,boolean aNameChange){
        
        
        string userEmail = '';
        string stepName = '';
        string clientEmail = [Select Email__c from Service_Request__c where ID =: aStp.SR__c].Email__c ;
        if(aFintec){
            stepName = 'Prepare Signage';
            
        }
        if(!aFintec){
            
            stepName = 'Prepare and Stamp Signage';
        }
        
        if(aIsAddressChanged || aIsDisolution){
            
            stepName = 'Prepare and Embed Signage';
        }
        
        string UnitLoc = alstOperatingLoc[0].Unit__r.Unit_Usage_Type__c;
        Signage_Settings__c setting = Signage_Settings__c.getValues(UnitLoc);
        if(setting != null){
            userEmail = setting.Team_Email_Address__c;
        }
        
        
        SR_Template__c srTemplate = [Select ID, Estimated_Hours__c from SR_Template__c where Name = 'Signage'];
        ID SRSubmit = [select id,name from SR_Status__c where Code__C  = 'Submitted'].id;   
        ID BHId = id.valueOf(Label.Business_Hours_Id);
        Service_Request__c newSR = new Service_Request__c();
        newSR.Customer__c = aStp.SR__r.Customer__c;
        newSR.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Signage').getRecordTypeId();
        newSR.Company_Type_list__c = aAcc.Company_Type__c;
        newSR.Legal_Structures__c =   aAcc.Legal_Type_of_Entity__c;
        
        insert newSR;
        
        newSR.Submitted_Date__c = system.today();
        newSR.Submitted_DateTime__c = system.now();
        newSR.Internal_SR_Status__c = SRSubmit;
        newSR.External_SR_Status__c = SRSubmit;
        newSR.SR_Template__c = srTemplate.ID;
        newSR.finalizeAmendmentFlg__c = true;
        newSR.Is_AOR__c = aIsAOR;
        newSR.Domestic_Helper__c = aIsDisolution;
        newSR.Block_Family_at_the_airport__c = aFintec;
        newSR.Address_Changed__c = aIsAddressChanged;
        newSR.Entity_Name_checkbox__c = aNameChange;
        newSR.Email_Address__c = userEmail;
        
        if(aIsAddressChanged){
            
            Service_Request__c addressSR = [Select ID, Description__c,Registered_Address__c FROM 
                                            Service_Request__c WHERE ID =: aStp.SR__c limit 1];
            
            newSR.Address_Details__c = addressSR.Description__c;
            newSR.Registered_Address__c = addressSR.Registered_Address__c;
        }
        
        
        /* workflow email fields*/
        
        newSR.SAP_SGUID__c = aAcc.Name;
        newSR.Proposed_Trading_Name_1__c = aAcc.Trade_Name__c;
        newSR.Type_of_Unit__c = alstOperatingLoc[0].Unit__r.Name+', '+alstOperatingLoc[0].Building_Name__c+' ('+UnitLoc+'), '+alstOperatingLoc[0].Type__c;
        newSR.Lease_Commencement_Date__c = alstOperatingLoc[0].Lease__r.Start_date__c;
        newSR.Position__c = stepName;
        newSR.Email__c = clientEmail;
        
        // Setting the business hours 
        if(BHId!=null && newSR.SR_Template__c!=null && srTemplate.Estimated_Hours__c!=null){
            
            Long sla = srTemplate.Estimated_Hours__c.longvalue();
            sla=sla*60*60*1000L;
            datetime CreatedTime = system.now(); 
            newSR.Due_Date__c= BusinessHours.add(BHId,CreatedTime,sla);
        }
        update newSR;
        
    }
}