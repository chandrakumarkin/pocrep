/*********************************************************************************************************************
*  Name      : Batch_UpdateAccountAge 
*  Author    : Shikha Khanuja
*  Purpose   : Batch class to update account age
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
v1.0		 	Shikha				08/03/2021				 Ticket #9563 - to update account age
************************************************************************************************************************/
public class Batch_UpdateAccountAge implements Schedulable,Database.Batchable<sObject>{
	 public Database.QueryLocator start (Database.BatchableContext bc){   
        String vcValue = 'Venture Capital New Regime';
        String fundValue = 'Fund Manager New Regime';
        String rocStat = 'Active';
     	String q = 'SELECT Id, Account_Age__c, Account_Age_No__c FROM Account WHERE (FinTech_Classification__c = '+'\''+vcValue+'\''+' OR FinTech_Classification__c = '+'\''+fundValue+'\''+') AND ROC_Status__c = '+'\''+rocStat+'\'';
        system.debug('### q '+q);
        return Database.getQueryLocator(q);
     }
    
    public void execute(Database.BatchableContext bc, List<Account> accList){
        for(Account a : accList){
            a.Account_Age_No__c = a.Account_Age__c;
        }
        system.debug('### accList '+accList.size());
        update accList;
    }
    
    public void finish (Database.BatchableContext bc){
        system.debug('### account field is updated');
    }
    
    public void execute(SchedulableContext ctx){
        Database.executeBatch(new Batch_UpdateAccountAge());
    }
}