/*************************************************************************************************
*  Name        : ObjectionReduceFine
*  Author      : Sai Kalyan
*  Company     : 
*  Date        : 2020-04-08     
*  Purpose     : This class used to reduce fine Amount 
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date            Updated By      Description
V1.0    08/APR/2020      Sai            Initial version
v1.1   30/06/2021        Utkarsh        Updated as per 11834
------------------------------------------------- --------------------------------------------------------------------         
**************************************************************************************************/

public class ObjectionReduceFine {
    
    public static void reduceFineAmount(Id stepID){
        
        try{
            Step__c eachStep = new Step__c();
            Set<String> setContactEmail = new Set<String>();
            eachStep = [SELECT Id,SR__c,SR__r.Linked_SR__c,Reduce__c,Old_Fine_Amount__c,Applicant_Email__c,SR__r.Linked_SR__r.FIne_Amount_Reduce_Date__c FROM Step__c where Id=:stepID AND SR_Record_Type__c='Objection_SR'];
            SR_Price_Item__c priceItem = new SR_Price_Item__c();
            List<SR_Price_Item__c> ListpriceItem = new List<SR_Price_Item__c>();
            system.debug('!!@##$#'+eachStep.SR__r.Linked_SR__c);
            ListpriceItem = [SELECT Id,Price_in_USD__c,Non_Reevaluate__c,ServiceRequest__c,ServiceRequest__r.Customer__c,Price__c FROM SR_Price_Item__c where ServiceRequest__c=:eachStep.SR__r.Linked_SR__c];
            
            if(!ListpriceItem.isEmpty() && ListpriceItem[0].Price__c != null){
                eachStep.Old_Fine_Amount__c = ListpriceItem[0].Price__c;
            
            if(eachStep.Reduce__c != null){
                ListpriceItem[0].Price__c = eachStep.Reduce__c;
            }
            
            for(Integer i = 1; i < ListpriceItem.size(); i++)
                ListpriceItem[i].Price__c = 0;
            }

        
            UPDATE ListpriceItem;
            
            UPDATE eachStep;
            system.debug('!!@@##'+eachStep);
            
            Service_Request__c eachRequest = new Service_Request__c();
            eachRequest = [SELECT FIne_Amount_Reduce_Date__c FROM Service_Request__c where ID=:eachStep.SR__r.Linked_SR__c];
            eachRequest.FIne_Amount_Reduce_Date__c = system.today();
            UPDATE eachRequest;
            
            EmailTemplate et=[Select id from EmailTemplate where name = 'Notice of objection update-Reduce' limit 1];
            OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
            if(lstOrgEA!=null && lstOrgEA.size()>0){
                mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
            }
            mail.setTargetObjectId(Label.Test_Contact_ID_For_Email);
            mail.setTreatTargetObjectAsRecipient(false);
            mail.setWhatId(eachStep.Id);
            mail.setTemplateId(et.id);
            mail.setToAddresses(new List<String>{eachStep.Applicant_Email__c});
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
            
        }
        catch(exception ex){
            
        }
    }
}