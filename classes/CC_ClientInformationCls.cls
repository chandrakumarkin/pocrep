/**
 * @description       : 
 * @author            : shoaib tariq
 * @group             : 
 * @last modified on  : 04-27-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   04-21-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public without sharing class CC_ClientInformationCls {
    
    /**
     * Stores the number of contacts a user
     * is required to add in the form
     */
    private static map<string,Integer> contactLimits           = new map<string,Integer>();
    
    /**
     * Stores the number of contacts a user
     * can optionally add
     */     
    private static map<String,Integer> contactAdditionalCount  = new map<string,Integer>();
    
    /**
     * Tells if a relationship type
     * has already reached the maximum 
     * number of required contacts
     */
    private static map<String,Boolean> contactRequiredMap      = new map<String,Boolean>();
    
    /**
     * Tells if a relationship type
     * has already reached the minumum 
     * number of required contacts
     */         
    private static map<String,Boolean> contactMinAchieved      = new map<String,Boolean>();
    
    /**
     * Tells if a relationship type
     * has already reached the maximum 
     * number of optional contacts
     */             
    private static map<String,Boolean> contactAddlCountMap     = new map<String,Boolean>();
    
    /**
     * Checks if the Client contact form has met the required
     * number of contacts
     * @params      srId        The Service Request record ID
     * @params      message     The validation message
     */
    public static String checkContactLimit(String srId){
        
        setContactLimit();
        
        setRequiredContacts();
        
        setAdditionalContactLimits();
        
        populateMaps(srId);
        
        return validateRequiredContacts() ? 'Success' : getMissingContactTypes();
    }
    
    /**
     * Sets the client contact form map
     * collections for validation
     * @params      srId        The Service Request record ID
     */
        @testVisible
    private static void populateMaps(String srId){
        
        Map<String,List<Amendment__c>> mapAmendments = new Map<String,List<Amendment__c>>();
        
        for(Amendment__c amnd:getCurrentAmendments(srId)){
                
            list<Amendment__c> lstAmnds = new list<Amendment__c>();
            
            amnd.Amendment_Type__c = 'Individual';
            
            if(mapAmendments.get(amnd.Relationship_Type__c)!=null) lstAmnds = mapAmendments.get(amnd.Relationship_Type__c);
                
            lstAmnds.add(amnd);
            
            mapAmendments.put(amnd.Relationship_Type__c,lstAmnds);
            
            Integer contactListSize = lstAmnds.size(); 
            
            if(contactAdditionalCount.containsKey(amnd.Relationship_Type__c)){
                
                contactAddlCountMap.put(amnd.Relationship_Type__c,lstAmnds.size() - contactAdditionalCount.get(amnd.Relationship_Type__c) == contactAdditionalCount.get(amnd.Relationship_Type__c) && !lstAmnds.isEmpty());
                
                if(lstAmnds.size() > contactLimits.get(amnd.Relationship_Type__c)) contactListSize = lstAmnds.size() - contactAdditionalCount.get(amnd.Relationship_Type__c);   
            }
            
            contactMinAchieved.put(amnd.Relationship_Type__c,contactListSize >= contactLimits.get(amnd.Relationship_Type__c) && !lstAmnds.isEmpty());
            
        }
        
    }
    
    /**
     * Displays any relationship type
     * that is missing in the form
     */
        @testVisible
    private static String getMissingContactTypes(){
        
        String missingDetails = '';
        
        for(String k : contactRequiredMap.keySet()) 
            if( contactRequiredMap.get(k) && (!contactMinAchieved.containsKey(k) || !contactMinAchieved.get(k)) ) missingDetails += k+ ', ';
        
        missingDetails = missingDetails.substringbeforeLast(',');
        
        return 'Please provide the following required Client Information details to proceed: ' + missingDetails;
        
    }
    
    /**
     * Checks if all required contacts
     * have been added
     * @return          isValid         Tells if the form can proceed
     */
        @testVisible
    private static Boolean validateRequiredContacts()
    {
        /*
         integer I=0;
            string Typeval='';
        
        //for(ContactWrapper ObjContactWrapper:lstAmendments)
             for(Amendment__c amnd:getCurrentAmendments(srId))
            {
                if(amnd.Relationship_Type__c!=Typeval)
                {
                    I++ ;
                    Typeval=ObjContactWrapper.contactRecord.Relationship_Type__c;
                }   
                
            }
        
         if(i>1)
            return true;
            else
            return false;
        */
        
        for(String k : contactRequiredMap.keySet()) if( contactRequiredMap.get(k) && (!contactMinAchieved.containsKey(k) || !contactMinAchieved.get(k)) ) return false;
        
        return true;
        
    }
    
    /**
     * Gets the contact amendments in the client contact form
     */
    public static List<Amendment__c> getCurrentAmendments(String srId){
        
        return [Select id,Customer__c,Status__c,Name_of_Declaration_Signatory__c,Name,Nationality__c,Title_new__c,Family_Name__c,Given_Name__c,Date_of_Birth__c,Place_of_Birth__c,Passport_No__c,Nationality_list__c,Phone__c,Non_Recognized_Auditors__c,
            Person_Email__c,Occupation__c,Sys_Secondary__c,Capacity__c,Other_Directorships__c,Date_of_Appointment_GP__c,Name_of_incorporating_Shareholder__c,Name_of_Body_Corporate_Shareholder__c,Recognized_Auditors__c,Mobile__c,Job_Title__c,
            Shareholder_enjoy_direct_benefits__c,First_Name__c,First_Name_Arabic__c,Representation_Authority__c,Type_of_Authority_Other__c,Amount_of_Contribution_US__c,Contribution_Type__c,DIFC_Recognized_Auditor__c,
            Contribution_Type_Other__c,Percentage_held__c,Class_of_Units_Other_Rights__c,Apt_or_Villa_No__c,Building_Name__c,Street__c,PO_Box__c,Auditors__c,sys_create__c,Last_Name_Arabic__c,Middle_Name_Arabic__c,
            Permanent_Native_City__c,Emirate_State__c,Post_Code__c,Address3__c,Permanent_Native_Country__c,Class__c,New_No_of_Shares__c,Total_Nominal_Value__c,Relationship_Type__c,Is_Designated_Member__c,Data_Center_Access_ID__c,
            Company_Name__c,Former_Name__c,Registration_Date__c,Gender__c,Date_of_Appointment_SP__c,Date_of_Appointment_Auditor__c,Emirate__c,Amendment_Type__c,Place_of_Registration__c,Financial_Year_End__c
            from Amendment__c where ServiceRequest__c=:srId and Relationship_Type__c!=null and (Status__c='Active' or Status__c='Draft') AND Related__c = ''];
        
    }
    
    /**
     * Sets the number of required
     * contacts per relationship type
     */
        @testVisible
    private static void setContactLimit(){
        
        contactLimits.put('Senior Management',1);
        contactLimits.put('General Communication',1);
        
        
        /*
        contactLimits.put('CEO',1);
        contactLimits.put('Personal Assistant',1);
        contactLimits.put('Senior Executive Officer (SEO)',1);
        contactLimits.put('Office Manager',1);
        contactLimits.put('Corporate Communication',1);
        contactLimits.put('IT contact',1);
        */
        
    }
    
    /**
     * Sets the required types in the form
     */
        @testVisible
    private static void setRequiredContacts(){
        
            contactRequiredMap.put('Senior Management',true);
        contactRequiredMap.put('General Communication',true);
        
        /*
        contactRequiredMap.put('CEO',true);
        contactRequiredMap.put('Personal Assistant',true);
        contactRequiredMap.put('Senior Executive Officer (SEO)',false);
        contactRequiredMap.put('Office Manager',false);
        contactRequiredMap.put('Corporate Communication',true);
        contactRequiredMap.put('IT contact',true);
        */
        
    }
    
    /**
     * Sets the number of optional
     * contacts per relationship type
     */
        @testVisible
    private static void setAdditionalContactLimits(){
        
        contactAdditionalCount.put('Corporate Communication',1);
        contactAdditionalCount.put('IT contact',1);
    }
    
    /**
     * Creates and/or updates any contact
     * referenced in the Client Information
     * form, and creates and/or deactivates 
     * relationships for the associated contacts.
     * @params      stp         The step record
     * @return      strResult   The result of the transaction
     */
    public static string ReplicateClientInfoRelationships(Step__c stp){
        
        string strResult = 'Success';
        
        try{
        
        
        /*
        
            
            map<string,string> map_Amnd_Rel_SAPRel = new map<string,string>{'CEO'=>'Has CEO/Regional Head/Chairman',
            'Personal Assistant'=>'Has Personal Assistant',
            'Senior Executive Officer (SEO)'=>'Has Senior Executive Officer (SEO)',
            'Office Manager'=>'Has Office Manager',
            'Corporate Communication'=>'Has Corporate Communication',
            'IT contact'=>'Has IT contact','IT Data Center contact'=>'Has IT Data Center contact',
            'General Communication'=>'Has General Communication'};
            
            
            map<string,string> map_SAPRel_AmndRel = new map<string,string>{'Has CEO/Regional Head/Chairman'=>'CEO',
            'Has Personal Assistant'=>'Personal Assistant','Has Senior Executive Officer (SEO)'=>'Senior Executive Officer (SEO)','Has Office Manager'=>'Office Manager','Has Corporate Communication'=>'Corporate Communication','Has IT contact'=>'IT contact','Has IT Data Center contact'=>'IT Data Center contact','Has General Communication'=>'General Communication'};
            
            
            */
            
            
             /* Relationship Map from Amendment to Relationship */
             
            map<string,string> map_Amnd_Rel_SAPRel = new map<string,string>{
            'Senior Management'=>'Has CEO/Regional Head/Chairman',
            'General Communication'=>'Has Corporate Communication','Emergency contact'=>'Emergency contact'};
            
            
            /* Relationship Map from Relationship to Amendment */
            
           map<string,string> map_SAPRel_AmndRel = new map<string,string>{'Has CEO/Regional Head/Chairman'=>'Senior Management','Has Corporate Communication'=>'General Communication','Emergency contact'=>'Emergency contact'};
            
            
            
            
                /* Stores the existing relationship types assigned to a particular user */
            map<string,Set<String>> mapRelationships_Old = new map<string,Set<String>>();
            
            /* Stores the existing active relationships assigned to a particular user */
            map<string,List<Relationship__c>> mapRelationships_OldRecs = new map<string,list<Relationship__c>>();
    
            /*
             * Get the existing relationships
             */
            for(Relationship__c rel:[select id,Active__c,Appointment_Date__c,Sys_Secondary__c,Object_Account__c,Object_Contact__c,Object_Contact__r.Passport_No__c,Relationship_Type__c,Subject_Account__c,Subject_Contact__c,Start_Date__c from Relationship__c where Subject_Account__c=:stp.SR__r.Customer__c and Active__c=true and Relationship_Type__c IN:map_SAPRel_AmndRel.keyset()]){
            
                List<Relationship__c> existingRelRecs = mapRelationships_OldRecs.containsKey(rel.Object_Contact__c) ? mapRelationships_OldRecs.get(rel.Object_contact__c) : new List<Relationship__c>();
            
                Set<String> existingRels = mapRelationships_Old.containsKey(rel.Object_Contact__c) ? mapRelationships_Old.get(rel.Object_contact__c) : new Set<String>();
                
                existingRels.add(map_SAPRel_AmndRel.get(rel.Relationship_Type__c));
                existingRelRecs.add(rel);
            
                mapRelationships_Old.put(rel.Object_Contact__c,existingRels);        // Store the existing relationship type of each contact
                mapRelationships_OldRecs.put(rel.Object_Contact__c,existingRelRecs); // Store the existing relationships of each contact
            }
    
            /* Stores any existing contact in the form */
            Set<String> contactIds = new Set<String>();
            
            /* Stores the list of contacts to created/updated */
            List<Contact> lstConUpsert = new List<Contact>();
    
            /* Stores the relationships that need to be created/deactivated */
            List<Relationship__c> lstRelUpsert = new List<Relationship__c>();
            
            /* Maps each amendment with their associated contact */
            Map<string,Amendment__c> mapOfNewContacts = new Map<String,Amendment__c>();
            
            /* Stores the assigned roles per contact */
            Map<String,Set<String>> mapOfContactRels = new Map<String,Set<String>>();
            
            List<Amendment__c> objAmnds = [select id,Passport_Expiry_Date__c,Gender__c,Office_Phone_Number__c,Date_of_Birth__c,Title_new__c,(SELECT Id FROM Amendments__r),Relationship_Type__c,Sys_Secondary__c,Status__c,Contact__c,Family_Name__c,Passport_No__c,Nationality_list__c,Given_Name__c,Job_Title__c,Data_Center_Access_ID__c,Phone__c,Person_Email__c,Mobile__c from Amendment__c where Relationship_Type__c!=null and ServiceRequest__c=:stp.SR__c and (Status__c='Draft' or Status__c='Active' or Status__c='Removed')];
            
            for(Amendment__c objAmnd:objAmnds){
                
                if(String.isNotBlank(objAmnd.Family_Name__c) && String.isNotBlank(objAmnd.Given_Name__c) && String.isNotBlank(objAmnd.Passport_No__c) && String.isNotBlank(objAmnd.Nationality_list__c)){
                    
                    /* Group all the relationships for each contact to accommodate multiple roles */
                    Set<String> relationshipTypes = mapOfContactRels.containsKey(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase()) ? 
                                                            mapOfContactRels.get(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase()) : new Set<String>();
                    
                    if(!objAmnd.status__c.equals('Removed')){
                                                            
                        relationshipTypes.add(objAmnd.Relationship_Type__c);
                        
                        /* 
                         * In case a contact is assigned to multiple roles,
                         * provided that the details are the same in all 
                         * iterations, then only one of the amendments will be
                         * added. In case the said contact already exists,
                         * it will replace the instance already stored, combined
                         * with the contact ID
                         */
                         
                        if(!mapOfNewContacts.containsKey(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase()) ||

                            (String.isBlank(mapOfNewContacts.get(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase()).Contact__c) && 
                                String.isNotBlank(objAmnd.Contact__c) &&
                                mapOfNewContacts.get(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase()).Amendments__r.isEmpty() &&
                                !objAmnd.Amendments__r.isEmpty())){
                                
                            mapOfNewContacts.put(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase(),objAmnd);   
                        }   
                    
                    } 
                    
                    mapOfContactRels.put(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase(),relationshipTypes);
                }
                
            }

            for(Amendment__c objAmnd:objAmnds){
                if( !mapOfNewContacts.isEmpty()
                   && objAmnd.Passport_No__c != null
                   && objAmnd.Nationality_list__c != null
                   && mapOfNewContacts.containsKey(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase())
                   && mapOfNewContacts.get(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase()).Contact__c == null
                   && String.IsNotBlank(objAmnd.Contact__c)){
                                   
                     mapOfNewContacts.put(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase(),objAmnd);
                }
            }
            
            /* Stores the list of relationship IDs that will be deactivated */
            Set<String> setOfDeletedRels = new Set<String>();
            
            for(Amendment__c objAmnd:objAmnds){
                
                if(String.isNotBlank(objAmnd.Contact__c) && mapRelationships_OldRecs.containsKey(objAmnd.Contact__c)){
    
                    /*
                     * Group all the deactivated amendments
                     */
                    for(Relationship__c objRelToDisable : mapRelationships_OldRecs.get(objAmnd.Contact__c)){
                        
                        /* We only need to check the types that was removed. */
                        if(String.isNotBlank(objRelToDisable.ID) && !setOfDeletedRels.contains(objRelToDisable.Id) && objAmnd.status__c.equals('Removed') && 
                            objRelToDisable.Relationship_Type__c.equals(map_Amnd_Rel_SAPRel.get(objAmnd.Relationship_Type__c))){
                            
                            objRelToDisable.Active__c = false;
                            objRelToDisable.End_Date__c = system.today();
        
                            lstRelUpsert.add(objRelToDisable);
                            
                            setOfDeletedRels.add(objRelToDisable.Id);
                            
                        }
                        
                    } 
                } 
                
            }
            
            if(lstConUpsert!=null && lstRelUpsert!=null){
            
                string RecordId = [select Id,Name from RecordType where DeveloperName='Individual' AND SobjectType='Contact' AND IsActive=true].Id;
            
                Contact objCon;
            
                /*
                 * Only one amendment will be evaluated 
                 * using the PASSPORT+'_'+NATIONALITY 
                 * combination. In case all instances 
                 * of the contact is new, a new contact
                 * will be created.
                 */
                 
                Map<String,String> jobFunction = new Map<String,String>();
                 
                for(Amendment__c objAmnd : mapOfNewContacts.values()){
                    
                    objCon = new Contact();
                    
                    objCon.Country__c = objAmnd.Nationality_list__c;
                    objCon.Passport_No__c = objAmnd.Passport_No__c;
                    objCon.Nationality__c = objAmnd.Nationality_list__c;
                    objCon.Office_Telephone_no__c = objAmnd.Office_Phone_Number__c;  // 7653
                    
                    objCon.Title = objAmnd.Title_new__c;
                    objCon.Gender__c = objAmnd.Gender__c;
                    
                    //V1.1 New company law
                    if(objAmnd.Title_new__c!=null) objCon.Salutation = objAmnd.Title_new__c;
                    if(objAmnd.Passport_Expiry_Date__c!=null) objCon.Passport_Expiry_Date__c = objAmnd.Passport_Expiry_Date__c;
                    if(objAmnd.Date_of_Birth__c!=null) objCon.Birthdate = objAmnd.Date_of_Birth__c;
                    
                    
    
                    if(objAmnd.Given_Name__c!=null) objCon.FirstName = objAmnd.Given_Name__c;
                        
                    if(objAmnd.Family_Name__c!=null) objCon.LastName = objAmnd.Family_Name__c; 
                        
                    if(objAmnd.Phone__c!=null){
                        
                        objCon.Work_Phone__c = objAmnd.Phone__c;
                        objCon.Phone = objAmnd.Phone__c;
                    }
                    
                    if(objAmnd.Mobile__c!=null) objCon.MobilePhone = objAmnd.Mobile__c;
                        
                    if(objAmnd.Person_Email__c!=null) objCon.Email = objAmnd.Person_Email__c;
                        
                    objCon.Sys_Relationship_Type__c = objAmnd.Passport_No__c.toLowerCase()+'_'+ String.join(new List<String>(mapOfContactRels.get(objAmnd.Passport_No__c.toLowerCase()+'_'+objAmnd.Nationality_list__c.toLowerCase())),';');

                    objCon.RecordTypeId = RecordId;
                    jobFunction.put(objAmnd.Passport_No__c.toLowerCase(),objAmnd.Job_Title__c);
                    if(String.isNotBlank(objAmnd.Contact__c) && !contactIds.contains(objAmnd.Contact__c)) objCon.Id = objAmnd.Contact__c;
                    
                    lstConUpsert.add(objCon);
                    
                    if(String.isNotBlank(objAmnd.Contact__c)) contactIds.add(objAmnd.Contact__c);
                    
                }
                
                upsert lstConUpsert;
                
                for(Contact c : lstConUpsert){
                    
                    if(mapOfContactRels.containsKey(c.Passport_No__c.toLowerCase()+'_'+c.Nationality__c.toLowerCase())){
                        
                        /*
                         * Loop through the new contacts
                         */
                        for(String relType : mapOfContactRels.get(c.Passport_No__c.toLowerCase()+'_'+c.Nationality__c.toLowerCase())){
                        
                            if(!mapRelationships_Old.containsKey(c.Id)){
                            
                                lstRelUpsert.add(getNewRelationship(stp.SR__r.Customer__c,c.Id,map_Amnd_Rel_SAPRel.get(relType),mapOfNewContacts.get(c.Passport_No__c.toLowerCase()+'_'+c.Nationality__c.toLowerCase()).Id,jobFunction.get(c.Passport_No__c.toLowerCase())));
                                
                            }
                            
                        }
                    
                        /*
                         * Loop through the existing contacts, 
                         * and check if new roles have been 
                         * assigned to the contact
                         */
                        if(mapRelationships_Old.containsKey(c.Id) && mapOfContactRels.containsKey(c.Passport_No__c.toLowerCase()+'_'+c.Nationality__c.toLowerCase())){
                            
                            for(String relType : mapOfContactRels.get(c.Passport_No__c.toLowerCase()+'_'+c.Nationality__c.toLowerCase())){
                                
                                if(!mapRelationships_Old.get(c.Id).contains(relType)){
                                    
                                    lstRelUpsert.add(getNewRelationship(stp.SR__r.Customer__c,c.Id,map_Amnd_Rel_SAPRel.get(relType),mapOfNewContacts.get(c.Passport_No__c.toLowerCase()+'_'+c.Nationality__c.toLowerCase()).Id,jobFunction.get(c.Passport_No__c.toLowerCase())));
                                    
                                }
                                
                            }
                            
                        }

                        
                    }
                    
                    
                }
                
                upsert lstRelUpsert;
                
                //New company law 
                //Account ObjAccount=[select Contact_Details_Provided__c,id from account where id=:stp.SR__r.Customer__c];
                //ObjAccount.Contact_Details_Provided__c=true;
                //update ObjAccount;
                //System.assertEquals(false,true);
            }
        
        }catch(DMLException e){
            strResult = e.getdmlMessage(0)+'';
            
            insert LogDetails.CreateLog(null,'Client Information Custom Code','Exception Line Number : '+e.getLineNumber()+'\nException is : '+e.getMessage()); 
                
        }
        
        return strResult;
    
    }
    
    /**
     * Returns a new relationship instance
     * @params      customerId          The customer record ID referenced in the form
     * @params      contactId           The associated contact record ID
     * @params      relationshipType    The type of relationship
     * @params      amendmentId         The amendment record ID the contact is associated with
     * @return      newRelationship     A relationship instance
     */
    private static Relationship__c getNewRelationship(String customerId, String contactId, String relationshipType, String amendmentId,String JobFunction){
        
        return new Relationship__c(
            Active__c = true,
            Subject_Account__c = customerId,
            Sys_Amendment_Id__c = amendmentId,
            Object_Contact__c = contactId,
            Start_Date__c = system.today(),
            Relationship_Type__c = relationshipType,
            Function__c = JobFunction
        );
        
    }
}