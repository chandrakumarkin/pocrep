@isTest
public class CC_PriceItemsCreateConditions_Test{

     static testMethod void myUnitTest(){
     
      Account objAccount = new Account();
    objAccount.Name = 'Test Account';
    objAccount.Phone = '1234567890';
    insert objAccount;
    
     string conRT;
    for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
               conRT = objRT.Id;
    Contact objContact = new Contact();
    objContact.LastName = 'Test Contact';objContact.FirstName = 'Test Contact';
    objContact.AccountId = objAccount.Id;
    objContact.Email = 'test@difc.com';
    objContact.Phone = '1234567890';
    objContact.RecordTypeId = conRT;
    insert objContact;
     
   SR_Status__c objSRStatus = new SR_Status__c();
    objSRStatus.Name = 'Draft';
    objSRStatus.Code__c = 'DRAFT';
    insert objSRStatus;
    
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_Amendment';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Amendment';
        objTemplate.Active__c = true;
        objTemplate.SR_Group__c='GS';
        insert objTemplate;
     
     
         Service_Request__c objSR = new Service_Request__c();
    objSR.Customer__c = objAccount.Id;
    objSR.Email__c = 'testsr@difc.com';
    objSR.Internal_SR_Status__c = objSRStatus.Id; // changing the status to Draft
    objSR.External_SR_Status__c = objSRStatus.Id; // changing the status to Draft
    objSR.Contact__c = objContact.Id;
    objSR.SR_Template__c = objTemplate.Id;
    objSR.Type_of_Amendment__c ='Transfer to New Passport (Renewal)';
    insert objSR;
    
    
     
       CC_PriceItemsCreateConditions c = new CC_PriceItemsCreateConditions();
       CC_PriceItemsCreateConditions.Check_Condition('checkVisaProfessionChange',objSR.id);
       
     
     }
     
      static testMethod void myUnitTest1(){
     
      Account objAccount = new Account();
    objAccount.Name = 'Test Account';
    objAccount.Phone = '1234567890';
    insert objAccount;
    
     string conRT;
    for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
               conRT = objRT.Id;
    Contact objContact = new Contact();
    objContact.LastName = 'Test Contact';objContact.FirstName = 'Test Contact';
    objContact.AccountId = objAccount.Id;
    objContact.Email = 'test@difc.com';
    objContact.Phone = '1234567890';
    objContact.RecordTypeId = conRT;
    insert objContact;
     
   SR_Status__c objSRStatus = new SR_Status__c();
    objSRStatus.Name = 'Draft';
    objSRStatus.Code__c = 'DRAFT';
    insert objSRStatus;
    
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_Amendment';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Amendment';
        objTemplate.Active__c = true;
        objTemplate.SR_Group__c='GS';
        insert objTemplate;
     
     
         Service_Request__c objSR = new Service_Request__c();
    objSR.Customer__c = objAccount.Id;
    objSR.Email__c = 'testsr@difc.com';
    objSR.Internal_SR_Status__c = objSRStatus.Id; // changing the status to Draft
    objSR.External_SR_Status__c = objSRStatus.Id; // changing the status to Draft
    objSR.Contact__c = objContact.Id;
    objSR.SR_Template__c = objTemplate.Id;
    objSR.Type_of_Amendment__c ='Transfer to New Passport (Renewal);Marital Status';
    insert objSR;
    
    
     
       CC_PriceItemsCreateConditions c = new CC_PriceItemsCreateConditions();
       CC_PriceItemsCreateConditions.Check_Condition('checkVisaProfessionChange',objSR.id);
       
     
     }
}