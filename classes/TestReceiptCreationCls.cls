/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
    Created By : Ravi on 2nd Dec
    Des : This class is mock test for the Receipt Services 
*/
@isTest(seeAllData=false)
global class TestReceiptCreationCls implements WebServiceMock {

    public static list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>(); 
	public static list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
	
    global void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
        //docSample.EchoStringResponse_element respElement = new docSample.EchoStringResponse_element();
        SAPECCWebService.Z_SF_RECEIPT_SERVICEResponse_element respElement = new SAPECCWebService.Z_SF_RECEIPT_SERVICEResponse_element();
        SAPECCWebService.ZSF_TT_RECE_SERV_OP EX_SF_RS = new SAPECCWebService.ZSF_TT_RECE_SERV_OP();
        SAPECCWebService.ZSF_TT_ACC_BAL objActBal = new SAPECCWebService.ZSF_TT_ACC_BAL();
        
        EX_SF_RS.item = resItems;
        objActBal.item = resActBals;
        respElement.EX_SF_RS = EX_SF_RS;
        respElement.EX_SF_RS_ACC = objActBal;
        response.put('response_x',respElement);
        //respElement.EchoStringResult = 'Mock response';
        //response.put('response_x', respElement); 
   }
}