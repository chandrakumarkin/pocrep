/******************************************************************************************
 *  Author   	: Claude Manahan
 *  Date     	: 24-05-2017
 *  Description	: Resets the date of notification of each uncoverted lead
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    24-05-2017  Claude         Created
****************************************************************************************************************/
public with sharing class CRM_cls_ScheduledLeadUpdate implements Schedulable, Database.Batchable<sObject>  {
	
	private list<lead> leadRecords;
	
	public CRM_cls_ScheduledLeadUpdate(){
	
		leadRecords = CRM_cls_LeadConversionOverride.getUncovertedLeads();
	
	}
  		
  	public list<Lead> start(Database.BatchableContext BC){
  		
  		return leadRecords;
			
    }
    	
	public void execute(Database.BatchableContext BC, list<Lead> leadRecords){
		
		CRM_cls_LeadConversionOverride.resetNotificationDates(leadRecords);
	}
	
	public void execute(SchedulableContext ctx) {
     
        database.executeBatch(this,200);
        
    }
    
    public void finish(Database.BatchableContext BC){
    
    	System.debug('Finished Processing Leads');
    	
    }
	
}