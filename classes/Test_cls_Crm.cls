/******************************************************************************************
 *  Name        : Test_cls_Crm 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2016-06-19
 *  Description : Main test class for CRM Enhancements
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   30-05-2016   Claude        Created
 V1.1   15-12-2016   Claude        Added test methods for other CRM-related modules
 V1.2   05-03-2017   Claude        Added new test method for Opportunity Stage History (#3935)
*******************************************************************************************/
@isTest(seeAllData=false)
public class Test_cls_Crm{
    
    /**
     * Creates Custom setting records to be used
     * for the entire test class
     */
    @testsetup static void setupData(){
        
        Test_cls_Crm_Utils.prepareSetup();
    }
    
    /**
     * Tests the Lead Conversion process
     * for CLS_CreateBP class
     */
    
    
    /**
     * Tests the affiliations count
     */
    static testMethod void testRelationshipCount(){
        
        List<Account> testAccounts = new List<Account>();
        
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
        /* Create 2 Main Accounts */
        Account objAccount = new Account();
        Account objAccount2 = new Account();
        
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        objAccount.ROC_Status__c = 'Account Created';
        objAccount2.Name = 'Test Account 2';
        objAccount2.Phone = '1234567890';
         objAccount2.ROC_Status__c = 'Account Created';
        /* Create 2 separate accounts, and 1 joined account */
        Account objAccount3 = new Account();
        objAccount3.Name = 'Test Account 3';
        objAccount3.Phone = '1234567890';
         objAccount3.ROC_Status__c = 'Account Created';
        Account objAccount4 = new Account();
        objAccount4.Name = 'Test Account 4';
        objAccount4.Phone = '1234567890';
         objAccount4.ROC_Status__c = 'Account Created';
        Account objAccountJoined = new Account();
        objAccountJoined.Name = 'Test Account Joined';
        objAccountJoined.Phone = '1234567890';
         objAccountJoined.ROC_Status__c = 'Account Created';
        /* Create 2 children */
        Account objAccountChild1 = new Account();
        objAccountChild1.Name = 'Test Account Child 1';
        objAccountChild1.Phone = '1234567890';
        objAccountChild1.ROC_Status__c = 'Account Created';
        Account objAccountChild2 = new Account();
        objAccountChild2.Name = 'Test Account Child 2';
        objAccountChild2.Phone = '1234567890';
        objAccountChild2.ROC_Status__c = 'Account Created';
        testAccounts.add(objAccount);
        testAccounts.add(objAccount2);
        testAccounts.add(objAccount3);
        testAccounts.add(objAccount4);
        testAccounts.add(objAccountJoined);
        testAccounts.add(objAccountChild1);
        testAccounts.add(objAccountChild2);
        
        insert testAccounts; // create the accounts
        
        List<String> accIds = new List<String>();
        
        for(Account a : testAccounts){
            accIds.add(a.Id);
            System.debug(a.Name + ' >>> ' + a.Id);
        }
        
        /* create a list of relationships */
        List<Relationship__c> testRelationships = new List<Relationship__c>();
        
        /* Create the relationships for the first account */
        Relationship__c testRel = new Relationship__c();
        testRel.Subject_Account__c = objAccount.Id;
        testRel.Object_Account__c = objAccount3.Id;
        testRel.Active__c = true;
        testRel.Relationship_Type__c = 'Has Affiliates with Same Management';
        testRel.Push_to_SAP__c = true;
        
        testRelationships.add(testRel);
        
        /* Create the relationships for the second account */
        Relationship__c testRel3 = new Relationship__c();
        testRel3.Subject_Account__c = objAccount2.Id;
        testRel3.Object_Account__c = objAccount4.Id;
        testRel3.Active__c = true;
        testRel3.Relationship_Type__c = 'Has Affiliates with Same Management';
        testRel3.Push_to_SAP__c = true;
        
        testRelationships.add(testRel3);
        
        /* Create the child relationship */
        Relationship__c testRel2 = new Relationship__c();
        testRel2.Subject_Account__c = objAccount3.Id;
        testRel2.Object_Account__c = objAccountChild1.Id;
        testRel2.Active__c = true;
        testRel2.Relationship_Type__c = 'Has Affiliates with Same Management';
        testRel2.Push_to_SAP__c = true;
        
        testRelationships.add(testRel2);
        
        /* Create the second child relationship */
        Relationship__c testRel4 = new Relationship__c();
        testRel4.Subject_Account__c = objAccount4.Id;
        testRel4.Object_Account__c = objAccountChild2.Id;
        testRel4.Active__c = true;
        testRel4.Relationship_Type__c = 'Has Affiliates with Same Management';
        testRel4.Push_to_SAP__c = true;
        
        testRelationships.add(testRel4);
        
        /* Create the joined relationships */
        Relationship__c testRelJoined = new Relationship__c();
        testRelJoined.Subject_Account__c = objAccount.Id;
        testRelJoined.Object_Account__c = objAccountJoined.Id;
        testRelJoined.Active__c = true;
        testRelJoined.Relationship_Type__c = 'Has Affiliates with Same Management';
        testRelJoined.Push_to_SAP__c = true;
        
        testRelationships.add(testRelJoined);
        
        Relationship__c testRelJoined2 = new Relationship__c();
        testRelJoined2.Subject_Account__c = objAccount2.Id;
        testRelJoined2.Object_Account__c = objAccountJoined.Id;
        testRelJoined2.Active__c = true;
        testRelJoined2.Relationship_Type__c = 'Has Affiliates with Same Management';
        testRelJoined2.Push_to_SAP__c = true;
        
        testRelationships.add(testRelJoined2);
        
        insert testRelationships;
        
        testRelJoined2.Active__c = false;
        
        update testRelJoined2;
        
        /* Deactivate one of the relationships */
        // Assuming the process is not active, we will call the 
        // method directly
        Test.startTest();
        CRM_RelationshipHandler.updateAffiliatedRelationships(accIds);
        Test.stopTest();
    }

    /**
     * Tests the relationship redirect
     * class
     */
    static testMethod void testRelationshipRedirect() {
        
        
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
		
        Account testAccount = Test_cls_Crm_Utils.getTestCrmAccount(Test_cls_Crm_Utils.CRM_DEFAULT_CUSTOMERNAME,Test_cls_Crm_Utils.CRM_DEFAULT_COMPANYTYPE);
        
        insert testAccount;
        
        Opportunity testOpportunity = Test_cls_Crm_Utils.getTestOpportunity(Test_cls_Crm_Utils.CRM_DEFAULT_OPPORTUNITYNAME,testAccount.Id,Test_cls_Crm_Utils.CRM_DEFAULT_STAGE);
        
        insert testOpportunity;
        
        /* Set the URL parameters */
        Apexpages.currentPage().getParameters().put('oppAcctId',testAccount.Id);
        Apexpages.currentPage().getParameters().put('oppAcctName',testAccount.Name);
        Apexpages.currentPage().getParameters().put('oppId',testOpportunity.Id);
        Apexpages.currentPage().getParameters().put('oppName',testOpportunity.Name);
        
        Test.setCurrentPage(Page.CRM_relationRedirect);
        
        Relationship__c testRelationship = new Relationship__c();
        
        ApexPages.StandardController controller = new ApexPages.StandardController(testRelationship); 
        
        CRM_cls_RelationshipRedirect testRelationshipRedirect = new CRM_cls_RelationshipRedirect(controller);
        
        testRelationshipRedirect.redirectToUrl();
    }
    
    /**
     * Tests CRM Contacts getting
     * parented using the Account Trigger
     */
    static testMethod void testCrmContactCreation(){
        
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
        ID ROCrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Individual Contact').getRecordTypeId();
        ID GSrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        ID RORPrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('RORP Contact').getRecordTypeId();        
        ID BDrectypeid = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Business Development').getRecordTypeId(); //Added as per V1.2
        
        List<Contact> testCrmContacts = new List<contact>();
        
        for(Integer ctr = 0; ctr < 4; ctr++){
            Contact testContact = new Contact(FirstName = 'Test First' + ctr, LastName = 'Test LastName' + ctr);
            testCrmContacts.add(testContact);
        }
        
        testCrmContacts[0].RecordTypeId = ROCrectypeid;
        testCrmContacts[1].RecordTypeId = GSrectypeid;
        testCrmContacts[2].RecordTypeId = RORPrectypeid;
        testCrmContacts[3].RecordTypeId = BDrectypeid;
        
        insert testCrmContacts;
    }
    
    /**
     * Tests CRM Relationships
     * with type 'Has the Employee Responsible'
     */
    static testMethod void testCRMRelationshipHandler(){
        
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
        Account testAccount = Test_cls_Crm_Utils.getTestCrmAccount(Test_cls_Crm_Utils.CRM_DEFAULT_CUSTOMERNAME,Test_cls_Crm_Utils.CRM_DEFAULT_COMPANYTYPE);
        
        testAccount.BD_Sector__c = Test_cls_Crm_Utils.CRM_DEFAULT_SECTORCLASSIFICATION;
        testAccount.Roc_Status__c = 'Account created';
        insert testAccount;
        
        CRM_cls_relcreationhandler.CRMRelationship testCrmRelDetails = new CRM_cls_relcreationhandler.CRMRelationship();
        
        testCrmRelDetails.accountId = testAccount.Id;
        testCrmRelDetails.userId = UserInfo.getUserId(); 
        testCrmRelDetails.relationshipType = 'Has the Employee Responsible'; 
        testCrmRelDetails.isActive = true; 
        
        CRM_cls_relcreationhandler.createUpdateRelationships(new List<CRM_cls_relcreationhandler.CRMRelationship>{testCrmRelDetails});
    }
    
    /**
     * Tests the CRM KPI module
     */
    static testMethod void testCRMKPI() {
        
        // TO DO: implement unit test
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
        Account testAccount = Test_cls_Crm_Utils.getTestCrmAccount(Test_cls_Crm_Utils.CRM_DEFAULT_CUSTOMERNAME,Test_cls_Crm_Utils.CRM_DEFAULT_COMPANYTYPE);
        
        testAccount.Priority_to_DIFC__c = 'Standard';
        testAccount.Company_Type__c = 'Financial';
        testAccount.Roc_Status__c = 'Active';
        testAccount.OwnerId = Userinfo.getUserId();

        Account testAccount2 = Test_cls_Crm_Utils.getTestCrmAccount(Test_cls_Crm_Utils.CRM_DEFAULT_CUSTOMERNAME+'Two',Test_cls_Crm_Utils.CRM_DEFAULT_COMPANYTYPE);
        
        testAccount2.BP_No__c = '004321';
        testAccount2.Priority_to_DIFC__c = 'Standard';
        testAccount2.Company_Type__c = 'Non-financial';
        testAccount2.Roc_Status__c = 'Under Formation';
        testAccount2.OwnerId = Userinfo.getUserId();
                
        insert new List<Account>{testAccount,testAccount2};
        
        Contact testContact = Test_CC_FitOutCustomCode_DataFactory.getTestContact(testAccount.Id);
        
        insert testContact;
        
        Relationship__c testAffiliateRel = new Relationship__c();
        
        testAffiliateRel.Subject_Account__c = testAccount.Id;
        testAffiliateRel.Object_Account__c = testAccount2.Id;        
        testAffiliateRel.Active__c = true;
        testAffiliateRel.Relationship_Type__c = 'Has Affiliates with Same Management';
        testAffiliateRel.Push_To_Sap__c = true;
        
        insert testAffiliateRel;         
        
        /* Create KPI Records */
        KPI__c kpiRecord = new KPI__c();
        
        kpiRecord.Related_Account__c = testAccount.Id;
        kpiRecord.year__c = System.today().Year();
        
        insert kpiRecord;
        
        /* Create a sample task */
        List<task> testTasks = new List<Task>();
        
        Task testTask = new Task();
        
        testTask.recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Task' AND name = 'Meeting'].Id;
        testTask.Description = 'Test';
        testTask.Date_Time_From__c = System.now() - 10;
        testTask.Date_Time_To__c = System.now() - 10;
        testTask.priority = 'Normal';
        testTask.subject = 'Test Subject';
        testTask.status = 'Completed';
        testTask.whatId = testAccount.Id;
        testTask.category__c = 'DIFC Mgmt MTG CEO/HQ';
        testTask.OwnerId = Userinfo.getUserId();
        
        testTasks.add(testTask);
        
        Task testTask2 = testTask.clone(false,true,true,true);
        testTask2.category__c = 'MTG RM with CEO';
        testTasks.add(testTask2);
        
        Task testTask3 = testTask.clone(false,true,true,true);
        testTask3.category__c = 'Sector L-Firm CEO/HQ';
        testTask3.WhoId = testContact.Id;
        testTasks.add(testTask3);
        
        insert testTasks;
        
        /* Create the events */
        
        List<Event> testEvents  = new list<Event>();
        
        Event testEvent = new Event(); 
        testEvent.recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Event' AND name = 'Meetings'].Id;
        
        testEvent.Description = 'Test';
        testEvent.StartDateTime = System.now() - 10;
        testEvent.EndDateTime = System.now() - 10;
        testEvent.whatId = testAccount.Id;
        testEvent.category__c = 'DIFC Mgmt MTG CEO/HQ';
        
        testEvents.add(testEvent);
        
        Event testEvent2 = testEvent.clone(false,true,true,true);
        testEvent2.category__c = 'MTG RM with CEO';
        testEvents.add(testEvent2);
        
        Event testEvent3 = testEvent.clone(false,true,true,true);
        testEvent3.category__c = 'Sector L-Firm CEO/HQ';
        testEvents.add(testEvent3);
        
        insert testEvents;
       
        Test.startTest();
        
        CRM_cls_KPI_Utils.createKPIRecords(new List<Id>{testAccount.Id});
        CRM_cls_ActivitiesKPIHandler.createKPIForActivities(new List<Id>{testAccount.Id});
        
        CRM_cls_KPI_Utils.createKPIForPrevYears(System.Today().year()-1);
        
        /* Schedule a job */
        String jobId = System.schedule('testCRM_cls_BatchKPIHandler',Test_cls_Crm_Utils.CRON_EXP, new CRM_cls_BatchKPIHandler());
        
        Test.stopTest();
    }
    
    /**
     * Tests the DFSA Email module
     */
    static testMethod void testCRMDFSAEmail() {
        
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
        Email_Regex__c dfsaDate = new Email_Regex__c(Name = 'DFSA Date',REGEX_Group__c = 'DFSA CRM',Regex_Expression__c= '^.*date of ([^"][^.]*).*$');
        Email_Regex__c dfsaStatus = new Email_Regex__c(Name = 'DFSA Status',REGEX_Group__c = 'DFSA CRM',Regex_Expression__c= '^.*status has now changed to ([^"]*) with.*$');
        Email_Regex__c dfsaAccountRegex = new Email_Regex__c(Name = 'DFSA Account Name',REGEX_Group__c = 'DFSA CRM',Regex_Expression__c= ' ^.*Please note that ([^"]*) status has now changed to.*$');
        
        insert new List<Email_Regex__c>{dfsaAccountRegex,dfsaDate,dfsaStatus};      
        
        List<String> bpNumberList = new List<String>{'001234','001235','001245','002235','002236'};
        List<String> rocStatusList = new List<String>{'Account Created','Application In-Principle','Voluntary Withdrawal','Application Withdrawn'};
        List<String> accountStatuses = new List<String>{'Authorised','Application In-Principle','Voluntary Withdrawal','Application Withdrawn'};
        
        List<String> accountNames = new List<String>();
        
        for(Integer counter = 1; counter < 5; counter++){
            accountNames.add(Test_cls_Crm_Utils.CRM_DEFAULT_CUSTOMERNAME+counter);
        }
        
        Set<String> plainTextBodySet = new Set<String>();
        
        for(Integer counter = 0; counter < accountStatuses.size(); counter++){
            plainTextBodySet.add('Please note that F003216 ' + accountNames[counter] + ' status has now changed to ' + accountStatuses[counter] + ' with an Authorised date of 20/10/2016.');
        }
        
        plainTextBodySet.add('Please note that F003216 Dummy Account status has now changed to Test with an Authorised date of 20/10/2016.');
        
        List<Messaging.InboundEmail> emailInboundList = new List<Messaging.InboundEmail>();
        
        for(String plainTextBody : plainTextBodySet){
            emailInboundList.add(Test_cls_Crm_Utils.getTestInboundEmail(plainTextBody));
        }
        
        Test.startTest();
        
        List<Account> testAccountList = new List<Account>();
        
        for(Integer counter = 0; counter < accountNames.size(); counter++)
        {
            
            Account testAccountRecord = Test_cls_Crm_Utils.getTestCrmAccount(accountNames[counter],Test_cls_Crm_Utils.CRM_DEFAULT_COMPANYTYPE);
            
            testAccountRecord.BP_No__c = bpNumberList[counter];
            testAccountRecord.Roc_Status__c = rocStatusList[counter];
            
            testAccountList.add(testAccountRecord);
            
        }
         //V.2
         Account testAccountRecord = Test_cls_Crm_Utils.getTestCrmAccount('Demo Account 1253','Non - financial');
         testAccountRecord.BP_No__c = bpNumberList[4];
         testAccountRecord.Roc_Status__c = rocStatusList[3];
           testAccountList.add(testAccountRecord);
            
        
        insert testAccountList;
        
        List<Id> accountIds = new List<Id>();
        
        for(Account a : testAccountList){
            accountIds.add(a.Id);
        }
        
        List<Opportunity> testOpportunities = new List<Opportunity>();
        
        for(Account testAccount : testAccountList){
            testOpportunities.add(Test_cls_Crm_Utils.getTestOpportunity(Test_cls_Crm_Utils.CRM_DEFAULT_OPPORTUNITYNAME,testAccount.Id,Test_cls_Crm_Utils.CRM_DEFAULT_STAGE));
        }
        
        insert testOpportunities;
        
        CRM_cls_DFSAEmailHandler edr = new CRM_cls_DFSAEmailHandler();
        
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        
        for(Messaging.InboundEmail email : emailInboundList){
            edr.handleInboundEmail(email, env);
        }
        
        for(Account a : testAccountList){
            a.ROC_Status__c = 'Active';
        }
        
        update testAccountList;
        
        CRM_cls_AccountHandler.processAccountRecords(accountIds);
        
        Set<Id> oppIds = new Set<Id>();
        
        for(Opportunity o : testOpportunities){
            oppIds.add(o.Id);
        }
        
        updateTestStageOpp(oppIds);
        
        updateTestStageHistory(oppIds);
        
        Test.stopTest();
    }
    
    /**
     * V1.2
     * Updates each opportunity to a set
     * of specific stages
     * @params      oppIds          The opportunity record IDs
     */
    @future
    private static void updateTestStageOpp(Set<Id> oppIds){
        
        List<Opportunity> testOpportunities = new List<Opportunity>();
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
        for(Id i : oppIds){  
            testOpportunities.add(new Opportunity(Id = i));
        }
        
        for(Opportunity o : testOpportunities){
            o.StageName = 'DFSA License withdrawn';
        }
        
        update testOpportunities;
        
        for(Opportunity o : testOpportunities){
            o.StageName = 'In principle issued by DFSA';
        }
        
        update testOpportunities;
        
        for(Opportunity o : testOpportunities){
            o.StageName = 'Application Submitted to DFSA';
        }
        
        update testOpportunities;
        
        for(Opportunity o : testOpportunities){
            o.StageName = 'Application Accepted by DFSA';
        }
        
        update testOpportunities;
        
        for(Opportunity o : testOpportunities){
            o.StageName = 'ROC Registered';
        }
        
        update testOpportunities;
        
    }
    
    /**
     * V1.2
     * Updates the most recent stage history
     * record of the opportunities
     * @params      oppIds          The opportunity record IDs
     */
    @future
    private static void updateTestStageHistory(Set<id> oppIds){
        
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
        CRM_cls_OpportunityUtils.enableExecution(); // We need to re-enable trigger execution. Otherwise, the update will not fire
        
        List<Opportunity> testOpps = [SELECT Id, (SELECT Id, Stage_history_last_modified_date__c FROM opportunity_stage_history__r ORDER BY name DESC) FROM opportunity WHERE Id in :oppIds];
        
        List<opportunity_stage_history__c> testOppStageHistory = new List<opportunity_stage_history__c>();
        
        for(opportunity o : testOpps){
            
            o.opportunity_stage_history__r[0].Stage_History_Last_Modified_Date__c = o.opportunity_stage_history__r[0].Stage_History_Last_Modified_Date__c.addDays(1);    
            testOppStageHistory.add(o.opportunity_stage_history__r[0]);
            
        }
        
        update testOppStageHistory;
        
    }
    
    /**
     * Tests the Error logging methods of 
     * CRM_cls_Utils class
     */
    static testMethod void testCRMUtilsErrorLog(){
        
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
        CRM_cls_Utils testUtils = new CRM_cls_Utils();
        
        testUtils.setLogType('Test Log');
        testUtils.setErrorMessage('Test Log');
        
        System.debug('The error: ' + testUtils.getErrorMessage());
        System.debug('Has error? ' + testUtils.hasError());
        
        testUtils.logError();
    } 
    
    /**
     * Tests the new lead override methods for CRM
     */
    static testMethod void testLeadUrlOverride(){
        
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
        Lead testLead = Test_cls_Crm_Utils.getTestLead();
        
        insert testLead;
        
        Test.setCurrentPage(Page.CRM_LeadOverride);
        
        Apexpages.currentPage().getParameters().put('Id',testLead.Id);
        
        CRM_cls_LeadConversionOverride testLeadOverrideInst = new CRM_cls_LeadConversionOverride(new Apexpages.StandardController(testLead));
        
        testLeadOverrideInst.removePendingActivities();
        
        testLead.Previous_Date_of_notification__c = System.Now();
        
        update testLead;    
        
        Test.startTest();
        
        String jobId = System.schedule('test_CRM_cls_ScheduledLeadUpdate',Test_cls_Crm_Utils.CRON_EXP, new CRM_cls_ScheduledLeadUpdate());
        
        Test.stopTest();
    }
    
    static testMethod void testEmailMessageValidation(){
        
        Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector;
		
        Account testCrmApproverAccount = new Account(
            Name = 'CRM Approver', 
                RecordTypeId = [SELECT Id FROM RecordType WHERE 
                    SObjectType = 'Account' AND 
                    DeveloperName = 'System_Account'].Id);
        
        
        insert testCrmApproverAccount;
        
        Contact testApprover = new Contact(
            LastName = 'Test Approver',
            Email = 'shabbir.ahmed@difc.ae',
            AccountId = testCrmApproverAccount.Id
        );
        
        
        insert testApprover;
        
        Test_cls_Crm_Utils.setTestContactId(testApprover.Id);
        
        Account testAccountRecord = 
            Test_cls_Crm_Utils.getTestCrmAccount(Test_cls_Crm_Utils.CRM_DEFAULT_CUSTOMERNAME,
                Test_cls_Crm_Utils.CRM_DEFAULT_COMPANYTYPE);
            
        testAccountRecord.BP_No__c = '0000123214';
        testAccountRecord.Roc_Status__c = 'Created';
        testAccountRecord.Company_Type__c = 'Retail';
        
        insert testAccountRecord;
        
        Opportunity testOpportunity = 
            Test_cls_Crm_Utils.getTestOpportunity(Test_cls_Crm_Utils.CRM_DEFAULT_OPPORTUNITYNAME,
                testAccountRecord.Id,Test_cls_Crm_Utils.CRM_DEFAULT_STAGE);
                
        testOpportunity.recordTypeId = [SELECT ID FROM RecordType WHERE sObjectType = 'Opportunity' AND DeveloperName LIKE '%Checklist%' LIMIT 1].Id;
        
        insert testOpportunity;
        
        Task t = new Task();
        t.WhatID =testOpportunity.id;
        t.WhoID = testApprover.Id;
        insert t;
        
        Attachment iAtt = new Attachment();
        iAtt.parentid = testOpportunity.ID;
        iAtt.Name = 'testABC';
        iAtt.Body = Blob.valueOf('Unit Test Attachment Body');
        insert iAtt;
        
     //   System.debug('Testing point person retrieval: ' + CRM_cls_EmailUtils.getPointPersons());
        
        System.debug('Testing point person retrieval: ' + 
            CRM_cls_EmailUtils.getClientContacts(testCrmApproverAccount.Id));
        
        System.debug('Testing point person retrieval: ' + 
            CRM_cls_EmailUtils.createEmailLink(testOpportunity.Id,testCrmApproverAccount.Id,'Obtain HOD Approval','In Progress','Client',''));
               CRM_cls_EmailUtils.createEmailLink(testOpportunity.Id,testCrmApproverAccount.Id,'RRC Gate','RRC Gate','RRC Gate','');
        
        CRM_cls_EmailUtils.getPointPersons(testAccountRecord.Id);//v.7 
        
        
        
        Test.startTest();
        
        Savepoint spdata = Database.setSavepoint();
        
        try {
        
            insert new EmailMessage(
                FromAddress = 'test@abc.org', 
                Incoming = true, 
                ToAddress= 'shabbir.ahmed@difc.ae', 
                Subject = 'Test email', 
                TextBody = 'Retail_Security_Approval',
                htmlBody = 'Retail_Security_Approval',
                RelatedToId = testOpportunity.Id);
            
        } catch (Exception e){
            
            System.debug('Expecting error! Test successful.');
            
            CRM_cls_AccountHandler.getErrorMessage(e,spdata);
            
        }
        
        try {
        
            EmailMessage successfulEmail = new EmailMessage(
                FromAddress = 'test@abc.org', 
                Incoming = false, 
                ToAddress= 'test@test.ae', 
                Subject = 'Test email', 
                TextBody = 'Test_Email_Template_Code',
                htmlBody = 'Test_Email_Template_Code',
                RelatedToId = testOpportunity.Id);
            
            insert successfulEmail;
            
            Attachment testAttachment = Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Test attachment','Test Body');
            
            testAttachment.ParentId = successfulEmail.Id;
            
            insert testAttachment;
            
            Task testTask = new Task();
        
            testTask.recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Task' AND name = 'Email'].Id;
            testTask.Description = 'Test';
            testTask.Date_Time_From__c = System.now() - 10;
            testTask.Date_Time_To__c = System.now() - 10;
            testTask.priority = 'Normal';
            testTask.status = 'Completed';
            testTask.WhatId = testOpportunity.Id;
            testTask.whoId = testApprover.Id;
            testTask.category__c = 'DIFC Mgmt MTG CEO/HQ';
            testTask.Subject = 'The Mass Email';
            insert testTask;
            
            Test_cls_Crm_Utils.setTestEmailMessageId(successfulEmail.Id);
            CRM_cls_EmailUtils.getMassEmailActivities();
            CRM_cls_EmailUtils.setLeadEmailContent(new List<Task> {testTask});
            
        } catch (Exception e){
            
            System.debug('Not expecting error! Test unsuccessful.' + e.getMessage() + ' ' + e.getLineNumber());
            
        }
        
        Test.stopTest();
        
        Service_Request__c objSR = new Service_Request__c();
        
        objSR.Customer__c = testAccountRecord.Id;
        
        objSR.RecordTypeId = [select id from RecordType where DeveloperName='Application_of_Registration' and sObjectType='Service_Request__c' LIMIT 1].Id;
        objSR.Entity_Name__c = 'Test Entity';
        
        insert objSR;
        
        Step__c objstp = new Step__c();
        
        objstp.SR__c = objSR.Id;
        
        insert objstp;
        
        objstp = [SELECT Id, SR__c, SR__r.Customer__c FROM Step__c WHERE Id =: objStp.id];
        
        System.debug('Testing attachment copy: ' + CRM_cls_OpportunityUtils.copySecurityEmailAttachments(objstp));
        System.debug('Testing attachment copy: ' + CRM_cls_OpportunityUtils.copyEmailAttachmentsFromOpportunity(objstp));
        
    }
    
   
    static testMethod void testcreateFundAccount()
    {               
                test.setMock(WebServiceMock.class, new Test_cls_Crm_Utils.CRMWebserviceMock());
                Account testAccount = Test_cls_Crm_Utils.getTestCrmAccount(Test_cls_Crm_Utils.CRM_DEFAULT_CUSTOMERNAME,Test_cls_Crm_Utils.CRM_DEFAULT_COMPANYTYPE);
                testAccount.recordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('BD Fund').getRecordTypeId();
                insert testAccount;   
                
                //string rectype = '{'relationshipType':'Has Exempt Fund'}';
                
              //  system.assertEquals(testAccount.id,'');
                
              
                
                String jsonInput = '{'+' "relationshipType" :"Has Exempt Fund" '+'}';
               
                CRM_cls_AccountHandler.processAccountRecords(testAccount.id,'createFundAccount',jsonInput );
                
                Map<Id,Set<String>> acctActivityMap = new Map<Id,Set<String>> ();
                
                Lead testCrmLead = Test_cls_Crm_Utils.getTestLead();
            
            testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Retail'].Id;

            testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
            testCrmLead.Activities__c = 'ATM';
            testCrmLead.Send_Email__c = 'NO';
            insert testCrmLead; // Create a test lead   
            
            /* 
             * Create a sample task
             */
            Task testTask = new Task();
        
            testTask.recordTypeId = [SELECT Id FROM RecordType WHERE sObjectType = 'Task' AND name = 'Meeting'].Id;
            testTask.Description = 'Test';
            testTask.Date_Time_From__c = System.now() - 10;
            testTask.Date_Time_To__c = System.now() - 10;
            testTask.priority = 'Normal';
            testTask.status = 'Completed';
            testTask.whoId = testCrmLead.Id;
            testTask.category__c = 'DIFC Mgmt MTG CEO/HQ';
            
            insert testTask;
            List<Id> lstAccount = new List<Id>();
            
             Account objAccount = new Account();
            objAccount.Name = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1';
            //objAccount.E_mail__c = 'test@test.com';
            objAccount.Trade_Name__c = 'Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1 Test Custoer 1';
            objAccount.BP_No__c = '12345';
            objAccount.Company_Type__c = 'Financial - related';
            objAccount.Sector_Classification__c = 'Authorised Market Institution';
            objAccount.Send_Portal_Email__c = false;
            objAccount.ROC_Status__c = '';
            insert objAccount;
            lstAccount.add(objAccount.ID);
            
            set<string> s1 = new set<string>();
            s1.add(testCrmLead.Activities__c);
                
                 License_Activity_Master__c objLAM = new License_Activity_Master__c();
                 objLAM.Name = 'Test DIFC Activity';
                 objLAM.Sector_Classification__c = 'Authorised Market Institution';
                 objLAM.Type__c = 'License Activity';
                 objLAM.Sys_Code__c = '048';
                 objLAM.Enabled__c = true;
                 insert objLAM;
                 
        
                License_Activity__c objLA = new License_Activity__c();
                objLA.Account__c = objAccount.Id;
                objLA.Activity__c = objLAM.Id;
                objLA.Sector_Classification__c = 'Authorised Market Institution';        
                insert objLA;
                
                acctActivityMap.put(objLAM.id,s1);
            
           
            
            Map<Id,Account> mappedAccountRecords = new Map<Id,Account> ();
            mappedAccountRecords.put(objAccount.id,objAccount);
            
            
            CLS_CreateBP.setBdUserId(lstAccount);
            //CLS_CreateBP.dummyTest();
            CLS_CreateBP.updateAccountRelationships(lstAccount);
            CRM_cls_AccountHandler.createLicenseActivities(acctActivityMap,mappedAccountRecords);
                
    }
    
    
     static testmethod void finaltest(){
         try{
    	
    	Sector_Lead_Mapping__c sector = new Sector_Lead_Mapping__c();
		sector.Name = 'Banks';
		sector.User_BP__c = 'HR00007760';
		INSERT sector; 
		
    	List<Lead> lstLeads = new List<Lead>();
    	Lead eachlead = new Lead();
    	eachlead.LastName = 'Test';
    	eachlead.Email ='test@test3.com';
    	eachLead.Company = 'DIFC';
    	eachlead.Lead_RecType__c = 'Event';
    	eachlead.Rating='High';
    	eachlead.MobilePhone = '+971443355667';
    	eachlead.Country__c = 'India';
    	eachlead.Geographic_Region__c = 'Asia';
    	eachlead.Priority_to_DIFC__c = 'T1: Global with high FTE';
    	eachlead.LeadSource = 'facebook';
    	eachlead.Send_Portal_Login_Link__c = 'Yes';
    	eachlead.Is_Commercial_Permission__c  = 'Yes';
    	eachlead.Company_Type__c = 'Financial - related';
    	eachlead.Status = 'Interested';
    	eachlead.BD_Sector_Classification__c = 'Banks';
    	//eachlead.BD_Sector__c = '';
    	INSERT eachlead;
    	lstLeads.add(eachlead);
    	
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(eachlead.id);
		lc.setConvertedStatus('Interested');
		
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		
		
		system.debug('@@####'+sector);
		CLS_CreateBP createBP = new CLS_CreateBP();
		//createBP.setSectorLeadMap();
		
		Account testAccount = Test_cls_Crm_Utils.getTestCrmAccount(Test_cls_Crm_Utils.CRM_DEFAULT_CUSTOMERNAME,Test_cls_Crm_Utils.CRM_DEFAULT_COMPANYTYPE);
        testAccount.Authorization_Group__c = 'BDD Team';
        testAccount.Company_Type__c = 'Retail';
        testAccount.Priority_to_DIFC__c = 'T1: Global with high FTE';
        testAccount.BD_Sector__c = 'Banks';
        testAccount.ROC_Status__c = 'Account created';
        testAccount.Financial_Sector_Activities__c = 'Private Bank';
        testAccount.BD_Financial_Services_Activities__c = 'Accepting Deposits';
      //  testAccount.Sector_Lead_user__c = userinfo.getuserid();
        insert testAccount;
        
        String RecordTypeId;
                for(RecordType objRT : [select Id,Name from RecordType where DeveloperName='BD_Financial_Fund' AND SobjectType='Opportunity' AND IsActive=true])
                    RecordTypeId = objRT.Id;
                    
        Opportunity testOpportunity = Test_cls_Crm_Utils.getTestOpportunity(Test_cls_Crm_Utils.CRM_DEFAULT_OPPORTUNITYNAME,testAccount.Id,Test_cls_Crm_Utils.CRM_DEFAULT_STAGE);
        testOpportunity.RecordtypeID = RecordTypeId;
        
        testOpportunity.Activities__c = 'Accounting & Bookkeeping';
        testOpportunity.Sector_Classification__c = 'Accounts';
        //testOpportunity.Company_Type__c = 'Retail';
        insert testOpportunity;
		opportunityTransferToROC.updateOpportunity(testOpportunity.ID);
		
    }
     
    catch(Exception e){}
}
}