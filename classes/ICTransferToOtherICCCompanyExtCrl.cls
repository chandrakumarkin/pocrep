/*
Created By  : Suchita - DIFC on 23 May 2021 
Description : This Class is extension class for ICTransferToOtherICCCompany visualforce page
--------------------------------------------------------------------------------------------------------------------------
Modification History
----------------------------------------------------------------------------------------
V.No        Date                Updated By          Description
v1.0      23 May 2021          Suchita Sharma,       This Class is extension class for ICTransferToOtherICCCompany visualforce page

----------------------------------------------------------------------------------------
*/
public  class ICTransferToOtherICCCompanyExtCrl {
    public Service_Request__c SRData { get; set;}
    public string RecordTypeId;
    public map < string, string > mapParameters;
    public List<Shareholder_Detail__c> ListShareholder { get; set; }
    public Attachment ObjAttachment { get;set; }
    public boolean SRisValid { get;set; }
    public Account ObjAccount { get;set; }
    public List<String> SuffixList = new List<String>{'Ltd','Limited','PLC'};
    public boolean IsSuffixAvailable{get;set;}
    public string EntityName{get;set;}
    public List<SelectOption> suffixOption{set;}
	public String selectedSuffix{get;set;}
    public boolean IsSuffixAvailableTrade{get;set;}
    public string TradeName{get;set;}
    public String selectedSuffixTrade{get;set;}
    public String selectedICType{get;set;}
    private string Rtype; //sR Record in URL 
    public String selectedCompanyId{get; set;}
  	public String selectedCompanyName{get; set;}
    Public string conversionValidation = 'Transfer of Incorporated Cell to another Incorporated Cell Company is only allowed for entities conducting Fund or Insurance business.';
   
    public ICTransferToOtherICCCompanyExtCrl(ApexPages.StandardController controller) {
      
        SRisValid = false;
        SRData = (Service_Request__c) controller.getRecord();
        
        Service_Request__c SRDataObj;
        if( SRData.Id != null ){
            SRDataObj = [SELECT Record_Type_Name__c, RecordTypeId,Entity_Name__c,Proposed_Trading_Name_1__c,Usage_Type__c,Summary__c,Transfer_to_account__c FROM Service_Request__c WHERE Id =: SRData.Id];
        	
            selectedCompanyName=SRDataObj.Summary__c;
        }
        
        ObjAccount = new account();
        ObjAttachment = new Attachment();
        if (apexpages.currentPage().getParameters() != null)
            mapParameters = apexpages.currentPage().getParameters();
        if (mapParameters.get('RecordType') != null)
            RecordTypeId = mapParameters.get('RecordType');
        
        if (mapParameters.get('type') != null){
            Rtype = mapParameters.get('type');
        }else if( SRDataObj != null){
            Rtype = SRDataObj.Record_Type_Name__c;
            RecordTypeId = SRDataObj.RecordTypeId;
        }
        PopulateInitialData();  
        Boolean activityValidation=checkValidActivity();
        if(activityValidation){           
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.SEVERITY.FATAL, conversionValidation);
            ApexPages.addMessage(myMsg);
        }else{SRisValid = true;}      
    }
    Public boolean checkValidActivity(){
        
        Boolean activityValidation= true;
        for(License_Activity__c Act:[select id from License_Activity__c where Account__c=:SRData.Customer__c and Activity_Name_Eng__c in('Investment Fund','Insurance Intermediation','Insurance Management')]){
            activityValidation=false;
        }
        
        return activityValidation;
    }
    
    public void setICCCompanydetails(){
      if(selectedCompanyId!=null){
        Account selectedAccount = [SELECT Id, Name, Active_License__r.License_Issue_Date__c, Registration_License_No__c FROM Account WHERE Id = :selectedCompanyId];
        selectedCompanyName = selectedAccount.Name;
        selectedCompanyId = '';
      }
     
  } 
    public PageReference SaveRecord() {
        
        Date dt = System.today();//current date
        try {
            if(selectedCompanyName!=null){
                SRData.Summary__c=selectedCompanyName;
            }
            upsert SRData;
            PageReference acctPage = new ApexPages.StandardController(SRData).view();
            acctPage.setRedirect(true);
            return acctPage;
        } catch (Exception e) {
            ApexPages.Message myMsg =new ApexPages.Message(ApexPages.SEVERITY.FATAL, e.getDmlMessage(0));
            ApexPages.addMessage(myMsg);
            return NULL;
        }
   
    }
    
    public void PopulateInitialData(){
        for (User objUsr: [SELECT id, ContactId, Email, Contact.Account.Legal_Type_of_Entity__c, Phone, Contact.AccountId, 
                           Contact.Account.Name,Contact.Account.Trading_Name_Arabic__c,Contact.Account.Trade_Name__c,Contact.Account.OB_Sector_Classification__c 
                           FROM User 
                           WHERE Id =: userinfo.getUserId() ]) {
                               
                               if (SRData.id == null) {
                                   List<Relationship__c> Existingrel = [select id, Object_Account__r.Name from Relationship__c where Subject_Account__c=:objUsr.Contact.AccountId and Active__c=true and Relationship_Type__c='Incorporated Cell Company' limit 1];
                                   system.debug('Existingrel**'+Existingrel);
                                   SRData.Customer__c = objUsr.Contact.AccountId;
                                   SRData.RecordTypeId = RecordTypeId;
                                   SRData.Email__c = objUsr.Email;
                                   SRData.Legal_Structures__c = objUsr.Contact.Account.Legal_Type_of_Entity__c;
                                   SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                                   SRData.Entity_Name__c = objUsr.Contact.Account.Name;
                                   SRData.Usage_Type__c=objUsr.Contact.Account.OB_Sector_Classification__c;
                                   if(Existingrel.size()>0){
                                    SRData.Sponsor_Middle_Name__c=Existingrel[0].Object_Account__r.Name;// Old ICC Company
                                   }
                                   
                                   ObjAccount = objUsr.Contact.Account;
                               }
                           }
    }
}