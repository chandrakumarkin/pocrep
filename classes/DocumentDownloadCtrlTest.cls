@isTest(seeAllData = false)
private class DocumentDownloadCtrlTest {
    
        static testMethod void myUnitTest() {
        list<Endpoint_URLs__c> lstCS = new list<Endpoint_URLs__c>();
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'GS ECC';
        objEP.URL__c = 'http://GSECC.com/sampledata';
        lstCS.add(objEP);
        objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://ECC.com/sampledata';
        lstCS.add(objEP);
        insert lstCS;
        
        list<Country_Codes__c> lstCC = new list<Country_Codes__c>();
        Country_Codes__c objCode = new Country_Codes__c();
        objCode.Name = 'India';
        objCode.Code__c = 'IN';
        objCode.Nationality__c = 'India';
        lstCC.add(objCode);
        insert lstCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        objTemplate.Menutext__c = 'New Employment Visa Package';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId =  Schema.SObjectType.Service_Request__C.getRecordTypeInfosByDeveloperName().get('DIFC_Sponsorship_Visa_New').getRecordTypeId();
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[0].Id;
        objSR.External_SR_Status__c = lstSRStatus[0].Id;
        insert objSR;

         SR_Doc__c objSRDoc = new SR_Doc__c();
        objSRDoc.Service_Request__c = objSR.Id;
        objSRDoc.Is_Not_Required__c = false;
        objSRDoc.Group_No__c = 1;
        objSRDoc.Name = 'EID Registration Form after Biometrics';
        
        insert objSRDoc;

        SR_Doc__c objSRDoc2 = new SR_Doc__c();
        objSRDoc2.Service_Request__c = objSR.Id;
        objSRDoc2.Is_Not_Required__c = false;
        objSRDoc2.Group_No__c = 1;
        objSRDoc2.Name = 'EID Registration form';
        
        insert objSRDoc2;

        Attachment AttachmentObj = new Attachment();     
        AttachmentObj.Name='Unit Test Attachment.jpg';
        AttachmentObj.body = Blob.valueOf('Unit Test Attachment Body');
        AttachmentObj.parentId = objSRDoc.id;

        insert AttachmentObj;

        Attachment AttachmentObj2 = new Attachment();     
        AttachmentObj2.Name='UnitTestAttachment.jpg';
        AttachmentObj2.body = Blob.valueOf('Unit Test Attachment Body');
        AttachmentObj2.parentId = objSRDoc2.id;

        insert AttachmentObj2;

        Attachment AttachmentObj3 = new Attachment();     
        AttachmentObj3.Name='Unit Test Attachment.jpg';
        AttachmentObj3.body = Blob.valueOf('Unit Test Attachment Body');
        AttachmentObj3.parentId = objSRDoc2.id;

        insert AttachmentObj3;

        Test.startTest();
        
        PageReference pageRef = Page.DocumentDownload;
        Test.setCurrentPage(pageRef);

        ApexPages.currentPage().getParameters().put('srid', objSR.Id);
        ApexPages.currentPage().getParameters().put('srno', objSR.Name);

        DocumentDownloadCtrl DocumentDownloadCtrlObj = new DocumentDownloadCtrl();
        DocumentDownloadCtrlObj.getAttachments();
        DocumentDownloadCtrl.getAttachment( String.valueOf( AttachmentObj.Id ) );
        Test.stopTest();
    }
}