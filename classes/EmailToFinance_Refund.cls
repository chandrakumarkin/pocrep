global without sharing class EmailToFinance_Refund {
    
    public ServiceRequestInfo srinfO{get;set;}
    public service_request__c sr{get;set;}
    private map<string,string> pageparameters;
    public string typeofRefund{get;set;}
    public EmailToFinance_Refund(){
        pageparameters = new map<string,string>();
        pageparameters = apexpages.currentPage().getParameters();
    }
    
    public void fetchsrinfO(){  
        srinfO = new ServiceRequestInfo();  
         sr = new Service_Request__c();  
        map<string,decimal> accBalMap;
        typeofRefund = (pageparameters.containsKey('typ') && pageparameters.get('typ') !=null) ? pageparameters.get('typ') : '';
        if(pageparameters !=null && pageparameters.size()>0 && pageparameters.containsKey('id')){
            
            for(Service_Request__c srObj : [select id,customer__c,Name,customer__r.Active_License__r.Name,customer__r.BP_NO__c,customer__r.ROC_Status__c,Refund_Amount__c,Approved_By__r.Name,Mode_of_Refund__c,Type_of_refund__c,
                                                    SAP_PR_No__c,Customer__r.Name,External_SR_status__r.Name,Comments__c,SAP_SGUID__c,SAP_OSGUID__c,Bank_Name__c,Bank_Address__c,IBAN_Number__c,SAP_GSTIM__c,
                                                    Transfer_to_account__r.Name,Transfer_to_account__r.BP_No__c,Transfer_to_account__r.Active_License__r.Name,Transfer_to_account__r.Roc_Status__c,
                                                    (select id,Name,owner.LastName,owner.FirstName,Step_Name__c,Comments__c from Steps_SR__r where (Step_Template__r.code__c='HOD_Review' or Step_Template__r.code__c='GS_LINE_MANAGER_APPROVAL' or Step_Template__r.code__c = 'REVIEW_FOR_CANCELLATION')),
                                                    (select id,status__c,price__c from SR_Price_Items1__r) from service_request__c where id =:pageparameters.get('id')]){
                srInfo.companyName = srObj.customer__r.Name;
                srInfo.licenseNo = srObj.customer__r.Active_License__r.Name;
                srInfo.BPNO = srObj.customer__r.BP_No__c; 
                srInfo.srStatus = srObj.External_SR_Status__r.Name;
                srInfo.servicerequestNumber = srObj.Name;
                srInfo.companyStatus  = srObj.customer__r.ROC_Status__c;
                srInfo.srApprovedBy = srObj.Approved_By__r.Name;
                srInfo.Modeofpayment = srObj.Mode_of_Refund__c;
                srInfo.PRnumber = srObj.SAP_PR_No__c;
                
                srinfo.companyNameTo = srObj.Transfer_to_account__r.Name;
                srinfo.bpnoTo =  srObj.Transfer_to_account__r.BP_No__c;
                srInfo.companyStatusTo  = srObj.Transfer_to_account__r.ROC_Status__c;
                 srInfo.licenseNoTo = srObj.Transfer_to_account__r.Active_License__r.Name;
                
                //srInfo.payeeDetails =
                srInfo.refundRequested = srObj.Type_of_Refund__c;   
                sr = srObj;              
                srInfo.totalamountPaidForService = 0;
                 for(SR_Price_Item__c srp: srObj.SR_Price_Items1__r){
                    srInfo.totalamountPaidForService += srp.price__c;
                }
                
                srinfo.totalamountdeducted = srObj.Refund_Amount__c!=null ? (srInfo.totalamountPaidForService - srObj.Refund_Amount__c) :0;
                
                
                srInfo.srApprovedBy='';
                srInfo.gsComments = '';
                for(step__c stp : srObj.Steps_SR__r){
                    srInfo.srApprovedBy += stp.Step_Name__c +':'+stp.owner.FirstName+' '+stp.owner.LastName+'\n\n';
                    if(stp.Comments__c !=null)
                        srInfo.gsComments += stp.Step_Name__c+' Comments : ' +stp.Comments__c +'\n\n';
                }                
               
                
            }   
            
            if(sr !=null && sr.customer__c !=null && (sr.type_of_Refund__c=='PSA Refund' || sr.type_of_Refund__c=='Portal Balance Refund')) {
                accBalMap = new map<string,decimal>();
                accBalMap = RefundRequestCls.checkPortalBalance(string.valueOf(sr.customer__c));
            }   
            
            // V1.11 Start
            decimal refundAmount =0;
            for(Service_Request__c srRefund : [select id,Refund_Amount__c,Type_of_refund__c from service_request__c where customer__c =: sr.customer__c and Record_type_Name__c ='Refund_Request' and Submitted_Date__c !=null and Is_Rejected__c = false and Refund_Amount__c !=null and External_SR_STatus__r.Name !='Approved' and External_SR_STatus__r.Name !='Rejected']){
                        
                refundAmount += srRefund.Refund_Amount__c;
            } 
            
            decimal psaRefundAmnt=0;
            for(Service_Request__c srObj :[select id,Refund_Amount__c from service_request__c where record_type_Name__c ='Refund_Request' and (Type_of_Refund__c ='PSA Refund' or Type_of_Refund__c ='PSA Transfer') and Submitted_date__c !=null and External_SR_Status__r.Name not in ('Approved','Cancelled','Rejected') and customer__c =:sr.customer__c]){
                psaRefundAmnt += srObj.Refund_Amount__c;
            }            
            
            
            if(accBalMap !=null && accBalMap.size()>0){
                srInfo.portalBalance = accBalMap.get('portalbalance') - refundAmount;
                srInfo.psaDeposit = accbalMap.get('psadeposit') - psaRefundAmnt;
            }
            
             decimal[] currentInfoCount =  GsHelperCls.getCurrentEmployeeCount(new set<string>{sr.customer__c}); // Modified as Per V1.3
             decimal[] pipeLineInfoCount =  GsHelperCls.getPipelineEmployeeCount(new set<string>{sr.customer__c});  // Modified according to V1.3 
             srinfO.numberOfTotalSponsoredEmp = 'Current :'+currentInfoCount[0]+'\n Pipeline :'+pipeLineInfoCount[0]; 
            srinfO.payeeDetails = '';
            srinfO.payeeDetails += 'Account Name: '+sr.SAP_SGUID__c +'\n';
            srinfO.payeeDetails += 'Account Number : '+sr.SAP_OSGUID__c +'\n';
            srinfO.payeeDetails += 'Bank Name: '+sr.Bank_Name__c +'\n';
            srinfO.payeeDetails += 'Bank Address : '+sr.Bank_Address__c+'\n';
            srinfO.payeeDetails += 'Iban Number : '+sr.IBAN_Number__c +'\n';
            srinfO.payeeDetails += 'Swift Code : '+sr.SAP_GSTIM__c +'\n';
            
            
            
           
        }
        //return srInfo;        
    }               
    

    Global class ServiceRequestInfo{
        public string companyName{get;set;}
        public string licenseNo{get;set;}
        public string BPNO {get;set;}
        public string companyStatus{get;set;}
        public string servicerequestNumber{get;set;}
        public string PRnumber{get;set;}
        public decimal totalamountPaidForService{get;set;}
        public decimal totalamountdeducted{get;set;}
        public string srstatus{get;set;}
        public string gsComments{get;set;}
        public string srApprovedBy{get;set;}
        public string  numberOfTotalSponsoredEmp{get;set;}
        Public decimal psaDeposit{get;set;}
        public decimal portalBalance{get;set;}
        public string refundRequested{get;set;}
        public string Modeofpayment{get;set;} 
        public string payeeDetails{get;set;}
        public string companyNameto{get;set;}
        public string licenseNoto{get;set;}
        public string BPNOto{get;set;}
        public string companyStatusto{get;set;}
                    
    }
    
    
    
    
    
}