/******************************************************************************************
*  Author         :   Rajil Ravindran
*  Company        :   PwC
*  Date           :   28-Dec-2019
*  Description    :   TestClass for OB_QuickStepTransitionController
*  Version        :   1.0
********************************************************************************************/
@isTest
public class OB_QuickStepTransitionControllerTest {
    @testSetup
    static void createTestData() {
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        insert listPage;
        
        
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
        insertNewSRs[0].HexaBPM__External_SR_Status__c = listSRStatus[0].id;
        insert insertNewSRs;

        
    }
    static testMethod void OB_QuickStepTransitionControllerTest() {
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = [select id from HexaBPM__Service_Request__c];
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = [select id from HexaBPM__SR_Steps__c];
        
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
        insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insert insertNewSteps;

        OB_QuickStepTransitionController quickStepTransition = new OB_QuickStepTransitionController(insertNewSRs[0].id,insertNewSteps[0].id);
        quickStepTransition.Check_Permissions();
        OB_QuickStepTransitionController.getLstTransWrapper(insertNewSRs[0].id,insertNewSteps[0].id);
        
    }
    static testMethod void OB_QuickStepTransitionTest() {
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;

        /*Create Transition Record */
         HexaBPM__Status__c stepStatus = new HexaBPM__Status__c(Name='Awaiting Verification',HexaBPM__Type__c='Start',HexaBPM__Code__c='AWAITING_VERIFICATION');
        insert stepStatus;
        
        HexaBPM__Step_Template__c stepTemplate = new HexaBPM__Step_Template__c(Name='Verification of Application',HexaBPM__Code__c='VERIFICATION_OF_APPLICATION',HexaBPM__Step_RecordType_API_Name__c='General',HexaBPM__Summary__c='Verification of Application');
        insert stepTemplate;
        
        HexaBPM__SR_Steps__c srSteps = new HexaBPM__SR_Steps__c(HexaBPM__SR_Template__c=createSRTemplateList[0].id,HexaBPM__Step_Template__c=stepTemplate.id,
        HexaBPM__Start_Status__c=stepStatus.id,HexaBPM__Active__c=true,HexaBPM__Summary__c='Verification of Application',
        HexaBPM__Step_RecordType_API_Name__c='General',HexaBPM__Step_Type__c='Quick Action',HexaBPM__Group_Name__c='Verification of Application');
        insert srSteps;
        
        
        HexaBPM__Status__c status = new HexaBPM__Status__c(Name='Awaiting Verification',HexaBPM__Code__c='AWAITING_VERIFICATION',HexaBPM__Type__c='Start');
        
        HexaBPM__Status__c status1 = new HexaBPM__Status__c
        (Name='Re-Upload Document',HexaBPM__Code__c='REUPLOAD_DOCUMENT',HexaBPM__Type__c='End');
        insert status1;
        
        HexaBPM__Transition__c transition = new HexaBPM__Transition__c(HexaBPM__From__c=status.id,HexaBPM__To__c=status1.id);
        insert transition;
        
        HexaBPM__SR_Status__c srStatus = new HexaBPM__SR_Status__c(Name='Awaiting Re-Upload',HexaBPM__Code__c='AWAITING_RE-UPLOAD');
        insert srStatus;
        
        HexaBPM__Step_Transition__c stepTransition = new HexaBPM__Step_Transition__c
        (HexaBPM__SR_Step__c=srSteps.id,HexaBPM__Transition__c=transition.id,HexaBPM__SR_Status_Internal__c=srStatus.id,HexaBPM__SR_Status_External__c=srStatus.id);
        insert stepTransition;
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        //List<HexaBPM__Step_Transition__c> stepTransition = new List<HexaBPM__Step_Transition__c>();
        map<id,HexaBPM__Step_Transition__c> mapStepTransition = new map<id,HexaBPM__Step_Transition__c>();
        list<HexaBPM__Status__c> lstStatus = new list<HexaBPM__Status__c>();
        insertNewSRs = [select id from HexaBPM__Service_Request__c];
        HexaBPM__Step__c step = new HexaBPM__Step__c();
        step.HexaBPM__SR__c = insertNewSRs[0].id;
        step.HexaBPM__Status__c = status.id;
        step.HexaBPM__SR_Step__c = srSteps.id;
        insert step;
        mapStepTransition.put(stepTransition.id,stepTransition);
        OB_QuickStepTransitionController.saveChanges(stepTransition.id, 'wetr', 'ewrew', insertNewSRs[0].id, step, mapStepTransition);
       
        OB_QuickStepTransitionController.acceptAction(step.id,true);
        OB_QuickStepTransitionController.acceptAction(step.id,false);
        OB_QuickStepTransitionController.CancelAction(insertNewSRs[0].id);
        OB_QuickStepTransitionController.ViewStep(step.id);
		OB_QuickStepTransitionController.getLstTransWrapper(insertNewSRs[0].id, step.id);
        
        //OB_QuickStepTransitionController.saveChanges('', '', '', insertNewSRs[0].id, step, mapStepTransition);
        OB_QuickStepTransitionController quickStepTransition = new OB_QuickStepTransitionController(insertNewSRs[0].id,step.id);
        quickStepTransition.Check_Permissions();
        quickStepTransition.StepID = step.id;

        
    }
    
    

}