/*********************************************************************************************************************
*  Name     : BPM_SecurityStepStatusUpdateBatch 
*  Author   : 
*  Purpose  : This class used to get response from Dubai police portal for Normal BPM Records
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
V1.1         DIFC                04-May-2021               Draft Version
**/

global class BPM_SecurityStepStatusUpdateBatch implements database.batchable<sobject>,Database.AllowsCallouts, Database.Stateful{
    
    public static string testStatus ='';
    global database.querylocator start(database.batchablecontext bc){
        
        String statusRequest = 'Verified'; 
        List<String> serviceType = Label.Normal_BPM_Service_Request_Type_Security_Check.split(',');
        String stepName = Label.SECURITYEMAILAPPROVAL;
        
        system.debug('----statusRequest---'+statusRequest);
        system.debug('----serviceType---'+serviceType);
        system.debug('----stepName---'+stepName);
        
        String s = 'SELECT Id,Name,Step_Status__c,SR__r.Customer__c,Status__c,SR__r.Name,DIFC_Security_Email_Response__c FROM Step__c';
        if(!Test.isRunningTest())s += ' where CreatedDate = LAST_N_DAYS:30 AND SR_Record_Type__c IN:serviceType and Step_Template__r.Code__c =\'SECURITY_EMAIL_APPROVAL\'  AND Step_Status__c !=\''+statusRequest+'\'';
        else s += ' limit 1';
        
        system.debug('=query==test='+s);
        system.debug('=query==test='+database.query(s));
        
        return database.getquerylocator(s);
    }
    
    global void execute(database.batchablecontext bc, List<Step__c> lstStepData){
        try{
        system.debug('====lstStepData======'+lstStepData);
        String USERNAME = Label.DYFC_API_User_Name;   //This Variable used to Store API user User Name
        String PASSWORD = Label.DYFC_Password;        //This Variable used to Store API user PASSWORD
        String CONSUMER_KEY = Label.DIFC_Consumer_KEY; //This Variable used to Store CONSUMER_KEY
        String CONSUMER_SECRET = Label.DIFC_Consumer_Secret; //This Variable used to Store CONSUMER_SECRET
        List<Step__c> lstStepDataUpdate = new List<Step__c>();
        
        Blob headerValue = Blob.valueOf(USERNAME + ':' + PASSWORD);
        String authorizationHeader = Label.Dubai_Police_Response_URL+CONSUMER_KEY+'&client_secret='+CONSUMER_SECRET+'&username='+USERNAME+'&password='+PASSWORD;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(authorizationHeader);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        HttpResponse response = http.send(request);
         system.debug('---response----'+response); 
        system.debug('---response--body--'+response.getBody()); 
        if(response.getStatusCode() == 200){    
            
            Http http1 = new Http();
            HttpRequest request1 = new HttpRequest();
            HttpResponse response1 = new HttpResponse(); 
            
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Object BearerCode1 = results.get('access_token');
            String BearerCode = String.valueOf(BearerCode1);
            BearerCode = 'Bearer '+BearerCode;
            system.debug('---BearerCode----'+BearerCode);
            Map<string,String> statuscodeIDMap = new Map<String,String>();
            List<string> statusList = new List<String>{'REJECTED','FORWARD_TO_CUSTOMER_FOR_MORE_INFORMATION','APPROVED','VERIFIED'};
            List<Account> acctList = new List<Account>();
            for(Status__c status:[Select Id,Code__c From Status__c
                                            Where Code__c IN: statusList]){
                statuscodeIDMap.put(status.Code__c,status.Id);
            } 
            
            for(Step__c eachStep:lstStepData){
                if(eachStep.DIFC_Security_Email_Response__c != null){
                    Account acct = new Account();
                    if(String.IsNotBlank(eachStep.SR__r.Customer__c)){
                         acct.Id = eachStep.SR__r.Customer__c;
                    }
                    system.debug(eachStep.SR__r.Name+'=eachStep==='+eachStep.Id);
                    request1 = new HttpRequest(); 
                    request1.setHeader('Authorization',BearerCode);
                    request1.setEndpoint(Label.Dubai_Police_Response_URL_2+ eachStep.SR__r.Name+CC_DubaiPoliceSecurityAmendment.DUMMY_REC_VALUE);
                    request1.setMethod('GET');    
                    response1 = http1.send(request1);
                    system.debug('---response1.getStatusCode()-----'+response1.getStatusCode());
                    system.debug('---response1.getBody()-----'+response1.getBody());
                    system.debug('---statuscodeIDMap----'+statuscodeIDMap);
                   //{"amendmentDto":{"requestCommonData":{"assignedUserId":0,"channelType":"API","className":"com.tinext.dfzc.models.rest.tradelicenses.AmendmentDto","classPK":2513591,"createDate":"2021-05-04T09:23:59.207Z","createdByUserId":35656,"createdByUsername":"DIFC API","documentNumber":"A16220645","freeZone":"DIFC","groupId":20138,"modifiedByUserId":35656,"modifiedByUsername":"DIFC API","modifiedDate":"2021-05-04T09:24:00.812Z","policeNotes":"","requestCommonDataId":2513582,"requestSourceId":"0572764_5678","requestType":"LICENSE_AMENDMENT","requestTypeGroup":"TRADE_LICENSES","status":"NEW","tradeLicenseNumber":""},"additionalAttachment":{"additionalAttachmentId":2513586,"fiveAddFileId":"","fiveAddFileName":"","fourAddFileId":"","fourAddFileName":"","oneAddFileId":"","oneAddFileName":"","threeAddFileId":"","threeAddFileName":"","twoAddFileId":"","twoAddFileName":""},"representativeAgency":{"email":"","nameAR":"","nameEN":"","originCountry":"","representativeAgencyId":2513584,"telephoneNumber":""},"representativeIndividual":{"address":"Dubai International Financial Centre,Sheikh Zayed Rd - Dubai","area":"DIFC","birthCity":"Alexandria","birthCountry":"Egypt","birthDate":"1987-11-14T00:00:00Z","companyNameAR":"Nancy Ahmed","companyNameEN":"Nancy Ahmed","companyPosition":"manager","currentNationality":"Afghanistan","documentTypeId":"national_id","documentTypePassport":"national_id","gender":"F","idCopyFileId":"a161j0000018sgJAAQ","idDateOfExpiry":"2022-09-07T00:00:00Z","idDateOfIssue":"2015-09-08T00:00:00Z","idIssuePlace":"Egypt","idNumber":"A16220645","mobileNumber":971569803616,"passportCopyFileId":"a161j0000018sgJAAQ","passportDateOfExpiry":"2022-09-07T00:00:00Z","passportDateOfIssue":"2015-09-08T00:00:00Z","passportIssuePlace":"Egypt","passportNumber":"A16220645","previousNationality":"","remarks":"","remarksManager":"","remarksSecretary":"","representativeIndividualId":2513585,"residenceCopyFileId":"NA","udbNumber":0},"shareholderCompany":{"companyNameAR":"Jio","companyNameEN":"Jio","originCountry":"India","ownershipPercent":1,"shareholderCompanyId":2513587,"sharesNumber":1,"sharesValue":1,"telephoneNumber":""},"shareholderIndividual":{"address":",Dubai,Dubai,Dubai,India,12345","area":"DIFC","birthCity":"Dubai","birthCountry":"India","birthDate":"1996-05-01T00:00:00Z","companyNameAR":"ذا ستيك بار ليميتد","companyNameEN":"The Steak Bar Ltd.","companyPosition":"manager","currentNationality":"India","documentTypeId":"national_id","documentTypePassport":"national_id","gender":"M","idCopyFileId":"a161j0000018sgJAAQ","idDateOfExpiry":"2023-05-28T00:00:00Z","idDateOfIssue":"2020-05-13T00:00:00Z","idIssuePlace":"India","idNumber":"HL12345","mobileNumber":"0000","ownershipPercent":1,"passportCopyFileId":"a161j0000018sgJAAQ","passportDateOfExpiry":"2023-05-28T00:00:00Z","passportDateOfIssue":"2020-05-13T00:00:00Z","passportIssuePlace":"India","passportNumber":"HL12345","previousNationality":"","remarks":"","remarksManager":"","remarksSecretary":"","residenceCopyFileId":2,"shareholderIndividualId":2513588,"sharesNumber":1,"sharesValue":1,"udbNumber":0},"amendmentTradeLicenseRequestId":2513591,"amendmentType":"Shareholder","bankStatementFileId":"NA","companyNameAR":"NA","companyNameEN":"The Steak Bar Ltd.","companyPercentage":100,"employeesNumber":1,"establishmentNumber":26205984,"establishmentNumberImmSys":"NA","iban":"","location":"DIFC","notes":{"noteId":2513590,"notes":""},"numberOfLicenses":0,"originCountry":"United Arab Emirates","parentCompanyData":{"bankStatementFileId":"","boardOfDirectorsDecisionFileId":"","companyNameAR":"","companyNameEN":"","companyPercentage":100,"employeesNumber":0,"establishmentContractFileId":"","establishmentNumber":"NA","establishmentNumberImmSys":"NA","iban":"","legalPowerOfAttorneyFileId":"","location":"United Arab Emirates","offshoreContractArticlesOfAssociationsFileId":"","offshoreContractMoUFileId":"","originCountry":"United Arab Emirates","parentCompanyDataId":2513589,"position":"outside_the_country","registrationType":"OFF","representativeContactNumber":"NA","representativeId":"NA","representativeName":"NA","representativeNationality":"","workArea":"DIFC"},"position":"outside_the_country","registrationType":"OFF","representativeContactNumber":971569803616,"representativeId":"NA","representativeName":"","representativeNationality":"Afghanistan","representativeType":"Individual","shareholderType":["Company","Individual"],"workArea":"DIFC"}}
                    if(response1.getStatusCode() == 200){
                        system.debug('==inside suv==');
                        BPM_SecurityStepStatusUpdateBatch.DubaiPoliceAmendmentJSONResponse myClass;
                        if(!Test.isRunningTest()){
                            myClass = BPM_SecurityStepStatusUpdateBatch.parse(response1.getBody());
                        }else{
                            myClass = new BPM_SecurityStepStatusUpdateBatch.DubaiPoliceAmendmentJSONResponse();
                            myClass.amendmentDto = new BPM_SecurityStepStatusUpdateBatch.AmendmentDto();
                            myClass.amendmentDto.requestCommonData = new BPM_SecurityStepStatusUpdateBatch.RequestCommonData();
                            myClass.AmendmentDto.requestCommonData.status ='APPROVED';
                            myClass.AmendmentDto.requestCommonData.policeNotes ='test';
                        }
                        String Status = myClass.AmendmentDto.requestCommonData.status;
                        String comments = myClass.AmendmentDto.requestCommonData.policeNotes;
                        system.debug('------Status-----'+Status);
                        system.debug('------comments-----'+comments);
                        //Status = 'APPROVED';
                        if(String.isNotBlank(Status) && Status =='APPROVED'){
                            eachStep.Status__c = statuscodeIDMap != null && statuscodeIDMap.size() != 0 && statuscodeIDMap.containskey('VERIFIED') ? statuscodeIDMap.get('VERIFIED'):null;
                            eachStep.Step_Notes__c= comments;
                            eachStep.Closed_Date__c = system.today();
                            lstStepDataUpdate.add(eachStep);
                            //account
                            acct.Security_Review_Date__c= system.now();
                            acct.Security_Review_Status__c= 'Approved';
                            acctList.add(acct);
                        } 
                        else if(String.isNotBlank(Status) && Status =='NEED_MORE_INFO'){
                            eachStep.Status__c = statuscodeIDMap != null && statuscodeIDMap.size() != 0 && statuscodeIDMap.containskey('FORWARD_TO_CUSTOMER_FOR_MORE_INFORMATION') ? statuscodeIDMap.get('FORWARD_TO_CUSTOMER_FOR_MORE_INFORMATION'):null;
                            eachStep.OwnerID= Label.ROC_Officer_Queue;
                            eachStep.Step_Notes__c= comments;
                            lstStepDataUpdate.add(eachStep);
                            //account
                            acct.Security_Review_Date__c= system.now();
                            acct.Security_Review_Status__c= 'Return for More Information';
                            acctList.add(acct);
                        }
                        else if(String.isNotBlank(Status) && Status =='Rejected'){
                            eachStep.Status__c = statuscodeIDMap != null && statuscodeIDMap.size() != 0 && statuscodeIDMap.containskey('REJECTED') ? statuscodeIDMap.get('REJECTED'):null;
                            eachStep.Step_Notes__c= comments;
                            lstStepDataUpdate.add(eachStep);
                            //account
                            acct.Security_Review_Date__c= system.now();
                            acct.Security_Review_Status__c= 'Rejected';
                            acctList.add(acct);
                        }
                    }  
                }    
            }
            system.debug('==lstStepDataUpdate=='+lstStepDataUpdate);
            system.debug('==acctList=='+acctList);
            if(lstStepDataUpdate!=null && lstStepDataUpdate.size()>0){
                database.update(lstStepDataUpdate);
                database.update(acctList);
            }   
        }
        }catch(Exception e){
            string errorresult = e.getMessage();
            system.debug('=========errorresult ==============='+errorresult +'\n------Line Number :------'+e.getLineNumber()+'\nException is : '+errorresult);
            insert LogDetails.CreateLog(null, 'BPM_SecurityStepStatusUpdateBatch: execute', 'Line Number : '+e.getLineNumber()+'\nException is : '+errorresult);
        }
    }
    
    global void finish(database.batchablecontext bc){
    
    }
    
    public class DubaiPoliceAmendmentJSONResponse {

        public AmendmentDto amendmentDto;       
    }
    public class RequestCommonData {
        public String policeNotes;
        public String status;
    }
    
    public class AmendmentDto{
        public RequestCommonData requestCommonData;
    }
    
    public static DubaiPoliceAmendmentJSONResponse parse(String json) {
        System.debug('$$$$'+json);
        return (DubaiPoliceAmendmentJSONResponse ) System.JSON.deserialize(json, DubaiPoliceAmendmentJSONResponse.class);
    }

   /* global static void executeBPMRec(List<Step__c> lstStepData,string dummyREc){
        try{
        system.debug('====lstStepData======'+lstStepData);
        String USERNAME = Label.DYFC_API_User_Name;   //This Variable used to Store API user User Name
        String PASSWORD = Label.DYFC_Password;        //This Variable used to Store API user PASSWORD
        String CONSUMER_KEY = Label.DIFC_Consumer_KEY; //This Variable used to Store CONSUMER_KEY
        String CONSUMER_SECRET = Label.DIFC_Consumer_Secret; //This Variable used to Store CONSUMER_SECRET
        List<Step__c> lstStepDataUpdate = new List<Step__c>();
        
        Blob headerValue = Blob.valueOf(USERNAME + ':' + PASSWORD);
        String authorizationHeader = Label.Dubai_Police_Response_URL+CONSUMER_KEY+'&client_secret='+CONSUMER_SECRET+'&username='+USERNAME+'&password='+PASSWORD;
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(authorizationHeader);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        HttpResponse response = http.send(request);
         system.debug('---response----'+response); 
        system.debug('---response--body--'+response.getBody()); 
        if(response.getStatusCode() == 200){    
            
            Http http1 = new Http();
            HttpRequest request1 = new HttpRequest();
            HttpResponse response1 = new HttpResponse(); 
            
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Object BearerCode1 = results.get('access_token');
            String BearerCode = String.valueOf(BearerCode1);
            BearerCode = 'Bearer '+BearerCode;
            system.debug('---BearerCode----'+BearerCode);
            Map<string,String> statuscodeIDMap = new Map<String,String>();
            List<string> statusList = new List<String>{'REJECTED','FORWARD_TO_CUSTOMER_FOR_MORE_INFORMATION','APPROVED','VERIFIED'};
            List<Account> acctList = new List<Account>();
            for(Status__c status:[Select Id,Code__c From Status__c
                                            Where Code__c IN: statusList]){
                statuscodeIDMap.put(status.Code__c,status.Id);
            } 
            
            for(Step__c eachStep:lstStepData){
                Account acct = new Account();
                if(String.IsNotBlank(eachStep.SR__r.Customer__c)){
                     acct.Id = eachStep.SR__r.Customer__c;
                }
                system.debug(eachStep.SR__r.Name+'=eachStep==='+eachStep.Id);
                request1 = new HttpRequest();
                request1.setHeader('Authorization',BearerCode);
                request1.setEndpoint(Label.Dubai_Police_Response_URL_2+ eachStep.SR__r.Name+dummyREc);
                request1.setMethod('GET');
                response1 = http1.send(request1);
                system.debug('---response1.getStatusCode()-----'+response1.getStatusCode());
                system.debug('---response1.getBody()-----'+response1.getBody());
                system.debug('---statuscodeIDMap----'+statuscodeIDMap);
               //{"amendmentDto":{"requestCommonData":{"assignedUserId":0,"channelType":"API","className":"com.tinext.dfzc.models.rest.tradelicenses.AmendmentDto","classPK":2513591,"createDate":"2021-05-04T09:23:59.207Z","createdByUserId":35656,"createdByUsername":"DIFC API","documentNumber":"A16220645","freeZone":"DIFC","groupId":20138,"modifiedByUserId":35656,"modifiedByUsername":"DIFC API","modifiedDate":"2021-05-04T09:24:00.812Z","policeNotes":"","requestCommonDataId":2513582,"requestSourceId":"0572764_5678","requestType":"LICENSE_AMENDMENT","requestTypeGroup":"TRADE_LICENSES","status":"NEW","tradeLicenseNumber":""},"additionalAttachment":{"additionalAttachmentId":2513586,"fiveAddFileId":"","fiveAddFileName":"","fourAddFileId":"","fourAddFileName":"","oneAddFileId":"","oneAddFileName":"","threeAddFileId":"","threeAddFileName":"","twoAddFileId":"","twoAddFileName":""},"representativeAgency":{"email":"","nameAR":"","nameEN":"","originCountry":"","representativeAgencyId":2513584,"telephoneNumber":""},"representativeIndividual":{"address":"Dubai International Financial Centre,Sheikh Zayed Rd - Dubai","area":"DIFC","birthCity":"Alexandria","birthCountry":"Egypt","birthDate":"1987-11-14T00:00:00Z","companyNameAR":"Nancy Ahmed","companyNameEN":"Nancy Ahmed","companyPosition":"manager","currentNationality":"Afghanistan","documentTypeId":"national_id","documentTypePassport":"national_id","gender":"F","idCopyFileId":"a161j0000018sgJAAQ","idDateOfExpiry":"2022-09-07T00:00:00Z","idDateOfIssue":"2015-09-08T00:00:00Z","idIssuePlace":"Egypt","idNumber":"A16220645","mobileNumber":971569803616,"passportCopyFileId":"a161j0000018sgJAAQ","passportDateOfExpiry":"2022-09-07T00:00:00Z","passportDateOfIssue":"2015-09-08T00:00:00Z","passportIssuePlace":"Egypt","passportNumber":"A16220645","previousNationality":"","remarks":"","remarksManager":"","remarksSecretary":"","representativeIndividualId":2513585,"residenceCopyFileId":"NA","udbNumber":0},"shareholderCompany":{"companyNameAR":"Jio","companyNameEN":"Jio","originCountry":"India","ownershipPercent":1,"shareholderCompanyId":2513587,"sharesNumber":1,"sharesValue":1,"telephoneNumber":""},"shareholderIndividual":{"address":",Dubai,Dubai,Dubai,India,12345","area":"DIFC","birthCity":"Dubai","birthCountry":"India","birthDate":"1996-05-01T00:00:00Z","companyNameAR":"ذا ستيك بار ليميتد","companyNameEN":"The Steak Bar Ltd.","companyPosition":"manager","currentNationality":"India","documentTypeId":"national_id","documentTypePassport":"national_id","gender":"M","idCopyFileId":"a161j0000018sgJAAQ","idDateOfExpiry":"2023-05-28T00:00:00Z","idDateOfIssue":"2020-05-13T00:00:00Z","idIssuePlace":"India","idNumber":"HL12345","mobileNumber":"0000","ownershipPercent":1,"passportCopyFileId":"a161j0000018sgJAAQ","passportDateOfExpiry":"2023-05-28T00:00:00Z","passportDateOfIssue":"2020-05-13T00:00:00Z","passportIssuePlace":"India","passportNumber":"HL12345","previousNationality":"","remarks":"","remarksManager":"","remarksSecretary":"","residenceCopyFileId":2,"shareholderIndividualId":2513588,"sharesNumber":1,"sharesValue":1,"udbNumber":0},"amendmentTradeLicenseRequestId":2513591,"amendmentType":"Shareholder","bankStatementFileId":"NA","companyNameAR":"NA","companyNameEN":"The Steak Bar Ltd.","companyPercentage":100,"employeesNumber":1,"establishmentNumber":26205984,"establishmentNumberImmSys":"NA","iban":"","location":"DIFC","notes":{"noteId":2513590,"notes":""},"numberOfLicenses":0,"originCountry":"United Arab Emirates","parentCompanyData":{"bankStatementFileId":"","boardOfDirectorsDecisionFileId":"","companyNameAR":"","companyNameEN":"","companyPercentage":100,"employeesNumber":0,"establishmentContractFileId":"","establishmentNumber":"NA","establishmentNumberImmSys":"NA","iban":"","legalPowerOfAttorneyFileId":"","location":"United Arab Emirates","offshoreContractArticlesOfAssociationsFileId":"","offshoreContractMoUFileId":"","originCountry":"United Arab Emirates","parentCompanyDataId":2513589,"position":"outside_the_country","registrationType":"OFF","representativeContactNumber":"NA","representativeId":"NA","representativeName":"NA","representativeNationality":"","workArea":"DIFC"},"position":"outside_the_country","registrationType":"OFF","representativeContactNumber":971569803616,"representativeId":"NA","representativeName":"","representativeNationality":"Afghanistan","representativeType":"Individual","shareholderType":["Company","Individual"],"workArea":"DIFC"}}
                if(response1.getStatusCode() == 200){
                    system.debug('==inside suv==');
                    BPM_SecurityStepStatusUpdateBatch.DubaiPoliceAmendmentJSONResponse myClass;
                    if(!Test.isRunningTest()){
                        myClass = BPM_SecurityStepStatusUpdateBatch.parse(response1.getBody());
                    }else{
                        myClass = new BPM_SecurityStepStatusUpdateBatch.DubaiPoliceAmendmentJSONResponse();
                    }
                    String Status = myClass.AmendmentDto.requestCommonData.status;
                    String comments = myClass.AmendmentDto.requestCommonData.policeNotes;
                    system.debug('------Status-----'+Status);
                    system.debug('------comments-----'+comments);
                    //Status = 'APPROVED';
                    if(String.isNotBlank(Status) && Status =='APPROVED'){
                        eachStep.Status__c = statuscodeIDMap != null && statuscodeIDMap.size() != 0 && statuscodeIDMap.containskey('VERIFIED') ? statuscodeIDMap.get('VERIFIED'):null;
                        eachStep.Step_Notes__c= comments;
                        eachStep.Closed_Date__c = system.today();
                        lstStepDataUpdate.add(eachStep);
                        //account
                        acct.Security_Review_Date__c= system.now();
                        acct.Security_Review_Status__c= 'Approved';
                        acctList.add(acct);
                    } 
                    else if(String.isNotBlank(Status) && Status =='NEED_MORE_INFO'){
                        eachStep.Status__c = statuscodeIDMap != null && statuscodeIDMap.size() != 0 && statuscodeIDMap.containskey('FORWARD_TO_CUSTOMER_FOR_MORE_INFORMATION') ? statuscodeIDMap.get('FORWARD_TO_CUSTOMER_FOR_MORE_INFORMATION'):null;
                        eachStep.OwnerID= Label.ROC_Officer_Queue;
                        eachStep.Step_Notes__c= comments;
                        lstStepDataUpdate.add(eachStep);
                        //account
                        acct.Security_Review_Date__c= system.now();
                        acct.Security_Review_Status__c= 'Return for More Information';
                        acctList.add(acct);
                    }
                    else if(String.isNotBlank(Status) && Status =='Rejected'){
                        eachStep.Status__c = statuscodeIDMap != null && statuscodeIDMap.size() != 0 && statuscodeIDMap.containskey('REJECTED') ? statuscodeIDMap.get('REJECTED'):null;
                        eachStep.Step_Notes__c= comments;
                        lstStepDataUpdate.add(eachStep);
                        //account
                        acct.Security_Review_Date__c= system.now();
                        acct.Security_Review_Status__c= 'Rejected';
                        acctList.add(acct);
                    }
                }      
            }
            system.debug('==lstStepDataUpdate=='+lstStepDataUpdate);
            system.debug('==acctList=='+acctList);
            if(lstStepDataUpdate!=null && lstStepDataUpdate.size()>0){
                database.update(lstStepDataUpdate);
                database.update(acctList);
                
            }   
        }
        }catch(Exception e){
            string errorresult = e.getMessage();
            insert LogDetails.CreateLog(null, 'BPM_SecurityStepStatusUpdateBatch : execute', 'Line Number : '+e.getLineNumber()+'\nException is : '+errorresult);
        }
    }
    */  
}