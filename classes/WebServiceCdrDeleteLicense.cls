public class WebServiceCdrDeleteLicense {
    
    public static String deleteJsonLicenseCdr(String LicenseId){
        
        JSONGenerator jsonObj = JSON.createGenerator(true);
        jsonObj.writeStartArray();    
        jsonObj.writeStartObject();
        //if(!String.isBlank(Acc.Unified_License_Number__c) && Acc.Unified_License_Number__c != null)
        jsonObj.writeStringField('unifiedNumber','005-0003445-020919');
        jsonObj.writeStringField('zoneName', 'DIFC');
        jsonObj.writeEndObject();
        jsonObj.writeEndArray();
        System.debug('jsonObj:-'+jsonObj.getAsString());
        return jsonObj.getAsString();
    }
    
    public static void deleteLicenseToCdr(String LicenseId){
        
        String jsonRequest;
        List<Log__c> ErrorObjLogList = new List<Log__c>();
        //Get authrization token 
        String Token = WebServiceCDRGenerateToken.cdrAuthToken();
        //WebService to Create the Account
        HttpRequest request = new HttpRequest();
        request.setEndPoint(System.label.Cdr_Post_License_Url);
        jsonRequest = deleteJsonLicenseCdr(LicenseId);
        request.setBody(jsonRequest);
        request.setTimeout(120000);
        request.setHeader('Accept', 'text/plain');
        request.setHeader('Content-Type', 'application/json-patch+json');
        request.setHeader('Authorization','Bearer'+' '+Token);
        request.setMethod('DELETE');
        HttpResponse response = new HTTP().send(request);
        System.debug('responseStringresponse:'+response.toString());
        System.debug('STATUS:'+response.getStatus());
        System.debug('STATUS_CODE:'+response.getStatusCode());
        System.debug('strJSONresponse:'+response.getBody());
        request.setTimeout(110000);
        
        if (response.getStatusCode() == 200)
        {           
            Log__c ErrorobjLog = new Log__c();
            ErrorobjLog.Type__c = 'CDR License Delete';
            ErrorobjLog.Description__c = 'Request: '+ jsonRequest +'Response: '+ response.getBody() ;
            ErrorobjLog.sObject_Record_Id__c = LicenseId;
            ErrorObjLogList.add(ErrorobjLog);
            
        }
        else
        {
            Log__c ErrorobjLog = new Log__c();
            ErrorobjLog.Type__c = 'CDR License Delete Error';
            ErrorobjLog.Description__c = 'Request: '+ jsonRequest +'Response: '+ response.getBody() ;
            ErrorobjLog.sObject_Record_Id__c = LicenseId;
            ErrorObjLogList.add(ErrorobjLog);      
        }
        
        Insert ErrorObjLogList;
        
    }
    
}