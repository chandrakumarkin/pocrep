/******************************************************************************************************
 *  Author   : Kumar Utkarsh
 *  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* This is a scheduler class for WebServiceGdrfaAppStatusEnquiryBatch.
*******************************************************************************************************/
global class WebServiceGdrfaAppStatusEnquiryScheduler implements Schedulable {

   global void execute(SchedulableContext sc) {
      WebServiceGdrfaAppStatusEnquiryBatch appStatusBatch = new WebServiceGdrfaAppStatusEnquiryBatch(); 
      database.executebatch(appStatusBatch, 1);
   }
    
}