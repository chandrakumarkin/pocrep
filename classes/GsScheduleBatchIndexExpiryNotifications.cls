global with sharing class GsScheduleBatchIndexExpiryNotifications implements Database.Batchable<sObject>,Database.AllowsCallouts,Schedulable {


  global void execute(SchedulableContext ctx) {
      Database.executeBatch(new GsScheduleBatchIndexExpiryNotifications(), 100);
    }

      global list<Identification__c> start(Database.BatchableContext BC ){
      
      
         list<Identification__c> lstIdentification    = [select id,Account__c,Valid_To__c  from Identification__c 
                                                    where Account__r.Index_Card_Status__c ='Expired' and Valid_To__c!= null and Identification_Type__c ='Index Card' and 
                                                    Send_Email_Fine__c = false order by Valid_To__c desc ];
                                                    
          return lstIdentification;                                           
    }
    
    global void execute(Database.BatchableContext bc, List<Identification__c> records){
    
       set<Id> lstAccountIds = new set<Id>();
        map<Id,list<string>> mapEmpServiceUsers = new map<Id,list<string>>();
        list<Identification__c> lstIdentification = new list<Identification__c>();
        list<Identification__c> lstIdentificationUpdate = new list<Identification__c>();
        
        for(Identification__c id : records){
       
           if((Date.valueOf(id.Valid_To__c).daysBetween(System.today())/30) > 1){        
             lstAccountIds.add(id.Account__c);
             lstIdentification.add(id);
           } 
       
         }
           if(lstAccountIds!=null && !lstAccountIds.isEmpty()){
             List<User> usr            = new List<User>();
             Set<String> contactIds    = new Set<String>(); 
             
             Map<String,AccountContactRelation> accountContactMap = new  Map<String,AccountContactRelation>();
             
              for(AccountContactRelation ACR: [Select Id,contactId,Roles from AccountContactRelation where AccountId  IN : lstAccountIds and  isActive = true ]){
                  contactIds.add(ACR.contactId);
                  accountContactMap.put(ACR.contactId,ACR);
               }
 
                for(User thisUser : [select Id,ContactId,Contact.AccountId,Community_User_Role__c FROM User where ContactId IN : contactIds and IsActive = true ]){
                     if(accountContactMap.containsKey(thisUser.ContactId) &&
                            accountContactMap.get(thisUser.ContactId).Roles != null &&
                            accountContactMap.get(thisUser.ContactId).Roles.Contains('Employee Services')){
                            
                     list<string> lstUserIds = mapEmpServiceUsers.containsKey(thisUser.Contact.AccountId) ? mapEmpServiceUsers.get(thisUser.Contact.AccountId) : new list<string>();
                     lstUserIds.add(thisUser.Id);
                     
                     if(lstUserIds!=null && !lstUserIds.isEmpty()){
                        mapEmpServiceUsers.put(thisUser.Contact.AccountId,lstUserIds);
                     }
                     
                   }
                 }
             } 

             if(mapEmpServiceUsers!=null && !mapEmpServiceUsers.isEmpty()){
                for(Identification__c obj : lstIdentification){
                    list<string> lst = mapEmpServiceUsers.get(obj.Account__c);
                    if(lst != null && !lst.isEmpty()){
                        Identification__c objTemp = new Identification__c(Id=obj.Id);
                        objTemp.Send_Email_Fine__c= true;
                        objTemp.Fine_Notification_last_Send_Date__c = system.today();
                        if(lst.size() > 0)
                            objTemp.Portal_User_1__c = lst[0];
                        if(lst.size() > 1)
                            objTemp.Portal_User_2__c = lst[1];
                        if(lst.size() > 2)
                            objTemp.Portal_User_3__c = lst[2];
                        if(lst.size() > 3)
                            objTemp.Portal_User_4__c = lst[3];
                        if(lst.size() > 4)
                            objTemp.Portal_User_5__c = lst[4];
                        lstIdentificationUpdate.add(objTemp);
                    }
                }
            }
            
            if(lstIdentificationUpdate!=null && !lstIdentificationUpdate.isEmpty()){
                update lstIdentificationUpdate;
            }            
    
       
    }
    
     global void finish(Database.BatchableContext BC){
    }

}