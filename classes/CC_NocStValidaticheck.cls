/**
 * @description       : 
 * @author            : Shoaib Tariq
 * @group             : 
 * @last modified on  : 05-06-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   05-06-2021   Shoaib Tariq   Initial Version
**/
global with sharing class CC_NocStValidaticheck implements HexaBPM.iCustomCodeExecutable {
  
      global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {

        HexaBPM__Service_Request__c registerWithDifcSR = [SELECT Id,
                                                                HexaBPM__Customer__c,
                                                                HexaBPM__Parent_SR__c,
                                                                Name_of_the_Hosting_Affiliate__c,
                                                                RecordTypeId,Setting_Up__c
                                                           FROM HexaBPM__Service_Request__c 
                                                           WHERE ID =: SR.Id ];

        HexaBPM__Service_Request__c initialApprovalSR = [SELECT Id,
                                                                HexaBPM__Customer__c,
                                                                Name_of_the_Hosting_Affiliate__c
                                                            FROM HexaBPM__Service_Request__c 
                                                            WHERE ID =: registerWithDifcSR.HexaBPM__Parent_SR__c ];

        if((initialApprovalSR.Name_of_the_Hosting_Affiliate__c == null && registerWithDifcSR.Name_of_the_Hosting_Affiliate__c != null)
            || initialApprovalSR.Name_of_the_Hosting_Affiliate__c != registerWithDifcSR.Name_of_the_Hosting_Affiliate__c){

            return 'True'; 
        }

        return 'False';
    }
}