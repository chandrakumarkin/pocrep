@isTest
public class OB_ReviewAndConfirmControllerTest {
    public static testmethod void doTestConstructor(){
        
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        insert listPage;
        
        HexaBPM__Section__c section = new HexaBPM__Section__c();
        section.HexaBPM__Default_Rendering__c=false;
        section.HexaBPM__Section_Type__c='PageBlockSection';
        section.HexaBPM__Section_Description__c='PageBlockSection';
        section.HexaBPM__layout__c='2';
        section.HexaBPM__Page__c =listPage[0].id;
        insert section;
        section.HexaBPM__Default_Rendering__c=true;
        update section;
        
        
        HexaBPM__Section_Detail__c sec= new HexaBPM__Section_Detail__c();
        sec.HexaBPM__Section__c=section.id;
        sec.HexaBPM__Component_Type__c='Input Field';
        sec.HexaBPM__Field_API_Name__c='first_name__c';
        sec.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec.HexaBPM__Default_Value__c='0';
        sec.HexaBPM__Mark_it_as_Required__c=true;
        sec.HexaBPM__Field_Description__c = 'desc';
        sec.HexaBPM__Render_By_Default__c=false;
        insert sec;
        
        HexaBPM__Section__c section1 = new HexaBPM__Section__c();
        section1.HexaBPM__Default_Rendering__c=false;
        section1.HexaBPM__Section_Type__c='CommandButtonSection';
        section1.HexaBPM__Section_Description__c='PageBlockSection';
        section1.HexaBPM__layout__c='2';
        section1.HexaBPM__Page__c =listPage[0].id;
        insert section1;
        section.HexaBPM__Default_Rendering__c=true;
        update section1;
        
        HexaBPM__Section_Detail__c sec1= new HexaBPM__Section_Detail__c();
        sec1.HexaBPM__Section__c=section1.id;
        sec1.HexaBPM__Component_Type__c='Input Field';
        sec1.HexaBPM__Field_API_Name__c='first_name__c';
        sec1.HexaBPM__Object_Name__c='HexaBPM__Service_Request__c';
        sec1.HexaBPM__Default_Value__c='0';
        sec1.HexaBPM__Mark_it_as_Required__c=true;
        sec1.HexaBPM__Field_Description__c = 'desc';
        sec1.HexaBPM__Render_By_Default__c=false;
        insert sec1;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].Entity_Type__c = 'Financial - related';
        insertNewSRs[1].Entity_Type__c = 'Financial - related';
        insertNewSRs[0].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_NF_R').getRecordTypeId();
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('AOR_Financial').getRecordTypeId();
        insert insertNewSRs;
        
         insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs[0].id;
        update insertNewSRs[1];
        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1,new List<String>{'Low'},new List<Integer>{1}, new List<String>{'Low'});
        insert listCS;
        //create amendments
        list<HexaBPM_Amendment__c> listAmendments = new list<HexaBPM_Amendment__c>() ;
        listAmendments = OB_TestDataFactory.createApplicationAmendment(1,listCS,insertNewSRs);
        insert listAmendments;
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;
        
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
		insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insert insertNewSteps;
        //---------------------------------------------------------------------------------------------------
        //
        Test.startTest();
        
        PageReference pageRef = Page.OB_ReviewAndConfirmVF;
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('Id', String.valueOf(insertNewSRs[0].Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(insertNewSRs[0]);
        OB_ReviewAndConfirmController testAccPlan = new OB_ReviewAndConfirmController(sc);
        
        OB_ReviewAndConfirmController.RequestWrap requestWrapperObj = new OB_ReviewAndConfirmController.RequestWrap();
        requestWrapperObj.flowID = listPageFlow[0].Id;
        requestWrapperObj.pageId = listPage[0].Id;
        requestWrapperObj.srId = insertNewSRs[0].Id;
        String reqParam = JSON.serialize(requestWrapperObj);
        
		OB_ReviewAndConfirmController.RespondWrap respObj = new OB_ReviewAndConfirmController.RespondWrap();
		respObj =  OB_ReviewAndConfirmController.fetchSRObjFields(reqParam);   
        OB_ReviewAndConfirmController.updateSRObj(reqParam);
        OB_ReviewAndConfirmController.ValidateWrapper wrapperObj = new OB_ReviewAndConfirmController.ValidateWrapper();
       // wrapperObj = OB_ReviewAndConfirmController.validateSRLines(insertNewSRs[0].Id);
       // wrapperObj = OB_ReviewAndConfirmController.validateSRLines(insertNewSRs[1].Id);
        
        List<HexaBPM__Service_Request__c> details = [Select Id,HexaBPM__Internal_SR_Status__r.Name from HexaBPM__Service_Request__c where HexaBPM__Internal_SR_Status__r.Name = 'Draft'];
        system.debug(details);
        system.debug(details[0].HexaBPM__Internal_SR_Status__r.Name);
        
        OB_ReviewAndConfirmController.CompanyWrapper wrapObj = new OB_ReviewAndConfirmController.CompanyWrapper();
        wrapObj = OB_ReviewAndConfirmController.retrieveCompanyWrapper(listPage[0].Id);
        
        OB_ReviewAndConfirmController.ButtonResponseWrapper btnWrap = new OB_ReviewAndConfirmController.ButtonResponseWrapper();
        btnWrap = OB_ReviewAndConfirmController.getButtonAction(insertNewSRs[0],insertNewSRs[0].Id,listPage[0].Id,listPageFlow[0].Id);
        OB_ReviewAndConfirmController.getLicenseActivities(insertNewSRs[0].Id);
        
        String sridsub = OB_ReviewAndConfirmController.SubmitSR(insertNewSRs[1].Id);
        OB_ReviewAndConfirmController.autoCloseCustomerStep(insertNewSRs[1].Id);
        
       OB_ReviewAndConfirmController.getAmendObj(insertNewSRs[0].id,insertNewSRs[0],listAmendments);
        OB_ReviewAndConfirmController.pageWrap tester = new OB_ReviewAndConfirmController.pageWrap();
        OB_ReviewAndConfirmController.dummytest();
        
        insertNewSRs[1].recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('Commercial_Permission_Renewal').getRecordTypeId();
        update insertNewSRs[1];
        
        requestWrapperObj.srId = insertNewSRs[1].Id;
         reqParam = JSON.serialize(requestWrapperObj);
        
        respObj =  OB_ReviewAndConfirmController.fetchSRObjFields(reqParam); 
        
        Test.stopTest();
    }
}