/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 15/1/2020
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    15/12/2019   shoaib        Created
**********************************************************************************************************************/
public class MiscellaneousServicesController {
    
    Private static final String TYPE_OF_REQUEST  = 'Family Hold Upon Cancellation';
    Private static final String RECORD_TYPE_ID   =   Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Miscellaneous_Services').getRecordTypeId();
    
    private static final Map<String, List<String>> RELATED_TO_MAP = new Map<String, List<String>>{
        'Open Family File'              => new List<String>{'New Dependent Visa Package','Renewal of Dependent Visa Package','Amendment of Dependent Visa','Transfer of Dependent Visa Package','Personal Visit Visa'}, 
        'Family Hold Upon Cancellation' =>  new List<String>{'Cancellation of Employment Visa'},
        'Emirates ID Application'       =>  new List<String>{'New Employment Visa Package','Renewal of Employment Visa Package','Transfer of Employment Visa from (UAE Government/Free Zone) to DIFC','New Dependent Visa Package','Renewal of Dependent Visa Package','Transfer of Dependent Visa Package'},
        'Guarantee Payment for the Son above 18'  =>  new List<String>{'New Dependent Visa Package','Renewal of Dependent Visa Package','Transfer of Dependent Visa Package'},
        'Guarantee payment for spouse'            =>  new List<String>{'New Dependent Visa Package','Renewal of Dependent Visa Package','Transfer of Dependent Visa Package'}
      };
          
    @testvisible
    private static string selectedMapKey ;
    
    @AuraEnabled
    public static wrapperClass getserviceRequest(String recordId){
     
      Service_Request__c thisServiceRequest = new  Service_Request__c();
      Account thisAccount                   = new Account();
        
      User  objUser =  [SELECT Id,
                        		Name,
                        			ContactId,
                        				AccountId,
                        					Phone,
                        						Email,
                        							Contact.Account.Name,
                        								Contact.Account.Active_License__c
                        FROM User
                        WHERE Id= :Userinfo.getUserId()]; 
        
        if(String.isBlank(recordId)){
            thisServiceRequest.Customer__c              = objUser.AccountId;
            thisServiceRequest.License__c               = objUser.Contact.Account.Active_License__c;
            thisServiceRequest.Email__c                 = objUser.Email;
            thisServiceRequest.Send_SMS_To_Mobile__c         = objUser.Phone;
        }
        else{
          thisServiceRequest = [SELECT Id,
                                        Type_of_Request__c,
                                      		Linked_SR__c,Quantity__c,
                                		 		Customer__c,License__c,
                                          			Email__c,Send_SMS_To_Mobile__c
                                        FROM Service_Request__c 
                                        WHERE Id =: recordId limit 1 ];  
        }
        thisAccount = [SELECT Id,
                       			Name,
                       				Active_License__r.Name,
                       					Index_Card__r.Name 
                       FROM Account 
                       WHERE Id=:objUser.AccountId 
                       LIMIT 1];
        
      return new wrapperClass(thisAccount,thisServiceRequest);
    }
    
    @AuraEnabled
    public static string saveRequest(Service_Request__c newServiceRequest,String relatedRequest){
        
         Service_Request__c thisServiceRequest        = new  Service_Request__c();
         thisServiceRequest.Customer__c               = newServiceRequest.Customer__c;
         thisServiceRequest.RecordTypeId              = RECORD_TYPE_ID;
         thisServiceRequest.Type_of_Request__c        = newServiceRequest.Type_of_Request__c;
         thisServiceRequest.Id                        = newServiceRequest.Id;
         thisServiceRequest.Linked_SR__c              = relatedRequest;
        
         if(newServiceRequest.Type_of_Request__c  == TYPE_OF_REQUEST){
           thisServiceRequest.Quantity__c         = newServiceRequest.Quantity__c;
         }
        else{
             thisServiceRequest.Quantity__c = null;
        }
        if(String.isNotBlank( thisServiceRequest.Id)){
            update thisServiceRequest;
        }
        else{
            insert thisServiceRequest;
        }
        return thisServiceRequest.Id;
      }
    
    @AuraEnabled
    public static List<Service_Request__c> getserviceRequestList(String searchKeyWord,String selectedMapKey ){
        String searchKey = searchKeyWord + '%';
        return [SELECT Id,
                		Name,Service_Type__c 
                     FROM Service_Request__c 
                     WHERE Name LIKE: searchKey AND 
                           isClosedStatus__c= false AND 
                    	   Service_Type__c IN :RELATED_TO_MAP.get(selectedMapKey) AND
                           External_Status_Name__c != 'Draft' AND
                           External_Status_Name__c != 'Cancelled' AND
                           SAP_Unique_No__c != null AND
                           Customer__c=:[SELECT Id,
                                               AccountId 
                                             FROM USER 
                                             WHERE Id =: Userinfo.getUserId() LIMIT 1].AccountId 
                                             LIMIT 10 ];
    }
    
    
    
    public class wrapperClass {
       @AuraEnabled
       public Account newAccount {get;set;}
       @AuraEnabled
       public Service_Request__c newSR {get;set;}
       @AuraEnabled
       public Service_Request__c LinkedSR {get;set;}
       
        public wrapperClass(Account newAccount,Service_Request__c newSR){
            this.newAccount = newAccount;
            this.newSR      = newSR;
            
            if(String.isNotBlank(newSR.Id) && newSR.Linked_SR__c != NULL){
                LinkedSR = [SELECT Id,
                            		Name,
                            			Service_Type__c
                            FROM Service_Request__c 
                            WHERE Id=:newSR.Linked_SR__c 
                            LIMIT 1];
            }
        } 
        
    }

}