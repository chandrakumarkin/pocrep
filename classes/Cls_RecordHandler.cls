/******************************************************************************************
 *  Name        : CRM_cls_OpportunityHandler 
 *  Author      : Claude Manahan
 *  Company     : PWC Digital Services
 *  Date        : 2017-2-3
 *  Description : Interface for handling records transactions in triggers
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   02-03-2017   Claude        Created
*******************************************************************************************/
public abstract class Cls_RecordHandler {
	
	/**
	 * Tells if the transaction occurred before the record was saved
	 */
	protected Boolean isBefore;
	
	/**
	 * Tells if the transaction occurred after the record was saved
	 */
	protected Boolean isAfter;
	
	/**
	 * Stores the old versions of the records in an sObject list
	 */
	protected List<sObject> oldRecords;
	
	/**
	 * Stores the new records in an sObject list
	 */
	protected List<sObject> newRecords;
	
	/**
	 * Stores the old versions of the records in a collection mapped by their record IDs
	 */
	protected Map<Id,sObject> oldRecordsMap;
	
	/**
	 * Stores the new records in a collection mapped by their record IDs
	 */
	protected Map<Id,sObject> newRecordsMap;
	
    /**
     * Handles all Update DML Transactions
     */
    public void handle(){
    	
    	isBefore = Trigger.isBefore;
    	isAfter = Trigger.isAfter;
    	
    	oldRecords = Trigger.Old == null ? new List<sObject>() : Trigger.Old;
    	newRecords = Trigger.New == null ? new List<sObject>() : Trigger.New;
    	oldRecordsMap = Trigger.OldMap == null ? new Map<Id,sObject>() : Trigger.OldMap;
    	newRecordsMap = Trigger.NewMap == null ? new Map<Id,sObject>() : Trigger.NewMap;
    	
    	if(Trigger.isInsert){
	  		handleInsert();
	  	} 
	  	
	  	if(Trigger.isUpdate){
	  		handleUpdate();
	  	} 
	  	
	  	if(Trigger.isDelete){
	  		handleDelete();
	  	} 
	  	
	  	if(Trigger.isUndelete){
	  		handleUndelete();
	  	} 
    	
    }
    
    /**
     * Handles all Insert DML Transactions
     */
	protected abstract void handleInsert();
	
	/**
     * Handles all Update DML Transactions
     */
	protected abstract void handleUpdate();
	
	/**
     * Handles all Delete DML Transactions
     */
	protected abstract void handleDelete();
	
	/**
     * Handles all Undelete DML Transactions
     */
	protected abstract void handleUndelete();
    
}