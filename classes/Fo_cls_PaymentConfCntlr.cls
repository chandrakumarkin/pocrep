/***********************************************************************************
 *  Author      : Claude Manahan
 *  Company     : NSI-DMCC
 *  Date        : 18-October-2016
 *  Description : Controls the transaction and other page behaviors of the payment confirmation page for FIT-OUT and EVENT Steps
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
    V.No    Date            Updated By      Description
    V1.0    18-Oct-2016     Claude          Created
    V2.0    27-Jun-2021    Arun           #DIFC2-7665 - Contractor with any rejected wallet refund is having issue in portal balance
 ----------------------------------------------------------------------------------------    
 **********************************************************************************************/
public without sharing class Fo_cls_PaymentConfCntlr {
    
    /**
     * Stores the final calculated portal
     * balance
     */
    public decimal AvailBalance             {get;set;}

    /**
     * Stores the available portal balance
     * of the Contractor
     */
    public decimal AvailablePortalBalance   {get;set;}

    /** 
     * Displas the total price item value
     * of the Service Request
     */
    public decimal TotalPriceItemVal        {get;set;}
    
    /** 
     * Stores the contractor information
     */ 
    @TestVisible
    private Account objacc;
    
    /**
     * This will store the balance that 
     * needs to be charge for PSA
     */
    private Decimal PSAtoCharge;

    /** 
     * Stores the Service Request id
     */
    private String srId;

    /**
     * Stores the step id passsed to the page
     */
    private String stepId;
    
    /**
     * This will store the refund amount if any 
     */
    private Decimal walletRefund;
    
    /**
     * This will store the service request details
     */
    public Service_Request__c objSR         {get;set;}

    /**
     * This will tell if the SR ID has been set
     */
    public Boolean isSrIdSet                {get;set;}

    /**
     * This will tell if the Step ID has been set
     */
    public Boolean isStepIdSet              {get;set;}
    
    /**
     * This will set the color of the balance text
     */
    public string AmountColor               {get;set;}
    
    /**
     * This will store the sr price items
     */
    public List<Sr_Price_item__c> lstSRPriceItems   {get;set;}

    /* Class constructor */
    public FO_Cls_PaymentConfCntlr(){

        init();             // initialize class memembers

        getUrlParams();     // Get the URL parameters
    }

    /** 
     * Initializes the class members
     */
    private void init(){
        // TODO: Add any member that needs to
        //       be initialized in this method
        objacc = new Account();
        PSAtoCharge = 0;
        TotalPriceItemVal = 0;
        walletRefund = 0;
    }

    /**
     * Retrieves URL Get variables passed
     * to the class
     */
    private void getUrlParams(){

        /* This will store the page parameters */
        map<string,string> mapParameters = new map<string,string>();

        stepId = '';
        srId = '';

        if(apexpages.currentPage().getParameters()!=null){

            mapParameters = apexpages.currentPage().getParameters();
            
            /* Get the Service Request and Step Ids */
            if(mapParameters.containsKey('stepId')){ stepId = mapParameters.get('stepId'); } 
            else { ApexPages.addMessage( new ApexPages.message(ApexPages.severity.Error, 'No Step Found.') ); }

            if(mapParameters.containsKey('srId')){ srId = mapParameters.get('srId'); } 
            else { ApexPages.addMessage( new ApexPages.message(ApexPages.severity.Error, 'No Service Request Found.') ); }

            /* Check if the IDs passed are valid */
            isStepIdSet = Id.valueOf(stepId).getSobjectType() == Schema.Step__c.SObjectType;
            isSrIdSet = Id.valueOf(srId).getSobjectType() == Schema.Service_Request__c.SObjectType;
        }

    }

    /**
     * Runs any code that needs to be 
     * executed when the class is called
     */
    public PageReference executeOnLoad(){
        
        // TODO: Add any method or set of code in this
        //       method that needs to be executed during
        //       the class's instantiation  

        if(isStepIdSet && isSrIdSet){   // Only Execute if the step id and service request id is set

            /* Set the portal balance */
            getPortalBalance();
        }
        
        return null;
    }

    /** 
     * Calculates the portal balance of
     * the contractor
     */
    private void getPortalBalance(){

        /* Get the Service Request */
        objSR = [select Id,Linked_SR__c,License_Renewal_Term__c,Name,SR_Template__c,isClosedStatus__c,IsCancelled__c,customer__r.accountnumber,customer__r.Credit_Customer__c,Document_Type__c,
            Document_Detail__c,CreatedDate,Internal_Status_Name__c,Internal_SR_Status__c,External_SR_Status__c,External_SR_Status__r.Type__c,Internal_SR_Status__r.Type__c,License__c,customer__r.Portal_Balance__c,customer__r.BG_Balance__c,
            External_SR_Status__r.Code__c,Internal_SR_Status__r.Code__c,Portal_Service_Request_Name__c,Lease__c,RecordType.Name,RecordType.DeveloperName,Record_Type_Name__c,Customer__c,Contact__c,External_Status_Name__c,Submitted_DateTime__c,
            Customer__r.BP_No__c,No_Change_in_the_Data_Protection_Permit__c,SR_Template__r.Estimated_Hours__c,Due_Date__c,SR_Template__r.Menu__c,
            Submitted_Date__c,License__r.License_Expiry_Date__c,Change_Activity__c,Change_Address__c,Document_Detail__r.Contact__c, //Apply_for_new_address_on_this_license__c,
            Statement_of_Undertaking__c,Customer__r.Next_Renewal_Date__c,Customer__r.Has_Permit_1__c,Customer__r.Has_Permit_2__c,Customer__r.Has_Permit_3__c,Registered_Office_Address__c,Is_From_Back_Office__c,
            Service_Category__c,SR_Template__r.Is_Exit_Process__c,SR_Group__c,Account_Legal_Entity__c,I_agree__c,Service_Type__c,
            Occupation_GS__c,Occupation_GS__r.Name,Contractor__r.BP_No__c, 
            Contractor__c,                      
            Contractor__r.Credit_Customer__c,   
            Contractor__r.Portal_Balance__c,    
            Contractor__r.BG_Balance__c,        
            (select id,Name,Price__c,Product__c,Product__r.ProductCode,Pricing_Line__c,Base_Price__c,Price_In_USD__c,Discount_Amount__c,Status__c,ServiceRequest__c,Sys_Added_through_Code__c,Pricing_Line__r.Product__r.Name,Product__r.Name,Pricing_Line__r.Name from SR_Price_Items1__r) 
            from Service_Request__c where Id=:SRID]; 

        decimal AccountPriceItemTotal = 0;
        decimal AccountReceiptsTotal = 0;
                                               
        for(SR_Price_Item__c SRPI:[select Price__c from SR_Price_Item__c where ServiceRequest__c!=null and Price__c!=null and ServiceRequest__r.Contractor__c=:objSR.Contractor__c and (Status__c='Blocked' OR Status__c='Consumed')]){
            AccountPriceItemTotal += SRPI.Price__c;
        }
                                
        for(Receipt__c Recp:[select Amount__c from Receipt__c where Customer__c=:objSR.Contractor__c and Amount__c!=null and Payment_Status__c='Success' and (Pushed_to_SAP__c=false or (Pushed_to_SAP__c=true and SAP_Receipt_No__c=null))]){
            AccountReceiptsTotal += Recp.Amount__c; 
        }

        system.debug('AccountPriceItemTotal===>'+AccountPriceItemTotal);
        system.debug('AccountReceiptsTotal===>'+AccountReceiptsTotal);

        if(objSR.SR_Price_Items1__r != null && !objSR.SR_Price_Items1__r.isEmpty()){
            
            integer priceItemCount = 0;

            for(SR_Price_Item__c PI:objSR.SR_Price_Items1__r){

                if(PI.Sys_Added_through_Code__c==false){
                   
                    if(PI.Pricing_Line__r.Product__r.Name != 'PSA'){
                        TotalPriceItemVal += PI.Price__c;
                    } else {
                        PSAtoCharge += PI.Price__c;
                    }
                        
                } else{
                    
                    if(objSR.License_Renewal_Term__c!=null && objSR.License_Renewal_Term__c!='' && objSR.License_Renewal_Term__c.isNumeric()){
                        
                        if(PI.Base_Price__c!=null){
                            objSR.SR_Price_Items1__r[priceItemCount].Price__c = PI.Base_Price__c * integer.valueOf(objSR.License_Renewal_Term__c);
                        }
                        
                        //Re-Calculating the SR-Price in case of License renewal process 
                        if(objSR.SR_Price_Items1__r[priceItemCount].Discount_Amount__c!=null && integer.valueOf(objSR.License_Renewal_Term__c)>=2){
                           objSR.SR_Price_Items1__r[priceItemCount].Price__c = objSR.SR_Price_Items1__r[priceItemCount].Price__c - objSR.SR_Price_Items1__r[priceItemCount].Discount_Amount__c;
                        }
                    } 
                    
                    TotalPriceItemVal += objSR.SR_Price_Items1__r[priceItemCount].Price__c;
                }
                
                priceItemCount = priceItemCount+1;
            }
            
            lstSRPriceItems = objSR.SR_Price_Items1__r;
        }

        if(String.isNotBlank(objSr.Contractor__c) && Bypass_GetBalance_Callout__c.getAll()!=null && Bypass_GetBalance_Callout__c.getAll().size()>0){
            
            system.debug('******* Code executed under Bypass available balance webservice *********');
            
            /* V1.22 - Claude - Start */
            objacc.Id = objSR.Contractor__c;
            objacc.Credit_Customer__c = objSR.Contractor__r.Credit_Customer__c;
            objacc.Portal_Balance__c = objSR.Contractor__r.Portal_Balance__c;
            objacc.BG_Balance__c = objSR.Contractor__r.BG_Balance__c;
            AvailBalance = objSR.Contractor__r.Portal_Balance__c;
            /* V1.22 - Claude - End */
            
            if(AvailBalance!=null){
                AvailablePortalBalance = AvailBalance - AccountPriceItemTotal + AccountReceiptsTotal;
            }

            checkBalance();

        } else{
            AccountBalanceInfo();
        }

    }

    /**
     * Gets the contractor's portal balance
     */
    private void AccountBalanceInfo(){
        
        try{
            list<AccountBalenseService.ZSF_S_ACC_BALANCE> items = new list<AccountBalenseService.ZSF_S_ACC_BALANCE>();
            
            AccountBalenseService.ZSF_S_ACC_BALANCE item = new AccountBalenseService.ZSF_S_ACC_BALANCE();
            
            item.KUNNR = objSR.Contractor__r.BP_No__c; // V1.18 - Commented by Claude - Replaced by bpNumber variable
            
            //item.BUKRS = '5000';
            item.BUKRS = system.label.CompanyCode5000;
            
            items.add(item);
            
            AccountBalenseService.ZSF_TT_ACC_BALANCE objTTActBal = new AccountBalenseService.ZSF_TT_ACC_BALANCE();
            objTTActBal.item = items;
            
            AccountBalenseService.account objActBal = new AccountBalenseService.account();
            objActBal.timeout_x = 100000;
            objActBal.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
            objActBal.inputHttpHeaders_x= new Map<String, String>();
           
            String decryptedSAPPassWrd  = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :WebService_Details__c.getAll().get('Credentials').Password__c ; 
            
            Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+decryptedSAPPassWrd);    // Added for V1.20 
            
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            
            objActBal.inputHttpHeaders_x.put('Authorization', authorizationHeader);
            
            AccountBalenseService.Z_SF_ACCOUNT_BALANCEResponse_element objAccountBalResponse= objActBal.Z_SF_ACCOUNT_BALANCE(objTTActBal);
            
            system.debug('objAccountBalResponse '+objAccountBalResponse);
            
            map<string,decimal[]> mapAccountBalInfo = new map<string,decimal[]>();
            
            if(objAccountBalResponse.EX_SF_RS_ACC != null && objAccountBalResponse.EX_SF_RS_ACC.item != null){
                for(AccountBalenseService.ZSF_S_ACC_BAL objAccountBalInfo : objAccountBalResponse.EX_SF_RS_ACC.item){
                    if(objAccountBalInfo.KUNNR != null){
                        decimal[] dActBals;
                        if(mapAccountBalInfo.containsKey(objAccountBalInfo.KUNNR))
                            dActBals = mapAccountBalInfo.get(objAccountBalInfo.KUNNR);
                        else
                            dActBals = new decimal[]{0,0,0,0};
                            
                        if(objAccountBalInfo.UMSKZ == 'D')
                            dActBals[0] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                        if(objAccountBalInfo.UMSKZ == 'H')
                            dActBals[1] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                        if(objAccountBalInfo.UMSKZ == '9')
                            dActBals[2] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;    
                        if(objAccountBalInfo.UMSKZ == 'O') //V1.18 - Claude - Added new condition for contractors
                            dActBals[3] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;    
                        
                        mapAccountBalInfo.put(objAccountBalInfo.KUNNR,dActBals);
                    }
                }//end of for 
            }
            
            if(!mapAccountBalInfo.isEmpty()){
               
                AvailablePortalBalance = mapAccountBalInfo.get(objSR.Contractor__r.BP_No__c)[3]; //V1.18 - Claude - Commented > Changed from customer to contractor
                
                decimal dSRPriceItemNotSentAmt = 0,PSAAmount=0;
                Boolean HasPSAItem = false;
                
                system.debug('AvailablePortalBalance is : '+AvailablePortalBalance);
                
                List<SR_Price_Item__c> objSrPriceItems = [SELECT Price__c, Pricing_Line__r.Product__r.Name FROM SR_Price_Item__c WHERE (Status__c='Blocked' OR Status__c='Consumed') AND ServiceRequest__c!=null AND Price__c!=null AND ServiceRequest__r.Contractor__c = :objSR.Contractor__c];
                
                for(SR_Price_Item__c objSRPriceItem : objSrPriceItems){
                    dSRPriceItemNotSentAmt += objSRPriceItem.Price__c;
                }

                system.debug('dSRPriceItemNotSentAmt is : '+dSRPriceItemNotSentAmt);
                
                for(Receipt__c objReceipt : [select Amount__c from Receipt__c where Customer__c=:objSR.Contractor__c and Amount__c!=null and Payment_Status__c='Success' and Pushed_to_SAP__c=false]){
                    AvailablePortalBalance += objReceipt.Amount__c;
                    system.debug('AvailablePortalBalance is : '+AvailablePortalBalance);
                }
                
                system.debug('PSAtoCharge $$$ '+PSAtoCharge);

                AvailablePortalBalance = AvailablePortalBalance - dSRPriceItemNotSentAmt;
                
                if(objSR.Record_Type_Name__c != 'DIFC_Sponsorship_Visa_Renewal'){
                    TotalPriceItemVal += PSAtoCharge;
                }
                
                checkBalance();
            }
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please try after sometime, server seems busy. '));
        }
    }
    
    /**
     * Confirms the payment of the SR
     * and deducts to the contractor's
     * balance
     */ 
    public pagereference confirmPayment(){
        
        Step__c stepToUpdate = [SELECT Id, SR_Step__c FROM Step__c WHERE Id =:stepId];
        
        String ExtSRStatus = '';
        String IntSRStatus = '';
        
        for(Step_Transition__c CurrentStepTrans:[select Name,Evaluate_Refund__c,Parent_SR_Status__c,Parent_Step_Status__c,SR_Status_External__c,SR_Status_Internal__c,SR_Step__c,Transition__c,Transition__r.From__c,Transition__r.To__c,Transition__r.To__r.Type__c,Transition__r.To__r.Code__c from Step_Transition__c where SR_Step__c=:stepToUpdate.SR_Step__c and Transition__c!=null]){
            ExtSRStatus = CurrentStepTrans.SR_Status_External__c;
            IntSRStatus = CurrentStepTrans.SR_Status_Internal__c;
        }
        
        system.debug('SubmitSR===>');
        string msg;

        if(objSR.contractor__c!=null && objacc.Credit_Customer__c!='Yes' && TotalPriceItemVal!=null && TotalPriceItemVal>0 && AvailablePortalBalance-walletRefund<TotalPriceItemVal && objSR.SR_Template__r.Is_Exit_Process__c == false ){//V1.17
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You dont have enough balance to Process the Request, Please click on the Top-up Balance in the Sidebar.'));
            return null;
        } 
        
        Map<String,String> mapOfStatus = new Map<String,String>();

        /* Get the step statuses */
        for(Status__c obj : [select id,name from Status__c where code__c!=null]){
            mapOfStatus.put(obj.name, obj.id);
        }
         
        if(mapOfStatus!=null && mapOfStatus.size()>0){

            Savepoint Price_Item_svpoint = Database.setSavepoint();

            stepToUpdate.Status__c = mapOfStatus.get('Completed');

            try{

                //added by ravi on 24th Dec, if there is any failure in WS price won't be moved to SAP and we are deducting from Protal Balance 
                list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
                
                for(SR_Price_Item__c objItem : [select Id,Status__c,Company_Code__c,ServiceRequest__r.Record_Type_Name__c,Pricing_Line__r.Product__r.Name from SR_Price_Item__c where Status__c != 'Invoiced' AND ServiceRequest__c=:SRID AND Price__c > 0]){
                    SR_Price_Item__c objItemTemp = new SR_Price_Item__c(Id=objItem.Id);
                    if(objSR.SR_Template__r.Menu__c == 'Employee Services'){
                        if(objItem.Pricing_Line__r.Product__r.Name == 'PSA' && PSAtoCharge == 0)
                            objItemTemp.Status__c = 'Utilized';
                        else{   
                            objItemTemp.Status__c = 'Blocked';
                            if(objItem.Pricing_Line__r.Product__r.Name == 'PSA')
                                objItemTemp.Price__c = PSAtoCharge;
                        }   
                    }else{
                        //objItemTemp.Status__c = (objItem.Company_Code__c=='1100' && objItem.ServiceRequest__r.Record_Type_Name__c!='License_Renewal')?'Blocked':'Consumed';
                        objItemTemp.Status__c = (objItem.Company_Code__c== System.label.CompanyCode1100 && objItem.ServiceRequest__r.Record_Type_Name__c!='License_Renewal')?'Blocked':'Consumed';
                    }

                    lstPriceItems.add(objItemTemp);
                }

                if(!lstPriceItems.isEmpty())
                    update lstPriceItems;

                update stepToUpdate;
                
                update new Service_Request__c(id = SRID, External_SR_Status__c = ExtSRStatus, Internal_SR_Status__c = IntSRStatus);

                SAPWebServiceDetails.ECCServiceCallForSchedule(new list<string>{SRID},true);

            } catch(DMLException e){
                
                Database.rollback(Price_Item_svpoint);
              
                string strDMLError;
                strDMLError = string.valueOf(e.getdmlMessage(0));
              
                if(strDMLError!=null && strDMLError!='' && strDMLError.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION')>-1){
                    
                    list<string> lstMsg = strDMLError.split('FIELD_CUSTOM_VALIDATION_EXCEPTION');
                    
                    if(lstMsg!=null && lstMsg.size()>=2){
                        strDMLError = lstMsg[1];
                    }
                }
              
                if(strDMLError!=null && strDMLError!='')
                    strDMLError = strDMLError.replace(': []','');
                
                if(strDMLError!=null && strDMLError!='')
                    strDMLError = strDMLError.replace(',','');
              
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,strDMLError));
                
                Log__c objLog = new Log__c();
                objLog.Type__c = 'SR Submission Error';
                objLog.Description__c = 'Exception Line Number : '+e.getLineNumber()+'\nException is : '+e.getMessage();
                insert objLog; 
                return null;
            }
        }

        return backToStep();
    }
    
    /**
     * Checks if the contractor has enough
     * balance to proceed
     */
    private void checkBalance(){
        
        //V2.0  //Updated the condition and filtered the rejected refunds - External_SR_Status__r.Name != 'Rejected' - V2.0
        List<Service_Request__C> openWalletRefunds = [SELECT Id, Refund_Amount__c FROM Service_Request__c WHERE  // TODO: Change field of amount
                                                        Customer__c =:objSR.Contractor__c AND
                                                        External_SR_Status__r.Name != 'Draft' AND 
                                                         External_SR_Status__r.Name != 'Rejected' AND
                                                        External_SR_Status__r.Name != 'Approved' AND
                                                        Refund_Amount__c != null AND Refund_Amount__c != 0];      // TODO: Completion status
        
        if(!openWalletRefunds.isEmpty()){
            walletRefund = openWalletRefunds[0].Refund_Amount__c;
        }
        
        if(String.isNotBlank(objSR.customer__c) 
            && String.isNotBlank(objSr.Contractor__c)
            && TotalPriceItemVal!=null 
            && TotalPriceItemVal>0 ){
            
            AmountColor = 'red';    
            
            if(AvailablePortalBalance<TotalPriceItemVal ){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'You dont have enough balance to process the request. Please recharge your wallet.'));
            } else if(AvailablePortalBalance - walletRefund < TotalPriceItemVal){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'There is still an open refund request of '+walletRefund+' AED. Please recharge your wallet to at least ' + TotalPriceItemVal + ' AED to continue.'));
            } else {
                AmountColor = 'green';    
            }
            
        } else {
            AmountColor = 'green';
        }
        
    }
    
    /**
     * Returns to the Step
     */
    public PageReference backToStep(){
        
        pagereference ref = new pagereference('/'+stepId); 
        ref.setRedirect(true);
        
        return ref;
    }
    
    public static void DummyTest(){
        integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++; i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
         i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
    }
    
}