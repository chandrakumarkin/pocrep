/*********************************************************************************************************************
--------------------------------------------------------------------------------------------------------------------------
Creation History 
----------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
----------------------------------------------------------------------------------------              
V1.1    26-May-2021  Vinod        exposed company info to community user 

**********************************************************************************************************************/

@RestResource(urlMapping = '/DIFCUserInfo/*')
global class CLS_DIFC_UserInfo{
    
    @HttpPost
    global static UserInfo getUserInfo(string userId) {
         UserInfo result = new UserInfo();
        try{
            List<AccountInfo> accInfoList = new List<AccountInfo>();
            Id portalRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
            if(userId ==null || userId ==''){
                result.success = false;
                result.accounts = null;
                return result;
            }
            User usr = [Select Id,ContactId from user WHERE id=:userId];
            for(AccountContactRelation accConRel : [Select Id,Account.Name,Account.Registration_License_No__c FROM AccountContactRelation WHERE ContactId=:usr.ContactId AND IsActive=True AND Contact.RecordTypeId=:portalRecordTypeId]){
                AccountInfo res = new AccountInfo();
                res.Name = accConRel.account.Name;
                res.Registration_License_No = accConRel.account.Registration_License_No__c;
                accInfoList.add(res);
            }
            result.accounts = accInfoList;
            result.success = true;
            return result;
        }catch(exception ex){
            result.success = false;
            result.accounts = null;
            return result;
        }
    }
    global class UserInfo {
        global boolean success;
        global List<AccountInfo> accounts;
    } 
    global class AccountInfo{
        global string Name;
        global string Registration_License_No;
    }
}