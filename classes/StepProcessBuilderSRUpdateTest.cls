/**

 */
@isTest
private class StepProcessBuilderSRUpdateTest {

    static testMethod void myUnitTest() {
        
      List<Account> lstAccount = new List<Account>();
   		Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        lstAccount.add(objAccount);
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
   
     map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
   
      
        
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'Fit - Out Service Request';
        objTemplate1.SR_RecordType_API_Name__c = 'Fit - Out Service Request';
        objTemplate1.Active__c = true;
        insert objTemplate1;
        
        
        
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Entry Permit is Issued';
        objStepType1.Code__c = 'Entry Permit is Issued';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Entry Permit is Issued';
        insert objStepType1;
        
        Step_Template__c objStepType2 = new Step_Template__c();
        objStepType2.Name = 'Original Passport Received';
        objStepType2.Code__c = 'Original Passport Received';
        objStepType2.Step_RecordType_API_Name__c = 'General';
        objStepType2.Summary__c = 'Original Passport Received';
        insert objStepType2;
        
        
        
    Service_Request__c objSR3 = new Service_Request__c();
    objSR3.Customer__c = objAccount.Id;
    objSR3.Email__c = 'testsr@difc.com';    
    objSR3.Contact__c = objContact.Id;
    objSR3.Express_Service__c = false;
    objSR3.SR_Template__c = objTemplate1.Id;
    
    insert objSR3;
    
    Status__c objStatus = new Status__c();
    objStatus.Name = 'Pending';
    objStatus.Type__c = 'Pending';
    objStatus.Code__c = 'Pending';
    insert objStatus;
    Status__c objStatus1 = new Status__c();
    objStatus1.Name = 'Completed';
    objStatus1.Type__c = 'Completed';
    objStatus1.Code__c = 'Completed';
    insert objStatus1;
        
        step__c objStep = new Step__c();
        objStep.SR__c = objSR3.Id;
        //objStep2.OwnerId = mapNameOwnerId.get('Customer');
        //objStep2.Biometrics_Required__c = 'No';
        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        //objStep.Step_Name__c='Landlord NOC Issued';
        objStep.status__c = objStatus.id;
        insert objStep;
        
        step__c objStep1 = new Step__c();
        objStep1.SR__c = objSR3.Id;
        //objStep2.OwnerId = mapNameOwnerId.get('Customer');
        //objStep2.Biometrics_Required__c = 'No';
        objStep1.Step_Template__c = objStepType2.id ;
        objStep1.No_PR__c = false;
        //objStep.Step_Name__c='Landlord NOC Issued';
        objStep1.status__c = objStatus.id;
        insert objStep1;
        
        //Step__c stepRecord = [Select id,step_Name__c from Step__c where id=:objStep.id];
        // system.assertEquals(null, stepRecord);
        
        Profile profile1 = [Select Id from Profile where name = 'DIFC Customer Community Plus User Custom'];
        User portalAccountOwner1 = new User(ProfileId = profile1.Id,Username = System.now().millisecond() + 'test2@test.com',ContactId=objContact.Id,
                                            Alias = 'TestDIFC',Email='test123@Difc.com',EmailEncodingKey='UTF-8',Firstname='Test',Lastname='DIFC',
                                            LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Chicago',isActive=true,Community_User_Role__c='Company Services');
        Database.insert(portalAccountOwner1);
         
        system.runas(portalAccountOwner1){
        	try{
        		StepProcessBuilderSRUpdate.GsStepProcessbuilderActionsSRUpdate(new List<id>{objStep.Id});
        		StepProcessBuilderSRUpdate.GsStepProcessbuilderActionsSRUpdate(new List<id>{objStep1.Id});
        	}
        		catch(exception ex){
        		
        	}
        }
        
   }
   static testMethod void myUnitTest2() {
        
      List<Account> lstAccount = new List<Account>();
   		Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        lstAccount.add(objAccount);
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
   
     map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
   
      
        
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'Fit - Out Service Request';
        objTemplate1.SR_RecordType_API_Name__c = 'Fit - Out Service Request';
        objTemplate1.Active__c = true;
        insert objTemplate1;
        
        
        
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Entry Permit is Issued';
        objStepType1.Code__c = 'Entry Permit is Issued';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Entry Permit is Issued';
        insert objStepType1;
        
        Step_Template__c objStepType2 = new Step_Template__c();
        objStepType2.Name = 'Original Passport Received';
        objStepType2.Code__c = 'Original Passport Received';
        objStepType2.Step_RecordType_API_Name__c = 'General';
        objStepType2.Summary__c = 'Original Passport Received';
        insert objStepType2;
        
        Step_Template__c objStepType3 = new Step_Template__c();
        objStepType3.Name = 'Original passport submission';
        objStepType3.Code__c = 'Original passport submission';
        objStepType3.Step_RecordType_API_Name__c = 'General';
        objStepType3.Summary__c = 'Original passport submission';
        insert objStepType3;
        
        
        
    Service_Request__c objSR3 = new Service_Request__c();
    objSR3.Customer__c = objAccount.Id;
    objSR3.Email__c = 'testsr@difc.com';    
    objSR3.Contact__c = objContact.Id;
    objSR3.Express_Service__c = false;
    objSR3.SR_Template__c = objTemplate1.Id;
    
    insert objSR3;
    
    Status__c objStatus = new Status__c();
    objStatus.Name = 'Closed';
    objStatus.Type__c = 'Closed';
    objStatus.Code__c = 'Closed';
    insert objStatus;
    Status__c objStatus1 = new Status__c();
    objStatus1.Name = 'Completed';
    objStatus1.Type__c = 'Completed';
    objStatus1.Code__c = 'Completed';
    insert objStatus1;
        
        step__c objStep = new Step__c();
        objStep.SR__c = objSR3.Id;
        //objStep2.OwnerId = mapNameOwnerId.get('Customer');
        //objStep2.Biometrics_Required__c = 'No';
        objStep.Step_Template__c = objStepType1.id ;
        objStep.No_PR__c = false;
        //objStep.Step_Name__c='Landlord NOC Issued';
        objStep.status__c = objStatus.id;
        insert objStep;
        
        step__c objStep1 = new Step__c();
        objStep1.SR__c = objSR3.Id;
        //objStep2.OwnerId = mapNameOwnerId.get('Customer');
        //objStep2.Biometrics_Required__c = 'No';
        objStep1.Step_Template__c = objStepType2.id ;
        objStep1.No_PR__c = false;
        //objStep.Step_Name__c='Landlord NOC Issued';
        objStep1.status__c = objStatus.id;
        insert objStep1;
        
        step__c objStep2 = new Step__c();
        objStep2.SR__c = objSR3.Id;
        //objStep2.OwnerId = mapNameOwnerId.get('Customer');
        //objStep2.Biometrics_Required__c = 'No';
        objStep2.Step_Template__c = objStepType3.id ;
        objStep2.No_PR__c = false;
        //objStep.Step_Name__c='Landlord NOC Issued';
        objStep1.status__c = objStatus.id;
        insert objStep2;
        
        
        //Step__c stepRecord = [Select id,step_Name__c from Step__c where id=:objStep.id];
        // system.assertEquals(null, stepRecord);
        
        Profile profile1 = [Select Id from Profile where name = 'DIFC Customer Community Plus User Custom'];
        User portalAccountOwner1 = new User(ProfileId = profile1.Id,Username = System.now().millisecond() + 'test2@test.com',ContactId=objContact.Id,
                                            Alias = 'TestDIFC',Email='test123@Difc.com',EmailEncodingKey='UTF-8',Firstname='Test',Lastname='DIFC',
                                            LanguageLocaleKey='en_US',LocaleSidKey='en_US',TimeZoneSidKey='America/Chicago',isActive=true,Community_User_Role__c='Company Services');
        Database.insert(portalAccountOwner1);
         
        system.runas(portalAccountOwner1){
        	try{
        		StepProcessBuilderSRUpdate.GsStepProcessbuilderActionsSRUpdate(new List<id>{objStep1.Id});
        		StepProcessBuilderSRUpdate.GsStepProcessbuilderActionsSRUpdate(new List<id>{objStep.Id});
        	}
        		catch(exception ex){
        		
        	}
        }
        
   }
}