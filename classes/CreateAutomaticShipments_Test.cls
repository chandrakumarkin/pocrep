@isTest
public class CreateAutomaticShipments_Test {
  
    static testMethod void CreateAutomaticShipments(){
        Status__c sts = new Status__c();
        sts.Name = 'PENDING_COLLECTION';
        sts.Code__c = 'PENDING_COLLECTION';
        insert sts;
        
        Step_Template__c testSrTemplate = new Step_Template__c();    
        testSrTemplate.Name = 'Courier Collection';
        testSrTemplate.Code__c = 'Courier Collection';
        testSrTemplate.Step_RecordType_API_Name__c = 'Courier Collection';
       insert testSrTemplate;
        
      Step__c thisStep = new Step__c();
      thisStep.Step_Template__c = testSrTemplate.Id;
      thisStep.Status__c = sts.Id;
      Insert thisStep;
        
      Test.startTest();
           SchedulableContext sc = null;
           CreateAutomaticShipments obj = new CreateAutomaticShipments();
           obj.execute(sc);
      Test.stopTest();
    }

}