/**
 * @description       : used in Process builder name:  DFSA Cancellation Price Items
 * @author            : Veera
 * @group             : 
 * @last modified on  : 01-05-2021
 * @last modified by  : Veera
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   01-04-2021   Veera Initial Version
**/

Public class GSDFSACancPriceItemController {


   @InvocableMethod(Label = 'DFSACancellationCardPriceItems' description = 'DFSACancellationCardPriceItems')
    public static void GSDFSACancPriceItemsMethod(List < id > ids) { 
    
        GSDFSACancPriceItemsFuture(ids);
    
       }
    
    // @future(callout = true)
    public static void GSDFSACancPriceItemsFuture(List < id > ids) {
    
    List<string> RelTypes = new List<string> {'Has DIFC Local /GCC Employees','Other Govt Sponsored Emp'};
    
       Service_request__C objSR = [select id,External_Status_Name__c ,Contact__c,Customer__c from Service_request__C  where id =: ids[0] limit 1];
      
       List<Relationship__c>  ListRel = new List<Relationship__c> ();
      //       [select id from Relationship__c  where  Subject_Account__c  =:objSR.Customer__c AND Active__c= true  AND Relationship_Type__c in : RelTypes AND Object_Contact__c = : objSR.Contact__c]; 
        Boolean isLocal = false;
        for (Relationship__c Rel : [select id,Relationship_Type__c from Relationship__c  where  Subject_Account__c  =:objSR.Customer__c AND Active__c= true  AND Relationship_Type__c in : RelTypes AND Object_Contact__c = : objSR.Contact__c]){
            ListRel.add(Rel);
            if(rel.Relationship_Type__c == 'Has DIFC Local /GCC Employees') isLocal = true;
         }
      
       
       if (ListRel.size()>0){
       
          CreateDFSAPriceItemsLocal(objSR,isLocal);
       }
    
    }
    
    public static void CreateDFSAPriceItemsLocal(Service_request__C objSR, boolean isLocal){
    
     list<SR_Price_Item__c> priceItemInsert = new list<SR_Price_Item__c>();
    
     list<Product2> productList = [select id from Product2 where ProductCode = 'Non-Sponsored Employment Cancellation'];
           if(objSR.id!=null && (objSR.External_Status_Name__c == 'Draft' || string.isBlank(objSR.External_Status_Name__c))){
           
              list<SR_Price_Item__c> tempListToDeletePrice = new list<SR_Price_Item__c>();
              tempListToDeletePrice = [select id from SR_Price_Item__c where ServiceRequest__c=:objSR.id];
               if(tempListToDeletePrice!=null && tempListToDeletePrice.size()>0){
                   //delete tempListToDeletePrice;
             }
             Pricing_Line__c pricingLine = new Pricing_Line__c();
             if(isLocal == true){
             
             list<Pricing_Line__c> tempList = [select id,(select Unit_Price__c,Unit_Price_in_USD__c from Dated_Pricing__r) from Pricing_Line__c where DEV_Id__c=:'GO-00229_Zero'];
                if(tempList!=null && !tempList.isEmpty()){
                    pricingLine = tempList[0];
                }
            }else {
            
            list<Pricing_Line__c> tempList = [select id,(select Unit_Price__c,Unit_Price_in_USD__c from Dated_Pricing__r) from Pricing_Line__c where DEV_Id__c=:'GO-00341'];
                if(tempList!=null && !tempList.isEmpty()){
                    pricingLine = tempList[0];
                }
            }   
                
                SR_Price_Item__c tempObj = new SR_Price_Item__c();
                tempObj.ServiceRequest__c = objSR.id;
                tempObj.Product__c = productList[0].id; 
                tempObj.Pricing_Line__c = pricingLine.id;
                tempObj.Price__c = pricingLine.Dated_Pricing__r[0].Unit_Price__c;
                tempObj.Price_in_USD__c = pricingLine.Dated_Pricing__r[0].Unit_Price_in_USD__c;
                tempObj.Sys_Added_through_Code__c = true;
                priceItemInsert.add(tempObj);
                
                if(priceItemInsert!=null && !priceItemInsert.isEmpty()){
                upsert priceItemInsert;
            }
         }
    
    }


}