@isTest
public class CasesListViewControllerTest {
    @isTest
    private static void init(){

        
        Case ca = new Case(Subject='Test Controller Acct Case');
        insert ca;

        List<Case> cases = new List<Case>();
        cases.add([ Select Id From Case Where Id = : ca.Id ]);

        // start the test execution context
        Test.startTest();

        // load the page       
        PageReference pageRef = Page.InvalidCases_ListView;
        pageRef.getParameters().put('Id',ca.Id);
        Test.setCurrentPageReference(pageRef);

        //load the controller extension
        //CasesListViewController crc = new CasesListViewController(new ApexPages.StandardSetController( cases ));
        
        //crc.markInvalidCases();
        //crc.updateCases();
        
        
        ApexPages.StandardSetController sc = new ApexPages.StandardSetController(cases);
        sc.setSelected(cases);   
        CasesListViewController ctrlr = new CasesListViewController(sc);
		ctrlr.markInvalidCases();
        ctrlr.updateCases();
        Test.stopTest();




    }
}