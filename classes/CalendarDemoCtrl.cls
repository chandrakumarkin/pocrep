public class CalendarDemoCtrl{
        
           public list<calEvent> events {get;set;}
           public string hiddenValue {get;set;}
             string SDateTime;
            string EDateTime;
            User currentUser ;
            
            
             
           public void saveEvent(){
           
             system.debug('%%%%%%%'+ hiddenValue.split(' ')[4] + hiddenValue.split(' ')[5]);
           
           }

            
            
           public CalendarDemoCtrl(){
           
              events = new list<calEvent>();
                 currentUser = [Select TimeZoneSidKey from User where id =: USerInfo.getUserId() limit 1]; 
               
             
              
              
              for(Appointment__c iEvt  : [Select id,Appointment_StartDate_Time__c,Appointment_EndDate_Time__c,step__c,Subject__c from Appointment__c where Appointment_StartDate_Time__c  > = YESTERDAY order by Appointment_StartDate_Time__c desc ]){
                      CalEvent cl = new CalEvent();
                  SDateTime = string.valueOf(iEvt.Appointment_StartDate_Time__c);//.format('yyyy/MM/dd HH:mm:ss', currentUser.TimeZoneSidKey);
                  EDateTime = string.valueOf(iEvt.Appointment_EndDate_Time__c);// .format('yyyy/MM/dd HH:mm:ss', currentUser.TimeZoneSidKey);
                  cl.title = iEvt.Subject__c ;
                  cl.AllDay = false;
                  cl.startString = SDateTime;
                  cl.endString = EDateTime;
                  cl.url = '/'+iEvt.ID;
                  cl.className = 'event-personal';
                  
               
              events.add(cl);
              }
              
              
           }
           
            //Class to hold calendar event data

    public class calEvent{

        public String title {get;set;}

        public Boolean allDay {get;set;}

        public String startString {get;private set;}

        public String endString {get;private set;}

        public String url {get;set;}

        public String className {get;set;}

    }

}