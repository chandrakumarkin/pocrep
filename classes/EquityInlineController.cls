/**
 * 
 */
public with sharing class EquityInlineController {

    private ApexPages.StandardController controller {get; set;}
    public Service_Request__c thisSR{get; set;}
    
    public EquityInlineController(ApexPages.StandardController controller) {
        this.controller = controller;
        Id srId = ApexPages.currentPage().getParameters().get('id');
        if(srId != null){
            thisSR = [select id,Sponsor_First_Name__c,Trade_License_No__c,Issuing_Authority_Names__c,Emirate__c,Kindly_Note__c,Current_Visa_Status__c,On_behalf_of_identical_name__c,
                         Contract_Start_Date__c,Duration_in_months__c,Reason_for_Request__c,Monthly_Salary__c,UID_Number__c from Service_Request__c where id =:srId];
        }
    }

     /**
     * Helper method to change the status of SR If HOD rejects the step
     * @param  searchValue,searchBy. 
     * @return List of tasks.          
     */
    public void LineManagerStepSRAction(string srID){
        Service_Request__c thisSr       = [SELECT Id,External_SR_Status__c,Internal_SR_Status__c FROM Service_Request__c WHERE ID =:srID];
        thisSr.External_SR_Status__c    = null;
        thisSr.Internal_SR_Status__c    = null;
        Update thisSr;

    }
}