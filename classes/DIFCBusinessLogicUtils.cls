/********************************************************************************************************************************************************
 *  Author      : Mudasir Wani
 *  Date        : 04-April-2020
 *  Description : This class will have the common business related functioalities across the DIFC organization
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date                Updated By          Description
---------------------------------------------------------------------------------------------------------------------  
V1.0    04-April-2020       Mudasir             Created this class 
V1.1    03-May-2020         Mudasir             #9102 - Introduce the new functionality where we will capture the web service load  
*********************************************************************************************************************************************************/

public without sharing class DIFCBusinessLogicUtils {
    /***********************************************************************
    *   Parameter       :   Set of account ids  - setOfAccountIds
    *   Return          :   SOQL in string format 
    *   Created By      :   Mudasir Wani
    *   Created Date    :   31-March-2020 
    ******************************************************************************************************************************************************/
    /*public static Map<id , List<Relationship__c>> getMapOfAccountIdAndListOfRelationships(Set<id> setOfAccountIds){
        String stringOfIds = DIFCBusinessLogicUtils.prepareStringOfIds(setOfAccountIds);
        String relSOQLFieldsToQuery ='id,Object_Contact__c,Subject_Account__c';
        String relSOQLFilters = 'Relationship_Type__c = \'Has DIFC Sponsored Employee\' AND status__c =\'Expired\' and Subject_Account__c in ('+stringOfIds+') and Object_Contact__r.Is_Absconder__c = false and End_Date__c != LAST_N_DAYS:30';
        List<Relationship__c> relationshipList = DIFCDataServiceUtils.getRelationshipList(relSOQLFieldsToQuery , relSOQLFilters);
        Map<id , List<Relationship__c>> mapOfAccountIdAndListOfRelationships = new Map<id , List<Relationship__c>>();
        for(Relationship__c relationshipRecord : relationshipList){
            if(mapOfAccountIdAndListOfRelationships != NULL && mapOfAccountIdAndListOfRelationships.keySet().contains(relationshipRecord.Subject_Account__c)){
                mapOfAccountIdAndListOfRelationships.get(relationshipRecord.Subject_Account__c).add(relationshipRecord);
            }else{
                mapOfAccountIdAndListOfRelationships.put(relationshipRecord.Subject_Account__c,new List<Relationship__c>{relationshipRecord});
            }
        }
        return mapOfAccountIdAndListOfRelationships;
    }*/
    /***********************************************************************
    *   Parameter       :   Set of account ids  - setOfAccountIds
    *   Return          :   SOQL in string format 
    *   Created By      :   Mudasir Wani
    *   Created Date    :   31-March-2020
    *************************************************************************/
    public static List<Relationship__c> getListOfRelationships(Set<id> setOfAccountIds){
        String stringOfIds = DIFCBusinessLogicUtils.prepareStringOfIds(setOfAccountIds);
        String relSOQLFieldsToQuery ='id,Object_Contact__c,Subject_Account__c';
        String relSOQLFilters = 'Relationship_Type__c = \'Has DIFC Sponsored Employee\' AND status__c =\'Expired\' and Subject_Account__c in ('+stringOfIds+') and Object_Contact__r.Is_Absconder__c = false and End_Date__c != LAST_N_DAYS:30';
        return DIFCDataServiceUtils.getRelationshipList(relSOQLFieldsToQuery , relSOQLFilters);
    }
    /***********************************************************************
    *   Parameter       :   List of Relationships 
    *   Return          :   Map<id , Set<id>> 
    *   Created By      :   Mudasir Wani
    *   Created Date    :   19-April-2020
    *************************************************************************/
    public static Map<id , Set<id>> getMapOfAccountIdAndSetOfContactIds(List<Relationship__c> relationshipList){
        Map<id , Set<id>> accountIdContactIdSet = new Map<id , Set<id>>();
        for(Relationship__c relationshipRecord: relationshipList){
            if(accountIdContactIdSet != NULL && accountIdContactIdSet.keySet().contains(relationshipRecord.Subject_Account__c)){
                accountIdContactIdSet.get(relationshipRecord.Subject_Account__c).add(relationshipRecord.Object_Contact__c);
            }else{
                accountIdContactIdSet.put(relationshipRecord.Subject_Account__c,new Set<id>{relationshipRecord.Object_Contact__c});
            }
        }
        return accountIdContactIdSet;
    }
    /***********************************************************************
    *   Parameter       :   set Of contact Ids Set<id>
    *   Return          :   Map<id , Set<id>>
    *   Created By      :   Mudasir Wani
    *   Created Date    :   19-April-2020
    *************************************************************************/
    public static Map<id , Set<id>> getContactIdsOfInProgressRequests(Set<id> contactIdSet){
        String stringOfIds = DIFCBusinessLogicUtils.prepareStringOfIds(contactIdSet);
        String srSOQLFieldsToQuery ='Contact__c,Customer__c ';
        String srSOQLFilters = 'Contact__c in ('+ stringOfIds +') AND External_Status_Name__c NOT IN (\'Process Completed\',\'Draft\') AND  Record_Type_Name__c in (\'DIFC_Sponsorship_Visa_Cancellation\',\'DIFC_Sponsorship_Visa_Renewal\') AND Is_Cancelled__c = FALSE AND Is_Rejected__c = FALSE';
        List<Service_Request__c> serviceRequestList = new List<Service_Request__c>();       
        serviceRequestList= DIFCDataServiceUtils.getServiceRequestList(srSOQLFieldsToQuery,srSOQLFilters);
        Map<id , Set<id>> accountIdContactIdSet = new Map<id , Set<id>>();
        for(Service_Request__c servreqList : serviceRequestList){
            if(accountIdContactIdSet != NULL && accountIdContactIdSet.size()> 0 && accountIdContactIdSet.containskey(servreqList.Customer__c)){
                accountIdContactIdSet.get(servreqList.Customer__c).add(servreqList.Contact__c);
            }else{
                accountIdContactIdSet.put(servreqList.Customer__c,new Set<id>{servreqList.Contact__c});
            }
        }
        return accountIdContactIdSet;   
    }
    /***********************************************************************
    *   Parameter       :   set Of Ids 
    *   Return          :   SOQL in string format 
    *   Created By      :   Mudasir Wani
    *   Created Date    :   19-April-2020
    *************************************************************************/
    public static String prepareStringOfIds(Set<id> setOfIds){
        String stringOfIds = '';
        for(String idString : setOfIds){
            stringOfIds =  stringOfIds != '' ? stringOfIds +','+ '\''+idString+'\'' : '\''+idString+'\'';
        }
        return stringOfIds;
    }  
    
    /***********************************************************************
    *   Parameter       :   List Of Ids 
    *   Return          :   SOQL in string format 
    *   Created By      :   Mudasir Wani
    *   Created Date    :   19-April-2020
    *************************************************************************/
    public static String prepareStringOfIdsFromListOfId(List<id> listOfIds){
        String stringOfIds = '';
        for(String idString : listOfIds){
            stringOfIds =  stringOfIds != '' ? stringOfIds +','+ '\''+idString+'\'' : '\''+idString+'\'';
        }
        return stringOfIds;
    } 
    
    /***********************************************************************
    *   Parameter       :   ClassName, Department , recordId , Request , response and service request number 
    *   Return          :   Web_Service_Load__c record
    *   Created By      :   Mudasir Wani
    *   Created Date    :   03-May-2020
    *   Class Version   :   V1.1 - #9102 - Introduce the new functionality where we will capture the web service load  
    *************************************************************************/
    public static Web_Service_Load__c prepareWebServiceLoad(String className , String recordId , 
                                                String departmentName , String webServiceRequest,
                                                String webServiceResponse, String serviceRequestNumber){
        Web_Service_Load__c webServiceLoadRecord = new Web_Service_Load__c();
        webServiceLoadRecord.Class_Name__c                  = className;
        webServiceLoadRecord.Source_Record_Id__c            = recordId;
        webServiceLoadRecord.Department__c                  = departmentName;
        webServiceLoadRecord.Web_Service_Request__c         = webServiceRequest;
        webServiceLoadRecord.Web_Service_Response__c        = webServiceResponse;
        webServiceLoadRecord.Service_Request_Number__c      = serviceRequestNumber;
        return webServiceLoadRecord;
    }   
}