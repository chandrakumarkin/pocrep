/**
    *Author      : Shikha
    *CreatedDate : 04-03-2021
    *Description : This apex class is used to view RM details on home page 
	*Test Class	 : Test_OB_RMDetailsController
**/
public without sharing class OB_RMDetailsController {

    @AuraEnabled
    public static OB_QuickContactController.RespondWrap viewContactInfo()
    {

        OB_QuickContactController.RespondWrap respWrap = new OB_QuickContactController.RespondWrap();
        
        string acctId;
        string ownerId;
        map<string,OB_QuickContactController.QuickContact> mapQuickContact = new map<string,OB_QuickContactController.QuickContact>();
        //   Merul   v1.0   Homepage Blockage 
        for(User usr:[ 
                        SELECT AccountId,
                              contact.OB_Block_Homepage__c ,
                              Contact.Account.Is_Commercial_Permission__c
                         FROM User 
                        WHERE id=: Userinfo.getUserId()
                     ]
            //
            //0051l000004IUYj
           )
        {
            acctId = usr.AccountId;
            respWrap.loggedInUser = usr;
            respWrap.displayComponent = (usr.Contact.OB_Block_Homepage__c ? false : true); 
        }


        //BD Owner Query
        if(acctId != null){
        	Account acc = [SELECT Id, Owner.Name, Owner.Id, Owner.Email,Owner.MobilePhone, Owner.Title, Owner.Gender__c
                           FROM Account WHERE Id = :acctId];  
            if(acc != null){
             	OB_QuickContactController.QuickContact quickContactObj = new OB_QuickContactController.QuickContact();
                quickContactObj.name = acc.Owner.Name;
                quickContactObj.email = acc.Owner.Email;
                quickContactObj.phoneNumber = acc.Owner.MobilePhone; 
                quickContactObj.title = acc.Owner.Title;
                quickContactObj.gender = acc.Owner.Gender__c;
                mapQuickContact.put('Business',quickContactObj);
            }            
        }
        
        /*for(Opportunity opportunityRecord : [Select Owner.Name,OwnerId,Owner.Email,Owner.MobilePhone, Owner.Title 
                                            From Opportunity
                                            where AccountId = :acctId
                                            Order By CreatedDate Desc LIMIT 1]){
            OB_QuickContactController.QuickContact quickContactObj = new OB_QuickContactController.QuickContact();
            if(opportunityRecord.Owner.Email == Label.OB_Integration_User_Email){
                quickContactObj.name = 'Business Development Team';
            }else{
                quickContactObj.name = opportunityRecord.Owner.Name;
                quickContactObj.email = opportunityRecord.Owner.Email;
                quickContactObj.phoneNumber = opportunityRecord.Owner.MobilePhone; 
                quickContactObj.title = opportunityRecord.Owner.Title;
            }
            
            //ConnectApi.Photo ph =  ConnectApi.UserProfiles.getPhoto(null, opportunityRecord.OwnerId);
            //quickContactObj.profileURL = ph.photoVersionId;
            mapQuickContact.put('Business',quickContactObj);
        }*/
        //SR Step Owner Query
        /*for(HexaBPM__Service_Request__c serviceRequest : [select id,
                                                            (select OwnerId,Owner.Name,
                                                                Owner.Email  
                                                                from HexaBPM__Steps_SR__r 
                                                                where Step_Template_Code__c = 'ENTITY_NAME_REVIEW'
                                                                Order By CreatedDate Desc limit 1
                                                            )
                                                             from HexaBPM__Service_Request__c
                                                            where HexaBPM__Customer__c = :acctId
                                                            and recordtype.DeveloperName in ('In_Principle','AOR_Financial')
                                                            Order By CreatedDate Desc LIMIT 1]){
            
            for(HexaBPM__Step__c step :serviceRequest.HexaBPM__Steps_SR__r) {
                
                OB_QuickContactController.QuickContact quickContactObj = new OB_QuickContactController.QuickContact();
                quickContactObj.name = step.Owner.Name;
                quickContactObj.email = step.Owner.Email;
                //ConnectApi.Photo ph =  ConnectApi.UserProfiles.getPhoto(null, step.OwnerId);
                //quickContactObj.profileURL = ph.fullEmailPhotoUrl;
                if(string.valueOf(step.OwnerId).startsWith('005')){ //Owner is User
                    ownerId = step.OwnerId;
                    //quickContactObj.phoneNumber = step.Owner.MobilePhone;
                }
                mapQuickContact.put('Registry',quickContactObj);
            }
            
        }
        OB_QuickContactController.QuickContact tempQuickContact = new OB_QuickContactController.QuickContact();
        for(User userRecord : [select MobilePhone from User where id = :ownerId ]){
            tempQuickContact = mapQuickContact.get('Registry');
            tempQuickContact.phoneNumber = userRecord.MobilePhone;
            mapQuickContact.put('Registry',tempQuickContact);
        }*/

        respWrap.quickContact = mapQuickContact;
        return respWrap;
    }
}