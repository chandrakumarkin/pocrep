@isTest
public with sharing class OB_SendLicensetoCustomerTest {
    @isTest
    private static void OB_SendLicensetoCustomerInit(){  
        // create account
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(2);
        insertNewAccounts[0].Registration_License_No__c = '1234';  
        insert insertNewAccounts;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Id GSUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        
        //create contact
        List<Contact> insertNewContacts = new List<Contact>();
        insertNewContacts =  OB_TestDataFactory.createContacts(2,insertNewAccounts);
        insertNewContacts[0].Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Previous_Nationality__c  = 'Russian Federation';
        insertNewContacts[0].Country_of_Birth__c  = 'Russian Federation';
        insertNewContacts[0].Country__c  = 'Russian Federation';
        insertNewContacts[0].Issued_Country__c  = 'Russian Federation';
        insertNewContacts[0].Gender__c  = 'Male';
        insertNewContacts[0].UID_Number__c  = '9712';
        insertNewContacts[0].MobilePhone  = '+97123576565';
        insertNewContacts[0].Passport_No__c  = '+97123576565';
        insertNewContacts[0].recordTypeId = portalUserId;
        // non portal
        insertNewContacts[1].Nationality__c  = 'India';
        insertNewContacts[1].Previous_Nationality__c  = 'India';
        insertNewContacts[1].Country_of_Birth__c  = 'India';
        insertNewContacts[1].Country__c  = 'India';
        insertNewContacts[1].Gender__c  = 'Female';
        insertNewContacts[1].UID_Number__c  = '9712';
        //insertNewContacts[1].MobilePhone  = '+97123576565';
        insertNewContacts[1].Passport_No__c = '12345';
        insert insertNewContacts;
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle','New_User_Registration'});
        insert createSRTemplateList;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        License__c ll =new License__c();
        ll.Account__c = insertNewAccounts[0].Id;
        insert ll;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'New_User_Registration', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'General Partner for a Limited Partnership Fund','Services'}, 
                                                   new List<string>{'Partnership','Company'}, 
                                                   new List<string>{'Recognized Limited Partnership (RLP)','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].HexaBPM__Contact__c=insertNewContacts[0].id;
        insertNewSRs[1].HexaBPM__Contact__c=insertNewContacts[0].id;
        insertNewSRs[0].Foreign_Entity_Name__c='Test Forien';
        insertNewSRs[0].Place_of_Registration_parent_company__c='Russian Federation';
        insertNewSRs[0].License_Application__c=ll.Id;
        insertNewSRs[1].License_Application__c=ll.Id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[1].id;
        insertNewSRs[0].HexaBPM__Email__c = 'test.test@test.com';
        insertNewSRs[0].RecordtypeID = OB_QueryUtilityClass.getRecordtypeID('HexaBPM__Service_Request__c','AOR_Financial');
        insert insertNewSRs;
        
        HexaBPM__Status__c srStatus = new HexaBPM__Status__c();
        srStatus.Name = 'In Progress';
        srStatus.HexaBPM__Code__c = 'In Progress';
        insert srStatus;
        
        List<HexaBPM__Step_Template__c> stepTemplate = OB_TestDataFactory.createStepTemplate(1,
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'SECURITY_REVIEW'},
                                                        new List<String>{'In_Principle','AOR_Financial'},
                                                        new List<String>{'Test'});

        insert stepTemplate;
        
        HexaBPM__SR_Steps__c objSRSteps = new HexaBPM__SR_Steps__c();
        objSRSteps.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objSRSteps.HexaBPM__Step_Template__c = stepTemplate[0].Id;
        objSRSteps.HexaBPM__Active__c = true;
       // objSRSteps.HexaBPM__Status__c = srStatus[0].Id;
        insert objSRSteps;
        
        List<HexaBPM__Step__c> stepList = OB_TestDataFactory.createSteps(2,insertNewSRs,new List<HexaBPM__SR_Steps__c>{objSRSteps});
        stepList[0].HexaBPM__Status__c = srStatus.Id;
        stepList[1].HexaBPM__Status__c = srStatus.Id;
        insert stepList;
        
        HexaBPM__Document_Master__c doc= new HexaBPM__Document_Master__c();
        doc.HexaBPM__Available_to_client__c = true;
        doc.HexaBPM__Code__c = 'LICENSE';
        doc.Download_URL__c = 'test';
        insert doc;
        
        HexaBPM__Document_Master__c doc1= new HexaBPM__Document_Master__c();
        doc1.HexaBPM__Available_to_client__c = true;
        doc1.HexaBPM__Code__c = 'CERTIFICATE_OF_STATUS';
        doc1.Download_URL__c = 'test';
        insert doc1;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        
        HexaBPM__SR_Doc__c srDoc = new HexaBPM__SR_Doc__c();
        srDoc.Name = 'Passport';
        srDoc.HexaBPM__Service_Request__c=insertNewSRs[0].Id;
        srDoc.HexaBPM__Doc_ID__c = documents[0].Id;
        srDoc.HexaBPM__Document_Master__c = doc.Id;
        insert srDoc;
        
        HexaBPM__SR_Doc__c srDoc1 = new HexaBPM__SR_Doc__c();
        srDoc1.Name = 'Passport 12';
        srDoc1.HexaBPM__Service_Request__c=insertNewSRs[0].Id;
        srDoc1.HexaBPM__Doc_ID__c = documents[0].Id;
        srDoc1.HexaBPM__Document_Master__c = doc1.Id;
        insert srDoc1;
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = srDoc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        //create ContentDocumentLink  record 
        ContentDocumentLink cdl1 = New ContentDocumentLink();
        cdl1.LinkedEntityId = srDoc1.id;
        cdl1.ContentDocumentId = documents[0].Id;
        cdl1.shareType = 'V';
        insert cdl1;
        
        test.starttest();
        OB_SendLicensetoCustomer createRel= new OB_SendLicensetoCustomer();
        system.debug('-service===-'+[select id,License_Application__c from HexaBPM__Service_Request__c]);
        List<HexaBPM__Step__c> stepNewList  = [select id,HexaBPM__SR__c,
                    HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.HexaBPM__Record_Type_Name__c,
                    HexaBPM__SR__r.License_Application__c from HexaBPM__Step__c];
        createRel.EvaluateCustomCode(insertNewSRs[0],stepNewList[0]);
        
        OB_SendInPrincipleDoctoCustomer ob = new OB_SendInPrincipleDoctoCustomer();
        List<OB_SendInPrincipleDoctoCustomer.FlowInputs> lst = new List<OB_SendInPrincipleDoctoCustomer.FlowInputs>(); 
        OB_SendInPrincipleDoctoCustomer.FlowInputs cls = new OB_SendInPrincipleDoctoCustomer.FlowInputs();
        cls.srDocId = srDoc1.Id;
        cls.documentMasterCode = 'CERTIFICATE_OF_STATUS';
        lst.add(cls);
        OB_SendInPrincipleDoctoCustomer.sendEmailFunction(lst);
        
        test.stopTest();
    
    }    
}