/*
    Author      : Durga Prasad
    Date        : 24-Dec-2019
    Description : Custom code to create Update Fintech Classification and Coworking Facility on Account
    ----------------------------------------------------------------------------------------------------
*/
global with sharing class CC_Coworking_FintechUpdate implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        system.debug('=====CC_Coworking_FintechUpdate======');
        if(stp!=null && stp.HexaBPM__SR__c!=null){
	        try{
				if(stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null){
					Account acc = new Account(Id=stp.HexaBPM__SR__r.HexaBPM__Customer__c);
					acc.ROC_Status__c = 'Under Formation';
					if(stp.HexaBPM__SR__r.Business_Sector__c=='FinTech' || stp.HexaBPM__SR__r.Co_working_Facility__c=='Yes'){
						if(stp.HexaBPM__SR__r.Business_Sector__c=='FinTech')
							acc.FinTech_Classification__c = 'FinTech';
						if(stp.HexaBPM__SR__r.Co_working_Facility__c=='Yes')
							acc.Working_from_Co_Working_Space__c = true;
					}
					update acc;
		        }
	        }catch(Exception e){
	        	strResult = e.getMessage()+'';
	        }
        }
        return strResult;
    }
}