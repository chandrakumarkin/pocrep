public with sharing class UserFrmInlineController {

    private ApexPages.StandardController controller {get; set;}
    public Service_Request__c thisSR{get; set;}
    
    public UserFrmInlineController(ApexPages.StandardController controller) {

        this.controller = controller;
        Id srId = ApexPages.currentPage().getParameters().get('id');

        if(srId != null){
            thisSR = [select id,Street_Address__c,City_Town__c,Name_of_Addressee__c,Emirate_State__c,Name_of_the_Country_GS__c,PO_BOX__c,Confirm_Change_of_Entity_Name__c,Confirm_Change_of_Trading_Name__c
                           from Service_Request__c where id =:srId];
        }
    }

     /**
     * Helper method to change the status of SR If HOD rejects the step
     * @param  searchValue,searchBy. 
     * @return List of tasks.          
     */
    public void LineManagerStepSRAction(string srID){
        Service_Request__c thisSr       = [SELECT Id,External_SR_Status__c,Internal_SR_Status__c FROM Service_Request__c WHERE ID =:srID];
        thisSr.External_SR_Status__c    = Null;
        thisSr.Internal_SR_Status__c    = Null;
        Update thisSr;

    }
    
   

}