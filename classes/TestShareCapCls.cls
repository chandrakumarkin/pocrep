/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 09-Dec-2014   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class TestShareCapCls {

	public static testmethod void testShare() {
    
        PageReference pageRef = Page.ShareCapitalForm;
        Test.setCurrentPageReference(pageRef);
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Change_in_Authorized_Shares_or_Nominal_Value';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Change_in_Authorized_Shares_or_Nominal_Value';
        insert objPageFlow;
        
        Page__c objPage = new Page__c();
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        insert objPage;
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        ApexPages.currentPage().getParameters().put('PageId',objPage.id);
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        list<Currency__c> currlist = new list<Currency__c>();
		Currency__c curr =new Currency__c(Name='US Dollar',Exchange_Rate__c=1.00,Active__c=true);
        Currency__c curr1 =new Currency__c(Name='UAE Dirham',Exchange_Rate__c=3.67,Active__c=true);
        currlist.add(curr);
        currlist.add(curr1);
        insert currlist;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Change_in_Authorized_Shares_or_Nominal_Value')]){
        	mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.Authorized_Share_Capital__c = 500000;
        objAccount.Currency__c = curr.id;
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        objSR.legal_structures__c ='LLC';
        objSR.currency_list__c = 'US Dollar';
        objSR.currency_rate__c = curr.id;
        objSR.Share_Capital_Membership_Interest__c = 50000;
        objSR.No_of_Authorized_Share_Membership_Int__c = 560;
        objSR.RecordTypeId = mapRecordTypeIds.get('Change_in_Authorized_Shares_or_Nominal_Value');
        insert objSR;
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        ShareCapitalCls shareform = new ShareCapitalCls();
        shareform.BackToServiceRequest();
		Account_Share_Detail__c accshrlist = new Account_Share_Detail__c();
        accshrlist.Account__c =  objAccount.Id;
        accshrlist.Name = 'Class10';
        accshrlist.Status__c = 'Draft';
        accshrlist.Sys_Proposed_Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_No_of_Shares_per_Class__c=50;
        insert accshrlist;    
        
        shareform.strNavigatePageId = objPage.Id;
        shareform.strActionId = objSD.Id;
        shareform.goTopage();
        shareform.getDyncPgMainPB();
        shareform.DynamicButtonAction();
        
        shareform.calcShares();
        Shareholder_Detail__c shareDet =  new Shareholder_Detail__c(Account__c=objAccount.Id,Account_Share__c=accshrlist.id,Sys_Proposed_No_of_Shares__c=2);
        insert shareDet;
        //shareform.addRow();
        shareform.calcShares();
        shareform.strType = 'Shareholder';
        shareform.accShare[0].Sys_Proposed_Nominal_Value__c =1000;
        shareform.accShare[0].Sys_Proposed_No_of_Shares_per_Class__c = 50;
        shareform.accShare[0].Name = 'TestClassA';
        shareform.Save_ShareDetail();
        shareform.delRow();
        shareform.strType = 'Member';
        shareform.addRow();
        //shareform.calcShares();
        shareform.Save_ShareDetail();
        shareform.strType = 'amendshares';
        shareform.objSRFlow.legal_Structures__c = 'LLC';
        shareform.addRow();
        shareform.calcShares();
        shareform.Save_ShareDetail();
        shareform.RowId = 0;
        shareform.delRow();
        shareform.obSR.legal_Structures__c = 'LTD SPC';
        shareform.obSR.Share_Capital_Membership_Interest__c = null;
        shareform.obSR.No_of_Authorized_Share_Membership_Int__c = null;
        shareform.obSR.Currency_list__c = null;
        shareform.strType = 'Member';
        shareform.Save_ShareDetail();
        shareform.calcShares();

        objSR.legal_structures__c ='LTD SPC';
        objSR.Share_Capital_Membership_Interest__c = 5;
        objSR.No_of_Authorized_Share_Membership_Int__c = 5;
        update objSR;
        shareform.delRow();
        shareform.Save_ShareDetail();
        shareform.calcShares();
   	}
}