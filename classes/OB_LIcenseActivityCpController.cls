/*
 Created By  : Manpreet Singh on 27th Oct,2019Prateek
 --------------------------------------------------------------------------------------------------------------------------
 Modification History
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By      Description
 ----------------------------------------------------------------------------------------
  1.0   15 AMY   2020     Prateek           controller for licence activity page for CP 
 ----------------------------------------------------------------------------------------
 */
public without sharing class OB_LIcenseActivityCpController {
    public class RequestWrap {
        @AuraEnabled
        public string flowID { get; set; }

        @AuraEnabled
        public string pageId { get; set; }

        @AuraEnabled
        public string srId { get; set; }
/*  */
        @AuraEnabled
        public string commandButtonId { get; set; }

        @AuraEnabled
        public string LicenseId { get; set; }

        public RequestWrap() {
        }
    }

    public class RespondWrap {
        @AuraEnabled
        public list<License_Activity__c> listLiscAct { get; set; }
        @AuraEnabled
        public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        @AuraEnabled
        public string licenseApplicationId { get; set; }
        @AuraEnabled
        public boolean hasError { get; set; }
        @AuraEnabled
        public string ErrorMessage { get; set; }
        @AuraEnabled
        public string uploadedFileName { get; set; }

        @AuraEnabled
        public string SearchBarLable { get; set; }
        @AuraEnabled
        public HexaBPM__Service_Request__c serviceObj { get; set; }
        public RespondWrap() {
            hasError = false;
            serviceObj = new HexaBPM__Service_Request__c();
            ErrorMessage = '';
            listLiscAct = new list<License_Activity__c>();
        }
    }


    /*Contains logged in user Id*/
    public static string userId = UserInfo.getUserId();

    /*Contains logged in user type*/
    String userType = UserInfo.getUserType();

    /*Contains logged in user contactID*/
    public static ID userContactId;
    // current logged in user account detail
    public static string userAccountID;

    public static License__c objLicense;



    @AuraEnabled
    public static RespondWrap getLicenseActivityData(string requestWrapParam) {

        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap = new RespondWrap();
        try{
            if(requestWrapParam != null && requestWrapParam != '') {
                //deseriliaze.
                reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
                respWrap.ButtonSection = [SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
                                          AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1];



                if(reqWrap.srId != null && reqWrap.srId != '') {
                    string AccountId;
                    string ServiceRequestId = reqWrap.srId;
                    /*
                    for(User usr :[Select contactid, Contact.AccountId, Contact.Account.Name from User where Id = :UserInfo.getUserId()]) {
                        if(usr.contactid != null) {
                            userContactId = usr.contactid;
                            if(usr.Contact.AccountId != null) 
                           
                            {
                                AccountId = usr.Contact.AccountID;
                                
                            }
                        }
                    }*/
                    //1.0                                     
                    for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
                        //1.1
                        if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                          AccountId = usrObj.contact.AccountId;
                        }
                    }


                    string licenseApplicationId;
                    if(AccountId != null && ServiceRequestId != null) {
                        //objLicense = new License__c();

                        for(HexaBPM__Service_Request__c SR :[Select Id, License_Application__c, Buying_Selling_Real_Estate_Property__c, Dealing_in_Precious_Metals__c, DNFBP__c, Setting_Up__c, 
                                                             Category_of_Service__c, Category_of_Service_Explanation__c, legal_structures__c, 
                                                             Nature_of_Business__c, Parent_or_Entity_Business_Activities__c, Type_of_commercial_permission__c,
                                                             Business_Sector__c, Entity_Type__c, RecordType.DeveloperName
                                                             from HexaBPM__Service_Request__c
                                                             where Id = :ServiceRequestId
                                                             and HexaBPM__Customer__c = :AccountId
                            ]) {

                            /* if(SR.License_Application__c != null) {
                             licenseApplicationId = SR.License_Application__c;
                             objLicense.Id = SR.License_Application__c;
                             } */
                            respWrap.serviceObj = SR;

                            if(SR.legal_structures__c == 'Company' || SR.legal_structures__c == 'Partnership') {
                                respWrap.SearchBarLable = 'Select business activities';
                            } else if(SR.legal_structures__c == 'Foundation') {
                                respWrap.SearchBarLable = 'Select foundation objects';
                            } else if(SR.legal_structures__c == 'Non Profit Incorporated Organisation (NPIO)') {
                                respWrap.SearchBarLable = 'Select Activities';
                            }

                        }

                        /* if(objLicense.Id == null) {
                         objLicense.Account__c = AccountId;
                         objLicense.Status__c = 'Draft';

                         insert objLicense;
                         HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id = ServiceRequestId, License_Application__c = objLicense.Id);
                         update objSR;
                         } */
                    }
                    system.debug('=======AccountId======' + AccountId)
                        ;

                    if(ServiceRequestId != null) {
                        respWrap.listLiscAct = getSRLicenseActivity(ServiceRequestId);
                    } else {
                        respWrap.listLiscAct = new list<License_Activity__c>();
                    }
                    respWrap.licenseApplicationId = licenseApplicationId;
                }
            } else {
                respWrap.hasError = true;
                respWrap.ErrorMessage = 'Application Id is missing.';
            }
        } catch(DMLException e) {
            respWrap.hasError = true;
            respWrap.ErrorMessage = e.getMessage() + '';
            //return respWrap;
        }

        for(HexaBPM__SR_Doc__c srDoc :[SELECT id, File_Name__c, HexaBPM__Document_Master__r.HexaBPM__Code__c
                                       FROM HexaBPM__SR_Doc__c
                                       WHERE HexaBPM__Service_Request__c = :reqWrap.srId
                                       AND HexaBPM_Amendment__c = null
                                       AND HexaBPM__Document_Master__r.HexaBPM__Code__c in('Federal_or_local_government_approval')
                                       LIMIT 1
            ]) {

            respWrap.uploadedFileName = srDoc.File_Name__c;
        }
        return  respWrap;

    }
    public static list<License_Activity__c> getSRLicenseActivity(string applicationId) {
        return [select Id, Name, Activity__c, Activity__r.Name, Account__c, Activity__r.Activity_Description_english__c, Activity__r.Activity_Code__c, 
                Account__r.Name, Sector_Classification__c, Activity__r.OB_DNFBP__c, Activity__r.Sys_Code__c, Activity__r.OB_Sell_or_Buy_Real_Estate__c, Activity__r.OB_Trade_Precious_Metal__c
                from License_Activity__c where Application__c = :applicationId AND OB_End_Activity__c = false ORDER BY Activity__r.Name ASC];
    }
    @AuraEnabled
    public static list<License_Activity_Master__c> getLicenseActivityMaster(String srId, String searchKeyWord) {
        String searchKey = searchKeyWord + '%';
        system.debug('=======searchKey======' + searchKey);
        system.debug('=======srID======' + srID);
        string userAccountID;
        string EntityType;
        boolean LeaseAKiosk = false;
        boolean sectorCheck = false;
        boolean nonFinancialFilter = false;
        HexaBPM__Service_Request__c obSR = new HexaBPM__Service_Request__c();
        List<String> listOfSectorToBeChecked = new List<String>{ 'Non Profit Incorporated Organisation (NPIO)', 'Foundation', 'Single Family Office', 'Prescribed Companies', 'General Partner for a Limited Partnership Fund', 'Investment Fund' };
        string businessSector;

        if(srId != null && srId != '') {
            for(HexaBPM__Service_Request__c objSR :[Select Entity_Type__c, Type_of_Property__c, RecordType.DeveloperName,Type_of_commercial_permission__c,
                                                    Business_Sector__c from HexaBPM__Service_Request__c where Id = :srId]) {
                                                        obSR = objSR;
                EntityType = objSR.Entity_Type__c;
                businessSector = objSR.Business_Sector__c;
                if(objSR.Type_of_Property__c == 'Lease a Kiosk') {
                    LeaseAKiosk = true;
                }
            }
        }

        System.debug(businessSector);

        if(businessSector != null) 
        {
            sectorCheck = listOfSectorToBeChecked.contains(businessSector);
        }
        System.debug(sectorCheck);

        /*
        for(User usr :[Select contactid, Contact.AccountId, Contact.Account.Name  from User where Id = :userId]) {
            if(usr.contactid != null) 
            {
                userContactId = usr.contactid;
                if(usr.Contact.AccountId != null) 
                
                {
                    userAccountID = usr.Contact.AccountID;
                    
                }
            }
        }*/
        
        //1.0
        for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
            //1.1
            if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
              userAccountID = usrObj.contact.AccountId;
              userContactId = usrObj.contactid;
            }
        }

        string LicenseId;
        /* if(userAccountID != null) {
         for(License__c objLic :[Select Id from License__c where Account__c = :userAccountID and Status__c = 'Draft']) {
         LicenseId = objLic.Id;
         }
         } */
        set<Id> setExistingActivities = new set<Id>()
            ;
        if(srId != null) {
            for(License_Activity__c LA :[Select Activity__c from License_Activity__c where Application__c = :srId AND OB_End_Activity__c = false]) {
                setExistingActivities.add(LA.Activity__c);
            }
        }

        string strSearchText = '%' + searchKey + '%';
        list<License_Activity_Master__c> licenseActivityMasterList = new list<License_Activity_Master__c>();
        list<License_Activity_Master__c> licenseActivityMasterListFiltered = new list<License_Activity_Master__c>();

        list<License_Activity_Master__c> licenseActivityMasterListFilteredNonFinancial = new list<License_Activity_Master__c>();

        if(obSR.RecordType.DeveloperName != 'Commercial_Permission' && obSR.RecordType.DeveloperName != 'Commercial_Permission_Renewal'){
            for(License_Activity_Master__c LAM :[Select Id, Name, Activity_Description_english__c, 
                                             OB_DNFBP__c, OB_Sector_Classification__c, Sector_Classification__c, 
                                             OB_Trade_Precious_Metal__c , OB_Government_Entity_Approval_Required__c, 
                                             OB_Sell_or_Buy_Real_Estate__c, Sys_Code__c, OB_Lease_a_kiosk__c, Activity_Code__c,OB_Type_of_commercial_permission__c
                                             from  License_Activity_Master__c where Name LIKE :strSearchText
                                             AND Sys_Activity_Type__c = :EntityType
                                             //AND Sector_Classification__c = :businessSector
                                             AND Enabled__c = true
                                             AND ID NOT IN :setExistingActivities ORDER BY Name ASC]) {
            //
            if(LeaseAKiosk) {
                if(LAM.OB_Lease_a_kiosk__c) {
                    licenseActivityMasterList.add(LAM);
                }
            } else {
                licenseActivityMasterList.add(LAM);
            }


        }

        if(sectorCheck) {
            for (License_Activity_Master__c LAM :licenseActivityMasterList) {

                if(LAM.OB_Sector_Classification__c == businessSector) {
                    licenseActivityMasterListFiltered.add(LAM);
                }

            }
        }


        // NON financial foundation activity hiding for sr with bussiness sector other than  foundation
        if(EntityType != null && EntityType == 'Non - financial' && (businessSector == null || (businessSector != null && businessSector != 'Foundation'))) {
            nonFinancialFilter = true;
        }

        if(sectorCheck) {
            if(nonFinancialFilter) {
                for (License_Activity_Master__c LAM :licenseActivityMasterListFiltered) {

                    if(LAM.OB_Sector_Classification__c != 'Foundation') {
                        licenseActivityMasterListFilteredNonFinancial.add(LAM);
                    }

                }
                return licenseActivityMasterListFilteredNonFinancial;
            } else {
                return licenseActivityMasterListFiltered;
            }

        } else {
            if(nonFinancialFilter) {
                for (License_Activity_Master__c LAM :licenseActivityMasterList) {

                    if(LAM.OB_Sector_Classification__c != 'Foundation') {
                        licenseActivityMasterListFilteredNonFinancial.add(LAM);
                    }

                }
                return licenseActivityMasterListFilteredNonFinancial;

            } else {
                return licenseActivityMasterList;
            }

        }
        }else{
            //for cp/*  */
            for(License_Activity_Master__c LAM :[Select Id, Name, Activity_Description_english__c, 
                                             OB_DNFBP__c, OB_Sector_Classification__c, Sector_Classification__c, 
                                             OB_Trade_Precious_Metal__c , OB_Government_Entity_Approval_Required__c, 
                                             OB_Sell_or_Buy_Real_Estate__c, Sys_Code__c, OB_Lease_a_kiosk__c, Activity_Code__c,OB_Type_of_commercial_permission__c
                                             from  License_Activity_Master__c where Name LIKE :strSearchText
                                             AND Enabled__c = true AND OB_Type_of_commercial_permission__c != null
                                             AND ID NOT IN :setExistingActivities ORDER BY Name ASC]) {

                                                licenseActivityMasterList.add(LAM);

                                                
        }
        

        return licenseActivityMasterList;

    }
}
    @AuraEnabled
    public static RespondWrap saveLicenseActivity(string ActivityId, string srID) {
        RespondWrap respWrap = new RespondWrap();
        list<License_Activity__c> licenseActivityList = new list<License_Activity__c>();
        string licenseId;
        string userAccountID;
        try{
            if(srID != null && ActivityId != null) {
                //for(User usr :[Select contactid, Contact.AccountId, Contact.Account.Name  from User where Id = :userId])                
                /*
                for(User usr : OB_AgentEntityUtils.getUserById(userId)) 
                {
                    userAccountID = usr.Contact.AccountID;
                
                    system.debug('userAccountID' + userAccountID);
                }*/
                //1.0
                for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
                    //1.1
                    if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                      userAccountID = usrObj.contact.AccountId;
                    }
                }
                License_Activity__c licenseObj = new License_Activity__c();
                for(HexaBPM__Service_Request__c SR :[Select Id, License_Application__c from HexaBPM__Service_Request__c where Id = :srID and HexaBPM__Customer__c = :userAccountID]) {
                    //licenseId = SR.License_Application__c;
                    //licenseObj.License__c = SR.License_Application__c;
                    licenseObj.Account__c = userAccountID;
                    licenseObj.Activity__c = ActivityId;
                    licenseObj.Application__c = SR.Id;
                    licenseObj.OB_End_Activity__c = false;
                    licenseActivityList.add(licenseObj);
                }

                for(License_Activity__c licObj : [select id from License_Activity__c where Account__c =: userAccountID
                AND Activity__c =: ActivityId AND  Application__c =: srID LIMIT 1]){
                    licenseObj.id = licObj.id;
                }
                if(licenseActivityList.size() > 0) {
                    System.debug('insert');
                    system.debug(licenseActivityList);
                    upsert licenseActivityList;
                }

                set<string> setGovernmentApproval = new set<string>();
                string GovtApprovalText = '';
                string BDFinancialServicesActivities = '';

                HexaBPM__Service_Request__c objSRTBU = new HexaBPM__Service_Request__c(Id = srID, Has_Exempt_Activity__c = false);
                for(License_Activity__c LA :[Select Id, Activity_Name_Eng__c, DED_Activity_Code__c, Activity__r.OB_DNFBP__c, Activity__r.Activity_Code__c, Activity_Description_english__c, Activity__r.OB_Government_Entity_Approval_Required__c from License_Activity__c where Application__c = :srID]) {

                    if(BDFinancialServicesActivities != '')
                        BDFinancialServicesActivities = LA.Activity_Name_Eng__c;
                    else 
                        BDFinancialServicesActivities = BDFinancialServicesActivities + ';' + LA.Activity_Name_Eng__c;

                    if(LA.Activity__r.Activity_Code__c == 'EXE_COM')
                        objSRTBU.Has_Exempt_Activity__c = true;
                    if(LA.Activity__r.OB_Government_Entity_Approval_Required__c != null)
                        setGovernmentApproval.add(LA.Activity__r.OB_Government_Entity_Approval_Required__c);
                    if(LA.DED_Activity_Code__c == 'RB')
                        setGovernmentApproval.add(LA.DED_Activity_Code__c);
                }
                for(string strVal :setGovernmentApproval) {
                    if(GovtApprovalText == '')
                        GovtApprovalText = strVal;
                    else 
                        GovtApprovalText = GovtApprovalText + ',' + strVal;
                }
                objSRTBU.Government_Entity_Approval_Required__c = GovtApprovalText;
                update objSRTBU;

                Account acc = new Account(Id = userAccountID);
                acc.BD_Financial_Services_Activities__c = BDFinancialServicesActivities;
                update acc;

                OB_RiskMatrixHelper.CalculateRisk(objSRTBU.Id);
            }
            respWrap.ErrorMessage = userAccountID;
        } catch(Exception ex) {
            respWrap.hasError = true;
            respWrap.ErrorMessage = ex.getMessage();
            system.debug(ex.getMessage());
        }
        if(srID != null)
            respWrap.listLiscAct = getSRLicenseActivity(srID);
        return respWrap;
    }
    @AuraEnabled
    public static RespondWrap deleteLicenseActivity(string LicenseId, string srID, string licenseAppId) {
        RespondWrap respWrap = new RespondWrap();
        if(LicenseId != null && srID != null) {
            try{
                list<License_Activity__c> delLicenseActivity = [Select Id,OB_existing_Activity__c, Activity__c, Account__c, Account__r.Name, Sector_Classification__c, Activity__r.Name, License__c, Activity_Description_english__c from License_Activity__c where Id = :LicenseId];
                string strLicenseId;
                if(delLicenseActivity != null && delLicenseActivity.size() > 0) {
                    if(delLicenseActivity[0].OB_existing_Activity__c == true){
                        delLicenseActivity[0].OB_End_Activity__c = true;
                        update delLicenseActivity[0];
                    }else{
                        strLicenseId = delLicenseActivity [0].License__c;
                    delete delLicenseActivity;
                    }
                    
                }
                if(strLicenseId != null && srID != null) {
                    set<string> setGovernmentApproval = new set<string>();
                    string AccountId;
                    string GovtApprovalText = '';
                    string BDFinancialServicesActivities = '';

                    HexaBPM__Service_Request__c objSRTBU = new HexaBPM__Service_Request__c(Id = srID, Has_Exempt_Activity__c = false);
                    for(License_Activity__c LA :[Select Id, Activity_Name_Eng__c, Application__c, Application__r.HexaBPM__Customer__c, Activity__r.OB_DNFBP__c, DED_Activity_Code__c, Activity__r.Activity_Code__c, Activity_Description_english__c, Activity__r.OB_Government_Entity_Approval_Required__c from License_Activity__c where Application__c = :srID]) {

                        if(BDFinancialServicesActivities != '')
                            BDFinancialServicesActivities = LA.Activity_Name_Eng__c;
                        else 
                            BDFinancialServicesActivities = BDFinancialServicesActivities + ';' + LA.Activity_Name_Eng__c;

                        if(LA.Application__c != null)
                            AccountId = LA.Application__r.HexaBPM__Customer__c;

                        if(LA.Activity__r.Activity_Code__c == 'EXE_COM')
                            objSRTBU.Has_Exempt_Activity__c = true;
                        if(LA.Activity__r.OB_Government_Entity_Approval_Required__c != null)
                            setGovernmentApproval.add(LA.Activity__r.OB_Government_Entity_Approval_Required__c);
                        if(LA.DED_Activity_Code__c == 'RB')
                            setGovernmentApproval.add(LA.DED_Activity_Code__c);
                    }
                    for(string strVal :setGovernmentApproval) {
                        if(GovtApprovalText == '')
                            GovtApprovalText = strVal;
                        else 
                            GovtApprovalText = GovtApprovalText + ',' + strVal;
                    }

                    if(AccountId != null) {
                        Account acc = new Account(Id = AccountId);
                        acc.BD_Financial_Services_Activities__c = BDFinancialServicesActivities;
                        update acc;
                    }

                    objSRTBU.Government_Entity_Approval_Required__c = GovtApprovalText;
                    update objSRTBU;
                    OB_RiskMatrixHelper.CalculateRisk(objSRTBU.Id);
                }
            } catch(DMLException e) {
                respWrap.hasError = true;
                string DMLError = e.getdmlMessage(0) + '';
                if(DMLError == null)
                    DMLError = e.getMessage() + '';

                respWrap.ErrorMessage = DMLError;
            }
        }
        respWrap.listLiscAct = getSRLicenseActivity(srID);
        return respWrap;
    }

    public static void saveSRDoc(string srId, map<string, string> docMap) {

        //delete the previous SR Docs for the same parent and documentMasterCode
        if(docMap != null && docMap.size() > 0)
            delete [select id from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c = :srId and HexaBPM__Document_Master__r.HexaBPM__Code__c in :docMap.keySet()];

        OB_QueryUtilityClass.createCompanySRDoc(srId, null, docMap);
    }

    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID, HexaBPM__Service_Request__c serviceRequest, string ButtonId, string pageId, map<string, string> docMap) {

        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        string DMLError;
        HexaBPM__Service_Request__c objRequest;
        try{
            if(serviceRequest != null) {
                serviceRequest = OB_QueryUtilityClass.setPageTracker(serviceRequest, SRID, pageId);
                update serviceRequest;
                System.debug(docMap);

                if(docMap != null) {
                    saveSRDoc(SRID, docMap);
                }

            }
            OB_RiskMatrixHelper.CalculateRisk(SRID);
            objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
            PageFlowControllerHelper.objSR = objRequest;
            PageFlowControllerHelper objPB = new PageFlowControllerHelper();

            PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
            system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
            respWrap.pageActionName = responseNextPage.pg;
            respWrap.communityName = responseNextPage.communityName;
            respWrap.CommunityPageName = responseNextPage.CommunityPageName;
            respWrap.sitePageName = responseNextPage.sitePageName;
            respWrap.strBaseUrl = responseNextPage.strBaseUrl;
            respWrap.srId = objRequest.Id;
            respWrap.flowId = responseNextPage.flowId;
            respWrap.pageId = responseNextPage.pageId;
            respWrap.isPublicSite = responseNextPage.isPublicSite;

        } catch(DMLException e) {
            DMLError = e.getdmlMessage(0) + '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
        }
        if(DMLError != null && DMLError != '')
            respWrap.errorMessage = DMLError;
        system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);

        return respWrap;
    }
    public class ButtonResponseWrapper {
        @AuraEnabled public String pageActionName { get; set; }
        @AuraEnabled public string communityName { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public string CommunityPageName { get; set; }
        @AuraEnabled public string sitePageName { get; set; }
        @AuraEnabled public string strBaseUrl { get; set; }
        @AuraEnabled public string srId { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public HexaBPM__Service_Request__c updatedSr { get; set; }
        @AuraEnabled public boolean isPublicSite { get; set; }

    }
}