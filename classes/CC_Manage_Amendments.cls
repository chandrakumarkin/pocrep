/*
    Author      :   Durga Prasad
    Class Name  :   CC_Manage_Amendments
    Description :   Custom code to excute the Amendments of different processes
*/

//V1.1      15th June 2016      Swati       as per #2693
//V1.2      13th Nov 2016       Sravan      as Per # 3325
//V1.3      15th Nov 2017       Arun        As per #3307


public without sharing class CC_Manage_Amendments {
    public static string Create_Name_Change_Amendment(Step__c stp){
        string strResult = 'Success';
        if(stp!=null && stp.SR__c!=null && stp.SR__r.Customer__c!=null){
            Amendment__c objNameChangeAmnd = new Amendment__c();
            objNameChangeAmnd.Customer__c = stp.SR__r.Customer__c;
            objNameChangeAmnd.ServiceRequest__c = stp.SR__c;
            objNameChangeAmnd.Amendment_Type__c = 'Name Change';
            try{
                insert objNameChangeAmnd;
            }catch(DMLException e){
                string DMLError = e.getdmlMessage(0)+'';
                if(DMLError==null){
                    DMLError = e.getMessage()+'';
                }
                strResult = DMLError;
            }
        }
        return strResult;
    }
    
    public static string Change_Company_Name(Step__c stp)
    {
        string strResult = 'Success';
        RecursiveControlCls.isThroughCustomCode = true;//V1.1
        if(stp!=null && stp.SR__c!=null && stp.SR__r.Customer__c!=null)
        {
            //if(stp.SR__r.Entity_Name__c!=null){
                Account objAcc = new Account(Id=stp.SR__r.Customer__c);
                
                //V1.3 Start 
                //Change Compnay Name 
                
                        //New Names
                        if(stp.SR__r.Entity_Name__c!=null)
                            objAcc.Name = stp.SR__r.Entity_Name__c;
                        
                        if(stp.SR__r.Pro_Entity_Name_Arabic__c!=null)
                            objAcc.Arabic_Name__c = stp.SR__r.Pro_Entity_Name_Arabic__c;
                        
                        //Old Names
                        if(stp.SR__r.Pre_Entity_Name_Arabic__c!=null)
                        objAcc.Former_Name_Arabic__c=stp.SR__r.Pre_Entity_Name_Arabic__c;
                        
                        if(stp.SR__r.Previous_Entity_Name__c!=null)
                        objAcc.Former_Name__c=stp.SR__r.Previous_Entity_Name__c;
                        
                
                
                //End Company Name 
                
                
                //Change compnay Trad name 
                
                    //New 
                        if(stp.SR__r.Proposed_Trading_Name_1__c!=null)
                            objAcc.Trade_Name__c = stp.SR__r.Proposed_Trading_Name_1__c;
                        if(stp.SR__r.pro_trade_name_1_in_arab__c!=null)
                            objAcc.Trading_Name_Arabic__c = stp.SR__r.pro_trade_name_1_in_arab__c;
                    //Old   
                        if(stp.SR__r.Previous_Trading_Name_1__c!=null)
                            objAcc.Former_Trading_Name__c = stp.SR__r.Previous_Trading_Name_1__c;
                        if(stp.SR__r.Pre_Trade_Name_1_in_Arab__c!=null)
                            objAcc.Former_Trading_Name_Arabic__c = stp.SR__r.Pre_Trade_Name_1_in_Arab__c;
                        
                        
                //End Change Company trad name 
                
                //V1.3 End
                
                try{
                    update objAcc;
                }catch(DMLException e){
                    string DMLError = e.getdmlMessage(0)+'';
                    if(DMLError==null){
                        DMLError = e.getMessage()+'';
                    }
                    strResult = DMLError;
                }
            //}else{
              //  strResult = 'Please provide the Proposed Entity Name in the Request.';
           // }
        }
        return strResult;
    }
    
    /*
        Method Name :   Amend_Auditors
        Description :   This is an Custom code which will amends the Auditor details of an Company.
    */
    public static string Amend_Auditors(Step__c stp){
        string strResult = 'Success';
        if(stp!=null && stp.SR__c!=null){
            map<string,string> MapAuditorId = new map<string,string>();
            
            list<Account> lstAuditorAccounts = new list<Account>();
            
            map<id,Relationship__c> mapCurrentRel = new map<id,Relationship__c>();
            
            Account SubAcc = new Account(Id=stp.SR__r.Customer__c);
            
            map<string,string> mapPARAcc = new map<string,string>();
            
            map<string,Amendment__c> auditorAmdmntLink = new map<string,Amendment__c>();  // V1.2
            
            for(Amendment__c amnd:[select id,Emirate_State__c,Status__c,Post_Code__c,DIFC_Recognized_Auditor__c,Date_of_Appointment_Auditor__c,Recognized_Auditors__c,Apt_or_Villa_No__c,Financial_Year_End__c,Non_Recognized_Auditors__c,Building_Name__c,
            Street__c,PO_Box__c,Permanent_Native_City__c,Permanent_Native_Country__c,Customer__c,Date_of_cessation__c from Amendment__c where 
            ServiceRequest__c=:stp.SR__c and Customer__c=:stp.SR__r.Customer__c]){
                
                // Added for V1.2 
                
                    if(amnd.Recognized_Auditors__c!=null){
                        auditorAmdmntLink.put(amnd.Recognized_Auditors__c,amnd);
                    }else if(amnd.Non_Recognized_Auditors__c!=null){
                         auditorAmdmntLink.put(amnd.Non_Recognized_Auditors__c,amnd);
                    }
                
                // End V1.2 
                
                if(amnd.Status__c=='Draft'){
                    Account objAcc = new Account();
                    if(amnd.Recognized_Auditors__c!=null){
                        MapAuditorId.put(amnd.Recognized_Auditors__c,amnd.Id);
                        objAcc.Id = amnd.Recognized_Auditors__c;
                    }else if(amnd.Non_Recognized_Auditors__c!=null){
                        MapAuditorId.put(amnd.Non_Recognized_Auditors__c,amnd.Id);
                        objAcc.Id = amnd.Non_Recognized_Auditors__c;
                    }
                    
                    system.debug('Recog Aud===>'+amnd.Recognized_Auditors__c);
                    system.debug('Non-Recog Aud===>'+amnd.Recognized_Auditors__c);
                    if(amnd.Financial_Year_End__c!=null && mapPARAcc.get(stp.SR__r.Customer__c)==null){
                        SubAcc.Financial_Year_End__c = amnd.Financial_Year_End__c;
                        lstAuditorAccounts.add(SubAcc);
                        mapPARAcc.put(stp.SR__r.Customer__c,stp.SR__r.Customer__c);
                    }
                    system.debug('Account Id===>'+objAcc.Id);
                    if(objAcc.Id!=null){
                        objAcc.Office__c = amnd.Apt_or_Villa_No__c; 
                        objAcc.Street__c = amnd.Street__c;
                        objAcc.Building_Name__c = amnd.Building_Name__c;//amnd.DIFC_Recognized_Auditor__c;
                        objAcc.Emirate_State__c = amnd.Emirate_State__c;
                        objAcc.Country__c = amnd.Permanent_Native_Country__c;
                        objAcc.PO_Box__c = amnd.PO_Box__c;
                        objAcc.Postal_Code__c = amnd.Post_Code__c;
                        objAcc.City__c = amnd.Permanent_Native_City__c;
                        //objAcc.Financial_Year_End__c = amnd.Financial_Year_End__c;
                        
                        Relationship__c objAudRel = new Relationship__c();
                        objAudRel.Appointment_Date__c = amnd.Date_of_Appointment_Auditor__c;
                        objAudRel.Start_Date__c = amnd.Date_of_Appointment_Auditor__c;
                        objAudRel.Object_Account__c = objAcc.Id;
                        objAudRel.Subject_Account__c = stp.SR__r.Customer__c;
                        objAudRel.Relationship_Type__c = 'Has Auditor';
                        objAudRel.Active__c = true;
                        
                        mapCurrentRel.put(objAudRel.Object_Account__c,objAudRel);
                        
                        lstAuditorAccounts.add(objAcc);
                    }
                }
            }
            
            if(lstAuditorAccounts!=null && lstAuditorAccounts.size()>0){
                try{
                    system.debug('lstAuditorAccounts===>'+lstAuditorAccounts);
                    update lstAuditorAccounts;
                    for(Relationship__c objRel:[select id,Object_Account__c,Active__c from Relationship__c where Subject_Account__c=:stp.SR__r.Customer__c and Relationship_Type__c='Has Auditor' and Active__c=true]){
                        Relationship__c objUpdt = new Relationship__c();
                        if(mapCurrentRel.get(objRel.Object_Account__c)!=null){
                            objUpdt = mapCurrentRel.get(objRel.Object_Account__c);
                            objUpdt.Id = objRel.Id;
                            mapCurrentRel.put(objRel.Object_Account__c,objUpdt);
                        }else{
                            objUpdt = objRel;
                            objUpdt.Active__c = false;
                            if(auditorAmdmntLink.containsKey(objRel.Object_Account__c) && auditorAmdmntLink.get(objRel.Object_Account__c).Date_of_cessation__c !=null) // Added for V1.2
                                objUpdt.End_date__c = auditorAmdmntLink.get(objRel.Object_Account__c).Date_of_cessation__c; // Added for V1.2
                            mapCurrentRel.put(objRel.Object_Account__c,objUpdt);
                        }
                    }
                    if(mapCurrentRel!=null && mapCurrentRel.size()>0){
                        list<Relationship__c> lstRelUpsert = mapCurrentRel.values();
                        system.debug('lstRelUpsert===>'+lstRelUpsert);
                        upsert lstRelUpsert;
                    }
                }catch(DMLException e){
                    string DMLError = e.getdmlMessage(0)+'';
                    if(DMLError==null){
                        DMLError = e.getMessage()+'';
                    }
                    strResult = DMLError;
                    return strResult;
                }
            }
        }
        return strResult;
    }
}