/*********************************************************************************************************************
*  Name     : DPOAssessmentApprovalBatch 
*  Author   : 
*  Purpose  : This class used to approve the Submitted DPO Assessment Records
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
V1.1         DIFC                28-JUN-2021               Draft Version
**/

global class DPOAssessmentApprovalBatch implements database.batchable<sobject>,Database.AllowsCallouts, Database.Stateful{
    
    global database.querylocator start(database.batchablecontext bc){
        
        String statusRequest = 'Submitted'; 
        string serviceType = 'DPO_Annual_Assessment';
        string autoApprovalCode = 'Auto Approval';
        String s = 'SELECT Id,HexaBPM__Status__c,HexaBPM__SR_Step__c,Name,HexaBPM__SR__r.HexaBPM__External_SR_Status__c,HexaBPM__SR__r.HexaBPM__Internal_SR_Status__c,HexaBPM__SR__c FROM HexaBPM__Step__c';
        if(!Test.isRunningTest())s += ' where CreatedDate = LAST_N_DAYS:30 AND SR_Recordtype_Name__c =:serviceType AND HexaBPM__SR__r.HexaBPM__External_Status_Name__c =\''+statusRequest+'\'';
        if(!Test.isRunningTest())s+= ' and Step_Template_Code__c =\''+autoApprovalCode+'\'';
        if(Test.isRunningTest()) s+= ' limit 1';
        system.debug('=query==test='+database.query(s));
        return database.getquerylocator(s);
    }
    
    global void execute(database.batchablecontext bc, List<HexaBPM__Step__c> stepList){
        
        try{
            system.debug('======stepList============='+stepList);
            for(HexaBPM__Step__c step : stepList){
                CC_AutoCloseStep closeStep = new CC_AutoCloseStep();
                closeStep.EvaluateCustomCode(null,step);
            }
        }catch(Exception e){
            string errorresult = e.getMessage();
            system.debug('=========errorresult =======DPOAssessmentApprovalBatch========'+errorresult +'\n------Line Number :------'+e.getLineNumber()+'\nException is : '+errorresult);
            insert LogDetails.CreateLog(null, 'DPOAssessmentApprovalBatch: execute', 'Line Number : '+e.getLineNumber()+'\nException is : '+errorresult);
        }
    }
    
    global void finish(database.batchablecontext bc){
    
    }
    
}