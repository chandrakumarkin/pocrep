@isTest
public class OB_OpportunityTriggerHandlerTest {
    @testSetup
    static  void createTestData() {
        //create lead
        Id RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Financial').getRecordTypeId(); 
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insertNewLeads[0].RecordTypeId = RecordTypeId;
        insertNewLeads[0].LOI_Date__c = date.today();
        insertNewLeads[0].LeadSource = 'Conferences'; 
        insertNewLeads[0].Send_Portal_Login_Link__c = 'Conferences';
        insertNewLeads[0].Company_Type__c = 'Financial - related';
        insertNewLeads[0].Sector_Classification__c = 'Banking';
        insertNewLeads[0].BD_Sector_Classification__c = 'Banks';
        insertNewLeads[0].Priority_to_DIFC__c = 'Standard'; 
        insertNewLeads[0].Country__c = 'Austria';
        insertNewLeads[0].Geographic_Region__c = 'GCC';
        insert insertNewLeads;
    }
    static testMethod void OpportunityTriggerHandlerTest() {
        test.startTest();
            List<Lead> lstLead = new List<Lead>();
            lstLead = [select id from Lead];
            database.leadConvert lc = new database.leadConvert();
            lc.setLeadId(lstLead[0].id);
            leadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            Database.LeadConvertResult lcr = Database.convertLead(lc);  
        test.stopTest();
    }

}