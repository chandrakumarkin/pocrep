@isTest
public class OB_LookupSObjectControllerTest {
	 @isTest static void testSearch() {
        // Create some accounts
        Account abc = new Account(Name = 'ABC Account');
        Account xyz = new Account(Name = 'XYZ Account');

        List<Account> accounts = new List<Account> { abc, xyz };

        insert accounts;

         //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        listLicenseActivityMaster[0].Name = 'tester';
        listLicenseActivityMaster[0].Enabled__c = true;
        insert listLicenseActivityMaster;

        License_Activity__c actObj = new License_Activity__c();
        actObj.Activity__c = listLicenseActivityMaster[0].Id;
        actObj.Account__c  = accounts[0].Id;
        insert actObj;
		
        Id[] fixedSearchResults = new Id[] { xyz.Id };
        Test.setFixedSearchResults(fixedSearchResults);
        List<OB_LookupSObjectController.Result> results = OB_LookupSObjectController.lookup('xy', 'Account', '', '', '','Name','Name','');

        System.assertEquals(1, results.size());
        System.assertEquals(xyz.Name, results[0].SObjectLabel);
        System.assertEquals(xyz.Id, results[0].SObjectId);
        string searchText = '%ABC%';
        string strSOQL = 'select id,name from Account where Name=\''+searchText+'\'';
        OB_LookupSObjectController.lookup('xy', 'Account', '', strSOQL, '','Name','Name','');
        if(accounts != null && accounts.size() > 0)
		    OB_LookupSObjectController.searchLookUpName(accounts[0].id,'Account');

        string licenseActivitySOQL = 'SELECT Account__r.id,Name, Account__c,Account__r.name,Activity__r.name,Account__r.Registration_License_No__c from License_Activity__c where  Account__r.name like \'%Account%\' ';
        OB_LookupSObjectController.lookup('', 'License_Activity__c', '', licenseActivitySOQL, '','Name','Account__r.name','');
        
   }   
}