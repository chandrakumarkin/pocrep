/************************************************************************************************************
    Author      :   Durga Prasad
    Class Name  :   Cls_AmendmentFieldMappingUtil
    Description :   This is an utility class where the Amendment fields has to be mapped to the appropriate Master and vice versa.
    ---------------------------------------------------------------------------------------------------------------------
    Modification History
    ---------------------------------------------------------------------------------------------------------------------
    V.No        Date        Updated By      Description
    ---------------------------------------------------------------------------------------------------------------------
    V1.0        14-06-2015  Saima Hidayat   Registered Entity in DIFC field added.
    V1.1        09-08-2015  Saima Hidayat   Fields added for tenants in RORP Lease Reg
    V1.2        22-10-2015  Kaavya          Fields added for additional passport in RORP Freehold
    V1.3        10-05-2016  Ravi            Added the logic to map the RORP fields correctly for the Accounts ref # 2946
    V1.4        19-05-2016  Ravi            Changed the Apartment No field mapping to Office Number in the contact.
    V1.5        22-Nov-2018 Arun            NEw company law 5858
    v1.6        08/05/2019 Selva            #5938 update the nationality field,updated on 03-07-2019 needs to check the logic again
    v1.7        24/07/2019  Sai             #7138
    v1.8        05-Nov-2020    selva        #DIFC2-12063 Include Nationality logic - UAE resident
    v1.9        09-Nov-2020    Arun         #10449
**************************************************************************************************************/
public without sharing class Cls_AmendmentFieldMappingUtil {
    public static Contact getObjectContact(Contact objCon,Amendment__c objAmnd,string strRelType){
        if(objAmnd.Title_new__c!=null)
            objCon.Salutation = objAmnd.Title_new__c;
        if(objAmnd.Family_Name__c!=null)
            objCon.LastName = objAmnd.Family_Name__c;
        if(objAmnd.Given_Name__c!=null)
            objCon.FirstName = objAmnd.Given_Name__c;
        if(objAmnd.Date_of_Birth__c!=null)
            objCon.Birthdate = objAmnd.Date_of_Birth__c;
        if(objAmnd.Place_of_Birth__c!=null)
            objCon.Place_of_Birth__c = objAmnd.Place_of_Birth__c;
        if(objAmnd.Passport_No__c!=null)
            objCon.Passport_No__c = objAmnd.Passport_No__c;
        if(objAmnd.Nationality__c!=null)
            objCon.Nationality_Lookup__c = objAmnd.Nationality__c;
        if(objAmnd.Nationality_list__c!=null)
            objCon.Nationality__c = objAmnd.Nationality_list__c;
        if(objAmnd.Phone__c!=null)
            objCon.Phone = objAmnd.Phone__c;
        if(objAmnd.Person_Email__c !=null) 
            objCon.Email = objAmnd.Person_Email__c;
        if(objAmnd.First_Name_Arabic__c!=null)
            objCon.Arabic_Name__c = objAmnd.First_Name_Arabic__c;
        if(objAmnd.Occupation__c!=null)
            objCon.Occupation__c = objAmnd.Occupation__c;
        if(objAmnd.Apt_or_Villa_No__c!=null)//V1.4
            objCon.Office_Unit_Number__c = objAmnd.Apt_or_Villa_No__c;
        if(objAmnd.Building_Name__c!=null)
            objCon.ADDRESS_LINE1__c = objAmnd.Building_Name__c;
        if(objAmnd.Street__c!=null) 
            objCon.ADDRESS_LINE2__c = objAmnd.Street__c; 
        if(objAmnd.PO_Box__c!=null)
            objCon.PO_Box_HC__c = objAmnd.PO_Box__c; 
        if(objAmnd.Permanent_Native_City__c!=null)
            objCon.CITY1__c = objAmnd.Permanent_Native_City__c; 
        if(objAmnd.Emirate_State__c!=null)
            objCon.Emirate_State_Province__c = objAmnd.Emirate_State__c; 
        if(objAmnd.Post_Code__c!=null)
            objCon.Postal_Code__c = objAmnd.Post_Code__c; 
        if(objAmnd.Emirate__c!=null)
            objCon.Emirate__c = objAmnd.Emirate__c; 
        if(objAmnd.Permanent_Native_Country__c!=null)
            objCon.Country__c = objAmnd.Permanent_Native_Country__c;
            
        if(objAmnd.Middle_Name__c !=null)
        objCon.Middle_Names__c = objAmnd.Middle_Name__c;
        
         if(objAmnd.Former_Name__c !=null)
        objCon.Former_Name__c = objAmnd.Former_Name__c;
        
        //1.5V New Start comapny law 5858
        
        //v1.7 
        if(objAmnd.Income_and_Origin_of_Wealth__c !=null)
            objCon.Country_Income_Source__c = objAmnd.Income_and_Origin_of_Wealth__c; 
        if(objAmnd.Is_this_individual_PEP__c!=null)
            objCon.Is_this_individual_a_PEP__c = objAmnd.Is_this_individual_PEP__c; 
        if(objAmnd.Please_provide_details_on_source_of_inco__c !=null)
            objCon.source_of_income_wealth__c = objAmnd.Please_state_the_source_of_income__c; 
        if(objAmnd.Please_provide_details_on_source_of_inco__c !=null)
            objCon.Details_on_source_of_income__c = objAmnd.Please_provide_details_on_source_of_inco__c;
        
        if(objAmnd.Passport_Expiry_Date__c!=null)
        objCon.Passport_Expiry_Date__c=objAmnd.Passport_Expiry_Date__c;
        
        if(objAmnd.Are_you_a_resident_in_the_U_A_E__c !=null)
        objCon.Are_you_a_resident_in_the_U_A_E__c =objAmnd.Are_you_a_resident_in_the_U_A_E__c;
        
        if(objAmnd.U_I_D_No__c !=null)
        objCon.UID_Number__c =objAmnd.U_I_D_No__c;
        
        if(objAmnd.E_I_D_No__c !=null)
        objCon.EID_Number__c =objAmnd.E_I_D_No__c;
        
        if(objAmnd.Is_Designated_Member__c !=null)
        objCon.Is_Designated_Member__c =objAmnd.Is_Designated_Member__c;
        
        if(objAmnd.Date_of_Appointment_GP__c !=null)
        objCon.Date_of_Appointment__c=objAmnd.Date_of_Appointment_GP__c;
        //v1.7  End here
        //if(objAmnd.Director_Appointed_Date__c!=null)
        //objCon.Director_Appointed_Date__c=objAmnd.Director_Appointed_Date__c;
    
        //End Start comapny law 5858
        
        
        //V1.1 - RORP
        if(objAmnd.Residence_Phone_Number__c!=null)
            objCon.Residence_Phone__c = objAmnd.Residence_Phone_Number__c; 
        if(objAmnd.Phone__c!=null)
            objCon.MobilePhone = objAmnd.Phone__c;
        if(objamnd.Country_of_Issuance__c !=null)
            objCon.Issued_Country__c = objAmnd.Country_of_Issuance__c;
        if(objAmnd.Emirates_ID_Number__c!=null)
            objCon.Emirate_ID__c = objAmnd.Emirates_ID_Number__c;
        if(objAmnd.Gender__c!=null)
            objCon.Gender__c = objAmnd.Gender__c;
        if(objAmnd.Office_Unit_Number__c!=null)
            objCon.Office_Unit_Number__c = objAmnd.Office_Unit_Number__c;
        if(objAmnd.Office_Phone_Number__c!=null)
            objCon.Work_Phone__c = objAmnd.Office_Phone_Number__c;
        //V 1.2 Added for freehold
        if(objAmnd.Passport_No_Occupant__c !=null)
            objCon.Passport_No_Addl__c=objAmnd.Passport_No_Occupant__c;
        if(objAmnd.Country_of_Issuance_Addl__c !=null)
            objCon.Country_of_Issuance_Addl__c=objAmnd.Country_of_Issuance_Addl__c;
        //v1.8 start
           if(objAmnd.Have_a_valid_UAE_residence_visa__c!=null)
          objCon.Does_the_individual_have_a_valid_UAE_res__c=objAmnd.Have_a_valid_UAE_residence_visa__c;
          if(objAmnd.Individual_reside_in_the_UAE__c!=null)
           objCon.Does_the_individual_reside_in_the_UAE__c =objAmnd.Individual_reside_in_the_UAE__c;
          //v1.8 end   
        
        objCon.Amendment__c =objAmnd.ID;
        
        
        //1.9 Start 
        if(objAmnd.Place_of_Issue__c!=null)
       objCon.Place_of_Issue__c= objAmnd.Place_of_Issue__c;
        if(objAmnd.Passport_Issue_Date__c!=null)
       objCon.Date_of_Issue__c=objAmnd.Passport_Issue_Date__c;
       
        //1.9 End
        
        return objCon;
    }
    public static Account getObjectAccount(Account objAcc,Amendment__c objAmnd,string strRelType){
        if(objAmnd.Is_Designated_Member__c!=null)
            objAcc.Is_Designated_Member__c = objAmnd.Is_Designated_Member__c;
        if(objAmnd.Company_Name__c!=null)
            objAcc.Name = objAmnd.Company_Name__c;
        if(objAmnd.Former_Name__c!=null)
            objAcc.Former_Name__c = objAmnd.Former_Name__c;
        if(objAmnd.Registration_Date__c!=null)
            objAcc.ROC_reg_incorp_Date__c = objAmnd.Registration_Date__c;
        if(objAmnd.Place_of_Registration__c!=null)
            objAcc.Place_of_Registration__c = objAmnd.Place_of_Registration__c;
        if(objAmnd.Phone__c!=null)
            objAcc.Phone = objAmnd.Phone__c;
        if(objAmnd.Apt_or_Villa_No__c!=null)
            objAcc.Office__c = objAmnd.Apt_or_Villa_No__c;
        if(objAmnd.Building_Name__c!=null)
            objAcc.Building_Name__c = objAmnd.Building_Name__c;
        if(objAmnd.Street__c!=null)
            objAcc.Street__c = objAmnd.Street__c;
        if(objAmnd.PO_Box__c!=null)
            objAcc.PO_Box__c = objAmnd.PO_Box__c;
        if(objAmnd.Permanent_Native_City__c!=null)
            objAcc.City__c = objAmnd.Permanent_Native_City__c;
        if(objAmnd.Emirate_State__c!=null)
            objAcc.Emirate_State__c = objAmnd.Emirate_State__c;
        if(objAmnd.Post_Code__c!=null)
            objAcc.Postal_Code__c = objAmnd.Post_Code__c;
        if(objAmnd.Permanent_Native_Country__c!=null)
            objAcc.Country__c = objAmnd.Permanent_Native_Country__c;
        //#7138 Start here
        if(objAmnd.Registration_No__c !=null){
            objAcc.Registration_No__c =  objAmnd.Registration_No__c;
        }
        //v1.9 
        if(objAmnd.Place_of_Registration_Text__c!=null)
        objAcc.Place_of_Registration_Text__c=objAmnd.Place_of_Registration_Text__c;
        
        if(objAmnd.Please_state_the_source_of_income__c !=null){
            objAcc.Please_state_the_source_of_income_weal__c =  objAmnd.Please_state_the_source_of_income__c;
        }
        if(objAmnd.Please_provide_details_on_source_of_inco__c !=null){
            objAcc.Source_of_income_wealth__c = objAmnd.Please_provide_details_on_source_of_inco__c;
        }
        
         if(objAmnd.Income_and_Origin_of_Wealth__c !=null){
            objAcc.Source_of_Income_and_Origin_of_Wealth__c = objAmnd.Income_and_Origin_of_Wealth__c;  
        }
        
        
        if(objAmnd.Title_new__c !=null){
            objAcc.Title__c =  objAmnd.Title_new__c;
        }
        
        if(objAmnd.Gender__c !=null){
            objAcc.Gender__c =  objAmnd.Gender__c;
        }
        if(objAmnd.First_Name__c !=null){
            objAcc.First_Name__c = objAmnd.First_Name__c;
        }
        
        if(objAmnd.Last_Name__c !=null){
            objAcc.Last_Name__c = objAmnd.Last_Name__c;
        }
        if(objAmnd.Nationality_list__c !=null){
            objAcc.Nationality__c =  objAmnd.Nationality_list__c;
        }
        
        if(objAmnd.Passport_No__c !=null){
            objAcc.Passport_Number__c =  objAmnd.Passport_No__c;
        }
        if(objAmnd.Passport_Expiry_Date__c !=null){
            objAcc.Passport_Expiry_Date__c = objAmnd.Passport_Expiry_Date__c;
        }
        
         if(objAmnd.Date_of_Birth__c !=null){
            objAcc.Date_of_Birth__c = objAmnd.Date_of_Birth__c;
        }
        if(objAmnd.Person_Email__c !=null){
            objAcc.Email_Address__c =  objAmnd.Person_Email__c;
        }
        
        if(objAmnd.Phone__c !=null){
            objAcc.Phone =  objAmnd.Phone__c;
        }
        
        
        
           //#7138 End here 
        //V1.1 - RORP
        if(objAmnd.IssuingAuthority__c!=null)
            objAcc.Issuing_Authority__c = objAmnd.IssuingAuthority__c;
        if(objAmnd.Issuing_Authority_Other__c!= null)
            objAcc.other__c = objAmnd.Issuing_Authority_Other__c;
        if(objAmnd.IssuingAuthority__c!=null && objAmnd.IssuingAuthority__c=='Others' && objAmnd.CL_Certificate_No__c!=null)
            objAcc.RORP_License_No__c = objAmnd.CL_Certificate_No__c;
        if(objAmnd.Office_Unit_Number__c!=null)
            objAcc.Office__c = objAmnd.Office_Unit_Number__c;
        if(objAmnd.Residence_Phone_Number__c!=null)
            objAcc.Residence_Phone__c = objAmnd.Residence_Phone_Number__c; 
        if(objAmnd.Phone__c!=null)
            objAcc.Mobile_Number__c = objAmnd.Phone__c;
        if(objAmnd.Office_Phone_Number__c!=null)
            objAcc.phone = objAmnd.Office_Phone_Number__c;
        if(objAmnd.Issuance_Date__c!=null)
            objAcc.Issuance_Date__c = objAmnd.Issuance_Date__c;
        //V1.3
        if(objAmnd.Fax__c != null)
            objAcc.Fax = objAmnd.Fax__c;
        //if(objAmnd.Place_of_Issue__c != null)
        //  objAcc.Place_of_Registration__c = objAmnd.Place_of_Issue__c; -------- This field needs to be decide yet
        //if(objAmnd.Business_Activity__c != null)
        //  objAcc. -------- This field needs to be decide yet
        //if(objAmnd.Legal_Structure_Type__c != null)
            //objAcc. ---- -------- This field needs to be decide yet
            
            
         objAcc.Amendment__c = objAmnd.Id;
            
        return objAcc;
    }
    public static Amendment__c getIndvAmendment(Contact objCon,Amendment__c objAmnd,string strRelType)
    {
        system.debug('objAmnd==>'+objAmnd);
        objAmnd.Title_new__c = objCon.Salutation;
        objAmnd.Family_Name__c = objCon.LastName;
        objAmnd.Given_Name__c = objCon.FirstName;
        if(objCon.FirstName!=null)
            objAmnd.Name_of_Body_Corporate_Shareholder__c = objCon.FirstName+' '+objCon.LastName;
        else
            objAmnd.Name_of_Body_Corporate_Shareholder__c = objCon.LastName;
        objAmnd.Date_of_Birth__c = objCon.Birthdate;
        objAmnd.Place_of_Birth__c = objCon.Place_of_Birth__c;
        objAmnd.Passport_No__c = objCon.Passport_No__c;
        //objAmnd.Nationality__c = objCon.Nationality_Lookup__c; v1.6 
        objAmnd.Nationality__c = objCon.Nationality_Lookup__c; //v1.6 
        objAmnd.Nationality_list__c = objCon.Nationality__c;
        objAmnd.Phone__c = objCon.Phone;
        objAmnd.Person_Email__c  = objCon.Email;
        objAmnd.First_Name_Arabic__c = objCon.Arabic_Name__c;
        objAmnd.Occupation__c = objCon.Occupation__c;
        objAmnd.Apt_or_Villa_No__c = objCon.Office_Unit_Number__c;//V1.4
        objAmnd.Building_Name__c = objCon.ADDRESS_LINE1__c;
        objAmnd.Street__c = objCon.ADDRESS_LINE2__c;
        objAmnd.PO_Box__c = objCon.PO_Box_HC__c;
        objAmnd.Permanent_Native_City__c = objCon.CITY1__c;
        objAmnd.Emirate_State__c = objCon.Emirate_State_Province__c;
        objAmnd.Post_Code__c = objCon.Postal_Code__c;
        objAmnd.Emirate__c = objCon.Emirate__c;
        objAmnd.Permanent_Native_Country__c = objCon.Country__c;
        objAmnd.Please_state_the_source_of_income__c = objCon.source_of_income_wealth__c;
        objAmnd.Please_provide_details_on_source_of_inco__c = objCon.Details_on_source_of_income__c ;
        objAmnd.Passport_Expiry_Date__c = objCon.Passport_Expiry_Date__c;
        
        //1.9 Start 
        objAmnd.Place_of_Issue__c=objCon.Place_of_Issue__c;
        objAmnd.Passport_Issue_Date__c=objCon.Date_of_Issue__c;
        objAmnd.Are_you_a_resident_in_the_U_A_E__c=objCon.Are_you_a_resident_in_the_U_A_E__c;
        objAmnd.Is_this_individual_PEP__c=objCon.Is_this_individual_a_PEP__c;
        objAmnd.Have_a_valid_UAE_residence_visa__c = objCon.Does_the_individual_have_a_valid_UAE_res__c;
        //objAmnd.Have_a_valid_UAE_residence_visa__c=objCon.Have_a_valid_UAE_residence_visa__c;
        objAmnd.U_I_D_No__c=objCon.UID_Number__c;
        objAmnd.E_I_D_No__c = objCon.EID_Number__c;
        objAmnd.Individual_reside_in_the_UAE__c = objCon.Does_the_individual_reside_in_the_UAE__c;
        //1.9 End
        
        
        
       // objAmnd.Income_and_Origin_of_Wealth__c = objCon.Country_of_Income_Source__c;
        
        //objAmnd.Other_Directorships__c = objCon.Other_Directorships__c;
        //objAmnd.Date_of_Appointment_GP__c = objCon.Appointment_Date__c;
        //objAmnd.Amount_of_Contribution_US__c = objCon.Amount_of_Contribution__c;
        //objAmnd.Contribution_Type__c = objCon.Contribution_Type_Other__c;
        //objAmnd.Contribution_Type_Other__c = objCon.Contribution_Type_Other__c;
        //objAmnd.Percentage_held__c = objCon.Percentage_held__c;
        //objAmnd.Class_of_Units_Other_Rights__c = objCon.Class_of_Units_Other_Rights__c;
        //objAmnd.Representation_Authority__c = objCon.Representation_Authority__c;
        //objAmnd.Type_of_Authority_Other__c = objCon.Type_of_Authority_Other__c;
        //objAmnd.Date_of_Appointment_SP__c = objCon.Appointment_Date__c;
        
        return objAmnd;
    }
    public static Amendment__c getBCAmendment(Account objAcc,Amendment__c objAmnd,string strRelType){
        system.debug('### account record '+objAcc);
        objAmnd.Is_Designated_Member__c = objAcc.Is_Designated_Member__c;
        objAmnd.Company_Name__c = objAcc.Name;
        objAmnd.Name_of_Body_Corporate_Shareholder__c = objAcc.Name;
        objAmnd.Former_Name__c = objAcc.Former_Name__c;
        system.debug('### objAcc.ROC_reg_incorp_Date__c '+objAcc.ROC_reg_incorp_Date__c);
        objAmnd.Registration_Date__c = objAcc.ROC_reg_incorp_Date__c;
        objAmnd.Place_of_Registration__c = objAcc.Place_of_Registration__c;
        //Shikha - added
        objAmnd.Place_of_Registration_Text__c = objAcc.Place_of_Registration_Text__c;
        system.debug('### place of Registration '+objAmnd.Place_of_Registration_Text__c);
        objAmnd.Phone__c = objAcc.Phone;
        objAmnd.Apt_or_Villa_No__c = objAcc.Office__c;
        objAmnd.Building_Name__c = objAcc.Building_Name__c;
        objAmnd.Street__c = objAcc.Street__c;
        objAmnd.PO_Box__c = objAcc.PO_Box__c;
        objAmnd.Permanent_Native_City__c = objAcc.City__c;
        objAmnd.Emirate_State__c = objAcc.Emirate_State__c;
        objAmnd.Post_Code__c = objAcc.Postal_Code__c;
        objAmnd.Permanent_Native_Country__c = objAcc.Country__c;
        
        //V1.0 Registered Entity in DIFC field added.
        objAmnd.Registered_Company__c = (objAcc.Active_License__c!=null) ? true:false;
        
        objAmnd.Please_state_the_source_of_income__c = objAcc.Please_state_the_source_of_income_weal__c;
        objAmnd.Please_provide_details_on_source_of_inco__c = objAcc.Source_of_income_wealth__c ;
        
        //New fields mapping 
        objAmnd.Place_of_Registration_Text__c =objAcc.Place_of_Registration_Text__c;
        objAmnd.Registration_No__c =objAcc.Registration_No__c;
        system.debug('### objAmnd. Registration_No__c '+objAmnd.Registration_No__c);  
        //Shikha - adding rest of the fields
        objAmnd.Title_new__c = objAcc.Title__c;
        objAmnd.Gender__c = objAcc.Gender__c;
        objAmnd.First_Name__c = objAcc.First_Name__c;
        objAmnd.Last_Name__c = 	objAcc.Last_Name__c;
        objAmnd.Nationality_list__c = objAcc.Nationality__c;
        objAmnd.Passport_No__c = objAcc.Passport_Number__c;
        objAmnd.Passport_Expiry_Date__c = objAcc.Passport_Expiry_Date__c;
        objAmnd.Date_of_Birth__c = objAcc.Date_of_Birth__c;
        objAmnd.Email_Address__c = objAcc.Email_Address__c;
        objAmnd.Phone__c = objAcc.Phone;	
        return objAmnd;
    }
}