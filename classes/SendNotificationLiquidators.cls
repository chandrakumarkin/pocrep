/******************************************************************************************
 *  Author      : Selva
 *  Date        : 2019-07-02    
 *  Purpose     : Class for scheduling the email notificaion for Liquidators   
 *  Modified    : 
 *  v1.1    28/06/2020    selva    add SOQL condition to send notification only to active Liquidators #8104
*******************************************************************************************/
global class SendNotificationLiquidators implements Schedulable {

    //Variable Section
    global FINAL String strQuery;
    global List<String> errorMessages = new List<String>();
    global List<Lookup__c> LookupList = new List<Lookup__c>();
    
    global SendNotificationLiquidators() { 
        string strLiq = 'Liquidator';
        Boolean isrecognized = true;
        Boolean isdisabled = false;
        strQuery = 'SELECT Id,Name, Email__c,Type__c, Recognized__c,CreatedBy.ContactId FROM Lookup__c WHERE Type__c=\''+strLiq+'\' and Disabled__c=false'; 
        system.debug('---'+strQuery);
    }
    
    global void execute(SchedulableContext SC) {
        LookupList = (List<Lookup__c>)Database.query(strQuery);
        sendNotification(LookupList);
    }
    public static void sendNotification(List<Lookup__c> LookupList){
        
        if(LookupList.size()>0){
        List<Lookup__c> LiqList = (List<Lookup__c>) LookupList;
        if(!LiqList.isEmpty()) { 
            EmailTemplate objET=[select Id, Name, Subject, Body,HTMLValue  from EmailTemplate where DeveloperName='Liquidators_Notification'];
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
            List<Messaging.SingleEmailMessage> mailList = new List<Messaging.SingleEmailMessage>();
            for (Lookup__c lk : LiqList)
            {               
                
                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage(); 
                String[] toAddresses = new String[] {lk.Email__c};
                Message.setToAddresses(toAddresses);
                //Message.setTargetObjectIds(lk.CreatedBy.ContactId);
                message.setTreatTargetObjectAsRecipient(false);
                message.setOrgWideEmailAddressId(owea[0].id);
                string bodyValue =  objET.Body;
                String subjectString = objET.Subject;
                message.setSubject(subjectString);
                bodyValue = bodyValue.replace('{!Lookup__c.Name}',lk.Name);
                message.setPlainTextBody(bodyValue); 
                if(objET.Id!=null){
                     Message.setTemplateId(objET.Id);
                } 
                Message.SaveAsActivity = false;
                mailList.add(Message);
                
            }
            if(!mailList.isEmpty()) {
                try{
                    Messaging.sendEmail(mailList);
                    ScheduleLiquidators  obj = new ScheduleLiquidators ();
                    obj.execute(null);
                }
                catch (Exception ex) {
                    //errorMessages.add('Unable to send email to Tech: '+ ex.getStackTraceString());
                }
            }
        }
        }
    }  
}