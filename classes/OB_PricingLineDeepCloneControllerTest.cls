/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class OB_PricingLineDeepCloneControllerTest {

    static testMethod void myUnitTest() {
        Product2 prod = new Product2();
        prod.Name = 'Test Prod';
        prod.IsActive = true;
        insert prod;
        
        HexaBPM__Pricing_Line__c PL = new HexaBPM__Pricing_Line__c();
        PL.HexaBPM__Product__c = prod.Id;
        PL.HexaBPM__Active__c = true;
        PL.HexaBPM__Priority__c = 1;
        insert PL;
        
        HexaBPM__Dated_Pricing__c DP = new HexaBPM__Dated_Pricing__c();
        DP.HexaBPM__Pricing_Line__c = PL.Id;
        DP.HexaBPM__Date_From__c = system.today();
        insert DP;
        
        HexaBPM__Pricing_Range__c PR = new HexaBPM__Pricing_Range__c();
        PR.HexaBPM__Dated_Pricing__c = DP.Id;
        PR.HexaBPM__Fixed_Price__c = 500;
        insert PR;
        
        HexaBPM__Condition__c con = new HexaBPM__Condition__c();
        con.HexaBPM__Pricing_Line__c = PL.Id;
        insert con;      
        
        apexpages.currentPage().getParameters().put('Id',PL.Id);
        apexpages.currentPage().getParameters().put('ProductId',prod.Id);
        
        OB_PricingLineDeepCloneController objDeepClonePL = new OB_PricingLineDeepCloneController();
        objDeepClonePL.DeepClone();
        
    }
}