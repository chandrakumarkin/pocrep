/*=============================================================================================
Version     Date         Author         Modification
1.0   	  04-08-2021     Vinod        Initial Version
=============================================================================================
*/
public class FitoutLWCController {
    @AuraEnabled
    public static void updateStepStatus(string stepId, string stepStatus) {
        system.debug('stepId***'+stepId+'***stepStatus***'+stepStatus);
        Status__c status = [Select Id,Name FROM Status__c WHERE Name=:stepStatus limit 1];
        Step__c stepRecord = new Step__c(Id=stepId, Status__c=status.Id);
        update stepRecord;
    }
}