/*  Author      :   Sai Kalyan
    Description :   This class used to update opportunity Stages
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date            Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    05-May-2019       Sai           This class used to update opportunity Stages   
*/

public class OpportunityStageUpdateController {
    
    public Id opportunityId;     //This variable used to Store opportunityID
    public List<OpportunityHistoryWrapper> lstWrapper;
    public Map<String,Opportunity_Stage_History__c> mapOpportunityStage;
    public Map<String,Attachment> mapAttachments;
    public Opportunity opportunityRecord;
    public boolean isClosedOpportunity{get;set;}
    public Map<String,String> mapAttachmentName;
    
    public List<OpportunityHistoryWrapper> getOpportunityWrapper(){
        
        lstWrapper = new List<OpportunityHistoryWrapper>();
        isClosedOpportunity = FALSE;
        opportunityId = ApexPages.currentPage().getParameters().get('id');     // Variable used to ger Opportunity ID from URL
        String picklistValues = '';                                            // This Variable used to Picklist Values
        List<String> lstStageValues = new List<String>();                      // This Variable used to Store Opportunity Stages
        List<String> lstOpportunityClosedStages = new List<String>();          // This Variable used to store Opportunity Closed stages
        Set<String> setClosedStages = new Set<String>();
        Opportunity_Stage_History__c stageHistoryData;                         
        Attachment eachattachment;                                             // This Variable used to Store Attachments
        mapOpportunityStage = new Map<String,Opportunity_Stage_History__c>();
        Boolean isClosed =False;
        Blob bodyvalue = null;
        eachattachment = new Attachment();
        if(eachattachment == null){
            eachattachment = new Attachment(Body=bodyvalue);
        }
        opportunityRecord = new Opportunity();
         if(opportunityId !=null){
            
            opportunityRecord = [SELECT Id,Recordtype.Name,StageName,Opportunity_Stage_Update__c,CloseDate FROM Opportunity where ID=:opportunityId];
         }
         if(opportunityRecord.Recordtype.Name !=null && (opportunityRecord.Recordtype.Name == Label.BD_Non_Financial_Label || opportunityRecord.Recordtype.Name == LABEL.NFS)){
            picklistValues = LABEL.NFS_Stages;
            
            lstOpportunityClosedStages = Label.Closed_Opportunities_Non_Financial.split(',');
            setClosedStages.addall(lstOpportunityClosedStages);
            
            for(Opportunity_Stage_History__c stageHistory:[SELECT Stage__c FROM Opportunity_Stage_History__c where Related_Opportunity__c=:opportunityRecord.ID]){
                if(setClosedStages.contains(stageHistory.Stage__c)){
                    isClosed = TRUE;
                }
            }
            
            
            if(setClosedStages.contains(opportunityRecord.StageName) || isClosed){
                isClosedOpportunity = TRUE;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Closed_Opportunity_Error_Message));
            }               
         }
         
         else if(opportunityRecord.Recordtype.Name !=null && opportunityRecord.Recordtype.Name == Label.BD_Financial_Fund_Label){
            picklistValues = LABEL.BD_Financial_Fund;
            lstOpportunityClosedStages = Label.Closed_Opportunities_Non_Financial.split(',');
            setClosedStages.addall(lstOpportunityClosedStages);
            
            for(Opportunity_Stage_History__c stageHistory:[SELECT Stage__c FROM Opportunity_Stage_History__c where Related_Opportunity__c=:opportunityRecord.ID]){
                if(setClosedStages.contains(stageHistory.Stage__c)){
                    isClosed = TRUE;
                }
            }
            if(setClosedStages.contains(opportunityRecord.StageName) || isClosed){
                isClosedOpportunity = TRUE;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Closed_Opportunity_Error_Message));
            }   
         }
         else if(opportunityRecord.Recordtype.Name !=null && (opportunityRecord.Recordtype.Name == Label.BD_Financial_Label || opportunityRecord.Recordtype.Name == Label.BD_Financial_with_Security)){
            picklistValues = LABEL.BD_Financial;
            
            lstOpportunityClosedStages = Label.Closed_Opportunities.split(',');
            setClosedStages.addall(lstOpportunityClosedStages);
            
            for(Opportunity_Stage_History__c stageHistory:[SELECT Stage__c FROM Opportunity_Stage_History__c where Related_Opportunity__c=:opportunityRecord.ID]){
                if(setClosedStages.contains(stageHistory.Stage__c)){
                    isClosed = TRUE;
                }
            }
            if(setClosedStages.contains(opportunityRecord.StageName) || isClosed){
                isClosedOpportunity = TRUE;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Closed_Opportunity_Error_Message));
            }   
         }
         
         if(picklistValues!=null && picklistValues.split(',') !=null ){
            lstStageValues = picklistValues.split(',');
         }
         for(Opportunity_Stage_History__c eachHistory:[SELECT Stage__c,Created_Date_Time_History__c,Comments__c FROM Opportunity_Stage_History__c 
                                                              where Related_Opportunity__c =:opportunityRecord.ID]){
            mapOpportunityStage.put(eachHistory.Stage__c,eachHistory);
         }
         for(String eachStage:lstStageValues){
            if(mapOpportunityStage.get(eachStage) !=null){
                lstWrapper.add(new OpportunityHistoryWrapper(eachStage,mapOpportunityStage.get(eachStage),eachattachment));
            }
            else{
                stageHistoryData = new Opportunity_Stage_History__c();
                lstWrapper.add(new OpportunityHistoryWrapper(eachStage,stageHistoryData,eachattachment));
            }
         }
         return lstWrapper;
    }
    
    public pagereference saveStageHistory(){
        
        Opportunity_Stage_History__c opportunityHistoryInsert;
        List<Opportunity_Stage_History__c> lstStageHistory = new List<Opportunity_Stage_History__c>();
        List<Opportunity_Stage_History__c> lstOppHistoryUpdate = new List<Opportunity_Stage_History__c>();
        mapAttachmentName = new Map<String,String>();
        
        Boolean isStageHistoryInsert = TRUE;
        pagereference pref = null;
        Attachment eachAttachMent;
        mapAttachments = new Map<String,Attachment>();
        try{
            for(OpportunityHistoryWrapper eachWrapper:lstWrapper){
                if(eachWrapper.insertHistoryData.Created_Date_Time_History__c !=null){
                    if( eachWrapper.insertHistoryData.Created_Date_Time_History__c <= system.today()){  
                        if(eachWrapper.opportunityStage !=null && mapOpportunityStage.get(eachWrapper.opportunityStage) ==null){
                            if(eachWrapper.opportunityStage != Label.Letter_of_Intent_LOI_Received){                            
                                opportunityHistoryInsert = new Opportunity_Stage_History__c();
                                opportunityHistoryInsert = eachWrapper.insertHistoryData;
                                opportunityHistoryInsert.Related_Opportunity__c = opportunityId;
                                opportunityHistoryInsert.Stage__c = eachWrapper.opportunityStage;
                                opportunityHistoryInsert.Last_Modified_User__c = userinfo.getUserId();
                                opportunityHistoryInsert = eachWrapper.insertHistoryData;
                                opportunityHistoryInsert.Stage_History_Last_Modified_Date__c = DateTime.newInstance(opportunityRecord.CloseDate.year(),opportunityRecord.CloseDate.month(),opportunityRecord.CloseDate.day());
                                lstStageHistory.add(opportunityHistoryInsert);
                                
                                if(eachWrapper.attachmentRecord.body !=null){
                                    eachAttachMent = new Attachment();
                                    eachAttachMent.Name = eachWrapper.opportunityStage;
                                    eachAttachMent.BODY = eachWrapper.attachmentRecord.body;
                                    mapAttachmentName.put(eachWrapper.opportunityStage,eachWrapper.attachmentRecord.name);
                                    mapAttachments.put(eachWrapper.opportunityStage,eachAttachMent);
                                }
                            }
                            else if(eachWrapper.opportunityStage == Label.Letter_of_Intent_LOI_Received && eachWrapper.attachmentRecord.body !=null){
                                opportunityHistoryInsert = new Opportunity_Stage_History__c();
                                opportunityHistoryInsert = eachWrapper.insertHistoryData;
                                opportunityHistoryInsert.Related_Opportunity__c = opportunityId;
                                opportunityHistoryInsert.Stage__c = eachWrapper.opportunityStage;
                                opportunityHistoryInsert = eachWrapper.insertHistoryData;
                                opportunityHistoryInsert.Last_Modified_User__c = userinfo.getUserId();
                                 opportunityHistoryInsert.Stage_History_Last_Modified_Date__c = DateTime.newInstance(opportunityRecord.CloseDate.year(),opportunityRecord.CloseDate.month(),opportunityRecord.CloseDate.day());
                                lstStageHistory.add(opportunityHistoryInsert);
                                
                                if(eachWrapper.attachmentRecord.body !=null){
                                    eachAttachMent = new Attachment();
                                    eachAttachMent.Name = eachWrapper.opportunityStage;
                                    eachAttachMent.BODY = eachWrapper.attachmentRecord.body;
                                    mapAttachmentName.put(eachWrapper.opportunityStage,eachWrapper.attachmentRecord.name);
                                    mapAttachments.put(eachWrapper.opportunityStage,eachAttachMent);
                                }
                            }
                            else if(eachWrapper.opportunityStage == Label.Letter_of_Intent_LOI_Received && eachWrapper.attachmentRecord.body ==null){
                                isStageHistoryInsert = FALSE;
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.LOI_Documents_Error_message));
                            }
                        }
                        else if(mapOpportunityStage.get(eachWrapper.opportunityStage) !=null){
                            opportunity_Stage_History__c stageHistoryUpdate = new Opportunity_Stage_History__c();
                            stageHistoryUpdate = mapOpportunityStage.get(eachWrapper.opportunityStage);
                            stageHistoryUpdate.Created_Date_Time_History__c = eachWrapper.insertHistoryData.Created_Date_Time_History__c;
                            stageHistoryUpdate.Comments__c = eachWrapper.insertHistoryData.Comments__c;
                            lstOppHistoryUpdate.add(stageHistoryUpdate);
                        }
                    }
                    else if(eachWrapper.insertHistoryData.Created_Date_Time_History__c > system.today()){
                        isStageHistoryInsert = FALSE;
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Created_Date_Error_message));
                    }
                }
            }
            
            if(isStageHistoryInsert){
                
                if(lstStageHistory.size()>0){
                    database.insert(lstStageHistory);
                }
                if(lstOppHistoryUpdate.size()>0){
                    database.update(lstOppHistoryUpdate);
                }
                
                attachmentInsert(lstStageHistory);
                if(lstStageHistory.size()>0 || lstOppHistoryUpdate.size()>0){
                    Opportunity_Stage_History__c stageHistory = [SELECT Created_Date_Time_History__c,Comments__c,Stage__c FROM Opportunity_Stage_History__c 
                                                                 where Related_Opportunity__c =:opportunityRecord.ID ORDER BY Created_Date_Time_History__c desc limit 1];
                    opportunityRecord.StageName = stageHistory.Stage__c;
                    if(stageHistory.Created_Date_Time_History__c !=null){
                        opportunityRecord.CloseDate = Date.valueOf(stageHistory.Created_Date_Time_History__c);
                    }
                    opportunityRecord.Comments__c = stageHistory.Comments__c;
                    if(opportunityRecord.Opportunity_Stage_Update__c == true){
                        opportunityRecord.Opportunity_Stage_Update__c = FALSE;
                    }
                    else{
                        opportunityRecord.Opportunity_Stage_Update__c = true;
                    }
                    UPDATE opportunityRecord;                                
                    pref =  pageRedirect(opportunityId);
                }
                else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,Label.Please_enter_Created_Date));
                }
            }
        }
        catch(Exception ex){
            ApexPages.addMessages(ex);
        }
        return pref;
    }
    
    public pagereference cancel(){
        return pageRedirect(opportunityId);
    }
    
    public pagereference pageRedirect(String recordID){
        
        pagereference pref = new pagereference('/'+recordID);
        pref.setredirect(true);
        return pref;
    }
    
    public void attachmentInsert(List<Opportunity_Stage_History__c> lstStageHistory){
        
        Attachment attachmentInsert;
        List<Attachment> lstAttachments = new List<Attachment>();
        for(Opportunity_Stage_History__c eachHistory:lstStageHistory){
            if(mapAttachments !=null && mapAttachments.get(eachHistory.Stage__c) !=null){
                attachmentInsert = new Attachment();
                attachmentInsert = mapAttachments.get(eachHistory.Stage__c);
                attachmentInsert.ParentID = eachHistory.ID;
                attachmentInsert.Name = mapAttachmentName.get(eachHistory.Stage__c);
                lstAttachments.add(attachmentInsert);
            }
        }
        
        if(lstAttachments.size()>0){
            database.insert(lstAttachments);
        }
        
    } 
     
    public class OpportunityHistoryWrapper{
        
         public String opportunityStage{get;set;} 
         public Opportunity_Stage_History__c insertHistoryData{get;set;}
         public Attachment attachmentRecord{get;set;}
         
         public OpportunityHistoryWrapper(String opportunityStageVar,Opportunity_Stage_History__c insertHistoryDataVar,Attachment attachmentRecordsVar){
            opportunityStage = opportunityStageVar;
            insertHistoryData = insertHistoryDataVar;
            attachmentRecord = attachmentRecordsVar;
         }
         
    }
    
   public static void dummyTestMethod(){
        integer i=0;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        i++;
        
    }
}