/*********************************************************************************************************************
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------    
 Class to use to validate customer visa status : https://www.difc.ae/visastatus           

**********************************************************************************************************************/
@RestResource(urlMapping = '/DIFC_AramexPicPush/*')
global class CLS_StepPictureUpdate 
{
       @HttpPost
     global static string jsonPost(String fileUploadName,string AWBNumber,string fileUploadBody) 
     {
     string reValu='';
     List<Step__c> ListSteps=[select id from Step__c where AWB_Number__c=:AWBNumber];
     if(!ListSteps.isEmpty())
     {
         
         
             attachment aa = new attachment();
             aa.parentId = ListSteps[0].id; //'0012000001H3yX6';
             aa.Body = EncodingUtil.base64Decode(fileUploadBody);
             aa.Name = fileUploadName;
             aa.description = AWBNumber;
             insert aa;

               reValu='success';
        
     }
     else
     reValu='fail';
     
     return reValu;
     
        
     }
     
   


}