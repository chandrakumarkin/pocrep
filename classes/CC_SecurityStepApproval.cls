/*
    Author      : Durga Prasad
    Date        : 23-Dec-2019
    Description : Custom code to Update the Security status on Account
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_SecurityStepApproval implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';

        if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null){
            try{
                Account acc = new Account(Id=stp.HexaBPM__SR__r.HexaBPM__Customer__c);
                //acc.Security_Check_Done__c = true;
                acc.Security_Review_Date__c= system.now();
                acc.Security_Review_Status__c = 'Approved';
                update acc;
            }catch(Exception e){
                strResult = e.getMessage()+'';
            }
        }
        return strResult;
    }
}