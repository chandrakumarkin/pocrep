/******************************************************************************************************
 *  Author   : Kumar Utkarsh
 *  Date     : 08-Feb-2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    08-Feb-2021  Utkarsh         Created
* This class contains logic to to fetch the GDRFA look Up Id's.
*******************************************************************************************************/
public class WebServiceFetchMDFromGDRFAClass {
    
    public Class LookUpDetails {    
        public NameDetails name{get; set;}
        public String loopkupId{get; set;}
        public Boolean IsOutsideCountryCancelReason{get; set;}
        public Integer parentLookupId{get; set;}
    }

    public class NameDetails {
        public String ar{get; set;}
        public String en{get; set;}
    }
    
    //Callout Method to fetch the LookUp details
    @future(callout=true)
    public static void GDRFAFetchLookUpDetails() 
    { 
        List<GDRFA_Master_Data__c> gdrfaMDList = new List<GDRFA_Master_Data__c>();
        List<Log__c> listErrorLog = new List<Log__c>();
        HttpRequest AuthRequest = new HttpRequest();
        For(GDRFAIntegrationSettings__c gdrfa :  GDRFAIntegrationSettings__c.getall().values())
        {
            AuthRequest.setEndpoint(gdrfa.GDRFAEndPointURL__c);
            AuthRequest.setMethod('GET');
            String username = System.Label.GDRFA_Username;
            String password = System.Label.GDRFA_Password;
            Blob headerValue = Blob.valueOf(username+':'+password);
            AuthRequest.setHeader('apikey',System.Label.GDRFA_API_Key);        
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            AuthRequest.setHeader('Authorization', authorizationHeader);  
            AuthRequest.setTimeout(120000);
            System.HttpResponse response = new System.Http().send(AuthRequest);
            System.debug('responseString:'+response.toString());
            System.debug('STATUS:'+response.getStatus());
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('Response ' +response.getBody()); 
            
            if (response.getStatusCode() == 200)
            {
                String strjson = response.getbody();
                List<LookUpDetails> wServiceList= (List<LookUpDetails>) JSON.deserialize(strjson, List<LookUpDetails>.class);
                if(wServiceList.size() >= 1 && wServiceList != null){
                    for(integer i= 0; i < wServiceList.size(); i++){
                        GDRFA_Master_Data__c gdrfaMd = new GDRFA_Master_Data__c();
                        if(wServiceList[i].loopkupId != null){
                            gdrfaMd.Look_Up_Id__c = wServiceList[i].loopkupId;
                        }
                        if(wServiceList[i].IsOutsideCountryCancelReason != null){
                            gdrfaMd.Outside_Country_Cancel_Reason__c = wServiceList[i].IsOutsideCountryCancelReason;
                        }
                        if(wServiceList[i].parentLookupId != null){
                            gdrfaMd.Parent_LookUp_Id__c = wServiceList[i].parentLookupId;
                        }
                        gdrfaMd.Lookup_Type__c = gdrfa.Name;
                        if(wServiceList[i].name.ar != null){
                            gdrfaMd.Name_Arabic__c = wServiceList[i].name.ar;
                        }
                        if(wServiceList[i].name.en != null){
                            gdrfaMd.Name_English__c = wServiceList[i].name.en;
                        }
                        gdrfaMd.GDRFA_Unique_Value__c = gdrfa.Name+'-'+wServiceList[i].loopkupId;
                        gdrfaMDList.add(gdrfaMd);
                    }                        
                }                   
            }
            else
            {
                Log__c objLog = new Log__c();
                objLog.Description__c = response.getStatus();
                objLog.Type__c = 'GDRFA Error'+''+gdrfa.Name;
                listErrorLog.add(objLog);    
            }  
        }
        upsert gdrfaMDList GDRFA_Unique_Value__c;
        
        if(listErrorLog.size() >= 1){
            Database.SaveResult[] srList = Database.insert(listErrorLog, false);
            for (Database.SaveResult sr : srList) {
                if (sr.isSuccess()) {
                    // Operation was successful, so get the ID of the record that was processed
                    System.debug('Successfully inserted account. Log ID: ' + sr.getId());
                }
                else {
                    // Operation failed, so get all errors                
                    for(Database.Error err : sr.getErrors()) {
                        System.debug('The following error has occurred.');                    
                        System.debug(err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('log fields that affected this error: ' + err.getFields());
                    }
                }
            }
        }
    }
}