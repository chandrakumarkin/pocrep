@isTest
public class OB_AmendmentfileUploadOCRTest 
{

    static testMethod void viewSRDocsTest() 
    {
        // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'GENERAL'});
        insert listDocMaster;
        // create SR Template doc
        
         //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'In_Principle','AOR_Financial'});
        insert createSRTemplateList;
        
        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        insert listSRTemplateDoc;
        
        
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
       
        
      
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        insert listSRSteps;
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        insert listPage;
        
        
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(1, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        
        insertNewSRs[0].HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        insert insertNewSRs;
        
        //create countryListScoring
        list<Country_Risk_Scoring__c> listCS = new list<Country_Risk_Scoring__c>();
        listCS = OB_TestDataFactory.createCountryRiskScorings(1,new List<String>{'Low'},new List<Integer>{1}, new List<String>{'Low'});
        insert listCS;
        
        
        
       
        
        
         //create amendments
        list<HexaBPM_Amendment__c> listAmendments = new list<HexaBPM_Amendment__c>() ;
        listAmendments = OB_TestDataFactory.createApplicationAmendment(2,listCS,insertNewSRs);
        listAmendments[0].ServiceRequest__c = insertNewSRs[0].Id;
       	listAmendments[0].Status__c = 'Active';
        insert listAmendments;
        
         // create SR Doc
        List<HexaBPM__SR_Doc__c> listSRDoc = new List<HexaBPM__SR_Doc__c>();
        listSRDoc = OB_TestDataFactory.createSRDoc(1, insertNewSRs, listDocMaster, listSRTemplateDoc);
         listSRDoc[0].HexaBPM_Amendment__c = 	listAmendments[0].Id;
        insert listSRDoc;
        
         ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = listSRDoc[0].id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
        Service_Request__c oldsr = new Service_Request__c ();
        insert oldsr;
        
        Amendment__c oldAmend = new Amendment__c();
        oldAmend.ServiceRequest__c = oldsr.id;
        insert oldAmend;
        
        Document_Master__c oldDocMaster = new Document_Master__c();
        oldDocMaster.Code__c = 'GENERAL';
        insert oldDocMaster;
        
        SR_Doc__c oldSrdoc = new SR_Doc__c();
        oldSrdoc.Amendment__c = oldAmend.id;
        oldSrdoc.Service_Request__c = oldsr.id;
        oldSrdoc.Document_Master__c = oldDocMaster.id;
        insert oldSrdoc;
        
        ContentVersion contentVersionInsert2 = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert2;
        List<ContentDocument> documents2 = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl2 = New ContentDocumentLink();
        cdl2.LinkedEntityId = oldSrdoc.id;
        cdl2.ContentDocumentId = documents2[0].Id;
        cdl2.shareType = 'V';
        //insert cdl2;
        //
        //
        Attachment attach=new Attachment();    
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=oldSrdoc.id;
        insert attach;

        
        
        test.startTest();
        OB_AmendmentfileUploadOCR.RequestWrapper reqParam = new OB_AmendmentfileUploadOCR.RequestWrapper();
        
         	 reqParam.flowId = listPageFlow[0].Id;
             reqParam.pageId = listPage[0].Id;
             reqParam.srId = insertNewSRs[0].Id;
             reqParam.amedId = listAmendments[0].Id;
             reqParam.docMasterCode = 'GENERAL';
             //reqParam.stepId = insertNewSteps[0].Id;
             //reqParam.stepObj = insertNewSteps[0];
             String reqParamStringSRDocs = JSON.serialize(reqParam);    
       		 OB_AmendmentfileUploadOCR.viewSRDocs(reqParamStringSRDocs);
        OB_AmendmentfileUploadOCR.viewRelatedSRDocs(reqParamStringSRDocs);
             OB_AmendmentfileUploadOCR.processOCR(documents[0].Id);
        
        reqParam.amedId = oldAmend.Id;
              reqParamStringSRDocs = JSON.serialize(reqParam);    
        OB_AmendmentfileUploadOCR.viewRelatedSRDocs(reqParamStringSRDocs);
        OB_AmendmentfileUploadOCR.viewRelatedSRDocs(reqParamStringSRDocs);
             
        
        test.stopTest();
        
        
        
        
    }
    
    
}