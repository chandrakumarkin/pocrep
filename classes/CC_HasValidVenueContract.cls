/*
    Author      : Durga Prasad
    Date        : 04-April-2020
    Description : Custom code to check the whether the commercial permission has valid Venue Contract
    -------------------------------------------------------------------------------------------------
* Version    Author    					Date           Comment                                                                *
  V.1.1      salma.rasheed@pwc.com   	15/12/2020     Added condition for Type of Commercial Permission as Pocket Shops
*/

global without sharing class CC_HasValidVenueContract implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp){
        string strResult = 'No Valid Venue Contract available for the Customer.';
        if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null){
        	if(stp.HexaBPM__SR__r.Type_of_commercial_permission__c=='ATM' || stp.HexaBPM__SR__r.Type_of_commercial_permission__c=='Vending Machines' || stp.HexaBPM__SR__r.Type_of_commercial_permission__c=='Dual license of DED licensed firms with an affiliate in DIFC' || stp.HexaBPM__SR__r.Type_of_commercial_permission__c=='Gate Avenue Pocket Shops'){ //V.1.1 - Added condition for Pocket Shops
	        	strResult = 'No Valid lease available for the Customer.';
	        	for(Lease__c lease:[Select Id from Lease__c where Account__c=:stp.HexaBPM__SR__r.HexaBPM__Customer__c and Is_Lease_valid_for_selection__c=true]){
	        		strResult = 'Success';
	        	}
        	}else{
	        	for(OB_Event_Contract__c EC:[Select Id from OB_Event_Contract__c where Account__c=:stp.HexaBPM__SR__r.HexaBPM__Customer__c and Is_Active__c=true]){
	        		strResult = 'Success';
	        	}
        	}
        }
    	return strResult;
    }
}