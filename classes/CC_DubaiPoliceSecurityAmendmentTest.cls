/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 06-01-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   05-31-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest(seeAllData=false)
public class CC_DubaiPoliceSecurityAmendmentTest {
    
    static testmethod void testMethod1() {
       
        Account objAccount = new Account(Name = 'Test Account',Home_Country__c ='India');
        insert objAccount;
        
        Contact objContact = new Contact(LastName = 'Test Contact',FirstName = 'Test Contact',
            AccountId = objAccount.Id,Email = 'test@difc.com',
            Nationality__c  = 'Afghanistan',Previous_Nationality__c ='India',
            Country_of_Birth__c = 'India',Country__c = 'India',MobilePhone ='+971569803616' ,
            UID_Number__c = '1234567',Passport_No__c = 'JL123456',Issued_Country__c ='Egypt',
            Date_of_Issue__c = system.today().addYears(-2),   
            Passport_Expiry_Date__c = system.today().addYears(4)
        );
        insert objContact;
        
       

        Id gsContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = gsContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        Profile testProfile = [SELECT Id 
                               FROM profile
                               WHERE Name = 'DIFC Customer Community User Custom' 
                               LIMIT 1];
    
        User testUser = new User(LastName = 'test2', 
                                 Username = 'test.user.1@example.com', 
                                 Email = 'test.1@example.com', 
                                 Alias = 'testu1', 
                                 TimeZoneSidKey = 'GMT', 
                                 LocaleSidKey = 'en_GB', 
                                 EmailEncodingKey = 'ISO-8859-1', 
                                 ProfileId = testProfile.Id, 
                                 ContactId = objContact.Id,
                                 Community_User_Role__c='Company Services'+';'+'Employee Services'+';'+'Property Services',
                                 LanguageLocaleKey = 'en_US');     
        insert testUser;

        SR_Template__c objTemplate = new SR_Template__c(Name = 'Allotment_of_Shares_Membership_Interest',SR_RecordType_API_Name__c = 'Allotment_of_Shares_Membership_Interest');
        objTemplate.Menutext__c = 'Allotment_of_Shares_Membership_Interest';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c(Name = 'Required Docs to Upload');
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c(SR_Template__c = objTemplate.Id,Document_Master__c = objDocMaster.Id,On_Submit__c = true,Generate_Document__c = true);
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        Lookup__c nat1 = new Lookup__c(Type__c='Nationality',Name='Afghanistan');
        insert nat1;
        Lookup__c nat2 = new Lookup__c(Type__c='Nationality',Name='India');
        insert nat2;
        Lookup__c nat3 = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat3;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Allotment_of_Shares_Membership_Interest' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        System.runAs(testUser) {  
            test.starttest();
                Test.setMock(HttpCalloutMock.class, new CC_DubaiPoliceSecurityAmendmentMockTest());
            
                Document document = new Document(Body = Blob.valueOf('Some Text'),ContentType = 'application/pdf',DeveloperName = 'Dubai_Police_Amendment_Portal_User_Document',IsPublic = true,Name = 'My Document');
                document.FolderId = testUser.Id;
                insert document;
                
                Service_Request__c objSR = new Service_Request__c();
                objSR.Express_Service__c = true;
                objSR.Customer__r = objAccount;
                objSR.Customer__c = objAccount.id;
                objSR.RecordTypeId = issueFineId;
                objSR.submitted_date__c = date.today();  
                //objSR.Place_of_Registration_parent_company__c ='India';
                // objSR.Internal_SR_Status__c = objStatus.id;
                //objSR.External_SR_Status__c = objStatus.id;
                objSR.contact__c = objContact1.id;
                objSR.Sponsor__c = objContact1.Id;
                insert objSR;
                Id indivi = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
                Id bodyCorp = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Body Corporate').getRecordTypeId();
                List<Amendment__c> amendMentList = new List<Amendment__c>{
                    new Amendment__c(RecordTypeId=indivi,Given_Name__c='Test',Family_Name__c='Test',First_Name__c='Test',
                        Passport_No__c='test123300',Nationality_list__c='Pakistan',ServiceRequest__c=objSR.Id,
                        Relationship_Type__c  = 'Shareholder',Status__c='Draft',
                        Amendment_Type__c  = 'Individual',Mobile__c = '+971569803616',
                        Address__c ='test',Permanent_Native_City__c ='test',Apt_or_Villa_No__c ='test',
                        Emirate_State__c ='Dubai',Permanent_Native_Country__c ='India',
                        PO_Box__c ='1234'
                    ),
                    new Amendment__c(RecordTypeId=bodyCorp,Given_Name__c='Test',Family_Name__c='Test',First_Name__c='Test',
                        Nationality_list__c='Pakistan',ServiceRequest__c=objSR.Id,
                        Relationship_Type__c  = 'Shareholder',Status__c='Draft',
                        Amendment_Type__c  = 'Body Corporate'
                    )
                };
                insert amendMentList;
                
                
                SR_Doc__c objSRDoc = new SR_Doc__c();
                objSRDoc.Service_Request__c = objSR.Id;
                objSRDoc.Is_Not_Required__c = false;
                objSRDoc.Group_No__c = 1;
                objSRDoc.Name = 'Passport Copy';
                objSRDoc.Amendment__c = amendMentList[0].Id;
                insert objSRDoc;

                Blob b = Blob.valueOf('Test Data');
                Attachment attachment = new Attachment();
                attachment.ParentId = objSRDoc.Id;
                attachment.Name = 'Passport Document';
                attachment.Body = b;
                insert attachment;

                objSRDoc.Doc_ID__c = attachment.Id;
                update objSRDoc;

                
                //Body_Corporate
                Status__c objStatus = new Status__c();
                objStatus.Name = 'Pending';
                objStatus.Type__c = 'Pending';
                objStatus.Code__c = 'Pending';
                insert objStatus;
        
                Step_Template__c objStepType1 = new Step_Template__c();
                objStepType1.Name = 'Security Email Appproval';
                objStepType1.Code__c = 'Security Email Appproval';
                objStepType1.Step_RecordType_API_Name__c = 'General';
                objStepType1.Summary__c = 'Security Email Appproval';
                insert objStepType1;
        
                step__c objStep = new Step__c();
                objStep.SR__c = objSR.Id;
                objStep.Step_Template__c = objStepType1.id ;
                objStep.status__c = objStatus.id;
                insert objStep;
                system.debug('==^^^^^^^^^^^^====objStep========='+objStep);
                CC_DubaiPoliceSecurityAmendment ccs = new CC_DubaiPoliceSecurityAmendment();
                CC_DubaiPoliceSecurityAmendment.stepNormalBpmHelperRecord(objStep.Id,'');
                
            
            test.stoptest();
        }
    }
    
    static testmethod void testMethod2() {
       
        Account objAccount = new Account(Name = 'Test Account',Home_Country__c ='India');
        insert objAccount;
        
        Contact objContact = new Contact(LastName = 'Test Contact',FirstName = 'Test Contact',
            AccountId = objAccount.Id,Email = 'test@difc.com',
            Nationality__c  = 'Afghanistan',Previous_Nationality__c ='India',
            Country_of_Birth__c = 'India',Country__c = 'India',MobilePhone ='+971569803616' ,
            UID_Number__c = '1234567',Passport_No__c = 'JL123456',Issued_Country__c ='Egypt',
            Date_of_Issue__c = system.today().addYears(-2),   
            Passport_Expiry_Date__c = system.today().addYears(4)
        );
        insert objContact;
        
       

        Id gsContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = gsContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        Profile testProfile = [SELECT Id 
                               FROM profile
                               WHERE Name = 'DIFC Customer Community User Custom' 
                               LIMIT 1];
    
        User testUser = new User(LastName = 'test2', 
                                 Username = 'test.user.1@example.com', 
                                 Email = 'test.1@example.com', 
                                 Alias = 'testu1', 
                                 TimeZoneSidKey = 'GMT', 
                                 LocaleSidKey = 'en_GB', 
                                 EmailEncodingKey = 'ISO-8859-1', 
                                 ProfileId = testProfile.Id, 
                                 ContactId = objContact.Id,
                                 Community_User_Role__c='Company Services'+';'+'Employee Services'+';'+'Property Services',
                                 LanguageLocaleKey = 'en_US');     
        insert testUser;

        SR_Template__c objTemplate = new SR_Template__c(Name = 'Allotment_of_Shares_Membership_Interest',SR_RecordType_API_Name__c = 'Allotment_of_Shares_Membership_Interest');
        objTemplate.Menutext__c = 'Allotment_of_Shares_Membership_Interest';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c(Name = 'Required Docs to Upload');
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c(SR_Template__c = objTemplate.Id,Document_Master__c = objDocMaster.Id,On_Submit__c = true,Generate_Document__c = true);
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        Lookup__c nat1 = new Lookup__c(Type__c='Nationality',Name='Afghanistan');
        insert nat1;
        Lookup__c nat2 = new Lookup__c(Type__c='Nationality',Name='India');
        insert nat2;
        Lookup__c nat3 = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat3;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Allotment_of_Shares_Membership_Interest' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        System.runAs(testUser) {  
            test.starttest();
                Test.setMock(HttpCalloutMock.class, new CC_DubaiPoliceSecurityAmendmentMockTest());
            
                Document document = new Document(Body = Blob.valueOf('Some Text'),ContentType = 'application/pdf',DeveloperName = 'Dubai_Police_Amendment_Portal_User_Document',IsPublic = true,Name = 'My Document');
                document.FolderId = testUser.Id;
                insert document;
                
                Service_Request__c objSR = new Service_Request__c();
                objSR.Express_Service__c = true;
                objSR.Customer__r = objAccount;
                objSR.Customer__c = objAccount.id;
                objSR.RecordTypeId = issueFineId;
                objSR.submitted_date__c = date.today();  
                //objSR.Place_of_Registration_parent_company__c ='India';
                // objSR.Internal_SR_Status__c = objStatus.id;
                //objSR.External_SR_Status__c = objStatus.id;
                objSR.contact__c = objContact1.id;
                objSR.Sponsor__c = objContact1.Id;
                insert objSR;
                Id indivi = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
                Id bodyCorp = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Body Corporate').getRecordTypeId();
                List<Amendment__c> amendMentList = new List<Amendment__c>{
                    new Amendment__c(RecordTypeId=indivi,Given_Name__c='Test',Family_Name__c='Test',First_Name__c='Test',
                        Passport_No__c='test123300',Nationality_list__c='Pakistan',ServiceRequest__c=objSR.Id,
                        Relationship_Type__c  = 'Shareholder',Status__c='Draft',
                        Amendment_Type__c  = 'Individual',Mobile__c = '+971569803616',
                        Address__c ='test',Permanent_Native_City__c ='test',Apt_or_Villa_No__c ='test',
                        Emirate_State__c ='Dubai',Permanent_Native_Country__c ='India',
                        PO_Box__c ='1234'
                    ),
                    new Amendment__c(RecordTypeId=bodyCorp,Given_Name__c='Test',Family_Name__c='Test',First_Name__c='Test',
                        Nationality_list__c='Pakistan',ServiceRequest__c=objSR.Id,
                        Relationship_Type__c  = 'Shareholder',Status__c='Draft',
                        Amendment_Type__c  = 'Body Corporate'
                    )
                };
                insert amendMentList;
                
                
                SR_Doc__c objSRDoc = new SR_Doc__c();
                objSRDoc.Service_Request__c = objSR.Id;
                objSRDoc.Is_Not_Required__c = false;
                objSRDoc.Group_No__c = 1;
                objSRDoc.Name = 'Passport Copy';
                objSRDoc.Amendment__c = amendMentList[0].Id;
                insert objSRDoc;

                Blob b = Blob.valueOf('Test Data');
                Attachment attachment = new Attachment();
                attachment.ParentId = objSRDoc.Id;
                attachment.Name = 'Passport Document';
                attachment.Body = b;
                insert attachment;

                objSRDoc.Doc_ID__c = attachment.Id;
                update objSRDoc;

                
                //Body_Corporate
                Status__c objStatus = new Status__c();
                objStatus.Name = 'Pending';
                objStatus.Type__c = 'Pending';
                objStatus.Code__c = 'Pending';
                insert objStatus;
        
                Step_Template__c objStepType1 = new Step_Template__c();
                objStepType1.Name = 'Security Email Appproval';
                objStepType1.Code__c = 'Security Email Appproval';
                objStepType1.Step_RecordType_API_Name__c = 'General';
                objStepType1.Summary__c = 'Security Email Appproval';
                insert objStepType1;
        
                step__c objStep = new Step__c();
                objStep.SR__c = objSR.Id;
                objStep.Step_Template__c = objStepType1.id ;
                objStep.status__c = objStatus.id;
                objStep.DIFC_Security_Email_Response__c ='235';
                insert objStep;
                system.debug('==^^^^^^^^^^^^====objStep========='+objStep);
                
                BPM_SecurityStepStatusUpdateBatch x = new BPM_SecurityStepStatusUpdateBatch();
                database.executeBatch(x);
               // List<Step__c> stepListNew = [SELECT Id,Name,Step_Status__c,SR__r.Customer__c,Status__c,SR__r.Name FROM Step__c limit 1];
                //BPM_SecurityStepStatusUpdateBatch.executeBPMRec(stepListNew,'');
                
                BPM_SecurityStepStatusUpdateBatchSch sh1= new BPM_SecurityStepStatusUpdateBatchSch();
                String sch = '0 0 23 * * ?';
                system.schedule('Test check', sch, sh1);
                
            test.stoptest();
        }
    }
    
}