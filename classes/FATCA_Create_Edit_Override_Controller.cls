public class FATCA_Create_Edit_Override_Controller{
    public String selectedKey{get; set;}
    public String selectedKeyB{get; set;}
    public Map<Integer,Fatca__c> fatcaMap{get; set;}
    public Map<Integer,Fatca__c> fatcaBMap{get; set;}
    public String giin{get; set;}
    public Boolean portalUser{get; set;}
    public String accName{get; set;}
    public Boolean approvedErrorMessage{get; set;}
    public Boolean errorMsg{get; set;}
    public String duplicateErrorMessage{get; set;}
    public Boolean noData{get; set;}
    public String validationErrorMessage{get; set;}
    public Boolean showValError{get; set;}
    
    FATCA_Submission__c fsub;
    Integer count = 1;
    Integer countB = 1;
    public FATCA_Create_Edit_Override_Controller(ApexPages.StandardController controller){
        if(!Test.isRunningTest()) 
            controller.addFields(new List<String>{'Reporting_FI__c'});

        fsub = (FATCA_Submission__c)controller.getRecord();
        fsub.As_On_Date__c = System.today();
        
        String userType = UserInfo.getUserType();
        
        if(userType.contains('CustomerSuccess')){
            portalUser = true;
            for(User userObj : [Select Contact.AccountId,Contact.Account.Name,Contact.Account.GIIN__c from User where id =: UserInfo.getUserId()]){
                fsub.Reporting_FI__c = userObj.Contact.AccountId;
                accName = userObj.Contact.Account.Name;
                giin = userObj.Contact.Account.GIIN__c;
            }
        } else{
            portalUser = false;
            if(fsub.Reporting_FI__c != null){
                Account acc = [Select Name,GIIN__c from Account where id=:fsub.Reporting_FI__c];
                accName = acc.Name;
                giin = acc.GIIN__c;
            }
        }
        
        fatcaMap = new Map<Integer,Fatca__c>();
        fatcaBMap = new Map<Integer,Fatca__c>();
        
        if(fsub.id==null){
            fsub.Status__c = 'Draft';
            for(Integer i=0; i<3; i++){
                fatcaMap.put(count, new Fatca__c(Status__c='New',Template_ID__c='FATCA-REP-A'));
                count = count+1;
            }
            for(Integer j=0; j<3; j++){
                fatcaBMap.put(countB, new Fatca__c(Status__c='New',Template_ID__c='FATCA-REP-B'));
                countB = countB+1;
            }
        }else{
            List<Fatca__c> fatcaList = [select Id,Sent_to_MoF__c,comment__c,Template_ID__c,FATCA_Submission__c,Name,First_Name__c,Last_Name__c,Entity_Name__c,US_TIN__c,Account_Holder_Type__c,Birth_Date__c,Address__c,City__c,Country_Code_Address__c,Account_Number__c,Account_Balance__c,Payment_Amount__c,Payment_Type__c,Account_Type__c,Status__c from FATCA__c where Status__c != 'Removed' and Fatca_Submission__c = :fsub.id];
            if(fatcaList.isEmpty()){
                add();
                addB();
            } else{
                for(Fatca__c fatca : fatcaList){
                    if(fatca.Template_ID__c == 'FATCA-REP-A'){
                        fatcaMap.put(count, fatca);
                        count = count+1;
                    }
                    if(fatca.Template_ID__c == 'FATCA-REP-B'){
                        fatcaBMap.put(countB, fatca);
                        countB = countB+1;
                    }
                }
            }  
        }    
    }
    
    public void add(){
        for(Integer i=0; i<2; i++){
            fatcaMap.put(count, new Fatca__c(Status__c='New',Template_ID__c='FATCA-REP-A'));
            count = count+1;
        }
    }
    
    public void addB(){
        for(Integer j=0; j<2; j++){
            fatcaBMap.put(countB, new Fatca__c(Status__c='New',Template_ID__c='FATCA-REP-B'));
            countB = countB+1;
        }
    }
    
    public void del(){
        try{
            if(fatcaMap.get(Integer.valueOf(selectedKey)).id != null){
                Fatca__c fatc = fatcaMap.get(Integer.valueOf(selectedKey));
                System.debug('fatc========='+fatc);
                if(fatc.Sent_To_MoF__c == true && fatc.Status__c == 'Rejected'){
                    fatc.Status__c = 'Removed';
                    update fatc;
                }
                else
                    delete fatcaMap.get(Integer.valueOf(selectedKey));
            }
            fatcaMap.remove(Integer.valueOf(selectedKey));
        } catch(Exception e){
            ApexPages.addMessages(e);
        }
    }
    
    public void delB(){
        try{
            if(fatcaBMap.get(Integer.valueOf(selectedKeyB)).id != null){
                Fatca__c fatc = fatcaBMap.get(Integer.valueOf(selectedKeyB));
                if(fatc.Sent_To_MoF__c == true && fatc.Status__c == 'Rejected'){
                    fatc.Status__c = 'Removed';
                    update fatc;
                }
                else
                    delete fatcaBMap.get(Integer.valueOf(selectedKeyB));
            }
            fatcaBMap.remove(Integer.valueOf(selectedKeyB));
        } catch(Exception e){
            ApexPages.addMessages(e);
        }
    }
    
    public void deleteRows(){
        List<Fatca__c> fatcaToDelete = new List<Fatca__c>();
        for(Fatca__c fatca : fatcaMap.values()){
            if(fatca.id != null)
                fatcaToDelete.add(fatca);
        }
        fatcaMap.clear();
        if(fatcaToDelete.size()>0) delete fatcaToDelete;
        List<Fatca__c> fatcaBToDelete = new List<Fatca__c>();
        for(Fatca__c fatca : fatcaBMap.values()){
            if(fatca.id != null)
                fatcaBToDelete.add(fatca);
        }
        fatcaBMap.clear();
        if(fatcaBToDelete.size()>0) delete fatcaBToDelete;
    }
    public String giinErrorMessage{get; set;}
    public PageReference save(){
        if(giin==null || giin == ''){
            
            giinErrorMessage = 'You must enter a value';
            return null;
        }
        Savepoint sp = Database.setSavepoint();
        Boolean isInsert = false;
        if(fsub.id==null){
            isInsert = true;
            try{
                if(giin != null && giin != '' && fsub.Reporting_FI__c != null){
                    Account acc = new Account(Id=fsub.Reporting_FI__c);
                    acc.GIIN__c = giin; 
                    update acc;
                }
                insert fsub;
            } catch(Exception ex){
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage()));
                //ApexPages.addMessages(e);
                String errMsg = ex.getMessage();
                if(errMsg.contains('GIIN')){
                    giinErrorMessage = 'GIIN should be entered with appropriate punctuation (period or decimal). Example: 98Q96B.00000.LE.250';
                    giin = null;
                }else{
                    //ApexPages.addMessages(ex.getMessage());
                    //giinErrorMessage = ex.getMessage().split(',')[1];
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.FATAL,ex.getMessage()));
                    validationErrorMessage = 'You do not have permission to perform this operation.';
                    showValError = true;
                }
                return null;
            }
            
        }else{
            if(fsub.Status__c == 'Approved'){
                for(Fatca__c fatca : fatcaMap.values()){
                    if(fatca.Status__c=='Rejected'){
                        approvedErrorMessage = true;
                        return null;
                    }
                    else
                        fatca.Status__c = 'Approved';
                }
                for(Fatca__c fatca : fatcaBMap.values()){
                    if(fatca.Status__c=='Rejected'){
                        approvedErrorMessage = true;
                        return null;
                    }
                    else
                        fatca.Status__c = 'Approved';
                }
            }
            try{
                update fsub;
            }catch(Exception ex){
                //ApexPages.addMessages(ex);
                validationErrorMessage = 'You do not have permission to perform this operation.';
                showValError = true;
                return null;
            }
            
        }
        
        string fatcaReportingPeriod = '';
        fatcaReportingPeriod = Label.FATCA_Reporting_Period;        
        for(Fatca__c fatca : fatcaMap.values()){
            fatca.FATCA_Submission__c = fsub.id;
            fatca.Key__c = giin + '--' + fatca.US_TIN__c + '--' + fatca.Account_Holder_Type__c + '--' + fatca.Payment_Type__c + '--' + fatca.Account_Type__c + '--' + fatca.Account_Number__c + '--' + fatcaReportingPeriod;
        }
        for(Fatca__c fatca : fatcaBMap.values()){
            fatca.FATCA_Submission__c = fsub.id;
            fatca.Key__c = giin + '--' + fatca.US_TIN__c + '--' + fatca.Account_Holder_Type__c + '--' + fatca.Payment_Type__c + '--' + fatca.Account_Type__c + '--' + fatca.Account_Number__c + '--' + fatcaReportingPeriod;
        }
        try{
            
            upsert fatcaMap.values();
            
            List<Fatca__c> fatcaBList = new List<Fatca__c>();
            for(Fatca__c f : fatcaBMap.values()){
                if(f.Entity_Name__c != null && f.Entity_Name__c != '' && f.US_TIN__c != null && f.US_TIN__c != '')
                    fatcaBList.add(f);
            }
            if(fatcaBList.size()>0) {
                string upsertKey = '';
                
                upsert fatcaBList;
            }
                
        } catch(Exception e){
            if(e.getMessage().contains('duplicate')){
                duplicateErrorMessage = 'Duplicate Entries found';
                errorMsg = true;
            }
            //else duplicateErrorMessage = e.getMessage();
            
            Database.rollback(sp);
            if(isInsert) fsub.id = null;
            System.debug('fsub id ----------------- '+fsub.id);
            return null;          
        }
        return new ApexPages.StandardController(fsub).view();
    }
    
    // Method added by Shabbir Ahmed - 04/June/2015 to record FATCA Audit
    public void CreateFATCAAuditRecord(){
        String userType = UserInfo.getUserType();
        if(!userType.contains('CustomerSuccess')){
            string url = '';
            string ipAddress = 'Not captured';
            if(Apexpages.currentPage() != NULL){
                url = Apexpages.currentPage().getUrl();
                ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
            }
            FATCAAudit.CreateAuditRecord(fsub.Id,url,ipAddress);        
        }   

    }   
    
}