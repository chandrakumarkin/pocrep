/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PageFlowUtil {
    global HexaBPM__Service_Request__c objRequest;
    global static SObject objSR;
    global static String ReqFieldIds;
    global static String strHiddenPageIds;
    global PageFlowUtil() {

    }
    global static Boolean ConvertToBoolean(String FieldValue) {
        return null;
    }
    global static Date ConvertToDate(String FieldValue) {
        return null;
    }
    global static Datetime ConvertToDateTime(String FieldValue) {
        return null;
    }
    global static Decimal ConvertToDecimal(String FieldValue) {
        return null;
    }
    global static Set<String> FetchObjectFields(String strFlowId, String strObjectName) {
        return null;
    }
    global static Boolean executeNavigationRules(String strTextCondition, SObject DyncObject) {
        return null;
    }
    global System.PageReference getButtonAction(String ActionId) {
        return null;
    }
    global Component.apex.pageblock getButtonsPB(String FlowId, String PageId, String PanelId) {
        return null;
    }
    global Component.apex.pageblock getFlowPageContent(String FlowId, String PageId) {
        return null;
    }
    global Component.apex.pageblock getFlowPageReadonlyContent(String FlowId, String PageId) {
        return null;
    }
    global static String getHiddenPageIds(String strFlowId, SObject objDycObject) {
        return null;
    }
    global System.PageReference getSideBarReference(String strSideBarPageId) {
        return null;
    }
    global static Boolean parseConditions(Boolean lastConditionResult, Boolean conditionResult, String conditionStrng) {
        return null;
    }
    global static Boolean parseEachCondition(String SRVal, String oprtr, String conditionVal) {
        return null;
    }
}
