/***********************************************************************************
 *  Author   : Swati
 *  Company  : NSI
 *  Date     : 30/03/2016
 *  Purpose  : This is test class for cls_SRTemplateDescription
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date        Updated By  Description
 ----------------------------------------------------------------------------------------              
      
 
 **********************************************************************************************/

@isTest 
private class Cls_SRTemplateDescription_Test {
    
    static testMethod void validateGetRelatedSRTemplate() {
    	PageReference pgref = Page.SRTemplateDescription;
    	Test.setCurrentPageReference(pgref); 
    	ApexPages.currentPage().getParameters().put('type','Lease_Application_Request');
    	Cls_SRTemplateDescription obj = new Cls_SRTemplateDescription();
        System.assert(true, 'Running as Expected');
    }
}