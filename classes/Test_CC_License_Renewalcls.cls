/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CC_License_Renewalcls {

    static testMethod void myUnitTest() {
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        License__c objLic = new License__c();
        objLic.Account__c = objAcc.Id;
        objLic.License_Issue_Date__c = system.today();
        objLic.License_Expiry_Date__c = system.today().addDays(365);
        objLic.Status__c = 'Active';
        insert objLic;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAcc.Id;
        objSR.License_Renewal_Term__c = '1';
        insert objSR;
        
        Step__c stp = new Step__c();
        stp.SR__c = objSR.Id;
        insert stp;
        
        Step__c objStp = [select id,SR__c,SR__r.Customer__c,SR__r.License_Renewal_Term__c from Step__c where Id=:stp.Id];
        CC_License_Renewalcls.Renew_License(objStp);
        
        
        objLic.Status__c = 'In Active';
        update objLic;
        
        CC_License_Renewalcls.Renew_License(objStp);
    }
}