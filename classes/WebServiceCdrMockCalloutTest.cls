@isTest
global class WebServiceCdrMockCalloutTest implements HttpCalloutMock {
    
    global HTTPResponse respond(HTTPRequest req) {
        HTTPResponse res = new HTTPResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{}');
        if (req.getEndpoint().endsWith('/api/Account') ||
            req.getEndpoint().endsWith('api/Activity') ||
            req.getEndpoint().endsWith('api/License')
        ) {
            res.setStatusCode(200);
        }else {
            System.assert(false, 'unexpected endpoint ' + req.getEndpoint());
            return null;
        }
        return res;
        
    }
}