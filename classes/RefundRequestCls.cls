/******************************************************************************************
 *  Author   : Sravan Booragadda
 *  Company  : DIFC
 *  Date     : 27-Aug-2017
 *  Description : This is a Contoller for GS Refund Automation Page                  
 ----------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
 1.1    2-Oct-17         Shabbir Ahmed       Added the logic for PSA refund calculation check.
 1.2    24-Apr-19        Azfer Pervaiz       Added the logic for Bank Address character length.
 1.3    4-March-2020     Mudasir        Moved the query to the custom label.
 1.3    4-March-2020     Shoaib        Refund Automation Project.
 1.4    10-June-2021     Veera         checking PSA avilable at the time of submit as well 
*******************************************************************************************/

Public class RefundRequestCls {
    
    Public string strPageId{get;set;}   
    Public Service_Request__c objSR{get;set;}
    public string templateDescription{get;set;}
    private transient SR_Template__c srTemplate;
    private transient map<string,string> pageparameters;
    public boolean termsAndAgreement{get;set;}
    public string displaymode{get;set;}
    public List<Account> accountsList{get;set;}
    
    public User loggedInUser                                {get;set;}
    public list<SelectOption> RefundModeOptions             {get;set;} 
    public String selectedCompanyId                         {get;set;}
    public String selectedRocStatus                         {get;set;}  
    public string accountName                               {get;set;} 
    public string accountid                                 {get;set;}
    public string AlertMsg                                  {get;set;}           
   
    

    
    Public RefundRequestCls(ApexPages.StandardController controller){       
        objSR = new Service_Request__c();        
        if(apexpages.currentpage().getParameters().size()>0){
            pageparameters = new map<string,string>();
            pageparameters = apexpages.currentpage().getParameters();
            
        }
        setTemplate();
        if(pageparameters !=null && pageparameters.containsKey('id') && pageparameters.get('id') !=null)    
            setServiceRequestFields(pageparameters.get('id'));
        else{
             setServiceRequestFields(null);  
             
        }
        
        if(pageparameters !=null && pageparameters.containsKey('isDetail') && pageparameters.get('isDetail') =='true')
            displaymode = 'D';
        else if(pageparameters !=null && pageparameters.containsKey('id') && !pageparameters.containsKey('isDetail'))
            displaymode = 'D';  
        else
            displaymode = 'E';
                     
        list<User> lstUser = [Select Id, Contact.Account.Registration_License_No__c,Community_User_Role__c , Contact.Account.BP_No__c  from User where Id=:Userinfo.getUserId() ];
        system.debug('lstUser ' + lstUser);
        if(lstUser!=null && lstUser.size()>0){
            loggedInUser =  lstUser[0];
        }        
    } 
    
    public pagereference saveRequest(){     
        
       List<RecordType> recordTypeNames;
       
       if(objSR.id ==null){
            recordTypeNames = [select id from recordType where DeveloperName='Refund_Request'];
           if(recordTypeNames !=null && recordTypeNames.size()>0)
            objSR.RecordTypeID = recordTypeNames[0].id;
       }
      
       //system.debug('AccountID***'+accountid);
       if(accountid !=null && accountid.length()>=15)
            objSR.Transfer_to_account__c = accountid;
        
       //system.debug('AccountID***'+accountid);
       if(objSR.Type_of_Refund__c  =='PSA Transfer'   || objSR.Type_of_Refund__c  =='Portal Balance Transfer'){
           objSR.Mode_of_Refund__c = '';
        }
           
                 
        AlertMsg = RefundRequestCls.checkFieldLevelValidations(objSR);
        if(AlertMsg =='Success'){
            
            string validationResult = RefundRequestCls.validateRefundSR(objSR);
            AlertMsg = validationResult;
            if(validationResult !=null && validationResult=='Success'){
                SavePoint sp =database.setSavePoint();
                try{
                    upsert objSR;
                    pagereference pg = new PageReference('/'+objSR.id);
                    return pg;          
                }catch(exception e){
                    system.debug(e.getMessage());
                    database.rollBack(sp);
                    AlertMsg = 'Error - Please correct the data on the form';
                  //  apexpages.addMessage(new apexpages.message(Apexpages.severity.Error,e.getMessage()));
                    return Null;
                   
                } 
            } 
            else{ apexpages.addMessage(new apexpages.message(Apexpages.severity.Error,validationResult)); } 
        }      
        return null;
    }
    
    public pagereference editRequest(){
        pagereference pg;
        if(objSR.Submitted_Date__c ==null){
             setServiceRequestFields(objSR.id);
             pg = new pagereference('/apex/GSRefundRequest');
             pg.getParameters().put('id',objSR.id);
             pg.getParameters().put('isDetail','false');
             pg.setRedirect(true);
             return pg;
        }else{
            apexpages.addMessage(new apexpages.message(Apexpages.severity.Error,'Service request once submitted cannot be edited'));
            return null;
        }
        
    }
    
    public  pagereference SubmitRefundRequest(){
        
        AlertMsg = '';
        if(AlertMsg == ''){
            Boolean isUploaded = true;
            for(SR_Doc__c objDoc : [select Id,Name,Is_Not_Required__c,Status__c from SR_Doc__c where Is_Not_Required__c = false AND Service_Request__c =: objSR.id]){
                if(objDoc.Status__c == 'Pending Upload' || objDoc.Status__c == 'Re-upload'){
                    isUploaded = false;
                 }
            }
                    
            if(isUploaded == false)
             AlertMsg = 'Please Upload the Required Docs before Submit.';
          //V1.4 --- start
            string validationResult = RefundRequestCls.validateRefundSR(objSR);
            if(validationResult !=null && validationResult!='Success')AlertMsg = validationResult;
           //V1.4 --- End  
        }   
        
        pagereference pg;
        string result='Success';
        if(objSR !=null && objSR.id !=null && AlertMsg ==''){
          //  result = RefundRequestCls.validateRefundSR(objSR);
            if(result =='Success'){
                pg = new pagereference('/apex/SRSubmissionPage?id='+objSR.id);
                return pg;
            }  
            apexpages.addMessage(new apexpages.message(Apexpages.severity.Error,result)); 
        }
        
        
        return null;
        
    }
    
    public pagereference cancelSR(){
        Pagereference objRef;
        try{
            if(displaymode == 'D'){ //Detail Page Button
                if(ObjSR.Submitted_Date__c != null){
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Request was already submitted.'));
                    return null;
                }else{
                    delete ObjSR;
                    if(UserInfo.getUserType() !='standard')
                        objRef = new Pagereference('/apex/home');
                    else
                        objRef = new Pagereference('/home/home.jsp');
                }
            }else if( displaymode != 'D'){ //Edit Page Button
                if(ObjSR.Id != null)
                    objRef = new Pagereference('/'+ObjSR.Id);
                else{
                    if(UserInfo.getUserType() !='standard')
                        objRef = new Pagereference('/apex/home');
                    else
                        objRef = new Pagereference('/home/home.jsp');
                }
            }
            if(objRef != null)
                objRef.setRedirect(true);
            return objRef;
        }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please contact support.'));
        }
        return null;    
    }
    
    Public static string validateRefundSR(service_Request__c srObj){
        
        map<string,decimal> portalBalanceDetails; 
        if(srObj.Type_of_Refund__c !=null && srObj.Refund_Amount__c !=null && srObj.Refund_Amount__c >0 ){ // && srObj.id==null
           portalBalanceDetails = new map<string,decimal>();
           portalBalanceDetails =  RefundRequestCls.checkPortalBalance(srObj.customer__c);
           if(srObj.Type_of_Refund__c == 'PSA Refund' || srObj.Type_of_Refund__c =='PSA Transfer'){
                if(portalBalanceDetails !=null && portalBalanceDetails.size()>0 && portalBalanceDetails.containsKey('psaavailable')){
                    Integer empcount = portalBalanceDetails.get('empCount').intValue();
                    if((srObj.Refund_Amount__c > portalBalanceDetails.get('psaavailable')) && (empcount < = Integer.valueOf(System.Label.EmpCount1)|| system.label.RefundEmpCheck != 'True' )){
                        return 'Refund amount cannot exceed PSA Available :'+portalBalanceDetails.get('psaavailable');  
                    }
                    else if((srObj.Refund_Amount__c > (portalBalanceDetails.get('psadeposit')-125000)) && empcount > Integer.valueOf(System.Label.EmpCount2) && system.label.RefundEmpCheck1 == 'True'){                        
                        return 'Refund amount cannot exceed PSA Available :'+(portalBalanceDetails.get('psadeposit')- 125000);                        
                    }
                }
               }else if(srObj.Type_of_Refund__c =='Portal Balance Refund' || srObj.Type_of_Refund__c=='Portal Balance Transfer'){
                if(portalBalanceDetails !=null && portalBalanceDetails.size()>0 && portalBalanceDetails.containsKey('portalbalance')){
                    if(srObj.Refund_Amount__c > portalBalanceDetails.get('portalbalance')){
                        return 'Refund amount cannot exceed available portal balance :'+portalBalanceDetails.get('portalbalance');  
                    }
                }
                
           }
        }
         
         if(srObj.Type_of_Refund__c ==null)
            return  'Please select Type of Refund';
         if((srObj.Mode_of_Refund__c ==null || srObj.Mode_of_Refund__c=='') && srObj.Type_of_Refund__c=='PSA Refund' && srObj.Type_of_Refund__c=='Portal Balance Refund')
            return 'Please select Mode of Refund';  
         
         if(srObj.Type_of_Refund__c !=null && srObj.Mode_of_Refund__c !=null){
            if(srObj.Mode_of_Refund__c=='Chequeue in favour of another individual' && (srObj.First_Name__c == null || srObj.Last_Name__c == null))
                return 'Please fill first Name & Last Name';                
            if(srObj.Mode_of_Refund__c=='Chequeue in favour of another Entity' && srObj.SAP_SGUID__c == null)
                return 'Please fill Account Name';
        }  
        
        return 'Success';    
    }
    
    
    Public static map<string,decimal> checkPortalBalance(id accountID){     
        id CustomerId;
        boolean hasEstablishmentCard;
        string AccountBpNo;
        Map<string,decimal> potalBalanceDetailMap = new map<string,decimal>();
        
        for(Account objAccount : [select Id,Index_Card_Status__c,BP_No__c from Account where id=:accountID and BP_No__c !=null]){                        
            CustomerId = objAccount.Id;
            hasEstablishmentCard = String.isNotBlank(objAccount.Index_Card_Status__c) && !objAccount.Index_Card_Status__c.equals('Not Available') && !objAccount.Index_Card_Status__c.equals('Cancelled');        
            AccountBpNo = objAccount.BP_No__c;
        }
    
        if(AccountBpNo != null && AccountBpNo != ''){ 
            string decryptedSAPPassWrd = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456' ; // V1.6          
            list<AccountBalenseService.ZSF_S_ACC_BALANCE> items = new list<AccountBalenseService.ZSF_S_ACC_BALANCE>();
            AccountBalenseService.ZSF_S_ACC_BALANCE item;
            item = new AccountBalenseService.ZSF_S_ACC_BALANCE();
            item.KUNNR = AccountBpNo;
            //item.BUKRS = '5000';
            item.BUKRS = System.Label.CompanyCode5000;
            items.add(item);
            AccountBalenseService.ZSF_TT_ACC_BALANCE objTTActBal = new AccountBalenseService.ZSF_TT_ACC_BALANCE();
            objTTActBal.item = items;
            AccountBalenseService.account objActBal = new AccountBalenseService.account();
            objActBal.timeout_x = 100000;
            objActBal.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;           
            objActBal.inputHttpHeaders_x= new Map<String, String>();
            Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+decryptedSAPPassWrd);
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            objActBal.inputHttpHeaders_x.put('Authorization', authorizationHeader);
            AccountBalenseService.Z_SF_ACCOUNT_BALANCEResponse_element objAccountBalResponse= objActBal.Z_SF_ACCOUNT_BALANCE(objTTActBal);
            map<string,decimal[]> mapAccountBalInfo = new map<string,decimal[]>();
            system.debug('objAccountBalResponse.EX_SF_RS_ACC $$$ '+objAccountBalResponse.EX_SF_RS_ACC);
            system.debug('objAccountBalResponse.EX_SF_RS_ACC.item $$$ '+objAccountBalResponse.EX_SF_RS_ACC.item);
            
            if(objAccountBalResponse.EX_SF_RS_ACC != null && objAccountBalResponse.EX_SF_RS_ACC.item != null){
                           
                for(AccountBalenseService.ZSF_S_ACC_BAL objAccountBalInfo : objAccountBalResponse.EX_SF_RS_ACC.item){
                    system.debug('objAccountBalInfo $$$ '+objAccountBalInfo);
                    if(objAccountBalInfo.KUNNR != null){
                        decimal[] dActBals;
                        dActBals = mapAccountBalInfo.containsKey(objAccountBalInfo.KUNNR) ? mapAccountBalInfo.get(objAccountBalInfo.KUNNR) : new decimal[]{0,0,0,0}; // V1.6 - Claude - Added another value to collection
                        if(objAccountBalInfo.UMSKZ == 'D')
                            dActBals[0] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                        if(objAccountBalInfo.UMSKZ == 'H')
                            dActBals[1] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                        if(objAccountBalInfo.UMSKZ == '9')
                            dActBals[2] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;    
                        if(objAccountBalInfo.UMSKZ == 'O')    //Fit out
                            dActBals[3] = (objAccountBalInfo.WRBTR != null && objAccountBalInfo.WRBTR != '')?decimal.valueOf(objAccountBalInfo.WRBTR):0;
                        mapAccountBalInfo.put(objAccountBalInfo.KUNNR,dActBals);
                    }
                                
                }//end of for                
                Decimal PortalBalance =0; 
                
                if(!mapAccountBalInfo.isEmpty() && mapAccountBalInfo.containsKey(AccountBpNo) && mapAccountBalInfo.get(AccountBpNo).size()==4){                        
                    //potalBalanceDetailMap.put('portalbalance',mapAccountBalInfo.get(AccountBpNo)[0]);
                    potalBalanceDetailMap.put('psaactual',mapAccountBalInfo.get(AccountBpNo)[1]);
                    potalBalanceDetailMap.put('psaguarantee',mapAccountBalInfo.get(AccountBpNo)[2]);                
                    PortalBalance += mapAccountBalInfo.get(AccountBpNo)[0];
                    decimal[] dPSA = RefundRequestCls.PSACalculation(CustomerId,(mapAccountBalInfo.get(AccountBpNo)[1] + mapAccountBalInfo.get(AccountBpNo)[2]),hasEstablishmentCard);
                    if(!hasEstablishmentCard && dPSA[1] < 0) dPSA[1] = 0;

                        potalBalanceDetailMap.put('psadeposit',dPSA[0]);                                                        
                        potalBalanceDetailMap.put('psaavailable',dPSA[1]); 
                        potalBalanceDetailMap.put('empCount',dPSA[2]);                          
                     
                    String priceItemQuery = System.Label.Refund_Price_Item_Validation_Query;

                    System.debug('The Query: '+ priceItemQuery);

                    for(SR_Price_Item__c objSRPriceItem : Database.query(priceItemQuery)){
                        PortalBalance -= objSRPriceItem.Total_Service_Amount__c; // added as part of vat
                    }

                    for(Receipt__c objReceipt : [select Amount__c from Receipt__c where Customer__r.BP_No__c=:AccountBpNo and Amount__c!=null and Payment_Status__c='Success' and Pushed_to_SAP__c=false]){
                        PortalBalance += objReceipt.Amount__c;
                    }
                    
                    PortalBalance -= RefundrequestUtils.getRefundAmount(CustomerId);
                    
                    potalBalanceDetailMap.put('portalbalance',PortalBalance); 
                                   
                    
                }else{ apexpages.addMessage(new apexpages.message(Apexpages.severity.Error,'Unable to get portal balance.Please contact Support')); }                       
            }     
        }else{ apexpages.addMessage(new apexpages.message(Apexpages.severity.Error,'Unable to get portal balance.Please contact Support')); }
        
        return potalBalanceDetailMap;
 }
 
 
    /**
     * Sets the SR Template for the form
     */ 
    private void setTemplate(){            
        srTemplate =  [select SR_Description__c,Terms_Conditions__c from SR_Template__c where SR_RecordType_API_Name__c=:'Refund_Request'];  
        templateDescription = srTemplate.SR_Description__c;
     }
    
     /**
     * Sets Service Request details
     */ 
    private void setServiceRequestFields(string srID){    
        if(srID !=null){
            for(Service_Request__c srTemp :[select id,Name,Customer__c,Confirm_Change_of_Trading_Name__c,Establishment_Card_No__c,License_Number__c,Email__c,Send_SMS_To_Mobile__c,Type_of_Refund__c,Mode_of_Refund__c,Refund_Amount__c,Last_Name__c,First_Name__c,SAP_SGUID__c,Company_Name__c,Bank_Name__c,Bank_Address__c,Car_Registration_number__c,SR_template__r.SR_Description__c,
                                                   Transfer_to_account__r.Name,Transfer_to_account__c,Transfer_to_account__r.Active_License__r.Name,Submitted_Date__c,Record_type_name__c,External_Status_Name__c,
                                                   SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,Post_Code__c,I_agree__c,Terms_Conditions__c,customer__r.Skip_PSA_Validation_for_Refund__c,Cancellation_With_Keys__c from service_Request__c where id =:srID]){                                                  
                objSR = srTemp;     
                if(objSR.Type_of_Refund__c =='PSA Transfer' || objSR.Type_of_Refund__c =='Portal Balance Transfer'){
                    accountName = objSR.Transfer_to_account__r.Name;
                    accountId = objSR.Transfer_to_account__c;
                }          
            }
            
            
        }else{
            for(User usr : [select id,Phone,Email,accountid,Community_User_Role__c from User where id =: userInfo.getUserId()]){
                objSR.customer__c = usr.accountid;                
                objSR.Email__c = usr.Email;
                objSR.Send_SMS_To_Mobile__c = usr.Phone;
                objSR.Car_Registration_Number__c = usr.Community_User_Role__c;        
            }
            
            for(Account acc :[select id,Active_License__c,Active_License__r.Name,Index_Card__r.Name,Skip_PSA_Validation_for_Refund__c from account where id =:objSR.customer__c]){
                objSR.Establishment_Card_No__c = acc.Index_Card__r.Name;
                objSR.License_Number__c= acc.Active_License__r.Name;
                objSR.Cancellation_With_Keys__c = acc.Skip_PSA_Validation_for_Refund__c; // v1.1
            }
            if(srTemplate !=null && srTemplate.Terms_Conditions__c!=null)
                objSR.Terms_Conditions__c = srTemplate.Terms_Conditions__c;
        }
        
        populateRefundOptions();
    }  
    
    
    public list<SelectOption> getRefundTypes(){
        
        list<SelectOption> options = new List<SelectOption>();
        options.add(new Selectoption('','--None--'));   
        options.add(new SelectOption('Portal Balance Refund','Portal Balance Refund'));
        options.add(new SelectOption('Portal Balance Transfer','Portal Balance Transfer'));
        
        if (loggedInUser != null && string.isNotBlank(loggedInUser.Community_User_Role__c)  && loggedInUser.Community_User_Role__c.contains('Employee Services')){
            options.add(new SelectOption('PSA Refund','PSA Refund'));
            options.add(new SelectOption('PSA Transfer','PSA Transfer'));
        }
        
        //populateRefundOptions();
        return options;     
    }
    
    public void populateRefundOptions(){
        
       RefundModeOptions = new list<Selectoption>();
       RefundModeOptions.add(new Selectoption('--None--','--None--'));
       //objSR.Mode_of_Refund__c = null;
        
       Schema.DescribeFieldResult fieldResult = Service_Request__c.Mode_of_Refund__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       
       if(objSR.Type_of_Refund__c != 'Portal Balance Transfer' && objSR.Type_of_Refund__c != 'PSA Transfer'){

           for( Schema.PicklistEntry f : ple) {
            
           if(objSR.Type_of_Refund__c == 'Portal Balance Refund' && f.getLabel() != 'Credit to the portal Account') 
                RefundModeOptions.add(new SelectOption(f.getLabel(), f.getValue()));
             
          
           
            if(objSR.Type_of_Refund__c =='PSA Refund')
                    RefundModeOptions.add(new SelectOption(f.getLabel(), f.getValue()));
            }           
        
       }    
                
    } 
    
    public void SearchAccounts(){
        accountsList = new List<Account>();
        
        if(accountName !=null && accountName !=''){ 
            string accName = accountName+'%';
            for(Account acc : [select id,Name,Active_License__r.Name from Account where ROC_Status__c ='Active' and  (name like : accName or Active_License__r.Name like : accName) and id !=: ObjSR.Customer__c  limit 10]){
                accountsList.add(acc);  
            }
        }
    }
    
    
    
    public static string checkFieldLevelValidations(service_Request__c srObj){
        
        if(string.isBlank(srObj.Type_of_refund__c))
            return 'Please provide Type of Refund';
        if(srObj.Type_of_refund__c !=null && (srObj.Type_of_Refund__c =='PSA Refund' || srObj.Type_of_Refund__c =='Portal Balance Refund') && (string.isBlank(srObj.Mode_of_Refund__c)|| srObj.Mode_of_Refund__c.contains('None'))){    
            return 'Please provide Mode of Refund';
        }

     /*   if(srObj.Mode_of_Refund__c !=null && (srObj.Mode_of_Refund__c =='Refund in Cash'  && srObj.Refund_Amount__c > 1000.00)){    
            return 'Cash Refund is applicable only if the refund amount is <=AED 1000';
        }

        if(srObj.Mode_of_Refund__c !=null && (srObj.Mode_of_Refund__c !='Refund in Cash' &&  srObj.Refund_Amount__c <= 100.00)){    
            return 'Only Cash Refund is applicable if the refund amount is <=AED100';
        }*/

        if(srObj.Type_of_refund__c !=null && (srObj.Type_of_Refund__c =='PSA Refund' || srObj.Type_of_Refund__c =='Portal Balance Refund') && !string.Isblank(srObj.Mode_of_Refund__c)){
            
            if(srObj.Mode_of_refund__c.contains('Wire Transfer')){
                if(string.IsBlank(srObj.SAP_SGUID__c) || string.Isblank(srObj.SAP_OSGUID__c) || string.isBlank(srObj.Bank_Name__c) || string.Isblank(srObj.Bank_Address__c) || string.isBlank(srObj.SAP_GSTIM__c))
                    return 'Please fill all the required fields';
            }
            
            // 1.2 - Start
            if( !string.isBlank(srObj.Bank_Address__c) && srObj.Bank_Address__c.length() > 80  ){ return 'Bank Address should be less or equal to 80 characters';}
            // 1.2 - End
                        
            if(srObj.Mode_of_refund__c == 'Cheque in favour of another Individual' && (string.isBlank(srObj.first_Name__c) || string.isBlank(srObj.Last_Name__c))){ return 'Please fill First Name and Last Name';}
            
            if(srObj.Mode_of_refund__c == 'Cheque in favour of another Entity' && string.isBlank(srObj.SAP_SGUID__c)){ return 'Please Provide Account Name';}
        }
        
        if((srObj.Type_of_Refund__c =='PSA Transfer' || srObj.Type_of_Refund__c =='Portal Balance Transfer') &&  string.isBlank(srObj.Transfer_to_account__c)){ 
                return 'Please select Account to transfer';
        }

        if(srObj.Confirm_Change_of_Trading_Name__c == 'Yes' && String.IsBlank(srObj.IBAN_Number__c)){
            return 'Please fill IBAN Number';
        }  
        
        if(srObj.Refund_Amount__c ==null ) return 'Please provide refund amount';
            
        if(srObj.Refund_Amount__c !=null && srObj.Refund_Amount__c ==0) return 'Refund Amount Should be greater than 0';    
            
        if(!srObj.I_agree__c){return 'Please agree terms and conditions';}
        
        decimal forpsaValidation =0;
        decimal newpsaValidation =0;
        
        decimal psaAvilableCheck = 0;
        
        map<string,decimal> portalBalanceDetails = RefundRequestCls.checkPortalBalance(srObj.customer__c);
        
        psaAvilableCheck  = portalBalanceDetails.get('psaavailable');
        
        User  objUser =  [SELECT Id,
                                     Name,ContactId,AccountId,Phone,Email,Contact.Account.Name,Contact.Account.Active_License__c
                                FROM User
                                WHERE Id= :Userinfo.getUserId()]; 
        
        String isFintechCompany = [SELECT Id,FinTech_Classification__c FROM Account WHERE Id =:objUser.AccountId].size() > 0 ?
                                                 [SELECT Id,FinTech_Classification__c FROM Account WHERE Id =:objUser.AccountId].FinTech_Classification__c : '';
       
        Boolean isFintechUser  = false;
        
        if(String.isNotBlank(isFintechCompany) && isFintechCompany =='FinTech'){isFintechUser = true;}
        
        if((srObj.type_of_Refund__c == 'PSA Refund' || srObj.type_of_Refund__c == 'PSA Transfer') && srObj.Refund_Amount__c !=null && srObj.Refund_Amount__c !=0 && !srObj.Cancellation_With_Keys__c){ //v1.1
           
          if(srObj.Refund_Amount__c != psaAvilableCheck){ 
           
           forpsaValidation = srObj.Refund_Amount__c.divide(2500,3);
           newpsaValidation = srObj.Refund_Amount__c.divide(1000,3);
           
           if(forpsaValidation.scale() > 0 && !isFintechUser){
                
                string decimalsplit = string.valueOf(forpsaValidation).split('\\.')[1];
                 if(decimalsplit !='000') return 'Amount should be multiples of 2500';
               }
              
              else if(newpsaValidation.scale() > 0 && isFintechUser){
                 string decimalsplit = string.valueOf(newpsaValidation).split('\\.')[1];
                  if(decimalsplit !='000') return 'Amount should be multiples of 1000';
              }
              
          }
        }
         return 'Success';
        
    }
    
    
    public static decimal[] PSACalculation(string CustomerId,decimal PSADeposit, Boolean hasEstablishmentCard){
      
        list<string> lstPSARecTypes = new list<string>{'DIFC_Sponsorship_Visa_New','Employment_Visa_from_DIFC_to_DIFC','Employment_Visa_Govt_to_DIFC','Visa_from_Individual_sponsor_to_DIFC'};
        
        map<Id,Service_Request__c> mapOpenSRs = new map<Id,Service_Request__c>([select Id,External_Status_Name__c from Service_Request__c where (NOT Occupation_GS__r.Name like '%STUDENT%') AND Customer__c=:CustomerId AND Record_Type_Name__c IN : lstPSARecTypes AND isClosedStatus__c != true AND Is_Rejected__c != true AND External_Status_Name__c != 'Cancelled' AND External_Status_Name__c != 'Rejected' AND External_Status_Name__c != 'Draft']);
        map<Id,Contact> mapActiveCons = new map<Id,Contact>([select Id from Contact where (NOT Job_Title__r.Name like '%STUDENT%') AND Id IN (select Object_Contact__c from Relationship__c where Subject_Account__c =:CustomerId AND Active__c = true AND Relationship_Type__c='Has DIFC Sponsored Employee')]);
        
        Integer iEmpCount = (mapActiveCons != null && !mapActiveCons.isEmpty()) ? mapActiveCons.size() : 0 ;
        
        if(!mapOpenSRs.isEmpty()) iEmpCount += mapOpenSRs.size();
        
        system.debug('PSADeposit is : '+PSADeposit);
        
        for(SR_Price_Item__c objSRItem : [select Id,Price__c from SR_Price_Item__c
                                     where Count_in_PSA__c = TRUE AND 
                                     ServiceRequest__r.Customer__c =: CustomerId AND (Status__c='Blocked' OR Status__c='Consumed') AND Pricing_Line__r.Product__r.Name = 'PSA']){ 
            PSADeposit += objSRItem.Price__c;
        }
        
        system.debug('PSADeposit is : '+PSADeposit);
        
        // v1.8 - Claude - Start
        if(hasEstablishmentCard) iEmpCount++;
        // v1.8 - Claude - End
        
       
        
        PSADeposit -=RefundRequestUtils.getPSAAmount(CustomerId); // V1.11
        
        String isFintechCompany = [SELECT Id,
                                          FinTech_Classification__c 
                                          FROM Account WHERE Id =:CustomerId].size() > 0 ?
                                                 [SELECT Id,FinTech_Classification__c FROM Account WHERE Id =:CustomerId].FinTech_Classification__c : '';
        Boolean isFintechUser   = false;
        
        if(String.isNotBlank(isFintechCompany) && isFintechCompany =='FinTech'){isFintechUser = true;}
        
        decimal dAvaPSA = isFintechUser ? (PSADeposit-(iEmpCount*1000)) :(PSADeposit-(iEmpCount*2500)); 
            
        Decimal EmpCount = Decimal.valueOf(iEmpCount);
           
        return new decimal[]{PSADeposit,dAvaPSA,EmpCount};//dAvaPSA;
    }
    
}