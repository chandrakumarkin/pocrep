/*************************************************************************************************
 *  Name        : CLS_PortalUtility
 *  Author      : Bilal Nazir
 *  Company     : NSI JLT
 *  Date        : 2015-08-18     
 *  Purpose     : This utility class is used for DIFC Portal for utility functions.       
***************************************************************************************************/

global class CLS_PortalUtility {
	
	//Load current user steps
	webservice static List<sobject> getSRSteps(string srId){
		string userId = UserInfo.getUserId();
		
		List<sobject> steps = [ SELECT id, Name, Step_No__c, Step_Name__c, Step_Status__c, Owner.Name, CreatedDate, LastModifiedDate 
								from Step__c 
								Where SR__c=:srId and (OwnerId=:userId OR Owner.Name = 'Client Entry User' OR Owner.Name = 'Client Entry Approver')
							];	
		
		return steps;
	}
	
	webservice static List<sObject> getSRInfo(string srId){
		 return [SELECT Portal_Service_Request_Name__c, External_Status_Name__c from Service_Request__c WHERE id=:srId];
	}
	
	webservice static List<sobject> getSRTemplateInfo(string recordType){
		return [SELECT Name, SR_Description__c FROM SR_Template__c WHERE SR_RecordType_API_Name__c=:recordType];
	}
}