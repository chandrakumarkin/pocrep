/*

Version  Name         ModifiedDate           Comments
v1.1     Veera        17-Jun-2021            Update the SRDco status to uploaded.

*/

public class WebServiceGdrfaFetchTrxReceiptBatch implements
Database.Batchable<sObject>, Database.AllowsCallouts{
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        
        return Database.getQueryLocator('SELECT Id, DNRD_Receipt_No__c,Step_Name__c, SR__c '+
                                        'FROM Step__c '+
                                        'WHERE Step_Name__c IN (\'Entry Permit is Typed\' '+
                                        ',\'VISA Stamping form is typed\' '+
                                        ', \'Renewal form is typed\' '+
                                        ', \'Cancellation form typed\' '+
                                        ', \'Online Entry permit is typed\') '+
                                        'AND Status__r.code__c = \'CLOSED\' '+
                                        'AND CreatedDate = Today '+
                                        'AND Transaction_Number__c != null '+
                                        'AND Payment_Date__c != null '+
                                        'AND DNRD_Receipt_No__c != null '+
                                        'AND SR__r.Record_Type_Name__c IN (\'DIFC_Sponsorship_Visa_Renewal\' '+
                                        ', \'DIFC_Sponsorship_Visa_New\' '+
                                        ', \'DIFC_Sponsorship_Visa_Cancellation\') '+
                                        'AND Is_Transaction_Receipt__c = False ');
    }
    
    public void execute(Database.BatchableContext bc, List<Step__c> scope){
        // process each batch of records
        WebServGDRFAAuthFeePaymentClass.ResponseBasicAuth ObjResponseAuth = WebServGDRFAAuthFeePaymentClass.GDRFAAuth();
        map<String, SR_Doc__c> mapSrIdSrDoc = new map<String, SR_Doc__c>();
        List<Attachment> listAttch = new List<Attachment>();
        List<String> SrIdList = new List<String>();
        List<Step__c> stepList = new List<Step__c>();
        List<SR_Doc__c> SRDOCList = new List<SR_Doc__c>();
        for(Step__c step: Scope){
            SrIdList.add(step.SR__c);
        }
        
        for(SR_Doc__c srDoc : [SELECT id, Service_Request__c FROM SR_Doc__c WHERE Service_Request__c IN:(SrIdList) AND Document_Master__r.Code__c = 'Transaction_Receipt_Document']){
            mapSrIdSrDoc.put(srDoc.Service_Request__c, SrDoc);
        }
        //Get authrization code 
        string access_token= ObjResponseAuth.access_token;
        string token_type=ObjResponseAuth.token_type;
        //WebService to Push the Fetch Transaction Receipt
        for(Step__c stepObj: Scope){
            HttpRequest request = new HttpRequest();
            request.setEndPoint(System.label.GDRFA_Payment_Receipt_Download);
            JSONGenerator jsonAppStatusObj = JSON.createGenerator(true);
            jsonAppStatusObj.writeStartObject();
            if(stepObj.DNRD_Receipt_No__c != null){
                jsonAppStatusObj.writeStringField('applicationId', stepObj.DNRD_Receipt_No__c);
            }
            jsonAppStatusObj.writeEndObject();
            request.setBody(jsonAppStatusObj.getAsString());
            request.setTimeout(120000);
            request.setHeader('apikey', System.label.GDRFA_API_Key);
            request.setHeader('Content-Type', 'application/json');
            request.setMethod('POST');
            request.setHeader('Authorization',token_type+' '+access_token);
            HttpResponse response = new HTTP().send(request);
            System.debug('responseStringresponse:'+response.toString());
            System.debug('STATUS:'+response.getStatus());
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('strJSONresponseSubmitRenew:'+response.getBody());
            
            request.setTimeout(101000);
            if (response.getStatusCode() == 200)
            {
                WebServGDRFAAuthFeePaymentClass.ReceiptPayment PaymentReceipt = (WebServGDRFAAuthFeePaymentClass.ReceiptPayment) System.JSON.deserialize(response.getBody(), WebServGDRFAAuthFeePaymentClass.ReceiptPayment.class);
                if(mapSrIdSrDoc.get(stepObj.SR__c) != null){
                    Attachment attachment = new Attachment();
                    attachment.Name = 'Transaction Receipt.pdf';
                    attachment.ParentId = mapSrIdSrDoc.get(stepObj.SR__c).Id;    
                    attachment.Body = EncodingUtil.base64Decode(PaymentReceipt.ReportBinary);
                    attachment.ContentType = 'application/pdf';
                    listAttch.add(attachment);
                    stepObj.Is_Transaction_Receipt__c = True;
                    stepList.add(stepObj);
                    //v1.1 -- Start
                    SR_Doc__c Doc = new SR_Doc__c();
                    Doc.id = mapSrIdSrDoc.get(stepObj.SR__c).id;
                    Doc.Step__c = stepObj.id;
                    Doc.status__c = 'Uploaded';
                    SRDOCList.add(Doc);
                    //V1.1 -- End
                }
            }
        }
        if(listAttch.size() > 0){
            insert listAttch;
        }
        if(stepList.Size() > 0){
            Update stepList;
        }
        if(SRDOCList.size()>0){
            update SRDOCList;
            
        }
        
    }
    
    public void finish(Database.BatchableContext bc){
        
    }
}