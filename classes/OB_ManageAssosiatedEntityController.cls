/*****************************************************************************************************************
Added login from existing class: DIFCServicesController, DIFCServicesControllerCom
* Version       Date                Author              Remarks                                                 
* 1.0 		    21th March 2020      Merul              switch user profile
* 2.0           5th MAY 2020        Prateek             employee violation logic
* 3.0           14th Mar 2021        Abbas               #14049 Nationality blank issue
*****************************************************************************************************************/


public without sharing class OB_ManageAssosiatedEntityController {

    @AuraEnabled(cacheable=true)
    public static RespondWrap fetchEntityData()  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
    
    
    
        User loggedInUser = new User();
    
        for(User UserObj : [Select contactid,Contact.AccountId,Contact.Account.Name,contact.Role__c ,Community_User_Role__c,contact.contractor__c
         from User where Id = :UserInfo.getUserId() LIMIT 1]) {
            loggedInUser =  UserObj;
        }

        //respWrap.newEntityWrap = OB_AgentEntityUtils.getCreateEntityData(respWrap.loggedInUser.ContactId);

       /*  //get relatedsr
        string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New User Registration').getRecordTypeId();
        for(HexaBPM__Service_Request__c relSr : [SELECT id,HexaBPM__Contact__c from HexaBPM__Service_Request__c WHERE recordTypeId =:recordId AND HexaBPM__Contact__c  =: loggedInUser.contactid 
        AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__IsCancelled__c = false AND 
        HexaBPM__Is_Rejected__c = false LIMIT 1]){
            respWrap.relEntitySr = relSr;
        } */
   
        OB_AgentEntityUtils.RespondWrap utilsWrapper = new OB_AgentEntityUtils.RespondWrap();
            utilsWrapper = OB_AgentEntityUtils.getRelatedAccounts(loggedInUser.ContactID);
        

            respWrap.relatedAccounts = utilsWrapper.relatedAccounts;

            for(account acc : respWrap.relatedAccounts){
                respWrap.relatedAccountIdsSet.add(acc.id);
            }

        //get pending action Item 

        //v2
        respWrap.emplymentViolationMap = DIFCApexCodeUtility.getCompanyEmployeeViolationDetails(utilsWrapper.relatedAccountIdsSet);
        //respWrap.pendingActionWrapper = getPendingActions(respWrap.relatedAccounts, loggedInUser);
        respWrap.loggedInUser = loggedInUser;
    
        return respWrap;
    }

    
    
    @AuraEnabled(cacheable=true)
    public static pendingActionWrapper getPendingActions(String requestWrapParam)  {
    
        

        pendingActionWrapper respWrap = new pendingActionWrapper();
        RequestWrap reqWrap = new RequestWrap();

		//deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        

        user LoggedInUser = reqWrap.LoggedInUser;
        list<string> relAccIdSet = reqWrap.relatedAccountIdsSet;

        /* set<string> relAccIdSet = new set<string>();
        if(relatedAccount != null){
            for(Account acc : relatedAccount){
            relAccIdSet.add(acc.Id);
        }
        } */

        respWrap.debugger = reqWrap.LoggedInUser;
        
        String CommunityUserRoles = '';
        if(LoggedInUser.contact.Role__c !=null && LoggedInUser.contact.Role__c!='')
        {
            CommunityUserRoles = LoggedInUser.contact.Role__c;
        }
        else if(LoggedInUser.Community_User_Role__c!=null && LoggedInUser.Community_User_Role__c!='')
        {
            CommunityUserRoles = LoggedInUser.Community_User_Role__c;
        }
        
        //respWrap.debugger = relAccIdSet;
       /*  return respWrap; */
        

       respWrap.communityRole = CommunityUserRoles;

         if(relAccIdSet.size() > 0 ){  

            for(HexaBPM__Step__c stepObj : [select id ,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR_Step__r.HexaBPM__Step_Template_Code__c from HexaBPM__Step__c WHERE HexaBPM__SR__r.HexaBPM__Customer__c IN : relAccIdSet AND HexaBPM__Status_Type__c != 'END' AND Is_Client_Step__c = true]){
                respWrap.actionItemList.add(stepObj);
            }

            if(String.isNotBlank(CommunityUserRoles) && CommunityUserRoles.contains('Fit-Out Services')){

                string statusType = 'END';
                string statusType2 = 'Draft';
                string ownerName = 'Contractorr';
                string ownerName2 = 'Client Entry User';
                string recTypeCheck = 'DM_SR';
                string LoggedInUserIdContractor = LoggedInUser.contact.contractor__c;
                
                string strQuery = 'select id,SR_Group__c,SR__r.Customer__c,SR__r.External_Status_Name__c,Step_Template__r.Code__c from Step__c WHERE SR__r.Customer__c IN : relAccIdSet '+ 
                                  'AND SR__r.contractor__c =: LoggedInUserIdContractor AND Owner__c=:ownerName AND Status_Type__c!=:statusType '+
                                    'AND SR__r.IsCancelled__c = false AND SR__r.Is_Rejected__c = false AND SR__r.isClosedStatus__c = false '+ 
                                    'AND SR__r.External_Status_Name__c !=:statusType2  AND SR__r.Record_Type_Name__c  !=:recTypeCheck AND  SR__r.Pre_GoLive__c = false';
                for(Step__c stepObj :database.query(strQuery)){
                    respWrap.stepList.add(stepObj); 
                }

                /* for(Step__c stepObj : [select id,SR_Group__c,SR__r.Customer__c,SR__r.External_Status_Name__c,Step_Template__r.Code__c from Step__c WHERE SR__r.Customer__c IN : relAccIdSet 
                AND SR__r.contractor__c =: LoggedInUser.contact.contractor__c AND Owner__c='Contractor' AND Status_Type__c!= 'END' 
                AND SR__r.IsCancelled__c = false AND SR__r.Is_Rejected__c = false AND SR__r.isClosedStatus__c = false
                AND SR__r.External_Status_Name__c != 'Draft'  AND SR__r.Record_Type_Name__c  != 'DM_SR' AND  SR__r.Pre_GoLive__c = false ]){

                respWrap.stepList.add(stepObj); 
                }  */
               /*  if((CommunityUserRoles.contains('Company Services') && (stepObj.SR_Group__c == 'Fit-Out & Events' || stepObj.SR_Group__c == 'ROC' || stepObj.SR_Group__c == 'RORP' || stepObj.SR_Group__c == 'BC'))
                    || (CommunityUserRoles.contains('Company Services') == false && CommunityUserRoles.contains('Property Services') && stepObj.SR_Group__c == 'RORP')
                    || (stepObj.SR_Group__c == 'GS' && CommunityUserRoles.contains('Employee Services')  && stepObj.SR__r.External_Status_Name__c !='Cancelled')
                    || (stepObj.SR_Group__c == 'IT' && CommunityUserRoles.contains('IT Services'))
                    || (stepObj.SR_Group__c == 'Listing' && CommunityUserRoles.contains('Listing Services'))
                    || (stepObj.SR_Group__c == 'Fit-Out & Events' && (CommunityUserRoles.contains('Fit-Out Services') || CommunityUserRoles.contains('Company Services')))
                    || (stepObj.SR_Group__c == 'Fit-Out & Events' && CommunityUserRoles.contains('Event Services'))
                    || (stepObj.SR_Group__c == 'Other')
                    || (stepObj.SR_Group__c =='RORP' && (CommunityUserRoles.contains('Mortgage Registration') || CommunityUserRoles.contains('Discharge of Mortgage') || CommunityUserRoles.contains('Variation of Mortgage')))
                
                ){
                    respWrap.stepList.add(stepObj); 
                } */

               
              
            }else{

                string statusType = 'END';
                string statusType2 = 'Draft';
                string ownerName = 'Client Entry User';
                string ownerName2 = 'Client Entry User';
                string recTypeCheck = 'DM_SR';
                string LoggedInUserId = LoggedInUser.Id;
                
                string strQuery = 'select id,SR_Group__c,SR__r.External_Status_Name__c,SR__r.Customer__c,SR__r.Service_Type__c,Step_Template__r.Code__c from Step__c WHERE SR__r.Customer__c IN : relAccIdSet  AND Status_Type__c!=: statusType '+ 
                                  'AND SR__r.IsCancelled__c = false AND SR__r.Is_Rejected__c = false AND SR__r.isClosedStatus__c = false '+
                                    'AND (OwnerId= :LoggedInUserId  OR Owner.Name=:ownerName OR Owner.Name=:ownerName2 OR Assigned_to_client__c =true) '+ 
                                    'AND SR__r.External_Status_Name__c !=:statusType2 AND SR__r.Record_Type_Name__c  !=:recTypeCheck  AND  SR__r.Pre_GoLive__c = false';
                for(Step__c stepObj :database.query(strQuery)){
                    respWrap.stepList.add(stepObj); 
                }

                /* for(Step__c stepObj : [select id,SR_Group__c,SR__r.External_Status_Name__c,SR__r.Customer__c,SR__r.Service_Type__c,Step_Template__r.Code__c from Step__c WHERE SR__r.Customer__c IN : relAccIdSet  AND Status_Type__c!= 'END' 
            AND SR__r.IsCancelled__c = false AND SR__r.Is_Rejected__c = false AND SR__r.isClosedStatus__c = false
            AND (OwnerId= : LoggedInUser.Id OR Owner.Name='Client Entry User' OR Owner.Name='Client Entry Approver' OR Assigned_to_client__c =true) 
            AND SR__r.External_Status_Name__c != 'Draft' AND SR__r.Record_Type_Name__c  != 'DM_SR'  AND  SR__r.Pre_GoLive__c = false ]){

                respWrap.stepList.add(stepObj); 

            }  */
                /* if((CommunityUserRoles.contains('Company Services') && (stepObj.SR_Group__c == 'Fit-Out & Events' || stepObj.SR_Group__c == 'ROC' || stepObj.SR_Group__c == 'RORP' || stepObj.SR_Group__c == 'BC'))
                    || (CommunityUserRoles.contains('Company Services') == false && CommunityUserRoles.contains('Property Services') && stepObj.SR_Group__c == 'RORP')
                    || (stepObj.SR_Group__c == 'GS' && CommunityUserRoles.contains('Employee Services')  && stepObj.SR__r.External_Status_Name__c !='Cancelled')
                    || (stepObj.SR_Group__c == 'IT' && CommunityUserRoles.contains('IT Services'))
                    || (stepObj.SR_Group__c == 'Listing' && CommunityUserRoles.contains('Listing Services'))
                    || (stepObj.SR_Group__c == 'Fit-Out & Events' && (CommunityUserRoles.contains('Fit-Out Services') || CommunityUserRoles.contains('Company Services')))
                    || (stepObj.SR_Group__c == 'Fit-Out & Events' && CommunityUserRoles.contains('Event Services'))
                    || (stepObj.SR_Group__c == 'Other')
                    || (stepObj.SR_Group__c =='RORP' && (CommunityUserRoles.contains('Mortgage Registration') || CommunityUserRoles.contains('Discharge of Mortgage') || CommunityUserRoles.contains('Variation of Mortgage')))
                
                ){
                    respWrap.stepList.add(stepObj); 
                } */

                
            

            }
                    
            return respWrap; 
        }else{
            return respWrap;
        }
        
    }

    @AuraEnabled
    public static RespondWrap setContactMainAccount(String requestWrapParam)  
    {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
    
    
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        system.debug(reqWrap);
    
        try {
    
            // 1.0 		    21th March 2020      Merul              switch user profile
            changeProfileBaseOnRelAcc(requestWrapParam);

            contact conToUpdate = new Contact(id=reqWrap.contactId);
            conToUpdate.AccountId = reqWrap.mainAccIdToSet;
            update conToUpdate;
           
           
    
        }
        catch(DMLException e) 
        {
    
            string DMLError = e.getdmlMessage(0) + '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
            respWrap.errorMessage = DMLError;
        }
        return respWrap;
    }

    // 1.0 		    21th March 2020      Merul              switch user profile
    //@future(callout=true)
    public static void changeProfileBaseOnRelAcc(String requestWrapParam) 
    {
          
          //try 
          //{
            
                //declaration of wrapper
                RequestWrap reqWrap = new RequestWrap();
                RespondWrap respWrap =  new RespondWrap();
                //deseriliaze.
                reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

                        
                String mainAccIdToSet = reqWrap.mainAccIdToSet;
                String contactId = reqWrap.contactId;
                String srAccess;
                String srRoles;
            
                User loggedInUser = new User();
                Map<String,String> mapProfileNameId = new Map<String,String>();   
                Map<String,String> mapProfileIdName = new Map<String,String>(); 

                for(User UserObj : [SELECT id,contactid,Contact.AccountId,Contact.Account.Name,ProfileId 
                                        FROM User 
                                    WHERE Id = :UserInfo.getUserId() 
                                    LIMIT 1
                                    ]
                    ) 
                {
                    loggedInUser =  UserObj;
                }

                Set<String> setProfile = new Set<String>{   'DIFC Customer Community Plus User Custom',
                                                            'DIFC Contractor Community Plus User Custom',
                                                            'DIFC Customer Community Plus User Custom - Read Only',
                                                            'DIFC Contractor Community Plus User Custom - Read Only'
                                                        };


                for(profile prof  :  [
                                        SELECT id,
                                            Name 
                                        FROM profile
                                        WHERE name =: setProfile
                                        
                                    ]
                    )
                {
                    mapProfileNameId.put(prof.Name,prof.Id);
                    mapProfileIdName.put(prof.Id,prof.Name);
                }                    
                system.debug('@@@@@@@@@@ mapProfileNameId '+mapProfileNameId); 
                system.debug('@@@@@@@@@@ mapProfileIdName '+mapProfileIdName);                  

                for( AccountContactRelation accContactRel : [
                                                                                SELECT Id,
                                                                                    AccountId,
                                                                                    ContactId,
                                                                                    Roles,
                                                                                    Access_Level__c 
                                                                            FROM AccountContactRelation
                                                                            WHERE ContactId =: contactId
                                                                                AND AccountId =: mainAccIdToSet
                                                            ]
                    )
                    {
                        srAccess = accContactRel.Access_Level__c;
                        srRoles = accContactRel.Roles;
                        system.debug('@@@@@@@@@@ srAccess '+srAccess);
                        system.debug('@@@@@@@@@@ srAccess '+srRoles);
                    }
                    
                    if( mapProfileIdName.size() > 0 )
                    {
                        User usr = new User(id=loggedInUser.Id);
                        String currentProfileName = mapProfileIdName.get(loggedInUser.profileId);
                        system.debug('@@@@@@@@@@ currentProfileName '+currentProfileName);
                    
                    
                        if( 
                                String.isNotBlank(currentProfileName) 
                                    && srAccess == 'Read Only' 
                        )
                        {
                            if( currentProfileName == 'DIFC Customer Community Plus User Custom' )
                            {
                                usr.profileId = mapProfileNameId.get('DIFC Customer Community Plus User Custom - Read Only');
                            }
                            else if( currentProfileName == 'DIFC Contractor Community Plus User Custom') 
                            {
                                usr.profileId = mapProfileNameId.get('DIFC Contractor Community Plus User Custom - Read Only');
                            }
                        }   
                        else if( 
                                    String.isNotBlank(currentProfileName) 
                                        && srAccess == 'Read/Write' 
                            )
                        {
                            if( currentProfileName == 'DIFC Customer Community Plus User Custom - Read Only' )
                            {
                                usr.profileId = mapProfileNameId.get('DIFC Customer Community Plus User Custom');
                            }
                            else if( currentProfileName == 'DIFC Contractor Community Plus User Custom - Read Only' ) 
                            {
                                usr.profileId = mapProfileNameId.get('DIFC Contractor Community Plus User Custom');
                            }
                        }
                        //update usr;

                            system.debug('@@@@@@@@@@@@@ usr.profileId '+usr.profileId);
                            system.debug('@@@@@@@@@@@@@ srRoles '+srRoles);
                            //String username = 'durga.kandula@pwc.com.uatfull';
                            //String password = 'Salesforce@1234';

                            //Generate the authorization header
                            //Blob headerValue = Blob.valueOf(username + ':' + password);
                            //String authorizationHeader = ' BASIC ' + EncodingUtil.base64Encode(headerValue);

                            //OB_LoginHelper.loginResult loginRst = OB_LoginHelper.validLogin(username,password,'difc--uatfull.my');
                            //system.debug('@@@@@@@@@@@@@ loginRst'+loginRst.sessionId);
                            
                            //String remoteURL = 'https://difc--uatfull.my.salesforce.com/services/data/v36.0/sobjects/User/'+loggedInUser.Id+'?_HttpMethod=PATCH';
                            String remoteURL = 'callout:Full_Access_Org/services/data/v36.0/sobjects/User/'+loggedInUser.Id+'?_HttpMethod=PATCH';
                            
                            
                            system.debug('@@@@@@@@@@@@@ remoteURL '+remoteURL);

                            map<string, object> mapToSerialize = new map<string, object>();
                            if( String.isNotBlank(usr.profileId) ) { mapToSerialize.put('profileId',usr.profileId);   }
                            if( String.isNotBlank(srRoles) ) { mapToSerialize.put('Community_User_Role__c',srRoles);   }    


                            String body = JSON.serialize(mapToSerialize);
                            system.debug('@@@@@@@@@ body '+ body);
                            if( String.isNotBlank(body) )
                            {
                                if(!Test.isRunningTest()){
                                    HTTPRequest httpRequest = new HTTPRequest();
                                    //httpRequest.setHeader('X-HTTP-Method-Override','PATCH');
                                    httpRequest.setMethod('POST');
                                    //httpRequest.setHeader('Authorization', 'OAuth '+loginRst.sessionId);
                                    //httpRequest.setHeader('Authorization', authorizationHeader);
                                    //system.debug('@@@@@@@@@@@@@ httpRequest Authorization   '+httpRequest.getHeader('Authorization') );
    
                                    httpRequest.setHeader('Content-Type', 'application/json');
                                    //httpRequest.setBody('{"profileId":"'+usr.profileId+'"}');
                                    httpRequest.setBody(body);
                                    httpRequest.setEndpoint(remoteURL);
                                    system.debug('@@@@@@@@@@@@@ httpRequest '+httpRequest);
                                    HTTPResponse res = new Http().send(httpRequest);
                                    System.debug('BODY: '+res.getBody());
                                    System.debug('STATUS:'+res.getStatus());
                                    System.debug('STATUS_CODE:'+res.getStatusCode());
                            	}
                            }
                            

                    } 
            
         // } 
          
          /*catch (Exception ex) 
          {
            system.debug('--ex--'+ex);
            
            //MedicalAppointmentCreate(step);
            Log__c objLog = new Log__c();
            objLog.Type__c = 'OB_ManageAssociatedEntityContrller';
            objLog.Description__c = 'Exceptio is : ' + ex.getMessage()+'-nLine # --' + ex.getLineNumber();
            insert objLog;

          }*/

             
            
    }



    
    

    @AuraEnabled
    public static RespondWrap createNewEntitySr(String requestWrapParam)  {
    
        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap =  new RespondWrap();
    
    
        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
        //System.debug('ZZZ🚀 OB_ManageAssoEntityCont.cls ~ line 427 ~ createNewEntitySr_M ~  ~ reqWrap-->'+ JSON.serialize(reqWrap));

        //get navigationUrl
        HexaBPM__Page_Flow__c pageFlow = new HexaBPM__Page_Flow__c();
        for(HexaBPM__Page_Flow__c pfFlow : [SELECT id , (select id from HexaBPM__Pages__r WHERE HexaBPM__Page_Order__c = 3 LIMIT 1) from HexaBPM__Page_Flow__c WHERE HexaBPM__Record_Type_API_Name__c = 'New_User_Registration' LIMIT 1]){
            if(!Test.isRunningTest()){
                respWrap.pageFlowURl = '?flowId='+pfFlow.Id+'&pageId='+pfFlow.HexaBPM__Pages__r[0].Id;
            pageFlow = pfFlow;
            }
            
        }
                        
    
        string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New User Registration').getRecordTypeId();
		string srTemplateId = '';
        contact conObj = new contact();
        
        for(contact relCont : [SELECT id,firstname,lastname , email, phone,Passport_No__c,Nationality__c,Birthdate,Gender__c,Place_of_Birth__c,
        Date_of_Issue__c,Passport_Expiry_Date__c,Place_of_Issue__c,Issued_Country__c,Country_of_Birth__c,OB_Passport_Document_Id__c
          from contact where id =: reqWrap.contactID]){
            conObj = relCont;
        }
        //System.debug('ZZZ🚀 OB_ManageAssoEntityCont.cls ~ line 427 ~ createNewEntitySr_M ~  ~ 1st conObj-->'+ JSON.serialize(conObj));
    
		for(HexaBPM__SR_Template__c srTempObj : [SELECT id from HexaBPM__SR_Template__c WHERE Name = 'New User Registration'
		AND HexaBPM__SR_RecordType_API_Name__c = 'New_User_Registration' AND HexaBPM__Active__c = true]){
			srTemplateId = srTempObj.Id;
		}

		if( srTemplateId!= null && srTemplateId!= '' && recordId !=null){

			HexaBPM__Service_Request__c srObj = new HexaBPM__Service_Request__c();
			srObj.recordTypeId = recordId;
            if(!Test.isRunningTest()){
                srObj.Page_Tracker__c = pageFlow.HexaBPM__Pages__r[0].Id;
            }
             srObj.HexaBPM__SR_Template__c = srTemplateId;
                
             //System.debug('ZZZ🚀 OB_ManageAssoEntityCont.cls ~ line 427 ~ createNewEntitySr_M ~  ~ conObj-->'+ JSON.serialize(conObj));
			//srObj.HexaBPM__Customer__c = reqWrap.accId;
            srObj.HexaBPM__Contact__c = reqWrap.contactID;
            srObj.Created_By_Agent__c = true;
            srObj.first_name__c = conObj.firstname; 
            srObj.last_name__c = conObj.lastname; 
            srObj.hexabpm__email__c = conObj.email;
            srObj.hexabpm__send_sms_to_mobile__c = conObj.phone;  
            srObj.Passport_No__c = conObj.Passport_No__c ;
            srObj.Nationality__c = conObj.Nationality__c;
            srObj.Date_of_Birth__c = conObj.Birthdate;
            srObj.Gender__c = conObj.Gender__c;
            srObj.Place_of_Birth__c = conObj.Place_of_Birth__c;
            srObj.Date_of_Issue__c = conObj.Date_of_Issue__c;
            srObj.Date_of_Expiry__c = conObj.Passport_Expiry_Date__c;
            srObj.Place_of_Issuance__c = conObj.Place_of_Issue__c;
            //srObj.Nationality__c = conObj.Issued_Country__c;  //v3.0 #14049 
            
            srObj.relationship_with_entity__c = 'Advisor';

			try{
                //System.debug('ZZZ🚀 OB_ManageAssoEntityCont.cls ~ line 427 ~ createNewEntitySr_M ~  ~ srObj-->'+ JSON.serialize(srObj));
			 insert srObj;
             respWrap.insertedSr =srObj;
             

             

             //set passport copy

             String srDocId = '';
             for(HexaBPM__SR_Doc__c SRDoc: [Select Id from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c =: srObj.Id and HexaBPM__Document_Name__c = 'Passport Copy' LIMIT 1]){
                srDocId = SRDoc.Id;
               
            }

            


            if(srDocId != '' && conObj.OB_Passport_Document_Id__c != null){
                ContentDocumentLink contDocLnkCloned = new ContentDocumentLink();
                contDocLnkCloned.LinkedEntityId = srDocId;
                contDocLnkCloned.contentdocumentid = conObj.OB_Passport_Document_Id__c;
                contDocLnkCloned.ShareType = 'V';
                insert contDocLnkCloned;
                system.debug('Content Link ' + contDocLnkCloned);
                respWrap.debugger = contDocLnkCloned;
            }
			 
			}catch(DMLException e) {
			
			 string DMLError = e.getdmlMessage(0) + '';
			 if(DMLError == null) {
			 DMLError = e.getMessage() + '';
			 }
			 respWrap.errorMessage = DMLError; 
			}

		}else{
			respWrap.errorMessage = 'SR Record Type Missing';
		}		     
		return respWrap;
    }
    
    // ------------ Wrapper List ----------- //
    
    public class RequestWrap {
    
    @AuraEnabled public String contactId;
    @AuraEnabled public String mainAccIdToSet;   
    @AuraEnabled public list<Account> relatedAccounts;   
    @AuraEnabled public User loggedInUser; 
    @AuraEnabled public list<string> relatedAccountIdsSet;
    
    }

    public class pendingActionWrapper {
    
        @AuraEnabled public list<HexaBPM__Step__c > actionItemList = new list<HexaBPM__Step__c > ();
        @AuraEnabled public list<Step__c> stepList = new list<Step__c>();  
        @AuraEnabled public string communityRole;   
        @AuraEnabled public object debugger;
        @AuraEnabled public User loggedInUser;    
        @AuraEnabled public list<Account> relatedAccounts;
        
        }
    
    public class RespondWrap {
     
	@AuraEnabled public object insertedSr;
    @AuraEnabled public list<Account> relatedAccounts = new list<Account>();
    @AuraEnabled public list<string> relatedAccountIdsSet = new list<string>();
    
    @AuraEnabled public string pageFlowURl;
    @AuraEnabled public User loggedInUser;
    @AuraEnabled public object debugger;
    @AuraEnabled public Map<id , boolean> emplymentViolationMap;
    @AuraEnabled public string errorMessage;
    @AuraEnabled public HexaBPM__Service_Request__c relEntitySr ;  
    @AuraEnabled public pendingActionWrapper pendingActionWrapper ;  
    @AuraEnabled public String mainAccIdToSet;       
     
    //@AuraEnabled public  OB_AgentEntityUtils.CreateEntityRespondWrap newEntityWrap = new OB_AgentEntityUtils.CreateEntityRespondWrap();                                                                                                                                                                                                                                 
    }
}