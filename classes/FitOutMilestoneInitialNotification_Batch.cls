global with sharing class FitOutMilestoneInitialNotification_Batch implements Database.Batchable < sObject > , Schedulable {
    global void execute(SchedulableContext ctx) {
        FitOutMilestoneInitialNotification_Batch bachable = new FitOutMilestoneInitialNotification_Batch();
        database.executeBatch(bachable);
    }
    
    String query = '';
    global Database.QueryLocator start(Database.BatchableContext bc) {
	    query = 'SELECT id,Account__r.Name,Building__c,Floor__c,Unit__c,Send_Initial_Conceptual_Notification__c,Send_Initial_Detail_Design__c,Send_Initial_Final_Inspection_Notificati__c,Send_Initial_First_Fix_Inspection__c,Unit_Numbers__c,Account__c,Type_of_Retail__c, Additional_Tenant_email__c,Lease__c, Service_Request__c, Conceptual_Design_Milestone_Achieved__c, Conceptual_Milestone_Start_Date__c, Detail_Design_Milestone_Achieved__c, Detail_Milestone_Start_Date__c, First_Inspection_Milestone_Achieved__c, First_Milestone_Start_Date__c, Final_Inspection_Milestone_Achieved__c, Final_Milestone_Start_Date__c, Next_Conceptual_Penalty_Date__c, Next_Detail_Design_Penalty_Date__c, Next_First_Inspection_Penalty_Date__c, Next_Final_Inspection_Penalty_Date__c, Is_Closed__c, Name FROM Fitout_Milestone_Penalty__c where Send_Initial_Conceptual_Notification__c = true OR Send_Initial_Detail_Design__c = true OR Send_Initial_Final_Inspection_Notificati__c = true OR Send_Initial_First_Fix_Inspection__c = true';  
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List < Fitout_Milestone_Penalty__c > scope) {
        string templatename ='On_the_1st_day_of_crossing_the_Fitout_Milestone';
        EmailTemplate emailTemp=[Select Id, Name, Subject, Body,HTMLValue from EmailTemplate where developerName =:templatename limit 1];
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
        List<String> sendingTo = new List<String>();
        if(Test.isRunningTest())sendingTo.add('mudasir@difc.ae');
        List<String> sendingToCC = system.label.ccFitOutMilestonePenaltyNotificationList.split(';');
        List<String> sendingToBCC = new List<String>(); 
        
        List<Messaging.SingleEmailMessage> messageList = new List<Messaging.SingleEmailMessage>();
        for(Fitout_Milestone_Penalty__c milestonePenaltyRec : (List<Fitout_Milestone_Penalty__c>) scope){
        	if(milestonePenaltyRec.Send_Initial_Conceptual_Notification__c){
				messageList.add(createEmailMessage(milestonePenaltyRec , sendingTo,sendingToCC,sendingToBCC,emailTemp,owea[0].id,'Conceptual Design Submission Milestone'));
			}
			if(milestonePenaltyRec.Send_Initial_Detail_Design__c){
				messageList.add(createEmailMessage(milestonePenaltyRec , sendingTo,sendingToCC,sendingToBCC,emailTemp,owea[0].id,'Detail Design Submission Milestone'));
			}
			if(milestonePenaltyRec.Send_Initial_First_Fix_Inspection__c){
				messageList.add(createEmailMessage(milestonePenaltyRec , sendingTo,sendingToCC,sendingToBCC,emailTemp,owea[0].id,'First Fix Inspection Milestone'));
			}
			if(milestonePenaltyRec.Send_Initial_Final_Inspection_Notificati__c){
				messageList.add(createEmailMessage(milestonePenaltyRec , sendingTo,sendingToCC,sendingToBCC,emailTemp,owea[0].id,'Final Inspection Milestone'));
			}
        } 
        if(messageList.size() > 0 && !Test.isRunningTest()){
        	system.debug('messageList-----------size----'+messageList.size());
        	//if(system.label.receipientsEmailAddressCheck=='false') 
            Messaging.sendEmail(messageList);
        }
    }
    global void finish(Database.BatchableContext BC) {
        
    }
    public Messaging.SingleEmailMessage createEmailMessage(Fitout_Milestone_Penalty__c milestonePenaltyRec , List<String> toaddresses,List<String> toCCaddresses,List<String> toBCCaddresses,EmailTemplate emailTemp,String orgWideEmailAddId, String milestoneDesc) {
        Messaging.SingleEmailMessage semail = new Messaging.SingleEmailMessage();
		system.debug('toaddresses----------'+toaddresses);
        toaddresses.add(milestonePenaltyRec.Additional_Tenant_email__c);
        semail.setToAddresses(toaddresses);
        semail.setCcAddresses(toCCaddresses);
        semail.templateid = emailTemp.id;        
        String subjectString = emailTemp.Subject;
        subjectString = subjectString.contains('{!Milestone}') ? subjectString.replace('{!Milestone}',milestoneDesc) : subjectString;
		subjectString = subjectString.contains('{!Fitout_Milestone_Penalty__c.Account__c}') ? subjectString.replace('{!Fitout_Milestone_Penalty__c.Account__c}',milestonePenaltyRec.Account__r.Name) : subjectString;
		subjectString = subjectString.contains('{!Fitout_Milestone_Penalty__c.Building__c}') ? subjectString.replace('{!Fitout_Milestone_Penalty__c.Building__c}',milestonePenaltyRec.Building__c) : subjectString;
		subjectString = subjectString.contains('{!Fitout_Milestone_Penalty__c.Floor__c}') ? subjectString.replace('{!Fitout_Milestone_Penalty__c.Floor__c}',milestonePenaltyRec.Floor__c) : subjectString;
		subjectString = subjectString.contains('{!Fitout_Milestone_Penalty__c.Unit__c}') ? subjectString.replace('{!Fitout_Milestone_Penalty__c.Unit__c}',milestonePenaltyRec.Unit__c) : subjectString;
        semail.setOrgWideEmailAddressId(orgWideEmailAddId);
        string bodyValue =  emailTemp.Body;
        semail.setSubject(subjectString);
        bodyValue = bodyValue.replace('{!Fitout_Milestone_Penalty__c.Account__c}',milestonePenaltyRec.Account__r.Name);
		bodyValue = bodyValue.replace('{!Fitout_Milestone_Penalty__c.Building__c}',milestonePenaltyRec.Building__c);
		bodyValue = bodyValue.replace('{!Fitout_Milestone_Penalty__c.Floor__c}',milestonePenaltyRec.Floor__c);
		bodyValue = bodyValue.replace('{!Fitout_Milestone_Penalty__c.Unit__c}',milestonePenaltyRec.Unit__c);
        bodyValue = bodyValue.replace('{!Milestone}',milestoneDesc);
        semail.setPlainTextBody(bodyValue); 
        system.debug('messageRec------Mudasir------'+semail);
        return semail;
    }
}