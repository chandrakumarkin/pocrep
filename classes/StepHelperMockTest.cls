/******************************************************************************************
 *  Class Name  : StepHelperMockTest
 *  Author      : Sai kalyan Sanisetty 
 *  Company     : DIFC
 *  Date        : 14 April 2019        
 *  Description :                
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1   14 April 2019         Sai                Initial Version  
*******************************************************************************************/
@isTest
global class StepHelperMockTest implements HttpCalloutMock {
	
	global HTTPResponse respond(HTTPRequest req) {
		
		system.debug('@@#$$$$#@@'+req.getEndpoint()+'@@!!$$$$!@@'+req.getMethod());
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        res.setStatusCode(200);
        res.setBody('{"access_token":"d2c793ba48c4329fc8042b6c7a6ea85d34281a45646cf8b8d61da4f5c4bfc"}');
        return res;
	}
}