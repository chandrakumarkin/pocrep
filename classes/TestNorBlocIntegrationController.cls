/******************************************************************************************
 *  Class Name  : TestNorBlocIntegrationController
 *  Author      : Sai kalyan Sanisetty 
 *  Company     : DIFC
 *  Date        : 23 FEB 2020        
 *  Description : Test Class for NorBlocIntegrationController                  
 ----------------------------------------------------------------------
   Version     Date              Author                Remarks                                                 
  =======   ==========        =============    ==================================
    v1.1    23 FEB 2020           Sai                Initial Version  
*******************************************************************************************/
@isTest
public class TestNorBlocIntegrationController {
    
    static testmethod void myUnitTest1(){
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        
        List<Contact> lstContact = new List<Contact>();
        lstContact = OB_TestDataFactory.createContacts(1,insertNewAccounts);
        insert lstContact;
        system.debug('!!@###'+lstContact);
        Profile profile1 = [Select Id from Profile where name = 'DIFC Customer Community User Custom'];
        User portalAccountOwner1 = new User(ProfileId = profile1.Id,Username = System.now().millisecond() + 'test2@test.com',Alias = 'Test11',
                                            Email='TestUser@Difc.ae',EmailEncodingKey='UTF-8',Firstname='Test',Lastname='DIFC',LanguageLocaleKey='en_US',
                                            LocaleSidKey='en_US',TimeZoneSidKey='America/Chicago',ContactId=lstContact[0].Id,isActive=true);
        
        INSERT portalAccountOwner1;
        Test.startTest();
            system.debug('!!@@##'+portalAccountOwner1);
            system.runAs(portalAccountOwner1){
                
                Test.setMock(HttpCalloutMock.class, new NorBlocIntegrationControllerMockTest());
                Integer turnOverVal = 12333;
                NorBlocIntegrationController.norBlocIntegration(insertNewAccounts[0].ID,turnOverVal );
                
            }
        Test.stopTest();
    }   
    
}