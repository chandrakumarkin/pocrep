/**
*Author : Merul Shah
*Description : This validate the proper allocation of share.
**/
public without sharing class OB_ShareAllocationValidateHelper
{
    static final String SHARE_ALLOCATION_ERROR = 'Please ensure the total number of issued shares is allocated.';
    // validate the proper allocation of share.
    @AuraEnabled  
    public static ResponseWrapper  validateShareHolderDetails(RequestWrapper reqWrapPram)
    {
        String srId = reqWrapPram.srId;
        String accID;
        ResponseWrapper respWrap = new ResponseWrapper();
        String finalAllocationStatus='';
        // getting SR.                                          
        for(HexaBPM__Service_Request__c srTemp : [ 
                                                    SELECT id,
                                                           HexaBPM__Customer__c 
                                                      FROM HexaBPM__Service_Request__c
                                                     WHERE id =:srId  
                                                  ] )
        {
            accID = srTemp.HexaBPM__Customer__c;
        
        }  
        system.debug('@@@@@@@@@@ accID '+accID);
        
        //Map<String,List<Shareholder_Detail__c>> mapShareDetail = new Map<String,List<Shareholder_Detail__c>>();
        Map<String,Decimal> mapShareDetailTotal = new Map<String,Decimal>();
        List<Shareholder_Detail__c> lstShrDetail = new  List<Shareholder_Detail__c>();
        String amedError = ''; 
        //Query Amendment and check its allocation assignment.
        system.debug(' All Amed '+getAmendmentBySrId(srId).Size());
        for(HexaBPM_Amendment__c amed : getAmendmentBySrId(srId)) 
        {
            if(amed.Role__c != NULL && amed.Role__c.contains('Shareholder') )
            {
                 system.debug('###### All Amed Shre'+amed);
                if(amed.Shareholder_Details__r != NULL && amed.Shareholder_Details__r.size()>0)
                {
                    for(Shareholder_Detail__c shrDetail : amed.Shareholder_Details__r)
                    {
                        lstShrDetail.add(shrDetail);
                    } 
                }    
                else 
                {
                    system.debug(' No shareholder '+amed.Individual_Corporate_Name__c);
                    amedError += 'Please allocate share to '+amed.Individual_Corporate_Name__c+'\n';
                   
                }
               
                
            }
        }
        
        if( !String.isBlank(amedError) )
        {
            //respWrap.finalAllocationStatus = amedError;
            //respWrap.finalAllocationStatus = 'Please ensure the total number of issued shares is allocated.';
            respWrap.finalAllocationStatus = SHARE_ALLOCATION_ERROR;
            return respWrap;
        }
        
        for(Shareholder_Detail__c shrDetail : lstShrDetail)
        {
            
            Decimal existAlloctionShare = OB_QueryUtilityClass.treatNullAsZero(shrDetail.No_of_Shares_Allotted__c);
            if( mapShareDetailTotal.containsKey( shrDetail.Account_Share__c ) )
            {
               Decimal noOfShare = OB_QueryUtilityClass.treatNullAsZero(mapShareDetailTotal.get(shrDetail.Account_Share__c));
               noOfShare = noOfShare + existAlloctionShare ;
               mapShareDetailTotal.put(shrDetail.Account_Share__c ,noOfShare);
            }
            else
            {
                 mapShareDetailTotal.put(shrDetail.Account_Share__c ,existAlloctionShare);
            }
         }
         system.debug('@@@@@@@@@@ mapShareDetailTotal '+mapShareDetailTotal );
         
         //Quering Account Share Details.
         //Checking sufficient allocation.
         
         Map<String,String> mapAllocationstatus = new Map<String,String>();
         for(Account_Share_Detail__c accShreDtl  : [SELECT id,
                                                    	   Name,
                                                           No_of_shares_per_class__c,
                                                    	   Account__c
                                                      FROM Account_Share_Detail__c
                                                     WHERE Account__c =:accID
                                                     //WHERE id =:mapShareDetailTotal.keySet() 
                                                   ])
         {
             String statusString;
             Decimal avaibleShare = OB_QueryUtilityClass.treatNullAsZero(accShreDtl.No_of_shares_per_class__c);
             system.debug('%%%%%%%%% '+accShreDtl.Name);   
                
             if( mapShareDetailTotal.containsKey(accShreDtl.Id) )
             {
                Decimal allocatedShare = OB_QueryUtilityClass.treatNullAsZero(mapShareDetailTotal.get(accShreDtl.Id));
                
                if( allocatedShare > avaibleShare )
                {
                    //statusString = 'Please reduce '+roundDec(allocatedShare-avaibleShare )+' share of class '+accShreDtl.name;
                	statusString = ''+accShreDtl.name+':'+'Reduce '+roundDec(allocatedShare-avaibleShare)+' shares.';
                }
                else if( allocatedShare < avaibleShare)
                {
                    // statusString = 'Please increase '+roundDec( avaibleShare-allocatedShare  )+' share of class '+accShreDtl.name;
                	statusString = ''+accShreDtl.name+':'+'Increase '+roundDec( avaibleShare-allocatedShare  )+' shares.';
                }
                
                
             }
             else 
             {
                 //statusString = 'Please allocate '+roundDec(avaibleShare)+' share of class '+accShreDtl.name;
                 statusString = ''+accShreDtl.name+':'+'Allocate '+roundDec(avaibleShare)+' shares.';
             }
            
             
             if( !String.isBlank(statusString) )
             {
                 //Replcaing with generic msg as per revised requirement.
                 statusString = SHARE_ALLOCATION_ERROR;
                 mapAllocationstatus.put(accShreDtl.name,statusString);
             }
         }
         
         
         system.debug('@@@@@@@@@@ mapAllocationstatus '+mapAllocationstatus);
         // finalAllocationStatus
         for( String shareClass : mapAllocationstatus.keySet() )
         {
             finalAllocationStatus +=  mapAllocationstatus.get(shareClass)+ '\n';
         }
         
         if( finalAllocationStatus <> NULL )
         {
             respWrap.finalAllocationStatus = finalAllocationStatus ;
         }
         return respWrap; 
        
    
    }
    
    public static Decimal roundDec(Decimal num)
    {
        return (num != NULL ? num.setScale(2) : 0);
    }
    
    public static List<HexaBPM_Amendment__c> getAmendmentBySrId(String srId)
    {
        return [SELECT id, 
                        Role__c, 
                        Passport_No__c, 
                        Nationality_list__c, 
                        Date_of_Birth__c, 
                        Passport_Issue_Date__c, 
                        Passport_Expiry_Date__c, 
                        Place_of_Issue__c, 
                        Are_you_a_resident_in_the_U_A_E__c, 
                        RecordType.DeveloperName, 
                        ServiceRequest__r.Id, 
                        ServiceRequest__r.HexaBPM__customer__c, 
                        Is_this_Entity_registered_with_DIFC__c, 
                        Registered_No__c, 
                        Place_of_Registration__c, 
                        Country_of_Registration__c, 
                        Individual_Corporate_Name__c, 
                        Entity_Name__c, 
                        Entity_Name__r.Name,
                		(
                            SELECT id,
                                    No_of_Shares_Allotted__c,
                                    Account_Share__c 
                            FROM Shareholder_Details__r
                            WHERE Status__c != 'Inactive'
                           
                         )
                   FROM HexaBPM_Amendment__c
                  WHERE ServiceRequest__c = :srId
            	];
    }
    

    
    public class RequestWrapper 
    {
        @AuraEnabled public Id srId { get; set; }
        public RequestWrapper()
        {
        }
    }

    public class ResponseWrapper 
    {
       
        @AuraEnabled 
        public String finalAllocationStatus{ get; set; }
        
        public ResponseWrapper()
        {
        }
    }
    
}