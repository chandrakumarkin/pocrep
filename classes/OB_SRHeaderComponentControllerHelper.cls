/*
    Author      : Durga Prasad
    Date        : 19-Feb-2020
    Description : helper class for OB_SRHeaderComponentController
    ---------------------------------------------------------------------------------------
*/
public without sharing class OB_SRHeaderComponentControllerHelper {
    public static HexaBPM__Step__c CreateActionItem(string SRID,string SRStepId){
    	HexaBPM__Step__c objstep = new HexaBPM__Step__c(HexaBPM__SR__c=SRID,HexaBPM__SR_Step__c=SRStepId);
    	string RecordTypeAPIName;
    	for(HexaBPM__SR_Steps__c srstp:[Select Id,HexaBPM__Summary__c,HexaBPM__Step_Template__c,HexaBPM__Start_Status__c,HexaBPM__Step_RecordType_API_Name__c,OwnerId,HexaBPM__Step_No__c,HexaBPM__Estimated_Hours__c from HexaBPM__SR_Steps__c where Id=:SRStepId]){
	        objstep.HexaBPM__Step_Template__c = srstp.HexaBPM__Step_Template__c;
	        objstep.HexaBPM__Status__c = srstp.HexaBPM__Start_Status__c;
	        if(srstp.HexaBPM__Step_RecordType_API_Name__c!=null)
	        	RecordTypeAPIName = srstp.HexaBPM__Step_RecordType_API_Name__c;
	        objstep.OwnerId = srstp.OwnerId;
	        objstep.HexaBPM__Start_Date__c = system.today();
	        objstep.HexaBPM__Step_No__c = srstp.HexaBPM__Step_No__c;
	        objstep.HexaBPM__Sys_Step_Loop_No__c = srstp.HexaBPM__Step_No__c+'_1';
	        objstep.HexaBPM__Summary__c = srstp.HexaBPM__Summary__c;
	        ID BHId = id.valueOf(Label.HexaBPM.Business_Hours_Id);
	        Long sla = srstp.HexaBPM__Estimated_Hours__c.longvalue();
	        sla = sla*60*60*1000L;
	        datetime CreatedTime = system.now();
	        if(sla!=null && BHId!=null)
	            objstep.HexaBPM__Due_Date__c = BusinessHours.add(BHId,CreatedTime,sla);
    	}
    	if(RecordTypeAPIName!=null){
        	for(RecordType RT:[Select Id from RecordType where sObjectType='HexaBPM__Step__c' and DeveloperName=:RecordTypeAPIName and IsActive=true]){
        		objstep.RecordTypeId = RT.Id;
        	}
    	}
    	return objstep;
    }
}