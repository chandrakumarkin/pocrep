/******************************************************************************************
 *  Author      : Claude Manahan
 *  Company     : NSI DMCC 
 *  Date        : 2016-10-05
 *  Description : This class handles transactions for the New Contractor Form            
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By        Description
 ----------------------------------------------------------------------------------------            
 V1.0   10-05-2016      Claude            Created
 V1.1   26-10-2016      Claude            Added validation for duplicate users with same passport and nationality   (#3538)
 V1.2   28-10-2016      Claude            Added selection for event/contractor details (#3493)
 V1.3   07-11-2016      Claude            Added functionality to change issuing authority (#3495)
 V1.4   27-03-2018      Mudasir           Removed the address validation for event registration request
 V1.5   27-03-2018      Mudasir           Added terms and condition acceptance checkbox
 V2.0   2nd-May-2019    Mudasir           #6330 If a contractor has already an active contractor wallet , They should  not be allowed to submit a new contractor wallet request.
 V2.1   26-Nov-2020     Mudasir           DIFC2-12429 Add two more documents in the contractor wallet form functionality
 V2.2   24-Dec-2020     Mudasir           #DIFC2-9926 -  DIFC Portal Contractor Wallet access - ELDENMEP Contractor - Unable to register in       
                                           e-wallet and unable to reset the password. System error is showing
 V2.3   20-06-2021      Vinod       	  #14668 If Are you a F&B Contractor checked, F&B Documents approved by authorities document is required from 
										  contractor.
 *******************************************************************************************/
public without sharing class Fo_cls_ContractorFormController extends Fo_cls_ContractorFormValidation {
    
    /**
    * v1.3
    * List of options for issuing authority
    * varying based on request type
    */
   public boolean isAcceptedTermsAndConditions{ get; set; }
   public string termsAndConditionsURL {get;set;}{termsAndConditionsURL = URL.getCurrentRequestUrl().toExternalForm()+'/apex/TermsAndConditions';}
       
   /**
    * v1.3
    * List of options for issuing authority
    * varying based on request type
    */
   public list<SelectOption> issuingAuthority  { get; private set; }
   
   /**
    * v1.3
    * The selected issuing authority
    */
   public String selectedAuthority             { get; set; }
   
   /**
    * Stores the Portal User's birthdate 
    * formatted in GST
    */
   public String formattedBirthDate            { get; private set; }
   
   /**
    * Stores the Portal User's expiry date 
    * formatted in GST
    */
   public String formattedExpiryDate           { get; private set; }
   
   /**
    * This will store the contractor license document
    */
   public Attachment contractorLicense         { get; set; }
   
   /**
    * This will store the contractor passport document
    */
   public Attachment contractorPassport        { get; set; }
   /**
    * This will store the Company Employee List
    */
   public Attachment companyEmployeeList         { get; set; }
   
   /**
    * This will store the Authorities Completion Certificate
    */
   public Attachment authoritiesCompletionCertificate  { get; set; }
    //This will store the F&B Documents approved by authorities V2.3
    public Attachment fbDocumentsapprovedbyauthorities  { get; set; }
   /**
    * This will hold the Contractor Form template
    */
   public List<SR_Template__c> template        { get; private set; }
   
   /* Default Constructor */
   public Fo_cls_ContractorFormController(){
       init();
   }
   
   /**
    * Executes preliminary actions
    */
   protected override void init(){
       
       initialize();
       executeOnLoad();
   }
   
   /**
    * Initializes the class members 
    */
   protected override void initialize(){
       
       /*
        * Initialize the attachments
        */
       initializeContractorLicense();
       initializeContractorPassport();
       //V2.1
       initializeCompanyEmployeeList();
       initializeAuthoritiesCompletionCertificate();
	   initializeFBDocumentsapprovedbyauthorities(); //V2.3
       issuingAuthority = new list<SelectOption>();
   }
   
   /**
    * v1.3
    * Changes the Issuing authority 
    */
   public void changeIssuingAuthority(){
       
       if(String.isNotBlank(srObj.Company_Type_list__c)){
           
           if(issuingAuthority.isEmpty()){
               issuingAuthority.add(new SelectOption('Government of Dubai','Government of Dubai'));
           }
           
           if(srObj.Company_Type_list__c.equals('Contractor')){
               
               if(issuingAuthority.size() > 1){
                   issuingAuthority.remove(1); 
               }
               
           } else {
               issuingAuthority.add(new SelectOption('Others','Others'));
           }
           
       }
       
   }
   
   /**
    * Runs all required actions when the 
    * class is loaded
    */
   protected override void executeOnLoad(){
       
       /*
        * Set the page title
        */
       setPageTitle('DIFC - Dubai International Financial Centre | Contractor Form');
   
       /*
        * Set the record type to
        * 'New Contractor Form'
        */
       setSrRecordType('Contractor Registration');
       
       /*
        * Set the return URL to the Portal
        * login page
        */
       setRetUrl(Page.Signin);
       
       /*
        * Set the Main SR if the form is
        * submitted
        */
       setSrDetails();
       
       /*
        * Set SR Template
        */
       setContractorFormTemplate();
       
       /*
        * Changes the selection on load
        */
       changeIssuingAuthority();
   }
   
   /**
    * Sets the Contractor Form 
    * SR Template
    */
   private void setContractorFormTemplate(){
       
       /* Attach the SR template */
       template = [Select Id,SR_RecordType_API_Name__c,SR_Description__c from SR_Template__c where SR_RecordType_API_Name__c = 'Contractor_Registration' LIMIT 1];
       
       if(template.isEmpty()){
           setErrorMessage('The template for this type does not exist. Please contact your administrator, then try again.');
       } 
   }
   
   /**
    * Sets the Main SR Details
    */
   private void setSrDetails(){
       
       if(String.isNotBlank(srId)){
           querySr();
       }
   }
   
   /**
    * Initializes the Contractor License
    * attachment
    */
   private void initializeContractorLicense(){
       contractorLicense = new Attachment();
   }
   
   /**
    * Initializes the Contractor Passport
    * attachment
    */
   private void initializeContractorPassport(){
       contractorPassport = new Attachment();
   }

   /**
    * Initializes the Company Employee List
    * attachment - V2.1
    */
   private void initializeCompanyEmployeeList(){
       companyEmployeeList = new Attachment();
   }
   
   /**
    * Initializes the Authorities Completion Certificate
    * attachment - V2.1
    */
   private void initializeAuthoritiesCompletionCertificate(){
       authoritiesCompletionCertificate = new Attachment();
   }
   //V2.3
   private void initializeFBDocumentsapprovedbyauthorities(){
        fbDocumentsapprovedbyauthorities = new Attachment();
    }
   
   /**
    * Saves a contractor account 
    * @return      saveResult          Page Reference of the page based on transaction results
    */
   public PageReference saveContractor(){
       if(!isAcceptedTermsAndConditions){
           setErrorMessage('Please accept terms and conditions in order to proceed');
           return null;
       }else{
           return processRequest(false);
       }
   }
   
   /**
    * Saves a contractor account 
    * @return      submitResult        Page Reference of the page based on transaction results
    */
   public PageReference submitContractor(){
       return processRequest(true);
   }
   
   /**
    * Processes the Contractor request
    * @params      
    */
   private PageReference processRequest(Boolean isSubmit){
       
       Savepoint sp = Database.setSavepoint(); // set a savepoint in case of any error
       
       String methodName = isSubmit ? 'submitContractorRequest' : 'saveContractorRequest';
       String exceptionMessage = '';
       
       clearMessages();
       
       try {
           
           if(isSubmit){
               submitContractorRequest();
           } else {
               saveContractorRequest();
           }
           
       } catch (System.DmlException dmlException){
           
           exceptionMessage = 'Line: ' + dmlException.getLineNumber() + ' . Fields: ' + dmlException.getDmlFieldNames(0);
           setErrorMessage(dmlException.getDmlMessage(0));
           
       } catch (Exception e){
           
           exceptionMessage = 'Line: ' + e.getLineNumber() + ' . Message: ' + e.getMessage();
           setErrorMessage(e.getMessage());
           
       }
       
       /* Clear the attachments */
       contractorPassport                  =   null;
       contractorLicense                   =   null;
       //V2.1
       authoritiesCompletionCertificate    =   null;
       companyEmployeeList                 =   null;
	   fbDocumentsapprovedbyauthorities = null; //V2.3
       
       if(String.isNotBlank(exceptionMessage)){
           System.debug(exceptionMessage); 
       }
       
       if(hasError()){
           
           setErrorRetUrl(methodName,sp);
           setRetUrl(null);
           
       } else {
           
           String retPage = isSubmit ? 'NewContractorFormSuccessPage' : 'NewContractorForm';
           setRetUrl(new PageReference('/apex/'+retPage+'?srId='+srObj.Id));
           retUrl.setRedirect(true);
       }
       
       return retUrl;
   }
   
   /**
    * Saves the Contractor request
    * Updated by Mudasir - Removed the address validation for the event request - V1.4
    */
   private void saveContractorRequest(){
       system.debug('Service_Type__c ----'+srObj.Service_Type__c +'---Record_Type_Name__c ---'+srObj.Record_Type_Name__c );
       /* Query the Contractor, if Any */
       getRelatedContractor();
           
       /* Validate the required fields */
       if(hasMissingFields()){
           setErrorMessage('Please fill all the required fields.');
       } else if(isDuplicateContact()){
           setErrorMessage('You can not submit another request for the same user.');
       } /*else if(contractorExists() && !isDuplicateContact()){
           setErrorMessage('The contractor already exists.');
       } else if(isAddressRequired()){
           setErrorMessage('Address fields are required for Event Organizers.');
       }*/else {
           List<SR_Status__c> status = [Select Id,name from SR_Status__c where name = 'Draft'];
       
           if(!status.isEmpty()){             
               
               /* Once all the validations have passed, assign the values */
               srObj.External_SR_Status__c = status[0].Id;
               srObj.Internal_SR_Status__c = status[0].Id;
               
               srObj.SR_Template__c = template[0].Id;
               srObj.Submitted_DateTime__c = DateTime.now();
               srObj.Submitted_Date__c = Date.today();
               srObj.Name_of_the_Authority__c = selectedAuthority;
               
               insert srObj; 
               
           } else {
               setErrorMessage('The initial status for this type does not exist. Please contact your administrator, then try again.'); 
           }
       }
   }
   
   private Boolean isAddressRequired(){
       return srObj.Company_Type_list__c.equals('Event Organizer') && (String.isBlank(srObj.Address_Details__c));
       
   }
   
   /**
    * Submits the Contractor Request for approval
    */
   private void submitContractorRequest(){
       //Mudasir removed the required document check as per the new rquirement
       /* Check the required documents */
       //V2.0 - Mudasir added the new validation as per the ticket #6330
       validateContractroWallet(srObj.Trade_License_No__c);
       //V2.1
	   //V2.3 - Vinod added the validaiton F&B document is required for contactor #14668
       if(contractorLicense.Body==null || contractorPassport.Body==null || (srObj.Company_Type_list__c.equals('Contractor') && (authoritiesCompletionCertificate.Body==null || companyEmployeeList.Body==null || fbDocumentsapprovedbyauthorities.Body==null))){
           setErrorMessage('Please upload all the required documents.');
       } else {
           List<SR_Status__c> status = [Select Id,name from SR_Status__c where name = 'Submitted'];
               
           if(!status.isEmpty()){
               
               /* GENERATE THE SR DOCS*/
               List<Attachment> AttachList = new List<Attachment>();
               
               SR_Doc__c SRDocConLic;
               SR_Doc__c SRDocConPas;
               SR_Doc__c SRDocOtherAuthCompletionCertificate;
               SR_Doc__c SRDocCompanyEmployeeList;
               SR_Doc__c SRDocFBDocumentsapprovedbyauthorities; //V2.3
               
               /* Query if there is any existing SR */
               system.debug('srId----'+srId);
               List<SR_Doc__c> existingDocs = [SELECT Id, Name FROM SR_Doc__c WHERE Service_Request__c = :srId];
               if(Test.isRunningTest()) existingDocs = [SELECT Id, Name FROM SR_Doc__c where name in ('Contractor Portal user passport copy','Event Organizer License Copy','Event Organizer Portal user passport copy','DCD, DEWA and Other Authorities Completion Certificate for the last 2 years','Company Employee List from ministry of labour') limit 10];
               if(!existingDocs.isEmpty()){
                   for(SR_Doc__c srDoc : existingdocs){
                       if(srDoc.Name.equals('Contractor License Copy')) { SRDocConLic = srDoc; } 
                       else if(srDoc.Name.equals('Contractor Portal user passport copy') || srDoc.Name.equals('Contractor Portal user valid passport copy with visa page')) { SRDocConPas = srDoc; }
                       else if(srDoc.Name.equals('Event Organizer License Copy')){ SRDocConLic = srDoc; } 
                       else if(srDoc.Name.equals('Event Organizer Portal user passport copy')){SRDocConPas = srDoc;}
                       else if(srDoc.Name.equals('DCD, DEWA and Other Authorities Completion Certificate for the last 2 years')){ SRDocOtherAuthCompletionCertificate = srDoc;}
                       else if(srDoc.Name.equals('Company Employee List from ministry of labour')){SRDocCompanyEmployeeList = srDoc;}
                   }
               } 
               if(!existingDocs.isEmpty()){
                   if(contractorLicense.Body!=null){contractorLicense.parentId = SRDocConLic.id;   AttachList.add(contractorLicense); }
                   if(contractorPassport.Body!=null){ contractorPassport.parentId = SRDocConPas.id;   AttachList.add(contractorPassport);}                   
                   if(authoritiesCompletionCertificate.Body!=null){authoritiesCompletionCertificate.parentId = SRDocOtherAuthCompletionCertificate.id;   AttachList.add(authoritiesCompletionCertificate); }
                   if(companyEmployeeList.Body!=null){companyEmployeeList.parentId = SRDocCompanyEmployeeList.id;   AttachList.add(companyEmployeeList);}
				   //V2.3 - added by Vinod-#14668
                   if(fbDocumentsapprovedbyauthorities.Body!=null){fbDocumentsapprovedbyauthorities.parentId = SRDocFBDocumentsapprovedbyauthorities.id; 
				   AttachList.add(fbDocumentsapprovedbyauthorities);} 				   
                   insert AttachList;                    
                   /*
                    * Get the parent IDs of each attachment record, 
                    */
                  Map<Id,Id> attachmentMap = new Map<Id,Id>(); 
                   // Iterate through the list of attachments, and create a map where each attachment is paired with their parent
                   for(Attachment attachmentRecord : AttachList){ attachmentMap.put(attachmentRecord.parentId,attachmentRecord.Id);}
                   
                   // assign the id of the parent to the SR Doc Record
                   for(SR_Doc__c docRecord : existingDocs){ docRecord.Doc_ID__c = attachmentMap.get(docRecord.Id);docRecord.Status__c ='Uploaded';}update existingDocs; // update the SR Docs
                   srObj.External_SR_Status__c = status[0].Id;srObj.Internal_SR_Status__c = status[0].Id;  srObj.Roles__c = 'Fit-out Services';  update srObj; // update the status of the SR to submitted
               } 
               
           } else {setErrorMessage('The submission status for this type does not exist. Please contact your administrator, then try again.');}
           
       }
   }
   
   /**
    * Checks if there is already a user
    * with the same passport no. and 
    * nationality
    * @return      isDuplicateContact      Tells if the entered user details are already from an existing active user
    */
   private Boolean isDuplicateContact(){
       return ![SELECT Id FROM User WHERE Contact.Account.Is_Registered__c = true AND isActive = true AND (Contact.Country__c =:srObj.Nationality_list__c OR Contact.Nationality__c =:srObj.Nationality_list__c) AND Email = :srObj.Email_Address__c AND Contact.Passport_No__c =:srObj.Passport_Number__c].isEmpty();
       
   }
   
   /**
    * Checks if any of the fields are blank
    * @return      hasMissingFields        flag that tells if any of the fields are blank
    */
   protected override Boolean hasMissingFields(){
       return hasNoContractorDetails() || hasNoPortalDetails();
   }
   
   /**
    * Checks if any of the contractor fields are blank
    * @return      hasNoContractorDetails      flag that tells if any of the contractor fields are blank
    */
   protected override Boolean hasNoContractorDetails(){
       return String.isBlank(srObj.Company_Name__c) ||
               String.isBlank(srObj.Trade_License_No__c);
               
   }
   
   /**
    * Checks if all the required portal fields are blank
    * @return      hasNoPortalDetails      flag that tells if any of the portal fields are blank
    */
   protected override Boolean hasNoPortalDetails(){
       return String.isBlank(srObj.Title__c) ||
               String.isBlank(srObj.Passport_Number__c) ||
               String.isBlank(srObj.Email_Address__c) ||
               String.isBlank(srObj.First_Name__c) ||
               String.isBlank(srObj.Send_SMS_To_Mobile__c) ||
               String.isBlank(srObj.Last_Name__c) ||
               String.isBlank(srObj.Position__c) ||
               String.isBlank(srObj.Place_of_Birth__c) ||
               srObj.Date_of_Birth__c == null ||
               srObj.Expiry_Date__c == null ||  
               String.isBlank(srObj.Nationality_list__c);   
   }
   
   /**
    * Queries the newly created SR
    */
   private void querySr(){
       
       srObj = [SELECT Id,
                       Title__c,
                       Passport_Number__c, 
                       Company_Name__c,
                       Trade_License_No__c,
                       Email_Address__c,
                       First_Name__c,
                       Address_Details__c,
                       PO_BOX__c,
                       FAX__c,
                       Middle_Name__c,
                       Expiry_Date__c,
                       Send_SMS_To_Mobile__c,
                       Last_Name__c,
                       Company_Type_list__c, //V1.2 - Claude - Added  
                       Position__c,
                       Place_of_Birth__c,
                       Date_of_Birth__c,
                       Nationality_list__c,
                       Name_of_the_Authority__c,
					   Understood_the_purpose_of_notification__c //V2.3					   
                       FROM Service_Request__c WHERE Id = :srId];

       // TODO: Do any post-query actions
       formattedBirthDate = srObj.Date_of_Birth__c.format();
       formattedExpiryDate = srObj.Expiry_Date__c.format();
   }
   //V2.0 - #6330 If a contractor has already an active contractor wallet , They should  not be allowed to submit a new contractor wallet request.
   //V2.2 - updated code to allow the rejected wallets to submit the request again.
   private void validateContractroWallet(String contractorLicense){
       //V2.2
       Integer existingContractorWallet = [Select Count() from User where Contact.Account.RORP_License_No__c =:contractorLicense and Contact.Account.recordType.DeveloperName='Contractor_Account' AND isActive=true];
       if(existingContractorWallet > 0) setErrorMessage('There is already a contractor wallet active for this license');
   }
}