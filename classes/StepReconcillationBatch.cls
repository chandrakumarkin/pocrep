global class StepReconcillationBatch implements 
    Database.Batchable<sObject>, Database.Stateful ,Schedulable {
    	
    global void execute(SchedulableContext ctx) {
        StepReconcillationBatch bachable = new StepReconcillationBatch();
        database.executeBatch(bachable);
    }
    
    // instance member to retain state across transactions
    global Integer recordsProcessed = 0;
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(
            'Select id,name,Step_Id__c from Temp_Salesforce_Step_Data__c'
        );
    }
    global void execute(Database.BatchableContext bc, List<Temp_Salesforce_Step_Data__c> salesforceSteps){
        // process each batch of records
        Map<String,String> salesforceStepMap = new Map<String , String>();
		for(Temp_Salesforce_Step_Data__c salesforceStep: salesforceSteps){
			salesforceStepMap.put(salesforceStep.Step_Id__c, salesforceStep.id);
		}
		//Now query Temp_SAP_Step_Data__c
		List<Temp_SAP_Step_Data__c> SAPSteps = new List<Temp_SAP_Step_Data__c>();
		for(Temp_SAP_Step_Data__c sapStep: [Select id,Temp_Salesforce_Step_Data__c,name,Salesforce_Step_Unique_Identifier__c from Temp_SAP_Step_Data__c where Salesforce_Step_Unique_Identifier__c != NULL AND Salesforce_Step_Unique_Identifier__c IN : salesforceStepMap.keyset()]){
			sapStep.Temp_Salesforce_Step_Data__c = salesforceStepMap.get(sapStep.Salesforce_Step_Unique_Identifier__c);
			sapStep.Step_Not_Created_in_Salesforce__c = false;
			SAPSteps.add(sapStep);
		}
		update SAPSteps;
    }    
    global void finish(Database.BatchableContext bc){
        System.debug(recordsProcessed + ' records processed!');
        AsyncApexJob job = [SELECT Id, Status, NumberOfErrors, 
            JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob
            WHERE Id = :bc.getJobId()];
        // call some utility to send email
        //EmailUtils.sendMessage(job, recordsProcessed);
    }    
}