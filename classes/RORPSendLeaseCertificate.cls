global without sharing class RORPSendLeaseCertificate implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        string AccountId = stp.HexaBPM__SR__r.HexaBPM__Customer__c;
        List<Id> srId = new List<Id>();
        for(Service_Request__c LeaseRegSR: [select id from Service_Request__c where Record_Type_Name__c='Lease_Registration' and Customer__c=:AccountId ]){
            srId.add(LeaseRegSR.id);
        }
        if(stp.HexaBPM__SR__c!=null && AccountId != null && !srId.isEmpty()){
            cls_ApprovalNotificationWithAttachment.callfuturetoSendEmail(srId);
        }
        Return strResult;
    }
}