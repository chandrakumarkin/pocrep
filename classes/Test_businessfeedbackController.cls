@isTest(SeeAllData=false)
public class Test_businessfeedbackController {
 static testMethod void businessfeedbackMethod() {     
     
     Account acc = new Account();
     acc.Name = 'feedback account';
     insert acc;
     
     Contact con = new Contact();
     con.FirstName = 'feedback';
     con.LastName = 'Form';
     con.AccountId = acc.id;
     insert con;     
     
     Feedback_Form__c ff = new Feedback_Form__c();
     ff.Name = 'feedback_form1';
     ff.Account__c = con.AccountId;    
     ff.Contact__c = con.id;
     ff.Ques1_Opt1__c = 12.12;
     ff.Ques4_opt1__c = 12.12;
     ff.Ques2_opt1__c = 12.12;
     ff.Ques3_opt1__c = 12.12;
     ff.Ques4_opt2__c = 12.12;
     ff.Ques1_opt2__c = 12.12;
     ff.Ques1_opt3__c = 12.12;
     ff.Ques4_opt3__c = 12.12;    
     insert ff;
          
     Test.startTest();
     ApexPages.StandardController controller = new ApexPages.StandardController(ff);
     businessfeedbackController feedctrl = new businessfeedbackController(controller);
     PageReference pageRef = Page.Business_Feedback; // Add your VF page Name here
     pageRef.getParameters().put('cid', con.Id);
      pageRef.getParameters().put('eid', 'aru@sd.com');
     Test.setCurrentPage(pageRef);     
     feedctrl.pageLoad();
     feedctrl.SaveMe();
     Test.stopTest();
 }     
      
}