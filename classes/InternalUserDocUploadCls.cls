/**************************************************************************************************************
Author      : DIFC Admin
Date        : 15/Dec/2014
ClassName   : InternalUserDocUploadCls
Description : Controller for custom SR Doc upload page
    
    ---------------------------------------------------------------------------------------------------------------------
    Modification History
    ---------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    ---------------------------------------------------------------------------------------------------------------------             
    V1.0    15/12/2014  Swati         Ceated
    V2.0    25/07/2016  Claude        Added support for SR Docs getting created for Steps
    V3.0    8/06/2018   Mudasir       Added logic for the manual SR Docs
    V3.1    06/06/2021  Veera         Added logic for backend Docs it mandatory to upload
****************************************************************************************************************/
public without sharing class InternalUserDocUploadCls {
    
    public SR_Doc__c objSRDoc               { get; set; }
    public Attachment objAttch              { get; set; }
    public string strSRId                   { get; set; }
    Service_Request__c objSR;
    
    public list<selectoption> lstDocMasters { get; set; }
    public string strSelDocMaster           { get; set; }
    
    private String stepId; // Added by Claude. 25-07-2016
    
    /* Map to hold the Document Master Id as key and DMS Index as value */
    private map<id,string> mapDocDMSIndex = new map<id,string>();   
    private map<String,Object> pageParamsMap;
    
    
    /**
     * Initializes the required members
     */
    private void init(){
        
        stepId = ''; // initialize step ID
        objSRDoc = new SR_Doc__c();
        objAttch = new Attachment();
        objSR = new Service_Request__c();
        lstDocMasters = new list<selectoption>();
        strSelDocMaster = '';
        strSRId = '';
        lstDocMasters.add(new selectoption('','--None--'));
    }
    
    /**
     * Gets the variables set in the URL
     */
    private void getPageParams(){
        
        pageParamsMap = apexpages.currentPage().getParameters();
        
        if(!pageParamsMap.isEmpty()){
            
            strSRId = getPageParam('Id');   
            system.debug('strSRId==>'+strSRId);
            
            stepId = getPageParam('stepId');
            system.debug('Step ID==>'+strSRId);
            
        }
            
    }
    
    /**
     * Checks if the page parameters map has the value
     * @params      key             the key to check
     * @return      String          the value in the URL 
     */
    private String getPageParam(String key){
        return pageParamsMap.containsKey(key) && String.isNotBlank(String.valueOf(pageParamsMap.get(key))) ? String.valueOf(pageParamsMap.get(key)) : '';
    }
    
    /* Class Constructor */
    public InternalUserDocUploadCls(){
        
        init();
        
        getPageParams();
            
        if(String.isNotBlank(strSRId)){
            
            strSRId = strSRId.trim();
            objSRDoc.Service_Request__c = strSRId;
            
            for(Service_Request__c sr:[select id,Name,SR_Template__c from Service_Request__c where id=:strSRId]){
                objSR = sr;
            }
            
            for(Document_Master__c DM:[select id,Name,DMS_Document_Index__c from Document_Master__c where DMS_Document_Index__c!=null limit 49999]){
                
                mapDocDMSIndex.put(DM.id,DM.DMS_Document_Index__c);
            }
        }
    }
    
    public PageReference CancelDocument(){
        return returnToPage();
    }
    
    /* Returns the user back the SR/Step */
    private pagereference returnToPage(){
        
        String pageId = String.isNotBlank(stepId) ? stepId : strSRId;
        
        Pagereference pg = new pagereference('/'+pageId);
        pg.setRedirect(true);
        return pg;
    }
    
    private void resetAttachment(){
        objAttch = new Attachment();
    }
    
    /* Saves the current document */
    public pagereference SaveDocument(){
        
        system.debug('strSelDocMaster==>'+strSelDocMaster);
        system.debug('strSRId==>'+strSRId);
        
        if(objSRDoc.Document_Master__c!=null){
            try{
                if(objSRDoc.Service_Request__c!=null){
                    // V3.0 - Start
                    if(objSRDoc.Document_to_be_Uploaded_by__c =='FOSP' && (objAttch==null || objAttch.Body==null)){
                        setErrorMessage('Please Upload Dococument');
                        //V3.0 - End
                    }else{
                        objSRDoc.Status__c = (objAttch!=null && objAttch.Body!=null) ? 'Uploaded' : 'Pending Upload';
                        
                        objSrDOc.Status__c = objSrDoc.Notify__c ? 'Re-upload' : objSrDOc.Status__c;
                        
                        /* If the SR Doc is getting created on step, add it to the Step */
                        if(String.isNotBlank(stepId)){
                            objSrDOc.Step__c = stepId;  
                        }
                        
                        if(objSRDoc.Document_Master__c!=null && mapDocDMSIndex!=null && mapDocDMSIndex.size()>0 && mapDocDMSIndex.get(objSRDoc.Document_Master__c)!=null){
                            objSRDoc.DMS_Document_Index__c = mapDocDMSIndex.get(objSRDoc.Document_Master__c);
                        }
                        
                        upsert objSRDoc;
                        
                        if(objAttch!=null && objAttch.Body!=null){
                            objAttch.ParentId = objSRDoc.Id;
                            upsert objAttch;
                            // latest attachement id is stored in the sr 
                            objSRDoc.Doc_ID__c = objAttch.Id;
                        }
                        
                        update objSRDoc;
                        
                        if(strSRId!=null && strSRId!=''){
                            
                            Service_Request__c objSR = new Service_Request__c(Id=strSRId);
                            //objSR.Required_Docs_not_Uploaded__c = false;V3.1  
                            
                            objSR.Required_Docs_not_Uploaded__c = (objSrDOc.Is_Not_Required__c) ? false : true; //V3.1  
                            
                            try{
                                RecursiveControlCls.isUpdatedAlready = true;
                                RecursiveControlCls.Do_not_Reevaluate = true;
                                update objSR;
                            }catch(Exception e){
                                setErrorMessage(e.getMessage());
                            }
                        }
                        
                        resetAttachment();
                        return returnToPage();
                    }
                    } else{
                        setErrorMessage('Service Request not linked up with the SR Doc.');
                    }
            } catch(Exception e){
                setErrorMessage(e.getMessage());
            }
            
        } else{
            
            if(strSelDocMaster==null || strSelDocMaster==''){
                setErrorMessage('Please select Document Master.');
            }
        }
        
        resetAttachment();
        
        return null;
    }
    
    /* Sets the Error message in the page */
    private void setErrorMessage(String message){
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,message));
    }
    
}