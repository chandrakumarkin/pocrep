public with sharing class clsCRS_reporting_questions_file {

  public Attachment myfile{get;set;}
  
   private final CRS_Question__c ObjQuestion;

    public clsCRS_reporting_questions_file(ApexPages.StandardController controller) 
    {
      this.ObjQuestion=(CRS_Question__c )controller.getRecord();
    myfile=new Attachment(); 
    }
    
       public PageReference SaveRecord() 
     {
    try
     {
     if(myfile.body!=null)
     {
      Attachment a = new Attachment(parentId = ObjQuestion.id, name=myfile.name, body = myfile.body);
       insert a;
       }
          PageReference UploadPage;
             UploadPage=Page.CRS_Question_View;
             UploadPage.setRedirect(true);
               UploadPage.getParameters().put('id',ObjQuestion.id);
         
         return UploadPage;
       
       }
       catch(Exception  ex)
       {
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()); ApexPages.addMessage(msg);  return null; 
    }
            
       
     }

}