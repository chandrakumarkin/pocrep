/**************************************************************************************************
* Name               : OB_AcctContactTriggerHandler                                                               
* Description        : Trigger Handler for the Account Contact Relation SObject.                                        
* Created Date       : 4th March 2020                                                                *
* Created By         : Leeba Shibu (PWC Digital)                                              
* -------------------------------------------------------------------------------------------------
* Version        Author        Date                           Comment                                                                *
  V1.0           Leeba         04th-March-2020                Created the Handler class        
  V1.1           Mudasir       24th-March-2020                #Handle the population of the Roles for the Contractor and Event Organizer
  V1.2           Sai           28-APR-2020                    #Ticket 18 (DOB Phase 2 Support)
  v1.3           Mudasir       22-April-2020                 #9021 - AccountContactRelation is not updated from IT Services users and RORP users
**************************************************************************************************/
public without sharing class OB_AcctContactTriggerHandler{

    //V1.1 - As this is before insert hence i put it at the begining of the class
    public static void onBeforeInsert(List<AccountContactRelation> TriggerNew){
        Map<id ,AccountContactRelation> accConMapToUpdate= new Map<id , AccountContactRelation>();
        for(AccountContactRelation accConRelRec : TriggerNew){
            if(accConRelRec.AccountId != NULL &&  accConRelRec.contactId != NULL && accConRelRec.Roles ==Null){
                accConMapToUpdate.put(accConRelRec.contactId ,accConRelRec);
            }
        }
        if(accConMapToUpdate.size() > 0){
            //If the role is not populated for the contractor or event organizer then populate the same.
            populateRole(accConMapToUpdate);
        }
    }
    
    public static void onAfterInsert(list<AccountContactRelation> TriggerNew){
        ShareAccounts(TriggerNew);
        shareApplications(TriggerNew,null,null,null,true);
        OB_AcctContactTriggerHelper.CreateHistoryRecords(TriggerNew,null,true);//v1.2
    }
    public static void ShareAccounts(list<AccountContactRelation> TriggerNew){
        set<string> setContactId = new set<string>();
        map<string,string> MapContactUser = new map<string,string>();
        list<AccountShare> lstAccShare = new list<AccountShare>();
        for(AccountContactRelation acr:TriggerNew){
            if(acr.AccountId!=null && acr.ContactId!=null && acr.IsDirect==false){
                setContactId.add(acr.ContactId);
            }
        }
        if(setContactId.size()>0){
            for(User usr:[Select Id,ContactId from User where IsActive=true and ContactId IN:setContactId and Profile.UserLicense.Name = 'Customer Community Plus Login' ]){
                MapContactUser.put(usr.ContactId,usr.Id);
            }
        }
        for(AccountContactRelation acr:TriggerNew){
            if(acr.AccountId!=null && acr.ContactId!=null && acr.IsDirect==false && MapContactUser.get(acr.ContactId)!=null){
                AccountShare accshare = new AccountShare();
                accshare.AccountId = acr.AccountId;
                accshare.UserOrGroupId = MapContactUser.get(acr.ContactId);
                accshare.AccountAccessLevel = 'Read';
                accshare.OpportunityAccessLevel = 'None';
                //accshare.RowCause = 'ImplicitParent';
                lstAccShare.add(accshare);
            }
        }
        if(lstAccShare.size()>0)
            insert lstAccShare;
    }
    public static void onAfterUpdate(list<AccountContactRelation> TriggerOld,list<AccountContactRelation> TriggerNew,map<Id,AccountContactRelation> TriggerOldMap,map<Id,AccountContactRelation> TriggerNewMap){
        removeshareApplications(TriggerNew,TriggerNewMap,TriggerOld,TriggerOldMap,false);
        shareApplications(TriggerNew,TriggerNewMap,TriggerOld,TriggerOldMap,false);
        OB_AcctContactTriggerHelper.CreateHistoryRecords(TriggerNew,TriggerOldMap,false);//v1.2
        OB_AcctContactTriggerHelper.UpdateUserContactRole(TriggerNewMap);
    }
    
    public static void onBeforeDelete(list<AccountContactRelation> TriggerOld){
        removeshareApplications(null,null,TriggerOld,null,true);
    }  
    
    //Creating Sharing Records for Service Requests and Steps on creation or updation of Account Contact Relation record.
    public static void shareApplications(list<AccountContactRelation> TriggerNew,
                                      map<Id,AccountContactRelation> TriggerNewMap,
                                      list<AccountContactRelation> TriggerOld,
                                      map<Id,AccountContactRelation> TriggerOldMap,
                                      boolean isInsert){
        system.debug('inside share');
        String AccId;        
        String ConId;
        String UserId;
        String strAccessLevel; 
        String SharingLevel;        
        List<HexaBPM__Service_Request__c>objHexaSRList = new List<HexaBPM__Service_Request__c>();
        List<Service_Request__c> objSRList = new List<Service_Request__c>();
        List<HexaBPM__Step__c>objHexaStepList = new List<HexaBPM__Step__c>();        
        List<Step__c>objStepList = new List<Step__c>();
        List<HexaBPM__Service_Request__Share> lstHexaSRShares = new List<HexaBPM__Service_Request__Share>();
        List<HexaBPM__Step__Share> lstHexaStepShares = new List<HexaBPM__Step__Share>();
        List<Service_Request__Share> lstSRShares = new List<Service_Request__Share>();
        List<Step__Share> lstStepShares = new List<Step__Share>();
        List<String> lstroles = new List<String>();
        
        //Get the Account and Contact Id from Account Contact Relationship record
        for(AccountContactRelation acr:TriggerNew){
            if(isInsert){
                if(acr.IsActive==true && acr.IsDirect==false){
                    AccId = acr.AccountId;
                    ConId = acr.ContactId;
                    if(acr.Access_Level__c=='Read/Write')
                        strAccessLevel = 'Edit';
                    else
                        strAccessLevel = 'Read';
                
                    if(acr.Roles !=null){   
                        lstroles = acr.Roles.split(';'); 
                        system.debug('lstroles==>'+lstroles);
                    }
                
                }
                
            }else{
                if(acr.Isactive != TriggerOldMap.get(acr.id).Isactive && acr.IsDirect==false){
                    if(acr.Isactive){
                        AccId = acr.AccountId;
                        ConId = acr.ContactId;
                        if(acr.Access_Level__c=='Read/Write')
                            strAccessLevel = 'Edit';
                        else
                            strAccessLevel = 'Read';
                        if(acr.Roles !=null){   
                            lstroles = acr.Roles.split(';'); 
                            system.debug('lstroles==>'+lstroles);
                        }
                    }
                }else{
                    if(acr.Roles != TriggerOldMap.get(acr.id).Roles && acr.Roles!=null && acr.IsDirect==false){
                        AccId = acr.AccountId;
                        ConId = acr.ContactId;
                        lstroles = acr.Roles.split(';'); 
                        system.debug('lstroles==>'+lstroles);
                        if(acr.Access_Level__c=='Read/Write')
                            strAccessLevel = 'Edit';
                        else
                            strAccessLevel = 'Read';
                       
                    }
                }
            
            }
        }
        
        if(ConId !=null){
            UserId = getUserId(ConId);
        }
        
        if(AccId !=null && !lstroles.isEmpty()){                  
            objHexaSRList = getHexaServiceRequest(AccId,lstroles,UserId);
            objSRList = getServiceRequest(AccId,lstroles,UserId);
        }
        
        if(objHexaSRList !=null && objHexaSRList.size()>0){
            objHexaStepList = getHexaStep(objHexaSRList);
        }
        
        if(objSRList !=null && objSRList.size()>0){
            objStepList = getStep(objSRList);
        }
        
        // Creating the Sharing record for HexaBPM__Service_Request__c records
        if(objHexaSRList !=null && objHexaSRList.size()>0 && UserId!=null){
            for(HexaBPM__Service_Request__c objSR  : objHexaSRList){
                HexaBPM__Service_Request__Share objSRShare = new HexaBPM__Service_Request__Share();
                objSRShare.ParentId = objSR.Id;
                objSRShare.AccessLevel = strAccessLevel;
                objSRShare.UserOrGroupId = UserId;
                lstHexaSRShares.add(objSRShare);
                system.debug('list of Share records'+lstHexaSRShares);
            }            
        }
        
        // Creating the Sharing record for HexaBPM__Step__c records related to HexaBPM__Service_Request__c records
        if(objHexaStepList !=null && objHexaStepList.size()>0 && UserId!=null){
            for(HexaBPM__Step__c objStep  : objHexaStepList){
                HexaBPM__Step__Share objStepShare = new HexaBPM__Step__Share();
                objStepShare.ParentId = objStep.Id;
                objStepShare.AccessLevel = strAccessLevel;
                objStepShare.UserOrGroupId = UserId;
                lstHexaStepShares.add(objStepShare);
                system.debug('list of Share records'+lstHexaStepShares);
            }            
        }
        
        // Creating the Sharing record for Service_Request__c records
        if(objSRList !=null && objSRList.size()>0 && UserId!=null){
            for(Service_Request__c objSR  : objSRList){
                Service_Request__Share objSRShare1 = new Service_Request__Share();
                objSRShare1.ParentId = objSR.Id;
                objSRShare1.AccessLevel = strAccessLevel;
                objSRShare1.UserOrGroupId = UserId;
                lstSRShares.add(objSRShare1);
                system.debug('list of Share records'+lstSRShares);
            }            
        }
        
        // Creating the Sharing record for Step__c records related to Service_Request__c records
        if(objStepList !=null && objStepList.size()>0 && UserId!=null){
            for(Step__c objStep  : objStepList){
                Step__Share objStepShare1 = new Step__Share();
                objStepShare1.ParentId = objStep.Id;
                objStepShare1.AccessLevel = strAccessLevel;
                objStepShare1.UserOrGroupId = UserId;
                lstStepShares.add(objStepShare1);
                system.debug('list of Share records'+lstStepShares);
            }            
        }
        
        if(lstHexaSRShares.size() > 0 )
        {
            insert lstHexaSRShares;                    
        }
        if(lstHexaStepShares.size() > 0 )
        {
            insert lstHexaStepShares;                    
        }
        if(lstSRShares.size() > 0 )
        {
            insert lstSRShares;                    
        }
        if(lstStepShares.size() > 0 )
        {
            insert lstStepShares;                    
        }
         
                
        
        
    }
    
    //Deleting Sharing Records for Service Requests and Steps on deletion or updation of Account Contact Relation record.  
    public static void removeshareApplications(list<AccountContactRelation> TriggerNew,
                                      map<Id,AccountContactRelation> TriggerNewMap,
                                      list<AccountContactRelation> TriggerOld,
                                      map<Id,AccountContactRelation> TriggerOldMap,
                                      boolean isDelete){
        system.debug('inside remove');
        String AccId;
        String ConId;
        String UserId;
        List<HexaBPM__Service_Request__c> objHexaSRList = new List<HexaBPM__Service_Request__c>();
        List<HexaBPM__Step__c>objHexaStepList = new List<HexaBPM__Step__c>();
        List<Service_Request__c>objSRList = new List<Service_Request__c>();
        List<Step__c>objStepList = new List<Step__c>();
        List<HexaBPM__Service_Request__Share> lstdelHexaSRShares = new List<HexaBPM__Service_Request__Share>();
        List<HexaBPM__Step__Share> lstdelHexaStepShares = new List<HexaBPM__Step__Share>();
        List<Service_Request__Share> lstdelSRShares = new List<Service_Request__Share>();
        List<Step__Share> lstdelStepShares = new List<Step__Share>();
        List<String> lstroles = new List<String>();
    
        for(AccountContactRelation acr:TriggerOld){
            if(isDelete){
                system.debug('inside delete');             
                AccId = acr.AccountId;
                ConId = acr.ContactId;
            }else{
                if(acr.Isactive != TriggerNewMap.get(acr.id).Isactive && acr.IsDirect==false){
                    if(!acr.Isactive){
                        AccId = acr.AccountId;
                        ConId = acr.ContactId;
                    }
                }else{
                    if(acr.Roles != TriggerNewMap.get(acr.id).Roles && acr.IsDirect==false){
                        AccId = acr.AccountId;
                        ConId = acr.ContactId;
                        if(acr.Roles != null)
                            lstroles = acr.Roles.split(';');
                        system.debug('lstroles==>'+lstroles);
                    }
                }
            
            }
        }
        
       
        
        if(ConId !=null){
            UserId = getUserId(ConId);
        }
        
         if(AccId != null && !lstroles.isEmpty()){
            objHexaSRList = getHexaServiceRequest(AccId,lstroles,UserId);
            objSRList = getServiceRequest(AccId,lstroles,UserId);
        }else{
            if(AccId !=null){
                objHexaSRList = getHexaServiceRequest1(AccId,UserId);
                objSRList = getServiceRequest1(AccId,UserId);           
            }
        }
        
        if(objHexaSRList !=null && objHexaSRList.size()>0){
            objHexaStepList = getHexaStep(objHexaSRList);
        }
        
        if(objSRList !=null && objSRList.size()>0){
            objStepList = getStep(objSRList);
        }
        
        if(objHexaSRList !=null && objHexaSRList.size()>0 && UserId!=null){            
            for(HexaBPM__Service_Request__Share objSRShare :[Select Id from HexaBPM__Service_Request__Share where ParentId IN :objHexaSRList
                                                            AND UserOrGroupId = :UserId ]){
                lstdelHexaSRShares.add(objSRShare);
            }
        
        }
        
        if(objHexaStepList !=null && objHexaStepList.size()>0 && UserId!=null){            
            for(HexaBPM__Step__Share objStepShare :[Select Id from HexaBPM__Step__Share where ParentId IN :objHexaStepList AND UserOrGroupId = :UserId ]){
                lstdelHexaStepShares.add(objStepShare);
            }
        
        }
                
        if(lstdelHexaSRShares.size() > 0 ){
            delete lstdelHexaSRShares;                    
        }
        if(lstdelHexaStepShares.size() > 0 ){
            delete lstdelHexaStepShares;                    
        }
    }
    
    public static List<HexaBPM__Service_Request__c> getHexaServiceRequest(String accId,List<String>lstroles,String userId){
        
            return [SELECT id,name                        
                    FROM HexaBPM__Service_Request__c
                    WHERE HexaBPM__Customer__c =:accId AND HexaBPM__SR_Template__r.HexaBPM__Menu__c IN :lstroles AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__Is_Rejected__c =false AND HexaBPM__IsCancelled__c = false AND OwnerId !=:userId];
        
        
    }
    
    public static List<HexaBPM__Service_Request__c> getHexaServiceRequest1(String accId,String userId){
        
            return [SELECT id,name                        
                    FROM HexaBPM__Service_Request__c
                    WHERE HexaBPM__Customer__c =:accId AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__Is_Rejected__c =false AND HexaBPM__IsCancelled__c = false AND OwnerId !=:userId];
        
        
    }
    
    public static List<Service_Request__c> getServiceRequest(String accId,List<String>lstroles,String userId){
        return [SELECT id,name                        
                FROM Service_Request__c
                WHERE Customer__c = :accId AND SR_Template__r.Menu__c IN :lstroles AND isClosedStatus__c = false AND Is_Rejected__c =false AND IsCancelled__c = false AND OwnerId !=:userId];
    }
    
    public static List<Service_Request__c> getServiceRequest1(String accId,String userId){
        return [SELECT id,name                        
                FROM Service_Request__c
                WHERE Customer__c = :accId AND isClosedStatus__c = false AND Is_Rejected__c =false AND IsCancelled__c = false AND OwnerId !=:userId];
    }
    
    public static List<HexaBPM__Step__c> getHexaStep(List<HexaBPM__Service_Request__c>HexaSRList){
        return [SELECT id,name                        
                FROM HexaBPM__Step__c
                WHERE HexaBPM__SR__c IN :HexaSRList  AND HexaBPM__Status_Type__c != 'END' AND Is_Client_Step__c = true];
    }
    
    public static List<Step__c> getStep(List<Service_Request__c>SRList){
        return [SELECT id,name                        
                FROM Step__c
                WHERE SR__c IN :SRList AND Status_Type__c!= 'END' AND Assigned_to_client__c  = true];
    }
    
    public static String getUserId(String conId){
        string strUserId;
        for(User user :[select Id from User where ContactId=:conId AND Profile.UserLicense.Name = 'Customer Community Plus Login' and IsActive=true]){
            strUserId = user.Id;
        }
        if(strUserId!=null)
            return strUserId;
        else
            return null;
    }    
    public static void populateRole(Map<id, AccountContactRelation> accConRelMapToUpdate){
        //If the role is not populated for the contractor or event organizer then populate the same.
        for(Contact con : [Select Recordtype.DeveloperName,id,Contractor__c,Contractor__r.RecordType.DeveloperName,Account.RecordType.DeveloperName,Role__c from Contact where Id IN:accConRelMapToUpdate.keyset()]){
            if(con.Contractor__r.RecordType.DeveloperName =='Contractor_Account' || con.Account.RecordType.DeveloperName =='Contractor_Account' || con.Role__c=='Fit-Out Services'){
                accConRelMapToUpdate.get(con.id).Roles ='Fit-Out Services';
                accConRelMapToUpdate.get(con.id).Access_Level__c='Read/Write';
            }else if(con.Contractor__r.RecordType.DeveloperName=='Event_Account' || con.Account.RecordType.DeveloperName =='Event_Account' || con.Role__c=='Event Services'){
                accConRelMapToUpdate.get(con.id).Roles  ='Event Services';
                accConRelMapToUpdate.get(con.id).Access_Level__c='Read/Write';
            }
            else if(con.Role__c=='IT Services' || con.Role__c=='Property Services'){//V1.3
                accConRelMapToUpdate.get(con.id).Roles  = con.Role__c;
                accConRelMapToUpdate.get(con.id).Access_Level__c='Read/Write';
            }
            else if(con.Recordtype.DeveloperName == 'Portal_User'){   //V1.2
                accConRelMapToUpdate.get(con.id).Roles  = con.Role__c;
                accConRelMapToUpdate.get(con.id).Access_Level__c='Read/Write';
            }
            /*else{
                accConRelMapToUpdate.get(con.id).Roles  = Label.Community_Roles;
                accConRelMapToUpdate.get(con.id).Access_Level__c='Read/Write';
            }*/
        }
    }
    
   
}