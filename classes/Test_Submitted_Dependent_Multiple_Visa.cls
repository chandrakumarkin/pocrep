@isTest(seeAllData=false)
public class Test_Submitted_Dependent_Multiple_Visa {
	 static testmethod void newDependentMultipleVisa1() {
        test.startTest();
        Map<string,string> mapRecordTypeIds = new Map<string,string>();
        
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('License_Renewal','Notification_of_Personal_Data_Operations','Annual_Return','Annual_Reporting','Add_or_Remove_Auditor','Allotment_of_Shares_Membership_Interest','Add_Audited_Accounts','Notice_of_Amemdment_of_AOA')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        insert objContact;
        
        Id GSContactId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        Contact objContact1 = new Contact();
        objContact1.firstname = 'Test Contact2';
        objContact1.lastname = 'Test Contact2';
        objContact1.accountId = objAccount.id;
        objContact1.recordTypeId = GSContactId;
        objContact1.Email = 'test2@difcportal.com';
        insert objContact1;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Consulate_or_Embassy_Letter';
        objTemplate.SR_RecordType_API_Name__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Menutext__c = 'Consulate_or_Embassy_Letter';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        Document_Master__c objDocMaster = new Document_Master__c();
        objDocMaster.Name = 'Required Docs to Upload';
        insert objDocMaster;
        
        SR_Template_Docs__c objTempDocs = new SR_Template_Docs__c();
        objTempDocs.SR_Template__c = objTemplate.Id;
        objTempDocs.Document_Master__c = objDocMaster.Id;
        objTempDocs.On_Submit__c = true;
        objTempDocs.Generate_Document__c = true;
        objTempDocs.SR_Template_Docs_Condition__c = 'Service_Request__c->Name#!=#DRIVER';
        insert objTempDocs;
        
        Lookup__c nat1 = new Lookup__c(Type__c='Nationality',Name='Afghanistan');
        insert nat1;
        Lookup__c nat2 = new Lookup__c(Type__c='Nationality',Name='India');
        insert nat2;
        Lookup__c nat3 = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat3;
        
        string issueFineId = '';
        for(RecordType rectyp:[select id from RecordType where DeveloperName='Consulate_or_Embassy_Letter' and sObjectType='Service_Request__c']){
            issueFineId = rectyp.Id;
        }
        SR_Status__c objSRStatus1 = new SR_Status__c();
        objSRStatus1.Name = 'Submitted';
        objSRStatus1.Code__c = 'Submitted';
        insert objSRStatus1;
         
        Service_Request__c objSR = new Service_Request__c();
        objSR.Express_Service__c = true;
        objSR.Customer__r = objAccount;
        objSR.Customer__c = objAccount.id;
        objSR.RecordTypeId = issueFineId;
        objSR.submitted_date__c = date.today();
        objSR.Country__c = 'India; ';
        objSR.Country__c = objSR.Country__c + 'Afghanistan; ';
        objSR.Country__c = objSR.Country__c + 'Pakistan; ';
        objSR.contact__c = objContact1.id;
        objSR.Sponsor__c = objContact1.Id;
        objSR.Express_Service__c = false;
        objSR.External_SR_Status__c = objSRStatus1.Id;
        objSR.Internal_SR_Status__c = objSRStatus1.Id;
        insert objSR;
         
        system.assertEquals(objSRStatus1.Id, objSR.External_SR_Status__c);
        SR_Status__c objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'Draft';
        objSRStatus.Code__c = 'Draft';
        insert objSRStatus;
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Express_Service__c = false;
        objSR1.Linked_SR__c = objSR.Id;
        objSR1.External_SR_Status__c = objSRStatus.Id;
        objSR1.Internal_SR_Status__c = objSRStatus.Id;
        objSR1.RecordTypeId = issueFineId;
        insert objSR1;
         
        PageReference pageRef = Page.New_Dependent_multiple_Visa_Request;
        Test.setCurrentPage(pageRef);
        
         ApexPages.StandardController sc                       = new ApexPages.StandardController(objSR);
         cls_Submitted_Dependent_Multiple_Visa_SR thisInstance = new cls_Submitted_Dependent_Multiple_Visa_SR(sc);
         thisInstance.SRData =objSR; 
         thisInstance.LinkedDraftSrs();
         thisInstance.add();
         test.stopTest(); 
      }
    
}