/*********************OB_OpportunityTriggerHandler********************/
/***
***Author:  Rajil
***Date:    15-01-2020
***Purpose: TriggerHandler used for Opportunity Object to populate the opportunity on Application
***/
public class OB_OpportunityTriggerHandler {
    public static void Execute_AI(List<Opportunity> lstOpportunity){
        populateOpportunity(lstOpportunity);
    }
    public static void populateOpportunity(List<Opportunity> lstOpportunity){
        map<string,string> MapAccountIds = new map<string,string>();
        for(Opportunity opp : lstOpportunity){
            if(opp.AccountId!=null)
                MapAccountIds.put(opp.AccountId,opp.Id);
        }
        if(MapAccountIds.size()>0){
            list<HexaBPM__Service_Request__c> lstSR = new list<HexaBPM__Service_Request__c>(); 
            for(HexaBPM__Service_Request__c objSR : [select HexaBPM__Customer__c from HexaBPM__Service_Request__c where HexaBPM__Customer__c IN:MapAccountIds.KeySet() and Opportunity__c=null and HexaBPM__IsCancelled__c=false and HexaBPM__Is_Rejected__c=false and HexaBPM__IsClosedStatus__c=false]){
                objSR.Opportunity__c = MapAccountIds.get(objSR.HexaBPM__Customer__c);
                lstSR.add(objSR);
            }
            if(lstSR.size()>0)
                update lstSR;
        }
    }
}