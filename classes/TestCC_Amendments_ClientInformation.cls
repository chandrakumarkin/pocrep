@isTest(seeAllData=false)
private class TestCC_Amendments_ClientInformation {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        Lookup__c nat = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat;
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.Authorized_Share_Capital__c = 500000;
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';        
        insert objContact;
        
        Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Corporate Communication'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        insert objRel;
        
        Relationship__c objRel1 = new Relationship__c();
        objRel1.Active__c = true;
        objRel1.Subject_Account__c = objAccount.Id;
        objRel1.Object_Contact__c = objContact.Id;
        objRel1.Relationship_Type__c = 'Has Corporate Communication'; 
        objRel1.Start_Date__c = system.today();
        objRel1.Push_to_SAP__c = true;
        insert objRel1;
        
        
    
        
        
        
        
        Account_Share_Detail__c ASD= new Account_Share_Detail__c();
        ASD.name='test';
        ASD.Account__c=objAccount.Id;
        insert ASD;
        
        Shareholder_Detail__c SD= new Shareholder_Detail__c();
        SD.Account__c= objAccount.Id;
        SD.Account_Share__c=ASD.id;
        SD.No_of_Shares__c=10;
        SD.Relationship__c=objRel.id;
        insert SD;
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971232342345';
        objSR.legal_structures__c ='LLC';
        objSR.currency_list__c = 'US Dollar';
        objSR.Share_Capital_Membership_Interest__c = 50000;
        objSR.No_of_Authorized_Share_Membership_Int__c = 560;
        objSR.Filled_Page_Ids__c='test1;test2';
        
        insert objSR;
        
        Step__c stp= new Step__c();
        stp.SR__c=objSR.id;
        
        insert stp;
        
        stp.SR__r=objSR;
        
        CC_AmendmentsCls.Process_Sale_Transfer_MembershipInterest(stp);
        CC_AmendmentsCls.Process_ChangeAmendments(stp);
        CC_AmendmentsCls.AuthSignFill(stp);
        
        Amendment__c amend =new Amendment__c();
        amend.ServiceRequest__c=objSR.id;
        amend.Amendment_Type__c = 'Individual';
        amend.Title_new__c = 'Mrs.';
        amend.Family_Name__c = 'Mrs.';
        amend.Given_Name__c = 'Mrs.';
        amend.Date_of_Birth__c=Date.newinstance(1980,10,17);
        amend.Place_of_Birth__c = 'Pakistan';
        amend.Passport_No__c = 'ASD122323';
        amend.Nationality__c = nat.id;
        amend.Nationality_list__c = 'Pakistan';
        amend.Phone__c = '+971562019803';
        amend.Person_Email__c = 'abc@nsigulf.com';
        amend.First_Name_Arabic__c ='AbcTest';
        amend.Occupation__c='Developer';
        amend.Apt_or_Villa_No__c='villa';
        amend.Building_Name__c='building';
        amend.Street__c='street';
        amend.PO_Box__c = '45231435';
        amend.Permanent_Native_City__c='Karachi';
        amend.Emirate_State__c='Emairate';
        amend.Post_Code__c='2322';
        amend.Emirate__c='Dubai';
        amend.Permanent_Native_Country__c = 'Pakistan';
        amend.Is_Designated_Member__c = true;
        amend.Company_Name__c = 'Company';
        amend.Former_Name__c = 'Former';
        amend.Registration_Date__c = Date.newinstance(1980,10,17);
        amend.Place_of_Registration__c = 'Registration';
        amend.Relationship_Type__c='CEO';
        amend.Status__c='Draft';
        insert amend;
        
        CC_ClientInformationCls.ReplicateClientInfoRelationships(stp);
        CC_ClientInformationCls.setContactLimit();
        CC_ClientInformationCls.checkContactLimit('srId');
        CC_ClientInformationCls.validateRequiredContacts();
        CC_ClientInformationCls.setAdditionalContactLimits();
        CC_ClientInformationCls.setRequiredContacts();

        CC_ClientInformationCls.getMissingContactTypes();
       
        
        CC_ClientInformationCls.populateMaps(objSR.id);
        delete amend;
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Client_Information';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Client_Information';
        insert objPageFlow;
        
        Page__c objPage = new Page__c();
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        insert objPage;
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        Section__c ButtonSec = new Section__c();
        
        ButtonSec.Page__c = objPage.Id;
        ButtonSec.Name = 'Page1 Section2';
        ButtonSec.Section_Description__c = 'Test';
        ButtonSec.Default_Rendering__c = true;
        ButtonSec.Layout__c = '1';
        ButtonSec.Order__c = 2;
        ButtonSec.Section_Title__c = 'Test Type';
        ButtonSec.Section_Type__c = 'CommandButtonSection';
        
        insert ButtonSec;
        
        Section_Detail__c NextBtn = new Section_Detail__c();
        
        NextBtn.Section__c = ButtonSec.Id;
        NextBtn.Order__c = 2;
        NextBtn.Render_By_Default__c = true;
        NextBtn.Component_Type__c = 'Command Button';
        NextBtn.Navigation_Directions__c = 'Forward';
        NextBtn.Button_Location__c = 'Top';
        NextBtn.Button_Position__c = 'Left';
        NextBtn.Component_Label__c = 'Client';
        
        insert NextBtn;
        
        ApexPages.CurrentPage().getparameters().put('Id', objSR.id);
        ApexPages.currentPage().getParameters().put('PageId',objPage.id);
        ApexPages.currentPage().getParameters().put('FlowId',objPageFlow.id);
        
        ROC_StatementOfUndertakingCls rocStatementCls = new ROC_StatementOfUndertakingCls();
        
        rocStatementCls.strActionId = NextBtn.Id;
        
        rocStatementCls.getSalesforceUrl();
        
        rocStatementCls.getDyncPgMainPB();
        
        rocStatementCls.objSr.Statement_of_Undertaking__c = false;
        
        rocStatementCls.commitRecord();
        
        rocStatementCls.goTopage();
        
        rocStatementCls.objSr.Statement_of_Undertaking__c = true;
        
        rocStatementCls.commitRecord();
        
        rocStatementCls.goTopage();
        
        rocStatementCls.Cancel_Request();
        
        Cls_ProcessFlowComponents processFlowMainClass = new Cls_ProcessFlowComponents();
        
        processFlowMainClass.goTopage();
        
        processFlowMainClass.CommitRecord();
        
        Test.startTest();
        
        PageReference pageRef = Page.ClientInformation; 
        
        Test.setCurrentPageReference(pageRef);
        
        ApexPages.CurrentPage().getparameters().put('Id', objSR.id);
        ApexPages.currentPage().getParameters().put('PageId',objPage.id);
        ApexPages.currentPage().getParameters().put('FlowId',objPageFlow.id);
        
        Client_Information_Cls cls1= new Client_Information_Cls();
        
        cls1.SRdocume();
        cls1.FetchExistingRelationships();
        cls1.disableForm();
        cls1.strActionId = NextBtn.Id;
        
        System.debug(cls1.listIndex);
        
        System.debug(cls1.contactRecord);
        
        System.debug(cls1.strSRId);
        
        cls1.enableClientForm();
        
        cls1.selectedRelType = 'IT contact';
        
        cls1.addContact();
        
        cls1.selectedRelType = 'IT contact';
        
        cls1.contactRecord.Given_Name__c = 'Test';
        cls1.contactRecord.Family_Name__c = 'First Name';
        cls1.contactRecord.Passport_No__c = 'ABDCD90312';
        cls1.contactRecord.Nationality_list__c = 'Philippines';
        cls1.contactRecord.Person_Email__c = 'claude@nsigulf.test.ae';
        
        cls1.saveDetails();
        
        cls1.contactRecord.Mobile__c = '+97112345678';
        cls1.contactRecord.Relationship_Type__c = cls1.selectedRelType;
        
        cls1.saveDetails();
        
        cls1.listIndex = 0;
        
        cls1.editContact();
        
        cls1.contactRecord.Gender__c= 'Male';
        cls1.contactRecord.Person_Email__c= 'test@gmail.com';
        cls1.saveDetails();
        
        cls1.listIndex = 0;
        
        cls1.deleteContact();
        
        cls1.goTopage();
        
        cls1.DynamicButtonAction();
        
         DPFSideBarComponentController cls2= new  DPFSideBarComponentController();
         cls2.getLstPageWrap();
         
        Test.stopTest();
    }
}