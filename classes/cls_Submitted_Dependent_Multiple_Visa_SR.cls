public with sharing class cls_Submitted_Dependent_Multiple_Visa_SR {

 public Service_Request__c SRData{get;set;}
 map<string,id> mapOfStatus = new map<string,id>();
 
    public cls_Submitted_Dependent_Multiple_Visa_SR(ApexPages.StandardController controller) 
    {
               List<String> fields = new List<String> {'External_SR_Status__c','Internal_SR_Status__c','External_Status_Name__c','Submitted_Date__c','Submitted_DateTime__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        
         SRData=(Service_Request__c )controller.getRecord();
         
            /* Get all available SR Statuses */
        for(SR_Status__c obj : [select id,name from SR_Status__c where code__c!=null and name='Submitted']) mapOfStatus.put(obj.name, obj.id);
        
        
    }
    
  public void LinkedDraftSrs()
     {
    
        //Change Linked SR status to Submitted from Draft 
             List<Service_Request__c> ListSer=new List<Service_Request__c>();
             
             for(Service_Request__c ObSr:[select id,Name,First_Name__c,Last_Name__c,Relation__c,External_Status_Name__c,External_SR_Status__c,Internal_SR_Status__c from Service_Request__c where Linked_SR__c!=null and  Linked_SR__c=:SRData.id])
             {
                 if(SRData.External_Status_Name__c=='Submitted' && ObSr.External_Status_Name__c=='Draft')
                 {ObSr.Internal_SR_Status__c = mapOfStatus.get('Submitted');ObSr.External_SR_Status__c = mapOfStatus.get('Submitted');ObSr.Submitted_Date__c=SRData.Submitted_Date__c;ObSr.Submitted_DateTime__c=SRData.Submitted_DateTime__c;ListSer.add(ObSr);
                     
                 }
                 
             }
             System.debug('======ListSer===>'+ListSer);
             if(!ListSer.isEmpty())
             {
                  update ListSer;
                  
                  //Change Change price line iteam price status to cancelled and Blocked for related SR;s 
            List<id> ListLinkedPrices=new List<id>();List<SR_Price_Item__c> ListUpdatePrice=new List<SR_Price_Item__c>();
            
               for(SR_Price_Item__c ObjPrice:[select Linked_SR_Price_Item__c,id from SR_Price_Item__c where ServiceRequest__c=:SRData.id and Status__c='Blocked'])
               {
                      ObjPrice.Status__c='Cancelled';ListLinkedPrices.add(ObjPrice.Linked_SR_Price_Item__c);ListUpdatePrice.add(ObjPrice);
               }
                
                
                for(SR_Price_Item__c ObjPrice:[select id,Price__c,ServiceRequest__c,Product__c,Status__c,Pricing_Line__c from SR_Price_Item__c where Status__c='Added' and  ServiceRequest__r.Linked_SR__c=:SRData.id and id in : ListLinkedPrices])
                 {
                    ObjPrice.Status__c='Blocked';ListUpdatePrice.add(ObjPrice);
                    
                 }
            if(!ListUpdatePrice.isEmpty())
             update ListUpdatePrice;
                          System.debug('======ListUpdatePrice===>'+ListUpdatePrice);
                          
             }
     }
    public void add(){
        integer i =0;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
         i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
        i = i +1;
    }

}