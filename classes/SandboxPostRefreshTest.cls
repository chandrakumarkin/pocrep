/**
* Author: Shoaib Tariq
* Date: 10/04/2020
* Description: Positive, Negative Test Cases for SandboxPostRefresh Class.
**/
@isTest
public class SandboxPostRefreshTest {
    // Test method for Positive value Sobject Records
    private static testmethod void testSobjectPositiveTestCase(){
        //Insert Lead
        Lead newLead              = TestUtils.createLead();
        insert newLead;

        //Insert Account
        Account newAccount        = TestUtils.createAccount();
        
        //Insert Contact
        Contact newContact        = TestUtils.createContact(newAccount.Id);
        // Assert values of email fields before running the test case
        // Assert Lead Email Fields
       
        Test.startTest();

        Test.testSandboxPostCopyScript(
            new SandboxPostRefresh(), UserInfo.getOrganizationId(),
                UserInfo.getOrganizationId(), UserInfo.getOrganizationName());
        Test.stopTest();
        // Assert email fields value after running the test cases
    }

}