/***********************************************
*Class Name :UpdateBDKPIs 
*Description: Calculating the meeting count for report
*Version: v1.1 07-10-2018 KPI changes added #5818

 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No           Date            Updated By       Description
 ----------------------------------------------------------------------------------------              
 V 1.2         03-01-2021       Arun Singh       Updated for Corporate Service Provider #13026
 
**********************************************************************************************/

global class UpdateBDKPIs implements Database.Batchable<sObject>{
   global final String Query;
  global boolean Year_Last;
   global UpdateBDKPIs (String q,boolean Year_Last){
      Query=q; 
         this.Year_Last=Year_Last;
   }
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }
   
   public static  Map<id,List<Task>> ListofTask(List<task> ListofTasks)
   {
       System.debug('ListofTasks--->'+ListofTasks);
          //Count for task
       Map<id,List<Task>> MapAccountIDTasks=new Map<id,List<Task>>();
       
       for(Task ObjTask:ListofTasks)
       {
            
                List<Task> TempTask=new List<Task>();
                
                if(MapAccountIDTasks.containsKey(ObjTask.Accountid))
                TempTask=MapAccountIDTasks.get(ObjTask.Accountid);
                
                TempTask.add(ObjTask);
                MapAccountIDTasks.put(ObjTask.Accountid,TempTask);
            
        }
        return MapAccountIDTasks;
       
   }
   
      
   public  static   Map<id,List<Event>> ListofEvents(List<Event> ListofEvent)
   {
       System.debug('ListofEvent--->'+ListofEvent);
        Map<id,List<Event>> mapAccountIDEvent=new Map<id,List<Event>>();
     for(Event ObjEvent:ListofEvent)
     {
           
                List<Event>  tempEvent=new List<Event>();
                
                if(mapAccountIDEvent.containsKey(ObjEvent.Accountid))
                   tempEvent=mapAccountIDEvent.get(ObjEvent.Accountid);
                
                tempEvent.add(ObjEvent);
                mapAccountIDEvent.put(ObjEvent.Accountid,tempEvent);
            
        }
        return mapAccountIDEvent;
    }
   global void execute(Database.BatchableContext BC, List<Account> ListAccount){
       // re set values and get task details 
        List<Id> ListAccountId=new List<id>();
        for(account ObjAccount : ListAccount)
        {
            ListAccountId.add(ObjAccount.id);
            ObjAccount.Total_Meeting_Count__c=0;
            ObjAccount.Total_Interaction_Count__c=0;
        }
       for(Account ObjAccount :ListAccount){
            ObjAccount.Same_Management__c=null;

            string Act='';
            for(License_Activity__c ObjActiv:ObjAccount.License_Activities__r)
            {
                Act+=ObjActiv.Formula_Activity_Name_Eng__c+';';
            }

              //Update Account Activity details name 
            ObjAccount.License_Activity_Details__c=Act;
            ObjAccount.Corporate_Service_Provider__c=Act.contains('Corporate Service Provider');//Added for CSP
            
            
            //Account and parent account 
            for(Relationship__c ObjRel:ObjAccount.Primary_Account__r)
            {
                if(ObjRel.Object_Account__c!=ObjAccount.Id) 
                ObjAccount.Same_Management__c=ObjRel.Object_Account__c;
            }
        }
      
      
        //Count for task
       Map<id,List<Task>> MapAccountIDTasks;
       if(Year_Last==true)
         MapAccountIDTasks=ListofTask([select id,Accountid,ActivityDate,Date_Time_From__c,Date_Time_To__c,subject,Recordtype.Name from task where AccountId in :ListAccountId and User_Department__c='Business Development' and CreatedDate=Last_year]);
         else
       MapAccountIDTasks=ListofTask([select id,Accountid,ActivityDate,Date_Time_From__c,Date_Time_To__c,subject,Recordtype.Name from task where AccountId in :ListAccountId and User_Department__c='Business Development' and CreatedDate=this_year]);
     
         
       
      
      
        //Count for Event
          Map<id,List<Event>> mapAccountIDEvent;
          
         if(Year_Last==true)
          mapAccountIDEvent=ListofEvents([select id,Accountid,ActivityDate,Date_Time_From__c,Date_Time_To__c,subject,Recordtype.Name from Event where AccountId in :ListAccountId and User_Department__c='Business Development' and CreatedDate=Last_year]);
     else          
      mapAccountIDEvent=ListofEvents([select id,Accountid,ActivityDate,Date_Time_From__c,Date_Time_To__c,subject,Recordtype.Name from Event where AccountId in :ListAccountId and User_Department__c='Business Development' and CreatedDate=this_year]);
 
      
       
      
      
      
       /*
       List<Account> ListAccount=[select id,Total_Meeting_Count__c,Total_Interaction_Count__c,(Select Id,Subject,Recordtype.Name from Tasks where CreatedDate=this_Year) from account where ROC_Status__c='Active' and temp__c='Clean' limit 200];
       */
    
        // set new values 
        Map<string,Date> checkStartDate = new Map<string,Date>();
        Map<string,Date> checkEndDate = new Map<string,Date>();
        Map<string,Date> checkEventDate = new Map<string,Date>();
        for(Account ObjAccount :ListAccount){
            Integer Meetings=0;
            Integer Interactions=0;
            Integer eventMeeting=0;
            if(MapAccountIDTasks.size()>0){
                if(MapAccountIDTasks.containsKey(ObjAccount.id)){
                    for(Task ObjTask:MapAccountIDTasks.get(ObjAccount.id)){
                       //v1.1 start
                        //System.debug('ObjTask Subject'+ObjTask.Subject);
                        
                            if(ObjTask.Recordtype.Name=='Meeting')
                            {
                                 Meetings++;
                                 /*
                                if(ObjTask.Date_Time_From__c!=null){
                                    if(checkStartDate.get(ObjAccount.id)!= ObjTask.Date_Time_From__c.date()){
                                    Meetings++;    
                                    }
                                    checkStartDate.put(ObjAccount.id,ObjTask.Date_Time_From__c.date());
                                    checkEndDate.put(ObjAccount.id,ObjTask.Date_Time_To__c.date());
                                }
                                */
                            }
                            else{
                                Interactions++;
                                //checkStartDate.put(ObjAccount.id,ObjTask.Date_Time_From__c.date());
                                //checkEndDate.put(ObjAccount.id,ObjTask.Date_Time_To__c.date());
                            }
                            
                        //v1.1 End
                    }
                    
                }
            }
            if(mapAccountIDEvent.size()>0)
            {
                for(Event objEvent:mapAccountIDEvent.get(ObjAccount.id))
                {
                       eventMeeting++;
                       
                    /*
                    if(objEvent.Date_Time_From__c!=null)
                    {
                        if(checkEventDate.get(ObjAccount.id)!= objEvent.Date_Time_From__c.date()){ // v1.1
                            eventMeeting++;
                            checkEventDate.put(ObjAccount.id,objEvent.Date_Time_From__c.date());
                        }
                    }
                    */
                    
                }
            }
            system.debug('----Meetings---'+Meetings+'------'+Interactions);
            ObjAccount.Total_Meeting_Count__c=Meetings;
            ObjAccount.Total_Meeting_Count__c= ObjAccount.Total_Meeting_Count__c+eventMeeting;
            ObjAccount.Total_Interaction_Count__c=Interactions;
        }
            System.debug('ListAccount'+ListAccount);
            Update ListAccount;
    }

   global void finish(Database.BatchableContext BC){
   
 
   
    List<account> UpdateAccount=new List<account>();
    for(account ObjAccount:[select id,Name,Child_Meeting_Count__c,Child_Interaction_Count__c,(select id,Total_Meeting_Count__c,Total_Interaction_Count__c from Accounts1__r ) from account where  Same_Management__c=null AND (ROC_Status__c='Active' or ROC_Status__c='Not Renewed' )])
    {
        Decimal Meetings=0;
        Decimal Interactions=0;
        boolean Add=false;
        for(Account ObjRel:ObjAccount.Accounts1__r){
            Meetings+=ObjRel.Total_Meeting_Count__c;
            Interactions+=ObjRel.Total_Interaction_Count__c;
            Add=true;
        }
        if(Add){
            ObjAccount.Child_Meeting_Count__c=Meetings;
            ObjAccount.Child_Interaction_Count__c=Interactions;
            UpdateAccount.add(ObjAccount);
        }

    }
     for(account ObjAccount:[select id,Parent_Is_KPI_Met__c,Same_Management__r.Is_KPI_Met_new__c,Name from account where  Same_Management__c!=null AND (ROC_Status__c='Active' or ROC_Status__c='Not Renewed' )])
    {
        ObjAccount.Parent_Is_KPI_Met__c=ObjAccount.Same_Management__r.Is_KPI_Met_new__c;
        UpdateAccount.add(ObjAccount);
    }
    
    
    if(!UpdateAccount.isEmpty()){
        update UpdateAccount;
    }
   
   }
}