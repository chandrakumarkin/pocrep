@isTest
global class DIFCLongResidenceVisaControllerTest {
    
  
    @testSetup static void setup() {

        Account objAccount = new Account(Name = 'Test Custoer 1',E_mail__c = 'test@test.com',BP_No__c = '001234');
        insert objAccount;
        
        Status__c  ss = new Status__c ();
        ss.name ='Completed';
        ss.code__c = 'COMPLETED';
        insert ss;
        
        Account objAccountOld = new Account();
        objAccountOld.Name = 'Test Custoer 1';
        objAccountOld.E_mail__c = 'test@test.com';
        objAccountOld.BP_No__c = '001235';
        insert objAccountOld;
        
        Identification__c objId = new Identification__c();
        objId.Id_No__c = '2/6/1234567';
        objId.Valid_From__c = system.today().addYears(-1);
        objId.Valid_To__c = system.today().addYears(1);
        objId.Identification_Type__c = 'Access Card';
        objId.Account__c = objAccountOld.Id;
        insert objId;
        
        License__c objLicense = new License__c();
        objLicense.License_Expiry_Date__c = system.today().addYears(1);
        objLicense.Status__c = 'Active';
        objLicense.License_No__c = '1234';
        objLicense.Account__c = objAccountOld.Id;
        insert objLicense;
        
        for(License__c objL : [select Id,Name from License__c where Id=:objLicense.Id ]){
            objLicense = objL;
        }
        
        objAccountOld.Active_License__c = objLicense.Id;
        objAccountOld.Index_Card__c = objId.Id;
        update objAccountOld;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
                 conRT = objRT.Id;
           
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.AccountId = objAccountOld.Id; 
        objContact.Passport_No__c = 'ABC12345';
        objContact.RecordTypeId = conRT;
        insert objContact;
        
        Document_Details__c objDD = new Document_Details__c();
        objDD.Document_Id__c = '201/2015/1234567';
        objDD.Document_Number__c = '201/2015/1234567';
        objDD.Document_Type__c = 'Employment Visa';
        objDD.Contact__c = objContact.Id;
        insert objDD;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Long_Term_Residence_Visa';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Long_Term_Residence_Visa';
        objTemplate.Menutext__c = 'DIFC_Long_Term_Residence_Visa';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'DIFC New Visa';
        lstProducts.add(objProd);
        insert lstProducts;
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC_Long_Term_Residence_Visa';
        objPL.DEV_Id__c = 'GO-00001';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00001';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            objDP.Unit_Price__c = 1234;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Long_Term_Residence_Visa')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }       
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Long_Term_Residence_Visa');
        objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[0].Id;
        objSR.External_SR_Status__c = lstSRStatus[0].Id;
        objSR.DIFC_License_No__c = objLicense.Name;
        objSR.Residence_Visa_No__c = '201/2015/1234567';
        objSR.Company_Name__c = objAccount.Name;
        objSR.Sponsor_Establishment_No__c = objId.Id_No__c;
        insert objSR;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 3210;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        lstPriceItems.add(objItem);
                
        insert lstPriceItems;
        //step template
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Front Desk Review';
        objStepType1.Code__c = 'Front Desk Review';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Front Desk Review';
        insert objStepType1;
        
      
      
        SR_Steps__c   stpTemplateCode = new SR_Steps__c   ();
        stpTemplateCode.Step_Template__c= objStepType1.ID;
        insert stpTemplateCode;
        
        Step__c objStep = new Step__c();
        objStep.Step_Template__c = objStepType1.id ;
        objStep.SR__c = objSR.Id;
        objStep.Paused_Date__c = system.today();
        objStep.SR_Step__c = stpTemplateCode.ID;
        objStep.Closed_Date_Time__c = system.today();
        insert objStep;
        
    }
    
    static testMethod void myUnitTest() {
            
     
        
            Portal_Specific_Customer_SR_Types__c pmr = new Portal_Specific_Customer_SR_Types__c(SR_Record_Type_API_Name__c ='DIFC_Long_Term_Residence_Visa',
                SR_Type__c ='DIFC Long Term Residence Visa',Is_Active__c = true,Name ='DIFC Long Term Residence Visa'
            );
            insert pmr;
            List<Service_Request__c> srList = [select Customer__c,Id,Place_of_Birth__c,Country_of_Birth__c,Qualification__c,Religion__c,
            Emirate__c,City__c,Area__c,First_Language__c,Second_Language__c,Marital_Status__c,Email_Address__c,PO_BOX__c,Residence_Phone_No__c,
            Building_Name__c,Street_Address__c,Mobile_Number__c,Work_Phone__c,Identical_Business_Domicile__c,
            City_Town__c,Address_Details__c,Sponsor_Visa_No__c,Gender__c,Nationality_list__c, Mother_Full_Name__c,
            Date_of_Birth__c   from Service_Request__c limit 1];
            Contact con = [select Id
              from Contact limit 1];
            Step__c  stepRec = [select Id,Step_Template__c,SR__c,Paused_Date__c,Closed_Date_Time__c,Is_Letter_Submitted_to_GDFRA__c,SR_Step__c  from Step__c ];
            test.startTest();
                
               // Id GSrectypeid = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC_Long_Term_Residence_Visa').getRecordTypeId();
                Id GSrectypeid = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('DIFC_Long_Term_Residence_Visa').getRecordTypeId();
                ApexPages.currentPage().getParameters().put('id',String.valueOf(srList[0].Id));
                ApexPages.currentPage().getParameters().put('RecordType',String.valueOf(GSrectypeid));
                ApexPages.currentPage().getParameters().put('RecordTypeName','DIFC_Long_Term_Residence_Visa');
                
                ApexPages.StandardController sc = new ApexPages.StandardController(srList[0]);
                DIFCLongResidenceVisaController difc = new DIFCLongResidenceVisaController(sc);
                difc.fetchWidowOccupationList();
                difc.fetchEmployeeDependentList();
                difc.showEmployeeSection();
                System.debug('==GSrectypeid =====>'+GSrectypeid );
                difc.populateOnLoad();
                difc.saveRequest();
                difc.submitSR();
                difc.srRequest.Required_Docs_not_Uploaded__c  = false;
                difc.submitSR();
                difc.editRequest();
                difc.cancelRequest();
                difc.populateDependentContactDetails();
                difc.employeeSelectionListWithoutNo();
                DIFCLongResidenceVisaController.removeNewContactFromSystem(stepRec);
                DIFCLongResidenceVisaController.populateContactFromSR(srList[0],con);
                
                difc.selectedDependent =con.Id;
                difc.populateDependentContactDetails();
                difc.selectedSponsor =con.Id;
                difc.populateContactDetails();
                
                PortalMenuAccessOfCustomer pm = new PortalMenuAccessOfCustomer();
                PortalMenuAccessOfCustomer.fetchPortalAccessList(srList[0].Customer__c);
                List<PortalMenuAccessOfCustomer.PortalMenuWrapper> pm1 = new List<PortalMenuAccessOfCustomer.PortalMenuWrapper>();
                PortalMenuAccessOfCustomer.saveMenuList(pm1 ,srList[0].Customer__c);
                
                CC_Validate_ReqDocument.checkLetterSubmittedToGDFRA(stepRec);
                CC_Validate_ReqDocument.Document_Upload_Check(stepRec);
                stepRec.Is_Letter_Submitted_to_GDFRA__c = true;
                update stepRec;
                CC_Validate_ReqDocument.checkLetterSubmittedToGDFRA(stepRec);
                Step__c  stepRec1 = [select Id,Step_Template__c,SR__c,Paused_Date__c,Closed_Date_Time__c from Step__c ];
                CC_Validate_ReqDocument.checkLetterSubmittedToGDFRA(stepRec1);
                CC_Validate_ReqDocument.Document_Upload_Check(stepRec1);
                CC_Validate_ReqDocument.DeactivatePortalSRServiceBasedOnCustomer(stepRec1);
               
            test.stopTest();
       
    }
}