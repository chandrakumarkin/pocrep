@isTest
public class TestIssueFineObjectionWithdraw {

     @testSetUp
    static void PrepareTestData(){
        
        Sr_Status__c srStatus1 = new Sr_Status__c();
        srStatus1.Code__c = 'Withdraw';
        insert srStatus1;
        
        Sr_Status__c srStatus = new Sr_Status__c();
        srStatus.Code__c = 'COMPLETED';
        insert srStatus;
        
        List<Account> accList = new List<Account>();
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 354545';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '1234';
        accList.add(objAccount);
        insert accList;
        
        List<SR_Template__c> srTemplateList = new List<SR_Template__c>();
        SR_Template__c srTemplate = new SR_Template__c();
        srTemplate.Name = 'Objection SR';
        srTemplate.SR_RecordType_API_Name__c = 'Objection_SR';
        srTemplate.SR_Group__c = 'ROC';
        srTemplate.Menu__c = 'Company Services';
        srTemplate.sub_Menu__c = 'Objection Notices';
        srTemplateList.add(srTemplate);
        
        SR_Template__c srTemplate1 = new SR_Template__c();
        srTemplate1.Name = 'Issue Fine';
        srTemplate1.SR_RecordType_API_Name__c = 'Issue_Fine';
        srTemplate1.SR_Group__c = 'ROC';
        srTemplate1.Menu__c = 'Company Services';
        srTemplate1.sub_Menu__c = 'Objection Notices';
        srTemplateList.add(srTemplate1);
        
        insert srTemplateList;
        
        List<Service_Request__c> SrList = new List<Service_Request__c>();
        Service_request__c srObj = new Service_Request__c();
        srObj.Refund_Amount__c = 10000;
        srObj.I_agree__c = true; 
        srObj.Type_of_Request__c = 'Preliminary Notice of Fines';
        srObj.Comments__c = 'Test';
        srObj.SR_template__c = srTemplate1.id;
        srObj.Customer__c = objAccount.Id;
        srObj.RecordTypeID = [select id from RecordType where DeveloperName = 'Issue_Fine' and SobjectType = 'Service_Request__c' limit 1].id;
        insert srObj;
        
        Service_request__c srObj1 = new Service_Request__c();
        srObj1.Refund_Amount__c = 10000;
        srObj1.I_agree__c = true; 
        srObj1.Comments__c = 'Test1';
        srObj1.Type_of_Request__c = 'Notification Renewal';
        srObj1.SR_template__c = srTemplate.id;
        srObj1.Customer__c = objAccount.Id;
        srObj1.RecordTypeID = [select id from RecordType where DeveloperName = 'Objection_SR' and SobjectType = 'Service_Request__c' limit 1].id;
        srObj1.Linked_SR__c = srObj.Id;
        SrList.add(srObj1);
        
        
        Service_request__c srObj2 = new Service_Request__c();
        srObj2.Refund_Amount__c = 10000;
        srObj2.I_agree__c = true; 
        srObj2.Comments__c = 'Test2';
        srObj2.Type_of_Request__c = 'Decision Notice of Fines';
        srObj2.SR_template__c = srTemplate1.id;
        srObj2.Customer__c = objAccount.Id;
        //srObj2.RecordTypeID = [select id from RecordType where DeveloperName = 'Issue_Fine' and SobjectType = 'Service_Request__c' limit 1].id;
        SrList.add(srObj2);
        insert SrList;
        
    }
    
     static testmethod void MyUnitTest1(){
        Test.startTest();
        service_request__c srObj = [select id,Name,Service_Type__c,Mobile_Number__c,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c, Linked_SR__r.Type_of_Request__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c,Linked_SR__c, Type_of_Request__c, Record_Type_Name__c, Linked_SR__r.Record_Type_Name__c
                                    from service_request__c where Type_of_Request__c = 'Notification Renewal' Limit 1];
         
         
         IssueFineObjectionWithdraw.withdrawIssueFine(srObj.Id);
         Test.stopTest();
     }
    
    
      static testmethod void MyUnitTest2(){
        Test.startTest();
        service_request__c srObj = [select id,Name,Service_Type__c,Mobile_Number__c,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                    Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c, Linked_SR__r.Type_of_Request__c,
                                    Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c,Linked_SR__c, Type_of_Request__c, Record_Type_Name__c, Linked_SR__r.Record_Type_Name__c
                                    from service_request__c where Type_of_Request__c = 'Decision Notice of Fines' Limit 1];
         
         
         IssueFineObjectionWithdraw.updateIssueFine(srObj.Id);
         Test.stopTest();
     }
}