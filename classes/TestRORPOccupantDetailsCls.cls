/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestRORPOccupantDetailsCls {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        objBuilding.SAP_Building_No__c = '123456';
        insert objBuilding;
        
        list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        lstUnits.add(objUnit);
        
        objUnit = new  Unit__c();
        objUnit.Name = '1235';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Residential';
        lstUnits.add(objUnit);
        
        insert lstUnits;
		
		map<string,string> mapRecordType = new map<string,string>();
		for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Lease_Registration','Lease_Unit','Landlord')]){
			mapRecordType.put(objRT.DeveloperName,objRT.Id);
		}
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Customer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Company_Code__c = 'test';
        objAccount.ROC_Status__c = 'Under Formation';
        insert objAccount;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordType.get('Lease_Registration');
        objSR.Type_of_Lease__c = 'Lease';
        objSR.Service_Category__c = 'New';
        objSR.Lease_Commencement_Date__c = system.today();
        objSR.Lease_Expiry_Date__c = system.today().addMonths(13);
        objSR.Agreement_Date__c = system.today();
        objSR.Building__c = objBuilding.Id;
        insert objSR;
        
        list<Amendment__c> lstAmendments = new list<Amendment__c>();
        Amendment__c objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.RecordTypeId = mapRecordType.get('Lease_Unit');
        objAmd.Amendment_Type__c = 'Unit';
        objAmd.Unit__c = lstUnits[0].Id;
        lstAmendments.add(objAmd);
        
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.RecordTypeId = mapRecordType.get('Lease_Unit');
        objAmd.Amendment_Type__c = 'Unit';
        objAmd.Unit__c = lstUnits[1].Id;
        lstAmendments.add(objAmd);
        insert lstAmendments;
        test.startTest();
        
        	Apexpages.currentPage().getParameters().put('id',objSR.Id);
        	
        	RORPOccupantDetailsCls objOD = new RORPOccupantDetailsCls();
        	objOD.AddOccupant();
        	
        	objOD.SaveOccupant();
        	objOD.AmendmentTemp.Unit__c = objOD.lstUnits[1].getValue();
        	objOD.SaveOccupant();
        	objOD.CancelOccupant();
        	objOD.BackToSR();
        	objOD.RowIndex = 0;
        	objOD.RemoveOccupant();
        test.stopTest();
    }
}