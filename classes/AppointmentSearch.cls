/**********************************************************************************************************************
    Author      :   Hussain Fazal Ullah
    Class Name  :   AppointmentSearch
    Description :   This class is an extension for the AppointmentSearch page used by DHA team.
  --------------------------------------------------------------------------------------------------------------------------
    Dependent Components
    --------------------------------------------------------------------------------------------------------------------------
  1. Custom Label : GS_Appointment_Error
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.1    31-07-2019  Fazal        Created
**********************************************************************************************************************/

public class AppointmentSearch 
{
    public list <Appointment__c> stp {get;set;}
    public boolean showAppt {get; set;}
    public boolean showError {get; set;}
    public String searchKey {get;set;}
    public ID AppointmentId {get; set;}
    public AppointmentSearch()
    {
        stp = new List <Appointment__c>();
        showError  = true;
      
        
    }

    public PageReference search1() 
    {
    String qry = 'select Name,id,DHA_Reference_Number__c,Applicant_s_Name__c,Step__c  from Appointment__c ' +
      'where DHA_Reference_Number__c =\''+searchKey+'\'';
      stp = Database.query(qry);

        if(stp.size()<=0){ showAppt = false; ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, Label.GS_Appointment_Error ));}
        else{ showAppt = true; }
        return null;
    }
  }