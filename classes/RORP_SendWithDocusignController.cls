/**
 * Description : Controller for component
 *
 * ****************************************************************************************
 * History :
 */
global  without sharing class RORP_SendWithDocusignController{

	public RORP_SendWithDocusignController(){
	
	}
	
	@AuraEnabled
	public static RespondWrap checkRender(String requestWrapParam){
	
			//declaration of wrapper
			RequestWrap reqWrap = new RequestWrap();
			RespondWrap respWrap =  new RespondWrap();
			
			//deseriliaze.
			reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
	
			respWrap.renderButton = true;

			for(Step__c stepOBj : [SELECT ID FROM Step__c WHERE Step_Code__c = 'ENTITY_SIGNING' 
			 AND SR__c =:reqWrap.srId  LIMIT 1]){
				respWrap.relActionIteam = stepOBj;
				respWrap.renderButton = true;
			}
            
			return respWrap;
	}
			
	
	@AuraEnabled
	public static RespondWrap fetchDocusignData(String requestWrapParam) {
	
		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();
		
		

		//declaration of variables
		list<SR_Doc__c> relatedSrDocList = new list<SR_Doc__c>();
		list<Amendment__c> relAmendList = new list< Amendment__c>();
		
		//deseriliaze the request wrapper
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		System.debug('reqWrap***'+reqWrap);
		for(Step__c stepOBj : [SELECT ID FROM Step__c WHERE Step_Code__c = 'ENTITY_SIGNING' 
			 AND SR__c =:reqWrap.srId  LIMIT 1]){
				respWrap.relActionIteam = stepOBj;
				
			}
	
		for (SR_Doc__c docObj :[SELECT id, Name,(select ContentDocument.Title,ContentDocumentId,LinkedEntityId,ContentDocument.LatestPublishedVersionId from ContentDocumentLinks ORDER BY SystemModstamp DESC limit 1 ) FROM SR_Doc__c WHERE Service_Request__c = :reqWrap.srId]) {
			relatedSrDocList.add(docObj);
		}
	
		for (Amendment__c amendObj :[SELECT id,Given_Name__c ,Family_Name__c,Email_Address__c,Company_Name__c,Record_Type_Name__c,Amendment_Type__c FROM Amendment__c WHERE ServiceRequest__c = :reqWrap.srId AND Amendment_Type__c in ('Landlord','Tenant')  ]) {
			relAmendList.add(amendObj);
		}
		system.debug('relatedSrDocList**'+relatedSrDocList);
		system.debug('relAmendList**'+relAmendList);
		respWrap.relDocList = relatedSrDocList;
		respWrap.relAmendList = relAmendList;
	
		return respWrap;
	
	}
	
	
	@AuraEnabled
	public static RespondWrap generateSendEnvelopeLink(String requestWrapParam) {
	
		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();
	
		//declaration of variables
		List < dfsle.Recipient > recipientList = new List < dfsle.Recipient >();
		set < id > docIdSet = new set < id >();
	
		//deseriliaze the request wrapper
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
	
	
		for(Amendment__c amendObj : reqWrap.amendList ){
	
			if(amendObj.Type_of_Landlord__c  == 'Individual' || amendObj.Record_Type_Name__c=='Individual_Tenant' ){
				dfsle.Recipient myRecipient = dfsle.Recipient.fromSource(
				amendObj.Given_Name__c + ' ' + amendObj.Family_Name__c ,
				amendObj.Email_Address__c,
			null,
			'Signer 1',
			new dfsle.Entity(amendObj.Id));
			recipientList.add(myRecipient);
			}else{
				dfsle.Recipient myRecipient = dfsle.Recipient.fromSource(
				amendObj.Company_Name__c,
				amendObj.Email_Address__c,
			null,
			'Signer 1',
			new dfsle.Entity(amendObj.Id));
			recipientList.add(myRecipient);
			}
		}
		system.debug('**reqWrap'+reqWrap);
		system.debug('**recipientList'+recipientList);
		system.debug('** reqWrap.docIds'+reqWrap.docIds);
		for(string Docid : reqWrap.docIds ){
			docIdSet.add(Docid);	
		}
        if(!Test.isRunningTest()){
           dfsle.Envelope envelope = dfsle.EnvelopeService.getEmptyEnvelope(
			new dfsle.Entity(reqWrap.srId))
											 .withDocuments(dfsle.DocumentService.getLinkedDocuments(
								ContentVersion.getSObjectType(),
								docIdSet,
								false))
											 .withRecipients(recipientList)
											 /* .withEmail('Test text','Test text') */;
										
		envelope = dfsle.EnvelopeService.sendEnvelope(
			envelope,
			false);
	
			respWrap.createdEnvilope = envelope; 
        }
		system.debug('respWrap ***'+respWrap);
		return respWrap;
	}
	
	
	
	@AuraEnabled
	
	public static RespondWrap sendEnvelopeTagging(String myEnvelopeID, String stepId, string srId) {
	
		RespondWrap respWrap = new RespondWrap();
		string responseUrl;
	
		//dfsle.UUID myTemplateId = dfsle.UUID.parse(myEnvelopeID);
	
		dfsle.UUID myTemplateId = dfsle.UUID.parse(myEnvelopeID);
		string returnUrl = system.URL.getSalesforceBaseUrl().toExternalForm() + '/apex/RORP_DocuSignCompletionPage';
	
		responseUrl = dfsle.EnvelopeService.getSenderViewUrl(myTemplateId, new Url(returnUrl + '?envilopeId=' + myTemplateId + '&stepId='+stepId+ '&srId='+srId )).toExternalForm();
	
		respWrap.docuSignTaggingURLString = responseUrl;
	
		respWrap.docuSignEnvilopeID = myEnvelopeID;
		return respWrap;
	}
	
	@AuraEnabled
	public static RespondWrap  updateEnvelopeSentDate(String requestWrapParam){
	
		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();
		//deseriliaze the request wrapper
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

            if(reqWrap.envelopeID != null){
                dfsle__Envelope__c envelopeToUpdate = new dfsle__Envelope__c();
            for(dfsle__Envelope__c envObj : [select id from dfsle__Envelope__c where 	dfsle__DocuSignId__c = : reqWrap.envelopeID]){
                envelopeToUpdate = envObj;
            }
            if(envelopeToUpdate != null){
                envelopeToUpdate.Step_SR__c = reqWrap.stepId;
                envelopeToUpdate.dfsle__Sent__c =  system.now();
            }
            upsert envelopeToUpdate;
            respWrap.debugger = envelopeToUpdate;
        }
            return respWrap;
				//createStatus(reqWrap.srId);
	}

    @AuraEnabled
    public static RespondWrap createEnvelopeStatus(String requestWrapParam) {

        //declaration of wrapper
    RequestWrap reqWrap = new RequestWrap();
    RespondWrap respWrap = new RespondWrap();
    //deseriliaze the request wrapper
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

            List<dfsle.Envelope.Status> myStatus = dfsle.StatusService.getStatus(
                new Set<Id> { 
                    reqWrap.srId
                },
                10000);
                respWrap.debugger = myStatus;
                return respWrap;	
    
    }
		
	
	// ------------ Wrapper List ----------- //
	
	public class RequestWrap {
	@AuraEnabled public list<string> docIds;
	@AuraEnabled public list<Amendment__c> amendList;
	@AuraEnabled public String srId;
	@AuraEnabled public String stepId;
	@AuraEnabled public String envelopeID;
	public RequestWrap() {
	}
	}
	
	public class RespondWrap {
	@AuraEnabled public string docuSignEnvilopeID;
	@AuraEnabled public list<SR_Doc__c> relDocList;
	@AuraEnabled public list<Amendment__c> relAmendList;
	@AuraEnabled public Object createdEnvilope;
	@AuraEnabled public Object DocumentList;
	@AuraEnabled public Object debugger;
	@AuraEnabled public Step__c relActionIteam;
	@AuraEnabled public boolean renderButton ;
	@AuraEnabled public string docuSignTaggingURLString;
	@AuraEnabled public string docuSignTaggingURL;
	public RespondWrap() {
	}
	}

}