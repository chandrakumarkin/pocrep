global class SearchAndReplaceContact implements Database.Batchable < sObject > {

    global final String Query;


    global List<sObject> passedRecords= new List<sObject>();

    global SearchAndReplaceContact(String q,List<sObject> listofRecords) {

        Query = q;
        system.debug('Query---'+Query);
        passedRecords = listofRecords;
    }

    global Database.QueryLocator start(Database.BatchableContext BC) {
        if(query != '')return Database.getQueryLocator(query);
        else return DataBase.getQueryLocator([select Id,Email,Account_Principle_User_Email__c,Additional_Email__c from contact WHERE Id IN: passedRecords and Id != null]);

    }

    global void execute(Database.BatchableContext BC, List < Contact > scope) {
        
        for (Contact ObjContact: scope) {
            ObjContact.email = ObjContact.email != NULl ?ObjContact.email+'.invalid' : ObjContact.email;
            ObjContact.AssistantPhone = '+971569803616';
            ObjContact.HomePhone = '+971569803616';
            ObjContact.MobilePhone = '+971569803616';
            ObjContact.OtherPhone = '+971569803616';
            ObjContact.Phone = '+971569803616';
            ObjContact.CONTACT_PHONE__c = '+971569803616';
            ObjContact.Residence_Phone__c = '+971569803616';
            ObjContact.Work_Phone__c = '+971569803616';

            ObjContact.email = ObjContact.email != NULl ?ObjContact.email+'.invalid' : ObjContact.email;
            ObjContact.Account_Principle_User_Email__c = ObjContact.Account_Principle_User_Email__c != NULl ?ObjContact.Account_Principle_User_Email__c+'.invalid' : ObjContact.Account_Principle_User_Email__c;
            ObjContact.Additional_Email__c = ObjContact.Additional_Email__c != NULl ?ObjContact.Additional_Email__c+'.invalid' : ObjContact.Additional_Email__c;
        }
        system.debug('scope---'+scope);
        Database.SaveResult[] srList = Database.update(scope, false);
        List<Log__c> logRecords = new List<Log__c>();
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                for(Database.Error err : sr.getErrors()) {
                    string errorresult = err.getMessage();
                    logRecords.add(new Log__c(
                      Type__c = 'SearchAndReplaceContact',
                      Description__c = 'Exception is : '+errorresult+'=Contact Id==>'+sr.getId()
                    ));
                    System.debug('The following error has occurred.');                    
                }
            }
        }
        if(logRecords.size()>0)insert logRecords;
    }

    global void finish(Database.BatchableContext BC) {}
}