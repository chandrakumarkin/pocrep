/*
    Author      : Durga Prasad
    Company     : PwC
    Date        : 26-Feb-2018
    Description : Case Trigger Handler
    -------------------------------------------------------------------------
    **Version History**
     * @Author: Rajil Ravindran 
     * @version: 1.0
     * @Date: 02-May-2019
     * @Description: 
        1. Modified the case sla logic to populate the correct sla on the record on status:awaiting response (sla configured in custom setting)
        2. Added the ownership change validation. The new owner should belong to the same queue or else throw an error message.
    -------------------------------------------------------------------------
    **Version History**
     * @Author: Rajil Ravindran 
     * @version: 1.1
     * @Date: 12-June-2019
     * @Description: 
        1. Populate the AwaitingResponse Client End Date and AutoClose Case date to send email(notification) through workflow to the customer

    -------------------------------------------------------------------------
    **Version History**
     * @Author: Anil Valluri 
     * @version: 1.2
     * @Date: 29-Jul-2021
     * @Description: Case Management Enhancements
        1. Remove rule of automatically invalidating cases with the same subject and email address
        2. When invalidating cases, the Invalidated By field should be populated with the person who invalidated the case and the type of feedback and status should be invalide, and the case closed.

*/
public without sharing class CaseTriggerHandler{
    public static boolean CaseTriggerRecursive = false;
    public static map<string,SLA_Configurations__mdt> MapSLAMaster;
    
    //Method to get the records in the custom metadata which contains the SLA hours for each type and each department
    public static map<string,SLA_Configurations__mdt> getSLAConfigurations(){
        map<string,SLA_Configurations__mdt> MapSLA = new map<string,SLA_Configurations__mdt>();
        if(MapSLAMaster!=null){
            MapSLA = MapSLAMaster;
        }else{
            MapSLAMaster = new map<string,SLA_Configurations__mdt>();
            for(SLA_Configurations__mdt SLA:[select Id,Business_Hour_Id__c,Queue_Name__c,SLA_Hours__c,SLA_Minutes__c,Type__c from SLA_Configurations__mdt limit 10000]){
                MapSLAMaster.put(SLA.Queue_Name__c.tolowercase()+'_'+SLA.Type__c,SLA);
            }
            MapSLA = MapSLAMaster;
        }
        return MapSLA;
    }
    
    //Method for before insert logic
    public static void Execute_BI(list<Case> TriggerNew){
        for(Case cs:TriggerNew){
            if(cs.OwnerId != null){
                if(string.valueof(cs.OwnerId).startsWith('00G') && cs.GN_Queue_Name__c!=null)
                    cs.GN_Sys_Case_Queue__c = cs.GN_Queue_Name__c;
            }
        }
        linkCaseToAccOnRegNo(TriggerNew);
    }
    
    
    //Method for after insert logic
    public static void Execute_AI(list<Case> TriggerNew){
        CaseTriggerHelper.invokeAssignmentRulesInsert(TriggerNew);
        Map<ID,String> UniqueSet = new Map<Id,String>();
        list<Case_SLA__c> lstNewSLAs = new list<Case_SLA__c>();
        map<string,Case> MapCasesTBU = new map<string,Case>();
        
        //get all the config records of SLA
        getSLAConfigurations();
        for(Case cs:TriggerNew){
            UniqueSet.put(cs.id,cs.SuppliedEmail+'_'+cs.Subject);
            
            if(cs.Origin == 'Email' && cs.GN_Type_of_Feedback__c == null){
                Case_SLA__c obj = new Case_SLA__c();
                obj.Parent__c = cs.Id;
                obj.Change_Type__c = 'Owner';
                obj.From__c = system.now();
                obj.Owner__c = cs.GN_Owner_Name__c;
                obj.Status__c = cs.Status;
                String type_Origin ='';
                if(cs.GN_Type_of_Feedback__c != null){
                    type_Origin = cs.GN_Type_of_Feedback__c;
                } else{
                    type_Origin = cs.Origin;
                }
                Long sla=0;
                system.debug('@@GN_CASE_QUEUE: '+cs.GN_Sys_Case_Queue__c);
                if(cs.GN_Sys_Case_Queue__c!=null && MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin)!=null){
                            obj.Business_Hours_Id__c = MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).Business_Hour_Id__c;
                            sla = MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).SLA_Minutes__c.longvalue();
                            sla = sla*60*1000L;
                            obj.Due_Date_Time__c = BusinessHours.add(obj.Business_Hours_Id__c,system.now(),sla);
                            Case objCS = new Case(Id=cs.Id,GN_Due_Date_Time__c = obj.Due_Date_Time__c);
                            system.debug('@@GOV SCS');
                            MapCasesTBU.put(objCS.Id,objCS);
                            lstNewSLAs.add(obj);
                }
                
            }
        }
        //if(!UniqueSet.isEmpty())
            //UpdateCase(UniqueSet); // PwC: Logic commented as per Case Management Enhancement (Remove rule of automatically invalidating cases with the same subject and email address)
            
        if(!lstNewSLAs.isEmpty()){
            try{
                //Database.UpsertResult[] srList = Database.upsert(lstNewSLAs, false);
                if(!CaseTriggerHandler.CaseTriggerRecursive){
                    CaseTriggerHandler.CaseTriggerRecursive = true;
                    Database.UpsertResult[] srList = Database.upsert(lstNewSLAs, false);
                    if(MapCasesTBU!=null && MapCasesTBU.size()>0)
                        update MapCasesTBU.values();
                }
            } catch(Exception e){
                TriggerNew[0].addError(e.getMessage()+'');
            }
        }
    }
    @future
    public static void UpdateCase(Map<ID,String> UniqueSet){
        system.debug('UniqueSet--->'+UniqueSet); 
        //Set<String> UniqueCase = new Set<String>(); 
        Map<String,Set<ID>> IsPrimaryCaseMap = new Map<String,Set<ID>>(); 
        for(Case ObjCase :[Select id,Subject,SuppliedEmail,Primary_Case__c from Case where UniqueCase__c IN: UniqueSet.Values() AND CreatedDate=Today]){
            system.debug('ObjCase.id--->'+ObjCase.id);  
           system.debug('ObjCase.id--->'+ObjCase.Primary_Case__c);
            system.debug('IsPrimaryCaseMap--->'+IsPrimaryCaseMap);
            Set<ID> CaseIDs = IsPrimaryCaseMap.containsKey(ObjCase.SuppliedEmail+'_'+ObjCase.Subject)?IsPrimaryCaseMap.get(ObjCase.SuppliedEmail+'_'+ObjCase.Subject):new Set<ID>();
            CaseIDs.add(ObjCase.id);
            IsPrimaryCaseMap.put(ObjCase.SuppliedEmail+'_'+ObjCase.Subject,CaseIDs);    
            }
        system.debug('UniqueCase--->'+IsPrimaryCaseMap);
        Map<String,Set<ID>> UniqueSetofCases = new Map<String,Set<ID>>();
        List<Case> CaseLst = new List<Case>();
        Set<String> UniqueCaseStr = new Set<String>();
        for(Case cs : [Select id,Subject,SuppliedEmail,Primary_Case__c,ParentID from Case where ID IN:UniqueSet.KeySet()]){
            system.debug('UniqueCase In==>'+IsPrimaryCaseMap);
            if(!UniqueCaseStr.contains(cs.SuppliedEmail+'_'+cs.Subject)){
                if(IsPrimaryCaseMap.containsKey(cs.SuppliedEmail+'_'+cs.Subject) && IsPrimaryCaseMap.get(cs.SuppliedEmail+'_'+cs.Subject).size()>1){
                    //UniqueSetofCases.put(cs.SuppliedEmail+'_'+cs.Subject,IsPrimaryCaseMap.get(cs.SuppliedEmail+'_'+cs.Subject));  
                    Integer i=0;
                    ID ParentID ;
                    for(ID CaseID : IsPrimaryCaseMap.get(cs.SuppliedEmail+'_'+cs.Subject)){
                        Case ObjCase = new Case(id=CaseID);
                        if(i==0){
                            ObjCase.Primary_Case__c = true; 
                            ParentID = ObjCase.id;
                            
                        }else if(!ObjCase.Primary_Case__c){
                            //ObjCase.OwnerId ='00G3E000000omKs';
                            objCase.OwnerId = Label.GN_Invalid_Cases_Queue;
                            system.debug('Parent--->'+ParentID);
                            ObjCase.ParentID = ParentID;
                        }
                        CaseLst.add(ObjCase);
                        i++;
                        UniqueCaseStr.add(cs.SuppliedEmail+'_'+cs.Subject);
                    }
                }
                
            }
        }
        if(!CaseLst.isEmpty()) update CaseLst;
    }
    public static Datetime addBusinessDaystoDate(Datetime pointer, Integer days)
    {
        pointer = BusinessHours.nextStartDate(Label.Business_Hours_Id, pointer);
        // make sure you're starting at a Datetime within BusinessHours

        for (Integer elapsed = 0; elapsed < days; elapsed++)
        {
            pointer = pointer.addDays(1);
            if (!BusinessHours.isWithin(Label.Business_Hours_Id, pointer))
                pointer = BusinessHours.nextStartDate(Label.Business_Hours_Id, pointer);
        }
        return pointer;
    }
    public Static Boolean RecursiveFlag = false;
    //Method for before update logic
    public static void Execute_BU(list<Case> TriggerNew,map<Id,Case> TriggerOldMap){
       //linkCaseToAccOnRegNo(TriggerNew);
       CaseTriggerHelper.reAssignToCurrentUser(TriggerOldMap,TriggerNew);
       CaseTriggerHelper.createLead(TriggerOldMap,TriggerNew);
       Map<ID,Case_SLA__c> prtCaseSLAMap = new  Map<ID,Case_SLA__c>();
       List<Case_SLA__c> lstCaseSLAToUpd = new List<Case_SLA__c>();
       
        
        Set<String> UniqueSubjectSet = new Set<String>();
        Set<String> UniqueEmailSet = new Set<String>();
       for(Case_SLA__c caseSLA : [SELECT Parent__c,From__c,Due_Date_Time__c,Until__c,Business_Hours_Id__c FROM Case_SLA__c WHERE Parent__c IN: TriggerOldMap.keySet() AND Until__c=null]){
           prtCaseSLAMap.put(caseSLA.Parent__c,caseSLA);
       }
       for(Case cs:TriggerNew){
           system.debug('before Update==>'+cs.OwnerID);
            if(cs.OwnerId!=TriggerOldMap.get(cs.Id).OwnerId && string.valueof(cs.OwnerId).startsWith('00G') && cs.GN_Queue_Name__c!=null){    
                cs.GN_Sys_Case_Queue__c = cs.GN_Queue_Name__c;
            }    
            
            //Case Management Enhancements- PwC
            //If case is invalidated, stamp the fields- 'Invalidated By' and 'Invalidated Date'
            if(cs.Status == 'Invalid' && cs.Status <> TriggerOldMap.get(cs.Id).Status){
                cs.Invalidated_By__c = UserInfo.getUserID();
                cs.Invalidated_Date__c = System.now();
            }

            //@version: 1.1 : Setting the GN_Awaiting_Response_Client_End_Date__c and GN_Auto_Close_Case_Date__c for the case
            //Populate the AwaitingResponse Client End Date and AutoClose Case date to send email(notification) through workflow to the customer
           if(Label.Days_for_Client_Response != null && Label.Days_For_Auto_Close_Case != null && cs.GN_Awaiting_Response_Client_Start_Date__c != null && cs.GN_Awaiting_Response_Client_Start_Date__c != TriggerOldMap.get(cs.Id).GN_Awaiting_Response_Client_Start_Date__c){
                cs.GN_Awaiting_Response_Client_End_Date__c = addBusinessDaystoDate(cs.GN_Awaiting_Response_Client_Start_Date__c, Integer.valueOf(Label.Days_for_Client_Response));
                cs.GN_Auto_Close_Case_Date__c = addBusinessDaystoDate(cs.GN_Awaiting_Response_Client_Start_Date__c, Integer.valueOf(Label.Days_For_Auto_Close_Case));
           }//@version: 1.1 ends here.
            DateTime CurrentDateTime = DateTime.now();
            Decimal ageInMilliSeconds =  BusinessHours.diff(Label.Business_Hours_Id,cs.CreatedDate,CurrentDateTime); //Get the case age in business hours
            system.debug('Agein miliseconds==>'+ageInMilliSeconds );  
            
            Long respDuration=0; 
            Case_SLA_Logic__c slalogic= Case_SLA_Logic__c.getinstance(); 
            
            if(!cs.isClosed || cs.Status != TriggerOldMap.get(cs.Id).Status){    
                     
                //updating the case age when the case is open or when it gets closed
                cs.GN_Case_Age__c = ageInMilliSeconds.divide((1000 * 60 * 60 * 8),3);  
                  
                if(cs.Status!=TriggerOldMap.get(cs.Id).Status)
                {  //if status is changed
                    if(cs.Status=='Awaiting Response')
                    {
                        cs.GN_Awaiting_Response_Start_Date__c=system.now();
                        
                        respDuration=slalogic.Awaiting_Response_Days__c.longvalue() * 8 * 60 * 60* 1000L; 
                        system.debug('respDuration==>'+respDuration);  
                        
                        cs.GN_Awaiting_Response_End_Date__c=BusinessHours.add(Label.Business_Hours_Id, CurrentDateTime , respDuration);
                        system.debug('Awaiting End==>'+cs.GN_Awaiting_Response_End_Date__c);
                        
                        //backup of duration time 
                        if(prtCaseSLAMap.containsKey(cs.id))
                        {
                            Case_SLA__c tempCaseSLA = prtCaseSLAMap.get(cs.id);
                            cs.GN_Due_Date_Time__c = tempCaseSLA.Due_Date_Time__c;
                            tempCaseSLA.Due_Date_Time__c = NULL;
                            lstCaseSLAToUpd.add(tempCaseSLA);
                        }
                        
                    }
                    
                    /* Commented because of Null Pointer Exception Kaavya will look into the issue and fix it
                    if(TriggerOldMap.get(cs.Id).Status=='Awaiting Response')
                    {
                        //Restoring the SLA time Logic when Previous Status was  Awaiting Response
                        // Age of Case when Status=='Awaiting Response'
                        
                         Case_SLA__c tempCaseSLA = prtCaseSLAMap.get(cs.id);
                         Long awaitDiffInMilliSec =  BusinessHours.diff(tempCaseSLA.Business_Hours_Id__c,system.now(),cs.GN_Awaiting_Response_Start_Date__c);
                         //Long awaitDiffInMilliSec = system.now().getTime() - cs.GN_Awaiting_Response_Start_Date__c.getTime();
                         system.debug('###### awaitDiffInMilliSec '+ awaitDiffInMilliSec);
                         system.debug('###### awaitDiffInMilliSec int '+ ((Integer)awaitDiffInMilliSec/1000));
                         tempCaseSLA.Due_Date_Time__c = cs.GN_Due_Date_Time__c.addSeconds((Integer)awaitDiffInMilliSec/1000);
                         lstCaseSLAToUpd.add(tempCaseSLA);
                        
                        cs.GN_Awaiting_Response_Start_Date__c=null;
                        cs.GN_Awaiting_Response_End_Date__c=null;
                      
                    }
                    */
                    
                    //updating Case SLA
                    /*if(lstCaseSLAToUpd != NULL && lstCaseSLAToUpd.size() > 0)
                    {
                        update lstCaseSLAToUpd;
                    }*/
                    
                    
                }
            }
          /* if(cs.Subject!=null)
                UniqueSubjectSet.add(cs.Subject);
            if(cs.SuppliedEmail!=null)
                UniqueEmailSet.add(cs.SuppliedEmail);*/
        }
        
       /* Set<String> UniqueCase = new Set<String>();
        set<string> caseIds = new set<string>();
        if(!UniqueSubjectSet.isEmpty() && !UniqueEmailSet.isEmpty()){
            for(Case ObjCase :[Select id,Subject,SuppliedEmail from Case where SuppliedEmail IN:UniqueEmailSet AND Subject IN:UniqueSubjectSet AND CreatedDate=Today and id not in :trigger.oldmap.keyset()]){
                UniqueCase.add(ObjCase.Subject+'_'+ObjCase.SuppliedEmail);  
                
                system.debug('UniqueCase In==>'+UniqueCase);
            } 
        }
        Set<String> SetOfIDs = new Set<String>();
        for(Case cs:TriggerNew){
            system.debug('cs==>'+cs.Subject+'_'+cs.SuppliedEmail);
            Map<String,ID> UniqueParentMap = new Map<String,ID>();
            /*
            if(!SetOfIDs.contains(cs.Subject+'_'+cs.SuppliedEmail)){
                cs.Primary_Case__c = true;
                UniqueParentMap.put(cs.Subject+'_'+cs.SuppliedEmail,cs.id);
                SetOfIDs.add(cs.Subject+'_'+cs.SuppliedEmail);
            }else {
                cs.OwnerId ='00G3E000000omKs'; 
                //if(UniqueParentMap.containsKey(cs.Subject+'_'+cs.SuppliedEmail))
                //cs.ParentID=UniqueParentMap.get(cs.Subject+'_'+cs.SuppliedEmail);
            }* /
            if(UniqueCase.contains(cs.Subject+'_'+cs.SuppliedEmail) ){
               cs.OwnerId ='00G3E000000omKs'; 
                continue;
            }
             cs.Primary_Case__c = true;
                
        }*/
        CaseTriggerHelper.closingcase( TriggerOldMap ,TriggerNew);
        
        //Version:1.1 :Added the ownership change validation. The new owner should belong to the same queue or else throw an error message.
         System.debug('>>CaseOwnershipValidation trying to invoke>>');
        DisableValidation_Trigger__c profileCustomSetting = DisableValidation_Trigger__c.getInstance(UserInfo.getUserId()); //Bypasses for System Admin profile the below validation
        System.debug('profileCustomSetting.Trigger_Variable__c '+profileCustomSetting.Trigger_Variable__c); //profileCustomSetting.Trigger_Variable__c == 'CaseOwnershipValidation' && 
        System.debug('profileCustomSetting.Disable_Triggers__c'+profileCustomSetting.Disable_Triggers__c);
        if((!profileCustomSetting.Disable_Triggers__c)
             || (ApexCodeUtility.skipTriggerValidation == false)){
            System.debug('>>CaseOwnershipValidation invoked>>');
            CaseTriggerHelper.ownershipValidation(TriggerNew,TriggerOldMap);
        }
        
    }
     
    
    //Method for after update logic
    public static void Execute_AU(list<Case> TriggerNew,map<Id,Case> TriggerOldMap){
        set<string> setOwnerChangeCaseIds = new set<string>();
        set<string> setStatusChangeCaseIds = new set<string>();
        map<string,string> mapClosedCaseSLAs = new map<string,string>();
        list<Case_SLA__c> lstNewSLAs = new list<Case_SLA__c>();
        
        //get all the config records of SLA
        getSLAConfigurations();
        
        system.debug('MapSLAMaster==>'+MapSLAMaster);
        map<string,Case> MapCasesTBU = new map<string,Case>();
        
        
        map<string,datetime> MapOldOwnerDueDate = new map<string,DateTime>();
        set<string> setCaseIds_AR = new set<string>();
        Set<Id> caseOwnerIds = new Set<Id>();
        for(Case objcs:TriggerNew){
            if(objcs.Status!=TriggerOldMap.get(objcs.Id).Status && TriggerOldMap.get(objcs.Id).Status=='Awaiting Response'){
                setCaseIds_AR.add(objcs.Id);
            }
            caseOwnerIds.add(objcs.Id);
        }
        if(setCaseIds_AR.size()>0){
            for(Case_SLA__c sla:[Select Id,Due_Date_Time__c,Parent__c,Owner__c from Case_SLA__c where Parent__c IN:setCaseIds_AR and Owner__c!=null order by CreatedDate Desc]){
                if(MapOldOwnerDueDate.get(sla.Owner__c)==null)
                    MapOldOwnerDueDate.put(sla.Owner__c,sla.Due_Date_Time__c);
            }
        }
        
        Map<String,Datetime> existSLAOwnerMap = new Map<String,Datetime>();
        for(Case_SLA__c objSLA : [SELECT Id, Owner__c, Due_Date_Time__c FROM Case_SLA__c WHERE Parent__c IN :caseOwnerIds]){
            existSLAOwnerMap.put(objSLA.Owner__c, objSLA.Due_Date_Time__c);
        }
        String type_Origin;
        
        List<Case> caseToClone = new List<Case>();
        List<EmailMessage> emailMessageToClone = new List<EmailMessage>();
        
        for(Case cs:TriggerNew){
            type_Origin ='';
            if(cs.GN_Type_of_Feedback__c != null){
                type_Origin = cs.GN_Type_of_Feedback__c;
            } else{
                type_Origin = cs.Origin;
            }
            //Type is changed or if Department is changed
            //if((cs.GN_Type_of_Feedback__c!=TriggerOldMap.get(cs.Id).GN_Type_of_Feedback__c )||(cs.OwnerId!=TriggerOldMap.get(cs.Id).OwnerId && string.valueof(cs.OwnerId).startsWith('00G'))||(cs.Status!=TriggerOldMap.get(cs.Id).Status) && cs.Status=='Re-Open'){
            if((cs.GN_Type_of_Feedback__c!=TriggerOldMap.get(cs.Id).GN_Type_of_Feedback__c )
               ||(cs.OwnerId!=TriggerOldMap.get(cs.Id).OwnerId)
               ||(cs.Status!=TriggerOldMap.get(cs.Id).Status)
               && cs.Status=='Re-Open'){
                //To create Case SLA records if department is changed
                if(cs.OwnerId!=TriggerOldMap.get(cs.Id).OwnerId && string.valueof(cs.OwnerId).startsWith('00G')
                   ||(cs.Status!=TriggerOldMap.get(cs.Id).Status) && cs.Status=='Re-Open'){
                //if(cs.OwnerId!=TriggerOldMap.get(cs.Id).OwnerId ||(cs.Status!=TriggerOldMap.get(cs.Id).Status) && cs.Status=='Re-Open'){
                    setOwnerChangeCaseIds.add(cs.Id);
                    Case_SLA__c obj = new Case_SLA__c();
                    obj.Parent__c = cs.Id;
                    obj.Change_Type__c = 'Owner';
                    obj.From__c = system.now();
                    obj.Owner__c = cs.GN_Owner_Name__c;
                    obj.Status__c = cs.Status;
                    system.debug('QueueName==>'+cs.GN_Sys_Case_Queue__c);                    
                    
                    //Assigning the logic to be used for SLA - whether global or department wise
                    Case_SLA_Logic__c slalogic= Case_SLA_Logic__c.getinstance(); 
                    DateTime DueDt = null;
                    Long sla=0;
                    system.debug('SLAlogic==>'+slalogic.Global_SLA__c);
                    
                    if(slalogic.Global_SLA__c){ // SLA logic is global
                        system.debug('MapSLAGlobal==>'+MapSLAMaster.get('global_'+cs.GN_Type_of_Feedback__c));
                        obj.Business_Hours_Id__c=Label.Business_Hours_Id;
                        
                        if(MapSLAMaster.get('global_'+type_Origin)!=null){
                            sla = MapSLAMaster.get('global_'+cs.GN_Type_of_Feedback__c).SLA_Minutes__c.longvalue(); 
                            system.debug('SLA==>'+sla);
                            sla = sla*60*1000L;
                            system.debug('$$DUEDATE: '+cs.GN_Due_Date_Time__c);
                            if(cs.GN_Due_Date_Time__c==null||(cs.Status!=TriggerOldMap.get(cs.Id).Status) && cs.Status=='Re-Open'){
                                DueDt=BusinessHours.add(obj.Business_Hours_Id__c,system.now(),sla);
                            }
                            else{
                                if(cs.OwnerId!=TriggerOldMap.get(cs.Id).OwnerId && string.valueof(cs.OwnerId).startsWith('00G')){
                                    system.debug('$$SCEN1');
                                    DueDt=BusinessHours.add(obj.Business_Hours_Id__c,system.now(),sla);
                                } else{
                                    system.debug('$$SCEN2');
                                    DueDt=cs.GN_Due_Date_Time__c;
                                }
                            }
                            system.debug('Due Date==>'+cs.GN_Due_Date_Time__c);
                            
                        }                        
                        obj.Due_Date_Time__c = DueDt;//
                        Case objCS = new Case(Id=cs.Id,GN_Due_Date_Time__c = DueDt);
                        MapCasesTBU.put(objCS.Id,objCS);
                    }
                    else{ //SLA logic department wise
                        
                        System.debug('@@SYS_TYPE: '+cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin);
                        System.debug('@@MAP_OLD_OWNER: '+MapOldOwnerDueDate);
                        System.debug('@@OLD_OWNER: '+TriggerOldMap.get(cs.Id).GN_Owner_Name__c);
                        System.debug('@@CS_DUE_DATE: '+cs.GN_Due_Date_Time__c);
                        if(cs.GN_Sys_Case_Queue__c!=null && MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin)!=null){
                            obj.Business_Hours_Id__c = MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).Business_Hour_Id__c;
                            sla = MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).SLA_Minutes__c.longvalue();
                            sla = sla*60*1000L;
                            //obj.Due_Date_Time__c = BusinessHours.add(obj.Business_Hours_Id__c,system.now(),sla);
                            if(cs.OwnerId!=TriggerOldMap.get(cs.Id).OwnerId && string.valueof(cs.OwnerId).startsWith('00G')){
                                system.debug('$$SCEN1');
                                obj.Due_Date_Time__c=BusinessHours.add(obj.Business_Hours_Id__c,system.now(),sla);
                                /*
                                if(existSLAOwnerMap.containskey(cs.GN_Sys_Case_Queue__c)){
                                    obj.Due_Date_Time__c=existSLAOwnerMap.get(cs.GN_Sys_Case_Queue__c);
                                } else{
                                    obj.Due_Date_Time__c=BusinessHours.add(obj.Business_Hours_Id__c,system.now(),sla);
                                }
                                */
                            } else{
                                system.debug('$$SCEN2');
                                obj.Due_Date_Time__c=cs.GN_Due_Date_Time__c;
                                //obj.Due_Date_Time__c = MapOldOwnerDueDate.get(TriggerOldMap.get(cs.Id).GN_Owner_Name__c);
                            }
                            Case objCS = new Case(Id=cs.Id,GN_Due_Date_Time__c = obj.Due_Date_Time__c);
                            MapCasesTBU.put(objCS.Id,objCS);
                            lstNewSLAs.add(obj);
                        }
                    }
                    
                }
            }
            if(cs.Status!=TriggerOldMap.get(cs.Id).Status && cs.isClosed){
                mapClosedCaseSLAs.put(cs.Id,cs.Id);
            }
            String[] strAwaitingStatus = new String[]{'Awaiting Response - Client','Awaiting Response - Internal Department','Awaiting Response - Authorities'};
            system.debug('@@@INDEX: '+strAwaitingStatus.indexOf(TriggerOldMap.get(cs.Id).Status) + ' --- '+strAwaitingStatus.indexOf(cs.Status)); 
            if(cs.Status!=TriggerOldMap.get(cs.Id).Status && 
               (strAwaitingStatus.indexOf(TriggerOldMap.get(cs.Id).Status) > -1 || strAwaitingStatus.indexOf(cs.Status) > -1)){
                system.debug('>>Awaiting response>>case sla check');
                system.debug('@@INPROG');
                
                Case_SLA__c obj = new Case_SLA__c();
                obj.Parent__c = cs.Id;
                obj.Change_Type__c = 'Owner';
                obj.From__c = system.now();
                //obj.Owner__c = cs.GN_Owner_Name__c;
                obj.Owner__c = cs.GN_Sys_Case_Queue__c;
                obj.Status__c = cs.Status;
                system.debug('QueueName==>'+cs.GN_Sys_Case_Queue__c); 
                if(cs.GN_Sys_Case_Queue__c!=null && MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin)!=null){
                        obj.Business_Hours_Id__c = MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).Business_Hour_Id__c;
                }
                
                
                //Assigning the logic to be used for SLA - whether global or department wise
                Case_SLA_Logic__c slalogic= Case_SLA_Logic__c.getinstance(); 
                DateTime DueDt = null;
                Long sla=0;
                system.debug('SLAlogic==>'+slalogic.Global_SLA__c);
                system.debug('@@MAP_OWNER:'+MapOldOwnerDueDate);
                system.debug('@@OWNER_NAME:'+cs.GN_Owner_Name__c);
                system.debug('@@MAP_OWNER_GET:'+MapOldOwnerDueDate.get(cs.GN_Owner_Name__c));
                /*if(MapOldOwnerDueDate.get(cs.GN_Owner_Name__c)!=null){
                    obj.Due_Date_Time__c = MapOldOwnerDueDate.get(cs.GN_Owner_Name__c);//
                    Case objCS = new Case(Id=cs.Id,GN_Due_Date_Time__c = MapOldOwnerDueDate.get(cs.GN_Owner_Name__c));
                    MapCasesTBU.put(objCS.Id,objCS);
                    lstNewSLAs.add(obj);
                }*/
                //version: 1.0; Code below modified to get the correct due datetime in sla from the configuration :[Status from "awaiting response -> other status, set the duedatetime"]
                //Do not set the Due Datetime  for awaiting response status.
                if(strAwaitingStatus.indexOf(TriggerOldMap.get(cs.Id).Status) > -1 && 
                    MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin)!=null && MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).SLA_Minutes__c != null){ 
                    sla = MapSLAMaster.get(cs.GN_Sys_Case_Queue__c.toLowerCase()+'_'+type_Origin).SLA_Minutes__c.longvalue();
                    sla = sla*60*1000L;
                    obj.Due_Date_Time__c = BusinessHours.add(obj.Business_Hours_Id__c,system.now(),sla);
                    Case objCS = new Case(Id=cs.Id,GN_Due_Date_Time__c = obj.Due_Date_Time__c);
                    MapCasesTBU.put(objCS.Id,objCS);
                    
                }
                
                lstNewSLAs.add(obj);
                //if(cs.Status=='Awaiting Response'){
                    setOwnerChangeCaseIds.add(cs.Id);
                //}
            }
            
            if(cs.Re_Create_Case__c && TriggerOldMap.get(cs.Id).Re_Create_Case__c <> cs.Re_Create_Case__c){ //PwC: Case Management Enhancements. To clone the case if email message is received after the case in closed.
            
                Case newCase = new Case();
                newCase = cs.Clone(false, true, false, false);
                newCase.ParentID = cs.ID;
                newCase.Status = 'New';
                //newCase.AutorabitExtId__c = null;
                newCase.Re_Create_Case__c = false;
                
                caseToClone.add(newCase);
                
                
            }
            
        }
        
        if(caseToClone <> null && !caseToClone.isEmpty()){
                insert caseToClone;
                /*
                for(Case newCase: caseToClone){
                    if(newCase.Email_Message_ID_Last__c <> null){
                        EmailMessage newEmailMessage = new EmailMessage();
                        newEmailMessage = cs.Clone(false, true, false, false);
                        
                    }
                }*/
        }
                
        if(setOwnerChangeCaseIds!=null && setOwnerChangeCaseIds.size()>0){ // calculate the business hours used if department is changed
            for(Case_SLA__c sl:[Select Id,Business_Hours_Id__c,From__c,Until__c,Calculated_Business_Minutes__c from Case_SLA__c where Parent__c IN:setOwnerChangeCaseIds and From__c!=null and Until__c=null and Change_Type__c='Owner']){
                sl.Until__c = system.now();
                if(sl.Business_Hours_Id__c!=null)
                    sl.Calculated_Business_Minutes__c = BusinessHours.diff(sl.Business_Hours_Id__c,sl.From__c,sl.Until__c)/1000/60;
                lstNewSLAs.add(sl);
            }
        }
        if(mapClosedCaseSLAs!=null && mapClosedCaseSLAs.size()>0){ // calculate the business hours when case is closed
            try{
                list<Case_SLA__c> openSLAs = new list<Case_SLA__c>();
                //openSLAs.addAll(mapClosedCaseSLAs.values());
                for(Case_SLA__c sla:[select Id,Until__c,Business_Hours_Id__c,From__c,Calculated_Business_Minutes__c from Case_SLA__c where Parent__c IN:mapClosedCaseSLAs.keyset() and Until__c=null]){
                    sla.Until__c = system.now();
                    if(sla.Business_Hours_Id__c!=null)
                        sla.Calculated_Business_Minutes__c = BusinessHours.diff(sla.Business_Hours_Id__c,sla.From__c,sla.Until__c)/1000/60;
                    openSLAs.add(sla);
                }
                if(openSLAs!=null && openSLAs.size()>0){
                    Database.UpsertResult[] srList = Database.upsert(openSLAs, false);   
                }
            }catch(Exception e){
                TriggerNew[0].addError(e.getMessage()+'');
            }
        }
        if(lstNewSLAs!=null && lstNewSLAs.size()>0){
            try{
                if(!CaseTriggerHandler.CaseTriggerRecursive){
                    CaseTriggerHandler.CaseTriggerRecursive = true;
                    Database.UpsertResult[] srList = Database.upsert(lstNewSLAs, false);
                    if(MapCasesTBU!=null && MapCasesTBU.size()>0)
                        update MapCasesTBU.values();
                }
            }catch(Exception e){
                TriggerNew[0].addError(e.getMessage()+'');
            }
        }
        CaseTriggerHelper.invokeAssignmentRulesInsert(TriggerNew, TriggerOldMap);
    }
    
    
    public static void linkCaseToAccOnRegNo(list<Case> TriggerNew){
    
        Set<String> acctRegNos = new Set<String>();
        for(Case objcase : TriggerNew){
            if(objcase.GN_Registration_No__c != null && objcase.GN_Registration_No__c != '' && objcase.isClosed == false){
                acctRegNos.add(objcase.GN_Registration_No__c);
            } 
        }
        System.debug('@@ACCTREG: '+acctRegNos);
        
        Map<String,Id> mapAcct = new Map<String,Id>();
        for(Account objAcct : [SELECT Id, Registration_License_No__c FROM Account 
                                WHERE Registration_License_No__c IN :acctRegNos]){
                                
            mapAcct.put(objAcct.Registration_License_No__c, objAcct.Id);
        }
        
        System.debug('@@MAP: '+mapAcct);
        
        for(Case objcase : TriggerNew){
            if(objcase.GN_Registration_No__c != null && objcase.GN_Registration_No__c != '' && objcase.isClosed == false){
                System.debug('@@OBJCASE: ');
                if(mapAcct.containskey(objcase.GN_Registration_No__c)){
                    objcase.AccountId = mapAcct.get(objcase.GN_Registration_No__c);
                } else{
                    objcase.GN_Account_Name_Non_DIFC__c = objcase.GN_Registration_No__c;
                }
            }
        }
    
    }
    
}