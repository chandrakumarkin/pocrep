/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class TestFineSRDetailsCls {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://eccdev.com/sampledata';
        insert objEP;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c = 'LTD';
        insert objAccount;
        
        Id portalUserId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact = new Contact();
        objContact.firstname = 'Test Contact';
        objContact.lastname = 'Test Contact1';
        objContact.accountId = objAccount.id;
        objContact.recordTypeId = portalUserId;
        objContact.Email = 'test@difcportal.com';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        list<Compliance__c> lstCompliance = new list<Compliance__c>();
        Compliance__c objCompliance = new Compliance__c();
        objCompliance = new Compliance__c();
		objCompliance.Name = 'License Renewal';
		objCompliance.Account__c = objAccount.Id;
		objCompliance.Start_Date__c = system.today().addDays(-7).addDays(-7);
		objCompliance.End_Date__c = system.today().addDays(-1);
		objCompliance.Exception_Date__c = objCompliance.End_Date__c;
		objCompliance.Status__c = 'Open';
		lstCompliance.add(objCompliance);
        
        insert lstCompliance;
        
        FineSRDetailsCls.IssueFine(lstCompliance[0].Id);
        
        SR_Status__c objIssue = new SR_Status__c();
        objIssue.name = 'Issued';
        objIssue.Code__c = 'ISSUED';
        insert objIssue;
        
        SR_Status__c objReverse = new SR_Status__c();
        objReverse.name = 'Reversed';
        objReverse.Code__c = 'RESERVED';
        insert objReverse;
	    
        String objRectype;
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Issue_Fine') AND SobjectType='Service_Request__c']){
            objRectype = objRT.id;
        }
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = objRectype;
        objSR.submitted_date__c = date.today();
        objSR.Internal_SR_Status__c = objIssue.id;
        objSR.External_SR_Status__c = objIssue.id;
        insert objSR; 
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Status__c = 'Added';
        objSRItem.Price__c = 1000;
        insert objSRItem;
        
        SR_Price_Item__c objSRItem1 = new SR_Price_Item__c();
        objSRItem1.ServiceRequest__c = objSR.Id;
        objSRItem1.Status__c = 'Added';
        objSRItem1.Price__c = 1000;
        insert objSRItem1;
        
        Test.setMock(WebServiceMock.class, new TestServiceCreationCls());
        
        list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = objSRItem.Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = objSRItem1.Id;
        objTemp.SFMSG = 'Records are Inserted Successfully in the custom table';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        
        list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
        SAPECCWebService.ZSF_S_ACC_BAL objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        
        TestServiceCreationCls.resItems = resItems;
        TestServiceCreationCls.resActBals = resActBals;
        
        FineSRDetailsCls.FineObjectionInfo(new Step__c(SR__c=objSR.Id));
        //FineSRDetailsCls.PushObjectionSRtoSAP(new list<string>{objSR.Id});
 		
 		resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = objSRItem.Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = objSRItem.Id;
        objTemp.SFMSG = 'Records are Inserted Successfully in the custom table';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
 		TestServiceCreationCls.resItems = resItems;
        TestServiceCreationCls.resActBals = resActBals;
        
        FineSRDetailsCls.FineObjectionInfo(new Step__c(SR__c=objSR.Id));
        
        FineSRDetailsCls.applyDiscount(objSR.Id);
        FineSRDetailsCls.reverseFine(objSR.Id);
 		
 		Service_Request__c objSRNew = new Service_Request__c();
        objSRNew.Customer__c = objAccount.Id;
        objSRNew.Send_SMS_To_Mobile__c = '+971123456789';
        //insert objSR;
 		
 		BatchProcessFineSRs objBatchProcessFineSRs = new BatchProcessFineSRs(new list<Service_Request__c>{objSR});
 		Database.executeBatch(objBatchProcessFineSRs);
 		
 		
 		objBatchProcessFineSRs = new BatchProcessFineSRs(new list<Service_Request__c>{objSRNew});
 		Database.executeBatch(objBatchProcessFineSRs);
 		       
    }
}