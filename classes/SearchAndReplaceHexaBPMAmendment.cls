global class SearchAndReplaceHexaBPMAmendment implements Database.Batchable < sObject > {
global final String Query;
    global List<sObject> passedRecords= new List<sObject>();
global SearchAndReplaceHexaBPMAmendment(String q,List<sObject> listofRecords) {
Query = q;
        passedRecords = listofRecords;

}
global Database.QueryLocator start(Database.BatchableContext BC) {
if(query != '')return Database.getQueryLocator(query);
        else return DataBase.getQueryLocator([Select id,Email__c,DI_Email__c,DI_Mobile__c,Mobile__c from HexaBPM_Amendment__c WHERE Id IN: passedRecords and Id != null]);
}
global void execute(Database.BatchableContext BC, List < HexaBPM_Amendment__c > scope) {
for (HexaBPM_Amendment__c ObjUser: scope) {
ObjUser.Email__c = 'Test@gmail.com.Invalid';
ObjUser.DI_Email__c = 'Test@gmail.com.Invalid';
ObjUser.DI_Mobile__c = ObjUser.DI_Mobile__c != NULl ? '+971569803616': '+971569803616';
ObjUser.Mobile__c = ObjUser.Mobile__c != NULl ? '+971569803616': '+971569803616';
}
Database.SaveResult[] srList = Database.update(scope, false);
        List<Log__c> logRecords = new List<Log__c>();
        // Iterate through each returned result
        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                System.debug('Successfully inserted account. Account ID: ' + sr.getId());
            }
            else {
                for(Database.Error err : sr.getErrors()) {
                    string errorresult = err.getMessage();
                    logRecords.add(new Log__c(
                      Type__c = 'SearchAndReplaceHexaBPMAmendment',
                      Description__c = 'Exception is : '+errorresult+'=Contact Id==>'+sr.getId()
                    ));
                    System.debug('The following error has occurred.');                    
                }
            }
        }
        if(logRecords.size()>0)insert logRecords;}

global void finish(Database.BatchableContext BC) {}
}