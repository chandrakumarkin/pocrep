/*=============================================================================================
Version     Date         Author         Modification
1.0   	15-07-2021     Vinod        Initial Version
=============================================================================================
*/
public class FitoutSiteVisitReportWaiverController {
    @AuraEnabled
    public static StepWrapper getViolations(string stepId){
        List<Violation__c> lstViolations = new List<Violation__c>(); 
        StepWrapper wrap = new StepWrapper();
        Step__c stepRecord = getStepRecord(stepId);
        system.debug('stepRecord***'+stepRecord);
        lstViolations = [Select Id, Name, Violations__r.Name, Fine_Amount__c, Fine_Amount_Formula__c,Blacklist_Point__c,
                         Waive_Black_Points__c, Waive_Fine_Amount__c, Waiver_Type_by_FOSP__c,Waiver_Type_by_DIFC__c FROM Violation__c WHERE Related_Request__c=:stepRecord.SR__c];
        system.debug('lstViolations***'+lstViolations);
        wrap.stepRecord = stepRecord;
        wrap.violations = lstViolations;
        if(stepRecord.Step_Name__c == 'Verification By DIFC Fit - Out Team'){
            wrap.isDIFCStep = true;
        }
        return wrap;
        
    }
    
    @AuraEnabled
    public static void saveViolations(step__c stepRecord, string seletedType, List<Violation__c> violations, string comments, string selectedWaiver){
        steprecord.Violation_Type_By_FOSP__c = seletedType;
        stepRecord.Step_Notes__c = comments;
        stepRecord.Waiver_Type__c = selectedWaiver;
        system.debug('stepRecord***'+stepRecord);
        for(Violation__c violation : violations){
            if(violation.Waiver_Type_by_FOSP__c !=null && stepRecord.Step_Name__c == 'Verify & Approve by FOSP Manager'){
                violation.Waiver_Type_by_DIFC__c = violation.Waiver_Type_by_FOSP__c;
            }
        }
        update stepRecord;
        if(!violations.isEmpty()) update violations;
    }
    
    private static Step__c getStepRecord(string stepId){
        Step__c stepRecord = new Step__c();
        stepRecord = [Select Id,SR__c,Step_Name__c,Violation_Type_By_FOSP__c,SR_Step__r.Step_No__c,Step_Notes__c, Waiver_Type__c FROM Step__c WHERE id=:stepId];
        return stepRecord;
    }
    
    public class StepWrapper {
        @AuraEnabled public Step__c stepRecord;
        @AuraEnabled public List<Violation__c> violations;
        @AuraEnabled public boolean isDIFCStep;
    }
}