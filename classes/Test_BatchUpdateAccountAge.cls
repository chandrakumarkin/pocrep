@istest
public class Test_BatchUpdateAccountAge {
    
    @istest
    public static void testBatchUpdate(){
        Account acc  = new Account();
        acc.name = 'test';  
        acc.Is_Commercial_Permission__c = 'Yes';
        acc.Company_Type__c = 'Financial-Related';
        acc.ROC_Status__c = 'Active';
        acc.Sector_Classification__c = 'Authorized Firm';  
        acc.FinTech_Classification__c = 'Venture Capital New Regime';
        acc.ROC_reg_incorp_Date__c = System.today();
        insert acc;
                
        Account acc1  = new Account();
        acc1.name = 'test';  
        acc1.Is_Commercial_Permission__c = 'Yes';
        acc1.Company_Type__c = 'Financial-Related';
        acc1.ROC_Status__c = 'Active';
        acc1.Sector_Classification__c = 'Investment Fund';           
        acc.FinTech_Classification__c = 'Fund Manager New Regime';
        acc.ROC_reg_incorp_Date__c = System.today().addDays(-30);
        insert acc1;
        
        Test.startTest();
        Database.executeBatch(new Batch_UpdateAccountAge());
        Test.stopTest();       
        
    }

}