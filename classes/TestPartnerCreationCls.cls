/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
global class TestPartnerCreationCls implements WebServiceMock {
	
	public static list<SAPCRMWebService.ZSF_IN_LOG> lstItems = new list<SAPCRMWebService.ZSF_IN_LOG>();
	
   global void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
        //docSample.EchoStringResponse_element respElement = new docSample.EchoStringResponse_element();
        SAPCRMWebService.Z_SF_INBOUND_DATAResponse_element respElement = new SAPCRMWebService.Z_SF_INBOUND_DATAResponse_element();
        SAPCRMWebService.ZSF_IN_LOGT objSFOut = new SAPCRMWebService.ZSF_IN_LOGT();
        
        objSFOut.item = lstItems;
        respElement.EX_SF_OUT = objSFOut;
        response.put('response_x',respElement);
        //respElement.EchoStringResult = 'Mock response';
        //response.put('response_x', respElement); 
   }
}