public without sharing class OB_LtngQuickStepTransitionWrapper {
        @AuraEnabled
        public List<OB_TransitionWrapper> ltngLstTrnsWrap {get;set;}
        @AuraEnabled
        public Boolean hasAccess {get; set;}
        @AuraEnabled
        public Integer listSize {get; set;}
        @AuraEnabled
        public String selectTransition {get; set;}
        @AuraEnabled
        public String rejectReason {get; set;}
        @AuraEnabled
        public String stepNotes {get; set;}
        @AuraEnabled
        public HexaBPM__Step__c step_ltng {get; set;}
        @AuraEnabled
        public map<id,HexaBPM__Step_Transition__c> ltngMapStepTransition {get;set;}
        @AuraEnabled
        public string SRId {get; set;}
        @AuraEnabled
        public string SRRequestType {get; set;}
        @AuraEnabled
        public string SRNumber {get; set;}
        @AuraEnabled
        public string flsErrorMessage {get; set;}
        @AuraEnabled
        public boolean flsErrorCheck {get; set;}
        @AuraEnabled
        public boolean isStepOwnedByQueue {get;set;}
        @AuraEnabled
        public string userType {get; set;}
        
        public OB_LtngQuickStepTransitionWrapper(){
            this.ltngLstTrnsWrap = new List<OB_TransitionWrapper>();
            this.hasAccess = false;
            this.listSize=0;
            this.selectTransition = '';
            this.rejectReason = '';
            this.stepNotes = '';
            this.step_ltng = null;
            this.ltngMapStepTransition = null;
            this.SRId='';
            this.SRRequestType='';
            this.SRNumber='';
            this.flsErrorMessage='';
            this.flsErrorCheck=false;
            this.isStepOwnedByQueue = false;
        }
}