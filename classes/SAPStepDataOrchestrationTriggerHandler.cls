/**
 * @description       : Integration used to create and update Steps
 * @author            : Mudasir Wani
 * @group             : 
 * @last modified on  : 09-09-2020
 * @last modified by  : Mudasir Wani
 * Modifications Log 
 * Ver   Date           Author         Modification
 * 1.0   Sept-09-2020   Mudasir Wani   Initial Version
 * 1.1   Sept-26-2020   Mudasir Wani   Added code for closing the steps which are closed from SAP and open in Salesforce
**/
public class SAPStepDataOrchestrationTriggerHandler {
    public static boolean triggerInvoked = false;
    public static void afterInsertOperation(List<Temp_SAP_Step_Data__c> sapStepDataOrchestrationList){
        steppopulationDetails(sapStepDataOrchestrationList);
    }
    
    public static void afterUpdateOperation(Map<id,Temp_SAP_Step_Data__c> sapStepDataOrchestrationOldMap ,
                                                Map<id,Temp_SAP_Step_Data__c> sapStepDataOrchestrationNewMap){
        List<Temp_SAP_Step_Data__c> sapStepDataOrchestrationList = new List<Temp_SAP_Step_Data__c>();
        for(Temp_SAP_Step_Data__c stepOrchesRec : sapStepDataOrchestrationNewMap.values()){
            system.debug('stepOrchesRec ---'+stepOrchesRec);
            system.debug('---stepOrchesRec.SAP_ACOMP__c ---'+stepOrchesRec.SAP_ACOMP__c 
                                +'---sapStepDataOrchestrationOldMap.get(stepOrchesRec.id).SAP_ACOMP__c---'
                                +sapStepDataOrchestrationOldMap.get(stepOrchesRec.id).SAP_ACOMP__c);
            //Added code for version 1.1 - || (stepOrchesRec.Status_in_Sync__c=='No' && stepOrchesRec.SAP_ACOMP__c =='X')
            if(((stepOrchesRec.SAP_ACOMP__c =='X' && sapStepDataOrchestrationOldMap.get(stepOrchesRec.id).SAP_ACOMP__c != stepOrchesRec.SAP_ACOMP__c) 
                    || stepOrchesRec.step__c == Null || (stepOrchesRec.Status_in_Sync__c=='No' && stepOrchesRec.SAP_ACOMP__c =='X')) && triggerInvoked ==false){
                sapStepDataOrchestrationList.add(stepOrchesRec);
            }
        }
        if(sapStepDataOrchestrationList.size() > 0)steppopulationDetails(sapStepDataOrchestrationList);
    }
    public static void steppopulationDetails(List<Temp_SAP_Step_Data__c> sapStepDataOrchestrationList){
        triggerInvoked = true;
        system.debug('---steppopulationDetails ---');
        List<Step__c> stepsToUpsert = new List<Step__c>();
        Map<String,Temp_SAP_Step_Data__c> newListToUpdate = new Map<String,Temp_SAP_Step_Data__c>();
        for(Temp_SAP_Step_Data__c stepOrchesRec : sapStepDataOrchestrationList){
            Step__c step = new Step__c();
            step.Step_template__c =stepOrchesRec.Step_Template_Id__c;
            step.SR__c = stepOrchesRec.Service_request_ID__c;
            step.Step_Id__c = stepOrchesRec.SAP_Unique_Id__c;
            step.SAP_Seq__c = stepOrchesRec.SAP_Seq__c;
            step.Status__c = stepOrchesRec.SAP_ACOMP__c =='X' ? 'a1M20000001iL9ZEAU' :'a1M20000001j3zgEAA';
            step.OwnerId   = stepOrchesRec.Step_Owner_Id__c;
            step.Completion_Date__c = stepOrchesRec.Completion_Date__c;
            step.SAP_CTEXT__c = stepOrchesRec.SAP_CTEXT__c;
            step.SAP_CTOTL__c = stepOrchesRec.SAP_CTOTL__c;
            step.SAP_DOCCO__c = stepOrchesRec.SAP_DOCCO__c;
            step.SAP_DOCCR__c = stepOrchesRec.SAP_DOCCR__c;
            step.SAP_DOCDL__c = stepOrchesRec.SAP_DOCDL__c;
            step.SAP_DOCRC__c = stepOrchesRec.SAP_DOCRC__c;
            step.Fine_Amount__c = stepOrchesRec.Fine_Amount__c;
            step.ID_End_Date__c = stepOrchesRec.ID_End_Date__c;
            step.ID_Start_Date__c    = stepOrchesRec.ID_Start_Date__c ;
            step.Push_to_SAP__c = stepOrchesRec.Push_to_SAP__c;
            step.SAP_ACRES__c = stepOrchesRec.SAP_ACRES__c;
            step.SAP_BANFN__c = stepOrchesRec.SAP_BANFN__c;
            step.SAP_BIOMT__c = stepOrchesRec.SAP_BIOMT__c;
            step.SAP_BNFPO__c = stepOrchesRec.SAP_BNFPO__c;
            step.SAP_CMAIL__c = stepOrchesRec.SAP_CMAIL__c;
            step.SAP_CRMAC__c = stepOrchesRec.SAP_CRMAC__c;
            step.SAP_EACTV__c = stepOrchesRec.SAP_EACTV__c;
            step.SAP_EMAIL__c = stepOrchesRec.SAP_EMAIL__c;
            step.SAP_ERDAT__c = stepOrchesRec.SAP_ERDAT__c;
            step.SAP_ERNAM__c = stepOrchesRec.SAP_ERNAM__c;
            step.SAP_ERTIM__c = stepOrchesRec.SAP_ERTIM__c;
            step.SAP_ETIME__c = stepOrchesRec.SAP_ETIME__c;
            step.SAP_FINES__c = stepOrchesRec.SAP_FINES__c;
            step.SAP_FMAIL__c = stepOrchesRec.SAP_FMAIL__c;
            step.SAP_GSDAT__c = stepOrchesRec.SAP_GSDAT__c;
            step.SAP_GSTIM__c = stepOrchesRec.SAP_GSTIM__c;
            step.SAP_IDDES__c = stepOrchesRec.SAP_IDDES__c;
            step.SAP_IDEDT__c = stepOrchesRec.SAP_IDEDT__c;
            step.SAP_IDNUM__c = stepOrchesRec.ID_Number__c;
            step.SAP_IDSDT__c = stepOrchesRec.SAP_IDSDT__c;
            step.SAP_MATNR__c = stepOrchesRec.SAP_MATNR__c;
            step.SAP_IDTYP__c = stepOrchesRec.SAP_IDTYP__c;
            step.SAP_MEDIC__c = stepOrchesRec.SAP_MEDIC__c;
            step.SAP_MEDRT__c = stepOrchesRec.SAP_MEDRT__c;
            step.SAP_MEDST__c = stepOrchesRec.SAP_MEDST__c;
            step.SAP_NMAIL__c = stepOrchesRec.SAP_NMAIL__c;
            step.SAP_MDATE__c = stepOrchesRec.Medical_Scheduled_Date__c;
            step.SAP_POSNR__c = stepOrchesRec.SAP_POSNR__c;
            step.SAP_RESCH__c = stepOrchesRec.SAP_RESCH__c;
            step.SAP_RMATN__c = stepOrchesRec.SAP_RMATN__c;
            step.SAP_TMAIL__c = stepOrchesRec.SAP_TMAIL__c;
            step.SAP_UNQNO__c = stepOrchesRec.SAP_UNQNO__c;
            //step.Updated_from_SAP__c = stepOrchesRec.Updated_from_SAP__c;
            step.SAP_MFDAT__c = stepOrchesRec.Medical_First_Scheduled_Date__c;
            step.UID_Number__c = stepOrchesRec.UID_Number__c;
            
            system.debug('Mudasir--------Before ---'+step);
            //upsert step Step_Id__c;
            system.debug('Mudasir---------'+step);
            newListToUpdate.put(stepOrchesRec.Step_Id__c,stepOrchesRec);
            stepsToUpsert.add(step);
        }
        if(stepsToUpsert.size() > 0){
            upsert stepsToUpsert Step_Id__c;
            /*for(Step__c stepRecValue : stepsToUpsert){
                if(newListToUpdate.get(stepRecValue.Step_Id__c) != NULL){
                    newListToUpdate.get(stepRecValue.Step_Id__c).step__c = stepRecValue.id;
                }
            }*/
        }       
       //update newListToUpdate.values();
    }
}