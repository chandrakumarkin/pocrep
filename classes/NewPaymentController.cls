public with sharing class NewPaymentController {
    
    //V2.8
    private Map<String,String> oPassedParams = new Map<String,String>();
    //String secretKey = Label.ADCBsecretKey; // Required for Portal Online Payment (Cybersource PG's secretkey)
    String secretKey = Label.ADCB_Portal_secretkey; // Required for Portal Online Payment (Cybersource PG's secretkey)
    public string access_key{get;set;}
    public string profile_id{get;set;}
    public string transaction_uuid{get;set;}
    public string unsigned_field_names{get;set;}
    public string signed_date_time{get;set;}
    public string reference_number{get;set;}
    public Decimal amount{get;set;}
    public string currencyvall{get;set;}
    public string signature{get;set;}
    public string customer_ip_address{get;set;}
    public Boolean isPaySuccess { get; set; }
    public Id leaseIdValue{get;set;}
    public string strSRID{get;set;}    
   public boolean Iframe{get;set;}  
   Receipt__c    receiptObj;
   public NewPaymentController() 
   {
       if(apexpages.currentPage().getParameters().get('srId')!=null && apexpages.currentPage().getParameters().get('srId')!='') 
           strSRID = apexpages.currentPage().getParameters().get('srId');
       if(apexpages.currentPage().getParameters().get('amountToPay')!=null && apexpages.currentPage().getParameters().get('amountToPay')!='') 
       amount = Decimal.ValueOf(apexpages.currentPage().getParameters().get('amountToPay'));
       
       if(apexpages.currentPage().getParameters().get('opensame')==null)
       Iframe=true;
       else
       Iframe=false;

       string accountId;
       boolean isFitOutContractor = false;
       
    
      for(User usr : [Select Contact.AccountId,contact.Contractor__c,Contact.Account.RecordType.DeveloperName from User where Id =:UserInfo.getUserId()]){
       //accountId = usr.Contact.AccountID;
       if(usr.contact.Contractor__c != null){
           isFitOutContractor = (String.isNotBlank(usr.Contact.Contractor__c) && !usr.Contact.Contractor__c.equals(usr.Contact.AccountId) ) || 
                           usr.Contact.Account.RecordType.DeveloperName.equals('Contractor_Account');
       }
       accountId = isFitOutContractor && String.isNotBlank(usr.Contact.Contractor__c) ? usr.Contact.Contractor__c : usr.Contact.AccountId;
   }


          String receiptCardRecTypeId = Schema.SobjectType.Receipt__c.getRecordTypeInfosByDeveloperName().get('Card').getRecordTypeId();
     
       
              receiptObj = new Receipt__c(
            RecordTypeId = receiptCardRecTypeId,
            Amount__c = amount,
            Payment_Status__c = 'Pending',
            Receipt_Type__c = 'Card',
            Transaction_Date__c = System.now(),
            Customer__c = accountId
                      
        );

      
       
       
   }

   //V2.8
   public String getSignedData(){
       String result = '';
       result += '\n <input type="hidden" id="signature" name="signature" value="' + sign(buildDataToSign(oPassedParams),secretKey) + '"/>';
       //system.debug('-- getSignedData' + result);
       return result;
   }

   //V2.8
   private static String sign(String data, String secretKey) 
   {
       String result = EncodingUtil.base64Encode(Crypto.generateMac('hmacSHA256', Blob.valueOf(data), Blob.valueOf(secretKey)));
       System.debug('--ZZZ sign ->'+result);
       return result;
   }

    //V2.8
    private static String buildDataToSign(Map<string,string> paramsArray) 
    {
        try{
        String[] signedFieldNames = paramsArray.get('signed_field_names').Split(',');
        List<string> dataToSign = new List<string>();
        
        for (String oSignedFieldName : signedFieldNames){
            dataToSign.Add(oSignedFieldName + '=' + paramsArray.get(oSignedFieldName));
        }
        
        system.debug('--commaSeparate-->' + commaSeparate(dataToSign));
        return commaSeparate(dataToSign);
        }
        catch(Exception ex){
            return 'test';
        }
    }

     
       //V2.8
   private static String commaSeparate(List<string> dataToSign) {
       String result = '';
       for(String Str : dataToSign) {
           result += (result==''?'':',')+Str;
       }
       return result;                         
   }

   public String getParametersValuesHidden(){
       String result = '';
       for (String oKey : oPassedParams.KeySet()){ 
           result += '\n <input type="hidden" id="' + oKey + '" name="' + oKey + '" value="' + oPassedParams.get(oKey) + '" />';
       }
       system.debug('--ParametersValuesHidden' + result);
       return result;
   }

   public void setupPGForm(){
       //strSRID

      
        insert receiptObj;


      // V3.0 
           //ADCB
           access_key=Label.ADCB_Portal_accesskey;//'ac12abb2254f3af781877d4767b957c9';
           profile_id=Label.ADCB_Portal_profile_id;//'F852B06D-14BB-4F28-A1FB-E850222BFB51';
           
           transaction_uuid = getUUID();
           signed_date_time = getUTCDateTime();
           
           reference_number = receiptObj.id+'_TOPUP'; //TotalPriceItemVal
           
            if(String.isNotBlank(strSRID ) && strSRID != 'null'){
              reference_number      = strSRID+'_'+receiptObj.Id+'_CPSR';   //For CP-Account Initial SR Payment//
          }else{
              reference_number      = receiptObj.Id+'_TOPUP';   //For Topup Only//
          }
          
           
           //amount=string.valueOf(TotalPriceItemVal);
           currencyvall='aed';
           unsigned_field_names='';
           customer_ip_address  = (Test.isRunningTest() ? '' : Auth.SessionManagement.getCurrentSession()?.get('SourceIp'));
           
           oPassedParams.put('access_key',access_key);
           oPassedParams.put('profile_id',profile_id);
           oPassedParams.put('transaction_uuid',transaction_uuid);
           oPassedParams.put('customer_ip_address',customer_ip_address);
           oPassedParams.put('signed_field_names','access_key,customer_ip_address,profile_id,transaction_uuid,signed_field_names,unsigned_field_names,signed_date_time,locale,transaction_type,reference_number,amount,currency');
           
           oPassedParams.put('unsigned_field_names',unsigned_field_names);
           oPassedParams.put('signed_date_time',signed_date_time);
           oPassedParams.put('locale','en');
           oPassedParams.put('transaction_type','sale');
           oPassedParams.put('reference_number',reference_number);
           oPassedParams.put('amount',string.valueof(receiptObj.Amount__c));
           oPassedParams.put('currency',currencyvall);
       
   }

   //V2.8
   private static String getUTCDateTime() 
   {
       DateTime oUTSDateTime = System.now().addHours(-4);
       String strUTCDateTime = oUTSDateTime.format('yyyy-MM-dd\'T\'HH:mm:ss\'Z\'');
       //system.debug('--strUTCDateTime -->' + strUTCDateTime );
       return strUTCDateTime;
   } 
   
   //V2.8
   public String getUUID(){
       Blob b = Crypto.generateAesKey(128);
       String h = EncodingUtil.convertToHex(b);
       String guid = h.substring(0,8) + '-' + h.substring(8,12) + '-' + h.substring(12,16) + '-' + h.substring(16,20) + '-' + h.substring(20);       
       //system.debug('--guid -->' + guid );
       return guid;
   }
}