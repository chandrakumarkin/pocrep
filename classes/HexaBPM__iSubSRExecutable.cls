/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global interface iSubSRExecutable {
    void Initiate_SUB_LinkedSR(Map<Id,Id> param0, Map<Id,Id> param1, Map<Id,Id> param2, Map<Id,Id> param3, Set<Id> param4, Set<Id> param5);
}
