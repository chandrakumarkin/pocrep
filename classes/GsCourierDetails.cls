/*
    Author      :   Ravi
    Description :   This class holds all GS related Courier Services 
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    09-02-2015  Ravi            Created   
    V1.1    13-06-2016  Sravan          Assign the courier step created to ARAMEX QUEUE THT # 1970
    v1.2    23-04-2021  Veera           Shippment should not create for Courier Delivery if Fine amount is pening 
*/
public without sharing class GsCourierDetails {
    public static Boolean IsFired = false;
    public static string CreateCourierStep(list<Step__c> lstSteps){
        
        map<string,string> mapStatus = new map<string,string>();
        map<string,string> mapTemplates = new map<string,string>();
         map<string,string> mapTemplateName = new map<string,string>();//V1.2
        
        list<Step__c> lstCourierSteps = new list<Step__c>();
        string res = 'Success';
        map<Id,Id> mapIds = new map<Id,Id>();
        map<Id,Id> mapAutoClosureIds = new map<Id,Id>();
        map<Id,Id> mapPreStepIds = new map<Id,Id>();
        
        // V1.1
        list<Group> aramexQueue = new list<Group>([select id,name from group where type ='queue' and developername ='ARAMEX' limit 1]);
        
        
        
        //for auto closure of Activities
        for(Step__c objS : lstSteps){
            if(objS.SAP_DOCCR__c == 'X' && objS.Step_Name__c != 'Front Desk Review' && objS.Status_Code__c != 'AWAITING_ORIGINALS' && objS.Is_Letter__c == false && objS.SR_Record_Type__c != 'PO_BOX' && objS.SR_Record_Type__c != 'Add_a_New_Service_on_the_Index_Card'){
                mapIds.put(objS.Id,objS.SR__c);
            }
        }
        if(!mapIds.isEmpty()){
            //mapAutoClosureIds
            for(Service_Request__c objSR : [select Id,Name,(select Id,SAP_DOCRC__c,SAP_Seq__c,Courier_Enabled__c from Steps_SR__r where SAP_Seq__c != null order by SAP_Seq__c desc) from Service_Request__c where Id IN :mapIds.values()]){
                Id StepId;
                String Seq;
                for(Step__c objS : objSR.Steps_SR__r){
                    if(mapIds.containsKey(objS.Id) == false){
                        if(objS.SAP_DOCRC__c == 'X' && Seq != null && Seq != '' && objS.Courier_Enabled__c){ 
                            // Courier_Enabled__c true means it's closed from Aramex response - if User manually closed the Step then Auto Closure should not happen
                            Integer iP = Integer.valueOf(objS.SAP_Seq__c);
                            Integer iC = Integer.valueOf(Seq);
                            if((iC - iP) == 10 ){
                                mapAutoClosureIds.put(StepId,StepId);
                                mapPreStepIds.put(objS.Id,StepId);
                            }
                            break;
                        }
                    }else{
                        StepId = objS.Id;
                        Seq = objS.SAP_Seq__c;
                    }
                }
            }
            
        }
        
        if(GsCourierDetails.IsFired == false){
            try{
                for(Status__c obj : [select Id,Code__c from Status__c where Code__c='PENDING_COLLECTION' OR Code__c='PENDING_DELIVERY']){
                    mapStatus.put(obj.Code__c,obj.Id);
                }
                for(Step_Template__c obj : [select Id,Code__c,name from Step_Template__c where Code__c='GS_COURIER_COLLECTION' OR Code__c='GS_COURIER_DELIVERY']){
                    mapTemplates.put(obj.Code__c,obj.Id);
                    mapTemplateName.put(obj.Code__c,obj.name);//V1.2
                }       
                for(Step__c objS : lstSteps){
                    if(mapAutoClosureIds.containsKey(objS.Id) == false){
                        if((objS.SAP_DOCDL__c == 'X' || objS.SAP_DOCRC__c == 'X' || objS.SAP_DOCCO__c == 'X' || objS.SAP_DOCCR__c == 'X') || 
                            (objS.Step_Name__c == 'Front Desk Review' && objS.Status_Code__c == 'AWAITING_ORIGINALS') ||
                            ((objS.Is_Letter__c || objS.SR_Record_Type__c == 'PO_BOX' || objS.SR_Record_Type__c == 'Add_a_New_Service_on_the_Index_Card') && (objS.Step_Name__c == 'Document Collected' || objS.Step_Name__c == 'Collected')) 
                        ){
                            Step__c objCStep = new Step__c();
                            objCStep.SR__c = objS.SR__c;
                            objCStep.Client_Name__c = objS.Customer_Name__c;
                            String Code = (objS.SAP_DOCDL__c == 'X' || objS.SAP_DOCRC__c == 'X') ? 'PENDING_DELIVERY' : 'PENDING_COLLECTION';
                            Boolean isReturn = (objS.SAP_DOCRC__c == 'X' || objS.SAP_DOCCR__c == 'X') ? true : false;
                            if(objS.Is_Letter__c && (objS.Step_Name__c == 'Document Collected' || objS.Step_Name__c == 'Collected'))
                                Code = 'PENDING_DELIVERY';
                            if(objS.SR_Record_Type__c == 'PO_BOX' || objS.SR_Record_Type__c == 'Add_a_New_Service_on_the_Index_Card')
                                Code = 'PENDING_DELIVERY';
                            if(objS.Step_Name__c == 'Front Desk Review' && objS.Status_Code__c == 'AWAITING_ORIGINALS')
                                Code = 'PENDING_COLLECTION';
                            if(mapStatus.containsKey(Code))
                                objCStep.Status__c = mapStatus.get(Code);
                            Code = (Code == 'PENDING_DELIVERY') ? 'GS_COURIER_DELIVERY' : 'GS_COURIER_COLLECTION';
                            if(mapTemplates.containsKey(Code))
                            objCStep.Step_Template__c = mapTemplates.get(Code);
                            objCStep.Sys_Courier_Check__c = true;
                            objCStep.Parent_Step__c = objS.Id;
                            objCStep.SR_Group__c = objS.SR_Group__c;
                            objCStep.SAP_CTOTL__c = objS.SAP_CTOTL__c;
                            objCStep.SAP_CTEXT__c = objS.SAP_CTEXT__c;
                            objCStep.SAP_DOCCO__c = objS.SAP_DOCCO__c;
                            objCStep.SAP_DOCCR__c = objS.SAP_DOCCR__c;
                            objCStep.SAP_DOCDL__c = objS.SAP_DOCDL__c;
                            objCStep.SAP_DOCRC__c = objS.SAP_DOCRC__c;
                            objCStep.Fine_Amount__c = objS.SR_Fine_Amount__c;//V1.2
                            if(mapTemplateName.containsKey(Code))         //V1.2
                            objCStep.Courier_Step_Name__C =mapTemplateName.get(code);////V1.2
                            // V1.1 Start
                            system.debug('Step owner+++++++'+aramexQueue);
                            if(aramexQueue !=null && aramexQueue.size()>0 )
                            objCStep.ownerID = aramexQueue[0].id;
                            // V1.1 END 
                           
                            lstCourierSteps.add(objCStep);
                        }
                    }/*else{ // Automatically close the Step
                        AutoClosureOfStep(objS.Id);
                    }*/
                }
                if(!lstCourierSteps.isEmpty()){
                    GsCourierDetails.IsFired = true;
                    insert lstCourierSteps;
                    
                    system.debug('AirWay Bill creation Log');
                    
                    
                    
                    for(Step__c obj : lstCourierSteps){
                    
                    system.debug(obj  + 'Veera added to get the Airway Bill');
                    
                    if(system.label.Old_Courier_Not_Execute == 'True'){
                    
                    system.debug( obj.Fine_Amount__c +'Indide the new logic Veera added to get the Airway Bill');
                    
                          if((obj.Fine_Amount__c <= 0 || obj.Fine_Amount__c == null ) && system.isFuture() == false && system.isBatch() == false){//v1.2 
                          
                            system.debug('Inside the process');
                            ProcessGSCourier(obj.Id);
                            
                            
                            
                            }
                          else{
                             
                            if(obj.Fine_Amount__c  > 0 && obj.Courier_Step_Name__C !='Courier Delivery'  && system.isFuture() == false && system.isBatch() == false ){
                                
                               System.debug('Aramex Inside first If');
                                ProcessGSCourier(obj.Id);
                            }    
                         
                            if( obj.Fine_Amount__c  > 0  && obj.Courier_Step_Name__C =='Courier Delivery' && system.isFuture() == false && system.isBatch() == false ){
                               System.debug('Aramex Inside first If 1');
                                if(!GSUtilityClass.CheckServiceRequestForFine(obj.SR__c, obj.Fine_Amount__c )){
                                   System.debug('Aramex Inside first If2');
                                     ProcessGSCourier(obj.Id);
                                 }
                           }
                         
                         } 
                       
                       } else {
                       
                            ProcessGSCourier(obj.Id);
                       }  // end v1.2
                            
                    }
                    
                }
                if(!mapAutoClosureIds.isEmpty()){
                    list<Step__c> lstUpdate = new list<Step__c>();
                    Step__c temp;
                    for(Id SId : mapAutoClosureIds.keySet()){
                        temp = new Step__c(Id=SId);
                        temp.Courier_Enabled__c = true;
                        temp.Is_Auto_Closure__c = true;
                        temp.Sys_Courier_Check__c = true;
                        lstUpdate.add(temp);
                    }
                    update lstUpdate;
                    if(system.isFuture() == false && system.isBatch() == false ){
                        for(Id SId : mapAutoClosureIds.keySet()){
                            AutoClosureOfStep(SId);
                        }
                    }
                }
            }catch(Exception ex){
                res = ex.getMessage();
            }
        }
        return res;
    }
    
    @future(callout=true)
    public static void ProcessGSCourier(String CourierStepId){
        if(test.isRunningTest() == false)
            CC_CourierCls.CreateShipmentNormal(CourierStepId);
    }
    @future(callout=true)
    public static void AutoClosureOfStep(String CourierStepId){
        GsSapWebServiceDetails.CurrentUserId = 'ARAMEX';
        GsSapWebServiceDetails.IsClosed = true;
        string result = GsSapWebServiceDetails.PushActivityToSAPNew(new list<string>{CourierStepId});
    }
    
    public static void CloseAwaitingOriginals(list<Id> lstS){
        list<Step__c> lstSteps = new list<Step__c>();
        list<Shipment__c> lstShips = new list<Shipment__c>();
        if(lstS != null && !lstS.isEmpty()){
            map<string,string> mapStatus = new map<string,string>();
            for(Status__c obj : [select Id,Code__c from Status__c where Code__c='CLOSED']){
                mapStatus.put(obj.Code__c,obj.Id);
            }
            for(Step__c obj : [select Id,Status__c,Status_Code__c,(select Id,Status__c from Shipments__r where Status__c !='Delivered') from Step__c where Parent_Step__c IN :lstS AND (Status_Code__c = 'PENDING_DELIVERY' OR Status_Code__c = 'PENDING_COLLECTION')]){
                String Code = 'CLOSED';//obj.Status_Code__c == 'PENDING_DELIVERY' ? 'DELIVERED' : 'COLLECTED';
                if(mapStatus.get(Code) != null){
                    obj.Status__c = mapStatus.get(Code);
                    lstSteps.add(obj);
                }
                if(obj.Shipments__r != null){
                    for(Shipment__c objSh : obj.Shipments__r){
                        objSh.Status__c = 'Closed';
                        lstShips.add(objSh);
                    }
                }
            }
            if(!lstSteps.isEmpty())
                update lstSteps;
            if(!lstShips.isEmpty())
                update lstShips;
        }
    }
    public static void CloseRocShipments(list<Id> lstS){
        list<Shipment__c> lstShips = new list<Shipment__c>();
        if(lstS != null && !lstS.isEmpty()){
            for(Step__c obj : [select Id,Status__c,Status_Code__c,(select Id,Status__c from Shipments__r where Status__c !='Delivered') from Step__c where Id IN :lstS]){
                if(obj.Shipments__r != null){
                    for(Shipment__c objSh : obj.Shipments__r){
                        objSh.Status__c = 'Closed';
                        lstShips.add(objSh);
                    }
                }
            }
            if(!lstShips.isEmpty())
                update lstShips;
        }
    }
}