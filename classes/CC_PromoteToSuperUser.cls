/*
    Author      : Durga Prasad
    Date        : 13-Mar-2020
    Description : Custom code to promote normal user to super user
    ---------------------------------------------------------------------------------------------------------------------------
*/
global without sharing class CC_PromoteToSuperUser implements HexaBPM.iCustomCodeExecutable{
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        
        string strResult = 'Success';
        List<Id> userIds = new List<Id>();
        List<Id> accountIds = new List<Id>();
        
        if(stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Contact__c!=null && stp.HexaBPM__SR__r.HexaBPM__Customer__c!=null){
            try{
                
                AccountContactRelation objAccContactRel = new AccountContactRelation();
               
                for(AccountContactRelation objRel:[Select Id,ContactId,AccountId,Roles from AccountContactRelation where AccountId=:stp.HexaBPM__SR__r.HexaBPM__Customer__c and ContactId=:stp.HexaBPM__SR__r.HexaBPM__Contact__c and IsActive=true]){
                    objAccContactRel = objRel;
                    userIds.add(objRel.ContactId);
                    accountIds.add(objRel.AccountId);
                } 
                
                objAccContactRel.Roles=  stp.HexaBPM__SR__r.Community_User_Roles__c; 
               // objAccContactRel.is_Super_user__c = true;       
                if(objAccContactRel.Id!=null)
                    update objAccContactRel;
               
                if(!userIds.isEmpty()){
                     UpdateUserRole(userIds,accountIds);
                }
                
             }catch(Exception e){
                strResult = e.getMessage()+'';
            }
        }
        return strResult;
    }
    
  @future
  public static void UpdateUserRole(List<Id> recordIds,List<Id> accountIds) {
    
    List<User> thisUser = [SELECT Id,ContactId,Community_User_Role__c FROM USER WHERE contactId IN : recordIds AND Contact.AccountId IN :accountIds];
    for(AccountContactRelation objRel:[Select Id,Roles from AccountContactRelation where ContactId IN :recordIds and IsActive=true AND IsDirect = true AND Contact.RecordTypeId ='0122000000033K5']){
        thisUser[0].Community_User_Role__c = objRel.Roles;          
    } 
    Update thisUser;
  }

}