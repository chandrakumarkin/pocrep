/**********************************************************************************************************************
* Name               : OB_CalculateDaysBreachedOnStepsBatch
* Description        : This batch class is used to update the CP SR open steps where SLA is violated
* Created Date       : 07-12-2020
* Created By         : PWC Digital Middle East
* --------------------------------------------------------------------------------------------------------------------*
* Version       Author                  Date             Comment
* 1.0           salma.rasheed@pwc.com   07-12-2020       Initial Draft
**********************************************************************************************************************/
global class OB_CalculateDaysBreachedOnStepsBatch implements Database.Batchable<sObject>,Schedulable  {

    /**********************************************************************************************
    * @Description  : Scheduler execute method to schedule code execution
    * @Params       : SchedulableContext        scheduling details
    * @Return       : none
    **********************************************************************************************/ 
    global void execute(SchedulableContext sc) {
        Database.executeBatch(this,10);
    }

    /**********************************************************************************************
    * @Description  : Batch start method to define the context
    * @Params       : BatchableContext        batch context
    * @Return       : Query locator
    **********************************************************************************************/ 
    global Database.QueryLocator start(Database.BatchableContext bc) {
        
        // Get all the CP SR Steps assigned to client queue & still open
        Date today = Date.today();
        String queryString = 'Select id,CreatedDate,HexaBPM__Due_Date__c,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR_Step__r.OwnerId from HexaBPM__Step__c where HexaBPM__SR__r.HexaBPM__Record_Type_Name__c=\'Commercial_Permission\' AND HexaBPM__Status_Type__c!=\'End\' AND HexaBPM__Due_Date__c<:today';
        system.debug(queryString);
        //if(test.isRunningTest()){queryString='select id from HexaBPM__Step__c ';}
        return Database.getQueryLocator(queryString);
    }

    /**********************************************************************************************
    * @Description  : Batch execute method to process the records in current scope
    * @Params       : scope     records in current scope
    * @Return       : none
    **********************************************************************************************/ 
    global void execute(Database.BatchableContext bc, List<HexaBPM__Step__c> scope){
        List<HexaBPM__Step__c> stepsList = new List<HexaBPM__Step__c>();
        try{
            Map<Id,Id> queueBusinessHourMap = new Map<Id,Id>();
            for(OBBusinessHour__c objBusinessHour : [SELECT id,Business_Hour_Id__c,Queue_Id__c FROM OBBusinessHour__c]){
                queueBusinessHourMap.put(Id.valueOf(objBusinessHour.Queue_Id__c), Id.valueOf(objBusinessHour.Business_Hour_Id__c));
            }
            
            Map<Id,HexaBPM__Step__c> stepMap = new Map<Id,HexaBPM__Step__c>();
            for(HexaBPM__Step__c objStep : scope){
                Long ms = BusinessHours.diff(queueBusinessHourMap.get(objStep.HexaBPM__SR_Step__r.OwnerId), objStep.HexaBPM__Due_Date__c, System.Now());
                system.debug(ms);
                Decimal decMS = Decimal.valueOf(ms);
                system.debug('MS=>'+decMS);
                Decimal minutes= (((decMS)/1000)/60).setscale(1);
                system.debug('minutes=>'+minutes);
                Decimal hours = minutes/60;
                system.debug('hours=>'+hours);
                Integer days = Integer.valueOf(hours/8);
                system.debug('days=>'+days);
                stepsList.add(new HexaBPM__Step__c(id=objStep.Id,OB_Days_Breached__c=days));
            }
            system.debug('stepsList=>'+stepsList);
            if(!stepsList.isEmpty()) update stepsList;
        }catch (Exception e){  Log__c objLog = new Log__c(Description__c = 'Exception on OB_CalculateDaysBreachedOnStepsBatch.execute() :'+e.getStackTraceString(),Type__c = 'Activate Deactivate CP');    insert objLog;}
    }

    /**********************************************************************************************
    * @Description  : Batch finish method
    * @Params       : BatchableContext  batch contect details
    * @Return       : none
    **********************************************************************************************/ 
    global void finish(Database.BatchableContext bc){}    
}