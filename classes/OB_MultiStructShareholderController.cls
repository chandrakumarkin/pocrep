/******************************************************************************************
*  Name        : OB_MultiStructShareholderController 
*  Author      : Maruf Bagwan
*  Company     : PwC
*  Date        : 29-March-2020
*  Description : Controller for Multi Structure Activity Controller 
----------------------------------------------------------------------------------------                               
Modification History 
----------------------------------------------------------------------------------------
V.No      Date          Updated By          Description
----------------------------------------------------------------------------------------              
V1.0   29-March-2020   Maruf Bagwan         Initial Version
V1.1   01-June-2020    Sai                  #59
*******************************************************************************************/
public without sharing class OB_MultiStructShareholderController{
    //reqest wrpper
    public static OB_MultiStructShareholderController.RequestWrapper reqWrap;
    //response wrpper
    public static OB_MultiStructShareholderController.ResponseWrapper respWrap;
    /*
	* Lightning component: OB_ManageShareholders
	* On load will fetch existing list of Service Request
	*/
    @AuraEnabled
    public static ResponseWrapper getExistingServiceRequest(String reqWrapPram){
        try{
            //reqest wrpper
            reqWrap = (OB_MultiStructShareholderController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_MultiStructShareholderController.RequestWrapper.class);
            //response wrpper
            respWrap = new OB_MultiStructShareholderController.ResponseWrapper();
            String srId = reqWrap.srId;
            
            respWrap.srList = new List<HexaBPM__Service_Request__c>();
            for(HexaBPM__Service_Request__c serObj :[SELECT id, Entity_Name__c,
                                                     Business_Sector__c, Entity_Type__c, Type_of_Entity__c, 
                                                     legal_structures__c, License_Application__c, 
                                                     HexaBPM__External_SR_Status__c, HexaBPM__External_SR_Status__r.Name, 
                                                     HexaBPM__Internal_SR_Status__c, HexaBPM__Internal_Status_Name__c
                                                     FROM HexaBPM__Service_Request__c
                                                     WHERE HexaBPM__Parent_SR__c = :srId
                                                     Limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())]) 
            { 
                respWrap.srList.add(serObj);
            }
        }catch(exception exp){
            respWrap.errorMessage = exp.getMessage();
        }
        return respWrap;
    }
    /*
* Lightning component: OB_ManageShareholders
* On load will fetch existing list of Shareholder amendment of Service Request
*/
    @AuraEnabled
    public static ResponseWrapper getExistingAmendment(String reqWrapPram)
    {
        
        system.debug('##########getExistingAmendment##########');
        //reqest wrpper
        reqWrap = (OB_MultiStructShareholderController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_MultiStructShareholderController.RequestWrapper.class);
        //response wrpper
        respWrap = new OB_MultiStructShareholderController.ResponseWrapper();
        
        String srId = reqWrap.srId;
        String entityTypeLabel = reqWrap.entityTypeLabel;
        
        respWrap = getAmendmentList(srId ,respWrap);
        
        
        for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, 
                                                    HexaBPM__Component_Label__c, 
                                                    name 
                                                    FROM HexaBPM__Section_Detail__c 
                                                    WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
                                                    AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) 
        {
            respWrap.ButtonSection = sectionObj;
        }
        return respWrap;
        
    }
    /*
* Lightning component: OB_ManageShareholders
* On load will fetch  list of Shareholder amendment of Service Request
*/
    public static ResponseWrapper getAmendmentList(String srId,ResponseWrapper respWrap) 
    {
        String declarationIndividualText = '';
        String declarationCorporateText = '';
        Set<String> existNonShareHolderAmed = new Set<String>{  'Approved Person','Authorised Signatory','Authorized Representative','Company Secretary','Director','Exempt Entity','Guardian','Signatory','UBO','Portal User'};
            
            for(Declarations__mdt declare :[Select ID, Declaration_Text__c, Application_Type__c, Field_API_Name__c, Record_Type_API_Name__c, Object_API_Name__c
                                            From Declarations__mdt
                                            Where (Record_Type_API_Name__c = 'Individual' OR Record_Type_API_Name__c = 'Body_Corporate')
                                            AND Field_API_Name__c = 'I_Agree_Shareholder__c'
                                            AND Object_API_Name__c = 'HexaBPM_Amendment__c']) 
        {
            
            if(String.IsNotBlank(declare.Record_Type_API_Name__c) && declare.Record_Type_API_Name__c.EqualsIgnorecase('Individual')) {
                declarationIndividualText = declare.Declaration_Text__c;
            } 
            else if(String.IsNotBlank(declare.Record_Type_API_Name__c) && declare.Record_Type_API_Name__c.EqualsIgnorecase('Body_Corporate')) {
                declarationCorporateText = declare.Declaration_Text__c;
            }
            
        }
        respWrap.srList = new List<HexaBPM__Service_Request__c>();
        for(HexaBPM__Service_Request__c serObj :[SELECT id, Entity_Name__c,
                                                 Business_Sector__c, Entity_Type__c, Type_of_Entity__c, 
                                                 legal_structures__c, License_Application__c, 
                                                 HexaBPM__External_SR_Status__c, HexaBPM__External_SR_Status__r.Name, 
                                                 HexaBPM__Internal_SR_Status__c, HexaBPM__Internal_Status_Name__c
                                                 FROM HexaBPM__Service_Request__c
                                                 WHERE HexaBPM__Parent_SR__c = :srId OR id =: srId
                                                 Limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())]) 
        { 
            respWrap.srList.add(serObj);
        }
        
        String flowId = reqWrap.flowId;
        string licenseId;
        
        system.debug('##########getAmendmentList##########' + srId);
        OB_MultiStructShareholderController.SRWrapper srWrap = new OB_MultiStructShareholderController.SRWrapper();
        for(HexaBPM__Service_Request__c serObj :[SELECT id, Business_Sector__c, Entity_Type__c, Type_of_Entity__c, 
                                                 legal_structures__c, License_Application__c, 
                                                 HexaBPM__External_SR_Status__c, HexaBPM__External_SR_Status__r.Name, 
                                                 HexaBPM__Internal_SR_Status__c, HexaBPM__Internal_Status_Name__c 
                                                 FROM HexaBPM__Service_Request__c
                                                 WHERE id = :srId
                                                 Limit :(Limits.getLimitQueryRows() - Limits.getQueryRows())]) {
                                                     
                                                     licenseId = serObj.License_Application__c;
                                                     system.debug('===inside service request===');
                                                     srWrap.srObj = serObj;
                                                     srWrap.amedWrapLst = new List<OB_MultiStructShareholderController.AmendmentWrapper>();
                                                     srWrap.shareholderAmedWrapLst = new List<OB_MultiStructShareholderController.AmendmentWrapper>();
                                                     
                                                     declarationIndividualText = OB_DIFCNamingUtility.manageDIFCLable(serObj, declarationIndividualText);
                                                     declarationCorporateText =  OB_DIFCNamingUtility.manageDIFCLable(serObj,declarationCorporateText);
                                                     respWrap.entityTypeLabel =  OB_DIFCNamingUtility.manageDIFCLable(serObj,'Shareholder');
                                                     
                                                     srWrap.declarationIndividualText = declarationIndividualText;
                                                     srWrap.declarationCorporateText = declarationCorporateText;
                                                     
                                                     //Always one SR so can be in loop.
                                                     respWrap.srWrap = srWrap;
                                                     
                                                 }
        
        for(HexaBPM_Amendment__c amed : [SELECT Id,Name__c,OB_Linked_Multistructure_Applications__c,OB_MultiStructure_Parent_SR__c,
                                         Registration_Date__c,First_Name__c,Last_Name__c,Middle_Name__c,OB_MultiStructure_SR__c, 
                                         Nationality_list__c, Date_of_Birth__c, Address__c, 
                                         I_Agree_Shareholder__c, Passport_No__c, RecordType.DeveloperName, 
                                         Please_select_if_applicable__c,Type_of_Ownership_or_Control__c, 
                                         Country_of_source_of_income__c,Title__c, Passport_Issue_Date__c, 
                                         Date_Of_Becoming_a_UBO__c, Are_you_a_resident_in_the_U_A_E__c, Gender__c, 
                                         Former_Name__c, Place_of_Birth__c, Place_of_Registration__c, 
                                         Company_Name__c, Country_of_Registration__c, 
                                         Passport_Expiry_Date__c, Place_of_Issue__c, Entity_Name__c, Entity_Name__r.Name, 
                                         Email__c, Mobile__c, PO_Box__c, Will_Individual_Sign_AoA__c, Source_and_origin_of_funds_for_entity__c, 
                                         Post_Code__c, Emirate_State_Province__c, Amount_of_Contribution__c, Source_of_income_wealth__c, 
                                         Apartment_or_Villa_Number_c__c, Building_Name__c, Type_of_Contribution__c, Is_this_Entity_registered_with_DIFC__c, 
                                         Permanent_Native_City__c, Permanent_Native_Country__c, Partner_Type__c, Other_contribution_type__c, 
                                         Is_this_individual_a_PEP__c, Certified_passport_copy__c, E_I_D_no__c, 
                                         Relationship_Type__c, Role__c, Is_this_member_a_designated_Member__c, Registration_No__c, 
                                         DI_Permanent_Native_Country__c, DI_Email__c, DI_Emirate_State_Province__c, DI_Address__c, DI_Apartment_or_Villa_Number_c__c, 
                                         DI_Building_Name__c, DI_Permanent_Native_City__c, Is_the_shareholder_under_formation__c,OB_MultiStructure_SR__r.Recordtype.Name,
                                         DI_First_Name__c, DI_Last_Name__c, DI_PO_Box__c, DI_Post_Code__c, DI_Mobile__c, Individual_Corporate_Name__c 
                                         FROM HexaBPM_Amendment__c
                                         WHERE OB_MultiStructure_SR__c =:srId AND (RecordType.DeveloperName = 'Body_Corporate' OR RecordType.DeveloperName = 'Individual')
                                         AND Role__c != null]){  
                                             system.debug('===inside amendment===');
                                             OB_MultiStructShareholderController.AmendmentWrapper amedWrap = new OB_MultiStructShareholderController.AmendmentWrapper();
                                             amedWrap.amedObj = amed;
                                             amedWrap.lookupLabel = (amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name :'');
                                             if(amed.Role__c != Null && amed.Role__c.containsIgnoreCase('Shareholder')){
                                                 srWrap.amedWrapLst.add(amedWrap);
                                             }
                                             
                                             // for picklist.
                                             Boolean pushToNonShrholder = false;
                                             for( String nonShareHolder : existNonShareHolderAmed ){
                                                 if(amed.Role__c != Null && amed.Role__c.containsIgnoreCase(nonShareHolder) && !amed.Role__c.containsIgnoreCase('Shareholder')){
                                                     pushToNonShrholder = true;
                                                     break;
                                                 }
                                             }
                                             
                                             if( pushToNonShrholder ){
                                                 srWrap.shareholderAmedWrapLst.add(amedWrap);
                                             }
                                             
                                         }
        
        
        //changed licenceID to srID
        if(srId != '' || srId != null) {
            for(License_Activity__c LA :[Select Id, Activity__r.OB_DNFBP__c, Activity__r.Activity_Code__c, Activity_Description_english__c from License_Activity__c where Application__c = :srId]) {
                
                if(LA.Activity__r.OB_DNFBP__c == 'Yes') {
                    respWrap.hasDNFBP = true;
                } else {
                    respWrap.hasDNFBP = false;
                }
            }
            
            
        }
        respWrap.test = 'test ' + srId;
        system.debug('=====amend====' + respWrap);
        return respWrap;
    }
    
    @AuraEnabled
    public static ResponseWrapper onAmedSaveDB(String reqWrapPram) 
    {
        system.debug('Response---->' + reqWrapPram);
        //reqest wrpper
        reqWrap = (OB_MultiStructShareholderController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_MultiStructShareholderController.RequestWrapper.class);
        
        //response wrpper
        respWrap = new OB_MultiStructShareholderController.ResponseWrapper();
        String RecordTypename = reqWrap.recordtypename;
        System.debug('----------RecordTypename----' + RecordTypename);
        system.debug('@@@@@@@ reqWrap.amedWrap ' + reqWrap.amedWrap);
        
        // service request ID
        String srId = reqWrap.srId;
        String amendID = '';
        Boolean isDuplicate = false;
        system.debug('@@@@@@@ reqWrap.amedWrap ' + reqWrap.amedWrap);
        
        
        
        try{
            if(String.IsnotBlank(srId)) 
            {
                if(reqWrap.amedWrap.amedObj != null) 
                {
                    if(reqWrap.amedWrap.amedObj.Id != null) 
                    {
                        amendID = reqWrap.amedWrap.amedObj.Id;
                        //RecordTypename = reqWrap.amedWrap.amedObj.recordtype.developername;
                    }
                    if( reqWrap.amedWrap.amedObj.recordTypeId != NULL)
                    {
                        RecordTypename = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosById().get(reqWrap.amedWrap.amedObj.recordTypeId).getDeveloperName();
                        system.debug('@@@@@  RecordTypename  '+RecordTypename);
                    }
                    
                    System.debug('----------RecordTypename----' + RecordTypename);
                    // check duplicate individual amendment based on nationality and passport
                    // query existing amendment record of that service request.
                    if(String.IsNotBlank(RecordTypename) 
                       && RecordTypename.equalsIgnoreCase('Individual')) 
                    {
                        String passport = String.isNotBlank(reqWrap.amedWrap.amedObj.Passport_No__c) ? reqWrap.amedWrap.amedObj.Passport_No__c :'';
                        String nationality = String.IsNotBlank(reqWrap.amedWrap.amedObj.Nationality_list__c) ? reqWrap.amedWrap.amedObj.Nationality_list__c.toLowerCase() :'';
                        String passportNationlity = (String.isNotBlank(passport) ? passport + '_' :'') + (String.IsNotBlank(nationality) ? nationality :'');
                        system.debug('===passportNationlity==' + passportNationlity);
                        
                        for(HexaBPM_Amendment__c amd :[Select ID, First_Name__c, 
                                                       Passport_No__c, Nationality_list__c, 
                                                       recordtype.developername, Role__c,OB_MultiStructure_Parent_SR__c,OB_Linked_Multistructure_Applications__c, 
                                                       ServiceRequest__r.legal_structures__c,OB_MultiStructure_SR__c 
                                                       From HexaBPM_Amendment__c
                                                       Where OB_MultiStructure_SR__c = :srId
                                                       AND (recordtype.developername = 'Individual' 
                                                            AND Id != :amendID)
                                                       AND Passport_No__c = :passport
                                                       AND Nationality_list__c = :nationality
                                                       Limit 1]) 
                        {
                            system.debug('===duplicate==');
                            //duplicateAmend = amd;
                            reqWrap.amedWrap.amedObj.Id = amd.Id;
                            amendID = amd.Id;
                            // appending sharholder role.
                            if(String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Shareholder')) 
                            {
                                isDuplicate = true;
                                break;
                            }
                            
                            
                            if( amd.Role__c != NULL
                               && !amd.Role__c.containsIgnorecase('Shareholder') )
                            {
                                reqWrap.amedWrap.amedObj.Role__c = amd.Role__c+';Shareholder';
                            }
                            else if(amd.Role__c == NULL)
                            {
                                reqWrap.amedWrap.amedObj.Role__c = 'Shareholder';
                            }
                        }
                    } 
                    else if(String.IsNotBlank(RecordTypename) && RecordTypename.equalsIgnoreCase('Body_Corporate')) 
                    {
                        String registrationNo = String.isNotBlank(reqWrap.amedWrap.amedObj.Registration_No__c) ? reqWrap.amedWrap.amedObj.Registration_No__c :'';
                        system.debug(amendID + '====registrationNo=====' + registrationNo);
                        for(HexaBPM_Amendment__c amd :[Select ID, Registration_No__c,OB_MultiStructure_Parent_SR__c,OB_Linked_Multistructure_Applications__c, 
                                                       recordtype.developername, Role__c,OB_MultiStructure_SR__c
                                                       From HexaBPM_Amendment__c
                                                       Where OB_MultiStructure_SR__c = :srId
                                                       AND (recordtype.developername = 'Body_Corporate' 
                                                            AND Id != :amendID)
                                                       AND Registration_No__c = :registrationNo AND Registration_No__c != ''
                                                       Limit 1]) 
                        {
                            system.debug('==registrationNo=duplicate== '+amd.Role__c);
                            reqWrap.amedWrap.amedObj.Id = amd.Id;
                            amendID = amd.Id;
                            if(String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Shareholder') ) 
                            {
                                system.debug('==inside dup== '+amd.Role__c);
                                isDuplicate = true;
                                break;   
                            }
                            
                            
                            if( amd.Role__c != NULL
                               && !amd.Role__c.containsIgnorecase('Shareholder') )
                            {
                                reqWrap.amedWrap.amedObj.Role__c = amd.Role__c+';Shareholder';
                                system.debug('#####  reqWrap.amedWrap.amedObj.Role__c '+ reqWrap.amedWrap.amedObj.Role__c);
                            }
                            else if(amd.Role__c == NULL)
                            {
                                reqWrap.amedWrap.amedObj.Role__c = 'Shareholder';
                                system.debug('#####  reqWrap.amedWrap.amedObj.Role__c alone '+ reqWrap.amedWrap.amedObj.Role__c);
                            }
                            
                            
                        }
                    }
                    system.debug(isDuplicate + '====isDuplicate=amendID====' + amendID);
                    // duplicate record throw an error
                    if(isDuplicate) 
                    {
                        respWrap.errorMessage = 'This person/corporate already exist';
                        
                    } 
                    else 
                    {
                          Id userAccountID;
                            User currentUser = new User();
                            for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
                                //1.1
                                if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
                                    userAccountID = usrObj.contact.AccountId;
                                }
                            }
                        
                        reqWrap.amedWrap.amedObj.Customer__c = userAccountID;
                        reqWrap.amedWrap.amedObj.OB_MultiStructure_SR__c = srId;
                        if(reqWrap.amedWrap.amedObj.Id == null) 
                        {
                            // for new Shareholder assign IsShareholder as true
                            if(reqWrap.amedWrap.amedObj.Role__c != 'Shareholder') 
                            {
                                reqWrap.amedWrap.amedObj.Role__c = 'Shareholder';
                            }
                            
                            // query Shareholder record type
                            Id recordTypeID = getRecordtypeID('HexaBPM_Amendment__c', RecordTypename);
                            if(String.IsNotBlank(recordTypeID)) {
                                reqWrap.amedWrap.amedObj.recordtypeId = recordTypeID;
                            }
                        }
                        else if( reqWrap.amedWrap.amedObj.Id != NULL)
                        {
                            
                            system.debug('@@@@@@@@@ reqWrap.amedWrap.amedObj.Role__c '+ reqWrap.amedWrap.amedObj.Role__c);
                            if( reqWrap.amedWrap.amedObj.Role__c != NULL
                               && !reqWrap.amedWrap.amedObj.Role__c.containsIgnorecase('Shareholder') )
                            {
                                reqWrap.amedWrap.amedObj.Role__c = reqWrap.amedWrap.amedObj.Role__c+';Shareholder';
                            }
                            else if(reqWrap.amedWrap.amedObj.Role__c == NULL)
                            {
                                reqWrap.amedWrap.amedObj.Role__c = 'Shareholder';
                            } 
                            system.debug('@@@@@@@@@ reqWrap.amedWrap.amedObj.Role__c '+ reqWrap.amedWrap.amedObj.Role__c);
                            
                        }
                        
                        try{
                        	//V1.1
                        	 Id BodyCorporateID = getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
                        	if(reqWrap.amedWrap.amedObj.Is_the_shareholder_under_formation__c == 'Yes' && reqWrap.amedWrap.amedObj.RecordtypeID == BodyCorporateID){
                        		
                        		reqWrap.amedWrap.amedObj.Is_this_Entity_registered_with_DIFC__c = 'Yes';
                        	} 
                            upsert reqWrap.amedWrap.amedObj;
                            Map<String, String> docMasterContentDocMap = reqWrap.docMasterContentDocMap;
                            // For rearenting SRDocs
                            OB_MultistructureAmendmentSRDocHelper.RequestWrapper srDocReq = new OB_MultistructureAmendmentSRDocHelper.RequestWrapper();
                            OB_MultistructureAmendmentSRDocHelper.ResponseWrapper srDocResp = new OB_MultistructureAmendmentSRDocHelper.ResponseWrapper();
                            srDocReq.amedId = reqWrap.amedWrap.amedObj.Id;
                            srDocReq.srId = srId;
                            srDocReq.docMasterContentDocMap = docMasterContentDocMap;
                            if(reqWrap.amedWrap.amedObj.OB_Linked_Multistructure_Applications__c != null && reqWrap.amedWrap.amedObj.OB_Linked_Multistructure_Applications__c != '' ){
                                list<Id> srIdList = new list<Id>();
                                srIdList = reqWrap.amedWrap.amedObj.OB_Linked_Multistructure_Applications__c.split(',');
                                srDocReq.srIdSet.addAll(srIdList);
                            }
                            srDocResp = OB_MultistructureAmendmentSRDocHelper.reparentSRDocsToAmendment(srDocReq);
                            
                            
                            // Updating BC boolean on SR.
                            updateBCCountBasedOnSRId(reqWrap);
                            
                        }catch(DMLException e){
                            string DMLError = e.getdmlMessage(0) + '';
                            if(DMLError == null) {
                                DMLError = e.getMessage() + '';
                            }
                            respWrap.errorMessage = DMLError;
                        }
                        system.debug('=======reqWrap.amedWrap.amedObj====' + reqWrap.amedWrap.amedObj);
                        if(String.IsNotBlank(srId)) {
                            // initialize the list again
                            //calculate Risk
                            OB_RiskMatrixHelper.CalculateRisk(srId);
                            respWrap = getAmendmentList(srId, respWrap);
                        }
                    }
                }
                respWrap.amedWrap = reqWrap.amedWrap;
                
                
            } else {
                respWrap.errorMessage = 'Service Request is null';
            }
            
        } 
        catch(DMLException e) 
        {
            string DMLError = e.getdmlMessage(0)+ '';
            if(DMLError == null) {
                DMLError = e.getMessage() + '';
            }
            respWrap.errorMessage = DMLError;
            system.debug('##########DMLError ' + DMLError);
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
            return respWrap;
        }
        return respWrap;
    }
    
    
    
    
    
    /*
* get Record type ID based on developer name
*/
    public static Id getRecordtypeID(String sObjectName, String developerName) 
    {
        Id recordTypeId = null;
        // query Shareholder record type
        List<RecordType> ShareholderRecordtypeIDList = [Select Id From RecordType where sobjecttype = :sObjectName
                                                        and developername = :developerName];
        System.debug('==============ShareholderRecordtypeIDList========' + ShareholderRecordtypeIDList);
        if(ShareholderRecordtypeIDList != null && ShareholderRecordtypeIDList.size() != 0) {
            recordTypeId = ShareholderRecordtypeIDList [0].Id;
        }
        return recordTypeId;
    }
    /*
* Lightning component: OB_manageShareholdersNew- to initialise
*
*/
    @AuraEnabled
    public static ResponseWrapper initAmendmentDB(String reqWrapPram) {
        //reqest wrpper
        reqWrap = (OB_MultiStructShareholderController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_MultiStructShareholderController.RequestWrapper.class);
        
        //response wrpper
        respWrap = new OB_MultiStructShareholderController.ResponseWrapper();
        String srId = reqWrap.srId;
        OB_MultiStructShareholderController.AmendmentWrapper amedWrap = new OB_MultiStructShareholderController.AmendmentWrapper();
        HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c();
        //amedObj.ServiceRequest__c = srId;
        //amedObj.recordtype.developername=recordtypename;
        amedWrap.amedObj = amedObj;
        //amedWrap.amedObj.Role__c = 'Shareholder';
        respWrap.amedWrap = amedWrap;
        
        
        return respWrap;
    }
    
    /*
* Lightning component: OB_ManageshareholderExist
* to remove existing Shareholders.
*/
    @AuraEnabled
    public static ResponseWrapper removeAmendment(String reqWrapPram) 
    {
        //reqest wrpper
        reqWrap = (OB_MultiStructShareholderController.RequestWrapper) JSON.deserializeStrict(reqWrapPram, OB_MultiStructShareholderController.RequestWrapper.class);
        //response wrpper
        respWrap = new OB_MultiStructShareholderController.ResponseWrapper();
        try{
            String amendmentID = reqWrap.amendmentID;
            String srId = reqWrap.srId;
            
            if(String.IsNotBlank(amendmentID)) 
            {
                List<HexaBPM_Amendment__c> amendRecordList = [Select ID, Recordtype.DeveloperName, Role__c From HexaBPM_Amendment__c where Id = :amendmentID];
                HexaBPM_Amendment__c amendObj = amendRecordList [0];
                if(amendRecordList != null && amendRecordList.size() != 0) 
                {
                    if(amendObj.Recordtype.DeveloperName == 'Individual' || amendObj.Recordtype.DeveloperName == 'Body_Corporate') 
                    {
                        // if is Shareholder record then directly delete this
                        //delete amendRecordList [0];
                        //
                        list<string> amendRoles = amendObj.Role__c.split(';');
                        if(amendRoles.indexOf('Shareholder') !=-1 && amendRoles.size() > 1) 
                        {
                            amendRoles.remove(amendRoles.indexOf('Shareholder'));
                            amendObj.Role__c = String.join(amendRoles, ';');
                            update amendObj;
                        } 
                        else if(amendRoles.indexOf('Shareholder') !=-1 && amendRoles.size() == 1) 
                        {
                            delete amendObj;
                        }
                        
                        if(String.IsNotBlank(srId)) 
                        {
                            // initialize the list again
                            //calculate risk
                            OB_RiskMatrixHelper.CalculateRisk(srId);
                            respWrap = getAmendmentList(srId, respWrap);
                        }
                        
                        // Updating BC boolean on SR.
                        updateBCCountBasedOnSRId(reqWrap);
                        
                        
                    }
                }
                
            } 
            else 
            {
                respWrap.errorMessage = 'Amendment record is not present';
            }
            
            
        } 
        catch(Exception e) 
        {
            string DMLError = e.getMessage();
            respWrap.errorMessage = DMLError;
            system.debug('##########DMLError ' + DMLError);
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,DMLError));
            return respWrap;
        }
        return respWrap;
    }
    @AuraEnabled
    public static string getCountryRiskScoring(string nationality) {
        string score;
        for(Country_Risk_Scoring__c cs :[select Name, Risk_Rating__c from
                                         Country_Risk_Scoring__c where Name = :nationality]) {
                                             score = cs.Risk_Rating__c;
                                         }
        return score;
    }
    
    public static void updateBCCountBasedOnSRId(RequestWrapper reqWrap)
    {
        string srId = reqWrap.srId;
        list<HexaBPM_Amendment__c> AmndLst = [SELECT Id,OB_MultiStructure_Parent_SR__c,OB_MultiStructure_SR__c,OB_Linked_Multistructure_Applications__c FROM HexaBPM_Amendment__c WHERE OB_MultiStructure_SR__c =:srId AND RecordType.DeveloperName='Body_Corporate' and Role__c INCLUDES('Shareholder')];
        HexaBPM__Service_Request__c sr = new HexaBPM__Service_Request__c(id=srId);                               
        if(AmndLst != NULL && AmndLst.size() > 0 ){
            sr.Body_Corporate_Shareholder__c = true;
        }else{
            sr.Body_Corporate_Shareholder__c = false;
        }
        update sr;
    }
    /////////////////////////////////Wrapper  classes//////////////////////////////////////
    /*
* data will come from client side
*/
    public class RequestWrapper {
        @AuraEnabled public Id amendmentID { get; set; }
        @AuraEnabled public Id srId { get; set; }
        @AuraEnabled public AmendmentWrapper amedWrap { get; set; }
        @AuraEnabled public String recordtypename { get; set; }
        @AuraEnabled public string pageID { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public Map<String, String> docMasterContentDocMap { get; set; }
        @AuraEnabled public SRWrapper srWrap { get; set; }
        @AuraEnabled
        public string entityTypeLabel { get; set; }       
        public RequestWrapper() {
        }
    }
    
    /*
* store detail of service request with list of amendment
*/
    public class SRWrapper {
        @AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }
        @AuraEnabled public List<OB_MultiStructShareholderController.AmendmentWrapper> amedWrapLst { get; set; }
        @AuraEnabled public List<OB_MultiStructShareholderController.AmendmentWrapper> shareholderAmedWrapLst { get; set; }
        @AuraEnabled public String declarationIndividualText { get; set; }
        @AuraEnabled public String declarationCorporateText { get; set; }
        @AuraEnabled public Boolean isDraft { get; set; }
        @AuraEnabled public string viewSRURL { get; set; }
        
        public SRWrapper() {
            
        }
    } 
    
    /*
* store detail of amendment object
*/
    public class AmendmentWrapper {
        @AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
        @AuraEnabled public String lookupLabel { get; set; }
        
        public AmendmentWrapper() {
        }
    }
    
    /*
* Result will send to client side
*/
    public class ResponseWrapper 
    {
        @AuraEnabled public SRWrapper srWrap { get; set; }
        @AuraEnabled public AmendmentWrapper amedWrap { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled
        public HexaBPM__Section_Detail__c ButtonSection { get; set; }
        @AuraEnabled
        public List<HexaBPM__Service_Request__c> srList { get; set; }
        @AuraEnabled
        public boolean hasDNFBP { get; set; }
        @AuraEnabled
        public string test { get; set; }
        @AuraEnabled
        public string entityTypeLabel { get; set; }        
        
        public ResponseWrapper() {
        }
    }
    @AuraEnabled
    public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId) {
        
        
        ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
        HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
        
        PageFlowControllerHelper.objSR = objRequest;
        PageFlowControllerHelper objPB = new PageFlowControllerHelper();
        
        if(OB_QueryUtilityClass.minRecValidationCheck(SRID, 'Shareholder')) {
            objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
            upsert objRequest;
        }/*else if(String.IsNotBlank(objRequest.Page_Tracker__c) && objRequest.Page_Tracker__c.contains(pageId)){
if(objRequest.Page_Tracker__c.contains(pageId+',')){
objRequest.Page_Tracker__c = objRequest.Page_Tracker__c.replace(pageId+',','');
}if(objRequest.Page_Tracker__c.contains(','+pageId)){
objRequest.Page_Tracker__c = objRequest.Page_Tracker__c.replace(','+pageId,'');
}else{
objRequest.Page_Tracker__c = '';
}
system.debug('===objRequest.Page_Tracker__c===='+objRequest.Page_Tracker__c);
upsert objRequest;
}*/
        //calculate risk
        OB_RiskMatrixHelper.CalculateRisk(objRequest.Id);
        
        PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
        system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
        respWrap.pageActionName = responseNextPage.pg;
        respWrap.communityName = responseNextPage.communityName;
        respWrap.CommunityPageName = responseNextPage.CommunityPageName;
        respWrap.sitePageName = responseNextPage.sitePageName;
        respWrap.strBaseUrl = responseNextPage.strBaseUrl;
        respWrap.srId = objRequest.Id;
        respWrap.flowId = responseNextPage.flowId;
        respWrap.pageId = responseNextPage.pageId;
        respWrap.isPublicSite = responseNextPage.isPublicSite;
        
        system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
        return respWrap;
    }
    public class ButtonResponseWrapper {
        @AuraEnabled public String pageActionName { get; set; }
        @AuraEnabled public string communityName { get; set; }
        @AuraEnabled public String errorMessage { get; set; }
        @AuraEnabled public string CommunityPageName { get; set; }
        @AuraEnabled public string sitePageName { get; set; }
        @AuraEnabled public string strBaseUrl { get; set; }
        @AuraEnabled public string srId { get; set; }
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string pageId { get; set; }
        @AuraEnabled public boolean isPublicSite { get; set; }
        
    }
    
}