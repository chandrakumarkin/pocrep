/*
Author :Shikha Khanuja
Date  : 10 Oct 2020
Descrption : Constants to be used in classes
*/
//Shikha - This class is used to have the hardcoded strings in one place
/*
--------------------------------------------------------------------------------------------------------------------------
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
v1.0        02/03/21    Shikha        Tkt #7857 adding constants to update entities to check audito validation
v2.0        11/04/21    Mari          Tkt #13814 
v3.0        16/04/2021  Mari          Added Constants for Dubai Police Amendment
v3.1        07/04/2021  Shikha        Tkt #14491 Added constants for Non Standard Documents
*/
public class GlobalConstants {

    public static final String DIFC= 'DIFC';
    
    public static final String STEP_STATUS = 'Approved';
    public static final Set<String> NOT_APP_STATUS = new Set<String> {'Rejected','AOR Rejected'};
    public static final String APP_SR_TEMPLATE = 'Initial Approval';
    public static final String APP_SR_TEMPLATE_RDFC = 'Register with DIFC';
    public static final Set<String> CHECK_SR_TEMPLATE = new Set<String>{'Initial Approval','Register with DIFC'};
        
    //Car Parking Constants
    public static final String TENANTS = 'Tenants';
    public static final String MEMBERSHIP = 'Membership';
    public static final String MEMBERSHIP_NEW = 'New Monthly Membership';
    public static final String MEMBERSHIP_RENEWAL = 'Membership Renewal';
    public static final String VEHICLE_REG = 'New Vehicle Registration';
    public static final String REPLACE_CARD = 'Replace/Lost Card';
    public static final String LOST_CARD = 'Lost Card';
    public static final String LOST_TAG = 'Lost Tag';
    public static final String REPLACE_TAG = 'Replace/Lost Tag';
    public static final String REPLACE_VEHICLE = 'Replace Vehicle Details';
    public static final String REMOVE_EMP_ACCESS = 'Remove Employee Access';
    public static final String REMOVE_VEHICLE = 'Remove Vehicle';
    public static final String ADD_VEHICLE = 'Add Vehicle';
    public static final String CANCELLATION = 'Cancellation';
    public static final String STAT_APPROVED = 'Approved';
    public static final String STAT_ACTIVE = 'Active';
    public static final String STAT_DRAFT = 'Draft';
    public static final String STAT_SUBMITTED = 'Submitted';
    public static final String STAT_EXP = 'Expired';
    public static final String STAT_REMOVE = 'Remove';
    public static final String STAT_NEW = 'New';
    public static final Set<String> TENANT_SERVICES = new Set<String> {VEHICLE_REG,REPLACE_CARD,LOST_CARD,LOST_TAG,REPLACE_TAG,REMOVE_EMP_ACCESS,REMOVE_VEHICLE,ADD_VEHICLE,REPLACE_VEHICLE};
    public static final Set<String> MEMBERSHIP_SERVICES = new Set<String> {MEMBERSHIP_NEW,MEMBERSHIP_RENEWAL,REPLACE_CARD,LOST_CARD,REMOVE_VEHICLE,ADD_VEHICLE,REPLACE_VEHICLE,CANCELLATION};   
    //Winding up
        public static final Set<String> WINDING_UP = new Set<String> {'Transfer_from_DIFC','Dissolution','De_Registration','transfer_from_difc','dissolution','de_registration'};
        
    //Used in Questionnaire Feedback page
    public static final String LOB_Country_UAE= 'UAE';
    public static final String LOB_Country_ME_EXCLUDE_UAE= 'ME (excl. UAE)';
    public static final String LOB_Country_GCC= 'GCC (excl. UAE)';
    public static final String LOB_Country_AFRICA= 'Africa';
    public static final String LOB_Country_OTHER= 'Other';
    
    public static final String TYPEOPERATION_PREMIUMS_2019= 'Premiums per LOB from January 1st, 2019 to December 31st, 2019';
    public static final String TYPEOPERATION_OTHER_CLAIMS_PAID= 'Other LOB - Claims paid';
    public static final String TYPEOPERATION_OTHER_RISKS_PAID = 'Other LOB - Risks paid';
    public static final String TYPEOPERATION_PREMIUMS_2020 = 'Premiums per LOB from January 1st, 2020 to December 31st, 2020';
    
    //V2.0 starts
    // public static final String RT_HAS_DIFC_SPONSORED_EMPLOYEE= 'Has DIFC Sponsored Employee';
    public static final Set<String> RT_HAS_DIFC_SPONSORED_EMPLOYEE= new Set<String> {'Has DIFC Sponsored Employee','HAS DIFC Seconded Sponsored Employee'};   
    public static final Set<String> RT_STATUS_BULKTRANSFER = new Set<String> {'Expired','Active'};   
    public static final String SR_STAT_APPROVED = 'Approved';
    public static final String SR_STAT_DRAFT = 'Draft';
    public static final String RECTYPE_BULKTRANSFER = 'Request_to_Approve_Bulk_Employee_Transfer';
    public static final String BULK_TRANSFER_VF_NAME= 'BulkTransferDIFC';
    public static final String RECTYPE_DIFC_SPONSORSHIP_VISA_CANCELLATION = 'DIFC_Sponsorship_Visa_Cancellation';
    public static final Set<String> BULKTRANSFER_DISSOLVED_COMPANY = new Set<String> {'Dissolved'};   
    
    public static final String RECTYPE_DIFCLONGRESIDENCEVISA = 'DIFC_Long_Term_Residence_Visa';
    public static final String DIFCLONGRESIDENCEVISA_VF_NAME= 'DIFCLongResidenceVisa';
    
    public static final String SUCEESS= 'Success';
    public static final String YES_SELECTION = 'Yes';
    public static final String NO_SELECTION = 'No'; 
    public static final String DOCUMENT_UPLOAD_MANDATORY= 'Kindly Upload All Mandatory Documents to submit the SR';
    public static final String MULITISELECT_OTHER= 'Other'; 
    public static final String SR_IS_BLANK = 'Service Request is blank';
    public static final String COMMAND_BUTTON_SECTION= 'CommandButtonSection';
    
    
    public static final String RECTYPE_DIFC_WIDOW_VISA = 'DIFC_Widow_Visa_Request';
    public static final String DIFC_WIDOW_VISA_VF_NAME= 'DIFCLongResidenceVisa';
    public static final String DIFC_WIDOW_VISA_EMPLOYEE_SELECT= 'Kindly select the Employee for Widow request';
    public static final String DIFC_LONG_TERM_EMPLOYEE_SELECTION= 'Kindly select the "Is it an employee"';

    public static final String RECTYPE_DIFC_EMPLOYEE_CANCELLATION = 'DIFC_Sponsorship_Visa_Cancellation';
    public static final String DIFC_EMPLOYEE_CANCELLATION_VF_NAME= 'DIFCEmployeeCancellation';
   
    public static final String RECTYPE_LETTER_DIFC_LONGRESIDENCEVISA = 'DIFC_Letter_to_Immigration_for_Long_Term_Residence_Visa';
    public static final String LEFFTER_DIFC_LONG_RESIDENCEVISA_VF_NAME= 'DIFCLongResidenceVisa';
    //V2.0 Ends
    public static final String EMPLOYEE_DL_ACCESS_VF_NAME= 'EmployeeCardNonSponsored';
    public static final String RECTYPE_EMPLOYEECARD_NONSPONSORED = 'Equity_Holder_Card';
    
    //v1.0
    public static final Set<String> LEGAL_ENTITY_AUDITOR_VAL = new Set<String> {'Foundation','GP','LLP','LP','NPIO','Public Company'};
    
    public static final String ANNUAL_ASSESSMENT_REC_TYPE = 'DPO_Annual_Assessment';
    public static final String ANNUAL_ASSESSMENT_REC_TYPE_NAME = 'Annual Assessment';
    public static final String IDENTIFY_AND_ASSESS_RISK= 'Identify and assess risks';
    public static final String IDENTIFY_MEASURES_AND_REDUCE_RISK= 'Identify measures to reduce risk';
    public static final String MULITISELECT_TYPE= 'MULTIPICKLIST'; 
    public static final String REPLACE_RESPONSE_ASSESMENT= '[Replace Selected Response]'; 
    
    //V4.1
    public static final String SUSPENSE_OF_LICENSE_REC_TYPE = 'Suspension_Of_License';
    //v3.1 Start
   public static final String NON_STD_REC_TYPE = 'Non_Standard_Document';
   public static final String NON_STD_REC_TYPE_DEV_NAME = 'Non Standard Document';
   public static final String NON_STD_CAR_REG_TYPE = 'Car_Request_Details';
   public static final String NON_STD_CAR_REG_TYPE_DEV_NAME = 'Car Request Details';
   //v3.1 End
    
    //v3.0 - Dubai Police Amendment
    public static final String INDIVIDUAL_PASSPORT_DOC= 'Passport';
    public static final Set<String> AMENDMENT_TYPE= new Set<String> {'Individual','Body Corporate'};
    public static final String AMENDMENTTYPE_INDIVIDUAL= 'Individual';   
    public static final String AMENDMENTTYPE_BODYCORPORATE = 'Body Corporate'; 
    public static final String CERTIFICATE_OF_INCORPORATION = 'Certificate of Incorporation';  
    public static final String UBO_RELATIONSHIP= 'UBO';
    public static final Set<string> AMENDMENT_STATUS= new Set<String>{'Draft','Active'};
    public static final String AMENDMENT_RELATIONSHIP_SHAREHOLDER = 'Shareholder';
    public static final String LEGAL_TYPE_ENTITY_LTD= 'LTD';
    public static final String LEGAL_TYPE_ENTITY_FRC= 'FRC';
    public static final String LEGAL_TYPE_ENTITY_LTD_PRIVATE_COMPANY= 'Private Company';
    public static final String GENDER_MALE= 'Male';
    public static final String GENDER_M= 'M';
    public static final String GENDER_F= 'F';
    public static final String DOCUMENTTYPE_PASSPORT= 'passport';
    public static final INTEGER COUNT_ONE= 1;
    public static final String COMPANY_MANAGER= 'manager';
    public static final String COMPANY_HOLDER= 'Company';
    public static final String UAE_COUNTRY= 'United Arab Emirates';
    public static final String OVER= 'OVER';
    public static final String INSIDE_THE_COUNTRY= 'inside_the_country';
    public static final String OUTSIDE_THE_COUNTRY= 'outside_the_country';
    public static final String NA= 'NA';
    public static final INTEGER COUNT_HUNRED= 100;
    public static final INTEGER COUNT_ZERO= 0;
    public static final String AMENDMENTTYPE_SHAREHOLDER= 'Shareholder';
    public static final String DUMMY_REC_VALUE= '_99791';
    
    public static list<string> pageFlowToQuery = new list<string>{
        'AOR_Financial','Superuser_Authorization','AOR_NF_R','In_Principle','Commercial_Permission','Commercial_Permission_Renewal','FINANCING_STATEMENT_AMENDMENT_CORRECTION_STATEMENT',
        'DPO_Annual_Assessment','FINANCING_STATEMENT','FINANCING_STATEMENT_AMENDMENT', 'FINANCING_STATEMENT_AMENDMENT_COLLATERAL_CHANGE', 'FINANCING_STATEMENT_AMENDMENT_COLLATERAL_RESTATEMENT', 'FINANCING_STATEMENT_AMENDMENT_PARTY_DETAILS'
    };
    
    public static Set<String> MEDICALRESULTSR = new Set<String>{
        'MedicalResult'
    };
    public static final String DOC_STATUS_UPLOADED= 'Uploaded';
    public static Set<String> MANUALSRPUSHINGMEDICALRESULT= new Set<String>{
        'Widow Visa Request','DIFC Long Term Residence Visa'
    };
    public static final String MEDICALRESULTDOCUMENT= 'MedicalResult';
    
    //Shikha
    public static final Set<String> UBO_SERVICE_TYPE = new Set<String> {'Allotment of Shares','Update Details, Add or Remove Founding Member','Update Details, Add or Remove Designated Member/Member','Update Details, Add or Remove Founder'};
}