/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
 
   This class is mock test for the PR Webservice 
*/
@isTest(seeAllData=false)
global class TestPaymentQueryWebservice implements WebServiceMock {

    public static WS_PaymentQuery.InvokeQueryWS request_x = new WS_PaymentQuery.InvokeQueryWS();
    public static WS_PaymentQuery.InvokeReversalWS request_x1 = new WS_PaymentQuery.InvokeReversalWS();  
    public static WS_PaymentQuery.InvokeFullAuthReversalWS request_x2 = new WS_PaymentQuery.InvokeFullAuthReversalWS();
    public static WS_PaymentQuery.InvokeCaptureWS request_x3 = new WS_PaymentQuery.InvokeCaptureWS();
    public static WS_PaymentQuery.InvokeVoidWS request_x4 = new WS_PaymentQuery.InvokeVoidWS();      
    
  
    global void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {
            
        WS_PaymentQuery.InvokeReversalWSResponse response_x1 = new  WS_PaymentQuery.InvokeReversalWSResponse();
        WS_PaymentQuery.InvokeFullAuthReversalWSResponse response_x2 = new  WS_PaymentQuery.InvokeFullAuthReversalWSResponse ();
        WS_PaymentQuery.InvokeCaptureWSResponse response_x3 = new  WS_PaymentQuery.InvokeCaptureWSResponse();        
        WS_PaymentQuery.InvokeVoidWSResponse response_x4 = new  WS_PaymentQuery.InvokeVoidWSResponse();
        
            
        WS_PaymentQuery.InvokeQueryWSResponse response_x = new  WS_PaymentQuery.InvokeQueryWSResponse();        
        String resp='LxSZngL0MyIwRk68lhOvPEZSFwLB1cvxsjLGGVOZ61D2F5axI0xWN0wxlR8sOhm0ic/vyYwyDZgAYffmNO5ry9w5fnr5rmyno5gYO8dFhptHwemK0lsUIBsbu4js5CpyZop0vycWo78AXBW65eDCag=='; 
        response_x.return_x=resp; 
        if(responseType == 'WS_PaymentQuery.InvokeQueryWSResponse')
        response.put('response_x',response_x);
       
        response_x1.return_x=resp;
        if(requestName == 'InvokeReversalWS')
        response.put('response_x',response_x1);
        response_x2.return_x=resp;
        if(requestName == 'InvokeFullAuthReversalWS')
        response.put('response_x',response_x2);
        response_x3.return_x=resp;
        if(requestName == 'InvokeCaptureWS')
        response.put('response_x',response_x3);
        response_x4.return_x=resp;
        if(requestName == 'InvokeVoidWS')
        response.put('response_x',response_x4);
        
        
   }
}