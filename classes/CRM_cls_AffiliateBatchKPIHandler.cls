/******************************************************************************************
 *  Name        : CRM_cls_BatchKPIHandler 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2016-11-2
 *  Description : Batchable Class for KPI records (Children)
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   18-11-2016   Claude        Created
*******************************************************************************************/
public class CRM_cls_AffiliateBatchKPIHandler implements Database.Batchable<sObject> {
    
    public List<KPI__c> kpiRecords;
    
    public CRM_cls_AffiliateBatchKPIHandler(List<KPI__c> kpiRecords){
        this.kpiRecords = kpiRecords;
    }
    
    public list<KPI__c> start(Database.BatchableContext BC){
  		return kpiRecords;
    }
    	
	public void execute(Database.BatchableContext BC, list<KPI__c> kpiRecords){
	    update kpiRecords;   
	}
	
	public void finish(Database.BatchableContext BC){
	    System.debug('KPI Records successfully created');
	}

}