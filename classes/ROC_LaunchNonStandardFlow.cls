/*********************************************************************************************************************
 *  Name        : ROC_LaunchNonStandardFlow
 *  Author      : Shikha Khanuja
 *  Company     : DIFC
 *  Date        : 14-04-2021     
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By      Description
 ----------------------------------------------------------------------------------------              

**********************************************************************************************************************/
public class ROC_LaunchNonStandardFlow {
    
    @AuraEnabled
    public static NonStdWrapper launchFlow(){
        NonStdWrapper wrapObj = new NonStdWrapper();
        try{
            string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get(GlobalConstants.NON_STD_REC_TYPE_DEV_NAME).getRecordTypeId();
            string srTemplateId = '';
            string accountID = '';
            User us = [select Id,contactId from User where Id=:userinfo.getUserId()];
            Contact cont = [SELECT Id, Name, Account.Name,Accountid FROM Contact WHERE Id= :us.contactId ];
            
            contact conObj = new contact();
            account AcctObj = new account();
            
            for(contact relCont : [SELECT id,AccountId,firstname,lastname , email, phone,Passport_No__c,Nationality__c,Birthdate,Gender__c,Place_of_Birth__c,
                                   Date_of_Issue__c,Passport_Expiry_Date__c,Place_of_Issue__c,Issued_Country__c,Country_of_Birth__c,OB_Passport_Document_Id__c,Account.Legal_Type_of_Entity__c,Account.Authorized_Signatory_Combinations_Arabic__c,
                                   Account.Authorized_Signatory_Combinations__c,Account.PO_Box__c,Account.Building_Name__c,Account.Name,Account.Trade_Name__c,Account.Arabic_Name__c,
                                   Account.OB_Type_of_commercial_permission__c,
                                   Account.Phone,
                                   Account.Estimated_FTE__c,
                                   Account.Place_of_Registration__c,
                                   Account.ROC_reg_incorp_Date__c,
                                   Account.Country__c,
                                   Account.City__c,
                                   Account.Emirate_State__c,
                                   Account.Postal_Code__c,
                                   Account.Hosting_Company__c,
                                   Account.ParentId
                                   from contact where id =: cont.Id]){
                                       conObj = relCont;
                                       accountID = relCont.AccountId;
                                   }
            
            if(conObj.AccountId != null){
                for( Account accObj :[select Id  ,
                                      (select id,Name from HexaBPM__Service_Requests__r WHERE HexaBPM__Record_Type_Name__c =:GlobalConstants.NON_STD_REC_TYPE 
                                       AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__Is_Rejected__c = false 
                                       AND HexaBPM__IsCancelled__c = false LIMIT 1
                                      )
                                      from account where id =: conObj.AccountId ]){
                                          AcctObj = accObj;
                                      }
            }
            system.debug('=======accObj==============='+AcctObj);
            system.debug('=======AcctObj.HexaBPM__Service_Requests__r==============='+AcctObj.HexaBPM__Service_Requests__r);
            for(HexaBPM__SR_Template__c srTempObj : [SELECT id from HexaBPM__SR_Template__c WHERE 
                HexaBPM__SR_RecordType_API_Name__c =:GlobalConstants.NON_STD_REC_TYPE  AND HexaBPM__Active__c = true]){
                srTemplateId = srTempObj.Id;
            }
    		system.debug('### srTemplateId '+srTemplateId);
            if( srTemplateId!= null && srTemplateId!= '' && recordId !=null){
    
                HexaBPM__Service_Request__c srObj = new HexaBPM__Service_Request__c();
                srObj.recordTypeId = recordId;
                srObj.HexaBPM__SR_Template__c = srTemplateId;
                srObj.HexaBPM__Customer__c = conObj.accountID;
                srObj.HexaBPM__Contact__c = conObj.ID;
                srObj.first_name__c = conObj.firstname; 
                srObj.last_name__c = conObj.lastname; 
                srObj.hexabpm__email__c = conObj.email;
                srObj.hexabpm__send_sms_to_mobile__c = conObj.phone;  
                srObj.Passport_No__c = conObj.Passport_No__c ;
                srObj.Nationality__c = conObj.Nationality__c;
                srObj.Date_of_Birth__c = conObj.Birthdate;
                srObj.Gender__c = conObj.Gender__c;
                srObj.Place_of_Birth__c = conObj.Place_of_Birth__c;
                srObj.Date_of_Issue__c = conObj.Date_of_Issue__c;
                srObj.Date_of_Expiry__c = conObj.Passport_Expiry_Date__c;
                srObj.Place_of_Issuance__c = conObj.Place_of_Issue__c;
    
                srObj.Arabic_Entity_Name__c = conObj.Account.Arabic_Name__c;
                srObj.Po_Box_Postal_Code__c = conObj.Account.PO_Box__c;
                srObj.Type_of_entity__c = conObj.Account.Legal_Type_of_Entity__c;
                insert srObj;
                //srObj.Step_Notes__c=mode;
            	system.debug('### srObj '+srObj);
                for(HexaBPM__Page_Flow__c pfFlow : [SELECT id , (select id from HexaBPM__Pages__r WHERE HexaBPM__Page_Order__c = 1 LIMIT 1) from HexaBPM__Page_Flow__c WHERE HexaBPM__Record_Type_API_Name__c =:GlobalConstants.NON_STD_REC_TYPE  LIMIT 1]){
                    wrapObj.pageFlowURl = '?flowId='+pfFlow.Id+'&pageId='+pfFlow.HexaBPM__Pages__r[0].Id+'&srId='+srObj.id;
                }
                system.debug('### wrapObj.pageFlowURl  '+wrapObj.pageFlowURl );
            }
        }
        catch(Exception ex){
            system.debug('### exception '+ex.getStackTraceString());
            wrapObj.errorMessage = ex.getMessage();
            Log__c objLog = new Log__c();objLog.Description__c = ex.getMessage() +'   '+ex.getLineNumber();objLog.Type__c = 'Launch Non Standard Flow';insert objLog;         
        }
        return wrapObj;
    }

    
    @AuraEnabled
    public static NonStdWrapper launchFlowCarReg(){
        NonStdWrapper wrapObj = new NonStdWrapper();
        try{
            string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get(GlobalConstants.NON_STD_CAR_REG_TYPE_DEV_NAME).getRecordTypeId();
            string srTemplateId = '';
            string accountID = '';
            User us = [select Id,contactId from User where Id=:userinfo.getUserId()];
            Contact cont = [SELECT Id, Name, Account.Name,Accountid FROM Contact WHERE Id= :us.contactId ];
            
            contact conObj = new contact();
            account AcctObj = new account();
            
            for(contact relCont : [SELECT id,AccountId,firstname,lastname , email, phone,Passport_No__c,Nationality__c,Birthdate,Gender__c,Place_of_Birth__c,
                                   Date_of_Issue__c,Passport_Expiry_Date__c,Place_of_Issue__c,Issued_Country__c,Country_of_Birth__c,OB_Passport_Document_Id__c,Account.Legal_Type_of_Entity__c,Account.Authorized_Signatory_Combinations_Arabic__c,
                                   Account.Authorized_Signatory_Combinations__c,Account.PO_Box__c,Account.Building_Name__c,Account.Name,Account.Trade_Name__c,Account.Arabic_Name__c,
                                   Account.OB_Type_of_commercial_permission__c,
                                   Account.Phone,
                                   Account.Estimated_FTE__c,
                                   Account.Place_of_Registration__c,
                                   Account.ROC_reg_incorp_Date__c,
                                   Account.Country__c,
                                   Account.City__c,
                                   Account.Emirate_State__c,
                                   Account.Postal_Code__c,
                                   Account.Hosting_Company__c,
                                   Account.ParentId
                                   from contact where id =: cont.Id]){
                                       conObj = relCont;
                                       accountID = relCont.AccountId;
                                   }
            
            if(conObj.AccountId != null){
                for( Account accObj :[select Id  ,
                                      (select id,Name from HexaBPM__Service_Requests__r WHERE HexaBPM__Record_Type_Name__c =:GlobalConstants.NON_STD_CAR_REG_TYPE 
                                       AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__Is_Rejected__c = false 
                                       AND HexaBPM__IsCancelled__c = false LIMIT 1
                                      )
                                      from account where id =: conObj.AccountId ]){
                                          AcctObj = accObj;
                                      }
            }
            system.debug('=======accObj==============='+AcctObj);
            system.debug('=======AcctObj.HexaBPM__Service_Requests__r==============='+AcctObj.HexaBPM__Service_Requests__r);
            for(HexaBPM__SR_Template__c srTempObj : [SELECT id from HexaBPM__SR_Template__c WHERE 
                HexaBPM__SR_RecordType_API_Name__c =:GlobalConstants.NON_STD_CAR_REG_TYPE  AND HexaBPM__Active__c = true]){
                srTemplateId = srTempObj.Id;
            }
    		
            if( srTemplateId!= null && srTemplateId!= '' && recordId !=null){
    
                HexaBPM__Service_Request__c srObj = new HexaBPM__Service_Request__c();
                srObj.recordTypeId = recordId;
                srObj.HexaBPM__SR_Template__c = srTemplateId;
                srObj.HexaBPM__Customer__c = conObj.accountID;
                srObj.HexaBPM__Contact__c = conObj.ID;
                srObj.first_name__c = conObj.firstname; 
                srObj.last_name__c = conObj.lastname; 
                srObj.hexabpm__email__c = conObj.email;
                srObj.hexabpm__send_sms_to_mobile__c = conObj.phone;  
                srObj.Passport_No__c = conObj.Passport_No__c ;
                srObj.Nationality__c = conObj.Nationality__c;
                srObj.Date_of_Birth__c = conObj.Birthdate;
                srObj.Gender__c = conObj.Gender__c;
                srObj.Place_of_Birth__c = conObj.Place_of_Birth__c;
                srObj.Date_of_Issue__c = conObj.Date_of_Issue__c;
                srObj.Date_of_Expiry__c = conObj.Passport_Expiry_Date__c;
                srObj.Place_of_Issuance__c = conObj.Place_of_Issue__c;
    
                srObj.Arabic_Entity_Name__c = conObj.Account.Arabic_Name__c;
                srObj.Po_Box_Postal_Code__c = conObj.Account.PO_Box__c;
                srObj.Type_of_entity__c = conObj.Account.Legal_Type_of_Entity__c;
                insert srObj;
                //srObj.Step_Notes__c=mode;
            	system.debug('### srObj '+srObj);
                for(HexaBPM__Page_Flow__c pfFlow : [SELECT id , (select id from HexaBPM__Pages__r WHERE HexaBPM__Page_Order__c = 1 LIMIT 1) from HexaBPM__Page_Flow__c WHERE HexaBPM__Record_Type_API_Name__c =:GlobalConstants.NON_STD_CAR_REG_TYPE  LIMIT 1]){
                    wrapObj.pageFlowURl = '?flowId='+pfFlow.Id+'&pageId='+pfFlow.HexaBPM__Pages__r[0].Id+'&srId='+srObj.id;
                }
                system.debug('### wrapObj.pageFlowURl  '+wrapObj.pageFlowURl );
            }
        }
        catch(Exception ex){
            system.debug('### exception '+ex.getStackTraceString());
            wrapObj.errorMessage = ex.getMessage();
            Log__c objLog = new Log__c();objLog.Description__c = ex.getMessage() +'   '+ex.getLineNumber();objLog.Type__c = 'Launch Car Registration Flow';insert objLog;         
            //throw new AuraHandledException(e.getMessage());
        }
        return wrapObj;
    }

    
    @AuraEnabled
    public static NonStdWrapper launchFlowLiquidator(){
        NonStdWrapper wrapObj = new NonStdWrapper();
        try{
            string recordId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('Liquidator_confirmation_letter').getRecordTypeId();
            string srTemplateId = '';
            string accountID = '';
            User us = [select Id,contactId from User where Id=:userinfo.getUserId()];
            Contact cont = [SELECT Id, Name, Account.Name,Accountid FROM Contact WHERE Id= :us.contactId ];
            
            contact conObj = new contact();
            account AcctObj = new account();
            
            for(contact relCont : [SELECT id,AccountId,firstname,lastname , email, phone,Passport_No__c,Nationality__c,Birthdate,Gender__c,Place_of_Birth__c,
                                   Date_of_Issue__c,Passport_Expiry_Date__c,Place_of_Issue__c,Issued_Country__c,Country_of_Birth__c,OB_Passport_Document_Id__c,Account.Legal_Type_of_Entity__c,Account.Authorized_Signatory_Combinations_Arabic__c,
                                   Account.Authorized_Signatory_Combinations__c,Account.PO_Box__c,Account.Building_Name__c,Account.Name,Account.Trade_Name__c,Account.Arabic_Name__c,
                                   Account.OB_Type_of_commercial_permission__c,
                                   Account.Phone,
                                   Account.Estimated_FTE__c,
                                   Account.Place_of_Registration__c,
                                   Account.ROC_reg_incorp_Date__c,
                                   Account.Country__c,
                                   Account.City__c,
                                   Account.Emirate_State__c,
                                   Account.Postal_Code__c,
                                   Account.Hosting_Company__c,
                                   Account.ParentId
                                   from contact where id =: cont.Id]){
                                       conObj = relCont;
                                       accountID = relCont.AccountId;
                                   }
            
            if(conObj.AccountId != null){
                for( Account accObj :[select Id  ,
                                      (select id,Name from HexaBPM__Service_Requests__r WHERE HexaBPM__Record_Type_Name__c = 'Liquidator_confirmation_letter' 
                                       AND HexaBPM__IsClosedStatus__c = false AND HexaBPM__Is_Rejected__c = false 
                                       AND HexaBPM__IsCancelled__c = false LIMIT 1
                                      )
                                      from account where id =: conObj.AccountId ]){
                                          AcctObj = accObj;
                                      }
            }
            system.debug('=======accObj==============='+AcctObj);
            system.debug('=======AcctObj.HexaBPM__Service_Requests__r==============='+AcctObj.HexaBPM__Service_Requests__r);
            for(HexaBPM__SR_Template__c srTempObj : [SELECT id from HexaBPM__SR_Template__c WHERE 
                HexaBPM__SR_RecordType_API_Name__c ='Liquidator_confirmation_letter'  AND HexaBPM__Active__c = true]){
                srTemplateId = srTempObj.Id;
            }
    		system.debug('### srTemplateId '+srTemplateId);
            if( srTemplateId!= null && srTemplateId!= '' && recordId !=null){
    
                HexaBPM__Service_Request__c srObj = new HexaBPM__Service_Request__c();
                srObj.recordTypeId = recordId;
                srObj.HexaBPM__SR_Template__c = srTemplateId;
                srObj.HexaBPM__Customer__c = conObj.accountID;
                srObj.HexaBPM__Contact__c = conObj.ID;
                srObj.first_name__c = conObj.firstname; 
                srObj.last_name__c = conObj.lastname; 
                srObj.hexabpm__email__c = conObj.email;
                srObj.hexabpm__send_sms_to_mobile__c = conObj.phone;  
                srObj.Passport_No__c = conObj.Passport_No__c ;
                srObj.Nationality__c = conObj.Nationality__c;
                srObj.Date_of_Birth__c = conObj.Birthdate;
                srObj.Gender__c = conObj.Gender__c;
                srObj.Place_of_Birth__c = conObj.Place_of_Birth__c;
                srObj.Date_of_Issue__c = conObj.Date_of_Issue__c;
                srObj.Date_of_Expiry__c = conObj.Passport_Expiry_Date__c;
                srObj.Place_of_Issuance__c = conObj.Place_of_Issue__c;
    
                srObj.Arabic_Entity_Name__c = conObj.Account.Arabic_Name__c;
                 srObj.Type_Of_Documents__c = 'Liquidator Confirmation Letter';
                srObj.Po_Box_Postal_Code__c = conObj.Account.PO_Box__c;
                srObj.Type_of_entity__c = conObj.Account.Legal_Type_of_Entity__c;
                insert srObj;
               
                for(HexaBPM__Page_Flow__c pfFlow : [SELECT id , (select id from HexaBPM__Pages__r WHERE HexaBPM__Page_Order__c = 1 LIMIT 1) from HexaBPM__Page_Flow__c WHERE HexaBPM__Record_Type_API_Name__c ='Liquidator_confirmation_letter'  LIMIT 1]){
                    wrapObj.pageFlowURl = '?flowId='+pfFlow.Id+'&pageId='+pfFlow.HexaBPM__Pages__r[0].Id+'&srId='+srObj.id;
                }
                
            }
        }
        catch(Exception ex){
            system.debug('### exception '+ex.getStackTraceString());
            wrapObj.errorMessage = ex.getMessage();
            Log__c objLog = new Log__c();objLog.Description__c = ex.getMessage() +'   '+ex.getLineNumber();objLog.Type__c = 'Liquidator_confirmation_letter';insert objLog;         
            //throw new AuraHandledException(e.getMessage());
        }
        return wrapObj;
    }
    public class NonStdWrapper{
        
        @AuraEnabled public String pageFlowURL {get;set;}  
        @AuraEnabled public String errorMessage {get;set;}
        @AuraEnabled public HexaBPM__Service_Request__c srObj {get;set;}        
    }
}