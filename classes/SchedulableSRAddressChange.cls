/*********************************************************************************************************************
*  Name     : schedulableSRAddressChangeBatch 
*  Author     : Ananya Acharya
*  Purpose   : This is to schedule all approved service request and create certificate for child customers.
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
V1.0          Ananya				15/06/2021
//questions
////1? Which approved Service Request records will be picked up: Today, LAST_N_DAYS, ?
//2? we need to store any information for failure to track failed for processed records to be picked in next batch run        
*/
global class SchedulableSRAddressChange implements schedulable {    
    global void execute(SchedulableContext sc)
    {
        ServiceRequestsAddressChangeBatch b = new ServiceRequestsAddressChangeBatch(); //ur batch class
        database.executebatch(b);
    }
}