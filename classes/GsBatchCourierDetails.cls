global without sharing class GsBatchCourierDetails implements Schedulable,Database.Batchable<sObject>,Database.AllowsCallouts {
	global list<Step__c> start(Database.BatchableContext BC ){
		Datetime dt = system.now().addMinutes(-10);
		list<Step__c> lstSteps = [select Id,Name,Is_Auto_Closure__c from Step__c 
								where SR_Group__c = 'GS' AND AWB_Number__c = null AND Sys_Courier_Check__c = true 
									AND (Parent_Step__c != null OR (Is_Auto_Closure__c = true AND Status_Code__c != 'CLOSED'))
									AND ((CreatedDate <:dt AND (Status_Code__c != 'DELIVERED' AND Status_Code__c != 'COLLECTED' AND Status_Code__c != 'CLOSED' )) OR (Pending__c != null AND (Status_Code__c != 'DELIVERED' AND Status_Code__c != 'COLLECTED') ) OR (Is_Auto_Closure__c = true AND Status_Code__c != 'CLOSED') )];
		return lstSteps;
    }
    global void execute(Database.BatchableContext BC, list<Step__c> lstSteps){
    	Id StatusId;
    	for(Status__c obj:[select Id from Status__c where Code__c='CLOSED' limit 1]){
	    	StatusId = obj.Id;
	    }
    	for(Step__c objStep : lstSteps){
    		if(objStep.Is_Auto_Closure__c == false)
    			CC_CourierCls.CreateShipmentNormal(objStep.Id);
    		else{
    			//GsCourierDetails.AutoClosureOfStep(objStep.Id);
    			GsSapWebServiceDetails.CurrentUserId = 'ARAMEX';
				GsSapWebServiceDetails.IsClosed = true;
				GsSapWebServiceDetails.IsAutoClosure = true;
				GsSapWebServiceDetails.StatusId = StatusId;
		  		string result = GsSapWebServiceDetails.PushActivityToSAPNew(new list<string>{objStep.Id});
    		}	
    	}
    }
    global void finish(Database.BatchableContext BC){
    	GsBatchCourierDetails objSchedule = new GsBatchCourierDetails();
    	Datetime dt = system.now().addMinutes(10);
    	string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();
    	for(CronTrigger obj : [select Id from CronTrigger where CronJobDetailId IN (SELECT Id FROM CronJobDetail where Name = 'Batch GS Courier Activity Process')])
    		system.abortJob(obj.Id);
    	system.schedule('Batch GS Courier Activity Process', sCornExp, objSchedule);
    }
    
    global void execute(SchedulableContext ctx) {
        database.executeBatch(new GsBatchCourierDetails(), 1);
    }
    
}