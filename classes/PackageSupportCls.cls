/******************************************************************************************
 * ----------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By        Description
 ----------------------------------------------------------------------------------------
 v1.0	12/3/2021		Shikha			#9563 - VC Fund manager Changes
 * *******************************************************************************************/
public class PackageSupportCls{

public class PackageData
{
    @InvocableVariable(required=true) //
    public ID AccountId;
    @InvocableVariable(required=true)//Account ROC status 
    public String AccountStatus;
    @InvocableVariable(required=true) //Account company type 
    public String CompanyType;
    
    @InvocableVariable(required=false) //Default package code 
    public String PackageCode;
 
    @InvocableVariable(required=false) //New package status 
    public String PackageStatus;
    
    @InvocableVariable(required=false) //Account currect assigned package 
    public String CurrentAccountPackageCode;
    
 }
 
  @InvocableMethod(label='Create Default Package' description='Create and Assign default account' category='CompanyPackages')
  public static void DefaultPackage(list<PackageData> requests) 
  {
  system.debug('### requests default package '+requests);
  //Create package only in case of IM.IA 
  //if(requests[0].AccountStatus=='Active')
  //v1.0 - Modified condition to avoid VC
  if(requests[0].PackageCode!=requests[0].CurrentAccountPackageCode && requests[0].CurrentAccountPackageCode!=null && !requests[0].CurrentAccountPackageCode.contains('VC'))
  {
    
    List<Company_package__c> newCompanyPack=new List<Company_package__c>();
    //default package applicable for this account based on inputs 
    list<Package__c> ListPackages=[Select id from Package__c where Package_Code__c=:requests[0].PackageCode and Company_Type__c=:requests[0].CompanyType and Status__c='Active' limit 1];
    
    List<Company_package__c> ListCompanyPack=[select id from Company_package__c where Account__c=:requests[0].AccountId and Status__c!='Expired' and Status__c!='Cancelled' and Status__c!='Rejected' and Package__r.Package_Code__c=:requests[0].PackageCode];
    
        if(ListCompanyPack.isEmpty())
        for(PackageData ObjPacka:requests)
        {
            Company_package__c objCompany=new Company_package__c();
            objCompany.Status__c=ObjPacka.PackageStatus;
            objCompany.Account__c=ObjPacka.AccountId;
            objCompany.Package__c=ListPackages[0].id;
            objCompany.Start_Date__c=date.today();
            newCompanyPack.add(objCompany);
        }   
        if(!newCompanyPack.isEmpty())
            insert newCompanyPack;
    
  }
   
  
  

  
  }
  
}