/*************************************************************************************************
 *  Name        : CourierTracking_Cls
 *  Author      : Saima Hidayat
 *  Company     : NSI JLT
 *  Date        : 2015-06-01     
 *  Purpose     : This class is to call the Courier Tracking API.    
---------------------------------------------------------------------------------------------------------------------
   Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date            Updated By      Description
V1.1    07-03-2016      Ravi            Modified the logic as per #2488
V1.2    05-10-2016      Sravan           As per Tkt # 3392 : Decrypt password.
V1.3    27-07-2017      Sravan          Eliminating shipments with closed status and parent step closed status 
v1.4    03-March-2020   Arun            limit number of records to last 90 days . ARAMEX only hold 2 month data 
---------------------------------------------------------------------------------------------------------------------          
**************************************************************************************************/
global without sharing class CourierTracking_Cls implements Schedulable,Database.Batchable<sObject>,Database.AllowsCallouts {
    global void execute(SchedulableContext ctx) {
        //TrackShipment();
        database.executeBatch(new CourierTracking_Cls(), 1);
    }
    
    global list<Shipment__c> start(Database.BatchableContext BC ){
        
        list<Shipment__c> lstShips = [Select id,name,Status__c,Courier_Type__c,Airway_Bill_No__c,Service_Request__c,Step__c,Express_Service__c,Step__r.SR_Group__c,
                                    Step__r.Step_Status__c,Step__r.Status_Code__c,Step__r.Step_Name__c,Step__r.Parent_Step__c,Step__r.Parent_Step__r.Step_Name__c,Step__r.Parent_Step__r.Status_Code__c,Step__r.Pending__c,
                                    Step__r.Pending__r.Start_Date__c,Step__r.Pending__r.End_Date__c,Step__r.Pending__r.SAP_PSUSR__c,
                                    Step__r.SAP_DOCCO__c,Step__r.SAP_DOCCR__c,Step__r.SAP_DOCDL__c,Step__r.SAP_DOCRC__c,Step__r.SAP_CTOTL__c,Step__r.SAP_CTEXT__c,
                                    Consignee_Name__c,Shipper_Name__c,Created_On__c,Step__r.Name,Step__r.SR__r.Name,Step__r.SR__c,Step__r.Parent_Step__r.Is_Letter__c,
                                    Step__r.SR_Record_Type__c,Step__r.Parent_Step__r.Step_Template__r.SR_Status__c,
                                    Step__r.Parent_Step__r.Is_Closed__c from Shipment__c where status__c!='Delivered' AND Step__c != null AND Status__c != 'Closed' and CreatedDate = LAST_90_DAYS];
        
        return lstShips;
    }
    global void execute(Database.BatchableContext BC, list<Shipment__c> Ships){
        TrackShipment(Ships);
    }
    global void finish(Database.BatchableContext BC){
        CourierTracking_Cls objSchedule = new CourierTracking_Cls();
        Datetime dt = DateCalculation(system.now(), Integer.valueOf(System.Label.Courier_Duration));//system.now().addMinutes(15);
        string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();
        for(CronTrigger obj : [select Id from CronTrigger where CronJobDetailId IN (SELECT Id FROM CronJobDetail where Name = 'Track Courier Information')])
            system.abortJob(obj.Id);
        system.schedule('Track Courier Information', sCornExp, objSchedule);
    }
    
    public static DateTime DateCalculation(DateTime dt,Integer iFrequnacy){
        Datetime temp = dt.addMinutes(iFrequnacy);
        if(temp.hour() >= 18 || test.isRunningTest()){
            temp = temp.addDays(1);
            string sDay = temp.format('EEEE');
            system.debug('sDay is : '+sDay);
            if(sDay == 'Friday'){
                temp = temp.addDays(2);
            }else if(sDay == 'Saturday'){
                temp = temp.addDays(1);
            }
            DateTime tempDate = Datetime.newInstance(temp.year(), temp.month(), temp.day(), 7, 30, 0);
            return tempDate;
        }
        return temp;
    }
    
    //@Future(callout=true)
    public static void TrackShipment(list<Shipment__c> lstBatchShips){
        list<Shipment__c> lstShipments = new list<Shipment__c>();
        list<Step__c> lstSteps = new list<Step__c>();
        list<Step__c> lstPSteps = new list<Step__c>(); //Paarent Steps 
        list<Service_Request__c> lstSRs = new list<Service_Request__c>();
        Shipment__c UpdateShp = new Shipment__c();
        Step__c stp = new Step__c();
        Service_Request__c updateSR = new Service_Request__c();
        map<string,Shipment__c> mapShipments = new map<string,Shipment__c>();
        //String[] AWB = new String[]{};//{'4985892575','4985892586','41178402473'};
        list<string> AWB = new list<string>();//{'4985892575','4985892586','41178402473'};
        list<Id> lstSIds = new list<Id>();
        
        for(Shipment__c objS : lstBatchShips){
            if(objS.Step__r.Parent_Step__c !=null && objS.Step__r.Parent_Step__r.Is_Closed__c)   // V1.3
                continue;
            else
                lstSIds.add(objS.Id);
        }

        Aramax_Status_Mapping__mdt[] aramaxMappings = [SELECT Label, 
                                                            Status_Description__c
                                                            FROM Aramax_Status_Mapping__mdt];

        Map<String,String> aramaxMappingsMap = new Map<String,String>();

        for(Aramax_Status_Mapping__mdt thisSetting : aramaxMappings){
            aramaxMappingsMap.put(thisSetting.Label,thisSetting.Status_Description__c);
        }


        for(Shipment__c objS : [Select id,name,Status__c,Courier_Type__c,Airway_Bill_No__c,Service_Request__c,Step__c,Express_Service__c,Step__r.SR_Group__c,
                                    Step__r.Step_Status__c,Step__r.Status_Code__c,Step__r.Step_Name__c,Step__r.Parent_Step__c,Step__r.Parent_Step__r.Step_Name__c,Step__r.Parent_Step__r.Status_Code__c,Step__r.Pending__c,
                                    Step__r.Pending__r.Start_Date__c,Step__r.Pending__r.End_Date__c,Step__r.Pending__r.SAP_PSUSR__c,
                                    Step__r.SAP_DOCCO__c,Step__r.SAP_DOCCR__c,Step__r.SAP_DOCDL__c,Step__r.SAP_DOCRC__c,Step__r.SAP_CTOTL__c,Step__r.SAP_CTEXT__c,
                                    Consignee_Name__c,Shipper_Name__c,Created_On__c,Step__r.Name,Step__r.SR__r.Name,Step__r.SR__c,Step__r.Parent_Step__r.Is_Letter__c,
                                    Step__r.SR_Record_Type__c
                                    from Shipment__c where status__c!='Delivered' AND Step__c != null AND Status__c != 'Closed' AND Id IN : lstSIds ]){

                 mapShipments.put(objS.Airway_Bill_No__c,objS);
                AWB.add(objS.Airway_Bill_No__c);
           
        }

        system.debug('AWB====>>>>   '+AWB);
        if(!mapShipments.isEmpty() && !AWB.isEmpty()){
            //Initializing tracking services
            WS_CourierTracking.BasicHttpBinding_Service_1_0 TrackingService = new WS_CourierTracking.BasicHttpBinding_Service_1_0();
            WS_CourierTracking.ShipmentTrackingResponse_element Resp = new WS_CourierTracking.ShipmentTrackingResponse_element();
            
            //Getting account details for DIFC
            WS_CourierTracking.ClientInfo ClientInfo = new WS_CourierTracking.ClientInfo();
            list<Courier_Client_Info__c> CCInfo = Courier_Client_Info__c.getall().values();
            if(CCInfo.size()>0 && CCInfo!=null){
                ClientInfo.UserName=CCInfo[0].Username__c;
                // V1.2
               
               // ClientInfo.Password = CCInfo[0].Password__c; Commented for V1.2
                ClientInfo.Password =  test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(CCInfo[0].Password__c) :CCInfo[0].Password__c;  // V1.2
                ClientInfo.Version = CCInfo[0].Version_No__c;
                ClientInfo.AccountNumber = CCInfo[0].Account_No__c;
                ClientInfo.AccountPin = CCInfo[0].Account_Pin__c;
                ClientInfo.AccountEntity = CCInfo[0].Account_Entity__c;
                ClientInfo.AccountCountryCode = CCInfo[0].Account_Country_Code__c;
                  
           }
           system.debug('ClientInfo is : '+ClientInfo);
           try{
            /*
                Transaction_x.Reference1=objStep.Name;
                Transaction_x.Reference2=objStep.SR__r.Name;
                Transaction_x.Reference3=objStep.Id;
                Transaction_x.Reference4=objStep.SR__c; 
            */
               WS_CourierTracking.Transaction_x Transaction_x = new WS_CourierTracking.Transaction_x();
               if(lstBatchShips != null && !lstBatchShips.isEmpty()){
                   Transaction_x.Reference1 = lstBatchShips[0].Step__r.Name;
                   Transaction_x.Reference2 = lstBatchShips[0].Step__r.SR__r.Name;
                   Transaction_x.Reference3 = lstBatchShips[0].Step__c;
                   Transaction_x.Reference4 = lstBatchShips[0].Step__r.SR__c;
               }
               system.debug('Transaction_x is : '+Transaction_x);
               WS_CourierTracking.ArrayOfstring Shipments = new WS_CourierTracking.ArrayOfstring();
               Shipments.string_x = AWB;
               system.debug('Shipments is : '+Shipments);
               Resp = TrackingService.TrackShipments(ClientInfo,Transaction_x,Shipments,false);
               //Resp = TrackingService.TrackShipments(ClientInfo,null,Shipments,true);
               CC_CourierCls.ShipmentInfo objShipmentInfo = new CC_CourierCls.ShipmentInfo();
               system.debug('Response is : '+Resp);
                
                if(!Resp.HasErrors && Resp.TrackingResults != null && Resp.TrackingResults.KeyValueOfstringArrayOfTrackingResultmFAkxlpY != null){

                    Map<String,WS_CourierTracking.TrackingResult> timeMap;
                    Map<String,WS_CourierTracking.TrackingResult> deliveredMap = new  Map<String,WS_CourierTracking.TrackingResult>();
                    DateTime newDateTime ;
                   
                    for(WS_CourierTracking.KeyValueOfstringArrayOfTrackingResultmFAkxlpY_element element:Resp.TrackingResults.KeyValueOfstringArrayOfTrackingResultmFAkxlpY){
                        //checking if aramax has closed the shipment.
                        for(WS_CourierTracking.TrackingResult thisArray : element.Value.TrackingResult){
                            
                            if(aramaxMappingsMap.containsKey(thisArray.UpdateCode)){
                                deliveredMap.put(thisArray.UpdateCode,thisArray);
                            }
                         
                        }
                        //Collecting the latest aramax status 
                        if(deliveredMap.isEmpty()){
                            for(WS_CourierTracking.TrackingResult thisArray : element.Value.TrackingResult){
                              
                            if(timeMap != null && newDateTime < thisArray.UpdateDateTime){

                                newDateTime =  thisArray.UpdateDateTime;
                                timeMap = new Map<String,WS_CourierTracking.TrackingResult>();
                                timeMap.put(thisArray.UpdateDateTime + '-' + thisArray.UpdateCode,thisArray);
                            }
                            else if(timeMap== null){
                                    newDateTime =  thisArray.UpdateDateTime;
                                    timeMap = new Map<String,WS_CourierTracking.TrackingResult>();
                                    timeMap.put(thisArray.UpdateDateTime + '-' + thisArray.UpdateCode,thisArray);
                                }
                            }
                        }
                        
                    }
                    
                    
                    for(WS_CourierTracking.KeyValueOfstringArrayOfTrackingResultmFAkxlpY_element element:Resp.TrackingResults.KeyValueOfstringArrayOfTrackingResultmFAkxlpY){
                  
                        system.debug('mapShipments.containsKey(element.Key) is : '+mapShipments.containsKey(element.Key));

                        if(mapShipments.containsKey(element.Key)){
                        
                            for(WS_CourierTracking.TrackingResult thisArray : element.Value.TrackingResult){
                               
                                if(deliveredMap.containsKey(thisArray.UpdateCode)||
                                    (timeMap != null 
                                      && timeMap.containsKey(thisArray.UpdateDateTime + '-' + thisArray.UpdateCode))){
                                
                                    string ShipmentStatus = deliveredMap.containsKey(thisArray.UpdateCode)
                                                            ? aramaxMappingsMap.get(thisArray.UpdateCode)
                                                            : thisArray.UpdateDescription;

                                    UpdateShp = mapShipments.get(element.Key);
                                    UpdateShp.Status__c = ShipmentStatus;
                                    UpdateShp.Courier_Status_Updated_on__c = thisArray.UpdateDateTime.addhours(-4);
                                    
                                    stp = new Step__c(id=(mapShipments.get(element.Key)).Step__c);
                                    stp.Courier_Update_Desc__c = ShipmentStatus;
                                    stp.Courier_Update_Code__c = thisArray.UpdateCode;
                                    stp.Step_Notes__c = thisArray.Comments;
                                    stp.Location__c = thisArray.UpdateLocation;
                                    stp.Courier_Status__c = ShipmentStatus;
                                    stp.Courier_Error_Message_API__c = thisArray.ProblemCode;
                                    stp.Courier_Status_Updated_on__c = thisArray.UpdateDateTime.addhours(-4);
                                    
                                    if(ShipmentStatus=='Delivered'){
                                        if(mapShipments.get(element.Key).Step__r.SR_Group__c != 'GS'){
                                            String stpStatusId ='';
                                            String SRstatusId = '';
                                            for(Status__c st : [Select id,Name,Code__c from Status__c where Code__c='DELIVERED' and Type__c='End']){
                                                stpStatusId = st.id;
                                            }
                                            for(SR_Status__c sts : [Select id,Name,Code__c from SR_Status__c where Code__c='Delivered']){
                                                SRstatusId = sts.id;
                                            }
                                            stp.Date_Delivered__c =thisArray.UpdateDateTime.addhours(-4);
                                            stp.Status__c = stpStatusId;
                                            
                                            UpdateShp.Delivery_Date__c =thisArray.UpdateDateTime.addhours(-4);
                                            UpdateShp.Status__c = 'Delivered';
                                            
                                            updateSR = new Service_Request__c(id=(mapShipments.get(element.Key)).Service_Request__c);
                                            updateSR.External_SR_Status__c = SRstatusId;
                                            updateSR.Internal_SR_Status__c = SRstatusId;
                                            lstSRs.add(updateSR);
                                            lstSteps.add(stp);
                                            lstShipments.add(UpdateShp);
                                        }else{
                                            //Close the Step and Push it to SAP
                                            map<string,string> mapStatus = new map<string,string>();
                                            for(Status__c obj : [select Id,Code__c from Status__c where Code__c='DELIVERED' OR Code__c='COLLECTED']){
                                                mapStatus.put(obj.Code__c,obj.Id);
                                            }
                                            string code = mapShipments.get(element.Key).Step__r.Status_Code__c;
                                            code = (code == 'PENDING_DELIVERY') ? 'DELIVERED' : 'COLLECTED';
                                            stp.Date_Delivered__c =thisArray.UpdateDateTime.addhours(-4);
                                            stp.Status__c = mapStatus.get(code);
                                            
                                            UpdateShp.Delivery_Date__c =thisArray.UpdateDateTime.addhours(-4);
                                            UpdateShp.Status__c = 'Delivered';
                                            if(mapShipments.get(element.Key).Step__r.Parent_Step__r.Step_Name__c != 'Front Desk Review' && mapShipments.get(element.Key).Step__r.Parent_Step__r.Status_Code__c != 'AWAITING_ORIGINALS' &&
                                                mapShipments.get(element.Key).Step__r.Parent_Step__r.Is_Letter__c == false && mapShipments.get(element.Key).Step__r.SR_Record_Type__c != 'PO_BOX' && mapShipments.get(element.Key).Step__r.SR_Record_Type__c != 'Add_a_New_Service_on_the_Index_Card' ){ // && mapShipments.get(element.Key).Step__r.Parent_Step__r.Step_Name__c != 'New Documents Generation'
                                                
                                                objShipmentInfo.Logs = new list<Log__c>();
                                                objShipmentInfo.Shipments = new list<Shipment__c>();
                                                objShipmentInfo.Steps = new list<Step__c>();
                                                
                                                stp.Parent_Step__c = mapShipments.get(element.Key).Step__r.Parent_Step__c;
                                                stp.SR__c = mapShipments.get(element.Key).Service_Request__c;
                                                //stp.Parent_Step__r.Step_Template__r.SR_Status__c = UpdateShp.Step__r.Parent_Step__r.Step_Template__r.SR_Status__c;
                                                if(UpdateShp.Step__r.Pending__c != null){
                                                    stp.Pending__c = UpdateShp.Step__r.Pending__c;
                                                }
                                                system.debug('stp.Pending__c is : '+stp.Pending__c);
                                                objShipmentInfo.Shipments.add(UpdateShp);
                                                objShipmentInfo.Steps.add(stp);
                                                objShipmentInfo = CC_CourierCls.PushShipmentToSAP(objShipmentInfo,'Change');
                                                if(mapShipments.get(element.Key).Step__r.Pending__c == null)
                                                    objShipmentInfo = CC_CourierCls.PushShipmentToSAP(objShipmentInfo,'Close');
                                            }else if((mapShipments.get(element.Key).Step__r.Parent_Step__r.Step_Name__c == 'Front Desk Review' && mapShipments.get(element.Key).Step__r.Parent_Step__r.Status_Code__c == 'AWAITING_ORIGINALS')
                                                || mapShipments.get(element.Key).Step__r.Parent_Step__r.Is_Letter__c == true || (mapShipments.get(element.Key).Step__r.SR_Record_Type__c == 'PO_BOX' || mapShipments.get(element.Key).Step__r.SR_Record_Type__c == 'Add_a_New_Service_on_the_Index_Card')){ // && mapShipments.get(element.Key).Step__r.Parent_Step__r.Step_Name__c == 'New Documents Generation'
                                                lstSteps.add(stp);
                                                lstShipments.add(UpdateShp);
                                                //Close the Collected Step
                                                if(mapShipments.get(element.Key).Step__r.Parent_Step__r.Is_Letter__c == true || (mapShipments.get(element.Key).Step__r.SR_Record_Type__c == 'PO_BOX' || mapShipments.get(element.Key).Step__r.SR_Record_Type__c == 'Add_a_New_Service_on_the_Index_Card')){
                                                    if(mapShipments.get(element.Key).Step__r.Parent_Step__c != null){
                                                        code = mapShipments.get(element.Key).Step__r.Status_Code__c;
                                                        code = (code == 'PENDING_DELIVERY') ? 'DELIVERED' : 'COLLECTED';
                                                        lstPSteps.add(new Step__c(Id=mapShipments.get(element.Key).Step__r.Parent_Step__c,Status__c=mapStatus.get(code),Courier_Enabled__c=true));
                                                    }
                                                }
                                            }
                                        }
                                    }else{
                                        lstShipments.add(UpdateShp);
                                        lstSteps.add(stp);
                                    }
                                }
                            }
                        
                        }
                    }
                    Savepoint sp = Database.setSavepoint();
                    try{
                        if(lstShipments.size()>0 && lstShipments!=null && !lstShipments.isEmpty())
                            update lstShipments;
                        if(lstSteps.size()>0 && lstSteps!=null && !lstSteps.isEmpty())
                            update lstSteps;
                        if(lstSRs.size()>0 && lstSRs!=null && !lstSRs.isEmpty())
                            update lstSRs;
                        if(objShipmentInfo.Steps != null && !objShipmentInfo.Steps.isEmpty())
                            update objShipmentInfo.Steps;
                        if(objShipmentInfo.Shipments != null && !objShipmentInfo.Shipments.isEmpty())
                            update objShipmentInfo.Shipments;
                        if(objShipmentInfo.StepsToInsert != null && !objShipmentInfo.StepsToInsert.isEmpty())
                            insert objShipmentInfo.StepsToInsert;
                        if(objShipmentInfo.lstSRs != null && !objShipmentInfo.lstSRs.isEmpty())
                            update objShipmentInfo.lstSRs;
                        if(!lstPSteps.isEmpty())
                            update lstPSteps;
                        if(objShipmentInfo.Logs != null && !objShipmentInfo.Logs.isEmpty())
                            insert objShipmentInfo.Logs;
                    }catch(Exception ex){
                        database.rollback(sp);
                        Log__c objLog = new Log__c();
                        objLog.Description__c ='Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage()+'\nAWBs : '+AWB;
                        objLog.Type__c = 'Schedule for Courier Tracking response update failed';
                        insert objLog;
                    }
                }
           }catch(Exception ex){
                Log__c objLog = new Log__c();
                objLog.Description__c ='Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage()+'\nAWBs : '+AWB;
                objLog.Type__c = 'Schedule for Courier Tracking has failed';
                insert objLog;
           }
        }
    }
}