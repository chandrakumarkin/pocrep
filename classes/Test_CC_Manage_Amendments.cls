/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_CC_Manage_Amendments {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Acc';
        insert objAcc;
        
        Account objAcc2 = new Account();
        objAcc2.Name = 'Test Acc2';
        objAcc2.Auditor_Type__c = 'Recognised';
        insert objAcc2;
        
        Account objAcc3 = new Account();
        objAcc3.Name = 'Test Acc3';
        objAcc3.Auditor_Type__c = 'Registered';
        insert objAcc3;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAcc.Id;
        objSR.Entity_Name__c = 'Test Entity';
        objSR.Proposed_Trading_Name_1__c = 'Test Proposed Name';
        objSR.pro_trade_name_1_in_arab__c = 'Arab Name';
        insert objSR;
        
        Lookup__c nat = new Lookup__c(Type__c='Nationality',Name='Pakistan');
        insert nat;
        
        Amendment__c amend =new Amendment__c();
        amend.Status__c = 'Draft';
        amend.ServiceRequest__c=objSR.id;
        amend.Customer__c = objAcc.Id;
        amend.Title_new__c = 'Mrs.';
        amend.Family_Name__c = 'Mrs.';
        amend.Given_Name__c = 'Mrs.';
        amend.Recognized_Auditors__c = objAcc2.Id;
        amend.Financial_Year_End__c = '2013';
        amend.Date_of_Birth__c = Date.newinstance(1980,10,17);
        amend.Place_of_Birth__c = 'Pakistan';
        amend.Passport_No__c = 'ASD122323';
        amend.Nationality__c = nat.id;
        amend.Nationality_list__c = 'Pakistan';
        //amend.Phone__c = '+971562019803';
        amend.Person_Email__c = 'abc@nsigulf.com';
        amend.First_Name_Arabic__c ='AbcTest';
        amend.Occupation__c='Developer';
        amend.Apt_or_Villa_No__c='villa';
        amend.Building_Name__c='building';
        amend.Street__c='street';
        amend.PO_Box__c = '45231435';
        amend.Permanent_Native_City__c='Karachi';
        amend.Emirate_State__c='Emairate';
        amend.Post_Code__c='2322';
        amend.Emirate__c='Dubai';
        amend.Permanent_Native_Country__c = 'Pakistan';
        amend.Is_Designated_Member__c = true;
        amend.Company_Name__c = 'Company2';
        amend.Former_Name__c = 'Former';
        amend.Registration_Date__c = Date.newinstance(1980,10,17);
        amend.Place_of_Registration__c = 'Registration';
        amend.Relationship_Type__c='CEO';
        amend.Status__c='Draft';
        insert amend;
        
        amendment__c amend2 = new amendment__c();
        amend2.Status__c = 'Draft';
        amend2.ServiceRequest__c=objSR.id;
        amend2.Customer__c = objAcc.Id;
        amend2.Title_new__c = 'Mrs.';
        amend2.Family_Name__c = 'Mrs.';
        amend2.Given_Name__c = 'Mrs.';
        amend2.Non_Recognized_Auditors__c = objAcc3.Id;
        amend2.Financial_Year_End__c = '2013';
        amend2.Date_of_Birth__c = Date.newinstance(1980,10,17);
        amend2.Place_of_Birth__c = 'Pakistan';
        amend2.Passport_No__c = 'ASD122324';
        amend2.Nationality__c = nat.id;
        amend2.Nationality_list__c = 'Pakistan';
        //amend2.Phone__c = '+971562019803';
        amend2.Person_Email__c = 'abc@nsigulf.com';
        amend2.First_Name_Arabic__c ='AbcTest';
        amend2.Occupation__c='Developer';
        amend2.Apt_or_Villa_No__c='villa';
        amend2.Building_Name__c='building';
        amend2.Street__c='street';
        amend2.PO_Box__c = '45231435';
        amend2.Permanent_Native_City__c='Karachi';
        amend2.Emirate_State__c='Emairate';
        amend2.Post_Code__c='2322';
        amend2.Emirate__c='Dubai';
        amend2.Permanent_Native_Country__c = 'Pakistan';
        amend2.Is_Designated_Member__c = true;
        amend2.Company_Name__c = 'Company1';
        amend2.Former_Name__c = 'Former';
        amend2.Registration_Date__c = Date.newinstance(1980,10,17);
        amend2.Place_of_Registration__c = 'Registration';
        amend2.Relationship_Type__c='CEO';
        amend2.Status__c='Draft';
        insert amend2;
        
        Relationship__c RelUser = new Relationship__c();
        RelUser.Subject_Account__c = objAcc.id;
        RelUser.Relationship_Type__c = 'Has Auditor';
        RelUser.Start_Date__c = Date.today(); 
        RelUser.Push_to_SAP__c = true;
        RelUser.Active__c = true;
        insert RelUser;
        
        Step__c stp = new Step__c();
        stp.SR__c = objSR.Id;
        insert stp; 
        
        Step__c objStp = [select id,SR__c,SR__r.Pre_Trade_Name_1_in_Arab__c,SR__r.Previous_Trading_Name_1__c,SR__r.Customer__c,SR__r.Previous_Entity_Name__c,SR__r.Pre_Entity_Name_Arabic__c ,SR__r.Pro_Entity_Name_Arabic__c ,SR__r.Entity_Name__c,SR__r.pro_trade_name_1_in_arab__c,SR__r.Proposed_Trading_Name_1__c from Step__c where Id=:stp.Id];
        CC_Manage_Amendments.Create_Name_Change_Amendment(objStp);
        CC_Manage_Amendments.Change_Company_Name(objStp);
        CC_Manage_Amendments.Amend_Auditors(objStp);
        
        
    }
}