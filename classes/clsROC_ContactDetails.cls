/***********************************************************************************
 *  Author   : Mudasir
 *  Company  : 
 *  Date     : 11 - November - 2018
 *  Purpose  : This class is to populate clsROC_ContactDetails
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
V.No    Date         Updated By    Description
 ----------------------------------------------------------------------------------------              
V1.0    11-Nov-2018  Mudasir       Added INTERMEDIATE HOLDING COMPANY document 
       
**********************************************************************************************/

public without sharing class clsROC_ContactDetails extends Cls_ProcessFlowComponents {
    public Service_Request__c testServiceRequest{get;set;}
    public void BackendSRUpdate(){ }
    public clsROC_ContactDetails(){
        if(!Test.isRunningTest())populateDetails();
    }
    public boolean CorporateCommunicationVisible {get;set;}
    public Amendment__c CEODetails{get;set;}
    public Amendment__c CorporateCommunication{get;set;}  
    public List<Amendment__c> listOfAmendment{get;set;}
    public Integer indexOfList{get;set;}
    public List<RecordType> recordtypeIdValue; 
    public pageReference EditAmendment() { if(indexOfList !=null){       CorporateCommunicationVisible = true;    CorporateCommunication = listOfAmendment[indexOfList];        }         return null;    }     
    public PageReference save(){
        //upsert objSr;
        try{
       	 if(String.isBlank(CEODetails.Given_Name__c) ||  String.isBlank(CEODetails.Family_Name__c) || String.isBlank(CEODetails.Passport_No__c) ||  String.isBlank(CEODetails.Nationality_list__c) || String.isBlank(CEODetails.Relationship_Type__c) ||  String.isBlank(CEODetails.Person_Email__c) || String.isBlank(CEODetails.Mobile__c)) {
                setMessage('Please fill the required fields for CEO/SEO/Managing Director');  
           } else{ CEODetails.ServiceRequest__c = objSr.id; upsert CEODetails;
           }           
           if(CorporateCommunication != Null){
                   if(String.isBlank(CorporateCommunication.Given_Name__c) || string.isBlank(CorporateCommunication.Family_Name__c) || String.isBlank(CorporateCommunication.Passport_No__c) || String.isBlank(CorporateCommunication.Nationality_list__c) || String.isBlank(CorporateCommunication.Relationship_Type__c) ||  String.isBlank(CorporateCommunication.Person_Email__c) || String.isBlank(CorporateCommunication.Mobile__c)){
                        CorporateCommunicationVisible = true;
                        setMessage('Please fill the required fields  for General Communications'); 
                    }else{   CorporateCommunication.ServiceRequest__c = objSr.id;  upsert CorporateCommunication;  CorporateCommunicationVisible = false;
                    }
        	}
        
        //PageReference acctPage = new ApexPages.StandardController(objSr).view();
        //acctPage.setRedirect(true);
        if(!CorporateCommunicationVisible) populateDetails();
        return null;
        }catch(Exception ex){   ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,+ex.getMessage());  ApexPages.addMessage(myMsg);           return null;   }
         
        
    }
    public void populateDetails(){
        if(Test.isRunningTest()) objSr = testServiceRequest;
        CorporateCommunicationVisible = true;
        recordtypeIdValue = [SELECT id from RecordType where (name='Individual' or name='CEO') and sobjectType = 'Amendment__c' order by Name];
        listOfAmendment = new List < Amendment__c > ();
        CEODetails = new Amendment__c();
        CEODetails.Status__c = 'Draft';
        CEODetails.ServiceRequest__c = objSr.id;
        CEODetails.Relationship_Type__c = 'Has CEO/Regional Head/Chairman';
        CEODetails.recordtypeId = recordtypeIdValue[0].id;
        indexOfList = 0;
        if (objSr.id != null) {
            for (Amendment__c ObjAmed: [Select id, Customer__c, Status__c, Name_of_Declaration_Signatory__c, Name, Nationality__c, Title_new__c, Family_Name__c, Given_Name__c, Date_of_Birth__c, Place_of_Birth__c, Passport_No__c, Nationality_list__c, Phone__c, Non_Recognized_Auditors__c,
                    Person_Email__c, Occupation__c, Sys_Secondary__c, Capacity__c, Other_Directorships__c, Date_of_Appointment_GP__c, Name_of_incorporating_Shareholder__c, Name_of_Body_Corporate_Shareholder__c, Recognized_Auditors__c, Mobile__c, Job_Title__c,
                    Shareholder_enjoy_direct_benefits__c, First_Name__c, First_Name_Arabic__c, Representation_Authority__c, Type_of_Authority_Other__c, Amount_of_Contribution_US__c, Contribution_Type__c, DIFC_Recognized_Auditor__c,
                    Contribution_Type_Other__c, Percentage_held__c, Class_of_Units_Other_Rights__c, Apt_or_Villa_No__c, Building_Name__c, Street__c, PO_Box__c, Auditors__c, sys_create__c, Last_Name_Arabic__c, Middle_Name_Arabic__c,
                    Permanent_Native_City__c, Emirate_State__c, Post_Code__c, Address3__c, Permanent_Native_Country__c, Class__c, New_No_of_Shares__c, Total_Nominal_Value__c, Relationship_Type__c, Is_Designated_Member__c, Data_Center_Access_ID__c,
                    Company_Name__c, Former_Name__c, Registration_Date__c, Date_of_Appointment_SP__c, Date_of_Appointment_Auditor__c, Emirate__c, Amendment_Type__c, Place_of_Registration__c, Financial_Year_End__c
                    from Amendment__c where ServiceRequest__c =: objSr.Id and Relationship_Type__c != null and(Status__c = 'Active'
                        or Status__c = 'Draft') AND Related__c = '' and (Relationship_Type__c ='Has CEO/Regional Head/Chairman' or Relationship_Type__c='Has General Communication')
                ]) {
                    if (ObjAmed.Relationship_Type__c == 'Has CEO/Regional Head/Chairman'){
                       CEODetails = ObjAmed; 
                    }
                  else{
                      //ObjAmed.Relationship_Type__c = 'Has Corporate Communication';
                      ObjAmed.Relationship_Type__c = 'Has General Communication';
                      ObjAmed.ServiceRequest__c = objSr.id;
                      ObjAmed.recordtypeId = recordtypeIdValue[1].id;
                      ObjAmed.Status__c = 'Draft';
                      //CorporateCommunication = ObjAmed;
                      listOfAmendment.add(ObjAmed);
                      CorporateCommunicationVisible = false;
                    }
            }
            IF(listOfAmendment.size() ==0){  CorporateCommunication = new Amendment__c();  CorporateCommunication.Amendment_Type__c = 'Individual'; CorporateCommunication.recordtypeId = recordtypeIdValue[1].id; CorporateCommunication.Relationship_Type__c = 'Has General Communication';  CorporateCommunication.Status__c = 'Draft';  CorporateCommunication.ServiceRequest__c = objSr.id; CorporateCommunicationVisible = true;
            }
        } else {
            CEODetails.Amendment_Type__c = 'Individual';
            CEODetails.Relationship_Type__c = 'Has CEO/Regional Head/Chairman';
            CEODetails.recordtypeId = recordtypeIdValue[0].id;
            CEODetails.Status__c = 'Draft';
            CEODetails.ServiceRequest__c = objSr.id;
            
            CorporateCommunication = new Amendment__c();
            CorporateCommunication.Amendment_Type__c = 'Individual';
            CorporateCommunication.recordtypeId = recordtypeIdValue[1].id;
            CorporateCommunication.Relationship_Type__c = 'Has General Communication';
            CorporateCommunication.Status__c = 'Draft';
            CorporateCommunication.ServiceRequest__c = objSr.id;
            CorporateCommunicationVisible = true;
        }
    }
    public pageReference AddNewAmendment(){
        CorporateCommunicationVisible = true;
        CorporateCommunication = new Amendment__c();
        CorporateCommunication.Amendment_Type__c = 'Individual';
        CorporateCommunication.recordtypeId = recordtypeIdValue[1].id;
        CorporateCommunication.Relationship_Type__c = 'Has General Communication';
        CorporateCommunication.Status__c = 'Active';
        CorporateCommunication.ServiceRequest__c = objSr.id;
        return null;
    }
}