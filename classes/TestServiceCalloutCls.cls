/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestServiceCalloutCls {
	
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://eccdev.com/sampledata';
        insert objEP;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        //objSR.Internal_SR_Status__c = lstStatus[0].Id;
        //objSR.External_SR_Status__c = lstStatus[0].Id;
        insert objSR;
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Status__c = 'Added';
        objSRItem.Price__c = 1000;
        insert objSRItem;
        
        Test.setMock(WebServiceMock.class, new TestServiceCreationCls());
        
        list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = objSRItem.Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        
        list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
        SAPECCWebService.ZSF_S_ACC_BAL objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        
        TestServiceCreationCls.resItems = resItems;
        TestServiceCreationCls.resActBals = resActBals;
        
        SAPWebServiceDetails.ECCServiceCallForSchedule(new list<string>{objSR.Id}, true);
        
    }
    
    static testMethod void PushDPPrice() {
        // TO DO: implement unit test
        
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'ECC';
        objEP.URL__c = 'http://eccdev.com/sampledata';
        insert objEP;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        //objSR.Internal_SR_Status__c = lstStatus[0].Id;
        //objSR.External_SR_Status__c = lstStatus[0].Id;
        insert objSR;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Sample Test';
        objProd.ProductCode = 'SampleTest';
        objProd.IsActive = true;
        insert objProd;  
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'Notification to process personal data';
        objPL.Product__c = objProd.Id;
        objPL.Priority__c = 1;
        objPL.Material_Code__c = 'FI_1100_RGF';
        lstPLs.add(objPL);
        
        insert lstPLs;
        
        SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = objSR.Id;
        objSRItem.Status__c = 'Added';
        objSRItem.Price__c = 1000;
        objSRItem.Pricing_Line__c = lstPLs[0].Id;
        objSRItem.Product__c = objProd.Id;
        insert objSRItem;
        
        Test.setMock(WebServiceMock.class, new TestServiceCreationCls());
        
        list<SAPECCWebService.ZSF_S_RECE_SERV_OP> resItems = new list<SAPECCWebService.ZSF_S_RECE_SERV_OP>();
        SAPECCWebService.ZSF_S_RECE_SERV_OP objTemp = new SAPECCWebService.ZSF_S_RECE_SERV_OP();
        objTemp.ACCTP = 'P';
        objTemp.SGUID = objSRItem.Id;
        objTemp.SFMSG = 'Test Class';
        objTemp.BELNR = '1234567';
        resItems.add(objTemp);
        
        list<SAPECCWebService.ZSF_S_ACC_BAL> resActBals = new list<SAPECCWebService.ZSF_S_ACC_BAL>();
        SAPECCWebService.ZSF_S_ACC_BAL objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'D';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = 'H';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        objActBal = new SAPECCWebService.ZSF_S_ACC_BAL();
        objActBal.KUNNR = '001234';
        objActBal.UMSKZ = '9';
        objActBal.WRBTR = '1234';
        resActBals.add(objActBal);
        
        TestServiceCreationCls.resItems = resItems;
        TestServiceCreationCls.resActBals = resActBals;
        
        test.startTest();
        	SAPWebServiceDetails.ECCServiceCallToPushDPFuture(new list<string>{objSR.Id}, true);
        test.stopTest();
    }
}