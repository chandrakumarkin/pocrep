@isTest
public with sharing class RefundAttachmentQueuableJobTest {

    @testSetUp
    static void PrepareTestData(){
            Endpoint_URLs__c objEP = new Endpoint_URLs__c();
            objEP.Name = 'SAP Attachment';
            objEP.URL__c = 'http://accbal.com/sampledata';
            insert objEP;
            
             objEP = new Endpoint_URLs__c();
            objEP.Name = 'AccountBal';
            objEP.URL__c = 'http://accbal.com/sampledata';
            insert objEP;
            
            Block_Trigger__c objCS = new Block_Trigger__c();
            objCS.Block_Web_Services__c = false;
            objCS.Name = 'Block';
            insert objCS;
            
            WebService_Details__c objWS = new WebService_Details__c();
            objWS.Name = 'Credentials';
            objWS.SAP_User_Id__c = 'testuserid';
            objWS.Username__c = 'testuserId';
            objWS.Password__c = '123456789';
            objWS.Certificate_Name__c='123456';
            insert objWS;
        
        
        
             SR_Template__c srTemplate = new SR_Template__c();
            srTemplate.Name = 'Request a Refund';
            srTemplate.SR_RecordType_API_Name__c = 'Refund_Request';
            srTemplate.SR_Group__c = 'Other';
            srTemplate.Menu__c = 'otherServices';
            srTemplate.sub_Menu__c = 'Administrative Services';
            
            insert srTemplate;
            
           
            List<Account> accList = new List<Account>();
            Account objAccount = new Account();
            objAccount.Name = 'Test Custoer 354545';
            objAccount.E_mail__c = 'test@test.com';
            objAccount.BP_No__c = '1234';
            accList.add(objAccount);
            
             objAccount = new Account();
            objAccount.Name = 'Test Custoer 354544';
            objAccount.E_mail__c = 'test@tests.com';
            objAccount.BP_No__c = '5678';
            accList.add(objAccount);
            
            insert accList;
            
            
        
            Service_Request__c srobj = new Service_Request__c();
            srobj.Type_of_refund__c ='Portal Balance Refund';
            srobj.Mode_of_refund__c = 'Cheque in Favour of the Entity';
            srobj.Refund_Amount__c = 10000;
            srobj.I_agree__c = true;
            srobj.Customer__c = [select id from account limit 1].id;
            srObj.SR_template__c = srTemplate.id;
            srObj.RecordTypeID = [select id from RecordType where DeveloperName = 'Refund_Request' and SobjectType = 'Service_Request__c' limit 1].id;
            insert srobj;  
            
            Step_Template__c stpTemp = new Step_Template__c();
            stpTemp.Name = 'HOD Review';
            stpTemp.Code__c = 'HOD_Review';
            //stpTemp.SR_Template__c =  srTemplate.id;
            stpTemp.Step_RecordType_API_Name__c = 'General';
            insert stpTemp;
            
            SR_Steps__c srStp = new SR_Steps__c();
            srStp.SR_Template__c = srTemplate.id;
            srStp.Step_Template__c = stpTemp.id;
            insert srStp;
            
            status__c st = new status__c();
            st.Name ='Approved';
            st.Code__c = 'Approved';
            insert st;
            
           
             
            step__c stp = new step__c();
            stp.sr__c = srObj.id;
          //  stp.step_Name__c = 'HOD Review';
            stp.Step_Template__c = stpTemp.id;
            stp.Applicant_Email__c = 'c-sravan.Booragadda@difc.ae';
            stp.status__c = st.id;
            stp.Closed_date_time__c = system.now();
            insert stp;
            
          
            
            Document_Master__c dm = new Document_Master__c();
            dm.Name = 'Commercial License';
            dm.Code__c = 'Commercial Licence';
            insert dm;
            
            
            SR_Doc__c srd = new SR_Doc__c();
            srd.Name = 'Commercial License';
            srd.Document_Master__c = dm.id;
            srd.Service_Request__c = srobj.id;
            insert srd;
            
            attachment att = new attachment();
            att.Name = 'test';
            att.body = blob.valueOf('dumy text');
            att.parentId = srd.id;
            insert att;
            
            
            Email_Addresses__c ea = new Email_Addresses__c();
            ea.Name = 'Refund Emails To Finance';
            ea.To_Addresses__c = 'test@test.com;test1@test.com';
            insert ea;
    
            st = new status__c();
            st.Name ='ERRORED_FROM_SAP';
            st.Code__c = 'ERRORED_FROM_SAP';
            insert st;     
     }

    static testMethod void testRefundAttachment() {
   
       service_request__c srObj = [select id,Name,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                            Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                            Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c
                                            from service_request__c Limit 1];
       
       step__c stp = [select id,Name,SR__r.Name,SR__r.Type_of_Refund__c,SR__r.Refund_Amount__c,SR__r.Mode_of_Refund__c,SR__r.Customer__r.BP_No__c,SR__r.Bank_Name__c,
                            SR__r.Bank_Address__c,SR__r.SAP_SGUID__c,SR__r.SAP_OSGUID__c,SR__r.IBAN_Number__c,SR__r.SAP_GSTIM__c,SR__r.first_Name__c,SR__r.Last_Name__c,SR__r.SAP_Unique_No__c,
                            SR__r.Contact__r.BP_No__c,SR__r.SAP_MATNR__c,SR__r.Transfer_to_Account__r.BP_No__c,SR__r.Transfer_to_Account__c from step__c where sr__c =: srObj.id];                                     
        Test.startTest();
        Test.setMock(WebServiceMock.class, new sapComDocumentSapSoapFunctionsMcSMock());
        sapComDocumentSapSoapFunctionsMcS.ZsfRefAttachment_element ttRefundOp = new sapComDocumentSapSoapFunctionsMcS.ZsfRefAttachment_element();
      	ttRefundOp.ImExtension ='gfg';
        ttRefundOp.ImFileName ='kdda';
        ttRefundOp.ImRefid ='sasad';
        ttRefundOp.Xstr='assj';
        
        sapComDocumentSapSoapFunctionsMcSMock.ttRefundOp = ttRefundOp;
           RefundAttachmentQueuableJob.PushAttachmentToSAP(srObj);
        Test.stopTest();
    }
    
    static testMethod void testRefundAttachment2() {
   
       service_request__c srObj = [select id,Name,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,
                                            Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                            Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c
                                            from service_request__c Limit 1];
       
       step__c stp = [select id,Name,SR__r.Name,SR__r.Type_of_Refund__c,SR__r.Refund_Amount__c,SR__r.Mode_of_Refund__c,SR__r.Customer__r.BP_No__c,SR__r.Bank_Name__c,
                            SR__r.Bank_Address__c,SR__r.SAP_SGUID__c,SR__r.SAP_OSGUID__c,SR__r.IBAN_Number__c,SR__r.SAP_GSTIM__c,SR__r.first_Name__c,SR__r.Last_Name__c,SR__r.SAP_Unique_No__c,
                            SR__r.Contact__r.BP_No__c,SR__r.SAP_MATNR__c,SR__r.Transfer_to_Account__r.BP_No__c,SR__r.Transfer_to_Account__c from step__c where sr__c =: srObj.id];                                     
        Test.startTest();
           System.enqueueJob(new PushToSAPAsyncQueuable(stp,'RFCL'));
        Test.stopTest();
    }
}