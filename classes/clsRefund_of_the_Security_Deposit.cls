/******************************************************************************************
 *  Author   : Suchita
 *  Company  : DIFC
 *  Date     : 06/05/2021  
 *  Description : This class will be used as controller for Refund_of_the_Security_Deposit and Refund_SecurityDeposit_Payment_Details pages which is being used for Security Deposite Refund SR.                       
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date                Updated By          Description
 ----------------------------------------------------------------------------------------              
 V1.0   06/05/2021         Suchita               Created
 v1.1   13/07/2021         Arun                  Updated    
 TestClass: clsRefund_of_the_Security_Deposit_Test
*******************************************************************************************/
public without sharing class clsRefund_of_the_Security_Deposit extends Cls_ProcessFlowComponents {
    
    public Service_Request__c ServiceRequest {get;set;}
    public CurrentContext ContextObj {get;set;}
    public list<Selectoption> Buildings {get;set;}
    public list<Selectoption> Floors {get;set;}
    public list<Selectoption> Units {get;set;}
    public list<Selectoption> currencies {get;set;}
    public list<UnitDetails> UnitInfo {get;set;}
    public list<UnitDetails> IndividualInfo {get;set;}
    private account AccountObj{get;set;}
    public string srID {get;set;}
    public Integer RowIndex {get;set;}
    public String TenantType {get;set;}
    public String landlordType {get;set;}
    public String ownerType {get;set;}
    public String SelBuildName {get;set;}
    public String SelectedUnit {get;set;}    
    public String UsageType {get;set;}
    public boolean isUserGroup {get;set;}
    public boolean isRORPUser {get;set;}    
    public integer rowNumberToEdit{get;set;}
    public Service_Request__c SRData{get;set;} 
    public String Type{get;set;}   
    public String Building{get;set;}
    public String Floor{get;set;}
    public String Unit{get;set;}  
    public String LandlordCurrency{get;set;} 
    public String TenantCurrency{get;set;}   
    public Amendment__c IndividualTenant {get;set;}
    public Amendment__c IndividualLandlord {get;set;}
    
    public Amendment__c CompanyLandlord {get;set;}
    public Amendment__c CompanyTenant {get;set;}//v1.1
    
    
    public Amendment__c UnitDetails {get;set;}
    public List<Amendment__c> ListAmend {get;set;}
   
    public List<Amendment__c> IndLandlordList {get;set;}
    public Set<Amendment__c> IndLandlordSet {get;set;}
    public List<Amendment__c> CompLandlordList {get;set;}
    public Set<Amendment__c> CompLandlordSet {get;set;}
    
    public List<Amendment__c> IndTenantList {get;set;}
    public List<Amendment__c> CompTenantList {get;set;}//v1.1
    public List<String> lsterrmsg;
  

    public Amendment__c TempObjAnd{get;set;}
    
    public PageReference FetchExistingRelationships(){
        IndividualLandlord = (IndividualLandlord != null) ? IndividualLandlord : new Amendment__c();
        CompanyLandlord = (CompanyLandlord != null) ? CompanyLandlord : new Amendment__c();
        CompanyTenant = (CompanyTenant != null) ? CompanyTenant : new Amendment__c();//v1.1
        
        IndividualTenant = (IndividualTenant != null) ? IndividualTenant : new Amendment__c();
        UnitDetails = (UnitDetails != null) ? UnitDetails : new Amendment__c();
        IndLandlordList = (IndLandlordList != null) ? IndLandlordList : new List<Amendment__c>();
        CompLandlordList = (CompLandlordList != null) ? CompLandlordList : new List<Amendment__c>();
        IndTenantList = (IndTenantList != null) ? IndTenantList : new List<Amendment__c>();
        CompTenantList = (CompTenantList != null) ? CompTenantList : new List<Amendment__c>();


        ContextObj = new CurrentContext();
        Floors = new list<Selectoption>();
        Units = new list<Selectoption>();
        Floors.add(new Selectoption('','--Select Floor--'));
        Units.add(new Selectoption('','--Select Unit--'));
        if(Apexpages.currentPage().getParameters().get('id') != null && Apexpages.currentPage().getParameters().get('id') != '')
            ContextObj.SRId = Apexpages.currentPage().getParameters().get('id');
        getContext();
        ServiceRequest = getSRDetails(ServiceRequest);
        if(ServiceRequest.Id != null){
            
            for(Amendment__c AmndObj : [select Id,Name,ServiceRequest__c, Building2__c, Floor2__c,Unit__c,Unit__r.Name,Shareholder_Company__c,Record_Type_Name__c,Is_Primary__c,Manager__c,Amendment_Type__c,Title_new__c,Nationality_list__c,Given_Name__c,Passport_No__c,Passport_Issue_Date__c,Family_Name__c,
                                        Company_Name__c,Passport_Expiry_Date__c,Type_of_Landlord__c ,Date_of_Birth__c,Country_of_Issuance__c,Place_of_Birth__c,Emirates_ID_Number__c,Gender__c,Office_Unit_Number__c,Office_Phone_Number__c,Building_Name__c,Phone__c,Street__c,
                                        Residence_Phone_Number__c,Permanent_Native_City__c,Person_Email__c,Permanent_Native_Country__c,PO_Box__c,Middle_Name__c,Relationship_Type__c,
                                        CL_Certificate_No__c,IssuingAuthority__c,Issuance_Date__c,Issuing_Authority_Other__c,Non_Reg_Company__r.ROC_Status__c,Non_Reg_Company__c,Fax__c,Name_of_Declaration_Signatory__c,Capacity__c
                                        from Amendment__c where (Amendment_Type__c = 'Unit' or Amendment_Type__c = 'Landlord' or Amendment_Type__c = 'Tenant' or Type_of_Landlord__c = 'Individual' or Type_of_Landlord__c = 'Company') AND ServiceRequest__c =:ServiceRequest.Id]){
                                            
                                            if(AmndObj.Amendment_Type__c == 'Unit'){
                                                UnitDetails.Building2__c = AmndObj.Building2__c;
                                                
                                                //Floor = AmndObj.Floor2__c;
                                                //Floors.add(new Selectoption(Floor,Floor));
                                                UnitDetails.Unit__c = AmndObj.Unit__c;
                                                Units.add(new Selectoption(UnitDetails.Unit__c,AmndObj.Unit__r.Name));
                                            }
                                            
                                            if(AmndObj.Amendment_Type__c == 'Landlord' && AmndObj.Type_of_Landlord__c == 'Individual'){
                                                IndLandlordList.add(AmndObj);  
                                            }
                                            
                                            if(AmndObj.Amendment_Type__c == 'Tenant' && AmndObj.Type_of_Landlord__c == 'Individual'){
                                                IndTenantList.add(AmndObj);  
                                            }
                                            
                                            if(AmndObj.Amendment_Type__c == 'Landlord' && AmndObj.Type_of_Landlord__c == 'Company'){
                                                CompLandlordList.add(AmndObj);  
                                            }
                                             if(AmndObj.Amendment_Type__c == 'Tenant' && AmndObj.Type_of_Landlord__c == 'Company'){
                                                CompTenantList.add(AmndObj);  
                                            }
                                            
                                        }
        }
        
        BuildingsInfo();
        fetchCurrencies();
        if(UnitDetails==null){
            UnitDetails=new Amendment__c(); 
            UnitDetails.Amendment_Type__c='Unit';
            UnitDetails.ServiceRequest__c=ServiceRequest.id;
            
        }
        
        
        return null;
    }
    
    public Service_Request__c getSRDetails(Service_Request__c objSR){
        
        objSR = (objSR != null) ? objSR : new Service_Request__c();
        UnitInfo = new list<UnitDetails>();
        UnitDetails objUnitDetails;
        if(ContextObj.SRId != null && ContextObj.SRId != ''){
            String sQry = 'select Id,Name,SR_Group__c,Sponsor_First_Name__c ,Authority_Name__c, Activity_NOC_Required__c,Downsizing_Applicable__c,is_RM_Rejected__c,Declaration_Signatory_Name__c,SAP_GSDAT__c,Address_Details__c,Customer__c,SAP_SGUID__c,SAP_OSGUID__c,SAP_GSTIM__c, Bank_Address__c,IBAN_Number__c,Bank_Name__c,Payment_Method__c,Address_Changed__c,Middle_Name_Previous_Sponsor__c'; 
            sQry += ',Cage_No__c,Security_Deposit_AED__c,Share_Value__c,Sys_Estimated_Share_Capital__c,Number_of_Bedrooms__c,Customer_Name__c,Others_Please_Specify__c,Building__r.Name,Lease_Contract_No__c,Email__c,Service_Type__c,Submitted_Date__c,External_Status_Name__c,Send_SMS_To_Mobile__c,Service_Category__c,Type_of_Lease__c,Lease_Commencement_Date__c,Penalty_Applicable_F__c ,Penalty_Applicable__c';
            sQry += ',Lease_Expiry_Date__c,Sponsor_Middle_Name__c,Purchase_Price__c,Annual_Amount_AED__c,Contract_Amount_AED__c,Type_of_Request__c,Sponsor_Last_Name__c,Street_Previous_Sponsor__c,Confirm_Change_of_Entity_Name__c,Confirm_Change_of_Trading_Name__c,Sponsor_P_O_Box__c,Unit__c,Name_Address_DOB_of_Shareholders__c,Name_Address_DOB_of_Directors__c,Entity_Name_checkbox__c';
            sQry += ',Post_Code__c,Quantity__c,Required_Docs_not_Uploaded__c,Rental_Amount__c,Agreement_Date__c,Option_to_Renew__c,Option_to_Purchase__c,Is_Mortgagee_s_Consent_to_the_Lease__c,Building__c,Usage_Type__c,Additional_Details__c,Tax_Registration_Number_TRN__c,Business_Name__c,Commercial_Activity_Previous_Sponsor__c';
            
            sQry += ',(select Id,Name,Unit__c,Nominal_Value__c,Unit__r.Name,Unit__r.Is_Sub_unit__c ,Unit__r.Floor__c,Unit__r.Unit_Type_DDP__c,Unit__r.Building__c,Unit_Rent__c,Unit__r.Unit_Internal_Key__c,Unit__r.Parent_Unit__c,Unit__r.Parent_Unit__r.Unit_Internal_Key__c,Unit__r.SAP_Unit_No__c from Amendments__r where Amendment_Type__c = \'Unit\' AND Status__c = \'Active\' )';
            
            sQry += ' From Service_Request__c where Id=\''+ContextObj.SRId+'\' ';
            for(Service_Request__c obj : database.query(sQry)){
                objSR = obj;
                ContextObj.CustomerName = obj.Customer_Name__c;
                if(ContextObj.CustomerId == null || ContextObj.CustomerId == '')
                    ContextObj.CustomerId = obj.Customer__c;
                if(obj.Amendments__r != null){
                    for(Amendment__c objAmd : obj.Amendments__r){
                        objUnitDetails = new UnitDetails();
                        objUnitDetails.Amendment = objAmd;
                        objUnitDetails.UnitId = objAmd.Unit__c;
                        objUnitDetails.UnitName = objAmd.Unit__r.Name;
                        objUnitDetails.Floor = objAmd.Unit__r.Floor__c;
                        objUnitDetails.Description = objAmd.Unit__r.Unit_Type_DDP__c;
                        objUnitDetails.Index = UnitInfo.size();
                        objUnitDetails.BuildingId = objAmd.Unit__r.Building__c;
                        objUnitDetails.RentalAmount = objAmd.Unit_Rent__c;
                        objUnitDetails.isSubUnit = objAmd.Unit__r.Is_Sub_unit__c;
                        objUnitDetails.SecurityDeposit = objAmd.Nominal_Value__c; 
                        UnitInfo.add(objUnitDetails);
                        
                    }
                }
            }
        }
        return objSR;
    }
    
    
    public void getContext(){
        for(User  objUser : [select Id,Name,ContactId,AccountId,MobilePhone,Phone,Email,Contact.Account.Name from User where Id=:Userinfo.getUserId()] ){
            ContextObj.CurrentUser = objUser;
            ContextObj.CustomerId = objUser.AccountId;
            ContextObj.CustomerName = objUser.Contact.Account.Name;
        }
        
        ContextObj.IsPortaluser = (Userinfo.getUserType() == 'Standard') ? false : true;
        
        if(ContextObj.IsPortaluser == false){
            
            groupmember[] GMlst = [SELECT id FROM GroupMember WHERE  UserOrGroupid = :UserInfo.getUserId() and group.name ='RORP_Penalty' limit 1] ;
            if(GMlst.size()>0){  isUserGroup = true;
                              }
        }
        
        string ProfileName = [Select profile.name from user where ID =: UserInfo.getUserId() limit 1].profile.name;
        isRORPUser =  (ProfileName.contains('RORP')) ? true : false;
        
        for(RecordType objRT : [select Id,DeveloperName,Name from RecordType where sObjectType = 'Amendment__c' AND RecordType.IsActive = true AND (DeveloperName = 'Landlord' OR DeveloperName = 'Individual_Tenant' OR DeveloperName = 'Body_Corporate_Tenant' OR DeveloperName = 'Lease_Unit' OR DeveloperName = 'Occupant')]){
            if(objRT.DeveloperName == 'Body_Corporate_Tenant')
                ContextObj.BCTenantRecordTypeId = objRT.Id;
            if(objRT.DeveloperName == 'Landlord')
                ContextObj.LandLordRecordTypeId = objRT.Id;
            if(objRT.DeveloperName == 'Individual_Tenant')
                ContextObj.IndTenantRecordTypeId = objRT.Id;    
            if(objRT.DeveloperName == 'Lease_Unit')
                ContextObj.LeaseUnitRecordTypeId = objRT.Id;    
            if(objRT.DeveloperName == 'Occupant')
                ContextObj.OccupantRecordTypeId = objRT.Id;
        }
    }
    
    public override pagereference DynamicButtonAction(){ //When user click on Save or next button
        system.debug('Call DynamicButtonAction ***');
       
        if(IsValidInfo()==true || !isNext()) {
            //Save the record
            return getButtonAction();
        }else {
            for(String errmsg:lsterrmsg){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,errmsg));
            }
        }         
        clearAction();
        return null;
    }
    /*public void CancelUnits(){
        //AllUnits = new list<UnitDetails>();
        //SearchString = '';
        //isError = false;
    }*/
    public boolean IsValidInfo(){ 
        String pageName = ApexPages.currentPage().getParameters().get('currentvfpage');
        system.debug('-----current page name----'+pageName);
        Boolean IsValidData=true;
        lsterrmsg=new list<String>();

        if(pageName=='Refund_of_the_Security_Deposit'){
            if(ServiceRequest.Unit__c==null && ServiceRequest.Building__c ==null){
                IsValidData=false;
                lsterrmsg.add('Please select unit details');
            }
            
            if(IndLandlordList.isEmpty() && CompLandlordList.isEmpty()){
                IsValidData=false;
                lsterrmsg.add('Please select landlord details');
            }
            if(IndTenantList.isEmpty() && CompTenantList.isEmpty()){
                IsValidData=false;
                lsterrmsg.add('Please select Tenant details');
            }

        }else if(pageName=='Refund_SecurityDeposit_Payment_Details'){
            if(ServiceRequest.Entity_Name_checkbox__c && !(ServiceRequest.Activity_NOC_Required__c || ServiceRequest.Downsizing_Applicable__c || ServiceRequest.is_RM_Rejected__c) ){
                IsValidData=false;
                lsterrmsg.add('Atleast select one reason for retaining any portion of the Security Deposit');
            }
            
        }
        system.debug('IsValidData**'+IsValidData);
        return IsValidData; 
    }
    
    public void BuildingsInfo(){
        Buildings = new list<Selectoption>();
        Buildings.add(new Selectoption('','--Select Building--'));
        for(Building__c objBul : [select Id,Name,Building_Name_on_Forms__c from Building__c where Company_Code__c =: System.label.CompanyCode2300 AND SAP_Building_No__c != null order by Name]){
            Buildings.add(new Selectoption(objBul.Id,objBul.Building_Name_on_Forms__c));
        }
    }
    
    public void fetchFloors(){
        Floors.clear();
        Units.clear();
        Floors = new list<Selectoption>();
        Units = new list<Selectoption>();
        Floors.add(new Selectoption('','--Select Floor--'));
        Units.add(new Selectoption('','--Select Unit--'));
        Set<string> FloorVal=new Set<string>();
        for(Unit__c UnitFlr : [select Id,Name,Floor__c from Unit__c where Building__c =: UnitDetails.Building2__c AND Floor__c != null order by Floor__c]){
            if(!FloorVal.contains(UnitFlr.Floor__c))
            {
                Floors.add(new Selectoption(UnitFlr.Floor__c,UnitFlr.Floor__c));
                FloorVal.add(UnitFlr.Floor__c);
            }
            
        }  
    }
    
    public void fetchUnits(){
        Units = new list<Selectoption>();
        Units.add(new Selectoption('','--Select Unit--'));//Bulding
        for(Unit__c Unit : [select Id,Name, Floor__c from Unit__c where Building__c =: UnitDetails.Building2__c and Floor__c =: Floor AND Floor__c != null  order by Name]){
            Units.add(new Selectoption(Unit.Id,Unit.Name));
        }
    }
    public void fetchCurrencies(){
        currencies = new list<Selectoption>();
        currencies.add(new Selectoption('','--Select Currency--'));
        for(Currency__c cur : [select id,Active__c, Currency_Code__c, DIFCStat_Currency__c, End_Date__c, Exchange_Rate__c, Start_Date__c  from Currency__c where Active__c=true]){
            currencies.add(new Selectoption(cur.Id,cur.DIFCStat_Currency__c));
        }
    }
    
    public void CancelIndividual(){
        Type='';
    }
    
    
    public void AddIndividual(){
        if(IndividualLandlord != null){
            IndividualLandlord.Type_of_Landlord__c = 'Individual';
            IndividualLandlord.Amendment_Type__c = 'Landlord';
            IndividualLandlord.ServiceRequest__c = ServiceRequest.Id;
            IndividualLandlord.RecordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Landlord').getRecordTypeId();
            Upsert IndividualLandlord;
            IndLandlordList = [Select Title_new__c, Given_Name__c, Middle_Name__c, Family_Name__c, Nationality_list__c, Passport_No__c,Email_Address__c From Amendment__c Where ServiceRequest__c =:ServiceRequest.Id And Amendment_Type__c = 'Landlord' And Type_of_Landlord__c = 'Individual'];
            if(UnitDetails.Building2__c != null && UnitDetails.Unit__c!= null && Floor != null){
                UnitDetails.Amendment_Type__c = 'Unit';
                UnitDetails.ServiceRequest__c = ServiceRequest.Id;
                UnitDetails.RecordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Lease_Unit').getRecordTypeId();
                UnitDetails.Floor2__c = Floor;
                UnitDetails.Building_Name__c= UnitDetails.Building2__r.Name;
                Upsert UnitDetails Id;
            }
            
        }
        IndividualLandlord.clear();
        IndividualLandlord = new Amendment__c();
        Type = '';
        
    }
    public void AddComp(){
        try{
            if(CompanyLandlord != null){
                CompanyLandlord.Type_of_Landlord__c = 'Company';
                CompanyLandlord.Amendment_Type__c = 'Landlord';
                CompanyLandlord.RecordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Landlord').getRecordTypeId();
                CompanyLandlord.ServiceRequest__c = ServiceRequest.Id;
                
                Upsert CompanyLandlord;
                CompLandlordList = [Select Company_Name__c, CL_Certificate_No__c, IssuingAuthority__c, Issuing_Authority_Other__c,Email_Address__c From Amendment__c Where ServiceRequest__c =:ServiceRequest.Id And Amendment_Type__c = 'Landlord' And Type_of_Landlord__c = 'Company' And Company_Name__c != null];
                
                if(UnitDetails.Building2__c != null && UnitDetails.Unit__c!= null && Floor != null){
                    UnitDetails.Amendment_Type__c = 'Unit';
                    UnitDetails.ServiceRequest__c = ServiceRequest.Id;
                    UnitDetails.RecordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Lease_Unit').getRecordTypeId();
                    UnitDetails.Floor2__c = Floor;
                    UnitDetails.Building_Name__c= UnitDetails.Building2__r.Name;
                    Upsert UnitDetails;
                }
            }
            CompanyLandlord.clear();
            CompanyLandlord = new Amendment__c();
            Type = '';
        }catch(Exception ex){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please contact the support.'+ex.getMessage()));
            system.debug('Exception is : '+ex.getMessage());
        }
        
    }
    
    public void AddTenant(){
        if(IndividualTenant != null){
            IndividualTenant.Type_of_Landlord__c = 'Individual';
            IndividualTenant.Amendment_Type__c = 'Tenant';
            IndividualTenant.ServiceRequest__c = ServiceRequest.Id;
            IndividualTenant.RecordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Individual_Tenant').getRecordTypeId();
            Upsert IndividualTenant;
            IndTenantList = [Select Title_new__c, Person_Email__c, Given_Name__c, Middle_Name__c, Family_Name__c, Nationality_list__c, Passport_No__c,Email_Address__c From Amendment__c Where ServiceRequest__c =:ServiceRequest.Id And Amendment_Type__c = 'Tenant' And Type_of_Landlord__c = 'Individual'];
            List<Amendment__c> UnitDetailsList = new List<Amendment__c>();
            if(UnitDetails.Building2__c != null && UnitDetails.Unit__c!= null && Floor != null){
                UnitDetails.Amendment_Type__c = 'Unit';
                UnitDetails.ServiceRequest__c = ServiceRequest.Id;
                UnitDetails.RecordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Lease_Unit').getRecordTypeId();
                UnitDetails.Floor2__c = Floor;
                UnitDetails.Building_Name__c= UnitDetails.Building2__r.Name;
                Upsert UnitDetails;
            }
        }
        IndividualTenant.clear();
        IndividualTenant = new Amendment__c();
        Type = '';
    }

//V1.1
        public void CopTenant(){
        if(CompanyTenant != null){
            System.debug('CompanyTenant==>'+CompanyTenant);
            CompanyTenant.Type_of_Landlord__c = 'Company';
            CompanyTenant.Amendment_Type__c = 'Tenant';
            CompanyTenant.ServiceRequest__c = ServiceRequest.Id;
            CompanyTenant.RecordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('BC_Tenant').getRecordTypeId();
            Upsert CompanyTenant;

            CompTenantList  = [Select Company_Name__c, CL_Certificate_No__c, IssuingAuthority__c, Issuing_Authority_Other__c,Email_Address__c From Amendment__c Where ServiceRequest__c =:ServiceRequest.Id And Amendment_Type__c = 'Tenant' And Type_of_Landlord__c = 'Company'];
            List<Amendment__c> UnitDetailsList = new List<Amendment__c>();

            if(UnitDetails.Building2__c != null && UnitDetails.Unit__c!= null && Floor != null)
            {
                UnitDetails.Amendment_Type__c = 'Unit';
                UnitDetails.ServiceRequest__c = ServiceRequest.Id;
                UnitDetails.RecordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Lease_Unit').getRecordTypeId();
                UnitDetails.Floor2__c = Floor;
                UnitDetails.Building_Name__c= UnitDetails.Building2__r.Name;
                Upsert UnitDetails;
            }
        }
        CompanyTenant.clear();
        CompanyTenant = new Amendment__c();
        Type = '';
    }
    public void AddUnit(){
        if(ServiceRequest != null){
                UnitDetails.Amendment_Type__c = 'Unit';
                UnitDetails.ServiceRequest__c = ServiceRequest.Id;
                UnitDetails.RecordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Lease_Unit').getRecordTypeId();
                UnitDetails.Building2__c = ServiceRequest.Building__c;
                UnitDetails.Unit__c=ServiceRequest.Unit__c;
                UnitDetails.Building_Name__c= ServiceRequest.Building__r.Name;
                Upsert UnitDetails Id;
                Upsert ServiceRequest;
        }
    }

    public void AddServiceRequest(){
        if(ServiceRequest != null){
            Boolean hasError = false;
            if(UnitDetails.Unit__c!=null){
                getBPNumberforUnit(UnitDetails.Unit__c,ServiceRequest.id);
            }
            
            if(UnitDetails.Unit__c !=null && UnitDetails.Building2__c!=null){
                ServiceRequest.Building__c=UnitDetails.Building2__c;
                ServiceRequest.Unit__c=UnitDetails.Unit__c;
            }
            if(ServiceRequest.SR_Group__c==null){ServiceRequest.SR_Group__c='RORP';}
            
            if(ServiceRequest.Confirm_Change_of_Trading_Name__c=='Domestic' && ServiceRequest.SAP_GSDAT__c.length()!=23){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please enter 23 char IBAN Number'));
                hasError=true;
            }else if(ServiceRequest.Confirm_Change_of_Entity_Name__c=='Domestic' && ServiceRequest.IBAN_Number__c.length()!=23){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please enter 23 char IBAN Number'));
                hasError=true;
            }
            if(!hasError){
                Update ServiceRequest;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Saved successfully!'));
            }
           
        }
    }
    
    public void SaveAmendment(){
        try{
            if(UnitInfo != null && RowIndex != null && UnitInfo.size() > RowIndex){
                if(UnitInfo[RowIndex].Amendment != null && UnitInfo[RowIndex].Amendment.Id != null){
                    update UnitInfo[RowIndex].Amendment;
                    
                    //UnitInfo[RowIndex].RentalAmount = UnitInfo[RowIndex].Amendment.Unit_Rent__c;
                    UnitInfo[RowIndex].isEdit = false;
                    
                    /* decimal totalRentalAmount = 0;
for(UnitDetails objUD : UnitInfo){
if(objUD.RentalAmount != null && objUD.RentalAmount != 0){   totalRentalAmount += objUD.RentalAmount;
}
}
ServiceRequest.Rental_Amount__c = totalRentalAmount;*/
                 
                    upsert ServiceRequest;  
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Saved successfully!'));
                }/*else if(UnitInfo[RowIndex].RentalAmount != null){
UnitInfo[RowIndex].isEdit = false;
}else{
Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please enter the rental amount.'));
}*/
            }
        }catch(Exception ex){ Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please contact the support.'));
                             system.debug('Exception is : '+ex.getMessage());
                            }  
    }
    
    public void AddLandLordBankDetails(){
        Upsert ServiceRequest;
        ServiceRequest = new Service_Request__c();
    }
    
    public void EditAmendment(){
        if(UnitInfo != null && RowIndex != null && UnitInfo.size() > RowIndex){
            for(UnitDetails objUD : UnitInfo){
                if(objUD.isEdit == true){
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please save/cancel the row which is open.'));
                    return;
                }
            }
            //UnitInfo[RowIndex].TempRentalAmount = UnitInfo[RowIndex].RentalAmount;
            UnitInfo[RowIndex].isEdit = true;
        }
    }
    
    public void CancelAmendment(){
        if(UnitInfo != null && RowIndex != null && UnitInfo.size() > RowIndex){
            if(UnitInfo[RowIndex].Amendment != null && UnitInfo[RowIndex].Amendment.Id != null){
                UnitInfo[RowIndex].isEdit = false;
                //UnitInfo[RowIndex].Amendment.Unit_Rent__c = UnitInfo[RowIndex].RentalAmount;
            }else{
                //UnitInfo[RowIndex].RentalAmount = UnitInfo[RowIndex].TempRentalAmount;
                UnitInfo[RowIndex].isEdit = false;
            }
        }
    }
    
    public void removingUBORow(){
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        if(IndLandlordList.size()>=param)
        {
            Amendment__c TempObjPro=IndLandlordList[param];
            IndLandlordList.remove(param);
            if(TempObjPro.id!=null)
                delete TempObjPro;
        }  
    }
    public void removingUBORowComp(){
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        if(CompLandlordList.size()>=param)
        {
            Amendment__c TempObjPro=CompLandlordList[param];
            CompLandlordList.remove(param);
            if(TempObjPro.id!=null)
                delete TempObjPro;
        }  
    }
    public void removingUBORowTen(){
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        if(IndTenantList.size()>=param)
        {
            Amendment__c TempObjPro=IndTenantList[param];
            IndTenantList.remove(param);
            if(TempObjPro.id!=null)
                delete TempObjPro;
        }  
    }
        public void removingRowTenBC(){
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        if(CompTenantList.size()>=param)
        {
            Amendment__c TempObjPro=CompTenantList[param];
            CompTenantList.remove(param);
            if(TempObjPro.id!=null)
                delete TempObjPro;
        }  
    }
            
    public  void AddIndividualLandlord(){
        Type='Individual';
        IndividualLandlord = new Amendment__c();
        CompanyLandlord.RecordTypeId =Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Landlord').getRecordTypeId();
    }
    public  void AddCompanyLandlord(){
        Type='Company';
        CompanyLandlord = new Amendment__c();
        CompanyLandlord.RecordTypeId =Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Landlord').getRecordTypeId();
    }
    
    public  void AddIndividualTenant(){
        Type='IndividualTenant';
        IndividualTenant = new Amendment__c();
        IndividualTenant.RecordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Individual_Tenant').getRecordTypeId();
    }

       public  void AddCompanyTenant(){
        Type='CompanyTenant';
         CompanyTenant = new Amendment__c();
         CompanyTenant.RecordTypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Body_Corporate_Tenant').getRecordTypeId();
    }




    Public Static Void securityDepositBPNumber(Service_Request__c ServiceRequest){
        String UnitId;
        Service_Request__c SRObj = [select id,Record_Type_Name__c,Unit__c from Service_Request__c where id=:ServiceRequest.id];
        String UnitRecordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Lease_Unit').getRecordTypeId();
        if(SRObj.Record_Type_Name__c=='Lease_Registration' || SRObj.Record_Type_Name__c=='Refund_of_The_Security_Deposit'){
            for(Amendment__c amdobj: [select id,ServiceRequest__c,Unit__c,Unit__r.Parent_Unit__c from Amendment__c where ServiceRequest__c=:SRObj.id and RecordTypeId=:UnitRecordtypeId]){
                UnitId=amdobj.Unit__c;
            }
        }else if(SRObj.Record_Type_Name__c=='Surrender_Termination_of_Lease_and_Sublease'){
            UnitId=SRObj.Unit__c;
        }
        if(UnitId!=null){
            clsRefund_of_the_Security_Deposit.getBPNumberforUnit(UnitId,SRObj.id);
        }
    }
    
    public Static void getBPNumberforUnit(String UnitId,String CurrentSRId){
        String LatestLeaseRegistrationSR;
        String LeaseNumber;
        String TanentBP;
        String LandlordBP;
        Service_Request__c currentSR = [select id,Lease_Expiry_Date__c,Lease_Commencement_Date__c,Record_Type_Name__c from Service_Request__c where id=:CurrentSRId];
        Boolean isLeaseRegRequest = currentSR.Record_Type_Name__c=='Lease_Registration'?false:true;
        String IndividualTenantRecordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Individual_Tenant').getRecordTypeId();
        String BCTenantRecordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Body_Corporate_Tenant').getRecordTypeId();
        String UnitRecordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Lease_Unit').getRecordTypeId();
        String LandlordRecordtypeId = Schema.SObjectType.Amendment__c.getRecordTypeInfosByDeveloperName().get('Landlord').getRecordTypeId();
        Date Lease_Commencement_Date= currentSR.Lease_Commencement_Date__c;
        Date Lease_Expiry_Date= currentSR.Lease_Expiry_Date__c;
        String amdQuery = 'select id,ServiceRequest__c,Unit__c,Unit__r.Parent_Unit__c,Address1__c,ServiceRequest__r.Completed_Date_Time__c from Amendment__c where Unit__c=:UnitId and RecordTypeId=:UnitRecordtypeId'+
            ' and ServiceRequest__c!=null and ServiceRequest__r.Record_Type_Name__c=\'Lease_Registration\' and ServiceRequest__r.External_Status_Name__c=\'Approved\''+
            ( isLeaseRegRequest ? ' and ServiceRequest__r.Lease_Commencement_Date__c=:Lease_Commencement_Date and ServiceRequest__r.Lease_Expiry_Date__c=:Lease_Expiry_Date': '' ) +
            ' order by ServiceRequest__r.Completed_Date_Time__c';
        
        for(Amendment__c amdobj: Database.query(amdQuery)){
            
            LatestLeaseRegistrationSR = amdobj.ServiceRequest__c;
            LeaseNumber=amdobj.Address1__c;
            if(amdobj.Unit__r.Parent_Unit__c!=null){
                UnitId=amdobj.Unit__r.Parent_Unit__c;// In Case of sub Unit Landlord available on Parent Unit
            }
        }
        System.debug('LatestLeaseRegistrationSR'+LatestLeaseRegistrationSR);
        //logic to get tanent BP
        for(amendment__c tenant:[select id,ServiceRequest__c,Company_Name__c,Shareholder_Company__c,Shareholder_Company__r.BP_No__c,Contact__c,Contact__r.BP_No__c,Is_Primary__c from Amendment__c 
                                 where (RecordTypeId=:BCTenantRecordtypeId or RecordTypeId=:IndividualTenantRecordtypeId) and Amendment_Type__c='Tenant' and ServiceRequest__c=:LatestLeaseRegistrationSR ]){
                                     if(tenant.Shareholder_Company__c!=null){
                                         TanentBP = tenant.Shareholder_Company__r.BP_No__c;
                                     }if(tenant.Contact__c!=null){
                                         TanentBP = tenant.Contact__r.BP_No__c;
                                     }
                                     if(tenant.Is_Primary__c){ // Condition when we have primary and joint tenant get the BP of Primary Tenant only 
                                         break;
                                     }
                                 }
        //Logic to get Landlord BP
        for(Lease_Partner__c leasepartnerObj:[select Contact__c,Contact__r.BP_No__c,Account__c,Account__r.BP_No__c from Lease_Partner__c where Unit__c=:UnitId and 
                                              ((Type__c='Landlord' and Lease_Types__c='Purchased Prop. Registration') or Type__c='Developer')  and Lease_Status__c='Active' order by createddate]){
                                                  if(leasepartnerObj.Contact__c != null){
                                                      LandlordBP = leasepartnerObj.Contact__r.BP_No__c;
                                                  }else if(leasepartnerObj.Account__c != null){
                                                      LandlordBP = leasepartnerObj.Account__r.BP_No__c;
                                                  }
                                              }
        List<Service_Request__c> BPNumberList = New List<Service_Request__c>();
        for(Service_Request__c SR: [select id,Sponsor_Last_Name__c,Sponsor_Street__c,Lease_Contract_No__c from Service_Request__c where id=:CurrentSRId]){
            SR.Sponsor_Last_Name__c=TanentBP;
            SR.Sponsor_Street__c=LandlordBP;
            BPNumberList.add(SR);
        }
        System.debug('TanentBP'+TanentBP);
        System.debug('LandlordBP'+LandlordBP);
        if(!BPNumberList.isEmpty()){
            Update BPNumberList;
        }
    }
    public class CurrentContext{
        public User CurrentUser {get;set;}
        public String CustomerId {get;set;}
        public String CustomerName {get;set;}
        public String RecordTypeId {get;set;}
        public String SRId {get;set;}
        public Boolean IsPortaluser {get;set;}
        public string MenuText {get;set;}
        public string Description {get;set;}
        public Boolean IsDetail {get;set;}
        public String LandLordRecordTypeId {get;set;}
        public String IndTenantRecordTypeId {get;set;}
        public String BCTenantRecordTypeId {get;set;}
        public String LeaseUnitRecordTypeId {get;set;}
        public String OccupantRecordTypeId {get;set;}
    }
    
    public class UnitDetails{
        public string UnitId {get;set;}
        public string UnitName {get;set;}
        public string Floor {get;set;}
        public string Description {get;set;}
        public string UnitArea {get;set;}
        public decimal RentalAmount {get;set;}
        public Amendment__c Amendment {get;set;}
        public Boolean isLease {get;set;}
        public Boolean isChecked {get;set;}
        public Integer Index {get;set;}
        public string BuildingId {get;set;}
        public string UnitUniqueKey {get;set;}
        public Boolean isEdit {get;set;}
        public decimal TempRentalAmount {get;set;}
        public Boolean issubunit {get;set;}
        public string Description1 {get;set;}
        public string UnitUsageType{get;set;}
        public decimal SecurityDeposit{get;set;}
        
    }
    
}