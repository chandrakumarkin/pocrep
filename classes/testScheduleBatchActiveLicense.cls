@istest(seealldata=true)

public class testScheduleBatchActiveLicense{  

     static testmethod void schedulerTest() 
    {
        String CRON_EXP = '0 0 0 15 3 ? *';
        
        // Create your test data
        Account acc = new Account();
        acc.name= 'test';
        acc.ROC_Status__c='Active';
        insert acc;
        
        Lease__c  lse = new Lease__c();     
        lse.Account__c = acc.id;
        lse.Lease_Types__c = 'Leased Property Registration';
        lse.Start_date__c = system.Today();
        lse.End_Date__c = system.Today() +10;
        lse.Status__c = 'Expired';
        lse.Square_Feet__c = '100000';
        lse.Type__c= 'Leased';
        insert lse;
        
        Test.startTest();

            String jobId = System.schedule('ScheduleApexClassTest',  CRON_EXP, new ScheduledBatchActiveLicense ());
            
            CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
            System.assertEquals(CRON_EXP, ct.CronExpression);
            System.assertEquals(0, ct.TimesTriggered);

        Test.stopTest();
        // Add assert here to validate result
    }
    

}