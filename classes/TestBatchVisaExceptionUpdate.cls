@isTest(seealldata=false)
private class TestBatchVisaExceptionUpdate{

     static testMethod void myUnitTest() {
     
      Account acc = new Account();
      
      acc.name = 'test';
      acc.Exception_Visa_Quota__c  = 10;
      insert acc;
      
     
       Visa_Exception__c  vx = new Visa_Exception__c ();
       vx.Visa_Exception_Review_Date__c  = system.today().addDays(-1);
       vx.Exception_Visa_Quota__c = 5;
       vx.Account__c = acc.id;
       insert vx;
       
        test.startTest();
        BatchVisaExceptionUpdate obj = new BatchVisaExceptionUpdate();
        database.executeBatch(obj);        
        test.stopTest();
       
       
     
     }
     
      static testMethod void myUnitTest1() {
     
      Account acc = new Account();
      
      acc.name = 'test';
      acc.Exception_Visa_Quota__c  = 10;
      insert acc;
      
     
       Visa_Exception__c  vx = new Visa_Exception__c ();
       vx.Visa_Exception_Review_Date__c  = system.today().addDays(-1);
       vx.Exception_Visa_Quota__c = 5;
       vx.Account__c = acc.id;
       insert vx;
       
        test.startTest();
           Datetime dt = system.now().addMinutes(1);
          string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();      
          system.schedule('TestGsScheduleBatchEmployeeNotifications1', sCornExp, new BatchVisaExceptionUpdate ());        
            
        test.stopTest();
       
       
     
     }
     
      static testMethod void myUnitTest2() {
     
       Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        //objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Index_Card_Status__c = 'Active';
        objAccount.Exception_Visa_Quota__c  = 10;
        
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Last RB';
        objContact.Email = 'test@difcportal.com';
        objContact.AccountId = objAccount.Id;
        objContact.FirstName = 'Nagaboina';        
        objContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        insert objContact;
         
        User objUser = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                        LocaleSidKey='en_US', ProfileId = objProfile.Id,
                        ContactId=objContact.Id, Community_User_Role__c='Company Services;Employee Services',
                        TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        Contact objGSContact = new Contact();
        objGSContact.LastName = 'Last RB';
        objGSContact.Email = 'test@difcportal.com';
        objGSContact.AccountId = objAccount.Id;
        objGSContact.FirstName = 'Nagaboina';
        objGSContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objGSContact;
        
      
      
      
     
       Visa_Exception__c  vx = new Visa_Exception__c ();
       vx.Visa_Exception_Review_Date__c  = system.today().addDays(15);
       vx.Exception_Visa_Quota__c = 5;
       vx.Account__c = objAccount.id;
       insert vx;
       
        test.startTest();
        GsScheduleBatchVisaExcpEmpNotifications obj = new GsScheduleBatchVisaExcpEmpNotifications();
        database.executeBatch(obj);        
        test.stopTest();
       
       
     
     }





}