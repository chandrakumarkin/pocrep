/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 4/28/2020
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    4/28/2020  shoaib    Created
**********************************************************************************************************************/
public  class  EmployeeComponentController {

    private static final String RECORD_TYPE_ID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('DIFC_VIRTUAL_JOB_MARKET').getRecordTypeId();
    private static final List < String > RELATIONSHIP_TYPE = new List < String > {
        'Has DIFC Sponsored Employee',
        'Has DIFC Local Employee',
        'Has DIFC Local /GCC Employees',
        'Has DIFC GCC Employee',
        'Has Temporary Employee'
    };
    private static final List < String > STATUSES = new List < String > {
        'Active',
        'Cancelled'
    };

    /**
     * return data to the component 
     * @param  Status,valueName,pageNumber. 
     * @return OuterWrapperClass.          
     */
    @AuraEnabled
    public static OuterWrapperClass thisWrapper(String Status, String valueName, Integer pageNumber, String sortField, boolean isAsc) {
        List < wrapperClass > thisWrapperClass = new List < wrapperClass > ();
        Integer totalRecords;

        User thisUser = [SELECT Id,
            Name,
            ContactId,
            AccountId,
            Phone,
            Email,
            Contact.Account.Name,
            Contact.BP_No__c,
            Contact.Account.Active_License__c
            FROM User
            WHERE Id =: Userinfo.getUserId()
        ];

        String AccountId = thisUser.AccountId;
        Set < String > bpNumbersSet         = bpNumbersSet(AccountId);
        Map < Id, Contact > newContactMap   = new Map < Id, Contact > ([SELECT Id FROM Contact WHERE Passport_No__c IN: bpNumbersSet]);
        Map < Id, Relationship__c > thisMap = new Map < Id, Relationship__c > ([SELECT Id FROM Relationship__c WHERE Object_Contact__c IN: newContactMap.keyset()]);

        Set <String> idsSet = new Set <String> ();
        Date soqlDate     = Date.valueOf(System.Label.CancelledDate);

        String soqlQuery = 'SELECT Id ,Status__c,Object_Contact__r.FirstName,Object_Contact__r.BP_No__c,Object_Contact__r.LastName,Object_Contact__r.Passport_No__c,';
        soqlQuery += ' Object_Contact__r.Email,Object_Contact__r.Job_Title_Text__c,Object_Contact__r.MobilePhone FROM Relationship__c ';
        soqlQuery += 'WHERE Subject_Account__c =: AccountId  AND Relationship_Type__c IN: RELATIONSHIP_TYPE  AND End_Date__c >: soqlDate ';


        if (!thisMap.isEmpty()) {
            for (string thisSet: thisMap.keyset()) {
                idsSet.add(thisSet);
            }
            soqlQuery += ' AND Id NOT IN : idsSet';
        }

        String newValue = valueName.trim();

        Set < Id > completeRelationShipIds    = new Set < Id > ();
        Set < String > duplicateIds           = new Set < String > ();
     
        for (Relationship__c thisRelationShip: Database.query(soqlQuery)) {
            if (!duplicateIds.contains(thisRelationShip.Object_Contact__c) && thisRelationShip.Status__c == 'active') {
                completeRelationShipIds.add(thisRelationShip.Id);
                duplicateIds.add(thisRelationShip.Object_Contact__c);
            }
        }

        for (Relationship__c thisRelationShip: Database.query(soqlQuery)) {
            if (!duplicateIds.contains(thisRelationShip.Object_Contact__c) && thisRelationShip.Status__c == 'cancelled') {
                completeRelationShipIds.add(thisRelationShip.Id);
                duplicateIds.add(thisRelationShip.Object_Contact__c);
            }
        }
        
      
        
        String sqlQuery = 'SELECT Id ,Relationship_Type__c,Object_Contact__r.Job_Title__c,End_Date__c,Individual_Phone__c,Individual_Job_Title__c,Individual_Email__c,Relationship_Passport__c,Object_Contact__c,Object_Contact__r.FirstName,Object_Contact__r.BP_No__c,Object_Contact__r.LastName,Object_Contact__r.Passport_No__c,';
        sqlQuery       += ' Object_Contact__r.Email,Object_Contact__r.Job_Title_Text__c,Object_Contact__r.MobilePhone FROM Relationship__c ';
        sqlQuery       += 'WHERE  Id  IN : completeRelationShipIds AND End_Date__c >: soqlDate ';
        sqlQuery       += ' AND Object_Contact__r.Name like \'' + string.escapeSingleQuotes(newValue) + '%\'';
        sqlQuery       += ' LIMIT 10';
        
        if(String.isNotBlank(valueName)){
            for (Relationship__c thisRelationShip: Database.query(sqlQuery)) {
                thisWrapperClass.add(new wrapperClass(thisRelationShip));
            }
            if(test.isRunningTest()) {
                thisWrapperClass.add(new wrapperClass(new Relationship__c()));
            }
        
        OuterWrapperClass thisOuterWrapper    = new OuterWrapperClass();
        thisOuterWrapper.thisRelationShipList = thisWrapperClass;
        return thisOuterWrapper;
        }
        return null;
    }

    /**
     * Save service Request
     * @param  selectedId. 
     * @return Service_Request__c.          
     */
    @AuraEnabled
    public static Service_Request__c saveSR(String selectedId,String objlist) {
        
        Service_Request__c thisServiceRequest = new Service_Request__c();
        Account thisAccount                   = new Account();

        List < Cancelled_Employees__c > previousCancelledEmployee = new List < Cancelled_Employees__c > ();
        List < Cancelled_Employees__c > updateCancelledEmployee = new List < Cancelled_Employees__c > ();
        List<Attachment> attachList = new List<Attachment>();

        
        User objUser = [SELECT Id,
            Name,
            ContactId,
            AccountId,
            Phone,
            Email,
            Contact.Account.Name,
            Contact.BP_No__c,
            Contact.Account.Active_License__c
            FROM User
            WHERE Id =: Userinfo.getUserId()
        ];

        String SUBMITTED = [SELECT Id 
            FROM SR_Status__c 
            WHERE Code__c = 'SUBMITTED'
            LIMIT 1
        ].Id;

        thisServiceRequest.Customer__c           = objUser.AccountId;
        thisServiceRequest.License__c            = objUser.Contact.Account.Active_License__c;
        thisServiceRequest.Email__c              = objUser.Email;
        thisServiceRequest.Send_SMS_To_Mobile__c = objUser.Phone;
        thisServiceRequest.RecordTypeId          = RECORD_TYPE_ID;


        insert thisServiceRequest;

        List < Object > fieldList  = (List < Object > ) JSON.deserializeUntyped(selectedId);
        List < Object > newObjList = (List < Object > ) JSON.deserializeUntyped(objlist);
        
        for (Object thisObject: fieldList) {
            Map < String, Object > dataMap = (Map < String, Object > ) thisObject;
            String FirstName               = (String) dataMap.get('FirstName');
            String LastName                = (String) dataMap.get('LastName');
            String PassportNumber          = (String) dataMap.get('passport');
            String Emails                  = (String) dataMap.get('email');
            String Mobile                  = (String) dataMap.get('mobile');
            String exp                     = (String) dataMap.get('exp');
            String desg                    = (String) dataMap.get('desg');
            String BPNumber                = (String) dataMap.get('bpNumber');
            String RelationshipType        = (String) dataMap.get('relationShip');
            
            previousCancelledEmployee.add(new Cancelled_Employees__c(First_Name__c = FirstName,
                Last_Name__c       = LastName,
                Passport_Number__c = passportNumber,
                Service_Request__c = thisServiceRequest.Id,
                Designation__c     = desg,
                Email__c           = Emails,
                EMPLOYEE_CONSENT__c  = exp,
                Mobile__c              = Mobile,
                Account__c             = objUser.AccountId,
                BP_Number__c           = BPNumber,
                Relationship_Type__c   = RelationshipType));
        }
        
       if (!previousCancelledEmployee.isEmpty()) insert previousCancelledEmployee;
        
        for(Cancelled_Employees__c thisCancelledEmployees : previousCancelledEmployee){
           for (Object thisObject: newObjList) {
              Map < String, Object > dataMap = (Map < String, Object > ) thisObject;
              String passport                = (String) dataMap.get('parentId');
              String fileName                = (String) dataMap.get('fileName');
              String base64Data              = (String) dataMap.get('base64Data');
              String contentType             = (String) dataMap.get('contentType');
              String experience              = (String) dataMap.get('experience');
              String linkedUrl               = (String) dataMap.get('linkedUrl');
              String relevantexperience               = (String) dataMap.get('relevantexperience');
              
              if(String.isBlank(experience)){
                 experience = '0';
               }
                
               if(thisCancelledEmployees.Passport_Number__c == passport){
                   
                   if(String.isNotBlank(base64Data)){
                      Attachment attach =  SaveFile(thisCancelledEmployees.Id, fileName, base64Data, contentType); 
                      attachList.add(attach);
                   }
                   
                   thisCancelledEmployees.Experience_In_Years__c = Decimal.valueOf(experience);
                   thisCancelledEmployees.LinkedIn_Profile__c = linkedUrl;
                   thisCancelledEmployees.Area_of_relevant_experience__c = relevantexperience;
                   updateCancelledEmployee.add(thisCancelledEmployees);
               }
           }  
        }
        
        insert attachList;
        
        update updateCancelledEmployee;
        
        thisServiceRequest.External_SR_Status__c = SUBMITTED;
        thisServiceRequest.Internal_SR_Status__c = SUBMITTED;
        Update thisServiceRequest;

        return [SELECT Id, Name, Customer__r.Name, External_Status_Name__c FROM Service_Request__c WHERE Id =: thisServiceRequest.id LIMIT 1];
    }

    /**
     * bpNumbersSet
     * @param  recordId. 
     * @return Set<String>.          
     */
    private static Set < String > bpNumbersSet(String recordId) {
        
        Set < String > BPNumbers = new Set < String > ();
        for (Cancelled_Employees__c thisEmployee: [SELECT Id, Service_Request__c, BP_Number__c FROM Cancelled_Employees__c WHERE Account__c =: recordId]) {
            BPNumbers.add(thisEmployee.BP_Number__c);
        }
        return bpNumbers;
    }
    
      /**
     * bpNumbersSet
     * @param  recordId. 
     * @return Set<String>.          
     */
   @AuraEnabled
    public static List <Lookup__c> getJobTitle(String searchKeyWord) {
        String searchString = searchKeyWord + '%';
        return [SELECT Id, Name FROM Lookup__c WHERE Name LIKE  :searchString  ORDER BY Name ASC NULLS LAST LIMIT 8];
    }

     /**
     * bpNumbersSet
     * @param  recordId. 
     * @return Set<String>.          
     */
   @AuraEnabled
   public static List <Contacts> getcontacts(String searchKeyWord,String relationshipType,string selectedType) {
       
       List<Contacts> thisContactList = new  List<Contacts>();
       String searchString = searchKeyWord + '%';
       
       Set<String> statusValid = new Set<String>();
       if(selectedType == 'Renewal' || selectedType == 'Cancellation')statusValid = new Set<String>{'Active','Expired'};
       if(selectedType == 'Amendment' || selectedType == 'Lost Replacement')statusValid = new Set<String>{'Active'};
        system.debug('========statusValid==========='+statusValid);
        for(Relationship__c thisRel : [SELECT Id,Object_Contact__c,Object_Contact__r.Name,Contact_Name__c FROM Relationship__c WHERE Relationship_Type__c =: relationshipType AND Object_Contact__r.Name LIKE  :searchString AND Status__c IN:statusValid]){
          Contacts thiscontact = new Contacts();
          thiscontact.Name     = thisRel.Object_Contact__r.Name;
          thiscontact.Id       = thisRel.Object_Contact__c;
          thisContactList.add(thiscontact);
        }
        system.debug('========thisContactList==========='+thisContactList);
       return thisContactList;
      
      // return [SELECT Id ,Name FROM Contact WHERE Name LIKE  :searchString AND ID IN : thisId ORDER BY Name ASC  LIMIT 8];
   }
    
    public class Contacts{
        @AuraEnabled
        public string Name {get;set;}
        @AuraEnabled
        public string Id {get;set;}
    }
    
    @AuraEnabled
    public static Attachment SaveFile(Id parentId, String fileName, String base64Data, String contentType) {
        base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');
        Attachment attach = new Attachment();
        attach.parentId = parentId;
        attach.Body = EncodingUtil.base64Decode(base64Data);
        attach.Name = fileName;
        attach.ContentType = contentType;
        return attach;
    }

  
    public class OuterWrapperClass {
        @AuraEnabled
        public List < wrapperClass > thisRelationShipList {
            get;
            set;
        }
        
        public outerWrapperClass() {

        }
    }

    public class wrapperClass {
        @AuraEnabled
        public Relationship__c thisRelationShip {
            get;
            set;
        }
        @AuraEnabled
        public string mobile {
            get;
            set;
        }
        @AuraEnabled
        public string email {
            get;
            set;
        }
        @AuraEnabled
        public string exp {
            get;
            set;
        }
        @AuraEnabled
        public string desg {
            get;
            set;
        }
        @AuraEnabled
        public boolean isEnabled {
            get;
            set;
        }


        Public wrapperClass(Relationship__c thisRelationShip) {
            this.thisRelationShip = thisRelationShip;
            isEnabled = false;
            this.mobile = thisRelationShip.Individual_Phone__c;
            this.email = thisRelationShip.Individual_Email__c;
            this.desg = thisRelationShip.Individual_Job_Title__c;
            this.exp = '';
            
        }
    }

}