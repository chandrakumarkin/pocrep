/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestRelationshipCreationTrg {
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        list<Relationship_Codes__c> lstCS = new list<Relationship_Codes__c>();
        lstCS.add(new Relationship_Codes__c(Name='Has Manager',Code__c='1234'));
        lstCS.add(new Relationship_Codes__c(Name='Has Director',Code__c='1235'));
    
        insert lstCS;
        
        Block_Trigger__c objBT = new Block_Trigger__c();
        objBT.Name = 'Block';
        objBT.Block_Relationship_Trg__c = false;
        insert objBT;
        
        Country_Codes__c objCountryCodes = new Country_Codes__c();
        objCountryCodes.Code__c = 'IN';
        objCountryCodes.Name = 'India';
        objCountryCodes.Nationality__c = 'India';
        insert objCountryCodes;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        insert objEP;
        
        Education_Details__c objED = new Education_Details__c();
        objED.Name = 'B.Tech';
        objED.Code__c = '12';
        insert objED;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        
        insert objContact;
        
        Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        GsHelperCls.AlreadyFired = false;
        list<SAPCRMWebService.ZSF_IN_LOG> lstItemsTemp = new list<SAPCRMWebService.ZSF_IN_LOG>();
        SAPCRMWebService.ZSF_IN_LOG objItem =  new SAPCRMWebService.ZSF_IN_LOG();
        objItem.ACTION = 'P';
        objItem.SFREFNO = objContact.Id;
        objItem.SFGUID = objContact.Id;
        objItem.PARTNER1 = '001234';
        objItem.PARTNER2 = '001201';
        objItem.MSG = 'Partner Created Successfully';
        objItem.STATUS = '1';
        lstItemsTemp.add(objItem);
        
        TestPartnerCreationCls.lstItems = lstItemsTemp;
        
        objRel.Push_to_SAP__c = false;
        
        Test.setMock(WebServiceMock.class, new TestPartnerCreationCls());       
        system.test.startTest();
            update objRel;
        system.test.stopTest();
    }
    
    static testMethod void myUnitTest4() {
        // TO DO: implement unit test
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        list<Relationship_Codes__c> lstCS = new list<Relationship_Codes__c>();
        lstCS.add(new Relationship_Codes__c(Name='Has Manager',Code__c='1234'));
        lstCS.add(new Relationship_Codes__c(Name='Has Director',Code__c='1235'));
        insert lstCS;
        
        Block_Trigger__c objBT = new Block_Trigger__c();
        objBT.Name = 'Block';
        objBT.Block_Relationship_Trg__c = false;
        insert objBT;
        
        Country_Codes__c objCountryCodes = new Country_Codes__c();
        objCountryCodes.Code__c = 'IN';
        objCountryCodes.Name = 'India';
        objCountryCodes.Nationality__c = 'India';
        insert objCountryCodes;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        insert objEP;
        
        Education_Details__c objED = new Education_Details__c();
        objED.Name = 'B.Tech';
        objED.Code__c = '12';
        insert objED;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Account objBodyCorp = new Account();
        objBodyCorp.Name = 'Test Body Corp Customer';
        objBodyCorp.E_mail__c = 'test@difcdep.ae';
        objBodyCorp.Country__c = 'India';
        //objBodyCorp.BP_No__c = '001236';
        insert objBodyCorp;
        
        Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Account__c = objBodyCorp.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        GsHelperCls.AlreadyFired = false;
        system.test.startTest();
        
        Test.setMock(WebServiceMock.class, new TestPartnerCreationCls());
        list<SAPCRMWebService.ZSF_IN_LOG> lstItemsTemp = new list<SAPCRMWebService.ZSF_IN_LOG>();
        SAPCRMWebService.ZSF_IN_LOG objItem =  new SAPCRMWebService.ZSF_IN_LOG();
        objItem.ACTION = 'P';
        objItem.SFREFNO = string.valueOf(objRel.Id).substring(0,15);
        objItem.SFGUID = string.valueOf(objRel.Id).substring(0,15);
        objItem.PARTNER1 = '001234';
        objItem.PARTNER2 = '001239';
        objItem.MSG = 'Partner Created Successfully RC';
        //objItem.STATUS = 'S';
        lstItemsTemp.add(objItem);
        TestPartnerCreationCls.lstItems = lstItemsTemp;

        objRel.Push_to_SAP__c = false;
        update objRel;       
        system.test.stopTest();
    }
    
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        list<Relationship_Codes__c> lstCS = new list<Relationship_Codes__c>();
        lstCS.add(new Relationship_Codes__c(Name='Has Manager',Code__c='1234'));
        lstCS.add(new Relationship_Codes__c(Name='Has Director',Code__c='1235'));
        insert lstCS;
        
        Block_Trigger__c objBT = new Block_Trigger__c();
        objBT.Name = 'Block';
        objBT.Block_Relationship_Trg__c = false;
        insert objBT;
        
        Country_Codes__c objCountryCodes = new Country_Codes__c();
        objCountryCodes.Code__c = 'IN';
        objCountryCodes.Name = 'India';
        objCountryCodes.Nationality__c = 'India';
        insert objCountryCodes;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        insert objEP;
        
        Education_Details__c objED = new Education_Details__c();
        objED.Name = 'B.Tech';
        objED.Code__c = '12';
        insert objED;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Account objBodyCorp = new Account();
        objBodyCorp.Name = 'Test Body Corp Customer';
        objBodyCorp.E_mail__c = 'test@difcdep.ae';
        objBodyCorp.Country__c = 'India';
        objBodyCorp.BP_No__c = '001236';
        insert objBodyCorp;
        
        Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Account__c = objBodyCorp.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        GsHelperCls.AlreadyFired = false;
        system.test.startTest();
        
        Test.setMock(WebServiceMock.class, new TestPartnerCreationCls());
        list<SAPCRMWebService.ZSF_IN_LOG> lstItemsTemp = new list<SAPCRMWebService.ZSF_IN_LOG>();
        SAPCRMWebService.ZSF_IN_LOG objItem =  new SAPCRMWebService.ZSF_IN_LOG();
        objItem.ACTION = 'P';
        objItem.SFREFNO = string.valueOf(objRel.Id).substring(0,15);
        objItem.SFGUID = string.valueOf(objRel.Id).substring(0,15);
        objItem.PARTNER1 = '001234';
        objItem.PARTNER2 = '001236';
        objItem.MSG = 'Partner Created Successfully RC';
        objItem.STATUS = 'S';
        lstItemsTemp.add(objItem);
        TestPartnerCreationCls.lstItems = lstItemsTemp;

        objRel.Push_to_SAP__c = false;
        update objRel;       
        system.test.stopTest();
    }
    static testMethod void myUnitTest2() {
        // TO DO: implement unit test
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        list<Relationship_Codes__c> lstCS = new list<Relationship_Codes__c>();
        lstCS.add(new Relationship_Codes__c(Name='Has Manager',Code__c='1234'));
        lstCS.add(new Relationship_Codes__c(Name='Has Partner',Code__c='1235'));
        insert lstCS;
        
        Block_Trigger__c objBT = new Block_Trigger__c();
        objBT.Name = 'Block';
        objBT.Block_Relationship_Trg__c = false;
        insert objBT;
        
        Country_Codes__c objCountryCodes = new Country_Codes__c();
        objCountryCodes.Code__c = 'IN';
        objCountryCodes.Name = 'India';
        objCountryCodes.Nationality__c = 'India';
        insert objCountryCodes;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        insert objEP;
        
        Education_Details__c objED = new Education_Details__c();
        objED.Name = 'B.Tech';
        objED.Code__c = '12';
        insert objED;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Account objBodyCorp = new Account();
        objBodyCorp.Name = 'Test Body Corp Customer';
        objBodyCorp.E_mail__c = 'test@difcdep.ae';
        objBodyCorp.Country__c = 'India';
        objBodyCorp.BP_No__c = '001237';
        insert objBodyCorp;
        
        Account_Share_Detail__c objASD = new Account_Share_Detail__c();
        objASD.Account__c = objAccount.Id;
        objASD.No_of_shares_per_class__c = 1000;
        objASD.Nominal_Value__c = 1000;
        objASD.Start_Date__c = system.today();
        objASD.Status__c = 'Active';
        insert objASD;
        
        Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Account__c = objBodyCorp.Id;
        objRel.Relationship_Type__c = 'Has Partner'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        
        Shareholder_Detail__c objSD = new Shareholder_Detail__c();
        objSD.Relationship__c = objRel.Id;
        objSD.Account__c = objAccount.Id;
        objSD.Account_Share__c = objASD.Id;
        objSD.No_of_Shares__c = 13;
        insert objSD;
        GsHelperCls.AlreadyFired = false;
        system.test.startTest();
        
        Test.setMock(WebServiceMock.class, new TestPartnerCreationCls());
        list<SAPCRMWebService.ZSF_IN_LOG> lstItemsTemp = new list<SAPCRMWebService.ZSF_IN_LOG>();
        SAPCRMWebService.ZSF_IN_LOG objItem =  new SAPCRMWebService.ZSF_IN_LOG();
        objItem.ACTION = 'P';
        objItem.SFREFNO = string.valueOf(objRel.Id).substring(0,15);
        objItem.SFGUID = string.valueOf(objRel.Id).substring(0,15);
        objItem.PARTNER1 = '001234';
        objItem.PARTNER2 = '001237';
        objItem.MSG = 'Partner Created Successfully RC 2';
        objItem.STATUS = 'S';
        lstItemsTemp.add(objItem);
        TestPartnerCreationCls.lstItems = lstItemsTemp;

        objRel.Push_to_SAP__c = false;
        update objRel;        
        system.test.stopTest();
    }
    static testMethod void myUnitTest3() {
        // TO DO: implement unit test
        User objLoggedinUser = new User(id=Userinfo.getUserId());
        objLoggedinUser.SAP_User_Id__c = 'SFIUSER';
        //update objLoggedinUser;
        
        list<Relationship_Codes__c> lstCS = new list<Relationship_Codes__c>();
        lstCS.add(new Relationship_Codes__c(Name='Has Manager',Code__c='1234'));
        lstCS.add(new Relationship_Codes__c(Name='Is Shareholder Of',Code__c='1235'));
        insert lstCS;
        
        Block_Trigger__c objBT = new Block_Trigger__c();
        objBT.Name = 'Block';
        objBT.Block_Relationship_Trg__c = false;
        insert objBT;
        
        Country_Codes__c objCountryCodes = new Country_Codes__c();
        objCountryCodes.Code__c = 'IN';
        objCountryCodes.Name = 'India';
        objCountryCodes.Nationality__c = 'India';
        insert objCountryCodes;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        insert objEP;
        
        Education_Details__c objED = new Education_Details__c();
        objED.Name = 'B.Tech';
        objED.Code__c = '12';
        insert objED;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Account objBodyCorp = new Account();
        objBodyCorp.Name = 'Test Body Corp Customer';
        objBodyCorp.E_mail__c = 'test@difcdep.ae';
        objBodyCorp.Country__c = 'India';
        objBodyCorp.BP_No__c = '001238';
        insert objBodyCorp;
        
        Account_Share_Detail__c objASD = new Account_Share_Detail__c();
        objASD.Account__c = objAccount.Id;
        objASD.No_of_shares_per_class__c = 1000;
        objASD.Nominal_Value__c = 1000;
        objASD.Start_Date__c = system.today();
        objASD.Status__c = 'Active';
        insert objASD;
        
        Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Account__c = objBodyCorp.Id;
        objRel.Relationship_Type__c = 'Is Shareholder Of';
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        
        Shareholder_Detail__c objSD = new Shareholder_Detail__c();
        objSD.Relationship__c = objRel.Id;
        objSD.Account__c = objAccount.Id;
        objSD.Account_Share__c = objASD.Id;
        objSD.No_of_Shares__c = 13;
        insert objSD;
        GsHelperCls.AlreadyFired = false;
        system.test.startTest();
        
        Test.setMock(WebServiceMock.class, new TestPartnerCreationCls());
        list<SAPCRMWebService.ZSF_IN_LOG> lstItemsTemp = new list<SAPCRMWebService.ZSF_IN_LOG>();
        SAPCRMWebService.ZSF_IN_LOG objItem =  new SAPCRMWebService.ZSF_IN_LOG();
        objItem.ACTION = 'P';
        objItem.SFREFNO = string.valueOf(objSD.Id).substring(0,15);
        objItem.SFGUID = string.valueOf(objSD.Id).substring(0,15);
        objItem.PARTNER1 = '001234';
        objItem.PARTNER2 = '001238';
        objItem.MSG = 'Partner Created Successfully RC 3';
        objItem.STATUS = 'S';
        lstItemsTemp.add(objItem);
        TestPartnerCreationCls.lstItems = lstItemsTemp;

        objRel.Push_to_SAP__c = false;
        update objRel;      
        system.test.stopTest();  
    }
    static testMethod void myUnitTest5() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Account objBodyCorp = new Account();
        objBodyCorp.Name = 'Test Body Corp Customer';
        objBodyCorp.E_mail__c = 'test@difcdep.ae';
        objBodyCorp.Country__c = 'India';
        objBodyCorp.BP_No__c = '001238';
        insert objBodyCorp;
        
        Contact objContact = new Contact();
        objContact.FirstName = 'test';
        objContact.LastName = 'lastname';
        objContact.Email = 'test@email.com';
        objContact.AccountId = objAccount.Id;
        objContact.RecordTypeId = [select Id from RecordType where DeveloperName='GS_Contact' AND IsActive=true].Id;
        insert objContact;
        
        list<Document_Details__c> lstDocs = new list<Document_Details__c>();
        Document_Details__c objDoc = new Document_Details__c();
        objDoc.Contact__c = objContact.Id;
        objDoc.Document_Number__c = '123456';
        objDoc.Document_Type__c = 'Employment Visa';
        lstDocs.add(objDoc);
        objDoc = new Document_Details__c();
        objDoc.Contact__c = objContact.Id;
        objDoc.Document_Number__c = '123456';
        objDoc.Document_Type__c = 'Seconded Access Card';
        lstDocs.add(objDoc);
        objDoc = new Document_Details__c();
        objDoc.Contact__c = objContact.Id;
        objDoc.Document_Number__c = '123456';
        objDoc.Document_Type__c = 'Local / GCC Smart Card';
        lstDocs.add(objDoc);
        objDoc = new Document_Details__c();
        objDoc.Contact__c = objContact.Id;
        objDoc.Document_Number__c = '123456';
        objDoc.Document_Type__c = 'Temporary Work Permit';
        lstDocs.add(objDoc);
        insert lstDocs;
        
        list<Relationship__c> lstRels = new list<Relationship__c>();
        Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has DIFC Sponsored Employee';
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Active__c = true;
        objRel.Relationship_Group__c = 'GS';
        lstRels.add( objRel );
        objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has DIFC Seconded';
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'GS';
        objRel.Active__c = true;
        lstRels.add( objRel );
        objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has DIFC Local /GCC Employees';
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Active__c = true;
        objRel.Relationship_Group__c = 'GS';
        lstRels.add( objRel );
        objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Temporary Employee';
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Active__c = true;
        objRel.Relationship_Group__c = 'GS';
        lstRels.add( objRel );
        
        objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Family Has the Member';
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Active__c = true;
        objRel.Relationship_Group__c = 'GS';
        lstRels.add( objRel );
        
        objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objBodyCorp.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has DIFC Sponsored Employee';
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Active__c = true;
        objRel.Relationship_Group__c = 'GS';
        lstRels.add( objRel );
        
        objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objBodyCorp.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Principal User';
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Active__c = true;
        objRel.Relationship_Group__c = 'GS';
        lstRels.add( objRel );
        
        objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objBodyCorp.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Principal User';
        objRel.End_Date__c = system.today().addYears(1);
        objRel.Push_to_SAP__c = true;
        objRel.Active__c = true;
        objRel.Relationship_Group__c = 'RORP';
        lstRels.add( objRel );
        
        insert lstRels;
        
        lstRels[0].Active__c = false;
        lstRels[4].Active__c = false;
        lstRels[7].Push_to_SAP__c = false;
        update lstRels;
    }
    static testMethod void myUnitTestNew() {
        list<Relationship_Codes__c> lstCS = new list<Relationship_Codes__c>();
        lstCS.add(new Relationship_Codes__c(Name='Has Manager',Code__c='1234'));
        lstCS.add(new Relationship_Codes__c(Name='Has Director',Code__c='1235'));
        insert lstCS;
        
        Block_Trigger__c objBT = new Block_Trigger__c();
        objBT.Name = 'Block';
        objBT.Block_Relationship_Trg__c = false;
        insert objBT;
        
        Country_Codes__c objCountryCodes = new Country_Codes__c();
        objCountryCodes.Code__c = 'IN';
        objCountryCodes.Name = 'India';
        objCountryCodes.Nationality__c = 'India';
        insert objCountryCodes;
        
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'CRM';
        objEP.URL__c = 'http://crmdev.com/sampledata';
        insert objEP;
        
        Education_Details__c objED = new Education_Details__c();
        objED.Name = 'B.Tech';
        objED.Code__c = '12';
        insert objED;
        
        WebService_Details__c objWS = new WebService_Details__c();
        objWS.Name = 'Credentials';
        objWS.SAP_User_Id__c = 'testuserid';
        objWS.Username__c = 'testuserId';
        objWS.Password__c = '123456789';
        insert objWS;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        
        insert objContact;
        
        Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = objAccount.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Sys_Test__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        GsHelperCls.AlreadyFired = false;
        test.startTest();
            database.executeBatch(new GSPortalUserRelBatchCls(), 1);
        test.stopTest();
        
    }
}