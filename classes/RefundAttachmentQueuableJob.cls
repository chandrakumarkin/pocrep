/******************************************************************************************
 *  Author   : Shoaib Tariq
 *  Company  : 
 *  Date     : 04/09/2020
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    04/09/2020   shoaib        Created
**********************************************************************************************************************/
public class RefundAttachmentQueuableJob implements Queueable,Database.AllowsCallouts{

    Public Step__c stp ; 
       
    public RefundAttachmentQueuableJob(Step__c stp){
        This.stp           = stp;
    }

    public void execute(QueueableContext context) { 
        Service_Request__c srObj = [select id,Name,Type_of_refund__c from service_request__c where id=:stp.SR__c];
        String response          = PushAttachmentToSAP(srObj);
    }

    public static String PushAttachmentToSAP(Service_Request__c srObj) {
        String response = '';

        string decryptedSAPPassWrd = test.isrunningTest()  ==   false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) : '123456';
       
        sapComDocumentSapSoapFunctionsMcS.ZSF_REF_ATTACHMENT  refundResponse;
      
        List <string> attachmentIDs                =   new List < string > ();

        for (sr_Doc__c srd: [select id, Doc_ID__c from sr_doc__c where Service_Request__c =: srObj.Id]) {
            attachmentIDs.add(srd.Doc_ID__c);
        }
        
        
            refundResponse                      = new sapComDocumentSapSoapFunctionsMcS.ZSF_REF_ATTACHMENT();
            refundResponse.timeout_x            = 120000;
            refundResponse.clientCertName_x     = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
            refundResponse.inputHttpHeaders_x   = new Map < String, String > ();
                    
            Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c + ':' +decryptedSAPPassWrd);
            String authorizationHeader         = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                    
            refundResponse.inputHttpHeaders_x.put('Authorization', authorizationHeader);
            
                if (attachmentIDs != null && attachmentIDs.size() > 0) {
                for (attachment att: [select id, body, Name, contentType from attachment where id in: attachmentIDs]) {
                        system.debug(srObj+''+ att.Name+''+ att.contentType+''+ EncodingUtil.Base64Encode(att.body));
                        sapComDocumentSapSoapFunctionsMcS.ZattResponse  item         = refundResponse.ZsfRefAttachment(att.contentType,att.Name,srObj.Name,EncodingUtil.Base64Encode(att.body));
                        response = string.valueof(item);
                }
            }
        
        return response;
     }
}