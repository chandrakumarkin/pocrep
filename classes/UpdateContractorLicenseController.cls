/******************************************************************************************
 *  Version 	 Author       		 Date 				Description
 * -----------------------------------------------------------------------------------------
 *  V1.1       Vinod Jetti        05-07-2021			Initial Creation : DIFC2-14668 - Controller to create the Update Contractor License SR.
 *  
*********************************************************************************************/
public class UpdateContractorLicenseController {
    
    public Service_Request__c srObj {get; set;}
    public String selectedAuthority { get;set;}
    public String selectedCompanyType { get;set;}    
    
    public UpdateContractorLicenseController() {
        srObj = new Service_Request__c();
        User u = [SELECT Id, Contact.AccountId, Contact.Account.Name FROM User WHERE Id =:UserInfo.getUserId()];
        if(u.Contact.AccountId !=null)
            srObj.Customer__c=u.Contact.AccountId;
        
    }
    
    public pagereference Save(){
        if(selectedCompanyType ==null || selectedCompanyType == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please Select Company Type'));
            return null;
        }
        srObj.RecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('Update_License_Details').getRecordTypeId();
        SR_Status__c status = [Select Id,name from SR_Status__c where name = 'Draft'];
        SR_Template__c template = [Select Id,SR_RecordType_API_Name__c,SR_Description__c from SR_Template__c where SR_RecordType_API_Name__c = 'Update_License_Details' LIMIT 1];       
        srObj.External_SR_Status__c = status.Id;
        srObj.Internal_SR_Status__c = status.Id;
        srObj.SR_Template__c = template.Id;
        srObj.Submitted_DateTime__c = DateTime.now();
        srObj.Submitted_Date__c = Date.today();
        srObj.Name_of_the_Authority__c = selectedAuthority;
        srObj.Company_Type_list__c = selectedCompanyType;
        try{
            insert srObj;
            string retURl = system.label.ForceSite_Domain;
            PageReference pageRef = new PageReference(retURl+'/customers/'+srObj.Id);
            return pageRef;
        }catch(DMLException e){
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
            ApexPages.addMessage(msg);
            return null;
        }
    }
    
    public pagereference Cancel(){
        string retURl = system.label.ForceSite_Domain;
        PageReference pageRef = new PageReference(retURl+'/clientportal/s/?tab=services');
        return pageRef;
    }
    
    public List<SelectOption> getCompanyTypeList(){
            List<Selectoption> typeList = new List<selectoption>();
            typeList.add(new selectOption('', '--None--'));
            typeList.add(new selectoption('Fitout Contractor', 'Fitout Contractor'));
            typeList.add(new selectoption('Event Organizer', 'Event Organizer'));
            return typeList; 
    }
    public List<SelectOption> getIssuingAuthority(){
            List<Selectoption> typeList = new List<selectoption>();
            typeList.add(new selectOption('', '--None--'));
            typeList.add(new selectoption('Government of Dubai', 'Government of Dubai'));
            typeList.add(new selectoption('Other', 'Other'));
            return typeList; 
    }
}