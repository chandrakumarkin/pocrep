public without sharing class TrainingInvitationController {
    public string trainingRequestId;
    public boolean isThank {get;set;}
    public boolean isSubmitted {get;set;}
    public string status {get;set;}
    public Training_Request__c trainingRequest {get;set;}
    public string warningMessage {get;set;}
    public boolean isExpiredTraining {get;set;}
    public TrainingInvitationController(){
        isExpiredTraining = false;
        trainingRequestId = ApexPages.currentPage().getParameters().get('id');
        isThank = false;
        isSubmitted = false;
        trainingRequest = new Training_Request__c();
        for(Training_Request__c tr : [select id,Status__c,TrainingMaster__r.Status__c,Category__c, 
                                      Location__c,TrainingMaster__r.Start_DateTime__c,TrainingMaster__r.To_DateTime__c from Training_Request__c 
                                      where id = :trainingRequestId ]){
            if(tr.TrainingMaster__r.Start_DateTime__c != null)
            	tr.TrainingMaster__r.Start_DateTime__c = tr.TrainingMaster__r.Start_DateTime__c.addHours(4);
            if(tr.TrainingMaster__r.To_DateTime__c != null)
            	tr.TrainingMaster__r.To_DateTime__c = tr.TrainingMaster__r.To_DateTime__c.addHours(4);
            trainingRequest = tr;
            if(tr.TrainingMaster__r.Status__c != 'Approved by HOD')
            	isExpiredTraining = true;
            if(isExpiredTraining == false && (tr.Status__c == 'Accepted' || tr.Status__c == 'Rejected'))
                isSubmitted = true;
        }
        if(isExpiredTraining)
            warningMessage = 'The Training is expired.';
        if(isSubmitted)
            warningMessage = 'Your response is already captured.';
    }
    public pageReference confirmAction(){
        try{
            if(trainingRequest != null && trainingRequest.id != null && status != null){
                trainingRequest.Status__c = status;
                update trainingRequest;
                isThank = true;
            }
        }
        catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, ex.getMessage()));
        }
        return null;
    }
}