/******************************************************************************************
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Description : Test Class for CC_cls_FitOutandEventCustomCode
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    06-06-2016  Claude        Created
****************************************************************************************************************/
@isTest(SeeAllData=false)
private class Test_cls_FitOutandEventCustomCode {
    
    /**
     * Initializes the test data for the entire test class
     * @author      Claude Manahan
     * @date        6-6-2016
     */
    @testSetup static void setTestData(){
        
        Test_FitoutServiceRequestUtils.createTestSecurityDetails();
        
        Account objAccount = Test_CC_FitOutCustomCode_DataFactory.getTestAccount();
        
        insert objAccount;
        
        Contact objContact = Test_CC_FitOutCustomCode_DataFactory.getTestContact(objAccount.Id);
        
        insert objContact;
        
        System.runAs(new User(Id = Userinfo.getUserId(), ContactId = objContact.Id)){
            
            Account objContractor = Test_CC_FitOutCustomCode_DataFactory.getTestContractor();
            Account objContractor2 = Test_CC_FitOutCustomCode_DataFactory.getTestContractor();
            
            objContractor2.Name = 'Test Empty Violations COntractor';
            objContractor2.Bp_No__c = 'BP12345678';
            
            List<Lookup__c> testLookups = Test_CC_FitOutCustomCode_DataFactory.getTestLookupRecords();
            List<Status__c> testStepStatus = Test_CC_FitOutCustomCode_DataFactory.getTestStepStatuses();
            List<SR_Status__c> testSRStatus = Test_CC_FitOutCustomCode_DataFactory.getTestSrStatuses();
            List<CountryCodes__c> countryCodesTestList = Test_CC_FitOutCustomCode_DataFactory.getTestCountryCodes();
            List<Step_Template__c> testStepTemplates = Test_CC_FitOutCustomCode_DataFactory.getTestStepTemplate();
            
            insert new List<Account>{objContractor,objContractor2};
            insert countryCodesTestList;
            insert testSRStatus;
            insert testStepStatus;
            insert testLookups;
            insert testStepTemplates;
            //insert testBuilding;
            
            Lease__c testLease = Test_CC_FitOutCustomCode_DataFactory.getTestLease(objAccount.Id);
            
            //insert testUnits;
            insert testLease;
            
        }
        
        Test_FitoutServiceRequestUtils.prepareSetup();
    }
    
    private static void testBypass(){
        
        Building__c testBuilding = Test_CC_FitOutCustomCode_DataFactory.getTestBuilding();
        insert testBuilding;
        insert Test_CC_FitOutCustomCode_DataFactory.getTestUnits(testBuilding.Id);  
    }
    
    /*static TestMethod void testBlackListJob(){
        
        testBypass();
        
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.runContractorAccess = false;
        Test_FitoutServiceRequestUtils.testBlackListJob();
    }
    
    static TestMethod void testCalculateContractorBlackPoints(){
        
        testBypass();
        
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.runContractorAccess = false;
        Test_FitoutServiceRequestUtils.testCalculateContractorBlackPoints();
    }*/
    
    static TestMethod void testCreateUserForContractor(){
        
        testBypass();
        
        Test_FitoutServiceRequestUtils.useDefaultUser();
        System.runAs (new User(Id=UserInfo.getUserId())){
            Test_FitoutServiceRequestUtils.testCreateUserForContractor();
        }
    }
    
    static TestMethod void testDeductFine(){
        
        testBypass();
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.runContractorAccess = false;
        Test_FitoutServiceRequestUtils.testDeductFine();
    }
    static TestMethod void testGenerateCertificateForFo(){
        
        testBypass();
        System.runAs(new User(Id=UserInfo.getUserId())){
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.runContractorAccess = false;
        
            Test_FitoutServiceRequestUtils.testGenerateCertificateForFo();
        }
    }
    
    static TestMethod void testGenerateCertificateForFoTwo(){
        
        testBypass();
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.runContractorAccess = false;
        Test_FitoutServiceRequestUtils.testGenerateCertificateForFoTwo();
    }
    
    static TestMethod void testCheckReUploadDocs(){
        
        testBypass();
        Test.startTest();
        Test_FitoutServiceRequestUtils.testCheckReUploadDocs();
        Test.stopTest();
    }
    
    static TestMethod void testCreateUploadRevisedDocs(){
        
        testBypass();
        Test.startTest();
        Test_FitoutServiceRequestUtils.testCreateUploadRevisedDocs();
        Test.stopTest();
    }
    
    //Methods that failed
    
    static TestMethod void testRequestForContractorPass(){
        
        testBypass();
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.testRequestForContractorPass();
    }
    
    static TestMethod void testUpdateClientDetails(){
        testBypass();
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.testUpdateClientDetails();
    }
    
    static TestMethod void testUpdateEstimatedHours(){
        testBypass();
        Test_FitoutServiceRequestUtils.useDefaultUser();
       // Test_FitoutServiceRequestUtils.testUpdateEstimatedHours();
    }
    
    static TestMethod void testGenerateNOCDocs(){
        testBypass(); 
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.testGenerateNOCDocs();
    }
    
    static TestMethod void testUpdateContactDetails(){
        testBypass();
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.testUpdateContactDetails();
    }
    
    static TestMethod void testRevisionOfFORequest(){
        testBypass();
        Test_FitoutServiceRequestUtils.useDefaultUser();
        Test_FitoutServiceRequestUtils.runContractorAccess = false;
        Test_FitoutServiceRequestUtils.testRevisionOfFORequest();
    }
     
    static TestMethod void RunDummyMethod(){
        CC_cls_FitOutandEventCustomCode.testCoverage();
    }
     
}