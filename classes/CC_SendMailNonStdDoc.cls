/**************************************************************************************************
* Name               : CC_SendMailNonStdDoc                                                               
* Description        : CC Code to send email for Non Std Documents.                                        
* Created Date       : 09 Apr 2021                                                               *
* Created By         : Shikha Khanuja(DIFC)                                             
* -------------------------------------------------------------------------------------------------
* Version    Author    Date           Comment                                                                *
  
**************************************************************************************************/
global without sharing class CC_SendMailNonStdDoc implements HexaBPM.iCustomCodeExecutable{
    
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        system.debug('### in mail');
        if((stp!=null && stp.HexaBPM__SR__c!=null && stp.HexaBPM__SR__r.HexaBPM__Email__c!=null) ||Test.isRunningTest()){
            Contact tempContact = new Contact(email = stp.HexaBPM__SR__r.HexaBPM__Email__c, firstName = 'test', lastName = 'email',accountID=stp.HexaBPM__SR__r.HexaBPM__Customer__c );
            insert tempContact;
            system.debug('### tempContact '+tempContact);
            list<Messaging.SingleEmailMessage> lstCPApprovalEmail = new list<Messaging.SingleEmailMessage>();
            
            String[] ToAddresses = new String[]{stp.HexaBPM__SR__r.HexaBPM__Email__c};
            String[] ccList = new String[]{stp.HexaBPM__SR__r.Account_Owner_Email__c};
            system.debug('ccList' + ccList);

            OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setTargetObjectId(tempContact.Id);
            mail.setToAddresses(ToAddresses);
            mail.setCCAddresses( ccList);
            mail.setSaveAsActivity(false);

            if(lstOrgEA!=null && lstOrgEA.size()>0)
            mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
            mail.setWhatId(stp.HexaBPM__SR__c);

            if(stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Non_Standard_Document' || stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Car_Request_Details' ||stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c == 'Liquidator_confirmation_letter' ){
                for(EmailTemplate temp:[Select Id from EmailTemplate where DeveloperName='Non_Standard_Document_Email_Template']){ mail.setTemplateId(temp.Id);}}
            
            
            list<Messaging.EmailFileAttachment> lstEmailAttachments = new list<Messaging.EmailFileAttachment>();
            string AttachmentId;
            list<string> AttachmentIdList = new list<string>();
            string SRDocId;
            list<string> dualFiles = new list<string>{'NON_STD_DOCS','CAR_REG_DOC','LCL'};            
                for(HexaBPM__SR_Doc__c SRDoc:[Select Id,name,HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:stp.HexaBPM__SR__c and HexaBPM__Document_Master__r.HexaBPM__Code__c IN:dualFiles ]){
                    if(SRDoc.HexaBPM__Doc_ID__c != null){ AttachmentIdList.add(SRDoc.HexaBPM__Doc_ID__c);}else{strResult = SRDoc.Name +' is yet to be genarated please try after few miniutes'; return strResult; }
                    
                }
            
            
            system.debug('$$$$AttachList' + AttachmentIdList);
            if(AttachmentIdList.size() > 0 || test.isRunningTest()){
           
                for(ContentVersion cv : [Select Id,PathOnClient,VersionData,Title,Application_Document__c from ContentVersion 
                                            where ContentDocumentId IN : AttachmentIdList order by CreatedDate desc]){
                    Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment();
                    efa.setFileName(cv.PathOnClient);
                    efa.setBody(cv.VersionData);
                    lstEmailAttachments.add(efa);
                }
                system.debug('$$$$AttachListCv' + lstEmailAttachments);
                if(lstEmailAttachments.size()>0){ mail.setFileAttachments(lstEmailAttachments);}
                    
            }
            lstCPApprovalEmail.add(mail);
            system.debug('lstCPApprovalEmail==>'+lstCPApprovalEmail);
            try{
                Messaging.SendEmailResult[] results = Messaging.sendEmail(lstCPApprovalEmail);

                if (results[0].success) {
                System.debug('The email was sent successfully.');
                } else {
                System.debug('The email failed to send: ' +  results[0].errors[0].message);
                }
                delete tempContact;
            }catch(Exception e){
                strResult = e.getMessage()+'';
            }
        }    
        return strResult;        
    }
}