global with sharing class Scheduled_PSA_Termination implements Database.Batchable<sObject>,Schedulable
{
    /*
    #8208 - Create BP records if existing web service fail to puch data 
    * Delveloper Arun singh 
    test class : Scheduled_failed_BP_BatchTest 
        */
        
    global void execute(SchedulableContext ctx) 
    {
        
        Database.executeBatch(new Scheduled_PSA_Termination (), 5);
    
    }


    global Database.QueryLocator start(Database.BatchableContext bc) 
    {
        List<Id> IsssuedPSAtem=new List<Id> ();
        string RecordTypeId=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('PSA_Termination').getRecordTypeId();

        for(Service_Request__c ObjSR:[select id,Customer__c,recordtype.developername from Service_Request__c where recordtypeid=:RecordTypeId and createddate=this_Year])
        {
            IsssuedPSAtem.add(ObjSR.Customer__c);
        }

    string query='';
      if(!Test.isRunningTest()){
        query = 'select id,Name,ROC_Status__c,Next_Renewal_Date__c,DFSA_Withdrawn_Date__c,Pending_Dissolution_Date__c,Pending_for_Renewal__c from account where Index_Card_Status__c=\'Expired\' and Registration_License_No__c!=null and (Next_Renewal_Date__c< LAST_90_DAYS OR DFSA_Withdrawn_Date__c<LAST_90_DAYS OR Pending_Dissolution_Date__c<LAST_90_DAYS OR ROC_Status__c=\'Inactive - Struck Off\') and ROC_Status__c!=\'Dissolved\' and id not in :IsssuedPSAtem ';
      }else{
        query = 'select id,Name,ROC_Status__c,Next_Renewal_Date__c,DFSA_Withdrawn_Date__c,Pending_Dissolution_Date__c,Pending_for_Renewal__c from account  limit 1';
      }        
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List <Account> scope) 
    {
     string RecordTypeId=Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('PSA_Termination').getRecordTypeId();
        List<Service_Request__c> ListPSATer=new List<Service_Request__c >();
        Id cancelledStatusId = [Select id from SR_Status__c where name ='Draft' limit 1].id;
    
        for(Account ObjAccount:scope)
        {
                System.debug('ObjAccount===>'+ObjAccount);
                Service_Request__c SRData=new Service_Request__c();
                SRData.RecordTypeId=RecordTypeId;
                SRData.Internal_SR_Status__c=cancelledStatusId;
                SRData.External_SR_Status__c=cancelledStatusId;
                SRData.Customer__c=ObjAccount.id;
                SRData.Would_you_like_to_opt_our_free_couri__c ='No';
                ListPSATer.add(SRData);
    
            
        }
        
        if(!ListPSATer.isEmpty())
        insert ListPSATer;

                        
           
      
    
    }
    global void finish(Database.BatchableContext BC) {
        
        
    
    }
}