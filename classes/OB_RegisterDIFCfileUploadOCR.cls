/**
*Author : Merul Shah
*Description : This is used to upload docs on configurabe screen (OCR feature).
**/
public without sharing  class OB_RegisterDIFCfileUploadOCR
{

    @AuraEnabled
    public static ResponseWrapper viewSRDocs(string reqWrapPram)
    {
        
         ResponseWrapper respWrap = new ResponseWrapper();
         RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
        
         string srId = reqWrap.srId;
         string docMasterCode = reqWrap.docMasterCode;
         
         Map<String,String> docMasterContentDocMap = reqWrap.docMasterContentDocMap;
         
        //Preparing the SRDocs for the amendment
        map<string,String> mapSRDocs = new map<string,String>();
        
        for(HexaBPM__SR_Doc__c srDoc : [SELECT id,File_Name__c,HexaBPM__Document_Master__r.HexaBPM__Code__c 
                                              FROM HexaBPM__SR_Doc__c
                                             WHERE HexaBPM__Service_Request__c = :srId 
                                              AND HexaBPM_Amendment__c = NULL 
                                              AND HexaBPM__Document_Master__r.HexaBPM__Code__c =:docMasterCode
                                            LIMIT 1
                                           ]
               )
        {
            
            mapSRDocs.put(srDoc.HexaBPM__Document_Master__r.HexaBPM__Code__c,srDoc.File_Name__c); 
            
        }
        // if not found on SRdocs then go for Content document
        string fileName;
        if(mapSRDocs.size() == 0 )
        {
            String ContentDocumentId = docMasterContentDocMap.get(docMasterCode);
            if( String.isNotBlank(ContentDocumentId))
            {
              for(  ContentDocument conDoc : [SELECT id,
                                                     title,FileExtension 
                                                FROM ContentDocument
                                               WHERE id=:ContentDocumentId
                                            ])
              {
                  fileName = conDoc.title;
                  mapSRDocs.put(docMasterCode,fileName);
              }

            }
            
        }

        respWrap.mapSRDocs = mapSRDocs;


        return respWrap;
    }
    
    

    @AuraEnabled  
    public static ResponseWrapper reparentToSRDocs(String reqWrapPram)
    {
        ResponseWrapper respWrap = new ResponseWrapper();
        RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
        
        String srId = reqWrap.srId; 
        String documentId = reqWrap.documentId;
        String secObjID = reqWrap.secObjID;
        
         system.debug('@@@@@@@@ secObjID '+secObjID );
        
        // getting documentMasterCode
        String documentMasterCode;  
        respWrap.secItmWrap = new SectionItemWrapper();
              
        for(HexaBPM__Section_Detail__c secDet : [SELECT id,
                                                        Document_Master_Code__c,
                                                        Parse_File__c  
                                                  FROM HexaBPM__Section_Detail__c 
                                                 WHERE id =:secObjID])
        {
             documentMasterCode = secDet.Document_Master_Code__c;
             respWrap.secItmWrap.sectionDetailObj = secDet;
        
        }
         system.debug('@@@@@@@@ documentMasterCode '+documentMasterCode  );
        
        // Getting existing SR Docs for which will be new parent.
        String srDocID; 
        if(!String.isBlank( documentMasterCode ) && !String.isBlank(srId ))
        {
            //HexaBPM__SR_Doc__c
            for(HexaBPM__SR_Doc__c srDoc : [SELECT Id,HexaBPM__Document_Master__c 
                                             FROM HexaBPM__SR_Doc__c 
                                            WHERE HexaBPM__Document_Master__c!=null 
                                              AND HexaBPM__Document_Master__r.HexaBPM__Code__c =:documentMasterCode
                                              AND HexaBPM__Service_Request__c =: srId 
                                            LIMIT 1])
            {
                    srDocID = srDoc.Id;
            }
              
        }
        
        system.debug('@@@@@@@@ srId '+srId );
        system.debug('@@@@@@@@ srDocID '+srDocID);
        system.debug('@@@@@@@@ documentId '+documentId );
        
        
        
        if( !String.isBlank(srId ) 
                    &&  !String.isBlank(srDocID) 
                                && !String.isBlank(documentId ) )
        {
            
            
            ContentDocumentLink cDe = new ContentDocumentLink();
            cDe.ContentDocumentId = documentId;
            cDe.LinkedEntityId = srDocID; // you can use objectId,GroupId etc
            cDe.ShareType = 'V'; // Viewer permission, checkout description of ContentDocumentLink object for more details
            cDe.Visibility = 'AllUsers';
            insert cDe;
            
            respWrap.successMessage = 'Success  '+cDe.Id ;
            
            
            ContentDocumentLink contDocumentLink;
            // Pulling existing link.
            for( ContentDocumentLink  contDocumentLinkTemp : [SELECT ContentDocument.title,
                                                                     LinkedEntityId,
                                                                     ShareType   
                                                                FROM ContentDocumentLink 
                                                               WHERE ContentDocumentId =: documentId  
                                                                 AND LinkedEntityId =: srId
                                                               LIMIT 1
                                                              ])
            {
                contDocumentLink = contDocumentLinkTemp;
            }   
            
            if(contDocumentLink != NULL)
            {
                
                delete contDocumentLink;
               
            } 
               
        }
        
       return respWrap;
    }
    
    
    @AuraEnabled
    public static ResponseWrapper processOCR(String fileId,String sObjectToProcess) 
    {
        //OCR Wrapper
        OB_OCRHelper.RequestWrapper ocrReqWrap = new OB_OCRHelper.RequestWrapper();
        OB_OCRHelper.ResponseWrapper ocrRespWrap  = new OB_OCRHelper.ResponseWrapper();
        
        
        //Current class wrapper.
        //RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
        ResponseWrapper respWrap = new ResponseWrapper();
        
        // preparing request.
        ocrReqWrap.fileId = fileId;
        ocrReqWrap.sObjectToProcess = sObjectToProcess;
        
        if(!test.isRunningTest())
        {
              ocrRespWrap = OB_OCRHelper.processOCR(ocrReqWrap);
        	  respWrap.mapFieldApiObject  = ocrRespWrap.mapFieldApiObject;
        }
        return respWrap;
        
    }
    
     public class SectionItemWrapper {
        @AuraEnabled public String FieldApi { get; set; }
        @AuraEnabled public Object FieldValue { get; set; }
        @AuraEnabled public Object LookupValue { get; set; }
        @AuraEnabled public String FieldType { get; set; }
        @AuraEnabled public String ObjectApi { get; set; }
        @AuraEnabled public boolean bRenderSecDetail { get; set; }
        @AuraEnabled public string sectionItemId { get; set; }
        @AuraEnabled public HexaBPM__Section_Detail__c sectionDetailObj { get; set; }
        
        
        public SectionItemWrapper()
        {
        }
    }
    
     public class RequestWrapper
    {
        @AuraEnabled public string flowId { get; set; }
        @AuraEnabled public string pageId {get;set;}
        @AuraEnabled public string srId { get; set; }
        @AuraEnabled public string docMasterCode{ get; set; }
        @AuraEnabled public Map<String,String> docMap { get; set; } //Added by Rajil
        
        @AuraEnabled public HexaBPM__Service_Request__c objRequest{get;set;}
        @AuraEnabled public Map<String,String> objRequestMap { get; set; }
        @AuraEnabled public string documentId {get;set;}
        @AuraEnabled public string secObjID{get;set;}
        @AuraEnabled public map<string,string> docMasterContentDocMap{get;set;}
        
        public RequestWrapper()
        {
            docMap = new Map<String,String>();
        }
    }
    
    
    public class ResponseWrapper
    {
      @AuraEnabled public list<OB_RegisterDIFCFlowController.PageFlowWrapper> pgFlwWrapLst {get;set;}
        @AuraEnabled public HexaBPM__Service_Request__c objRequest{get;set;}
        @AuraEnabled public String errorMessage{get;set;}
        @AuraEnabled public String successMessage{get;set;}
        @AuraEnabled public string srId{get;set;}
        @AuraEnabled public string flowId{get;set;}
        @AuraEnabled public string  pageId{get;set;}
        @AuraEnabled public SectionItemWrapper secItmWrap{get;set;}
      
        // For OCR
        @AuraEnabled public Map<String,Object> mapFieldApiObject {get;set;}
        
        //For Load SRDoc
        //@AuraEnabled public map<string,HexaBPM__SR_Doc__c> mapSRDocs  {get;set;}
        @AuraEnabled public map<string,string> mapSRDocs  {get;set;}
               
        public ResponseWrapper()
        {
           
        }
    }
}