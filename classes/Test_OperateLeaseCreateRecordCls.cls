@isTest(seealldata=false)
public class Test_OperateLeaseCreateRecordCls {
    
    static testMethod void myUnitTest() {
            
            Account objAccount = new Account();
            objAccount.Name = 'Test Custoer 354544';
            objAccount.E_mail__c = 'test@tests.com';
            objAccount.BP_No__c = '5678';
            insert objAccount;
            
            System.debug('objAccount===>'+objAccount);
        
        Lease__c lease = new Lease__c();
        lease.Name = 'Name';
        lease.Account__c = objAccount.id;
        lease.Start_Date__c = system.today();
        lease.End_Date__c = system.today() +7;
        lease.Square_Feet__c = '1234';
       
        lease.Lease_Types__c= 'CoWorking';
        lease.Type__c = 'Leased';
        insert lease;
        
        Unit__c unit = new Unit__c();
        unit.Name = 'unit';
        unit.Unit_Square_Feet__c = 1234.56;
         unit.Co_Working_Type__c ='CoWorking';
        unit.Available_for_CoWorking__c = True;
        insert unit;
        
        Tenancy__c tn = new Tenancy__c();
        tn.Unit__c = unit.id;
        tn.Start_Date__c = system.today();
        tn.End_Date__c = system.today() +7;
        tn.Lease__c = lease.Id;
        tn.SAP_Tenancy_No__c = lease.Name + unit.Name;
        tn.Square_Feet__c = unit.Unit_Square_Feet__c;
        insert tn;
        
        Lease_Partner__c lp = new Lease_Partner__c();
        lp.Name = unit.Name;
        lp.Unit__c = unit.id;
        lp.Account__c = lease.Account__r.id;
        lp.Lease__c = lease.Id;
        lp.Unique_Number__c = lease.Name + unit.Name + lease.Account__r.BP_No__c;
        lp.SAP_PARTNER__c = lease.Account__r.BP_No__c;
        lp.Square_Feet__c = lease.Square_Feet__c;
        lp.Lease_Types__c = lease.Lease_Types__c;
        lp.Start_date__c = lease.Start_date__c;
        lp.End_Date__c = lease.End_Date__c;
        lp.Type_of_Lease__c = lease.Type__c;
        //insert lp;
        
        list<id> idlist = new list<id>();
        idlist.add(lease.Id);
        
       // OperateLeaseCreateRecordCls.listLeaseIds = idlist;
        Test.startTest();
        OperateLeaseCreateRecordCls.pushRecordsfromLease(idlist);
        OperateLeaseCreateRecordCls.createRecordsfromLease(idlist);      
        Test.stopTest();
        
    }

}