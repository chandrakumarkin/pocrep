global class SearchAndReplaceUser implements Database.Batchable < sObject > {

global final String Query;


global SearchAndReplaceUser(String q) {

Query = q;
}

global Database.QueryLocator start(Database.BatchableContext BC) {
return Database.getQueryLocator(query);
}

global void execute(Database.BatchableContext BC, List < User > scope) {
for (User ObjUser: scope) {
ObjUser.email = ObjUser.email != NULl ?ObjUser.email+'.invalid' : ObjUser.email;
ObjUser.GN_Line_Manager_Email__c = ObjUser.GN_Line_Manager_Email__c != NULl ?ObjUser.GN_Line_Manager_Email__c+'.invalid' : ObjUser.GN_Line_Manager_Email__c;
ObjUser.GN_Line_Manager_Email_External__c = ObjUser.GN_Line_Manager_Email_External__c != NULl ?ObjUser.GN_Line_Manager_Email_External__c+'.invalid' : ObjUser.GN_Line_Manager_Email_External__c;

ObjUser.Extension = ObjUser.Extension != NULl ? '+971569803616': ObjUser.Extension;
ObjUser.MobilePhone = ObjUser.MobilePhone != NULl ? '+971569803616': ObjUser.MobilePhone;
ObjUser.Phone = ObjUser.Phone != NULl ? '+971569803616': ObjUser.Phone;


}
update scope;
}

global void finish(Database.BatchableContext BC) {}
}