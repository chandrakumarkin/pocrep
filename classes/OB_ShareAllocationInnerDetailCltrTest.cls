@isTest
public class OB_ShareAllocationInnerDetailCltrTest 
{
	
    public static testmethod void saveAndDeleteTest()
    {
       
        // create account
        Account newAccount = new Account();
        newAccount.Name = 'TestAccount'; 
        insert newAccount;
       
       HexaBPM__Service_Request__c newSR = new HexaBPM__Service_Request__c();
       newSR.HexaBPM__Customer__c =  newAccount.Id;
       newSR.recordtypeid = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByDeveloperName().get('In_Principle').getRecordTypeId(); 
       insert newSR;

       
       HexaBPM_Amendment__c amedTemp = new HexaBPM_Amendment__c();
       amedTemp.Role__c = 'Shareholder';
       amedTemp.ServiceRequest__c = newSR.id;
       amedTemp.Status__c = 'Active'; 
       amedTemp.recordTypeId  = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c', 'Body_Corporate');
       insert amedTemp; 
        
       Account_Share_Detail__c shareClass= new Account_Share_Detail__c();
       shareClass.Account__c = newAccount.Id;
       shareClass.No_of_shares_per_class__c = 23;
       shareClass.Nominal_Value__c =100;
       insert shareClass;
       
       Shareholder_Detail__c sd = new Shareholder_Detail__c();
       sd.Account__c= newAccount.Id;
       sd.Account_Share__c= shareClass.Id;
       sd.OB_Amendment__c= amedTemp.Id;
       sd.No_of_Shares_Allotted__c = 23;
       insert sd;
        
        
          test.startTest();
             OB_ShareAllocationInnerDetailController.RequestWrapper reqParam = new OB_ShareAllocationInnerDetailController.RequestWrapper();
             OB_ShareAllocationInnerDetailController.ResponseWrapper respParam = new OB_ShareAllocationInnerDetailController.ResponseWrapper();
            
             OB_ShareAllocationInnerDetailController.AmendmentWrapper amedWrap =  new OB_ShareAllocationInnerDetailController.AmendmentWrapper();
             OB_ShareAllocationInnerDetailController.ShareholderDetailWrapper shrDetailWrap =  new OB_ShareAllocationInnerDetailController.ShareholderDetailWrapper();
             OB_ShareAllocationInnerDetailController.AccountShareDetailWrapper accShrDtlWrap = new OB_ShareAllocationInnerDetailController.AccountShareDetailWrapper();
             accShrDtlWrap.accShrDetObj = new Account_Share_Detail__c();
             respParam.amedWrap = amedWrap; 
             respParam.shrDetailWrapLst = new List<OB_ShareAllocationInnerDetailController.ShareholderDetailWrapper>();
        	 
             shrDetailWrap.shareHolderDetObj = sd;
             shrDetailWrap.amedObj = amedTemp;
             amedWrap.amedObj = amedTemp;
             amedWrap.isIndividual = false;
             amedWrap.isBodyCorporate = true;
             amedWrap.lookupLabel = '';
             amedWrap.shrDetailWrapLst = new List<OB_ShareAllocationInnerDetailController.ShareholderDetailWrapper>();    
             respParam.accShrDetailWrapLst = new List<OB_ShareAllocationInnerDetailController.AccountShareDetailWrapper>();
             
        
             reqParam.flowId = NULL;
             reqParam.pageId = NULL;
             reqParam.srId = newSR.Id;
             reqParam.amedWrap = amedWrap;
             reqParam.amendmentID = amedTemp.Id;
             
        
             //reqParam.stepId = insertNewSteps[0].Id;
             //reqParam.stepObj = insertNewSteps[0];
             reqParam.shrDetailWrap = shrDetailWrap;
             String reqParamString = JSON.serialize(reqParam);    
             OB_ShareAllocationInnerDetailController.saveShareHolderDetails(reqParamString);
        	 OB_ShareAllocationInnerDetailController.deleteShareHolderDetails(reqParamString);
        
        	respParam.errorMsg = 'Test';
        	 
        test.stopTest();
        
    }    
    
    
    
    
}