/**
 * @description       : 
 * @author            : Shoaib Tariq
 * @group             : 
 * @last modified on  : 04-22-2021
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   04-20-2021   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
global with sharing class CC_UpdateRegisterAddress implements HexaBPM.iCustomCodeExecutable {
    
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';

        if(stp!=null && stp.HexaBPM__SR__c!=null){

            List<Operating_Location__c> lstOptLoc = new List<Operating_Location__c>();
              
            for(HexaBPM__Service_Request__c serReq : [SELECT Id,
                                                            Name_of_the_Hosting_Affiliate__c,
                                                            Registered_Agent__c,
                                                            Co_working_Facility__c,
                                                            Fund_Manager__c,
                                                            Fund_Administrator_Name__c,
                                                            HexaBPM__Customer__c
                                                            FROM HexaBPM__Service_Request__c WHERE Id =: stp.HexaBPM__SR__c ]){
                
                if(serReq.Name_of_the_Hosting_Affiliate__c != NULL){
                    lstOptLoc.addAll(OB_RegisteredAddressContainerController.getActiveAndRegisteredOpertingLocation(serReq.Name_of_the_Hosting_Affiliate__c));
                }
                else if(serReq.Registered_Agent__c != NULL){
                    lstOptLoc.addAll(OB_RegisteredAddressContainerController.getActiveAndRegisteredOpertingLocation(serReq.Registered_Agent__c)); 
                }
                else if( serReq.Co_working_Facility__c != NULL){
                    lstOptLoc.addAll(OB_RegisteredAddressContainerController.getActiveAndRegisteredOpertingLocation(serReq.Co_working_Facility__c));
                }
                else if( serReq.Fund_Manager__c != NULL){
                    lstOptLoc.addAll(OB_RegisteredAddressContainerController.getActiveAndRegisteredOpertingLocation(serReq.Fund_Manager__c));
                }
                else if( serReq.Fund_Administrator_Name__c != NULL){
                    lstOptLoc.addAll(OB_RegisteredAddressContainerController.getActiveAndRegisteredOpertingLocation(serReq.Fund_Administrator_Name__c));
                }
                else if( serReq.HexaBPM__Customer__c != NULL){
                    lstOptLoc.addAll(OB_RegisteredAddressContainerController.getActiveAndRegisteredOpertingLocation(serReq.HexaBPM__Customer__c));
                } 
            }
                                                                            
            if(lstOptLoc.size() == 1){
                insertOperatingLocationAmendment(lstOptLoc[0],[SELECT Id FROM HexaBPM__Service_Request__c WHERE Id =: stp.HexaBPM__SR__c LIMIT 1]);
            }            
            
        }

        return strResult;
    }

    public static void insertOperatingLocationAmendment(Operating_Location__c objOL,HexaBPM__Service_Request__c srObj){
        
        List<RecordType> lstRecTyp = new List<RecordType>();
        
        lstRecTyp = [SELECT id, 
                             DeveloperName, 
                             SobjectType
                     FROM RecordType
                     WHERE DeveloperName = 'Operating_Location'
                     AND SobjectType = 'HexaBPM_Amendment__c'];
        
    	HexaBPM_Amendment__c objAmd     = new HexaBPM_Amendment__c();
        objAmd.Customer__c              = objOL.Account__c;
        objAmd.ServiceRequest__c        = srObj.Id;
        objAmd.Amendment_Type__c        = 'Operating Location';
        objAmd.IsRegistered__c          = true;
        objAmd.Unit__c                  = objOL.Unit__c;
        objAmd.Hosting_Company__c       = objOL.Hosting_Company__c;
        objAmd.Operating_Type__c        = objOL.Type__c;
        objAmd.Operating_Location__c    = objOL.Id;
        objAmd.Operating_Location_Status__c = objOL.Status__c;
        objAmd.Status__c               = 'Active';
        objAmd.recordTypeId = (lstRecTyp != NULL ? lstRecTyp [0].Id :NULL);

        if(objAmd.Role__c == NULL) {
           objAmd.Role__c = 'Operating Location';
        } 
        else if(objAmd.Role__c.indexOf('Operating Location') ==-1) {
            objAmd.Role__c = objAmd.Role__c + ';Operating Location';
        }
        
        upsert objAmd;
    }
}