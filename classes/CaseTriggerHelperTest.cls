/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CaseTriggerHelperTest {
    static testMethod void insertCaseTest(){
    
        Account accObj = new Account(
        Name = 'Test Account',Registration_License_No__c='123456');
        
        insert accObj;
    
        Case objCase = new Case();
        objCase.Origin = 'Phone';
        objCase.Subject = 'Test Subject';
        objCase.GN_Registration_No__c = '123456';
        test.startTest();
            insert objCase;
        test.stopTest();
    }
    
    static testMethod void updateCaseTest(){
        Case objCase = new Case();
        objCase.Origin = 'Phone';
        objCase.Subject = 'Test Subject';
        objCase.GN_Account_Name_Non_DIFC__c = 'test';
        insert objCase;
        test.startTest();
            objCase.GN_Department__c = 'Business Centre';
            update objCase;
        test.stopTest();
    }
    
    static testMethod void updateCaseTest2(){
        Case objCase = new Case();
        objCase.Origin = 'Phone';
        objCase.Subject = 'Test Subject';
        objCase.GN_Department__c = 'Enquiry';
        objCase.Status = 'New';
        objCase.GN_Account_Name_Non_DIFC__c = 'test';
        insert objCase;
        test.startTest();
            objCase.Status = 'Closed - Resolved';
            update objCase;
        test.stopTest();
    }
    
    static testMethod void updateCaseTest3(){
        Case objCase = new Case();
        objCase.Origin = 'Phone';
        objCase.Subject = 'Test Subject';
        objCase.GN_Department__c = 'Enquiry';
        objCase.Status = 'New';
        objCase.GN_Account_Name_Non_DIFC__c = 'test';
        insert objCase;
        test.startTest();
            objCase.GN_Are_you_sure_you_want_accept__c = 'Yes';
            update objCase;
        test.stopTest();
    }
    static testMethod void validateCreateLead() 
    {
        Mapping_Case_To_Lead__c caseMap = new Mapping_Case_To_Lead__c();
        caseMap.Name = '1';
        caseMap.Case_Field_API_Name__c = 'Company_Name__c';
        caseMap.Lead_Field_API_Name__c = 'Company';
        caseMap.Active__c = true;
        insert caseMap;
        
        Mapping_Case_To_Lead__c caseMap2 = new Mapping_Case_To_Lead__c();
        caseMap2.Name = '2';
        caseMap2.Case_Field_API_Name__c = 'GN_Last_Name__c';
        caseMap2.Lead_Field_API_Name__c = 'LastName';
        caseMap2.Active__c = true;
        insert caseMap2;

        Case objCase = new Case();
        objCase.Origin = 'Phone';
        objCase.Subject = 'Test Subject';
        objCase.GN_Last_Name__c = 'Test LastName';
        objCase.GN_Department__c = 'Enquiry';
        objCase.Status = 'New';
        objCase.Company_Name__c = 'Test Company';

        insert objCase;

        Case objCase2 = new Case();
        objCase2.Origin = 'Phone';
        objCase2.Subject = 'Test Subject';
        objCase2.GN_Department__c = 'Enquiry';
        objCase2.Status = 'New';
        objCase2.GN_Account_Name_Non_DIFC__c = 'test';
        objCase2.Company_Name__c = 'Test Company';
        insert objCase2;
        
        test.startTest();
            //objCase.GN_Are_you_sure_you_want_to_create_lead__c = 'Yes';
            update objCase;
        test.stopTest();
        
    }
    static testMethod void ownershipValid(){
       
        
        
        Group grp = new Group();
        grp.Name = 'Business Grp';
        grp.Type = 'Queue';
        insert grp;
        
        
        User objUser = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            //UserRoleId = r.Id
        );
        
        insert objUser;
        
        GroupMember grpmember = new GroupMember();
        grpmember.GroupId = grp.Id;
        grpmember.UserOrGroupId = objUser.Id;
        insert grpmember;
        
        User objUser2 = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'puser000@amamama.com',
            Username = 'puser000@amamama.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
            //UserRoleId = r.Id
        );
        
        insert objUser2;
        
        GroupMember grpmember2 = new GroupMember();
        grpmember2.GroupId = grp.Id;
        grpmember2.UserOrGroupId = objUser2.Id;
        insert grpmember2;
    
        system.runAs(objUser){

            test.startTest();
             Case objCase = new Case();
            objCase.Origin = 'Phone';
            objCase.Subject = 'Test Subject';
            objCase.GN_Last_Name__c = 'Test LastName';
            objCase.GN_Department__c = 'Enquiry';
            objCase.Status = 'New';
            objCase.Company_Name__c = 'Test Company';
            objCase.GN_Account_Name_Non_DIFC__c = 'test';
            insert objCase;
            list<case> lstCase = new list<case>();
            lstCase.add(objCase);
            map<Id,Case> mapCases = new map<id,Case>();
            mapCases.put(objCase.id,objCase);
            objCase.GN_Are_you_sure_you_want_to_create_lead__c = 'Yes';
            objCase.ownerId = objUser2.id;
            try{
            update objCase;
            }
            catch(Exception ex){
                Boolean expectedExceptionThrown =  ex.getMessage().contains('Please provide the Full Name ') ? true : false;
                System.assertEquals(expectedExceptionThrown, true);
            }
            Trigger_Settings__c TS = new Trigger_Settings__c(Name='CaseOwnershipValidation',is_Active__c = true);
            insert TS;
            CaseTriggerHelper.ownershipValidation(lstCase,mapCases);
            test.stopTest();
        }
        
        
    }
    	/* Added by Salma - To handle the validation on Case closure*/ 
        static testMethod void ValidaiononCaseClosure(){
        	Case objCase = new Case();
            objCase.Origin = 'Phone';
            objCase.Subject = 'Test Subject';
            objCase.GN_Last_Name__c = 'Test LastName';
            objCase.GN_Department__c = 'Enquiry';
            objCase.Status = 'New';
            objCase.Company_Name__c = 'Test Company';
            objCase.GN_Account_Name_Non_DIFC__c = 'test';
            objCase.GN_Type_of_Feedback__c = 'Enquiry';
            insert objCase;    
            
            try{
                ObjCase.Status = 'Closed - Resolved';
                ObjCase.GN_Are_you_sure_you_want_to_create_lead__c = 'No';
                update ObjCase;
            }catch(Exception e){
				Boolean expectedExceptionThrown =  e.getMessage().contains('Cannot update status to Closed - Resolved without atleast one Activity or Attachment.') ? true : false;
				System.AssertEquals(expectedExceptionThrown, true);
			} 
        }
        public static void init(){
            List<Mapping_Case_To_Lead__c> ObjMappingList =  new List<Mapping_Case_To_Lead__c>{
                   new Mapping_Case_To_Lead__c(Name='Area_of_Business',Active__c=true,Case_Field_API_Name__c='GN_Area_of_Business__c',Lead_Field_API_Name__c='Area_of_Business__c'),
                    new Mapping_Case_To_Lead__c(Name='Company_Name',Active__c=true,Case_Field_API_Name__c='Company_Name__c',Lead_Field_API_Name__c='Company'),
                    new Mapping_Case_To_Lead__c(Name='Email',Active__c=true,Case_Field_API_Name__c='Email__c',Lead_Field_API_Name__c='Email'),
                    new Mapping_Case_To_Lead__c(Name='First_Name',Active__c=true,Case_Field_API_Name__c='GN_First_Name__c',Lead_Field_API_Name__c='FirstName'),
                    new Mapping_Case_To_Lead__c(Name='Last_Name',Active__c=true,Case_Field_API_Name__c='GN_Last_Name__c',Lead_Field_API_Name__c='LastName'),
                    new Mapping_Case_To_Lead__c(Name='Origin',Active__c=true,Case_Field_API_Name__c='Origin',Lead_Field_API_Name__c='LeadSource')
                    };
                 
                insert ObjMappingList;
                
                List<Case_Origin_to_Lead_Source_Map__c> ObjOriginMap  = new List<Case_Origin_to_Lead_Source_Map__c>{
                    new Case_Origin_to_Lead_Source_Map__c(Name='Email',Case_Origin__c='Email',Lead_Source__c='Other')};
                insert ObjOriginMap;
        }
        static testMethod void createLeadTest1(){
            init();
                                                                                                                                                      
        	Case OldobjCase = new Case();
            OldobjCase.Origin = 'Phone';
            OldobjCase.Subject = 'Test Subject';
            OldobjCase.GN_Last_Name__c = 'Test LastName';
            OldobjCase.GN_Department__c = 'Enquiry';
            OldobjCase.Status = 'New';
            OldobjCase.Company_Name__c = 'Test Company';
            OldobjCase.GN_Account_Name_Non_DIFC__c = 'test';
            OldobjCase.GN_Type_of_Feedback__c = 'Enquiry';
            OldobjCase.GN_Are_you_sure_you_want_to_create_lead__c = 'No';
            insert OldobjCase; 
             try{
                Case NewobjCase = new Case(id = OldObjCase.id);
                NewobjCase.GN_Are_you_sure_you_want_to_create_lead__c = 'Yes';
                NewobjCase.Company_Name__c = '';
                update NewobjCase; 
            	
           
                Map<Id,Case> OldCaseMap = new Map<Id,Case>();
                OldCaseMap.put(OldobjCase.id,OldobjCase);
                List<Case> CaseList = new List<Case>{NewobjCase};
				CaseTriggerHelper.createLead(OldCaseMap, caseList);
            }catch(Exception e){
				Boolean expectedExceptionThrown =  e.getMessage().contains('Please provide the Full Name ') ? true : false;
                System.assertEquals(expectedExceptionThrown, true);
			} 
        }
    	static testMethod void createLeadTest2(){
            init();
                                                                                                                                                      
        	Case OldobjCase = new Case();
            OldobjCase.Origin = 'Phone';
            OldobjCase.Subject = 'Test Subject';
            OldobjCase.GN_Last_Name__c = 'Test LastName';
            OldobjCase.GN_Department__c = 'Enquiry';
            OldobjCase.Status = 'New';
            OldobjCase.Company_Name__c = 'Test Company';
            OldobjCase.GN_Account_Name_Non_DIFC__c = 'test';
            OldobjCase.GN_Type_of_Feedback__c = 'Enquiry';
            OldobjCase.GN_Are_you_sure_you_want_to_create_lead__c = 'No';
            insert OldobjCase; 
             try{
                Case NewobjCase = new Case(id = OldObjCase.id);
                NewobjCase.GN_Are_you_sure_you_want_to_create_lead__c = 'Yes';
                update NewobjCase; 
            	
           
                Map<Id,Case> OldCaseMap = new Map<Id,Case>();
                OldCaseMap.put(OldobjCase.id,OldobjCase);
                List<Case> CaseList = new List<Case>{NewobjCase};
				CaseTriggerHelper.createLead(OldCaseMap, caseList);
            }catch(Exception e){
				Boolean expectedExceptionThrown =  e.getMessage().contains('Please provide the Full Name ') ? true : false;
                System.assertEquals(expectedExceptionThrown, true);
			} 
        }
}