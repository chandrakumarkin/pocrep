/*********************************************************************************************************************
*  Name     : schedulableSRAddressChange_Test
*  Author     : Ananya Acharya
*  Purpose   : This is the test class  of schedulableSRAddressChange which all approved service request and create certificate for child customers.
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
V1.0          Ananya				15/06/2021
//questions
////1? Which approved Service Request records will be picked up: Today, LAST_N_DAYS, ?
//2? we need to store any information for failure to track failed for processed records to be picked in next batch run        
*/

@isTest
public class SchedulableSRAddressChange_Test {
    
     static testMethod void SchedulableSRAddressChange_Test() {        
        test.starttest();
        SchedulableSRAddressChange myClass = new SchedulableSRAddressChange ();   
        String chron ='0 0 0 16 2 ? 2022';       
        system.schedule('Test Sched', chron, myClass);
        test.stopTest();
    }
}