@RestResource(urlMapping = '/DubaiData/*')
global class CLS_Dubai_DataController{

    @Httpget 
    global static  void jsonResponse()
    {
          String Mode =  RestContext.request.params.get('sqltype');
          if(!test.isRunningTest())
          {
                RestContext.response.addHeader('Content-Type', 'application/json');
          }
      
      try
      {
       
        RestContext.response.responseBody = Blob.valueOf(jsonResString(Mode ));
      }
      catch(Exception ex)
      {
        
      }
      
      
    }
    
    public static string jsonResString(string Mode)
    {
     //Dubai_Data__c ObjSQL=Dubai_Data__c.getInstance(Mode );
     
     List<Dubai_Data_Meta__mdt> DataList= [SELECT SQL_Values__c   FROM Dubai_Data_Meta__mdt   WHERE DeveloperName =:Mode];
                                                                
     if(DataList.Size()>0)     
    return      JSON.serialize( Database.query(DataList[0].SQL_Values__c));
    else
    return  '';
    
    
    }
    
 }