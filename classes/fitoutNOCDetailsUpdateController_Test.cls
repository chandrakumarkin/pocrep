@isTest
public class fitoutNOCDetailsUpdateController_Test {
	 static testMethod void mytest() {
        
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Request_Contractor_Access','Fit_Out_Service_Request','Fit_Out_Induction_Request')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Fit_Out_Service_Request';
        testSrTemplate.Menutext__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_RecordType_API_Name__c = 'Fit_Out_Service_Request';
        testSrTemplate.SR_Group__c = 'Fit-Out & Events';
        testSrTemplate.Active__c = true;
        
        insert testSrTemplate;
        
        SR_Steps__c srstep = new SR_Steps__c();
        insert srstep;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
         
        Service_Request__c portalAccess= new Service_Request__c();
        portalAccess.RecordTypeId = mapRecordType.get('Request_Contractor_Access');
        portalAccess.Customer__c = objAccount.Id;
        //SR.Service_Category__c = 'New';       
        insert portalAccess;
        
        Service_Request__c SR1= new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Fit_Out_Induction_Request');
        SR1.Customer__c = objAccount.Id;
        SR1.Linked_SR__c = portalAccess.id;        
        insert SR1;
         
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('Fit_Out_Service_Request');
        SR.Customer__c = objAccount.Id;
        SR.Linked_SR__c = SR1.id;     
        SR.Type_of_Request__c = 'Tenant Authorized Representative';   
        
        insert SR;
         
         
        Step__c objStep = new Step__c();
        objStep.SR__c = SR.Id;
        objStep.Step_No__c = 1.0;
        objStep.step_notes__c = 'abc';
        objStep.SR_Step__c=srstep.id;
        insert objStep; 
         
        Test.startTest();
        	fitoutNOCDetailsUpdateController.getRequestData(objStep.id);
         	fitoutNOCDetailsUpdateController.saveNOCDetails(SR);
        Test.stopTest();
     }
}