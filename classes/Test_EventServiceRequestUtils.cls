/******************************************************************************************
 *  Author   	: Claude Manahan
 *  Company  	: NSI JLT
 *  Description : Test Utility Class for actions related to Event Service Requests
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    06-06-2016	Claude		  Created
V1.1	03-10-2016	Claude		  Added check for duplicate SR Docs
****************************************************************************************************************/
@isTest(SeeAllData=false)
public class Test_EventServiceRequestUtils {
	
	public static Boolean hasContractor = false;
	
	/**
	 * Tells whether the validation of a test should execute
	 */
	public static Boolean hasValidation = false;
	
	public static void testRequestAccessEventsForm(){
	    
	    Test.startTest();
	    
	    SR_Template__c contractorWalletTemplate = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Contractor_Registration','Contractor_Registration','Fit-Out & Events');
	    
	    insert contractorWalletTemplate;
            
        Fo_cls_ContractorFormController contractorWalletCls = new Fo_cls_ContractorFormController();
        
        contractorWalletCls.srObj = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
        
        contractorWalletCls.srObj.Company_Type_list__c = 'Event Organizer';
        
        contractorWalletCls.changeIssuingAuthority();
        
        contractorWalletCls.srObj.Trade_License_No__c = '123456';
        contractorWalletCls.selectedAuthority = 'Government of Dubai';
        contractorWalletCls.srObj.Company_Name__c = 'Test Contractor';
        
        contractorWalletCls.srObj.Title__c = 'Mr.';
        contractorWalletCls.srObj.Passport_Number__c = '123456AB';
        contractorWalletCls.srObj.Email_Address__c = 'test.contractoruat@difc.dev.org';
        contractorWalletCls.srObj.First_Name__c = 'TestContractor';
        contractorWalletCls.srObj.Mobile_Number__c = '+971 12345678';         
        contractorWalletCls.srObj.Send_SMS_To_Mobile__c= '+971 12345678'; 
        contractorWalletCls.srObj.Last_Name__c = 'Lname';
        contractorWalletCls.srObj.Position__c = 'Tester';
        contractorWalletCls.srObj.Place_of_Birth__c = 'Dubai';
        contractorWalletCls.srObj.Date_of_Birth__c = System.Today() - 200;
        contractorWalletCls.srObj.Expiry_Date__c = System.Today() + 200;
        contractorWalletCls.srObj.Nationality_list__c = 'United Arab Emirates';
        contractorWalletCls.isAcceptedTermsAndConditions = true;
        
        Service_Request__c tempSr = contractorWalletCls.srObj.clone(false,true,false,false);
        
        contractorWalletCls.saveContractor();
        
        String newRequestId = [SELECT Id from service_request__c where createddate = TODAY AND Record_Type_Name__c = 'Contractor_Registration'].Id;
        
        ApexPages.currentPage().getParameters().put('srId',newRequestId);
        
        contractorWalletCls = new Fo_cls_ContractorFormController();
        
        contractorWalletCls.contractorLicense = Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Test Name','Test Body');
        contractorWalletCls.contractorPassport = Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Test Name 2','Test Body 2');
        
        contractorWalletCls.submitContractor();
        
        Step__c testStep = new Step__c(SR__c = newRequestId);     // TODO: Remove comments on second deployment
        
        CC_cls_FitOutandEventCustomCode.createContractor(testStep);    
        
        System.assertEquals(false, String.isBlank(newRequestId)); 
        
        Service_Request__c testSR2 = CC_cls_FitOutandEventCustomCode.relatedServiceRequest(newRequestId);
        
        Account newContractor = [SELECt Id, Is_Registered__c FROM Account where createddate = TODAY limit 1];
        
        Contact testContact = CC_cls_FitOutandEventCustomCode.createContractorContact(testSR2,newContractor.Id,true);
        
        User testUser = CC_cls_FitOutandEventCustomCode.createContractorUser(testSR2,testContact,newContractor);
        
        ApexPages.currentPage().getParameters().put('srId','');
        
		performEventTest(newContractor.Id,hasValidation,hasContractor);
		
		testStep();
		
		Test.stopTest();
	}
	
	@future
    private static void performEventTest(String contractorId, Boolean hasValidations, Boolean isContractor){
        
        /* Create an sample Request Form SR Template */
		SR_Template__c testEventRequestFormTemplate = Test_CC_FitOutCustomCode_DataFactory.getTestSRTemplate('Request Access For Events','Request_Access_For_Events','Fit-Out & Events');
		
		insert testEventRequestFormTemplate;
		
		Cls_EventRequestAccessForm testEventRequestAccessController = getEventRequestClassInstance(contractorId,hasValidations,isContractor);      // Get the Event Form Controller with the saved Service Request
		
		PageReference testSubmissionRef;
		
		if(hasValidations){
			
			/* 
			    The Validations:
			    checkContactDetails();      // #1 - Contact Validation
		        checkSrDetails();           // #2 - Blank Field validation
		        checkOrganizerDetails();    // #3 - Check organizer details
		        checkOtherLocation();       // #4 - Location details
		        checkContractorDetails();   // #5 - Contractor details // TODO: Remove when Contractor form has been implemented
		        checkLicenseExpiryDates();  // #6 - expiry date validation
			 */
			
			//#1 - Create duplicate passport names
			
			/* Make a copy of the fields */
			String nationalityCopy = testEventRequestAccessController.srObj.Nationality_list__c;
			String passportCopy = testEventRequestAccessController.srObj.Nationality_list__c;
			String eventBriefCopy = testEventRequestAccessController.srObj.Event_Brief__c;
			String otherLocationCopy = testEventRequestAccessController.srObj.Location__c;
			String companyNameCopy = testEventRequestAccessController.srObj.Company_Name__c;
			String registeredAddCopy = testEventRequestAccessController.srObj.Registered_Address__c;
			
			testEventRequestAccessController.srObj.Nationality_list__c = testEventRequestAccessController.srObj.Sponsor_Nationality__c;
			testEventRequestAccessController.srObj.Passport_Number__c = testEventRequestAccessController.srObj.Sponsor_Passport_No__c;
            
			testSubmissionRef = testEventRequestAccessController.submitSRrequest(); // Trigger the 1st validation
			
			/* Revert the changes, then trigger the 2nd validation */
			testEventRequestAccessController.srObj.Nationality_list__c = nationalityCopy;
			testEventRequestAccessController.srObj.Passport_Number__c = passportCopy;
			
			testEventRequestAccessController.srObj.Event_Brief__c = null; // Trigger the 2nd validation
			
			testSubmissionRef = testEventRequestAccessController.saveSRrequest(); // Trigger the 2nd validation
			
			testEventRequestAccessController.srObj.Event_Brief__c = eventBriefCopy;
			testEventRequestAccessController.srObj.Location__c = null;
			
			testSubmissionRef = testEventRequestAccessController.saveSRrequest(); // Trigger the 2nd validation
			
			testEventRequestAccessController.srObj.Company_Name__c = null;
			
			testSubmissionRef = testEventRequestAccessController.saveSRrequest(); // Trigger the 3rd validation
			
			testEventRequestAccessController.srObj.Company_Name__c = companyNameCopy;
			testEventRequestAccessController.isContractorAvailable = true;
			testEventRequestAccessController.srObj.Registered_Address__c = null;
			
			testSubmissionRef = testEventRequestAccessController.saveSRrequest(); // Trigger the 3rd validation
			
			testEventRequestAccessController.srObj.Registered_Address__c = registeredAddCopy;
			testEventRequestAccessController.srObj.Expiry_Date__c = system.today() - 10;
			
			testSubmissionRef = testEventRequestAccessController.saveSRrequest(); // Trigger the 3rd validation
			
			/* Trigger an invalid phone number validation */
			testEventRequestAccessController.srObj.Sponsor_Street__c = '1234';
			testEventRequestAccessController.srObj.Sponsor_Mobile_No__c = '1234';
			testEventRequestAccessController.srObj.Sponsor_Office_Telephone__c = '1234';
			testEventRequestAccessController.srObj.Work_Phone__c = '1234';
			testSubmissionRef = testEventRequestAccessController.saveSRrequest(); // Trigger the 4th validation
			
		} else {
			
			//testEventRequestAccessController.isContractorAvailable = true;
			testSubmissionRef = testEventRequestAccessController.submitSRrequest(); // Submit the SR
			
			/* V1.1 - Claude - Start */
			// Trigger the submission again
			
			String srIdParam = [SELECT Id FROM Service_Request__c WHERE CreatedDate = TODAY AND Record_Type_Name__c = 'Request_Access_For_Events'].ID;
			
			Test.setCurrentPage(Page.EventRequestAccessForm);			   		// commence redirect
			
			apexpages.currentpage().getparameters().put('srId',srIdParam);		// Set the SR ID parameter
			
			testEventRequestAccessController = new Cls_EventRequestAccessForm();
			
			String defaultBody = 'Unit Test Attachment Body';
			
			testEventRequestAccessController.agreement 
				= Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Unit Test Attachment 1',defaultBody);
				
			testEventRequestAccessController.contractorLicense 
				= Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Unit Test Attachment 4',defaultBody);
				
			testEventRequestAccessController.contractorPassport
				= Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Unit Test Attachment 5',defaultBody);
				
			testEventRequestAccessController.proposal 
				= Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Unit Test Attachment 6',defaultBody);	
			
			testSubmissionRef = testEventRequestAccessController.submitSRrequest(); // Submit the SR
			
			/* V1.1 - Claude - End */
		}
        
    }
    
    @future
    private static void testStep(){
    	
    	/* Create a step corresponding to 'Verification & Approval By IDAMA' */
		
		Step__c testVerificationStep = new Step__c();
		
		Test_CC_FitOutCustomCode_DataFactory.setEventStepType(testVerificationStep);
		
		/* Set the other mandatory fields */
		testVerificationStep.SR__c = [SELECT ID from Service_Request__c WHERE CreatedDAte = TODAY LIMIT 1].Id; // Get the SR
		testVerificationStep.Client_Name__c = [SELECT Name FROM Account WHERE CreatedDate = TODAY LIMIT 1].Name;
		
		testVerificationStep.Status__c = [SELECT Id FROM Status__c WHERE Name = 'Approved' LIMIT 1].Id;
		
		insert testVerificationStep; //create the sample step
		
		// Manually call the CreateUserForEventAccess() method
		
		CC_cls_FitOutandEventCustomCode.createUserForEventAccess(testVerificationStep);
		CC_cls_FitOutandEventCustomCode.createUserForContractor(testVerificationStep);
    	
    }
	
	private static Cls_EventRequestAccessForm getEventRequestClassInstance(String contractorId, Boolean hasValidations, Boolean isContractor){
	    
	    Test_CC_FitOutCustomCode_DataFactory.isDefaultCountry = true;
		
		/* Create the SR Template for Event Request acces */
		
		Cls_EventRequestAccessForm testEventRequestAccessController = new Cls_EventRequestAccessForm(); // initialize page controller
		
		testEventRequestAccessController.accountId = contractorId;
		
		Test.setCurrentPage(Page.EventRequestAccessForm);			   // set the page to the form
		
		//testEventRequestAccessController.isContractorAvailable = isContractor; // set to true to add a contractor
		testEventRequestAccessController.isContractorAvailable = false; // set to true to add a contractor
		
		Building__c build = [SELECT Id FROM Building__c WHERE CreatedDate = TODAY]; // Query the newly created building
		
		//setServiceRequestRecordType(testEventRequestAccessController.srObj,'Request_Access_For_Events'); //Set the record type to 'Request Event Contractor Access;
		
		/* Set the Event Form Details */
		
		testEventRequestAccessController.srObj = Test_CC_FitOutCustomCode_DataFactory.getTestFitOutServiceRequest();
		
		testEventRequestAccessController.accountName = 'test';
		
		testEventRequestAccessController.srObj.Nationality_list__c = 'Philippines';
		testEventRequestAccessController.srObj.Passport_Number__c = '9175047';
		testEventRequestAccessController.srObj.Sponsor_Passport_No__c = '';
		testEventRequestAccessController.srObj.Additional_Details__c = 'Test Additional Details';
		testEventRequestAccessController.srObj.Approving_Authority__c = 'Government of Dubai';
		testEventRequestAccessController.srObj.Building__c = build.Id;
		testEventRequestAccessController.srObj.Clean_up_End_Time__c = System.Now() + 2;
		testEventRequestAccessController.srObj.Courier_Cell_Phone__c = '+971 12345678';
		testEventRequestAccessController.srObj.Current_Address_Country__c = 'United Arab Emirates';
		testEventRequestAccessController.srObj.Event_Brief__c = 'Test Brief';
		testEventRequestAccessController.srObj.Event_End_Date__c = System.Today() + 2;
		testEventRequestAccessController.srObj.Event_Name__c = 'Test Event';
		testEventRequestAccessController.srObj.Event_Start_Date__c = System.Today() + 1;
		testEventRequestAccessController.srObj.Expiry_Date__c = System.Today() + Test_CC_FitOutCustomCode_DataFactory.FUTURE_DAYS_SIZE;
		testEventRequestAccessController.srObj.Location__c =  'Other Test Location';
		testEventRequestAccessController.srObj.Name_of_Contact_Person__c = 'Test Contact Person';
		testEventRequestAccessController.srObj.Number_of_Participants__c = 2;
		testEventRequestAccessController.srObj.Number_of_Spectators__c = 2;
		testEventRequestAccessController.srObj.Set_up_Begin_Time__c = System.Now() + 1;
	
		if(!hasValidations){
		    
			PageReference testSubmissionRef = testEventRequestAccessController.saveSRrequest();
		
			/* Create the mandatory attachments */
			
			//String srIdParam = [SELECT Id FROM Service_Request__c WHERE CreatedDate = TODAY AND Record_Type_Name__c = 'Request_Access_For_Events'].ID;
			String srIdParam = testEventRequestAccessController.srObj.Id;
			
			Test.setCurrentPage(Page.EventRequestAccessForm);			   		// commence redirect
			
			apexpages.currentpage().getparameters().put('srId',srIdParam);		// Set the SR ID parameter
			
			testEventRequestAccessController = new Cls_EventRequestAccessForm(); // re-initialize page controller
			
			String defaultBody = 'Unit Test Attachment Body';
			
			testEventRequestAccessController.agreement 
				= Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Unit Test Attachment 1',defaultBody);
				
			testEventRequestAccessController.contractorLicense 
				= Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Unit Test Attachment 4',defaultBody);
				
			testEventRequestAccessController.contractorPassport
				= Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Unit Test Attachment 5',defaultBody);
				
			testEventRequestAccessController.proposal 
				= Test_CC_FitOutCustomCode_DataFactory.getTestAttachment('Unit Test Attachment 6',defaultBody);	
				
		} /*else {
		    
		    System.assertEquals(false,[SELECT Id FROM User WHERE (Contact.AccountId = :newContractor.Id OR Contact.Contractor__c =: newContractor.Id) AND isActive = true AND (Contact.Country__c =:'United Arab Emirates' OR Contact.Nationality__c = :'United Arab Emirates' ) AND Contact.Passport_No__c =: '123456AB'].isEmpty());
		    
		}*/
		
        return testEventRequestAccessController;
	}
    
}