/******************************************************************************************
 *  Name        : CLS_LeadUtils 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2016-05-30
 *  Description : This class will handle all Lead-related transactions for CRM
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   30-05-2016	 Claude		   Created
*******************************************************************************************/
public abstract without sharing class CLS_LeadUtils {
	
	/* Class members to be used throughout the transaction */
	protected List<Lead> newLeads;
	protected Map<Id,Lead> newLeadMap;
	protected Map<Id,Lead> oldLeadMap;
	
	/**
	 * Handles lead transactions after they have been converted
	 * @auhtor		Claude Manahan
	 * @date		2016-05-30
	 * @param		newLeads			the converted leads
	 * @param		newLeadMap			the map of the new values of the converted leads
	 * @param		oldLeadMap			the map of the old values of the converted leads
	 */
	public void afterConvertion(List<Lead> newLeads, Map<Id,Lead> newLeadMap, Map<Id,Lead> oldLeadMap){
		
		/* Set the transaction collection */
		this.newLeads = newLeads;
		this.newLeadMap = newLeadMap;
		this.oldLeadMap = oldLeadMap;
		
		executeLeadTransaction();
	}
	
	/**
	 * Checks if the leads are converted leads
	 * @auhtor		Claude Manahan
	 * @date		2016-05-30
	 * @param		newLeads				the converted leads
	 * @return		isLeadsForConversion	flag to check if the leads are converted leads
	 */
	public static Boolean isLeadsForConversion(List<Lead> newLeads){
		
		for(Lead l : newLeads){
			if(l.IsConverted == false){
				return false;
			}
		}
		
		return true;
		
	}
	
	/* 
	 * Runs all related methods for processing leads
	 * @author		Claude Manahan
	 * @date		2016-05-30
	 *
	 * NOTE: If you want to define your own lead and cross-object 
	 * 		 transactions, simply extend the class, override the 
	 * 		 processLeads() and executeTransactions() method, 
	 * 		 and then call the executeLeadTransaction() in your class.
	 *
	 */
	protected void executeLeadTransaction(){
		processLeads();
		executeTransactions();
	}
    
    /**
     * Contains methods that will process the lead values
     */
    protected abstract void processLeads();
    
    /**
     * Contains methods that will execute after the lead
     * values have been processed
     */
    protected abstract void executeTransactions();
    
    /**
     * Checks each lead using custom validations
     * 
     * @author		Claude Manahan
     * @date		2016-30-05
     *
     *	NOTE: Please add all validation procedures here.
	 * 		  Use the errMsg to add your message
     */
    protected abstract void validateLeads();
    
    /**
     * Sets the error message
     */
    protected void setErrorMessage(Lead convLead, String errMsg){
    	convLead.addError(errMsg);
    }
    
    /**
     * Gets the related accounts of the leads
     * @author		Claude Manahan
     * @date		2016-30-05
     * @param		accountIds			set of account IDs
     * @return		accountMap			list of related accounts
     */
    public Map<Id,Account> getRelatedAccounts(Set<String> accountIds){ 
    	
    	return new Map<Id,Account>([SELECT Id, 
    								Sector_Lead_lookup__c,
    								Sector_Classification__c,
    								BD_Sector__c , 
    								BP_No__c, 
    								Company_Type__c 
    								FROM Account 
    								where Id in :accountIds]); // query the account records
    }
    
    /**
     * Gets the related opportunities of the leads
     * @author		Claude Manahan
     * @date		2016-30-05
     * @param		oppIds				set of opportunity IDs
     * @return		oppMap				map of related opportunities
     */
    public Map<Id,Opportunity> getRelatedOpportunties(Set<String> oppIds){
		
		return new Map<Id,Opportunity>([SELECT 
	    									Id,
	    									StageName,
	    									Account.BD_Sector__c,
	    									RecordType.DeveloperName,
	    									Oppty_Rec_Type__c,
	    									OwnerId
	    									FROM Opportunity 
	    									WHERE Id in :oppIds]);
	}
	
	/**
     * Returns a map of the account IDs per converted lead record
     * @author		Claude Manahan
     * @date		2016-30-05
     * @return		convertedAccountIdMap				map of converted account IDs
     */
	public Map<Id,Id> getConvertedAccountIdMap(){
		
		Map<Id,Id> convertedAccountIdMap = new Map<Id,Id>();
		
		if(!newLeads.isEmpty()){
			
			for(Lead l : newLeads){
				
				if(String.isNotBlank(l.ConvertedAccountId)){
					convertedAccountIdMap.put(l.Id,l.ConvertedAccountId);
				}
			}
		}
		
		return convertedAccountIdMap;
	}
    
    /**
     * Get a list of leads based on criteria
     * @params		filter		The filter criteria
     * @return		leads		The list of leads
     */
    public static list<Lead> getLeads(String filter){
    	
    	string strLeadQuery = Apex_Utilcls.getAllFieldsSOQL('Lead');
		strLeadQuery += ' where ' + filter;
		
		System.debug(strLeadQuery);
		
		return database.query(strLeadQuery);
    	
    }
}