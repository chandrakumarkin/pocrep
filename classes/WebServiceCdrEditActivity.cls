public class WebServiceCdrEditActivity {

    
    public static String editJsonActivityCdr(String ActivityID){
        
        License_Activity__c Activity =   [SELECT id, Account__r.Unified_License_Number__c, Name, Activity__r.Activity_Code__c, Activity__r.Name from License_Activity__c Where Id =:ActivityID];
        JSONGenerator createActivityJson = JSON.createGenerator(true);
        createActivityJson.writeStartArray();
        createActivityJson.writeStartObject();
        if(!String.isBlank(Activity.Account__r.Unified_License_Number__c))
        createActivityJson.writeStringField('unifiedNumber', Activity.Account__r.Unified_License_Number__c);
        //if(!String.isBlank(Activity.Account__r.Unified_License_Number__c))
        createActivityJson.writeStringField('zoneName', 'zoneName');
        if(!String.isBlank(Activity.Activity__r.Activity_Code__c))
        createActivityJson.writeStringField('activityCode', Activity.Activity__r.Activity_Code__c);
        if(!String.isBlank(Activity.Activity__r.Name))
        createActivityJson.writeStringField('activityName', Activity.Activity__r.Name);
        createActivityJson.writeEndObject();
        createActivityJson.writeEndArray();
        System.debug('createActivityJson:-'+createActivityJson.getAsString());
        return createActivityJson.getAsString();
        
    }
    
    public static void editActivityCdr(String ActivityId){
        
            String jsonRequest;
            List<Log__c> ErrorObjLogList = new List<Log__c>();
            //Get authrization token 
            String Token = WebServiceCDRGenerateToken.cdrAuthToken();
            //WebService to Push the Activity Form
            HttpRequest request = new HttpRequest();
            request.setEndPoint(System.label.Cdr_Create_Activity_Url);
            jsonRequest = editJsonActivityCdr(ActivityId);
            request.setBody(jsonRequest);
            request.setTimeout(120000);
            request.setHeader('Accept', 'text/plain');
            request.setHeader('Content-Type', 'application/json-patch+json');
            request.setHeader('Authorization','Bearer'+' '+Token);
            request.setMethod('PUT');
            HttpResponse response = new HTTP().send(request);
            System.debug('responseStringresponse:'+response.toString());
            System.debug('STATUS:'+response.getStatus());
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('strJSONresponse:'+response.getBody());
            request.setTimeout(110000);
        
        if (response.getStatusCode() == 200)
        {           
            Log__c ErrorobjLog = new Log__c();
            ErrorobjLog.Type__c = 'CDR Activity Edit';
            ErrorobjLog.Description__c = 'Request: '+ jsonRequest +'Response: '+ response.getBody() ;
            ErrorobjLog.sObject_Record_Id__c = ActivityId;
            ErrorObjLogList.add(ErrorobjLog);
            
        }
        else
        {
            Log__c ErrorobjLog = new Log__c();
            ErrorobjLog.Type__c = 'CDR Activity Edit Error';
            ErrorobjLog.Description__c = 'Request: '+ jsonRequest +'Response: '+ response.getBody() ;
            ErrorobjLog.sObject_Record_Id__c = ActivityId;
            ErrorObjLogList.add(ErrorobjLog);      
        }
        
        Insert ErrorObjLogList;
    }
}