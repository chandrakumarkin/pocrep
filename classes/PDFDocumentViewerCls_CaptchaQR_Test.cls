@isTest(seealldata=false)
public class PDFDocumentViewerCls_CaptchaQR_Test{

static testMethod void PDFDocumentViewerTestMethod(){
    Service_Request__c objSR3 = new Service_Request__c();
    objSR3.Email__c = 'testsr@difc.com';    
    objSR3.Express_Service__c = false;
    insert objSR3;
      step__c objStep = new Step__c();
      objStep.SR__c = objSR3.Id;
      objStep.No_PR__c = false;
      insert objStep;
      Attachment objAttachment1 = new Attachment();
      objAttachment1.Name = 'Test Document';
      objAttachment1.Body = blob.valueOf('test');     
      objAttachment1.ParentId =objStep.id;
      insert objAttachment1;
        system.debug('objAttachment1 id'+objAttachment1.Id);
      SR_Doc__c srDoc = new SR_Doc__c();
      srDoc.Name ='SR Docs';
      srDoc.Service_Request__c =objSR3.id;
      srDoc.Status__c= 'Approved';
      srDoc.doc_id__c=objAttachment1.id;
      srDoc.Document_No_Id__c = 'ReferenceNumber';
      insert srDoc;
    system.debug('SR Docs =='+srDoc.doc_id__c);
    Test.startTest();
    PDFDocumentViewerCls_CaptchaQR pdfdocs = new PDFDocumentViewerCls_CaptchaQR();
    pdfdocs.documentNumber = 'documentNumber';
   // pdfdocs.AttachBlobVal = Blob.valueOf('AttachBlobVal');
    pdfdocs.AtchContentType = 'AttachBlobVal';
    pdfdocs.isCaptchaEnabled = True;
    pdfdocs.debugMessage = 'debugMessage';
    pdfdocs.PageLoad();
    pdfdocs.loadDocument('ReferenceNumber');
    Test.stopTest();
    }   
    
    static testMethod void PDFDocumentViewerTestMethod1(){
    
    
    	 List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(2);
        insertNewAccounts[0].Registration_License_No__c = '1234';  
        insert insertNewAccounts;
        
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'In_Principle','In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insertNewSRs[0].Passport_No__c = 'A12345';
        insertNewSRs[0].Nationality__c = 'India';
        insertNewSRs[0].Date_of_Birth__c = Date.today().addmonths(-300);
        insertNewSRs[0].Gender__c = 'Male';
        insertNewSRs[0].Place_of_Birth__c = 'Dubai';
        insertNewSRs[0].Date_of_Issue__c = Date.today().addmonths(-20);
        insertNewSRs[0].Last_Name__c = 'test';
        insertNewSRs[0].HexaBPM__Email__c = 'test@test.com';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Business_Sector__c = 'Hotel';
        insertNewSRs[0].entity_name__c = 'Tentity';
        insertNewSRs[0].Apply_for_Establishment_Card__c = 'Yes';
       // insertNewSRs[0].Place_of_Issue__c = 'Dubai';
        insert insertNewSRs; 
    
    
      
      
       HexaBPM__SR_Doc__c srDoc = new HexaBPM__SR_Doc__c();
        srDoc.Name = 'License';
        srDoc.HexaBPM__Service_Request__c=insertNewSRs[0].Id;
        insert srDoc;
        
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = srDoc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
        
    Test.startTest();
    PDFDocumentViewerCls_CaptchaQR pdfdocs = new PDFDocumentViewerCls_CaptchaQR();
    pdfdocs.documentNumber = 'documentNumber';
   // pdfdocs.AttachBlobVal = Blob.valueOf('AttachBlobVal');
    pdfdocs.AtchContentType = 'AttachBlobVal';
    pdfdocs.isCaptchaEnabled = True;
    pdfdocs.debugMessage = 'debugMessage';
    pdfdocs.PageLoad();
    pdfdocs.loadDocument('SRReferenceNumber');
    Test.stopTest();
    }   
}