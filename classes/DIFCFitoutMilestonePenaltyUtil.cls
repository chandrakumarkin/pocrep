/**
 * @description       : 
 * @author            : Mudasir Wani
 * @group             : 
 * @last modified on  : 12-14-2020
 * @last modified by  : Mudasir Wani
 * Modifications Log 
 * Ver   Date         Author         Modification
 * 1.0   12-13-2020   Mudasir Wani   Initial Version
**/
public with sharing class DIFCFitoutMilestonePenaltyUtil {
    public DIFCFitoutMilestonePenaltyUtil() {

    }
    public static Fitout_Milestone_Penalty__c createFitoutMilestonePenalty(String leaseId , String accountId , String serviceRequestId ,
                                                                    Date handoverDate){
        if(Test.isRunningTest()) handoverDate = system.today();
        String monthValue = '';
        if(handoverDate.month() < 10) monthValue = '0'+handoverDate.month();
        else monthValue  = ''+handoverDate.month();
        String dateString                   = handoverDate.day()+'/'+monthValue +'/'+handoverDate.year();
        Fitout_Milestone_Penalty__c milestonePenalty = new Fitout_Milestone_Penalty__c();
        milestonePenalty.Lease__c           =   leaseId !='' ? leaseId : null;
        milestonePenalty.Account__c         =   accountId !=''? accountId : null;
        milestonePenalty.service_request__c =   serviceRequestId !='' ? serviceRequestId : null;
        //milestonePenalty.unique_ID__c       =   accountId+''+leaseId+''+String.valueOf(DateTime.newInstance(handoverDate.year(), handoverDate.month(), handoverDate.day()).format('yyyy-MM-dd'));
        milestonePenalty.unique_ID__c       =   accountId+''+leaseId+''+dateString;
        system.debug('milestonePenalty---Mudasir---'+milestonePenalty);
        return milestonePenalty;
    }
    public static void upsertFitoutMilestonePenalty(List<Fitout_Milestone_Penalty__c> penaltyListToUpdate){
        //system.assertEquals(null,penaltyListToUpdate);
        Schema.SObjectField esternalIdField = Fitout_Milestone_Penalty__c.Fields.unique_ID__c;
        Database.UpsertResult[] penaltyList = Database.upsert(penaltyListToUpdate,esternalIdField,false);
        list<Log__c> logList = new List<Log__c>();
        for(Database.UpsertResult penaltyMilestoenRec : penaltyList) {
            if (penaltyMilestoenRec.isSuccess()) {
                system.debug('UpsertResult---Mudasir---'+penaltyMilestoenRec.getId());
            // Operation was successful, so get the ID of the record that was processed
            }
            else {
                // Operation failed, so get all errors                
                for(Database.Error err : penaltyMilestoenRec.getErrors()) {
                    Log__c objLog               =   new Log__c();
                    objLog.Description__c       =   'Message==>'+err.getMessage() +'---Field resulting in error ---'+ err.getFields();
                    objLog.Type__c              =   'Milestone Penalty Failed to Update';
                    objLog.sObject_Record_Id__c =   penaltyMilestoenRec.getId();
                    system.debug('objLog------'+objLog);
                    logList.add(objLog);
                }
            }
        }
        if(logList != null && logList.size() > 0){
            Database.insert(logList);
        }
    }
}