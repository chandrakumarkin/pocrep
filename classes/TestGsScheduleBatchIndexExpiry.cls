@isTest
private class TestGsScheduleBatchIndexExpiry {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        //objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Index_Card_Status__c = 'Expired';
        
        insert objAccount;
        
        Contact objContact = new Contact();
        objContact.LastName = 'Last RB';
        objContact.Email = 'test@difcportal.com';
        objContact.AccountId = objAccount.Id;
        objContact.FirstName = 'Nagaboina';
        insert objContact;
         
        Contact objGSContact = new Contact();
        objGSContact.LastName = 'Last RB';
        objGSContact.Email = 'test@difcportal.com';
        objGSContact.AccountId = objAccount.Id;
        objGSContact.FirstName = 'Nagaboina';
        objGSContact.RecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objGSContact;
        
         Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Contractor Community User Custom' limit 1];  //Customer Community Login User Custom
       
        User u                         = new User(Alias = 'standt', Email='tesclasst@difc.com', 
        EmailEncodingKey               ='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
        LocaleSidKey                   ='en_US', ProfileId = p.Id, 
        TimeZoneSidKey                 ='America/Los_Angeles', UserName='tesclasstuat@difc.com');
        u.Community_User_Role__c       = 'Admin;Company Services Approver;Employee Services';    
        u.IsActive                     = true;
        u.contactID                    = objContact.id; 
        insert u;
        
        
        AccountContactRelation acctcr1 = new AccountContactRelation(Id= [SELECT Id FROM AccountContactRelation WHERE AccountId =: objAccount.Id LIMIT 1].Id,AccountId = objAccount.id, ContactId = objContact.id, Roles = 'Employee Services');
        update acctcr1;
       
        
        Document_Details__c objDD = new Document_Details__c();
        objDD.Contact__c = objGSContact.Id;
        objDD.EXPIRY_DATE__c = system.today();
        objDD.Document_Type__c = 'Employment Visa';
        insert objDD;
        
    Identification__c objId = new Identification__c();
        objId.Id_No__c = '2/6/1234567';
        objId.Valid_From__c = system.today().addYears(-1);
        objId.Valid_To__c = system.today().addMonths(-2);
        objId.Identification_Type__c = 'Index Card';
        objId.Account__c = objAccount.Id;
         insert objId;
    
        test.startTest();
        
         database.executeBatch(new GsScheduleBatchIndexExpiryNotifications() , 10);
        
        test.stopTest();
        
    }
    
   
}