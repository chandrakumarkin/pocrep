@IsTest(SeeAllData=false)
private class CarParkingCustomNotification_Test{
      
       @IsTest(SeeAllData=true) 
      private static void testReportNotification() {
         
        Report report = [SELECT Id FROM Report limit 1];
        Test.startTest();
        
        Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
         
       SR_Template__c srTemp = new SR_Template__c ();
       srTemp.Name = 'Car Parking Process';
       srTemp.SR_RecordType_API_Name__c = 'Car Parking';
       insert srTemp;
       
       srTemp = new SR_Template__c ();
       srTemp.Name = 'Daily Parking Collection';
       srTemp.SR_RecordType_API_Name__c = 'Daily Parking Collection';
       insert srTemp;
       
       Service_Request__c sr = new Service_Request__c();
       sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
       sr.Customer__c = acc.ID;
       sr.Contact__c = objContact.ID;
      // sr.Client_s_Representative_in_Charge__c = 'New Vehicle Registration';
      // insert sr;
       
      /* SR_Status__c srStatus1 = new SR_Status__c ();
       srStatus1 .Code__c = 'Submitted';
       srStatus1 .Name = 'Submitted';
       insert srStatus1 ;*/
       
      // sr.External_SR_Status__c = srStatus1 .ID;
      // sr.Internal_SR_Status__c = srStatus1 .ID;
     /*  sr.Submitted_Date__c = Date.Today();
       sr.Submitted_DateTime__c = DateTime.now();
       sr.Dedicated_Cable_Quantity__c = 1;
       update sr;*/
       
      /* SR_PRICE_ITEM__C srPrice = new SR_PRICE_ITEM__C ();
       srPrice.Status__c  = 'Consumed';
       srPrice.ServiceRequest__c = sr.ID;
       srPrice.Price__c= 500;
       insert srPrice;*/
       
       Document_Master__c  docMaster = new Document_Master__c ();
       docMaster.Name = 'Daily Car Parking Collection Document';
       insert docMaster;
       
       
     
       
        Reports.ReportInstance reportInstance = Reports.ReportManager.runAsyncReport(report.Id, true);
        
        Test.stopTest();
        
        CarParking_ReportNotification objCustomNotify = new CarParking_ReportNotification();     
        Reports.NotificationActionContext context = 
            new Reports.NotificationActionContext(
                reportInstance, 
                new Reports.ThresholdInformation( 
                    new List<Reports.EvaluatedCondition> {new Reports.EvaluatedCondition('RecordCount', 
                               'Record Count', Double.valueOf(0), Double.valueOf(1), '0!T','0!T',
                                Reports.EvaluatedConditionOperator.GREATER_THAN) }));
        objCustomNotify .execute(context);
        
     
     
     }
        
}