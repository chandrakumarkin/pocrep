/**********************************************************
*Apex class :RORP Page Notification - RORPPageNotificationTest
*Description : test class for RORPPageNotification
*Created on: 09-Jul-2019    Selva   #7099
***********************************************************/
@isTest(seealldata=false)
public class RORPPageNotificationTest {

    static testMethod void method1()
    {
        
         Schema.DescribeSObjectResult Amnd = Schema.SObjectType.Amendment__c; 
            Map<String,Schema.RecordTypeInfo> rtMapByName = Amnd.getRecordTypeInfosByName();
            
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Freehold_Transfer_Registration','Individual_Buyer','RORP_Shareholder')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Building__c objBuilding = new Building__c();
        objBuilding.Name = 'RORP The Gate Building';
        objBuilding.Building_No__c = '000001';
        objBuilding.Company_Code__c = '5300';
        objBuilding.Permit_Date__c = system.today().addMonths(-1);
        objBuilding.Building_Permit_Note__c = 'test class data';
        insert objBuilding;
        
        Account objAcc = new Account();
        objAcc.Name = 'Test Account';
        objAcc.Phone = '1234567890';
        objAcc.Sector_Classification__c = 'Authorized Firm';
        insert objAcc;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        objAccount.Sector_Classification__c = 'Investment Fund';
        objAccount.Hosting_Company__c = objAcc.Id;
        insert objAccount;
        
        SR_Status__c srStatus = new SR_Status__c ();
        srStatus.name  = 'Submitted';
        srStatus.Code__c = 'Submitted';
        insert srStatus;
        
         list<Unit__c> lstUnits = new list<Unit__c>();
        Unit__c objUnit;
        objUnit = new  Unit__c();
        objUnit.Name = '1234';
        objUnit.Building__c = objBuilding.Id;
        objUnit.Building_Name__c = 'Gate Building';
        objUnit.Floor__c = '1';
        objUnit.Floor_Description__c = '1st Floor';
        objUnit.Unit_Square_Feet__c = 12345;
        objUnit.Unit_Usage_Type__c = 'Commercial';
        objUnit.Is_official_Search__c=true;
        lstUnits.add(objUnit);
        
        insert lstUnits;
        
        Service_Request__c objSR3= new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Date_of_Declaration__c = Date.today();
        objSR3.Email__c = 'test@test.com';
        objSR3.RecordTypeId = mapRecordType.get('Freehold_Transfer_Registration');    
        objSR3.Internal_SR_Status__c = srStatus.Id;
        objSR3.Express_Service__c = false;
        objSR3.Service_Category__c = 'New';
        objSR3.Building__c = objBuilding.Id;
        objSR3.Unit__c = lstUnits[0].Id;
        objSR3.Type_of_Lease__c = 'Lease';
        objSR3.Rental_Amount__c = 1234;
        insert objSR3;  
        
        Amendment__c objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = objSR3.Id;
        objAmd.Relationship_Type__c='Director';
        objAmd.Amendment_Type__c='Shareholder';
       // objAmd.RecordTypeId = '01220000000QysF';  
        objAmd.Passport_No__c = 'NNE1212';
        objAmd.Nationality_list__c ='India';
        objAmd.Place_of_Issue__c='India';
        system.debug('!!@@##@'+objAmd);
        INSERT objAmd;
        
        test.startTest();
            Apexpages.currentPage().getParameters().put('id', objSR3.id);
            Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(objSR3);
            RORPPageNotification obj = new RORPPageNotification(con);           
        test.stopTest();
    }
}