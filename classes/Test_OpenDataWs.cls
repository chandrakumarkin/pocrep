@isTest(seeAllData=true)
private class Test_OpenDataWs
{

 static testmethod void newDependentMultipleVisa1() 
 {
        test.startTest();
        
        Account Obj=[select id from account where ROC_Status__c='Active' and Legal_Type_of_Entity__c='LTD' limit 1];
        
        OpenDataWs.OpenDataWsDataResult(Obj.id);

        
  }
  
     static testmethod void CompanyDetailsTest()
    {
     
            Account Obj=[select id from account where ROC_Status__c='Active' and Legal_Type_of_Entity__c='LTD' limit 1];
            TEST.startTest();
            RestContext.request = new RestRequest();
            RestContext.request.params.put('accountId',Obj.Id);
            CLS_DIFC_CompanyDetails.jsonResponse();
            TEST.stopTest();
    }

     static testmethod void CompanySearch_Test()
    {
     
            
            TEST.startTest();
            RestContext.request = new RestRequest();
            RestContext.request.params.put('q_search','Mashreq');
            CLS_DIFC_CompanySearch.jsonResponse();
            TEST.stopTest();
    }
    

}