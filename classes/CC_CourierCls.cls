/*************************************************************************************************
 *  Name        : CC_CourierCls
 *  Author    : Kaavya Raghuram
 *  Company  : NSI JLT
 *  Date        : 2015-06-01     
 *  Purpose  : This class is to call the Courier webservice 
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By  Description
V1.0    08/10/2015  Ravi        Added the logic to support this class for GS Courier as well
V1.1    08/03/2016  Ravi        To update the SR for Close Shipments
V1.2    06/06/2016  Ravi        we need you to send the "name of Applicant' to Aramex in the description filed as per #1938
V1.3    04/10/2016  Sravan      Added code to decrypt SAP Password as per Tkt # 3392
V1.4    07/02/2018  Shabbir     Modified for ROC Courier
V1.5    21/01/2019  Azfer       Modified for Domestic Helper
V1.6    09/01/2020  Shoaib      Modified as per comment #7536
v1.7    29/03/2020  Arun        Phone number no there pass mobile no
v1.8    13/09/2020  Mudasir     #DIFC2-11100 - SAP PR Number and SR menu text to be added in the 'courier description'
v1.9    25/07/2021  Veera       New Born Baby added for courier description  
v2.0    29/07/2021  Veera       Separate the transaction of Courier Info push to SAP 
v2.1    08/08/2021  Veera       Added code Courier Delivery created from Pendings
v2.2    15/08/21021 Veera       Truncated courier description if more than 255 characters 
------------------------------------------------- --------------------------------------------------------------------         
**************************************************************************************************/
global without sharing class CC_CourierCls {

    Private static string  decryptedSAPPassWrd = test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(WebService_Details__c.getAll().get('Credentials').Password__c) :'123456'; // V1.3

    @Future(callout=true)
    public static void CreateShipment(Id StepId){
        CreateShipmentNormal(StepId);
    }
    
    public static void CreateShipmentNormal(Id StepId){
        //Service_Request__c objSR = new Service_Request__c();
        //String PkGUID;
        //String PkID;
        //Boolean flagPK;
        //Pickup__c SFPickup = new Pickup__c();
        
        Boolean flagSH = false;
        String SFPkId;
        String ErrorMsg='Success';
        Step__c objStep;// = new Step__c();
        
        if(StepId!=null){
            for(Step__c objSt : [select Id,Name,SR__c,SR__r.Name,SR_Group__c,SR__r.Courier_Cell_Phone__c,SR__r.Courier_Mobile_Number__c,SR__r.Consignee_FName__c,SR__r.Consignee_LName__c,
            SR__r.Avail_Courier_Services__c,SR__r.Would_you_like_to_opt_our_free_couri__c,SR__r.Send_SMS_To_Mobile__c,SR__r.Apt_or_Villa_No__c,SR__r.Customer__r.Office__c,SR__r.Customer__r.Building_Name__c,SR__r.Customer__r.Name,
            SR__r.Email__c,SR__r.Customer__c,SR__r.Use_Registered_Address__c,SR__r.Express_Service__c,Step_Template__r.Courier_Type__c,
            Step_Name__c,Status_Code__c,Pending__c,Pending__r.Start_Date__c,Pending__r.End_Date__c,Pending__r.SAP_PSUSR__c,
            SAP_DOCDL__c,SAP_DOCRC__c,SAP_DOCCO__c,SAP_DOCCR__c,SAP_CTOTL__c,SAP_CTEXT__c,Parent_Step__c,
            Is_Letter__c,Parent_Step__r.Is_Letter__c,Parent_Step__r.Step_Name__c,Parent_Step__r.Status_Code__c,
            SR__r.Customer__r.Registration_License_No__c,SR__r.Service_Type__c,SR_Record_Type__c,
            //V1.2
            SR__r.Contact__r.Name,SR__r.First_Name__c,SR__r.Last_Name__c,
            //v1.3
            SR__r.Domestic_Helper__c,SR__R.Type_of_Request__c,
            SR__r.SAP_PR_No__c,SR__r.SR_Menu_Text__c,(select id,Type_Of_Courier__c from Pendings__r where Is_Courier_Related__c = true and End_Date__c =null)//v1.8 ,v2.1
            from Step__c where Id=:StepId AND Id NOT IN (select Step__c from Shipment__c where Step__c =:StepId)]){
                objStep = objSt;
            }
        }
        if(objStep!=null){
            ShipmentInfo objInfo = PrepareShipment(objStep);
           
            system.debug(objInfo);
            if(objInfo.Shipments != null && !objInfo.Shipments.isEmpty())
                insert objInfo.Shipments;
            if(objInfo.Steps != null && !objInfo.Steps.isEmpty())
                update objInfo.Steps;
            if(objInfo.Logs != null && !objInfo.Logs.isEmpty())
                insert objInfo.Logs;
                
                 if((objStep.SR_Group__c == 'GS' && objStep.Parent_Step__r.Step_Name__c != 'Front Desk Review' && objStep.Parent_Step__r.Status_Code__c != 'AWAITING_ORIGINALS' && objStep.Parent_Step__r.Is_Letter__c == false && objStep.SR_Record_Type__c != 'PO_BOX' && objStep.SR_Record_Type__c != 'Add_a_New_Service_on_the_Index_Card') || Test.isRunningTest() ){
                //send the Change to SAP
                 //PushShipmentToSAP(objInfo,'Change');
                 
                 System.enqueueJob(new PushShipmentToSAP_QC(JSON.serialize(objInfo),'Change')); //v2.0 
            }
        }
    }   
    
    public static ShipmentInfo PushShipmentToSAP(ShipmentInfo objShipmentInfo,string Action){
        //Step__c objStep;
        if(objShipmentInfo.Steps != null && objShipmentInfo.Steps.isEmpty() == false){
            Step__c objCStp = objShipmentInfo.Steps[0];
            Shipment__c objShip = (objShipmentInfo.Shipments != null && objShipmentInfo.Shipments.isEmpty() == false) ? objShipmentInfo.Shipments[0] : new Shipment__c();
            SAPGSWebServices.ZSF_S_GS_ACTIVITY item;
            list<SAPGSWebServices.ZSF_S_GS_ACTIVITY> items = new list<SAPGSWebServices.ZSF_S_GS_ACTIVITY>(); 
            map<string,string> mapStepSRIds = new map<string,string>();
            //Service_Request__c.LastModifiedBy
            
            //V1.1
            string SRStatusId;
            
            for(Step__c obj : [select Id,Name,SAP_Seq__c,Step_Status__c,OwnerId,Owner.Name,LastModifiedBy.SAP_User_Id__c,Biometrics_Required__c,
                        SAP_AMAIL__c,SAP_BANFN__c,SAP_BIOMT__c,SAP_BNFPO__c,SAP_CMAIL__c,SAP_CRMAC__c,SAP_EACTV__c,SAP_EMAIL__c,SAP_ERDAT__c,SAP_ERNAM__c,
                        SAP_ERTIM__c,SAP_ETIME__c,SAP_FINES__c,SAP_FMAIL__c,SAP_GSDAT__c,SAP_GSTIM__c,SAP_IDDES__c,SAP_IDNUM__c,
                        SAP_IDTYP__c,SAP_MDATE__c,SAP_MEDIC__c,SAP_MEDRT__c,SAP_MEDST__c,SAP_MFDAT__c,SAP_MFSTM__c,SAP_NMAIL__c,SAP_POSNR__c,SAP_RESCH__c,
                        SAP_RMATN__c,SAP_STIME__c,SAP_TMAIL__c,SAP_UNQNO__c,SAP_ACRES__c,ID_End_Date__c,ID_Start_Date__c,SAP_MATNR__c,
                        SR__c,SR__r.SAP_Unique_No__c,SR__r.SAP_PR_No__c,SR__r.SAP_SGUID__c,SR__r.SAP_MATNR__c,SR__r.Is_Pending__c,
                        SR__r.SAP_UMATN__c,SR__r.SAP_OSGUID__c,SR__r.Name,DNRD_Receipt_No__c,SAP_OSGUID__c,SAP_UMATN__c,
                        Step_Template__c,Step_Template__r.Name,Step_Id__c,No_PR__c,SAP_DOCDL__c,SAP_DOCRC__c,SAP_DOCCO__c,SAP_DOCCR__c,SAP_CTOTL__c,SAP_CTEXT__c,
                        Step_Template__r.SR_Status__c
                        //ARBNO,CNSNM,SHPNM,CCRDT,CCRTM,CDLDT,CDLTM,CSTAT
                        from Step__c where Id =:objCStp.Parent_Step__c AND SR__r.SAP_Unique_No__c != null ]){
                item = GsSapWebServiceDetails.PrepareActivity(obj);
                item.ACTIN = (Action == 'Change') ?  'CHG' : 'COM';
                item.ARBNO = objShip.Airway_Bill_No__c;
                item.CNSNM = objShip.Consignee_Name__c.length()>80? objShip.Consignee_Name__c.substring(0,80) : objShip.Consignee_Name__c;//Consignee Name v2.0 
                item.SHPNM = objShip.Shipper_Name__c.length()>80? objShip.Shipper_Name__c.substring(0,80) : objShip.Shipper_Name__c;//Shipper Name v2.0 
                //if(objShip.Status__c == 'Ready'){
                    item.CCRDT = GsPendingInfo.SAPDateFormat(objShip.Created_On__c);//Courier Creation Date in Aramex
                    item.CCRTM = GsPendingInfo.SAPTimeFormat(objShip.Created_On__c);//Courier Creation Time in Aramex
                //}
                if(objShip.Status__c == 'Delivered'){
                    item.CDLDT = GsPendingInfo.SAPDateFormat(objShip.Courier_Status_Updated_on__c);//Courier Delivery Date
                    item.CDLTM = GsPendingInfo.SAPTimeFormat(objShip.Courier_Status_Updated_on__c);//Courier Delivery Time
                }   
                if(Action == 'Close'){
                    item.CDATE = GsPendingInfo.SAPDateFormat(system.now()); // Step Close Date
                    item.CTIME = GsPendingInfo.SAPTimeFormat(system.now()); // Step Close Time
                }
                item.CSTAT = objShip.Status__c;//Courier Status
                //if(obj.SR__r.Is_Pending__c)
                    //item.PCFLG = 'X';//Courier Flag - if any Pending
                if(obj.No_PR__c)
                    item.PRDEL = 'X';
                if(obj.DNRD_Receipt_No__c != null)
                    item.PRMRK = obj.DNRD_Receipt_No__c;
                system.debug('objCStp.Pending__c is : '+objCStp.Pending__c);
                if(objCStp.Pending__c != null || test.isRunningTest()){
                    if(objShip.Id != null){
                        item.DOCCO = objShip.Step__r.SAP_DOCCO__c ;
                        item.DOCCR = objShip.Step__r.SAP_DOCCR__c;
                        item.DOCDL = objShip.Step__r.SAP_DOCDL__c;
                        item.DOCRC = objShip.Step__r.SAP_DOCRC__c;
                        item.CTOTL = objShip.Step__r.SAP_CTOTL__c;
                        item.CTEXT = objShip.Step__r.SAP_CTEXT__c;
                        if(objShip.Step__r.Pending__r.Start_Date__c != null)
                            item.PSDAT = GsPendingInfo.SAPDateFormat(objShip.Step__r.Pending__r.Start_Date__c);
                        if(objShip.Step__r.Pending__r.Start_Date__c != null)
                            item.PSTIM = GsPendingInfo.SAPTimeFormat(objShip.Step__r.Pending__r.Start_Date__c);
                        if(objShip.Step__r.Pending__r.SAP_PSUSR__c != null)
                            item.PSUSR = objShip.Step__r.Pending__r.SAP_PSUSR__c;
                    }else{
                        item.DOCCO = objCStp.SAP_DOCCO__c;
                        item.DOCCR = objCStp.SAP_DOCCR__c;
                        item.DOCDL = objCStp.SAP_DOCDL__c;
                        item.DOCRC = objCStp.SAP_DOCRC__c;
                        item.CTOTL = objCStp.SAP_CTOTL__c;
                        item.CTEXT = objCStp.SAP_CTEXT__c;
                        item.PSDAT = GsPendingInfo.SAPDateFormat(objCStp.Pending__r.Start_Date__c);
                        item.PSTIM = GsPendingInfo.SAPTimeFormat(objCStp.Pending__r.Start_Date__c);
                        item.PSUSR = objCStp.Pending__r.SAP_PSUSR__c;
                    }
                    system.debug('item.PSTIM is : '+item.PSTIM);
                    system.debug('item.PSUSR is : '+item.PSUSR);
                }else{
                    item.DOCCO = obj.SAP_DOCCO__c ;
                    item.DOCCR = obj.SAP_DOCCR__c;
                    item.DOCDL = obj.SAP_DOCDL__c;
                    item.DOCRC = obj.SAP_DOCRC__c;
                    item.CTOTL = obj.SAP_CTOTL__c;
                    item.CTEXT = obj.SAP_CTEXT__c;
                }
                item.AENAM = 'ARAMEX';
                items.add(item);
                mapStepSRIds.put(obj.SR__r.SAP_Unique_No__c,obj.SR__c);
                SRStatusId = obj.Step_Template__r.SR_Status__c;
            }
            if(!items.isEmpty()){
                SAPGSWebServices.ZSF_TT_GS_ACTIVITY objActivityData = new SAPGSWebServices.ZSF_TT_GS_ACTIVITY();
                objActivityData.item = items;
                
                SAPGSWebServices.serv_actv objSFData = new SAPGSWebServices.serv_actv();        
                objSFData.timeout_x = 120000;
                objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
                objSFData.inputHttpHeaders_x= new Map<String, String>();
            //    Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+WebService_Details__c.getAll().get('Credentials').Password__c);  Commented for V1.3
                  Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+CC_CourierCls.decryptedSAPPassWrd); // Added for V1.3
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
                
                system.debug('request $$$ : '+objActivityData);
                
                SAPGSWebServices.Z_SF_GS_ACTIVITYResponse_element response = objSFData.Z_SF_GS_ACTIVITY(objActivityData,null);
                
                objShipmentInfo.Logs = (objShipmentInfo.Logs != null) ? objShipmentInfo.Logs : new list<Log__c>();
                
                system.debug('response $$$ : '+response.EX_GS_SERV_OUT.item);
                
                if(response != null && response.EX_GS_SERV_OUT != null && response.EX_GS_SERV_OUT.item != null){
                    string StatusId;
                    if(Action=='Close'){
                        for(Status__c objstat:[Select id,Name,Type__c,Rejection__c,SR_Closed_Status__c,Code__c from Status__c where Name ='Closed']){
                            StatusId = objstat.Id;
                        }
                    }
                    objShipmentInfo.lstSRs = new list<Service_Request__c>();
                    for(SAPGSWebServices.ZSF_S_GS_SERV_OP objStepRes : response.EX_GS_SERV_OUT.item){
                        if(objStepRes.ACCTP == 'P' && Action=='Close'){
                            Step__c objStp = new Step__c(Id=objCStp.Parent_Step__c);
                            objStp.Push_to_SAP__c = true;
                            objStp.Status__c = StatusId;
                            objStp.Courier_Enabled__c = true;
                            objShipmentInfo.Steps.add(objStp);
                            system.debug('SRStatusId is : '+SRStatusId);
                            if(SRStatusId != null){
                                Service_Request__c objSR = new Service_Request__c(Id=objCStp.SR__c);
                                objSR.External_SR_Status__c = SRStatusId;
                                objSR.Internal_SR_Status__c = SRStatusId;
                                objShipmentInfo.lstSRs.add(objSR);
                            }else{
                                objShipmentInfo.Logs.add(new Log__c(Type__c = 'Courier Status change ',Description__c = 'SR Status came as blank from step tepmlate for step Id : '+objCStp.Parent_Step__c));
                            }
                        }else if(objStepRes.ACCTP != 'P'){
                            Log__c objLog = new Log__c();
                            objLog.Type__c = 'Courier Push to SAP '+objStepRes.SGUID;
                            objLog.Description__c = objStepRes.SFMSG;
                            objShipmentInfo.Logs.add(objLog);
                        }
                    }
                }
                if(response != null && response.EX_GS_NEXT_ACT != null && response.EX_GS_NEXT_ACT.item != null){
                    list<Step__c> lstSteps = GsSapWebServiceDetails.ProcessSteps(response.EX_GS_NEXT_ACT.item, null, mapStepSRIds);
                    if(lstSteps != null && !lstSteps.isEmpty()){
                        //upsert lstSteps Step_Id__c;//insert lstSteps;
                        objShipmentInfo.StepsToInsert = lstSteps;
                    }
                }
            }
        }
        return objShipmentInfo;
    }
    
    public static ShipmentInfo PrepareShipment(Step__c objStep){
        
        system.debug(objStep.pending__R + 'active Pending');
        ShipmentInfo objInfo = new ShipmentInfo();
        objInfo.Logs = new list<Log__c>();
        objInfo.Shipments = new list<Shipment__c>();
        objInfo.Steps = new list<Step__c>();
        
        //for(Service_Request__c obj : [select Id,Express_Service__c,Customer__c,Email__c,Send_SMS_To_Mobile__c,Customer__r.Name,Nature_of_business__c,Nature_of_Business_Arabic__c from Service_Request__c where Id=:objStep.SR__c])
            //objSR = obj;
    
        //Getting custom setting for Client Info
        list<Courier_Client_Info__c> CCInfo = Courier_Client_Info__c.getall().values();
        system.debug('Courier_Client_Info===>>  '+CCInfo);
        
        WS_Courier.Service_1_0 CourierService= new WS_Courier.Service_1_0 ();
        WS_Courier.Contact DIFCCon = new WS_Courier.Contact();
        WS_Courier.ClientInfo ClientInfo = new WS_Courier.ClientInfo();
        WS_Courier.Address DIFCAddress= new WS_Courier.Address();
        
        if(CCInfo.size()>0 && CCInfo!=null){
            ClientInfo.UserName=CCInfo[0].Username__c;
           // ClientInfo.Password = CCInfo[0].Password__c; //Commented for V1.3
             ClientInfo.Password =  test.isrunningTest()==false ? PasswordCryptoGraphy.DecryptPassword(CCInfo[0].Password__c) :CCInfo[0].Password__c;  // V1.3
            ClientInfo.Version = CCInfo[0].Version_No__c;
            ClientInfo.AccountNumber = CCInfo[0].Account_No__c;
            ClientInfo.AccountPin = CCInfo[0].Account_Pin__c;
            ClientInfo.AccountEntity = CCInfo[0].Account_Entity__c;
            ClientInfo.AccountCountryCode = CCInfo[0].Account_Country_Code__c;
            
            //DIFC Contact Info
            DIFCCon.PersonName=CCInfo[0].Person_Name__c;
            DIFCCon.CompanyName=CCInfo[0].Company_Name__c; // Diffrenciate GS & RoC
            DIFCCon.PhoneNumber1=CCInfo[0].Phone_Number__c;
            DIFCCon.CellPhone= CCInfo[0].Cell_Phone__c;
            DIFCCon.EmailAddress=CCInfo[0].Email_Address__c;
            
            //DIFC Address Info
            DIFCAddress.Line1=CCInfo[0].Address_Line1__c;
            DIFCAddress.Line2=CCInfo[0].Address_Line2__c;
            DIFCAddress.City=CCInfo[0].City__c;
            DIFCAddress.CountryCode=CCInfo[0].Country_Code__c;
            DIFCAddress.StateOrProvinceCode=CCInfo[0].State_Province_Code__c;
        }
        
        WS_Courier.Transaction_x Transaction_x = new WS_Courier.Transaction_x ();
        Transaction_x.Reference1=objStep.Name;
        Transaction_x.Reference2=objStep.SR__r.Name;
        Transaction_x.Reference3=objStep.Id;
        Transaction_x.Reference4=objStep.SR__c;   
        
        
        WS_Courier.Contact CLientCon= new WS_Courier.Contact();
        CLientCon.PersonName=objStep.SR__r.Consignee_FName__c+' '+objStep.SR__r.Consignee_LName__c;
        CLientCon.CompanyName=objStep.SR__r.Customer__r.Name; 
        CLientCon.PhoneNumber1=objStep.SR__r.Courier_Mobile_Number__c; 
        
        CLientCon.CellPhone=objStep.SR__r.Courier_Cell_Phone__c!=null? objStep.SR__r.Courier_Cell_Phone__c : objStep.SR__r.Courier_Mobile_Number__c; // v1.7
        
        CLientCon.EmailAddress=objStep.SR__r.Email__c;
        
        WS_Courier.Address ClientAddress = new WS_Courier.Address();
        if(objStep.SR__r.Use_Registered_Address__c){ // || objStep.SR_Group__c=='ROC'
            ClientAddress.Line1=objStep.SR__r.Customer__r.Office__c+' '+objStep.SR__r.Customer__r.Building_Name__c+' DIFC Dubai';
        }else{
            ClientAddress.Line1=objStep.SR__r.Apt_or_Villa_No__c;
        }
        ClientAddress.City='Dubai';
        ClientAddress.StateOrProvinceCode='Dubai';
        ClientAddress.CountryCode='AE';
        //ClientAddress.PostCode='676123';
        
        WS_Courier.Party DIFCParty= new WS_Courier.Party();
        DIFCParty.Reference1=objStep.SR__r.Express_Service__c ? 'VIP' : 'Normal';
        DIFCParty.Reference2=objStep.SR__r.Name;
        DIFCParty.AccountNumber=CCInfo[0].Account_No__c;
        DIFCParty.PartyAddress=DIFCAddress;
        DIFCParty.Contact=DIFCCon;
        
        WS_Courier.Party ClientParty= new WS_Courier.Party();
        ClientParty.Reference1=objStep.Step_Template__r.Courier_Type__c;
        ClientParty.Reference2=objStep.SR__r.Customer__c;
        ClientParty.AccountNumber=CCInfo[0].Account_No__c;
        ClientParty.PartyAddress=ClientAddress;
        ClientParty.Contact=ClientCon;
        
        //Shipment Weight
        WS_Courier.Weight ActualWeight = new WS_Courier.Weight();
        ActualWeight.Value=0.5;
        ActualWeight.Unit='KG';
        WS_Courier.Weight ChargeableWeight = new WS_Courier.Weight();
        ChargeableWeight.Value=0.5;
        ChargeableWeight.Unit='KG';    
        
        WS_Courier.ShipmentDetails Details= new WS_Courier.ShipmentDetails();
        Details.ActualWeight=ActualWeight;
        Details.ChargeableWeight=ChargeableWeight;
        Details.GoodsOriginCountry ='IN';
        
        if(objStep.SR_Group__c == 'GS' && objStep.SAP_CTOTL__c != null)
            Details.NumberOfPieces = Integer.valueOf(objStep.SAP_CTOTL__c);
        else
            Details.NumberOfPieces =1;
        Details.NumberOfPieces = Details.NumberOfPieces == 0 ? 1 : Details.NumberOfPieces; // If Pending is raised in SAP, No of Docs will be Zero(0)
        //V1.2 - added the applicant namem in the description
        string sAppName = '';
        if(objStep.SR_Group__c == 'GS'){
            if(objStep.SR__r.Contact__r.Name != null){
                sAppName = objStep.SR__r.Contact__r.Name+' ';
            }else if(objStep.SR__r.First_Name__c != null && objStep.SR__r.Last_Name__c != null){
                sAppName = objStep.SR__r.First_Name__c+' '+objStep.SR__r.Last_Name__c+' ';
            }
        }
        //Following Code is added in V1.5
        String DomesticHelper = '';
        if( objStep.SR__r.Domestic_Helper__c ){
            DomesticHelper = ' - Domestic Helper';
        }
        // changes end
        
       // v1.9  --start
        string newBornbaby ='';
        if(objStep.SR__R.Type_of_Request__c =='New Born Baby') newBornbaby ='-New Born Baby';
        
       // v1.9 --end

        // added the vriable in the below lines to update the description of good accordingly
        // V1.5        
        if(objStep.SR_Group__c == 'GS' && objStep.SAP_CTEXT__c != null){
            Details.DescriptionOfGoods = objStep.SR__r.Name+' - '+objStep.SAP_CTEXT__c+DomesticHelper+newBornbaby+'-'+sAppName;
        }else if(objStep.SR_Group__c != 'GS' && objStep.SR__r.Customer__r.Name != null && objStep.SR__r.Customer__r.Registration_License_No__c != null && objStep.SR__r.Service_Type__c != null)
            Details.DescriptionOfGoods = objStep.SR__r.Name+' - '+objStep.SR__r.Customer__r.Name +' - '+ objStep.SR__r.Customer__r.Registration_License_No__c +' - '+ objStep.SR__r.Service_Type__c + DomesticHelper + newBornbaby ;
        else if(objStep.SR__r.Service_Type__c != null && objStep.SR__r.Customer__r.Name != null)
            Details.DescriptionOfGoods = objStep.SR__r.Name+' - '+objStep.SR__r.Customer__r.Name +' - '+ objStep.SR__r.Service_Type__c+DomesticHelper+newBornbaby+'-'+sAppName;
        else
            Details.DescriptionOfGoods = objStep.SR__r.Name+' - '+objStep.SR__r.Customer__r.Name +' - Corier Docs'+DomesticHelper+newBornbaby+'-'+sAppName;        
        //v1.8 - #DIFC2-11100 - SAP PR Number and SR menu text to be added in the 'courier description'
        if(objStep.SR__r.SAP_PR_No__c != NULL){
                Details.DescriptionOfGoods =  Details.DescriptionOfGoods +'-'+objStep.SR__r.SAP_PR_No__c;
        }
        if(objStep.SR__r.SR_Menu_Text__c != NULL){
            Details.DescriptionOfGoods =   Details.DescriptionOfGoods +'-'+objStep.SR__r.SR_Menu_Text__c;
        }
        Details.ProductGroup ='DOM';
        Details.ProductType ='OND'; 
        Details.PaymentType='P';
        //Details.PaymentOptions;
        //Details.Dimensions Dimensions;
        //Details.CustomsValueAmount;
        //Details.CashOnDeliveryAmount;
        //Details.InsuranceAmount;
        //Details.CashAdditionalAmount;
        //Details.CashAdditionalAmountDescription;
        //Details.CollectAmount;
        //Details.Services;
        //Details.Items;
        if(objStep.SAP_DOCRC__c == 'X' || objStep.SAP_DOCCR__c == 'X')//only for GS - at least for now
            Details.Services = 'RTRN';
        
        //Shipment Contact details based on Delivery or Collection
        WS_Courier.Shipment Shipment= new WS_Courier.Shipment();
        Shipment.Reference1=objStep.SR__c;
        Shipment.Reference2=objStep.Id;
        
        //Get from the User or DIFC
        if(objStep.Step_Template__r.Courier_Type__c=='Delivery'){
            Shipment.Shipper = DIFCParty;
            Shipment.Consignee = ClientParty;
            Shipment.PickupLocation = objStep.SR_Group__c == 'GS' ? 'DIFC - GSO Services' : 'DIFC - Registry Services';
        }else{
            Shipment.Shipper = ClientParty;
            Shipment.Consignee = DIFCParty;
            Shipment.PickupLocation='Client Address';
        }
        
        Shipment.ShippingDateTime = system.now().addMinutes(5);
        
        
        Shipment.Details= Details;
        Shipment.ForeignHAWB=objStep.Id;
        Shipment.TransportType_x0020_x =0;
        //Shipment.DueDate;
        //Shipment.Comments;
        //Shipment.OperationsInstructions;
        //Shipment.AccountingInstrcutions;
        
        WS_Courier.Shipment[] ShipmentArray = new WS_Courier.Shipment[]{Shipment};
        WS_Courier.ArrayOfShipment Shipments= new WS_Courier.ArrayOfShipment();
        Shipments.Shipment=ShipmentArray;
        
        WS_Courier.LabelInfo LabelInfo = new WS_Courier.LabelInfo();
        CourierService.timeout_x = 120000;
        
        /*
        list<Pickup__c> pklist=[Select Id,Name,Pickup_ID__c,Pickup_GUID__c,Status__c,Closed_Date__c,Last_Pickup_Time__c,Pickup_Date__c,Closed_DateTime__c,Ready_Date__c from Pickup__c where Status__c='Ready' AND Closed_Date__c=TODAY AND Closed_DateTime__c>:system.now()];
        if(pklist.size()>0 && pklist!=null){
            //Assigning existing Pickup GUID for the shipment
            PkGUID = pklist[0].Pickup_GUID__c;
            PkID = pklist[0].Pickup_ID__c;
            SFPkId = pklist[0].Id;
        }else{
            Decimal hrs = system.now().hour();
            Decimal[] hr = new Decimal[]{};
            for(Courier_Timings__c CT:[Select Name__c,Hours__c from Courier_Timings__c where Hours__c>:hrs order by Hours__c]){
                 hr.add(CT.Hours__c);
            }
            Integer PKHrs;
            if(hr.size()==1)
                PKHrs = hr[0].intValue();
            else if(hr.size()==2)
                PKHrs = math.min(hr[0].intValue(),hr[1].intValue());
            else
                PKHrs = hr[0].intValue();
            system.debug('PKHrs =====>>   '+PKHrs);
            //Creating the Pickup for the Shipments
            //Pickup Weight
            WS_Courier.Weight PKShipmentWeight = new WS_Courier.Weight();
            PKShipmentWeight.Unit = 'KG';
            PKShipmentWeight.Value = 4;//depends on No. of Shipments attached
            
            //Pickup Volume
            WS_Courier.Volume PKShipmentVolume = new WS_Courier.Volume();
            PKShipmentVolume.Unit = 'Cm3';
            PKShipmentVolume.Value=20;
            
            //Pickup Money
            //WS_Courier.Money CashAmount = new WS_Courier.Money();
            //WS_Courier.Money ExtraCharges = new WS_Courier.Money();
            //Pickup Shipment dimensions
            //WS_Courier.Dimensions ShipmentDimensions = new WS_Courier.Dimensions();
            
            //Pickup Items detail
            WS_Courier.PickupItemDetail PickupItem = new WS_Courier.PickupItemDetail();
            PickupItem.ProductGroup = 'DOM';
            PickupItem.ProductType='OND';
            PickupItem.NumberOfShipments = 1;//depends on the No. of Shipments attached
            PickupItem.PackageType = 'Documents';
            PickupItem.Payment='P';
            PickupItem.ShipmentWeight = PKShipmentWeight;
            PickupItem.ShipmentVolume = PKShipmentVolume;
            PickupItem.NumberOfPieces = 1; //No. of shipments
            //PickupItem.CashAmount;
            //PickupItem.ExtraCharges;
            //PickupItem.ShipmentDimensions;
            PickupItem.Comments = 'Pickup at Noon';
            
            WS_Courier.PickupItemDetail[] PickupItemsArray = new WS_Courier.PickupItemDetail[]{PickupItem};
            WS_Courier.ArrayOfPickupItemDetail PickupItems= new WS_Courier.ArrayOfPickupItemDetail();
            PickupItems.PickupItemDetail=PickupItemsArray;
            
            WS_Courier.Pickup Pickup = new WS_Courier.Pickup();
            Datetime PickDate = datetime.newInstanceGMT(system.today().year(),system.today().month(),system.today().day(),PKHrs,00,00);
            Pickup.PickupAddress = DIFCAddress;
            Pickup.PickupContact=DIFCCon;
            Pickup.PickupLocation='DIFC Reception';
            Pickup.PickupDate=PickDate.addHours(3);//system.now().addHours(9);
            Pickup.ReadyTime=PickDate;//system.now().addHours(5);
            Pickup.LastPickupTime=PickDate.addHours(3);//system.now().addHours(10);
            Pickup.ClosingTime=PickDate.addHours(3);//system.now().addHours(10);
            Pickup.Comments='Pickup at Noon';
            Pickup.Reference1 = 'DIFC Pickup';
            Pickup.Reference2 = 'Delivery';
            //Pickup.Vehicle;
            //Pickup.Shipments=Shipments;
            Pickup.PickupItems = PickupItems;
            Pickup.Status='Ready';
            
            system.debug('PickupDate==> '+PickDate.timeGMT().addHours(3));
            system.debug('Pickup.ReadyTime==>  '+PickDate.timeGMT());
            system.debug('Pickup.LastPickupTime==>  '+PickDate.timeGMT().addHours(3));
            system.debug('Pickup.ClosingTime==>  '+PickDate.timeGMT().addHours(3));
            
            try{
                WS_Courier.PickupCreationResponse_element PkResp = new WS_Courier.PickupCreationResponse_element();
                CourierService.timeout_x = 100000;
                PkResp = CourierService.CreatePickup(ClientInfo,Transaction_x,Pickup,null);
                system.debug('PickupResp===>'+PkResp);
                flagPK=PkResp.HasErrors;
                //String flag = 'Success'; //Setting based on response
                //list<Shipment__c> updateShip = new list<Shipment__c>();
                if(!flagPK){
                    PkGUID = PkResp.ProcessedPickup.GUID;
                    PkID = PkResp.ProcessedPickup.ID;
                    SFPickup = new Pickup__c();
                    SFPickup.Status__c = 'Ready';
                    SFPickup.Pickup_ID__c = PkResp.ProcessedPickup.ID;
                    SFPickup.Pickup_GUID__c = PkResp.ProcessedPickup.GUID;
                    SFPickup.Closed_Date__c = system.today();
                    SFPickup.Closed_DateTime__c = PickDate.addHours(-1);//system.now().addHours(10);
                    SFPickup.Last_Pickup_Time__c = PickDate.addHours(-1);//system.now().addHours(10);
                    SFPickup.Pickup_Date__c = PickDate.addHours(-1);//system.now().addHours(9);
                    SFPickup.Ready_Date__c = PickDate.addHours(-4);//system.now().addHours(5);
                    //insert SFPickup;
                
                }
            }catch(Exception ex){
                system.debug('Error Message Pickup ====>>> '+ ex.getMessage());
            }
        }
        */
       // system.debug('PkGUID =====>>>>'+ PkGUID);
        //if(PkGUID!=null && PkGUID!='')
            //Shipment.Reference3=PkID;
            //Shipment.PickupGUID=PkGUID;
        try{
            WS_Courier.ShipmentCreationResponse_element Resp= new WS_Courier.ShipmentCreationResponse_element();
            Resp= CourierService.CreateShipments(ClientInfo,Transaction_x,Shipments,null);
            system.debug('RRResp===>'+Resp);
            Log__C CClog = new Log__C();
            cclog.Description__c ='Response: '+Resp + '===>> RequestID===>>'+objStep.SR__c+' - stepId'+objStep.Id+' ';
            cclog.Type__c ='Shipment Response'+objStep.Id;
            insert cclog;
            //if(Resp!=null)
                //flagSH=Resp.HasErrors;
            if(Resp!=null && Resp.HasErrors == false){
                //try{
                    /*if(!flagPK){
                        insert SFPickup;
                        SFPkId = SFPickup.Id;
                    }*/
                    objStep.AWB_Number__c =  Resp.Shipments.ProcessedShipment[0].ID;
                    objStep.Courier_Status__c = 'Ready';
                    Shipment__c SFShipment = new Shipment__c();
                    SFShipment.Airway_Bill_No__c= Resp.Shipments.ProcessedShipment[0].ID;
                    SFShipment.Service_Request__c = objStep.SR__c;
                    SFShipment.Step__c = objStep.Id; 
                    SFShipment.Status__c = 'Ready';
                    SFShipment.Courier_Type__c = (objStep.SR_Group__c =='GS' || (objStep.SR_Group__c =='ROC' && objStep.SR_Record_Type__c != 'Application_of_Registration')) ? objStep.SR__r.Would_you_like_to_opt_our_free_couri__c:objStep.SR__r.Avail_Courier_Services__c;//Veera added. // v1.4
                    SFShipment.Express_Service__c = objStep.SR__r.Express_Service__c;
                    SFShipment.Created_On__c = system.now();
                    //V1.6 
                    if(objStep.SR__r.Service_Type__c =='Non Sponsored Employment Cancellation'){
                        SFShipment.Description_of_Goods__c = 'SR '+objStep.SR__r.Name+' - '+objStep.SR__r.Contact__r.Name+' - '+'DIFC Employee Card';
                    }
                    else{
                         if(Details.DescriptionOfGoods!=''){ //v2.2  -- start
                         
                          string CCDescription = Details.DescriptionOfGoods;                         
                         
                         
                          SFShipment.Description_of_Goods__c = CCDescription.length()>255 ?CCDescription.substring(0,255) : CCDescription;
                          
                          } //v2.2  -- End
                   }
                    if(objStep.Step_Template__r.Courier_Type__c=='Delivery'){ 
                        SFShipment.Shipper_Name__c = 'DIFC';
                        SFShipment.Consignee_Name__c = objStep.SR__r.Consignee_FName__c+' '+objStep.SR__r.Consignee_LName__c;
                    }else if(objStep.Step_Template__r.Courier_Type__c=='Collection'){
                        SFShipment.Shipper_Name__c = objStep.SR__r.Consignee_FName__c+' '+objStep.SR__r.Consignee_LName__c;
                        SFShipment.Consignee_Name__c = 'DIFC';
                    }else { //v2.1 -- Start
                        
                        if(objStep.Pendings__r.size()>0) {
                            
                            if(objStep.Pendings__r[0].Type_Of_Courier__c == 'Delivery' || objStep.Pendings__r[0].Type_Of_Courier__c =='Delivery with Return'){
                                SFShipment.Shipper_Name__c = 'DIFC';
                                SFShipment.Consignee_Name__c = objStep.SR__r.Consignee_FName__c+' '+objStep.SR__r.Consignee_LName__c;
                            }else{
                                
                                SFShipment.Shipper_Name__c = objStep.SR__r.Consignee_FName__c+' '+objStep.SR__r.Consignee_LName__c;
                                SFShipment.Consignee_Name__c = 'DIFC';
                            }
                            
                        }
                        
                        
                    } //v2.1 -- end
                    //SFShipment.Pickup__c = SFPkId;
                    //insert SFShipment;
                    
                    objInfo.Shipments.add(SFShipment);
                    //update objStep;
                    objInfo.Steps.add(objStep);
                //}
            }   
            else{
                
                
                Log__c objLog = new Log__c();
                //V1.6
                objLog.Description__c ='Response: '+Resp + '===>> RequestID===>>'+objStep.SR__c+' - stepId'+objStep.Id+' ';
                objLog.Type__c = 'Webservice Shipment Creation failed Status stepId'+objStep.Id;
                objInfo.Logs.add(objLog);//insert objLog; 
                //ErrorMsg = 'Webservice Shipment Creation failed Status===>>'+Resp.HasErrors;
                }
            
        }catch(Exception ex){
            Log__c objLog = new Log__c();
            objLog.Type__c = 'Courier : Shipment Creation Error stepId'+objStep.Id+'-'+ex.getMessage();
            objLog.Description__c ='Request Id ===>> '+objStep.SR__c+' - stepId'+objStep.Id+'#'+'\nMessage==> '+ex.getMessage()+'\nLine No===> '+ex.getLineNumber();
            objInfo.Logs.add(objLog);//insert objLog;   
        }
        //else
            //ErrorMsg = 'Pickup ID is blank.';
        return objInfo;
    }
    
    public class ShipmentInfo{
        public list<Log__c> Logs = new list<Log__c>();
        public list<Shipment__c> Shipments = new list<Shipment__c>();
        public list<Step__c> Steps = new list<Step__c>();
        public list<Step__c> StepsToInsert = new list<Step__c>();
        public list<Service_Request__c> lstSRs = new list<Service_Request__c>();
    }
    
    public static void Run(){
    
       integer i =0;
       i++;
       i++;
       i++;
       i++;
       i++;
       i++;
       i++;
    }
    
}