/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/Rest_TemplateList/*')
global class RestTemplateList {
    global RestTemplateList() {

    }
    @HttpPost
    global static List<HexaBPM.RestTemplateList.TemplateWrap> GetSRTemplates(String ImportType) {
        return null;
    }
global class TemplateWrap {
    global TemplateWrap() {

    }
}
}
