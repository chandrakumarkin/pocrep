/******************************************************************************************
 *  Author   : Veera 
 *  Company  : techcarrot
 *  Date     : 17-oct-2017   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class Test_BatchRelStatusUpdate {

    static testMethod void myUnitTest() {
        Id gsContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();

        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = gsContactRecordTypeId;
        insert objContact;
        
        List<Document_Details__c > lstDocs = new List<Document_Details__c >();
    
        Document_Details__c empVisa = new Document_Details__c();
        empVisa.Contact__c = objContact.Id;
        empVisa.Document_Type__c = 'Temporary Work Permit';
        empVisa.DOCUMENT_STATUS__c = 'Active';
        empVisa.ISSUE_DATE__c = system.today().addDays(-3);
        empVisa.EXPIRY_DATE__c = system.today().addDays(-1);
        lstDocs.add(empVisa);
        
        
        Document_Details__c empVisa1 = new Document_Details__c();
        empVisa1.Contact__c = objContact.Id;
        empVisa1.Document_Type__c = 'Employment Visa';
        empVisa1.DOCUMENT_STATUS__c = 'Active';
        empVisa1.Document_Number__c = 'Test1234';
        empVisa1.ISSUE_DATE__c = system.today().addDays(-3);
        empVisa1.EXPIRY_DATE__c = system.today().addDays(-1);
        lstDocs.add(empVisa1);
        
        insert lstDocs;
    
        List<Relationship__c> Listrel = new List<Relationship__c>();
        Relationship__c rel = new Relationship__c();        
        rel.Subject_Account__c = objAccount.Id;
        rel.Object_Contact__c = objContact.Id;
        rel.Relationship_Type__c = 'Has Temporary Employee';
        rel.End_Date__c = system.today().addDays(-1);
        rel.Active__c = true;  
        Listrel.add(rel); 
        
        
        
        Relationship__c rel1 = new Relationship__c();        
        rel1.Subject_Account__c = objAccount.Id;
        rel1.Object_Contact__c = objContact.Id;
        rel1.Relationship_Type__c = 'Has DIFC Sponsored Employee';
        rel1.start_Date__c = system.today();
        rel1.Active__c = true;  
        Listrel.add(rel1);   
           
        insert Listrel;
    
        test.startTest();
        BatchRelStatusUpdate obj = new BatchRelStatusUpdate();
        database.executeBatch(obj);        
        test.stopTest();
        Contact con = [select id,Visa_Number__c,Visa_Expiry_Date__c from contact where id = :objContact.id limit 1];
        system.assertEquals(con.Visa_Number__c,'Test1234');
        system.assertEquals(con.Visa_Expiry_Date__c,system.today().addDays(-1));
        Relationship__c rels= [select id,Active__c from Relationship__c where id =:Listrel[0].id];
        system.assertEquals(rels.Active__c,false);
    }
}