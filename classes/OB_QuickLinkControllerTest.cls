/*
* Test Class: OB_QuickLinkController
**/
@isTest
public class OB_QuickLinkControllerTest {
    @isTest
    private static void initTest(){
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con;  
                    
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                Community_User_Role__c = 'Company Services',
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        OB_QuickLinkController.createComplianceCalendar();
        system.runAs(user){
            OB_QuickLinkController.viewAccountInfo();
            OB_QuickLinkController.DFSAPortalDetails();
        }
    }

}