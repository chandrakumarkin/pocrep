/*
    Author      : Durga Prasad
    Date        : 26-Nov-2019
    Description : Custom code to clone the Rejected SR and create as Draft
    ---------------------------------------------------------------------------------------
*/
global without sharing class CC_CloneRejectedSR implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
        	try{
        		system.debug('====CC_CloneRejectedSR=======');
            	OB_CustomCodeHelper.SRDeepClone(stp.HexaBPM__SR__c,stp.HexaBPM__SR__r.Opportunity__c);
        	}catch(Exception e){
        		strResult = e.getMessage()+'';
        	}
        }
        return strResult;
    }
}