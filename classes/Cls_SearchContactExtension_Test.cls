/******************************************************************************************
 *  Name        : Cls_SearchContactExtensionTest 
 *  Author      : Veera Narayana
 *  Date        : 2018-06-28
 *  Description : Test class for VF controller
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        	Updated By    	Description
 v1.1	 23 Dec 2020	Shikha Khanuja	Modified test class as per parameters request type and service type
*******************************************************************************************/
@isTest
public class Cls_SearchContactExtension_Test{
    
    static testMethod void SearchContact(){
        
        
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
      
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        
        
        Cls_SearchContactExtension searchExt = new Cls_SearchContactExtension();
        searchExt.ClientName = 'Test';
        searchExt.currentAccountId = acc.ID;
        searchExt.SearchContact();
        searchExt.clearList();
    
    }

    static testMethod void testSearchContactMembership(){        
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
        
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        
        Membership__c objMember = new Membership__c();
        objMember.RecordTypeId = Schema.SObjectType.Membership__c.getRecordTypeInfosByName().get('Membership').getRecordTypeId();
        objMember.Card_Number__c = 'ABC123';
        objMember.Contact__c = objContact.Id;
        objMember.MembershipStatus__c = 'Active';
        insert objMember;
        
        Cls_SearchContactExtension searchExt = new Cls_SearchContactExtension();
        searchExt.ClientName = 'Test';
        searchExt.currentAccountId = acc.ID;
        searchExt.serviceTypeValue = 'New Monthly Membership';
        searchExt.requestTypeValue = 'Membership';
        searchExt.SearchContact();
        searchExt.clearList();
    
    }

    static testMethod void testSearchContact(){        
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
        
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        
        Membership__c objMember = new Membership__c();
        objMember.RecordTypeId = Schema.SObjectType.Membership__c.getRecordTypeInfosByName().get('Tenants').getRecordTypeId();
        objMember.Card_Number__c = 'ABC123';
        objMember.Contact__c = objContact.Id;
        objMember.MembershipStatus__c = 'Active';
        insert objMember;
        
        Cls_SearchContactExtension searchExt = new Cls_SearchContactExtension();
        searchExt.ClientName = 'Test';
        searchExt.currentAccountId = acc.ID;
        searchExt.serviceTypeValue = 'New Vehicle Registration';
        searchExt.requestTypeValue = 'Tenants';
        searchExt.SearchContact();
        searchExt.clearList();
    
    }

	 static testMethod void testSearchContactOtherValue(){        
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
        
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        
        Membership__c objMember = new Membership__c();
        objMember.RecordTypeId = Schema.SObjectType.Membership__c.getRecordTypeInfosByName().get('Tenants').getRecordTypeId();
        objMember.Card_Number__c = 'ABC123';
        objMember.Contact__c = objContact.Id;
        objMember.MembershipStatus__c = 'Active';
        insert objMember;
        
        Cls_SearchContactExtension searchExt = new Cls_SearchContactExtension();
        searchExt.ClientName = 'Test';
        searchExt.currentAccountId = acc.ID;
        searchExt.serviceTypeValue = 'Add Vehicle';
        searchExt.requestTypeValue = 'Tenants';
        searchExt.SearchContact();
        searchExt.clearList();
    
    }
	
    static testMethod void testSearchContactRemove(){        
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
        
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        
        Membership__c objMember = new Membership__c();
        objMember.RecordTypeId = Schema.SObjectType.Membership__c.getRecordTypeInfosByName().get('Tenants').getRecordTypeId();
        objMember.Card_Number__c = 'ABC123';
        objMember.Contact__c = objContact.Id;
        objMember.MembershipStatus__c = 'Active';
        insert objMember;
        
        Cls_SearchContactExtension searchExt = new Cls_SearchContactExtension();
        searchExt.ClientName = 'Test';
        searchExt.currentAccountId = acc.ID;
        searchExt.serviceTypeValue = 'Remove Employee Access';
        searchExt.requestTypeValue = 'Tenants';
        searchExt.SearchContact();
        searchExt.clearList();
    
    }
    
    static testMethod void testSearchContactFilter(){        
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
        
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
        objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        
        Membership__c objMember = new Membership__c();
        objMember.RecordTypeId = Schema.SObjectType.Membership__c.getRecordTypeInfosByName().get('Tenants').getRecordTypeId();
        objMember.Card_Number__c = 'ABC123';
        objMember.Contact__c = objContact.Id;
        objMember.MembershipStatus__c = 'Active';
        insert objMember;
        
        Cls_SearchContactExtension searchExt = new Cls_SearchContactExtension();
        searchExt.ClientName = 'Test';
        searchExt.currentAccountId = acc.ID;
        searchExt.serviceTypeValue = 'Remove Employee Access';
        searchExt.requestTypeValue = 'Tenants';
        searchExt.SearchContact();
        searchExt.clearList();
    
    }
    
     static testMethod void testSearchContactFilterWithNoMembership(){        
      Account acc = new Account();
      acc.Name = 'Test Account';
      insert acc;
        
      Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = acc.ID;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;                
        
       Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = acc.Id;
        objRel.Object_Contact__c = objContact.Id;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        
        Membership__c objMember = new Membership__c();
        objMember.RecordTypeId = Schema.SObjectType.Membership__c.getRecordTypeInfosByName().get('Membership').getRecordTypeId();
        objMember.Card_Number__c = 'ABC123';
        objMember.Contact__c = objContact.Id;
        objMember.MembershipStatus__c = 'Active';
        insert objMember;
        
        Cls_SearchContactExtension searchExt = new Cls_SearchContactExtension();
        searchExt.ClientName = 'Test';
        searchExt.currentAccountId = acc.ID;
        searchExt.serviceTypeValue = 'New Vehicle Registration';
        searchExt.requestTypeValue = 'Tenants';
        searchExt.SearchContact();
        searchExt.clearList();
    
    }
}