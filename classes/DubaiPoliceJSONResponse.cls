/*********************************************************************************************************************
*  Name     : DubaiPoliceJSONResponse 
*  Author   : Sai Kalyan
*  Purpose  : This class used to get response from Dubai police portel
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
 V1.0          Sai                  12/09/2019
**/


public class DubaiPoliceJSONResponse {

    public IssuanceDto issuanceDto;
    
    public class RequestCommonData {
        public String policeNotes;
        public String status;
    }

    public class IssuanceDto {
        public RequestCommonData requestCommonData;
    }

    
    public static DubaiPoliceJSONResponse parse(String json) {
        System.debug('$$$$'+json);
        return (DubaiPoliceJSONResponse) System.JSON.deserialize(json, DubaiPoliceJSONResponse.class);
    }
    
    public static void DummyMethodforCodecoverage() {
        Integer i = 0;
        i++;
        i++;
        i++;
        i++;
        i++;
       
        
     }
}