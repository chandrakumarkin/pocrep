@isTest
public class ReregistrationPublicToPrivateExtCrlTest {
    public static testMethod void TestMethod_1(){
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;
        Account accountObj = new Account ();
        accountObj.Name='Test Public Account PLC';
        Insert accountObj;
       
        Contact con = new Contact(LastName ='testCon',AccountId = accountObj.Id);
        insert con;  
                  
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert user;
        Service_request__c servicerequestObj = new Service_request__c (); 
        servicerequestObj.Date_of_Resolution__c=System.Today();
        servicerequestObj.Entity_Name__c='Test Public Account PLC';
        servicerequestObj.Proposed_Trading_Name_1__c='Test Public Account PLC';
        servicerequestObj.RecordTypeId=Schema.SObjectType.Service_request__c.getRecordTypeInfosByDeveloperName().get('Re_registration_of_Public_Company_as_Private_Company').getRecordTypeId();
        servicerequestObj.Customer__c=accountObj.id;
        insert servicerequestObj;
		Account_Share_Detail__c AccountShareObj = new Account_Share_Detail__c();
        AccountShareObj.Account__c =accountObj.id;
        insert AccountShareObj;
        Shareholder_Detail__c ShareholderObj = new Shareholder_Detail__c();
        ShareholderObj.Account__c=accountObj.id;
        ShareholderObj.Account_Share__c=AccountShareObj.id;
        insert ShareholderObj;
        PageReference pageRef = Page.ReregistrationPublicToPrivate;
       
		
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('RecordType',servicerequestObj.RecordTypeId);
        
        Test.Starttest();
        
        Apexpages.StandardController sc = new Apexpages.StandardController(servicerequestObj);
        ReregistrationPublicToPrivateExtCrl ext = new  ReregistrationPublicToPrivateExtCrl(sc);         
		ext.getsuffixOption();
		ext.SaveRecord();

        Test.StopTest();
        
    }
    public static testMethod void TestMethod_2(){
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;
       
        Account accountObj = new Account();
        accountObj.Name='Test Private Account PLC';
        Insert accountObj;
        
        Account_Share_Detail__c accountShareDetailObj = new Account_Share_Detail__c();
        accountShareDetailObj.Account__c=accountObj.id;
        accountShareDetailObj.Issued_Capital__c=200000;
        Insert accountShareDetailObj;
       
        Contact con = new Contact(LastName ='testCon',AccountId = accountObj.Id);
        insert con;  
                  
        User u = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                ContactId = con.Id,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
       
        insert u;
        
        Service_request__c servicerequestObj = new Service_request__c ();
        servicerequestObj.Date_of_Resolution__c=System.today();
        servicerequestObj.Entity_Name__c='Test Public Account PLC';
        servicerequestObj.Proposed_Trading_Name_1__c='Test Public Account PLC';
        servicerequestObj.RecordTypeId=Schema.SObjectType.Service_request__c.getRecordTypeInfosByDeveloperName().get('Re_registration_of_Public_Company_as_Private_Company').getRecordTypeId();
        servicerequestObj.Customer__c=accountObj.id;
        insert servicerequestObj;
		
        List<Shareholder_Detail__c> ShareholderList = new List<Shareholder_Detail__c>();
        for(Integer i=0;i<51;i++){
            Shareholder_Detail__c ShareholderObj = new Shareholder_Detail__c();
            ShareholderObj.Account__c=accountObj.id;
            ShareholderObj.Account_Share__c=accountShareDetailObj.id;
            ShareholderList.add(ShareholderObj);
		}
        
        insert ShareholderList;
        PageReference pageRef = Page.ReregistrationPublicToPrivate;
        
		
        Test.setCurrentPage(pageRef);
        pageRef.getParameters().put('RecordType',servicerequestObj.RecordTypeId);
        pageRef.getParameters().put('type','Re_registration_of_Public_Company_as_Private_Company');
        Test.Starttest();
        
        Apexpages.StandardController sc = new Apexpages.StandardController(servicerequestObj);
        //ReregistrationPublicToPrivateExtCrl ext = new  ReregistrationPublicToPrivateExtCrl(sc);         
        System.runAs(u){
            ReregistrationPublicToPrivateExtCrl ext = new  ReregistrationPublicToPrivateExtCrl(sc);
            ext.PopulateInitialData();
            ext.SaveRecord();
        }
        Test.StopTest();
        
    }
    public static testMethod void TestMethod_3(){
        
        Service_request__C ServicerequestObj = new Service_request__C (); 

        PageReference pageRef = Page.ReregistrationPublicToPrivate;
        
        Test.setCurrentPage(pageRef);
        
        Apexpages.StandardController sc = new Apexpages.StandardController(ServicerequestObj);
        ReregistrationPublicToPrivateExtCrl ext = new  ReregistrationPublicToPrivateExtCrl(sc);         
        
    }
}