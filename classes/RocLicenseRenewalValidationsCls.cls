/*
	Author Name	:	Ravi
	Description	:	Custom code which will check for the active lease for the customer.
    --------------------------------------------------------------------------------------------------------------------------
	Modification History
 	--------------------------------------------------------------------------------------------------------------------------
	V.No	Date		Updated By    	Description
	--------------------------------------------------------------------------------------------------------------------------             
 	V1.1	05-Feb-16	Shabbir			Add logic for future lease Assembla# 2122   	
*/
public without sharing class RocLicenseRenewalValidationsCls {
	public static string CheckAciveLease(Step__c stp){
		if(stp.SR__c != null){
			Boolean isError = true;
			set<string> setLeaseStatus = new set<string>{'Active' , 'Future Lease'}; //v1.1
			for(Lease__c objLease : [select Id,Status__c from Lease__c where Account__c =: stp.SR__r.Customer__c AND Status__c IN:setLeaseStatus AND (Type__c = 'Leased' OR Is_License_to_Occupy__c = true) limit 1]){ //v1.1
				isError = false;
			}
			if(isError){ // check for the Active Lease of type Sharing
				for(Operating_Location__c objOL : [select Id from Operating_Location__c where Account__c=:stp.sr__r.Customer__c AND Status__c='Active' AND (Type__c='Sharing' OR Type__c='Corporate Service Provider')]){
					isError = false;
				}
			}
			
			if(isError)
				return 'There is no active lease for the client.';
			//addded on 04th Feb,2015
			isError = true;
			for(Operating_Location__c objOL : [select Id from Operating_Location__c where Account__c=:stp.sr__r.Customer__c AND Status__c='Active' AND IsRegistered__c=true limit 1]){
				isError = false;
			}
			if(isError){
				return 'There is no registered operating location for the client.';
			}else{ //added on 8th Feb,2015
				isError = true;
				for(Tenancy__c objTen : [select Id,Lease__c from Tenancy__c where Lease__r.Status__c IN:setLeaseStatus AND (Lease__r.Type__c = 'Leased' OR Lease__r.Is_License_to_Occupy__c = true) AND Lease__r.Account__c =: stp.SR__r.Customer__c AND Unit__c IN (select Unit__c from Operating_Location__c where IsRegistered__c=true AND Status__c='Active' AND Account__c=:stp.SR__r.Customer__c)]){
					isError = false;
				}
				//if(isError){ // check for the Active Lease of type Sharing
					for(Lease__c objLea : [select Id,Account__c from Lease__c where Status__c IN:setLeaseStatus AND (Type__c = 'Leased' OR Is_License_to_Occupy__c = true) AND Account__c IN (select Hosting_Company__c from Operating_Location__c where IsRegistered__c=true AND Status__c='Active' AND Account__c=:stp.SR__r.Customer__c)]){
						isError = false;
					}
				//}
				if(isError)
					return 'There is no active lease assigned to registered operating location for the client.';
			}
		}
		return 'Success';
	}
}