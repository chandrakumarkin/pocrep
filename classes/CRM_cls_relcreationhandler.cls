/******************************************************************************************
 *  Author   	: Claude Manahan
 *  Company  	: NSI DMCC 
 *  Date     	: 2016-12-6    
 *  Description : This class handles transaction for creating/updating relationships
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By        Description
 ----------------------------------------------------------------------------------------            
 V1.0   6-12-2016       Claude            Created
 *******************************************************************************************/
public without sharing class CRM_cls_relcreationhandler {
    
    @InvocableMethod(label='Send SRDoc Email' description='Creates/updates relationships')
	public static void createUpdateRelationships(List<CRMRelationship> crmRelationshipDetails){
		
		List<Relationship__c> listRels = new List<Relationship__c>();
		
		for(CRMRelationship c : crmRelationshipDetails){
			
			listRels.add(new Relationship__c(Subject_account__c = c.accountId, User__c = c.userId, Relationship_Type__c = c.relationshipType, Active__c = c.isActive));
			
		}
		
		System.debug('New Job: ' + System.enqueueJob(new CRM_cls_QueueableRelationship(listRels)));
		
	}
	
	public class CRMRelationship {

        @InvocableVariable(required=true)
        public String accountId; 
        
        @InvocableVariable(required=true)
        public String userId;
        
        @InvocableVariable(required=true)
        public String relationshipType;
        
        @InvocableVariable(required=true)
        public Boolean isActive;

    }
    
    public class CRM_cls_QueueableRelationship implements Queueable {
	
		public List<Relationship__c> newRelationShipContacts;
		
		public void execute(QueueableContext SC) {
	   		
			System.debug('Relationships created');
			upsert newRelationShipContacts;
	   	}
	   	
	   	public CRM_cls_QueueableRelationship(List<Relationship__c> newRelationShipContacts){
	   		this.newRelationShipContacts = newRelationShipContacts;
	   	}
	    
	}
    
}