/*
Author      :   Azfer
Description :   This class is used Get the company Survey
--------------------------------------------------------------------------------------------------------------------------
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date            Updated By          Description
--------------------------------------------------------------------------------------------------------------------------             
V1.0    06-Mar-2019     Azfer               Created
v1.1    08-Jul-2021     Arun Singh          Added new code 
*/
public class CompanySurveyExtCtrl {
    
    public Company_Survey__c CompanySurveyObj { get; set; }
    public boolean IsValid {  get;   set; }
   
    String ContactId;

    public CompanySurveyExtCtrl(ApexPages.StandardController ctrl) {
        CompanySurveyObj = (Company_Survey__c) ctrl.getRecord();
    }

    public void pageLoad() {
        IsValid = true;

        ContactId = ApexPages.currentPage().getParameters().get('cid');
        string emailId = ApexPages.currentPage().getParameters().get('eid');
        CompanySurveyObj.Email_Address__c = emailId;

        if (string.isblank(ContactId) == false && string.isblank(emailId) == false) 
        {
            List < Contact > ListContact = [SELECT Id, FirstName, LastName 
                                            FROM Contact 
                                            WHERE id =: ContactId];
            if (ListContact.size() < 1) 
            {

                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You are not a valid Contact'));
                IsValid = false;
            } 
            else 
            {
                CompanySurveyObj.Contact__c = ListContact[0].id;
                List < Company_Survey__c > ListCompanySurvey = [SELECT id, Name, Contact__c 
                                                                FROM Company_Survey__c 
                                                                WHERE Contact__c =: Contactid];
                if (ListCompanySurvey.size() > 0) 
                {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You have already submitted the Information'));

                    IsValid = false;
                }
            }
        }
        else if(string.isblank(emailId) == false){}
        else 
        {
            IsValid = false;
              ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, 'You are not a valid Contact'));
        }
    }

   public  void RowValueChange()
    {
        string ValParam= Apexpages.currentPage().getParameters().get('ValParam');
        string myParam= Apexpages.currentPage().getParameters().get('myParam');
        System.debug('================ValParam==========>'+ValParam);
        System.debug('================myParam==========>'+myParam);
        if(myParam=='true')
            CompanySurveyObj.put(ValParam,true);
        else   if(myParam=='false')
            CompanySurveyObj.put(ValParam,false);
        else
            CompanySurveyObj.put(ValParam,myParam);        
      
    }

    public PageReference SaveComapnySurvey() 
    {
        if(string.isblank(ContactId) == false)
        {  
            List < contact > ListContact = [SELECT Id, Name, AccountId 
                                            FROM Contact      WHERE id =: Contactid 
                                            AND AccountId != Null];
            if (ListContact.size() > 0) {
                CompanySurveyObj.Company__c = ListContact[0].AccountId;
            } 

        }
              

        upsert CompanySurveyObj;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm, 'Your Information has been saved successfully.'));
        IsValid = false;
        return Null;
    }
}