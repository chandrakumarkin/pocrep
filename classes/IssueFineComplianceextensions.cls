public with sharing class IssueFineComplianceextensions {

private Compliance__c ObjComp;

    public IssueFineComplianceextensions(ApexPages.StandardController stdController) {
    
    
    this.ObjComp = (Compliance__c)stdController.getRecord();


    }
    
    public PageReference save() 
    {

        // Add the account to the database. 
        ObjComp.Status__c='Defaulted';
        ObjComp.Defaulted_Date__c=date.today()-1;
        
        update  ObjComp;
        
        ScheduleComplianceProcess Obj=new ScheduleComplianceProcess();
        Obj.ProcessFineBatch([select Id,Apply_Fine__c, Name, Account__c,Is_Valid_Fine__c,Account__r.Legal_Type_of_Entity__c ,Account__r.ROC_Status__c, Fine__c, Account__r.E_mail__c,Objection_Approved__c,Account__r.Company_Type__c, Sys_Actual_End_Date__c , Status__c,six_Fine_issued__c ,Start_Date__c, End_Date__c, Exception_Date__c, Fine_Issued_Date__c, Fine_Renewed_Date__c,Last_Date_To_Pay_Fine__c,Is_Fine_Issued__c, Defaulted_Date__c,Fine_Reissued__c,Sys_Last_Updated_Date__c from Compliance__c where id=:ObjComp.id]);
    
    // Send the user to the detail page for the new account.
        PageReference acctPage = new ApexPages.StandardController(ObjComp).view();
        acctPage.setRedirect(true);
        return acctPage;
        

    }

}