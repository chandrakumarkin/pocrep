@isTest
public class CourierCollectionControllerTest {
    
     @testSetup static void setup() {
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact'; 
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
        
       map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
       for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
       } 
       SR_Template__c objTemplate1 = new SR_Template__c();
       objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
       objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
       objTemplate1.Active__c = true;
       objTemplate1.SR_Group__c='GS';
       insert objTemplate1;
        
       Step_Template__c objStepType1 = new Step_Template__c();
       objStepType1.Name = 'Courier Collection';
       objStepType1.Code__c = 'Courier Collection';
       objStepType1.Step_RecordType_API_Name__c = 'General';
       objStepType1.Summary__c = 'Courier Collection';
       insert objStepType1;
        
       Service_Request__c objSR3 = new Service_Request__c();
       objSR3.Customer__c = objAccount.Id;
       objSR3.Email__c = 'testsr@difc.com';    
       objSR3.Contact__c = objContact.Id;
       objSR3.Express_Service__c = false;
       objSR3.Statement_of_Undertaking__c  = true;
       objSR3.SR_Template__c = objTemplate1.Id;
       insert objSR3;
       
         
       Status__c objStatus = new Status__c();
       objStatus.Name = 'Pending Collection';
       objStatus.Type__c = 'Start';
       objStatus.Code__c = 'Pending Collection';
       insert objStatus;
       
       step__c objStep = new Step__c();
       objStep.SR__c = objSR3.Id;
       objStep.Step_Template__c = objStepType1.id ;
       objStep.No_PR__c = false;
       objStep.status__c = objStatus.id;
       insert objStep;
        
       Passport_Tracking__c thisTracking = new Passport_Tracking__c();
       thisTracking.Step__c = objStep.Id;
       thisTracking.Has_Status_Updated__c = false;
       thisTracking.Status__c = 'Pending Collection'; 
       thisTracking.Passport_Number__c = 'Ready'; 
       thisTracking.Client_Name__c ='test';
       thisTracking.SR_Number__c = '1234';
       insert thisTracking;
     }
    
    @isTest static void testgetwrapperList() {
       Test.startTest(); 
       CourierCollectionController.ResponseTrackingWrapper wrapperList = new  CourierCollectionController.ResponseTrackingWrapper();
       wrapperList = CourierCollectionController.getResponseWrapperList('','','');
       
       List<Passport_Tracking__c> thisTracking = [SELECT Id,Name,Status__c,Step__c FROM Passport_Tracking__c LIMIT 1];
       String selectedStatus = 'Received by Office Boy';
       String record = JSON.serialize(thisTracking);
       CourierCollectionController.updatePassportTracking(record, selectedStatus);
       
   }
}