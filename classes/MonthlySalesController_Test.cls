/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-28-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   10-27-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
private class MonthlySalesController_Test {
    static testMethod void myUnitTest() {
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Monthly_Sales')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.RF_Valid_To__c = system.today().addDays(10);
        objLease.RF_Valid_From__c = system.today();
        objLease.Lease_Types__c = 'Retail Lease';
        insert objLease;

        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Monthly_Sales';
        testSrTemplate.Menutext__c = 'New Employment Visa Package';
        testSrTemplate.SR_RecordType_API_Name__c = 'Monthly_Sales';
        testSrTemplate.SR_Group__c = 'GS';
        testSrTemplate.Active__c = true;
        insert testSrTemplate;
        
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = objAccount.Id);
        insert con; 

        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
               
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        Map<String,Lease__c> leaseMap = new Map<String,Lease__c>();
        Lease__c leaseRec = [Select id,name,Account__c,Account__R.BP_No__c,Status__c,Start_date__c,End_Date__c,Type__c,RF_Valid_To__c,RF_Valid_From__c,Lease_Types__c,Consider_For_Monthly_Sales_Submission__c from Lease__c where id=:objLease.id];
        leaseMap.put(leaseRec.Name,leaseRec);
        system.debug('LeaseMap---'+leaseMap);
        String base64Data ='12345';
        
        Service_Request__c SR1 = new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Monthly_Sales');
        SR1.Customer__c = objAccount.Id;
        SR1.Mobile_Number__c ='+971500000000';
        SR1.Year__c = '2020';
        SR1.Month__c = 'AUGUST';
        SR1.Transaction_Amount__c =1000;
        SR1.Lease__c = leaseRec.id;
        
        //system.runAs(user){        
            Test.startTest();
        		MonthlySalesController.leaseMap = new Map<string , Lease__c>();
            	MonthlySalesController.leaseMap.put(leaseRec.Name,leaseRec);
            	system.debug('MonthlySalesController.leaseMap----'+MonthlySalesController.leaseMap);
                MonthlySalesController.getSalesData(objAccount.Id,2020,objLease.name);
                MonthlySalesController.getMonthlySalesWrapperData(user.id,2020 ,objLease.name,leaseMap);
                MonthlySalesController.getMonthlySalesViewForLease(user.id,2020 ,objLease.name,leaseMap);
                try
                {
                    MonthlySalesController.createNewMonthlySalesSR(SR1, 'fileName', base64Data ,'PDF',2020,user.Id,'AUGUST',objLease.name,leaseMap);
                }Catch(Exception ex){
                    
                }
                MonthlySalesController.initiateMonthlySalesView(user.id,2020);
           Test.stopTest(); 
        //}
    }
    
     @isTest static void testCallout() {
          Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Monthly_Sales')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Lease__c objLease = new Lease__c();
        objLease.Account__c = objAccount.Id;
        objLease.Status__c = 'Active';
        objLease.Start_date__c = system.today();
        objLease.End_Date__c = system.today().addYears(3);
        objLease.Type__c = 'Leased';
        objLease.Name = '1345';
        objLease.RF_Valid_To__c = system.today().addDays(10);
        objLease.RF_Valid_From__c = system.today();
        objLease.Lease_Types__c = 'Retail Lease';
        insert objLease;

        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'Monthly_Sales';
        testSrTemplate.Menutext__c = 'New Employment Visa Package';
        testSrTemplate.SR_RecordType_API_Name__c = 'Monthly_Sales';
        testSrTemplate.SR_Group__c = 'GS';
        testSrTemplate.Active__c = true;
        insert testSrTemplate;
        
        //crreate contact
        Contact con = new Contact(LastName ='testCon',AccountId = objAccount.Id);
        insert con; 

        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
               
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        Map<String,Lease__c> leaseMap = new Map<String,Lease__c>();
        Lease__c leaseRec = [Select id,name,Account__c,Account__R.BP_No__c,Status__c,Start_date__c,End_Date__c,Type__c,RF_Valid_To__c,RF_Valid_From__c,Lease_Types__c,Consider_For_Monthly_Sales_Submission__c from Lease__c where id=:objLease.id];
        leaseMap.put(leaseRec.Name,leaseRec);
        system.debug('LeaseMap---'+leaseMap);
        String base64Data ='12345';
        
        Service_Request__c SR1 = new Service_Request__c();
        SR1.RecordTypeId = mapRecordType.get('Monthly_Sales');
        SR1.Customer__c = objAccount.Id;
        SR1.Mobile_Number__c ='+971500000000';
        SR1.Year__c = '2020';
        SR1.Month__c = 'AUGUST';
        SR1.Transaction_Amount__c =1000;
        SR1.Lease__c = leaseRec.id;
         
        // Set mock callout class 
        Test.setMock(WebServiceMock.class, new FinanceContactPersonWebservice_Mock());
        Endpoint_URLs__c objEP = new Endpoint_URLs__c();
        objEP.Name = 'FinanceContactPerson';
        objEP.URL__c = 'http://eccdev.com/sampledata';
        insert objEP;
         
        // Call method to test.
        // This causes a fake response to be sent
        // from the class that implements HttpCalloutMock. 
        //HttpResponse res = 
        Test.startTest();
            MonthlySalesController.webserviceCallForMonthlySalesNew(new List<id>{SR1.id});
        Test.stopTest();
        // Verify response received contains fake values
        /*
         String contentType = res.getHeader('Content-Type');
        System.assert(contentType == 'application/json');
        String actualValue = res.getBody();
        String expectedValue = '{"example":"test"}';
        System.assertEquals(actualValue, expectedValue);
        System.assertEquals(200, res.getStatusCode());
		*/
    }
}