/*
    Author      : Prateek
    Date        : 06-Feb-2019
    Description : Custom code to execute logic on Application Withdrawn Approval
    --------------------------------------------------------------------------------------
 */
global without sharing class CC_WithdrawApplication implements HexaBPM.iCustomCodeExecutable {
global string EvaluateCustomCode(HexaBPM__Service_Request__c SR,HexaBPM__Step__c stp) {

	string strResult = 'Success';

	try{

		//udate account status
		string AccountId = stp.HexaBPM__SR__r.HexaBPM__Customer__c;
		if(AccountId != null) {
			account accObj = new account(id=AccountId);
			accObj.ROC_Status__c = 'Withdrawn';
			update accObj;
		}

		//update opportunity stage
		string OpportunityId = stp.HexaBPM__SR__r.Opportunity__c;
		if(OpportunityId != null) {
			Opportunity oppObj = new Opportunity (id=OpportunityId);
			oppObj.StageName = 'Application withdrawn';
			update oppObj;
		}


		//HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__c);
		//do logic on application;

		list<HexaBPM__Step__c> actionItemsToBeUpdated = new list<HexaBPM__Step__c>();
		HexaBPM__Status__c statusToSet = new HexaBPM__Status__c();

		for(HexaBPM__Status__c actionItemStatusObj : [SELECT id FROM HexaBPM__Status__c where 	HexaBPM__Code__c = 'REQUEST_WITHDRAWN' LIMIT 1]){
			statusToSet = actionItemStatusObj;
			}

		if(stp.HexaBPM__SR__c != null && statusToSet != null){
			for(HexaBPM__Step__c actionItemObj : [SELECT id,HexaBPM__Status__c FROM HexaBPM__Step__c where HexaBPM__SR__c =: stp.HexaBPM__SR__c AND HexaBPM__Status_Type__c != 'End']){

				actionItemObj.HexaBPM__Status__c = statusToSet.id;
				actionItemsToBeUpdated.add(actionItemObj);

			}
		}

		if(actionItemsToBeUpdated.size() > 0){
			update actionItemsToBeUpdated;
		}
		
		OB_CustomCodeHelper.SRDeepClone(stp.HexaBPM__SR__c,OpportunityId);

	}catch(Exception e) {
		strResult = e.getMessage()+'';
	}
	return strResult;
}
}