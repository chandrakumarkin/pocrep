/*
 *  
 * Author: Diana Correa
 * Purpose: Test class for MobileForgotPasswordController.
 * 
 */

@isTest
public class TestMobileForgotPasswordController {

    static testmethod void TestForgotPassword(){
        
        MobileForgotPasswordController forgotPasswordController = new MobileForgotPasswordController();
        
        forgotPasswordController.success = true;
        forgotPasswordController.forgotPassword();
        
        forgotPasswordController.username = 'test';
        forgotPasswordController.forgotPassword();
        
        forgotPasswordController.navigateBack();
        
        
    }
}