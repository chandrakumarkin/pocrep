@istest(seealldata=true)
public class CLS_DIFC_insuranceTest {
 

 public static void testData()
    {
    T001__c Obj001=new T001__c();
    Obj001.BUKRS__c='w';
    Obj001.BUTXT__c='d';
    insert Obj001;
    
    }
    
    static testmethod void singleRecordTest()
    {
       testData();
       // negativeTest();
    
        TEST.startTest();
            RestContext.request = new RestRequest();
            RestContext.request.params.put('mode','listcompanies' );
            Map<string,String> params=new Map<string,String>();
            CLS_DIFC_insurance.jsonResponse();
            CLS_DIFC_insurance.jsonResString('listcompanies',params);
            
            params.put('lic','1111');
            params.put('email','as@dic.com');
            params.put('mobile','+971569803616');
            
            CLS_DIFC_insurance.jsonResString('authorized',params);
        TEST.stopTest();
    }
    
    
}