@isTest
public class OB_CreateCPTest {

    @testSetup
    static  void createTestData() {
        // create account
        List<Account> lstNewAccount = new List<Account>();
        lstNewAccount =  OB_TestDataFactory.createAccounts(1);
        lstNewAccount[0].OB_Type_of_commercial_permission__c = 'Gate Avenue Pocket Shops';
        insert lstNewAccount;

        // create contact
        List<Contact> lstContacts = new List<Contact>();
        lstContacts = OB_TestDataFactory.createContacts(1,lstNewAccount);
        insert lstContacts;
        
         Opportunity opp = new Opportunity();
		opp.AccountId = lstNewAccount[0].id;
        opp.Name = 'test';
        opp.StageName='Prospecting';
        opp.CloseDate=System.today().addMonths(1);
        insert opp;

        //Create Custom Setting Records
    }
    static testMethod void CreateNonFinancialSRTest() {
        List<Contact> lstContacts = [select id,AccountId,Account.Phone from Contact];
        List<account> lstNewAccount = [select id,Is_Commercial_Permission__c,Name,Phone,OB_Type_of_commercial_permission__c from account];
        test.startTest();
          OB_CreateCP.CreateCommercialPermission(lstContacts);
        OB_CreateCP.CreateCommercialPermission(null);
        OB_CreateCP.CreateCP(new map<id,account>{lstContacts[0].id => lstNewAccount[0]});
        test.stopTest();
    }
}