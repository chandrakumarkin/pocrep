/******************************************************************************************
 *  Name        : CRM_cls_DFSAEmailHandler 
 *  Author      : Claude Manahan
 *  Company     : NSI JLT
 *  Date        : 2016-12-14
 *  Description : Email Handler for Inbound DFSA Emails
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.0   14-12-2016   Claude        Created (#3749)
*******************************************************************************************/
global without sharing class CRM_cls_DFSAEmailHandler implements Messaging.InboundEmailHandler {
    
    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.Inboundenvelope envelope) {
                          
        CRM_cls_EmailUtils.processDFSAEmail(email,envelope);
        
        return null;
    }
    
}