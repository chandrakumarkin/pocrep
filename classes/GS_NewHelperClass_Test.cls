/*****************************************************************************************************************************
Author      :   Mudasir Wani	
Date        :   08/12/2019
Description :   This is a test class for the clas GS_NewHelperClass
--------------------------------------------------------------------------------------------------------------------------
Modification History
--------------------------------------------------------------------------------------------------------------------------
V.No    Date            Updated By      Description
--------------------------------------------------------------------------------------------------------------------------             
V1.1    08/12/2019      Mudasir         Created a new test class for the GS related functionalities
*****************************************************************************************************************************/
@isTest
private class GS_NewHelperClass_Test {

    static testMethod void myUnitTest() {
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.Menutext__c = 'New Employment Visa Package';
        testSrTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.SR_Group__c = 'GS';
        testSrTemplate.Active__c = true;
        insert testSrTemplate;
        
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('DIFC_Sponsorship_Visa_New');
        SR.Customer__c = objAccount.Id;
        SR.Mobile_Number__c ='+971500000000';
        insert SR;
        
        Status__c status = Test_CC_FitOutCustomCode_DataFactory.getTestStepStatus('Pending Review', 'Pending_Review');        
        insert status;
        
        Step_Template__c testStepTemplates = new Step_Template__c(Code__c = 'Mobile Number Update',Name='Mobile Number Update',Step_RecordType_API_Name__c = 'General');
        insert testStepTemplates;
        
        Step_Template__c testStepTemplates1 = new Step_Template__c(Code__c = 'Original Passport and Entry Permit are Received',Name='Original Passport and Entry Permit are Received',Step_RecordType_API_Name__c = 'General');
        insert testStepTemplates1;
        
        SR_Steps__c srstep = new SR_Steps__c();
        srstep.Step_Template__c = testStepTemplates.id;
        insert srstep;
        
        SR_Steps__c srstep1 = new SR_Steps__c();
        srstep1.Step_Template__c = testStepTemplates1.id;
        insert srstep1;
        
        step__c step = Test_CC_FitOutCustomCode_DataFactory.getTestStep(SR.id, status.Id, objAccount.Name);
        step.Step_Template__c = testStepTemplates.Id;
        step.SR_Step__c = srstep.id;
        step.mobile_No__c ='+971500000000';
        insert step;
        
        step__c step1 = Test_CC_FitOutCustomCode_DataFactory.getTestStep(SR.id, status.Id, objAccount.Name);
        step.Step_Template__c = testStepTemplates1.Id;
        step.SR_Step__c = srstep1.id;
        step.mobile_No__c ='+971500000000';
        insert step1;
        
        Test.startTest();
        	GS_NewHelperClass.processActions([Select id,name,step_name__c from Step__c where id=:step.id limit 1]);
        	GS_NewHelperClass.processActions([Select id,name,step_name__c from Step__c where id=:step1.id limit 1]);
        Test.stopTest();
    }
    
     static testMethod void myUnitTest1() {
        Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
        
        Status__c objStatus = new Status__c();
        objStatus.Name = 'Pending';
        objStatus.Code__c = 'Pending';
        insert objStatus;
        
        Business_Hours__c bh= new Business_Hours__c();
        BH.name='Fit-Out';
        BH.Business_Hours_Id__c='01m20000000BOf6';
        insert BH; 
        
        SR_Template__c testSrTemplate = new SR_Template__c();
    
        testSrTemplate.Name = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.Menutext__c = 'New Employment Visa Package';
        testSrTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
        testSrTemplate.SR_Group__c = 'GS';
        testSrTemplate.Active__c = true;
        insert testSrTemplate;
        
        Service_Request__c SR = new Service_Request__c();
        SR.RecordTypeId = mapRecordType.get('DIFC_Sponsorship_Visa_New');
        SR.Customer__c = objAccount.Id;
        SR.Mobile_Number__c ='+971500000000';
        insert SR;
        
        Status__c status = Test_CC_FitOutCustomCode_DataFactory.getTestStepStatus('Pending Review', 'Pending_Review');        
        insert status;
        
        Step_Template__c testStepTemplates = new Step_Template__c(Code__c = 'Mobile Number Update',Name='Mobile Number Update',Step_RecordType_API_Name__c = 'General');
        insert testStepTemplates;
        
        Step_Template__c testStepTemplates1 = new Step_Template__c(Code__c = 'Original Passport and Entry Permit are Received',Name='Original Passport and Entry Permit are Received',Step_RecordType_API_Name__c = 'General');
        insert testStepTemplates1;
        
        SR_Steps__c srstep = new SR_Steps__c();
        srstep.Step_Template__c = testStepTemplates.id;
        insert srstep;
        
        SR_Steps__c srstep1 = new SR_Steps__c();
        srstep1.Step_Template__c = testStepTemplates1.id;
        insert srstep1;
        
        step__c step = Test_CC_FitOutCustomCode_DataFactory.getTestStep(SR.id, status.Id, objAccount.Name);
        step.Step_Template__c = testStepTemplates.Id;
        step.SR_Step__c = srstep.id;
        step.mobile_No__c ='+971500000000';
        insert step;
        
        step__c step1 = Test_CC_FitOutCustomCode_DataFactory.getTestStep(SR.id, status.Id, objAccount.Name);
        step1.Step_Template__c = testStepTemplates1.Id;
        step1.SR_Step__c = srstep1.id;
        step1.mobile_No__c ='+971500000000';
        insert step1;
        
        Test.startTest();
        	GS_Step_ProcessBuilder_Actions.GsStepProcessbuilderActions(new List<id> {step1.id});
        Test.stopTest();
    }
}