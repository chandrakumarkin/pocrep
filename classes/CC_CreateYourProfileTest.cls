/*
    Author      : Leeba
    Date        : 29-March-2020
    Description : Test class for CC_CreateYourProfile
    --------------------------------------------------------------------------------------
*/

@isTest
public class CC_CreateYourProfileTest{

    public static testMethod void CC_CreateYourProfile() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       Contact con = new Contact();
       con.LastName = 'test';
       con.Email = 'test@test.com';
       insert con;
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.Date_of_Issue__c = system.today();
        objHexaSR.Date_of_Expiry__c = system.today()+100;
        objHexaSR.HexaBPM__Contact__c = con.id;
        objHexaSR.Entity_Type__c = 'Financial - related';
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.First_Name__c = 'First';
        
        objHexaSR.Last_Name__c = 'Last';
        objHexaSR.hexabpm__send_sms_to_mobile__c = '+971111111111';
        objHexaSR.Passport_No__c = 'XXXXXXXX';
        objHexaSR.Nationality__c = 'India';
        objHexaSR.Date_of_Birth__c = system.today().addYears(-25);
        objHexaSR.Place_of_Issuance__c = 'Hyderabad';
        objHexaSR.Gender__c = 'Male';
        objHexaSR.Place_of_Birth__c = 'Tirupati';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
       
        Test.startTest();
            string stepId = objHexastep.Id;
            map<String, Schema.SObjectType>  m = Schema.getGlobalDescribe();
            SObjectType objtype;
            DescribeSObjectResult objDef1;
            map<String, SObjectField> fieldmap;
            if(m.get('HexaBPM__Service_Request__c')!= null){
                objtype = m.get('HexaBPM__Service_Request__c');
                objDef1 =  objtype.getDescribe();
                fieldmap =  objDef1.fields.getmap();
            }
            set<string> setFields = new set<string>();
            if(fieldmap!=null){
                for(Schema.SObjectField strFld:fieldmap.values()){
                    Schema.DescribeFieldResult fd = strFld.getDescribe();
                    if(fd.isCustom())
                        setFields.add(string.valueOf(strFld).toLowerCase());
                }
            }
            setFields.add('id');
            setFields.add('recordtypeid');
            string SRqry =  'select HexaBPM__SR__c,Id';
            if(setFields.size()>0){
                for(string srField : setFields){
                    SRqry += ','+'HexaBPM__SR__r.'+srField;
                }
            }
            SRqry += ' From HexaBPM__Step__c where Id=:stepId';
            HexaBPM__Step__c step = database.query(SRqry);
            /*
            [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c,
                                      HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__Contact__c, HexaBPM__SR__r.HexaBPM__Contact__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Entity_Name__c,HexaBPM__SR__r.Business_Sector__c                         
                                      from HexaBPM__Step__c where Id=:objHexastep.Id];
            */
            CC_CreateYourProfile CC_CreateYourProfileObj = new CC_CreateYourProfile();
            CC_CreateYourProfileObj.EvaluateCustomCode(objHexaSR,step); 
        Test.stopTest();
       
    }
    
    public static testMethod void CC_CreateYourProfile2() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       Contact con = new Contact();
       con.LastName = 'test';
       con.Email = 'test@test.com';
       insert con;
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.Date_of_Issue__c = system.today();
        objHexaSR.Date_of_Expiry__c = system.today()+100;
        objHexaSR.HexaBPM__Contact__c = con.id;
        objHexaSR.Entity_Type__c = 'Retail';
        objHexaSR.Entity_Name__c = 'test';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
       
        Test.startTest();
            string stepId = objHexastep.Id;
            map<String, Schema.SObjectType>  m = Schema.getGlobalDescribe();
            SObjectType objtype;
            DescribeSObjectResult objDef1;
            map<String, SObjectField> fieldmap;
            if(m.get('HexaBPM__Service_Request__c')!= null){
                objtype = m.get('HexaBPM__Service_Request__c');
                objDef1 =  objtype.getDescribe();
                fieldmap =  objDef1.fields.getmap();
            }
            set<string> setFields = new set<string>();
            if(fieldmap!=null){
                for(Schema.SObjectField strFld:fieldmap.values()){
                    Schema.DescribeFieldResult fd = strFld.getDescribe();
                    if(fd.isCustom())
                        setFields.add(string.valueOf(strFld).toLowerCase());
                }
            }
            setFields.add('id');
            setFields.add('recordtypeid');
            string SRqry =  'select HexaBPM__SR__c,Id';
            if(setFields.size()>0){
                for(string srField : setFields){
                    SRqry += ','+'HexaBPM__SR__r.'+srField;
                }
            }
            SRqry += ' From HexaBPM__Step__c where Id=:stepId';
            HexaBPM__Step__c step = database.query(SRqry);
            /*
            [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c,
                                      HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__Contact__c, HexaBPM__SR__r.HexaBPM__Contact__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Entity_Name__c,HexaBPM__SR__r.Business_Sector__c                         
                                      from HexaBPM__Step__c where Id=:objHexastep.Id];
            */
            CC_CreateYourProfile CC_CreateYourProfileObj = new CC_CreateYourProfile();
            CC_CreateYourProfileObj.EvaluateCustomCode(objHexaSR,step); 
        Test.stopTest();
       
    }
    
    public static testMethod void CC_CreateYourProfile3() {
    
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       Contact con = new Contact();
       con.LastName = 'test';
       con.Email = 'test@test.com';
       insert con;
        
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.Date_of_Issue__c = system.today();
        objHexaSR.Date_of_Expiry__c = system.today()+100;
        objHexaSR.HexaBPM__Contact__c = con.id;
        objHexaSR.Entity_Type__c = 'Financial - related';
        objHexaSR.Business_Sector__c = 'Investment Fund';
        objHexaSR.Entity_Name__c = 'test';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
       
        Test.startTest();
            string stepId = objHexastep.Id;
            map<String, Schema.SObjectType>  m = Schema.getGlobalDescribe();
            SObjectType objtype;
            DescribeSObjectResult objDef1;
            map<String, SObjectField> fieldmap;
            if(m.get('HexaBPM__Service_Request__c')!= null){
                objtype = m.get('HexaBPM__Service_Request__c');
                objDef1 =  objtype.getDescribe();
                fieldmap =  objDef1.fields.getmap();
            }
            set<string> setFields = new set<string>();
            if(fieldmap!=null){
                for(Schema.SObjectField strFld:fieldmap.values()){
                    Schema.DescribeFieldResult fd = strFld.getDescribe();
                    if(fd.isCustom())
                        setFields.add(string.valueOf(strFld).toLowerCase());
                }
            }
            setFields.add('id');
            setFields.add('recordtypeid');
            string SRqry =  'select HexaBPM__SR__c,Id';
            if(setFields.size()>0){
                for(string srField : setFields){
                    SRqry += ','+'HexaBPM__SR__r.'+srField;
                }
            }
            SRqry += ' From HexaBPM__Step__c where Id=:stepId';
            HexaBPM__Step__c step = database.query(SRqry);
            /*
            [select HexaBPM__SR__c,id,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Date_of_Issue__c,
                                      HexaBPM__SR__r.Date_of_Expiry__c,HexaBPM__SR__r.HexaBPM__Contact__c, HexaBPM__SR__r.HexaBPM__Contact__c,HexaBPM__SR__r.Entity_Type__c,HexaBPM__SR__r.Entity_Name__c,HexaBPM__SR__r.Business_Sector__c                         
                                      from HexaBPM__Step__c where Id=:objHexastep.Id];
            */
            CC_CreateYourProfile CC_CreateYourProfileObj = new CC_CreateYourProfile();
            CC_CreateYourProfileObj.EvaluateCustomCode(objHexaSR,step);
            
            CC_CreateYourProfileObj.CreateFundAccount(acc.Id,'Has Exempt Fund');
            CC_CreateYourProfile.UpdateUserDetails(step.HexaBPM__SR__c,con.Id);
         //   CC_CreatePortalUser.dummytest();
        Test.stopTest();
       
    }
 }