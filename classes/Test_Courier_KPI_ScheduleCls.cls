@isTest
private class Test_Courier_KPI_ScheduleCls{

  static testMethod void myUnitTest() {
        
   Test.startTest();
   
    List<Account> lstAccount = new List<Account>();
        Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;
        lstAccount.add(objAccount);
        string conRT;
         
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
            conRT = objRT.Id;
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;
   
        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
        for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c']){
            mapSRRecordTypes.put(objType.DeveloperName,objType);
        }
       
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'New Employment Visa Package';
        objTemplate1.SR_RecordType_API_Name__c = 'New Employment Visa Package';
        objTemplate1.Active__c = true;
        insert objTemplate1;
        
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Courier Collection';
        objStepType1.Code__c = 'Courier Collection';
        objStepType1.Step_RecordType_API_Name__c = 'Courier Collection';
        objStepType1.Summary__c = 'Courier Collection';
        insert objStepType1;
       
        Service_Request__c objSR3 = new Service_Request__c();
        objSR3.Customer__c = objAccount.Id;
        objSR3.Email__c = 'testsr@difc.com';    
        objSR3.Contact__c = objContact.Id;
        objSR3.Express_Service__c = false;
        objSR3.SR_Template__c = objTemplate1.Id;
        objSR3.sr_group__c ='GS';
    
        insert objSR3;
    
       Status__c objStatus = new Status__c();
       objStatus.Name = 'Pending Collection';
       objStatus.Type__c = 'Pending Collection';
       objStatus.Code__c = 'Pending Collection';
       insert objStatus;
         
        SR_Status__c objSRStatus = new SR_Status__c();
        objSRStatus.Name = 'AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
        objSRStatus.Code__c = 'AWAITING_HEALTH_INSURANCE_TO_BE_UPLOADED';
        insert objSRStatus;
        
       step__c objStep = new Step__c();
       objStep.SR__c = objSR3.Id;
       objStep.Step_Template__c = objStepType1.id ;
       objStep.No_PR__c = false;
       objStep.status__c = objStatus.id;
       objStep.Closed_Date_Time__c = system.now();
       
       insert objStep;
       
       
       step__c s = [ Select id,step_name__c,Created_Date_Time__c,Closed_Date_Time__c,Sys_SR_Group__c from step__c where id =:objStep.id];
         
       Shipment__c thishp = new Shipment__c();
       thishp.step__c     = objStep.Id;
       thishp.Status__c   = 'Delivered';
       insert thishp;
    
     database.executeBatch(new Courier_KPI_BatchCls());
    String sch = '20 30 8 10 2 ?';
    System.schedule('Courier KPI Scheduler',sch,new Courier_KPI_ScheduleCls());
        
   Test.stopTest();
    
    }

}