/******************************************************************************************
 *  BatchRelStatusUpdateClass
 
 *  Author      : Veera
 *  Company     : techCarrot
 *  Date        : 2015-10-17     
 *  Modification 
        History :
----------------------------------------------------------------------
   Version  Date         Author             Remarks                                                 
  =======   ==========   =============  ==================================
  
*******************************************************************************************/


global with sharing class BatchRelStatusUpdate implements Database.Batchable <sObject > {

 global List <Relationship__c> start(Database.BatchableContext BC) {

  List < Relationship__c > lstRels = [select id, Active__c,Object_Contact__c,Relationship_Type__c from Relationship__c where (End_Date__c = YESTERDAY and Active__c = true and Relationship_Type__c IN('Has Temporary Employee', 'Has DIFC Seconded')) OR (Start_Date__c = TODAY AND Relationship_Type__c='Has DIFC Sponsored Employee')];
  return lstRels;
 }

 global void execute(Database.BatchableContext BC, list < Relationship__c> lstRels) {
  List < Relationship__c > lstRelsToUpdate = new List < Relationship__c > ();
  set<id> sponsoredIds = new set<id>();
  try {
      for (Relationship__c objSRel: lstRels) {
           if(objSRel.Relationship_Type__c!='Has DIFC Sponsored Employee'){
              objSRel.Active__c = false;
              objSRel.IsUpdated_ByBatch__c = true;
              lstRelsToUpdate.add(objSRel);
            }else{    
              sponsoredIds.add(objSRel.Object_Contact__c);
            }
     } 
    
    
    map <id,contact> mapConUpdate = new map<id,contact>();
    
    for(Document_Details__c docDetailsList:[select id,contact__c,contact__r.Visa_Number__c,contact__r.Visa_Expiry_Date__c,Document_Number__c,EXPIRY_DATE__c from Document_Details__c where Contact__c in :sponsoredIds AND createdDate=TODAY AND Document_Type__c ='Employment Visa' ORDER BY createdDate desc]){
        contact con = new contact();
        set<id> conIds =  new set<id>();
        if(docDetailsList.contact__r.Visa_Number__c != docDetailsList.Document_Number__c && docDetailsList.contact__r.Visa_Expiry_Date__c !=docDetailsList.EXPIRY_DATE__c){
             if(!conIds.contains(docDetailsList.contact__c)){
             con.id = docDetailsList.contact__c;
             con.Visa_Number__c =docDetailsList.Document_Number__c;
             con.Visa_Expiry_Date__c = docDetailsList.EXPIRY_DATE__c;
             conIds.add(docDetailsList.contact__c);              
             mapConUpdate.put(con.id,con);
             
            }
        }
    }   
    if (!lstRelsToUpdate.isEmpty())
       update lstRelsToUpdate;

    if (!mapConUpdate.isEmpty())
       update mapConUpdate.values();


    
  }catch (Exception ex) {
   Log__c objLog = new Log__c();
   objLog.Type__c = 'Schedule Batch Relationship status Update ';
   objLog.Description__c = 'Exceptio is : ' + ex.getMessage() + '\nLine # ' + ex.getLineNumber();
   insert objLog;
  }

 }


 global void finish(Database.BatchableContext BC) {}
}