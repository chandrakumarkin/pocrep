/*******************************************************
*Class Name : AppointmentCreationUtilityClsTest 
*Description : test class for AppointmentCreationUtilityCls
*Modified: 
*Assembla : #7365
*********************************************************/
@isTest
public class AppointmentCreationUtilityClsTest {

    public static List<Service_Request__c> srList;
    public static List<Service_Request__c> ExpresssrList;
    
    public static List<Appointment_Schedule_Counter__c> appCounterlist;
    public static List<Appointment__c> appList;
    
    public static List<Appointment_Schedule_Counter__c> expappCounterlist;
    public static List<Appointment__c> expappList;
    public static List<step__c> expobjStepList;
    public static List<step__c> normalobjStepList;
    public static Account objAccount;
    public static Contact objContact;
    public static SR_Template__c objTemplate1;
    public static Step_Template__c objStepType1;
    public static  Status__c objAwaiting;
    public static void CreateData() {

        objAccount = new Account();
        objAccount.Name = 'Test Account';
        objAccount.Phone = '1234567890';
        insert objAccount;

        string conRT;
        for (RecordType objRT: [select Id, Name, DeveloperName from RecordType where DeveloperName = 'GS_Contact' AND SobjectType = 'Contact']){
                 conRT = objRT.Id;
            }
           
        objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = conRT;
        insert objContact;

        objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate1.Active__c = true;
        objTemplate1.SR_Group__c = 'GS';
        insert objTemplate1;

        objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Medical Fitness Test scheduled';
        objStepType1.Code__c = 'Medical Fitness Test scheduled';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Medical Fitness Test scheduled';
        insert objStepType1;

        Status__c objReschStatus = new Status__c();
        objReschStatus.Name = 'Re-Scheduled';
        objReschStatus.Type__c = 'Start';
        objReschStatus.Code__c = 'RE-SCHEDULED';
        insert objReschStatus;
        
        objAwaiting = new Status__c();
        objAwaiting.Name = 'AWAITING_REVIEW';
        objAwaiting.Type__c = 'Start';
        objAwaiting.Code__c = 'AWAITING_REVIEW';
        insert objAwaiting;
    }
    static testMethod void unittest1(){
        CreateData();
        Test.startTest();
        srList = new List<Service_Request__c>();
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Email__c = 'testsr123@difc.com';
        objSR1.Contact__c = objContact.Id;
        objSR1.Express_Service__c = false;
        objSR1.SR_Template__c = objTemplate1.Id;
        objSR1.Statement_of_Undertaking__c = true;
        srList.add(objSR1);
       
        insert srList;
        
        normalobjStepList = new List<step__c>();
        step__c objStep = new Step__c();
        objStep.SR__c = srList[0].Id;
        objStep.Biometrics_Required__c = 'No';
        objStep.Step_Template__c = objStepType1.id;
        objStep.No_PR__c = false;
        objStep.status__c = objAwaiting.id;
        objStep.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep);
        
        step__c objStep2 = new Step__c();
        objStep2.SR__c = srList[0].Id;
        objStep2.Biometrics_Required__c = 'yes';
        objStep2.Step_Template__c = objStepType1.id;
        objStep2.No_PR__c = false;
        objStep2.status__c = objAwaiting.id;
        objStep2.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep2);
        
        step__c objStep3 = new Step__c();
        objStep3.SR__c = srList[0].Id;
        objStep3.Biometrics_Required__c = 'yes';
        objStep3.Step_Template__c = objStepType1.id;
        objStep3.No_PR__c = false;
        objStep3.status__c = objAwaiting.id;
        objStep3.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep3);
        
        step__c objStep4 = new Step__c();
        objStep4.SR__c = srList[0].Id;
        objStep4.Biometrics_Required__c = 'yes';
        objStep4.Step_Template__c = objStepType1.id;
        objStep4.No_PR__c = false;
        objStep4.status__c = objAwaiting.id;
        objStep4.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep4);
       
        insert normalobjStepList;
        
        appList = new List<Appointment__c>();
        Datetime settodaytime = Datetime.newInstance(2020,06,09,08,00,00);
        
        Appointment__c app1 = new Appointment__c();
        app1.Appointment_StartDate_Time__c = settodaytime.addminutes(05);
        app1.Appointment_EndDate_Time__c  = app1.Appointment_StartDate_Time__c.addminutes(5);
        app1.step__c = normalobjStepList[0].id;
        appList.add(app1);
        Appointment__c app2 = new Appointment__c();
        app2.Appointment_StartDate_Time__c =   settodaytime.addhours(0).addminutes(15);
        app2.Appointment_EndDate_Time__c  =  app2.Appointment_StartDate_Time__c.addminutes(5);
        app2.step__c = normalobjStepList[0].id;
        appList.add(app2);
        Appointment__c app3 = new Appointment__c();
        app3.Appointment_StartDate_Time__c =   settodaytime.addhours(20).addminutes(17);
        app3.Appointment_EndDate_Time__c  =   app3.Appointment_StartDate_Time__c.addminutes(5);
        app3.step__c = normalobjStepList[0].id;
        appList.add(app3);
        
        insert appList;
        
        appCounterlist= new List<Appointment_Schedule_Counter__c>();
        Appointment_Schedule_Counter__c appCounter = new Appointment_Schedule_Counter__c();
        appCounter.Appointment_Time__c = appList[0].Appointment_EndDate_Time__c;
        appCounter.Application_Type__c = 'Express';
        appCounter.Appointment__c = appList[0].Id;
        appCounterlist.add(appCounter);
        Appointment_Schedule_Counter__c appCounter1 = new Appointment_Schedule_Counter__c();
        appCounter1.Appointment_Time__c = appList[0].Appointment_EndDate_Time__c;
        appCounter1.Application_Type__c = 'Express';
        appCounter1.Appointment__c = appList[1].Id;
        appCounterlist.add(appCounter1);
        Appointment_Schedule_Counter__c appCounter2 = new Appointment_Schedule_Counter__c();
        appCounter2.Appointment_Time__c = appList[0].Appointment_EndDate_Time__c;
        appCounter2.Application_Type__c = 'Express';
        appCounter2.Appointment__c = appList[2].Id;
        appCounterlist.add(appCounter2);
        Appointment_Schedule_Counter__c resappCounter3 = new Appointment_Schedule_Counter__c();
        resappCounter3.Appointment_Time__c = app3.Appointment_EndDate_Time__c;
        resappCounter3.Application_Type__c = 'Express';
        resappCounter3.Appointment__c = appList[2].Id;
        resappCounter3.Type__c='RE-SCHEDULED';
        appCounterlist.add(resappCounter3);
        
        insert appCounterlist;
        
        BusinessHours bh =[select id from BusinessHours where IsDefault=true];

        List<step__c> stepitr = [select id,SR__c,createdDate,Step_Code__c,Biometrics_Required__c from step__c where step_code__c = 'Medical Fitness Test scheduled' and sr__r.Express_Service__c=false];
        for(step__c s:stepitr){
            AppointmentCreationUtilityCls.normalTestTime = settodaytime;
             AppointmentCreationUtilityCls.MedicalAppointmentCreate(s);
        }
        Test.stopTest();
    }
    static testMethod void unittest2() {
        CreateData();
        Test.startTest();
        srList = new List<Service_Request__c>();
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Email__c = 'testsr123@difc.com';
        objSR1.Contact__c = objContact.Id;
        objSR1.Express_Service__c = true;
        objSR1.SR_Template__c = objTemplate1.Id;
        objSR1.Statement_of_Undertaking__c = true;
        srList.add(objSR1);
       
        insert srList;
        
        normalobjStepList = new List<step__c>();
        step__c objStep = new Step__c();
        objStep.SR__c = srList[0].Id;
        objStep.Biometrics_Required__c = 'No';
        objStep.Step_Template__c = objStepType1.id;
        objStep.No_PR__c = false;
        objStep.status__c = objAwaiting.id;
        objStep.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep);
        
        step__c objStep2 = new Step__c();
        objStep2.SR__c = srList[0].Id;
        objStep2.Biometrics_Required__c = 'yes';
        objStep2.Step_Template__c = objStepType1.id;
        objStep2.No_PR__c = false;
        objStep2.status__c = objAwaiting.id;
        objStep2.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep2);
        
        step__c objStep3 = new Step__c();
        objStep3.SR__c = srList[0].Id;
        objStep3.Biometrics_Required__c = 'yes';
        objStep3.Step_Template__c = objStepType1.id;
        objStep3.No_PR__c = false;
        objStep3.status__c = objAwaiting.id;
        objStep3.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep3);
        
        step__c objStep4 = new Step__c();
        objStep4.SR__c = srList[0].Id;
        objStep4.Biometrics_Required__c = 'yes';
        objStep4.Step_Template__c = objStepType1.id;
        objStep4.No_PR__c = false;
        objStep4.status__c = objAwaiting.id;
        objStep4.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep4);
       
        insert normalobjStepList;
        
        appList = new List<Appointment__c>();
        Datetime settodaytime = Datetime.newInstance(2020,06,09,08,00,00);
        Appointment__c app1 = new Appointment__c();
        app1.Appointment_StartDate_Time__c = settodaytime.addminutes(07);
        app1.Appointment_EndDate_Time__c  = app1.Appointment_StartDate_Time__c.addminutes(5);
        app1.step__c = normalobjStepList[0].id;
        appList.add(app1);
        Appointment__c app2 = new Appointment__c();
        app2.Appointment_StartDate_Time__c =   settodaytime.addhours(0).addminutes(17);
        app2.Appointment_EndDate_Time__c  =  app2.Appointment_StartDate_Time__c.addminutes(5);
        app2.step__c = normalobjStepList[0].id;
        appList.add(app2);
        Appointment__c app3 = new Appointment__c();
        app3.Appointment_StartDate_Time__c =   settodaytime.addhours(20).addminutes(17);
        app3.Appointment_EndDate_Time__c  =   app3.Appointment_StartDate_Time__c.addminutes(5);
        app3.step__c = normalobjStepList[0].id;
        appList.add(app3);
        
        insert appList;
        
        appCounterlist= new List<Appointment_Schedule_Counter__c>();
        Appointment_Schedule_Counter__c appCounter = new Appointment_Schedule_Counter__c();
        appCounter.Appointment_Time__c = appList[0].Appointment_EndDate_Time__c;
        appCounter.Application_Type__c = 'Express';
        appCounter.Appointment__c = appList[0].Id;
        appCounterlist.add(appCounter);
        Appointment_Schedule_Counter__c appCounter1 = new Appointment_Schedule_Counter__c();
        appCounter1.Appointment_Time__c = appList[0].Appointment_EndDate_Time__c;
        appCounter1.Application_Type__c = 'Express';
        appCounter1.Appointment__c = appList[1].Id;
        appCounterlist.add(appCounter1);
        Appointment_Schedule_Counter__c appCounter2 = new Appointment_Schedule_Counter__c();
        appCounter2.Appointment_Time__c = appList[0].Appointment_EndDate_Time__c;
        appCounter2.Application_Type__c = 'Express';
        appCounter2.Appointment__c = appList[2].Id;
        appCounterlist.add(appCounter2);
        Appointment_Schedule_Counter__c resappCounter3 = new Appointment_Schedule_Counter__c();
        resappCounter3.Appointment_Time__c = app3.Appointment_EndDate_Time__c;
        resappCounter3.Application_Type__c = 'Express';
        resappCounter3.Appointment__c = appList[2].Id;
        resappCounter3.Type__c='RE-SCHEDULED';
        appCounterlist.add(resappCounter3);
        
        insert appCounterlist;
        
        BusinessHours bh =[select id from BusinessHours where IsDefault=true];

        List<step__c> stepitr = [select id,SR__c,createdDate,Step_Code__c,Biometrics_Required__c from step__c where step_code__c = 'Medical Fitness Test scheduled' and sr__r.Express_Service__c=true];
        for(step__c s:stepitr){
            AppointmentCreationUtilityCls.expressTestTime = settodaytime;
            AppointmentCreationUtilityCls.MedicalAppointmentCreate(s);
        }
        Test.stopTest();
    }
    static testMethod void unittest3() {
        CreateData();
        Test.startTest();
        srList = new List<Service_Request__c>();
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Email__c = 'testsr123@difc.com';
        objSR1.Contact__c = objContact.Id;
        objSR1.Express_Service__c = false;
        objSR1.SR_Template__c = objTemplate1.Id;
        objSR1.Statement_of_Undertaking__c = true;
        srList.add(objSR1);
       
        insert srList;
        
        normalobjStepList = new List<step__c>();
        step__c objStep = new Step__c();
        objStep.SR__c = srList[0].Id;
        objStep.Biometrics_Required__c = 'No';
        objStep.Step_Template__c = objStepType1.id;
        objStep.No_PR__c = false;
        objStep.status__c = objAwaiting.id;
        objStep.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep);
        
        step__c objStep2 = new Step__c();
        objStep2.SR__c = srList[0].Id;
        objStep2.Biometrics_Required__c = 'yes';
        objStep2.Step_Template__c = objStepType1.id;
        objStep2.No_PR__c = false;
        objStep2.status__c = objAwaiting.id;
        objStep2.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep2);
        
        step__c objStep3 = new Step__c();
        objStep3.SR__c = srList[0].Id;
        objStep3.Biometrics_Required__c = 'yes';
        objStep3.Step_Template__c = objStepType1.id;
        objStep3.No_PR__c = false;
        objStep3.status__c = objAwaiting.id;
        objStep3.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep3);
        
        step__c objStep4 = new Step__c();
        objStep4.SR__c = srList[0].Id;
        objStep4.Biometrics_Required__c = 'yes';
        objStep4.Step_Template__c = objStepType1.id;
        objStep4.No_PR__c = false;
        objStep4.status__c = objAwaiting.id;
        objStep4.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep4);
       
        insert normalobjStepList;

        BusinessHours bh =[select id from BusinessHours where IsDefault=true];
        Datetime settodaytime = Datetime.newInstance(2020,06,09,08,00,00);
        List<step__c> stepitr = [select id,SR__c,createdDate,Step_Code__c,Biometrics_Required__c from step__c where step_code__c = 'Medical Fitness Test scheduled' and sr__r.Express_Service__c=false];
        for(step__c s:stepitr){
            AppointmentCreationUtilityCls.normalTestTime = settodaytime.addhours(20).addminutes(17);
            AppointmentCreationUtilityCls.MedicalAppointmentCreate(s);
        }
        Test.stopTest();
    }
    static testMethod void unittest4() {
        CreateData();
        Test.startTest();
        srList = new List<Service_Request__c>();
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Email__c = 'testsr123@difc.com';
        objSR1.Contact__c = objContact.Id;
        objSR1.Express_Service__c = true;
        objSR1.SR_Template__c = objTemplate1.Id;
        objSR1.Statement_of_Undertaking__c = true;
        srList.add(objSR1);
       
        insert srList;
        
        normalobjStepList = new List<step__c>();
        step__c objStep = new Step__c();
        objStep.SR__c = srList[0].Id;
        objStep.Biometrics_Required__c = 'No';
        objStep.Step_Template__c = objStepType1.id;
        objStep.No_PR__c = false;
        objStep.status__c = objAwaiting.id;
        objStep.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep);
        
        step__c objStep2 = new Step__c();
        objStep2.SR__c = srList[0].Id;
        objStep2.Biometrics_Required__c = 'yes';
        objStep2.Step_Template__c = objStepType1.id;
        objStep2.No_PR__c = false;
        objStep2.status__c = objAwaiting.id;
        objStep2.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep2);
        
        step__c objStep3 = new Step__c();
        objStep3.SR__c = srList[0].Id;
        objStep3.Biometrics_Required__c = 'yes';
        objStep3.Step_Template__c = objStepType1.id;
        objStep3.No_PR__c = false;
        objStep3.status__c = objAwaiting.id;
        objStep3.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep3);
        
        step__c objStep4 = new Step__c();
        objStep4.SR__c = srList[0].Id;
        objStep4.Biometrics_Required__c = 'yes';
        objStep4.Step_Template__c = objStepType1.id;
        objStep4.No_PR__c = false;
        objStep4.status__c = objAwaiting.id;
        objStep4.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep4);
       
        insert normalobjStepList;
        
        BusinessHours bh =[select id from BusinessHours where IsDefault=true];
        Datetime settodaytime = Datetime.newInstance(2020,06,09,08,00,00);
        List<step__c> stepitr = [select id,SR__c,createdDate,Step_Code__c,Biometrics_Required__c from step__c where step_code__c = 'Medical Fitness Test scheduled' and sr__r.Express_Service__c=true];
        for(step__c s:stepitr){
            AppointmentCreationUtilityCls.expressTestTime = settodaytime.addhours(20).addminutes(17);
            AppointmentCreationUtilityCls.MedicalAppointmentCreate(s);
        }
        Test.stopTest();
    }
    static testMethod void unittest5() {
        CreateData();
        Test.startTest();
        srList = new List<Service_Request__c>();
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Email__c = 'testsr123@difc.com';
        objSR1.Contact__c = objContact.Id;
        objSR1.Express_Service__c = false;
        objSR1.SR_Template__c = objTemplate1.Id;
        objSR1.Statement_of_Undertaking__c = true;
        srList.add(objSR1);
       
        insert srList;
        
        normalobjStepList = new List<step__c>();
        step__c objStep = new Step__c();
        objStep.SR__c = srList[0].Id;
        objStep.Biometrics_Required__c = 'No';
        objStep.Step_Template__c = objStepType1.id;
        objStep.No_PR__c = false;
        objStep.status__c = objAwaiting.id;
        objStep.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep);
        
        step__c objStep2 = new Step__c();
        objStep2.SR__c = srList[0].Id;
        objStep2.Biometrics_Required__c = 'yes';
        objStep2.Step_Template__c = objStepType1.id;
        objStep2.No_PR__c = false;
        objStep2.status__c = objAwaiting.id;
        objStep2.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep2);
        
        step__c objStep3 = new Step__c();
        objStep3.SR__c = srList[0].Id;
        objStep3.Biometrics_Required__c = 'yes';
        objStep3.Step_Template__c = objStepType1.id;
        objStep3.No_PR__c = false;
        objStep3.status__c = objAwaiting.id;
        objStep3.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep3);
        
        step__c objStep4 = new Step__c();
        objStep4.SR__c = srList[0].Id;
        objStep4.Biometrics_Required__c = 'yes';
        objStep4.Step_Template__c = objStepType1.id;
        objStep4.No_PR__c = false;
        objStep4.status__c = objAwaiting.id;
        objStep4.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep4);
       
        insert normalobjStepList;

        BusinessHours bh =[select id from BusinessHours where IsDefault=true];
        Datetime settodaytime = Datetime.newInstance(2020,06,10,08,00,00);
        List<step__c> stepitr = [select id,SR__c,createdDate,Step_Code__c,Biometrics_Required__c from step__c where step_code__c = 'Medical Fitness Test scheduled' and sr__r.Express_Service__c=false];
          Integer i = 5;
        for(step__c s:stepitr){
            AppointmentCreationUtilityCls.normalTestTime = settodaytime.addhours(0).addminutes(i);
            i=i+5;
            AppointmentCreationUtilityCls.MedicalAppointmentCreate(s);
        }
        Test.stopTest();
    }
    static testMethod void unittest6() {
        CreateData();
        Test.startTest();
        srList = new List<Service_Request__c>();
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.Email__c = 'testsr123@difc.com';
        objSR1.Contact__c = objContact.Id;
        objSR1.Express_Service__c = true;
        objSR1.SR_Template__c = objTemplate1.Id;
        objSR1.Statement_of_Undertaking__c = true;
        srList.add(objSR1);
       
        insert srList;
        
        normalobjStepList = new List<step__c>();
        step__c objStep = new Step__c();
        objStep.SR__c = srList[0].Id;
        objStep.Biometrics_Required__c = 'No';
        objStep.Step_Template__c = objStepType1.id;
        objStep.No_PR__c = false;
        objStep.status__c = objAwaiting.id;
        objStep.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep);
        
        step__c objStep2 = new Step__c();
        objStep2.SR__c = srList[0].Id;
        objStep2.Biometrics_Required__c = 'yes';
        objStep2.Step_Template__c = objStepType1.id;
        objStep2.No_PR__c = false;
        objStep2.status__c = objAwaiting.id;
        objStep2.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep2);
        
        step__c objStep3 = new Step__c();
        objStep3.SR__c = srList[0].Id;
        objStep3.Biometrics_Required__c = 'yes';
        objStep3.Step_Template__c = objStepType1.id;
        objStep3.No_PR__c = false;
        objStep3.status__c = objAwaiting.id;
        objStep3.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep3);
        
        step__c objStep4 = new Step__c();
        objStep4.SR__c = srList[0].Id;
        objStep4.Biometrics_Required__c = 'yes';
        objStep4.Step_Template__c = objStepType1.id;
        objStep4.No_PR__c = false;
        objStep4.status__c = objAwaiting.id;
        objStep4.AppointmentCreation__c = 'Create';
        normalobjStepList.add(objStep4);
       
        insert normalobjStepList;
        
        BusinessHours bh =[select id from BusinessHours where IsDefault=true];
        Datetime settodaytime = Datetime.newInstance(2020,06,10,08,00,00);
        List<step__c> stepitr = [select id,SR__c,createdDate,Step_Code__c,Biometrics_Required__c from step__c where step_code__c = 'Medical Fitness Test scheduled' and sr__r.Express_Service__c=true];
        Integer i = 10;
        for(step__c s:stepitr){
            AppointmentCreationUtilityCls.expressTestTime = settodaytime.addhours(0).addminutes(i);
            i=i+5;
            AppointmentCreationUtilityCls.MedicalAppointmentCreate(s);
        }
        Test.stopTest();
    }
}