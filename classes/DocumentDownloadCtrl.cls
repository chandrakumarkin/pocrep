/**
 * @author      Azfer 
 * @version     1.1.0
 * @since       08/01/2019
 */
public class  DocumentDownloadCtrl{
    // Properties which are being used in the VFP
    public String zipFileName {get; set;}
    public String zipContent {get; set;}

    //Getter method for the attachments property
    public List<Attachment> getAttachments() {
        //Getting the ID and SR no passed as reference 
        Id ServiceRequestId = ApexPages.currentPage().getParameters().get('srid'); 
        zipFileName = ApexPages.currentPage().getParameters().get('srno'); 
        
        //Collections, variables that will be used.
        Map<String, String> MapDocSaveToDocRemove = new Map<String, String>();
        Map<String, Attachment> MapNametoAttachment = new Map<String, Attachment>();
        
        List<String> ListDocumentNameToDownload = new List<String>();
        List<Id> ListSRDocId = new List<Id>();
        
        Service_Request__c ServiceRequestObj;
        
        // getting the details related to the SR, Service type and record type name is needed as its being used in Custom metadata
        try{
            ServiceRequestObj = [SELECT Id, Service_Type__C, Record_Type_Name__c FROM Service_Request__c WHERE Id =: ServiceRequestId];
        }catch(Exception Ex){ system.debug('Exception :: '+ Ex.getMessage()); }
        
        //Getting the document information from the custom metadata.
        //The documents will be the one which will be displayed and downloaded
        List<Attachment_Detail__mdt> ListAttachmentDetail = [SELECT Documents_Name__c
                                                                FROM Attachment_Detail__mdt 
                                                                WHERE DeveloperName =: ServiceRequestObj.Record_Type_Name__c
                                                                AND Service_Type__c =: ServiceRequestObj.Service_Type__C ];
        if( ListAttachmentDetail.size() > 0 ){
            
            //This list will contains the name of the documents.
            //The document names are kept ';'separated in the custom settings.
            if( ListAttachmentDetail[0].Documents_Name__c.contains(';') ){
                for(String Str : ListAttachmentDetail[0].Documents_Name__c.Split(';') ){
                    ListDocumentNameToDownload.add( Str.trim() );
                }
            }else{ ListDocumentNameToDownload.add( ListAttachmentDetail[0].Documents_Name__c.trim() ); }
        }

        //If there is not document mentioned in the custom metadata an empty list will be sent back
        // the page will then show an alert.
        if( ListDocumentNameToDownload.size() == 0 ){ return new List<Attachment>(); }

        //Getting the information related to the file precedence from the custom metadata
        for( Attachment_Detail_Precedence__mdt AttachmentDetailPrecedence : [SELECT Precedence__c
                                                                            FROM Attachment_Detail_Precedence__mdt 
                                                                            WHERE DeveloperName =: ServiceRequestObj.Record_Type_Name__c
                                                                            AND Service_Type__c =: ServiceRequestObj.Service_Type__C ])
        {
            //The following map will contain the name of file as key which will take precedence
            //over the file which is stored as the value against that name of the file
            //the precendece structure is like this:
            // A=>B;C=>D (for more than one file)
            // or
            // A=>B (for a single file)
            if( AttachmentDetailPrecedence.Precedence__c.contains(';') ){
                for(String Str : AttachmentDetailPrecedence.Precedence__c.split(';') ){
                    if( Str.contains( '=>' ) ){ MapDocSaveToDocRemove.put( Str.split('=>')[0].trim() , Str.split('=>')[1].trim() ); }
                }
            }else{ MapDocSaveToDocRemove.put( AttachmentDetailPrecedence.Precedence__c.split('=>')[0].trim(), AttachmentDetailPrecedence.Precedence__c.split('=>')[1].trim() ); }
        }
        
        //Getting the SRDocs Id related to that SR which will then be used to get the attachments
        for(SR_Doc__c SRDocObj : [SELECT Id 
                                FROM SR_Doc__c 
                                WHERE Service_Request__c =: ServiceRequestId
                                AND Name IN : ListDocumentNameToDownload] )
        {
            ListSRDocId.add( SRDocObj.Id );
        }

        //Getting the attachment and using the SR Doc name to store the file as it will be the same name used to display,download the file
        for( Attachment AttachmentObj : [SELECT Id, BodyLength, ContentType, LastModifiedDate, Name,
                                            ParentId, Parent.Name 
                                        FROM Attachment
                                        WHERE ParentId IN : ListSRDocId
                                        ORDER By Parent.Name ] )
        {
            if( MapNametoAttachment.containsKey( AttachmentObj.Parent.Name ) ){
                Attachment TempAttachmentObj = MapNametoAttachment.get( AttachmentObj.Parent.Name );
                if( TempAttachmentObj.LastModifiedDate < AttachmentObj.LastModifiedDate ){ MapNametoAttachment.put( AttachmentObj.Parent.Name, AttachmentObj ); }
            }else{
                MapNametoAttachment.put( AttachmentObj.Parent.Name, AttachmentObj );
            }
        }

        //This following code checks the precedence.
        //if the name which is the key is found then it removes the value part from the attachment map
        // the value part of the precedence map is key in Attachment map
        if( MapDocSaveToDocRemove.size() > 0 ){
            for(String Key : MapDocSaveToDocRemove.KeySet() )
            {
                if( MapNametoAttachment.containsKey( Key ) ){
                    MapNametoAttachment.remove( MapDocSaveToDocRemove.get( Key ) );
                }
            }
        }
        //retruning the values which are finalized 
        return MapNametoAttachment.values();
    }
    
    //This remove action (remoting fuction/Javascript remoting) is being used to get the attachment body, 
    //convert it into a blob and return it as a reponse to the page.
    //This function is being called from the JS part of the page.
    @RemoteAction
    public static AttachmentWrapper getAttachment(String attId) {
        
        Attachment att = [select Id, Parent.Name, Name, ContentType, Body
                          from Attachment
                          where Id = :attId];
        
        String Extension = '';
        if( att.Name != null && att.Name.contains('.') ){
        	Extension = att.Name.SubString(att.Name.LastIndexOf('.'),att.Name.length());    
        }

        AttachmentWrapper attWrapper = new AttachmentWrapper();
        attWrapper.attEncodedBody = EncodingUtil.base64Encode(att.body);
        attWrapper.attName = att.Parent.Name + Extension;
                          
        return attWrapper;
    }
    
    //Wrapper Class which will be sent from the remote function.
    public class AttachmentWrapper {
        public String attEncodedBody {get; set;}
        public String attName {get; set;}
    }
}