public class DIFCPortalSupportController {

    /* Configuration */

    // The API endpoint for the reCAPTCHA service
    private static String baseUrl = 'https://www.google.com/recaptcha/api/verify'; 

    // The keys you get by signing up for reCAPTCHA for your domain
    private static String privateKey = Label.Captcha_Private_Key;
    public String publicKey { 
        get { 
            return Label.Captcha_Public_Key;
        }
    } 

    /* Implementation */
    
    // Simple form fields for the example form
    public String companyName { get; set; }
    public String fullName { get; set; }
    public String email { get; set; }
    public String phoneNumber { get; set; }
    public String subject { get; set; }
    public String caseType { get; set; }
    public String description { get; set; }
    
    public Case c { get; set; }
    //Error message for invalid captcha entry
    public String captchaError { get; set; }            
    
    // Create properties for the non-VF component input fields generated
    // by the reCAPTCHA JavaScript.
    public String challenge { 
        get {
            return ApexPages.currentPage().getParameters().get('recaptcha_challenge_field');
        }
    }
    public String response  { 
        get {
            return ApexPages.currentPage().getParameters().get('recaptcha_response_field');
        }
    }
    
    // Whether the submission has passed reCAPTCHA validation or not
    public Boolean verified { get; private set; }
    
    public DIFCPortalSupportController() {
        this.verified = false;
        this.captchaError = '';
        
    }
    
    public PageReference verify() {
        System.debug('reCAPTCHA verification attempt');
        // On first page load, form is empty, so no request to make yet
        if ( challenge == null || response == null ) { 
            System.debug('reCAPTCHA verification attempt with empty form');
            return null; 
        }
                    
        HttpResponse r = makeRequest(baseUrl,
            'privatekey=' + privateKey + 
            '&remoteip='  + remoteHost + 
            '&challenge=' + challenge +
            '&response='  + response
        );
        
        if ( r!= null ) {
            this.verified = (r.getBody().startsWithIgnoreCase('true'));
        }
        
        if(this.verified) {
            // If they pass verification, you might do something interesting here
            // Or simply return a PageReference to the "next" page
            addNewSupportTicket();
            
            return null;//PageReference pageRef = new PageReference('../DIFC/communitylogin'); pageRef.setRedirect(True);
        }
        else {
            // stay on page to re-try reCAPTCHA
            this.captchaError = 'Invalid captcha: please retry';
            return null; 
        }
    }
    
    public Boolean addNewSupportTicket() {

        c = new Case();
        c.Company_Name__c    		= this.companyName;
        c.Full_Name__c       		= this.fullName;
        c.Email__c    		 		= this.email;
        c.Phone_Number__c    		= this.phoneNumber;
        //c.Type               = this.caseType;
        c.Type_of_Login_Issue__c 	= this.caseType;
        c.Subject            		= this.subject;
        c.Description        		= this.description;
        
        system.debug('Case Info: ' + c.Subject + ' ,' + c.Description +', User Description:'+ this.description );
        
        try {
        	//String RecordId;      
           // for(RecordType objRT : [select Id,Name from RecordType where (DeveloperName='Portal_User' OR DeveloperName='Individual') AND SobjectType='Contact' AND IsActive=true])
            	//RecordId = objRT.Id;
        	list<Contact> conlist = [Select id,Email from Contact where Email=:this.email AND (RecordType.DeveloperName='Individual' OR RecordType.DeveloperName='Portal_User')];
        	if(conlist!=null && conlist.size()>0)
        		c.ContactId = conlist[0].id;
            insert c;
            
            this.companyName 	= '';
            this.fullName 		= '';
            this.email			= '';
            this.phoneNumber 	= '';
            this.caseType 		= '';
            this.subject 		= '';
            this.description 	= '';
            this.verified       =false;
            
        }catch(Exception e) {
            System.debug('New Portal Case Error: ' + c);
            return false;
        }
        
        return true;
    }
    

    public PageReference reset() {
        return null; 
    }   

    /* Private helper methods */
    
    @TestVisible private static HttpResponse makeRequest(string url, string body)  {
        HttpResponse response = null;
        HttpRequest req = new HttpRequest();   
        req.setEndpoint(url);
        req.setMethod('POST');
        req.setBody (body);
        try {
            Http http = new Http();
            response = http.send(req);
            System.debug('reCAPTCHA response: ' + response);
            System.debug('reCAPTCHA body: ' + response.getBody());
        } catch(System.Exception e) {
            System.debug('ERROR: ' + e);
        }
        return response;
    }   
        
    @TestVisible private String remoteHost { 
        get { 
            String ret = '127.0.0.1';
            // also could use x-original-remote-host 
            Map<String, String> hdrs = ApexPages.currentPage().getHeaders();
            if (hdrs.get('x-original-remote-addr')!= null)
                ret =  hdrs.get('x-original-remote-addr');
            else if (hdrs.get('X-Salesforce-SIP')!= null)
                ret =  hdrs.get('X-Salesforce-SIP');
            return ret;
        }
    }
}