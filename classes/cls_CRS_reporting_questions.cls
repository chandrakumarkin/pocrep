public with sharing class cls_CRS_reporting_questions {

public CRS_Question__c ObjQuestion{get;set;}
public Account ObjAccount{get;set;}
public string LicenseActivity{get;set;}


    public cls_CRS_reporting_questions(ApexPages.StandardController controller) 
    {
    
   ObjQuestion= new CRS_Question__c();
    PageLoad();
    }
    
    public void PageLoad()
    {
    

    
                for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Registration_License_No__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                             Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                             Contact.Account.Qualifying_Type__c,Contact.Account.Qualifying_Purpose_Type__c,Contact.Account.Contact_Details_Provided__c,
                             Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  
                             from User where Id=:userinfo.getUserId()])
                             {
                                 ObjAccount= objUsr.Contact.Account;
                                 LicenseActivity=objUsr.Contact.Account.License_Activity__c;
                             }
                             if(ObjAccount!=null)
                             {
                             ObjQuestion.Account__c=ObjAccount.id;
                             ObjQuestion.Company_Type__c=ObjAccount.Company_Type__c;
                             ObjQuestion.CRS_Year__c=System.Label.CRS_Year_ID;
                             }
                         List<CRS_Question__c> ListCRS=new    List<CRS_Question__c>();     
                     
                     if(ObjAccount!=null)
                      ListCRS=[select Designation__c,Entity_is_not_a_Reporting_Financial_Inst__c,Last_Name__c,Status__c,Account__c,Address_current_based_on_documentary__c,Clearly_identified_group_s__c,Closed_during_the_reporting_period__c,Company_Type__c,Complied_with_for_the_reporting_period__c,CRS_Year__c,Custodial_Institution__c,Depository_Institution__c,Designated_Person_nominated__c,Detailed_reasons__c,Due_diligence_not_conducted__c,Pre_existing_Accounts_during_the_reporti__c,due_diligence_procedures__c,Email_Address__c,Entity_Name__c,High_value_applied_to_Lower_Value__c,Insurance__c,Investment_Entity__c,Jurisdiction__c,Maintained_for_the_entity_s_obligations__c,First_Name__c,Null_return_being_filed__c,Number_is_unknown__c,Offence_to_provide_information_that_is_f__c,Operating_Law_DIFC_Law_No_7_of_2018__c,Please_provide_detailed_reasons__c,due_diligence_detailed_reasons__c,Please_provide_the_reason__c,Please_select_relevant_option_2__c,due_diligence_the_relevant_option__c,Reason_for_filing_a_Null_return__c,Registration_No__c,Reportable_Accounts_for_the_reporting_pe__c,Reporting_Financial_Institution_type__c,Resident_Address_test_used__c,Please_select_the_relevant_option__c,Shared_with_The_Dubai_Financial_Services__c,Telephone_No__c,The_number_of_Undocumented_Reports__c,Third_Party_Service_Provider__c,Title__c,Total_number_of_Excluded_Accounts__c,Undocumented_Accounts_being_reported__c from CRS_Question__c where CRS_Year__c=:System.Label.CRS_Year_ID and Account__c=:ObjAccount.id];
                         
                          if(!ListCRS.isEmpty())
                          {
                             ObjQuestion=ListCRS[0];
                          }
    
    }
    
      public PageReference RediectURL() 
     {
        
         PageReference UploadPage;
         List<CRS_Form__c> ListCrs=[select Id,Is_Null_return__c,Status__c from CRS_Form__c  where CRS_Year__c=:label.CRS_Year_ID and Account__c=:ObjAccount.id];
         if(!ListCrs.IsEmpty())
         {
            
            if(ListCrs[0].Status__c=='Submitted')
            {
                  UploadPage=new PageReference('/'+ListCrs[0].id) ;
            }
            else
            {
                 UploadPage=page.CRS_Upload_Form; 
                            
            }
            
            UploadPage.setRedirect(true);
           
          }
          else if(ObjQuestion.id!=null)
          {
          UploadPage=Page.CRS_Question_View;
            UploadPage.getParameters().put('id',ObjQuestion.id);
          
          }
         return UploadPage;
         
         
     }
     
    
      public PageReference SaveRecord() 
     {
     try
     {
     // &&
     ///ObjQuestion.Offence_to_provide_information_that_is_f__c==true &&
     //ObjQuestion.Operating_Law_DIFC_Law_No_7_of_2018__c==true &&
     //ObjQuestion.Designated_Person_nominated__c==true
     
     if(ObjQuestion.Shared_with_The_Dubai_Financial_Services__c==true)
     {
         ObjQuestion.Status__c='Submitted';
          upsert ObjQuestion;
            
         if(ObjQuestion.Null_return_being_filed__c=='Yes')
         {
            return  submitNull();
         }
         else
         {
         PageReference UploadPage;
          if(ObjQuestion.Reporting_Financial_Institution_type__c.contains('Not a Reporting Financial Institution'))
          {
              UploadPage=Page.CRS_reporting_questions_file;
               UploadPage.getParameters().put('id',ObjQuestion.id);
             
              
          }
            else
            {
                 UploadPage=Page.CRS_Upload_Form;
                    
            }   
              UploadPage.setRedirect(true);
            return UploadPage;
          
         }
     }
     else
     {
  ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Declaration Required.'));
    
     }
     }
     catch(Exception e)
     {
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, e.getMessage()));
     }
     
     return null;
     }
      /*  
      public void UploadFile() 
     {
     
     if(myfile!=null)
     {
       Attachment a = new Attachment(parentId = ObjQuestion.id, name=myfile.name, body = myfile.body);
       insert a;
      }
         
      
        if(file!=null)
        {
        ContentVersion conVer = new ContentVersion();
        conVer.versionData = file;
        conVer.title = 'CRS upload';
        insert conVer;
        
        // First get the Content Document Id from ContentVersion Object
Id conDoc = [SELECT ContentDocumentId FROM ContentVersion WHERE Id =:conVer.Id].ContentDocumentId;
//create ContentDocumentLink  record 
ContentDocumentLink conDocLink = New ContentDocumentLink();
conDocLink.LinkedEntityId =  ObjQuestion.id;// Specify RECORD ID here i.e Any Object ID (Standard Object/Custom Object)
conDocLink.ContentDocumentId = conDoc;  //ContentDocumentId Id from ContentVersion
conDocLink.shareType = 'V';
insert conDocLink;

        
        
       
        
        }        
        
         
     }*/    
     public PageReference submitNull() 
     {
     
           CRS_Form__c crsform=new CRS_Form__c();
           crsform.CRS_Year__c=label.CRS_Year_ID;
           crsform.Account__c=ObjAccount.id;
           crsform.CRS_Question__c=ObjQuestion.id;
           crsform.Official_FI_Legal_Name__c=ObjAccount.Name;
           crsform.FI_ID__c=ObjAccount.Registration_License_No__c;
           crsform.Status__c='Submitted';
           crsform.Is_Null_return__c='YES';
           upsert crsform;
           return new PageReference('/'+crsform.id) ;
          
     
     }
     
    
   //************************************************
   //Control flow 
  //***************************************************************************************************************
    //Reporting Financial Institution type
       public  void RFI_Selected()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjQuestion.Reporting_Financial_Institution_type__c=passedParam1 ;
    }
    
     public  void NullReturn_Other()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjQuestion.Reason_for_filing_a_Null_return__c=passedParam1 ;
    }
     public  void ThirdParty_Yes()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjQuestion.Third_Party_Service_Provider__c=passedParam1 ;
    }
    
    public  void due_diligence_procedures_yes()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjQuestion.due_diligence_procedures__c=passedParam1 ;
        if(passedParam1 =='Yes')
        ObjQuestion.High_value_applied_to_Lower_Value__c=null;
        
    }
     public  void clearly_identified_group_other_check()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjQuestion.Please_select_the_relevant_option__c=passedParam1 ;
    }
   
    public  void due_diligence_procedures_check()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjQuestion.due_diligence_the_relevant_option__c=passedParam1 ;
    }
    
    //Were any due diligence procedures applied to Pre-existing Accounts during the reporting period?
     public  void Pre_existingAccountss_check()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParamva');
        ObjQuestion.Pre_existing_Accounts_during_the_reporti__c=passedParam1 ;
    }
    
    //Were Undocumented Accounts reported for the reporting period?
     public  void Undocumented_Accounts_check()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjQuestion.Undocumented_Accounts_being_reported__c=passedParam1 ;
    }
    
    //Please specify the total number of Excluded Accounts (non-reportable accounts) for the reporting period
     public  void Excluded_Accounts_Accounts_check()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjQuestion.Total_number_of_Excluded_Accounts__c=passedParam1 ;
    }
    
    //Were due diligence procedures for High Value Accounts applied to Lower Value Accounts ?
     public  void High_Value_Accounts_check()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjQuestion.High_value_applied_to_Lower_Value__c=passedParam1 ;
    }
    
     //Were any due diligence procedures applied for Lower Value Accounts?
     public  void  Lower_Value_Accounts_check()
    {
        string passedParam1 = Apexpages.currentPage().getParameters().get('myParam');
        ObjQuestion.Resident_Address_test_used__c=passedParam1 ;
    }
    
    
 
 
    
    
    

}