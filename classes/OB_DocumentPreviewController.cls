/******************************************************************************************
 *  Author         :   Merul
 *  Company        :   HexaBPM
 *  Date           :   9-April-2017
 *  Description    :   Apex Controller for Preview Documents in LEX
 ********************************************************************************************/

public without sharing class OB_DocumentPreviewController 
{
	/*** Method to fetch all the SR Docs related to the Service Request ***/
	@AuraEnabled
	public static RespondWrap getSRDocs( String reqWrapPram) 
    {
		 //reqest wrpper
        RequestWrapper reqWrap = (RequestWrapper) JSON.deserializeStrict(reqWrapPram, RequestWrapper.class);
        String srId = reqWrap.srId;
        RespondWrap respWrap = new RespondWrap();
		list<HexaBPM__SR_Doc__c> SRDocs = new list<HexaBPM__SR_Doc__c>();
		for(HexaBPM__SR_Doc__c docObj :[select id, Name, createddate, lastmodifieddate, 
									    HexaBPM__Document_Description_External__c, 
									    HexaBPM__Is_Not_Required__c, 
									    HexaBPM__Document_Master__r.Name, 
									    HexaBPM__Document_Master__r.Document_Description__c, 
									    HexaBPM__Service_Request__c, 
									    HexaBPM__Service_Request__r.HexaBPM__Internal_Status_Name__c, 
									    HexaBPM__Service_Request__r.HexaBPM__External_Status_Name__c, 
									    HexaBPM__Doc_ID__c, 
									    HexaBPM__Sys_IsGenerated_Doc__c, 
									    Download_URL__c, 
									    HexaBPM__Customer_Comments__c, 
									    HexaBPM__Comments__c, 
									    HexaBPM__Document_Type__c, 
									    HexaBPM__Rejection_Reason__c, 
									    HexaBPM__Status__c, 
									    HexaBPM__Letter_Template_Id__c,
									    Amendment_Name__c,
									    Copied_from_InPrinciple__c,
									    In_Principle_Document__c,In_Principle_Document__r.HexaBPM__Doc_ID__c,
                                        HexaBPM__Document_Name__c
								    from HexaBPM__SR_Doc__c 
                                   where HexaBPM__Service_Request__c = :srId order by Name]) {
			SRDocs.add(docObj);
		}
		
		respWrap.SRDocs = SRDocs;
		return respWrap;
	}
	
	public class RequestWrapper 
    {
		@AuraEnabled 
        public String srId { get; set; }
		
	}
    public class RespondWrap 
    {
		@AuraEnabled 
        public list<HexaBPM__SR_Doc__c> SRDocs { get; set; }
		
	}
}