/******************************************************************************************
 *  Author   : Saima Hidayat
 *  Company  : NSI JLT
 *  Date     : 03-Sep-2014   
                
*******************************************************************************************/
//Declaration Form controller class

public class RoCDeclarationCls {
    
    /* Start of Properties for Dynamic Flow */
    public boolean isChangesSaved = false;
    public boolean isProcessFlow{get;set;}
    public string strPageId{get;set;}
    public string strHiddenPageIds{get;set;}
    public string strActionId{get;set;}
    public string strNavigatePageId{get;set;}
    public map<string,string> mapParameters;
    public Service_Request__c objSRFlow{get;set;}
    /* End of Properties for Dynamic Flow */

  String SRId;
  public Integer RowId{get;set;}
  public List<Declaration_Signatory__c> decSignatory {get; set;} 
  public RoCDeclarationCls()
  {
     SRId=ApexPages.currentPage().getParameters().get('Id');
     decSignatory = new List<Declaration_Signatory__c>(); 
     List <Declaration_Signatory__c> decSign = [Select id,Signatory_Name__c,Capacity__c 
                                                from Declaration_Signatory__c 
                                                where Service_Request__c =:SRId];
     if(decSign.size()==0)
     {
     decSignatory.add(new Declaration_Signatory__c(Service_Request__c = SRId));    
     }
     else
     {
     decSignatory.addAll(decSign);
     }
      /* Start of Properties Initialization for Dynamic Flow */
      mapParameters = new map<string,string>();
      if(apexpages.currentPage().getParameters()!=null)
      mapParameters = apexpages.currentPage().getParameters();
      
      objSRFlow = new Service_Request__c();
      objSRFlow.Id = SRId;
      if(objSRFlow.Id!=null){
         strPageId = mapParameters.get('PageId');
           if(mapParameters!=null && mapParameters.get('FlowId')!=null){
             set<string> SetstrFields = Cls_Evaluate_Conditions.FetchObjectFields(mapParameters.get('FlowId'),'Service_Request__c');
             string strQuery = 'select Id';
             if(SetstrFields!=null && SetstrFields.size()>0){
               for(String strFld:SetstrFields){
                 if(strFld.toLowerCase()!='id')
              strQuery += ','+strFld.toLowerCase();
               }
             }
             if(!strQuery.contains('customer__c'))
               strQuery += ',customer__c';
             
             strQuery = strQuery+',finalizeamendmentflg__c from Service_Request__c where Id=:SRId';
             system.debug('strQuery===>'+strQuery);
             for(Service_Request__c SR:database.query(strQuery)){
               objSRFlow = SR;
              
             }
             strHiddenPageIds = PreparePageBlockUtil.getHiddenPageIds(mapParameters.get('FlowId'),objSRFlow);
           }
        }
        /* End of Properties Initialization for Dynamic Flow */
     
  }
  
     public Component.Apex.PageBlock getDyncPgMainPB(){
        PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = objSRFlow; 
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        return objPB.getDyncPgMainPB();
    }
    
    
    public pagereference goTopage(){
                if(strNavigatePageId!=null && strNavigatePageId!=''){
                                PreparePageBlockUtil objSidebarRef = new PreparePageBlockUtil();
                                PreparePageBlockUtil.strSideBarPageId = strNavigatePageId;
                                PreparePageBlockUtil.objSR = objSRFlow;
                                return objSidebarRef.getSideBarReference();
                }
                return null;
    }
    
    public pagereference DynamicButtonAction(){
      if(strActionId!=null && strActionId!=''){
          boolean isNext = false;
          boolean isAllowToproceed = false;
        for(Section_Detail__c objSec:[select id,Name,Component_Label__c,Navigation_Direction__c from Section_Detail__c where Id=:strActionId]){
          if(objSec.Navigation_Direction__c=='Forward'){
            isNext = true;
          }else{
            isNext = false;
          }
        }
        if(isNext==true){
           for(Declaration_Signatory__c decsg:[select id from Declaration_Signatory__c where Service_Request__c=:objSRFlow.Id]){
             isAllowToproceed = true;
           }
        }else{
          isAllowToproceed = true;
        }
        if(isAllowToproceed==true){
          PreparePageBlockUtil.FlowId = mapParameters.get('FlowId'); 
            PreparePageBlockUtil.PageId = mapParameters.get('PageId');
            PreparePageBlockUtil.objSR = objSRFlow;
            PreparePageBlockUtil.ActionId = strActionId;
            
            PreparePageBlockUtil objPB = new PreparePageBlockUtil();
            pagereference pg = objPB.getButtonAction();
            system.debug('pg==>'+pg);
            return pg;
        }else{
          ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Please add atleast one signatory to proceed.'));
          return null;
        }
        }
        return null;
    }
    
  
   public void add_rows() {
  
        decSignatory.add(new Declaration_Signatory__c(Service_Request__c = SRid));    

   }
     
  public void save_close(){ 
  	if(!objSRFlow.finalizeAmendmentFlg__c){      
      for(Integer i=0; i<decSignatory.size();i++)
      {
          if(decSignatory[i].Signatory_Name__c == NULL || decSignatory[i].Capacity__c == NULL)
          {
              Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Please fill in the required fields below!'));
              return;
          }
      }
      
       upsert decSignatory;          
       objSRFlow.Sys_ROC_Declared__c = true;
       update objSRFlow;
       //Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.CONFIRM,'Declaration details are saved sucessfully!'));
       return;
  	}else{
      	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Your request has been finalised and cannot be updated.'));
        return;
    }
  }
  

  
  public void deleteRow(){
	if(!objSRFlow.finalizeAmendmentFlg__c){
	   if(decSignatory.size() == 1){
	       Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error,'Atleast one signatory is required!'));
	       return;
	   }else{
	       List <Declaration_Signatory__c> decSign = [Select id from Declaration_Signatory__c where id= :decSignatory[RowId].id];
	       if(decSign.size()==0)
	       decSignatory.remove(RowId);
	       else{
	           delete decSign;
	           decSignatory.remove(RowId);
	       }
	   }
	}else{
      	ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Your request has been finalised and cannot be updated.'));
        return;
    }
}



}