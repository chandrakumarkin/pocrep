/***********************************************************************************
 *  Author   : Sravan
 *  Company  : Pwc
 *  Date     : 01-sep-2014
  --------------------------------------------------------------------------------------------------------------------------
 Description : Thic class handels the custom code required for creation of Establishment Card from ROC requests 
 ---------------------------------------------------------------------------------------------------------------------------
 Change Description
 Version      ModifiedBy    Date                Description
 V1.0         Sravan        08-06-2017          Linking the Price Items for Proper Calculation of Portal Balance
 V1.1         Sravan        31-07-2017          Created New method to submit Establishment Card from Step. 
 v1.2         Veera         28-11-2017          mapping avial courier to free courier service field
 V1.3         Veera         11-12-2017          Update establishment card price 
 v1.4         Selva         08-01-2020          Update the class to avoid duplicate price item creation #8108
 v1.5		  Shikha		11-04-2021			#11621 - Capture mobile number for License Renewal for establishment card.
 ************************************************************************************/
 
public without sharing class CC_EstablishmentCard_Utils{
    
    
    Public static string insertEstablishmentCardDraftFromReq(step__c stp){
        
        Map<string,string> srStatusMap = new map<string,string>(); // Map with status code as Key and Id as Value ** SR Statuses **
        List<SR_Template__c> srTemplate; // Template details of the SR that needs to be created
        string srRecordTypeTobecreated;
        string srRecordTypeNameTobecreated;
        Service_Request__c serviceRequest = new Service_Request__c(SR_group__c='GS');
        
        if(stp.sr__r.Record_Type_Name__c == 'Application_of_Registration'){
            srRecordTypeTobecreated = 'New_Index_Card';
            srRecordTypeNameTobecreated = 'New Index Card';
         }else if(stp.sr__r.Record_Type_Name__c == 'Change_of_Entity_Name' || stp.sr__r.Record_Type_Name__c == 'License_Renewal'){
            srRecordTypeTobecreated = 'Index_Card_Other_Services';
            srRecordTypeNameTobecreated = 'Index Card Other Services';
         }           
         for(SR_Status__c srStatus: [select id,code__c from SR_Status__c where Code__c IN  ('SUBMITTED','DRAFT')]){
            srStatusMap.put(srStatus.code__c,srStatus.id);
         }
         
         if(srRecordTypeTobecreated !=null)
             srTemplate = [select id from sr_template__c where SR_RecordType_API_Name__c =:srRecordTypeTobecreated];    
         
         
                     
        for(service_Request__c sr :[select id,Name,Avail_Courier_Services__c,Use_Registered_Address__c,Would_you_like_to_opt_our_free_couri__c,Consignee_FName__c,Consignee_LName__c,Courier_Mobile_Number__c,Courier_Cell_Phone__c,Apt_or_Villa_No__c,Customer__c,Customer__r.Active_License__r.Name,customer__r.Index_Card__r.Name,Business_Activity__c,Address_Changed__c,
                                                    Customer__r.Active_License__r.License_Expiry_Date__c,Email__c,Send_SMS_To_Mobile__c,Record_type_Name__c,Establishment_Card_Express__c,customer__r.Index_Card__r.Valid_To__c,Is_Applicant_Salary_Above_AED_20_000__c                                                
                                                    from Service_Request__c where id=:stp.sr__c])
        {                          
                    
                    // Prepare Service Request
                    
                    if(srTemplate !=null && !srTemplate.isEmpty()){
                       serviceRequest.SR_Template__c =  srTemplate[0].id;
                        serviceRequest.Customer__c = sr.Customer__c;
                        serviceRequest.License_Number__c = serviceRequest.Customer__r.Active_License__r.Name  ;
                        serviceRequest.Current_License_Expiry_Date__c = sr.Customer__r.Active_License__r.License_Expiry_Date__c;
                        serviceRequest.Email__c = sr.Email__c;
                        serviceRequest.Send_SMS_To_Mobile__c =  SR.Send_SMS_To_Mobile__c ;
                        serviceRequest.Express_Service__c = SR.Establishment_Card_Express__c=='Yes'? true : false;
                        serviceRequest.Avail_Courier_Services__c = sr.Avail_Courier_Services__c;
                        serviceRequest.Would_you_like_to_opt_our_free_couri__c = (sr.Avail_Courier_Services__c == 'No'||sr.Avail_Courier_Services__c =='' ||sr.Avail_Courier_Services__c ==null)? 'No':'Yes';// v1.2
                        serviceRequest.Consignee_FName__c  = SR.Consignee_FName__c;                   
                        serviceRequest.Use_Registered_Address__c = SR.Use_Registered_Address__c;
                        serviceRequest.Consignee_FName__c = SR.Consignee_FName__c;
                        serviceRequest.Consignee_LName__c = SR.Consignee_LName__c;
                        serviceRequest.Courier_Mobile_Number__c = SR.Courier_Mobile_Number__c;
                        serviceRequest.Courier_Cell_Phone__c = SR.Courier_Cell_Phone__c;
                        serviceRequest.Apt_or_Villa_No__c  = SR.Apt_or_Villa_No__c;
                        serviceRequest.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get(srRecordTypeNameTobecreated).getRecordTypeId();
                        serviceRequest.internal_SR_status__c = srStatusMap.get('DRAFT');
                        serviceRequest.External_SR_Status__c =srStatusMap.get('DRAFT');
                        serviceRequest.Is_Applicant_Salary_Above_AED_20_000__c = 'Yes';
                        serviceRequest.Establishment_Card_No__c = sr.customer__r.Index_Card__r.Name;
                        serviceRequest.Establishment_Card_Expiry_Date__c = sr.customer__r.Index_Card__r.Valid_To__c;
                        serviceRequest.License_Number__c = sr.customer__r.Active_License__r.Name;
                        serviceRequest.Current_License_Expiry_Date__c = sr.customer__r.Active_License__r.License_Expiry_Date__c;
                        serviceRequest.Linked_SR__c = sr.id;
                        if(sr.Record_type_Name__c == 'Change_of_Entity_Name'){
                            serviceRequest.Service_Category__c = 'Amendment';
                            serviceRequest.Trade_Name_Changed__c = true;
                        }else if(sr.Record_type_Name__c == 'License_Renewal' && sr.Is_Applicant_Salary_Above_AED_20_000__c !='Yes'){  
                            serviceRequest.Service_Category__c = 'Renewal';                                
                        }else if(sr.Record_type_Name__c == 'License_Renewal' && sr.Is_Applicant_Salary_Above_AED_20_000__c =='Yes'){
                            //serviceRequest.Service_Category__c = 'Renewal & Amendment'; 
                            serviceRequest.Service_Category__c = 'Renewal'; //V1.3                      
                            serviceRequest.Address_Changed__c = sr.Address_Changed__c;
                            serviceRequest.Business_Activity__c = sr.Business_Activity__c;
                        }
                        serviceRequest.Quantity__c = 0;
                    }
        
            }
            
            if(serviceRequest!=null){
                SavePoint sp;
                try{
                    sp = database.setSavepoint();
                    insert serviceRequest;
                    
                    Service_request__c AORSR = new Service_request__c();
                    AORSR.id = stp.sr__r.id;
                    AORSR.Linked_SR__c = serviceRequest.id; 
                    update  AORSR ;
                    
                    return 'Success';
                }catch(DMLexception e){
                    Database.RollBack(sp);
                    return e.getMessage();                  
                }
            }else{
                return 'Error occured while creating a service request.Please contact system administrator';
            }    
    } 
    
    
    
    
    Public static string createEstablishmentCardFromRocRequest(step__c stp){
        
        set<string> documentMasters;
        string result; 
        string docErrorMsg;  
        documentMasters = new Set<string>(); 
        Map<string,Establishment_Card_Components__c>  establishmentCardMigrationComp = Establishment_Card_Components__c.getAll();   
        docErrorMsg = 'Please upload the following document(s) - ';
        if(establishmentCardMigrationComp.containsKey(stp.Sr__r.Record_Type_Name__c) && establishmentCardMigrationComp.get(stp.Sr__r.Record_Type_Name__c).Validate_Docs__c !=null){
            documentMasters.addAll(establishmentCardMigrationComp.get(stp.Sr__r.Record_Type_Name__c).Validate_Docs__c.split(','));
        }
        if(documentMasters!=null && documentMasters.size()>0){
            for(string docName: documentMasters)
                docErrorMsg+='\"'+docName+'\" ';
        }   
          
        /*if(stp.Sr__r.Record_Type_Name__c == 'Application_of_Registration'){
            documentMasters = new Set<string>{'Signed Commercial Lincense','Signed Certificate of Incorporation_Registration'};
            docErrorMsg = 'Please upload Signed Commercial License and Signed Certificate of Incorporation';
        }    
        else if(stp.Sr__r.Record_Type_Name__c == 'Change_of_Entity_Name' || stp.Sr__r.Record_Type_Name__c == 'License_Renewal') { //|| stp.Sr__r.Record_Type_Name__c == 'License_Renewal' 
            documentMasters = new Set<string>{'Signed Commercial License'};   
            docErrorMsg = 'Please upload Signed Commercial License';      
        } 
        
        */               
        if(documentMasters!=null && documentMasters.size()>0){
            integer docCount = [select count() from SR_Doc__c where Document_Master__r.Code__c in :documentMasters and Doc_ID__c =null and Service_Request__c =:stp.sr__c];
            if(docCount >0 && docErrorMsg!=null)
                return docErrorMsg;
        }    
        result = 'Success';
        if(stp !=null){
               
            string stepSerialized = Json.Serialize(stp);   
            createEstablishmentCardFromRocRequestAsync(stepSerialized);
            
            return result;
        }
        return result;
        
               /*
               //string srRecordTypeTobecreated;
               //string srRecordTypeNameTobecreated;
               Map<string,set<string>> documentMastersandProductsList = new  Map<string,set<string>>();   // Collects required documents  and Productsfor copying  to the establishment Card
               
               set<string> requiredProducts = new set<string>();
               set<string> requiredDocuments = new set<string>();
               //Map<string,Establishment_Card_Components__c>  establishmentCardMigrationComp = Establishment_Card_Components__c.getAll();                   
               Map<string,string> srStatusMap = new map<string,string>(); // Map with status code as Key and Id as Value ** SR Statuses **           
               // Collecting list of documents and products that needs to be copied from the ROC Request to GS Request   
               string recordTypeName = stp.Sr__r.Record_Type_Name__c;
               
               if(establishmentCardMigrationComp.containsKey(recordTypeName) && establishmentCardMigrationComp.get(recordTypeName).product_Master__c !=null)
                requiredProducts.addAll(establishmentCardMigrationComp.get(recordTypeName).product_Master__c.split(','));
               if(establishmentCardMigrationComp.containsKey(recordTypeName) && establishmentCardMigrationComp.get(recordTypeName).Document_Master__c !=null)
                requiredDocuments.addAll(establishmentCardMigrationComp.get(recordTypeName).Document_Master__c.split(',')); 
               
               if(requiredProducts!=null && !requiredProducts.isEmpty())
                 documentMastersandProductsList.put('Products',requiredProducts);
               if(requiredDocuments !=null && !requiredDocuments.isEmpty())
                documentMastersandProductsList.put('Documentmaster',requiredDocuments);              
                
                
               
                
                for(SR_Status__c srStatus: [select id,code__c from SR_Status__c where Code__c IN  ('SUBMITTED','DRAFT')]){
                    srStatusMap.put(srStatus.code__c,srStatus.id);
                }   
                                
                
                // Definitions
                service_request__c serviceRequest = new Service_Request__c(SR_group__c='GS'); // internal_SR_Status__c=srStatusMap.get('Draft').id,external_sr_status__c=srStatusMap.get('Draft').id,
                List<Amendment__c> amendmentList = new List<Amendment__c>();    
                List<SR_Price_Item__c> pricingList = new List<SR_Price_Item__c>();
                Map<id,SR_Price_Item__c> pricingMap = new  Map<id,SR_Price_Item__c>();
                List<SR_Doc__c> srDocList = new List<SR_Doc__c>();  
                Map<id,attachment> attachmentMap = new Map<id,Attachment>();             
                set<string> GSOAuthorizedPassportNo;
               
                
                for(service_Request__c sr :[select id,Name,Avail_Courier_Services__c,Would_you_like_to_opt_our_free_couri__c,Use_Registered_Address__c,Consignee_FName__c,Consignee_LName__c,Courier_Mobile_Number__c,Courier_Cell_Phone__c,Apt_or_Villa_No__c,Customer__c,Customer__r.Active_License__r.Name,
                                                    Customer__r.Active_License__r.License_Expiry_Date__c,Email__c,Send_SMS_To_Mobile__c,Record_type_Name__c,Establishment_Card_Express__c,
                                                    (select id,Quantity__c from Service_Requests1__r where (Record_Type_Name__c ='New_Index_Card' or Record_Type_Name__c='Index_Card_Other_Services') and Internal_Status_Name__c = 'Draft'),
                                                    (select id,Family_Name__c,Given_Name__c,Job_Title__c,Passport_No__c,Nationality_list__c,Email_Address__c,Phone__c,recordTypeID,Status__c,RecordType.developerName,serviceRequest__r.Record_Type_Name__c,Person_Email__c from Amendments__r),
                                                    (select id,Amendment__c,Service_Request__c,Document_Master__c,Doc_ID__c,customer__c,Status__c,Name,Amendment__r.Passport_No__c,Sys_IsGenerated_Doc__c,Document_Master__r.code__c from SR_Docs__r),
                                                    (select id,ServiceRequest__c,Product__c,Price__c,Price_in_USD__c,Pricing_Line__c,SAP_PR__c,SAP_SO__c,Status__c,Sys_Added_through_Code__c,Transaction_Date__c,product__r.productcode from SR_Price_Items1__r)
                                                    from Service_Request__c where id=:stp.sr__c])
                {                    
                    
                          
                      
                   parentSRDetails = sr;
                 
                    
                    if(sr.Service_Requests1__r !=null && sr.Service_Requests1__r.size() ==1){
                        serviceRequest = sr.Service_Requests1__r[0];
                        serviceRequest.Quantity__c =0;
                    }
                 
                    if(stp.sr__r.Record_Type_Name__c=='Application_of_Registration')GSOAuthorizedPassportNo = new set<string>();
                    // Copy Amendments
                    for(Amendment__c amd : sr.Amendments__r){
                        if(amd.RecordType.developerName=='GSO_Authorized_Signatory' && amd.serviceRequest__r.Record_Type_Name__c =='Application_of_Registration'){  // if application of Registration
                            amendmentList.add(amd.clone(false,false,false,false));
                            serviceRequest.Quantity__c +=1;
                            if(amd.Passport_No__c !=null)GSOAuthorizedPassportNo.add(amd.Passport_No__c.tolowerCase());
                        }
                    } 
                    
                                      
                    
                    // Copy SR Docs
                    
                    for(SR_Doc__c srDoc: sr.SR_Docs__r){
                        if(srDoc.Document_Master__r.code__c !=null && (documentMastersandProductsList.get('Documentmaster').contains(srDoc.Document_Master__r.code__c) || (srDoc.Amendment__c !=null && srDoc.Amendment__r.Passport_No__c !=null &&  GSOAuthorizedPassportNo.contains(srDoc.Amendment__r.Passport_No__c.toLowerCase()) && srDoc.Name.contains('Passport Copy')))){
                            srDocList.add(srDoc.clone(false,false,false,false));
                            if(srDoc.Doc_ID__c !=null)
                                attachmentMap.put(srDoc.Doc_ID__c,null);
                        }
                    }
                    
                     // Copy Pricing Line // V1.0
                    if(sr.SR_Price_Items1__r !=null){
                        SR_Price_Item__c srpNew;
                        for(SR_Price_Item__c srpOld :sr.SR_Price_Items1__r){
                            srpNew = new SR_Price_Item__c();
                            if(documentMastersandProductsList.containsKey('Products') && documentMastersandProductsList.get('Products').contains(srpOld.product__r.productcode)){
                                srpNew = srpOld.clone(false,false,false,false);
                                srpNew.Linked_SR_Price_Item__c = srpOld.id;
                                pricingMap.put(srpOld.id,srpNew);
                                //pricingList.add(srpTemp);
                            }  
                                
                        }
                    }                    
                }    
                
                
                if(attachmentMap !=null && attachmentMap.size()>0){
                    For(Attachment att :[select id,Body,Name,ContentType,Description,ParentId,IsPrivate from attachment where id in: attachmentMap.KeySet()]){
                        attachmentMap.put(att.id,att);
                    }
                }                 
                // Save SR 
                
                result = SaveandSubmitServiceRequestDetails(serviceRequest,amendmentList,srDocList,pricingMap,attachmentMap,srStatusMap);
                      
            }
        if(result !='Success' && parentSRDetails!=null){
            Log__c errorLogcatch = new Log__c();
            errorLogcatch.Account__c = parentSRDetails.Customer__c;
            errorLogcatch.Description__c =  result;
            errorLogcatch.Type__c    = 'Establishment Card Submission from ROC request #'+parentSRDetails.Name;
            insert errorLogcatch;
        }        
        
        return result;*/
    }   
    
    Public static string SaveandSubmitServiceRequestDetails(service_request__c sr,List<Amendment__c> amndList,List<SR_Doc__c> SRDocList,Map<id,SR_Price_Item__c> pricingMap,Map<id,Attachment> attachMap,map<string,string> statusMap){
         Savepoint SP;
        try{
            SP = Database.SetSavepoint();
            //if(sr !=null)insert sr;
            if(sr !=null && sr.id !=null){
                sr.internal_SR_Status__c = statusMap.get('SUBMITTED');
                sr.External_sr_status__c = statusMap.get('SUBMITTED');
                sr.Submitted_Date__c = Date.Today();
                sr.Submitted_DateTime__c = dateTime.Now();
                update SR;
            }
            if(sr.id !=null){
                if(amndList !=null && !amndList.isEmpty()){
                    for(Amendment__c amnd: amndList){amnd.servicerequest__c = sr.id;}
                    insert amndList;
                }               
                if(SRDocList !=null && !SRDocList.isEmpty()){                    
                    for(SR_Doc__c doc: SRDocList){
                        doc.Service_Request__c = sr.id;                
                    }              
                    insert SRDocList ;
                    
                    Attachment attachTemp;
                    for(SR_Doc__c srDoc :[select id,Doc_ID__c from SR_Doc__c where service_Request__c =:sr.id]){
                        if(attachMap.containsKey(srDoc.Doc_ID__c) && attachMap.get(srDoc.Doc_ID__c) !=null){
                            //attachTemp = attachmentMap.get(srDoc.Doc_ID__c).clone(false,false,false,false);
                            attachTemp = new attachment();
                            attachTemp.parentID = srDoc.id;
                            attachTemp.body = attachMap.get(srDoc.Doc_ID__c).body;
                            attachTemp.Name = attachMap.get(srDoc.Doc_ID__c).Name;
                            attachTemp.Description = attachMap.get(srDoc.Doc_ID__c).Description;
                            //system.debug('Attach 1'+attachTemp);                           
                            attachMap.put(srDoc.Doc_ID__c,attachTemp);
                       }else if(attachMap.containsKey(srDoc.Doc_ID__c) && attachMap.get(srDoc.Doc_ID__c) ==null)
                            attachMap.remove(srDoc.Doc_ID__c);
                            
                    }
                    
                    if(!attachMap.values().isEmpty())
                        insert attachMap.values();
                      
                }
                if(pricingMap !=null && !pricingMap.IsEmpty()){
                    Map<Id,SR_Price_Item__c> updatenewPriceItem = new Map<Id,SR_Price_Item__c>();
                    
                    for(SR_Price_Item__c price : pricingMap.values()){price.Servicerequest__c = sr.id;}
                    // added v1.4 start
                    for(service_Request__c newSR :[select id,Name,(select id,ServiceRequest__c,Product__c,Price__c,Price_in_USD__c,Pricing_Line__c,SAP_PR__c,SAP_SO__c,Status__c,Sys_Added_through_Code__c,Transaction_Date__c,product__r.productcode from SR_Price_Items1__r)
                                            from Service_Request__c where id=:sr.id]){
                        for(SR_Price_Item__c prItem:newSR.SR_Price_Items1__r){
                            updatenewPriceItem.put(prItem.Id,prItem);
                        }
                    }
                    
                    if(updatenewPriceItem.size()>0){
                        List<SR_Price_Item__c> updateSRPriceitem  = new List<SR_Price_Item__c>();
                        List<SR_Price_Item__c> insertSRPriceitem  = new List<SR_Price_Item__c>();
                        for(SR_Price_Item__c itr:updatenewPriceItem.values()){
                        //if(pricingMap.get(itr.Id).Price__c!=null){
                            itr.Status__c='Blocked';
                            itr.Servicerequest__c = sr.id;
                            updateSRPriceitem.add(itr);
                        }
                        if(updateSRPriceitem.size()>0){
                            upsert updateSRPriceitem;
                        }
                         // added v1.4 end
                    }else{
                        insert pricingMap.values();
                    }
                    
                     
                    // Link New Price Items to Old Price Items V1.0
                    List<SR_Price_Item__c> srpOldListToUpdate = new  List<SR_Price_Item__c>();                   
                    for(id srpOldId : pricingMap.keySet()){
                        if(srpOldId !=null && pricingMap.get(srpOldId).id !=null)
                        srpOldListToUpdate.add(new SR_Price_Item__c(id=srpOldId,Linked_SR_Price_Item__c=pricingMap.get(srpOldId).id));
                    }
                    if(srpOldListToUpdate !=null && !srpOldListToUpdate.isEmpty()) update srpOldListToUpdate;
                    
                    
                } 
                
                
                
                
                
                              
            }else{
                Database.Rollback(SP);
                return 'Error occured while updating a service request.Please contact system administrator';
                
                 
            }
        }catch(DMLException e){
            Database.Rollback(SP);            
            return e.getMessage();
        }
        
        return 'Success';
        
    }  
    
    
 
    
    
    @InvocableMethod(label='Update License Renewal' description='Updates SR to enable establishment amendment conditions')
    public static void updateESTServiceRequest(List<ID> ids) {
        map<id,Service_Request__c> serviceRequestsToUpdate = new map<id,Service_Request__c>();
        
        for(Service_Request__c sr: [select id,customer__c from Service_Request__c where id in : ids]){
            serviceRequestsToUpdate.put(sr.customer__c,sr);
        }   
        
        Date lastYear = date.today().addYears(-1);
        boolean isUpdate;
        
        map<id,Service_Request__c> srToUpdate = new map<id,Service_Request__c>();
        if(serviceRequestsToUpdate !=null && !serviceRequestsToUpdate.isEmpty()){
        for(Service_Request__c sr:[select id,Customer__c,(select id,Amendment_Type__c from Amendments__r where Amendment_Type__c in ('Change of PO Box','Operating Location','Business Activity','Change of Business Activity','License Activity')) from service_Request__c where customer__c in:serviceRequestsToUpdate.keySet() and Record_Type_Name__c in ('Change_of_Address_Details','Change_of_Business_Activity') and External_Status_Name__c='Approved' and (Submitted_Date__c >=:lastYear and Submitted_Date__c<=:Date.Today())]){
            if(sr.Amendments__r.size()>0 && serviceRequestsToUpdate.containsKey(sr.customer__c)){
                serviceRequestsToUpdate.get(sr.customer__c).Is_Applicant_Salary_Above_AED_20_000__c = 'Yes';
                for(Amendment__c amd :sr.Amendments__r){
                    if(amd.Amendment_Type__c =='Change of PO Box' || amd.Amendment_Type__c == 'Operating Location')             
                        serviceRequestsToUpdate.get(sr.customer__c).Address_Changed__c = true;
                    else 
                        serviceRequestsToUpdate.get(sr.customer__c).Business_Activity__c = true;    
                }   
                isUpdate=true;              //srToUpdate.add(serviceRequestsToUpdate.get(sr.customer__c));                  
            }           
        }
        } 
        if(serviceRequestsToUpdate!=null && serviceRequestsToUpdate.Values().size()>0 && isUpdate==true) 
            update serviceRequestsToUpdate.values();         
    }
    
    //V1.1    
    @future
    public static void createEstablishmentCardFromRocRequestAsync(string stepSerialized){
        
        string result = '';
        service_request__c parentSRDetails = new service_request__c();
        Map<string,Establishment_Card_Components__c>  establishmentCardMigrationComp = Establishment_Card_Components__c.getAll(); 
        step__c stp = (step__c)Json.deserialize(stepSerialized,step__c.class);
        //CC_EstablishmentCard_Utils.createEstablishmentCardFromRocRequest(stp); 
        
       //string srRecordTypeTobecreated;
       //string srRecordTypeNameTobecreated;
       Map<string,set<string>> documentMastersandProductsList = new  Map<string,set<string>>();   // Collects required documents  and Productsfor copying  to the establishment Card
       
       set<string> requiredProducts = new set<string>();
       set<string> requiredDocuments = new set<string>();
       //Map<string,Establishment_Card_Components__c>  establishmentCardMigrationComp = Establishment_Card_Components__c.getAll();                   
       Map<string,string> srStatusMap = new map<string,string>(); // Map with status code as Key and Id as Value ** SR Statuses **           
       // Collecting list of documents and products that needs to be copied from the ROC Request to GS Request   
       string recordTypeName = stp.Sr__r.Record_Type_Name__c;
       
       if(establishmentCardMigrationComp.containsKey(recordTypeName) && establishmentCardMigrationComp.get(recordTypeName).product_Master__c !=null)
        requiredProducts.addAll(establishmentCardMigrationComp.get(recordTypeName).product_Master__c.split(','));
       if(establishmentCardMigrationComp.containsKey(recordTypeName) && (establishmentCardMigrationComp.get(recordTypeName).Document_Master__c !=null))
        requiredDocuments.addAll(establishmentCardMigrationComp.get(recordTypeName).Document_Master__c.split(',')); 
       if(establishmentCardMigrationComp.containsKey(recordTypeName) && (establishmentCardMigrationComp.get(recordTypeName).Document_Master1__c !=null)) // added veera
        requiredDocuments.addAll(establishmentCardMigrationComp.get(recordTypeName).Document_Master1__c.split(',')); 
       
       if(requiredProducts!=null && !requiredProducts.isEmpty())
         documentMastersandProductsList.put('Products',requiredProducts);
       if(requiredDocuments !=null && !requiredDocuments.isEmpty())
        documentMastersandProductsList.put('Documentmaster',requiredDocuments);              
        
        
       
        
        for(SR_Status__c srStatus: [select id,code__c from SR_Status__c where Code__c IN  ('SUBMITTED','DRAFT')]){
            srStatusMap.put(srStatus.code__c,srStatus.id);
        }   
                        
        
        // Definitions
        service_request__c serviceRequest = new Service_Request__c(SR_group__c='GS'); // internal_SR_Status__c=srStatusMap.get('Draft').id,external_sr_status__c=srStatusMap.get('Draft').id,
        List<Amendment__c> amendmentList = new List<Amendment__c>();    
        List<SR_Price_Item__c> pricingList = new List<SR_Price_Item__c>();
        Map<id,SR_Price_Item__c> pricingMap = new  Map<id,SR_Price_Item__c>();
        List<SR_Doc__c> srDocList = new List<SR_Doc__c>();  
        Map<id,attachment> attachmentMap = new Map<id,Attachment>();             
        set<string> GSOAuthorizedPassportNo;
       
        // v1.5 started
        for(service_Request__c sr :[select id,Name,Avail_Courier_Services__c,Would_you_like_to_opt_our_free_couri__c,Use_Registered_Address__c,Consignee_FName__c,Consignee_LName__c,Courier_Mobile_Number__c,Courier_Cell_Phone__c,Apt_or_Villa_No__c,Customer__c,Customer__r.Active_License__r.Name,Mobile_Number__c,
                                            Customer__r.Active_License__r.License_Expiry_Date__c,Email__c,Send_SMS_To_Mobile__c,Record_type_Name__c,Establishment_Card_Express__c,
                                            (select id,Quantity__c,Mobile_Number__c from Service_Requests1__r where (Record_Type_Name__c ='New_Index_Card' or Record_Type_Name__c='Index_Card_Other_Services') and Internal_Status_Name__c = 'Draft'),
                                            (select id,Family_Name__c,Given_Name__c,Job_Title__c,Passport_No__c,Nationality_list__c,Email_Address__c,Phone__c,recordTypeID,Status__c,RecordType.developerName,serviceRequest__r.Record_Type_Name__c,Person_Email__c from Amendments__r),
                                            (select id,Amendment__c,Service_Request__c,Document_Master__c,Doc_ID__c,customer__c,Status__c,Name,Amendment__r.Passport_No__c,Sys_IsGenerated_Doc__c,Document_Master__r.code__c from SR_Docs__r),
                                            (select id,ServiceRequest__c,Product__c,Price__c,Price_in_USD__c,Pricing_Line__c,SAP_PR__c,SAP_SO__c,Status__c,Sys_Added_through_Code__c,Transaction_Date__c,product__r.productcode from SR_Price_Items1__r)
                                            from Service_Request__c where id=:stp.sr__c])
        {                    
            
                  
              
           parentSRDetails = sr;
         
            
            if(sr.Service_Requests1__r !=null && sr.Service_Requests1__r.size() ==1){
                serviceRequest = sr.Service_Requests1__r[0];
                serviceRequest.Quantity__c =0;
                //v1.5 changes
                serviceRequest.Mobile_Number__c = sr.Mobile_Number__c;
            }
         	//v1.5 end
            if(stp.sr__r.Record_Type_Name__c=='Application_of_Registration')GSOAuthorizedPassportNo = new set<string>();
            // Copy Amendments
            for(Amendment__c amd : sr.Amendments__r){
                if(amd.RecordType.developerName=='GSO_Authorized_Signatory' && amd.serviceRequest__r.Record_Type_Name__c =='Application_of_Registration'){  // if application of Registration
                    amendmentList.add(amd.clone(false,false,false,false));
                    serviceRequest.Quantity__c +=1;
                    if(amd.Passport_No__c !=null)GSOAuthorizedPassportNo.add(amd.Passport_No__c.tolowerCase());
                }
            } 
            
                              
            
            // Copy SR Docs
            
            for(SR_Doc__c srDoc: sr.SR_Docs__r){
                if(srDoc.Document_Master__r.code__c !=null && (documentMastersandProductsList.get('Documentmaster').contains(srDoc.Document_Master__r.code__c) || (srDoc.Amendment__c !=null && srDoc.Amendment__r.Passport_No__c !=null &&  GSOAuthorizedPassportNo.contains(srDoc.Amendment__r.Passport_No__c.toLowerCase()) && srDoc.Name.contains('Passport Copy')))){
                    srDocList.add(srDoc.clone(false,false,false,false));
                    if(srDoc.Doc_ID__c !=null)
                        attachmentMap.put(srDoc.Doc_ID__c,null);
                }
            }
            
             // Copy Pricing Line // V1.0
            if(sr.SR_Price_Items1__r !=null){
                SR_Price_Item__c srpNew;
                for(SR_Price_Item__c srpOld :sr.SR_Price_Items1__r){
                    system.debug('----srpOld----'+srpOld.Id);
                    srpNew = new SR_Price_Item__c();
                    if(documentMastersandProductsList.containsKey('Products') && documentMastersandProductsList.get('Products').contains(srpOld.product__r.productcode)){
                        srpNew = srpOld.clone(false,false,false,false);
                        srpNew.Linked_SR_Price_Item__c = srpOld.id;
                        pricingMap.put(srpOld.id,srpNew);
                        //pricingList.add(srpTemp);
                    }  
                        
                }
            }                    
        }    
        
        
        if(attachmentMap !=null && attachmentMap.size()>0){
            For(Attachment att :[select id,Body,Name,ContentType,Description,ParentId,IsPrivate from attachment where id in: attachmentMap.KeySet()]){
                attachmentMap.put(att.id,att);
            }
        }                 
        // Save SR 
                
        result = SaveandSubmitServiceRequestDetails(serviceRequest,amendmentList,srDocList,pricingMap,attachmentMap,srStatusMap);
        
        if(result !='Success' && parentSRDetails!=null){
            Log__c errorLogcatch = new Log__c();
            errorLogcatch.Account__c = parentSRDetails.Customer__c;
            errorLogcatch.Description__c =  result;
            errorLogcatch.Type__c    = 'Establishment Card Submission from ROC request #'+parentSRDetails.Name;
            insert errorLogcatch;
        }
                      
           
    }
    
     public static void testsample(){
    Integer i=0;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
        i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    i++;
    }
}