@isTest
public with sharing class SLATrackerControllerTest {
    
    public static Case caseobj;
    public static Case_SLA__c sla;
    public static Case_SLA__c sla2;
    public static void init(){
        
        caseObj = new Case(
        GN_Type_of_Feedback__c = 'Complaint',
        Type = 'Parking',
        Status = 'In Progress',
        Origin = 'Phone',
        Subject = 'Testing',
        Full_Name__c='Raghav');
        
        insert caseObj;
        
        List<BusinessHours> busHours = [SELECT Id FROM BusinessHours WHERE isDefault=true LIMIT 1];
        
        sla= new Case_SLA__c(
        Parent__c=caseObj.id,
        Until__c=system.now(),
        Business_Hours_Id__c=busHours[0].Id, 
        From__c=system.now(),
        Due_Date_Time__c=system.now().addmonths(1));
        
        insert sla;
        
        sla2= new Case_SLA__c(
        Parent__c=caseObj.id,
        Until__c=null,
        Business_Hours_Id__c=busHours[0].Id, 
        From__c=system.now(),
        Due_Date_Time__c=system.now().addmonths(1));
        
        insert sla2;
        
    }
    
    @isTest static void test(){
        
        init();
        ApexPages.StandardController sc= new ApexPages.StandardController(caseObj);
        apexpages.currentPage().getParameters().put('Id',caseObj.id);
        SLATrackerController t1= new SLATrackerController(sc);
    }
    
    @isTest static void test1(){
        
        init();
        
        caseObj.Status='New';
        update caseObj;
        ApexPages.StandardController sc= new ApexPages.StandardController(caseObj);
        apexpages.currentPage().getParameters().put('Id',caseObj.id);
        SLATrackerController t1= new SLATrackerController(sc);
    }
    
    @isTest static void test2(){
        
        init();
        sla.Due_Date_Time__c=system.now().addmonths(-2);
        update sla;
        ApexPages.StandardController sc= new ApexPages.StandardController(caseObj);
        apexpages.currentPage().getParameters().put('Id',caseObj.id);
        SLATrackerController t1= new SLATrackerController(sc);
    }
}