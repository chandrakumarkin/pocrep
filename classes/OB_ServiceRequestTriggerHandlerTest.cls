@isTest
public class OB_ServiceRequestTriggerHandlerTest {
    /*
	@testSetup
    static  void createTestData() {
        // create Country_Risk_Scoring__c
        List<Country_Risk_Scoring__c> listCountryRiskScoring = new List<Country_Risk_Scoring__c>();
        listCountryRiskScoring = OB_TestDataFactory.createCountryRiskScorings(1, new List<string> {'Medium'}, new List<Integer> {10}, new List<string> {'Medium'});
        insert listCountryRiskScoring;
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insert insertNewSRs;
        
        //create HexaBPM_Amendment__c
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        listAmendment = OB_TestDataFactory.createApplicationAmendment(2, listCountryRiskScoring, insertNewSRs);
        insert listAmendment;
    }
*/
    
    static testMethod void serviceRequestTriggerTest() {
        
         // create Country_Risk_Scoring__c
        List<Country_Risk_Scoring__c> listCountryRiskScoring = new List<Country_Risk_Scoring__c>();
        listCountryRiskScoring = OB_TestDataFactory.createCountryRiskScorings(1, new List<string> {'Medium'}, new List<Integer> {10}, new List<string> {'Medium'});
        insert listCountryRiskScoring;
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insert insertNewSRs;
        
        //create HexaBPM_Amendment__c
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        listAmendment = OB_TestDataFactory.createApplicationAmendment(2, listCountryRiskScoring, insertNewSRs);
        insert listAmendment;
        
        
        
        History_Tracking__c objCS = new History_Tracking__c();
        objCS.Field_Label__c = 'Status';
        objCS.Field_Name__c = 'HexaBPM__Internal_SR_Status__c';
        objCS.Object_Name__c = 'hexabpm__service_request__c';
        objCS.Name = 'SR.hexabpm__service_request__c';
        insert objCS;
        
       // List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = [select id,HexaBPM__Customer__c,Lead_Id__c from HexaBPM__Service_Request__c];
        //list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = [select id from HexaBPM__SR_Status__c where Name = 'Submitted'];
        system.debug('--->'+insertNewSRs[0].HexaBPM__Customer__c+'---->'+insertNewSRs[0].Lead_Id__c);
        test.startTest();
            insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus[0].id;
            insertNewSRs[1].HexaBPM__External_SR_Status__c = listSRStatus[0].id;
            update insertNewSRs;
        
        test.stopTest();
    }
    
    
      static testMethod void addPortalUserAmedTest() 
      {
          
           // create Country_Risk_Scoring__c
        List<Country_Risk_Scoring__c> listCountryRiskScoring = new List<Country_Risk_Scoring__c>();
        listCountryRiskScoring = OB_TestDataFactory.createCountryRiskScorings(1, new List<string> {'Medium'}, new List<Integer> {10}, new List<string> {'Medium'});
        insert listCountryRiskScoring;
         // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1);
        insert insertNewAccounts;
          
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(2,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
          // create document master
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        listDocMaster = OB_TestDataFactory.createDocMaster(1, new List<string> {'Passport Copy'});
        listDocMaster[0].HexaBPM__Code__c =  'Passport_Copy_Individual';
        insert listDocMaster;

          //create createSRTemplate
       	List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
          createSRTemplateList[1].Name = 'Initial Approval';
        insert createSRTemplateList;
        
        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        listSRTemplateDoc = OB_TestDataFactory.createSR_Template_Docs(1, listDocMaster);
        listSRTemplateDoc[0].HexaBPM__SR_Template__c = createSRTemplateList[1].Id;
           listSRTemplateDoc[0].HexaBPM__Document_Master__c = listDocMaster[0].Id;
        insert listSRTemplateDoc; 
          
                     
          
        
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'New_User_Registration', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        //insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        //insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='New';
        insertNewSRs[0].Setting_Up__c='New';
        insertNewSRs[1].Type_of_Entity__c='Public';
        insertNewSRs[0].Type_of_Entity__c='Public';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].first_name__c = 'test';
        insertNewSRs[1].first_name__c = 'test';
        insertNewSRs[0].Entity_Type__c = 'Retail';
        insertNewSRs[0].Passport_No__c = '32135t';
        insertNewSRs[0].relationship_with_entity__c = 'Advisor';
        insert insertNewSRs;
          
          // create SR Doc
        List<HexaBPM__SR_Doc__c> listSRDoc = new List<HexaBPM__SR_Doc__c>();
        listSRDoc = OB_TestDataFactory.createSRDoc(1, insertNewSRs, listDocMaster, listSRTemplateDoc);
        listSRDoc[0].Name = 'Passport Copy';
        insert listSRDoc;

          
        //insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs[0].id;
          //insertNewSRs[1].HexaBPM__Linked_SR__c = insertNewSRs[0].id;
          //update insertNewSRs[1];
              
        //create HexaBPM_Amendment__c
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        listAmendment = OB_TestDataFactory.createApplicationAmendment(1, listCountryRiskScoring, insertNewSRs);
        insert listAmendment;
          
          
        // create SR Doc
        HexaBPM__SR_Doc__c srDoc = new HexaBPM__SR_Doc__c();
        srDoc.HexaBPM_Amendment__c = listAmendment[0].Id;
        srDoc.HexaBPM__Doc_ID__c = listAmendment[0].Id;
        srDoc.Name = 'Passport';
        srDoc.HexaBPM__Service_Request__c=insertNewSRs[0].Id;
        insert srDoc;
          
          HexaBPM__SR_Doc__c srDoc2 = new HexaBPM__SR_Doc__c();
        srDoc2.HexaBPM_Amendment__c = listAmendment[0].Id;
        srDoc2.HexaBPM__Doc_ID__c = listAmendment[0].Id;
        srDoc2.Name = 'Passport';
        srDoc2.HexaBPM__Service_Request__c=insertNewSRs[1].Id;
        insert srDoc2;
          
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test',
            PathOnClient = 'Test.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        insert contentVersionInsert;
        List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record 
        ContentDocumentLink cdl = New ContentDocumentLink();
        cdl.LinkedEntityId = srDoc.id;
        cdl.ContentDocumentId = documents[0].Id;
        cdl.shareType = 'V';
        insert cdl;
          
          
          
          
        test.startTest();
             list<HexaBPM__SR_Status__c> listSRStatuSub = new list<HexaBPM__SR_Status__c>() ;
             listSRStatuSub = [select id from HexaBPM__SR_Status__c where Name = 'Submitted'];
          
            List<HexaBPM__Service_Request__c> insertSRToUpdate = [SELECT id,HexaBPM__External_SR_Status__c,
                                                                  		 HexaBPM__Record_Type_Name__c,
                                                                  		 HexaBPM__External_Status_Name__c
																    FROM HexaBPM__Service_Request__c
																   WHERE HexaBPM__Record_Type_Name__c = 'New_User_Registration'
																   LIMIT 1
																 ];
          
            List<HexaBPM__Service_Request__c> insertSRInPrincipal = [SELECT id,HexaBPM__External_SR_Status__c,
                                                                  		 HexaBPM__Record_Type_Name__c,
                                                                  		 HexaBPM__External_Status_Name__c
																    FROM HexaBPM__Service_Request__c
																   WHERE HexaBPM__Record_Type_Name__c = 'In_Principle'
																   LIMIT 1
																 ];
          
          	insertSRInPrincipal[0].HexaBPM__Linked_SR__c = insertSRToUpdate[0].Id;
           update insertSRInPrincipal;
          
			
            OB_ServiceRequestTriggerHandler.firstRunPrtlUsrAmed = true;
            system.debug('@@@@@@@@@@@@@@@ insertSRToUpdate[0].HexaBPM__Record_Type_Name__c '+insertSRToUpdate[0].HexaBPM__Record_Type_Name__c);
            insertSRToUpdate[0].HexaBPM__Auto_Submit__c = true;
            insertSRToUpdate[0].HexaBPM__Internal_SR_Status__c = listSRStatuSub[0].id;
            insertSRToUpdate[0].HexaBPM__External_SR_Status__c = listSRStatuSub[0].id;
            update insertSRToUpdate;
          
          OB_ServiceRequestTriggerHandler.AddLicenseActivitiesToSR(new list<HexaBPM__Service_Request__c>{insertNewSRs[1]});
          	OB_ServiceRequestTriggerHandler.cloneSRDoc(srDoc,listAmendment[0]);
          
          insertNewSRs[1].Setting_Up__c='New';
          update insertNewSRs[1];
          OB_ServiceRequestTriggerHandler.EmptyNon_RelevantFields(new list<HexaBPM__Service_Request__c>{insertNewSRs[1]});
          
          OB_ServiceRequestTriggerHandler.createUserAmedBasedOnSR(insertNewSRs[1]);
          OB_ServiceRequestTriggerHandler.getSRBasedOnRecType(new list<HexaBPM__Service_Request__c>{insertNewSRs[1]},'test',insertSRToUpdate[0]);
          string rf = insertSRToUpdate[0].id;
          map<string,list<HexaBPM__SR_Doc__c>> teste = new map<string,list<HexaBPM__SR_Doc__c>>();
          //teste.put('test',new list<HexaBPM__Service_Request__c>{insertNewSRs[1]});
          
          try{
            OB_ServiceRequestTriggerHandler.getSRDocBasedOnCode(rf,teste,'test');  
          }catch(Exception e){
              
          }
          
          
           
          
        test.stopTest();
          
          
      }
}