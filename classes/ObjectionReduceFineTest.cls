@isTest
private class ObjectionReduceFineTest {

    static testMethod void myUnitTest() {
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Objection_SR','Issue_Fine')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        List<Account> lstAccount = new List<Account>();
        List<Contact> lstContact = new List<Contact>();
        List<Service_Request__c> lstServiceRequestData = new List<Service_Request__c>();
        List<Service_Request__c> lstServiceRequestDataFine = new List<Service_Request__c>();
        lstAccount = TestUtilityClass.accountDataInsert(1,false);
        lstAccount[0].ROC_Status__c = 'Under Formation';
        
        if(lstAccount.size()>0){
            INSERT lstAccount;
        }
        lstServiceRequestData = TestUtilityClass.createServiceRequestData(2,false);
        lstServiceRequestData[0].Customer__c =lstAccount[0].ID;
            lstServiceRequestData[0].RecordtypeID = mapRecordTypeIds.get('Issue_Fine');
            
            if(lstServiceRequestData.size()>0){
                UPSERT lstServiceRequestData[0];
            }
            
           
            lstServiceRequestData[1].Customer__c =lstAccount[0].ID;
            lstServiceRequestData[1].RecordtypeID = mapRecordTypeIds.get('Objection_SR');
            lstServiceRequestData[1].Linked_SR__c = lstServiceRequestData[0].ID;
            if(lstServiceRequestData.size()>0){
                UPSERT lstServiceRequestData[1];
            }
            
                     
            
            Step__c stepRecord = new Step__c();
            stepRecord.SR__c = lstServiceRequestData[1].Id;
            stepRecord.SR_Record_Type_Text__c = 'Objection_SR';
            
            insert stepRecord;  
            
             SR_Price_Item__c objSRItem = new SR_Price_Item__c();
        objSRItem.ServiceRequest__c = lstServiceRequestData[0].Id;
        objSRItem.Status__c = 'Invoiced';
        objSRItem.Price__c = 1000;
        insert objSRItem;
              
        ObjectionReduceFine.reduceFineAmount(stepRecord.ID);        
    }
}