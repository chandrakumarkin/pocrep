/*********************************************************************************************************************
*  Name     : Person_Authorised_DEWS_Registration_edit
*  Author     : Arun Singh
*  Purpose   : This is custom code class for ROC processes.
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
V1.0          Arun                  09-Dec-2019              Created for 7838
V1.1          Sai                   23-Dec-2019              #7935
**********************************************************************************************************************/


public with sharing class Person_Authorised_DEWS_Registration_edit {

    public Service_Request__c SRData{get;set;}
    public string RecordTypeId;
    public map<string,string> mapParameters;
    public String CustomerId;
    public Account ObjAccount{get;set;}
    Map<Id,String> MapAuth{get;set;}
    
    public Person_Authorised_DEWS_Registration_edit(ApexPages.StandardController controller) {

        List<String> fields = new List<String> {'Commercial_Activity__c'};
        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        
        MapAuth=new Map<Id,String>();
   SRData=(Service_Request__c )controller.getRecord();
   
   
     mapParameters = new map<string,string>();        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();              
       if(mapParameters.get('RecordType')!=null) 
       RecordTypeId= mapParameters.get('RecordType');  
       
       
        for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                             Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                             Contact.Account.Qualifying_Type__c,Contact.Account.Qualifying_Purpose_Type__c,Contact.Account.Contact_Details_Provided__c,
                             Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  
                             from User where Id=:userinfo.getUserId()])
            {
              CustomerId = objUsr.Contact.AccountId;
             if(SRData.id==null){
                 
                
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                SRData.Expiry_Date__c=objUsr.Contact.Account.Next_Renewal_Date__c;
                ObjAccount= objUsr.Contact.Account;
                
              }
              
             
          
           
            }
              
       
   
    }
    
     public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--Select--'));
       // V1.1  Added Relationship_Group__c ='ROC' condition in the Query
        for(Relationship__c ObjRel:[select id,Relationship_Type__c,Relationship_Name__c,Email__c,Object_Contact__c from Relationship__c where Object_Contact__c!=null and Relationship_Type__c='Is Authorized Signatory for'and Active__c=true and Subject_Account__c=:SRData.Customer__c AND Relationship_Group__c ='ROC'])
        {
           
            {
                options.add(new SelectOption(ObjRel.id,ObjRel.Relationship_Name__c+' : '+((ObjRel.Email__c==null)? '' : ObjRel.Email__c)));    
                MapAuth.put(ObjRel.id,ObjRel.Relationship_Name__c+' : '+ObjRel.Email__c);
            }
            
        }
        
        //options.add(new SelectOption('CANADA','authorised Name : Demo@sd.com'));
        //options.add(new SelectOption('MEXICO','Mexico'));

        return options;
    }
    
    
public  PageReference SaveConfirmation()
{
 try {  
            
            SRData.Authority_Name__c=MapAuth.get(SRData.Commercial_Activity__c);
            upsert SRData;
            PageReference acctPage = new ApexPages.StandardController(SRData).view();        
            acctPage.setRedirect(true);        
            return acctPage;     
        }catch (Exception e)     
        {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());         
            ApexPages.addMessage(myMsg);    
        }  
     
     return null; 
     
}


}