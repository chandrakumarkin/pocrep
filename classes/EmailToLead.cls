/**
 * Email services are automated processes that use Apex classes
 * to process the contents, headers, and attachments of inbound
 * email.
 */
global class EmailToLead implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) 
    {
    String myPlainText= '';
           
    // Add the email plain text into the local variable 
    myPlainText = email.plainTextBody;
    // New Task object to be created
        Task[] newTask = new Task[0];
        Lead lead;
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();        
        try {
            //Look for lead whos name is the email and create it if necessary
            List<lead> ListLead=new List<lead>();
            Boolean isLeadExist = false;
            //=[select id from Lead where Email=:email.FromAddress and isConverted=false order by createddate limit 1] ;
            if (ListLead.isEmpty()==true) 
            {
                    lead = new Lead();
                    if(email.fromName!=null)
                    lead.LastName = email.fromName;
                    else
                    lead.LastName = email.FromAddress;
                    
                    lead.Email    = email.FromAddress;
                    lead.Company  = email.FromAddress;
                    lead.LeadSource ='Email';
                   
               
                   Set<String> addresses = new Set<String>();
               
                   for(String thisString : email.toAddresses ){
                     addresses.add(thisString.toLowercase()); 
                   }
                  
                   boolean isEvent = addresses.contains('events@difc.ae');
                   boolean isRetail = addresses.contains('retail@difc.ae');
                
                  Date todayDate = System.today();
                
                  if(isEvent || isRetail){
                  
                   if([SELECT Id,Email FROM Lead Where Email =: email.FromAddress AND IsConverted = false ].size()== 0){
                     
                    if(isRetail){
                         lead.Company_Type__c  = 'Retail';
                         lead.Email_To_Lead__c =  True;
                         lead.RecordTypeId     =  Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Retail').getRecordTypeId();
                       }
                   
                      if(isEvent){
                        lead.Company_Type__c     = 'Retail';
                        lead.Type_of_Property__c = 'Lease a Kiosk';
                        lead.RecordTypeId        =  Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Special_Leasing').getRecordTypeId(); 
                        lead.Email_To_Lead__c    =  True;
                       }
                  }
                  else{
                    isLeadExist = true;
                  }
                }
                
               if(!isLeadExist ){
                    Database.DMLOptions dmo = new Database.DMLOptions();
                    dmo.assignmentRuleHeader.useDefaultRule = true;
                    dmo.EmailHeader.triggerUserEmail = true;
                    Database.SaveResult lsr= Database.insert(lead, dmo);
               }
            } 
            else 
            { //Lead already exists
                lead = ListLead[0];
            } 
                 
              // Add a new Task to the contact record we just found above.
                   newTask.add(new Task(Description =  myPlainText,
                   Priority = 'Normal',
                   Status = 'Inbound Email',
                   Subject = email.subject,
                   WhoId =  lead.Id));
                   // Insert the new Task 
                  insert newTask;    
               
                      
            result.success = true;
       }
        catch (Exception e) 
        {
            result.success = false;
            result.message = 'Undeliverable...';
        }
        return result;
    }
}