public class IssueFineObjectionWithdraw {
    
    
    Public static String withdrawIssueFine(String SrId){
        system.debug('testIssueFineSr');
        List<Service_Request__c> srList = new List<Service_Request__c>();
        Sr_Status__c srStatus = [Select Id, name from Sr_Status__c where Code__c = 'Withdraw'];
        Service_request__c SrObj = [Select Id, name, External_SR_Status__c, Linked_SR__r.External_SR_Status__r.Name,  Record_Type_Name__c,Linked_SR__r.Record_Type_Name__c, Linked_SR__c, Linked_SR__r.Type_of_Request__c from Service_Request__c Where Id=:SrId];
        if(SrObj.Linked_SR__c != null && SrObj.Linked_SR__r.Type_of_Request__c == 'Preliminary Notice of Fines' && SrObj.Linked_SR__r.Record_Type_Name__c == 'Issue_Fine'){
            system.debug('Testing');
            for(Service_Request__c SrObjPreNotice: [SELECT Id, Name, Type_of_Request__c, Customer__c, External_SR_Status__c,External_SR_Status__r.Code__c, Internal_SR_Status__c FROM Service_Request__c WHERE Id =:SrObj.Linked_SR__c]){
                
                if(SrObjPreNotice.External_SR_Status__r.Code__c != 'Withdraw'){
                    SrObjPreNotice.External_SR_Status__c = srStatus.Id;
                    SrObjPreNotice.Internal_SR_Status__c = srStatus.Id;
                    srList.add(srObjPreNotice);
                }
            }
            
            if(srList != null && srList.size()>0){
                System.debug('srList:::--'+srList);
                Update srList;
            }
            
        }
        return 'Success';
    }
    
    Public static String updateIssueFine(String SrId){
        System.debug('SrId:::'+SrId);
        List<Service_Request__c> srOjPreNoticeList = new List<Service_Request__c>();
        List<Service_Request__c> srList = new List<Service_Request__c>();
        Service_request__c SrObj = [Select Id, name, Linked_SR__c, Type_of_Request__c, Customer__c from Service_Request__c Where Id=:SrId];
        Sr_Status__c srStatus = [Select Id, name from Sr_Status__c where code__c = 'COMPLETED' Limit 1];
        if(SrObj.Customer__c != null){
            for(Service_Request__c SrObjPreNotice : [SELECT Id, Name, Type_of_Request__c, Customer__c, External_SR_Status__c, Internal_SR_Status__c, External_SR_Status__r.Code__c FROM Service_Request__c WHERE Customer__c =:SrObj.Customer__c and Type_of_Request__c ='Preliminary Notice of Fines' AND Record_Type_Name__c = 'Issue_Fine']){
                
                if(SrObjPreNotice != null && SrObjPreNotice.External_SR_Status__r.code__c != 'COMPLETED'){
                    SrObjPreNotice.External_SR_Status__c = srStatus.Id;
                    SrObjPreNotice.Internal_SR_Status__c = srStatus.Id; 
                }
                srOjPreNoticeList.add(SrObjPreNotice);
            }
            Update srOjPreNoticeList;  
        }
        return 'Success';
    }
}