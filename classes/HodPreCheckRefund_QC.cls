/******************************************************************************************
 *  Author   : Veera
 *  Company  : 
 *  Date     : 04/04/2021
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    04/04/2021  Veera    Created
********/

 global class HodPreCheckRefund_QC implements Queueable,Database.AllowsCallouts{
 
    String SerReqID ;
    String stepId ;
    String stepName;
 
    
    global HodPreCheckRefund_QC (string SRId, string stpId, String stpName){
    
      SerReqID  = SRId;
      stepId =   stpId ;
      stepName= stpName;
    
    system.debug('SerReqID==='+ SerReqID + 'stepId==== '+ stepId +'stepName==='  + stepName );
    
    }
 
    global void execute(QueueableContext context){
    
       Service_request__c srObj = new Service_Request__c();

        for(Service_Request__c sr :[select id,Name,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,record_Type_Name__c,
                                            Submitted_Date__c,Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c,
                                            Contact__r.BP_No__c,SAP_MATNR__c,
                                            (select id,Name,Status__c,Step_Name__c,Status__r.Name from Steps_SR__r)
                                            from service_request__c where id=:SerReqID ]){
            srObj = sr;                                             
        }  
      
        SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP refundResp        = RefundRequestUtils.PushToSAP(srObj,'RFCK');
        List<SAPPortalBalanceRefundService.ZSF_S_REFUND_OP> respItemList = refundResp.item;

        List<Log__c>  lstLogs = new list<log__c>();
        for(SAPPortalBalanceRefundService.ZSF_S_REFUND_OP resp : respItemList){
         
            if(resp.RFAMT !=null || Test.isRunningTest()){
                       
                Step__c thisStep                    = new Step__c(Id = stepId );
                thisStep.refund_amount__c           = decimal.valueOf(resp.RFAMT); 
                Update thisStep;
           
            }else if(resp.RSTAT == 'E' && resp.SFMSG !='No Service Refund' && resp.SFMSG !='Not Applicable'){
                Log__c objLog = new Log__c( Type__c = 'HOD Cancellatio Pre check failed#'+resp.RENUM ,Description__c = resp.SFMSG);
               
                lstLogs.add(objLog);           
            }     
          }
             if(lstLogs !=null && lstLogs.size()>0){
                insert  lstLogs;
            
        }  
     } 
   }