public with sharing class Filing_of_Special_resolutions_edit {

public Service_Request__c SRData{get;set;}

 public string RecordTypeId;
 public map<string,string> mapParameters;
 
  public  Attachment ObjAttachment{get;set;}
  
 public Account ObjAccount{get;set;}

    public Filing_of_Special_resolutions_edit(ApexPages.StandardController controller) 
    {
    
    ObjAttachment=new Attachment();
     ObjAccount=new account();
    
   // List<String> fields = new List<String> {'Financial_Year_End_mm_dd__c'};
     //   if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        
        SRData=(Service_Request__c )controller.getRecord();
        //if (Test.isRunningTest()) system.assertEquals(null, SRData);
        
    
        mapParameters = new map<string,string>();
        
        if(apexpages.currentPage().getParameters()!=null)
        mapParameters = apexpages.currentPage().getParameters();
            
        
        
       if(mapParameters.get('RecordType')!=null) 
       RecordTypeId= mapParameters.get('RecordType');
       
       
            for(User objUsr:[select id,ContactId,Email,Phone,Contact.AccountId,Contact.Account.Company_Type__c,Contact.Account.Financial_Year_End__c,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  from User where Id=:userinfo.getUserId()])
            {
             if(SRData.id==null)
             {
                
                SRData.Customer__c = objUsr.Contact.AccountId;
                SRData.RecordTypeId=RecordTypeId;
                SRData.Email__c = objUsr.Email;
                SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                ObjAccount= objUsr.Contact.Account;
                SRData.Customer__r=ObjAccount;
               
                SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                
                    
             }
             
            
               ObjAccount=objUsr.Contact.Account;
                
                
                
            }

            
       
        

    }
    
    public  PageReference SaveConfirmation()
    {
        
        System.debug('================SaveConfirmation======================');
        
     try
     {
       
         upsert SRData;
        
         
                        
        PageReference acctPage = new ApexPages.StandardController(SRData).view();
        acctPage.setRedirect(true);
        return acctPage;
     }
       catch (Exception e)
     {
         //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
        // ApexPages.addMessage(myMsg);
        ApexPages.addMessages(e);
         
     }
      return null;

     
    }

}