@isTest
public class OB_CalculateDaysBreachedOnStepsBatchTest {
    @testSetup
    static  void createTestData() {
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
             
       List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(1, new List<string> {'Commercial_Permission'});
        insert createSRTemplateList;

       //create step template
       List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
       listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'BD Detailed Review'}, new List<String> {'BD_DETAILED_REVIEW'}
                                                                 , new List<String> {'General'},  new List<String> {'BD Detailed Review'});
       insert listStepTemplate;

        Group testGroup = [Select id from Group where Name='Client Entry User'];
        system.debug('testGroup==>'+testGroup);

       List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
       listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
       listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
       listSRSteps[0].HexaBPM__Step_No__c = 1;
       listSRSteps[0].OwnerId = testGroup.Id;
       insert listSRSteps;
       
       list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(2, new List<string> {'Draft','Expired'}, new List<string> {'DRAFT','EXPIRED'}, new List<string> {'End',''});
        insert listSRStatus;
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New Commercial Permission').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.Type_of_commercial_permission__c='Gate Avenue Pocket Shops';
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = createSRTemplateList[0].Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.HexaBPM__External_SR_Status__c = listSRStatus[0].Id;
        objHexaSR.HexaBPM__Internal_SR_Status__c = listSRStatus[0].Id;
        insert objHexaSR;

        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        objHexastep.HexaBPM__SR_Step__c = listSRSteps[0].Id;
        objHexastep.HexaBPM__Due_Date__c = system.Today()-2;
        insert objHexastep;

        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true limit 1];

        OBBusinessHour__c objBH = new OBBusinessHour__c(Name='Customer - default business hours',Business_Hour_Id__c=bhs[0].Id,Queue_Id__c=testGroup.Id);
        insert objBH;

    }

    static testMethod void executeBatchTest() {
        Test.startTest(); 
        Database.executeBatch(new OB_CalculateDaysBreachedOnStepsBatch());
        String sch ='0 48 * * * ?';  
        System.schedule('Schedule to update SR', sch,new OB_CalculateDaysBreachedOnStepsBatch());
        Test.stopTest();       
        list<HexaBPM__Step__c> stepList = [Select id,OB_Days_Breached__c,CreatedDate,HexaBPM__Due_Date__c,HexaBPM__SR_Step__c,HexaBPM__SR_Step__r.OwnerId,HexaBPM__SR_Step__r.HexaBPM__Owner_Queue_Name__c from HexaBPM__Step__c];
        System.assertEquals(true,stepList[0].OB_Days_Breached__c>0); 
    }
}