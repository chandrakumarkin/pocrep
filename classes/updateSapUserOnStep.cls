/***********************************************************************************
 *  Author      : Mudasir Wani
 *  Date        : 17th-November-2019
 *  Description : Update the SAP user details based on the SAP user id 
                : We have information coming from SAP on the steps via informatica.
                : We need to update the Created By User and Closed by User fields on the step level based on the SAP user ids present in the system
 *  Logic       : Get the User details based on the SAP user id and update the step with the name of the user against created and closed fields on the step.
 *  Invocation  : The class is invoced from the process builder 
  --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
    V.No    Date                    Updated By      Description
    V1.0    17th-November-2019      Mudasir         Created
 ----------------------------------------------------------------------------------------    
 **********************************************************************************************/
 public without sharing class updateSapUserOnStep {
    @InvocableMethod(Label ='Update SAP User Details' Description='This process is used to update the SAP user details on the step level')
    public static void updateSapUserDetailsOnStep(List<id> recordIds) {
            List<Step__c> stepRecords = new List<Step__c>();
            Set<String> SAP_ENAME = new Set<String>();
            Map<String,String> SAP_Name_To_Salesforce_Username = new Map<String ,String>();
            for(Step__c stepRec : [Select id ,Step_Name__c,SAP_ERTIM__c,SAP_AETIM__c,SAP_ERDAT__c,SAP_AEDAT__c,SF_ERDAT__c,SF_AEDAT__c,SAP_AENAM__c,SAP_ERNAM__c,Created_By_User__c,Closed_by_User__c,Owner.Name,createdBy.name,LastModifiedby.name,createddate,Closed_Date_Time__c from Step__c where id in : recordIds])
            {
            	if(String.isNotBlank(stepRec.SAP_AENAM__c)){SAP_ENAME.add(stepRec.SAP_AENAM__c);}
                if(String.isNotBlank(stepRec.SAP_ERNAM__c)){SAP_ENAME.add(stepRec.SAP_ERNAM__c);}
                for(User userRec : [Select id,name,SAP_User_Id__c from user where SAP_User_Id__c in :SAP_ENAME]){
                    SAP_Name_To_Salesforce_Username.put(userRec.SAP_User_Id__c,userRec.Name);
                }
            }
            for(Step__c stepRec : [Select id ,Step_Name__c,SAP_ERTIM__c,SAP_AETIM__c,SAP_ERDAT__c,SAP_AEDAT__c,SF_ERDAT__c,SF_AEDAT__c,SAP_AENAM__c,SAP_ERNAM__c,Created_By_User__c,Closed_by_User__c,Owner.Name,createdBy.name,LastModifiedby.name,createddate,Closed_Date_Time__c from Step__c where id in : recordIds])
            {
	                if(System.Label.gsSystemGeneratedOpenSteps.contains(stepRec.Step_name__c)){
	                	stepRec.Created_By_User__c = 'Integration User';
	                	stepRec.SF_ERDAT__c = string.valueOfGmt(stepRec.createdDate).replaceAll('-','/');
	                }else{
	                	stepRec.SF_ERDAT__c = stepRec.SAP_ERDAT__c != Null &&  stepRec.SAP_ERDAT__c !='' ? stepRec.SAP_ERDAT__c.substring(6,8)+'/'+stepRec.SAP_ERDAT__c.substring(4,6)+'/'+stepRec.SAP_ERDAT__c.substring(0,4)+' '+stepRec.SAP_ERTIM__c.substring(0,2)+':'+stepRec.SAP_ERTIM__c.substring(2,4)+':'+stepRec.SAP_ERTIM__c.substring(4,6) : '';
	                	if(stepRec.SAP_ERNAM__c =='' || stepRec.SAP_ERNAM__c == Null){
	                		stepRec.Created_By_User__c = stepRec.Createdby.Name;
	                		stepRec.SF_ERDAT__c = string.valueOfGmt(stepRec.createdDate).replaceAll('-','/');
	                	}
	                	else if(stepRec.SAP_ERNAM__c !=Null && stepRec.SAP_ERNAM__c !='' && SAP_Name_To_Salesforce_Username.size() > 0 && SAP_Name_To_Salesforce_Username.keyset().contains(stepRec.SAP_ERNAM__c)){
	                		stepRec.Created_By_User__c = SAP_Name_To_Salesforce_Username.get(stepRec.SAP_ERNAM__c);
	                	}else if(stepRec.SAP_ERNAM__c !=Null  && stepRec.SAP_ERNAM__c !='' ){
	                		stepRec.Created_By_User__c = stepRec.SAP_ERNAM__c;
	                	}else{stepRec.Created_By_User__c  ='';}
	                }
	                if(System.Label.gsSystemGeneratedClosedSteps.contains(stepRec.Step_name__c)){
	                	stepRec.SF_AEDAT__c = stepRec.Closed_Date_Time__c != Null ? string.valueOfGmt(stepRec.Closed_Date_Time__c).replaceAll('-','/'): '';
	                	stepRec.Closed_by_User__c  = stepRec.Closed_Date_Time__c != NULL ? 'Integration User' : '';
	                }else{
	                	stepRec.SF_AEDAT__c = stepRec.SAP_AEDAT__c != Null && stepRec.SAP_AEDAT__c != '' ? stepRec.SAP_AEDAT__c.substring(6,8)+'/'+stepRec.SAP_AEDAT__c.substring(4,6)+'/'+stepRec.SAP_AEDAT__c.substring(0,4)+' '+stepRec.SAP_AETIM__c.substring(0,2)+':'+stepRec.SAP_AETIM__c.substring(2,4)+':'+stepRec.SAP_AETIM__c.substring(4,6) : '';
	                	if(stepRec.SAP_ERNAM__c ==Null || stepRec.SAP_AENAM__c ==''){
	                		stepRec.SF_AEDAT__c = stepRec.Closed_Date_Time__c != Null ? string.valueOfGmt(stepRec.Closed_Date_Time__c).replaceAll('-','/') : '';
	                		stepRec.Closed_by_User__c  = stepRec.Closed_Date_Time__c != NULL ? 'Integration User' : '';
	                	}
	                	else if(stepRec.SAP_AENAM__c !='' && SAP_Name_To_Salesforce_Username.size() > 0 && SAP_Name_To_Salesforce_Username.keyset().contains(stepRec.SAP_AENAM__c)){
	                		stepRec.Closed_by_User__c = stepRec.SF_AEDAT__c != '' ? SAP_Name_To_Salesforce_Username.get(stepRec.SAP_ERNAM__c) : '';
	                	}else if(stepRec.SAP_ERNAM__c !='' ){
	                		stepRec.Created_By_User__c = stepRec.SF_AEDAT__c != '' ?  stepRec.SAP_ERNAM__c : '';
	                	}else{stepRec.Created_By_User__c  ='';}
	                }
                	stepRecords.add(stepRec);
            }
            update stepRecords;
    }
}