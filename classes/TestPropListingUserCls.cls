/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestPropListingUserCls {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
         map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Property_Listing_User_Access_Form','RORP_Account','RORP_Contact','Portal_User')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        SR_Template__c objSRTemp = new SR_Template__c(Name = 'Property_Listing_User_Access_Form',SR_RecordType_API_Name__c='Property_Listing_User_Access_Form');
        insert objSRTemp;
        
        Lookup__c Nati = new Lookup__c(Type__c='Nationality',Name='USA');
        insert Nati;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        Account acc = new Account();
        acc.RORP_License_No__c = '12345';
        acc.Issuing_Authority__c = 'ABC';
        acc.recordTypeId = mapRecordTypeIds.get('RORP_Account');
        acc.Name = 'Non DIFC Company';
        acc.ROC_Status__c='Active';
        insert acc;
        
        License__c lic = new License__c(name='12345',Account__c=acc.id);
        insert lic;
        
        acc.Active_License__c = lic.id;
        update acc;
        
        Lease__c lsc = new Lease__c();
        lsc.Status__c = 'Active';
        lsc.Name = '002309';
        lsc.Type__c = 'Purchased';
        insert lsc;
        
        Lease_Partner__c LPAccount= new Lease_Partner__c();
        LPAccount.Account__c = acc.id;
        LPAccount.Lease__c = lsc.id;
        insert LPAccount;
        
        list<SR_Status__c> lstStatus = new list<SR_Status__c>();
        lstStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new SR_Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new SR_Status__c(Name='Approved',Code__c='Approved'));
        
        
        ApexPages.StandardController controller = new ApexPages.StandardController(new Service_Request__c());
        PropListingUserCls objRORPUserCls = new PropListingUserCls(controller);
        PropListingUserCls objRORPUserCls1 = new PropListingUserCls(controller);
        PropListingUserCls objRORPUserCls2 = new PropListingUserCls(controller);
        
        /*Non DIFC Reg Companies*/
        objRORPUserCls = new PropListingUserCls();
        objRORPUserCls.PassportCopy = new Attachment();
        objRORPUserCls.CompanyCopy = new Attachment();
        objRORPUserCls.sUser = new Service_Request__c();
        objRORPUserCls.sUser.First_Name__c = 'RB';
        objRORPUserCls.sUser.Last_Name__c = 'Nagaboina';
        objRORPUserCls.sUser.Position__c = 'Consultant';
        objRORPUserCls.sUser.Date_of_Birth__c = Date.newInstance(1947, 08, 15);
        objRORPUserCls.sUser.Nationality_list__c = 'USA';
        objRORPUserCls.sUser.Passport_Number__c = '123456';
        objRORPUserCls.sUser.Send_SMS_To_Mobile__c = '+971555789456';
        objRORPUserCls.sUser.Title__c = 'CEO';
        objRORPUserCls.sUser.Email__c = 'test@test.test';
        objRORPUserCls.sUser.Name_of_the_Authority__c = 'Non-DIFC Registered Company';
        objRORPUserCls.sUser.Others_Please_Specify__c ='ABC';
        objRORPUserCls.sUser.License_Number__c = '12345';
        objRORPUserCls.sUser.I_agree__c = true;
        
        objRORPUserCls.saveInfo();  
        
        objRORPUserCls.PassportCopy = new Attachment();
        objRORPUserCls.PassportCopy.Body = blob.valueOf('Test Passport');
        objRORPUserCls.PassportCopy.Name = 'Test Passport.pdf';
        
        objRORPUserCls.CompanyCopy = new Attachment();
        objRORPUserCls.CompanyCopy.Body = blob.valueOf('Test Company');
        objRORPUserCls.CompanyCopy.Name = 'Test Company.pdf';
        
        objRORPUserCls.saveInfo();
        
        insert lstStatus;
        
        objRORPUserCls.PassportCopy = new Attachment();
        objRORPUserCls.PassportCopy.Body = blob.valueOf('Test Passport');
        objRORPUserCls.PassportCopy.Name = 'Test Passport.pdf';
        
        objRORPUserCls.saveInfo();
        objRORPUserCls.ChangeOption();
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.First_Name__c = 'RB';
        objSR.Last_Name__c = 'Nagaboina';
        objSR.Position__c = 'Consultant';
        objSR.Date_of_Birth__c = Date.newInstance(1947, 08, 15);
        objSR.Nationality_list__c = 'USA';
        objSR.Passport_Number__c = '123456';
        objSR.Send_SMS_To_Mobile__c = '+971555789456';
        objSR.Title__c = 'CEO';
        objSR.Email__c = 'test@test.test';
        objSR.Name_of_the_Authority__c = 'Non-DIFC Registered Company';
        objSR.Others_Please_Specify__c ='ABC';
        objSR.License_Number__c = '12345';
        insert objSR;
        
        Step__c objStep = new Step__c();
        //for(Service_Request__c objS : [select Id from Service_Request__c where Passport_Number__c = '123456']){
            objStep.SR__c = objSR.Id;
        //\\}
        PropListingUserCls.createListingUser(objStep);
        
        /*DIFC Reg Companies*/
        objRORPUserCls1 = new PropListingUserCls();
        objRORPUserCls1.PassportCopy = new Attachment();
        objRORPUserCls1.CompanyCopy = new Attachment();
        objRORPUserCls1.sUser = new Service_Request__c();
        objRORPUserCls1.sUser.First_Name__c = 'RB';
        objRORPUserCls1.sUser.Last_Name__c = 'Nagaboina';
        objRORPUserCls1.sUser.Position__c = 'Consultant';
        objRORPUserCls1.sUser.Date_of_Birth__c = Date.newInstance(1947, 08, 15);
        objRORPUserCls1.sUser.Nationality_list__c = 'USA';
        objRORPUserCls1.sUser.Passport_Number__c = '123456';
        objRORPUserCls1.sUser.Send_SMS_To_Mobile__c = '+971555789456';
        objRORPUserCls1.sUser.Title__c = 'CEO';
        objRORPUserCls1.sUser.Email__c = 'test@test.test';
        objRORPUserCls1.sUser.Name_of_the_Authority__c = 'DIFC Registered Company';
        //objRORPUserCls1.sUser.Others_Please_Specify__c ='ABC';
        objRORPUserCls1.sUser.License_Number__c = '12345';
        objRORPUserCls1.sUser.I_agree__c = true;
        
        objRORPUserCls1.saveInfo();  
        
        objRORPUserCls1.PassportCopy = new Attachment();
        objRORPUserCls1.PassportCopy.Body = blob.valueOf('Test Passport');
        objRORPUserCls1.PassportCopy.Name = 'Test Passport.pdf';
        
        objRORPUserCls1.CompanyCopy = new Attachment();
        objRORPUserCls1.CompanyCopy.Body = blob.valueOf('Test Company');
        objRORPUserCls1.CompanyCopy.Name = 'Test Company.pdf';
        
        objRORPUserCls1.saveInfo();
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.First_Name__c = 'RB';
        objSR1.Last_Name__c = 'Nagaboina';
        objSR1.Position__c = 'Consultant';
        objSR1.Date_of_Birth__c = Date.newInstance(1947, 08, 15);
        objSR1.Nationality_list__c = 'USA';
        objSR1.Passport_Number__c = '123456';
        objSR1.Send_SMS_To_Mobile__c = '+971555789456';
        objSR1.Title__c = 'CEO';
        objSR1.Email__c = 'test@test.test';
        objSR1.Name_of_the_Authority__c = 'DIFC Registered Company';
        //objSR1.Others_Please_Specify__c ='ABC';
        objSR1.License_Number__c = '12345';
        insert objSR1;
        
        Step__c objStep1 = new Step__c();
        //for(Service_Request__c objS : [select Id from Service_Request__c where Passport_Number__c = '123456']){
            objStep1.SR__c = objSR1.Id;
        //\\}
        PropListingUserCls.createListingUser(objStep1);
       
        
    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
         map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Property_Listing_User_Access_Form','RORP_Account','RORP_Contact','Portal_User')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        SR_Template__c objSRTemp = new SR_Template__c(Name = 'Property_Listing_User_Access_Form',SR_RecordType_API_Name__c='Property_Listing_User_Access_Form');
        insert objSRTemp;
        
        Lookup__c Nati = new Lookup__c(Type__c='Nationality',Name='USA');
        insert Nati;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        Contact con = new Contact();
        con.LastName='Khan';
        con.RecordTypeId = mapRecordTypeIds.get('RORP_Contact');
        con.Nationality__c = 'USA';
        con.Passport_No__c = '123456';
        con.BP_No__c='324323';
        insert con;
        
        Account acc = new Account();
        acc.RORP_License_No__c = '12345';
        acc.Issuing_Authority__c = 'ABC';
        acc.recordTypeId = mapRecordTypeIds.get('RORP_Account');
        acc.Name = 'Non DIFC Company';
        acc.ROC_Status__c='Active';
        insert acc;
        
        Lease__c lsc = new Lease__c();
        lsc.Status__c = 'Active';
        lsc.Name = '002309';
        lsc.Type__c = 'Purchased';
        insert lsc;
        
        Lease_Partner__c LPAccount= new Lease_Partner__c();
        LPAccount.Contact__c = con.id;
        LPAccount.Lease__c = lsc.id;
        insert LPAccount;
        
        list<SR_Status__c> lstStatus = new list<SR_Status__c>();
        lstStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new SR_Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new SR_Status__c(Name='Approved',Code__c='Approved'));
        
        ApexPages.StandardController controller = new ApexPages.StandardController(new Service_Request__c());
        PropListingUserCls objRORPUserCls2 = new PropListingUserCls(controller);
         
        /*Individual Users*/
        objRORPUserCls2 = new PropListingUserCls();
        objRORPUserCls2.PassportCopy = new Attachment();
        objRORPUserCls2.CompanyCopy = new Attachment();
        objRORPUserCls2.sUser = new Service_Request__c();
        objRORPUserCls2.sUser.First_Name__c = 'RB';
        objRORPUserCls2.sUser.Last_Name__c = 'Nagaboina';
        objRORPUserCls2.sUser.Position__c = 'Consultant';
        objRORPUserCls2.sUser.Date_of_Birth__c = Date.newInstance(1947, 08, 15);
        objRORPUserCls2.sUser.Nationality_list__c = 'USA';
        objRORPUserCls2.sUser.Passport_Number__c = '123456';
        objRORPUserCls2.sUser.Send_SMS_To_Mobile__c = '+971555789456';
        objRORPUserCls2.sUser.Title__c = 'CEO';
        objRORPUserCls2.sUser.Email__c = 'test@test.test';
        objRORPUserCls2.sUser.Name_of_the_Authority__c = 'Individual User';
        objRORPUserCls2.sUser.Others_Please_Specify__c ='ABC';
        objRORPUserCls2.sUser.License_Number__c = '12345';
        objRORPUserCls2.sUser.I_agree__c = true;
        
        objRORPUserCls2.saveInfo();  
        
        objRORPUserCls2.PassportCopy = new Attachment();
        objRORPUserCls2.PassportCopy.Body = blob.valueOf('Test Passport');
        objRORPUserCls2.PassportCopy.Name = 'Test Passport.pdf';
      
        objRORPUserCls2.CompanyCopy = new Attachment();
        objRORPUserCls2.CompanyCopy.Body = blob.valueOf('Test Company');
        objRORPUserCls2.CompanyCopy.Name = 'Test Company.pdf';
        
        objRORPUserCls2.saveInfo();
        
        Service_Request__c objSR2 = new Service_Request__c();
        objSR2.First_Name__c = 'RB';
        objSR2.Last_Name__c = 'Nagaboina';
        objSR2.Position__c = 'Consultant';
        objSR2.Date_of_Birth__c = Date.newInstance(2010, 08, 15);
        objSR2.Nationality_list__c = 'USA';
        objSR2.Passport_Number__c = '123456';
        objSR2.Send_SMS_To_Mobile__c = '+971555789456';
        objSR2.Title__c = 'CEO';
        objSR2.Email__c = 'test@test.test';
        objSR2.Name_of_the_Authority__c = 'Individual User';
        //objSR1.Others_Please_Specify__c ='ABC';
        objSR2.License_Number__c = '12345';
        insert objSR2;
        
        Step__c objStep2 = new Step__c();
        //for(Service_Request__c objS : [select Id from Service_Request__c where Passport_Number__c = '123456']){
            objStep2.SR__c = objSR2.Id;
        //\\}
        PropListingUserCls.createListingUser(objStep2);
        
        
        User usr = new User();
        usr.IsActive = true;
        usr.CompanyName = acc.id;
        usr.username = 'abc@difcportal.ae';
        usr.contactId=con.Id;
		usr.firstname='Test';
		usr.lastname='Test';
		usr.email='abc@difc.ae';
		usr.phone = '+97154345343';
		usr.Title = 'Mr.';
        usr.communityNickname = ('Khan' +string.valueof(Math.random()).substring(4,9));
		usr.alias = string.valueof(usr.communityNickname.substring(0,1) + string.valueof(Math.random()).substring(4,9));            
		usr.profileid = Label.Profile_ID;
		usr.emailencodingkey='UTF-8';
		usr.languagelocalekey='en_US';
		usr.localesidkey='en_GB';
		usr.timezonesidkey='Asia/Dubai';
		usr.Community_User_Role__c = 'Company Services';
        insert usr; 
        list<User> usrlist = new list<User>();
        usrlist.add(usr);
        list<Service_Request__c> SRList = new list<Service_Request__c>();
        SRList.add(objSR2);
        
        PropListingUserCls.CreatePortalCont(usrlist,con.id,null,null,SRList);
        
    }
}