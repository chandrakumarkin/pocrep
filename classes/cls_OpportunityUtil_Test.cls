@isTest(seeAllData=false)
public class cls_OpportunityUtil_Test {
  
    static testmethod void opportunitytest(){
        
            Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
            UserRole r = [SELECT Id FROM UserRole WHERE Name='Business Development'];
           user u = new User(alias = 'jsmith', email='jsmith@acme.com', 
                emailencodingkey='UTF-8', lastname='Smith', 
                languagelocalekey='en_US', 
                localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
                timezonesidkey='America/Los_Angeles', 
                username='shyam@acme.com');
            insert u;
        
   system.runAs(u){                      
      EmailTemplate e = new EmailTemplate (developerName = 'In_Principal_Approval_NFS1', FolderId = '00l20000001DSMmAAO', TemplateType= 'Text', Name = 'test'); 
      insert e;     
             
      Lead testCrmLead;
      testCrmLead  = new Lead();
      testCrmLead = Test_cls_Crm_Utils.getTestLead();
      testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
      testCrmLead.Activities__c = 'ATM';
      insert testCrmLead; 
            
      list<OrgWideEmailAddress> owa = [select id, Address from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
      
      In_Principal_BD__c prinbd = new In_Principal_BD__c();
      prinbd.Name = 'Non - financial';
      prinbd.CC_Email__c = 'c-mudasir.saleem@difc.ae';
      prinbd.Email_Template_Name__c = 'In_Principal_Approval_NFS1';
      insert prinbd; 
        
     List<Id> oppidlist = new List<id>();
            
     Account acc = new Account();
     acc.Company_Type__c = 'Non - financial';
     acc.Name = 'DIFC';
     acc.Lead_Reference_Number__c = '123456';
     insert acc;      
     
    RecordType rt =  [Select Id From RecordType where sobjecttype = 'Opportunity' and developername = 'NFS'];   
       
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today();
    opp.AccountId = acc.Id;
    opp.Lead_Email__c = 'abc@abc.com';
    opp.CC_Emails__c = 'abc@abc.com';
    opp.Lead_Id__c = testCrmLead.Id;
   // opp.StageName = 'Submitted to Leasing Committee'; 
    opp.StageName = 'Application Submitted to DFSA'; 
    opp.RecordTypeId = rt.id;
    insert opp;       
            
    oppidlist.add(opp.Id);    
            
     Test.startTest();
     cls_OpportunityUtil.SendInPrincipalEmailOpportunity(oppidlist);
     Test.stopTest();   
        }     
    }   
}