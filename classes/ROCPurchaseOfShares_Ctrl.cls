/*********************************************************************************************************************
    Author      :   Danish Farooq
    Description :   Purchase  of shares
    Date        :   20-March-2018
    Description :   This class is used for both Purchase of shares.
    
*********************************************************************************************************************/
public without sharing class ROCPurchaseOfShares_Ctrl{

    /* Start of Properties for Dynamic Flow */
    public boolean isChangesSaved = false;
    public boolean isProcessFlow {
        get;
        set;
    }
    public string strPageId {
        get;
        set;
    }
    public string strHiddenPageIds {
        get;
        set;
    }
    public string strActionId {
        get;
        set;
    }
    public map < string,string > mapParameters;
    public Service_Request__c objSRFlow {
        get;
        set;
    }

    public string pageTitle {
        get;
        set;
    }
    public string pageDescription {
        get;
        set;
    }
    public string checkFrmDetail {
        set;
        get;
    } // V1.3
  
    
    public List < AmendmentWrapper > lstAmendment {
        get;
        set;
    }
    
    public List<selectOption> lstShareHolder{
        
        get;
        set;
    }
    
    public string purposeOfShares {get;set;}
    
  
    
    Map<string,string> mapOfShareHolder;
    
    

    /* End of Properties for Dynamic Flow */

    Public string srStatus {
        get;
        set;
    } // V1.4 

    String SRId;
   
    Public String SRtype {
        get;
        set;
    }
   
    Public String strType {
        get;
        set;
    }
    public Service_Request__c obSR {
        get;
        set;
    }
    public Integer RowId {
        get;
        set;
    }
   
    
    public Map<string,List<selectOption>> mapofShareHolderClass;
    
    public Map<string,Shareholder_Detail__c> mapofClassesWithValues;
    
    boolean IsError;
    
    string RecordTypeID;

    public ROCPurchaseOfShares_Ctrl() {
        

     
       
        
        RecordTypeID = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Account Share Holder Detail').getRecordTypeId();
        SRId = ApexPages.currentPage().getParameters().get('Id');
        /* Start of Properties Initialization for Dynamic Flow */
        isProcessFlow = false;
        isChangesSaved = false;
        mapParameters = new map < string,string > ();
        
        if (apexpages.currentPage().getParameters() != null) mapParameters = apexpages.currentPage().getParameters();
        //V1.3
        if (mapParameters != null && mapParameters.containsKey('fd')) {
            checkFrmDetail = mapParameters.get('fd'); // V1.3
        }

        objSRFlow = new Service_Request__c();
        objSRFlow.Id = SRId;
       

        PropertyIntilizationFlow();
        /* End of Properties Initialization for Dynamic Flow */

        ServiceRequestDetail();
        
     
        
        GetShareholderDetails();
        
        GetAmendment();
  

    }
    
    public void GetShareholderDetails(){
        
        
        mapOfShareHolder = new Map<string,string>();
        List<Shareholder_Detail__c> lstShareHolderDetail = [Select ID, Account_Share__c, Account_Share__r.Name,
                                                    Account_Share__r.Nominal_Value__c,Account_Share__r.Currency__c,
                                                    No_of_Shares__c, Total_Nominal_Val__c,Shareholder_Account__c,
                                                    Shareholder_Account__r.Name,Shareholder_Contact__c,
                                                    Shareholder_Contact__r.FirstName,Shareholder_Contact__r.LastName
                                                    from Shareholder_Detail__c
                                                    where Account__c =: obSR.Customer__c
                                                    AND (NOT Shareholder_Account__r.Name Like '%Treasury%')
                                                    AND Status__c = 'Active'];
        
        if(lstShareHolderDetail.isEmpty()) return;
        
        mapofClassesWithValues = new Map<string,Shareholder_Detail__c>();
        mapofShareHolderClass = new Map<string,List<selectOption>>();
        lstShareHolder = new List<selectOption>();
        lstShareHolder.add(new SelectOption('','-None-'));
        string ContactName = '';
       
        
        for(Shareholder_Detail__c iShareHolder : lstShareHolderDetail){
            
            
            if(iShareHolder.Shareholder_Account__c != null){
                if(mapofShareHolderClass.containsKey(iShareHolder.Shareholder_Account__c))
                    mapofShareHolderClass.get(iShareHolder.Shareholder_Account__c).add(new SelectOption (iShareHolder.Account_Share__r.Name , iShareHolder.Account_Share__r.Name));
                else{
                      lstShareHolder.add(new SelectOption(iShareHolder.Shareholder_Account__c,iShareHolder.Shareholder_Account__r.Name));
                      mapOfShareHolder.put(iShareHolder.Shareholder_Account__c,iShareHolder.Shareholder_Account__r.Name);
                      
                      mapofShareHolderClass.put(iShareHolder.Shareholder_Account__c , new List<selectOption> { new SelectOption ('' , '-None-')});
                      mapofShareHolderClass.get(iShareHolder.Shareholder_Account__c).add(new SelectOption (iShareHolder.Account_Share__r.Name , iShareHolder.Account_Share__r.Name));
                }
                
                mapofClassesWithValues.put (iShareHolder.Shareholder_Account__c+'-'+iShareHolder.Account_Share__r.Name ,iShareHolder);
            }
            if(iShareHolder.Shareholder_Contact__c != null){
                
                ContactName = iShareHolder.Shareholder_Contact__r.FirstName + ' ' + iShareHolder.Shareholder_Contact__r.LastName;
                
                if(mapofShareHolderClass.containsKey(iShareHolder.Shareholder_Contact__c))
                    mapofShareHolderClass.get(iShareHolder.Shareholder_Contact__c).add(new SelectOption (iShareHolder.Account_Share__r.Name , iShareHolder.Account_Share__r.Name));
                else{
                    
                      lstShareHolder.add(new SelectOption(iShareHolder.Shareholder_Contact__c,ContactName));
                      mapOfShareHolder.put(iShareHolder.Shareholder_Contact__c,ContactName);
                      
                      mapofShareHolderClass.put(iShareHolder.Shareholder_Contact__c , new List<selectOption> { new SelectOption ('' , '-None-')});
                      mapofShareHolderClass.get(iShareHolder.Shareholder_Contact__c).add(new SelectOption (iShareHolder.Account_Share__r.Name , iShareHolder.Account_Share__r.Name));
                }
                
                mapofClassesWithValues.put (iShareHolder.Shareholder_Contact__c+'-'+iShareHolder.Account_Share__r.Name ,iShareHolder);
            }
            
            
            
        }
    }
    
    void GetAmendment(){
        
         IsError = false;
        List<Amendment__c> lstOfExistingAmm = [Select ID, Class_Name__c,No_of_shares_per_class__c,Currency__c,sys_create__c,
                                                Nominal_Value__c,Sys_Proposed_Nominal_Value__c,Sys_Proposed_No_of_Shares_per_Class__c,
                                                Currency__r.Name,Share_holder_company_txt__c,Shareholder_Company_val__c,
                                                Total_Issued__c, ServiceRequest__c FROM Amendment__c WHERE ServiceRequest__c =: SRId
                                                AND Status__c = 'Active' ORDER BY ID];
        
        lstAmendment = new List<AmendmentWrapper>();
        
        for(Amendment__c iAmm : lstOfExistingAmm){
            
            lstAmendment.add(new AmendmentWrapper(iAmm,false,false));
        }
        
        if(lstAmendment.isEmpty()){
            
              addRow();
              IsError = true;
              return;
        }
        
        string selectedShareHolder;
        for(AmendmentWrapper iAmnd : lstAmendment){
            
            selectedShareHolder = iAmnd.amm.Shareholder_Company_val__c;
            if(selectedShareHolder != '' && selectedShareHolder != null)
                iAmnd.lstSelectedClass = mapofShareHolderClass.get(selectedShareHolder);
            
        }
     
    }
    
    public void GetClassesAgainstShareHolder(){
        
        integer index = integer.valueof(ApexPages.currentPage().getParameters().get('index'));
        string selectedShareHolder = lstAmendment[index].amm.Shareholder_Company_val__c;
        
        
        if(selectedShareHolder != '' && selectedShareHolder != null){
            lstAmendment[index].lstSelectedClass = mapofShareHolderClass.get(selectedShareHolder);
            lstAmendment[index].EditClass = false;
            lstAmendment[index].amm.Share_holder_company_txt__c = mapOfShareHolder.get(selectedShareHolder);
            return;
        }
       
       lstAmendment[index].EditClass = true;
        
            
        
        
    }
    
    public void GetSharesAgainstClass(){
        
        integer index = integer.valueof(ApexPages.currentPage().getParameters().get('index'));
        string className = lstAmendment[index].amm.Class_Name__c;
        string shareHolder = lstAmendment[index].amm.Shareholder_Company_val__c;
        
        if(className != '' && className != null){
        
            lstAmendment[index].amm.Total_Issued__c = mapofClassesWithValues.get(shareHolder+'-'+className).No_of_Shares__c;
            lstAmendment[index].amm.Sys_Proposed_No_of_Shares_per_Class__c = 0.00;
          //  lstAmendment[index].amm.Class_of_Shares__c = mapofClassesWithValues.get(className).Account_Share__r.Name;
            
            lstAmendment[index].EditShares = false;
            return;
        }
        lstAmendment[index].amm.Total_Issued__c = 0.00;
        lstAmendment[index].amm.Sys_Proposed_No_of_Shares_per_Class__c = 0.00;
        lstAmendment[index].EditShares = true;
        
    }
    
    
 
   
    
    void ServiceRequestDetail(){
        
        Service_Request__c srDetails = [Select Share_Capital_Membership_Interest__c, Issued_Share_Capital_Membership_Interest__c, Date_of_Resolution__c, No_of_Authorized_Share_Membership_Int__c, No_of_Issued_Share_Membership_Interest__c, External_Status_Code__c, 
                                        currency_rate__r.Name,Total_Proposed_Authorized_Capital__c,Customer__c,currency_rate__c,
                                        Record_type_Name__c, currency_list__c,RecordType.DeveloperName from Service_Request__c
                                        where id = :objSRFlow.Id];

        srStatus = srDetails.External_Status_Code__c; // V1.4
      
        obSR = new Service_Request__c(Id = SRId,legal_structures__c = objSRFlow.legal_structures__c, Customer__c = objSRFlow.Customer__c, Share_Capital_Membership_Interest__c = srDetails.Share_Capital_Membership_Interest__c, Issued_Share_Capital_Membership_Interest__c = srDetails.Issued_Share_Capital_Membership_Interest__c, No_of_Authorized_Share_Membership_Int__c = srDetails.No_of_Authorized_Share_Membership_Int__c, No_of_Issued_Share_Membership_Interest__c = srDetails.No_of_Issued_Share_Membership_Interest__c, currency_list__c = srDetails.currency_list__c , Date_of_Resolution__c = srDetails.Date_of_Resolution__c, Total_Proposed_Authorized_Capital__c = srDetails.Total_Proposed_Authorized_Capital__c,currency_rate__r = srDetails.currency_rate__r,currency_rate__c = srDetails.currency_rate__c);
      
        strType = mapParameters.get('Type');
       
            if (strType == 'amendshares' && strType != null && objSRFlow.legal_structures__c == 'LLC') {
                strType = 'Member';
            } else {
                strType = 'Shareholder';
            }
        

        if (strType == 'Shareholder') {
            SRtype = 'Shares';
         
        }
        else {
            SRtype = 'Membership Interest';
         
        }
    }
    
    
   
    
    void PropertyIntilizationFlow() {

        if (objSRFlow.Id != null) {
            strPageId = mapParameters.get('PageId');
            if (strPageId != null && strPageId != '') {
                for (Page__c page: [select id, Name, Page_Description__c from Page__c where Id = :strPageId]) {
                    pageTitle = page.Name;
                    pageDescription = page.Page_Description__c;
                }
            }
            if (mapParameters != null && mapParameters.get('FlowId') != null) {
                set < string > SetstrFields = Cls_Evaluate_Conditions.FetchObjectFields(mapParameters.get('FlowId'), 'Service_Request__c');
                string strQuery = 'select Id';
                SetstrFields.add('legal_structures__c');
                SetstrFields.add('customer__c');
                SetstrFields.add('finalizeamendmentflg__c');
                SetstrFields.add('share_capital_membership_interest__c');
                SetstrFields.add('record_type_name__c');
                SetstrFields.add('currency_list__c');
                SetstrFields.add('currency_rate__c');
                SetstrFields.add('External_Status_Code__c');
                SetstrFields.add('isClosedStatus__c');
                SetstrFields.add('Commercial_Activity__c');
                //   SetstrFields.add('Record_type_Name__c'); // V1.3
                if (SetstrFields != null && SetstrFields.size() > 0) {
                    for (String strFld: SetstrFields) {
                        if (strFld.toLowerCase() != 'id') strQuery += ',' + strFld.toLowerCase();
                    }
                }

                strQuery = strQuery + ' from Service_Request__c where Id=:SRId';
                system.debug('strQuery===>' + strQuery);
                for (Service_Request__c SR: database.query(strQuery)) {
                    objSRFlow = SR;
                }
                strHiddenPageIds = PreparePageBlockUtil.getHiddenPageIds(mapParameters.get('FlowId'), objSRFlow);
            }
        }

    }

    
  
    public Component.Apex.PageBlock getDyncPgMainPB() {
        PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
        PreparePageBlockUtil.PageId = mapParameters.get('PageId');
        PreparePageBlockUtil.objSR = objSRFlow;
        PreparePageBlockUtil objPB = new PreparePageBlockUtil();
        return objPB.getDyncPgMainPB();
    }

    public pagereference DynamicButtonAction() {
        system.debug('strActionId==>' + strActionId);
        
       
       
        
        if(obSR.Date_of_Resolution__c > Date.Today()){
              Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Date of Resolution is not Greater than Today!'));
                return null;
        }
        
       

        if (strActionId != null && strActionId != '') {
            boolean isNext = false;
            boolean isAllowToproceed = false;
            for (Section_Detail__c objSec: [select id, Name, Component_Label__c, Navigation_Direction__c from Section_Detail__c where Id = :strActionId]) {
                if (objSec.Navigation_Direction__c == 'Forward') {
                    isNext = true;
                    /*if(IsError ||  (lstAmendment.size() > 0 && lstAmendment[0].amm .ID == NULL)){
            
                        Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.Error, 'Cannot Proceed to Next Step before Saving this Record'));
                            return null;
                    }*/
                    
                    if(!SaveShareDetails()) return null;

                    
                } else {
                    isNext = false;
                }
            }
            
            PreparePageBlockUtil.FlowId = mapParameters.get('FlowId');
            PreparePageBlockUtil.PageId = mapParameters.get('PageId');
            PreparePageBlockUtil.objSR = objSRFlow;
            PreparePageBlockUtil.ActionId = strActionId;

            PreparePageBlockUtil objPB = new PreparePageBlockUtil();
            pagereference pg = objPB.getButtonAction();
            system.debug('pg==>' + pg);
            //update SR fields
            update obSR;
            return pg;
            
        }
        return null;
    }
    
    public void Save_ShareDetail() {
        
        SaveShareDetails();
        
    }
    

    boolean  SaveShareDetails() {
        
        List<Amendment__c> lstAmnd = new List<Amendment__c>();
        Set<string> setOfClasses = new Set<string>();
        for(AmendmentWrapper iAmmWrapper : lstAmendment){
              
              if(setOfClasses.contains(iAmmWrapper.amm.Shareholder_Company_val__c+'-'+iAmmWrapper.amm.Class_Name__c)) {
                 ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Please remove duplicate classes'));
                 //IsError = true;
                 return false;
              }  
            
              if( iAmmWrapper.amm.Sys_Proposed_No_of_Shares_per_Class__c > iAmmWrapper.amm.Total_Issued__c){
                
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Purchase of shares cannot be greater than Issued shares of class "'+iAmmWrapper.amm.Class_Name__c +'"'));
                   //IsError = true;
                 return false;
              }     
              
              lstAmnd.add(iAmmWrapper.amm);
              setOfClasses.add(iAmmWrapper.amm.Shareholder_Company_val__c+'-'+iAmmWrapper.amm.Class_Name__c);

        }
                  
        if(lstAmnd.isEmpty()) return false;
        
        upsert lstAmnd;
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Records are saved successfully'));
        //IsError = false;
        return true;
        
    }
    
  
    
    public void delRow() {
        
        Amendment__c amm =   lstAmendment.get(RowId).amm;
        
        if(amm.ID != NULL)
            delete [Select id from Amendment__c  where ID =: amm.ID limit 1];
            
        lstAmendment.remove(RowId);
        IsError = false;
    
    }
    public void addRow() {
        
        system.debug(obSR.finalizeAmendmentFlg__c);
        if (!obSR.finalizeAmendmentFlg__c) {
            
            IsError = true;
            Amendment__c amnd = new Amendment__c();
            amnd.Total_Issued__c = 0;
            amnd.ServiceRequest__c = SRId;
            amnd.Status__c = 'Active';
            amnd.sys_create__c = false;
            amnd.RecordTypeID = RecordTypeID;
            lstAmendment.add(new AmendmentWrapper(amnd,true,true));            
        } else {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error, 'Your request has been finalised and cannot be updated.'));
            return;
        }
    }

    public string strNavigatePageId {
        get;
        set;
    }
    public pagereference goTopage() {
        if (strNavigatePageId != null && strNavigatePageId != '') {
            PreparePageBlockUtil objSidebarRef = new PreparePageBlockUtil();
            PreparePageBlockUtil.strSideBarPageId = strNavigatePageId;
            PreparePageBlockUtil.objSR = objSRFlow;
            return objSidebarRef.getSideBarReference();
        }
        return null;
    }

   

    public pagereference BackToServiceRequest() {
        pagereference pg;
        if (obSR.id != null) pg = new pagereference('/' + obSR.id);

        return pg;
    }
    
    public class AmendmentWrapper{
        
        
        public amendment__c amm {get;set;}
        public boolean EditClass {get;set;}
        public boolean EditShares {get;set;}
        public List<selectOption> lstSelectedClass{ get; set;}
        
        public AmendmentWrapper(amendment__c aAmm, boolean aEditClass, boolean aEditShares){
            amm = aAmm;
            EditClass = aEditClass;
            EditShares = aEditShares;
        }
        
    }
    
   
  }