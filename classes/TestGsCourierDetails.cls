/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class TestGsCourierDetails {
    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;
        
        list<Status__c> lstStatus = new list<Status__c>();        
        Status__c sts = new Status__c();
        sts.Name = 'PENDING_COLLECTION';
        sts.Code__c = 'PENDING_COLLECTION';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'PENDING_DELIVERY';
        sts.Code__c = 'PENDING_DELIVERY';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'AWAITING_ORIGINALS';
        sts.Code__c = 'AWAITING_ORIGINALS';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'PENDING_REVIEW';
        sts.Code__c = 'PENDING_REVIEW';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'VERIFIED';
        sts.Code__c = 'VERIFIED';
        lstStatus.add(sts);
        insert lstStatus;
        
        SR_Status__c SRsts = new SR_Status__c();
        SRsts.Name = 'Delivered';
        SRsts.Code__c = 'Delivered';
        SRsts.Type__c = 'End';
        insert SRsts;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Step_Template__c> lstStepTemplates = new list<Step_Template__c>();
        
        Step_Template__c stpTem = new Step_Template__c();
        stpTem.Name = 'GS_COURIER_COLLECTION';
        stpTem.Code__c = 'GS_COURIER_COLLECTION';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Delivery';
        stpTem.Courier_Type__c = 'Collection';
        lstStepTemplates.add( stpTem );
        stpTem = new Step_Template__c();
        stpTem.Name = 'GS_COURIER_DELIVERY';
        stpTem.Code__c = 'GS_COURIER_DELIVERY';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Delivery';
        stpTem.Courier_Type__c = 'Collection';
        lstStepTemplates.add( stpTem );
        insert lstStepTemplates;
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_Shareholder_Details','Sale_or_Transfer_of_Shares_or_Membership_Interest','Allotment_of_Shares_Membership_Interest')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.legal_structures__c ='LTD SPC';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c =90;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR1.Consignee_FName__c = 'Test';
        objSR1.Consignee_LName__c = 'Test';
        //objSR1.Send_SMS_To_Mobile__c = '+97112345670';
        objSR1.Use_Registered_Address__c = false;
        objSR1.Apt_or_Villa_No__c = 'testing123';
        objSR1.Email__c = 'test@test.com';
        objSR1.Avail_Courier_Services__c = 'Yes';
        objSR1.Courier_Cell_Phone__c = '+97112345670';
        objSR1.Courier_Mobile_Number__c = '+97112345670';
        objSR1.SR_Group__c = 'GS';
        objSR1.SAP_Unique_No__c = '1234567890';
        insert objSR1;
        
        Test.setMock(WebServiceMock.class, new Test_CC_CourierCls());
        
        Step__c objStp1 = new Step__c();
        objStp1.SR__c = objSR1.Id;
        //objStp1.Step_Template__c = lstStepTemplates[1].id;
        objStp1.SR_Group__c = 'GS';
        objStp1.SAP_DOCRC__c = 'X';
        insert objStp1;
        
        test.startTest();
            database.executeBatch(new GsBatchCourierDetails(), 1);
            GsCourierDetails.CloseAwaitingOriginals(new list<Id>{objStp1.Id});
        test.stopTest();
    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;
        
        list<Status__c> lstStatus = new list<Status__c>();        
        Status__c sts = new Status__c();
        sts.Name = 'PENDING_COLLECTION';
        sts.Code__c = 'PENDING_COLLECTION';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'PENDING_DELIVERY';
        sts.Code__c = 'PENDING_DELIVERY';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'AWAITING_ORIGINALS';
        sts.Code__c = 'AWAITING_ORIGINALS';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'PENDING_REVIEW';
        sts.Code__c = 'PENDING_REVIEW';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'VERIFIED';
        sts.Code__c = 'VERIFIED';
        lstStatus.add(sts);
        //insert lstStatus;
        sts = new Status__c();
        sts.Name = 'CLOSED';
        sts.Code__c = 'CLOSED';
        lstStatus.add(sts);
        insert lstStatus;
        
        SR_Status__c SRsts = new SR_Status__c();
        SRsts.Name = 'Delivered';
        SRsts.Code__c = 'Delivered';
        SRsts.Type__c = 'End';
        insert SRsts;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Step_Template__c> lstStepTemplates = new list<Step_Template__c>();
        
        Step_Template__c stpTem = new Step_Template__c();
        stpTem.Name = 'GS_COURIER_COLLECTION';
        stpTem.Code__c = 'GS_COURIER_COLLECTION';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Delivery';
        stpTem.Courier_Type__c = 'Collection';
        lstStepTemplates.add( stpTem );
        stpTem = new Step_Template__c();
        stpTem.Name = 'GS_COURIER_DELIVERY';
        stpTem.Code__c = 'GS_COURIER_DELIVERY';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Delivery';
        stpTem.Courier_Type__c = 'Collection';
        lstStepTemplates.add( stpTem );
        insert lstStepTemplates;
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_Shareholder_Details','Sale_or_Transfer_of_Shares_or_Membership_Interest','Allotment_of_Shares_Membership_Interest')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.legal_structures__c ='LTD SPC';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c =90;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR1.Consignee_FName__c = 'Test';
        objSR1.Consignee_LName__c = 'Test';
        //objSR1.Send_SMS_To_Mobile__c = '+97112345670';
        objSR1.Use_Registered_Address__c = false;
        objSR1.Apt_or_Villa_No__c = 'testing123';
        objSR1.Email__c = 'test@test.com';
        objSR1.Avail_Courier_Services__c = 'Yes';
        objSR1.Courier_Cell_Phone__c = '+97112345670';
        objSR1.Courier_Mobile_Number__c = '+97112345670';
        objSR1.SR_Group__c = 'GS';
        objSR1.SAP_Unique_No__c = '1234567890';
        insert objSR1;
        
        Test.setMock(WebServiceMock.class, new Test_CC_CourierCls());
        
        Step__c objStp1 = new Step__c();
        objStp1.SR__c = objSR1.Id;
        //objStp1.Step_Template__c = lstStepTemplates[1].id;
        objStp1.SR_Group__c = 'GS';
        objStp1.SAP_DOCRC__c = 'X';
        objStp1.Sys_Courier_Check__c = true;
        objStp1.Courier_Enabled__c = true;
        objStp1.SAP_Seq__c = '0010';
        insert objStp1;
        
        GsCourierDetails.IsFired = false;
        
        Step__c objStp2 = new Step__c();
        objStp2.SR__c = objSR1.Id;
        objStp2.Step_Template__c = lstStepTemplates[1].id;
        objStp2.SR_Group__c = 'GS';
        objStp2.Status__c = lstStatus[1].Id;
        objStp2.Sys_Courier_Check__c = true;
        objStp2.Is_Auto_Closure__c = true;
        objStp2.SAP_Seq__c = '0020';
        objStp2.SAP_DOCCR__c = 'X';
        insert objStp2;
        
        
        
        
        
        test.startTest();
            //database.executeBatch(new GsBatchCourierDetails());
        test.stopTest();       
        
       
         
      
     
        Shipment__c shpm = new Shipment__c();
        shpm.Step__c = objStp1.id;
        shpm.Service_Request__c = objSR1.id;
        insert shpm;
        
        GsCourierDetails.CloseRocShipments(new list<id>{objStp1.id});
        
        
        
        
        
       
        
        
    }
   
    static  testmethod void mytestmethod2()
    {
        Courier_Client_Info__c CCInfo = new Courier_Client_Info__c();
        CCInfo.Name = 'Courier Delivery';
        CCInfo.Username__c = 'abc@test.com';
        CCInfo.Password__c = 'abcd1234';
        CCInfo.Version_No__c = 'v1.0';
        CCInfo.Account_No__c = '1234';
        CCInfo.Account_Pin__c = '1234';
        CCInfo.Account_Entity__c = 'DXB';
        CCInfo.Account_Country_Code__c = 'AE';
        CCInfo.Person_Name__c = 'Test';
        CCInfo.Company_Name__c = 'DIFC';
        CCInfo.Phone_Number__c = '+9711234567';
        CCInfo.Cell_Phone__c = '+9711234567';
        CCInfo.Email_Address__c = 'test@test.com';
        insert CCInfo;
        
        list<Status__c> lstStatus = new list<Status__c>();        
        Status__c sts = new Status__c();
        sts.Name = 'PENDING_COLLECTION';
        sts.Code__c = 'PENDING_COLLECTION';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'PENDING_DELIVERY';
        sts.Code__c = 'PENDING_DELIVERY';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'AWAITING_ORIGINALS';
        sts.Code__c = 'AWAITING_ORIGINALS';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'PENDING_REVIEW';
        sts.Code__c = 'PENDING_REVIEW';
        lstStatus.add(sts);
        sts = new Status__c();
        sts.Name = 'VERIFIED';
        sts.Code__c = 'VERIFIED';
        lstStatus.add(sts);
        //insert lstStatus;
        sts = new Status__c();
        sts.Name = 'CLOSED';
        sts.Code__c = 'CLOSED';
        lstStatus.add(sts);
        insert lstStatus;
        
        SR_Status__c SRsts = new SR_Status__c();
        SRsts.Name = 'Delivered';
        SRsts.Code__c = 'Delivered';
        SRsts.Type__c = 'End';
        insert SRsts;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Application_of_Registration';
        objTemplate.SR_RecordType_API_Name__c = 'Application_of_Registration';
        objTemplate.Menutext__c = 'Application_of_Registration';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Step_Template__c> lstStepTemplates = new list<Step_Template__c>();
        
        Step_Template__c stpTem = new Step_Template__c();
        stpTem.Name = 'GS_COURIER_COLLECTION';
        stpTem.Code__c = 'GS_COURIER_COLLECTION';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Delivery';
        stpTem.Courier_Type__c = 'Collection';
        lstStepTemplates.add( stpTem );
        stpTem = new Step_Template__c();
        stpTem.Name = 'GS_COURIER_DELIVERY';
        stpTem.Code__c = 'GS_COURIER_DELIVERY';
        stpTem.Step_RecordType_API_Name__c = 'Courier_Delivery';
        stpTem.Courier_Type__c = 'Collection';
        lstStepTemplates.add( stpTem );
        insert lstStepTemplates;
        
        map<string,string> mapRecordType = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Application_of_Registration','Change_Shareholder_Details','Sale_or_Transfer_of_Shares_or_Membership_Interest','Allotment_of_Shares_Membership_Interest')]){
            mapRecordType.put(objRT.DeveloperName,objRT.Id);
        }
        
        Service_Request__c objSR1 = new Service_Request__c();
        objSR1.Customer__c = objAccount.Id;
        objSR1.legal_structures__c ='LTD SPC';
        objSR1.currency_list__c = 'US Dollar';
        objSR1.Share_Capital_Membership_Interest__c =90;
        objSR1.No_of_Authorized_Share_Membership_Int__c = 5;
        objSR1.Consignee_FName__c = 'Test';
        objSR1.Consignee_LName__c = 'Test';
        //objSR1.Send_SMS_To_Mobile__c = '+97112345670';
        objSR1.Use_Registered_Address__c = false;
        objSR1.Apt_or_Villa_No__c = 'testing123';
        objSR1.Email__c = 'test@test.com';
       // objSR1.Avail_Courier_Services__c = 'Yes';
        objSR1.Courier_Cell_Phone__c = '+97112345670';
        objSR1.Courier_Mobile_Number__c = '+97112345670';
        objSR1.SR_Group__c = 'GS';
        objSR1.SAP_Unique_No__c = '1234567890';
        insert objSR1;
        
        Test.setMock(WebServiceMock.class, new Test_CC_CourierCls());
        
        Step__c objStp1 = new Step__c();
        objStp1.SR__c = objSR1.Id;
        //objStp1.Step_Template__c = lstStepTemplates[1].id;
        objStp1.SR_Group__c = 'GS';
        objStp1.SAP_DOCRC__c = 'X';
        objStp1.Sys_Courier_Check__c = true;
        objStp1.Courier_Enabled__c = true;
        objStp1.SAP_Seq__c = '0010';
        insert objStp1;
        
        GsCourierDetails.IsFired = false;
        
        Step__c objStp2 = new Step__c();
        objStp2.SR__c = objSR1.Id;
        objStp2.Step_Template__c = lstStepTemplates[1].id;
        objStp2.SR_Group__c = 'GS';
        objStp2.Status__c = lstStatus[1].Id;
        objStp2.Sys_Courier_Check__c = true;
        objStp2.Is_Auto_Closure__c = true;
        objStp2.SAP_Seq__c = '0020';
        objStp2.SAP_DOCCR__c = 'X';
        insert objStp2;
        
        
        
        
        
        test.startTest();
            database.executeBatch(new GsBatchCourierDetails());
        test.stopTest();       
        
       
         
      
     
        Shipment__c shpm = new Shipment__c();
        shpm.Step__c = objStp1.id;
        shpm.Service_Request__c = objSR1.id;
        insert shpm;
        
        GsCourierDetails.CloseRocShipments(new list<id>{objStp1.id});
         
        
    }
   
    
    
    
}