@isTest(SeeAllData=false)
public class Test_OpportunityTrigger {
    public static Offer_Process__c op;
    public static List<Offer_Process__c> opList;
    public static Lead testCrmLead;
 public static void createData(){
        
        Building__c b = new Building__c();
        b.Company_Code__c = '5300';
        b.DIFC_FitOut__c = true;
        insert b;
        
        Unit__c unit = new Unit__c();
        unit.Building__c = b.ID;
        unit.Name = 'Test Building';
        insert unit;
        
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;

        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        
        
        insert testCrmLead; // Create a test lead   
        
        op = new Offer_Process__c();
        op.OfferValidityTill__c = Date.Today();
        op.isActive__c = true;
        op.Units__c = unit.Id;
        op.IsCutomerSelected__c = false;
        op.Lead__c= testCrmLead.id;
        op.Leasing_Terms_Year__c =2;
        op.Rent_Free_Period__c = 45;
        op.Leasing_Terms_Months__c = 1;

        insert op;
        
        Offer_Process__c op2 = new Offer_Process__c();
        op2.OfferValidityTill__c = Date.Today();
        op2.isActive__c = false;
        op2.Units__c = unit.Id;
        op2.IsCutomerSelected__c = false;
        op2.Lead__c= testCrmLead.id;
        op2.Leasing_Terms_Year__c =2;
        op2.Rent_Free_Period__c = 40000;
        op.Leasing_Terms_Months__c = 2;

        insert op2;
        
        opList = new List<Offer_Process__c>();
        opList.add(op);
        opList.add(op2);
        
    }
static Testmethod void Testmethod_OpportunityTrigger(){        
Id RecordTypeIdopportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();        
String gsRecordTypeID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
 OpportunityTriggerUtility.isOfferProcessupdated = false;
    
        Lead testCrmLead;
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        insert testCrmLead;
    
    
    
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    insert acc;
   system.debug('Lead reference Number' +acc.Lead_Reference_Number__c);
    
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
     opp.AccountId = acc.Id;
    opp.RecordTypeId = gsRecordTypeID;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Confirmed Unit';
    opp.Lead_Id__c = testCrmLead.Id;
    insert opp;
    
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:opp.id];
        System.assertEquals(1, attachments.size());
    
    
  
    Map<Id,string> leadId = new Map<Id,string>();
    leadId.put(opp.Id,opp.Lead_Id__c);
    system.debug('leadId Value lead id'+leadId);
    Test.startTest();  
    List<Opportunity> oppList =  new List<Opportunity>();
    oppList.add(opp);   
     OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
     OpportunityTriggerUtility.isAttched(oppList);
    
    Test.stopTest();  
    }
    
static Testmethod void Testmethod1_OpportunityTrigger(){   
   test.startTest();
        createData();
        
        Account testAccount = Test_cls_Crm_Utils.getTestCrmAccount(Test_cls_Crm_Utils.CRM_DEFAULT_CUSTOMERNAME,Test_cls_Crm_Utils.CRM_DEFAULT_COMPANYTYPE);
        
        insert testAccount;
        
        Opportunity testOpportunity = Test_cls_Crm_Utils.getTestOpportunity(Test_cls_Crm_Utils.CRM_DEFAULT_OPPORTUNITYNAME,testAccount.Id,'Confirmed Unit');
        testOpportunity.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Opportunity' AND DeveloperName = 'Offer_Process'].Id;
        testOpportunity.Oppty_Rec_Type__c = 'Leasing OfferProcess';
        insert testOpportunity;
        
        op.Opportunity__c = testOpportunity.id;
        
        update op;
        
        testOpportunity.Lead_Id__c = testCrmLead.Id;
        update testOpportunity;
        
        Map<Id,string> leadId = new Map<Id,string>();
        leadId.put(testOpportunity.Lead_Id__c,testOpportunity.Id);
        
        List<Attachment> attList = new List<Attachment>();
        
        for(Integer i= 0;i<10;i++){
            Attachment iAtt = new Attachment();
            iAtt.parentid = testOpportunity.ID;
            iAtt.Name = 'testABC';
            iAtt.Body = Blob.valueOf('Unit Test Attachment Body');
            attList.add(iAtt);
        }
        
        Insert attList;
        
        List<Opportunity> oppList =  new List<Opportunity>();
        oppList.add(testOpportunity);
        
        OpportunityTriggerUtility opp = new OpportunityTriggerUtility(); 
        OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
        OpportunityTriggerUtility.isAttched(oppList);
        test.stopTest();
    }
    
static Testmethod void Testmethod2_OpportunityTrigger(){        
Id RecordTypeIdopportunity = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();        
String gsRecordTypeID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
 OpportunityTriggerUtility.isOfferProcessupdated = false;
    
        Lead testCrmLead;
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        insert testCrmLead;
    
    
    
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    insert acc;
   system.debug('Lead reference Number' +acc.Lead_Reference_Number__c);
    
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
     opp.AccountId = acc.Id;
    opp.RecordTypeId = gsRecordTypeID;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Send Signed Lease Agreement to Client';
    opp.Lead_Id__c = testCrmLead.Id;
    insert opp;
    
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:opp.id];
        System.assertEquals(1, attachments.size());
    
    
  
    Map<Id,string> leadId = new Map<Id,string>();
    leadId.put(opp.Id,opp.Lead_Id__c);
    system.debug('leadId Value lead id'+leadId);
    Test.startTest();  
    List<Opportunity> oppList =  new List<Opportunity>();
    oppList.add(opp);   
     OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
     OpportunityTriggerUtility.isAttched(oppList);
    Test.stopTest();  
    }    
 static Testmethod void Testmethod3_OpportunityTrigger(){ 
 String gsRecordTypeID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
    
        Lead testCrmLead;
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        insert testCrmLead;
     
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    insert acc;
   system.debug('Lead reference Number' +acc.Lead_Reference_Number__c);
    
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
     opp.AccountId = acc.Id;
    opp.RecordTypeId = gsRecordTypeID;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Lease Agreement Sent to Client';
    opp.Lead_Id__c = testCrmLead.Id;
    insert opp;
    
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:opp.id];
        System.assertEquals(1, attachments.size());
    Map<Id,string> leadId = new Map<Id,string>();
    leadId.put(opp.Id,opp.Lead_Id__c);
     
    Test.startTest();  
    List<Opportunity> oppList =  new List<Opportunity>();
    oppList.add(opp);   
     OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
     OpportunityTriggerUtility.isAttched(oppList);   
 }
    
 static Testmethod void Testmethod4_OpportunityTrigger(){ 
 String gsRecordTypeID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
    
        Lead testCrmLead;
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        insert testCrmLead;
     
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    insert acc;
   system.debug('Lead reference Number' +acc.Lead_Reference_Number__c);
    
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
     opp.AccountId = acc.Id;
    opp.RecordTypeId = gsRecordTypeID;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Reservation Payment Received and Confirmed';
    opp.Lead_Id__c = testCrmLead.Id;
    insert opp;
    
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:opp.id];
        System.assertEquals(1, attachments.size());
    Map<Id,string> leadId = new Map<Id,string>();
    leadId.put(opp.Id,opp.Lead_Id__c);
     
    Test.startTest();  
    List<Opportunity> oppList =  new List<Opportunity>();
    oppList.add(opp);   
     OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
     OpportunityTriggerUtility.isAttched(oppList);   
 }

static Testmethod void Testmethod5_OpportunityTrigger(){ 
 String gsRecordTypeID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
    
        Lead testCrmLead;
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        insert testCrmLead;
     
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    insert acc;
   system.debug('Lead reference Number' +acc.Lead_Reference_Number__c);
    
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
     opp.AccountId = acc.Id;
    opp.RecordTypeId = gsRecordTypeID;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Send Reservation Letter Received';
    opp.Lead_Id__c = testCrmLead.Id;
    insert opp;
    
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:opp.id];
        System.assertEquals(1, attachments.size());
    Map<Id,string> leadId = new Map<Id,string>();
    leadId.put(opp.Id,opp.Lead_Id__c);
     
    Test.startTest();  
    List<Opportunity> oppList =  new List<Opportunity>();
    oppList.add(opp);   
     OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
     OpportunityTriggerUtility.isAttched(oppList);   
 }  
    static Testmethod void Testmethod6_OpportunityTrigger(){ 
 String gsRecordTypeID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
    
        Lead testCrmLead;
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        insert testCrmLead;
     
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    insert acc;
   system.debug('Lead reference Number' +acc.Lead_Reference_Number__c);
    
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
     opp.AccountId = acc.Id;
    opp.RecordTypeId = gsRecordTypeID;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Signed Lease Agreement Received';
    opp.Lead_Id__c = testCrmLead.Id;
    insert opp;
    
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:opp.id];
        System.assertEquals(1, attachments.size());
    Map<Id,string> leadId = new Map<Id,string>();
    leadId.put(opp.Id,opp.Lead_Id__c);
     
    Test.startTest();  
    List<Opportunity> oppList =  new List<Opportunity>();
    oppList.add(opp);   
     OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
     OpportunityTriggerUtility.isAttched(oppList);   
 } 
    
    static Testmethod void Testmethod7_OpportunityTrigger(){ 
 String gsRecordTypeID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
    
        Lead testCrmLead;
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        insert testCrmLead;
     
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    insert acc;
   system.debug('Lead reference Number' +acc.Lead_Reference_Number__c);
    
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
     opp.AccountId = acc.Id;
    opp.RecordTypeId = gsRecordTypeID;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Signed Reservation LetterReceived';
    opp.Lead_Id__c = testCrmLead.Id;
    insert opp;
    
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:opp.id];
        System.assertEquals(1, attachments.size());
    Map<Id,string> leadId = new Map<Id,string>();
    leadId.put(opp.Id,opp.Lead_Id__c);
     
    Test.startTest();  
    List<Opportunity> oppList =  new List<Opportunity>();
    oppList.add(opp);   
     OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
     OpportunityTriggerUtility.isAttched(oppList);   
 }     
    
   static Testmethod void Testmethod8_OpportunityTrigger(){ 
 String gsRecordTypeID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
    
        Lead testCrmLead;
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        insert testCrmLead;
     
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    insert acc;
   system.debug('Lead reference Number' +acc.Lead_Reference_Number__c);
    
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
     opp.AccountId = acc.Id;
    opp.RecordTypeId = gsRecordTypeID;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'Reservation Letter Sent to Client';
    opp.Lead_Id__c = testCrmLead.Id;
    insert opp;
    
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:opp.id];
        System.assertEquals(1, attachments.size());
    Map<Id,string> leadId = new Map<Id,string>();
    leadId.put(opp.Id,opp.Lead_Id__c);
     
    Test.startTest();  
    List<Opportunity> oppList =  new List<Opportunity>();
    oppList.add(opp);   
     OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
     OpportunityTriggerUtility.isAttched(oppList);   
 }         
    
      static Testmethod void Testmethod9_OpportunityTrigger(){ 
 String gsRecordTypeID = Schema.getGlobalDescribe().get('opportunity').getDescribe().getRecordTypeInfosByName().get('Offer Process').getRecordTypeId();
    
        Lead testCrmLead;
        testCrmLead  = new Lead();
        testCrmLead = Test_cls_Crm_Utils.getTestLead();
        testCrmLead.RecordTypeId = [SELECT Id FROM RecordType WHERE SObjectType = 'Lead' AND DeveloperName = 'Leasing_OfferProcess'].Id;
        testCrmLead.Sector_Classification__c = 'Automated Teller Machines';
        testCrmLead.Activities__c = 'ATM';
        insert testCrmLead;
     
    Account acc = new Account();
    acc.Name = 'DIFC';
    acc.Lead_Reference_Number__c = '123456';
    insert acc;
   system.debug('Lead reference Number' +acc.Lead_Reference_Number__c);
    
    Opportunity opp = new Opportunity();
    opp.Name = 'DIFC Financial';
    opp.CloseDate = System.today() + 5;
     opp.AccountId = acc.Id;
    opp.RecordTypeId = gsRecordTypeID;
    opp.Oppty_Rec_Type__c = 'Leasing OfferProcess';
    opp.StageName = 'LAF Process';
    opp.Lead_Id__c = testCrmLead.Id;
    insert opp;
    
        Attachment attach=new Attachment();       
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=opp.id;
        insert attach;
        
        List<Attachment> attachments=[select id, name from Attachment where parent.id=:opp.id];
        System.assertEquals(1, attachments.size());
    Map<Id,string> leadId = new Map<Id,string>();
    leadId.put(opp.Id,opp.Lead_Id__c);
     
    Test.startTest();  
    List<Opportunity> oppList =  new List<Opportunity>();
    oppList.add(opp);   
     OpportunityTriggerUtility.updateOfferProcessonOpportunity(leadId);
     OpportunityTriggerUtility.isAttched(oppList);   
 }         
    
    
    }