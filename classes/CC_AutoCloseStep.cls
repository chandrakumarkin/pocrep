/*
Author      : Durga Prasad
Date        : 09-Oct-2019
Description : Custom code to Close the Current Step
--------------------------------------------------------------------------------------
[6.MAY.2020] Prateek Kadkol - Edited to incorporate docusign auto close

      23.Nov.2020   Abbas          - To update draft ChildSR's status to submitted(#12453)
v1.0  11.Apr.2021   Abbas          - (#12453) updated code as per Arun's comment
*/
global without sharing class CC_AutoCloseStep implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR,HexaBPM__Step__c stp) {
        string strResult = 'Success';
        HexaBPM__Step__c step = new HexaBPM__Step__c(Id=stp.Id);
        HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__c);
        
        //System.debug('ZZZ CC_AutoCloseStep.cls->stp.HexaBPM__SR__c->'+stp.HexaBPM__SR__c);
        //System.debug('ZZZ CC_AutoCloseStep.cls->stp.HexaBPM__Status__c->'+stp.HexaBPM__Status__c);
        //System.debug('ZZZ CC_AutoCloseStep.cls->stp.HexaBPM__SR_Step__c->'+stp.HexaBPM__SR_Step__c );
        
        for(HexaBPM__Step_Transition__c stptran : [select HexaBPM__SR_Status_Internal__c,HexaBPM__SR_Status_External__c,HexaBPM__Transition__r.HexaBPM__To__c,HexaBPM__Transition__r.HexaBPM__From__c
                                                   from HexaBPM__Step_Transition__c where HexaBPM__Transition__c!=null and HexaBPM__SR_Step__c=:stp.HexaBPM__SR_Step__c and HexaBPM__Transition__r.HexaBPM__From__c=:stp.HexaBPM__Status__c and HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Type__c = 'End' and HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Rejection__c = false and HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Reupload_Document__c=false and HexaBPM__Transition__r.HexaBPM__To__r.Return_for_More_Information__c=false limit 1
                                                  ]){
                                                      step.HexaBPM__Status__c = stptran.HexaBPM__Transition__r.HexaBPM__To__c;
                                                      if(stptran.HexaBPM__SR_Status_Internal__c!=null)
                                                          objSR.HexaBPM__Internal_SR_Status__c = stptran.HexaBPM__SR_Status_Internal__c;
                                                      if(stptran.HexaBPM__SR_Status_External__c!=null)
                                                          objSR.HexaBPM__External_SR_Status__c = stptran.HexaBPM__SR_Status_External__c;
                                                      break;
                                                  }
        
        
        //Added on 23/11/2020 to update draft ChildSR's status to submitted
        HexaBPM__Service_Request__c[] childAppToUpdateLst = new List<HexaBPM__Service_Request__c>();
        HexaBPM__Service_Request__c[] exisitingChildSRLst  = [SELECT Id,Name
                                                              FROM HexaBPM__Service_Request__c
                                                              WHERE HexaBPM__Parent_SR__c = :stp.HexaBPM__SR__c
                                                              AND HexaBPM__Parent_SR__r.OB_Is_Multi_Structure_Application__c='Yes' //v1.0 Added on 11-04-2021 to take only multistructure SR (#12453)
                                                              AND HexaBPM__Internal_SR_Status__r.Name = 'Draft'
                                                             ];
        System.debug('ZZZ CC_AutoCloseStep.cls->childSRLst Size->'+exisitingChildSRLst.size());
        if(!exisitingChildSRLst.isEmpty()){
            for(HexaBPM__SR_Status__c status :[select id, Name 
                                               from HexaBPM__SR_Status__c 
                                               where Name = 'Submitted' or Name = 'submitted' limit 1]) {
                                                   for(HexaBPM__Service_Request__c childSR : exisitingChildSRLst){
                                                       childSR.HexaBPM__Internal_SR_Status__c = status.id;
                                                       childSR.HexaBPM__External_SR_Status__c = status.id;
                                                       childAppToUpdateLst.add(childSR);
                                                   }
                                               }
        }
        
        
        
        
        
        try{
            //system.debug('ZZZ CC_AutoCloseStep.cls->STEP To Update-->'+step);
            update step;
            if(objSR.HexaBPM__Internal_SR_Status__c!=null || objSR.HexaBPM__External_SR_Status__c!=null)
                update objSR;
            //system.debug('ZZZ CC_AutoCloseStep.cls->UPDATED SR STATUS======'+objSR.HexaBPM__Internal_SR_Status__c+'===='+objSR.HexaBPM__External_SR_Status__c);
            
            if(!childAppToUpdateLst.isEmpty()){
                //System.debug('ZZZ CC_AutoCloseStep.cls->childSRToUpdateLst SIZE->'+childAppToUpdateLst.size());
                update childAppToUpdateLst;
            }
        }catch(DMLException e){
            //strResult = e.getmessage()+'';
            string DMLError = e.getdmlMessage(0)+'';
            if(DMLError==null){
                DMLError = e.getMessage()+'';
            }
            strResult = DMLError;
            
        }
        return strResult;
    }
}