/******************************************************************************************
 *  Author   : 
 *  Company  : 
 *  Date     :
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    23/06/2021                 Initial Draft
 
**********************************************************************************************************************/
public class CourierCollectionController {
  static String receptionId = '';
   
   /**
   * Helper method to update status of passport for tracking 
   * @param  searchValue,Selected Passport Tracking Ids. 
   * @return Either success or Error message.          
   */
  @AuraEnabled
public static string updatePassportTracking(String selectedId,String selectedValue) {
  
  Map<String,String> newDataMap                       = new Map<String,String>();
  List<Passport_Tracking__c> previousPassportTracking = new List<Passport_Tracking__c>();
  system.debug('========selectedId=========='+selectedId);
  system.debug('========selectedValue=========='+selectedValue);
  List<Object> fieldList = (List<Object>)JSON.deserializeUntyped(selectedId);

  Set<string> stepIds = new Set<string>();
    
  for(Object thisObject : fieldList){ 
    Map<String,Object> dataMap = (Map<String,Object>)thisObject;
    String Ids             = (String)dataMap.get('Id');
    String srNumber        = (String)dataMap.get('SR_Number__c');
    String passportNumber  = (String)dataMap.get('Passport_Number__c');
    String clientName      = (String)dataMap.get('Client_Name__c');
    String stepname        = (String)dataMap.get('Step__c');
    String srType          =  (String)dataMap.get('SRType__c');
    String applicantName   =  (String)dataMap.get('Applicant_Name__c');
    
    if(stepname != null)stepIds.add(stepname);
    previousPassportTracking.add(new Passport_Tracking__c(Id =Ids,
      SR_Number__c = srNumber,
      //Passport_Number__c = passportNumber,
      client_Name__c = clientName,
      Step__c = stepname,
      SRType__c = srType,
      Applicant_Name__c=applicantName
    ));
  }

  Map<Id,Step__c> stepMap = new Map<Id,Step__c>([
      select Id,Step_Name__c from Step__c where Id IN:stepIds
  ]);
       
  List<Passport_Tracking__c> newPassportTrackingList = new List<Passport_Tracking__c>();
  List<Passport_Tracking__c> updatePassportTrackingList = new List<Passport_Tracking__c>();
       
  try{
    for(Passport_Tracking__c previousPassport: previousPassportTracking){
      previousPassport.Has_Status_Updated__c    = true;
               
      Passport_Tracking__c newPassportTracking = previousPassport.clone(false, true);
          newPassportTracking.Status__c            = selectedValue;
          newPassportTracking.Has_Status_Updated__c = false;
          if(newPassportTracking.Step__c != null &&
              stepMap.containskey(newPassportTracking.Step__c) &&
              stepMap.get(newPassportTracking.Step__c).Step_Name__c != null &&
              stepMap.get(newPassportTracking.Step__c).Step_Name__c =='Courier Collection' &&
              selectedValue =='Received by Office Boy'
          ){
              newPassportTracking.Has_Status_Updated__c = true; 
          }
          newPassportTrackingList.add(newPassportTracking); 
          updatePassportTrackingList.add(previousPassport);
      }
    system.debug('-11----updatePassportTrackingList------------------'+updatePassportTrackingList);
    system.debug('-111---newPassportTrackingList------------------'+newPassportTrackingList);
    
    if(!updatePassportTrackingList.isEmpty()){ 
              update updatePassportTrackingList;
    }
    if(!newPassportTrackingList.isEmpty()){
      insert newPassportTrackingList;
    }
    system.debug('-22----updatePassportTrackingList------------------'+updatePassportTrackingList);
    system.debug('-222-newPassportTrackingList------------------'+newPassportTrackingList);
           return 'Success : Record Status Updated!';
  }catch(Exception ex){
            Log__c objLog = new Log__c();
            objLog.Type__c = 'Document Tracking';
            objLog.Description__c = ex.getMessage() + ' at line ' + ex.getLineNumber() + ' of class PassportTrackingController';
            insert objLog; 
            return 'error occured' +ex.getMessage();
  }
}

  @AuraEnabled
   public static ResponseTrackingWrapper getResponseWrapperList(
       String searchValue,string searchStatus,string changeStatus
  ) {
      ResponseTrackingWrapper resp = new ResponseTrackingWrapper();
      
      system.debug('==============searchValue==============='+searchValue); 
      system.debug('==============searchStatus==============='+searchStatus); 
       
      List<PassportTrackingWrapper> thisWrapperList = new List<PassportTrackingWrapper>();
      String queryString = 'SELECT Id,client_Name__c,Step__c,Applicant_Name__c,SRType__c,Passport_Number__c,SR_Number__c,Status__c,Step__r.Name,Step__r.Step_Name__c,Step__r.AWB_Number__c,Step__r.SR_Type_Step__c,Step__r.SR_Status__c,Step__r.SR_Menu_Text__c,Step__r.Closed_Date_Time__c FROM Passport_Tracking__c';
       
      String profileName=[Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;
      resp.isOfficeBoy = (profileName == 'GS Office Boy Team' ) ? true : false;
      resp.isAramex = (profileName != 'GS Office Boy Team' ) ? true : false;

      if(!Test.isRunningTest())  { receptionId = [SELECT Id From Profile WHERE Name=: System.Label.Document_Tracking_Profile].Id;}
  
  //queryString  = queryString+ ' WHERE Status__c != null and Status__c != \'Delivered\' and Status__c != \'Collected\' and ( Step__r.Step_Name__c = \'Courier Collection\' OR Step__r.Step_Name__c = \'Courier Delivery\') AND Has_Status_Updated__c= FALSE ';
    // queryString  = queryString+ ' WHERE Status__c != null and (( Status__c != \'Delivered\' and Step__r.Step_Name__c = \'Courier Delivery\') OR (Status__c != \'Received by Office boy\' and Step__r.Step_Name__c = \'Courier Collection\' ))AND Has_Status_Updated__c= FALSE ';
  queryString  = queryString+ ' WHERE Status__c != null and (( Status__c != \'Delivered\' and Step__r.Step_Name__c = \'Courier Delivery\') OR (Status__c != \'Closed\' and Status__c != \'Received by Office boy\' and Step__r.Step_Name__c = \'Courier Collection\' ))AND Has_Status_Updated__c= FALSE ';

      if(searchStatus != '')queryString  += ' and Step__r.Step_Name__c =:searchStatus ';
      
  if(String.isNotBlank(searchValue)){
          string newValue = String.valueOf(searchValue.trim());
          //queryString =queryString+ ' AND SR_Number__c like \'%'+ string.escapeSingleQuotes(newValue) + '%\'';  
    queryString =queryString+ ' AND SR_Number__c =:newValue';  
  }
         
      queryString = queryString+ ' order by CreatedDate desc';
      system.debug('=======queryString==========='+queryString);

       for(Passport_Tracking__c thisTracking : Database.query(queryString)){
          system.debug('=======thisTracking==========='+thisTracking);
           thisWrapperList.add(new PassportTrackingWrapper(thisTracking,resp.isOfficeBoy,resp.isAramex));
        }
      resp.passportTrackingList = thisWrapperList;
      return resp;
   }

  public class ResponseTrackingWrapper{
      @AuraEnabled
      public List<PassportTrackingWrapper> passportTrackingList {get;set;}
      @AuraEnabled
      public Boolean isOfficeBoy  {get;set;}
      @AuraEnabled
      public Boolean isAramex  {get;set;}
  }
  
  public class PassportTrackingWrapper{
      @AuraEnabled
      public Passport_Tracking__c passportTrackingList {get;set;}
      @AuraEnabled
      public Boolean isChecked  {get;set;}
      @AuraEnabled
      public Boolean isReceptionId  {get;set;}

      @AuraEnabled
      public Boolean isOfficeBoy  {get;set;}
      @AuraEnabled
      public string changeStatusMatch{get;set;}
      @AuraEnabled
      public Boolean isUpdateAllowed  {get;set;}
      
      public PassportTrackingWrapper(Passport_Tracking__c passportTrackingList, boolean isOfficeBoyLogin,boolean isAramexLogin){
          this.passportTrackingList = passportTrackingList;
          this.isUpdateAllowed = true;
          this.changeStatusMatch =''; 
          
          //if(passportTrackingList.Step__r.Step_Name__c == 'Courier Collection' && passportTrackingList.Status__c =='Received by Office boy')this.isUpdateAllowed = false;
          if(passportTrackingList.Step__r.Step_Name__c == 'Courier Collection' && passportTrackingList.Status__c =='Pending Collection')this.isUpdateAllowed = false;
         // if(passportTrackingList.Step__r.Step_Name__c == 'Courier Collection' && passportTrackingList.Status__c =='Pending Collection' && isOfficeBoyLogin)this.changeStatusMatch = 'Collected';         
          if(passportTrackingList.Step__r.Step_Name__c == 'Courier Collection' && passportTrackingList.Status__c =='Courier Collected' && isOfficeBoyLogin)this.changeStatusMatch = 'Received by Office Boy';         
  
          if(passportTrackingList.Step__r.Step_Name__c == 'Courier Delivery' && passportTrackingList.Status__c =='Pending Delivery' && isOfficeBoyLogin)this.isUpdateAllowed = false;
          if(passportTrackingList.Step__r.Step_Name__c == 'Courier Delivery' && passportTrackingList.Status__c =='Received by Aramex' && isAramexLogin)this.isUpdateAllowed = false;
          if(passportTrackingList.Step__r.Step_Name__c == 'Courier Delivery' && passportTrackingList.Status__c =='Received by Office boy')this.isUpdateAllowed = false;
          if(passportTrackingList.Step__r.Step_Name__c == 'Courier Delivery' && passportTrackingList.Status__c =='Pending Delivery' && isAramexLogin)this.changeStatusMatch = 'Received by Aramex';         
          if(passportTrackingList.Step__r.Step_Name__c == 'Courier Delivery' && passportTrackingList.Status__c =='Received by Aramex' && isOfficeBoyLogin)this.changeStatusMatch = 'Received by Office Boy';
          system.debug('=====final======this.isUpdateAllowed========================'+this.isUpdateAllowed);
          if(UserInfo.getProfileId() == receptionId){
              this.isReceptionId   = true;
          }
      }
  }
}