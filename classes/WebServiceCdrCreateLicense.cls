public class WebServiceCdrCreateLicense {
    
    public static String createJsonLicenseCdr(String LicenseId){
        
        Account Acc =   [SELECT id, Unified_License_Number__c, BP_No__c, Active_License__r.name, Active_License__r.License_Issue_Date__c, Active_License__r.License_Type__c, Active_License__r.License_Expiry_Date__c, Active_License__r.License_Terminated_Date__c,
                         Active_License__r.ROC_Status_List__c, Name, Arabic_Name__c, Authorized_Share_Capital__c, Home_Country__c, Company_Type__c, Trade_Name__c, Trading_Name_Arabic__c, (Select Id, License_Type__c from Licenses__r),
                         Sectors__c, Issued_Share_Capital_AccShare__c, (Select Id, Activity__r.Name, Activity__r.Activity_Code__c FROM License_Activities__r) from Account Where Active_License__c =:LicenseId];
        
        JSONGenerator jsonObj = JSON.createGenerator(true);
        jsonObj.writeStartArray();    
        jsonObj.writeStartObject();
        jsonObj.writeFieldName('activityMaster');
        jsonObj.writeStartArray(); 
        for(License_Activity__c ActivityObj :Acc.License_Activities__r){
            jsonObj.writeStartObject();
            if(ActivityObj != null)
                if(!String.isBlank(ActivityObj.Activity__r.Activity_Code__c))
            jsonObj.writeStringField('activityCode', ActivityObj.Activity__r.Activity_Code__c);
            if(!String.isBlank(ActivityObj.Activity__r.Name))
            jsonObj.writeStringField('activityName', ActivityObj.Activity__r.Name);
            jsonObj.writeEndObject();     
        }
        jsonObj.writeEndArray();
        if(!String.isBlank(Acc.BP_No__c) && Acc.BP_No__c != null)
        jsonObj.writeStringField('accountNumber',Acc.BP_No__c);
        if(!String.isBlank(Acc.Unified_License_Number__c) && Acc.Unified_License_Number__c != null)
        jsonObj.writeStringField('unifiedNumber',Acc.Unified_License_Number__c);
        jsonObj.writeStringField('zoneName', 'DIFC');
           if(Acc.Active_License__r != null && !String.isBlank(Acc.Active_License__r.name))
        jsonObj.writeStringField('licenseNumber', Acc.Active_License__r.name);
           //if(Acc.Active_License__r != null && !String.isBlank(Acc.Active_License__r.License_Type__c))
        jsonObj.writeStringField('licenseType', 'License_Type__c');
           if(Acc.Active_License__r != null && (Acc.Active_License__r.License_Issue_Date__c != null))
        jsonObj.writeDateTimeField('issuedOn', Acc.Active_License__r.License_Issue_Date__c);
        if(Acc.Active_License__r != null && (Acc.Active_License__r.License_Expiry_Date__c != null))
        jsonObj.writeDateTimeField('expiresOn', Acc.Active_License__r.License_Expiry_Date__c);
        if(Acc.Active_License__r != null && (Acc.Active_License__r.License_Terminated_Date__c != null))
        jsonObj.writeDateTimeField('cancelledOn', Acc.Active_License__r.License_Terminated_Date__c);
        if(Acc.Active_License__r != null && !String.isBlank(Acc.Active_License__r.ROC_Status_List__c))
        jsonObj.writeStringField('licenseStatus', Acc.Active_License__r.ROC_Status_List__c);
        jsonObj.writeEndObject();
        jsonObj.writeEndArray(); 
        System.debug('jsonObj:-'+jsonObj.getAsString());
        return jsonObj.getAsString();
    }
    
    public static void PostLicenseToCdr(String LicenseId){
        
            String jsonRequest;
            List<Log__c> ErrorObjLogList = new List<Log__c>();
            //Get authrization token 
            String Token = WebServiceCDRGenerateToken.cdrAuthToken();
            //WebService to Create the Account
            HttpRequest request = new HttpRequest();
            request.setEndPoint(System.label.Cdr_Post_License_Url);
            jsonRequest = createJsonLicenseCdr(LicenseId);
            request.setBody(jsonRequest);
            request.setTimeout(120000);
            request.setHeader('Accept', 'text/plain');
            request.setHeader('Content-Type', 'application/json-patch+json');
            request.setHeader('Authorization','Bearer'+' '+Token);
            request.setMethod('POST');
            HttpResponse response = new HTTP().send(request);
            System.debug('responseStringresponse:'+response.toString());
            System.debug('STATUS:'+response.getStatus());
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('strJSONresponse:'+response.getBody());
            request.setTimeout(110000);
        
        if (response.getStatusCode() == 200)
        {           
            Log__c ErrorobjLog = new Log__c();
            ErrorobjLog.Type__c = 'CDR License Upload';
            ErrorobjLog.Description__c = 'Request: '+ jsonRequest +'Response: '+ response.getBody() ;
            ErrorobjLog.sObject_Record_Id__c = LicenseId;
            ErrorObjLogList.add(ErrorobjLog);
            
        }
        else
        {
            Log__c ErrorobjLog = new Log__c();
            ErrorobjLog.Type__c = 'CDR License Upload Error';
            ErrorobjLog.Description__c = 'Request: '+ jsonRequest +'Response: '+ response.getBody() ;
            ErrorobjLog.sObject_Record_Id__c = LicenseId;
            ErrorObjLogList.add(ErrorobjLog);      
        }
        
        Insert ErrorObjLogList;
            
    }

}