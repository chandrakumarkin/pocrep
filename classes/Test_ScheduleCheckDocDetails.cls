/******************************************************************************************
 *  Author   : Shabbir Ahmed
 *  Company  : DIFC
 *  Date     : 24-Apr-2015   
                
*******************************************************************************************/
@isTest(seealldata=false)
private class Test_ScheduleCheckDocDetails {

    static testMethod void myUnitTest() {
        Id gsContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();

    	Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;

    	Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = gsContactRecordTypeId;
        insert objContact;

		Document_Details__c empVisa = new Document_Details__c();
		empVisa.Contact__c = objContact.Id;
		empVisa.Document_Type__c = 'Temporary Work Permit';
		empVisa.DOCUMENT_STATUS__c = 'Active';
		empVisa.ISSUE_DATE__c = system.today().addDays(-3);
		empVisa.EXPIRY_DATE__c = system.today().addDays(-1);
		insert empVisa;
		
		Relationship__c rel = new Relationship__c();
		rel.Subject_Account__c = objAccount.Id;
		rel.Object_Contact__c = objContact.Id;
		rel.Relationship_Type__c = 'Has Temporary Employee';
		insert rel;
		
		ScheduleCheckDocDetails s = new ScheduleCheckDocDetails();
		s.execute(null);
    }
    
    static testMethod void visaQuotaUnitTest() {
        
        Id gsContactRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
		Id gsSRTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-New').getRecordTypeId();
		
		map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
	    for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c' OR SobjectType='Contact']){
	        mapSRRecordTypes.put(objType.DeveloperName,objType);
	    }
		
    	Account objAccount = new Account();
        objAccount.Name = 'Test Account';
        insert objAccount;
		
		CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
		
	    list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
	    SR_Status__c objSRStatus;
	    objSRStatus = new SR_Status__c();
	    objSRStatus.Name = 'Submitted';
	    objSRStatus.Code__c = 'SUBMITTED';
	    lstSRStatus.add(objSRStatus);
		insert lstSRStatus;
		
        Service_Request__c newSR = new Service_Request__c();
        newSR.recordTypeId = gsSRTypeId;
        newSR.Customer__c = objAccount.Id;
        newSR.Service_Category__c = 'New';
        newSR.External_SR_Status__c = lstSRStatus[0].Id;
        insert newSR;

    	Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = gsContactRecordTypeId;
        objContact.SR__c = newSR.Id;
        objContact.Passport_No__c = 'ABC12345';
        objContact.Nationality__c = 'India';
        insert objContact;

		Document_Details__c empVisa = new Document_Details__c();
		empVisa.Contact__c = objContact.Id;
		empVisa.Document_Type__c = 'Employment Visa';
		empVisa.DOCUMENT_STATUS__c = 'Active';
		empVisa.ISSUE_DATE__c = system.today().addDays(-3);
		empVisa.EXPIRY_DATE__c = system.today().addDays(-1);
		insert empVisa;
		
		Relationship__c rel = new Relationship__c();
		rel.Subject_Account__c = objAccount.Id;
		rel.Object_Contact__c = objContact.Id;
		rel.Relationship_Type__c = 'Has DIFC Sponsored Employee';
		rel.Active__c = true;
		rel.Document_Active__c = true;
		insert rel;
		
		Lease__c lease = new Lease__c();
		//lease.Visa_Factor__c = 8;
		lease.Square_Feet__c = '5000';
		lease.Account__c = objAccount.Id;
		lease.Lease_Types__c = 'Residential Lease-Out';
		lease.Status__c = 'Active';
		insert lease;
		
		Service_Request__c objSR = new Service_Request__c();
		objSR.Customer__c = objAccount.Id;
		objSR.RecordTypeId = mapSRRecordTypes.get('Company_Temporary_Work_Permit_Request_Letter').Id;
		objSR.Service_Category__c = 'Renewal Temporary Work Permit';
		objSR.Email__c = 'testsr@difc.com';
		objSR.ApplicantId_hidden__c = objContact.Id;
		objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
		insert objSR;
		
		objSR.Internal_SR_Status__c = objSRStatus.Id;
		objSR.External_SR_Status__c = objSRStatus.Id;
		update objSR;
		
		ScheduleCheckDocDetails s = new ScheduleCheckDocDetails();
		s.execute(null);
    }    
    
        
}