@isTest
global class webServiceGdrfaLoginMock implements HttpCalloutMock{
    
    //Implement http mock callout here
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest request){
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setStatusCode(200);
        response.setBody('{"access_token": "eyJc0MDU1RjVFMTBDQjRFRTI0QjZDMUQiLg","expires_in": 1,"token_type": "Bearer","IsSuccess": true, "Success": true, "TranslatedText": "على","applicationId": "123","batchId": "123", "paymentDate": "2021-10-10", "Documents": [ { "documentTypeId": "907fe67d-95ac-4cff-b427-53d18b325392", "IsMandatory": true,"documentNameEn": "Personal Photo", "documentNameAr": "صورة شخصية" }], "fees": [{ "feeTypeId": "7757f3d3-9fae-4cb9-a85b-70ded1d91a38",  "amount": 50, "nameEn": "Service Fee","nameAr": "رسم الخدمة","isMandatory": true, "isParent": false, "isChild": false}]}');     
        return response;
    }
    
}