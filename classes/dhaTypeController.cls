public class dhaTypeController{

public dhaTypeController(){}

public List<Step__c> ListSteps {get;set;}

public void queryAllDhaTypeSRs(){

    try{
        //Collecting SRs for requirement: Remove applications if the status of upload 'health insurance certificate' not equal to 'uploaded'    
        Set<ID> SRDIds = new Set<Id>();
        List<SR_Doc__c> SRDocs = [select id, Service_Request__c from SR_Doc__c where Name = 'Health Insurance Certificate' and Status__c != 'Uploaded' and Service_Request__r.SR_Group__c = 'GS'];    
        for(SR_Doc__c srdoc : SRDocs){
            SRDIds.add(srdoc.Service_Request__c);
        }    
        
        
        // Collecting SRs for requirement: Remove SRs in case a step 'Documents received after bio metrics' is generated but the step status is equal to 'Awaiting Review
        Set<ID> SRIds = new Set<Id>();
        List<Step__c> lstSteps = [select id, SR__c from Step__c where  Step_Name__c = 'Received Documents after EID biometrics' 
                                    AND Status__r.Code__c = 'AWAITING_REVIEW' AND SR__r.SR_Group__c = 'GS'  ];    
        for(Step__c stp : lstSteps ){
            SRIds.add(stp.SR__c);
        }
        
        // List of Steps to be shown on vf page                         
        ListSteps = [select id, Name, SR__c, SR__r.Name, SR__r.Customer__c, Applicant_Name__c,Created_Date_Time__c, Is_It_Express_Service__c, Step_Name__c, SR_Menu_Text__c, SR_Status__c, Ok_to_Proceed__c 
                                    from Step__c 
                                    where 
                                    (SR__c != null AND                                                                 
                                    SR__c  NOT In : SRIds AND 
                                    SR__c  NOT In : SRDIds AND                                 
                                    SR__r.SR_Group__c = 'GS' AND                          
                                    ( SR_Status__c != 'Application is pending' AND
                                    SR_Status__c != 'The applicant is medically unfit' AND 
                                    SR_Status__c != 'Cancelled' AND SR_Status__c != 'Cancellation Approved' AND 
                                    SR_Status__c != 'Cancellation approved - Refund pending' AND 
                                    SR_Status__c != 'Process Completed' AND
                                    SR_Status__c != 'Refund Processed'                                 
                                    ) AND 
                                    (Step_Name__c = 'Visa Stamping Form is Typed' OR
                                    Step_Name__c = 'Visa Stamping Application is submitted online' OR
                                    Step_Name__c = 'Visa Stamping Typed' OR
                                    Step_Name__c = 'Renewal Form is Typed' ) AND
                                    (Step_Status__c = 'Awaiting Review' OR Step_Status__c = 'Pending Review')
                                    )                                
                                    Limit 2000
                                  ];
    }    catch(exception ex){ApexPages.addMessages(ex);}
   
}

}