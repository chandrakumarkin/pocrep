public class Batch_UpdateActiveLease  implements Schedulable,Database.Batchable<sObject>{

    public Database.QueryLocator start(Database.BatchableContext BC){
      	String query ='select id,Active_Leases__c,Lease_Expiry_Date__c,(select id,Lease__c from Operating_Locations__r where Status__c=\'Active\' AND Area__c !=\'0\' ORDER BY Area_WF__c desc limit 1),(select id from Lease__r where Status__c=\'Active\') from Account where (ROC_Status__c=\'Pending Dissolution\' OR ROC_Status__c=\'Pending De-registration\')';
        system.debug('### query '+query);
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<account> ListAccounts){
        for(Account acc : ListAccounts){
            acc.Active_Leases__c=acc.Lease__r.Size();
        }
        update ListAccounts;
    }
    
    public void finish(Database.BatchableContext BC){
    	system.debug('Job is completed successfully');
    }
    
    public void execute(SchedulableContext ctx){
        Database.executeBatch(new Batch_UpdateActiveLease());
    }
}