/*
    Author      : Leeba
    Date        : 03-April-2020
    Description : Test class for OB_AcctContactTrigger,OB_AcctContactTriggerHandler and OB_AcctContactTriggerHelper
    --------------------------------------------------------------------------------------
 */
@isTest
public class OB_AcctContactTriggerHandlerTest{

    public static testMethod void OB_AcctContactTriggerHandler1() {
    
        
       
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       Account acc1 = new Account();
       acc1.name = 'test1';
       insert acc1;
       
       contact con = new Contact();
       con.LastName = 'test';
       con.FirstName = 'test';
       con.Email = 'test@test.com';
       con.AccountId = acc1.id;
       con.Role__c = 'Fit-Out Services';
       insert con;   

        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community Plus User Custom']; 
         
        User u = new User(Alias = 'standt', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',contactid = con.Id,IsActive=true);
       
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.OB_Application__c = objHexaSR.Id;
        insert objSR;
        
        SR_Steps__c objSrStep = new SR_Steps__c();
        objSrStep.Active__c = true;
        objSrStep.Assigned_to_Client__c = true;
        insert objSrStep;
        
        Status__c st = new Status__c();
        st.Code__c = 'Draft';
        st.Type__c = 'Start';
        insert st;
        
        Step__c objstep = new Step__c();
        objstep.SR__c = objSR.Id;
        objstep.SR_Step__c = objSrStep.id;
        objstep.Status__c = st.id;
        insert objstep;
        
        test.startTest();
       
            AccountContactRelation acccon = new AccountContactRelation();
            acccon.AccountId = acc.Id;
            acccon.contactId = con.id;
            acccon.IsActive = true;
            acccon.Access_Level__c = 'Read';
            acccon.Roles = 'Company Services';
            insert acccon;
            
            acccon.Roles = 'Property Services';
            update acccon;
            
            acccon.IsActive = false;
            update acccon;
            
            acccon.IsActive = true;
            update acccon;
            
            delete acccon;
         
       	 test.stopTest();
       
    }
    
    
    public static testMethod void OB_AcctContactTriggerHandler2() 
    {
    
        
       
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       Account acc1 = new Account();
       acc1.name = 'test1';
       insert acc1;
       
       contact con = new Contact();
       con.LastName = 'test';
       con.FirstName = 'test';
       con.Email = 'test@test.com';
       con.AccountId = acc1.id;
       con.Role__c = 'Event Services';
       insert con;   

        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community Plus User Custom']; 
         
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',contactid = con.Id);
       
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'In_Principle';
       insert objsrTemp;
       
       
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('In Principle').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.OB_Application__c = objHexaSR.Id;
        insert objSR;
        
        
        AccountContactRelation acccon = new AccountContactRelation();
        acccon.AccountId = acc.Id;
        acccon.contactId = con.id;
        acccon.IsActive = true;
        acccon.Access_Level__c = 'Read/Write';
        acccon.Roles = 'Company Services';
        insert acccon;
        
        
        
        acccon.Roles = 'Property Services';
        update acccon;
        
        acccon.IsActive = false;
        update acccon;
        
        acccon.IsActive = true;
        update acccon;
        
        delete acccon;
       
    }
    
    
    // CreateHistoryRecords
    public static testMethod void OB_AcctContactTriggerHelperTest() 
    {
           
           Map<Id,AccountContactRelation> mapNew = new Map<Id,AccountContactRelation>();
            Map<Id,AccountContactRelation> mapOld = new Map<Id,AccountContactRelation>();
		   List<AccountContactRelation> lstNew = new List<AccountContactRelation>();			

           Account acc = new Account();
           acc.name = 'test';
           insert acc;
        
           Account acc1 = new Account();
           acc1.name = 'test';
           insert acc1;
           
           contact con = new Contact();
           con.LastName = 'test';
           con.FirstName = 'test';
           con.Email = 'test@test.com';
           con.AccountId = acc.id;
           con.Role__c = 'Event Services';
           insert con;  
        
           //anothe relation ship
            AccountContactRelation acccon1 = new AccountContactRelation();
            acccon1.AccountId = acc1.Id;
            acccon1.contactId = con.id;
            acccon1.IsActive = true;
            acccon1.Access_Level__c = 'Read Only';
            acccon1.Roles = 'Company Services';
            insert acccon1;
        
        	mapOld.put(acccon1.Id,acccon1);
            
        
        
       		mapNew = new Map<Id,AccountContactRelation>( [
                                                           SELECT id,
                                                                  AccountId,
                                                                  contactId,
                                                                  IsActive,
                                                                  Access_Level__c,
                                                                  Roles
                                                             FROM AccountContactRelation 
                                                            WHERE AccountId =: acc.Id
                                                              AND contactId =: con.id
                                                         ]
                                                       );  
        	History_Tracking__c objCS = new History_Tracking__c();
            objCS.Field_Label__c = 'Access_Level__c';
            objCS.Field_Name__c = 'Access_Level__c';
            objCS.Object_Name__c = 'accountcontactrelation';
            objCS.Name = 'accountcontactrelation';
            insert objCS;
            
        
        
        test.startTest();
        	lstNew = mapNew.values();
        	OB_AcctContactTriggerHelper.CreateHistoryRecords(lstNew ,null,true);
            //mapOld = mapNew;
			lstNew[0].Access_Level__c = 'Read Only';
            update lstNew[0];
            //OB_AcctContactTriggerHelper.CreateHistoryRecords(lstNew,mapOld,false);
        	
        test.stopTest();
        
	
    }
   public static testMethod void OB_AcctContactTriggerHelperTestPropertyService() 
    {  
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       Account acc1 = new Account();
       acc1.name = 'test1';
       insert acc1;
       
       contact con = new Contact();
       con.LastName = 'test';
       con.FirstName = 'test';
       con.Email = 'test@test.com';
       con.AccountId = acc1.id;
       con.Role__c = 'Property services';
       insert con;  
       List<AccountContactRelation> lstNew = new List<AccountContactRelation>();
        test.startTest();
        	OB_AcctContactTriggerHelper.CreateHistoryRecords(lstNew ,null,true);
        test.stopTest();
    }
  	public static testMethod void OB_AcctContactTriggerHelperTestportalUser() 
    {  
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       Account acc1 = new Account();
       acc1.name = 'test1';
       insert acc1;
       
       string contRecTypeId = '';
       for(RecordType rectyp:[select id from RecordType where DeveloperName='Portal_User' and sObjectType='Contact']){
            contRecTypeId = rectyp.Id;
       }  
       contact con = new Contact();
       con.LastName = 'test';
       con.FirstName = 'test';
       con.Email = 'test@test.com';
       con.AccountId = acc1.id;
       con.Role__c = 'Company services';
       con.RecordTypeId = contRecTypeId;
       insert con;  
       List<AccountContactRelation> lstNew = new List<AccountContactRelation>();
        test.startTest();
        	OB_AcctContactTriggerHelper.CreateHistoryRecords(lstNew ,null,true);
        test.stopTest();
    }
}