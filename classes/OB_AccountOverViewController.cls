/**
 * Description : used to fech data to be displayed in the Acount overview tab
 *
 * ****************************************************************************************
 * History :
 * [31.OCT.2019] Prateek Kadkol - Code Creation
 */
public without sharing class OB_AccountOverViewController {
    @AuraEnabled
    public static RespondWrap getConDetails() {
        //declaration of wrapper

        RespondWrap respWrap = new RespondWrap();
        /*
         for(user conObj :[SELECT id, contact.Account.Registration_No__c, contact.Account.Registration_License_No__c, contact.Account.Office__c, contact.Account.Building_Name__c, contact.Account.Active_License__r.Lic_Expiry_Date_with_format__c FROM user where Id = :UserInfo.getUserId() LIMIT 1]) {
         respWrap.currentContact = conObj;
         }
         */
        //Mudasir updated the below query to display the contrator details if the logged in user is contractor
        for(user conObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())){
            if(conObj.ContactId!=null && conObj.contact.AccountId!=null) {
                respWrap.currentContact = conObj;
            }
            
        }
        respWrap.isSuperUser = OB_AgentEntityUtils.hasSpecifiedRole(respWrap.currentContact.ContactId,respWrap.currentContact.contact.AccountId,'Super User');
        return respWrap;
    }

    // ------------ Wrapper List ----------- //



    public class RespondWrap {

        @AuraEnabled public user currentContact { get; set; }
        @AuraEnabled public boolean isSuperUser { get; set; }

        public RespondWrap() {
        }
    }
}