/*
    Author      : 
    Date        : 
    Description : Test class for CC_SuspenseOfLicense_Clearance
    --------------------------------------------------------------------------------------
*/
@isTest
public with sharing class CC_SuspenseOfLicense_ClearanceTest {

    public static testmethod void testMethod1(){
      
        Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
       
       contact con = new Contact();
       con.LastName = 'test';
       con.FirstName = 'test';
       con.Email = 'test@test.com';
       insert con;   

        Profile p = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community Plus User Custom']; 
         
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com',contactid = con.Id);
       
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'License Suspension';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'Suspension_Of_License';
       insert objsrTemp;
       
       
        Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('Request for Suspension Of License').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.HexaBPM__Email__c ='test@gmail.com';
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        objHexastep.HexaBPM__Step_No__c = 70;
        insert objHexastep;
        
        Lease__c lease= new Lease__c ();
        lease.Account__c = acc.ID;
        lease.status__c = 'Active';
        lease.End_Date__c = Date.Today().AddDays(5);
        lease.Square_Feet__c = '58';
        lease.Type__c = 'Leased Property';
        lease.Is_License_to_Occupy__c  = true;
        lease.No_of_Desks__c = 2;
        lease.Lease_Types__c ='Leased Property';
        insert lease;
        
       
        test.startTest();
            CC_SuspenseOfLicense_Clearance nsr = new CC_SuspenseOfLicense_Clearance();
            nsr.EvaluateCustomCode(null,objHexastep);
            
            objHexastep.HexaBPM__Step_No__c = 80;
            update objHexastep;
            
            CC_SuspenseOfLicense_Clearance nsr1 = new CC_SuspenseOfLicense_Clearance();
            nsr1.EvaluateCustomCode(null,objHexastep);
            
            
            objHexastep.HexaBPM__Step_No__c = 90;
            update objHexastep;
            
            objHexaSR.Approval_Date_Sharing_of_Office__c = system.today();
            update objHexaSR;
            
            CC_SuspenseOfLicense_Clearance_Approval nsr2 = new CC_SuspenseOfLicense_Clearance_Approval();
            nsr2.EvaluateCustomCode(null,objHexastep);
            
            objHexastep.HexaBPM__Step_No__c = 80;
            update objHexastep;
            
            CC_SuspenseOfLicense_Clearance_Approval nsr3 = new CC_SuspenseOfLicense_Clearance_Approval();
            nsr3.EvaluateCustomCode(null,objHexastep);
            
            CC_SuspenseOfLicense_Internal_Clearance nsr4 = new CC_SuspenseOfLicense_Internal_Clearance();
            nsr4.EvaluateCustomCode(null,objHexastep);
            

            HexaBPM__Step__c objHexastepNew = [select Id,HexaBPM__SR__c,HexaBPM__SR__r.HexaBPM__Email__c,HexaBPM__SR__r.HexaBPM__Customer__c 
                from HexaBPM__Step__c where Id =:objHexastep.Id limit 1];
            NewSRUtilityController.SendEmailNotifications(objHexaSR,objHexastepNew,'',new list<string>(),'');
        test.stopTest();
        
        
    }

}