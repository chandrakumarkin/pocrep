/**
 * Description : Controller for OB_EntityDropdownComponent component
 *
 * ****************************************************************************************
 * History :
 * [06.FEB.2020] Prateek Kadkol - Code Creation
 */
public without sharing class OB_EntityDropdownComponentController {


@AuraEnabled
public static RespondWrap fetchRelatedEntities(String requestWrapParam)  {

    //declaration of wrapper
    RequestWrap reqWrap = new RequestWrap();
    RespondWrap respWrap =  new RespondWrap();

    //deseriliaze.
    reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

    for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
        if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
            respWrap.loggedInUser = usrObj;
            respWrap.isContractor = (String.isNotBlank(usrObj.contact.Contractor__c) && usrObj.contact.Contractor__r.RecordType.DeveloperName.equals('Contractor_Account')) || usrObj.Profile.name.containsIgnoreCase('Contractor')  ? true : false;
        }
    }

    
    /* only show manage entity drpdown if an account has more than
    one account relted to it */
    OB_AgentEntityUtils.RespondWrap utilsWrapper = new OB_AgentEntityUtils.RespondWrap();
        utilsWrapper = OB_AgentEntityUtils.getRelatedAccounts(respWrap.loggedInUser.ContactID);
    if(utilsWrapper.relatedAccounts != null && utilsWrapper.relatedAccounts.size() > 1){
        respWrap.showManageEntityButton = true;
    }

       
    return respWrap;
}

@AuraEnabled
    public static OB_ManageAssosiatedEntityController.RespondWrap createNewEntitySr(String requestWrapParam)  {
    
        
        
        OB_ManageAssosiatedEntityController.RespondWrap respWrap =  new OB_ManageAssosiatedEntityController.RespondWrap();
        respWrap = OB_ManageAssosiatedEntityController.createNewEntitySr(requestWrapParam);
    	     
		return respWrap;
    }

// ------------ Wrapper List ----------- //

public class RequestWrap {

@AuraEnabled public String contactID;
}


public class RespondWrap {

@AuraEnabled public list<Account> relatedAccounts = new list<Account>();
@AuraEnabled public boolean isContractor;
@AuraEnabled public User loggedInUser;
@AuraEnabled public string errorMessage;
@AuraEnabled public boolean showManageEntityButton = false;
    @AuraEnabled public HexaBPM__Service_Request__c insertedSr ;  
@AuraEnabled public  OB_AgentEntityUtils.CreateEntityRespondWrap newEntityWrap = new OB_AgentEntityUtils.CreateEntityRespondWrap();

}

}