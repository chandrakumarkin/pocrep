global class DynamicSMSContent implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
        // get the plain text body
            string strEmailBody = email.plainTextBody;
            
            system.debug('strEmailBody===>'+strEmailBody);
            string strEmailSubject = email.subject;
            // get the subject 
            // parse the phone number available in the subject as  "Phone="
            string phoneNumber = '';
            if(strEmailSubject!=null && strEmailSubject!='' && strEmailSubject.IndexOf('Phone=')>-1){
                list<string> lstPhone = strEmailSubject.split('Phone=');
                if(lstPhone!=null && lstPhone.size()>0 && lstPhone.size()==2){
                    phoneNumber = lstPhone[1];
                }
            }
            system.debug('phoneNumber===>'+phoneNumber);
            if(phoneNumber!=null && phoneNumber!='' && strEmailBody!=null && strEmailBody!=''){
              system.debug('Send SMS Invoking===>');
                CLS_SendSMS.sendsms(phoneNumber,strEmailBody);
            }
        return result;
    }
}