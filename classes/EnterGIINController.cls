/*****************************************************************************
* Name             : EnterGIINController
* Created By       : Shabbir Ahmed
* Created Date     : Feb 15, 2014  
* Modifed Date     : Feb 15, 2014
* Purpose          : A custom controller class created for EnterGIIN Visualforce page. To capture the GIIN Number on Account
*V1.1 changed      : changed to support Communite plus 

/*****************************************************************************/

public class EnterGIINController {

    // Properties and variable define here
    public Account account { get; set; }
    public boolean isGIINExsit {get; set;}
        
    // Constructor
    public EnterGIINController(){
        
        account = new Account();
        String userType = UserInfo.getUserType();
        
        if(userType.contains('CustomerSuccess') || Test.isRunningTest()){
            for(User userObj : [Select Contact.AccountId,Contact.Account.Name,Contact.Account.GIIN__c from User where id =: UserInfo.getUserId()]){
                account.Id = userObj.Contact.AccountId;
                account.GIIN__c = userObj.Contact.Account.GIIN__c;
            }
            
            if(string.isnotBlank(account.GIIN__c)){
                isGIINExsit = true;
            }else{
                isGIINExsit = false;
            }
        }
    }
    
    // Custom Save method which will update the account and redirect to the companyinfo tab 
    public PageReference save(){
        
        try{
            update account;
            return Page.CompanyInfo;
            
        }
        catch (Exception e){
            system.debug('Exception occured:- ' + e.getMessage());
            return null;
        }
    }

}