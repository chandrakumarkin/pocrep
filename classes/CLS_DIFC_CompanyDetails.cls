@RestResource(urlMapping = '/CompanyDetails/*')
global class CLS_DIFC_CompanyDetails{

    @Httpget 
    global static  void jsonResponse()
    {
        //Name from URL
          String accountId=  RestContext.request.params.get('accountId');
          if(!test.isRunningTest())
          {
                RestContext.response.addHeader('Content-Type', 'application/json');
          }
      
      try
      {
       

        RestContext.response.responseBody = Blob.valueOf(OpenDataWs.OpenDataWsDataResult(accountId));
      }
      catch(Exception ex)
      {
        
      }
      
      
    }
    
   
    
 }