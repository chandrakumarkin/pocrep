/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=false)
private class TestGsScheduleBatachVisaQuota {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        insert objAccount;
        
        Account objBodyCorp = new Account();
		objBodyCorp.Name = 'Test Body Corp Customer';
		objBodyCorp.E_mail__c = 'test@difcdep.ae';
		objBodyCorp.Country__c = 'India';
		objBodyCorp.BP_No__c = '001238';
		objBodyCorp.Hosting_Company__c = objAccount.Id;
		insert objBodyCorp;
		
		License__c objL = new License__c();
		objL.Account__c = objAccount.Id;
		objL.Last_Renewal_Date__c = system.today().addYears(-1);
		objL.License_Expiry_Date__c = system.today().addYears(1);
		insert objL;
		
		License__c objL1 = new License__c();
		objL1.Account__c = objBodyCorp.Id;
		objL1.Last_Renewal_Date__c = system.today().addYears(-1);
		objL1.License_Expiry_Date__c = system.today().addYears(1);
		insert objL1;
		
		objAccount.Active_License__c = objL.Id;
		update objAccount;
		
		objBodyCorp.Active_License__c = objL1.Id;
		update objBodyCorp;
		
		Lease__c objLease = new Lease__c();
		objLease.Account__c = objAccount.Id;
		objLease.Status__c = 'Active';
		objLease.Lease_Types__c = 'Retail';
		objLease.Square_Feet__c = '1200';
		objLease.No_of_Desks__c = 10;
		insert objLease;
		
		CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,RecordType> mapSRRecordTypes = new map<string,RecordType>();
	    for(RecordType objType : [select Id,Name,DeveloperName from RecordType where sObjectType='Service_Request__c' OR SobjectType='Contact']){
	        mapSRRecordTypes.put(objType.DeveloperName,objType);
	    }
	           
        Contact objContact = new Contact();
        objContact.FirstName = 'Test Contact';
        objContact.LastName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Email = 'test@difc.com';
        objContact.Phone = '1234567890';
        objContact.RecordTypeId = mapSRRecordTypes.get('GS_Contact').Id;
        objContact.Passport_No__c = 'ABC12345';
        objContact.Nationality__c = 'India';
        insert objContact;
        
        list<Relationship__c> lstRels = new list<Relationship__c>();
        Relationship__c objR = new Relationship__c();
        objR.Subject_Account__C = objAccount.Id;
        objR.Object_Contact__c = objContact.Id;
        objR.Active__c = true;
        objR.Start_Date__c = system.today().addDays(-30);
        objR.End_Date__c = system.today().addYears(1);
        insert objR;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
	    SR_Status__c objSRStatus;
	    
	    objSRStatus = new SR_Status__c();
	    objSRStatus.Name = 'Submitted';
	    objSRStatus.Code__c = 'SUBMITTED';
	    lstSRStatus.add(objSRStatus);
	    
	    objSRStatus = new SR_Status__c();
	    objSRStatus.Name = 'Draft';
	    objSRStatus.Code__c = 'DRAFT';
	    lstSRStatus.add(objSRStatus);
        
        insert lstSRStatus;
        list<Service_Request__c> lstSR = new list<Service_Request__c>();
        for(Integer i=0;i<2;i++){
	        Service_Request__c objSR = new Service_Request__c();
			objSR.Customer__c = objAccount.Id;
			if(i==0)
				objSR.RecordTypeId = mapSRRecordTypes.get('DIFC_Sponsorship_Visa_Renewal').Id;
			else if(i==1)
				objSR.RecordTypeId = mapSRRecordTypes.get('Company_Temporary_Work_Permit_Request_Letter').Id;
			objSR.Service_Category__c = 'Renewal Temporary Work Permit';
			objSR.Email__c = 'testsr@difc.com';
			objSR.ApplicantId_hidden__c = objContact.Id;
			objSR.Title__c = 'Mr.';
	        objSR.First_Name__c = 'India';
	        objSR.Last_Name__c = 'Hyderabad';
	        objSR.Middle_Name__c = 'Andhra';
	        objSR.Nationality_list__c = 'India';
	        objSR.Previous_Nationality__c = 'India';
	        objSR.Qualification__c = 'B.A. LAW';
	        objSR.Gender__c = 'Male';
	        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
	        objSR.Place_of_Birth__c = 'Hyderabad';
	        objSR.Country_of_Birth__c = 'India';
	        objSR.Passport_Number__c = 'ABC12345';
	        objSR.Passport_Type__c = 'Normal';
	        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
	        objSR.Passport_Country_of_Issue__c = 'India';
	        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
	        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
	        objSR.Religion__c = 'Hindus';
	        objSR.Marital_Status__c = 'Single';
	        objSR.First_Language__c = 'English';
	        objSR.Email_Address__c = 'applicant@newdifc.test';
	        objSR.Mother_Full_Name__c = 'Parvathi';
	        objSR.Monthly_Basic_Salary__c = 10000;
	        objSR.Monthly_Accommodation__c = 4000;
	        objSR.Other_Monthly_Allowances__c = 2000;
	        objSR.Emirate__c = 'Dubai';
	        objSR.City__c = 'Deira';
	        objSR.Area__c = 'Air Port';
	        objSR.Street_Address__c = 'Dubai';
	        objSR.Building_Name__c = 'The Gate';
	        objSR.PO_BOX__c = '123456';
	        objSR.Residence_Phone_No__c = '+97152123456';
	        objSR.Work_Phone__c = '+97152123456';
	        objSR.Domicile_list__c = 'India';
	        objSR.Phone_No_Outside_UAE__c = '+97152123456';
	        objSR.City_Town__c = 'Hyderabad';
	        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
	        objSR.Statement_of_Undertaking__c = true;
	        lstSR.add(objSR);
        }
		insert lstSR;
		for(Service_Request__c objS : lstSR){
			objS.Internal_SR_Status__c = lstSRStatus[0].Id;
			objS.External_SR_Status__c = lstSRStatus[0].Id;
		}
		update lstSR;
		
		//test.startTest();
		Datetime dt = system.now().addMinutes(1);
		string str = dt.second()+' '+(dt.minute() )+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' '+' ? '+ dt.year();
		system.schedule('GsScheduleBatachVisaQuota', str, new GsScheduleBatachVisaQuota());
		Database.executeBatch(new GsScheduleBatachVisaQuota());
		//test.stopTest();
		//Database.executeBatch(new GsScheduleBatachVisaQuota());
		
    }
}