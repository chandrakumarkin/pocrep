/*
    Author      : Durga Prasad
    Date        : 24-Nov-2019
    Description : Helper calss for Customer Onboarding related Integrations
    --------------------------------------------------------------------------------------
    [24.APRL.2020] Prateek Kadkol - to include CP price items
*/
public without sharing class OB_IntegrationHelper {
    @future(callout=true)
    public static void SendPriceItemsToSAP(list<string> lstSRIds, boolean IsFormSubmit){
        SendPriceItemsToSAPSchedule(lstSRIds,IsFormSubmit);// from trigger of SR
    }
    
    
    //scheduler
    public static void SendPriceItemsToSAPSchedule(list<string> lstSRIds, boolean IsFormSubmit){
        system.debug('$$$$$$$$$$ SendPriceItemsToSAPSchedule enter');
        system.debug('$$$$$$$$$$ SendPriceItemsToSAPSchedule enter'+lstSRIds);
        
        String Month = string.valueOf(system.today().month()).length()==1?'0'+system.today().month():''+system.today().month();
        String Day = string.valueOf(system.today().day()).length()==1?'0'+system.today().day():''+system.today().day();
        String Hours = string.valueOf(system.now().hour()).length()==1?'0'+system.now().hour():''+system.now().hour();
        String Minitues = string.valueOf(system.now().minute()).length()==1?'0'+system.now().minute():''+system.now().minute();
        String Seconds = string.valueOf(system.now().second()).length()==1?'0'+system.now().second():''+system.now().second();
        
        set<string> lstCustomerIds = new set<string>();
        List<String> productCodeList = Label.OB_ProductCODE.split(',');
        list<SAPECCWebService.ZSF_S_RECE_SERV> items = new list<SAPECCWebService.ZSF_S_RECE_SERV>();
        map<string,string> mapSRCustomerIds = new map<string,string>();
        
        SAPECCWebService.ZSF_S_RECE_SERV item;
        boolean gsPricingSkipforRoc=true;
        
        set<string> setRecordTypeNames = new set<string>{'Name_Check','Non_Standard_Document','In_Principle','AOR_NF_R','AOR_Financial','Commercial_Permission','Commercial_Permission_Renewal','Car_Request_Details','Liquidator_confirmation_letter','Suspension_Of_License'};
        
        try{          
            for(HexaBPM__SR_Price_Item__c objSRItem : [select Id,Tax_Rate__c,HexaBPM__Price__c,VAT_Amount__c,VAT_Description__c,HexaBPM__Pricing_Line__r.TAX_CODE__c,Company_Code__c,HexaBPM__ServiceRequest__c,HexaBPM__ServiceRequest__r.HexaBPM__Record_Type_Name__c,HexaBPM__ServiceRequest__r.HexaBPM__Customer__c,HexaBPM__ServiceRequest__r.HexaBPM__Customer__r.BP_No__c,Total_Service_Amount__c,SAP_Web_Service_Indicator__c,
                            HexaBPM__Pricing_Line__c,HexaBPM__Pricing_Line__r.Material_Code__c,
                            HexaBPM__Product__c,HexaBPM__Product__r.CurrencyIsoCode,
                            HexaBPM__Product__r.ProductCode,
                            Total_Price_AED__c 
                            from HexaBPM__SR_Price_Item__c where HexaBPM__ServiceRequest__c IN:lstSRIds 
                            AND HexaBPM__Status__c != 'Invoiced' AND HexaBPM__Status__c != 'Added' and HexaBPM__Status__c != 'Cancelled' 
                            AND HexaBPM__Price__c> 0 AND Transaction_Id__c = null 
                            AND Company_Code__c!='1100' 
                            AND HexaBPM__Product__c != null 
                            AND HexaBPM__ServiceRequest__r.RecordType.DeveloperName IN:setRecordTypeNames
                            AND HexaBPM__Product__r.ProductCode Not IN:  productCodeList
                            Limit : (Limits.getLimitQueryRows()-Limits.getQueryRows())])
            {
                system.debug('$$$$$$$$$$ SendPriceItemsToSAPSchedule item ' +objSRItem);
                item = new SAPECCWebService.ZSF_S_RECE_SERV();
                item.WSTYP = objSRItem.SAP_Web_Service_Indicator__c;
                item.RENUM = string.valueOf(objSRItem.Id).substring(0,15);
                item.SGUID = string.valueOf(objSRItem.Id);
                item.BUKRS = (objSRItem.Company_Code__c != null)? string.valueOf(objSRItem.Company_Code__c):'5000';
                item.KUNNR = objSRItem.HexaBPM__ServiceRequest__r.HexaBPM__Customer__r.BP_No__c;
                item.TDATE = system.today().year()+'-'+Month+'-'+Day;//'2014-09-16';
                item.GSPSN = '00';
                //item.WAERS = objSRItem.HexaBPM__Product__r.CurrencyIsoCode;
                item.WAERS = 'AED';
                //Added VAT
                item.TAX_CODE = objSRItem.HexaBPM__Pricing_Line__r.TAX_CODE__c;
                item.TAX_AMT = string.valueOf(objSRItem.VAT_Amount__c);
                item.TAX_RATE = string.valueOf(objSRItem.Tax_Rate__c);
                //item.WRBTR = (objSRItem.HexaBPM__Price__c != null)?string.valueOf(objSRItem.HexaBPM__Price__c):'0';
                item.WRBTR =  (objSRItem.Total_Price_AED__c != null)?string.valueOf(objSRItem.Total_Price_AED__c):'0';
                
                item.TOTAL =  string.valueOf(objSRItem.VAT_Amount__c+ Decimal.valueOf(item.WRBTR)) ;
                ////Added VAT
                
                item.MATNR = objSRItem.HexaBPM__Pricing_Line__r.Material_Code__c;
                item.POSNR = '000010'; // Item # of SD Document, Always - 000010  
                item.MEINS = 'EA'; // Base unit of measure, always - EA
                item.KWMENG = '1.000'; //// Order Quantity, always 1
                item.RUSER = WebService_Details__c.getAll().get('Credentials').SAP_User_Id__c;
                item.RTIME = Hours+':'+Minitues+':'+Seconds;//'10:10:10';
                items.add(item);
                lstCustomerIds.add(objSRItem.HexaBPM__ServiceRequest__r.HexaBPM__Customer__c);
                mapSRCustomerIds.put(objSRItem.Id,objSRItem.HexaBPM__ServiceRequest__r.HexaBPM__Customer__c);
            }

            system.debug('====lstCustomerIds====='+lstCustomerIds);
            system.debug('====mapSRCustomerIds====='+mapSRCustomerIds);
            SAPECCWebService.ZSF_TT_RECE_SERV objZSF_TT_RECE_SERV = new SAPECCWebService.ZSF_TT_RECE_SERV();
            objZSF_TT_RECE_SERV.item = items;
            
            SAPECCWebService.rece_serv objSFData = new SAPECCWebService.rece_serv();        
            objSFData.timeout_x = 120000;
            objSFData.clientCertName_x = WebService_Details__c.getAll().get('Credentials').Certificate_Name__c;
            objSFData.inputHttpHeaders_x= new Map<String, String>();
            Blob headerValue = Blob.valueOf(WebService_Details__c.getAll().get('Credentials').Username__c+':'+SAPWebServiceDetails.decryptedSAPPassWrd); // Added for V1.8
            String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
            objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);
            SAPECCWebService.Z_SF_RECEIPT_SERVICEResponse_element response;
            system.debug('====items====='+items);
            if(
                items != null 
                    && !items.isEmpty() 
                        
              )
                response = objSFData.Z_SF_RECEIPT_SERVICE(objZSF_TT_RECE_SERV);
             
            system.debug('====response====='+response);
            
            if(response != null){
                System.debug('The response: ' + response);
                SAPWebServiceDetails.ServiceResponseResult objServiceResponseResult = SAPWebServiceDetails.ProcessPriceItemResponse(response.EX_SF_RS,mapSRCustomerIds);
                System.debug('Price Items--hexaPriceItems: ' + objServiceResponseResult.hexaPriceItems);
                list<HexaBPM__SR_Price_Item__c> lstSRPriceItems = new list<HexaBPM__SR_Price_Item__c>();
                if(objServiceResponseResult != null){
                    list<string> lstIds= new list<string>();
                    lstIds.addAll(lstCustomerIds);
                    if(isFormSubmit == true){
                        if(objServiceResponseResult.hexaPriceItems != null)
                            update objServiceResponseResult.hexaPriceItems;
                        if(objServiceResponseResult.Logs != null)
                            insert objServiceResponseResult.Logs;
                    }else{
                        if(objServiceResponseResult.hexaPriceItems != null)
                            update objServiceResponseResult.hexaPriceItems;
                    }
                }
            }
        }catch(Exception ex){
            system.debug('Exception is : '+ex.getMessage());
            Log__c objLog = new Log__c();
            objLog.Description__c = 'Exception in OB_IntegrationHelper Line-167 :'+ex.getMessage();
            objLog.Type__c = 'Webservice for Service / PSA Deposit';
            insert objLog;
        }
    }
}