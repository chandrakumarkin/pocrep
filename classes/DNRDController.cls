public class DNRDController 
{
    //public list<Step__c> stepRec{get;set;}
    //public Service_Request__c srRec{get;set;}
    public ID recordID;
    public DNRDController(ApexPages.StandardController controller){}
    
    public static void dnrd()
    {
        list<Step__c> stepRec;
        ID recordID;
    if(ApexPages.currentPage().getParameters().get('id') != null)
    {
         recordID = ApexPages.currentPage().getParameters().get('id');
         if(recordID != null)
         {
             stepRec = [Select Id, Name,Sr__c,SR__r.id,DNRD_Receipt_No__c,DNRD_New_Field__c from Step__c where Step_Name__c = 'Front Desk Review' and SR_Record_Type__c in ('Index_Card_Other_Services','New_Index_Card') and id =: recordID];
             if(stepRec.size()>0)
             {
                 if(stepRec[0].DNRD_Receipt_No__c != NULL){List <Step__c> sRc = [Select Id, Name,Sr__c,SR__r.id,DNRD_Receipt_No__c,Establishment_Card_No__c,DNRD_New_Field__c from Step__c where Step_Name__c = 'Ready for Collection' and SR_Record_Type__c in ('Index_Card_Other_Services','New_Index_Card') and SR__c=:stepRec[0].SR__c and Establishment_Card_No__c= NULL];if(sRc.size()>0){sRc[0].DNRD_New_Field__c = stepRec[0].DNRD_Receipt_No__c;update sRc;}
                }
             }
         }
   }
 }
  
}