/*  -------------------------------------------------------------------------
    **Version History**
     * @Author: Rajil Ravindran 
     * @version: 1.0
     * @Date: 02-May-2019
     * @Description: 
        1. Added the ownership change validation. The new owner should belong to the same queue or else throw an error message.
*/
/*  -------------------------------------------------------------------------
    **Version History**
     * @Author: Salma Rasheed 
     * @version: 1.1
     * @Date: 21-Nov-2019
     * @Description: 
        1. As per Mark's request, updated the existing validation on case closure to make sure atlease one attachment is added in order to close. (Existing rule on activities still applies)
        2. Additionally, bypassing this validation in case a lead is created from the case.
        3. As per Mark's request, removed the condition for contact centre profile & added a new condition to bypass the validation when the origin is Chat
        4. Remove 'Type' field whereever referred.
*/
public without sharing class CaseTriggerHelper{

    public static void invokeAssignmentRulesInsert(List<Case> caseList){
        Database.DMLOptions caseAssignmentRuleOptions = new Database.DMLOptions();
        caseAssignmentRuleOptions.assignmentRuleHeader.useDefaultRule = true;
        
        Set<Id> caseIds = new Set<Id>();
        for(Case objCase : caseList){
            if(objCase.isClosed == false){
                if(objCase.Origin != 'Email'){
                    caseIds.add(objCase.Id);
                } else{
                    if(objCase.GN_Type_of_Feedback__c != null){
                        caseIds.add(objCase.Id);
                    }
                }
            }
        }
        
        List<Case> caseToUpdate = new List<Case>();
        for(Case objCase : [SELECT Id FROM Case WHERE Id IN: caseIds]){
            objCase.setOptions(caseAssignmentRuleOptions);
            caseToUpdate.add(objCase);
        }
        System.debug('$$CASE: '+caseToUpdate);
        if(!caseToUpdate.isEmpty()){
            update caseToUpdate;
        }
    }
    
    public static void invokeAssignmentRulesInsert(List<Case> caseList, Map<Id,Case> caseOldMap){
        Database.DMLOptions caseAssignmentRuleOptions = new Database.DMLOptions();
        caseAssignmentRuleOptions.assignmentRuleHeader.useDefaultRule = true;
        caseAssignmentRuleOptions.EmailHeader.TriggerUserEmail = true;
        
        Set<Id> caseIds = new Set<Id>();
        for(Case objCase : caseList){
            if(objCase.GN_Department__c != caseOldMap.get(objCase.Id).GN_Department__c){ // PwC: Case management ehnancements. Removed:- (objCase.Type != caseOldMap.get(objCase.Id).Type && caseOldMap.get(objCase.Id).Type != null) ||
                if(objCase.isClosed == false){
                    caseIds.add(objCase.Id);
                }
            }
        }
        
        List<Case> caseToUpdate = new List<Case>();
        for(Case objCase : [SELECT Id, GN_Are_you_sure_you_want_accept__c FROM Case WHERE Id IN: caseIds]){
            objCase.setOptions(caseAssignmentRuleOptions);
            objCase.GN_Are_you_sure_you_want_accept__c = 'No';
            caseToUpdate.add(objCase);
        }
        if(!caseToUpdate.isEmpty()){
            update caseToUpdate;
        }
    }
    
    public static void closingcase(Map<Id,Case> caseOldMap, List<case> caseList){
    system.debug('===========closingcase========');
        /*Version 1.1 - Removing the condition for contact centre profile*/
        /*Boolean excludeValidation = false;
        List<Profile> contactCentreProf = [SELECT Id, Name FROM Profile WHERE Id = :Userinfo.getProfileId()];
        if(!contactCentreProf.isEmpty()){
            if(contactCentreProf[0].Name == 'Contact Centre'){
                excludeValidation = true;
            }
        }*/
        
        Set<Id> caseIds = new Set<Id>();
        for(Case c : caseList){         
           // if(excludeValidation == false){// Version 1.1
                if((c.Status != caseOldMap.get(c.Id).Status) && c.Status == 'Closed - Resolved'){
                    caseIds.add(c.Id);
                }
           // }
        }
        Map<Id, Integer> caseActMap = new Map<Id, Integer>();
        Set<Id> LEX_CaseIds = new Set<Id>();
        /* Version 1.1 - Included a sub query for attachments as well */
        for(Case c : [SELECT Id, Status, (SELECT id,Description from ActivityHistories),(SELECT Id, Name FROM Attachments)
                        FROM Case WHERE GN_Type_of_Feedback__c = 'Enquiry' 
                        AND Id IN :caseIds]){
            LEX_CaseIds.add(c.id);            
            system.debug('##ACT: '+c.ActivityHistories.size());
            /* Version 1.1 - Adding the total no of activities and attachments */
            Integer Count= c.ActivityHistories.size() + c.Attachments.size();
            system.debug('Count==>'+c.ActivityHistories.size() +'---'+c.Attachments.size());
            caseActMap.put(c.Id, Count);
        }
        system.debug('##LEX_CaseIds: '+LEX_CaseIds);
        Integer LEX_AttachmentCount = 0;
        if(!LEX_CaseIds.isEmpty()){
            /* Version 1.1 - Handling attachments added from Lightning mode. Attachments are stored in the ContentDocument*/
            Set<Id> ContentDocumentLink_Ids = new Set<Id>();
            for(ContentDocumentLink ObjContentDocLink : [SELECT ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId IN: LEX_CaseIds]){
                ContentDocumentLink_Ids.add(ObjContentDocLink.ContentDocumentId );
                system.debug('##ContentDocumentLink_Ids: '+ContentDocumentLink_Ids);
            }
            LEX_AttachmentCount = [Select count() from ContentDocument Where ID In :ContentDocumentLink_Ids];
            system.debug('##LEX_AttachmentCount: '+LEX_AttachmentCount);
        }
        for(Case c : caseList){
            if((c.Status != caseOldMap.get(c.Id).Status) && c.Status == 'Closed - Resolved'){
            system.debug('##Flag: '+c.GN_Are_you_sure_you_want_to_create_lead__c);
              if(caseActMap.get(c.Id) == 0 && LEX_AttachmentCount ==0 && c.GN_Related_Lead__c == null && c.Origin!='Chat'){ /* Version 1.1 - Bypassing this validation in case a lead is created from the case. */
                //if(caseActMap.get(c.Id) == 0 && LEX_AttachmentCount ==0 && c.GN_Are_you_sure_you_want_to_create_lead__c != 'Yes'){ /* Version 1.1 - Bypassing this validation in case a lead is created from the case. */
                    c.addError('Cannot update status to Closed - Resolved without atleast one Activity or Attachment.');/* Version 1.1 - Updated the error message to include attachment validation */
                }
            }
        }
    }
    
    public static void reAssignToCurrentUser(Map<Id,Case> caseOldMap, List<case> caseList){
        for(Case objCase : caseList){
            if(objCase.GN_Are_you_sure_you_want_accept__c == 'Yes' && objCase.GN_Are_you_sure_you_want_accept__c != caseOldMap.get(objCase.Id).GN_Are_you_sure_you_want_accept__c){
                objCase.Status = 'In Progress';
                objCase.OwnerId = Userinfo.getuserId();
            }
        }
    }
    //Version 1.0 : Added the ownership change validation. The new owner should belong to the same queue or else throw an error message.
    public static void ownershipValidation(list<Case> TriggerNew,map<Id,Case> TriggerOldMap)
    {
        list<string> listQueueNames = new list<string>();
        for(Case cs:TriggerNew){
            System.debug('ApexCodeUtility.CaseOwnerChange:'+ApexCodeUtility.CaseOwnerChange);
            System.debug('cs.OwnerId:'+cs.OwnerId);
            //Checks the Ownership change validation - The new owner should belong to the same queue.
            if(ApexCodeUtility.CaseOwnerChange == true && cs.OwnerId!=TriggerOldMap.get(cs.Id).OwnerId 
                && string.valueOf(cs.OwnerId).startsWith('005') ){
                listQueueNames.add(TriggerOldMap.get(cs.Id).GN_Sys_Case_Queue__c);
            }
            //Checking the change in queue - validation //
            /*
            System.debug('New NAme:'+cs.GN_Sys_Case_Queue__c);
            System.debug('Old Name: '+TriggerOldMap.get(cs.Id).GN_Sys_Case_Queue__c);
            if(ApexCodeUtility.CaseOwnerChange == true && cs.GN_Sys_Case_Queue__c!=TriggerOldMap.get(cs.Id).GN_Sys_Case_Queue__c 
                && string.valueOf(cs.OwnerId).startsWith('00G')  
                    && String.IsNotEmpty(TriggerOldMap.get(cs.Id).GN_Sys_Case_Queue__c )){
                        cs.addError('Owner should belong to '+TriggerOldMap.get(cs.Id).GN_Sys_Case_Queue__c);
            }
            */
        }
        Trigger_Settings__c TS = Trigger_Settings__c.getValues('CaseOwnershipValidation');
        if(TS != null && TS.is_Active__c == TRUE){
            map<string,list<SetupObjectDataHelper.GroupDetails>> MapGroupMembers = new map<string,list<SetupObjectDataHelper.GroupDetails>>();
            if(listQueueNames.size()>0){
                MapGroupMembers = SetupObjectDataHelper.getGroupMembers(listQueueNames);
            
                if(ApexCodeUtility.CaseOwnerChange == true){
                    ApexCodeUtility.CaseOwnerChange = false;
                    for(Case cs:TriggerNew){
                        string OldQueueName = TriggerOldMap.get(cs.Id).GN_Sys_Case_Queue__c;
                        if(cs.OwnerId!=TriggerOldMap.get(cs.Id).OwnerId && string.valueOf(cs.OwnerId).startsWith('005') && OldQueueName!=null && MapGroupMembers.get(OldQueueName)!=null){
                            boolean IsGroupMember = false;
                            for(SetupObjectDataHelper.GroupDetails QMember:MapGroupMembers.get(OldQueueName)){
                                if(QMember.GroupOrUserId==cs.OwnerId){
                                    IsGroupMember = true;
                                    break;
                                }
                            }
                            if(!IsGroupMember)
                                cs.addError('Owner should belong to '+OldQueueName);
                        }
                    }
                }
            }
        }
    }
    public static void createLead(Map<Id,Case> caseOldMap, List<case> caseList){
    
        Map<String, String> mapcaseLeadField = new Map<String, String>();
        //iterate on the custom setting for the field mapping
        for(Mapping_Case_To_Lead__c caseLeadField : Mapping_Case_To_Lead__c.getAll().values()){
            if(caseLeadField.Active__c == TRUE){
                mapcaseLeadField.put(caseLeadField.Lead_Field_API_Name__c,caseLeadField.Case_Field_API_Name__c);
            }
        }
        
        Map<String, String> mapcOriginlSource = new Map<String, String>();
        for(Case_Origin_to_Lead_Source_Map__c originSource : Case_Origin_to_Lead_Source_Map__c.getAll().values()){
            mapcOriginlSource.put(originSource.Case_Origin__c,originSource.Lead_Source__c);
        }
        
        Map<String,Id> caseUnqKeyMap = new Map<String,Id>();
        List<Lead> newLeadList = new List<Lead>();
        for(Case objCase : caseList){
            system.debug('@@LEADCreate: '+objCase.GN_Are_you_sure_you_want_to_create_lead__c);
            if(caseOldMap.containskey(objCase.Id)){
                system.debug('@@LEADISCHANGED: '+(objCase.GN_Are_you_sure_you_want_to_create_lead__c != caseOldMap.get(objCase.Id).GN_Are_you_sure_you_want_to_create_lead__c));
                if(objCase.GN_Related_Lead__c == null && objCase.GN_Are_you_sure_you_want_to_create_lead__c == 'Yes' && objCase.GN_Are_you_sure_you_want_to_create_lead__c != caseOldMap.get(objCase.Id).GN_Are_you_sure_you_want_to_create_lead__c){
                    system.debug('@@LEADCompanyNAME: '+objCase.Company_Name__c);
                    if(objCase.Company_Name__c == null || objCase.Company_Name__c == '' || objCase.GN_Last_Name__c == null || objCase.GN_Last_Name__c == ''){
                        objCase.addError('Please provide the Full Name and Company Name');
                    } else{
                        Lead objLead = new Lead();
                        if(!mapcaseLeadField.isEmpty()){
                            for(String key : mapcaseLeadField.keyset()){
                                objLead.put(key,objCase.get(mapcaseLeadField.get(key)));
                            }
                            objLead.GN_Origin_Case_Id__c = objCase.Id;
                            newLeadList.add(objLead);
                        }
                        system.debug('@@leadOrigin: '+objCase.Origin);
                        if(objCase.Origin != '' && objCase.Origin != null){
                            system.debug('@@leadOriginSOURCE: ');
                            if(mapcOriginlSource.containskey(objCase.Origin)){
                                objLead.LeadSource = mapcOriginlSource.get(objCase.Origin);
                            } else{
                                objLead.LeadSource = 'Other';
                            }
                        }
                    }
                }
            }
        }
        
        system.debug('@@LEADLIST: '+newLeadList);
        if(!newLeadList.isEmpty()){
        
            try{
                insert newLeadList;
                
                Map<Id,Id> caseleadIdMap = new Map<Id,Id>();
                for(Lead objlead : newLeadList){
                    caseleadIdMap.put(objLead.GN_Origin_Case_Id__c,objLead.Id);
                }
                
                if(!caseleadIdMap.isEmpty()){
                    for(Case objCase : caseList){
                        if(caseleadIdMap.containskey(objCase.Id)){
                            objCase.GN_Related_Lead__c = caseleadIdMap.get(objCase.Id);
                            objCase.GN_Are_you_sure_you_want_to_create_lead__c = 'No';
                        }
                    }
                }
                
            } catch(DMLException e){
                string DMLError = e.getdmlMessage(0)+'';
                if(DMLError==null){
                    DMLError = e.getMessage()+'';
                }
                for(Case objCase : caseList){
                    objCase.addError(DMLError);
                }
            }
        }   
    }
}