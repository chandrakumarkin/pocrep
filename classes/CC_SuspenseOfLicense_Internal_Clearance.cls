/*
    Author      : 
    Date        : 12-July-2021
    Description : Custom code to 
    ---------------------------------------------------------------------------------------
*/
global with sharing class CC_SuspenseOfLicense_Internal_Clearance implements HexaBPM.iCustomCodeExecutable {
    
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR, HexaBPM__Step__c stp) {
        string strResult = 'Success';
        if(stp!=null && stp.HexaBPM__SR__c!=null){
            List<Sobject> sobjectList= UtilitySoqlHelper.fetchSobjectRecordBasedOnId(Id.valueOf(stp.HexaBPM__SR__c),'HexaBPM__Customer__r.Index_Card_Status__c');
            List<HexaBPM__Service_Request__c> srList = (List<HexaBPM__Service_Request__c>) sobjectList;
            HexaBPM__Service_Request__c srRecord = (srList.size() >0) ? srList[0]:new HexaBPM__Service_Request__c();
          
			List<Lease__c> activeLeases =[select Id,Status__c,Name from Lease__c where Status__c ='Active' and Account__c =: srRecord.HexaBPM__Customer__c];
            system.debug('^^^^^^^activeLeases^^^^^^^^^^^^^^^^^^^^'+activeLeases);

            if(activeLeases.size() > 0){ 
				strResult ='';
				for(Lease__c leas : activeLeases){
					strResult += (strResult != '') ? ','+leas.Name : leas.Name;
				}
				strResult = 'You are not allowed to close the Step.The following leases ('+strResult+') are active.';
			}            
        }
        system.debug('^^^^^^^^^check lease^^^^^^^^^^^^^^^^^^^^^^'+strResult);
        return strResult;
    }      
}