/******************************************************************************************
 *  Author         :   Jayanta
 *  Company        :   HexaBPM
 *  Date           :   9-April-2017
 *  Description    :   Apex Controller for Uploading Documents in LEX
 * 
 * v2 Prateek MAY 5 2020 - validation of min 1 authorized signatory
 ********************************************************************************************/

public without sharing class OB_LEX_UploadDocumentsController {
	/*** Method to fetch all the SR Docs related to the Service Request ***/
	@AuraEnabled
	public static RespondWrap getSRDocs(ID SRId, string pageID) {
		RespondWrap respWrap = new RespondWrap();
		list<HexaBPM__SR_Doc__c> SRDocs = new list<HexaBPM__SR_Doc__c>();

		set<string> docCodeToInclude = new set<string>();
	
		for(HexaBPM__Service_Request__c srObj : [select id,HexaBPM__Record_Type_Name__c,Authorised_person_details__c,
		Foreign_entity_details__c,Foreign_entity_business_activity_details__c,Registered_address_details__c,Hosting_Affiliated_DIFC_Entity__c
		 from HexaBPM__Service_Request__c where id =: SRId]){
			respWrap.srObject = srObj;
		}

		if(respWrap.srObject.Id != null && respWrap.srObject.HexaBPM__Record_Type_Name__c == 'Commercial_Permission_Renewal'){
			for(OB_CP_Renewal_Doc_Settings__mdt docSettings :[Select ID, Field_API_Name__c,MasterLabel,Sr_Doc_LIst__c from  OB_CP_Renewal_Doc_Settings__mdt ]){ 
				if(docSettings.Field_API_Name__c != 'default documents'){
					if(respWrap.srObject.get(docSettings.Field_API_Name__c) == 'Yes'){
						if(docSettings.Sr_Doc_LIst__c != null){
							for(string docMadsterCode : docSettings.Sr_Doc_LIst__c.split(',')){
								docCodeToInclude.add(docMadsterCode);
							}
						}
						
					}
				}else{
					if(docSettings.Sr_Doc_LIst__c != null){
						for(string docMadsterCode : docSettings.Sr_Doc_LIst__c.split(',')){
							docCodeToInclude.add(docMadsterCode);
						}
					}
				}
				
			}

		}

		respWrap.debugger = docCodeToInclude;
		for(HexaBPM__SR_Doc__c docObj :[select id, Name, createddate, lastmodifieddate, 
									    HexaBPM__Document_Description_External__c, 
									    HexaBPM__Is_Not_Required__c, 
									    HexaBPM__Document_Master__r.Name, 
									    HexaBPM__Document_Master__r.Document_Description__c, 
									    HexaBPM__Service_Request__c, 
									    HexaBPM__Service_Request__r.HexaBPM__Internal_Status_Name__c, 
									    HexaBPM__Service_Request__r.HexaBPM__External_Status_Name__c, 
									    HexaBPM__Doc_ID__c, 
									    HexaBPM__Sys_IsGenerated_Doc__c, 
									    Download_URL__c, 
									    HexaBPM__Customer_Comments__c, 
									    HexaBPM__Comments__c, 
									    HexaBPM__Document_Type__c, 
									    HexaBPM__Rejection_Reason__c, 
									    HexaBPM__Status__c, 
									    HexaBPM__Letter_Template_Id__c,
											Amendment_Name__c,
											HexaBPM_Amendment__r.cessation_date__c,
											HexaBPM__Document_Master__r.HexaBPM__Code__c,
									    Copied_from_InPrinciple__c,
									    In_Principle_Document__c,In_Principle_Document__r.HexaBPM__Doc_ID__c
									    from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c = :SRId order by Name]) {

												if(respWrap.srObject.Id != null && respWrap.srObject.HexaBPM__Record_Type_Name__c == 'Commercial_Permission_Renewal'){
													if(docCodeToInclude.contains(docObj.HexaBPM__Document_Master__r.HexaBPM__Code__c) && docObj.HexaBPM_Amendment__r.cessation_date__c == null){
														SRDocs.add(docObj);
													}
												}else{
													SRDocs.add(docObj);
												}
			
		}
		if(pageID!=null){
			for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
													    AND HexaBPM__Section__r.HexaBPM__Page__c=:pageID limit 1]) {
				respWrap.ButtonSection = sectionObj;
			}
		}
		respWrap.SRDocs = SRDocs;
		return respWrap;
	}
	/*** Method to fetch all the SR Docs to be generated related to the Service Request ***/
	@AuraEnabled
	public static list<HexaBPM__SR_Doc__c> getGeneratedSRDocs(ID SRId) {
		list<HexaBPM__SR_Doc__c> SRDocs = [select id, Name, createddate, lastmodifieddate, 
										   HexaBPM__Document_Description_External__c, 
										   HexaBPM__Is_Not_Required__c, 
										   HexaBPM__Document_Master__r.Name, 
										   HexaBPM__Document_Master__r.Document_Description__c, 
										   HexaBPM__Service_Request__c, 
										   HexaBPM__Service_Request__r.HexaBPM__Internal_Status_Name__c, 
										   HexaBPM__Service_Request__r.HexaBPM__External_Status_Name__c, 
										   HexaBPM__Doc_ID__c, 
										   Download_URL__c, 
										   HexaBPM__Sys_IsGenerated_Doc__c, 
										   HexaBPM__Customer_Comments__c, 
										   HexaBPM__Comments__c, 
										   HexaBPM__Document_Type__c, 
										   HexaBPM__Rejection_Reason__c, 
										   HexaBPM__Status__c, 
										   HexaBPM__Letter_Template_Id__c,
										   Amendment_Name__c,
										   Copied_from_InPrinciple__c
										   from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c = :SRId order by CreatedDate asc];
		system.debug('SRDocs==>' + SRDocs);
		return SRDocs;
	}
	/*** Method to fetch the Attachment ID to show in the document preview screen ***/
	@AuraEnabled
	public static string getAttachmentURL(ID SRID, string AttachmentID) {
		string AttachmentURL = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file=';
		if(AttachmentID == '') {
			/* Getting the first attachment Id when the page is loaded */
			list<HexaBPM__SR_Doc__c> SRDocList = [Select id, Name, HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c = :SRID order by createdDate DESC];
			if(!SRDocList.isEmpty())
				AttachmentURL+= '&' + SRDocList [0].HexaBPM__Doc_ID__c;
			if(SRDocList != null && SRDocList.size() == 1)
				//In case there is just one SR Doc attachment, disabling the Next button
				AttachmentURL+= '&DisableNext';
		} else 
			AttachmentURL+= '&' + AttachmentID;
		return AttachmentURL;
	}

	/*** Method to get the next Attachment ID when Next button is clicked in the document preview section***/
	@AuraEnabled
	public static string getNextAttachment(string SRID, string AttachmentID) {
		Integer index = 0;
		string AttachmentURL = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file=';
		list<HexaBPM__SR_Doc__c> SRDocList = [Select id, Name, HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c = :SRID order by createdDate DESC];
		for(HexaBPM__SR_Doc__c ObjSRDoc :SRDocList) {
			if(ObjSRDoc.HexaBPM__Doc_ID__c == AttachmentID)
				break;
			index++;
		}
		if(!SRDocList.isEmpty()) {
			if(SRDocList.size() == index + 2) {
				AttachmentURL += '&' + SRDocList [index + 1].HexaBPM__Doc_ID__c + '&' + SRDocList [index + 1].Name + '&LastAttachment';
			} else if(SRDocList.size() > index)
				AttachmentURL+= '&' + SRDocList [index + 1].HexaBPM__Doc_ID__c + '&' + SRDocList [index + 1].Name;
		}
		return AttachmentURL;
	}

	/*** Method to get the previous Attachment ID when Previous button is clicked in the document preview section***/
	@AuraEnabled
	public static string getPreviousAttachment(string SRID, string AttachmentID) {
		Integer index = 0;
		string AttachmentURL = URL.getSalesforceBaseUrl().toExternalForm() + '/servlet/servlet.FileDownload?file=';
		list<HexaBPM__SR_Doc__c> SRDocList = [Select id, Name, HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c = :SRID order by createdDate ASC];
		for(HexaBPM__SR_Doc__c ObjSRDoc :SRDocList) {
			system.debug('ObjSRDoc.HexaBPM__Doc_ID__c=>' + ObjSRDoc.HexaBPM__Doc_ID__c + '-' + AttachmentID);
			if(ObjSRDoc.HexaBPM__Doc_ID__c == AttachmentID)
				break;
			index++;
		}
		system.debug('index==>' + index);
		if(!SRDocList.isEmpty()) {
			if(SRDocList.size() == index + 2) {
				AttachmentURL += '&' + SRDocList [index + 1].HexaBPM__Doc_ID__c + '&' + SRDocList [index + 1].Name + '&FirstAttachment';
			} else 
				AttachmentURL+= '&' + SRDocList [index - 1].HexaBPM__Doc_ID__c + '&' + SRDocList [index - 1].Name;
		}
		return AttachmentURL;
	}

	/*** Method to save the Uploaded file under the selected SR Doc ***/
	@AuraEnabled
	public static Id saveTheFile(ID SRId, Id parentId, string fileName, string base64Data, string contentType, string CustomerComments) {
		try{
			base64Data = EncodingUtil.urlDecode(base64Data, 'UTF-8');

			//Insert ContentVersion
			ContentVersion cVersion = new ContentVersion();
			cVersion.ContentLocation = 'S'; //S-Document is in Salesforce. E-Document is outside of Salesforce. L-Document is on a Social Netork.
			cVersion.PathOnClient = fileName; //attach.Name;//File name with extention
			cVersion.Origin = 'H'; //C-Content Origin. H-Chatter Origin.
			//cVersion.OwnerId = parentId; // attach.OwnerId;//Owner of the file
			cVersion.Title = fileName; //attach.Name;//Name of the file
			cVersion.VersionData = EncodingUtil.base64Decode(base64Data); //File content
			Insert cVersion;

			//After saved the Content Verison, get the ContentDocumentId
			Id conDocument = [SELECT ContentDocumentId FROM ContentVersion WHERE Id=:cVersion.Id].ContentDocumentId;

			//Insert ContentDocumentLink
			ContentDocumentLink cDocLink = new ContentDocumentLink();
			cDocLink.ContentDocumentId = conDocument; //Add ContentDocumentId
			cDocLink.LinkedEntityId = parentId; //attach.ParentId;//Add attachment parentId
			cDocLink.ShareType = 'I'; //V - Viewer permission. C - Collaborator permission. I - Inferred permission.
			cDocLink.Visibility = 'AllUsers'; //AllUsers, InternalUsers, SharedUsers
			Insert cDocLink;


			//system.debug('ObjAttachment==>'+ObjAttachment);
			HexaBPM__SR_Doc__c ObjSRDoc = new HexaBPM__SR_Doc__c(Id = parentId);
			ObjSRDoc.HexaBPM__Customer_Comments__c = CustomerComments;
			ObjSRDoc.HexaBPM__Status__c = 'Uploaded';
			ObjSRDoc.HexaBPM__Doc_ID__c = conDocument;
			update ObjSRDoc;
			system.debug('ObjSRDoc=>' + ObjSRDoc);
			return cDocLink.Id;
		} catch(Exception e) {
			system.debug('Exception IN saveTheFile()====>' + e.getMessage());
		}
		return null;
	}

	/*** Method to fetch the Status picklist values ***/
	@AuraEnabled
	public static list<string> getSRDocStatus() {
		list<string> options = new list<string>();
		Schema.DescribeFieldResult fieldResult = HexaBPM__SR_Doc__c.HexaBPM__Status__c.getDescribe();
		list<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for (Schema.PicklistEntry f :ple) {
			options.add(f.getLabel());
		}
		system.debug('options==>' + options);
		return options;
	}

	/*** Method to Update the SR Docs when Save is clicked on the list of SR Docs. Also auto closed the step in case of Documents re-uploaded.***/
	@AuraEnabled
	public static void UpdateSRDocs(string SRDocList, string SRId) {
		HexaBPM__SR_Doc__c[] lstUploadableDocs = (HexaBPM__SR_Doc__c []) JSON.deserialize(SRDocList, list<HexaBPM__SR_Doc__c>.class);
		if(lstUploadableDocs != null && lstUploadableDocs.size() > 0) {
			Savepoint svpoint = Database.setSavepoint();
			upsert lstUploadableDocs;
			//Auto Close Document Re-upload step when the status is set to document reuploaded
			AutoCloseReuploadDocStep(SRId);
		}

	}

	/*** Generic Method to auto close the Reupload step once the status is set to Re-upload ***/
	public static void AutoCloseReuploadDocStep(ID SRId) {
		try{
			list<HexaBPM__Step__c> Step = [select id, HexaBPM__SR_Step__c, HexaBPM__Status__c from HexaBPM__Step__c where HexaBPM__Status__r.HexaBPM__Type__c != 'End' AND
										   HexaBPM__Status__r.HexaBPM__Reupload_Document__c = true AND(HexaBPM__SR__c = :SRId OR HexaBPM__SR__r.HexaBPM__Parent_SR__c = :SRId)];
			if(Step != null && Step.size() > 0) {
				//Check is there are any documents are not yet uploaded or approved
				Integer SRDocCount = [select count() from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c = :SRId AND(HexaBPM__Status__c != 'Uploaded' AND HexaBPM__Status__c != 'Approved')];
				//If there is no such document with Re-upload or pending status, then close the step
				if(SRDocCount != null && SRDocCount == 0) {
					Map<string, string> MapStatusValues = new Map<string, string>();
					for(HexaBPM__Step_Transition__c ObjTransition :[Select Id, HexaBPM__Transition__c, HexaBPM__Transition__r.HexaBPM__To__c, HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Code__c from HexaBPM__Step_Transition__c
																    where HexaBPM__SR_Step__c = :Step [0].HexaBPM__SR_Step__c]) {
						MapStatusValues.put(ObjTransition.HexaBPM__Transition__r.HexaBPM__To__r.HexaBPM__Code__c, ObjTransition.HexaBPM__Transition__r.HexaBPM__To__c);
					}
					if(MapStatusValues.containsKey('DOCUMENT_RE_UPLOADED')) {
						Step[0].HexaBPM__Status__c = MapStatusValues.get('DOCUMENT_RE_UPLOADED');
					}
					update Step;
				}
			}
		} catch(Exception e) {
			system.debug('Exception In AutoCloseReuploadDocStep()===>' + e.getMessage());
		}
	}

	/*** Method to Enable/Disable Next & Previous Buttons for the document preview section***/
	@AuraEnabled
	public static string EnableDisableButtons(string SRID, string AttachmentID) {
		Integer index = 0;
		list<HexaBPM__SR_Doc__c> SRDocList = [Select id, Name, HexaBPM__Doc_ID__c from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c = :SRID order by createdDate ASC];
		Integer SRDocListSize = 0;
		if(SRDocList != null && SRDocList.size() > 0)
			SRDocListSize = SRDocList.size();
		for(HexaBPM__SR_Doc__c ObjSRDoc :SRDocList) {
			if(ObjSRDoc.HexaBPM__Doc_ID__c == AttachmentID)
				break;
			index++;
		}
		if(SRDocListSize - index == SRDocListSize) {
			return 'disablePrevious-enableNext';
		}else if(SRDocListSize - index == 1) {
			return 'disableNext-enablePrevious';
		}else 
			return 'enablePrevious-enableNext';
	}
	/*** Method to check if the logged in user is a community user or not***/
	@AuraEnabled
	public static Boolean CheckIfCommunityUser() {
		Boolean flag = false;
		for(User ObjUser :[select id, ContactId from User where Id = :userInfo.getUserId()]) {
			if(ObjUser.ContactId != null) 
				flag = true;
			else 
				flag = false;
		}
		return flag;
	}
	// dynamic button section
	@AuraEnabled
	public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId) {
		ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
		if(SRID!=null){
			HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
			PageFlowControllerHelper.objSR = objRequest;
			PageFlowControllerHelper objPB = new PageFlowControllerHelper();
			boolean bReqDocsUploaded = true;
			boolean hasAuthorizedSig = false;

			//v2
			for(HexaBPM_Amendment__c amendObj :[Select ID
			 from HexaBPM_Amendment__c WHERE Role__c INCLUDES ('Authorised Signatory') AND ServiceRequest__c = :SRID LIMIT 1]){
				hasAuthorizedSig = true; 
			}
            
            //v3
            If(objRequest.Recordtype.DeveloperName =='Superuser_Authorization'){
                for(HexaBPM__SR_Doc__c objSRDoc:[Select Id from HexaBPM__SR_Doc__c 
                                                 where HexaBPM__Service_Request__c=:SRID 
                                                 AND HexaBPM__Is_Not_Required__c=false 
                                                 AND HexaBPM__Doc_ID__c=null 
                                                 limit 1])
                {
                    bReqDocsUploaded = false;
                }
            }
            
            System.debug('ZZZ BEFORE bReqDocsUploaded ->'+bReqDocsUploaded);
            
		if(hasAuthorizedSig || objRequest.Recordtype.DeveloperName !='Superuser_Authorization'){
			for(HexaBPM__SR_Doc__c objSRDoc:[Select Id from HexaBPM__SR_Doc__c where HexaBPM__Service_Request__c=:SRID 
                                                        and HexaBPM__Sys_IsGenerated_Doc__c=false 
                                                        and HexaBPM__Is_Not_Required__c=false 
                                                        and (HexaBPM__Doc_ID__c=null 
                                                             and In_Principle_Document__c != null) 
                                              limit 1])
            {
				bReqDocsUploaded = false;
			}
			if(bReqDocsUploaded)
            {
				objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
							
				upsert objRequest;
				OB_RiskMatrixHelper.CalculateRisk(SRID);
				if(objRequest.Recordtype.DeveloperName =='Superuser_Authorization'){
					string strSubmitResult = SubmitSR(SRID);
					respWrap.debugger =strSubmitResult;
                if(strSubmitResult != 'Success') {
                    
                    respWrap.errormessage = strSubmitResult;
								}
				}
				PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
				system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
				respWrap.pageActionName = responseNextPage.pg;
				respWrap.communityName = responseNextPage.communityName;
				respWrap.CommunityPageName = responseNextPage.CommunityPageName;
				respWrap.sitePageName = responseNextPage.sitePageName;
				respWrap.strBaseUrl = responseNextPage.strBaseUrl;
				respWrap.srId = objRequest.Id;
				respWrap.flowId = responseNextPage.flowId;
				respWrap.pageId = responseNextPage.pageId;
				respWrap.isPublicSite = responseNextPage.isPublicSite;
				system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
			}else{
				respWrap.errorMessage = 'Please upload required documents to proceed.';
			}
		}else{
			respWrap.errorMessage = 'Please add at least one authorised Signatory';
		}
		}else{
			respWrap.errorMessage = 'No Application Id found.';
		}
			
		return respWrap;
	}

	public static string SubmitSR(string SRID) {
		string strResult = 'Success';
		HexaBPM__Service_Request__c objRequest = new HexaBPM__Service_Request__c(Id = SRID);
		boolean IsAllowedToSubmit = false;
		for(HexaBPM__Service_Request__c SR :[Select Id from HexaBPM__Service_Request__c where Id = :SRID and HexaBPM__Internal_SR_Status__r.Name = 'Draft']) {
				IsAllowedToSubmit = true;
		}
		if(IsAllowedToSubmit) {
				objRequest.HexaBPM__finalizeAmendmentFlg__c = true;
				objRequest.HexaBPM__Submitted_Date__c = system.today();
				objRequest.HexaBPM__Submitted_DateTime__c = system.now();
				for(HexaBPM__SR_Status__c status :[select id, Name from HexaBPM__SR_Status__c where Name = 'Submitted' or Name = 'submitted' limit 1]) {
						objRequest.HexaBPM__Internal_SR_Status__c = status.id;
						objRequest.HexaBPM__External_SR_Status__c = status.id;
				}
				try{
						update objRequest;
				} catch(DMLException e) {
						string DMLError = e.getdmlMessage(0) + '';
						if(DMLError == null) {
								DMLError = e.getMessage() + '';
						}
						strResult = DMLError;
				}
		}
		return strResult;
}

	public class ButtonResponseWrapper {
		@AuraEnabled public string pageActionName { get; set; }
		@AuraEnabled public string communityName { get; set; }
		@AuraEnabled public string errorMessage { get; set; }
		@AuraEnabled public string CommunityPageName { get; set; }
		@AuraEnabled public string sitePageName { get; set; }
		@AuraEnabled public string strBaseUrl { get; set; }
		@AuraEnabled public string srId { get; set; }
		@AuraEnabled public string flowId { get; set; }
		@AuraEnabled public string pageId { get; set; }
		@AuraEnabled public boolean isPublicSite { get; set; }
		@AuraEnabled public object debugger { get; set; }
	}
	public class RespondWrap {
		@AuraEnabled public list<HexaBPM__SR_Doc__c> SRDocs { get; set; }
		@AuraEnabled
		public HexaBPM__Section_Detail__c ButtonSection { get; set; }
		@AuraEnabled public object debugger { get; set; }
		@AuraEnabled
		public HexaBPM__Service_Request__c srObject { get; set; }
	}
}