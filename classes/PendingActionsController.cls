/*********************************************************************************************************************
 *  Name        : PendingActionsController
 *  Author      : Bilal Nazir
 *  Company     : NSI JLT
 *  Date        : 2014-10-18     
 *  Purpose     : This class returns all pending steps based on user and account information.  
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    18-Jan-2015 Kaavya        Include Awaiting Re- Upload steps   
 V1.2    18-Aug-2015 Kaavya        Modified to include IT pending actions  
 V1.3    23-Aug-2015 Saima         Added PROP services list
 V1.4    31-Jan-2016 Ravi          Added the logic for Listing Services
 V1.5    01-Feb-2015 Ravi          Added the Fitout Services access 
 V2.0    03-Apr-2016 Claude       Added separate list for events
 V2.1    07-Apr-2016 Claude       Added Contact record roles
 V2.2    04-Sep-2016 Sravan       Added conditions to restrict Steps with SR status set to Cancelled TKT # 2954
 V2.3    27-Oct-2016 Sravan       Added code for Mortgage User
**********************************************************************************************************************/
public with sharing class PendingActionsController {

    public string accId{get;set;}
    public string contractorId{get;set;}
    public List<Step__c> steps {get; set;} 
     
    public List<Step__c> stepsGS {get; set;} 
    public List<Step__c> stepsPROP {get; set;} 
    public string accessType{set;get;} 
    public Boolean ROCaccess{set;get;} 
    public Boolean GSaccess{set;get;} 
	// Fazal Added
	public Boolean OTaccess{set;get;}
    public Boolean ITaccess{set;get;}
    public Boolean PROPaccess{set;get;} 
    
    public List<Step__c> allsteps {get; set;}
    public List<Step__c> stepsIT {get; set;}
    public Map<String,List<Step__c>> stepmap {get;set;}
    
    //V1.4
    public Boolean ListingAccess {set;get;}
    public list<Step__c> ListingSteps {get; set;}
     
    //V1.5
    public Boolean FitOutAccess {set;get;}
    public list<Step__c> FitOutSteps {get; set;}
    
    //V2.0
  public Boolean isEvent             { get; set; }
  public List<Step__c> eventFitOutSteps    { get; set; }
  
  // Fazal Added
  public Boolean isOthers { get; set; }
  public List<Step__c> OtherSteps {get; set; }
  
  //V2.3
  Public Boolean isMortgage{get;set;}
  Public List<Step__c> mortgageSteps{get;set;}
     
    /**
     * Returns all pending steps based on user and account information.
     */        
    public PageReference Steps() {
        string userId = Userinfo.getUserId();
        string CommunityUserRoles = '';
        
        System.debug('The user ID: ' + userID);
        
        for(User usr:[select id,contact.contractor__c,contact.Role__c,Account_Id__c,Community_User_Role__c,AccountId from User where id=:userId]){
            /*V2.1 - Claude - Replaced by contact level role
            if(usr.Community_User_Role__c!=null && usr.Community_User_Role__c!=''){
                CommunityUserRoles = usr.Community_User_Role__c;
            }*/
            if(usr.contact.Role__c !=null && usr.contact.Role__c!=''){
                CommunityUserRoles = usr.contact.Role__c;
            }else if(usr.Community_User_Role__c!=null && usr.Community_User_Role__c!=''){
                CommunityUserRoles = usr.Community_User_Role__c;
            }
            accId = usr.AccountId;
            if(usr.contact!=null && usr.contact.contractor__c!=null){
              contractorId = usr.contact.contractor__c;
            }
        }
        
        if(String.isNotBlank(CommunityUserRoles)){
          System.debug('The role: ' + CommunityUserRoles);  
        }
        
        if(String.isNotBlank(contractorId)){
          system.debug('---contractorId---'+contractorId);
        }
        
        if(String.isNotBlank(CommunityUserRoles) && CommunityUserRoles.contains('Fit-Out Services')){
            allsteps = [SELECT id, SR_Record_Type__c, Name, Assigned_to_client__c,Step_Name__c,SR_Group__c, Step_Status__c, CreatedDate, SR__c, SR__r.External_Status_Name__c, SR__r.Portal_Service_Request_Name__c 
                        from Step__c 
                        where SR__r.contractor__c =:contractorId  
                        AND (Owner__c='Contractor') 
                        //AND (Step_Status__c ='Pending Customer Inputs' OR Step_Status__c ='Awaiting Re-upload' OR Step_Status__c ='Payment Pending') //v1.1 //Payment pending status added fro Exit process
                        AND Status_Type__c != 'End'
                        AND SR__r.Pre_GoLive__c = false
                        //AND(SR_Group__c='ROC') // V1.2 Commented by Kaavya   on Aug 18th,2015
                        ORDER BY CreatedDate ASC,SR_Group__c asc];
            system.debug('---allsteps---'+allsteps);
        } else{
            allsteps = [SELECT id, SR_Record_Type__c, Name,Assigned_to_client__c, Step_Name__c,SR_Group__c, Step_Status__c, CreatedDate, SR__c, SR__r.External_Status_Name__c, SR__r.Portal_Service_Request_Name__c 
                        from Step__c 
                        where SR__r.Customer__c =:accId  
                        AND (OwnerId= :userId OR Owner.Name='Client Entry User' OR Owner.Name='Client Entry Approver' OR Assigned_to_client__c =true) //Added by Kaavya on 20th Mar to include FitOut steps assgined to client
                        //AND (Step_Status__c ='Pending Customer Inputs' OR Step_Status__c ='Awaiting Re-upload' OR Step_Status__c ='Payment Pending') //v1.1 //Payment pending status added fro Exit process
                        AND Status_Type__c != 'End'
                        AND SR__r.Pre_GoLive__c = false
                        //AND(SR_Group__c='ROC') // V1.2 Commented by Kaavya   on Aug 18th,2015
                        ORDER BY CreatedDate ASC,SR_Group__c asc];
        }
        
        if(String.isNotBlank(accId)){
          System.debug('Account ID:' + accId);  
        }
        
        if(String.isNotBlank(userId)){
          System.debug('User ID:' + userId);  
        }
                
       /******* Start of V1.2 **************
       //Commented by Kaavya   on Aug 18th,2015    
       stepsGS = [SELECT id, Name, Step_Name__c,SR_Group__c, Step_Status__c, CreatedDate, SR__c, SR__r.External_Status_Name__c, SR__r.Portal_Service_Request_Name__c 
                from Step__c 
                where SR__r.Customer__c =:accId  
                AND (OwnerId= :userId OR Owner.Name='Client Entry User' OR Owner.Name='Client Entry Approver') 
                AND (Step_Status__c ='Pending Customer Inputs' OR Step_Status__c ='Awaiting Re-upload' ) //v1.1  
                AND(SR_Group__c='GS')
                ORDER BY CreatedDate ASC];
           */
                
        steps = new List<Step__c>();
        stepsGS = new List<Step__c>();
        stepsIT = new List<Step__c>();
        stepsPROP = new list<Step__c>();
        ListingSteps = new list<Step__c>();
        FitOutSteps = new list<Step__c>();
    eventFitOutSteps = new list<Step__c>();
    mortgageSteps = new List<Step__c>(); //V2.3
	OtherSteps = new List<Step__c>(); //Fazal Added
	
        
        if(String.isNotBlank(CommunityUserRoles)){
          
          for(Step__c stp: allsteps){
            
            System.debug('The step details: ' + stp);
              
              if(CommunityUserRoles.contains('Company Services') && (stp.SR_Group__c == 'Fit-Out & Events' || stp.SR_Group__c == 'ROC' || stp.SR_Group__c == 'RORP' || stp.SR_Group__c == 'BC'))
			  {
                  if(stp.SR_Group__c == 'ROC')//Commented by Kaavya on Mar 20th--- || stp.SR_Group__c == 'Fit-Out & Events')
                      steps.add(stp);
                  else if(stp.SR_Group__c == 'RORP' || stp.SR_Group__c == 'BC')
                      stepsPROP.add(stp); 
                  ROCaccess = true;
                  PROPaccess = true;
              }
              if(CommunityUserRoles.contains('Company Services') == false && CommunityUserRoles.contains('Property Services') && stp.SR_Group__c == 'RORP')
			  {
                  if(stp.SR_Group__c == 'RORP')
                      stepsPROP.add(stp);
                  PROPaccess = true;
              }
              if(stp.SR_Group__c == 'GS' && CommunityUserRoles.contains('Employee Services')  && stp.SR__r.External_Status_Name__c !='Cancelled')
			  {    // V2.2
                  stepsGS.add(stp);
                  GSaccess = true;
              }
              system.debug('CommunityUserRoles is : '+CommunityUserRoles);
              system.debug('stp.SR_Group__c is : '+stp.SR_Group__c);
              if(stp.SR_Group__c == 'IT' && CommunityUserRoles.contains('IT Services') )
			  {
                  stepsIT.add(stp);
                  ITaccess =true;
              }
              
              //V1.4
              if(stp.SR_Group__c == 'Listing' && CommunityUserRoles.contains('Listing Services') )
			  {
                  ListingSteps.add(stp);
                  ListingAccess = true;
              }
              /* V2.0 - Replaced by separated logic
          if(stp.SR_Group__c == 'Fit-Out & Events' && (CommunityUserRoles.contains('Fit-Out Services') || CommunityUserRoles.contains('Event Services')) ){
            FitOutSteps.add(stp);
            FitOutAccess = true;
          }*/
          
          if(stp.SR_Group__c == 'Fit-Out & Events' && (CommunityUserRoles.contains('Fit-Out Services') || CommunityUserRoles.contains('Company Services')) )
		  {
            FitOutSteps.add(stp);
            FitOutAccess = true;
          }
          
          if(stp.SR_Group__c == 'Fit-Out & Events' && CommunityUserRoles.contains('Event Services') )
		  {
            eventFitOutSteps.add(stp);
            isEvent = true;
          }
          
		 //Fazal Added
		 
		 if(stp.SR_Group__c == 'Other')
		{
                    //steps.add(stp);
                  
                    OtherSteps.add(stp); 
					isOthers = true;
                  
         }
		// V2.3
        if(stp.SR_Group__c =='RORP' && (CommunityUserRoles.contains('Mortgage Registration') || CommunityUserRoles.contains('Discharge of Mortgage') || CommunityUserRoles.contains('Variation of Mortgage')))
        {
          isMortgage = true;
          mortgageSteps.add(stp);
        }
        
              /*
              if(stp.SR_Group__c == 'ROC')
                  steps.add(stp);
              else if(stp.SR_Group__c == 'GS')
                  stepsGS.add(stp);
              else if(stp.SR_Group__c == 'IT')
                  stepsIT.add(stp);
              else if(stp.SR_Group__c == 'BC')
                  stepsPROP.add(stp);
              */
          }
        }
        if(test.isRunningTest())
       dummyMethod();
       
       /*************************End of V1.2 *********/
        return null;
    }
    public void dummyMethod(){
      Service_Request__c objSR = new Service_Request__c();
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
      objSR.Address_Changed__c = false;
      objSR.Additional_Details__c = 'test';
      objSR.Additional_Details__c = 'Code Coverage';
      objSR.Adhering_to_Safe_Horbour__c = true;
      objSR.Annual_Amount_AED__c = 124;
      objSR.Address_Details__c = '';
    }
    /*public PendingActionsController() {
        string userId = userinfo.getUserId();
        for(User usr:[select id,Account_Id__c,Community_User_Role__c from User where id=:userId]){
            accId = usr.Account_Id__c;
            if(usr.Community_User_Role__c!=null && usr.Community_User_Role__c!=''){
            // Start of V1.2 Modified by Kaavya
                if(usr.Community_User_Role__c.contains('Company Services')){
                    ROCaccess = true;
                }
                if(usr.Community_User_Role__c.contains('Employee Services')){
                    GSaccess = true;
                }
                if(usr.Community_User_Role__c.contains('IT Services')){
                    ITaccess =true;
                }// End of V1.2
                if(usr.Community_User_Role__c.contains('Property Services')){//V1.3
                    PROPaccess = true;
                }
            }
        }
    }*/
}