/*
        Author      :   Arun 
        Description :   Class to suport SR PSA_Deduction Service request 
        
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By      Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    29-06-2020 Arun Singh  Created
    V1.1    31-03-2021 Veera Added PSA Deposit to updated in SR field Security_Deposit_AED__c.
    
*/    

public with sharing class cls_PSA_Deduction_Process {

    public Service_Request__c SRData {get;set;}
    public map < string, string > mapParameters;
    string RecordTypeId;

    public Decimal afterDeduction {get;set;}
    public Account ObjAccount {get;set;}

    public List < PSA_Deduction_Detail__c > ListPSADeductions {get;set;}
    public PSA_Deduction_Detail__c ObjPSADeduction {get;set;}

    public cls_PSA_Deduction_Process(ApexPages.StandardController controller) {



        List < String > fields = new List < String > {
            'Submitted_Date__c',
            'Issued_Share_Capital_Membership_Interest__c',
            'No_of_documents_lost__c',
            'Fine_Amount_AED__c',
            'Expiry_Date__c',
            'Customer__c',
            'Customer__r.Registration_License_No__c',
            'Customer__r.PSA_Deposit__c',
            'Customer__r.Index_Card__r.Name','Customer__r.Index_Card_Status__c',
            'Customer__r.Sponsored_Employee__c',
            'Customer__r.ROC_Status__c',
            'Reason_for_Renewal__c',
            'Quantity__c',
            'Court_Order__c'
        };

        if (!Test.isRunningTest()) controller.addFields(fields); // still covered
        SRData = (Service_Request__c) controller.getRecord();

        mapParameters = new map < string, string > ();
        if (apexpages.currentPage().getParameters() != null)
            mapParameters = apexpages.currentPage().getParameters();
        if (mapParameters.get('RecordType') != null)
            RecordTypeId = mapParameters.get('RecordType');
        else
            RecordTypeId = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByDeveloperName().get('PSA_Deduction').getRecordTypeId();

        ListPSADeductions = new List < PSA_Deduction_Detail__c > ();

        if (SRData.id == null) {
            SRData.RecordTypeId = RecordTypeId;
        }
        else
            SRaccount(SRData.Customer__c);
        
        SRData.Would_you_like_to_opt_our_free_couri__c = 'No';

    }

    public List < PSA_Deduction_Detail__c > getPSADeductionDetail() {
        afterDeduction = 0;

        ListPSADeductions = [select id, Applicant__c, Amount_to_be_Deducted__c, Other_Applicant_Name__c, Applicant_Name__c, Description__c, Estimated_Service_Fee__c, Description_of_Fines__c, Fines__c, Service_Fee__c, Service_Request__c, Service_request_list__c, Total_Amount_to_be_Deducted__c from PSA_Deduction_Detail__c where Service_Request__c != null and Service_Request__c =: SRData.id];
        for (PSA_Deduction_Detail__c ObjFD: ListPSADeductions) {
            afterDeduction += ObjFD.Amount_to_be_Deducted__c;
        }
        if (!ListPSADeductions.isEmpty() && !test.isRunningTest()) {
            SRData.Fine_Amount_AED__c = SRData.Customer__r.PSA_Deposit__c - afterDeduction;
            SRData.Issued_Share_Capital_Membership_Interest__c = afterDeduction;
        }
        return ListPSADeductions;


    }

    public void AddRecord() {
        ObjPSADeduction = new PSA_Deduction_Detail__c();
        ObjPSADeduction.Account__c = SRData.Customer__c;
        // ObjPro.Status__c='Draft';
    }


    public void CancelSaveRow() {
        ObjPSADeduction = null;

    }

    public void SaveRow() {
        
        boolean Check=IsValid();
        if(Check)
        try {
            if (SRData.id == null)
                upsert SRData;

            ObjPSADeduction.Service_Request__c = SRData.id;


            upsert ObjPSADeduction;
            ObjPSADeduction = null;
            getPSADeductionDetail();
        } catch (Exception e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }
       



    }
public boolean IsValid()
{
    boolean Ischecked=true;
    
    if(string.isblank(ObjPSADeduction.Other_Applicant_Name__c) && string.isblank(ObjPSADeduction.Applicant_Name__c))
    {
         ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide Applicant details.');
            ApexPages.addMessage(myMsg);
            Ischecked= false;
    }
    
    if(string.isblank(ObjPSADeduction.Description__c) && ObjPSADeduction.Service_Fee__c!=ObjPSADeduction.Estimated_Service_Fee__c)
    {
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide Description.');
            ApexPages.addMessage(myMsg);
            Ischecked= false;
        
    }
    return Ischecked;
        
}
    public void removingRow() {
        Integer param = Integer.valueOf(Apexpages.currentpage().getParameters().get('index'));
        if (ListPSADeductions.size() >= param) {
            PSA_Deduction_Detail__c TempObjPro = ListPSADeductions[param];
            ListPSADeductions.remove(param);
            if (TempObjPro.id != null)
                delete TempObjPro;
        }



    }
    
 public void SRaccount(string fieldValue) 
 {
     
       List < Account > ListAccount = [select id, Name,Index_Card_Status__c, Registration_License_No__c, PSA_Deposit__c, Index_Card__r.Name, Sponsored_Employee__c, ROC_Status__c from account where id =: fieldValue];
                if (!ListAccount.isEmpty()) {
                    ObjAccount = ListAccount[0];
                    
                }
                
 }

    //Reporting Financial Institution type
    public void RecordvalueChange() {
        string fieldChanged = Apexpages.currentPage().getParameters().get('field_name');
        string fieldValue = Apexpages.currentPage().getParameters().get('field_value');


        System.debug('fieldValue===>' + fieldValue);
        System.debug('fieldChanged===>' + fieldChanged);

        if (!string.isblank(fieldChanged)) {


            if (fieldChanged == 'Customer__c') {

                SRData.put(fieldChanged, fieldValue);

                //List < Account > ListAccount = [select id, Name,Index_Card_Status__c, Registration_License_No__c, PSA_Deposit__c, Index_Card__r.Name, Sponsored_Employee__c, ROC_Status__c from account where id =: fieldValue];
                SRaccount(fieldValue);
                if (ObjAccount!=null) 
                {
                   // ObjAccount = ListAccount[0];
                    SRData.Customer__r = ObjAccount;


                }


                List < user > ListContact = [select ContactId, Email, Phone from User where Contact.AccountId =: fieldValue AND IsActive = true AND Community_User_Role__c INCLUDES('Employee Services') order by createdDate limit 1];

                if (!ListContact.isempty() && ObjAccount != null) {
                    SRData.Email__c = ListContact[0].Email;
                    SRData.Send_SMS_To_Mobile__c = ListContact[0].Phone;
                    SRData.Entity_Name__c = ObjAccount.Name;
                }

            } else {
                ObjPSADeduction.put(fieldChanged, fieldValue);

                PSA_Deduction_Fee__c myCS1 = PSA_Deduction_Fee__c.getValues(fieldValue);
                if (myCS1 != null) {
                    ObjPSADeduction.Service_Fee__c = myCS1.Service_Fee__c;
                    ObjPSADeduction.Estimated_Service_Fee__c = ObjPSADeduction.Service_Fee__c;
                } else {
                    ObjPSADeduction.Service_Fee__c = 0;
                    ObjPSADeduction.Estimated_Service_Fee__c = 0;

                }
            }




        }


    }

    public List < SelectOption > getApplicantName() {
        List < SelectOption > options = new List < SelectOption > ();

        options.add(new SelectOption('', '-None-'));
        for (Relationship__c contSpn: [select id, Object_Contact__c, Relationship_Name__c from Relationship__c where
                Subject_Account__c =: ObjAccount.id and Object_Contact__c != null and Relationship_Type__c = 'Has DIFC Sponsored Employee'
                and Active__c = true
            ]) {
            options.add(new SelectOption(contSpn.Object_Contact__c, contSpn.Relationship_Name__c));
        }
        return options;
    }


    public PageReference SaveRecord() {

        try {
            SRData.Security_Deposit_AED__c = SRData.Customer__r.PSA_Deposit__c; //Veera added for 12534
            upsert SRData;
            PageReference acctPage = new ApexPages.StandardController(SRData).view();
            acctPage.setRedirect(true);
            return acctPage;

        } catch (Exception e) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }

        return null;
    }

}