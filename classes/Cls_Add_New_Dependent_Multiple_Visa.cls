/*********************************************************************************************************************
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    30-Dec-2019 Arun         Created base class for VF Renewal_New_Dependent_Multiple_Visa,Add_Cancellation_Dependent_Multiple_Visa and Add_New_Dependent_Multiple_Visa
 v1.2    17-Jan-2020 Arun         change applicanet list    
**********************************************************************************************************************/


public class Cls_Add_New_Dependent_Multiple_Visa {


    public string RecordTypeId;
    public map<string,string> mapParameters;
    public Account ObjAccount{get;set;}
    public Service_Request__c SRData{get;set;}
    @testvisible string RecordTypeName;
    public string LinkedSRId;
    public string Occupation{get;set;}
    public boolean ApplicantSelected{set;get;}

    public Cls_Add_New_Dependent_Multiple_Visa(ApexPages.StandardController controller) 
    {
            ApplicantSelected=false;
            ObjAccount=new account();
            
            List<String> fields = new List<String> {'Occupation_GS__c','Customer__c','Sponsor__c'};
            if (!Test.isRunningTest()) controller.addFields(fields); // still covered
            
            SRData=(Service_Request__c )controller.getRecord();
            Occupation=SRData.Occupation_GS__c;
           
            mapParameters = new map<string,string>();        
            if(apexpages.currentPage().getParameters()!=null)
            mapParameters = apexpages.currentPage().getParameters();              
            if(mapParameters.get('RecordType')!=null) 
            RecordTypeId= mapParameters.get('RecordType');   

            LinkedSRId=mapParameters.get('related_requestId');
            
             if(mapParameters.get('type')!=null) 
            RecordTypeName= mapParameters.get('type');    
        
            Service_Request__c eachRequest = new Service_Request__c();
            if(SRData.id !=null){

            eachRequest = [SELECT Id,Customer__c,RecordType.Name,Customer__r.Exempted_from_UBO_Regulations__c,Customer__r.Qualifying_Purpose_Type__c,Customer__r.Qualifying_Type__c FROM Service_Request__c where Id=:SRData.id];

             ObjAccount= eachRequest.Customer__r;
              RecordTypeName=eachRequest.RecordType.Name;
            }
            
   

            for(User objUsr:[select id,ContactId,Email,Phone,Contact.Account.Exempted_from_UBO_Regulations__c,Contact.Account.Legal_Type_of_Entity__c,Contact.AccountId,Contact.Account.Company_Type__c,
                             Contact.Account.Registration_License_No__c,Contact.Account.Index_Card__r.Name,Contact.Account.Next_Renewal_Date__c,Contact.Account.Name,
                             Contact.Account.Qualifying_Type__c,Contact.Account.Qualifying_Purpose_Type__c,Contact.Account.Contact_Details_Provided__c,
                             Contact.Account.Sector_Classification__c,Contact.Account.License_Activity__c,Contact.Account.Is_Foundation_Activity__c  
                             from User where Id=:userinfo.getUserId()])
            {
              
                 if(SRData.id==null)
                 {
                    SRData.Customer__c = objUsr.Contact.AccountId;
                   SRData.RecordTypeId=RecordTypeId;
                    SRData.Email__c = objUsr.Email;
                    SRData.Legal_Structures__c=objUsr.Contact.Account.Legal_Type_of_Entity__c;
                    SRData.Send_SMS_To_Mobile__c = objUsr.Phone;
                    SRData.Entity_Name__c=objUsr.Contact.Account.Name;
                    //SRData.Expiry_Date__c=objUsr.Contact.Account.Next_Renewal_Date__c;
                    
                    ObjAccount= objUsr.Contact.Account;
                    
                    SRData.License_Number__c=objUsr.Contact.Account.Registration_License_No__c;
                    SRData.Establishment_Card_No__c=objUsr.Contact.Account.Index_Card__r.Name;
                    SRData.Linked_SR__c=LinkedSRId;
                    if(!string.isEmpty(LinkedSRId))
                    {
                        
                        Service_Request__c LinkedSRData=[select Sponsor__c ,Email__c,IBAN_Number__c,Express_Service__c,Already_opened_a_family_file_with_DNRD__c,Emirate__c,City__c,Area__c,Street_Address__c,Building_Name__c,PO_BOX__c,P_O_Box_Emirate__c,Residence_Phone_No__c,Mobile_Number__c,Work_Phone__c,Identical_Business_Domicile__c,Phone_No_Outside_UAE__c,City_Town__c,Address_Details__c,Title_Sponsor__c,Sponsor_First_Name__c,Sponsor_Middle_Name__c,Sponsor_Last_Name__c,First_Name_Arabic_Sponsor__c,Middle_Name_Arabic_Sponsor__c,Last_Name_Arabic_Sponsor__c,Sponsor_Visa_No__c,Sponsor_Passport_Expiry__c,Sponsor_Gender__c,Sponsor_Nationality__c,Sponsor_Visa_Expiry_Date__c,Sponsor_Passport_No__c,Sponsor_Mother_Full_Name__c,Sponsor_Date_of_Birth__c,Is_Applicant_Salary_Above_AED_20_000__c,Sponsor_Monthly_Salary_AED__c,Would_you_like_to_opt_our_free_couri__c,Use_Registered_Address__c,Consignee_FName__c,Courier_Mobile_Number__c,Consignee_LName__c,Courier_Cell_Phone__c,Apt_or_Villa_No__c from Service_Request__c where id=:LinkedSRId];
                        
                       //SRData=LinkedSRData.clone(); tried but was not working
                      // SRData.RecordTypeId=RecordTypeId;
                      //SRData.Linked_SR__c=LinkedSRId;
//Entity Details:Already copied from User


//Correspondence Details: Already copied from User


//Service Type

                        SRData.Sponsor__c =LinkedSRData.Sponsor__c;  
                        SRData.SponsorId_hidden__c =LinkedSRData.Sponsor__c;  
                        
                        SRData.Express_Service__c=LinkedSRData.Express_Service__c;
                        SRData.Already_opened_a_family_file_with_DNRD__c=LinkedSRData.Already_opened_a_family_file_with_DNRD__c;

//Address Inside UAE



                         SRData.Emirate__c=LinkedSRData.Emirate__c;
                         SRData.City__c=LinkedSRData.City__c;
                         SRData.Area__c=LinkedSRData.Area__c;
                         SRData.Street_Address__c=LinkedSRData.Street_Address__c;
                         SRData.Building_Name__c=LinkedSRData.Building_Name__c;
                                            
                         SRData.PO_BOX__c=LinkedSRData.PO_BOX__c;
                         SRData.P_O_Box_Emirate__c=LinkedSRData.P_O_Box_Emirate__c;
                         SRData.Residence_Phone_No__c=LinkedSRData.Residence_Phone_No__c;
                         SRData.Mobile_Number__c=LinkedSRData.Mobile_Number__c;
                         SRData.Work_Phone__c=LinkedSRData.Work_Phone__c;
 
 //Address Outside UAE

 
                         SRData.Identical_Business_Domicile__c=LinkedSRData.Identical_Business_Domicile__c;
                         SRData.Phone_No_Outside_UAE__c=LinkedSRData.Phone_No_Outside_UAE__c;
                         SRData.City_Town__c=LinkedSRData.City_Town__c;
                         SRData.Address_Details__c=LinkedSRData.Address_Details__c;

 
 //Details of the Sponsor
                        
                        SRData.Title_Sponsor__c=LinkedSRData.Title_Sponsor__c;
                        SRData.Sponsor_First_Name__c=LinkedSRData.Sponsor_First_Name__c;
                        SRData.Sponsor_Middle_Name__c=LinkedSRData.Sponsor_Middle_Name__c;
                        SRData.Sponsor_Last_Name__c=LinkedSRData.Sponsor_Last_Name__c;

                        SRData.First_Name_Arabic_Sponsor__c=LinkedSRData.First_Name_Arabic_Sponsor__c;
                        SRData.Middle_Name_Arabic_Sponsor__c=LinkedSRData.Middle_Name_Arabic_Sponsor__c;
                        SRData.Last_Name_Arabic_Sponsor__c=LinkedSRData.Last_Name_Arabic_Sponsor__c;

                        SRData.Sponsor_Visa_No__c=LinkedSRData.Sponsor_Visa_No__c;
                        SRData.Sponsor_Passport_Expiry__c=LinkedSRData.Sponsor_Passport_Expiry__c;
                        SRData.Sponsor_Gender__c=LinkedSRData.Sponsor_Gender__c;
                        SRData.Sponsor_Nationality__c=LinkedSRData.Sponsor_Nationality__c;
                        SRData.IBAN_Number__c=LinkedSRData.IBAN_Number__c;
                        SRData.Sponsor_Visa_Expiry_Date__c=LinkedSRData.Sponsor_Visa_Expiry_Date__c;
                        SRData.Sponsor_Passport_No__c=LinkedSRData.Sponsor_Passport_No__c;
                        SRData.Sponsor_Mother_Full_Name__c=LinkedSRData.Sponsor_Mother_Full_Name__c;
                        SRData.Sponsor_Date_of_Birth__c=LinkedSRData.Sponsor_Date_of_Birth__c;
                        
                        
//Sponsor Salary Details
                        SRData.Is_Applicant_Salary_Above_AED_20_000__c=LinkedSRData.Is_Applicant_Salary_Above_AED_20_000__c;
                        SRData.Sponsor_Monthly_Salary_AED__c=LinkedSRData.Sponsor_Monthly_Salary_AED__c;

//Courier Details

                        SRData.Would_you_like_to_opt_our_free_couri__c=LinkedSRData.Would_you_like_to_opt_our_free_couri__c;
                        SRData.Use_Registered_Address__c=LinkedSRData.Use_Registered_Address__c;
                        SRData.Consignee_FName__c=LinkedSRData.Consignee_FName__c;
                        SRData.Courier_Mobile_Number__c=LinkedSRData.Courier_Mobile_Number__c;
                        SRData.Consignee_LName__c=LinkedSRData.Consignee_LName__c;
                        SRData.Courier_Cell_Phone__c=LinkedSRData.Courier_Cell_Phone__c;
                        SRData.Apt_or_Villa_No__c=LinkedSRData.Apt_or_Villa_No__c;


                        
                    }
                    
                    
                  }
              

            }
    }
     public PageReference ApplicantClickSR() 
     { 
         ApplicantSelected=true;
         if(SRData.Contact__c!=null)
         {
         List<contact> ListContact=[select id,Job_Title__c,Place_of_Birth__c,Sponsor_Type__c,Country_of_Birth__c,Previous_Nationality__c,Date_of_Issue__c,Nationality__c,Salutation,FirstName, LastName,Middle_Names__c,Visa_Number__c,Gender__c,Passport_Expiry_Date__c,Passport_No__c,RELIGION__c,Issued_Country__c,Passport_Type__c,Birthdate,First_Name_Arabic__c,Last_Name_Arabic__c,Place_of_Issue__c,Spoken_Language1__c,Marital_Status__c,Spoken_Language2__c,
            Middle_Name_Arabic__c,Visa_Expiry_Date__c,Qualification__c,Mother_Full_Name__c,Emirate__c,City__c,Area__c,email,Spoken_Language3__c,
            PO_Box_HC__c,Phone,UID_Number__c,ADDRESS_LINE1__c,ADDRESS_LINE2__c,Country__c,Domicile__c,CITY1__c,OtherPhone,Address_outside_UAE__c,MobilePhone,Work_Phone__c 
            from Contact where id=:SRData.Contact__c];
         if(!ListContact.isEmpty())
         {
             Contact Objcontact=ListContact[0];
             
             SRData.Title__c=Objcontact.Salutation;
             SRData.First_Name__c=Objcontact.FirstName;
             SRData.Middle_Name__c=Objcontact.Middle_Names__c;
             SRData.Last_Name__c=Objcontact.LastName;
             SRData.Nationality_list__c=Objcontact.Nationality__c;
             SRData.Qualification__c=Objcontact.Qualification__c;
             SRData.Previous_Nationality__c=Objcontact.Previous_Nationality__c;
             SRData.Gender__c=Objcontact.Gender__c;
             SRData.Date_of_Birth__c=Objcontact.Birthdate;
             SRData.Passport_Number__c=Objcontact.Passport_No__c;
             SRData.Country_of_Birth__c=Objcontact.Country_of_Birth__c;
             SRData.Passport_Place_of_Issue__c=Objcontact.Place_of_Issue__c;
             SRData.Passport_Type__c=Objcontact.Passport_Type__c;
             SRData.Passport_Date_of_Issue__c=Objcontact.Date_of_Issue__c;
             SRData.Passport_Country_of_Issue__c=Objcontact.Issued_Country__c;
             SRData.Religion__c=Objcontact.RELIGION__c;
             SRData.Passport_Date_of_Expiry__c=Objcontact.Passport_Expiry_Date__c;
             SRData.First_Language__c=Objcontact.Spoken_Language1__c;
             SRData.Marital_Status__c=Objcontact.Marital_Status__c;
             SRData.Second_Language__c=Objcontact.Spoken_Language2__c;
             SRData.Email_Address__c=Objcontact.email;
             SRData.Third_Language__c=Objcontact.Spoken_Language3__c    ;
             SRData.Mother_Full_Name__c=Objcontact.Mother_Full_Name__c;
             SRData.Residence_Visa_No__c=Objcontact.Visa_Number__c;
             SRData.UID_Number__c=Objcontact.UID_Number__c;
             //SRData.Applicant_Visa_Issued_Date__c=Objcontact.Salutation;
             SRData.Applicant_Visa_Expired_Date__c=Objcontact.Visa_Expiry_Date__c;
             SRData.Relation__c=Objcontact.Sponsor_Type__c;
             SRData.Place_of_Birth__c=Objcontact.Place_of_Birth__c;
             SRData.Occupation_GS__c=Objcontact.Job_Title__c;
             if(Objcontact.Job_Title__c!=null)
             Occupation=String.valueOf(Objcontact.Job_Title__c).substring(0, 15);
             
         }
         }
         return null;
     }
      public PageReference SavedSR() 
     { 
      
      SRData.Occupation_GS__c=Occupation;
      //System.debug('Occ===>'+Occ);
      
     try
     {
        
     // Add the account to the database. 
        upsert SRData;
        
        
        // Send the user to the detail page for the new account.
        Map<id,Attachment> MapAttch=new  Map<id,Attachment>();
            
            Map<id,SR_Doc__c> SR_Doc_masterID=new  Map<id,SR_Doc__c>();
            Map<id,id> SR_Doc_Linked_masterID=new  Map<id,id>();
            List<SR_Doc__c> ListSRDoc=new List<SR_Doc__c>();
            
            for(SR_Doc__c ObjDoc:[select id,Document_Master__c,Status__c,Document_Master__r.Code__c from SR_Doc__c where Service_Request__c =:SRData.id])
            {
                SR_Doc_masterID.put(ObjDoc.Document_Master__c,ObjDoc);
            }
            
            for(SR_Doc__c ObjDoc:[select id,Document_Master__c,Doc_ID__c,Document_Master__r.Code__c from SR_Doc__c where Service_Request__c =:SRData.Linked_SR__c])
            {
                SR_Doc_Linked_masterID.put(ObjDoc.Doc_ID__c,ObjDoc.Document_Master__c);
            }
            List<Attachment> ListAttc=new List<Attachment> ();
            for(Attachment ObjAttachment:[SELECT Id,
                              Body,
                              Name,
                              Description,
                              ParentId
                              FROM Attachment
                              WHERE Id in : SR_Doc_Linked_masterID.keyset()])
                              {
                                
                                  SR_Doc__c ObjDoc =SR_Doc_masterID.get(SR_Doc_Linked_masterID.get(ObjAttachment.Id));
                                  if(ObjDoc !=null)
                                  {
                                    Attachment newFile = ObjAttachment.clone();
                                    newFile.ParentId =ObjDoc.Id;
                                    ListAttc.add(newFile);
                                    ObjDoc.Status__c = 'Uploaded';
                                    update ObjDoc;
                                    ListSRDoc.add(ObjDoc);
        
                                }
                              }
            if(!ListAttc.isEmpty())
            {
                insert ListAttc;
                update ListSRDoc;
            }
           
        
        
        return MoveToUploadDocuments();
        }
        catch(Exception e) {
       // SRData.Occupation_GS__c=Occ;
         ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getMessage() );
        ApexPages.addMessage(msg);
           //   System.debug('Occ===>'+Occ);

    //        String S1 = e.getMessage();
//            S1 = S1.substringBetween('FIELD_CUSTOM_VALIDATION_EXCEPTION, ' , ': [');
  //          Trigger.New[0].adderror(S1);
            
   // System.debug('The following exception has occurred: ' + e.getMessage());
            }
        
        return null;
        
         
     }
     
       public List<SelectOption> getApplicantList()
     {
         //v1.2
    //SRData.Linked_SR__c   
    Set<Id> AddedContacts=new Set<Id>();
    if(SRData.Linked_SR__c!=null)
    {
        for(Service_Request__c ObjSR:[select id,Contact__c from Service_Request__c where Contact__c!=null and Linked_SR__c=:SRData.Linked_SR__c])
        {
            AddedContacts.add(ObjSR.Contact__c);
        }
        
    }
    


         List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        
        for(Contact contSpn : [select Id,Name,Is_Violator__c,Active_Employee__c from Contact where Sponsor__c=:SRData.Sponsor__c AND RecordType.DeveloperName='GS_Contact' AND SR__r.Customer__c=:SRData.Customer__c order by Name ])
        {
            if(!AddedContacts.contains(contSpn.id))
             options.add(new SelectOption(contSpn.Id,contSpn.Name));

        }
          System.debug('options==>'+options);
         return options;
         
        
        /*
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        string sQuery = 'select Id,Name,Is_Violator__c,Active_Employee__c from Contact where Sponsor__c=\''+SRData.Sponsor__c+'\'  AND RecordType.DeveloperName = \'GS_Contact\' AND SR__r.Customer__c=\''+SRData.Customer__c+'\' order by Name limit 50';
        System.debug('sQuery ==>'+sQuery );
        for(Contact contSpn : Database.query(sQuery))
        {
             options.add(new SelectOption(contSpn.Id,contSpn.Name));

        }
          System.debug('options==>'+options);
         return options;
    */
         
    }
     
      public PageReference MoveToUploadDocuments() 
     {
        PageReference pageRef =  Page.DocumentViewer;
        pageRef.getParameters().put('id',SRData.id);
        
        if(RecordTypeName.containsIgnoreCase('RENEWAL'))
        pageRef.getParameters().put('retURLval','apex/Renewal_Dependent_multiple_Visa?id='+SRData.Linked_SR__c);
                else
         if(RecordTypeName.containsIgnoreCase('CANCELLATION'))
        pageRef.getParameters().put('retURLval','apex/Cancellation_Dependent_multiple_Visa?id='+SRData.Linked_SR__c);
        
        else
         if(RecordTypeName.containsIgnoreCase('NEW'))
        pageRef.getParameters().put('retURLval','apex/New_Dependent_multiple_Visa_Request?id='+SRData.Linked_SR__c);
    
        pageRef.setRedirect(true);
        return pageRef;
     }
     


}