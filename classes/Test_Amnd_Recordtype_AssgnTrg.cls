/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Test_Amnd_Recordtype_AssgnTrg {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        Account objAcc = new Account();
        objAcc.Name = 'Amnd Test';
        insert objAcc;
        
        Account objAcc1 = new Account();
        objAcc1.Name = 'TEST';
        objAcc1.ROC_Status__c = 'Active';
        insert objAcc1;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAcc.Id;
        insert objSR;
        
        Amendment__c objAmnd = new Amendment__c();
        objAmnd.ServiceRequest__c = objSR.Id;
        objAmnd.Amendment_Type__c = 'Individual';
        objAmnd.Relationship_Type__c = 'UBO';
        objAmnd.Passport_No__c = 'WEER324324';
        objAmnd.Nationality_list__c = 'Pakistan';
        objAmnd.RecordTypeID = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Liquidator').getRecordTypeId();
      
        
        insert objAmnd;
        
        Amendment__c objAmnd1 = new Amendment__c();
        objAmnd1.ServiceRequest__c = objSR.Id;
        objAmnd1.Amendment_Type__c = 'Individual';
        objAmnd1.Relationship_Type__c = 'Operating Location';
        objAmnd1.RecordTypeID = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Business Centre Office').getRecordTypeId();
       
        insert objAmnd1;
        
        Amendment__c objAmnd2 = new Amendment__c();
        objAmnd2.ServiceRequest__c = objSR.Id;
        objAmnd2.Amendment_Type__c = 'Individual';
        objAmnd2.Relationship_Type__c = 'Auditor';
        objAmnd2.CL_Certificate_No__c = '123';
        objAmnd2.IssuingAuthority__c = 'Others';
        objAmnd2.RecordTypeID = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Body Corporate Tenant').getRecordTypeId();
        objAmnd2.IssuingAuthority__c = 'DIFC - Registered';
        objAmnd2.CL_Certificate_No__c = 'abc';
        objAmnd2.Company_Name__c = objAcc1.ID;
        
        insert objAmnd2;
        
        
        Lease__c l = new Lease__c();
        l.Status__c = 'Active';
        l.Lease_Types__c = 'Data Centre Lease';
        l.Account__c = objAcc.Id;
        insert l;
        
        Amendment__c objAmnd3 = new Amendment__c();
        objAmnd3.ServiceRequest__c = objSR.Id;
        objAmnd3.Amendment_Type__c = 'Body Corporate';
        objAmnd3.Relationship_Type__c = 'Auditor';
        objAmnd3.RecordTypeID = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Individual Tenant').getRecordTypeId();
        objAmnd3.Passport_No__c = '3455';
       
        
        insert objAmnd3;
        
        Amendment__c objAmnd4 = new Amendment__c();
        objAmnd4.ServiceRequest__c = objSR.Id;
        objAmnd4.Amendment_Type__c = 'Body Corporate';
        objAmnd4.Relationship_Type__c = 'Shareholder';
        objAmnd4.RecordTypeID = Schema.SObjectType.Amendment__c.getRecordTypeInfosByName().get('Data Center Requestor').getRecordTypeId();
        insert objAmnd4;
        
        Amendment__c objAmnd5 = new Amendment__c();
        objAmnd5.ServiceRequest__c = objSR.Id;
        objAmnd5.Amendment_Type__c = 'Individual';
        objAmnd5.Relationship_Type__c = 'Shareholder';
        objAmnd.First_Name__c = 'Test Record';
        objAmnd.Passport_No_Occupant__c = 'LLLLL';
        objAmnd.Nationality_Occupant__c = 'India';
        insert objAmnd5;
        
        Amendment__c objAmnd6 = new Amendment__c();
        objAmnd6.ServiceRequest__c = objSR.Id;
        objAmnd6.Amendment_Type__c = 'Body Corporate';
        objAmnd6.Relationship_Type__c = 'Limited Partner';
        insert objAmnd6;
        
        Amendment__c objAmnd7 = new Amendment__c();
        objAmnd7.ServiceRequest__c = objSR.Id;
        objAmnd7.Amendment_Type__c = 'Body Corporate';
        objAmnd7.Relationship_Type__c = 'Limited Partners';
        insert objAmnd7;
        
        Amendment__c objAmnd8 = new Amendment__c();
        objAmnd8.ServiceRequest__c = objSR.Id;
        objAmnd8.Amendment_Type__c = 'Body Corporate';
        objAmnd8.Relationship_Type__c = 'UBO';
        insert objAmnd8;
        
        
    }
}