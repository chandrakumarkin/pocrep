/**
 * Description : Controller for OB_ManageApprovedPerson component
 *
 * ****************************************************************************************
 * History :
 * [06.NOV.2019] Prateek Kadkol - Code Creation
 */
public without sharing class OB_ManageApprovedPersonController {

	@AuraEnabled
	public static RespondWrap fetchAmendRec(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		system.debug(reqWrap);


		respWrap.recordId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();
		//respWrap.amendWrapp = FetchUpdatedAmendListHelper(reqWrap.srId);
		respWrap.amendWrappList = FetchUpdatedAmendListHelper(reqWrap.srId);

		for(HexaBPM__Section_Detail__c sectionObj :[SELECT id, HexaBPM__Component_Label__c, name FROM HexaBPM__Section_Detail__c WHERE HexaBPM__Section__r.HexaBPM__Section_Type__c = 'CommandButtonSection'
												    AND HexaBPM__Section__r.HexaBPM__Page__c = :reqWrap.pageId LIMIT 1]) {
			respWrap.ButtonSection = sectionObj;
		}

		
		return respWrap;

	}

	@AuraEnabled
	public static RespondWrap initNewAmendment(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);

		AmendmentWrapper amedWrap = new AmendmentWrapper();
    

		string recordId = Schema.SObjectType.HexaBPM_Amendment__c.getRecordTypeInfosByName().get('Individual').getRecordTypeId();

		respWrap.recordId = recordId;




		HexaBPM_Amendment__c amedObj = new HexaBPM_Amendment__c();
		amedObj.ServiceRequest__c = reqWrap.srId;
		amedObj.RecordTypeId = recordId;
		amedObj.Role__c = 'Approved Person';

		amedWrap.amedObj = amedObj;
		//respWrap.newAmendToCreate = amedObj;

		respWrap.newAmendWrappToCreate = amedWrap;

		return respWrap;
	}

	@AuraEnabled
	public static amendWrappList FetchUpdatedAmendListHelper(String srId) {

		amendWrappList respWrap = new amendWrappList();

		String indRecTyp = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Individual');
    String  bodyRecTyp = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Body_Corporate');

		//* v1.0 Merul  26 march 2020  Libary implementation
		Map<Id,AccountContactRelation> mapRelatedAccForAmed = new Map<Id,AccountContactRelation>();
		Map<Id,AccountContactRelation> mapRelatedAccForRelshp = new Map<Id,AccountContactRelation>();
		
		//* v1.0 Merul  26 march 2020  Libary implementation
		HexaBPM__Service_Request__c currentSR = new HexaBPM__Service_Request__c();
                
		// * v1.0 Merul  26 march 2020  Libary implementation
		Map<String,AmendmentWrapper> mapExistingAmed = new  Map<String,AmendmentWrapper>();
		Map<String,AmendmentWrapper> mapShareholder = new  Map<String,AmendmentWrapper>();
		Map<String,AmendmentWrapper> mapRelShpToAmedWrap = new  Map<String,AmendmentWrapper>();
		Map<String,AmendmentWrapper> mapShareholderCombine = new  Map<String,AmendmentWrapper>();

		list<HexaBPM_Amendment__c> libAmendmentList = new list<HexaBPM_Amendment__c>();

		 // getting all related account based on current loggedin user.
		 User loggedInUser = new User();
		 String contactId;

		 for(User UserObj : [
														 SELECT id,
																 contactid,
																 Contact.AccountId,
																 ProfileId 
														 FROM User 
														 WHERE Id = :UserInfo.getUserId() 
														 
														 LIMIT 1
												 ]
				 ) 
		 {
				 loggedInUser =  UserObj;
				 contactId = UserObj.contactid;
		 }

		 
		 // getting account contact relation ship
		 for( 
				 AccountContactRelation accConRel :  [
																								 SELECT Id,
																												 AccountId,
																												 ContactId,
																												 Roles,
																												 Access_Level__c,
																												 Account.ROC_Status__c
																								 FROM AccountContactRelation
																								 WHERE ContactId =:contactId
																								 AND Roles includes ('Company Services')
																				 
																						 ]
		 )
		 {
				 if( accConRel.Account.ROC_Status__c == 'Under Formation' 
										 || accConRel.Account.ROC_Status__c == 'Account Created' )    
										 {
												 mapRelatedAccForAmed.put( accConRel.AccountId , accConRel);
										 }
				 
				 if( accConRel.Account.ROC_Status__c == 'Active' 
										 || accConRel.Account.ROC_Status__c == 'Not Renewed' )    
										 {
												 mapRelatedAccForRelshp.put( accConRel.AccountId , accConRel);
										 }
		 }

		 // getting relationship
		 set<id> addedContacts = new set<id>();
		 set<id> addedAccounts = new set<id>();

		 List<Relationship__c> lstRel = OB_libraryHelper.getRelationShip( mapRelatedAccForRelshp );
		 for(Relationship__c relShip : lstRel)                           
		 {
			system.debug('############## relShip '+relShip);
			AmendmentWrapper amedWrap = new AmendmentWrapper();
			amedWrap.amedObj = new HexaBPM_Amendment__c();
			if(relShip.Amendment_ID__c != null){
				amedWrap.relatedAmendID = relShip.Amendment_ID__c;
			}else{
				amedWrap.relatedAmendID = relShip.Sys_Amendment_Id__c;
			}
			amedWrap.RelatedRelId = relShip.id;

			// amedWrap.lookupLabel = (amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name :'');
			if( relShip.Object_Contact__c != NULL )
			{
					if(!addedContacts.contains(relShip.Object_Contact__c)){

						 addedContacts.add(relShip.Object_Contact__c);
						 amedWrap.amedObj = OB_libraryHelper.setAmedFromObjContact(amedWrap.amedObj,relShip);
						 amedWrap.isIndividual = true;
						 amedWrap.displayName = (String.isNotBlank( amedWrap.amedObj.First_Name__c) ? amedWrap.amedObj.First_Name__c : '') +' '+  (String.isNotBlank( amedWrap.amedObj.Last_Name__c) ? amedWrap.amedObj.Last_Name__c : '');
						 //amedWrap.amedObj.recordTypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Individual');
						 amedWrap.amedObj.recordTypeId = indRecTyp;
						 
						 system.debug('@@@@@@@@@@@   amedWrap.amedObj.recordTypeId  '+ amedWrap.amedObj.recordTypeId );

						 String passport = ( String.isNotBlank( amedWrap.amedObj.Passport_No__c )  ? amedWrap.amedObj.Passport_No__c : '' );
						 String nationality = ( String.isNotBlank( amedWrap.amedObj.Nationality_list__c )  ? amedWrap.amedObj.Nationality_list__c : '' ); 
						 String uniqKey = passport + nationality;
						 if( String.isNotBlank( uniqKey  ) )
						 {
								 mapRelShpToAmedWrap.put( uniqKey.toLowercase() , amedWrap);
						 }
					}
					
																

			}
			else if( relShip.Object_Account__c != NULL )
			{
					
			 if(!addedAccounts.contains(relShip.Object_Account__c)){
				 addedAccounts.add(relShip.Object_Account__c);
				 amedWrap.amedObj = OB_libraryHelper.setAmedFromObjAccount(amedWrap.amedObj,relShip);
				 amedWrap.isBodyCorporate = true;
				 amedWrap.displayName = amedWrap.amedObj.Company_Name__c;
				 //amedWrap.amedObj.recordTypeId = OB_QueryUtilityClass.getRecordtypeID('HexaBPM_Amendment__c','Body_Corporate');
				 amedWrap.amedObj.recordTypeId = bodyRecTyp;

				 if( String.isNotBlank( amedWrap.amedObj.Registration_No__c  ) )
				 {
						 mapRelShpToAmedWrap.put( amedWrap.amedObj.Registration_No__c.toLowercase(), amedWrap);
				 }
			 }
				 
					
			}

	}

	set<string> dupAmendId = new set<string>();
		 List<HexaBPM__Service_Request__c> lstSerReq =   OB_libraryHelper.getSR(srId,mapRelatedAccForAmed);
                    for(HexaBPM__Service_Request__c serObj :  lstSerReq) 
                    {
                            system.debug('===inside service request===');

                            /* if( serObj.Id == srId )
                            { */
                                    
                                    for(HexaBPM_Amendment__c amed :serObj.Amendments__r) 
                                    {

																			String passport = ( String.isNotBlank( amed.Passport_No__c )  ? amed.Passport_No__c : '' );
																			String nationality = ( String.isNotBlank( amed.Nationality_list__c )  ? amed.Nationality_list__c : '' ); 
																			String uniqKey = passport + nationality;

																			if(serObj.Id != srId ){
																				dupAmendId.add(uniqKey);
																			}
																			
																				if(!(serObj.Id != srId && dupAmendId.contains(uniqKey))){

                                        system.debug('===inside amendment===');
                                        AmendmentWrapper amedWrap = new AmendmentWrapper();
																				amedWrap.amedObj = amed;
																				amedWrap.relatedAmendID = amed.Id;
																				if(serObj.Id != srId){
																						amedWrap.amedObj.Id = null;
																				}
                                        
                                        amedWrap.serviceRequest = serObj;
                                        amedWrap.lookupLabel = amed.Entity_Name__r.Name;
                                        amedWrap.isIndividual = (amed.RecordType.DeveloperName == 'Individual' ? true :false);
																				amedWrap.isBodyCorporate = (amed.RecordType.DeveloperName == 'Body_Corporate' ? true :false);
																				amedWrap.displayName = (String.isNotBlank( amedWrap.amedObj.First_Name__c) ? amedWrap.amedObj.First_Name__c : '') +' '+  (String.isNotBlank( amedWrap.amedObj.Last_Name__c) ? amedWrap.amedObj.Last_Name__c : '');
                                        //amedWrap.lookupLabel = (amed.Entity_Name__c != NULL ? amed.Entity_Name__r.Name :'');

																				if(amedWrap.amedObj.Role__c != null && serObj.Id == srId && amedWrap.isIndividual){
																					list<string> amendRole = amedWrap.amedObj.Role__c.split(';');
																					if( amendRole.contains('Approved Person')) {
																						respWrap.AddedAmendList.add(amedWrap);
																					} else if(!amendRole.contains('Approved Person') && amendRole.size() > 0 && !amendRole.contains('DPO') && !amendRole.contains('Operating Location')) {
																						respWrap.AmendListForPicklist.add(amedWrap);
																					}
																				}else if(amedWrap.amedObj.Role__c != null && amedWrap.isIndividual){
																					respWrap.AmendListForPicklist.add(amedWrap);
																				}
                                        
																			}
																				
                                    }
                            /* } */
                    } 

									
		 
										if( mapRelShpToAmedWrap.size() > 0 )
                    {
											respWrap.AmendListForPicklist.addAll( mapRelShpToAmedWrap.values() );
                    }


		return respWrap;

	}

	@AuraEnabled
	public static RespondWrap amendRemoveAction(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		system.debug(reqWrap);

		HexaBPM_Amendment__c amendObjToUpdate = reqWrap.amendObj;

		try {
			
				list<string> amendRoles = amendObjToUpdate.Role__c.split(';');
				if(amendRoles.indexOf('Approved Person') !=-1 && amendRoles.size() == 1) {
					delete amendObjToUpdate;
				}else if(amendRoles.indexOf('Approved Person') !=-1 && amendRoles.size() > 1) {
					amendRoles.remove(amendRoles.indexOf('Approved Person'));
					amendObjToUpdate.Role__c = String.join(amendRoles, ';');
				update amendObjToUpdate;
				}
				

		
			//respWrap.amendWrapp = FetchUpdatedAmendListHelper(reqWrap.srId);
			respWrap.amendWrappList = FetchUpdatedAmendListHelper(reqWrap.srId);
			respWrap.errorMessage = 'no error recorded';
			respWrap.updatedRec = amendObjToUpdate;
		} catch(exception e) {
			respWrap.errorMessage = e.getMessage();
		}


		return respWrap;
	}

	@AuraEnabled
	public static RespondWrap amenSaveAction(String requestWrapParam) {

		//declaration of wrapper
		RequestWrap reqWrap = new RequestWrap();
		RespondWrap respWrap = new RespondWrap();
		boolean isDuplicate = false;

		//deseriliaze.
		reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);
		system.debug(reqWrap);

		HexaBPM_Amendment__c amendObj = reqWrap.amendObj;
		//amendObj.id = reqWrap.amendId;

		amendObj.Id = ( amendObj.ServiceRequest__c != reqWrap.srId ? NULL : amendObj.Id);
		amendObj.ServiceRequest__c = reqWrap.srId;

		String passport = String.isNotBlank(amendObj.Passport_No__c) ? amendObj.Passport_No__c :'';
		String nationality = String.IsNotBlank(amendObj.Nationality_list__c) ? amendObj.Nationality_list__c.toLowerCase() :'';


		for(HexaBPM_Amendment__c amd :[Select ID, First_Name__c, Passport_No__c, Nationality_list__c, 
									   recordtype.developername, Role__c
									   FROM HexaBPM_Amendment__c
									   WHERE ServiceRequest__c = :amendObj.ServiceRequest__c
									   AND (recordtype.developername = 'Individual'
									   AND Id != :amendObj.Id)
									   AND Passport_No__c = :passport
									   AND Nationality_list__c = :nationality
									   Limit 1]) {

			amendObj.Id = amd.Id;
			// For director dup check.
			if(String.IsNotBlank(amd.Role__c) && amd.Role__c.containsIgnorecase('Approved Person')) {

				isDuplicate = true;
				break;
			}

			if(amd.Role__c != NULL
			   && !amd.Role__c.containsIgnorecase('Approved Person')) {
				amendObj.Role__c = amd.Role__c + ';Approved Person';
			} 
			else if(amd.Role__c == NULL) {
				amendObj.Role__c = 'Approved Person';
			}


		}


		if(isDuplicate) {
			respWrap.errorMessage = 'This person already exist';
		} else {



			try {

				upsert amendObj;
				//Insert SR Docs
				if(reqWrap.docMap != null) {

					Map<String, String> docMasterContentDocMap = reqWrap.docMap;
					String srId = reqWrap.srId;
					String amendID = amendObj.Id;


					OB_AmendmentSRDocHelper.RequestWrapper srDocReq = new OB_AmendmentSRDocHelper.RequestWrapper();
					OB_AmendmentSRDocHelper.ResponseWrapper srDocResp = new OB_AmendmentSRDocHelper.ResponseWrapper();
					srDocReq.amedId = amendObj.Id;
					srDocReq.srId = srId;
					srDocReq.docMasterContentDocMap = docMasterContentDocMap;

					srDocResp = OB_AmendmentSRDocHelper.reparentSRDocsToAmendment(srDocReq);
					//OB_ManageApprovedPersonController.saveSRDoc(reqWrap.srId, amendObj.Id, reqWrap.docMap);
				}
				//respWrap.amendWrapp = FetchUpdatedAmendListHelper(reqWrap.srId);
				respWrap.amendWrappList = FetchUpdatedAmendListHelper(reqWrap.srId);
				respWrap.errorMessage = 'no error recorded';
				respWrap.updatedRec = amendObj;
			} catch(DMLException e) {
				string DMLError = e.getdmlMessage(0) + '';
				if(DMLError == null) {
					DMLError = e.getMessage() + '';
				}
				respWrap.errorMessage = DMLError;
			}
		}
		return respWrap;
	}

	

	public class RequestWrap {
		@AuraEnabled
		public String flowId { get; set; }

		@AuraEnabled
		public String pageId { get; set; }

		@AuraEnabled
		public String srId { get; set; }

		@AuraEnabled
		public String amendId { get; set; }

		@AuraEnabled
		public HexaBPM_Amendment__c amendObj { get; set; }

		@AuraEnabled
		public Boolean isAssigned { get; set; }

		@AuraEnabled
		public map<string, string> docMap { get; set; }


		public RequestWrap() {
		}
	}

	public class RespondWrap {

		@AuraEnabled
		public amendListWrapp amendWrapp { get; set; }

		@AuraEnabled
		public amendWrappList amendWrappList { get; set; }

		@AuraEnabled
		public string errorMessage { get; set; }

		@AuraEnabled
		public string recordId { get; set; }

		@AuraEnabled
		public HexaBPM_Amendment__c updatedRec { get; set; }

		@AuraEnabled
		public HexaBPM_Amendment__c newAmendToCreate { get; set; }

		@AuraEnabled
		public AmendmentWrapper newAmendWrappToCreate { get; set; }

		@AuraEnabled
		public HexaBPM__Section_Detail__c ButtonSection { get; set; }

		@AuraEnabled public SRWrapper srWrap { get; set; }

		public RespondWrap() {
			AmendWrapp = new amendListWrapp();
			amendWrappList = new amendWrappList();
		}
	}
	public class AmendmentWrapper {
		@AuraEnabled public HexaBPM_Amendment__c amedObj { get; set; }
	 
		@AuraEnabled public boolean hasOtherRoles {get;set;}
		@AuraEnabled public map<string,string> declaration {get;set;}
		@AuraEnabled public HexaBPM__Service_Request__c serviceRequest {get;set;}

		@AuraEnabled public Boolean isIndividual { get; set; }
		@AuraEnabled public Boolean isBodyCorporate { get; set; }
		@AuraEnabled public String lookupLabel { get; set; }
		@AuraEnabled public String displayName { get; set; }
		@AuraEnabled public Boolean disableAllFlds { get; set; }
		@AuraEnabled public String relatedAmendID { get; set; }
		@AuraEnabled public String relatedRelId { get; set; }


		public AmendmentWrapper() {
				hasOtherRoles = false;
				serviceRequest = new HexaBPM__Service_Request__c();
				disableAllFlds = true;
		}
}

public class amendWrappList {

		@AuraEnabled
		public list<AmendmentWrapper> AmendListForPicklist { get; set; }

		@AuraEnabled
		public list<AmendmentWrapper> AddedAmendList { get; set; }


		@AuraEnabled
		public object debugger ;


		public amendWrappList() {

			AmendListForPicklist = new list<AmendmentWrapper>();
			AddedAmendList = new list<AmendmentWrapper>();

		}
	}

	public class amendListWrapp {

		@AuraEnabled
		public list<HexaBPM_Amendment__c> otherRolesAmendList { get; set; }

		@AuraEnabled
		public list<HexaBPM_Amendment__c> currentRoleAndOtherAmendList { get; set; }

		@AuraEnabled
		public list<HexaBPM_Amendment__c> onlyCurrentRoleAmendList { get; set; }


		public amendListWrapp() {

			otherRolesAmendList = new list<HexaBPM_Amendment__c>();
			currentRoleAndOtherAmendList = new list<HexaBPM_Amendment__c>();
			onlyCurrentRoleAmendList = new list<HexaBPM_Amendment__c>();

		}
	}

	// dynamic button section
	@AuraEnabled
	public static ButtonResponseWrapper getButtonAction(string SRID, string ButtonId, string pageId) {


		ButtonResponseWrapper respWrap = new ButtonResponseWrapper();
		HexaBPM__Service_Request__c objRequest = OB_QueryUtilityClass.QueryFullSR(SRID);
		PageFlowControllerHelper.objSR = objRequest;
		PageFlowControllerHelper objPB = new PageFlowControllerHelper();

		respWrap.tester = OB_QueryUtilityClass.minRecValidationCheck(SRID, 'Approved Person');
		objRequest = OB_QueryUtilityClass.setPageTracker(objRequest, SRID, pageId);
		upsert objRequest;

		OB_RiskMatrixHelper.CalculateRisk(SRID);

		PageFlowControllerHelper.responseWrapper responseNextPage = objPB.getLightningButtonAction(ButtonId);
		system.debug('@@@@@@@@2 responseNextPage ' + responseNextPage);
		respWrap.pageActionName = responseNextPage.pg;
		respWrap.communityName = responseNextPage.communityName;
		respWrap.CommunityPageName = responseNextPage.CommunityPageName;
		respWrap.sitePageName = responseNextPage.sitePageName;
		respWrap.strBaseUrl = responseNextPage.strBaseUrl;
		respWrap.srId = objRequest.Id;
		respWrap.flowId = responseNextPage.flowId;
		respWrap.pageId = responseNextPage.pageId;
		respWrap.isPublicSite = responseNextPage.isPublicSite;

		system.debug('@@@@@@@@2 respWrap.pageActionName ' + respWrap.pageActionName);
		return respWrap;
	}
	public class ButtonResponseWrapper {
		@AuraEnabled public String pageActionName { get; set; }
		@AuraEnabled public string communityName { get; set; }
		@AuraEnabled public String errorMessage { get; set; }
		@AuraEnabled public string CommunityPageName { get; set; }
		@AuraEnabled public string sitePageName { get; set; }
		@AuraEnabled public string strBaseUrl { get; set; }
		@AuraEnabled public string srId { get; set; }
		@AuraEnabled public string flowId { get; set; }
		@AuraEnabled public string pageId { get; set; }
		@AuraEnabled public boolean isPublicSite { get; set; }
		@AuraEnabled public boolean tester { get; set; }

	}
	public class SRWrapper {
		@AuraEnabled public HexaBPM__Service_Request__c srObj { get; set; }

		@AuraEnabled public String declarationIndividualText { get; set; }
		@AuraEnabled public String declarationCorporateText { get; set; }

		@AuraEnabled public String viewSRURL { get; set; }
		@AuraEnabled public Boolean isDraft { get; set; }


		public SRWrapper() {
		}
	}


}