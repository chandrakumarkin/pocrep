public class cls_OpportunityUtil {
    @InvocableMethod(label='Send In-Principal Email' description='Send In-Principal email to multiple addresses')   
        public static void SendInPrincipalEmailOpportunity(List<Id> oppIds){
        if(oppIds.size()>0){
            try{
                
                Map<string,In_Principal_BD__c>  inPrincipalCustomSettings = In_Principal_BD__c.getAll();   
                list<Opportunity> listOpp = [SELECT Id,Name,Company_Type__c,Lead_Email__c,CC_Emails__c,Owner.Email FROM Opportunity where id = :oppIds];     
                OrgWideEmailAddress orgWideNoReply = new OrgWideEmailAddress();
                for(OrgWideEmailAddress owa : [select id, Address from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae']) {
                    orgWideNoReply = owa;
                } 
    
                if(listOpp != null && listOpp.size() > 0 && inPrincipalCustomSettings != null && !inPrincipalCustomSettings.isEmpty()){
                    
                    for(Opportunity opp : listOpp){
                        //Variable Initializations
                        string emailTemplateName = '';               
                        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
                        list<string> lstCcEmails = new list<string>();
                        
                        In_Principal_BD__c selectedCompanyType = inPrincipalCustomSettings.get(opp.Company_Type__c);
                        if(selectedCompanyType!=null){
                            emailTemplateName = selectedCompanyType.Email_Template_Name__c;
                            if(string.isNotBlank(selectedCompanyType.CC_Email__c))
                                lstCcEmails.addAll(selectedCompanyType.CC_Email__c.split(','));
                        }

                        system.debug('emailTemplateName'+emailTemplateName);       
                        EmailTemplate template = [Select id,Name,DeveloperName from EmailTemplate where DeveloperName =:emailTemplateName ];  
                        if(template!=null){
                        
                            if(string.isNotBlank(opp.CC_Emails__c))
                                lstCcEmails.addAll(opp.CC_Emails__c.split(','));

                            if(string.isNotBlank(opp.Owner.Email))
                                lstCcEmails.add(opp.Owner.Email);
                            
                            system.debug('lstCcEmails ' +lstCcEmails);  
                            list<Lead> lstLead = [Select Id,ConvertedContactId from Lead where ConvertedOpportunityId=: opp.Id];        

                            if(lstLead != null && lstLead.size()>0){
                           //  if(!Test.isRunningTest()){
                                 email.setTargetObjectId(lstLead[0].ConvertedContactId);   
                           //  }                          
                            }
                             
                             email.setWhatId(opp.Id);  
                             email.setToAddresses(new String[] {opp.Lead_Email__c});
                             if(lstCcEmails.size()>0)
                                email.setCcAddresses(lstCcEmails);
                             email.setTemplateId(template.id);
                             email.setSaveAsActivity(true);
                             email.setOrgWideEmailAddressId(orgWideNoReply.id);

                          //   if(!Test.isRunningTest()){
                                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {Email});
                           //  }
                         }
                    }  // end of for loop for each Opportunity
                }
            
            }catch (Exception e){
                insert LogDetails.CreateLog(null, 'cls_OpportunityUtil/SendInPrincipalEmailOpportunity', 'Opp Id is ' + oppIds + 'Line # '+e.getLineNumber()+'\n'+e.getMessage());
            }           
         
        } // end of main If
    }   
}