@isTest
public class OB_TestDataFactory {
    static Map<String, Id> srRecordTypes = new Map<String, Id>();
    //request wrpper
    public static OB_TestDataFactory.RequestWrapper reqWrap;
    //response wrpper
    public static OB_TestDataFactory.ResponseWrapper respWrap;
    
    /*
    static ResponseWrapper createAccount(RequestWrapper reqWrapPram){
        respWrap = new OB_TestDataFactory.ResponseWrapper();
        Account acc = new Account();
        acc.name =  reqWrapPram.AccountName; 
        respWrap.acc =  acc;       
        return respWrap;
    }
    
    static ResponseWrapper createContact(RequestWrapper reqWrapPram){
        respWrap = new OB_TestDataFactory.ResponseWrapper();
        Contact con = new Contact();
        con.lastname =  reqWrapPram.ContactlastName; 
        
        respWrap.con = con;
        
        return respWrap;
    }
    
    static ResponseWrapper createOpportunity(RequestWrapper reqWrapPram){
        respWrap = new OB_TestDataFactory.ResponseWrapper();
        Opportunity opp = new Opportunity();
        opp.name =  reqWrapPram.opportuntiyName; 
        respWrap.opp = opp;
        return respWrap;
    }
    
    static ResponseWrapper createLead(RequestWrapper reqWrapPram){
        respWrap = new OB_TestDataFactory.ResponseWrapper();
        Lead leadObj = new Lead();
        leadObj.lastname =  reqWrapPram.leadLastName; 
        leadObj.company= reqWrapPram.leadCompany;
        leadObj.email= reqWrapPram.leadEmail;
        leadObj.Status= reqWrapPram.leadStatus;
        respWrap.leadObj = leadObj;
        return respWrap;
    }
    
    static ResponseWrapper createServiceRequest(RequestWrapper reqWrapPram){
        respWrap = new OB_TestDataFactory.ResponseWrapper();
        HexaBPM__Service_Request__c serviceObj = new HexaBPM__Service_Request__c();
        
        respWrap.serviceObj = serviceObj;
        return respWrap;
    }
    
    static ResponseWrapper createAmendment(RequestWrapper reqWrapPram){
        respWrap = new OB_TestDataFactory.ResponseWrapper();
        HexaBPM_Amendment__c amendmentObj = new HexaBPM_Amendment__c();
        
        respWrap.amendmentObj = amendmentObj;
        return respWrap;
    }
    
    static ResponseWrapper createLicense(RequestWrapper reqWrapPram){
        respWrap = new OB_TestDataFactory.ResponseWrapper();
        License__c licenseObj = new License__c();
        
        respWrap.licenseObj = licenseObj;
        return respWrap;
    }
    
    static ResponseWrapper createLicenseActivityMaster(RequestWrapper reqWrapPram){
        respWrap = new OB_TestDataFactory.ResponseWrapper();
        License_Activity_Master__c licenseActivityObj = new License_Activity_Master__c();
        
        respWrap.licenseActivityObj = licenseActivityObj;
        return respWrap;
    }
	*/
    //create lead
    public static List<Lead> createLead(Integer noOfRecord){
        List<Lead> insertNewLeads = new List<Lead>();
        for(Integer i=1;i<=noOfRecord;i++){
            Lead leadObj = new Lead();
            leadObj.lastname =  'TestLead'+i;
            leadObj.company= 'TestLeadComp'+i;
            leadObj.email= 'testemail'+i+'@test.com';
            leadObj.Status= 'Open';
            insertNewLeads.add(leadObj);
        }
        return insertNewLeads;
    }
    
    //create License Activity Master
    public static List<License_Activity_Master__c> createLicenseActivityMaster(Integer noOfRecord, List<string> listActivityType, List<String> listType){
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        for(Integer i=1;i<=noOfRecord;i++){
            License_Activity_Master__c objLicenseActivityMaster = new License_Activity_Master__c();
            objLicenseActivityMaster.Name =  'Test Lead Master'+i;
            objLicenseActivityMaster.Sys_Activity_Type__c = listActivityType.size() >= i ? listActivityType[i-1] : listActivityType[0]  ;
            objLicenseActivityMaster.Type__c = listType.size() >= i ? listType[i-1] : listType[0]  ;
            listLicenseActivityMaster.add(objLicenseActivityMaster);
        }
        return listLicenseActivityMaster;
    }
    
    //create lead activity
    public static List<Lead_Activity__c> createLeadActivity(Integer noOfRecord, List<Lead> listLeads, List<License_Activity_Master__c> listLicenseActivityMaster){
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        for(Integer i=1;i<=noOfRecord;i++){
            Lead_Activity__c objLeadActivity = new Lead_Activity__c();
            objLeadActivity.Lead__c =  listLeads.size() >= i ? listLeads[i-1].id : listLeads[0].id  ;
            objLeadActivity.License_Activity_Master__c= listLicenseActivityMaster.size() >= i ? listLicenseActivityMaster[i-1].id : listLicenseActivityMaster[0].id  ;
            listLeadActivity.add(objLeadActivity);
        }
        return listLeadActivity;
    }
	//create Account
    public static List<Account> createAccounts(Integer noOfRecord){
        List<Account> insertNewAccounts = new List<Account>();
        for(Integer i=1;i<=noOfRecord;i++){
            Account newAccount = new Account();
            newAccount.Name = 'TestAccount'+i;            
            insertNewAccounts.add(newAccount);
        }
        
        return insertNewAccounts;
    }    
    
    //create Contact
    public static List<Contact> createContacts(Integer ctr, List<Account> listAccount){
        List<Contact> insertNewContacts = new List<Contact>();
        for(Integer i=1;i<=ctr;i++){
            Contact newContact = new Contact();
            newContact.AccountId = listAccount.size() >= i ? listAccount[i-1].id : listAccount[0].id  ;
            newContact.FirstName = 'FirstName'+i;
            newContact.LastName = 'LastName'+i;
            newContact.Email = 'testemail'+i+'@test.com';
            
            insertNewContacts.add(newContact);
        }
        
        return insertNewContacts;
    }
    
    //create Srtemplate
    public static List<HexaBPM__SR_Template__c> createSRTemplate(Integer noOfRecord, List<string> listRecordTypeName){
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        for(Integer i=1;i<=noOfRecord;i++){
            //TODO: populate required fields values
            HexaBPM__SR_Template__c SRTemplateDetailRecord = new HexaBPM__SR_Template__c();
            SRTemplateDetailRecord.HexaBPM__Active__c = true;
            SRTemplateDetailRecord.HexaBPM__SR_RecordType_API_Name__c = listRecordTypeName.size() >= i ? listRecordTypeName[i-1] : listRecordTypeName[0]  ;
            createSRTemplateList.add(SRTemplateDetailRecord);
        }
        
        system.debug('#### SrTemplate Record Created = '+createSRTemplateList);
        return createSRTemplateList;   
    }
    // create step template
    public static List<HexaBPM__Step_Template__c> createStepTemplate(Integer noOfRecord, 
                                                                     List<String> liststepNames, 
                                                                     List<String> listCodes, 
                                                                     List<String> listRecType, 
                                                                     List<String> listSummary)
    {
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        HexaBPM__Step_Template__c objStepTemplate ;
        for(Integer i=1;i<=noOfRecord;i++){ 
            objStepTemplate = new HexaBPM__Step_Template__c();
            objStepTemplate.Name = liststepNames.size() >= i ? liststepNames[i-1] : liststepNames[0];
            objStepTemplate.HexaBPM__Code__c = listCodes.size() >= i ? listCodes[i-1] : listCodes[0];
            objStepTemplate.HexaBPM__Step_RecordType_API_Name__c = listRecType.size() >= i ? listRecType[i-1] : listRecType[0];
            objStepTemplate.HexaBPM__Summary__c = listSummary.size() >= i ? listSummary[i-1] : listSummary[0];
            
            listStepTemplate.add(objStepTemplate);
        }
        return listStepTemplate;
    }
    // create SR step
    public static List<HexaBPM__SR_Steps__c> createSRStep(Integer noOfRecord, List<HexaBPM__SR_Template__c> listSRTemplate, List<HexaBPM__Step_Template__c> listStepTemp, 
                                                             List<HexaBPM__Status__c> listStatus ){
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        HexaBPM__SR_Steps__c objSRSteps;
        for(Integer i=1;i<=noOfRecord;i++){ 
        	objSRSteps = new HexaBPM__SR_Steps__c();
            objSRSteps.HexaBPM__SR_Template__c = listSRTemplate.size() >= i ? listSRTemplate[i-1].id : listSRTemplate[0].id;
            objSRSteps.HexaBPM__Step_Template__c = listStepTemp.size() >= i ? listStepTemp[i-1].id : listStepTemp[0].id;
            //objSRSteps.HexaBPM__Start_Status__c = listStatus.size() >= i ? listStatus[i-1].id : listStatus[0].id;
            objSRSteps.HexaBPM__Active__c=true;
            
            listSRSteps.add(objSRSteps);
        }
        return listSRSteps;
    }
    
    //create Step records
    public static List<HexaBPM__Step__c> createSteps(Integer ctr, List<HexaBPM__Service_Request__c> listServiceRequest, List<HexaBPM__SR_Steps__c> listSRStep){
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
        for(Integer i=1;i<=ctr;i++){
            HexaBPM__Step__c newStep = new HexaBPM__Step__c();
            newStep.HexaBPM__SR__c = listServiceRequest.size() >= i ? listServiceRequest[i-1].id : listServiceRequest[0].id;
            newStep.HexaBPM__Due_Date__c = System.Now().addDays(2);
            newStep.HexaBPM__Step_No__c = i * 10;
            newStep.HexaBPM__SR_Step__c = listSRStep.size() >= i ? listSRStep[i-1].id : listSRStep[0].id;
            
            newStep.HexaBPM__Step_Notes__c = 'test';
            insertNewSteps.add(newStep);
        }
        
        return insertNewSteps;
    }
    //create a map for SR recordtypes
    public static void srRecordtypesMap(){
        for(Recordtype rt : [Select Id, DeveloperName FROM RecordType 
                            WHERE SObjectType = 'HexaBPM__Service_Request__c']){
            srRecordTypes.put(rt.DeveloperName, rt.Id);
        }
    }
    
    //create Service Request records
    public static List<HexaBPM__Service_Request__c> createSR(Integer ctr, List<string> listRecordtypeName, List<Account> listAccount, List<string> listEntityType,List<string> listBusiness_Sector, List<string> listLegalStructures, List<string> listTypeofEntity){
        srRecordtypesMap();       
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        for(Integer i=1;i <= ctr;i++){
            HexaBPM__Service_Request__c newSR = new HexaBPM__Service_Request__c();
            newSR.RecordtypeId = listRecordtypeName.size() >= i ? srRecordTypes.get(listRecordtypeName[i-1]) : srRecordTypes.get(listRecordtypeName[0]) ;  
            newSR.Last_Name__c = 'LastName'+i;
            newSR.HexaBPM__Customer__c = listAccount.size() >= i ? listAccount[i-1].id : listAccount[0].id  ;
            newSR.Entity_Type__c =  listEntityType.size() >= i ? listEntityType[i-1] : listEntityType[0];
            newSR.Business_Sector__c =  listBusiness_Sector.size() >= i ? listBusiness_Sector[i-1] : listBusiness_Sector[0];
            newSR.legal_structures__c =  listLegalStructures.size() >= i ? listLegalStructures[i-1] : listLegalStructures[0];
            newSR.Type_of_Entity__c =  listTypeofEntity.size() >= i ? listTypeofEntity[i-1] : listTypeofEntity[0];
            insertNewSRs.add(newSR);
        }
        
        return insertNewSRs;
    }
    
    //Create SR status
    public static list<HexaBPM__SR_Status__c> createSRStatus(Integer noOfRecord, List<string> listNames, List<String> listCodes, List<String> listTypes) {
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        HexaBPM__SR_Status__c objSRStatus;
        for(Integer i=1; i <= noOfRecord ; i++ ) {
            objSRStatus = new HexaBPM__SR_Status__c();
            objSRStatus.Name = listNames.size() >= i ? listNames[i-1] : listNames[0]  ;
            objSRStatus.HexaBPM__Code__c = listCodes.size() >= i ? listCodes[i-1] : listCodes[0]  ;
            objSRStatus.HexaBPM__Type__c = listTypes.size() >= i ? listTypes[i-1] : listTypes[0]  ;
            listSRStatus.add(objSRStatus);
        }
        return listSRStatus;
    }
    //create CountryRiskScore
    public static list<Country_Risk_Scoring__c> createCountryRiskScorings (Integer noOfRecord, List<string> riskRatings, List<Integer> riskPercentage, List<string> riskIncomes) {
        List<Country_Risk_Scoring__c> listCountryRiskScoring = new List<Country_Risk_Scoring__c>();
        Country_Risk_Scoring__c objCountryRiskScoring;
        for(Integer i=1; i <= noOfRecord ; i++ ) {
            objCountryRiskScoring = new Country_Risk_Scoring__c();
            objCountryRiskScoring.Risk_Rating__c = riskRatings.size() >= i ? riskRatings[i-1] : riskRatings[0]  ;
            objCountryRiskScoring.Risk_Score_Percentage__c = riskPercentage.size() >= i ? riskPercentage[i-1] : riskPercentage[0]  ;
            objCountryRiskScoring.Source_of_Income_Risk_Rating__c = riskIncomes.size() >= i ? riskIncomes[i-1] : riskIncomes[0]  ;
            listCountryRiskScoring.add(objCountryRiskScoring);
        }
        return listCountryRiskScoring;
    }
    // create document Master
    public static list<HexaBPM__Document_Master__c> createDocMaster (Integer noOfRecord, List<string> listDocCode) {
        List<HexaBPM__Document_Master__c> listDocMaster = new List<HexaBPM__Document_Master__c>();
        HexaBPM__Document_Master__c objDocMaster;
        for(Integer i=1; i <= noOfRecord ; i++ ) {
            objDocMaster = new HexaBPM__Document_Master__c();
            objDocMaster.HexaBPM__Code__c = listDocCode.size() >= i ? listDocCode[i-1] : listDocCode[0]  ;
            objDocMaster.HexaBPM__Available_to_client__c = true;
            listDocMaster.add(objDocMaster);
        }
        return listDocMaster;
    }
    
    // create document Master
    public static list<HexaBPM__SR_Template_Docs__c> createSR_Template_Docs(Integer noOfRecord, List<HexaBPM__Document_Master__c> listDocMaster) {
        List<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc = new List<HexaBPM__SR_Template_Docs__c>();
        HexaBPM__SR_Template_Docs__c objSRTemplateDoc ;
        for(Integer i=1; i <= noOfRecord ; i++ ) {
            objSRTemplateDoc = new HexaBPM__SR_Template_Docs__c();
            objSRTemplateDoc.HexaBPM__Document_Master__c = listDocMaster.size() >= i ? listDocMaster[i-1].id : listDocMaster[0].id  ;
            listSRTemplateDoc.add(objSRTemplateDoc);
        }
        return listSRTemplateDoc;
    }
    
    // create SR Doc
    public static list<HexaBPM__SR_Doc__c> createSRDoc(Integer noOfRecord, List<HexaBPM__Service_Request__c> listSR, List<HexaBPM__Document_Master__c> listDocMaster, list<HexaBPM__SR_Template_Docs__c> listSRTemplateDoc) {
        List<HexaBPM__SR_Doc__c> listSRDoc = new List<HexaBPM__SR_Doc__c>();
        HexaBPM__SR_Doc__c objSRDoc ;
        for(Integer i=1; i <= noOfRecord ; i++ ) {
            objSRDoc = new HexaBPM__SR_Doc__c();
            //objSRTemplateDoc.HexaBPM__Code__c = listDocCode.size() >= i ? listDocCode[i-1] : listDocCode[0]  ;
            objSRDoc.HexaBPM__Document_Master__c = listDocMaster.size() >= i ? listDocMaster[i-1].id : listDocMaster[0].id  ;
            objSRDoc.HexaBPM__SR_Template_Doc__c = listSRTemplateDoc.size() >= i ? listSRTemplateDoc[i-1].id : listSRTemplateDoc[0].id  ;
            objSRDoc.HexaBPM__Service_Request__c = listSR.size() >= i ? listSR[i-1].id : listSR[0].id  ;
            listSRDoc.add(objSRDoc);
        }
        return listSRDoc;
    }
    
    // create application amendment
    public static list<HexaBPM_Amendment__c> createApplicationAmendment (Integer noOfRecord, List<Country_Risk_Scoring__c> listCountryRiskScoring, List<HexaBPM__Service_Request__c> listSR) {
        List<HexaBPM_Amendment__c> listAmendment = new List<HexaBPM_Amendment__c>();
        HexaBPM_Amendment__c objAmendment;
        for(Integer i=1; i <= noOfRecord ; i++ ) {
            objAmendment = new HexaBPM_Amendment__c();
            objAmendment.Nationality_list__c = 'India';
            objAmendment.Permanent_Native_Country__c = 'United Arab Emirates';
            objAmendment.Role__c = 'Shareholder';
            objAmendment.Country_Risk_Scoring__c = listCountryRiskScoring.size() >= i ? listCountryRiskScoring[i-1].id : listCountryRiskScoring[0].id  ;
            system.debug('SR Size--->'+listSR.size()+'--->'+i);
            objAmendment.ServiceRequest__c = listSR.size() >= i ? listSR[i-1].id : listSR[0].id  ;
            listAmendment.add(objAmendment);
        }
        return listAmendment;
    }
    //create page flow
    public static  List<HexaBPM__Page_Flow__c>  createPageFlow(String RTApiName, Integer countVal) {
        List<HexaBPM__Page_Flow__c> PF_List = new List<HexaBPM__Page_Flow__c>();

        HexaBPM__Page_Flow__c pf;
        for (Integer i = 0; i < countVal; i++) {
            pf = new HexaBPM__Page_Flow__c();
            pf.Name = 'New Page Flow :' + i;
            pf.HexaBPM__Flow_Description__c   = pf.Name + '_Description';
            pf.HexaBPM__Master_Object__c  = 'HexaBPM__Service_Request__c';
            pf.HexaBPM__Record_Type_API_Name__c   = RTApiName;
            PF_List.add(pf);
        }
        
        return PF_List;

    }
    //create pages
    public static List<HexaBPM__Page__c> createPageRecords(List<HexaBPM__Page_Flow__c> pageFlow_List) {
        List<HexaBPM__Page__c> Page_List = new List<HexaBPM__Page__c>();
        //For every flow
        for (Integer i = 0 ; i < pageFlow_List.Size(); i++) {
            HexaBPM__Page__c page;
            //Create 5 Pages
            for (Integer j = 0; j < 5; j++) {
                page = new HexaBPM__Page__c();
                page.HexaBPM__No_Quick_navigation__c = true;
                page.HexaBPM__Is_Custom_Component__c = false;
                page.HexaBPM__Page_Description__c = 'Test Page Description';
                page.HexaBPM__Page_Flow__c = pageFlow_List[i].id;
                page.HexaBPM__Page_Order__c = j++;
                page.HexaBPM__Render_By_Default__c = true;
                page.HexaBPM__VF_Page_API_Name__c = 'PageFlow';
                page.HexaBPM__What_Id__c = '';
                
                page.Completion_Weightage__c = 10;
                Page_List.add(page);
            }
        }
        

        return Page_List;
    }
    
    // Cretae Pricing
    public static HexaBPM__Pricing_Line__c createSRPriceItem(){
        HexaBPM__Pricing_Line__c pricing = new HexaBPM__Pricing_Line__c();
        pricing.HexaBPM__Priority__c = 1;
        pricing.Non_Deductible__c = false;
        return pricing;
    }
	
    // Create Product
    public static List<Product2> createProduct(List<String> productCodes){
        List<Product2> products = new List<Product2>();
        for(String eachCode : productCodes)
        {
            Product2 pro2 = new Product2(Name='BXCD',ProductCode=eachCode, isActive=true);
            products.add(pro2);
        }
        return products;
    }
        
    
	// Create HexaBPM__SR_Price_Item__c    
    public static List<HexaBPM__SR_Price_Item__c> createSRPriceItem(List<Product2> products,String srId, String pricingId) {
        List<HexaBPM__SR_Price_Item__c> listSRPriceItem = new List<HexaBPM__SR_Price_Item__c>();
        for(Product2 prod : products){
            HexaBPM__SR_Price_Item__c createdSRPriceItem = new HexaBPM__SR_Price_Item__c();
            createdSRPriceItem.HexaBPM__ServiceRequest__c = srId;
            createdSRPriceItem.HexaBPM__Price__c = 40;
            createdSRPriceItem.HexaBPM__Pricing_Line__c = pricingId;
            createdSRPriceItem.HexaBPM__Product__c = prod.id;
            createdSRPriceItem.HexaBPM__Status__c = 'Added';
            listSRPriceItem.add(createdSRPriceItem);        
        }
        return listSRPriceItem;
    }
    
    // Create HexaBPM__Section__c
    public static List<HexaBPM__Section__c> createSection(Integer nos, String pgId) {
        List<HexaBPM__Section__c> sections = new List<HexaBPM__Section__c>();
        for(Integer i = 0; i <nos; i++){
            HexaBPM__Section__c section = new HexaBPM__Section__c(HexaBPM__Page__c = pgId);
            sections.add(section);
        }
        return sections;
    }
    
    // Create HexaBPM__Section_Detail__c
    public static List<HexaBPM__Section_Detail__c> createSectionDetails(Integer nos, String secId) {
        List<HexaBPM__Section_Detail__c> sectionDetails = new List<HexaBPM__Section_Detail__c>();
        for(Integer i = 0; i <nos; i++){
            HexaBPM__Section_Detail__c sectionDetail = new HexaBPM__Section_Detail__c(HexaBPM__Section__c = secId);
            sectionDetails.add(sectionDetail);
        }
        return sectionDetails;
    }
    
    // create Company_Name__c
    public static List<Company_Name__c> createCompanyName(List<String> names){
        List<Company_Name__c> companies = new List<Company_Name__c>();
        for(String eachString : names){
            Company_Name__c company = new Company_Name__c(Trading_Name__c = eachString, Status__c = 'Draft');
            companies.add(company);
        }
        return companies;
    }
    
    public class RequestWrapper {
        public String AccountName { get; set; }
        public String ContactlastName { get; set; }
        public String opportuntiyName { get; set; }
        public String leadLastName { get; set; }
        public String leadCompany { get; set; }
        public String leadEmail { get; set; }
        public String leadStatus { get; set; }
        
        public RequestWrapper() {
        }
    }
    
    
    public class ResponseWrapper {
        
        public String errorMessage { get; set; }
        public Account acc { get; set; }
        public Contact con { get; set; }
        public Opportunity opp { get; set; }
        public Lead leadObj { get; set; }
        public HexaBPM__Service_Request__c serviceObj { get; set; }
        public HexaBPM_Amendment__c amendmentObj { get; set; }
        public License__c licenseObj { get; set; }
        public License_Activity_Master__c licenseActivityObj { get; set; }
        public ResponseWrapper() {
        }
    }
    
    
}