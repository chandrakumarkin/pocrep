@isTest
public class EmployeeComponentController_Test {
    
    public static testMethod void testgetEmployeeList(){
     
        Account objAccount          = new Account();
        objAccount.Name             = 'Test Custoer 1';
        objAccount.E_mail__c        = 'test@test.com';
        objAccount.BP_No__c         = '001234';
        objAccount.Company_Type__c  ='Financial - related';
        objAccount.Sector_Classification__c     = 'Authorised Market Institution';
        objAccount.Legal_Type_of_Entity__c      = 'LTD';
        objAccount.ROC_Status__c                = 'Active';
        objAccount.Financial_Year_End__c        = '31st December';
        insert objAccount;
        
        License__c objLic = new License__c();
        objLic.License_Issue_Date__c    = system.today();
        objLic.License_Expiry_Date__c   = system.today().addDays(365);
        objLic.Status__c                = 'Active';
        objLic.Account__c               = objAccount.id;
        insert objLic;
        
        objAccount.Active_License__c = objLic.id;
        update objAccount;
        Id portalUserId                 = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('Portal User').getRecordTypeId();
        Contact objContact              = new Contact();
        objContact.firstname            = 'Test Contact';
        objContact.lastname             = 'Test Contact1';
        objContact.accountId            = objAccount.id;
        objContact.recordTypeId         = portalUserId;
        objContact.Email                = 'test@difcportal.com';
        objContact.BP_No__c             = 'Test111';
        objContact.Is_Absconder__c      = false;
        objContact.Passport_No__c       = '1234567';
        insert objContact;
        
        Profile objProfile = [SELECT Id FROM Profile WHERE Name='DIFC Customer Community User Custom'];
        
        User objUser    = new User(Alias = 'tstusr', Email='testuser@difcportal.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = objProfile.Id,
                           ContactId=objContact.Id,Community_User_Role__c='Company Services',
                           TimeZoneSidKey='America/Los_Angeles', UserName='newuser@testorg.com');
        insert objUser;
        
        Relationship__c thisRelatioShip = new Relationship__c();
        thisRelatioShip.Object_Contact__c = objContact.Id;
        thisRelatioShip.Subject_Account__c = objAccount.id;
        thisRelatioShip.Relationship_Type__c = 'Has DIFC Sponsored Employee';
        thisRelatioShip.End_Date__c = System.today().adddays(30);
        thisRelatioShip.Relationship_Group__c ='GS';
        thisRelatioShip.Active__c = true;
        insert thisRelatioShip; 
        
        SR_Status__c thisStatus = new SR_Status__c();
        thisStatus.Code__c = 'SUBMITTED';
        insert thisStatus;
            
        Test.startTest(); 
     
            EmployeeComponentController.thisWrapper('active','',1,'Object_Contact__r.FirstName',false);
            EmployeeComponentController.thisWrapper('cancelled','',1,'Object_Contact__r.FirstName',false);
            EmployeeComponentController.thisWrapper('','tes',1,'Object_Contact__r.FirstName',true);
            EmployeeComponentController.getJobTitle('soft');
            String testStr ='[{"FirstName":"Ahmed","LastName":"Al Janahi","mobile":"+971569803616","email":"test@difctest.com.invalid.invalid","desg":"MANAGER","exp":"yes","bpNumber":"2121117"}]';
            String newtestStr ='[{"parentId":"1234567","fileName":"Al Janahi","base64Data":"+971569803616","contentType":".png","experience":"3","linkedUrl":"2","bpNumber":"2121117"}]';
            EmployeeComponentController.saveSR(testStr,newtestStr);
            String newJson = '[{"selectedField": "" ,"operator": "LIKE","value": "","disabled": "false"}]';
            
            ViewCancelledEmployeeController.getWrapperList('Te', 1,  newJson);     
        Test.stopTest();
         
    }
 
    public static testMethod void testMethod2(){
 
     Account objAccount = new Account(Name = 'Test Custoer 1',E_mail__c = 'test@test.com',BP_No__c = '001234');
        insert objAccount;
        
        Status__c  ss = new Status__c ();
        ss.name ='Completed';
        ss.code__c = 'COMPLETED';
        insert ss;
        
        Account objAccountOld = new Account();
        objAccountOld.Name = 'Test Custoer 1';
        objAccountOld.E_mail__c = 'test@test.com';
        objAccountOld.BP_No__c = '001235';
        insert objAccountOld;
        
        Identification__c objId = new Identification__c();
        objId.Id_No__c = '2/6/1234567';
        objId.Valid_From__c = system.today().addYears(-1);
        objId.Valid_To__c = system.today().addYears(1);
        objId.Identification_Type__c = 'Access Card';
        objId.Account__c = objAccountOld.Id;
        insert objId;
        
        License__c objLicense = new License__c();
        objLicense.License_Expiry_Date__c = system.today().addYears(1);
        objLicense.Status__c = 'Active';
        objLicense.License_No__c = '1234';
        objLicense.Account__c = objAccountOld.Id;
        insert objLicense;
        
        for(License__c objL : [select Id,Name from License__c where Id=:objLicense.Id ]){
            objLicense = objL;
        }
        
        objAccountOld.Active_License__c = objLicense.Id;
        objAccountOld.Index_Card__c = objId.Id;
        update objAccountOld;
        
        string conRT;
        for(RecordType objRT : [select Id,Name,DeveloperName from RecordType where DeveloperName='GS_Contact' AND SobjectType='Contact'])
                 conRT = objRT.Id;
           
        Contact objContact = new Contact();
        objContact.FirstName = 'Test';
        objContact.LastName = 'Name';
        objContact.AccountId = objAccountOld.Id; 
        objContact.Passport_No__c = 'ABC12345';
        objContact.RecordTypeId = conRT;
        insert objContact;
        
        Document_Details__c objDD = new Document_Details__c();
        objDD.Document_Id__c = '201/2015/1234567';
        objDD.Document_Number__c = '201/2015/1234567';
        objDD.Document_Type__c = 'Employment Visa';
        objDD.Contact__c = objContact.Id;
        insert objDD;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        lstStatus.add(new Status__c(Name='Verified',Code__c='VERIFIED'));
        lstStatus.add(new Status__c(Name='Posted',Code__c='POSTED'));
        insert lstStatus;
        
        list<SR_Status__c> lstSRStatus = new list<SR_Status__c>();
        lstSRStatus.add(new SR_Status__c(Name='Draft',Code__c='DRAFT'));
        lstSRStatus.add(new SR_Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstSRStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate.Menutext__c = 'DIFC_Sponsorship_Visa_Cancellation';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Employee Services';
        objTemplate.Active__c = true;
        insert objTemplate;
        
        list<Product2> lstProducts = new list<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'DIFC New Visa';
        lstProducts.add(objProd);
        insert lstProducts;
        
        list<Pricing_Line__c> lstPLs = new list<Pricing_Line__c>();
        Pricing_Line__c objPL;
        objPL = new Pricing_Line__c();
        objPL.Name = 'DIFC Cancellation';
        objPL.DEV_Id__c = 'GO-00001';
        objPL.Product__c = lstProducts[0].Id;
        objPL.Material_Code__c = 'GO-00001';
        objPL.Priority__c = 1;
        objPL.Additional_Item__c = false;
        lstPLs.add(objPL);
        insert lstPLs;
        
        list<Dated_Pricing__c> lstDP = new list<Dated_Pricing__c>();
        Dated_Pricing__c objDP;
        for(Pricing_Line__c objP : lstPLs){
            objDP = new Dated_Pricing__c();
            objDP.Pricing_Line__c = objP.Id;
            objDP.Unit_Price__c = 1234;
            objDP.Date_From__c = system.today().addMonths(-1);
            objDP.Date_To__c = system.today().addYears(2);
            lstDP.add(objDP);
        }
        insert lstDP;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_Cancellation')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }       
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('DIFC_Sponsorship_Visa_Cancellation');
        objSR.Send_SMS_To_Mobile__c = '+97152123456';
        objSR.Email__c = 'testclass@difc.ae.test';
        objSR.Type_of_Request__c = 'Applicant Outside UAE';
        objSR.Port_of_Entry__c = 'Dubai International Airport';
        objSR.Title__c = 'Mr.';
        objSR.First_Name__c = 'India';
        objSR.Last_Name__c = 'Hyderabad';
        objSR.Middle_Name__c = 'Andhra';
        objSR.Nationality_list__c = 'India';
        objSR.Previous_Nationality__c = 'India';
        objSR.Qualification__c = 'B.A. LAW';
        objSR.Gender__c = 'Male';
        objSR.Date_of_Birth__c = Date.newInstance(1989,1,24);
        objSR.Place_of_Birth__c = 'Hyderabad';
        objSR.Country_of_Birth__c = 'India';
        objSR.Passport_Number__c = 'ABC12345';
        objSR.Passport_Type__c = 'Normal';
        objSR.Passport_Place_of_Issue__c = 'Hyderabad';
        objSR.Passport_Country_of_Issue__c = 'India';
        objSR.Passport_Date_of_Issue__c = system.today().addYears(-1);
        objSR.Passport_Date_of_Expiry__c = system.today().addYears(2);
        objSR.Religion__c = 'Hindus';
        objSR.Marital_Status__c = 'Single';
        objSR.First_Language__c = 'English';
        objSR.Email_Address__c = 'applicant@newdifc.test';
        objSR.Mother_Full_Name__c = 'Parvathi';
        objSR.Monthly_Basic_Salary__c = 10000;
        objSR.Monthly_Accommodation__c = 4000;
        objSR.Other_Monthly_Allowances__c = 2000;
        objSR.Emirate__c = 'Dubai';
        objSR.City__c = 'Deira';
        objSR.Area__c = 'Air Port';
        objSR.Street_Address__c = 'Dubai';
        objSR.Building_Name__c = 'The Gate';
        objSR.PO_BOX__c = '123456';
        objSR.Residence_Phone_No__c = '+97152123456';
        objSR.Work_Phone__c = '+97152123456';
        objSR.Domicile_list__c = 'India';
        objSR.Phone_No_Outside_UAE__c = '+97152123456';
        objSR.City_Town__c = 'Hyderabad';
        objSR.Address_Details__c = 'Banjara Hills, Hyderabad';
        objSR.Statement_of_Undertaking__c = true;
        objSR.Internal_SR_Status__c = lstSRStatus[0].Id;
        objSR.External_SR_Status__c = lstSRStatus[0].Id;
        objSR.DIFC_License_No__c = objLicense.Name;
        objSR.Residence_Visa_No__c = '201/2015/1234567';
        objSR.Company_Name__c = objAccount.Name;
        objSR.Sponsor_Establishment_No__c = objId.Id_No__c;
        insert objSR;
        
        list<SR_Price_Item__c> lstPriceItems = new list<SR_Price_Item__c>();
        SR_Price_Item__c objItem;
        
        objItem = new SR_Price_Item__c();
        objItem.ServiceRequest__c = objSR.Id;
        objItem.Price__c = 3210;
        objItem.Pricing_Line__c = lstPLs[0].Id;
        objItem.Product__c = lstProducts[0].Id;
        lstPriceItems.add(objItem);
                
        insert lstPriceItems;
        //step template
        Step_Template__c objStepType1 = new Step_Template__c();
        objStepType1.Name = 'Front Desk Review';
        objStepType1.Code__c = 'Front Desk Review';
        objStepType1.Step_RecordType_API_Name__c = 'General';
        objStepType1.Summary__c = 'Front Desk Review';
        insert objStepType1;
        
        Step__c objStep = new Step__c();
        objStep.Step_Template__c = objStepType1.id ;
        objStep.SR__c = objSR.Id;
        objStep.Paused_Date__c = system.today();
        objStep.Closed_Date_Time__c = system.today();
        insert objStep;
        
        List<Service_Request__c> srList = [select Id,Required_Docs_not_Uploaded__c from Service_Request__c limit 1];
        
        Id GSrectypeid = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-Cancellation').getRecordTypeId();
            
        ApexPages.currentPage().getParameters().put('id',String.valueOf(srList[0].Id));
        ApexPages.currentPage().getParameters().put('RecordType',String.valueOf(GSrectypeid));
        ApexPages.currentPage().getParameters().put('RecordTypeName','DIFC_Sponsorship_Visa_Cancellation');
        
        ApexPages.StandardController sc = new ApexPages.StandardController(srList[0]);
        EmployeeCardNonSponsoredController difc = new EmployeeCardNonSponsoredController(sc);
        difc.saveRequest();
        difc.submitSR();
        difc.editRequest();
        difc.cancelRequest();
        difc.populateOnLoad();
        
        
        delete srList[0];
        difc.saveRequest();
    }
}