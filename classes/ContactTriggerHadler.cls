/******************************************************************************************
 *  Name        : ContactTriggerHadler 
 *  Author      : Veera
 *  Description : Contract Trigger Handler Class to execute some business logic    
 ----------------------------------------------------------------------------------------                               
    Modification History 
 ----------------------------------------------------------------------------------------
 V.No    Date        Updated By    Description
 ----------------------------------------------------------------------------------------              
 V1.1    07-Oct-2019 Durga         Added new method to create portal user based on some checks
 v1.2    17-Mar-2019 Durga         Added logic to Re-Create the Accountshare on change of AccountId on Contact
 v1.3    17-Mar-2019 Durga         invoke UpdateAccountContactRelation method to update the Role and Access Level on contact creation. 
 v1.4    20-Nov-2020 Mudasir       Finance contact person contact creation 
*******************************************************************************************/
public class ContactTriggerHadler {
    
    public static void onAfterInsert(list<Contact> newContacts){
        set<string> contactIds = new set<string>();
        //v1.4
        set<string> financeContactIds = new set<string>();
        list<Contact> lstPortalUserContact = new list<Contact>();
        for(Contact conObject : newContacts){
            if(conObject.Send_Portal_Login_Link__c=='Yes' && conObject.AccountId!=null){
                contactIds.add(conObject.Id);
                lstPortalUserContact.add(conObject);
            }
            //v1.4
            if(conObject.Role__c=='Financial Services'){financeContactIds.add(conObject.Id);}
        }
        if(contactIds.size()>0){
            OB_ContactTriggerHelper.UpdateAccountContactRelation(newContacts);//v1.3
            OB_ContactTriggerHelper.CreateRelationship(lstPortalUserContact);
            OB_ContactTriggerHelper.CreatePortalUser(contactIds);
        }
        //v1.4
        if(financeContactIds.size() > 0){OB_ContactTriggerHelper.CreatePortalUser(financeContactIds);}
    }
    public static void onAfterUpdate(list<Contact> newContacts,map<Id,Contact> TriggerOldMap,map<Id,Contact> TriggerNewMap){
        //v1.2
        list<AccountContactRelation> lstAccContactRelationships = new list<AccountContactRelation>();
        set<string> setContactIds = new set<string>();
        for(Contact con:newContacts){
            if(con.AccountId!=null && TriggerOldMap.get(con.Id).AccountId!=con.AccountId) setContactIds.add(con.Id);
        }
        if(setContactIds.size()>0){
            for(AccountContactRelation ACR:[Select Id,IsDirect,AccountId,ContactId from AccountContactRelation where ContactId IN:setContactIds and IsDirect=false]){
                lstAccContactRelationships.add(ACR);
            }
            if(lstAccContactRelationships.size()>0) OB_AcctContactTriggerHandler.ShareAccounts(lstAccContactRelationships);
        }
        //v1.2
        
        set<string> contactIds = new set<string>();
        list<Contact> lstPortalUserContact = new list<Contact>();
        map<string,string> MapUserExists = new map<string,string>();
        for(User usr:[Select Id,ContactId from User where ContactId IN:TriggerNewMap.keyset()]){
            MapUserExists.put(usr.ContactId,usr.Id);
        }
        for(Contact conObject : newContacts){
            if(MapUserExists.get(conObject.Id)==null && conObject.Send_Portal_Login_Link__c=='Yes' && TriggerOldMap.get(conObject.Id).Send_Portal_Login_Link__c!=conObject.Send_Portal_Login_Link__c && conObject.AccountId!=null){
                lstPortalUserContact.add(conObject);
                contactIds.add(conObject.Id);
            }
        }
        system.debug('=contactIds==='+contactIds);
        if(contactIds.size()>0){
            OB_ContactTriggerHelper.CreateRelationship(lstPortalUserContact);
            OB_ContactTriggerHelper.CreatePortalUser(contactIds);
        }
    }    
    public static void IsImportantCheck(List<Contact> Con , boolean IsUpdate){
        
        set<id> accountIds =  new set<id>();
        set<id> contactID =  new set<id>();
        
        for(Contact c: Con){
            if(c.AccountId != null || c.AccountId!='')
                accountIds.add(c.AccountId) ;
            
            if(IsUpdate && (c.Id != null || c.Id != '')){
                contactID.add(c.Id);
            }
        }
        List<contact> lstcontacts = new  List<contact> ();
        if(accountIds.isEmpty()) return;
        
        Map<string,string> mapOfContactWithImp = new Map<string,string>();
        
        If(!IsUpdate){    
            for(Contact iCon : [select id,name,important__c,AccountId  from contact where AccountId in :accountIds and important__c  = true AND Recordtype.Name = 'Business Development' AND Is_Active__c = true]){
            	mapOfContactWithImp.put(iCon .AccountId ,iCon.name);
            }
        }
        else{
            
             for(Contact iCon : [select id,name,important__c,AccountId  from contact where AccountId in :accountIds and important__c  = true AND ID NOT IN :contactID  AND Recordtype.Name = 'Business Development' AND Is_Active__c = true]){
             	mapOfContactWithImp.put(iCon .AccountId ,iCon.name);
            }
            
        
        }
        
        for (contact cNew:con){
         
             if( mapOfContactWithImp.containsKey(cNew.AccountId)){
                
                    cNew.addError('Important has already selected for '+ mapOfContactWithImp.get(cNew.AccountId));
            }
            
        }
           
        
    }
    
}