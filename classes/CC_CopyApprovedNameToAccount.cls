/*
    Author      : Durga Prasad
    Date        : 03-Dec-2019
    Description : Custom code to copy the approved company name to account
    --------------------------------------------------------------------------------------
*/
global without sharing class CC_CopyApprovedNameToAccount implements HexaBPM.iCustomCodeExecutable {
    global string EvaluateCustomCode(HexaBPM__Service_Request__c SR,HexaBPM__Step__c stp) {
        string strResult = 'Success';
        try{
            if(stp!=null && stp.HexaBPM__SR__c!=null){
                string ApprovedCompanyName = '';
                Account acc = new Account(Id=stp.HexaBPM__SR__r.HexaBPM__Customer__c);
                if(stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c!=null && (stp.HexaBPM__SR__r.HexaBPM__Record_Type_Name__c=='Name_Check' || system.test.isRunningTest())){
                    for(Company_Name__c CN:[select Id,Application__c,Application__r.HexaBPM__Parent_SR__c,Ends_With__c,Arabic_Ends_With__c,Entity_Name__c,Arabic_Entity_Name__c,Arabic_Trading_Name__c,Trading_Name__c from Company_Name__c where Application__c=:stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c and Status__c='Approved' and Entity_Name__c!=null]){
                        ApprovedCompanyName = CN.Entity_Name__c;
                        acc.Name = ApprovedCompanyName;
                        if(CN.Ends_With__c!=null){
                            acc.Name = acc.Name+' '+CN.Ends_With__c;
                            //acc.Legal_structure_full__c = CN.Ends_With__c;
                        }
                        acc.Trade_Name__c = CN.Trading_Name__c;
                        acc.Trading_Name_Arabic__c = CN.Arabic_Trading_Name__c;
                        acc.Arabic_Name__c = CN.Arabic_Entity_Name__c;
                        if(CN.Arabic_Ends_With__c!=null){
                            acc.Arabic_Name__c = acc.Arabic_Name__c+' '+CN.Arabic_Ends_With__c;
                            //acc.Legal_Structure_Arabic__c = CN.Arabic_Ends_With__c;
                        }
                    }
                }else{
                    for(Company_Name__c CN:[select Id,Application__c,Application__r.HexaBPM__Parent_SR__c,Ends_With__c,Arabic_Ends_With__c,Entity_Name__c,Arabic_Entity_Name__c,Arabic_Trading_Name__c,Trading_Name__c from Company_Name__c where Application__c=:stp.HexaBPM__SR__c and Status__c='Approved' and Entity_Name__c!=null]){
                        ApprovedCompanyName = CN.Entity_Name__c;
                        acc.Name = ApprovedCompanyName;
                        if(CN.Ends_With__c!=null){
                            acc.Name = acc.Name+' '+CN.Ends_With__c;
                            //acc.Legal_structure_full__c = CN.Ends_With__c;
                        }
                        acc.Trade_Name__c = CN.Trading_Name__c;
                        acc.Trading_Name_Arabic__c = CN.Arabic_Trading_Name__c;
                        acc.Arabic_Name__c = CN.Arabic_Entity_Name__c;
                        if(CN.Arabic_Ends_With__c!=null){
                            acc.Arabic_Name__c = acc.Arabic_Name__c+' '+CN.Arabic_Ends_With__c;
                            //acc.Legal_Structure_Arabic__c = CN.Arabic_Ends_With__c;
                        }
                    }
                }
                if(ApprovedCompanyName!=''){
                    list<HexaBPM__Service_Request__c> lstSRTBU = new list<HexaBPM__Service_Request__c>();
                    if(stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c!=null){
                        HexaBPM__Service_Request__c objParentSR = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__r.HexaBPM__Parent_SR__c);
                        objParentSR.Entity_Name__c = ApprovedCompanyName;
                        lstSRTBU.add(objParentSR);
                    }
                    HexaBPM__Service_Request__c objSR = new HexaBPM__Service_Request__c(Id=stp.HexaBPM__SR__c);
                    objSR.Entity_Name__c = ApprovedCompanyName;
                    lstSRTBU.add(objSR);
                    update lstSRTBU;
                    
                    update acc;
                }
            }
        }catch(Exception e){
            strResult = e.getMessage()+'';
        }
        return strResult;
    }
    
}