@isTest
global class webServiceGdrfaAppFeeMock implements HttpCalloutMock{
   //Implement http mock callout here
   // Implement this interface method
   global HTTPResponse respond(HTTPRequest request){
       // Create a fake response
       HttpResponse response = new HttpResponse();
       response.setStatusCode(200);
       response.setBody('{"applicationId": "1920100000073","fees": [{"feeTypeId": "7d101c62-9b27-4792-b47e-b251999ef7b6","amount": 50,"nameEn": "Service Fee","nameAr": "رسم الخدمة"}], "Success":true,  "isValid": "true",  "ReportBinary": "Base64Content"}');     
       return response;
   }
}