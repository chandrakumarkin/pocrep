/*********************************************************************************************************************
*  Name     : BPM_SecurityStepStatusUpdateBatchSchedule
*  Author   : Durga Kandula
*  Purpose  : This class used to schedule BPM_SecurityStepStatusUpdateBatch class
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
 V1.0         Durga
**/

global class BPM_SecurityStepStatusUpdateBatchSch implements schedulable{
    global void execute(schedulablecontext sc){
        BPM_SecurityStepStatusUpdateBatch sc1 = new BPM_SecurityStepStatusUpdateBatch(); 
        database.executebatch(sc1,1);
    }
}