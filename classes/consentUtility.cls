/******************************************************************************************
 *  Author   : Selva
 *  Date     : 04-sep-2018
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
v1.0   04/09/2018  Selva        createConsent- method to capture the customer Ip address when the user registring for poral access.mail:22/7/2018 Assembla #5666
*********************************************************************************************************************/
//User Access Form controller class

public without sharing class consentUtility{
    //Added V2.2 23/07/2018 Assembla #5666
    @InvocableMethod(label='Create Consent' description='Create Consent when user request through portal')
    public static void createConsent(List<Service_Request__c> servReqList) {
        system.debug('--method invoked--');
        String ipaddr ;//= Auth.SessionManagement.getCurrentSession().get('SourceIp');
        String ipAddress = ApexPages.currentPage().getHeaders().get('X-Salesforce-SIP');
        
        string BaseURL = URL.getSalesforceBaseUrl().toExternalForm();
        string fullURL = ApexPages.currentPage().getUrl();
        String newstr;
        if (null != fullURL && fullURL.length() > 0 ){
            integer endIndex = fullURL.lastIndexOf('?');
            if (endIndex != -1) {
                newstr = fullURL.substring(0, endIndex); // not forgot to put check if(endIndex != -1)
            }
        }  
        if(servReqList.size()>0){
            try{
                Consent__c c = new Consent__c();
                string str = servReqList[0].First_Name__c +' '+servReqList[0].Last_Name__c;
                c.FullName__c = str.trim();
                c.ServiceRequest__c = servReqList[0].id;
                if(ipaddr!=null){
                    c.IpAddress__c = ipaddr;
                }else{
                    c.IpAddress__c = ipAddress;
                }
                c.DateTime__c = system.now();
                c.Consent_Type__c = 'Yes';
                c.PageURL__c = BaseURL+newstr;
                insert c;
                
            }catch(Exception e){
                system.debug('--e--'+e);
            }
        }
    }
}