@isTest(seeAllData=false)
public class ROCconsolidateSubDivideShares_Test{

    static testMethod void myUnitTest() {
    
        Account objAccount = new Account();
        objAccount.Name = 'Test Account123';
        objAccount.Place_of_Registration__c ='Pakistan';
        objAccount.ROC_Status__c = 'Active';
        insert objAccount;
        
        Account objAccount2 = new Account();
        objAccount2.Name = 'Test Account';
        objAccount2.Place_of_Registration__c ='Pakistan';
        insert objAccount2;
        
        string ContacRecType = '';
          for(RecordType recType:[select id from RecordType where sObjectType='Contact' and DeveloperName='Individual']){
            ContacRecType = recType.Id;
          }
          
          string RecTypeId = '';
        for(RecordType objRec:[select id from RecordType where DeveloperName='Consolidate_Subdivide_Share' and sObjectType='Service_Request__c']){
            RecTypeId = objRec.Id;
        } 
      
    
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.FirstName = 'Test Contact';
        objContact.AccountId = objAccount.Id;
        objContact.Passport_No__c = 'AS4324SDF';
        objContact.Email = 'test@difc.com';
        objContact.RecordTypeId = ContacRecType;
        objContact.Nationality__c = 'Pakistan';
        insert objContact;
        
        Account_Share_Detail__c accshrlist = new Account_Share_Detail__c();
        accshrlist.Account__c =  objAccount.Id;
        accshrlist.Name = 'A';
        accshrlist.Status__c = 'Active';
        accshrlist.No_of_shares_per_class__c = 50;
        accshrlist.Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_Nominal_Value__c = 1000;
        accshrlist.Sys_Proposed_No_of_Shares_per_Class__c=50;
        insert accshrlist;
        
        SR_Template__c objTemplate = new SR_Template__c();
        objTemplate.Name = 'Consolidate/ Subdivide shares';
        objTemplate.SR_RecordType_API_Name__c = 'Consolidate_Subdivide_Share';
        objTemplate.Menutext__c = ' Consolidate or Subdivide shares';
        objTemplate.Available_for_menu__c = true;
        objTemplate.Template_Sequence_No__c = 123;
        objTemplate.Menu__c = 'Company';
        objTemplate.Active__c = true;
        insert objTemplate;
        
         Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.legal_structures__c = 'LLC';
        objSR.Foreign_Registration_Name__c = 'Test FR Name';
        objSR.Foreign_Registration_Date__c = system.today();
        objSR.Foreign_Registration_Place__c = 'United Arab Emirates';
        objSR.Foreign_Registered_Building__c = 'Building1';
        objSR.Foreign_Registered_Street__c = 'Street1';
        objSR.Foreign_Registered_PO_Box__c = '13124';
        if(RecTypeId!=null && RecTypeId!='')
            objSR.RecordTypeId = RecTypeId;
        objSR.Foreign_Registered_Post__c = '124214';
        objSR.Foreign_registered_office_no__c = '124124';
        objSR.Foreign_Registered_Level__c = '12';
        objSR.Foreign_Registered_Country__c = 'United Arab Emirates';
        objSR.SR_Template__c = objTemplate.Id;
        objSR.Entity_Name__c='Test';
        objSR.Proposed_Trading_Name_1__c='Test';
        objSR.Incorporated_Organization_Name__c='Test';
        objSR.Nature_of_business__c='Test';
        objSR.Nature_of_Business_Arabic__c='Test';
        objSR.Name_identical_to_another_company__c=true;
        objSR.Business_Name__c='Test';
        objSR.Identical_Business_Domicile__c='Test';
        objSR.Pro_Entity_Name_Arabic__c='Test';
        objSR.Pro_Trade_Name_1_in_Arab__c ='Test';
        objSR.Legal_Structures__c='Test';
        objSR.Registration_Type__c='Test';
        objSR.AOA_Format__c='Test';
        objSR.Standard_Charter_Format__c='Test';
        objSR.Current_Registered_Name__c='Test';
        objSR.Domicile_list__c='Test';
        objSR.Current_Registered_Office_No__c='Test';
        objSR.Current_Registered_Building__c='Test';
        objSR.Current_Registered_Level__c='Test';
        objSR.Current_Registered_Street__c='Test';
        objSR.Current_Registered_PO_Box__c='Test';
        objSR.Current_Address_Country__c='Test';
        objSR.Current_Registered_Postcode__c='Test';
        objSR.Principal_Business_Activity__c='Test';
        objSR.Reasons_to_transfer_Foreign_Entity__c='Test';
        objSR.DFSA_Approval__c=true;
        objSR.DFSA_In_principle_Approval_Date__c=system.today();
        objSR.Date_of_Resolution__c = Date.Today();
        insert objSR;
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Consolidate/ Subdivide shares';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Consolidate_Subdivide_Share';
        insert objPageFlow;
        
        Page__c objPage = new Page__c();
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        objPage.VF_Page_API_Name__c = 'Change_SH_Members';
        objPage.Is_Custom_Component__c = true;
        objPage.Render_By_Default__c = true;
        insert objPage;
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Navigation_Directions__c = 'Forward';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        Amendment__c iAmm = new Amendment__c();
        iAmm.Status__c = 'Active';
        iAmm.No_of_shares_per_class__c = 1;
        iAmm.Sys_Proposed_Nominal_Value__c = 1;
        iAmm.Nominal_Value__c = 1;
        iAmm.Sys_Proposed_No_of_Shares_per_Class__c = 1;
        iAmm.Shareholder_Company__c = objAccount.ID;
        iAmm.ServiceRequest__c = objSR.ID;
        iAmm.Class_Name__c ='A';
     
        
        insert iAmm;
        
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        Apexpages.currentPage().getParameters().put('PageId',objPage.Id);
        Apexpages.currentPage().getParameters().put('Id',objSR.Id);
        Apexpages.currentPage().getParameters().put('index','0');
        
        ROCconsolidateSubDivideShares_Ctrl con = new ROCconsolidateSubDivideShares_Ctrl();
        ROC_ShareForms_Ctrl shareForm = new ROC_ShareForms_Ctrl();
        con.strNavigatePageId = objPage.Id;
        con.strActionId = objSD.Id;
        con.GetSharesAgainstClass();
        con.getDyncPgMainPB();
        con.BackToServiceRequest();
        con.DynamicButtonAction();
        con.addRecord();
        con.goTopage();
       // con.Save_ShareDetail();
        con.RowId = 0;
        con.delRow();
    }
}