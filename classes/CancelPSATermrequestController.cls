public class CancelPSATermrequestController
{
    public static String myString {get; set;}
    public static boolean intStatus{get;set;}
    // public static boolean priItem{get;set;}
    private ApexPages.StandardController sctrl;
    private Service_Request__c SR1;
    public Id SRID =null;
    
    //Constructor 
    public CancelPSATermrequestController(ApexPages.StandardController stdCtrl)
    {
        
        SRID = stdCtrl.getRecord().id;
        this.SR1 = (Service_Request__c)stdCtrl.getRecord();
        this.sctrl = stdCtrl;
        for(Service_Request__c sr : [select Id,Name,Internal_Status_Name__c,SR_Template__r.Menu__c, RecordType.DeveloperName,(select id ,step_name__c,Status_Code__c from Steps_SR__r) ,(Select id,name,status__c from SR_Price_Items1__r) from Service_Request__c where id=:SRID and (SR_Group__c='GS' )  limit 1])
        {
            this.SR1 = sr;
            
              if(sr.Internal_Status_Name__c != 'Draft')
            {
                
                intStatus = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Not Allowed to Cancel the Request'));
               
            }
            
         }
     }
    
    
    // Code we will invoke on page load.
    public void autoRun() 
    {
        String SRID = ApexPages.currentPage().getParameters().get('id');
        system.debug('!!!!!!ID' +SRID);
        system.debug('intStatus11111' +intStatus);
        Id cancelledStatusId = [Select id from SR_Status__c where name ='Cancelled' limit 1].id;
      
        
        SR1.External_SR_Status__c = cancelledStatusId ;
        SR1.Internal_SR_Status__c = cancelledStatusId ;
        SR1.Is_Cancelled__c = true;
        SR1.Description__c = myString;
        system.debug('!!!!!!!!!!---' +myString);
        
       
       
        
      
        
       
        
       
            update SR1;
        }
        
        
   
    
}