@isTest
public class OB_PaymentModeControllerTest {
    
    @testsetup
    static void initializaData(){
        Test.startTest();
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insertNewAccounts[0].Legal_Type_of_Entity__c = 'Public Company';
        //insertNewAccounts[0].Next_Renewal_Date__c = Date.today();
        insert insertNewAccounts;
        
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con; 
        
        //Create user
        Profile portalProfile = [SELECT Id FROM Profile Where Name='DIFC Customer Community Plus User Custom' Limit 1];
        User user1 = new User(
            Username = System.now().millisecond() + 'test12345@test.com',
            ContactId = con.Id,
            ProfileId = portalProfile.Id,
            Alias = 'test123',
            Email = 'portaltest12345@test.com',
            EmailEncodingKey = 'UTF-8',
            LastName = 'PortalTestUser',
            CommunityNickname = 'test12345',
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US'
        );
        Database.insert(user1);
        
        Test.stopTest();
    }
    
    public static testmethod void test1(){
      
        Account acc = OB_PaymentModeController.getBpNumberFromAccount();
        String rectypeId = OB_PaymentModeController.getRecordTypeId();
        List<String> countries = OB_PaymentModeController.getCountries();
        Map<String,String> countryvals = OB_PaymentModeController.getCountryValues();
        Id recId = OB_PaymentModeController.getRecTypeId();
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(3);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(3,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(3, new List<string> {'AOR_Financial', 'In_Principle','Name_Reservation_Renewal'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[2].Lead_Id__c = string.valueof(insertNewLeads[2].id).substring(0,15);
        
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[2].Setting_Up__c='Branch';
        
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[2].Page_Tracker__c = listPage[0].id;
        
        
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[2].Foreign_entity_registered_number__c = '64545';
        
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insert insertNewSRs;
        
        insertNewSRs[2].HexaBPM__Parent_SR__c =  insertNewSRs[1].Id;
        update insertNewSRs;
        
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
        listSRSteps[0].HexaBPM__Step_No__c = 1;
        insert listSRSteps;
        
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
        insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insert insertNewSteps;
        
        // create pricing 
        HexaBPM__Pricing_Line__c pricing = OB_TestDataFactory.createSRPriceItem();
        insert pricing;
        // Create Products
        List<Product2> products = OB_TestDataFactory.createProduct(new List<String>{'OB Name Reservation', 'OB Name Reservation 1'});
        insert products;
        //create priceitem
        List<HexaBPM__SR_Price_Item__c> createSRPriceItemObj = OB_TestDataFactory.createSRPriceItem(products,insertNewSRs[0].id,pricing.id);
        insert createSRPriceItemObj;
        
        
        OB_PaymentModeController.PriceItem pi = OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve',true);
        OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve1',true);
        OB_PaymentModeController.getSRLinesAmount(insertNewSRs[0].id);
        OB_PaymentModeController.createReceiptForCheque(new Receipt__c());
        // OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve1');
        // OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve');
        // 
        OB_SR_StepsCls.getStepGroupWrapper(insertNewSRs[0].id);
        /*
User portalAccountOwner = createPortalAccountOwner();   
User communityUser = createCommunityUser(portalAccountOwner); 
*/
        
        
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con;  
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        
        
        System.debug('SOQL USED----> 1 -->'+Limits.getQueries());
        System.debug('SOQL TOTAL LIMIT----> 1 -->'+Limits.getLimitQueries());
        
     
        system.runAs(user){
            List<Company_Name__c> companies = OB_TestDataFactory.createCompanyName(new List<String>{'Com1','Com2'});
            companies[0].Application__c = insertNewSRs[1].id;
            companies[0].Status__c = 'Approved';
            companies[0].Reserved_Name__c = true;
            companies[0].Date_of_Approval__c = system.today();
            companies[0].OB_Is_Trade_Name_Allowed__c = 'Yes';
            insert companies;
            
            
            Receipt__c receipt = new Receipt__c();
            Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
            receipt.RecordTypeId=rt;
            insert receipt; 
            OB_PaymentController.RequestWrapper reqWrap = new OB_PaymentController.RequestWrapper();
            reqWrap.paymentType = 'Bank Transfer';
            reqWrap.amount = '100.87';
            reqWrap.receiptId = receipt.id;
            String reqString = JSON.serialize(reqWrap);
            OB_PaymentController.updateReceipt(reqString);
            OB_PaymentModeController.createReceiptForCard(reqString);
            
            Test.startTest();
            
            reqWrap.srId = insertNewSRs[2].id;
            String reqStringNew = JSON.serialize(reqWrap);
            OB_PaymentController.processNameRenewalSR(reqStringNew);
            
            reqWrap.srId = insertNewSRs[0].id;
            reqStringNew = JSON.serialize(reqWrap);
    
            
            
            OB_PaymentController.viewProductInformation(insertNewSRs[0].id, 'namereserve');
            OB_PaymentController.viewProductInformation(insertNewSRs[0].id, '');

            OB_PaymentController.getselectOptions(new account(),'billingcountry');
            
            OB_PaymentController.submitForNameApproval('123',insertNewSRs[0].id);
            
            OB_NamingConvention.updateSR(insertNewSRs[1], 500.00);
            OB_PaymentController.createNameReservationSR(insertNewSRs[1].id);
            OB_PaymentController.updateSRLines(insertNewSRs[0].id);
            
            //System.debug('SOQL USED----> 5 -->'+Limits.getQueries());
            //System.debug('SOQL TOTAL LIMIT----> 5 -->'+Limits.getLimitQueries());
            //OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve',true);
            //OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve1',true);
            
            //System.debug('SOQL USED----> 6 -->'+Limits.getQueries());
            //System.debug('SOQL TOTAL LIMIT----> 6 -->'+Limits.getLimitQueries());
            //OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve1');
            //OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve');

            Test.stopTest();
        }
        
    }
    
    public static testmethod void test3(){
      
        Account acc = OB_PaymentModeController.getBpNumberFromAccount();
        String rectypeId = OB_PaymentModeController.getRecordTypeId();
        List<String> countries = OB_PaymentModeController.getCountries();
        Map<String,String> countryvals = OB_PaymentModeController.getCountryValues();
        Id recId = OB_PaymentModeController.getRecTypeId();
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(3);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(3,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(3, new List<string> {'AOR_Financial', 'In_Principle','Name_Reservation_Renewal'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[2].Lead_Id__c = string.valueof(insertNewLeads[2].id).substring(0,15);
        
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[2].Setting_Up__c='Branch';
        
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[2].Page_Tracker__c = listPage[0].id;
        
        
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[2].Foreign_entity_registered_number__c = '64545';
        
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insert insertNewSRs;
        
        insertNewSRs[2].HexaBPM__Parent_SR__c =  insertNewSRs[1].Id;
        update insertNewSRs;
        
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
        listSRSteps[0].HexaBPM__Step_No__c = 1;
        insert listSRSteps;
        
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
        insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insert insertNewSteps;
        
        // create pricing 
        HexaBPM__Pricing_Line__c pricing = OB_TestDataFactory.createSRPriceItem();
        insert pricing;
        // Create Products
        List<Product2> products = OB_TestDataFactory.createProduct(new List<String>{'OB Name Reservation', 'OB Name Reservation 1'});
        insert products;
        //create priceitem
        List<HexaBPM__SR_Price_Item__c> createSRPriceItemObj = OB_TestDataFactory.createSRPriceItem(products,insertNewSRs[0].id,pricing.id);
        insert createSRPriceItemObj;
        
        
        OB_PaymentModeController.PriceItem pi = OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve',true);
        OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve1',true);
        OB_PaymentModeController.getSRLinesAmount(insertNewSRs[0].id);
        OB_PaymentModeController.createReceiptForCheque(new Receipt__c());
        // OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve1');
        // OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve');
        // 
        OB_SR_StepsCls.getStepGroupWrapper(insertNewSRs[0].id);
        /*
User portalAccountOwner = createPortalAccountOwner();   
User communityUser = createCommunityUser(portalAccountOwner); 
*/
        
        
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con;  
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        
        
     
        system.runAs(user){
            List<Company_Name__c> companies = OB_TestDataFactory.createCompanyName(new List<String>{'Com1','Com2'});
            companies[0].Application__c = insertNewSRs[1].id;
            companies[0].Status__c = 'Approved';
            companies[0].Reserved_Name__c = true;
            companies[0].Date_of_Approval__c = system.today();
            companies[0].OB_Is_Trade_Name_Allowed__c = 'Yes';
            insert companies;
            Test.startTest();
                        

            OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve',true);
            OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve1',true);
            
            OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve1');
            OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve');
            

            Test.stopTest();
        }
        
    }
    
    public static testmethod void test2(){
        Account acc = OB_PaymentModeController.getBpNumberFromAccount();
        String rectypeId = OB_PaymentModeController.getRecordTypeId();
        List<String> countries = OB_PaymentModeController.getCountries();
        Map<String,String> countryvals = OB_PaymentModeController.getCountryValues();
        Id recId = OB_PaymentModeController.getRecTypeId();
        
        // create account
        List<Account> insertNewAccounts = new List<Account>();
        insertNewAccounts =  OB_TestDataFactory.createAccounts(1); 
        insert insertNewAccounts;
        //create lead
        List<Lead> insertNewLeads = new List<Lead>();
        insertNewLeads = OB_TestDataFactory.createLead(2);
        insert insertNewLeads;
        //create License Activity Master
        List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
        listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string> {'Non - financial'},new List<string> {'License Activity'});
        insert listLicenseActivityMaster;
        
        //create lead activity
        List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
        listLeadActivity = OB_TestDataFactory.createLeadActivity(3,insertNewLeads, listLicenseActivityMaster);
        insert listLeadActivity;
        
        //create createSRTemplate
        List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
        createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string> {'AOR_Financial','In_Principle'});
        insert createSRTemplateList;
        
        //create page flow
        list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>() ;
        listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
        insert listPageFlow;
        
        //create page
        list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>() ;
        listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
        listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
        listPage[0].Community_Page__c = 'ob-searchentityname';
        insert listPage;
        
        //create SR status
        list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string> {'Closed','Draft','Submitted'}, new List<string> {'CLOSED','DRAFT','SUBMITTED'}, new List<string> {'End','',''});
        insert listSRStatus;
        //create SR
        List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
        insertNewSRs = OB_TestDataFactory.createSR(2, new List<string> {'AOR_Financial', 'In_Principle'}, insertNewAccounts, 
                                                   new List<string>{'Non - financial','Retail'}, 
                                                   new List<string>{'Foundation','Services'}, 
                                                   new List<string>{'Foundation','Company'}, 
                                                   new List<string>{'Foundation','Recognized Company'});
        insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads[0].id).substring(0,15);
        insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads[1].id).substring(0,15);
        insertNewSRs[1].Setting_Up__c='Branch';
        insertNewSRs[0].Setting_Up__c='Branch';
        insertNewSRs[1].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Page_Tracker__c = listPage[0].id;
        insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
        insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
        insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
        insert insertNewSRs;
        
        //create step template
        List<HexaBPM__Step_Template__c> listStepTemplate = new List<HexaBPM__Step_Template__c>();
        listStepTemplate = OB_TestDataFactory.createStepTemplate(1, new List<string> {'RM_Assignment'}, new List<String> {'RM_Assignment'}
                                                                 , new List<String> {'General'},  new List<String> {'RM_Assignment'});
        insert listStepTemplate;
        //create SR Step
        List<HexaBPM__SR_Steps__c> listSRSteps = new List<HexaBPM__SR_Steps__c>();
        listSRSteps = OB_TestDataFactory.createSRStep(1, createSRTemplateList, listStepTemplate, null);
        listSRSteps[0].HexaBPM__Group_Name__c = 'Test';
        listSRSteps[0].HexaBPM__Step_No__c = 1;
        insert listSRSteps;
        
        List<HexaBPM__Step__c> insertNewSteps = new List<HexaBPM__Step__c>();
        insertNewSteps = OB_TestDataFactory.createSteps(2, insertNewSRs, listSRSteps);
        //insertNewSteps[0].Step_Template_Code__c = 'RM_Assignment';
        insert insertNewSteps;
        
        // create pricing 
        HexaBPM__Pricing_Line__c pricing = OB_TestDataFactory.createSRPriceItem();
        insert pricing;
        // Create Products
        List<Product2> products = OB_TestDataFactory.createProduct(new List<String>{'OB Name Reservation', 'OB Name Reservation 1'});
        insert products;
        //create priceitem
        List<HexaBPM__SR_Price_Item__c> createSRPriceItemObj = OB_TestDataFactory.createSRPriceItem(products,insertNewSRs[0].id,pricing.id);
        insert createSRPriceItemObj;
        
        
        OB_PaymentModeController.PriceItem pi = OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve',true);
        OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve1',true);
        OB_PaymentModeController.getSRLinesAmount(insertNewSRs[0].id);
        OB_PaymentModeController.createReceiptForCheque(new Receipt__c());
        // OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve1');
        // OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve');
        // 
        OB_SR_StepsCls.getStepGroupWrapper(insertNewSRs[0].id);
        /*
User portalAccountOwner = createPortalAccountOwner();   
User communityUser = createCommunityUser(portalAccountOwner); 
*/
        
        
        Id p = [select id from profile where name='DIFC Customer Community Plus User Custom'].id;      
        Contact con = new Contact(LastName ='testCon',AccountId = insertNewAccounts[0].Id);
        insert con;  
        
        User user = new User(alias = 'test123', email='test123@noemail.com',
                             emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                             localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                             ContactId = con.Id,
                             timezonesidkey='America/Los_Angeles', username='tester@noemail.com');
        
        insert user;
        
        system.runAs(user){
            List<Company_Name__c> companies = OB_TestDataFactory.createCompanyName(new List<String>{'Com1','Com2'});
            companies[0].Application__c = insertNewSRs[1].id;
            insert companies;
            
            Receipt__c receipt = new Receipt__c();
            Id rt=Schema.SObjectType.Receipt__c.getRecordTypeInfosByName().get('Card').getRecordTypeId();
            receipt.RecordTypeId=rt;
            insert receipt; 
            OB_PaymentController.RequestWrapper reqWrap = new OB_PaymentController.RequestWrapper();
            reqWrap.paymentType = 'Bank Transfer';
            reqWrap.amount = '100.87';
            reqWrap.receiptId = receipt.id;
            String reqString = JSON.serialize(reqWrap);
            
            OB_PaymentController.updateReceipt(reqString);
            OB_PaymentModeController.createReceiptForCard(reqString);
            OB_PaymentController.viewProductInformation(insertNewSRs[0].id, 'namereserve');
            OB_PaymentController.viewProductInformation(insertNewSRs[0].id, '');
            OB_PaymentController.getselectOptions(new account(),'billingcountry');
            OB_PaymentController.submitForNameApproval('123',insertNewSRs[0].id);
            OB_NamingConvention.updateSR(insertNewSRs[1], 500.00);
            OB_PaymentController.createNameReservationSR(insertNewSRs[0].id);
            Test.startTest();
            OB_PaymentController.updateSRLines(insertNewSRs[0].id);
            OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve',true);
            OB_PaymentModeController.loadPriceItem(insertNewSRs[0].id,'namereserve1',true);
            OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve1');
            OB_PaymentModeController.updatePriceItem(insertNewSRs[0].id,'namereserve');
            Test.stopTest();
        }
       
    }
    
    private static User createPortalAccountOwner() {
        UserRole portalRole = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role', PortalType = 'None');
        insert portalRole;
        System.debug('portalRole is ' + portalRole);
        Profile sysAdminProfile = [Select Id from Profile where name = 'System Administrator'];
        User portalAccountOwner = new User(
            UserRoleId = portalRole.Id,
            ProfileId = sysAdminProfile.Id,
            Username = 'portalOwner' + System.currentTimeMillis() + '@test.com',
            Alias = 'Alias',
            Email = 'portal.owner@test.com',
            EmailEncodingKey = 'UTF-8',
            Firstname = 'Portal',
            Lastname = 'Owner',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US',
            TimeZoneSidKey = 'America/Los_Angeles'
        );
        Database.insert(portalAccountOwner);
        return portalAccountOwner;
    }
    private static User createCommunityUser(User portalAccountOwner) {
        User communityUser;
        System.runAs(portalAccountOwner) {
            //Create account  
            Account portalAccount = new Account(
                Name = 'portalAccount',
                OwnerId = portalAccountOwner.Id
            );
            Database.insert(portalAccount);
            //Create contact  
            Contact portalContact = new Contact(
                FirstName = 'portalContactFirst',
                Lastname = 'portalContactLast',
                AccountId = portalAccount.Id,
                Email = 'portalContact' + System.currentTimeMillis() + '@test.com'
            );
            Database.insert(portalContact);
            Set<String> customerUserTypes = new Set<String> {'CspLitePortal'};
                communityUser = new User(
                    ProfileId = [SELECT id FROM profile where usertype in :customerUserTypes limit 1].Id,
                    FirstName = 'CommunityUserFirst',
                    LastName = 'CommunityUserLast',
                    Email = 'community.user@test.com',
                    Username = 'community.user.' + System.currentTimeMillis() + '@test.com',
                    Title = 'Title',
                    Alias = 'Alias',
                    TimeZoneSidKey = 'America/Los_Angeles',
                    EmailEncodingKey = 'UTF-8',
                    LanguageLocaleKey = 'en_US',
                    LocaleSidKey = 'en_US',
                    ContactId = portalContact.id
                );
            Database.insert(communityUser);
        }
        return communityUser;
    }
    
    static testmethod void testPrepareCardRequest1(){
        User puser = [Select Id,Name,Contact.AccountId From User Where LastName='PortalTestUser' OR Email='portaltest12345@test.com' LIMIT 1];
        Test.startTest();
        System.runAs(puser){
         

            
            OB_PaymentModeController.prepareCardRequest(100,null);
            
        }
        
        Test.stopTest();
    }
    
    static testmethod void testPrepareCardRequest2(){
        User puser = [Select Id,Name,Contact.AccountId From User Where LastName='PortalTestUser' OR Email='portaltest12345@test.com' LIMIT 1];
        Test.startTest();
        System.runAs(puser){
            Account acc = new Account(Id = puser.Contact.AccountId);
            List<Account> accList = new List<Account>{acc};
            
        SR_Template__c objTemplate1 = new SR_Template__c();
        objTemplate1.Name = 'Fit - Out Service Request';
        objTemplate1.SR_RecordType_API_Name__c = 'Fit - Out Service Request';
        objTemplate1.Active__c = true;
        insert objTemplate1;    
                
                
            RecordType srRecordType = [SELECT Id, Name, DeveloperName, SobjectType FROM RecordType where SobjectType ='Service_request__c' and DeveloperName='Letter_of_Good_Standing'];
            SR_Status__c srStatus = new SR_Status__c(Name ='Draft',Code__c='DRAFT');
            insert srStatus;
            Service_Request__c objSR3 = new Service_Request__c();
            objSR3.Customer__c = acc.Id;
            objSR3.Contractor__c = acc.Id;
            objSR3.RecordTypeId = srRecordType.id;
            objSR3.External_SR_Status__c = srStatus.id;
            objSR3.Internal_SR_Status__c = srStatus.id;
            objSR3.Submitted_Date__c = Date.today();
            objSR3.SR_Group__c = 'ROC';
            //objSR3.Pre_GoLive__c = false; 
            //objSR3.SAP_Unique_No__c = '3323243243241';
            objSR3.SR_Template__c      = objTemplate1.Id;
            insert objSR3; 
            
            
            
            SR_Price_Item__c objSRItem = new SR_Price_Item__c();
            objSRItem.ServiceRequest__c = objSR3.Id;
            objSRItem.Status__c = 'Invoiced';
            objSRItem.Price__c = 1000;
            insert objSRItem;
            

            
            OB_PaymentModeController.prepareCardRequest(100,objSR3.Id);
            
            Receipt__c[] rlst = [Select Id,name,Service_Request__c From Receipt__c Where Service_Request__c=:objSR3.Id LIMIT 1];
            if(!rlst.isEmpty()){
             OB_PaymentModeController.getReceiptData(rlst[0].Id);   
            }
            
        }
        
        Test.stopTest();
    }
}