/******************************************************************************************
 *  Author   :  Veera
 *  Company  : VRK
  *  Date     : 30/05/202
---------------------------------------------------------------------------------------------------------------------
Modification History
---------------------------------------------------------------------------------------------------------------------
V.No    Date        Updated By    Description
---------------------------------------------------------------------------------------------------------------------             
V1.0    30/05/2021  Created      This class used to check the pricing conditions for the Amendment of Emp/Dep Visas

**********************************************************************************************************************/


public without sharing class CC_VisaAmenedmentPriceController{

    public static boolean checkVisaProfessionChange(string strSRID){
    
    
        List<Service_Request__c> srList= [select Id,Type_of_Amendment__c from Service_Request__c where Id=:strSRID limit 1];
        boolean bResult;
        
        if(srList.size() >0 && srList[0].Type_of_Amendment__c != null) {
            
            if(srList[0].Type_of_Amendment__c == 'Transfer to New Passport (Renewal)'){
                
                bResult = ((srList[0].Type_of_Amendment__c.contains( 'Designation' ))||         
                (srList[0].Type_of_Amendment__c.contains( 'First Name' )) ||
                (srList[0].Type_of_Amendment__c.contains( 'Middle Name' )) ||
                (srList[0].Type_of_Amendment__c.contains( 'Last Name' )) ||    
                (srList[0].Type_of_Amendment__c.contains( 'Company Name' )) ||
                (srList[0].Type_of_Amendment__c.contains( 'Applicant Photograph')))? true : false;
            }
            else if(!srList[0].Type_of_Amendment__c.contains('Transfer of New Passport (Lost Replacement)') && !srList[0].Type_of_Amendment__c.contains('Transfer to New passport (Change of Nationality)')){
                
                bResult = ((srList[0].Type_of_Amendment__c.contains( 'Designation' ))||         
                (srList[0].Type_of_Amendment__c.contains( 'First Name' )) ||
                (srList[0].Type_of_Amendment__c.contains( 'Middle Name' )) ||
                (srList[0].Type_of_Amendment__c.contains( 'Last Name' )) ||    
                (srList[0].Type_of_Amendment__c.contains( 'Company Name' )) ||
                (srList[0].Type_of_Amendment__c.contains( 'Applicant Photograph')))? true : false;
                
            }
            
        }
        
        
         
         system.debug('========CC_VisaAmenedmentPriceController=======bResult =========='+bResult );
        return bResult;
    
    }
      

}