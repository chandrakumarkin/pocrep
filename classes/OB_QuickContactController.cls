/**
    *Author      : Rajil
    *CreatedDate : 10-11-2019
    *Description : This apex class is used to retrieve the business contact details to be shown on the Home Page.

    Merul   v1.0   Homepage Blockage 
-----------------------------------------------------------------------------------------------------------------------
	Modification history
	Version			Modified By			Comments
	v1.0			Shikha				#14282 - Updated wrapper to gender and title to be used on home page
**/
public without sharing class OB_QuickContactController {

    @AuraEnabled
    public static RespondWrap viewContactInfo()
    {

        RespondWrap respWrap = new RespondWrap();
        
        string acctId;
        string ownerId;
        map<string,QuickContact> mapQuickContact = new map<string,QuickContact>();
        //   Merul   v1.0   Homepage Blockage 
        for(User usr:[ 
                        SELECT AccountId,
                              contact.OB_Block_Homepage__c ,
                              Contact.Account.Is_Commercial_Permission__c
                         FROM User 
                        WHERE id=: Userinfo.getUserId()
                     ]
            //
            //0051l000004IUYj
           )
        {
            acctId = usr.AccountId;
            respWrap.loggedInUser = usr;
            respWrap.displayComponent = (usr.Contact.OB_Block_Homepage__c ? false : true); 
        }


        //BD Owner Query
        for(Opportunity opportunityRecord : [Select Owner.Name,OwnerId,Owner.Email,Owner.MobilePhone, Owner.Title 
                                            From Opportunity
                                            where AccountId = :acctId
                                            Order By CreatedDate Desc LIMIT 1]){
            QuickContact quickContactObj = new QuickContact();
            if(opportunityRecord.Owner.Email == Label.OB_Integration_User_Email){
                quickContactObj.name = 'Business Development Team';
            }else{
                quickContactObj.name = opportunityRecord.Owner.Name;
                quickContactObj.email = opportunityRecord.Owner.Email;
                quickContactObj.phoneNumber = opportunityRecord.Owner.MobilePhone; 
                quickContactObj.title = opportunityRecord.Owner.Title;
            }
            
            //ConnectApi.Photo ph =  ConnectApi.UserProfiles.getPhoto(null, opportunityRecord.OwnerId);
            //quickContactObj.profileURL = ph.photoVersionId;
            mapQuickContact.put('Business',quickContactObj);
        }
        //SR Step Owner Query
        for(HexaBPM__Service_Request__c serviceRequest : [select id,
                                                            (select OwnerId,Owner.Name,
                                                                Owner.Email  
                                                                from HexaBPM__Steps_SR__r 
                                                                where Step_Template_Code__c = 'ENTITY_NAME_REVIEW'
                                                                Order By CreatedDate Desc limit 1
                                                            )
                                                             from HexaBPM__Service_Request__c
                                                            where HexaBPM__Customer__c = :acctId
                                                            and recordtype.DeveloperName in ('In_Principle','AOR_Financial')
                                                            Order By CreatedDate Desc LIMIT 1]){
            
            for(HexaBPM__Step__c step :serviceRequest.HexaBPM__Steps_SR__r) {
                
                QuickContact quickContactObj = new QuickContact();
                quickContactObj.name = step.Owner.Name;
                quickContactObj.email = step.Owner.Email;
                //ConnectApi.Photo ph =  ConnectApi.UserProfiles.getPhoto(null, step.OwnerId);
                //quickContactObj.profileURL = ph.fullEmailPhotoUrl;
                if(string.valueOf(step.OwnerId).startsWith('005')){ //Owner is User
                    ownerId = step.OwnerId;
                    //quickContactObj.phoneNumber = step.Owner.MobilePhone;
                }
                mapQuickContact.put('Registry',quickContactObj);
            }
            
        }
        QuickContact tempQuickContact = new QuickContact();
        for(User userRecord : [select MobilePhone from User where id = :ownerId ]){
            tempQuickContact = mapQuickContact.get('Registry');
            tempQuickContact.phoneNumber = userRecord.MobilePhone;
            mapQuickContact.put('Registry',tempQuickContact);
        }

        respWrap.quickContact = mapQuickContact;
        return respWrap;
    }

    /* @AuraEnabled
    public static RespondWrap saveApplicationComment(String requestWrapParam){

        //declaration of wrapper
        RequestWrap reqWrap = new RequestWrap();
        RespondWrap respWrap = new RespondWrap();

        //deseriliaze.
        reqWrap = (RequestWrap) JSON.deserializeStrict(requestWrapParam, RequestWrap.class);


        User portalUser = new User();
        for(User usr:[select AccountId,email,contact.firstName,contact.lastName from User where id=: Userinfo.getUserId()]){
            portalUser = usr;
        }

        Application_Comment__c commObj = new Application_Comment__c();
        commObj.Comment__c = reqWrap.commentMessage;
        commObj.Internal_Email__c = reqWrap.InternalEmail;
        commObj.Portal_user_email__c = portalUser.email;
        commObj.Portal_user_name__c = portalUser.contact.firstName + ' ' + portalUser.contact.lastName;
        commObj.Service_Request__c = reqWrap.RelatedSr;

        try{
            insert commObj;
            respWrap.createdComment =commObj;
        }catch(DMLException e) {
                string DMLError = e.getdmlMessage(0) + '';
                if(DMLError == null) {
                    DMLError = e.getMessage() + '';
                }
                respWrap.errorMessage = DMLError;
            }

        
        return respWrap;


    } */

    @AuraEnabled
public static OB_SRHeaderComponentController.ResponseWrapper loadPageFlow() {
    
    string accId;
    boolean SelfRegistrationUser = false;
    string Entitytype;
    string SectorClassification;
    map<string, HexaBPM__Service_Request__c> rtSerReqMap = new map<string, HexaBPM__Service_Request__c>();
    
    for(User usrObj : OB_AgentEntityUtils.getUserById(userInfo.getUserId())) {
        if(usrObj.ContactId!=null && usrObj.contact.AccountId!=null) {
            accId = usrObj.contact.AccountId;
            Entitytype = usrObj.contact.Account.Company_Type__c;
            SectorClassification = usrObj.contact.Account.OB_Sector_Classification__c;
            if(usrObj.OB_Created_by_User_License__c == 'Guest User License')
            {
                SelfRegistrationUser = true;
            }
        }
    }

    system.debug(accId);
    //declarations
    OB_SRHeaderComponentController.ResponseWrapper respWrap = new OB_SRHeaderComponentController.ResponseWrapper();
    set<string> setPageFlowId = new set<string>();
    // deserilazation of reqWrapPram remaining.
    map<Id, OB_SRHeaderComponentController.PageFlowWrapper> pageFlowWrapMap = new map<Id, OB_SRHeaderComponentController.PageFlowWrapper>();
    map<string, OB_SRHeaderComponentController.PageFlowWrapper> rtPgFlwMap = new map<string, OB_SRHeaderComponentController.PageFlowWrapper>();

    string strQuery = 'select Id,Name,HexaBPM__Master_Object__c,HexaBPM__Preview_Flow__c,HexaBPM__Record_Type_API_Name__c,HexaBPM__Flow_Description__c,SLA_Display__c,Applicable_for_SelfReg_User__c FROM HexaBPM__Page_Flow__c';
    boolean DisplayIndex = true;
    set<string> setExcludeRecordTypes = new set<string>();
    if(SelfRegistrationUser) {
        setExcludeRecordTypes.add('Converted_User_Registration');
    } else {
        setExcludeRecordTypes.add('New_User_Registration');
    }
    if(Entitytype != null && SectorClassification != null) {
        if(Entitytype.tolowercase() == 'Financial - related' || (Entitytype.tolowercase() == 'Non - financial' && SectorClassification == 'Investment Fund')) {
            setExcludeRecordTypes.add('AOR_NF_R');
            setExcludeRecordTypes.add('In_Principle');
        } else {
            setExcludeRecordTypes.add('AOR_Financial');
        }
    }
    
    //for contact picklist
    setExcludeRecordTypes.add('New_User_Registration');
    
    strQuery = strQuery + ' WHERE Display_as_Index__c=:DisplayIndex and HexaBPM__Record_Type_API_Name__c NOT IN:setExcludeRecordTypes ORDER BY Seq__c asc';

    set<string> setRecordTypeNames = new set<string>();
    list<HexaBPM__Page_Flow__c> lstPageFlows = new list<HexaBPM__Page_Flow__c>();
    for(HexaBPM__Page_Flow__c pgf :database.query(strQuery)) {
        setRecordTypeNames.add(pgf.HexaBPM__Record_Type_API_Name__c);
        lstPageFlows.add(pgf);
    }
    //[SELECT Id, HexaBPM__Record_Type_API_Name__c from HexaBPM__Page_Flow__c where Display_as_Index__c = true and HexaBPM__Record_Type_API_Name__c != null]
    system.debug('setRecordTypeNames==>' + setRecordTypeNames);

    if(setRecordTypeNames.size() > 0) {
        map<String, Schema.SObjectType> m = Schema.getGlobalDescribe();
        SObjectType objtype;
        DescribeSObjectResult objDef1;
        map<String, SObjectField> fieldmap;
        if(m.get('HexaBPM__Service_Request__c') != null)
            objtype = m.get('HexaBPM__Service_Request__c');
        if(objtype != null)
            objDef1 = objtype.getDescribe();
        if(objDef1 != null)
            fieldmap = objDef1.fields.getmap();

        set<string> setSRFields = new set<string>();

        if(fieldmap != null) {
            for(Schema.SObjectField strFld :fieldmap.values()) {
                Schema.DescribeFieldResult fd = strFld.getDescribe();
                if(fd.isCustom()) {
                    setSRFields.add(string.valueOf(strFld).toLowerCase());
                }
            }
        }
        string SRqry = 'Select Id,Name';
        if(setSRFields.size() > 0) {
            for(string srField :setSRFields) {
                SRqry += ',' + srField;
            }
        }
        boolean RejectedSR = false;
        SRqry += ' From HexaBPM__Service_Request__c where HexaBPM__Record_Type_Name__c IN:setRecordTypeNames AND HexaBPM__Customer__c=:accId and HexaBPM__Is_Rejected__c=:RejectedSR and HexaBPM__IsCancelled__c=:RejectedSR ORDER BY CreatedDate Desc';

        for(HexaBPM__Service_Request__c srRequest :database.query(SRqry)) {
            if(!rtSerReqMap.containsKey(srRequest.HexaBPM__Record_Type_Name__c.toLowerCase())) {
                rtSerReqMap.put(srRequest.HexaBPM__Record_Type_Name__c.toLowerCase(), srRequest);
            }
        }
    }
    system.debug('lstPageFlows==>' + lstPageFlows);
    for(HexaBPM__Page_Flow__c pgflow :lstPageFlows) {

        map<string, OB_SRHeaderComponentController.PageWrapper> pageWrapMap = new map<string, OB_SRHeaderComponentController.PageWrapper>();
        OB_SRHeaderComponentController.PageFlowWrapper pageFlowWrap = new OB_SRHeaderComponentController.PageFlowWrapper();
        pageFlowWrap.relatedLatestSR = rtSerReqMap.get(pgflow.HexaBPM__Record_Type_API_Name__c.tolowercase());
        pageFlowWrap.pageFlowObj = pgflow;

        string SRId = '';
        string PageTracker;
        if(pageFlowWrap.relatedLatestSR != NULL && pageFlowWrap.relatedLatestSR.Id != NULL) {
            SRId = pageFlowWrap.relatedLatestSR.Id;
            PageTracker = pageFlowWrap.relatedLatestSR.Page_Tracker__c;
            
            if(pageFlowWrap.relatedLatestSR.HexaBPM__Internal_Status_Name__c == 'Draft') {
                pageFlowWrap.ApplicationStatus = 'In Progress';
            }else{
                //pageFlowWrap.ApplicationStatus = 'Completed';//Commented
                pageFlowWrap.ApplicationStatus = pageFlowWrap.relatedLatestSR.HexaBPM__External_Status_Name__c;
            }
            
        } else {
            pageFlowWrap.ApplicationStatus = 'Not Started';
        }
        
        
        // Assigning a wrapper list.
        pageFlowWrap.pageWrapLst = pageWrapMap.values();
        rtPgFlwMap.put(pgflow.HexaBPM__Record_Type_API_Name__c.toLowerCase(), pageFlowWrap);
        pageFlowWrapMap.put(pgflow.Id, pageFlowWrap);
    }
    system.debug('pageFlowWrapMap ---> ' + pageFlowWrapMap);
    //Getting latest SR for each page flow based on record type ID.
    respWrap.pageFlowWrapLst = pageFlowWrapMap.values();
    system.debug(pageFlowWrapMap.values());
    return respWrap;
}

    public class RequestWrap {

        @AuraEnabled public String commentMessage; 
        @AuraEnabled public String portaluserEmail; 
        @AuraEnabled public String InternalEmail; 
        @AuraEnabled public String RelatedSr;
    }

    public class QuickContact{
        @AuraEnabled 
        public string name {get;set;}
        @AuraEnabled 
        public string email {get;set;}
        @AuraEnabled 
        public string phoneNumber {get;set;}
        //v1.0 - Started
        @AuraEnabled
        public string title {get;set;}
        @AuraEnabled
        public string gender {get;set;}
        //v1.0 - Ended
        /*
        @AuraEnabled 
        public string profileURL {get;set;}
        */
    }

    public class RespondWrap {

        @AuraEnabled public user loggedInUser;
        @AuraEnabled public string errorMessage;
        @AuraEnabled public map<string,QuickContact> quickContact; 
        @AuraEnabled public Boolean displayComponent;
        
    }

    
}