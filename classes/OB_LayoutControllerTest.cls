/*
    Author      : PWC
    Date        : 30-March-2020
    Description : Test class for OB_LayoutController 
    --------------------------------------------------------------------------------------
    Version   Author     Date                    Description
    v1.1      Leeba      11 April 2020           Updated the test class to cover new methods
*/
@ isTest
public class OB_LayoutControllerTest {



	public static testmethod void test1() {

		// create account
		List<Account> insertNewAccounts = new List<Account>();
		insertNewAccounts = OB_TestDataFactory.createAccounts(1);
		insert insertNewAccounts;
		//create lead
		List<Lead> insertNewLeads = new List<Lead>();
		insertNewLeads = OB_TestDataFactory.createLead(2);
		insert insertNewLeads;
		//create License Activity Master
		List<License_Activity_Master__c> listLicenseActivityMaster = new List<License_Activity_Master__c>();
		listLicenseActivityMaster = OB_TestDataFactory.createLicenseActivityMaster(1, new List<string>{ 'Non - financial' }, new List<string>{ 'License Activity' });
		insert listLicenseActivityMaster;

		//create lead activity
		List<Lead_Activity__c> listLeadActivity = new List<Lead_Activity__c>();
		listLeadActivity = OB_TestDataFactory.createLeadActivity(2, insertNewLeads, listLicenseActivityMaster);
		insert listLeadActivity;

		//create createSRTemplate
		List<HexaBPM__SR_Template__c> createSRTemplateList = new List<HexaBPM__SR_Template__c>();
		createSRTemplateList = OB_TestDataFactory.createSRTemplate(2, new List<string>{ 'AOR_Financial', 'In_Principle' });
		insert createSRTemplateList;

		//create page flow
		list<HexaBPM__Page_Flow__c> listPageFlow = new list<HexaBPM__Page_Flow__c>();
		listPageFlow = OB_TestDataFactory.createPageFlow('In_Principle', 1);
		insert listPageFlow;

		//create page
		list<HexaBPM__Page__c> listPage = new list<HexaBPM__Page__c>();
		listPage = OB_TestDataFactory.createPageRecords(listPageFlow);
		listPage[0].HexaBPM__VF_Page_API_Name__c = 'Review & Submit';
		listPage[0].Community_Page__c = 'ob-searchentityname';
		insert listPage;

		//create SR status
		list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>();
		listSRStatus = OB_TestDataFactory.createSRStatus(3, new List<string>{ 'Rejected', 'Draft', 'Submitted' }, new List<string>{ 'REJECTED', 'DRAFT', 'SUBMITTED' }, new List<string>{ 'End', '', '' });
		insert listSRStatus;

		//crreate contact
		Contact con = new Contact(LastName = 'testCon', AccountId = insertNewAccounts [0].Id);
		insert con;

		//create SR
		List<HexaBPM__Service_Request__c> insertNewSRs = new List<HexaBPM__Service_Request__c>();
		insertNewSRs = OB_TestDataFactory.createSR(2, new List<string>{ 'In_Principle', 'Name_Check' }, insertNewAccounts, 
												   new List<string>{ 'Non - financial', 'Retail' }, 
												   new List<string>{ 'Foundation', 'Services' }, 
												   new List<string>{ 'Foundation', 'Company' }, 
												   new List<string>{ 'Foundation', 'Recognized Company' });
		insertNewSRs[0].Lead_Id__c = string.valueof(insertNewLeads [0].id).substring(0, 15);
		insertNewSRs[1].Lead_Id__c = string.valueof(insertNewLeads [1].id).substring(0, 15);
		insertNewSRs[1].Setting_Up__c = 'Branch';
		insertNewSRs[0].Setting_Up__c = 'Branch';
		insertNewSRs[1].HexaBPM__customer__c = insertNewAccounts [0].Id;
		insertNewSRs[0].HexaBPM__customer__c = insertNewAccounts [0].Id;
		insertNewSRs[1].Page_Tracker__c = listPage [0].id;
		insertNewSRs[0].Page_Tracker__c = listPage [0].id;
		insertNewSRs[0].Foreign_entity_registered_number__c = '12345';
		insertNewSRs[1].Foreign_entity_registered_number__c = '3234';
		insertNewSRs[0].HexaBPM__Submitted_DateTime__c = datetime.now();
		insertNewSRs[1].HexaBPM__Internal_SR_Status__c = listSRStatus [0].id;
		insert insertNewSRs;
		insertNewSRs[1].HexaBPM__Parent_SR__c = insertNewSRs [0].id;
		update insertNewSRs;

		List<Company_Name__c> companies = OB_TestDataFactory.createCompanyName(new List<String>{ 'Com1', 'Com2' });
		companies[0].Application__c = insertNewSRs [0].id;
		companies[0].Status__c = 'Approved';
        companies[0].OB_Is_Trade_Name_Allowed__c = 'Yes';
		insert companies;

		test.startTest();


		Id p = [select id from profile where name = 'DIFC Customer Community Plus User Custom'].id;


		User user = new User(alias = 'test123', email = 'test123@noemail.com', 
								     emailencodingkey= 'UTF-8', lastname = 'Testing', languagelocalekey = 'en_US', 
								     localesidkey= 'en_US', profileid = p, country = 'United States', IsActive = true, 
								     ContactId = con.Id, 
								     timezonesidkey= 'America/Los_Angeles', username = 'tester@noemail.com');

		insert user;
		system.runAs(user) {


			user tester = OB_LayoutController.getUserInfo();

			OB_LayoutController.RequestWrap reqWrapper = new OB_LayoutController.RequestWrap();
			OB_LayoutController.RespondWrap repWrapper = new OB_LayoutController.RespondWrap();

			reqWrapper.pageflowId = listPageFlow [0].Id;
			reqWrapper.srId = insertNewSRs [0].Id;
			String requestString = JSON.serialize(reqWrapper);
			repWrapper = OB_LayoutController.fetchPageFlowWrapper(requestString);
            repWrapper = OB_LayoutController.difcTabsGetInfo(requestString);
            repWrapper = OB_LayoutController.getRelation();

		}

	}
}