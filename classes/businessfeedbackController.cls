public class businessfeedbackController {
@TestVisible private Feedback_Form__c feedback{get;set;}    
public businessfeedbackController(ApexPages.StandardController ctrl)
{
    feedback = (Feedback_Form__c)ctrl.getRecord();
}    
  public boolean isvalid{get;set;}
    string Contactid;
public void pageLoad(){
    
    isvalid=true;
    string ids = ApexPages.currentPage().getParameters().get('cid');
    string Eid = ApexPages.currentPage().getParameters().get('eid');
    Contactid=ids;
    feedback.Email__c=Eid;
   
    if(string.isblank(ids)==false && string.isblank(Eid )==false)
    {     
        

    List<Contact> ct = [select id, FirstName,LastName from Contact where id = :ids]; 
     if(ct.size()<1)
     {   
       
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You are not a valid Contact'));  
         isvalid=false;
    }
        else
        {
          feedback.Contact__c=ct[0].id;
            List<Feedback_Form__c> fb = [select id,Name,Contact__c from Feedback_Form__c where Contact__c = :Contactid];
            if(fb.size()>0)
            { 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,'You have already submitted the Information'));
           
            isvalid=false;
             }   
        }
   
    } 
    
}  
   
Public PageReference SaveMe()
{          
    

    List<contact> c = [select id,Name,AccountId from Contact where id =:Contactid and AccountId != Null];
        if(c.size()>0){
        feedback.Account__c = c[0].AccountId;        
        }  
            
        upsert feedback;         
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Confirm,'Your Information has been saved successfully.'));
        isvalid=false;
            return Null;    
        
           
        }

    
}