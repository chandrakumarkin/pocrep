/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class Test_GS_Utils {
	
	private static Id testGSAccountId;
	
	private static Id testGSLicenseId;
	
	private static Id testGSIdentificationId;

    public static void setGSAccountId(Id accountId){
		testGSAccountId = accountId; 
	}
	
	public static void setGSLicenseId(Id licenseId){
		testGSLicenseId = licenseId; 
	}
	
	public static void setGSIdentificationId(Id idenfiticationId){
		testGSIdentificationId = idenfiticationId; 
	}
	
	public static Id getGsAccountId(){
		return testGSAccountId;
	}
	
	public static Id getLicenseId(){
		return testGSIdentificationId;
	}
	
	public static Id getGsIdentificationId(){
		return testGSIdentificationId;
	}
	
	public static void testGSPoBoxController(){
		
		setGSAccountId([SELECT Id FROM Account WHERE Name LIKE :'%'+Test_CC_FitOutCustomCode_DataFactory.TEST_CUSTOMER_NAME+'%'].Id);
		setGSIdentificationId([SELECT Id FROM Identification__c WHERE ID_No__c = :Test_GS_DataFactory.DEFAULT_IDENTIFICATION_NUMBER].Id);
		setGSLicenseId([SELECT Id FROM License__c WHERE Account__c = :getGsAccountId()].Id);
		
		ApexPages.StandardController testStd = new ApexPages.StandardController(new Service_Request__c());
        
        GSPoBoxController testInst = new GSPoBoxController(testStd);
		
		testStd = new ApexPages.StandardController(new Service_Request__c(Service_Category__c = 'New'));
		
		testInst = new GSPoBoxController(testStd);
		
		testInst.accountName = Test_CC_FitOutCustomCode_DataFactory.TEST_CUSTOMER_NAME;
		
		testInst.getAccountNames();
		
		System.debug('Accounts name filter >> ' + testInst.accountName);
		System.debug('Accounts list >> ' + testInst.accountsList);
		System.debug('Accounts ID >> ' + getGsAccountId());
		
		testInst.accountId = getGsAccountId();
		
		testInst.setAccountId();
		testInst.checkCategory();
		
	}
	
}