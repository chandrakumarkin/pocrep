public class EmergencyContactPreviewPageCtrl {
	
    @AuraEnabled
    public static previewWrapper getDetails (String recordId){
        previewWrapper previewObj = new previewWrapper();
        List<Recepient__c> recepientList = [SELECT Id, Contact__c, Contact__r.Name, Phone__c, Email__c, Communication_Trail__c, AccountLookup__c, 
                                            AccountLookup__r.Name, Entity_Type__c,
                                           	Building_Name__c,UniqueId__c FROM Recepient__c WHERE Communication_Trail__c = :recordId];
        if(String.isNotBlank(recordId)){
            CommunicationTrail__c communicationObject = [SELECT Id,Name,Building_Name__c, Body__c, Channel__c, Entity_Type__c, SMS_Body__c, Subject__c,
                                                         Status__c,Email_Template__c, AdditionalEmails__c	
                                                         FROM CommunicationTrail__c
                                                         WHERE Id = :recordId];
            system.debug('### commObj '+communicationObject);
            previewObj.commObject = communicationObject;
        }            
        previewObj.recepientList = recepientList;
        previewObj.countOfRecepients = recepientList.size();
        return previewObj;
    }
    
    @AuraEnabled
    public static Map<String,String> sendMessage (List<Recepient__c> recepientList, CommunicationTrail__c commObj, String additionalEmails){    
        Map<String,String> responseMap = new Map<String,String>();
        Map<String,String> smsResponseMap = new Map<String,String>();
        try{
        system.debug('### recepientList '+recepientList);
        system.debug('### commObj '+commObj);  
            if(recepientList.isEmpty()){	
               responseMap.put('Error','Please select criteria to have atleast 1 recepient');	
               return responseMap;	
            }
        String[] addressList = new List<String>();
        String[] phoneNumberList = new List<String>();
        String recepientIds = '';
        List<Id> recepientIdList = new List<Id>();
        List<Id> contactIdList = new List<id>();
        for(Recepient__c recepObj : recepientList){
            addressList.add(recepObj.Email__c);
            phoneNumberList.add(recepObj.Phone__c);
            recepientIdList.add(recepObj.Id);
            contactIdList.add(recepObj.Contact__c);
        }
        recepientIds = String.join(recepientIdList, ',');
        system.debug('### addressList '+addressList);
            //Shikha - adding additionalEmail functionality
        String[] additionalEmailList = String.isNotBlank(additionalEmails) ? additionalEmails.split(';') : new list<String>();
            if(!additionalEmailList.isEmpty()){
                addressList.addAll(additionalEmailList);
            }
            system.debug('#### addressList '+addressList);
        if(commObj.Channel__c == 'Email' || commObj.Channel__c == 'Both'){
            if(String.isBlank(commObj.Email_Template__c )){
                responseMap = sendEmailCommunication(commObj,addressList);
            }
            else{
                responseMap = sendEmailTemplateCommunication(commObj,addressList,contactIdList);
                system.debug('email Response in send button '+responseMap);
            }
        }        
        //if(commObj.Channel__c == 'SMS' || commObj.Channel__c == 'Both'){
            if(commObj.Channel__c == 'Both'&& responseMap != null && responseMap.containsKey('Success')){
                smsResponseMap = sendSMSCommunication(commObj,phoneNumberList);
            }
            else if(commObj.Channel__c == 'SMS'){
                smsResponseMap = sendSMSCommunication(commObj,phoneNumberList);
            }
        //}
        if(smsResponseMap != null && smsResponseMap.containsKey('Error')){
            system.debug('### in sms response map ');
            responseMap = smsResponseMap;   
        }
        if(commObj != null && responseMap != null && responseMap.containsKey('Success')){
            commObj.Status__c = 'Submitted';
            update commObj;
        }  
        List<Recepient__c> existingRecepients = [SELECT Id, Communication_Trail__c FROM Recepient__c WHERE Communication_Trail__c=:commObj.Id];
        List<Recepient__c> deleteRecepientList = new List<Recepient__c>();
        for(Recepient__c recepient : existingRecepients ){
            if(!recepientIds.contains(String.valueOf(recepient.Id))){
                deleteRecepientList.add(recepient);
            }
        }
        system.debug('### deleteRecepientList '+deleteRecepientList);
        if(!deleteRecepientList.isEmpty()){
            delete deleteRecepientList;
        }
         //Store additional Emails 
            //if(String.isNotBlank(additionalEmails)){
                commObj.AdditionalEmails__c = additionalEmails;
                update commObj;
            //}  
        }
        catch(Exception e){
            system.debug('### Exception '+e.getMessage());
            responseMap.put('Error','Something went wrong. Please connect with system administrator');
        }
        return responseMap;
    }
	
    @AuraEnabled
    public static Map<String,String> sendSampleCommunication (String emailId, String phoneNumber, String recordId){    
        Map<String,String> responseMap = new Map<String,String>();
        Map<String,String> smsResponseMap = new Map<String,String>();
        try{
        CommunicationTrail__c communicationObject = [SELECT Id,Name,Building_Name__c, Body__c, Channel__c, Entity_Type__c, 
                                                     SMS_Body__c, Subject__c,Email_template__c
                                                     FROM CommunicationTrail__c WHERE Id = :recordId];
        if(String.isNotBlank(emailId) && String.isBlank(communicationObject.Email_Template__c)){
            String[] toAddresses = new List<String>{emailId};
            responseMap = sendEmailCommunication(communicationObject,toAddresses);
        } 
        else if(String.isNotBlank(emailId)){
            String[] toAddresses = new List<String>{emailId};
            responseMap = sendEmailTemplateCommunication(communicationObject,toAddresses,new List<id>{});
            system.debug('email Response '+responseMap);
        }
        if(String.isNotBlank(phoneNumber)){
            system.debug('### in phone Number');
            List<String> phoneNumberList = new List<String>{phoneNumber};            
            //sendSMSCommunication(communicationObject,phoneNumberList);
            if(String.isNotBlank(emailId) && responseMap != null && responseMap.containsKey('Success')){
                smsResponseMap = sendSMSCommunication(communicationObject,phoneNumberList);
            }
            else if(String.isBlank(emailId)){
                smsResponseMap = sendSMSCommunication(communicationObject,phoneNumberList);
            }
        }
        if(smsResponseMap != null && smsResponseMap.containsKey('Error')){
                system.debug('in smsResponseMap');
                responseMap = smsResponseMap;
            }
            system.debug('### response map '+responseMap);
        }
        catch(Exception e){
            responseMap.put('Error','Something went wrong. Please contact your system administrator');
        }
        return responseMap;
    }
    
    public static Map<String,String> sendEmailCommunication(CommunicationTrail__c commObj, String[] toAddressList){
        Map<String,String> emailResponseMap = new Map<String,String>();
        Set<Id> contentDocIdSet = new Set<Id>();
        Set<Id> contentVersionIdSet = new Set<Id>();
        List<ContentVersion> contentVersionList = new List<ContentVersion>();
        try{
        for(ContentDocumentLink contentObj : [SELECT ContentDocumentId, ContentDocument.Title 
                                                       FROM ContentDocumentLink 
                                                       WHERE LinkedEntityId = :commObj.Id]){
            contentDocIdSet.add(contentObj.ContentDocumentId);
        }
        if(!contentDocIdSet.isEmpty()){
            for(ContentDocument doc : [SELECT Id, LatestPublishedVersionId FROM ContentDocument WHERE Id IN :contentDocIdSet]){
                contentVersionIdSet.add(doc.LatestPublishedVersionId);
            }
        }
        if(!contentVersionIdSet.isEmpty()){
            contentVersionList = [SELECT Id, Title, VersionData, FileType, FileExtension,ContentSize FROM ContentVersion WHERE Id IN :contentVersionIdSet];
        }
        //Adding validation for file formats
        	AppConfig__c appConfigObj = AppConfig__c.getValues('Attachment Accepted Formats');
        	String[] acceptedFormats = (appConfigObj != null && appConfigObj.AppValue__c != null) ? appConfigObj.AppValue__c.split(',') : new List<String>();
            system.debug('### acceptedFormats '+acceptedFormats);
            //String[] acceptedFormats = new List<String>{'png','jpg','jpeg','pdf','xlsx','doc','docx','gif','xls'};
            Boolean noAcceptedFormat = false;
            Boolean maxSize = false;
            for(ContentVersion cotVersion : contentVersionList){
                system.debug('### check accepted format condition '+acceptedFormats.contains(cotVersion.FileExtension));
                if(!acceptedFormats.contains(cotVersion.FileExtension)){
                    noAcceptedFormat = true;
                    break;
                }
                if(cotVersion.ContentSize > 5242880){
                	maxSize = true;
                    break;
                }
            }
            system.debug('### noAcceptedFormat '+noAcceptedFormat);
            if(noAcceptedFormat){
                emailResponseMap.put('Error','Please enter valid attachment formats');
                system.debug('### in validation');
                return emailResponseMap;
            }
            if(maxSize){
                emailResponseMap.put('Error','Please upload attachment having size less than 5 MB.');
                system.debug('### in validation');
                return emailResponseMap;
            }
        List<String> addressList = new List<String>();
        //addressList.add('shikhakhanuja@gmail.com');
        //addressList.add('c-shikha.khanuja@difc.ae');
        addressList.addAll(toAddressList);
        	//SetorgWide Address
        	OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply.portal@difc.ae'];
            List<Messaging.SingleEmailMessage> sEmailList = new List<Messaging.SingleEmailMessage>();
            for(String addr : addressList){
                Messaging.SingleEmailMessage sEmail = new Messaging.SingleEmailMessage();
                sEmail.setToAddresses(new List<String> {addr});        
                //sEmail.setBccAddresses(addressList);
                //set from address
                if(!owea.isEmpty()){
                    sEmail.setOrgWideEmailAddressId(owea[0].Id);
                }
                sEmail.setSubject(commObj.Subject__c);
                sEmail.setHtmlBody(commObj.Body__c);
                if(!contentVersionList.isEmpty()){
                    List<Messaging.Emailfileattachment> fileAttachments = new List<Messaging.Emailfileattachment>();
                    for(ContentVersion cotVersion : contentVersionList){
                        Blob emailBody = cotVersion.VersionData;
                        Messaging.Emailfileattachment efa = new Messaging.Emailfileattachment(); 
                        efa.body = emailBody;
                        String contentTypeStr = getContentMimeType(cotVersion.FileType);
                        system.debug('### contentTypeStr '+contentTypeStr);
                        if(String.isNotBlank(contentTypeStr)){
                            efa.setContentType(contentTypeStr);
                        }                    
                        String fileName = cotVersion.Title+'.'+cotVersion.FileExtension;
                        system.debug('### fileName '+fileName);
                        efa.setFileName(fileName);
                        fileAttachments.add(efa);
                    }
                    sEmail.setFileAttachments(fileAttachments);
            	}
            system.debug('### sEmail '+sEmail);
                sEmailLisT.add(sEmail);
            }
            Messaging.sendEmail(sEmailLisT); 
            emailResponseMap.put('Success','Communication sent successfully');
        }
        catch(Exception e){
            system.debug('### exception in send Email '+e.getMessage());
            system.debug('### stack trace '+e.getStackTraceString());
            emailResponseMap.put('Error','Something went wrong. Please contact your system administrator');
        }
        return emailResponseMap;
    }
    
    public static Map<String,String> sendSMSCommunication(CommunicationTrail__c commObj, List<String> phoneNumberList){
        String[] addressList = new List<String>();
        //Setting SMS Email Service in custom setting
        Map<String,String> emailResponseMap = new Map<String,String>();
        try{
        AppConfig__c appConfigObj = AppConfig__c.getValues('SMS Configuration');
        addressList.add(appConfigObj.AppValue__c);
        Messaging.SingleEmailMessage[] sendSMSList = new List<Messaging.SingleEmailMessage>();        
        for(String str : phoneNumberList){
            Messaging.SingleEmailMessage sEmail = new Messaging.SingleEmailMessage();
        	sEmail.setToAddresses(addressList);        
        	sEmail.setSubject('Phone='+str);
        	sEmail.setPlainTextBody(commObj.SMS_Body__c);        
        	system.debug('### sms '+sEmail);
            sendSMSList.add(sEmail);
        }        
        Messaging.sendEmail(sendSMSList);  
        emailResponseMap.put('Success','Communication sent successfully');
        }
        catch(Exception e){
            system.debug('### exception in send Email '+e.getMessage());
            emailResponseMap.put('Error','Something went wrong. Please contact your system administrator');
        }
        return emailResponseMap;
    }
    
    public static Map<String,String> sendEmailTemplateCommunication (CommunicationTrail__c commTrail, String[] addressList,List<Id> recepientIdList){
        Map<String,String> responseMap = new Map<String,String>();
        String EmailTemplateName = commTrail.Email_Template__c;
        AppConfig__c appConfigObj = AppConfig__c.getValues('Attachment Accepted Formats');
        String[] acceptedFormats = (appConfigObj != null && appConfigObj.AppValue__c != null) ? appConfigObj.AppValue__c.split(',') : new List<String>();
            //new List<String>{'png','jpg','jpeg','pdf','xlsx','doc','docx','gif','xls'};
        if(String.isNotBlank(EmailTemplateName)){
            List<EmailTemplate> templateObj = [SELECT Id, Name, Subject, HtmlValue,Body FROM EmailTemplate WHERE Name = :EmailTemplateName];
            system.debug('### templateObj in Email template '+templateObj);
            if(templateObj.isEmpty()){
                responseMap.put('Error','There is no Email template associated with entered Name. Please enter valid template name');
            }
            else{
                try{
                List<Messaging.SingleEmailMessage> sEmailList = new List<Messaging.SingleEmailMessage>();
                List<Attachment> attachmentList = [SELECT Id, Name, Body, ContentType, BodyLength FROM Attachment WHERE ParentId = :templateObj[0].Id ];
                system.debug('### attachmentList '+attachmentList);
                if(recepientIdList.isEmpty()){
                    Messaging.SingleEmailMessage sEmail = new Messaging.SingleEmailMessage();                
                    sEmail.setToAddresses(addressList);  
                    sEmail.setTemplateId(templateObj[0].Id);
                    sEmail.setSubject(templateObj[0].Subject);
                    sEmail.setHtmlBody(templateObj[0].HtmlValue);
                    if(!attachmentList.isEmpty()){
                    List<Messaging.EmailFileAttachment> mailAttList = new List<Messaging.EmailFileAttachment>();
                        Boolean noAcceptedFormat = false;	
                    Boolean maxSize = false;	
                    	
                    for(Attachment att : attachmentList){	
                    	String fileName =  att.Name;	
                        system.debug('### fileName '+fileName);	
                        List<String> fileNameithExtension = fileName.split('\\.');	
                        system.debug('### fileNameithExtension '+fileNameithExtension);	
    					String fileExtension = fileNameithExtension[fileNameithExtension.size() - 1];	
                        system.debug('### fileExtension '+fileExtension);	
                        if(!acceptedFormats.contains(fileExtension)){	
                    		noAcceptedFormat = true;	
                    		break;	
                		}	
                        if(att.BodyLength > 5242880){	
                			maxSize = true;	
                    		break;	
                		}	
                    }	
                    system.debug('### noAcceptedFormat '+noAcceptedFormat);	
                    if(noAcceptedFormat){	
                        responseMap.put('Error','Please enter valid attachment formats');	
                        system.debug('### in validation');	
                        return responseMap;	
                    }	
                    if(maxSize){	
                		responseMap.put('Error','Please upload attachment having size less than 5 MB.');	
                		system.debug('### in validation');	
                		return responseMap;	
            		}	

                    for(Attachment att : attachmentList){
                        Messaging.EmailFileAttachment email_att = new Messaging.EmailFileAttachment();
    					email_att.setBody(att.Body);
    					email_att.setContentType(att.ContentType);
    					email_att.setFileName(att.Name);
    					email_att.setinline(false);
    					mailAttList.add(email_att);
                    }
                    sEmail.setFileAttachments(mailAttList);
                    sEmailList.add(sEmail);
                    //Messaging.sendEmail(sEmailList); 
                }
                    if(sEmailList.isEmpty()){	
                    	Messaging.sendEmail(sEmailList);     	
                        responseMap.put('Success','Communication has been sent successfully');	
                    } 
                }
                
                Integer i=0;
                if(!recepientIdList.isEmpty()){
                    Messaging.MassEmailMessage mail = new Messaging.MassEmailMessage();
       				mail.setTargetObjectIds(recepientIdList);
       				mail.setTemplateId(templateObj[0].Id);
                    Boolean noAcceptedFormat = false;	
                    Boolean maxSize = false;	
                		
                    for(Attachment att : attachmentList){	
                    	String fileName =  att.Name;	
                        system.debug('### fileName '+fileName);	
                        List<String> fileNameithExtension = fileName.split('\\.');	
                        system.debug('### fileNameithExtension '+fileNameithExtension);	
    					String fileExtension = fileNameithExtension[fileNameithExtension.size() - 1];	
                        system.debug('### fileExtension '+fileExtension);	
                        if(!acceptedFormats.contains(fileExtension)){	
                    		noAcceptedFormat = true;	
                    		break;	
                		}	
                        if(att.BodyLength > 5242880){	
                			maxSize = true;	
                    		break;	
                		}	
                    }	
                    system.debug('### noAcceptedFormat '+noAcceptedFormat);	
                    if(noAcceptedFormat){	
                        responseMap.put('Error','Please enter valid attachment formats');	
                        system.debug('### in validation');	
                        return responseMap;	
                    }	
                    if(maxSize){	
                		responseMap.put('Error','Please upload attachment having size less than 5 MB.');	
                		system.debug('### in validation');	
                		return responseMap;	
                    }	
                    responseMap.put('Success','Communication has been sent successfully');
                    Messaging.sendEmail(new Messaging.MassEmailMessage[] { mail });
                
                }	                                
                }
                catch(Exception e){
                    system.debug('### exception in send Email '+e.getMessage());
                    system.debug('### exception stack trace '+e.getStackTraceString());
            		responseMap.put('Error','Something went wrong. Please contact your system administrator');
                }
            }
        }
        return responseMap;
    }
    
    public static String getContentMimeType (String fileType){
        String mimeType;
        Map<String,String> mimeTypeMap = new Map<String,String> {
        	'bin,dms,lrf,mar,so,dist,distz,pkg,bpk,dump,elc,deploy' => 'application/octet-stream',
            'json'=>'application/json',
            'pdf'=> 'application/pdf',
            'bmp'=>'image/bmp',
			'cgm'=>'image/cgm',
			'g3'=>'image/g3fax',
			'gif'=>'image/gif',
			'ief'=>'image/ief',
            'jpeg'=>'image/jpeg',
            'jpe'=>'image/jpeg',
            'ktx'=>'image/ktx',
            'png'=>'image/png',
            'btif'=>'image/prs.btif',
            'sgi'=>'image/sgi',
            'svg'=>'image/svg+xml',
            'svgz'=>'image/svg+xml',
            'tiff'=>'image/tiff',
            'tif'=>'image/tiff',
            'psd'=>'image/vnd.adobe.photoshop',
            'uvi,uvvi,uvg,uvvg'=>'image/vnd.dece.graphic',            
            'sub'=>'image/vnd.dvb.subtitle',            
            'image/x-rgb'=>'rgb',
            'txt'=>'text/plain',
            'text'=>'text/plain',
            'list'=>'text/plain',
            'log'=>'text/plain',
            'in'=>'text/plain',
            'zip'=> 'application/zip',
            'bz'=>'application/x-bzip',
            'rar'=>'application/x-rar-compressed',
            'tar'=>'application/x-tar'
        };        				 				
        if(mimeTypeMap.containsKey(fileType)){
            mimeType = mimeTypeMap.get(fileType);
        }
        else{
            mimeType = '';
        }
        return mimeType;
    }
    
    
    public class previewWrapper{
        @AuraEnabled
        public CommunicationTrail__c commObject{get;set;}        
        
        @AuraEnabled
        public Integer countOfRecepients {get;set;}
        
        @AuraEnabled
        public List<Recepient__c> recepientList {get;set;}
            
    }
}