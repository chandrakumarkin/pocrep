@istest
public class CarParking_BatchTest {
    
    static testMethod void testExpiryBatch(){
		Account acc = createAccount();
		Contact objContact = createContact(acc.Id);  
      	Relationship__c objRel = createRelationship(acc.Id,objContact.Id); 
	  	Service_Request__c sr = createSR(acc.Id,objContact.Id,GlobalConstants.TENANTS,GlobalConstants.REMOVE_VEHICLE);                   	
      	insert sr;   
        Membership__c membershipId = createMembership(sr,GlobalConstants.MEMBERSHIP,'Active',sr.Id);
      	membershipId.Card_Number__c = 'ABC123';
        membershipId.ToDate__c = Date.today() - 1;
      	update membershipId;
        system.debug('### membershipId '+membershipId);
      	Map<String,String> inputParams = new Map<String,String>();
      	inputParams.put('Status','Active');
      	inputParams.put('contactId',objContact.Id);
      	inputParams.put('membershipId',membershipId.Id);
      	inputParams.put('accountId',acc.id); 
      	Car_Parking__c carPark = createParking(inputParams, GlobalConstants.MEMBERSHIP);
        
        List<Membership__c> objMembership = [SELECT Id, MembershipStatus__c, Contact__c,Contact__r.AccountId, Service_Request__r.Customer__c, Service_Request__c, FromDate__c, ToDate__c 
                                       FROM Membership__c 
                                       WHERE ToDate__c <= YESTERDAY AND RecordType.Name = 'Membership' AND MembershipStatus__c != 'Expired'];
        system.debug('## objMembership '+objMembership);
        Test.startTest();
        Database.executeBatch(new CarParking_ExpiryBatch());   
        
        Test.stopTest();
    }
    
    static testMethod void testActivationBatch(){
		Account acc = createAccount();
		Contact objContact = createContact(acc.Id);  
      	Relationship__c objRel = createRelationship(acc.Id,objContact.Id); 
	  	Service_Request__c sr = createSR(acc.Id,objContact.Id,GlobalConstants.TENANTS,GlobalConstants.REMOVE_VEHICLE);                   	
      	insert sr;   
        Membership__c membershipId = createMembership(sr,GlobalConstants.MEMBERSHIP,'Active',sr.Id);
      	membershipId.Card_Number__c = 'ABC123';
        membershipId.Effective_Start_Date__c = Date.today(); 
        membershipId.MembershipStatus__c = 'Submitted';
      	update membershipId;
        system.debug('### membershipId '+membershipId);
      	Map<String,String> inputParams = new Map<String,String>();
      	inputParams.put('Status','Active');
      	inputParams.put('contactId',objContact.Id);
      	inputParams.put('membershipId',membershipId.Id);
      	inputParams.put('accountId',acc.id); 
      	Car_Parking__c carPark = createParking(inputParams, GlobalConstants.MEMBERSHIP);
        
        List<Membership__c> objMembership = [SELECT Id, MembershipStatus__c, Contact__r.AccountId, Service_Request__c, Service_Request__r.Customer__c, FromDate__c, ToDate__c 
                                             FROM Membership__c 
                                             WHERE Effective_Start_Date__c = TODAY AND ToDate__c > TODAY AND RecordType.Name ='Membership'];
        system.debug('## objMembership act '+objMembership);
        Test.startTest();
        Database.executeBatch(new CarParkingActivationBatch());                                     
        Test.stopTest();
    }
    
    private static Account createAccount(){
 		 Account acc = new Account();
      	 acc.Name = 'Test Account';
      	 insert acc;
        return acc;
    }
    
    private static Contact createContact(String AccountId){
        Contact objContact = new Contact();
        objContact.LastName = 'Test Contact';
        objContact.Email = 'test@difctesst.com';
        objContact.MobilePhone = '+9711234567';
        objContact.Occupation__c = 'Director';
        objContact.Salutation = 'Mr.';
        objContact.FirstName = 'Test';
        objContact.Nationality__c ='India';
        objContact.Birthdate = system.today().addYears(-24);
        objContact.RELIGION__c = 'Hindu';
        objContact.Marital_Status__c = 'Single';
        objContact.Gender__c = 'Male';
        objContact.Qualification__c = 'B.Tech';
        objContact.Mothers_Name__c = 'My Mom';
        objContact.Gross_Monthly_Salary__c = 10000;
        objContact.ADDRESS2_LINE1__c = 'Test Street';
        objContact.Country__c = 'India';
        objContact.AccountId = AccountId;
        objContact.RecordTypeID = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert objContact;
        return objContact;
    }
    
    private static Relationship__c createRelationship(String accountId, String contactId){
        Relationship__c objRel = new Relationship__c();
        objRel.Active__c = true;
        objRel.Subject_Account__c = accountId;
        objRel.Object_Contact__c = contactId;
        objRel.Relationship_Type__c = 'Has Manager'; 
        objRel.Start_Date__c = system.today();
        objRel.Push_to_SAP__c = true;
        objRel.Relationship_Group__c = 'RoC';
        insert objRel;
        return objRel;
    }
    
    private static Service_Request__c createSR(String accId, String conId, String reqType, String type){
        SR_Status__c srStatus = new SR_Status__c ();
      	srStatus.name  = 'Submitted';
      	srStatus.Code__c = 'Submitted';
      	insert srStatus;
        
        Service_Request__c sr = new Service_Request__c();
      	sr.RecordTypeID = Schema.SObjectType.Service_Request__c.getRecordTypeInfosByName().get('Car Parking').getRecordTypeId();
      	sr.Customer__c = accId;
      	sr.Contact__c = conId;
      	sr.Commercial_Activity__c = reqType;
      	sr.Client_s_Representative_in_Charge__c = type;        
        sr.Incorporated_Organization_Name__c = 'ABC123';
        sr.Mortgage_Start_Date__c = System.today();
      	sr.Agreement_Date__c = System.today();
      	sr.Mortgage_End_Date__c = System.today().addDays(15);
	  	sr.Commercial_Activity_Previous_Sponsor__c = 'Days';
	  	sr.Mortgage_Amount__c = 15;  
        sr.Submitted_Date__c = system.today();
      	sr.Submitted_DateTime__c = system.now();
      	sr.Internal_SR_Status__c = srStatus.ID;
      	sr.External_SR_Status__c = srStatus.ID;
      	sr.finalizeAmendmentFlg__c  = true;
      	sr.Incorporated_Organization_Name__c = 'ABC123'; 
      	sr.Activity_NOC_Required__c = false;
        return sr;
    }
    
	private static Car_Parking__c createParking(Map<String,String> inputParams, String requestType){
        Car_Parking__c carPark = new Car_Parking__c();
      	carPark.Status__c = inputParams.get('Status') ;
      	carPark.Vehicle_Manufacturer__c = 'Honda';
      	carPark.Applicant__c = inputParams.get('contactId');
        carPark.Membership__c = inputParams.get('membershipId');
        carPark.Vehicle_Color__c = 'White';
       	carPark.GD_GV__c = 'GD';
        carPark.Category__c = 'Dubai';
        carPark.Vehicle_Type__c = 'Sedan';
        carPark.Vehicle_Number__c = '1234';
        carPark.Customer__c = inputParams.get('accountId');
        carPark.Paid__c = requestType != null && requestType == GlobalConstants.TENANTS ? false : true ;
      	insert carPark;
        return carPark;
    }
    
    private static Membership__c createMembership(Service_Request__c objSR, String requestType, String status, String srId){        
        Membership__c objMem = new Membership__c();               
            objMem.Contact__c = objSR.Contact__c;
            objMem.FromDate__c = objSR.Mortgage_Start_Date__c;
            objMem.Effective_Start_Date__c = objSR.Agreement_Date__c;
            objMem.ToDate__c = objSR.Mortgage_End_Date__c;
            objMem.Period__c = objSR.Commercial_Activity_Previous_Sponsor__c;
            objMem.Tenure__c = objSR.Mortgage_Amount__c;
            objMem.Card_Number__c = objSR.Incorporated_Organization_Name__c;
        	objMem.MembershipStatus__c = status;  
        	objMem.Service_Request__c = srId;
            if(requestType == GlobalConstants.TENANTS){
                Id recordTypeId = Schema.SObjectType.Membership__c.getRecordTypeInfosByName().get(GlobalConstants.TENANTS).getRecordTypeId(); 
                objMem.RecordTypeId = recordTypeId;                
            }
            else{
                Id recordTypeId = Schema.SObjectType.Membership__c.getRecordTypeInfosByName().get(GlobalConstants.MEMBERSHIP).getRecordTypeId(); 
                objMem.RecordTypeId = recordTypeId;
            }
            system.debug('### membership object created '+objMem);
            insert objMem;
     		return objMem;   
    }
    
}