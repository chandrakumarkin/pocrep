@isTest
public class CC_CPPerformFinalUpdatesTest {
    @testSetup
    static  void createTestData() {
       Account acc  = new Account();
       acc.name = 'test';      
       insert acc;
             
       HexaBPM__SR_Template__c objsrTemp = new HexaBPM__SR_Template__c();
       objsrTemp.HexaBPM__Menu__c = 'Company Services';
       objsrTemp.HexaBPM__SR_RecordType_API_Name__c = 'Commercial_Permission';
       insert objsrTemp;
       
       list<HexaBPM__SR_Status__c> listSRStatus = new list<HexaBPM__SR_Status__c>() ;
        listSRStatus = OB_TestDataFactory.createSRStatus(2, new List<string> {'Draft','Expired'}, new List<string> {'DRAFT','EXPIRED'}, new List<string> {'End',''});
        insert listSRStatus;
       Id SRRecId = Schema.SObjectType.HexaBPM__Service_Request__c.getRecordTypeInfosByName().get('New Commercial Permission').getRecordTypeId();
    
        HexaBPM__Service_Request__c objHexaSR = new HexaBPM__Service_Request__c();
        objHexaSR.Entity_Name__c = 'test';
        objHexaSR.RecordtypeId = SRRecId;
        objHexaSR.Type_of_commercial_permission__c='Gate Avenue Pocket Shops';
        objHexaSR.HexaBPM__Customer__c = acc.id;
        objHexaSR.HexaBPM__SR_Template__c = objsrTemp.Id;
        objHexaSR.HexaBPM__Email__c = 'test@test.com';
        objHexaSR.HexaBPM__External_SR_Status__c = listSRStatus[0].Id;
        objHexaSR.HexaBPM__Internal_SR_Status__c = listSRStatus[0].Id;
        objHexaSR.Authorized_Signatory_Combinations__c = 'AS Combination';
        insert objHexaSR;
        
        HexaBPM__Step__c objHexastep = new HexaBPM__Step__c();
        objHexastep.HexaBPM__Start_Date__c = system.Today();
        objHexastep.HexaBPM__SR__c = objHexaSR.Id;
        insert objHexastep;
    }

    static testMethod void executeBatchTest() {
        List<HexaBPM__Service_Request__c> srList = [Select id,CreatedDate from HexaBPM__Service_Request__c]; 
        List<HexaBPM__Step__c> stepList = [Select id,CreatedDate,HexaBPM__SR__c,HexaBPM__SR__r.HexaBPM__Customer__c,HexaBPM__SR__r.Authorized_Signatory_Combinations__c from HexaBPM__Step__c]; 
        test.startTest();
        CC_CPPerformFinalUpdates customCode = new CC_CPPerformFinalUpdates();
        String strResult = customCode.EvaluateCustomCode(srList[0],stepList[0]);  
        system.assertEquals('Success',strResult);

        List<Account> accountList = [Select id,CreatedDate,Authorized_Signatory_Combinations__c from Account]; 
        system.assertEquals('AS Combination',accountList[0].Authorized_Signatory_Combinations__c);
        test.stopTest();
    }
}