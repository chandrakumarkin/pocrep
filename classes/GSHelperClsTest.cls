@istest
public class GSHelperClsTest {

    Static testmethod void TestMethod1() {
        Account accObj = new Account();
        accObj.Name = 'Test Account';
        accobj.RecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non Registered Accounts').getRecordTypeId();
        accObj.Exception_Visa_Quota__c = 10;
        insert accObj;


        Account childAccObj = new Account();
        childAccObj.Name = 'Test Account Child';
        childAccObj.RecordTypeID = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Non Registered Accounts').getRecordTypeId();
        childAccObj.Exception_Visa_Quota__c = 10;
        childAccObj.Hosting_Company__c = accObj.id;
        insert childAccObj;

        Lease__c lse = new Lease__c();
        lse.Account__c = accObj.id;
        lse.Lease_Types__c = 'Leased Property Registration';
        lse.Start_date__c = system.Today();
        lse.End_Date__c = system.Today() + 10;
        lse.Status__c = 'Active';
        lse.Square_Feet__c = '100000';
        lse.Type__c = 'Leased';
        insert lse;


        Contact con = new Contact();
        con.LastName = 'test contact';
        con.email = 'testUser@difc.ae';
        con.recordTypeId = Schema.SObjectType.contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert con;

        contact con1 = new contact();
        con1.LastName = 'test contact';
        con1.email = 'testUser@difc.ae';
        con1.recordTypeId = Schema.SObjectType.contact.getRecordTypeInfosByName().get('GS Contact').getRecordTypeId();
        insert con1;

        List<Relationship__c> ListRelation = new List<Relationship__c>();
        
        Relationship__c rel = new Relationship__c();
        rel.Subject_Account__c = accObj.id;
        rel.Relationship_Type__c = 'Has DIFC Seconded';
        rel.Active__c = true;
        rel.End_Date__c = system.Today() + 1;
        rel.Document_Active__c = true;
        rel.Object_Contact__c = con1.id;
        //insert rel;
        ListRelation.add( rel );

        Relationship__c rel2 = new Relationship__c();
        rel2.Subject_Account__c = accObj.id;
        rel2.Relationship_Type__c = 'Has DIFC Local /GCC Employees';
        rel2.Active__c = true;
        rel2.End_Date__c = system.Today() + 1;
        rel2.Document_Active__c = true;
        rel2.Object_Contact__c = con1.id;
        //insert rel2;
        ListRelation.add( rel2 );

        Relationship__c rel3 = new Relationship__c();
        rel3.Subject_Account__c = accObj.id;
        rel3.Relationship_Type__c = 'Has Temporary Employee';
        rel3.Active__c = true;
        rel3.End_Date__c = system.Today() + 1;
        rel3.Document_Active__c = true;
        rel3.Object_Contact__c = con1.id;
        //insert rel3;
        ListRelation.add( rel3 );

        Relationship__c rel4 = new Relationship__c();
        rel4.Subject_Account__c = accObj.id;
        rel4.Relationship_Type__c = 'Has DIFC Sponsored Employee';
        rel4.Active__c = true;
        rel4.End_Date__c = system.Today() + 1;
        rel4.Document_Active__c = true;
        rel.Object_Contact__c = con.id;
        //insert rel4;
        ListRelation.add( rel4 );

        insert ListRelation;


        list < service_request__c > srList = new list < service_request__c > ();
        service_request__c sr = new service_request__c();
        sr.customer__c = accObj.id;
        sr.RecordTypeID = Schema.SObjectType.service_request__c.getRecordTypeInfosByName().get('DIFC Sponsorship Visa-New').getRecordTypeId();
        sr.Passport_Number__c = 'ihjihihu';
        sr.Nationality_list__c = 'India';
        srList.add(sr);

        service_request__c sr1 = new service_request__c();
        sr1.customer__c = accObj.id;
        sr1.RecordTypeID = Schema.SObjectType.service_request__c.getRecordTypeInfosByName().get('Access Card').getRecordTypeId();
        sr1.Service_Category__c = 'Renewal Employee Card for UAE / GCC Nationals';
        sr1.ApplicantId_hidden__c = con.id;
        sr1.Passport_Number__c = 'ihjihihu';
        sr1.Nationality_list__c = 'India';
        srList.add(sr1);

        service_request__c sr2 = new service_request__c();
        sr2.customer__c = accObj.id;
        sr2.RecordTypeID = Schema.SObjectType.service_request__c.getRecordTypeInfosByName().get('Access Card').getRecordTypeId();
        sr2.Passport_Number__c = 'ihjihihu';
        sr2.Nationality_list__c = 'India';
        sr2.Service_Category__c = 'New Seconded Employee Card';
        sr2.ApplicantId_hidden__c = con.id;
        sr2.Passport_Number__c = 'ihjihihu';
        sr2.Nationality_list__c = 'India';
        srList.add(sr2);

        service_request__c sr3 = new service_request__c();
        sr3.customer__c = accObj.id;
        sr3.RecordTypeID = Schema.SObjectType.service_request__c.getRecordTypeInfosByName().get('Access Card').getRecordTypeId();
        sr3.Passport_Number__c = 'ihjihihu';
        sr3.Nationality_list__c = 'India';
        //sr3.Service_Category__c = 'New Employee Card for UAE / GCC Nationals';
        sr3.ApplicantId_hidden__c = con.id;
        sr3.Passport_Number__c = 'ihjihihu';
        sr3.Nationality_list__c = 'India';
        srList.add(sr3);

        insert srList;

        test.startTest();

        GSHelperCls.CalculateVisa(accObj.id);
        //  GSHelperCls.QuotaBreakdownForAccounts(new set<id>{accobj.id,childAccObj.id});
        GsHelperCls.getCurrentEmployeeCount(new set < string > {
            accobj.id,
            childAccObj.id
        });
        GsHelperCls.updateVisaQuota(accObj.id);
        GsHelperCls.getPipelineEmployeeCount(new set < string > {
            accobj.id,
            childAccObj.id
        });
        GsHelperCls.ValidateNewEmpQuota( srList[0] );
        //   GsHelperCls.checkVisaAvailability(accobj.id,childAccObj.id);

        lse.Square_Feet__c = '160';
        update lse;

        childAccObj.Exception_Visa_Quota__c = 0;
        Update childAccObj;

        //  GsHelperCls.checkVisaAvailability(accobj.id,childAccObj.id);
        test.StopTest();

    }

}