/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 10-28-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   10-28-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@isTest
global class FinanceContactPersonWebservice_Mock implements WebServiceMock,HttpCalloutMock  {

	FinanceContactPersonWebservice.ZSF_SRENT_TT  objActivityData = new FinanceContactPersonWebservice.ZSF_SRENT_TT();
    FinanceContactPersonWebservice.ZRE_LEASE_SALES_RENT_element  objSFData = new FinanceContactPersonWebservice.ZRE_LEASE_SALES_RENT_element();
    FinanceContactPersonWebservice.ZSF_SRENT_ST ZSF_SRENT_STRec = new FinanceContactPersonWebservice.ZSF_SRENT_ST();
    FinanceContactPersonWebservice.ZSF_SRENT_ST_EX ZSF_SRENT_ST_EXVal = new FinanceContactPersonWebservice.ZSF_SRENT_ST_EX();
    FinanceContactPersonWebservice.ZRE_LEASE_SALES_RENTResponse_element element = new FinanceContactPersonWebservice.ZRE_LEASE_SALES_RENTResponse_element();
    FinanceContactPersonWebservice.ZSF_SRENT_TT_EX ZSF_SRENT_TT_EXRec = new FinanceContactPersonWebservice.ZSF_SRENT_TT_EX();
    //FinanceContactPersonWebservice.ZRE_SALES_RENT  objSFData1 = new FinanceContactPersonWebservice.ZRE_SALES_RENT();
    
	global void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType){
                
                 Test_FitoutServiceRequestUtils.setFitoutBusinessHours();
        
                map<string,string> mapRecordType = new map<string,string>();
                for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('DIFC_Sponsorship_Visa_New')]){
                    mapRecordType.put(objRT.DeveloperName,objRT.Id);
                }
                
                Account objAccount = new Account();
                objAccount.Name = 'Test Account';
                insert objAccount;
                
                Status__c objStatus = new Status__c();
                objStatus.Name = 'Pending';
                objStatus.Code__c = 'Pending';
                insert objStatus;
                
                Business_Hours__c bh= new Business_Hours__c();
                BH.name='Fit-Out';
                BH.Business_Hours_Id__c='01m20000000BOf6';
                insert BH; 
                
                SR_Template__c testSrTemplate = new SR_Template__c();
            
                testSrTemplate.Name = 'DIFC_Sponsorship_Visa_New';
                testSrTemplate.Menutext__c = 'New Employment Visa Package';
                testSrTemplate.SR_RecordType_API_Name__c = 'DIFC_Sponsorship_Visa_New';
                testSrTemplate.SR_Group__c = 'GS';
                testSrTemplate.Active__c = true;
                insert testSrTemplate;
                
                Service_Request__c SR1 = new Service_Request__c();
                SR1.RecordTypeId = mapRecordType.get('DIFC_Sponsorship_Visa_New');
                SR1.Customer__c = objAccount.Id;
                SR1.Mobile_Number__c ='+971500000000';
                SR1.Year__c = '2020';
                SR1.Month__c = 'AUGUST';
                SR1.Transaction_Amount__c =1000;
                insert SR1;
                        
                Service_Request__c servRequest = [Select id,Month__c,name,customer__r.BP_No__c,Lease__r.Name,Year__c,Transaction_Amount__c from Service_Request__c where id =:SR1.id];
                string result = 'Success';
                FinanceContactPersonWebservice.ZSF_SRENT_ST item;
                list<FinanceContactPersonWebservice.ZSF_SRENT_ST> items = new list<FinanceContactPersonWebservice.ZSF_SRENT_ST>();
                    
                item = new FinanceContactPersonWebservice.ZSF_SRENT_ST();
                item.RENUM = servRequest.name; //'0430272';
                item.SGUID = servRequest.Id; //'a1I3N000000JOo1';
                //item.PARTNER = customerBPNumber; //'0000406414';
                item.PARTNER = servRequest.customer__r.BP_No__c; //'0000406414';
                //item.RECNNR = leaseNumber; //'0000000000057';
                item.RECNNR = servRequest.Lease__r.Name; //'0000000000057';
                item.GJAHR = servRequest.Year__c;        
                item.MONAT = MonthlySalesController.getCalanderMonthValue(servRequest.Month__c);
                item.SAMT = String.valueof(servRequest.Transaction_Amount__c); 
                items.add(item);
                FinanceContactPersonWebservice.ZSF_SRENT_TT  objActivityData = new FinanceContactPersonWebservice.ZSF_SRENT_TT();
                objActivityData.item = items;	
                FinanceContactPersonWebservice.ZRE_SALES_RENT  objSFData = new FinanceContactPersonWebservice.ZRE_SALES_RENT();       
                objSFData.timeout_x = 120000;
                objSFData.clientCertName_x = 'test';
                objSFData.inputHttpHeaders_x= new Map<String, String>();
                string decryptedSAPPassWrd = '123456';
                Blob headerValue =Blob.valueOf('test'); 
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
                objSFData.inputHttpHeaders_x.put('Authorization', authorizationHeader);	
                system.debug('request $$$ : '+objActivityData);	
                FinanceContactPersonWebservice.ZSF_SRENT_TT_EX EX_LOG_TT = objSFData.ZRE_LEASE_SALES_RENT(objActivityData);	
                system.debug('response $$$ : '+EX_LOG_TT);
                response.put('response_x',EX_LOG_TT);
                result = JSON.serialize(EX_LOG_TT);
                system.debug('result--'+result);
	}
	
	global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/xml');
        res.setBody('<xml><EncodedData>TestBarCodeservice</EncodedData></xml>');
        res.setStatusCode(200);
        return res;
    }
}