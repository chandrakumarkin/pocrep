public class WebServiceCdrDeleteAccount {
    
    public static String deleteJsonAccCdr(String AccID){
        
        Account Acc =   [SELECT id, BP_No__c from Account Where Id =:AccID];
        JSONGenerator deleteAccJson = JSON.createGenerator(true);
        deleteAccJson.writeStartArray();
        deleteAccJson.writeStartObject();
        if(!String.isBlank(Acc.BP_No__c))
        deleteAccJson.writeStringField('accountNumber', Acc.BP_No__c);
        deleteAccJson.writeEndObject();
        deleteAccJson.writeEndArray();
        System.debug('deleteAccJson:-'+deleteAccJson.getAsString());
        return deleteAccJson.getAsString();
    }
    public static void deleteAccountToCdr(String AccId){
        
            String jsonRequest;
            List<Log__c> ErrorObjLogList = new List<Log__c>();
            //Get authrization token 
            String Token = WebServiceCDRGenerateToken.cdrAuthToken();
            //WebService to Push the delete the Account
            HttpRequest request = new HttpRequest();
            request.setEndPoint(System.label.Cdr_Delete_Account_Url);
            jsonRequest = deleteJsonAccCdr(AccId);
            request.setBody(jsonRequest);
            request.setTimeout(120000);
            request.setHeader('Accept', 'text/plain');
            request.setHeader('Content-Type', 'application/json-patch+json');
            request.setHeader('Authorization','Bearer'+' '+Token);
            request.setMethod('DELETE');
            HttpResponse response = new HTTP().send(request);
            System.debug('responseStringresponse:'+response.toString());
            System.debug('STATUS:'+response.getStatus());
            System.debug('STATUS_CODE:'+response.getStatusCode());
            System.debug('strJSONresponse:'+response.getBody());
            request.setTimeout(110000);
        
        if (response.getStatusCode() == 200)
            {
                
            }
        
    }

}