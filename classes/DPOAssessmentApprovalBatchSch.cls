/*********************************************************************************************************************
*  Name     : DPOAssessmentApprovalBatchSchedule
*  Author   : DIFC2-16935
*  Purpose  : This class used to schedule DPOAssessmentApprovalBatch class
--------------------------------------------------------------------------------------------------------------------------
Version       Developer Name        Date                     Description 
 V1.0         Durga
**/

global class DPOAssessmentApprovalBatchSch implements schedulable{
    global void execute(schedulablecontext sc){
        DPOAssessmentApprovalBatch sc1 = new DPOAssessmentApprovalBatch(); 
        database.executebatch(sc1,1);
    }
}