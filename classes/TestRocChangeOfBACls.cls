/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=false)
private class TestRocChangeOfBACls {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        list<License_Activity_Master__c> lstLMAs = new list<License_Activity_Master__c>();
        
        License_Activity_Master__c objLAM = new License_Activity_Master__c();
        objLAM.Name = 'Test DIFC Activity 1';
        objLAM.Sector_Classification__c = 'Authorised Market Institution';
        objLAM.Type__c = 'License Activity';
        objLAM.Sys_Code__c = '048';
        objLAM.Enabled__c = true;
        //insert objLAM;
        lstLMAs.add(objLAM);
        
        objLAM = new License_Activity_Master__c();
        objLAM.Name = 'Test DIFC Activity 2';
        objLAM.Sector_Classification__c = 'Authorised Market Institution';
        objLAM.Type__c = 'License Activity';
        objLAM.Sys_Code__c = '049';
        objLAM.Enabled__c = true;
        //insert objLAM;
        lstLMAs.add(objLAM);
        
        objLAM = new License_Activity_Master__c();
        objLAM.Name = 'Test DIFC Activity 3';
        objLAM.Sector_Classification__c = 'Corporate Services';
        objLAM.Type__c = 'Business Activity';
        objLAM.Sys_Code__c = '050';
        objLAM.Enabled__c = true;
        lstLMAs.add(objLAM);
        
        insert lstLMAs;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Financial - related';
        objAccount.Sector_Classification__c = 'Authorised Market Institution';
        objAccount.Registration_License_No__c = '0001';
        insert objAccount;
        
        License_Activity__c objLA = new License_Activity__c();
        objLA.Account__c = objAccount.Id;
        objLA.Activity__c = lstLMAs[0].Id;
        objLA.Sector_Classification__c = 'Authorised Market Institution';        
        insert objLA;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Change_of_Business_Activity')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Change Of Business Activity';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Change_of_Business_Activity';
        insert objPageFlow;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        //objSR.Internal_SR_Status__c = lstStatus[0].Id;
        //objSR.External_SR_Status__c = lstStatus[0].Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('Change_of_Business_Activity');
        insert objSR;
        
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        
        RocChangeOfBACls objRocChangeOfBACls = new RocChangeOfBACls();
        objRocChangeOfBACls.pageLoad();
        objRocChangeOfBACls.EditActivity();
        objRocChangeOfBACls.ChangeCompanyType();
        objRocChangeOfBACls.RowIndex = 0;
        objRocChangeOfBACls.RemoveLicenseActivity();
        objRocChangeOfBACls.AddNewRow();
        objRocChangeOfBACls.RowIndex = 0;
        objRocChangeOfBACls.LicenseAcctivities[0].MasterId = lstLMAs[1].Id;
        objRocChangeOfBACls.SaveLicenseActivity();
        objRocChangeOfBACls.RemoveLicenseActivity();
        objRocChangeOfBACls.SaveActivity();
        objRocChangeOfBACls.CancelActivity();
        
        Amendment__c objAmd = new Amendment__c();
        for(Amendment__c objAmdTemp : [select Id from Amendment__c where ServiceRequest__c =: objSR.Id AND Amendment_Type__c='Change of Business Activity']){
            objAmd = new Amendment__c(Id=objAmdTemp.Id);
            objAmd.Company_Type_Changed__c = true;
            objAmd.Sector_Classification_Changed__c = true;
            update objAmd;
        }
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        insert objStep;
        
        objStep.sr__r = objSR;
        RocChangeOfBACustomCode.UpdateBusinessActivities(objStep);
        
        
    }
    
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        
        list<License_Activity_Master__c> lstLMAs = new list<License_Activity_Master__c>();
        
        License_Activity_Master__c objLAM;
        
        objLAM = new License_Activity_Master__c();
        objLAM.Name = 'Test DIFC Activity 2';
        objLAM.Sector_Classification__c = 'Corporate Services';
        objLAM.Type__c = 'Business Activity';
        objLAM.Sys_Code__c = '049';
        objLAM.Enabled__c = true;
        //insert objLAM;
        lstLMAs.add(objLAM);
        
        objLAM = new License_Activity_Master__c();
        objLAM.Name = 'Test DIFC Activity 3';
        objLAM.Sector_Classification__c = 'Corporate Services';
        objLAM.Type__c = 'Business Activity';
        objLAM.Sys_Code__c = '050';
        objLAM.Enabled__c = true;
        lstLMAs.add(objLAM);
        
        insert lstLMAs;
        
        Account objAccount = new Account();
        objAccount.Name = 'Test Custoer 1';
        objAccount.E_mail__c = 'test@test.com';
        objAccount.BP_No__c = '001234';
        objAccount.BP_No__c = '12345';
        objAccount.Company_Type__c = 'Non - financial';
        objAccount.Sector_Classification__c = 'Corporate Services';
        insert objAccount;
        
        License_Activity__c objLA = new License_Activity__c();
        objLA.Account__c = objAccount.Id;
        objLA.Activity__c = lstLMAs[0].Id;
        objLA.Sector_Classification__c = 'Corporate Services';        
        insert objLA;
        
        list<Status__c> lstStatus = new list<Status__c>();
        lstStatus.add(new Status__c(Name='Draft',Code__c='DRAFT'));
        lstStatus.add(new Status__c(Name='Submitted',Code__c='SUBMITTED'));
        insert lstStatus;
        
        CountryCodes__c objCC = new CountryCodes__c();
        objCC.Name = '971';
        insert objCC;
        
        map<string,string> mapRecordTypeIds = new map<string,string>();
        for(RecordType objRT : [select Id,DeveloperName from RecordType where DeveloperName IN ('Change_of_Business_Activity')]){
            mapRecordTypeIds.put(objRT.DeveloperName,objRT.Id);
        }
        
        Page_Flow__c objPageFlow = new Page_Flow__c();
        objPageFlow.Name = 'Change Of Business Activity';
        objPageFlow.Master_Object__c = 'Service_Request__c';
        objPageFlow.Record_Type_API_Name__c = 'Change_of_Business_Activity';
        insert objPageFlow;
        
        Service_Request__c objSR = new Service_Request__c();
        objSR.Customer__c = objAccount.Id;
        objSR.Send_SMS_To_Mobile__c = '+971123456789';
        //objSR.Internal_SR_Status__c = lstStatus[0].Id;
        //objSR.External_SR_Status__c = lstStatus[0].Id;
        objSR.RecordTypeId = mapRecordTypeIds.get('Change_of_Business_Activity');
        insert objSR;
        
        Apexpages.currentPage().getParameters().put('id',objSR.Id);
        Apexpages.currentPage().getParameters().put('FlowId',objPageFlow.Id);
        
        Page__c ObjPage=new Page__c();
        ObjPage.Page_Description__c='asas';
        ObjPage.Name='demo';
        objPage.Page_Flow__c = objPageFlow.Id;
        objPage.Page_Order__c = 1;
        insert objPage;
        
        Apexpages.currentPage().getParameters().put('PageId',ObjPage.Id);
        
         
        
        
        RocChangeOfBACls objRocChangeOfBACls = new RocChangeOfBACls();
        
        objRocChangeOfBACls.pageDescription='asdasd';
        objRocChangeOfBACls.pageTitle='Demo';
        
        
        
        objRocChangeOfBACls.pageLoad();
        objRocChangeOfBACls.EditActivity();
        objRocChangeOfBACls.ChangeCompanyType();
        objRocChangeOfBACls.AddNewRow();
        objRocChangeOfBACls.RowIndex = 0;
        objRocChangeOfBACls.RemoveLicenseActivity();
        objRocChangeOfBACls.SectorClassifications = new list<RocChangeOfBACls.LicenseAcctivityWrapper>();
        objRocChangeOfBACls.AddNewRow();
        objRocChangeOfBACls.RowIndex = 0;
        objRocChangeOfBACls.SectorClassifications[0].MasterId = lstLMAs[1].Id;
        objRocChangeOfBACls.SaveLicenseActivity();
        objRocChangeOfBACls.RemoveLicenseActivity();
        objRocChangeOfBACls.SaveActivity();
        objRocChangeOfBACls.CancelActivity();
        
        objRocChangeOfBACls.init();
        
        Amendment__c objAmd = new Amendment__c();
        for(Amendment__c objAmdTemp : [select Id from Amendment__c where ServiceRequest__c =: objSR.Id AND Amendment_Type__c='Change of Business Activity']){
            objAmd = new Amendment__c(Id=objAmdTemp.Id);
            objAmd.Company_Type_Changed__c = true;
            objAmd.Sector_Classification_Changed__c = true;
            update objAmd;
        }
        
        objAmd = new Amendment__c();
        objAmd.ServiceRequest__c = objSR.Id;
        objAmd.License_Activity_Master__c = lstLMAs[1].Id;
        objAmd.Amendment_Type__c = 'License Activity';
        insert objAmd;
        
        Step__c objStep = new Step__c();
        objStep.SR__c = objSR.Id;
        insert objStep;
        
        objStep.sr__r = objSR;
      //  RocChangeOfBACustomCode.UpdateBusinessActivities(objStep);
        
        
       
        
        Section__c objSection = new Section__c();
        objSection.Page__c = objPage.Id;
        objSection.Name = 'Test';
        insert objSection;
        
        Section_Detail__c objSD = new Section_Detail__c();
        objSD.Component_Type__c='Command Button';
        objSD.Component_Label__c = 'Forward';
        objSD.Section__c = objSection.Id;
        insert objSD;
        
        objRocChangeOfBACls.strNavigatePageId = objPage.Id;
        objRocChangeOfBACls.strActionId = objSD.Id;
        
        objRocChangeOfBACls.goTopage();
        objRocChangeOfBACls.getDyncPgMainPB();
        objRocChangeOfBACls.DynamicButtonAction();
        
        objRocChangeOfBACls.NextPage();
        
    }
    
}