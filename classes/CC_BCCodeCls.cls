/*****************************************************************************************************************************
    Name        :   CC_BCCodeCls
    Author      :   Kaavya Raghuram
    Date        :   23-02-2015
    Description :   This class contains the Custom codes for the Business Center process
    --------------------------------------------------------------------------------------------------------------------------
    Modification History
    --------------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    --------------------------------------------------------------------------------------------------------------------------             
    V1.0    25-08-2015  Saima         Add zeros to make 3 digits in SAP
    V1.1    24-08-2015  Shabbir       Added security email for IT Form
    V1.2    02-09-2015  Saima         Added validation for leased units
    V1.3    13-09-2015  Kaavya        Added logic for updating dates for pushing and re-pushing to SAP
    V1.4    28-09-2015  Shabbir       Updated security email for IT Form to run on custom setting
    v1.5    20/03/2016  Shabbir       Added lease validation for business centre to include notices leases in new lease registration request
    V1.6    15/08/2019  Arun          Added Add new email address 
*****************************************************************************************************************************/
public without sharing class CC_BCCodeCls{

    public static string CreateSAPAttr(Step__c stp){
        string strResult ='Failure';
         if(stp!=null && stp.Id!=null && stp.SR__c!=null){
           try{
               list<Amendment__c> unitlist = [select id,name,Lease_Unit__r.Rental_Object__c,ServiceRequest__c from Amendment__c where ServiceRequest__c=:stp.SR__c];
               if(unitlist.size()>0){
                   List<SAP_Attribute__c> sapunitlist= new List<SAP_Attribute__c>();
                   decimal count=unitlist.size();
                   double sapno= math.ceil(count/5);
                   
                   system.debug('SSS==='+ sapno);
                   
                   for(integer i=0;i<sapno;i++){
                       SAP_Attribute__c sapunit = new SAP_Attribute__c();
                       sapunit.Sequence_No__c = '00'+string.valueof(i+1);//V1.0 - prepending zero to make 3 digits in SAP
                       sapunit.Service_Request__c=unitlist[0].ServiceRequest__c;
                       integer cntr= i*5;
                       if(count> cntr)
                       sapunit.Obj1__c= unitlist[cntr].Lease_Unit__r.Rental_Object__c;
                       if(count> (cntr+1))
                       sapunit.Obj2__c= unitlist[cntr+1].Lease_Unit__r.Rental_Object__c;
                       if(count> (cntr+2))
                       sapunit.Obj3__c= unitlist[cntr+2].Lease_Unit__r.Rental_Object__c;  
                       if(count> (cntr+3)) 
                       sapunit.Obj4__c= unitlist[cntr+3].Lease_Unit__r.Rental_Object__c;
                       if(count> (cntr+4))
                       sapunit.Obj5__c= unitlist[cntr+4].Lease_Unit__r.Rental_Object__c;
                       sapunitlist.add(sapunit);
                   }
                   if(sapunitlist.size()>0)
                   insert sapunitlist;
                   strResult = 'Success';
                   //V1.3 adding dates to SR for pushing to SAP
                   Service_Request__c objSR = new Service_Request__c(Id=stp.SR__c);
                    objSR.Confirm_Change__c = true;
                    //objSR.Transaction_Amount__c = dAmt;
                    objSR.Updated_from_SAP__c = system.now();
                    objSR.Approved_By__c = Userinfo.getUserId();
                    update objSR;
                    

               }else{
                    strResult = 'No unit record has been added to the request';
               }
           }
           catch(exception ex){
                strResult = 'Failure';
                system.debug('Exception is : '+ex.getMessage());
                Log__c objLog = new Log__c();
                objLog.Description__c ='Line No===>'+ex.getLineNumber()+'---Message==>'+ex.getMessage();
                objLog.Type__c = 'Custom code for BC SAP Attribute';
                insert objLog;        
           }
         }     
         return strResult;      
    }
    
    //V1.2 - Leased units validate after submission
    public static string validateUnits(string SRId){
        string result='Success';
        set<string> setunitId = new set<string>();
        Boolean isActive = false;
        list<Tenancy__c> lstActiveTen = new list<Tenancy__c>();
        list<Service_Request__c> newSRs = [Select id,Name,Record_Type_Name__c,External_Status_Code__c,recordtype.developername,(Select id,Name,Lease_Unit__c,Lease_Unit__r.Active__c,Record_Type_Name__c from Amendments__r) , Customer__r.ROC_Status__c , Office_Signage_Details__c from Service_Request__c where id=:SRId];
        if(newSRs[0].Record_Type_Name__c=='Lease_Application_Request'){
            for(Amendment__c amnd : newSRs[0].Amendments__r){
                isActive = amnd.Lease_Unit__r.Active__c;
                setunitId.add(amnd.Lease_Unit__c);
            }
            //v1.5 added notice active in SOQL
            lstActiveTen = [Select id,name,Unit__c,Lease__c,Lease__r.Account__c,Lease__r.Status__c,Lease__r.Lease_Types__c from Tenancy__c 
                            where Unit__c =:setunitId AND Unit__c != null AND Lease__c != null AND Lease__r.Status__c = 'Active' and Lease__r.Notice_Active__c = false]; 
            if(!isActive)
                result = 'The selected unit is not active or available for regisration.';
            else if(lstActiveTen.size()>0 && lstActiveTen!=null)
                result = 'The selected unit is not available for registration.';
        }
        return result;
    }
    
    public static string UpdateSAPDate(Step__c objStep){
        Service_Request__c objSR = new Service_Request__c(Id=objStep.SR__c);
        objSR.Updated_from_SAP__c = system.now();
        objSR.Confirm_Change__c = true;
        objSR.Is_Error_From_SAP__c = false;
        objSR.Approved_By__c = Userinfo.getUserId();
        update objSR;
        return 'Success';
    }
    
 //IT Form Permit to Access Permanent
 public static string createPTARel(Step__c stp){
    string strResult = 'Success';
    if(stp!=null && stp.Id!=null && stp.SR__c!=null){
        try{
            list<Service_Request__c> SR = [Select id,Name,Customer__c from Service_Request__c where id=:stp.SR__c];
            String RecordId; 
            String strRecTypeName;     
            for(RecordType objRT : [select Id,DeveloperName,Name from RecordType where DeveloperName='Individual' AND SobjectType='Contact' AND IsActive=true]){
                RecordId = objRT.Id;
                strRecTypeName = objRT.DeveloperName;
            }
            Set<string> setNat = new Set<string>();
            Set<string> setPassportNo = new Set<string>();
            Set<string> setRemNat = new Set<string>();
            Set<string> setRemPassportNo = new Set<string>();
            map<string,Contact> mapExistingContacts = new map<string,Contact>();
            map<string,string> mapCompanyName = new map<string,string>();
            map<string,string> mapDCLoc = new map<string,string>();
            map<string,string> mapRemRel = new map<string,string>();
            map<string,string> mapIDCardNo = new map<string,string>();
            map<string,string> mapOtherLoc = new map<string,string>();
            
            list<Contact> lstNewCon = new list<Contact>();
            list<Amendment__c> AddAmend = new list<Amendment__c>();
            list<Relationship__c> lstRel = new list<Relationship__c>();
            for(Amendment__c lstAmnd : [select id,Phone__c,Status__c,Others_Please_Specify__c,ID_Identification__c,Data_Center_Location__c,Record_Type_Name__c,Full_Name__c,Person_Email__c,Permanent_Native_Country__c,Company_Name__c,Passport_No__c,Nationality_list__c,ServiceRequest__c from Amendment__c where ServiceRequest__c=:stp.SR__c]){
                if(lstAmnd.Status__c=='Remove'){
                    setRemNat.add(lstAmnd.Nationality_list__c);
                    setRemPassportNo.add(lstAmnd.Passport_No__c);
                }else if(lstAmnd.Status__c=='Add'){
                    setNat.add(lstAmnd.Nationality_list__c);
                    setPassportNo.add(lstAmnd.Passport_No__c);
                    AddAmend.add(lstAmnd);
                }
            }
            string strContactQuery = Apex_Utilcls.getAllFieldsSOQL('Contact');
            strContactQuery = strContactQuery+' where Passport_No__c IN:setPassportNo and Nationality__c IN:setNat and RecordType.DeveloperName=:strRecTypeName order by CreatedDate';
            for(Contact objCon:database.query(strContactQuery)){
                mapExistingContacts.put(objCon.Passport_No__c.toLowerCase()+'_'+objCon.Nationality__c.toLowerCase(),objCon);
            }
            for(Amendment__c addAmnd : AddAmend){
                if(mapExistingContacts!=null && mapExistingContacts.size()>0 && mapExistingContacts.containsKey(addAmnd.Passport_No__c.toLowerCase()+'_'+addamnd.Nationality_list__c.toLowerCase())){
                    lstNewCon.add(mapExistingContacts.get(addAmnd.Passport_No__c.toLowerCase()+'_'+addamnd.Nationality_list__c.toLowerCase()));
                }else{
                    Contact cont = new Contact();
                    cont.FirstName = addAmnd.Full_Name__c.left(addAmnd.Full_Name__c.indexof(' ',0));
                    cont.lastName = addAmnd.Full_Name__c.substring(addAmnd.Full_Name__c.indexof(' ',0)+1,addAmnd.Full_Name__c.length());
                    cont.Passport_No__c = addAmnd.Passport_No__c;
                    cont.Nationality__c = addAmnd.Nationality_list__c;
                    cont.Email = addAmnd.Person_Email__c;
                    cont.MobilePhone = addAmnd.Phone__c;
                    cont.RecordTypeId = RecordId;
                    if(addAmnd.Permanent_Native_Country__c!=null)
                        cont.Country__c = addAmnd.Permanent_Native_Country__c;
                    else
                        cont.Country__c = 'United Arab Emirates';
                    lstNewCon.add(cont);
                }
                mapCompanyName.put(addAmnd.Passport_No__c+'_'+addAmnd.Nationality_list__c,addAmnd.Company_Name__c);
                mapDCLoc.put(addAmnd.Passport_No__c+'_'+addAmnd.Nationality_list__c,addAmnd.Data_Center_Location__c);
                mapIDCardNo.put(addAmnd.Passport_No__c+'_'+addAmnd.Nationality_list__c,addAmnd.ID_Identification__c);
                if(addAmnd.Data_Center_Location__c=='Others' && addAmnd.Others_Please_Specify__c!=null)
                    mapOtherLoc.put(addAmnd.Passport_No__c+'_'+addAmnd.Nationality_list__c,addAmnd.Others_Please_Specify__c);
            }
            //To remove existing relationships
            for(Relationship__c RemRel : [Select id,Object_Contact__c,Subject_Account__c,Relationship_Type__c,Relationship_Passport__c,Relationship_Nationality__c from Relationship__c where Relationship_Type__c='Data Center Access' and Relationship_Passport__c IN:setRemPassportNo and Relationship_Nationality__c IN :setRemNat and Active__c=true and subject_account__c=:SR[0].Customer__c]){
                    RemRel.Active__c = false;
                    RemRel.End_Date__c = system.today();
                    lstRel.add(RemRel);
                    mapRemRel.put(RemRel.Relationship_Passport__c,RemRel.Relationship_Passport__c);
            }
            for(String strppno : setRemPassportNo){
                if(!mapRemRel.containskey(strppno)){
                    strResult = 'No active relationship with passport number: '+strppno+' found.';
                    break;
                }
            }
            if(strResult=='Success'){
                upsert lstNewCon;
                for(Contact cont : lstNewCon){
                    Relationship__c rel = new Relationship__c();
                    rel.object_contact__c = cont.id;
                    rel.Subject_Account__c = SR[0].Customer__c;
                    rel.Relationship_Type__c = 'Data Center Access';
                    rel.Active__c = true;
                    rel.Start_Date__c = system.today();
                    rel.Relationship_Group__c = 'IT';
                    if(mapCompanyName!=null && mapCompanyName.containsKey(cont.Passport_No__c+'_'+cont.Nationality__c))
                        rel.Company_Name__c = mapCompanyName.get(cont.Passport_No__c+'_'+cont.Nationality__c);
                    if(mapDCLoc!=null && mapDCLoc.containsKey(cont.Passport_No__c+'_'+cont.Nationality__c))
                        rel.Data_Center_Location__c = mapDCLoc.get(cont.Passport_No__c+'_'+cont.Nationality__c);
                    if(mapIDCardNo!=null && mapIDCardNo.containsKey(cont.Passport_No__c+'_'+cont.Nationality__c))
                        rel.Data_Center_Access_ID__c = mapIDCardNo.get(cont.Passport_No__c+'_'+cont.Nationality__c);
                    if(mapOtherLoc!=null && mapOtherLoc.containsKey(cont.Passport_No__c+'_'+cont.Nationality__c))
                        rel.Other_IT__c = mapOtherLoc.get(cont.Passport_No__c+'_'+cont.Nationality__c);
                    lstRel.add(rel);
                }
                
                upsert lstRel;
            }
        }catch(DMLException e){
           string DMLError = e.getdmlMessage(0)+'';
           if(DMLError==null){
               DMLError = e.getMessage()+'';
           }
           strResult = DMLError;
           return strResult;
        }
    }
    return strResult;
}

    //IT Form email to security team  V1.1 
    public static string sendSecurityEmailITForm(Step__c objStep){
    
        try{
            
            if(objStep != null && objStep.SR__c != null){
                
                OrgWideEmailAddress[] lstOrgEA = [select Id from OrgWideEmailAddress where Address = 'portal@difc.ae'];
                string strTemplateId;
                for(EmailTemplate objET : [select Id from EmailTemplate where DeveloperName='Email_to_Security_Team_IT_Form'])
                    strTemplateId = objET.Id;
                    
                list<Messaging.SingleEmailMessage> mails = new list<Messaging.SingleEmailMessage>();
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                
                list<string> lstEmails = new list<string>();
                map<string,string> mapContactEmails = new map<string,string>();
                
                Messaging.Emailfileattachment efa;
                list<Messaging.Emailfileattachment> lstAttach = new list<Messaging.Emailfileattachment>();
                
                list<string> lstIds = new list<string>();
                // v1.4 - start
                //set<string> setDocumentName = new set<string>{'Permit to Access - Permanent' , 'Permit to Work' , 'Permit to Access - Temporary' , 'Permit to Add or Remove Equipment'};
                set<string> setDocumentName = new set<string>();
  
                for(IT_Form_Security_Email__c securityEmail : IT_Form_Security_Email__c.getAll().values()){
                    setDocumentName.add(securityEmail.Name);
                }
                // v1.4 - end          
                for(SR_Doc__c objSRDoc : [select Id,Doc_ID__c from SR_Doc__c where Service_Request__c=:objStep.SR__c AND Name =:setDocumentName]){
                    lstIds.add(objSRDoc.Doc_ID__c);
                }                
                
                for(Attachment objAttah : [select Id,Name,Body,ContentType from Attachment where Id IN : lstIds order by CreatedDate desc]){
                    efa = new Messaging.Emailfileattachment();
                    string contentType = objAttah.Name.indexOf('.') > 0 ? '.'+objAttah.Name.substring(objAttah.Name.lastIndexOf('.'),objAttah.Name.length()) : '';
                    efa.setFileName(objAttah.Name + contentType);
                    efa.setBody(objAttah.Body);
                    lstAttach.add(efa);
                }
                
                for(Contact objCont : [select Id,Email from Contact where Email = 'alsadeek@difc.ae' limit 1]){
                    mapContactEmails.put(objCont.Id,objCont.Email);
                }
                system.debug('mapContactEmails are : '+mapContactEmails);// Arun - 15.Aug.2019 Can be replaced with Custom setting ?
                lstEmails = new list<string>();
                lstEmails.add('Security.Services@arkansecurity.ae');//V1.6
                lstEmails.add('IT.Infrastructure@difc.ae');
                
                for(string ContactId : mapContactEmails.keySet()){
                    mail = new Messaging.SingleEmailMessage();
                    mail.setCCAddresses(lstEmails);
                    mail.setReplyTo('portal@difc.ae');
                    if(strTemplateId!=null && strTemplateId!=''){
                        mail.setWhatId(objStep.SR__c);
                        mail.setTemplateId(strTemplateId);
                        mail.setSaveAsActivity(false);
                        if(lstOrgEA!=null && lstOrgEA.size()>0)
                            mail.setOrgWideEmailAddressId(lstOrgEA[0].Id);
                        if(!lstAttach.isEmpty())
                            mail.setFileAttachments(lstAttach);
                        mail.setTargetObjectId(ContactId);
                        mails.add(mail);
                    }
                }
                if(mails != null && !mails.isEmpty()){
                    if(!test.isRunningTest()){
                        Messaging.sendEmail(mails);
                    }                
                }
            }   
            return 'Success';       
            
        }catch(Exception e){
            system.debug('Error is here: ' + e.getMessage());
            return e.getMessage();
        }

    } //V1.1 
    
}