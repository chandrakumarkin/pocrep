/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class PageFlowCSSController {
    global String NavigationFontColor {
        get;
        set;
    }
    global String NavigationPrimaryColor {
        get;
        set;
    }
    global String PrimaryLogoURL {
        get;
        set;
    }
    global String SecondaryLogoURL {
        get;
        set;
    }
    global String SectionHeaderColor {
        get;
        set;
    }
    global String SectionHeaderFontColor {
        get;
        set;
    }
    global PageFlowCSSController() {

    }
}
