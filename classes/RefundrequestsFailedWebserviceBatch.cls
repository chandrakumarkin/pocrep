/******************************************************************************************
 *  Author   : Sravan Booragadda
 *  Company  : DIFC
 *  Date     : 27-Aug-2017
 *  Description : This is a Contoller for GS Refund Automation Page                  
 --------------------------------------------------------------------------------------------------------------------------
 Modification History 
 ----------------------------------------------------------------------------------------
 V.No   Date            Updated By          Description
 ----------------------------------------------------------------------------------------              
V1.0    04-10-2017      Sravan              Added vars to display request and response in logs
*******************************************************************************************/
global class RefundrequestsFailedWebserviceBatch implements  Schedulable,Database.Batchable<sObject>,Database.AllowsCallouts{
    
    global list<Step__c> start(Database.BatchableContext BC){
        Datetime dt = !test.isRunningTest() ? system.now().addMinutes(-10) : system.now().addMinutes(-0);
        list<Step__c> lstSteps = [select id,SR__c,SR__r.Customer__c,sr__r.Name,status__c,sr__r.Type_of_Refund__c from Step__c 
                                         where  Step_Status__c = 'Approved' and Push_to_SAP__c = false and closed_Date_Time__c <=:dt and Step_Template__r.Code__c ='HOD_Review'];
        return lstSteps;
    }
    global void execute(Database.BatchableContext BC, list<Step__c> lstSteps){
        List<Status__c> stpStatus = new List<Status__c>();
        Service_request__c srObj = new Service_Request__c();
        List<Log__c> refundLogs = new List<Log__c>();
        stpStatus = [select id from Status__c where code__c='ERRORED_FROM_SAP'];    
        
        
        boolean isErrored=false;
        
        
        for(Service_Request__c sr :[select id,Name,Type_of_Refund__c,Refund_Amount__c,Mode_of_Refund__c,Customer__r.BP_No__c,Bank_Name__c,External_SR_Status__r.Code__c,
                                            Bank_Address__c,SAP_SGUID__c,SAP_OSGUID__c,IBAN_Number__c,SAP_GSTIM__c,first_Name__c,Last_Name__c,SAP_Unique_No__c, 
                                            Contact__r.BP_No__c,SAP_MATNR__c,Transfer_to_Account__r.BP_No__c,Transfer_to_Account__c,Event_Name__c,Record_Type_Name__c
                                            from service_request__c where id = : lstSteps[0].SR__c]){
            srObj = sr;                                             
        }  
        
        try{
            SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP refundResponse;
            if(SRObj.Record_Type_Name__c =='Refund_request')
                 refundResponse =   RefundRequestUtils.PushToSAP(srObj,null);  
            else if(SRObj.Event_Name__c == 'Cancellation')
                refundResponse = RefundRequestUtils.PushToSAP(srObj,'RFCL');
             else if(SRObj.Event_Name__c == 'Rejection')    
                refundResponse = RefundRequestUtils.PushToSAP(srObj,'RFRJ');         
        
                if(refundResponse !=null){
                    for(SAPPortalBalanceRefundService.ZSF_S_REFUND_OP resp : refundResponse.item){
                        if(resp !=null && resp.RSTAT !=null){
                            if(resp.RSTAT == 'E' &&  resp.SFMSG !=null){
                                Log__c refundLog = new Log__c();
                                refundLog.Account__c = srObj.Customer__c;
                                refundLog.Type__c = 'Unable to Push details to SAP'+srObj.Name;
                                refundLog.Description__c = resp.SFMSG ;            
                                refundLogs.add(refundLog);
                                isErrored = true;        
                                                      
                             }else if(resp.RSTAT == 'S'){
                                lstSteps[0].Push_to_SAP__c = true;
                             }
                        }
                        
                         if(stpStatus!=null && stpStatus.size()>0 && isErrored){
                            lstSteps[0].status__c = stpStatus[0].id;
                            update lstSteps[0];      
                         }else if(lstSteps[0].Push_to_SAP__c = true){
                                         
                           if(SRObj.Record_Type_Name__c =='Refund_request')  
                            RefundRequestUtils.sendEmailToFinance(lstSteps[0],srObj.Type_of_refund__c);
                           else if(SRObj.Event_Name__c == 'Rejection')                     
                            RefundRequestUtils.sendEmailToFinance(lstSteps[0],'Service Rejection');
                           else if(SRObj.Event_Name__c == 'Cancellation') 
                            RefundRequestUtils.sendEmailToFinance(lstSteps[0],'Service Cancellation');    
                            
                             update lstSteps[0];                             
                         }
                              
                         if(refundLogs !=null && refundLogs.size()>0) insert refundLogs; 
                    
                    }
                }     
            }catch(exception e){             
                refundLogs.add(new log__c(Account__c=srObj.customer__c,Type__c='PushToSAP Failure SR#'+srObj.Name,Description__c =e.getMessage()+'\\n'+e.getStackTraceString()));
                insert  refundLogs;   
            }   
        
    }
    global void finish(Database.BatchableContext BC){
        RefundrequestsFailedWebserviceBatch objSchedule = new RefundrequestsFailedWebserviceBatch();
        Datetime dt = system.now().addMinutes(10);
        string sCornExp = '0 '+dt.minute()+' '+dt.hour()+' '+dt.day()+' '+dt.month()+' ? '+dt.year();
        for(CronTrigger obj : [select Id from CronTrigger where CronJobDetailId IN (SELECT Id FROM CronJobDetail where Name = 'Batch Refund Activity Process')])
            system.abortJob(obj.Id);
        system.schedule('Batch Refund Activity Process', sCornExp, objSchedule);
    }
    
    global void execute(SchedulableContext ctx) {
        database.executeBatch(new RefundrequestsFailedWebserviceBatch(), 1);
    }
    
}