global class StepClosedBatch implements Database.Batchable<SObject>, Database.Stateful{    
    
    Map <id,Step__c> stepList;
   
    global StepClosedBatch (){
       stepList= new Map<id,step__c>();
              
    }
   
    global Database.QueryLocator start(Database.BatchableContext BC){
        Datetime dt =  system.now().addMinutes(-60);
        Datetime dt1 = system.now();
        string SRGroup ='GS';
        string status  = 'CLOSED';
        string srstatus = system.label.EntryPermitBatchStatus;
        
        string stepName = system.label.StepsClosedBatch;
        
        List<string> stepnames = new List<string>();
        
        for(string s : stepName.split(',')){
        
           stepnames.add(s); 
        }
        
        
        
        //List<string> stepnames = new List<string>{'Entry Permit Form is Typed','Online Entry Permit is Typed','Visit Visa Form is Typed','E-Form Visit Visa is Typed'};
        return Database.getQueryLocator('Select Id, OwnerId,DNRD_Receipt_No__c,Step_Id__c,sr__r.name,Applicant_Name__c,Customer__c,Step_Name__c,SR_Status__c,SAP_PR_No__c,Closed_Date_Time__c,Name From step__c  where  Closed_Date_Time__c >=: dt and Closed_Date_Time__c < :dt1 and Sr_group__c = :SRGroup and Step_Name__c in : stepnames and Status_Code__c = : status and SR_Status__c =:srstatus  ');
    }
   
    global void execute(Database.BatchableContext BC, List<SObject> scope) {
       
          for(SObject s : scope){
            step__c step = (step__c) s;
            stepList.put(step.id,step);
        }
        
    }
   
    global void finish(Database.BatchableContext BC) {
        if(!stepList.isEmpty()) {
            
          //  Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
            // Define email file attachment list
          //  Messaging.EmailFileAttachment[] emailAttList = new List<Messaging.EmailFileAttachment>();     
            
                // Creating the CSV file for successful updates
                String finalstr = 'DNRD Receipt No,Step Id, Service Request No,Applicant Name,Customer, step Name,SAP PR Number,SR Status,Closed Date Time \n';
                String attName = 'step' + system.now().format('YYYYMMDDhhmm') + '.csv';
                for(Id id  : stepList.keySet()){
                    string suc = 'sucesss';
                    step__c stp = stepList.get(id);
                    string recordString = '"'+stp.DNRD_Receipt_No__c+'","'+stp.Name+'","'+stp.sr__r.name+'","'+stp.Applicant_Name__c+'","'+stp.Customer__c+'","'+stp.Step_Name__c+'","'+stp.SAP_PR_No__c+'","'+stp.SR_Status__c+'","'+stp.Closed_Date_Time__c+'",""\n';
                    finalstr = finalstr +recordString;
                }
               
            string EmailAdd =  system.label.BatchEmails ;  
            List<string> Emails = new List<string>();
            
            for(string s : EmailAdd.split(',')){                
                Emails.add(s);              
            }
               
            Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
            blob csvBlob = Blob.valueOf(finalstr);
            string csvname= 'Entry Permit For Posting.csv';
            csvAttc.setFileName(attName );
            csvAttc.setBody(csvBlob);
            Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
            //String[] toAddresses = new list<string> {Emails};
            String[] toAddresses = Emails;
            String subject ='step CSV';
            email.setSubject(subject);
            email.setToAddresses( toAddresses );
            email.setPlainTextBody('Entry permit form Step CSV ');
            email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
            Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
               
               
               
             
        }
    }
}