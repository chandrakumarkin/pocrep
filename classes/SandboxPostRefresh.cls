/**
* Author: Shoaib Tariq
* Date: 10/04/2020
* Description: Class implements SandboxPostCopy Interface which will invoked automatically by Automated Process
**/

global class SandboxPostRefresh implements SandboxPostCopy {
    global void runApexClass(SandboxContext context) {
        SandboxPostRefreshHelper.executeBatch();
    }
}