@isTest
global class TestSAPRefundRequestServiceMock implements WebServiceMock {
     
     public static List<SAPPortalBalanceRefundService.ZSF_S_REFUND_OP> items = new List<SAPPortalBalanceRefundService.ZSF_S_REFUND_OP>();
     
     global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) 
      {
      	
      	SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP ttRefundOp = new SAPPortalBalanceRefundService.ZSF_TT_REFUND_OP();
      	ttRefundOp.item =  items;
      	SAPPortalBalanceRefundService.Z_SF_REFUND_PROCESSResponse_element respElement = new SAPPortalBalanceRefundService.Z_SF_REFUND_PROCESSResponse_element();
      	respElement.EX_SF_RP = ttRefundOp;
       	//respElement.EchoStringResult = 'Mock response';
       	response.put('response_x', respElement); 
   	  }
}