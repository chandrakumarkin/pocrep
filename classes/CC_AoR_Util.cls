/*********************************************************************************************************************
    Author Name :   Shabbir Ahmed
    Class Name  :   CC_AoR_Util
    Description :   Custom code which will be clone AoR.
    ---------------------------------------------------------------------------------------------------------------------
    Modification History
    ---------------------------------------------------------------------------------------------------------------------
    V.No    Date        Updated By    Description
    ---------------------------------------------------------------------------------------------------------------------             
    V1.0    21-Mar-18   Shabbir       Created
    ************************************************************************************************************************/

public without sharing class CC_AoR_Util{


    @future
    public static void CloneAORAsync(string stepSerialized){
        step__c stp = (step__c)Json.deserialize(stepSerialized,step__c.class);
        CC_AoR_Util.CloneAOR(stp);
    }
    
    //public static Service_Request__c CloneAOR(Id stringSRId){
    public static string CloneAOR(step__c stp){
        string strResult = 'Success';
    
        List<Service_Request__c> parentSObjects;
        Service_Request__c parent;
        //CloneModel cloneModelResult = new CloneModel();
        
        Id sObjectId = stp.SR__c;
        //Id sObjectId = stringSRId;

        Schema.SObjectType token = sObjectId.getSObjectType();

        
        String query = String.format(
            'SELECT {0} FROM {1} WHERE Id = \'\'{2}\'\'',
            new String[] {
                String.join(
                    new List<String>(
                        token.getDescribe().fields.getMap().keySet()
                    ),
                    ','
                ),
                String.valueOf(Service_Request__c.SObjectType),
                sObjectId
           }
        );

        try {
            
            // Query and gets results
            parentSObjects = Database.query(query);         
            
            // Clone the original object. Here you can change anything without affecting the original sObject
            parent = parentSObjects[0].clone(false, true, false, false);
            //parent.Name = parent.Name + ' CLONED';
            
            parent.finalizeAmendmentFlg__c = false;
            parent.Cancellation_With_Keys__c = true;
            
            Database.insert(parent);
            system.debug(parent.Name);
            
            
            // Amendment query  
            query = String.format(
                'SELECT {0} FROM {1} WHERE ServiceRequest__c = \'\'{2}\'\'',
                new String[] {
                    String.join(
                        new List<String>(
                            Amendment__c.SObjectType.getDescribe().fields.getMap().keySet()
                        ),
                        ','
                    ),
                    String.valueOf(Amendment__c.SObjectType),
                    sObjectId
               }
            );
            
            List<Amendment__c> childrenAmendment = new List<Amendment__c>(); 
            Set<String> amendShareMemberIds= new Set<String>();
            map<string,string> mapShareholderWithId = new map<string,string>();
            map<string,string> mapShareholderWithNewId = new map<string,string>();
            map<string,string> mapOldAmendmentWithNewId = new map<string,string>();
            List<Amendment__c> lstAmendmentToUpdate = new List<Amendment__c>();   
            // Query and clone the children. Here you can change anything without affecting the original sObject
            for (Amendment__c child:(List<Amendment__c>)Database.query(query)){
                if(child.Related__c != null){
                    child.Level__c = child.Related__c;
                }
                child.ID_Identification__c = child.Id;
                childrenAmendment.add(child.clone(false,true,false,false));
            }
            
            // Set the parent's Id
            for (Amendment__c child : childrenAmendment) {
                child.ServiceRequest__c = parent.Id;
            }
        
            Database.insert(childrenAmendment);
            for (Amendment__c child : childrenAmendment) {
                if(!mapOldAmendmentWithNewId.containsKey(child.ID_Identification__c)){
                    mapOldAmendmentWithNewId.put(child.ID_Identification__c,child.id);
                }   
            }
            amendShareMemberIds.addAll(mapOldAmendmentWithNewId.keySet());
            
            for (Amendment__c child : childrenAmendment){
                if(string.isNotBlank(child.Level__c)){
                    Amendment__c tempAmend = new Amendment__c(Id = child.Id);
                    tempAmend.Related__c = mapOldAmendmentWithNewId.get(child.Level__c);
                    lstAmendmentToUpdate.add(tempAmend);
                }
            }
            
            if(!lstAmendmentToUpdate.isEmpty()){
                update lstAmendmentToUpdate;
            }
            
            // Declaration Signatory query  
            query = String.format(
                'SELECT {0} FROM {1} WHERE Service_Request__c = \'\'{2}\'\'',
                new String[] {
                    String.join(
                        new List<String>(
                            Declaration_Signatory__c.SObjectType.getDescribe().fields.getMap().keySet()
                        ),
                        ','
                    ),
                    String.valueOf(Declaration_Signatory__c.SObjectType),
                    sObjectId
               }
            );
            
            List<Declaration_Signatory__c> childrenDecSignatory = new List<Declaration_Signatory__c>(); 

            // Query and clone the children. Here you can change anything without affecting the original sObject
            for (Declaration_Signatory__c child:(List<Declaration_Signatory__c>)Database.query(query)){
                childrenDecSignatory.add(child.clone(false,true,false,false));
            }
            
            // Set the parent's Id
            for (Declaration_Signatory__c child : childrenDecSignatory) {
                child.Service_Request__c = parent.Id;
            }
        
            Database.insert(childrenDecSignatory); 
            
            // Shareholder Detail  
            query = String.format(
                'SELECT {0} FROM {1} WHERE Amendment__c IN: amendShareMemberIds',
                new String[] {
                    String.join(
                        new List<String>(
                            Shareholder_Detail__c.SObjectType.getDescribe().fields.getMap().keySet()
                        ),
                        ','
                    ),
                    String.valueOf(Shareholder_Detail__c.SObjectType)
               }
            );
            
            List<Shareholder_Detail__c> childrenShareDetails = new List<Shareholder_Detail__c>(); 

            // Query and clone the children. Here you can change anything without affecting the original sObject
            for (Shareholder_Detail__c child:(List<Shareholder_Detail__c>)Database.query(query)) {
                
                childrenShareDetails.add(child.clone(false,true,false,false));
            }
            
            // Set the parent's Id
            for (Shareholder_Detail__c child : childrenShareDetails) {
                child.Amendment__c = mapOldAmendmentWithNewId.get(child.Amendment__c);
            }
        
            Database.insert(childrenShareDetails);               
                        
            //strResult  = CC_AmendmentsCls.RejectSR(stp);                   
            
            
        }
         catch (DmlException error) {
            system.debug(error.getMessage());
            strResult = error.getMessage();
            //cloneModelResult.message = 'An error occurred while cloning the object.' + error.getMessage();
            
            //return JSON.serialize(cloneModelResult);        
        }
        return strResult;
    } 


}