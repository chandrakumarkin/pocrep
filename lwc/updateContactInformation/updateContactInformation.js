import { LightningElement,api,track,wire } from 'lwc';
import getContactInfo  from '@salesforce/apex/UpdateContactInformation.getContactInfo';
import updateContact  from '@salesforce/apex/UpdateContactInformation.updateContact';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class UpdateContactInformation extends LightningElement {

    areDetailsVisible = false;

    @track currentField = 'Name';
    @api recordId;
    @track record = '';
    @api newemail = '';
    @api newphone = '';
    @api isCommunityEvents  = false;
    @api isDIFCAcademy  = false;
    @api isDIFCFintechHive = false;
    @api isGeneralAnnouncements = false;
    @api isSaved = false;
    @api isErr = false;
  
    @track error;
    
    handleChange(event) {
        this.areDetailsVisible = true;
    }

    handlenewemail(event) {
        this.newemail = event.target.value;
    }

    handlenewphone(event) {
        this.newphone = event.target.value;
    }

    handleisCommunityEvents(event) {
        this.isCommunityEvents = event.target.checked;
    }

    handleisDIFCAcademy(event) {
        this.isDIFCAcademy = event.target.checked;
    }

    handleisDIFCFintechHive(event) {
        this.isDIFCFintechHive = event.target.checked;
    }

    handleisGeneralAnnouncements(event) {
        this.isGeneralAnnouncements = event.target.checked;
    }

    handleissave() {
       
       if(!this.isCommunityEvents  && !this.isDIFCAcademy && !this.isDIFCFintechHive && !this.isGeneralAnnouncements
          && this.newemail== '' &&  this.newphone == ''){
            this.isErr  = true;
            this.error  = 'You have have not selected anything to update';
            return;
          }
        if(this.newphone !== '' && !this.newphone.startsWith('+971')){
            return;
        }
        updateContact({recordId:this.recordId,isCommunityEvents:this.isCommunityEvents,isDIFCAcademy:this.isDIFCAcademy,newemail:this.newemail,isDIFCFintechHive:this.isDIFCFintechHive,isGeneralAnnouncements:this.isGeneralAnnouncements,newphone:this.newphone})
        .then(result => {
            if(result == 'Details Updated'){
                this.isSaved = true;
            }
                   })
        .catch(error => {
            alert(this.error);
            this.error = error;
        });
    }

   
    
    connectedCallback() {
        getContactInfo({recordId:this.recordId})
        .then(result => {
             this.record = result;
          })
        .catch(error => {
            alert(this.error);
            this.error = error;
        });
    }
}