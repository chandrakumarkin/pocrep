import { LightningElement, track,api } from 'lwc';  
import SaveFile from '@salesforce/apex/UserUploadFormController.SaveFile';  
import { NavigationMixin } from 'lightning/navigation';  
import Salesforce_Images from '@salesforce/resourceUrl/OB_DIFCCommunityLogo';
 

 const MAX_FILE_SIZE = 100000000; //10mb  
 const CHUNK_SIZE = 750000;

 export default class NewRecordWithFileUpload extends NavigationMixin(LightningElement) {  

   @api recordId;
   loaded;
   uploaded = true;

   image1 = Salesforce_Images;
 
   uploadedFiles = []; file; fileContents; fileReader; content; fileName  
   areDetailsVisible = true;
 
   onFileUpload(event) {  
     if (event.target.files.length > 0) {  

       this.uploadedFiles    = event.target.files;  
       this.fileName         = event.target.files[0].name;  
       this.file             = this.uploadedFiles[0]; 
       this.uploaded = false;

       if (this.file.size > this.MAX_FILE_SIZE) {  
         alert("File Size Can not exceed" + MAX_FILE_SIZE);  
       }  

     }  
   }  
   saveContact() {  

     this.fileReader = new FileReader();  

     this.fileReader.onloadend = (() => {  

       this.fileContents      = this.fileReader.result;  
       let base64             = 'base64,';  
       this.content           = this.fileContents.indexOf(base64) + base64.length;  
       this.fileContents      = this.fileContents.substring(this.content);  
       this.saveRecord();  
     });  

     this.fileReader.readAsDataURL(this.file);  
   }  
   saveRecord() {  

    var startPosition = 0;
    var endPosition   = Math.min(this.fileContents.length, startPosition + this.CHUNK_SIZE);
    
    var getchunk = this.fileContents.substring(startPosition, endPosition);
    this.loaded = true;
   
    SaveFile({  
        parentId: this.recordId,  
        fileName: this.fileName,
        base64Data : encodeURIComponent(getchunk),
        contentType : this.file.type,
     })  
       .then(result => {  
         if(result == 'success'){
           this.areDetailsVisible = false;
           this.loaded = false;
         }
         else{
            this.areDetailsVisible = true;
            this.loaded = false;
         }
        
       }).catch(error => {  
         console.log('error ', error);  
       });  
   }  



 
 

 }