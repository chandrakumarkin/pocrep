import { LightningElement, wire, api } from 'lwc';
import updateStepStatus from '@salesforce/apex/FitoutLWCController.updateStepStatus';

export default class UpdateMilestoneStepStatus extends LightningElement {
    @api recordId;
    isSpinner = false;
    error;

    selectedValue;

    get options() {
        return [
            { label: 'Verified', value: 'Verified' }
        ];
    }

    handleChange(event){
        this.selectedValue = event.detail.value;
        console.log(this.selectedValue);
        console.log(this.recordId);
    }
    handleClickCancel(event){
       // self.close();
       window.location.href = '/'+this.recordId;
    }

    handleClickSave(){
        console.log('value***'+this.selectedValue);
        this.isSpinner = true;
        updateStepStatus({stepId:this.recordId, stepStatus:this.selectedValue})
        .then(result=>{
            window.location.href = '/'+this.recordId;
        })
        .catch(error=>{
            this.isSpinner = false;
            if(this.selectedValue == null){
                this.error = 'Please select the verified option';
            }
            for(let prop in error.body.fieldErrors){
                console.log(prop);
                let val = Object.values(error.body.fieldErrors);
                console.log(val[0][0]["message"]);
                this.error = val[0][0]["message"];
            }
        });
      }
}