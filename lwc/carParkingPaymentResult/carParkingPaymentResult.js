import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getReceiptRecord from '@salesforce/apex/CarParking_Visitors_Valet_Controller.getReceiptRecord';
export default class CarParkingPaymentResult extends LightningElement {
    receiptId;
    receiptObj;
    showForm = false;
    apexerror;
    connectedCallback() {
        let allParams = window.location.search;
        //alert('on Load'+allParams);
        if(allParams && allParams.length > 0 ){
            let urlParams = new URLSearchParams(allParams);
            if(urlParams){
                let status = urlParams.get('s'); //paystatus
                let rptId = window.atob(urlParams.get('rid'));
                console.log('rptId: '+rptId);
                this.receiptId = rptId;
                //console.log('rptId: '+rptId);
                if(status && status  == 'y' && rptId && rptId.length > 0){
                    //this.showForm = true;
                    this.showToastMessage('Success!!','Payment completed successfully. Thank you!', 'success');
                    this.getReceiptData();
                }
                else if(status && status  == 'n' && rptId && rptId.length > 0){
                    //this.showForm = true;
                    this.showToastMessage('Oops!','Payment failed, please try again later.', 'error');
                    this.getReceiptData();
                }
                else{
                    //console.log('INIT->Invalid Status');
                    //this.showForm = true;
                    this.showToastMessage('Aww!','Error Ocurred!, please contact system administrator..', 'error');                    
                }
            }
            else{                
                //console.log('INIT->No status found!'); 
                this.showToastMessage('Aww!','Error Ocurred!, please contact system administrator..', 'error');                
            }
        }
        else{
            //console.log('INIT->No parameter found!');
            this.showToastMessage('Aww!','Error Ocurred!, please contact system administrator..', 'error');            
        }
        //console.log('form: '+this.showForm);
    }
    getReceiptData(){
        getReceiptRecord({recId: this.receiptId})
            .then(result => {
                this.receiptObj = result;
                console.log('showForm: '+this.showForm+' result: '+this.receiptObj);
                this.showForm = true;
            })
            .catch(error => {
                this.apexerror = error;
                //console.log('apexerror: '+JSON.stringify(this.apexerror));
        });
    }
    checkPaymentStatus(event){
        if(this.receiptObj.Payment_Status__c == 'Success')
            return true;
        else   
            return false
    }
    showToastMessage(title, message, variant){
        let toastEvt = new ShowToastEvent({
            title: title,
            mode: "pester",
            variant: variant,
            message: message,
            duration:6000
        });
        this.dispatchEvent(toastEvt);
    }
}