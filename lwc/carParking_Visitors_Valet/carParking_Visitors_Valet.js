import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import communityURL from '@salesforce/label/c.Community_URL';
import fetchAmount from '@salesforce/apex/CarParking_Visitors_Valet_Controller.fetchAmount';
import createRecords from '@salesforce/apex/CarParking_Visitors_Valet_Controller.createRecords';
import fetchCarParkDetails from '@salesforce/apex/CarParking_Visitors_Valet_Controller.fetchCarParkDetails';
import fetchDetailsFromSparkApp from '@salesforce/apex/CarParking_Visitors_Valet_Controller.fetchDetailsFromSparkApp';
export default class CarParking_Visitors_Valet extends NavigationMixin(LightningElement) {
    value = 'visitor';
    navigateTo = 'https://difc--uatjuly21.my.salesforce.com/apex/GoogleRecaptcha';
    emiratesValue;
    vehicleSeries;
    vehicleNumber;
    buttonLabel = 'Yes';
    buttonLabel1 = 'No';
    emailId;
    mobile;
    patternMismatchMessage='';
    isVehNumRequired=true;
    showCardNumber;
    cardNumber;
    fetchDetails = true;
    loaded = false;
    carParkDetObj;
    ismodalpopup = false;
    enterEmailDet = false;
    uniqueId='emailopt';
    isGracePeriod = false;
    connectedCallback() {
        //alert(communityURL);
        let vfOrigin='https://difc--uatjuly21.my.salesforce.com/';
        window.addEventListener("message", function(event){
            console.log('data: '+event.data);
            if(event.origin !== vfOrigin){
                return;
            }
            if(event.data === "Unlock"){
                //do nothing
            }
        },false);
    }
    get options() {
        return [
            { label: 'Visitor', value: 'visitor' },
            { label: 'Valet', value: 'valet' },
        ];
    }
    get emiratesOptions() {
        return [
            { label: 'Dubai', value: 'UAEDU' },
            { label: 'Sharjah', value: 'UAESH' },
            { label: 'Abu Dhabi', value: 'UAEAZ' },
            { label: 'Ajman', value: 'UAEAJ' },
            { label: 'Umm Al Quwain', value: 'UAEUQ' },
            { label: 'Ras al Khaima', value: 'UAERK' },
            { label: 'Fujairah', value: 'UAEFJ' },
            { label: 'Oman', value: 'OM' },
            { label: 'KSA', value: 'KSA' },
            { label: 'Bahrain', value: 'BRN' },
            { label: 'Kuwait', value: 'KWT' }
        ];
    }
    handleOnchange(event){
        this.value = event.detail.value;
        if(this.value == 'visitor'){
            this.isVehNumRequired = true;
            this.showCardNumber = false;
        }
        else{
            this.showCardNumber = true;
            this.isVehNumRequired = false;
        }
    }
    handleOnEmirChange(event){
        this.emiratesValue = event.detail.value;
        var vehSer = this.template.querySelector('lightning-input[data-id="vehicleSeries"]');//this.template.querySelector('[data-id="vehicleSeries"]');
        if(this.emiratesValue == 'BRN'){            
            if(vehSer){
                this.template.querySelector('lightning-input[data-id="vehicleSeries"]').required = false;
            }
        }
        else{
            if(vehSer){
                this.template.querySelector('lightning-input[data-id="vehicleSeries"]').required = true;
            }
        }
    }
    handleVehclSerChange(event){
        this.vehicleSeries = event.detail.value;
    }
    handleVehclNumChange(event){
        this.vehicleNumber = event.detail.value;
        if(this.vehicleNumber.length <= 5){
            this.patternMismatchMessage = 'Enter only numbers and Minimum length is 5';
        }
        else{
            this.patternMismatchMessage = 'Enter only numbers and Maximum length is 6';
        }
    }
    handleCardNumChange(event){
        this.cardNumber = event.detail.value;
    }
    handleNext(event) {
        const allValid = [...this.template.querySelectorAll('lightning-input, lightning-combobox')]
                .reduce((validSoFar, inputCmp) => {
                            inputCmp.reportValidity();
                            return validSoFar && inputCmp.checkValidity();
                }, true);
        if (allValid) {            
            if(this.value == 'valet'){
                /*if(this.vehicleNumber == undefined && this.cardNumber == undefined){
                    this.showToastMessage("Error!", "Please enter Vehicle Number or Card Number!!", "error");
                    this.fetchDetails = true;
                }
                else{*/
                    //this.fetchDetails = false;
                    this.fetchDetailsFromSpark(this.value);
                //}
            }
            else{
                this.fetchDetailsFromDesigna(this.value);
                //this.fetchDetails = false;
            }
        }
        else {
            this.showToastMessage("Error!", "Please enter valid information and try again!!", "error");
            this.fetchDetails = true;
        }
    }
    fetchDetailsFromDesigna(type){
        let carPDet = { 'sobjectType': 'Car_Parking_Details_Visitor_Valet__c' };
        carPDet.Vehicle_Number__c = this.vehicleNumber;        
        carPDet.Type__c = type;
        carPDet.Vehicle_Series__c = this.vehicleSeries;
        carPDet.Emirates__c = this.emiratesValue;
        if(this.emiratesValue == 'OM'){
            carPDet.Plate_Number__c = this.vehicleNumber+this.vehicleSeries;
        }
        else if(this.emiratesValue == 'BRN'){
            carPDet.Plate_Number__c = this.vehicleNumber;
        }
        else if(this.emiratesValue == 'KSA'){
            carPDet.Plate_Number__c = this.vehicleNumber+' '+this.vehicleSeries;
        }
        else{
            carPDet.Plate_Number__c = this.vehicleSeries + ' ' + this.vehicleNumber;
        }
        console.log('carPDet: '+JSON.stringify(carPDet));
        this.loaded = true;
        fetchCarParkDetails({carParkDetStr: JSON.stringify(carPDet)})
            .then(result => {
                this.apexresult = result;
                if(result != null){
                    console.log('result: '+JSON.stringify(this.apexresult));
                    if(result.hasError){
                        this.showToastMessage("Error!", result.errorMessage, "error");
                    }
                    else{
                        if(result.carParkDetObj.Ticket_Number__c != undefined && result.carParkDetObj.Ticket_Number__c != null){
                            if(result.carParkDetObj.Amount__c == 0){
                                this.fetchDetails = true;
                                //show prompt to user that no need to pay anything
                            }else{
                                this.carParkDetObj = result.carParkDetObj;
                                this.fetchDetails = false;
                            }
                        }
                        else{
                            this.carParkDetObj = null;
                            this.showToastMessage("Error!", "Vehicle not found.", "error");
                            this.fetchDetails = true;
                        }
                        if(result.carParkDetObj.Grace_Period__c != undefined && result.carParkDetObj.Grace_Period__c != null){
                            this.isGracePeriod = true;
                        }                            
                    }
                }
                else{
                    console.log('false here');
                    this.fetchDetails = true;
                    carParkDetObj = null;
                }
                console.log('carParkDetObj: '+JSON.stringify(this.carParkDetObj));             
                this.loaded = false;
            })
            .catch(error => {
                this.apexerror = error;
                this.loaded = false;
                console.log('apexerror: '+JSON.stringify(this.apexerror));
        });
    }
    handleBack(event) {
        this.fetchDetails = true;
    }
    handlePayNow(event) {        
        this.ismodalpopup = true;        
    }
    fetchAmountfromDesigna(){
        this.loaded = true;
        fetchAmount({carParkDetStr: JSON.stringify(this.carParkDetObj)})
        .then(result => {
            console.log('result of fetching amount 2nd time: '+result);
            if(!result.includes('Error') && this.carParkDetObj.Amount__c == result){
                console.log('in if');
                this.createCarPDetRec();
                this.loaded = false;
            }
            else if(!result.includes('Error') && this.carParkDetObj.Amount__c != result){
                //open a prompt to user
                console.log('in else if');
                this.carParkDetObj.Amount__c = result;
                this.createCarPDetRec();
                this.loaded = false;
            }
            else{
                this.loaded = false;
                //ask kiran if there is any error while trying for fetching amount on pay now
            }
        })
        .catch(error => {
            this.loaded = false;
            this.apexerror = error;
            console.log('apexerror: '+JSON.stringify(this.apexerror));
    });
        
    }
    createCarPDetRec(){
        createRecords({carParkDetStr: JSON.stringify(this.carParkDetObj)})
            .then(result => {
                this.apexresult = result;
                console.log('result: '+this.apexresult);
                //this.navigateToPaymentPage();
                //this.navigateToWebPage();
                //alert('encrypt: '+btoa(this.apexresult));
                let refNum = btoa(this.apexresult);
                let amountVal = btoa(this.carParkDetObj.Amount__c);
                window.open(communityURL+"/apex/CarParkingPayment?referenceNumber=" + refNum+'&amount='+amountVal,"_self");
            })
            .catch(error => {
                this.apexerror = error;
                console.log('apexerror: '+JSON.stringify(this.apexerror));
        });
    }
    navigateToWebPage() {
        this[NavigationMixin.Navigate]({
            "type": "standard__webPage",
            "attributes": {
                "url": communityURL+"/apex/CarParkingPayment?referenceNumber=" + this.apexresult+'&amount='+this.carParkDetObj.Amount__c
            }
        });
    }
    navigateToVFPage() {
        this[NavigationMixin.GenerateUrl]({
            type: 'standard__webPage',
            attributes: {
                url: '/apex/CarParkingPayment?referenceNumber=' + this.apexresult+'&amount='+this.carParkDetObj.Amount__c
            }
        }).then(generatedUrl => {
            window.open(generatedUrl);
        });
    }
    navigateToPaymentPage() {  
        this[NavigationMixin.Navigate]({
            type: 'standard__namedPage',
            attributes: {
                pageName: 'car-parking-checkout'
            },
            state: {
                referenceNumber: this.apexresult,
                amount: this.carParkDetObj.Amount__c
            },
        });
    }
    closeModal(event) {
        var carParkObj = this.carParkDetObj;
        carParkObj.Mail_Invoice__c = 'No';
        this.carParkDetObj = carParkObj;
        // to close modal set isModalOpen tarck value as false
        this.ismodalpopup = false;
        this.fetchAmountfromDesigna();
    }
    handleYes(event){
        if(this.enterEmailDet){
            const allValid = [...this.template.querySelectorAll('lightning-input[data-id='+this.uniqueId+']')]
                .reduce((validSoFar, inputCmp) => {
                            inputCmp.reportValidity();
                            return validSoFar && inputCmp.checkValidity();
            }, true);
            if (allValid) {
                var carParkObj = this.carParkDetObj;
                console.log('email: '+this.emailId);
                if(this.emailId != undefined){
                    carParkObj.Email__c	= this.emailId;
                    carParkObj.Mail_Invoice__c = 'Yes';
                    this.carParkDetObj = carParkObj;
                }                
                console.log('final Obj: '+JSON.stringify(this.carParkDetObj));
                this.ismodalpopup = false;
                if(this.value == 'visitor'){
                    this.fetchAmountfromDesigna();
                }
                else{
                    this.fetchAmountFromSpark();
                }
            }
            else {
                this.showToastMessage("Error!","Please update the invalid form entries and try again!!","error");
            }
        }
        else{
            this.enterEmailDet = true;
            this.buttonLabel = 'Ok';
            this.buttonLabel1 = 'Cancel';
        }
    }
    handleEmailChange(event){
        this.emailId = event.detail.value;
    }
    handleMobileChange(event){
        this.mobile = event.detail.value;
    }
    showToastMessage(title, message, variant){
        let toastEvt = new ShowToastEvent({
            title: title,
            mode: "pester",
            variant: variant,
            message: message,
            duration:6000
        });
        this.dispatchEvent(toastEvt);
    }
    fetchDetailsFromSpark(type){
        let carPDet = { 'sobjectType': 'Car_Parking_Details_Visitor_Valet__c' };               
        carPDet.Type__c = type;
        carPDet.Card_Number__c = this.cardNumber;
        console.log('carPDet: '+JSON.stringify(carPDet));
        this.loaded = true;
        fetchDetailsFromSparkApp({carParkDetStr: JSON.stringify(carPDet)})
            .then(result => {
            this.apexresult = result;
            if(result != null){
                console.log('result: '+JSON.stringify(this.apexresult));
                if(result.hasError){
                    this.showToastMessage("Error!", result.errorMessage, "error");
                }
                else{
                    if(result.carParkDetObj.Amount__c == 0){
                        this.fetchDetails = true;
                        //show prompt to user that no need to pay anything
                    }
                    else{
                        this.carParkDetObj = result.carParkDetObj;
                        this.fetchDetails = false;
                    }                  
                }
            }
            else{
                console.log('false here');
                this.fetchDetails = true;
                carParkDetObj = null;
            }
            console.log('carParkDetObj: '+JSON.stringify(this.carParkDetObj));             
            this.loaded = false;
        })
        .catch(error => {
            this.apexerror = error;
            this.loaded = false;
            console.log('apexerror: '+JSON.stringify(this.apexerror));
        });
    }
    fetchAmountFromSpark(){
        this.loaded = true;
        fetchDetailsFromSparkApp({carParkDetStr: JSON.stringify(this.carParkDetObj)})
            .then(result => {
            this.apexresult = result;
            if(result != null){
                console.log('result: '+JSON.stringify(this.apexresult));
                if(result.hasError){
                    this.showToastMessage("Error!", result.errorMessage, "error");
                }
                else{
                    if(result.carParkDetObj.Amount__c == this.carParkDetObj){
                        this.createCarPDetRec();
                    }
                    else if(!result.includes('Error') && this.carParkDetObj.Amount__c != result){
                        //open a prompt to user
                        console.log('in else if');
                        this.carParkDetObj.Amount__c = result;
                        this.createCarPDetRec();
                    }
                    else{
                        //this.loaded = false;
                        //ask kiran if there is any error while trying for fetching amount on pay now
                    }                 
                }
            }
            else{
                console.log('false here');                
            }
            console.log('carParkDetObj: '+JSON.stringify(this.carParkDetObj));             
            this.loaded = false;
        })
        .catch(error => {
            this.apexerror = error;
            this.loaded = false;
            console.log('apexerror: '+JSON.stringify(this.apexerror));
        });
    }
}