<apex:page id="invitationpage" docType="html-5.0" controller="TrainingInvitationController" showHeader="false" sidebar="false" standardStylesheets="false" applyHtmlTag="false" applyBodyTag="false" title="{!$Label.site.site_login}">
    <!--[if IE 7 ]>
    <html class="ie lt-ie8" lang="en">
        <![endif]-->
    <!--[if IE 8 ]>
        <html class="ie lt-ie9" lang="en">
            <![endif]-->

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"></meta>
        <title>DIFC - Dubai International Financial Centre</title>
        <link rel="shortcut icon" href="{!URLFOR($Resource.DIFC_Favicon)}" type="image/x-icon" />
        <link rel="icon" href="{!URLFOR($Resource.DIFC_Favicon)}" type="image/x-icon" />
        <!-- <apex:stylesheet value="{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/css/difc.min.css')}"/> -->

        <apex:stylesheet value="{!URLFOR($Resource.DIFCPortalLoginResource, 'DIFC_Portal_Login/DIFC/Community/Frameworks/foundation/css/normalize.css')}" />
        <apex:stylesheet value="{!URLFOR($Resource.DIFCPortalLoginResource, 'DIFC_Portal_Login/DIFC/Community/Frameworks/foundation/css/foundation.css')}" />
        <apex:stylesheet value="{!URLFOR($Resource.difc_portal, 'DIFC_CommunityResource/css/style_override.css')}" />
        <apex:stylesheet value="{!URLFOR($Resource.DIFCPortalLoginResource, 'DIFC_Portal_Login/DIFC/Community/css/style.css')}" />
        <!--
        <apex:stylesheet value="{!URLFOR($Resource.difc_portal, 'DIFC_CommunityResource/css/difc.min.css')}"/><apex:stylesheet value="{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/css/ie.css')}"/>
         -->
        <!--[if (gte IE 9)|!(IE)]>
                <!-->
        <link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/css/responsive.css')}"></link>
        <!--
                <![endif]-->
        <!--[if lt IE 9]>
                <link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.DIFCPortalLoginResource, 'DIFC_Portal_Login/DIFC/Community/css/ie.css')}"></link>
                <script type="text/javascript" src="{!URLFOR($Resource.DIFCPortalLoginResource, 'DIFC_Portal_Login/DIFC/Community/js/libs/respond.min.js')}"></script>
                <script type="text/javascript" src="{!URLFOR($Resource.DIFCPortalLoginResource, 'DIFC_Portal_Login/DIFC/Community/js/vendor/custom.modernizr.js')}"></script>
                <![endif]-->
        <style>
            .main-content {
                margin: 0 auto;
            }
            
            .errMsg {
                color: red;
            }
            
           
            
            .login-dashboard {
                margin-bottom: 20px;
            }
            
            .difc-logo img {
                height: 55px;
            }
            
            @media (max-width: 980px) {
                nav.top-bar ul {
                    padding: 0 6px;
                }
            }
            
            .top-nav.news ul.mobile-logo,
            .top-nav.dashboard ul.mobile-logo {
                padding: 0px;
                margin: 0 10px !important;
            }
            
            .top-bar-section ul li > a {
                font-family: inherit;
            }
            
            footer small {
                font-style: italic;
            }
            
            .bPageBlock.brandSecondaryBrd,
            body .secondaryPalette.bPageBlock,
            html .brandSecondaryBrd {
                border: none;
                /*
                border-top-color: #00205B !important;
                border-bottom: 1px solid #eaeaea;
                border-left: 1px solid #eaeaea;
                border-right: 1px solid #eaeaea;
                */
            }
            
            label,
            nav.top-nav .top-bar-section ul.right li,
            nav.top-nav .top-bar-section ul.right li a {
                font-size: 14px !important;
                background: rgba(0, 0, 0, 0);
            }
            
            i.icon-phone-white {
                background: url("{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/images/icon-phone-small-white.png')}") no-repeat scroll 0 0 rgba(0,
                0,
                0,
                0) !important;
                width: 20px !important;
            }
            
            nav.top-nav .top-bar-section ul li .icon-phone-white {
                background-position: left 3px !important;
            }
            
            .top-bar-section li:not(.has-form) a:not(.button) {
                background: none !important;
                padding: 0 15px !important;
                line-height: 65px;
            }
            
            .top-bar-section i.icon {
                float: none !important;
                line-height: 65px;
            }
            
            .top-bar.top-nav ul,
            .top-bar.top-nav ul li {
                /*margin-left: 0 !important;*/
                line-height: 65px;
                font-size: 12px;
            }
            
            .top-bar-section li.call-centre {
                margin-right: 0 !important;
            }
            
            body .apexp .bPageBlock.apexDefaultPageBlock .pbHeader {
                border-bottom: 0px !important;
            }
            
            table.multiSelectPicklistTable tbody tr:first-of-type {
                display: none;
            }
            
            tr.multiSelectPicklistRow select {
                height: 70px;
                width: 160px;
            }
            
            .contact .inquiry {
                width: 100%;
                background: #e5e5e5;
                padding: 2em;
                overflow: hidden;
                text-align: center;
            }
            
            nav.top-bar,
            nav.top-bar.expanded-nav {
                height: inherit;
            }
            
            .header-section {
                padding: 10px 0;
            }
            
            .pbBody > div,
            .recaptchatable * {
                overflow: hidden;
            }
            
            [data-abide] .error small.error,
            [data-abide] span.error,
            [data-abide] small.error {
                text-transform: none;
            }
            
            table.detailList {
                width: 100%;
            }
            
            footer .row.footer-row {
                margin-top: 0;
            }
            
            footer.row.full-width {
                height: 100%;
                min-height: 300px;
            }
            
            p {
                color: #454A50;
            }
            
            footer .copyright {
                clear: both;
                position: absolute;
                bottom: 40px;
            }
            /* HIDE RADIO */
            [type=radio] { 
              position: absolute;
              opacity: 0;
              width: 0;
              height: 0;
            }
            form.smileys span.pic {
              cursor: pointer;
              transition: border 0.2s ease;
               -webkit-filter: grayscale(0);
                      filter: grayscale(0);
              
              margin: 0 5px;
              transition: all 0.2s ease;
            }
            form.smileys input[type="radio"]:hover span.pic, form.smileys input[type="radio"]:checked span.pic{
              -webkit-filter: grayscale(0);
                      filter: grayscale(0);
            }
            form.smileys input[type="radio"]:focus {
              outline: 0;
            }
            form.smileys span.pic{ 
                background-size:contain;
                background-repeat:no-repeat;
                display:inline-block;
                width:100px;height:100px;
                outline: none;
            }
            form.smileys .happy {
              background: url("{!URLFOR($Resource.CaseSmileys, 'CaseSmileys/img/happy.png')}") center;
              background-size: cover;
              
            }
            form.smileys .neutral {
              background: url("{!URLFOR($Resource.CaseSmileys, 'CaseSmileys/img/neutral.png')}") center;
              background-size: cover;
            }
            form.smileys .sad {
              background: url("{!URLFOR($Resource.CaseSmileys, 'CaseSmileys/img/sad.png')}") center;
              background-size: cover;
            }
            
            .mtt {
              position: fixed;
              bottom: 10px;
              right: 20px;
              color: #999;
              text-decoration: none;
            }
            .mtt span {
              color: #e74c3c;
            }
            .mtt:hover {
              color: #666;
            }
            .mtt:hover span {
              color: #c0392b;
            }
            .pad-left{
                padding-left:10px;
            }
            .pad-btm{padding-bottom:5px;}
            .doneBlock{min-height: 300px;}
            .btnbgcolor {background-color: #141b4c;color: #fff;}
            textarea {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }

        </style>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    </head>

    <body>
        <div class="contact">
           
            <nav class="row full-width top-bar top-nav dashboard">
                <ul class="mobile-logo left">
                    <li>
                        <a target="_blank" href="http://www.difc.ae">
                            <img src="{!URLFOR($Resource.DIFCPortalLoginResource, 'DIFC_Portal_Login/images/DIFC_white_logo.png')}" />
                        </a>
                    </li>
                </ul>
                <section class="top-bar-section">
                    <ul class="title-area right">
                        <li class="toggle-topbar">
                            <a href="#"></a>
                        </li>
                    </ul>
                    <ul class="right hide-for-small-only hide-for-medium-only">
                        <li class="call-centre">
                            <i class="icon icon-phone-white"></i>
                            <a href="el:+97143622222">Call Centre: +971 4 362 2222</a>
                        </li>
                    </ul>
                </section>
            </nav>
            <apex:outputPanel id="panelBlock">
            <apex:form >
            <div class="row full-width">
                <div class="large-12 columns login-dashboard">
                    <div id="main-content" class="large-8 columns large-centered">
                        <div class="header-section">
                            <h3>Confirm your attendance</h3>
                        </div>
                        <apex:pageMessages />
                            <div class="inquiry">
                        <apex:outputPanel rendered="{!isSubmitted || isExpiredTraining}" layout="block" styleClass="doneBlock">
                            <h4>{!warningMessage}</h4>
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{!isThank}" layout="block" styleClass="doneBlock">
                            <h4>Thank you for your confirmation</h4>
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{! !isSubmitted && !isThank && !isExpiredTraining}" layout="block">
                                <div class="info">
                                    <p>Category: {!trainingRequest.Category__c} </p>
                                    <p>Start DateTime: <apex:outputText value="{0,date, dd/MM/YYYY HH:mm }"> <apex:param value="{!trainingRequest.TrainingMaster__r.Start_DateTime__c}" /> </apex:outputText> </p>
                                    <p>End DateTime: <apex:outputText value="{0,date, dd/MM/YYYY HH:mm }"> <apex:param value="{!trainingRequest.TrainingMaster__r.To_DateTime__c}" /> </apex:outputText></p>
                                    <p>Location: {!trainingRequest.Location__c} </p>
                                </div>
                                <div>
                                <p>Please click on the below action to accept/reject your attendance.</p>
                                
                                    <apex:commandButton value="Accept" styleClass="btn btn-lg btn-warning btn-block btnbgcolor" action="{!confirmAction}" rerender="panelBlock" >
                                        <apex:param name="status" value="Accepted" assignTo="{!status}" />
                                    </apex:commandButton>
                                    <apex:commandButton value="Reject" styleClass="btn btn-lg btn-warning btn-block btnbgcolor"  action="{!confirmAction}"  rerender="panelBlock">
                                        <apex:param name="status" value="Rejected" assignTo="{!status}" />
                                    </apex:commandButton>
                                </div>
                            
                        </apex:outputPanel>
                                </div>
                    </div>
                </div>
            </div>
            </apex:form>
            </apex:outputPanel>
            <footer class="row full-width">
                <div class="row full-width footer-row">
                    <div class="large-12 columns">
                        <h3>About DIFC</h3>
                        <p>The Dubai International Financial Centre (DIFC) is a federal financial free zone situated in the Emirate of Dubai, United Arab Emirates. It was established pursuant to UAE Federal Decree No. 35 of 2004, UAE Federal Law No. 8 of 2004 and Dubai Law No. 12 of 2004. The DIFC aims to create an environment for growth, progress and economic development in the UAE and the wider region by providing the needed legal and business as well as physical infrastructure benchmarked against international standards.</p>
                        <p>
                            The DIFC occupies a physical territory of approximately 110 acres. It has its own legal system and courts distinct from those of UAE, and provides a platform for business and financial institutions to reach into and out of the emerging markets of the region.
                        </p>
                    </div>
                </div>
                <div class="full-width copyright">
                    <div class="large-12 columns">
                        <small>All rights reserved. Copyright DIFC &copy;2014</small>
                    </div>
                </div>
            </footer>
            <!-- footer -->
        </div>
        <!--         <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script><script src="{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/js/foundation.min.js')}"></script>     

 -->
        <script src="{!URLFOR($Resource.difc_portal, 'DIFC_CommunityResource/js/foundation.min.js')}"></script>
    </body>
</apex:page>