<!-- 
Modification History:
Version      Change Date     Change By         Description    
V1.1         13-2-19         Azfer Pervaiz     Added .zip in the name of the zip file to solve the IE issue.

-->

<apex:page controller="DocumentDownloadCtrl" showHeader="true" sidebar="false">
    <head>
        <!-- Libraries being used these are part of the static reosurces -->
        <script type="text/javascript" src="/soap/ajax/26.0/connection.js"> </script>
        <apex:includeScript value="{!URLFOR($Resource.JQuery, '/jquery-1.8.2.min.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.js_zip, '/jszip.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.js_zip, '/jszip-load.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.js_zip, '/jszip-deflate.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.js_zip, '/jszip-inflate.js')}"/>
        <apex:includeScript value="{!URLFOR($Resource.downloadJS, '/download.js')}"/>

        <!-- The JS functions being used to automate the user interaction as well as perform the required functionality -->
        <script type="text/javascript">
            var j$ = jQuery.noConflict();

            j$(document).ready(function() {
                //Hide upload button
                var uploadZipButton = j$('input[id$=uploadZipButton]');
                uploadZipButton.css('display', 'none');
                
                //Instantiate JSZip
                var zipFile = new JSZip();
 
                //Intercept click event
                j$('a[id=generateZipLink]').click(function() {
                    var checkedSize = j$("input[name='att_ids_group[]']:checked").size();
                    var count = 0;
                    if( checkedSize > 0 ){
                            //Iterate over each selected file               
                            j$("input[name='att_ids_group[]']:checked").each(function() {
                            //Refresh zip process status
                            count++;
                        
                            CallgetAttachment( j$(this).val(), count, checkedSize);
                        });//end each selected attId
                    }else if ( checkedSize == 0 ){
                        j$('[id=IdloadingDiv]').hide();
                        alert('System was not able to find any files to download');
                        window.close();
                    }
                    
                });//end click
                
                function CallgetAttachment(paramId, paramcount, paramcheckedSize ){
                //Get file using javascript remoting
                    Visualforce.remoting.Manager.invokeAction(
                        '{!$RemoteAction.DocumentDownloadCtrl.getAttachment}',
                        paramId,
                        function(result, event){
                            if (event.status) {
                                compressFile(zipFile, result.attName, result.attEncodedBody);
                                //Once all the selected files have been compressed
                                if (paramcount == paramcheckedSize) {
                                   setTimeout(function() {
                                        sendZip(zipFile);
                                    }, 10000);
                                   
                                }
                            } else if (event.type === 'exception') {
                                alert('Exception: ' + event.message);
                            } else {
                                alert('Message: ' + event.message);
                            }
                        }, 
                        {
                            escape: true,
                            buffer: false
                        }
                    );//End getAttachment
                }

                //Compress one single file
                function compressFile(zipFile, name, data) {
                    zipFile.file(name, data, {base64:true});
                }

                //Generate and upload zip file
                function sendZip(zipFile) {

                    var data = zipFile.generate();
                    var varzipFileName = j$('input[id$=zipFileName]').val()+'.zip'; //V1.1 

                    download("data:application/zip;base64,"+data,varzipFileName, "application/zip");
                    j$('[id=IdloadingDiv]').hide();
                    /*setTimeout(function() {
                        window.close();
                    },2000);*/
                }
                
                j$('[id=IdloadingDiv]').show();
                j$('a[id=generateZipLink]').trigger('click');
            });
        </script>
    </head>

    <!-- This apex form will be used to display the details about the attachement related to the SR -->
    <apex:form id="uploadZipForm" enctype="multipart/form-data">
        
        <apex:inputHidden id="zipContent" value="{!zipContent}" />
        
        <apex:pageBlock id="thePageBlock" title="SR Docs">
            <apex:pageBlockTable value="{!attachments}" var="att">
                
                <apex:column >
                    <input type="checkbox" name="att_ids_group[]" value="{!att.Id}" checked="true" />
                </apex:column>
                <apex:column value="{!att.Parent.Name}" />
                <apex:column value="{!att.ContentType}" />

                <!-- <apex:column value="{!att.BodyLength} kb" />
                <apex:column value="{!att.ParentId}" /> -->

            </apex:pageBlockTable>
        </apex:pageBlock>

        <!-- This part of the form is needed but there is no need to display it to the user thats why its hidden -->
        <div style="display:none;">
            
            <apex:outputLabel for="zipFileName" value="File name: " />
        
            <apex:inputText id="zipFileName" value="{!zipFileName}" />.zip
        
            <p>
                <a id="generateZipLink" href="#">Generate zip</a>
            </p>
        
            <p>
                <span>Status: </span>
                <span id="zipStatus"></span>
            </p>
        
        </div>
        
    </apex:form>
    
    <c:LoadingComponent />
</apex:page>