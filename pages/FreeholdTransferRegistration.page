<apex:page standardController="Service_Request__c"  sidebar="false" showHeader="true" id="editPage" extensions="FreeholdTransferRegistrationExtCrl">
    <style>
        .vfHelpText a            {position:relative;}
        .vfHelpText a span       {display: none;}
        .vfHelpText a:hover span {display: block;
                                  position:absolute;
                                  top:1.25em;
                                  padding:2px 5px;
                                  left:-15em; width:15em;
                                  z-index:100;
                                  border:1px solid orange;
                                  background-color:#FEFDB9;
                                  color:black;
                                 }
    </style>
    
  <apex:form >
        <apex:pageBlock title="Service Request Edit" mode="edit" >
            <apex:pageMessages id="PMMessage" ></apex:pageMessages>
             <apex:pageBlockButtons location="bottom">
                <apex:commandButton action="{!SaveRecord}" value="Save" />
                <apex:commandButton value="Cancel" action="{!cancel}"/>
             </apex:pageBlockButtons>
            <apex:pageBlockSection title="Service Request Details" columns="2" rendered="{!IF($Site.BaseUrl == null,False,True)}">
                <apex:outputField value="{!Service_Request__c.Name}" />
                <apex:pageBlockSectionItem />
                <apex:outputField value="{!Service_Request__c.Customer__c}" />
                <apex:pageBlockSectionItem />
            </apex:pageBlockSection>            
            <apex:pageBlockSection title="Service Request Details" columns="2" rendered="{!IF($Site.BaseUrl == null,True,False)}">
                <apex:inputField value="{!Service_Request__c.Name}" required="false" />
                <apex:pageBlockSectionItem />
                <apex:inputField value="{!Service_Request__c.Customer__c}" required="false"/>
                <apex:pageBlockSectionItem />
            </apex:pageBlockSection>
            <apex:pageBlockSection title="Correspondence Details" columns="2" >
                <apex:outputField value="{!Service_Request__c.Email__c}" />
                <apex:outputField value="{!Service_Request__c.Send_SMS_To_Mobile__c}"/>
            </apex:pageBlockSection> 
            <!--<apex:pageBlockSection title="Correspondence Details" columns="2" rendered="{!IF($Site.BaseUrl == null,True,False)}">
                <apex:inputField value="{!Service_Request__c.Email__c}" required="false"/>
                <apex:inputField value="{!Service_Request__c.Send_SMS_To_Mobile__c}" required="false"/>
            </apex:pageBlockSection>-->   
            <apex:outputPanel style="display:none">
                <apex:inputField id="RecordTypeId" value="{!Service_Request__c.RecordtypeId}"/>
            </apex:outputPanel>         
                        
            <apex:pageBlockSection title="Property Details" columns="2" >
                <apex:inputField value="{!Service_Request__c.Building__c}" required="true"/>
                <apex:outputField value="{!Service_Request__c.Folio_Number__c}"/>
                <apex:inputField value="{!Service_Request__c.Unit__c}" required="true"/>
                <apex:outputField value="{!Service_Request__c.Floor__c}" />
                <apex:pageBlockSectionItem />
                <apex:outputField value="{!Service_Request__c.Usage_Type__c}"/>
                <apex:pageBlockSectionItem />
                <apex:outputField value="{!Service_Request__c.Area_sqft__c}"/>
            </apex:pageBlockSection>   
            <!--
            <apex:pageBlockSection title="Property Details" columns="2" rendered="{!IF($Site.BaseUrl == null,True,False)}">
                <apex:inputField value="{!Service_Request__c.Building__c}"/>
                <apex:inputField value="{!Service_Request__c.Folio_Number__c}" required="false"/>
                <apex:inputField value="{!Service_Request__c.Unit__c}" required="true"/>
                <apex:inputField value="{!Service_Request__c.Floor__c}" required="false"/>
                <apex:pageBlockSectionItem />
                <apex:inputField value="{!Service_Request__c.Usage_Type__c}" required="false"/>
                <apex:pageBlockSectionItem />
                <apex:inputField value="{!Service_Request__c.Area_sqft__c}" required="false"/>
            </apex:pageBlockSection>    
           -->
            <apex:pageBlockSection title="Sale and Purchase Agreement Details" columns="2">
                <apex:inputField value="{!Service_Request__c.Date_of_Sale_and_Purchase_Agreement__c}" required="true"/>
                <apex:inputField value="{!Service_Request__c.Freehold_Transaction_Date__c}" required="true"/>
                <apex:inputField value="{!Service_Request__c.Purchase_Price__c}" required="true"/>
                <apex:inputField value="{!Service_Request__c.Purchase_Price_in_Words__c}" required="true"/>
                <apex:inputField value="{!Service_Request__c.Payment_Method__c}" required="true"/>
                <apex:inputField value="{!Service_Request__c.Nature_of_interest_transferred__c}" required="true"/>
            </apex:pageBlockSection>            
            
            <apex:pageBlockSection title="Mortgage details (applicable if unit is purchased through a mortgage)" columns="2">
                <apex:inputField value="{!Service_Request__c.Mortgage_Start_Date__c}" required="false"/>
                <apex:inputField value="{!Service_Request__c.Name_of_Authorized_Signatory__c}" required="{!IF(Service_Request__c.Payment_Method__c == 'Mortgage',True,False)}"/>
                <apex:inputField value="{!Service_Request__c.Mortgage_End_Date__c}" required="false"/>
                <apex:pageBlockSectionItem helpText="Kindly enter the name of Joint Authorized signatory (if applicable)">
                    <apex:outputLabel value="Name of Joint Authorized Signatory (if applicable)"/>
                    <apex:inputField value="{!Service_Request__c.New_Sponsor_AuthorizedSignatory__c}" required="false"/>
                </apex:pageBlockSectionItem>
                <apex:inputField value="{!Service_Request__c.Mortgage_Amount__c}" required="false"/>
                <apex:inputField value="{!Service_Request__c.Email_Address__c}" required="false"/>
                <apex:inputField value="{!Service_Request__c.Name_of_Mortgagee__c}" required="true"/>
                <apex:inputField value="{!Service_Request__c.Type_of_Mortgage__c}" required="true"/>
                <apex:inputField value="{!Service_Request__c.Registered_Address__c}" required="false"/>
                <apex:inputField value="{!Service_Request__c.Degree_of_Mortgage__c}" required="false"/>
                <apex:inputField value="{!Service_Request__c.Current_Registered_Phone__c}" required="false"/>
                <apex:inputField value="{!Service_Request__c.Nature_of_Interest_being_mortgaged__c}" required="false"/>
                <apex:pageBlockSectionItem />
            </apex:pageBlockSection>            
            
            <apex:pageBlockSection title="Discharge Mortgage (applicable if mortgage is registered previously on the unit)" columns="2">
                <apex:inputField value="{!Service_Request__c.Start_Date__c}" required="false"/>
               <apex:inputField value="{!Service_Request__c.Discharge_Name_of_Authorized_Signatory__c}" required="false"/>
                <apex:inputField value="{!Service_Request__c.End_Date__c}" required="false"/>
               <apex:pageBlockSectionItem helpText="Kindly enter the name of Joint Authorized signatory (if applicable)">
                    <apex:outputLabel value="Name of Joint Authorized Signatory (if applicable)"/>
                    <apex:inputField value="{!Service_Request__c.Issuing_Authority_Names__c}" required="false"/>
               </apex:pageBlockSectionItem>
                 <apex:inputField value="{!Service_Request__c.DateofDischargeofMortgage__c}" required="false"/>
                
                 <apex:inputField value="{!Service_Request__c.EmailDischargeMortgagee__c}" required="false"/>
                             
                <apex:inputField value="{!Service_Request__c.Discharge_Name_of_Mortgagee__c}" required="false"/>
                  <apex:inputField value="{!Service_Request__c.Sys_Estimated_Share_Capital__c}" required="false"/>  
                <apex:inputField value="{!Service_Request__c.Courier_Cell_Phone__c}" required="false"/>
               
                
                <apex:inputField value="{!Service_Request__c.Address_Details__c}" required="false"/>
                <apex:pageBlockSectionItem />
            </apex:pageBlockSection>            
            
            <apex:pageBlockSection title="Appointment Details (Preferred date and time)" columns="2">
                <apex:inputField value="{!Service_Request__c.Appointment_Date__c}" required="true"/>
                <apex:pageBlockSectionItem />
                <apex:inputField value="{!Service_Request__c.Appointment_Time__c}" required="true"/>
                <apex:pageBlockSectionItem />
            </apex:pageBlockSection>            
            
        </apex:pageBlock>
    </apex:form>
</apex:page>