<!--
  @description       : Common Reporting Standard (CRS) and FATCA user information
  @author            : ChangeMeIn@UserSettingsUnder.SFDoc
  @group             : 
  @last modified on  : 10-07-2020
  @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
  Modifications Log 
  Ver   Date         Author                               Modification
1.0   20/04/2021   Suchita                         Initial Version
-->
<apex:page standardController="Service_Request__c" extensions="Common_Reporting_Standard"  title="Common Reporting Standard (CRS) and FATCA user information" id="page" tabStyle="Service_Request__c" sidebar="false">
  <style type = "text/css">  
        .rightAlign {text-align:right;} 
    </style>
<apex:sectionHeader title="Common Reporting Standard (CRS) and FATCA information"/>
    <apex:actionStatus id="actStatusId">
    
     <apex:facet name="start">
          <div style="position: fixed; top: 0; left: 0; right: 0; bottom: 0; opacity: 0.25; z-index: 1000; background-color: black;">
                &nbsp;
            </div>
        <div style="position: fixed; left: 0; top: 0; bottom: 0; right: 0; z-index: 1001; margin: 15% 50%">
        <div style="display: inline-block; padding: 2px; background-color: #fff; width: 125px;">
            <img src="/img/loading.gif" style="float: left; margin: 8px;" />
            <span style="display: inline-block; padding: 10px 0px;">Please wait...</span>
        </div>
         </div>
         </apex:facet>
        </apex:actionStatus>       
<div id="DyncPgFlow:frm:j_id24" class="pageDescription">


<ui>
   This information is required in order to create your entity’s account / profile on the Ministry of Finance (MOF) portal to enable centralized filing of CRS and FATCA reporting.  
   Please select whether you will be making a CRS, FATCA or  FATCA and CRS submission on the MOF Portal. 
   <br /> <br />
   In case you have a FATCA filing, please ensure you insert the correct GIIN number (Global Intermediary identification Number (GIIN)). 
   
</ui>
<br />

<br />
<ul>
You are required to add a minimum of one (1) <b>‘Maker’</b> and  one (1) <b>‘Checker’</b>. Please ensure they are different individuals.  <br />

<li><b>‘Maker’</b> refers to the individual who shall be submitting and completing the filling and </li>
<li><b>‘Checker’</b> refers to the individual who shall be reviewing the filling before the submission is made to MOF. </li>

</ul>

</div>
<apex:form rendered="{!isNull(Service_Request__c.Submitted_Date__c)}">

<apex:pageBlock mode="Edit" id="fullpage" >
<apex:pageMessages id="errors"></apex:pageMessages>

<apex:pageBlockButtons location="bottom" rendered="{!!(TempObjAnd!=null)}" >
<apex:commandButton action="{!SaveRecord}" onClick="this.value='Saving...';this.classList.add('btnDisabled');" value="Save"/>
<apex:commandButton action="{!cancel}" value="Cancel"/>
</apex:pageBlockButtons>
<apex:pageBlockSection columns="1" collapsible="false" title=" Correspondence Details">
    <apex:outputField label="Client" value="{!Service_Request__c.Entity_Name__c}"/>
    <apex:outputField value="{!Service_Request__c.Email__c}"/>
    <apex:outputField value="{!Service_Request__c.Send_SMS_To_Mobile__c}"/>
</apex:pageBlockSection>
<apex:pageBlockSection collapsible="false" columns="1" title="Details of the Individual submitting the request">
<apex:inputField required="true" value="{!Service_Request__c.Declaration_Signatory_Name__c}"/>
<apex:inputField required="true" value="{!Service_Request__c.Declaration_Signatory_Capacity__c}"/>
</apex:pageBlockSection>
<apex:pageBlockSection collapsible="false" id="TypeOfRequest" columns="1" title="Details of the Submission Type">
    <apex:pageBlockSectionItem >
    <apex:outputLabel value="Type Of Request"/>
    <apex:actionRegion >
     <apex:selectList value="{!Service_Request__c.Type_of_Request__c}" size="1" > 
                 <apex:selectOption itemValue="Both" itemLabel="Both" />
                 <apex:selectOption itemValue="FATCA" itemLabel="FATCA" />
                 <apex:selectOption itemValue="CRS" itemLabel="CRS" />
                  <apex:actionSupport event="onchange" rerender="TypeOfRequest"/>
     </apex:selectList>
    </apex:actionRegion>
    </apex:pageBlockSectionItem>
    
    <apex:pageBlockSectionItem id="Giin" rendered="{!IF(Service_Request__c.Type_of_Request__c !='CRS',true,false)}">
       
            <apex:outputLabel style="color: #4a4a56;" value="Global Intermediary identification Number (GIIN)" />
            <apex:inputField label="Global Intermediary identification Number (GIIN)" Required="true" value="{!Service_Request__c.Summary__c}" />
        
        
        
    </apex:pageBlockSectionItem>
    
     <apex:outputPanel rendered="{!IF(Service_Request__c.Type_of_Request__c !='CRS',true,false)}">
        <br />
        <small>The GIIN is a 19-digit alphanumeric identifier and should be entered with appropriate punctuation (period or decimal). Example: 98Q96B.00000.LE.250  
         </small>
        </apex:outputPanel>
        
</apex:pageBlockSection>
<apex:outputPanel id="ContactPanel">  
<apex:pageBlockSection columns="1" collapsible="false" title="Contact Details">
   <apex:variable var="rowNum" value="{!0}"  />            
 <apex:pageBlockTable value="{!ListofNewAmend}" var="page" id="table" > 
 
 
   <apex:column headerValue="Action" width="100" >
            <apex:commandLink value="Delete" action="{!removingUBORow}" immediate="true">
              <apex:param value="{!rowNum}" name="index" assignTo="{!rowNumberToEdit}"/> 
            </apex:commandLink>
            &nbsp; | &nbsp;
             <apex:commandLink value="Edit" action="{!EditUBORecord}" immediate="true">
              <apex:param value="{!rowNum}" name="index" assignTo="{!rowNumberToEdit}"/>
            </apex:commandLink>
            <apex:facet name="footer">
                  <!-- <apex:commandButton value="Add" action="{!AddAndRecord}" rendered="{!!(TempObjAnd!=null)}" immediate="true" />-->
               
            </apex:facet>  
            
             <apex:variable var="rowNum" value="{!rowNum + 1}"/>                
            </apex:column>            
            <apex:column headerValue="Name" >
            {!page.Title_new__c} {!page.Given_Name__c} {!page.Family_Name__c}            
             </apex:column>            
            <apex:column value="{!page.Person_Email__c}"/>
            <apex:column headerValue="User Name*" value="{!page.UserName__c}"/>
            <apex:column value="{!page.Mobile__c}"/>
            <apex:column value="{!page.Amendment_Type__c}"/>
            
            
             <apex:facet name="footer">
             <apex:outputPanel >
                  
         <apex:commandButton value="Add Maker" action="{!AddMaker}"  immediate="true" />
         <apex:commandButton value="Add Checker" action="{!AddChecker}" immediate="true" />
        </apex:outputPanel>
                </apex:facet>
                
                
                
 </apex:pageBlockTable> 
 <p> *User Name for the Ministry of Finance (MOF) portal for centralising CRS and FATCA filing</p>
 
  
 <apex:pageBlockSection rendered="{!Type!=null}"  collapsible="false" columns="1" Title="Please select whether you want to add existing portal user or add new">

     <apex:actionRegion > 
         <apex:selectList value="{!Summary}" size="1" label="Type" > 
                <apex:actionSupport event="onchange" reRender="ExistingPanel,ContactPanel,fullpage" action="{!addNewExisting}" />
                 <apex:selectOption itemValue="None" itemLabel="Please Select" />
                 <apex:selectOption itemValue="AddExisting" itemLabel="Add existing portal user" />
                 <apex:selectOption itemValue="AddNew" itemLabel="Add New" />
            </apex:selectList>
      </apex:actionRegion>

  </apex:pageBlockSection>
  
   <apex:pageBlockSection title="Select Existing Portal User"  collapsible="false" columns="1" id="ExistingPanel" rendered="{!addnewExistingBool}"> 
            <apex:pageBlockSectionItem >
            <apex:selectList value="{!selectedVal}" size="1" rendered="{!addnewExistingBool}" > 
                <apex:actionSupport event="onchange" action="{!fillContactDetails}" reRender="ContactPanel,errors"/>
                 <apex:selectOptions value="{!ContactOptions}" /> 
            </apex:selectList>
       </apex:pageBlockSectionItem>
 </apex:pageBlockSection>
 <apex:pageBlockSection title="Add New"  collapsible="false" columns="1" id="out" rendered="{!TempObjAnd!=null}"> 
 <apex:message />
        <apex:inputField required="true" value="{!TempObjAnd.Title_new__c}" />
        <apex:inputField required="true" label="First Name" value="{!TempObjAnd.Given_Name__c}" />
        <apex:inputField required="true" label="Last Name" value="{!TempObjAnd.Family_Name__c}" />
        <apex:inputField required="true" value="{!TempObjAnd.Person_Email__c}" />
        <apex:inputField required="true" value="{!TempObjAnd.Mobile__c}" />
    <apex:pageBlockSectionItem >
        <apex:outputPanel >
            <apex:commandButton value="Save"  onClick="this.value='Adding...';this.classList.add('btnDisabled');"   action="{!SaveAmdRow}"/>
            <apex:commandlink value="Cancel" action="{!CancelSaveRow}" immediate="true" />
        </apex:outputPanel>                       
    </apex:pageBlockSectionItem>
 </apex:pageBlockSection>
 
 </apex:pageBlockSection>
   </apex:outputPanel>
 </apex:pageBlock>
</apex:form>
</apex:page>