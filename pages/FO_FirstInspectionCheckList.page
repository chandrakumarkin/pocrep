<apex:page docType="html-5.0" applyHtmlTag="false" applyBodyTag="false" standardStylesheets="true" controller="FO_Cls_FirstInspection" showHeader="false" sidebar="false" id="inspectionChecklist">
    <head>
    <!-- <meta charset="utf-8"/>  -->
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <!-- <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"></meta>  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"></meta>
    <title>DIFC - Dubai International Financial Centre</title>
    
            <link rel="shortcut icon" href="{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/favicon.ico')}" type="image/x-icon"/>
    <link rel="icon" href="{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/favicon.ico')}" type="image/x-icon"/>
    
    <apex:stylesheet value="{!URLFOR($Resource.difc_portal, 'DIFC_CommunityResource/css/difc.min.css')}" html-media="all" />
    <apex:stylesheet value="{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/css/ie.css')}" html-media="all" />
    
    <!--[if (gte IE 9)|!(IE)]><!-->
            <link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/css/responsive.css')}" media="all" ></link>
    <!--<![endif]-->
    
    <style media="all">
        .main-content {margin: 0 auto;}
        .errMsg{color:red;}
                
        .difc-logo img {height: 55px;}
        @media (max-width: 980px){
        nav.top-bar ul {
                padding: 0 6px;
        }
        }
        .top-nav.news ul.mobile-logo, .top-nav.dashboard ul.mobile-logo {
        padding: 0px;
        margin: 0 10px !important;
        } 
        .top-bar-section ul li > a {font-family: inherit;}  
        footer small {font-style: italic;}    
        html .brandSecondaryBrd {
                border-top-color: #00205B !important;
                border-bottom: 1px solid #eaeaea;
                border-left: 1px solid #eaeaea;
                border-right: 1px solid #eaeaea;
        }
        nav.top-nav .top-bar-section ul.right li, nav.top-nav .top-bar-section ul.right li a{            
                font-size: 14px !important;
                background: rgba(0, 0, 0, 0);            
        }
        i.icon-phone-white{            
        background: url("{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/images/icon-phone-small-white.png')}") no-repeat scroll 0 0 rgba(0, 0, 0, 0) !important;
        width: 20px !important;
        }
        nav.top-nav .top-bar-section ul li .icon-phone-white{            
        background-position: left 3px !important;
        }
        .top-bar-section li:not(.has-form) a:not(.button){
        background: none !important;
        padding: 0 15px !important;
        line-height: 45px;
    
        }
        .top-bar-section i.icon{            
        float: none !important;
        line-height: 45px;
        }
        .top-bar.top-nav ul, .top-bar.top-nav ul li{                
        line-height: 65px;
        font-size: 12px;
        }
        .top-bar-section li.call-centre{margin-right: 0 !important; background: transparent;}           
        body .apexp .bPageBlock.apexDefaultPageBlock .pbHeader {           
        border-bottom: 0px !important;   
        }
        table.multiSelectPicklistTable tbody tr:first-of-type{
        display:none;
        }
        tr.multiSelectPicklistRow select{           
        height:70px;
        width:160px;
        }
        input[type=file]{ width: 400px;}
        
        /* 04/05/2016 - Claude - Added CSS to change display of IDAMA comments */
        @media screen
        {
            .noPrint {
                display:initial;
            }
            
            .noScreen{
                display:none;
            }
        }
        
        @media print
        {
            .noPrint{
                display:none;
            }
            
            .noScreen{
                display:initial;
            }
            
            nav.top-bar {
                height: 70px !important;
            }
            
            .primary-nav {
                background: #f8f8f8 !important;
                margin-bottom: 0 !important;
                border-bottom: solid 1px #d8d9da !important;
                z-index: 9999999 !important;
                height: 60px!important;
            }
            
            .colored-label {
                font-weight: bold !important;
                background-color:#141B4D !important;
                color: white !important;
            }
            
            #main-table {
                margin-top: 7px !important;
            }
            
            #date--label, #location-label {
                width: 15% !important;
            }
            
            #location-value, #date--value, #permit-no-value, #idama-only--label {
                width: 13.53% !important;
            }
            
            #tenant-value, #fit-out-contractor--value {
                width: 20% !important;
            }
            
            #fit-out-contractor--label, #tenant-label {
                width: 15% !important;
            }
            
            #si-label {
                width:5% !important;
            }
            
            #general-arch--label {
                width: 50% !important;
            }
            
            #fit-out-works-label {
                width: 35% !important;
            }
            
            #permit-no-label {
                width: 15% !important;
            }
            
            a[href]:after {
                content: none !important;
              }
            
        }
       
        textarea {
            overflow: hidden;
        }
        
        .dateFormat {
            display: none;
        }
        
        
    </style>
    </head>
    <body>
        <div class="login">
            <nav class="row full-width top-bar top-nav dashboard">
                <ul class="mobile-logo left">
                    <li class="difc-logo">
                        <a target="_blank" href="http://www.difc.ae">
                            <img src="{!URLFOR($Resource.DIFC_PortalResource, 'DIFC_CommunityResource/images/DIFC_white_logo.png')}"/>
                        </a>
                    </li>
                </ul>
            </nav>    
            <div class="row full-width top-bar primary-nav hide-for-small hide-for-small"></div>
            <div class="row full-width">  
                <div class="large-12 columns">   
                    <br></br>
                    <h4>
                    {!HTMLENCODE(checkListHeader)} (for FOSP use only)</h4>
                    <br></br>
                </div>
                <!------------------User Access Form Code---------------------------->
                <apex:form id="inspectionForm">
                
                    <!-- Added by: Claude Manahan. 06-02-2016 Action function to capture inspection date -->
                    <apex:actionFunction name="setInspectionDate" action="{!setInspectionDate}" reRender="inspectionDateSection" >
                        <apex:param name="param1" value="" assignTo="{!inspectionDate}" />
                    </apex:actionFunction>
                
                    <apex:messages styleClass="errMsg"/>
                    <div class="large-12 columns">
                        <apex:pageBlock id="inspectionBlock">  
                            <table id='main-table' class="wrap-container" >
                                <apex:outputPanel >
                                    <table border="1" style="width:100%; margin-top:-21px;">
                                        <tr>
                                            <td id="fit-out-works-label" class="colored-label" style="font-weight: bold; background-color:#141B4D;color: white;">Fit-Out Works Information</td>
                                            <td class="colored-label" id='permit-no-label' style="font-weight: bold;width:125px;background-color:#141B4D;color: white;">Permit No: </td>
                                            <td id='permit-no-value' style="width:276px"><apex:outputText value="{!HTMLENCODE(srObj.Current_Registered_Name__c)}"/></td>
                                        </tr>
                                    </table>
                                    <table border="1" style="width:100%; margin-top:-21px;">
                                        <tr>
                                            <td id='tenant-label' class="colored-label" style="font-weight: bold;width:208px;background-color:#141B4D;color: white;">Tenant:</td>
                                            <td id='tenant-value'><apex:outputText value="{!HTMLENCODE(srObj.Customer__r.name)}"/></td>
                                            <td id="location-label" class="colored-label" style="font-weight: bold;width:125px;background-color:#141B4D;color: white;">Location:</td>
                                            <td id='location-value' style="width:276px"><apex:outputText value="{!HTMLENCODE(srObj.Location__c)}"/></td>
                                        </tr>
                                    </table>
                                    <table border="1" style="width:100%; margin-top:-21px;">
                                        <tr>
                                            <td id='fit-out-contractor--label' class="colored-label" style="font-weight: bold;width:208px;background-color:#141B4D;color: white;">Fit-Out Contractor:</td>
                                            <td id='fit-out-contractor--value' ><apex:outputText value="{!HTMLENCODE(contractor)}"/></td>
                                            <td id='date--label' class="colored-label" style="font-weight: bold;width:125px;background-color:#141B4D;color: white;">Date/Time:</td>
                                            <td id='date--value' style="width:276px">
                                                <apex:outputPanel id="inspectionDateSection">                   
                                                    <apex:inputField value="{!foObj.Inspection_Date__c}" rendered="{!showresult=='false' || showresult==''}" onchange="setInspectionDate(this.value);" onblur="setInspectionDate(this.value)" />
                                                </apex:outputPanel>
                                                <apex:outputText value="{!HTMLENCODE(inspectionDateFormatted)}" rendered="{!showresult=='true'}"  /> 
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="1" style="width:100%; margin-top:-21px;">   
                                        <tr>
                                            <td id='general-arch--label' class="colored-label" style="font-weight: bold;;background-color:#141B4D;color: white;">General and Architectural</td>
                                            <td id='idama-only--label' class="colored-label left-labels" style="font-weight: bold;text-align:center ; width:276px;background-color:#141B4D;color: white;">For FOSP use only</td>
                                        </tr>
                                    </table>
                                    <table border="1" style="width:100%; margin-top:-21px;">
                                        <tr>
                                            <td class="colored-label" id='si-label' style="font-weight: bold;text-align:center; width:5%;background-color:#141B4D;color: white;">SI.</td>
                                            <td class="colored-label" style="font-weight: bold;text-align:center; background-color:#141B4D;color: white; -webkit-print-color-adjust: exact;">Item Description</td>
                                            <td class="colored-label" style="font-weight: bold;text-align:center; background-color:#141B4D;color: white;width:7.45%">Yes</td>
                                            <td class="colored-label" style="font-weight: bold;text-align:center; background-color:#141B4D;color: white;width:7%">No</td>
                                            <td class="colored-label" style="font-weight: bold;text-align:center; background-color:#141B4D;color: white;width:7%">N/A</td>
                                        </tr>
                                        <apex:variable value="{!1}" var="count"/>
                                        <apex:repeat id="questionList1" value="{!questionsList}" var="qObj">
                                            <apex:outputPanel rendered="{!qObj.lookUpObj.Check_List_classification__c == 'General and Architectural'}">
                                                <tr>
                                                    <td style="font-weight: bold;text-align:center; width:5%">{!FLOOR(count)}</td>
                                                    <td>{!HTMLENCODE(qObj.lookUpObj.Description__c)}</td>
                                                    <td style="font-weight: bold;text-align:center; width:7%">
                                                        <apex:inputCheckBox disabled="{!IF(showresult=='false' || showresult=='', false, true)}"  value="{!qObj.yes}" id="yes1" onchange="changeValues('yes-{!count}', {!count})" styleClass="yes-{!count}"/>
                                                    </td>
                                                    <td style="font-weight: bold;text-align:center; width:7%">
                                                        <apex:inputCheckBox disabled="{!IF(showresult=='false' || showresult=='', false, true)}"  value="{!qObj.no}" id="no1" onchange="changeValues('no-{!count}', {!count})" styleClass="no-{!count}"/>
                                                    </td>
                                                    <td style="font-weight: bold;text-align:center; width:7%">
                                                        <apex:inputCheckBox disabled="{!IF(showresult=='false' || showresult=='', false, true)}"  value="{!qObj.na}" id="na1" onchange="changeValues('na-{!count}', {!count})" styleClass="na-{!count}"/>
                                                    </td>
                                                </tr>
                                                <apex:variable var="count" value="{!count+ 1}"/>
                                            </apex:outputPanel>
                                        </apex:repeat>
                                        <tr>
                                            <td class="colored-label" colspan="5" style="font-weight: bold;background-color:#141B4D;color: white;">Electromechanical Installation</td>
                                        </tr>
                                        <apex:repeat id="questionList2" value="{!questionsList}" var="qObj">
                                            <apex:outputPanel rendered="{!qObj.lookUpObj.Check_List_classification__c == 'Electromechanical Installation'}">
                                                <tr>
                                                    <td style="font-weight: bold;text-align:center; width:5%">{!FLOOR(count)}</td>
                                                    <td><apex:outputText value="{!HTMLENCODE(qObj.lookUpObj.Description__c)}" escape="false"/></td> 
                                                    <td style="font-weight: bold;text-align:center; width:7%">
                                                        <apex:inputCheckBox disabled="{!IF(showresult=='false' || showresult=='', false, true)}"  value="{!qObj.yes}" id="yes2" onchange="changeValues('yes-{!count}', {!count})" styleClass="yes-{!count}"/>
                                                    </td>
                                                    <td style="font-weight:bold;text-align:center; width:7%">
                                                        <apex:inputCheckBox disabled="{!IF(showresult=='false' || showresult=='', false, true)}"  value="{!qObj.no}" id="no2" onchange="changeValues('no-{!count}', {!count})" styleClass="no-{!count}"/>
                                                    </td>
                                                    <td style="font-weight: bold;text-align:center; width:7%">
                                                        <apex:inputCheckBox disabled="{!IF(showresult=='false' || showresult=='', false, true)}"  value="{!qObj.na}" id="na2" onchange="changeValues('na-{!count}', {!count})" styleClass="na-{!count}"/>
                                                    </td>
                                                </tr>
                                                <apex:variable var="count" value="{!count+ 1}"/>
                                            </apex:outputPanel>
                                        </apex:repeat>
                                        <tr>
                                            <td class="colored-label" colspan="5" style="font-weight: bold;background-color:#141B4D;color: white;">FOSP COMMENTS:</td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="height:130px;">
                                                <apex:inputTextarea styleClass="noPrint" rendered="{!IF(showresult=='false' || showresult=='', true, false)}"  id="idamaComments" value="{!foObj.IDAMA_Comments__c}" style="height:100%;width:100%;"/>
                                                <div> 
                                                    <apex:outputField value="{!foObj.IDAMA_Comments__c}" rendered="{!IF(showresult=='false' || showresult=='', false, true)}" />
                                                </div>
                                            </td>
                                        </tr>
                                        <apex:outputPanel rendered="{!checklistType == 'Final Inspection Checklist'}" layout="none" >
                                            <tr>
                                                <td colspan="7">
                                                    <strong>Inspection Passed / Inspection Failed: {!HTMLENCODE(stepObj.Status_Code__c)}</strong>
                                                </td>
                                            </tr>
                                        </apex:outputPanel>
                                    </table>
                                    <table border="1" style="width:100%; margin-top:-21px;">
                                        <tr>
                                            <td class="colored-label" rowspan="2" style="font-weight: bold;background-color:#141B4D;color: white;"></td>
                                            <td class="colored-label" rowspan="2" style="text-align:center;font-weight: bold;background-color:#141B4D;color: white;">FOSP HSE</td>
                                            <td class="colored-label" colspan="3" style="text-align:center;font-weight: bold;background-color:#141B4D;color: white;">FOSP Fit-Out</td>
                                        </tr>
                                        <tr>
                                            <td class="colored-label" style="text-align:center;font-weight: bold;background-color:#141B4D;color: white;">Electrical Engr.</td>
                                            <td class="colored-label" style="text-align:center;font-weight: bold;background-color:#141B4D;color: white;">Mechanical Engr.</td>
                                            <td class="colored-label" style="text-align:center;font-weight: bold;background-color:#141B4D;color: white;">Civil Engr.</td>
                                        </tr>
                                        <tr>
                                            <td class="colored-label" style="text-align:center;font-weight: bold;background-color:#141B4D;color: white;">Name</td>
                                            <td style="text-align:center;"><apex:inputText disabled="{!IF(showresult=='false' || showresult=='', false, true)}"  id="idama" value="{!foObj.IDAMA_HSE_Name__c}" style="height:100%;width:100%;"/></td>
                                            <td style="text-align:center;"><apex:inputText disabled="{!IF(showresult=='false' || showresult=='', false, true)}"  id="electrical" value="{!foObj.IDAMA_FO_Electrical__c}" style="height:100%;width:100%;"/></td>
                                            <td style="text-align:center;"><apex:inputText disabled="{!IF(showresult=='false' || showresult=='', false, true)}"  id="mechanical" value="{!foObj.IDAMA_FO_Mechanical__c}" style="height:100%;width:100%;"/></td>
                                            <td style="text-align:center;"><apex:inputText disabled="{!IF(showresult=='false' || showresult=='', false, true)}"  id="civil" value="{!foObj.IDAMA_FO_Civil__c}" style="height:100%;width:100%;"/></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;font-weight: bold;background-color:#141B4D;color: white;"></td>
                                            <td colspan="2" style="text-align:center;font-weight: bold;background-color:#141B4D;color: white;">Tenant</td>
                                            <td colspan="2" style="text-align:center;font-weight: bold;background-color:#141B4D;color: white;">Contractor</td>
                                        </tr>
                                        <tr>
                                            <td class="colored-label" style="text-align:center;font-weight: bold;background-color:#141B4D;color: white;">Name</td>
                                            <td colspan="2" style="text-align:center;">
                                                <apex:inputText style="height:100%;width:100%;" disabled="{!IF(showresult=='false' || showresult=='', false, true)}" value="{!foObj.Client_Name__c}" id="clientName"/>
                                            </td>
                                            <td colspan="2" style="text-align:center;">
                                                    <apex:inputText style="height:100%;width:100%;" disabled="{!IF(showresult=='false' || showresult=='', false, true)}" value="{!foObj.Contractor_Name__c}" id="contractorName"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5" style="text-align:center;">
                                            <apex:actionFunction action="{!submitInspection}" name="saveValue"/>
                                            <apex:commandButton value="Submit Inspection" onClick="checkRequiredValues(); return false;" style="display:{!IF(showresult=='false' || showresult=='','','none')};"/>
                                            &nbsp;&nbsp;
                                            <apex:commandButton action="{!cancelInspection}" value="{!IF(showresult=='false' || showresult=='','Cancel','Back')}" styleclass="noPrint" />
                                            </td>
                                        </tr>
                                    </table>
                                </apex:outputPanel>
                            </table>
                        </apex:pageBlock>
                    </div>
                    <script>
                    
                        function checkRequiredValues(){
                            var hasError = false;
                            hasYes = false;
                            hasNo = false;
                            hasNa = false;
                            
                            var listSize = {!questionsList.size};
                            
                            if(document.getElementById('inspectionChecklist:inspectionForm:inspectionBlock:idama').value == '' || document.getElementById('inspectionChecklist:inspectionForm:inspectionBlock:electrical').value == '' || document.getElementById('inspectionChecklist:inspectionForm:inspectionBlock:mechanical').value == '' || document.getElementById('inspectionChecklist:inspectionForm:inspectionBlock:civil').value == ''){
                                    hasError = true;
                                    alert('Please Provide FOSP HSE and FOSP Fit-Out Details');
                                    return false;
                            }
                            else if(document.getElementById('inspectionChecklist:inspectionForm:inspectionBlock:clientName').value == '' || document.getElementById('inspectionChecklist:inspectionForm:inspectionBlock:contractorName').value == ''){
                                hasError = true;
                                alert('Please Provide Client and Contractor details.');
                                return false;
                            }
                            
                            for(var i=1;i<=listSize;i++){
                                var className = 'yes-'+[i];
                                if(document.getElementsByClassName(className)[0].checked == true){
                                    hasYes = true;
                                }
                            }
                            
                            for(var i=1;i<=listSize;i++){
                                var className = 'no-'+[i];
                                if(document.getElementsByClassName(className)[0].checked == true){
                                    hasNo = true;
                               }
                            }
                            
                            for(var i=1;i<=listSize;i++){
                                var className = 'na-'+[i];
                                if(document.getElementsByClassName(className)[0].checked == true){
                                    hasNa = true;
                                }
                            }
                            
                            if(hasYes==false && hasNo==false && hasNa==false){
                                alert('Please perform inspection checklist before submitting');
                            }
                            
                            if(hasError==false && (hasYes ==true || hasNo ==true || hasNa ==true)){
                                saveValue();
                            }
                        }
                        
                        function changeValues(temp,count){
                            console.log('--temp--'+temp);
                            var yesClass = 'yes-'+[count];
                            var noClass = 'no-'+[count];
                            var naClass = 'na-'+[count];
                            if(temp== 'yes-'+[count] && document.getElementsByClassName(temp)[0].checked == true){
                                document.getElementsByClassName(noClass)[0].checked = false;
                                document.getElementsByClassName(naClass)[0].checked = false;
                            }    
                            else if(temp== 'no-'+[count] && document.getElementsByClassName(noClass)[0].checked == true){
                                document.getElementsByClassName(yesClass)[0].checked = false;
                                document.getElementsByClassName(naClass)[0].checked = false;
                            }
                            else if(temp== 'na-'+[count] && document.getElementsByClassName(naClass)[0].checked == true){
                                document.getElementsByClassName(noClass)[0].checked = false;
                                document.getElementsByClassName(yesClass)[0].checked = false;
                            }
                        }
                        
                       var te = document.querySelector('textarea');
                       te.addEventListener('keydown', resizeTextarea);
                    
                       function resizeTextarea(ev) {
                           this.style.height = '24px';
                           this.style.height = this.scrollHeight + 12 + 'px';
                       }
                       
                       te.style.height = '24px';
                       te.style.height = this.scrollHeight + 12 + 'px';
                       
                    </script>
                </apex:form>
            </div>
        </div>
    </body>   
</apex:page>