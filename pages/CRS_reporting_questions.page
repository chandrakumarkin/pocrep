<apex:page standardController="CRS_Form__c" action="{!RediectURL}" sidebar="false" extensions="cls_CRS_reporting_questions">

<style>

.bPageBlock .detailList .labelCol 
{
width:33% !important;
}
.helptext
{
font-size:11px;
color:#707070;
padding-left: 10px;
}
.helptextp
{
font-size:11px;
color:#707070;
padding-left: 0px;
}

.helptext h2
{
font-size:15px;
color:#707070;
font-weight: 900;

}

.helptext ul
{
padding-left: 40px;
}



 /* This is for the full screen DIV */
    .popupBackground {
        /* Background color */
        background-color:black;
        opacity: 0.20;
        filter: alpha(opacity = 20);
    
        /* Dimensions */
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 998;
        position: absolute;
        
        /* Mouse */
        cursor:wait;
    }
 
    /* This is for the message DIV */
    .PopupPanel {
        /* Background color */
        border: solid 2px blue;
        background-color: white;
 
        /* Dimensions */
        left: 50%;
        width: 200px;
        margin-left: -100px;
        top: 50%;
        height: 50px;
        margin-top: -25px;
        z-index: 999;
        position: fixed;
        
        /* Mouse */
        cursor:pointer;
        
</style>



<apex:pageMessages ></apex:pageMessages>
<apex:sectionHeader title="CRS COMPLIANCE FORM"/>
 <apex:form >
 
 
    <apex:actionStatus id="actStatusId">
    
     <apex:facet name="start">
          <div style="position: fixed; top: 0; left: 0; right: 0; bottom: 0; opacity: 0.25; z-index: 1000; background-color: black;">
                &nbsp;
            </div>
        <div style="position: fixed; left: 0; top: 0; bottom: 0; right: 0; z-index: 1001; margin: 15% 50%">
        <div style="display: inline-block; padding: 2px; background-color: #fff; width: 125px;">
            <img src="/img/loading.gif" style="float: left; margin: 8px;" />
            <span style="display: inline-block; padding: 10px 0px;">Please wait...</span>
        </div>
         </div>
         </apex:facet>
        </apex:actionStatus>
        
     <p>
  
<p> 
The purpose of this Common Reporting Standard (CRS) Compliance Form is to provide the Registrar of Companies with information relating to the reporting and due diligence procedures of Reporting Financial Institutions under the<a href="https://www.difc.ae/files/7915/2162/8256/Common_Reporting_Standard_Law_DIFC_Law_No._2_of_2018.pdf" target="_blank"> Common Reporting Standards Law 2018</a> and  <a href="https://www.difc.ae/files/6715/2327/6154/Common_Reporting_Standard_Regulations.pdf" target="_blank"> Regulations</a>, in respect to the reporting period ending 31 December 2019. The CRS Law and Regulations primarily apply to financial services entities that are regulated by the DFSA, but certain non-financial 
services entities may be required to report under the DIFC CRS and should therefore complete this form as well. </p>

<p>Your CRS filing for the reporting period ending 31 December 2019 can be filed after completing this form. This form and your CRS and FATCA (where applicable) filing must be submitted by <b>31 July 2020</b>.</p>
<p>Further information on CRS can be found in the <a target="_blank" href="https://www.difc.ae/files/8915/9007/2577/CRS_Guidance_Notes.pdf"> Guidance</a> and the DIFC CRS <a href="https://www.difc.ae/business/operating/common-reporting-standard/"  target="_blank"> webpage</a> . A sample of the CRS Compliance Form for Financial entities can be viewed <a target="_blank" href="https://difc.my.salesforce.com/sfc/p/20000000nkZ5/a/0J000000YCDj/F93HyUkR6G83PHsn9dr8OjbUuZGifWwjoRAHyl4Ql8A">here</a>, while the Non-Financial entities form can be can be viewed <a target="_blank" href="https://difc.my.salesforce.com/sfc/p/20000000nkZ5/a/0J000000YCDo/wwGCW4Jo8tCQXQIwKFkFj_XiwtqHcG2FfvWbSqc8l54">here</a> .</p>



     
     </p>
     

 <!---mindetail--->
<apex:pageBlock mode="mindetail">
<apex:pageBlockButtons location="bottom">
<apex:commandButton action="{!SaveRecord}" rendered="{!ObjQuestion.Status__c!='Submitted'}" value="Submit CRS Compliance Form"/>
<apex:commandButton action="{!Cancel}" value="Cancel" immediate="true"/>
</apex:pageBlockButtons>
<apex:inputField value="{!ObjQuestion.Company_Type__c}"  style="display:none;" />
<apex:pageBlockSection columns="1" title="Designated Contact Person" id="MainDesignated" collapsible="false">
<p class="helptext">
The Designated Contact Person can be anyone who has the authority to respond to queries relating to this form and provide further clarification or documentation (if required).
<br />
Examples of a <b>Designated Contact Person</b> may include the entity's:
<ul class="helptext">

<li>CEO, General Manager or Senior Executive Officer;</li>
<li> Director or Authorised Signatories;</li>
<li> Compliance Officer or a member of its in-house legal team</li>

</ul>

</p>

<!----5----> <apex:inputField required="true" value="{!ObjQuestion.Title__c}"/>
<!----6----> <apex:inputField required="true" value="{!ObjQuestion.First_Name__c}"/>
<apex:inputField required="true" value="{!ObjQuestion.Last_Name__c}"/>
<!----6----> <apex:inputField required="true" value="{!ObjQuestion.Designation__c}"/>

<!----7----> <apex:inputField required="true" value="{!ObjQuestion.Telephone_No__c}"/>
<!----8----> <apex:inputField required="true" value="{!ObjQuestion.Email_Address__c}"/>



</apex:pageBlockSection>

<apex:pageBlockSection columns="1" title="Reporting Financial Institution Type" collapsible="false">

<p class="helptext">
The following Institutions are Reporting Financial Institutions ("RFI"):
<ul class="helptext">
<li > <b>Custodial Institution</b> means any Entity that holds, as a substantial portion of its business Financial Assets for the account of others. <br /></li>
<li ><b>Depository Institution</b> means any Entity that accepts deposits in the ordinary course of a banking or similar business.<br /></li>
<li ><b>Specified Insurance Company</b> means any Entity that is an insurance company (or the holding company of an insurance company) which issues, or is obligated to make payments with respect to, a Cash Value Insurance Contract or an Annuity Contract. <br /></li>
<li > <b>Investment Entity</b> means any Entity that: 
<ol class="helptext" type="a">

    <li>primarily conducts a business involving managing, investing, reinvesting or trading in Financial Assets on behalf of a customer; or </li>
    <li >if the Entity is managed by a Reporting Financial Institution referred to in (a) above.</li>
     
</ol>
</li>
   <p class="helptextp"><br />
   Please refer to the <a href="https://www.difc.ae/files/8915/9007/2577/CRS_Guidance_Notes.pdf" target="_blank">Guidance</a> for more information on the definition of Investment Entity in particular <br />
Please refer to <a href="https://www.difc.ae/files/6715/2327/6154/Common_Reporting_Standard_Regulations.pdf" target='_blank'>the Common Reporting Standards Regulations ("Regulations") </a> for the full definitions of each of each of the above terms. 
</p>

</ul> 

</p>

<script>


function InstitutionDropdown(objCm,CheckVale,pickListvale)
{

if(objCm.value=='Yes')
return FinancialInstitution(objCm,CheckVale,pickListvale);
else
return true;

}
function FinancialInstitution(PickVal,CheckVale,seInstitution_type)
{
var ValExist=false;
if(seInstitution_type!=undefined)
ValExist=seInstitution_type.includes(CheckVale);

if(ValExist==false)
{
PickVal.value='';
alert('Please select Reporting Financial Institution type '+CheckVale);
}
return ValExist;
//

}


function MultiSelectValues(ObjSel) 
{
var Complatevalue='';

 for (var i = 0; i < ObjSel.length; i++) {
         if(ObjSel[i].selected ==true){
         Complatevalue+=ObjSel[i].value+';';

          }
          
}
//alert(Complatevalue);
return Complatevalue;

}

</script>

<apex:actionFunction name="RFI_Selected" action="{!RFI_Selected}" immediate="true" reRender="ConfirmRFIType,divreported" status="actStatusId">
   <apex:param name="myParam" value=""/>
    </apex:actionFunction> 
    
<!----8----> <apex:inputField required="true" onchange="RFI_Selected(MultiSelectValues(this));"  value="{!ObjQuestion.Reporting_Financial_Institution_type__c}"/>

</apex:pageBlockSection>
<apex:outputPanel id="divreported">
<apex:outputPanel rendered="{!NOT(Contains(ObjQuestion.Reporting_Financial_Institution_type__c,'Not a Reporting Financial Institution'))}">

<!---title="Confirm RFI Type"--->
 <apex:pageBlockSection columns="1"  id="ConfirmRFIType" collapsible="false">
 
    <!----12----> <apex:inputField onchange="return InstitutionDropdown(this,'Custodial Institution','{!ObjQuestion.Reporting_Financial_Institution_type__c}');" required="true" rendered="{!AND(Contains(LicenseActivity,'072'),NOT(Contains(ObjQuestion.Reporting_Financial_Institution_type__c,'Custodial Institution')))}" label="Was the entity a Custodial Institution during the reporting period?" value="{!ObjQuestion.Custodial_Institution__c}"/>
    
    <!----13----> <apex:inputField onchange="return InstitutionDropdown(this,'Depository Institution','{!ObjQuestion.Reporting_Financial_Institution_type__c}');" required="true" rendered="{!AND(Contains(LicenseActivity,'001'),NOT(Contains(ObjQuestion.Reporting_Financial_Institution_type__c,'Depository Institution')))}" label="Was the entity a Depository Institution during the reporting period?" value="{!ObjQuestion.Depository_Institution__c}"/>
    
    <!----14----> <apex:inputField onchange="return InstitutionDropdown(this,'Investment Entity','{!ObjQuestion.Reporting_Financial_Institution_type__c}');" required="true"  label="Was the entity an Investment Entity during the  reporting period ?" rendered="{!NOT(Contains(ObjQuestion.Reporting_Financial_Institution_type__c,'Investment Entity'))}" value="{!ObjQuestion.Investment_Entity__c}"/>
    
    <!----15----> <apex:inputField onchange="return InstitutionDropdown(this,'Specified Insurance Company','{!ObjQuestion.Reporting_Financial_Institution_type__c}');" required="true" rendered="{!AND(OR(Contains(LicenseActivity,'028'),Contains(LicenseActivity,'015'),Contains(LicenseActivity,'040')), NOT(Contains(ObjQuestion.Reporting_Financial_Institution_type__c,'Specified Insurance Company')))}" label="Was the entity a Specified Insurance Company during the reporting period ?" value="{!ObjQuestion.Insurance__c}"/>
    
</apex:pageBlockSection>

<apex:actionFunction name="crstypeChanged" action="{!NullReturn_Other}" immediate="true" reRender="crstype" status="actStatusId">
   <apex:param name="myParam" value=""/>
    </apex:actionFunction> 
 
 
<!-----===============================================================================================-----> 
<apex:pageBlockSection columns="1" title="Nil/Null Returns" collapsible="false" id="crstype">

 <p class="helptext">
A Nil/Null return is filed to indicate that the RFI did not maintain any Reportable Accounts during the reporting period.
 </p>

<!----16----> <apex:inputField required="true" label="Is a Nil/Null return being filed" value="{!ObjQuestion.Null_return_being_filed__c}"/>
<!----17----> <apex:inputField required="true" onchange="crstypeChanged(this.value);"  value="{!ObjQuestion.Reason_for_filing_a_Null_return__c}"/>
<!----18----> <apex:inputField required="true"  rendered="{!OR(ObjQuestion.Reason_for_filing_a_Null_return__c='Other')}"  label="Please provide detailed reasons" value="{!ObjQuestion.Detailed_reasons__c}"/>

</apex:pageBlockSection>


<apex:actionFunction name="thirdpartychanged" action="{!ThirdParty_Yes}" immediate="true" reRender="thirdpartylist" status="actStatusId">
   <apex:param name="myParam" value=""/>
    </apex:actionFunction> 
    
<!-----===============================================================================================-----> 

<apex:pageBlockSection columns="1" title="Service Provider Details" id="thirdpartylist" collapsible="false">

    <p class="helptext">
    
Section II(E) of the Regulations enables an RFI to use a service provider to fulfill its reporting and due diligence
obligations, however these obligations shall remain the responsibility of the RFI. <br />
Please use this
section to provide details about such third party service provider that was engaged to fulfill the RFI’s due diligence obligations.

   
    </p>
    

    <!----20----> <apex:inputField required="true" onchange="thirdpartychanged(this.value);" label="Was a Service Provider used to fulfil the entity's Reporting and Due Diligence obligations for CRS " value="{!ObjQuestion.Third_Party_Service_Provider__c}"/>
    <!----21----> <apex:inputField required="true"  rendered="{!AND(ObjQuestion.Third_Party_Service_Provider__c!=null,Contains(ObjQuestion.Third_Party_Service_Provider__c,'Yes'))}" value="{!ObjQuestion.Entity_Name__c}"/>
    <!----22----> <apex:inputField required="true" rendered="{!AND(ObjQuestion.Third_Party_Service_Provider__c!=null,Contains(ObjQuestion.Third_Party_Service_Provider__c,'Yes'))}" value="{!ObjQuestion.Registration_No__c}"/>
    <!----23----> <apex:inputField required="true" rendered="{!AND(ObjQuestion.Third_Party_Service_Provider__c!=null,Contains(ObjQuestion.Third_Party_Service_Provider__c,'Yes'))}" value="{!ObjQuestion.Jurisdiction__c}"/>
    
    
    <apex:outputPanel styleClass="helptext"  rendered="{!AND(ObjQuestion.Third_Party_Service_Provider__c!=null,Contains(ObjQuestion.Third_Party_Service_Provider__c,'Yes'))}" >
  The Reporting Financial Institution remains responsible for fulfilling CRS obligations under the DIFC Common Reporting Standards Law and Regulations where the Due Diligence Obligations are outsourced.
      
    </apex:outputPanel>
  <p class="helptext"><b>If more than one (1) service provider was used, please contact roc.helpdesk@difc.ae</b></p>
</apex:pageBlockSection>

<!-----===============================================================================================-----> 

<apex:actionFunction name="Pre_existingAccountss" action="{!Pre_existingAccountss_check}" immediate="true" reRender="diligencelist,duediligenceq,duediligence_a" status="actStatusId">
   <apex:param name="myParamva" value=""/>
    </apex:actionFunction> 

<apex:actionFunction name="diligencechanged" action="{!due_diligence_procedures_yes}" immediate="true" reRender="duediligenceq,duediligence_a" status="actStatusId">
   <apex:param name="myParam" value=""/>
    </apex:actionFunction> 
    
<apex:pageBlockSection columns="1" rendered="{!ObjQuestion.Company_Type__c=='Financial - related'}" title="Due Diligence Procedures" id="diligencelist" collapsible="false">


<p class="helptext">
This section asks for information relating to the due diligence procedures applied by the RFI during the reporting period. <br />

<ul class="helptext">

<li><b>Pre-existing Accounts</b> are defined in Section VIII(C)(9) of the CRS Regulations</li>
<li><b>Cash Value Insurance Contract </b> means an Insurance Contract (other than an indemnity reinsurance contract between two insurance companies) that has a Cash Value.</li>
<li><b>Annuity Contract</b> means a contract under which the issuer agrees to make payments for a period of time determined in whole or in part by reference to the life expectancy of one or more individuals. </li>
<li><b>Pre-existing Entity Account</b> means a Pre-existing Account held by one or more Entities.</li>
<li><b>Pre-existing Individual Account</b> means a Pre-existing Account held by one or more individuals</li>

<p class="helptextp">
<br />
Please refer to <a href='https://www.difc.ae/files/6715/2327/6154/Common_Reporting_Standard_Regulations.pdf' target="_blank"> the Common Reporting Standards Regulations ("CRS Regulations")</a> for the full definitions of each of these terms.

</p>
</ul>

</p>

<!----24----><apex:inputField required="true" onchange="Pre_existingAccountss(this.value);"  label="Were any due diligence procedures applied to <b>Pre-existing Accounts</b> during the reporting period?"
     value="{!ObjQuestion.Pre_existing_Accounts_during_the_reporti__c}" />
     
<!----25----><apex:inputField required="true" onchange="diligencechanged(this.value);" rendered="{!ObjQuestion.Pre_existing_Accounts_during_the_reporti__c='Yes'}"  label="Were the due diligence procedures for <b>New Accounts</b> applied to <u>all</u> Pre-existing Accounts during the reporting period?"
     value="{!ObjQuestion.due_diligence_procedures__c}" />
     
     
    
     
     </apex:pageBlockSection>
     
     
     <apex:actionFunction name="clearly_identified_group_other_checkd" action="{!clearly_identified_group_other_check}" immediate="true" reRender="duediligenceq" status="actStatusId">
   <apex:param name="myParam" value=""/>
    </apex:actionFunction> 
    
    
     
     <apex:pageBlockSection columns="1"  id="duediligenceq"   collapsible="false">
     
     <apex:outputPanel rendered="{!AND(ObjQuestion.Pre_existing_Accounts_during_the_reporti__c='Yes',ObjQuestion.due_diligence_procedures__c='No')}">
     
     <p class="helptext"> <br />
<h2>Pre-existing Accounts</h2>
</p>
     
     </apex:outputPanel>
    
 

 <!----26---->  <apex:inputField required="true" label="<b>Self Certification</b> was obtained for all Pre-existing Accounts :"   onchange="clearly_identified_group_other_checkd(MultiSelectValues(this));" rendered="{!AND(ObjQuestion.Pre_existing_Accounts_during_the_reporti__c='Yes',ObjQuestion.due_diligence_procedures__c='No')}" value="{!ObjQuestion.Please_select_the_relevant_option__c}" />
 <!----27---->  <apex:inputField required="true"  rendered="{!AND(ObjQuestion.Pre_existing_Accounts_during_the_reporti__c='Yes',ObjQuestion.due_diligence_procedures__c='No',Contains(ObjQuestion.Please_select_the_relevant_option__c,'clearly identified group'))}"  label="Specify the clearly identified group(s)" value="{!ObjQuestion.Clearly_identified_group_s__c}"/>
 <!----28---->  <apex:inputField required="true"  rendered="{!AND(ObjQuestion.Pre_existing_Accounts_during_the_reporti__c='Yes',ObjQuestion.due_diligence_procedures__c='No',Contains(ObjQuestion.Please_select_the_relevant_option__c,'Other'))}" label="If 'other' is selected, please provide details here" value="{!ObjQuestion.Detailed_reasons__c}"/>
<!----26.1---->  <apex:inputField required="true" label="<b>Due diligence</b>  was <u> not</u> conducted for Pre-existing Accounts :"  rendered="{!AND(ObjQuestion.Pre_existing_Accounts_during_the_reporti__c='Yes',ObjQuestion.due_diligence_procedures__c='No')}" value="{!ObjQuestion.Due_diligence_not_conducted__c}" /> 
            

            
    </apex:pageBlockSection>
     <!---- 
       <apex:pageBlockSection columns="1"  id="duediligenceqPre_existing" title="111" >
   
  <apex:inputField required="true" label="Due diligence not conducted on  Pre-existing" value="{!ObjQuestion.Please_select_relevant_option_2__c}"/>
    <apex:inputField required="true" value="{!ObjQuestion.Please_provide_detailed_reasons__c}"/> 
    
    </apex:pageBlockSection>
    --->
<!-----===============================================================================================-----> 
    
  <apex:pageBlockSection columns="1"  id="duediligence_a"  collapsible="false" >
  <!----====- title="6. Due diligence procedures on Pre-existing Accounts" ---->
  <apex:outputPanel rendered="{!OR(ObjQuestion.Pre_existing_Accounts_during_the_reporti__c='No',ObjQuestion.due_diligence_procedures__c='No')}">
<p class="helptext"><br />
<h2>Lower Value Accounts</h2>
</p>
<ul class="helptext">
<li><b>Lower Value Account</b> means a Pre-existing Individual Account with an aggregate balance or value as of 31 December 2016 that does not exceed USD1,000,000. </li>
<li><b>High Value Account</b> means a Pre-existing Individual Account with an aggregate balance or value that exceeds USD1,000,000 as of 31 December 2016, or 31 December of any subsequent year.</li>
<li><b>Residence Address Test</b> means the residence address test described in of Section III(B)(1) of the CRS Regulations.</li>
<li><b>Documentary Evidence</b> has the meaning given to the term in Section III(E)(6) of the CRS Regulations.</li>

<p class="helptextp">
<br />
Please refer to <a href='https://www.difc.ae/files/6715/2327/6154/Common_Reporting_Standard_Regulations.pdf' target="_blank"> the Common Reporting Standards Regulations ("CRS Regulations")</a> for the full definitions of each of these terms.

</p>

</ul>
</apex:outputPanel>
   
     <apex:actionFunction name="due_diligence_procedures_checkd" action="{!due_diligence_procedures_check}" immediate="true" reRender="duediligence_a" status="actStatusId">
   <apex:param name="myParam" value=""/>
    </apex:actionFunction> 
    
      <apex:actionFunction name="High_Value_Accounts_check" action="{!High_Value_Accounts_check}" immediate="true" reRender="duediligence_a" status="actStatusId">
   <apex:param name="myParam" value=""/>
    </apex:actionFunction> 
    
     <apex:actionFunction name="Lower_Value_Accounts_check" action="{!Lower_Value_Accounts_check}" immediate="true" reRender="duediligence_a" status="actStatusId">
   <apex:param name="myParam" value=""/>
    </apex:actionFunction> 
    
    
 <!----29----> <apex:inputField required="true"  onchange="High_Value_Accounts_check(this.value);" rendered="{!OR(ObjQuestion.Pre_existing_Accounts_during_the_reporti__c='No',ObjQuestion.due_diligence_procedures__c='No')}" label="Were due diligence procedures for <b> High Value Accounts</b> applied to Lower Value Accounts ?" value="{!ObjQuestion.High_value_applied_to_Lower_Value__c}"/>
 
 <!----30----><apex:inputField required="true" onchange="Lower_Value_Accounts_check(this.value);"  rendered="{!OR(ObjQuestion.High_value_applied_to_Lower_Value__c=='No')}" label="Were any due diligence procedures applied for <b>Lower Value Accounts</b>?" value="{!ObjQuestion.Resident_Address_test_used__c}"/>
 
 <!----31----> <apex:inputField required="true" rendered="{!OR(ObjQuestion.Resident_Address_test_used__c=='Yes')}" onchange="due_diligence_procedures_checkd(MultiSelectValues(this));" value="{!ObjQuestion.due_diligence_the_relevant_option__c}"/>
 
 <!----32----> <apex:inputField required="true" rendered="{!AND(ObjQuestion.due_diligence_the_relevant_option__c!=null,Contains(ObjQuestion.due_diligence_the_relevant_option__c,'Other'))}" label="If 'other' is selected, please provide details here" value="{!ObjQuestion.due_diligence_detailed_reasons__c}"/>


    
</apex:pageBlockSection>

 
     <apex:actionFunction name="Undocumented_Accounts_check" action="{!Undocumented_Accounts_check}" immediate="true" reRender="UndocumentedDL" status="actStatusId">
   <apex:param name="myParam" value=""/>
    </apex:actionFunction> 
    
    
       <apex:actionFunction name="Excluded_Accounts_Accounts_check" action="{!Excluded_Accounts_Accounts_check}" immediate="true" reRender="UndocumentedDL" status="actStatusId">
   <apex:param name="myParam" value=""/>
    </apex:actionFunction> 
    
    
<apex:pageBlockSection columns="1" title="Undocumented, Reportable and Excluded Accounts for the reporting period ending 31 December 2019"  rendered="{!ObjQuestion.Company_Type__c=='Financial - related'}" collapsible="false" id="UndocumentedDL">    

<p class="helptext">
<ul class="helptext"> 

<li><b>Undocumented Account</b> exists where the only evidence for the residence status for a Pre-existing Individual Account is a "hold mail" or "in-care-of address" and the Reporting Financial Institution has not been able to obtain a self-certification from the individual. </li>
<li><b>Reportable Account</b> has the meaning given to the term in Section III(E)(6) of the CRS Regulations.</li>
<li><b>Excluded Account</b> has the meaning given to the term in Section VIII(C)(17) of the CRS Regulations.</li>
<p class="helptextp">
<br />
Please refer to <a href='https://www.difc.ae/files/6715/2327/6154/Common_Reporting_Standard_Regulations.pdf' target="_blank"> the Common Reporting Standards Regulations ("CRS Regulations")</a> for the full definitions of each of these terms.

</p>

</ul>
</p>
    
<!----33---->   <apex:inputField required="true" onchange="Undocumented_Accounts_check(this.value);" label="Were <b>Undocumented Accounts</b> reported ?" value="{!ObjQuestion.Undocumented_Accounts_being_reported__c}"/>
<!----34---->    <apex:inputField required="true" rendered="{!ObjQuestion.Undocumented_Accounts_being_reported__c='Yes'}" label="Specify the number of <b>Undocumented Accounts</b>" value="{!ObjQuestion.The_number_of_Undocumented_Reports__c}"/>
<!----35---->    <apex:inputField required="true" label="Specify the number of <b>accounts closed</b> during the reporting period" value="{!ObjQuestion.Closed_during_the_reporting_period__c}"/>
<!----36---->    <apex:inputField required="true" label="Specify the total number of <b>Reportable Accounts</b> " value="{!ObjQuestion.Reportable_Accounts_for_the_reporting_pe__c}"/>

<!----37---->    <apex:inputField required="true" onchange="Excluded_Accounts_Accounts_check(this.value);"  label="Specify the total number of <b>Excluded Accounts</b> (non-reportable accounts)" value="{!ObjQuestion.Total_number_of_Excluded_Accounts__c}"/>
<!----38---->    <apex:inputField required="true"  rendered="{!ObjQuestion.Total_number_of_Excluded_Accounts__c='unknown'}" label="Specify the reason(s) why the number is unknown" value="{!ObjQuestion.Number_is_unknown__c}"/>

</apex:pageBlockSection>

<apex:pageBlockSection columns="1" title="CRS Polices and Procedures" collapsible="false" id="UndocumentedDL11">    


<!----39---->    <apex:inputField required="true" label="Have CRS policies and procedures been established and maintained?" value="{!ObjQuestion.Maintained_for_the_entity_s_obligations__c}"/>
<!----40---->    <apex:inputField required="true" label="Were the CRS policies and procedures  implemented and complied with?" value="{!ObjQuestion.Complied_with_for_the_reporting_period__c}"/>
         
   
</apex:pageBlockSection>

</apex:outputPanel>



<apex:outputPanel rendered="{!Contains(ObjQuestion.Reporting_Financial_Institution_type__c,'Not a Reporting Financial Institution')}">

 <apex:pageBlockSection columns="1"  id="NotReporting" collapsible="false">
 
 <apex:inputField required="true" label="Please specify why the entity is not a Reporting Financial Institution" value="{!ObjQuestion.Entity_is_not_a_Reporting_Financial_Inst__c}" />
 
 
 </apex:pageBlockSection>


</apex:outputPanel>

</apex:outputPanel>
<!-----===============================================================================================-----> 

<!----41-42----> 
<apex:pageBlockSection columns="1" title="Declaration " collapsible="false">
<apex:pageBlockSectionItem >

<apex:inputField value="{!ObjQuestion.Shared_with_The_Dubai_Financial_Services__c}"/> 
<b>Declaration: By submitting this request, the entity confirms that the information provided in this notification is accurate and acknowledges that</b><br /><br />
<ul class="helptext">
<li>The information contained in this notification may be shared with the Dubai Financial Services Authority (DFSA), the UAE Competent Authority and other relevant government or regulatory authorities in connection with the Common Reporting Standard Law and Regulations;</li>
<li>It is an offence to provide information that is false, misleading or deceptive under the Common Reporting Standard Law, DIFC Law No. 2 of 2018;</li>
<li>It is also an offence to provide the Registrar with information that is false, misleading or deceptive or to conceal information where the concealment of such information is likely to mislead or deceive, under the Operating Law, DIFC Law No. 7 of 2018; and</li>
<li>The Designated Person nominated on this notification is authorised by the entity to file this notification, submit any required document(s) on the DIFC Client Portal, respond to any queries and provide further information on its behalf to the Registrar of Companies.</li>

</ul>

</apex:pageBlockSectionItem>



    
<!----
<apex:pageBlockSectionItem >
<apex:inputField value="{!ObjQuestion.Offence_to_provide_information_that_is_f__c}"/>

</apex:pageBlockSectionItem>
<apex:pageBlockSectionItem >
<apex:inputField value="{!ObjQuestion.Operating_Law_DIFC_Law_No_7_of_2018__c}"/>

</apex:pageBlockSectionItem>

<apex:pageBlockSectionItem >
<apex:inputField value="{!ObjQuestion.Designated_Person_nominated__c}"/>

</apex:pageBlockSectionItem>
-->

</apex:pageBlockSection>

</apex:pageBlock>
</apex:form>
</apex:page>