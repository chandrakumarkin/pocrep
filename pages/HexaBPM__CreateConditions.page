<apex:page id="condition_page" controller="HexaBPM.CreateConditionsController" sidebar="false" tabstyle="HexaBPM__SR_Steps__c">
  <apex:sectionHeader title="Create Conditions for Step No : {!objSRStep.HexaBPM__Step_No__c}" subtitle="{!objSRStep.HexaBPM__Summary__c}"/>
  <style>
     .delth{
        text-align:center;
        min-width:35px;
     }
     .return_btn{
         font-size: 0.9em;
         color:#333333;
         display:block !important;
         width:125px !important;
         border: none !important;
         height:24px !important;
         line-height:14px !important;
         float: left !important;
    }
    /* This is for the full screen DIV */
    .popupBackground {
        /* Background color */
        background-color:black;
        opacity: 0.20;
        filter: alpha(opacity = 20);
     
        /* Dimensions */
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 998;
        position: absolute;
    }
    /* This is for the message DIV */
    .PopupPanel{
        left: 35%;
        width: 100px;
        border-radius: 15px
        top: 50%;
        height: 100px;
        margin-top: 50px;
        z-index: 999;
        position: fixed;
    }
  </style>
  <apex:actionStatus id="pleasewait" stopText="">
      <apex:facet name="start">
          <div>
          <!-- to disable whole page when user clicks on next button  -->
              <div class="popupBackground"></div>
              <div class="PopupPanel">
                  <img src="{!$Resource.LoadingImage}"/>
              </div>
          </div>
      </apex:facet>
  </apex:actionStatus>
  <apex:form id="condition_form">
    <apex:inputhidden id="hdnRowId" value="{!iSelectedRow}"/>
    <apex:inputhidden id="hdnSelIndex" value="{!selectedRowIndex}"/>
    <apex:actionfunction name="changeFields" action="{!getSelectedObjectFlds}" rerender="pb,errormsg" status="pleasewait"/>
    <apex:actionfunction name="deleteCondtn" action="{!DeleteCondition}" rerender="pb,errormsg" status="pleasewait"/>
    <apex:actionfunction name="ShowFieldValue" action="{!ChangesObjectField}" rerender="pb,errormsg" status="pleasewait"/>
    <apex:actionfunction name="saveConditionsFun" action="{!saveConditions}" rerender="pb,errormsg" status="pleasewait"/>
    <apex:pagemessages id="errormsg"/>
    <apex:pageblock id="BRDetail" title="Business Rule Detail">
        <apex:pageblockbuttons location="top">
            <apex:commandbutton value="Save" Action="{!SaveBR}" rendered="{!isSaveMode}" rerender="errormsg,BRDetail,conditionspnl" status="pleasewait" style="float:left;"/>
            <apex:commandbutton value="Cancel" Action="{!CancelBR}" rendered="{!isSaveMode}" rerender="errormsg,BRDetail,conditionspnl" status="pleasewait" style="float:left;"/>
            <apex:commandbutton value="Edit" Action="{!EditBR}" rendered="{!!isSaveMode}" rerender="errormsg,BRDetail,conditionspnl" status="pleasewait" style="float:left;"/>
            <apex:commandButton value="Return to SR Step" status="pleasewait" action="/{!$CurrentPage.parameters.Id}" title="Returns to SR-Steps Detail Page"/>
        </apex:pageblockbuttons>
        <apex:pageblocksection columns="2">
            <apex:outputfield value="{!BR.Name}"/>
            <apex:inputfield value="{!BR.HexaBPM__Description__c}" rendered="{!isSaveMode}"/>
            <apex:outputfield value="{!BR.HexaBPM__Description__c}" rendered="{!!isSaveMode}"/>
            <apex:outputfield value="{!BR.HexaBPM__Action_Type__c}"/>
            <apex:outputfield value="{!BR.HexaBPM__SR_Steps__c}"/>
        </apex:pageblocksection>
    </apex:pageblock>
    <apex:outputpanel id="conditionspnl" layout="block">
        <apex:pageBlock title="Manage Conditions" id="pb" rendered="{!showConditions}">
          <apex:pageBlockButtons location="top">
            <apex:commandButton action="{!addRow}" value="Add Condition" rerender="pb,errormsg" status="pleasewait" style="float:left;"/>
            <apex:commandButton onclick="saveCon();return false;" value="Save" style="float:left;"/>
            <apex:commandButton action="{!cancelCondition}" value="Cancel" status="pleasewait" style="float:left;"/>
          </apex:pageBlockButtons>  
          <apex:pageblocktable value="{!ConditionWrapperLst}" id="conditions" var="Condition" width="100%">
                 <apex:column headervalue="Seq#">
                    <apex:outputlabel value="{!Condition.rowIndex+1}"/>
                 </apex:column>
                 <apex:column >
                    <apex:facet name="header">
                         <div style="float: left;">Based on</div>
                     </apex:facet>
                     <apex:selectList id="ObjectName" multiselect="false" size="1" value="{!Condition.objCondition.HexaBPM__Object_Name__c}" onchange="ChangeObject('{!JSENCODE(TEXT(Condition.rowIndex))}');">
                           <apex:selectOptions value="{!Condition.lstObjects}" />
                     </apex:selectList>
                 </apex:column>
                 <apex:column headerValue="Class Name">
                    <!-- This will be displayed when user selects the Custom Code option  -->
                     <apex:selectList id="className" multiselect="false" size="1" value="{!Condition.objCondition.HexaBPM__Class_Name__c}" onchange="ChangeObject('{!JSENCODE(TEXT(Condition.rowIndex))}');" rendered="{!IF(Condition.objCondition.HexaBPM__Object_Name__c=='Custom Code',true,false)}">
                           <apex:selectOptions value="{!Condition.lstClasses}" />
                     </apex:selectList>
                 </apex:column>
                 <apex:column >
                     <apex:facet name="header">
                         <div style="float: left;">Field Name / Step No</div>
                     </apex:facet>
                     <apex:selectList id="ObjectFieldName" multiselect="false" size="1" value="{!Condition.objCondition.HexaBPM__Field_Name__c}" disabled="{!if(Condition.lstFields.size==1,true,false)}" onchange="FieldChange('{!JSENCODE(TEXT(Condition.conditionNo))}');" rendered="{!IF(Condition.objCondition.HexaBPM__Object_Name__c!='Step Status' && Condition.objCondition.HexaBPM__Object_Name__c!='Step Closure',true,false)}">
                        <apex:selectOptions value="{!Condition.lstFields}" />
                     </apex:selectList>
                     <apex:selectList id="SRSteps" multiselect="false" size="1" value="{!Condition.objCondition.HexaBPM__Field_Name__c}" rendered="{!IF(Condition.objCondition.HexaBPM__Object_Name__c=='Step Status' || Condition.objCondition.HexaBPM__Object_Name__c=='Step Closure',true,false)}">
                        <apex:selectOptions value="{!lstExistingSRSteps}" />
                     </apex:selectList>
                 </apex:column>
                 <apex:column headerValue="Operator"><!-- headerValue="Operator" -->
                    <!-- The below Picklist will display if user selects the values apart from Step Status contains = & != & Contains etc -->
                    <apex:inputField value="{!Condition.objCondition.HexaBPM__Operator__c}" rendered="{!IF(Condition.objCondition.HexaBPM__Object_Name__c!='Step Status' && Condition.objCondition.HexaBPM__Object_Name__c!='Step Closure',TRUE,FALSE)}"/>
                    <!-- The below Picklist will display if user selects the values Step Status contains = & != operators -->
                    <apex:selectlist id="statusoperators" multiselect="false" size="1" value="{!Condition.objCondition.HexaBPM__Operator__c}" rendered="{!IF(Condition.objCondition.HexaBPM__Object_Name__c=='Step Status',TRUE,FALSE)}">
                        <apex:selectoptions value="{!statusOperators}"/>
                    </apex:selectlist>
                 </apex:column>
                 <apex:column headerValue="Value">
                    <apex:inputField id="valinputfield" value="{!Condition.objCondition.HexaBPM__Value__c}" rendered="{!IF(Condition.FieldType!='PICKLIST' && Condition.objCondition.HexaBPM__Object_Name__c!='Step Status' && Condition.objCondition.HexaBPM__Object_Name__c!='Step Closure',IF(Condition.objCondition.HexaBPM__Object_Name__c=='Step__c' && Condition.objCondition.HexaBPM__Field_Name__c=='Step_Status__c',false,true),false)}" onkeypress="return inputOnlyNumbers(event,'{!JSENCODE(Condition.FieldType)}')" onfocus="isdateFld(this,'{!JSENCODE(Condition.FieldType)}');"/>
                    <apex:selectlist id="pickfld" multiselect="false" size="1" value="{!Condition.objCondition.HexaBPM__Value__c}" rendered="{!IF(Condition.FieldType=='PICKLIST' && Condition.objCondition.HexaBPM__Object_Name__c!='Step Status',TRUE,FALSE)}">
                        <apex:selectoptions value="{!Condition.lstPickList}"/>
                    </apex:selectlist>
                    <!-- This will displays If user Chooses the Step Status option in Based on column, then the List of Step Statuses available -->
                    <apex:selectlist id="SRStepStatus" multiselect="false" size="1" value="{!Condition.objCondition.HexaBPM__Value__c}" rendered="{!IF(Condition.objCondition.HexaBPM__Object_Name__c=='Step Status',TRUE,FALSE)}">
                        <apex:selectoptions value="{!lstAvailableStatus}"/>
                    </apex:selectlist>
                    <apex:selectlist id="StatusValues" multiselect="false" size="1" value="{!Condition.objCondition.HexaBPM__Value__c}" rendered="{!IF(AND(Condition.objCondition.HexaBPM__Object_Name__c=='Step__c',Condition.objCondition.HexaBPM__Field_Name__c=='Step_Status__c'),TRUE,FALSE)}">
                        <apex:selectoptions value="{!statusValues}"/>
                    </apex:selectlist>
<!--                    <apex:outputlabel value=" End " rendered="{!IF(Condition.objCondition.Object_Name__c=='Step Closure',true,false)}"/> -->
                 </apex:column>
                 <apex:column headerValue="Delete" style="text-align:center;" headerclass="delth">
                       <apex:image value="{!$Resource.HexaBPM__remove_image}" style="cursor:pointer;" onclick="DeleteRow('{!JSENCODE(TEXT(Condition.conditionNo))}');" title="Remove the Condition"/>
                 </apex:column>
             </apex:pageblocktable> 
             <apex:panelGrid width="80%" columns="4"  style="padding:3px;display:none;">
                 <apex:outputLabel value="BR Condition: " style="margin-left:10px"/>
                 <apex:outputText value="{!BRCondition}"/>
             </apex:panelGrid>
             <br/><br/>
             <apex:outputPanel layout="block" id="formulapnl">
                <apex:outputLabel value="Filter Logic " style="font-weight:bold;"/> 
                <apex:inputText id="formulatxt" value="{!FilterCondition}" onblur="checkFormula(this.value);" style="min-width: 400px;"/>
                <a id="clearlogic" onclick="ClearFilterLogic();" style="cursor:pointer;text-decoration:underline;">Clear Filter Logic</a>
             </apex:outputPanel>
             <apex:outputPanel layout="block" id="errorpnl">
                <apex:outputLabel id="errorlbl" value="Error: Your filter is imprecise or incorrect. Please see the help for tips on filter logic." style="color:#d74c3b;display:none;"/>
                <a id="hlplnk" style="display:none;" href="javascript:openPopupFocusEscapePounds(%27https://help.salesforce.com/apex/htdoor?loc=help&target=working_with_advanced_filter_conditions_in_reports_and_list_views.htm&section=Reports&language=en_US&release=186.12.14&instance=CS17%27, %27Help%27, 1024, 768, %27width=1024,height=768,resizable=yes,toolbar=yes,status=yes,scrollbars=yes,menubar=yes,directories=no,location=yes,dependant=no%27, false, false);" class="tipsLink" title="Tips (New Window)"><span class="helpLink brandTertiaryFgr">Tips</span><img src="/s.gif" alt="Help" class="helpIcon" title="Help"/></a>
             </apex:outputPanel>
        </apex:pageBlock>
    </apex:outputpanel>
  </apex:form>
  <script>
        function ChangeObject(RowNo){
            if(document.getElementById('condition_page:condition_form:hdnRowId')!=null)
                document.getElementById('condition_page:condition_form:hdnRowId').value = RowNo;
            changeFields();
        }
        function DeleteRow(RowNo){
            if(document.getElementById('condition_page:condition_form:hdnSelIndex')!=null)
                document.getElementById('condition_page:condition_form:hdnSelIndex').value = RowNo;
            deleteCondtn();
        }
        function FieldChange(RowNo){
            if(document.getElementById('condition_page:condition_form:hdnSelIndex')!=null)
                document.getElementById('condition_page:condition_form:hdnSelIndex').value = RowNo;
            ShowFieldValue();
        }
        function ClearFilterLogic(){
            if(document.getElementById('condition_page:condition_form:pb:formulatxt')!=null)
                document.getElementById('condition_page:condition_form:pb:formulatxt').value='';
            document.getElementById('condition_page:condition_form:pb:errorlbl').style.display='none';
            document.getElementById('hlplnk').style.display='none';
        }
        function saveCon(){
            var Formulaval = document.getElementById('condition_page:condition_form:pb:formulatxt').value;
            Formulaval = Formulaval.trim();
            var flag;
            if(Formulaval!=null && Formulaval!=''){
                flag = isValidLogicalStatement(Formulaval);
            }
            if(flag!=false){
                document.getElementById('condition_page:condition_form:pb:errorlbl').style.display='none';
                document.getElementById('hlplnk').style.display='none';
                //console.log();
                saveConditionsFun();
            }
        }
        function checkFormula(txt){
            txt = txt.trim();
            if(txt != null && txt != ''){
                document.getElementById('condition_page:condition_form:pb:formulatxt').value = txt;
                var flag = isValidLogicalStatement(txt);
                //console.log(flag);
            }else{
                document.getElementById('condition_page:condition_form:pb:errorlbl').style.display='none';
                document.getElementById('hlplnk').style.display='none';
            }
        }
        function isValidLogicalStatement(text) {
            //http://www.rqna.net/qna/iiuyuq-need-to-create-regular-expression-in-javascript-to-check-the-valid-conditional-string.html
            //http://stackoverflow.com/questions/13208282/need-to-create-regular-expression-in-javascript-to-check-the-valid-conditional-s
            var RowCount;
            var indexExceed; 
            var matchedcount=0; 
            var allConIncluded = true;
            if(document.getElementById('condition_page:condition_form:pb:conditions')!=null)
                RowCount = document.getElementById('condition_page:condition_form:pb:conditions').rows.length;
            var rowsmap = {};
            if(RowCount!=null && RowCount>0 && text!=null && text.length>0){
                RowCount = RowCount-1;
                var numbr = /\d+/;
                for(var i=0;i<text.length;i++){
                    //console.log('char==>'+text[i]);
                    if(text[i].match(numbr)){
                        var rnum = eval(text[i]);
                        rowsmap[rnum] = {
                                     Seq : rnum
                                     };
                        if(eval(text[i])>RowCount){
                            indexExceed = eval(text[i]);
                        }
                        else if(eval(text[i])==0){
                            indexExceed = 0;
                        }
                        
                    }
                }
                var flConCount=0;
                //console.log('RowCount===>'+RowCount);
                //console.log('rowsmap===>'+rowsmap);
                for(var r=1;r<=RowCount;r++){
                    if(rowsmap!=null && rowsmap[r]!=null){
                        //console.log('json===>'+rowsmap[r].Seq);
                        flConCount = flConCount+1;
                    }
                }
                if(flConCount==RowCount){
                    allConIncluded = true;
                }else{
                    allConIncluded = false;
                }
                
                    
            }
            
            //onsole.log('RowCount===>'+RowCount);
            //console.log('rowsmap===>'+rowsmap);
            //console.log('indexExceed at ===>'+indexExceed);
            //console.log('matchedcount===>'+indexExceed);
            
            var re_paren = /\(\s*[+-]?\d+(?:(?:\s+AND\s+[+-]?\d+)+|(?:\s+OR\s+[+-]?\d+)+)\s*\)/ig;
            var re_valid =  /^\s*[+-]?\d+(?:(?:\s+AND\s+[+-]?\d+)+|(?:\s+OR\s+[+-]?\d+)+)\s*$/ig;
            // Iterate from the inside out.
            while (text.search(re_paren) !== -1) {
                // Replace innermost parenthesized units with integer.
                text = text.replace(re_paren, "0");
            }
            
            /*
            if (text.search(re_valid) === 0 && indexExceed==null && allConIncluded==true){ 
                document.getElementById('condition_page:condition_form:pb:errorlbl').style.display='none';
                document.getElementById('hlplnk').style.display='none';
                return true;
            }else{
                if(indexExceed!=null){
                    document.getElementById('condition_page:condition_form:pb:errorlbl').innerHTML = 'Error: The filter logic references an undefined filter: '+indexExceed+'.';
                }else if(allConIncluded==false){
                    document.getElementById('condition_page:condition_form:pb:errorlbl').innerHTML = 'Error: Some filter conditions are defined but not referenced in your filter logic.';
                }else{
                    document.getElementById('condition_page:condition_form:pb:errorlbl').innerHTML = 'Error: Your filter is imprecise or incorrect. Please see the help for tips on filter logic.';
                }
                document.getElementById('condition_page:condition_form:pb:errorlbl').style.display='';
                document.getElementById('hlplnk').style.display='';
                return false;
            }
            */
            return true;
            
        }
        
        function showExportMenu(){
            if(document.getElementById('menuButtonMenu')!=null){
                if(document.getElementById('menuButtonMenu').style.display=='block')
                    document.getElementById('menuButtonMenu').style.display='none';
                else if(document.getElementById('menuButtonMenu').style.display=='none'){
                    document.getElementById('menuButtonMenu').style.display='block';
                    ExportClick = 'true';
                }       
            }
        }
        function isdateFld(myField,FldType){
            //console.log('FldType===>'+FldType);
            if(FldType=='DATE'){
                DatePicker.pickDate(false, myField , false);
            }
        }
        function inputOnlyNumbers(evt,FldType) {
            if(FldType=='DOUBLE' || FldType=='CURRENCY'){
                var e = window.event || evt; // for trans-browser compatibility  
                //If user clicks on the key in keypad this will returns the appropriate ASCII value
                var charCode = e.which || e.keyCode;  
                if ((charCode > 47 && charCode < 58) || charCode == 8){  
                    return true;  
                }  
                return false;  
            }else{
                return true;
            }
        }
  </script>
</apex:page>