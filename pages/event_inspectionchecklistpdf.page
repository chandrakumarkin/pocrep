<apex:page docType="html-5.0" applyHtmlTag="false" applyBodyTag="false" standardStylesheets="true" controller="FO_Cls_FirstInspection" showHeader="false" renderAs="PDF" sidebar="false" id="inspectionChecklist">
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"></meta>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"></meta>
        <title>DIFC - Dubai International Financial Centre</title>

        <c:fo_difc_checklistheader />
        
        <style>
            
            .difc-footer-note--custom {
                margin-top: 50px !important;
            }
            
            .difc-footer-signature--custom {
                margin-bottom: 20px !important;
            }
            
            .difc-footer-row--custom {
                position: fixed !important;
                top: 85%;
            }
            
            .pageNumber:before {
                content: counter(page);
            }
            
            .lastPage:before {
                content: counter(pages);
            }
            
            table {
                table-layout: fixed;
            }
            
        </style>
        
        
    </head>

    <body>
        <div class="login">
            
            <div class="row full-width">  
                
                <c:fo_difc_checklistlogo headerTitle="Event Premise Inspection (Setup and Final Handover) Checklist" headerSubtitle="Property Management" />
                
                <!------------------User Access Form Code---------------------------->
               
                <div class="large-12 columns">  
                    <apex:outputPanel id="inspectionBlock" layout="none">
                        <apex:outputPanel rendered="{!checkListType != '' && serviceRequestId !='' && stepId!=''}">
                            <table id='main-table' border="0" style="width:100%;"> <!--  margin-top:-21px; -->
                                <tr>
                                    <td class='left-label' style="font-weight: bold; background-color:#141B4D;color: white;font-size:14px;padding: 10px;">Event Premise Status:</td>
                                    <td class='right-content--half' style="padding: 10px;text-align:center; font-size: 12px">
                                        <img src="{!if(setup,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        SET-UP
                                    </td>
                                    <td class='right-content--half' style=" text-align:center; font-size: 12px;padding: 10px;">
                                        <img src="{!if(handOver,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        FINAL PREMISE HANDOVER
                                    </td>
                                </tr>
                                <tr>
                                    <td class='left-label' style="width:20%;font-weight: bold; background-color:#141B4D;color: white;font-size:14px;padding: 10px;">Event Name: </td>
                                    <td class='right-content--expanded' colspan="2"><apex:outputText style="font-size:14px; padding: 10px;" value="{!HTMLEncode(srObj.Event_Name__c)}"/></td>
                                </tr>
                                <tr>
                                    <td class='left-label' style="width:20%;font-weight: bold; background-color:#141B4D;color: white;font-size:14px;padding: 10px;">Event Permit No: </td>
                                    <td class='right-content--expanded' colspan="2"><apex:outputText style="font-size:14px; padding: 10px;" value="{!HTMLEncode(srObj.Current_Registered_Name__c)}"/></td>
                                </tr>
                                <tr>
                                    <td class='left-label' style="width:20%;font-weight: bold; background-color:#141B4D;color: white;font-size:14px;padding: 10px;"><div style="width: 150px">Client/Event Organizer: </div></td>
                                    <td class='right-content--expanded' colspan="2"><apex:outputText style="font-size:14px; padding: 10px;" value="{!HTMLEncode(srObj.Customer__r.name)}"/></td>
                                </tr>
                                <tr>
                                    <apex:outputPanel layout="none" rendered="{!NOT(IsBlank(contractor))}" >
                                    <td class='left-label' style="width:20%;font-weight: bold;background-color:#141B4D;color: white;padding: 10px;font-size:14px;">Event Contractor (as applicable): </td>
                                    <td class='right-content--expanded' style="padding: 10px;" colspan="2"><apex:outputText style="font-size:14px;" value="{!HTMLEncode(contractor)}"/></td>
                                    </apex:outputPanel>
                                </tr>
                                <tr>
                                    <td class='left-label' style="font-weight: bold;width:20%;background-color:#141B4D;color: white; padding: 10px;font-size:14px;">Location: </td>
                                    <td class='right-content--expanded' colspan="2" style="padding: 10px;"><apex:outputText style="font-size:14px;" value="{!HTMLEncode(srObj.Location__c)}"/></td>
                                </tr>
                            </table>
                            <table border="0" style="width:100%; border: 0">
                                <tr>
                                    <td style="padding: 20px 0; border: none; font-size: 14px">
                                        This is to record that the above location was inspected by FOSP on&nbsp;   
                                        <apex:outputPanel layout="none" id="inspectionDateSection">                   
                                            {!HTMLEncode(inspectionDateFormatted)}&nbsp;and the following points have been noted:
                                        </apex:outputPanel>    
                                    </td>
                                </tr>    
                            </table>
                            <table border="0" style="width:100%; ">
                                <tr>
                                    <!-- <td class='colored-label' style="font-weight: bold;text-align:center; width:3%;background-color:#141B4D;color: white;">SI.</td> -->
                                    <td class='colored-label' style="font-weight: bold;text-align:center; background-color:#141B4D;color: white;width:20%;font-size:14px">Item Description</td>
                                    <td class='colored-label' style="font-weight: bold;text-align:center; background-color:#141B4D;color: white;width:3%;font-size:14px">Acceptable</td>
                                    <td class='colored-label' style="font-weight: bold;text-align:center; background-color:#141B4D;color: white;width:3%;font-size:14px">Unacceptable </td>
                                    <td class='colored-label' style="font-weight: bold;text-align:center; background-color:#141B4D;color: white;width:3%;font-size:14px">Not Applicable</td>
                                    <td class='colored-label' style="font-weight: bold;text-align:center; background-color:#141B4D;color: white;width:20%;font-size:14px">Comments </td>
                                </tr>
                                <apex:variable value="{!1}" var="count"/>
                                <apex:repeat id="questionList1" value="{!questionsList}" var="qObj" >
                                    <apex:outputPanel rendered="{!count < 10}" layout="none" >
                                    <tr>
                                        <!-- <td style="font-weight: bold;text-align:center; width:3%">{!FLOOR(count)}</td> -->
                                        
                                        <td style="padding-left: {!IF(CONTAINS(qObj.lookUpObj.Description__c,'a) Floors') || CONTAINS(qObj.lookUpObj.Description__c,'b) Walls') || CONTAINS(qObj.lookUpObj.Description__c,'c) Ceiling')  || CONTAINS(qObj.lookUpObj.Description__c,'d) Waste disposal'),'50px','')}"><apex:outputText escape="false" value="{!HTMLENCODE(qObj.lookUpObj.Description__c)}"/></td>
                                        <td style="font-weight: bold;text-align:center">
                                            <apex:image URL="{!if(qObj.yes,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                        </td>
                                        <td style="font-weight: bold;text-align:center">
                                            <apex:image URL="{!if(qObj.no,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                        </td>
                                        <td style="font-weight: bold;text-align:center">
                                            <apex:image URL="{!if(qObj.na,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                        </td>
                                         
                                        <td style="font-size: 10px; font-weight: bold;text-align:center; ">
                                            {!HTMLEncode(qObj.comments)}
                                        </td>                                                                                                                        
                                    </tr>
                                    </apex:outputPanel>
                                    <apex:variable var="count" value="{!count+ 1}"/>
                                </apex:repeat>
                            </table>
                            
                                
                            <table style="position: absolute; top: {!IF(handOver, IF(NOT(IsBlank(contractor)),'111%','121%'),IF(NOT(IsBlank(contractor)),'111%','119%') )}; margin: 0 15px">
                                <tr>
                                    <!-- <td class='colored-label' style="font-weight: bold;text-align:center; width:3%;background-color:#141B4D;color: white;">SI.</td> -->
                                      
                                    <td class='colored-label' style="border: none;font-weight: bold;text-align:center; color: white;width:20%;font-size:14px;">Item Description</td>
                                    <td class='colored-label' style="border: none; font-weight: bold;text-align:center; color: white;width:3%;font-size:14px;">Acceptable</td>
                                    <td class='colored-label' style="border: none;font-weight: bold;text-align:center; color: white;width:3%;font-size:14px;">Unacceptable </td>
                                    <td class='colored-label' style="border: none;font-weight: bold;text-align:center; color: white;width:3%;font-size:14px;">Not Applicable</td>
                                    <td class='colored-label' style="border: none;font-weight: bold;text-align:center; color: white;width:20%;font-size:14px;">Comments </td>
                                </tr>
                                
                                <apex:variable value="{!1}" var="count"/>
                                <apex:repeat id="questionList2" value="{!questionsList}" var="qObj" >
                                    <apex:outputPanel rendered="{!count >= 10}" layout="none" >
                                    <tr>
                                        <!-- <td style="font-weight: bold;text-align:center; width:3%">{!FLOOR(count)}</td> -->
                                        
                                        <td style="width: 20%; padding-left: {!IF(CONTAINS(qObj.lookUpObj.Description__c,'a) Floors') || CONTAINS(qObj.lookUpObj.Description__c,'b) Walls') || CONTAINS(qObj.lookUpObj.Description__c,'c) Ceiling')  || CONTAINS(qObj.lookUpObj.Description__c,'d) Waste disposal'),'50px','')}"><apex:outputText escape="false" value="{!HTMLENCODE(qObj.lookUpObj.Description__c)}"/></td>
                                         
                                        <td style="font-weight: bold;text-align:center; width:11%">
                                            <apex:image URL="{!if(qObj.yes,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                        </td>
                                        
                                        <td style="font-weight: bold;text-align:center; width:11%">
                                            <apex:image URL="{!if(qObj.no,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                        </td>
                                        
                                        <td style="font-weight: bold;text-align:center; width:11%">
                                            <apex:image URL="{!if(qObj.na,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                        </td>
                                          
                                        <td style="font-weight: bold;text-align:left; width:20%;"> <!-- style value "white-space: nowrap;" - removed on 03-OCT-2017 - M.Prem - #4121 -->
                                            <apex:outputText style="font-size: 12px" value="{!qObj.comments}"/>
                                        </td>
                                        
                                    </tr>
                                    </apex:outputPanel>
                                    <apex:variable var="count" value="{!count+ 1}"/>
                                </apex:repeat>                               
                                 
                            </table>
                            
                            
                            
                            <table style="width:96%; position: absolute; top: {!IF(NOT(IsBlank(contractor)),'168%','170%')}; margin-left: 15px" border="0">
                                <tr>
                                    <td class='colored-label' colspan="2" style="font-size: 12px;font-weight: bold;background-color:#141B4D;color: white; width:50%">
                                        <apex:outputText value="Client/Event Organizer:"/>
                                    </td>
                                    <td class='colored-label' colspan="2" style="font-size: 12px;font-weight: bold;background-color:#141B4D;color: white; width:50%">
                                        <apex:outputText value="Event Contractor:"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class='colored-label' style="font-size: 12px;font-weight: bold;background-color:#141B4D;color: white; width:20%">Name:</td>
                                    <td><apex:outputText style="font-size: 12px;height:100%;width:100%;" value="{!HTMLEncode(foObj.Client_Name__c)}" id="clientName"/></td>
                                    <td class='colored-label' style="font-size: 12px;font-weight: bold;background-color:#141B4D;color: white; width:20%">Name:</td>
                                    <td><apex:outputText style="font-size: 12px;height:100%;width:100%;" value="{!HTMLEncode(foObj.Contractor_Name__c)}" id="contractorName"/></td>
                                </tr>
                            </table>
                            <table style="width:96%; position: absolute; top: {!IF(NOT(IsBlank(contractor)),'178%','180%')}; margin: 0 15px" border="0">
                                <tr>
                                    <td class='colored-label' colspan="2" style="font-size: 12px;font-weight: bold;background-color:#141B4D;color: white; width:50%">
                                        <apex:outputText value="FOSP HSE:" style="font-size: 12px;"/>
                                    </td>
                                    <td class='colored-label' colspan="2" style="font-size: 12px;font-weight: bold;background-color:#141B4D;color: white; width:50%">
                                        <apex:outputText value="FOSP Fit-out:" style="font-size: 12px;"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td class='colored-label' style="font-size: 12px;font-weight: bold;background-color:#141B4D;color: white; width:20%">Name:</td>
                                    <td><apex:outputText value="{!HTMLEncode(foObj.IDAMA_HSE_Name__c)}" style="font-size: 12px;width:100%"/></td>
                                    <td class='colored-label' style="font-size: 12px;font-weight: bold;background-color:#141B4D;color: white; width:20%">Name:</td>
                                    <td><apex:outputText id="FOName" value="{!HTMLEncode(foObj.IDAMA_FO_Name__c)}" style="font-size: 12px;width:100%"/></td>
                                </tr>
                                <tr>
                                    <td class='colored-label' style="font-size: 12px;font-weight: bold;background-color:#141B4D;color: white; width:20%">Position:</td>
                                    <td><apex:outputText id="HSEPos" value="{!HTMLEncode(foObj.IDAMA_HSE_Position__c)}" style="font-size: 12px;width:100%"/></td>
                                    <td class='colored-label' style="font-size: 12px;font-weight: bold;background-color:#141B4D;color: white; width:20%">Position:</td>
                                    <td><apex:outputText id="FOPos" value="{!HTMLEncode(foObj.IDAMA_FO_Position__c)}" style="font-size: 12px;width:100%"/></td>
                                </tr>
                            </table>
                            
                            
                            <table style="font-size: 12px; border:none; margin: 0 15px; position: absolute; top: {!IF(NOT(IsBlank(contractor)),'191%','193%')}; margin: 0 15px">
                                <tr style="font-size: 12px; border:none;" >
                                    <td style="font-weight:bold; font-size: 12px; border:none;">
                                        Status:
                                    </td>
                                </tr>
                                <tr style="font-size: 12px; border:none;" >
                                    <td style="font-size: 12px; border:none;" >
                                        1.  Adjustment in area of event is required &nbsp;&nbsp;&nbsp; <apex:image URL="{!if(readjustment,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                        
                                    </td>
                                </tr>
                                <tr style="font-size: 12px; border:none;" >
                                    <td style="font-size: 12px; border:none;" >
                                        2.  Event location is accepted with all facilities in satisfactory condition. &nbsp;&nbsp;&nbsp; <apex:image URL="{!if(accepted,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                    </td>
                                </tr>
                                <tr style="font-size: 12px; border:none;" >
                                    <td style="font-size: 12px; border:none;" >
                                        3.  Event location to be re-inspected for comments on &nbsp;
                                        <apex:outputText style="float:left;" id="reinspectComments" value="{!foObj.IDAMA_Comments__c}"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <apex:image URL="{!if(reinspection,'/img/checkbox_checked.gif','/img/checkbox_unchecked.gif')}" />
                                    </td>
                                </tr>
                                <tr style="font-size: 12px; border:none;" >
                                    <td style="font-weight:bold;font-size: 12px; border:none;">
                                        Notes:
                                    </td>
                                </tr>                                                                                               
                                
                                <!-- <tr> Commented by: Claude Manahan. 06-02-2016
                                    <td >
                                        1.  This form shall be submitted to IDAMA Fit-out after Client’s signature and company stamp immediately after the job completion.
                                    </td>
                                </tr> -->
                                  
                                <tr style="font-size: 12px; border:none;" >
                                    <td style="font-size: 12px; border:none;" >
                                        1.  The security deposit will be returned after three (3) working days of the issuance of the Completion Certificate.
                                    </td>
                                </tr>
                                <tr style="font-size: 12px; border:none;" >
                                    <td style="font-size: 12px; border:none;" >
                                        2.  Original Security Deposit receipt must be presented to FOSP at the time of claiming refund.
                                    </td>
                                </tr>
                            </table>
                        </apex:outputPanel>
                    </apex:outputPanel>
                </div>
                
                <!-- New Footer -->
                                            
                <c:fo_difc_checklistfooter noteClass="difc-footer-note--custom" signatureClass="difc-footer-signature--custom" rowClass="difc-footer-row--custom" refNum="{!refNum}" updateDate="{!updatedDate}"  />
            </div>
        </div>
    </body>   
</apex:page>