({
	getContactList: function(component, pageNumber, pageSize) {
		var action = component.get("c.getWrapperList");
      // set param to method  
        action.setParams({
            "searchKey": '',
            "pageNumber":  pageNumber,
            "jsonstring" : JSON.stringify(component.get("v.objectList"))
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var storeResponse = response.getReturnValue();
              
                if(storeResponse !== null ) {
                    
                    component.set("v.ListOfContact", storeResponse.thisRelationShipList);
                    component.set("v.PageNumber", storeResponse.pageNumber);
                    component.set("v.TotalRecords", storeResponse.totalRecords);
                    component.set("v.RecordStart", storeResponse.recordStart);
                    component.set("v.RecordEnd", storeResponse.recordEnd);
                    component.set("v.TotalPages", Math.ceil(storeResponse.totalRecords / 10));
                    
                    if(storeResponse.totalRecords > 10){
                      component.set("v.isPagination", true);
                  }
                else{
                     component.set("v.isPagination", false);
                }
                if(storeResponse.thisRelationShipList.length == 0){
                     component.set("v.isEmpty", true);
                }
                    else{
                     component.set("v.isEmpty", false);
                    }
               }
               
            }
            else{
                 
            }
 
        });
      
      // enqueue the Action  
        $A.enqueueAction(action); 
   },
})