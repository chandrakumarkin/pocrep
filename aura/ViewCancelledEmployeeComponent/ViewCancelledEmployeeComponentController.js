({
	doInit : function(component, event, helper) { 
      var obj = [];
      var newarr = {'selectedField': '' ,'operator': 'LIKE','value': '','disabled': 'false'}  ;
      obj.push(newarr); 
      component.set("v.objectList", obj); 
      var action = component.get("c.getWrapperList");
      // set param to method  
        action.setParams({
            "searchKey": '',
            "pageNumber":  1,
            "jsonstring" : ''
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var storeResponse = response.getReturnValue();
             
                if(storeResponse !== null ) {
                    component.set("v.ListOfContact", storeResponse.thisRelationShipList);
                    component.set("v.PageNumber", storeResponse.pageNumber);
                    component.set("v.TotalRecords", storeResponse.totalRecords);
                    component.set("v.RecordStart", storeResponse.recordStart);
                    component.set("v.RecordEnd", storeResponse.recordEnd);
                    component.set("v.TotalPages", Math.ceil(storeResponse.totalRecords / 10));
              }
                
                if(storeResponse.totalRecords > 10){
                      component.set("v.isPagination", true);
                      component.set("v.isNext", false);
                  }
                else{
                     component.set("v.isPagination", false);
                }
                 component.set("v.disableprev", true);
            }
            if(storeResponse.length ==0){
                component.set("v.isEmpty", true);
            }
            else{
                component.set("v.isEmpty", false);
            }
 
        });
      
      // enqueue the Action  
        $A.enqueueAction(action); 
   },
   viewLinkedIN : function(component, event, helper) { 
      
       window.open(event.getSource().get("v.value"));
   },
   download : function(component, event, helper) { 
       
       window.open('/servlet/servlet.FileDownload?file='+event.getSource().get("v.value"));
   },
    
    handlePrev: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = 10;
        pageNumber--;
        if(pageNumber == 1){
            component.set("v.disableprev", true);
        }
        component.set("v.isNext", false);
        helper.getContactList(component, pageNumber, pageSize);
      
    },
    
    handleNext: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = 10;
        pageNumber++;
        
        component.set("v.disableprev", false);
        if(component.get("v.TotalRecords")<(10*pageNumber)){
               component.set("v.isNext", true);
        }
        helper.getContactList(component, pageNumber, pageSize);
   },
   
   search : function(component, event, helper) {
     var pageNumber = component.get("v.PageNumber");  
     var pageSize = 10;
     helper.getContactList(component, 1, pageSize);
   },
    
   removeDeletedRow : function(component, event, helper) {
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
        var newRecords = component.get("v.objectList");
        newRecords.splice(index, 1);
       if(newRecords.length==0){
         var newobj =  {'selectedField': '' ,'operator': '','value': '','disabled':'true'} ;
         newRecords.push(newobj);
       }
        component.set("v.objectList",newRecords);
   },
   AddRow : function(component, event, helper) {
      var objlst = component.get("v.objectList");
      var optionval = component.get("v.options1");
       
      var newlist = [];
       for(var i=0;i<objlst.length;i++){
           for(var k =0;k<optionval.length;k++){
               if(objlst[i].selectedField ==optionval[k].value ){
                    optionval.splice(optionval[k], 1);
               }
           }
           newlist.push(objlst[i]);
        }
       var newobj =  {'selectedField': '' ,'operator': '','value': '','disabled':'true'} ;
      newlist.push(newobj);
      component.set("v.objectList",newlist);
       component.set("v.options1",optionval);
   },
    reset : function(component, event, helper) { 
            $A.get('e.force:refreshView').fire();
    },
    clear : function(component, event, helper) { 
           
    },
    viewPricing: function (component, event, helper) {
         component.set("v.email", '');
        component.set("v.mobile", '');
        component.set("v.addDetails", true);
        var objList  = component.get("v.ListOfContact");
        for (var i = 0; i < objList.length; i++) {
            if (event.getSource().get("v.value") === objList[i].thisEmployee.Id) {
               component.set("v.email", objList[i].thisEmployee.Email__c);
               component.set("v.mobile", objList[i].thisEmployee.Mobile__c);
               component.set("v.isLinkedInDisabled", objList[i].isLinkedInDisabled);
               component.set("v.linkedIn", objList[i].thisEmployee.LinkedIn_Profile__c);
               component.set("v.isDownload", objList[i].isDisabled);
               component.set("v.DownloadLink", objList[i].thisAttachment.Id);
               break;
            }
            else{
               component.set("v.email","");
               component.set("v.mobile", "");
            }
        }
    },
     closePrice: function (component, event, helper) {
        component.set("v.addDetails", false);
        component.set("v.viewExperience", false);
    },
    viewJob: function (component, event, helper) {
        component.set("v.jobFunction", '');
        component.set("v.viewExperience", true);
        var objList  = component.get("v.ListOfContact");
        for (var i = 0; i < objList.length; i++) {
            if (event.getSource().get("v.value") === objList[i].thisEmployee.Id) {
               component.set("v.jobFunction", objList[i].thisEmployee.Area_of_relevant_experience__c);
               break;
            }
            else{
               component.set("v.jobFunction", '');
            }
        }
    },
     refresh: function(component, event, helper) {
      location.reload(true);
   }
    
    
})