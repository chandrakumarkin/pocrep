({
  sendDocument: function(component, event, helper) {
    var urlEvent = $A.get("e.force:navigateToURL");
    console.log(component.get("v.respWrap.relActionIteam.Id"));

    urlEvent.setParams({
      url:
        "/apex/OB_DocuSignVFPage?srId=" +
        component.get("v.recordId") +
        "&stepId" +
        component.get("v.respWrap.relActionIteam.Id")
    });
    urlEvent.fire();

    /* var evt = $A.get("e.force:navigateToComponent");
    evt.setParams({
      componentDef: "c:OB_SendWithDocusignPage",
      componentAttributes: {
        srId: component.get("v.recordId")
      }
    });
    evt.fire(); */
  },
  doInit: function(component, event, helper) {
    var action = component.get("c.checkRender");
    var requestWrap = {
      srId: component.get("v.recordId")
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        component.set("v.respWrap", respWrap);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  }
});