({
    getReceiptData : function(component,event,rptId) {
        let action = component.get('c.getReceiptData');
        action.setParams({
            "rptId":rptId
        });
        action.setCallback(this, function(response){
            let state = response.getState(); // get the response state
            
            if(state == 'SUCCESS') {
                let respData = response.getReturnValue();
                //console.log('Apex resp-->'+respData);
                if(!$A.util.isEmpty(respData)){
                    component.set("v.receiptObj",respData); 
                }
                
            }else if (state === 'ERROR') { // Handle any error by reporting it
                //var errors = response.getError();
                console.log(response.getError());
                
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log('Apex Error1...');
                    }
                } else {
                    console.log('Apex Error2...');
                }
            }else{
                console.log('Apex Error3...');
            }
        });
        $A.enqueueAction(action);
    }
})