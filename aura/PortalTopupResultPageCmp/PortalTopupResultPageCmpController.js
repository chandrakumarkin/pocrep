({
    oninit : function(component, event, helper) {
        window.onbeforeunload = function () {
            window.scrollTo(0, 0);
        }
        let allParams = window.location.search;
        if(allParams && allParams.length > 0 ){
            let urlParams = new URLSearchParams(allParams);
            //console.log('INIT->urlParams-->',urlParams);
            if(urlParams){
                let status = urlParams.get('s'); //paystatus
                let rptId = window.atob(urlParams.get('rid'));
                //console.log('INIT->status-->',status);
                //console.log('INIT->Decoded Id-->',rptId);
                if(status && status  == 'y' && rptId && rptId.length > 0){
                    //component.set("v.msgTitle","Success!!");
                    //component.set("v.msgType","confirm");
                    //component.set("v.msgToShow","Payment process successfully. Thank you!");
                    
                    component.set("v.showForm",true);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Success!!",
                        "mode": "pester",
                        "type": "success",
                        "message": "Payment completed successfully. Thank you!",
                        "duration":6000
                    });
                    toastEvent.fire();
                    
                    helper.getReceiptData(component,event,rptId); 
                    
                }else if(status && status  == 'n' && rptId && rptId.length > 0){
                    //component.set("v.msgTitle","Error!!");
                    //component.set("v.msgType","error");
                    //component.set("v.msgToShow","Payment failed, please try again later.");
                    
                    component.set("v.showForm",true);
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Oops!",
                        "mode": "pester",
                        "type": "error",
                        "message": "Payment failed, please try again later.",
                        "duration":6000
                    });
                    toastEvent.fire();
                    
                    helper.getReceiptData(component,event,rptId);
                    
                }else{
                    console.log('INIT->Invalid Status');
                    //component.set("v.msgTitle","Error!!");
                    //component.set("v.msgType","error");
                    //component.set("v.msgToShow","Error Ocurred!!, Please contact system administrator.");
                    
                    let toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Aww!",
                        "mode": "pester",
                        "type": "error",
                        "message": "Error Ocurred!, please contact system administrator..",
                        "duration":6000
                    });
                    toastEvent.fire();
                    
                    
                }
            }else{
                
                console.log('INIT->No status found!'); 
                //component.set("v.msgTitle","Error!!");
                //component.set("v.msgType","error");
                //component.set("v.msgToShow","Error Ocurred!!, Please contact system administrator.");
                let toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Aww!",
                    "mode": "pester",
                    "type": "error",
                    "message": "Error Ocurred!, please contact system administrator..",
                    "duration":6000
                });
                toastEvent.fire();
            }
            
        }else{
            console.log('INIT->No parameter found!');
            //component.set("v.msgTitle","Error!!");
            //component.set("v.msgType","error");
            // component.set("v.msgToShow","Error Ocurred!!, Please contact system administrator.");
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                "title": "Aww!",
                "mode": "pester",
                "type": "error",
                "message": "Error Ocurred!, please contact system administrator..",
                "duration":6000
            });
            toastEvent.fire();
        }	
    },
    handleClick : function(cmp,event,helper){
        
    }
})