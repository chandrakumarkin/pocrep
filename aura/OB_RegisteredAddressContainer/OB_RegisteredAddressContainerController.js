({
  doInit: function(cmp, event, helper) {
    try {
      var getFormAction = cmp.get("c.getOperatingLocations");
      let newURL = new URL(window.location.href).searchParams;
      var srIdParam = newURL.get("srId")
        ? newURL.get("srId")
        : cmp.get("v.srId");
      var flowId = newURL.get("flowId")
        ? newURL.get("flowId")
        : cmp.get("v.flowId");
      var pageId = newURL.get("pageId");
      cmp.set("v.srId", srIdParam);
      console.log("in init-->" + srIdParam);

      var reqWrapPram = {
        srId: srIdParam,
        pageId: pageId,
        flowId: flowId
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      console.log(
        "@@@@@@@@@@33121 reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
      getFormAction.setCallback(this, function(response) 
      {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") 
        {
          var respWrap = response.getReturnValue();
             console.log("### respWrap rec ", JSON.stringify(respWrap));
             console.log(respWrap);
          if( respWrap.errorMessage )
          {
               cmp.set("v.respInit", respWrap);
               cmp.set("v.srWrap", respWrap.srWrap);
               cmp.set("v.commandButton", respWrap.ButtonSection);
          }
          else
          {
              cmp.set("v.respInit", respWrap);
              cmp.set("v.srWrap", respWrap.srWrap);
              cmp.set("v.commandButton", respWrap.ButtonSection); 
          }
         
        } 
        else 
        {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
    }
  },
  handleLookupEvent: function(cmp, event, helper) 
  {
    console.log("handleLookupEvent");
    var accObj = event.getParam("sObject");
    console.log("Account ", JSON.parse(JSON.stringify(accObj)));
    console.log(
      "Account ",
      cmp.get("{!v.srWrap.srObj.Corporate_Service_Provider_Name__c}")
    );

    try {
      var getFormAction = cmp.get("c.getOperatingLocationsForCSP");
      let newURL = new URL(window.location.href).searchParams;
      var srIdParam = newURL.get("srId")
        ? newURL.get("srId")
        : cmp.get("v.srId");
      var pageId = newURL.get("pageId");
      cmp.set("v.srId", srIdParam);
      console.log("in init-->" + srIdParam);

      var reqWrapPram = {
        srId: srIdParam,
        pageId: pageId,
        srWrap: cmp.get("{!v.srWrap}")
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      console.log(
        "@@@@@@@@@@33121 reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
      getFormAction.setCallback(this, function(response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") 
        {
          var respWrap = response.getReturnValue();
          // if (!respWrap.srWrap.isDraft) {
          //   var pageURL = respWrap.srWrap.viewSRURL;
          //   window.open(pageURL, "_self");
          // }

          console.log("### respWrap.srWrap ", JSON.stringify(respWrap.srWrap));
          cmp.set("v.respCSP", respWrap);
          cmp.set("v.srWrap", respWrap.srWrap);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
    }
  },

  handleRefreshEvnt: function(cmp, event, helper) {
    var message = event.getParam("message");
    $A.enqueueAction(cmp.get("c.doInit"));
  },
  clearLookupEvent: function(cmp, event, helper) 
  {
    if (cmp.get("v.respCSP.lstOprLocWrap")) {
      cmp.set("v.respCSP.lstOprLocWrap", []);
    }
  },
  handleButtonAct: function(component, event, helper) 
  {
    //try 
    //{
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");
	  var srWrap = component.get("v.srWrap");
        console.log('###### srWrap   ',JSON.stringify(srWrap) );
         //console.log("###  srWrap Location_of_records_and_registers__c ", srWrap.srObj.Location_of_records_and_registers__c);
      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId,
        srWrap : srWrap
      });

      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");

          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
   // } 
    //catch (err) 
    //{
      //console.log(err.message);
        
    //}
  },
  handleRegAddOthr : function(cmp, event) 
  {
            var srWrapOthrLoc = event.getParam("srWrapOthrLoc");
            
            console.log('##### srWrapOthrLoc.srObj.Location_of_records_and_registers__c ',srWrapOthrLoc.srObj.Location_of_records_and_registers__c);
        	//console.log('##### srWrap.srObj.Records_and_registers_held_by__c ',srWrap.srObj.Records_and_registers_held_by__c);
        	
      		//if(srWrapOthrLoc.srObj.Location_of_records_and_registers__c)
            //{
                cmp.set('v.srWrap.srObj.Location_of_records_and_registers__c',srWrapOthrLoc.srObj.Location_of_records_and_registers__c);
            //}
      		
  
        	
      		//if(srWrapOthrLoc.srObj.Records_and_registers_held_by__c)
            //{
                cmp.set('v.srWrap.srObj.Records_and_registers_held_by__c',srWrapOthrLoc.srObj.Records_and_registers_held_by__c);
            //}
        	
        
            
  }  
    
});