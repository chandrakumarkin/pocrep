({
	searchChange : function(component, event, helper) {
		var selectedValue = component.find("searchBy").get("v.value");
        if(selectedValue !== undefined || selectedValue != ''){
            component.set("v.searchByValue",selectedValue);
            component.set("v.issearchByValue",true);
        }
        else {
            component.set("v.issearchByValue",false);
            component.set("v.searchByValue",'');
        }
	},
    
    cancel : function(component, event, helper) {
       location.reload();
    },
    showSelected : function(component, event, helper) {
       var selectedRecords = [];
       var allRecords = component.get("v.selectedpassportTracking");
        
           for(var j=0;j<allRecords.length;j++){
              if(allRecords[j].isChecked == true ){
                  selectedRecords.push(allRecords[j]); 
            }
        }
        component.set("v.passportTracking", selectedRecords);
      },
    
    searchTextChange : function(component, event, helper) {
       var selectedValue = component.find("searchText").get("v.value");
      if(selectedValue !== undefined || selectedValue != ''){
            
       var action = component.get("c.getwrapperList");
      // set param to method  
        action.setParams({
            "searchValue": selectedValue,
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                     component.set("v.norecordfound", true);
                } else {
                   component.set("v.norecordfound", false);
                }
                var allRecords = component.get("v.selectedpassportTracking");
                  for(var i=0;i<storeResponse.length;i++){
                      
                        for(var j=0;j<allRecords.length;j++){
                            
                        if(allRecords[j].passportTrackingList.Id == storeResponse[i].passportTrackingList.Id
                               && allRecords[j].isChecked == true ){
                            
                            storeResponse[i].isChecked = true; 
                        }
                   }
                }
               component.set("v.passportTracking", storeResponse);
               component.set("v.TotalCount", storeResponse.length);
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);

        }
       
	},
    
    doInit  : function(component, event, helper) {
        var action = component.get("c.getwrapperList");
      // set param to method  
        action.setParams({
            "searchValue": component.get("v.searchTexts")
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                     component.set("v.norecordfound", true);
                } else {
                    if(storeResponse[0].isReceptionId){
                         component.set("v.isRep", true);
                    }
                    else{
                        component.set("v.isRep", false);
                    }
                     component.set("v.norecordfound", false);
                }
               component.set("v.passportTracking", storeResponse);
               component.set("v.TotalCount", storeResponse.length);
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);
    
    },
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    
    pickValChangeChange : function(component, event, helper) {
		var selectedValue = event.getSource().get("v.value");
         component.set("v.selectedStatus", selectedValue);
       },
   newPickValChangeChange : function(component, event, helper) {
		var selectedValue = event.getSource().get("v.value");
        
         if(selectedValue !== undefined || selectedValue != ''){
            
       var action = component.get("c.getwrapperList");
      // set param to method  
        action.setParams({
            "searchValue": selectedValue,
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                     component.set("v.norecordfound", true);
                } else {
                   component.set("v.norecordfound", false);
                }
                var allRecords = component.get("v.selectedpassportTracking");
                  for(var i=0;i<storeResponse.length;i++){
                      
                        for(var j=0;j<allRecords.length;j++){
                            
                        if(allRecords[j].passportTrackingList.Id == storeResponse[i].passportTrackingList.Id
                               && allRecords[j].isChecked == true ){
                            
                            storeResponse[i].isChecked = true; 
                        }
                   }
                }
               component.set("v.passportTracking", storeResponse);
               component.set("v.TotalCount", storeResponse.length);
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);
         }
    },
    
    checkboxSelect : function(component, event, helper) {
        
		var selectedRec = event.getSource().get("v.value");
        var selectedText = event.getSource().get("v.text");
        var getSelectedNumber = component.get("v.selectedCount");
       
        var allRecords = component.get("v.passportTracking");
        var newRecords = component.get("v.selectedpassportTracking");
         
        for(var i=0;i<allRecords.length;i++){
            if(allRecords[i].passportTrackingList.Id == selectedText  ){
                if(selectedRec == true){
					  allRecords[i].isChecked = true;
                      newRecords.push(allRecords[i]);                    
                }
             }
            }
      
            for(var i=0;i<newRecords.length;i++){
              if(newRecords[i].passportTrackingList.Id == selectedText 
                 && selectedRec == false){
                newRecords.splice(i, 1);
              }
            }
        
        if (selectedRec == true) {
            component.set("v.isCheckboxSelected", true);
            
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
            if(getSelectedNumber ==0){
                  component.set("v.isCheckboxSelected", false);
            }
        }
          component.set("v.selectedpassportTracking", newRecords);
         component.set("v.selectedCount", getSelectedNumber);
    },
    Save: function(component, event, helper) {
        var allRecords = component.get("v.passportTracking");
        if(component.get("v.selectedStatus")=== ''){
            component.set("v.isUpdated", true);
            component.set("v.message", 'Status is required field.');
            
            return;
        }
        else{
              component.set("v.isUpdated", false);
        }
        var selectedRecords = [];
        var isError = false;
        for (var i = 0; i < allRecords.length; i++) {
            if (allRecords[i].isChecked) {
                if(allRecords[i].passportTrackingList.Status__c == 'Ready for Collection' 
                   				&& ( component.get("v.selectedStatus")=='Returned to Office Boy'
                   || component.get("v.selectedStatus")=='Received by Office boy')){
                    isError = true;
                    break;
                }
                 if(allRecords[i].passportTrackingList.Status__c == 'Received by Receptionist' 
                   				&& component.get("v.selectedStatus")=='Received by Office boy'
                  ){
                    isError = true;
                    break;
                }
                 if(allRecords[i].passportTrackingList.Status__c == 'Returned to Office Boy' 
                   				&& component.get("v.selectedStatus")=='Received by Receptionist'
                  ){
                    isError = true;
                    break;
                }
                selectedRecords.push(allRecords[i].passportTrackingList);
            }
        }
        if(isError){
             component.set("v.message", 'You are not allowed to updated the status.Please check current Status.');
              component.set("v.isUpdated", true); 
            return;
        }
      
       // var statusList = component.get("v.statusList");
        var action = component.get("c.updatePassportTracking");
      // set param to method  
        action.setParams({
            "selectedId": JSON.stringify(selectedRecords),
            'selectedValue'       : component.get("v.selectedStatus")
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                location.reload();
              
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);

    }
    

})