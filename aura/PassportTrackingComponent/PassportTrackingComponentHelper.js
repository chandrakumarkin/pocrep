({
	helperMethod1: function(component) {
		 var action = component.get("c.getwrapperList");
      // set param to method  
        action.setParams({
            "searchValue": component.get("v.searchTexts"),
            'searchBy'       : component.get("v.searchByValue")
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                    
                } else {
                  
                }
               component.set("v.passportTracking", storeResponse);
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);
	}
})