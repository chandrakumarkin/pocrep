({
  doInit: function (component, event, helper) 
  {
     console.log('######## IN Do ');
    var action = component.get("c.getCPRegisterAddress");
    let newURL = new URL(window.location.href).searchParams;
    var srID = newURL.get("srId");
    var pageId = newURL.get("pageId");
    var requestWrap = {
      srId: srID,
      pageId: pageId
    };

    action.setParams({ reqWrapPram: JSON.stringify(requestWrap) });
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") 
      {
        var respWrap = response.getReturnValue();
        console.log('######## respWrap ',respWrap);
        
        component.set("v.respInit", respWrap);
        component.set("v.lstOprLocWrap", respWrap.lstOprLocWrap);
        component.set("v.lstVenueWrap", respWrap.lstVenueWrap);
        //srWrap
        component.set("v.srWrap", respWrap.srWrap);
		console.log('######## SR Details '+JSON.stringify(respWrap.srWrap.amedWrapLst));
        //respInit
		

        component.set(
          "v.commandButton",
          response.getReturnValue().ButtonSection
        );
      } 
      else 
      {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log("@@@@@ Error Location " + response.getError()[0].stackTrace);
      }
    });
    $A.enqueueAction(action);
  },

  handleRefreshEvnt: function(cmp, event, helper) {
    var message = event.getParam("message");
    $A.enqueueAction(cmp.get("c.doInit"));
  },

  handleButtonAct: function (component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");

          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  }
});