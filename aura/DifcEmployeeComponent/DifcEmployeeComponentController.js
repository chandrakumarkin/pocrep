({
   loadContactList : function(component, event, helper) {
	
   },
    
   refresh: function(component, event, helper) {
      location.reload(true);
   },
  
   searchTextChange : function(component, event, helper) { 
    
    // helper.showSpinner(component);
      var selectedValue = component.find("searchText").get("v.value");
      
      if(selectedValue !== undefined || selectedValue != ''){
          
       component.set("v.showSpinner",true);
       var action = component.get("c.thisWrapper");
      // set param to method  
        action.setParams({
            "Status": component.get("v.value"),
            "valueName" :selectedValue,
            "pageNumber" : component.get("v.PageNumber"),
            "sortField" : 'Object_Contact__r.FirstName',
            "isAsc" : component.get("v.isAsc")
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
              // if storeResponse siz!e is equal 0 ,display No Result Found... message on screen.
                if(storeResponse !== null ) {
                       
                     var allRecords = component.get("v.selectedListOfContact");
                     var finalRecords = [];
                     for(var i=0;i<storeResponse.thisRelationShipList.length;i++){
                            for(var j =0;j<allRecords.length;j++) {
                               if(allRecords[j].thisRelationShip.Id == storeResponse.thisRelationShipList[i].thisRelationShip.Id){
                                    storeResponse.thisRelationShipList[i].isEnabled = true;
                               }
                              
                            }
                             finalRecords.push(storeResponse.thisRelationShipList[i]);  
                         }
                      component.set("v.ListOfContact", finalRecords);
                       
                }
                else{
                     component.set("v.ListOfContact", []);
                }
                   
              }
 
        });
      
      // enqueue the Action  
        $A.enqueueAction(action);
          
     }
   },
  open: function(component, event, helper) {   
     //window.open('/digitalOnboarding/s/',"_self");
     window.open('/clientportal/s/',"_self");
   },
  checkbocChange: function(component, event, helper) {   
   var selectedRec = event.getSource().get("v.checked");
      var selectedText = event.getSource().get("v.name");
      
      var getSelectedNumber = component.get("v.selectedCount");
       
        var allRecords = component.get("v.ListOfContact");
        var newRecords = component.get("v.selectedListOfContact");
        var newrecordList = []; 
        for(var i=0;i<allRecords.length;i++){
            if(allRecords[i].thisRelationShip.Id == selectedText){
                if(selectedRec == true){
                      allRecords[i].isEnabled = true;
                      newRecords.push(allRecords[i]);  
               }
             }
            }
           
            var objList  = component.get("v.objList");
      
            for(var i=0;i<newRecords.length;i++){
              if(newRecords[i].thisRelationShip.Id == selectedText 
                 && selectedRec == false){
                 
               for (var k = 0; k < objList.length; k++) {
                 if (newRecords[i].thisRelationShip.Relationship_Passport__c === objList[k].parentId) {
                    objList.splice(k, 1);
                 }
               }
                newRecords.splice(i, 1);
               }
             }
            
       
        if (selectedRec == true) {
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
        }
         if(getSelectedNumber !== 0)
         {
              component.set("v.isPriview", true);
         }
         else{
               component.set("v.isPriview", false);
               component.set("v.addDetails", false);
               
               component.set("v.fileName", "No File Selected..");
               component.set("v.experience", "");
               component.set("v.linkedUrl", "");
               component.set("v.relevantexperience", "");
             
         }
         component.set("v.selectedListOfContact", newRecords);
        
         component.set("v.selectedCount", getSelectedNumber);
        
   },
    showSelected : function(component, event, helper) {
       var selectedRecords = [];
       
       var allRecords = component.get("v.selectedListOfContact");
        
           for(var j=0;j<allRecords.length;j++){
              if(allRecords[j].isEnabled == true ){
                  selectedRecords.push(allRecords[j]); 
            }
        }
        component.get("v.selectedListOfContact",[]);
        component.set("v.isselected", true);
        component.set("v.isPriview", false);
        component.set("v.ListOfContact", selectedRecords);
        component.set("v.isPagination", false);
      },
    
    doSave : function(component, event, helper) {
             
        var allRecords = component.get("v.ListOfContact");
        var selectedRecords = [];
             for(var j=0;j<allRecords.length;j++){
                
              if((allRecords[j].mobile ===  undefined || allRecords[j].mobile === '') || 
                 (allRecords[j].email ===  undefined || allRecords[j].email == '') ||
                  (allRecords[j].desg ===  undefined || allRecords[j].thisRelationShip.desg ==='') ||
                  (allRecords[j].exp ===  undefined || allRecords[j].exp === null || allRecords[j].exp === '')){
                  
                   component.set("v.showError", true); 
             	   component.set("v.errorMessage", "All required fields should be filled before submit.");
                   setTimeout(function(){ 
                   component.set("v.showError", false); 
                   component.set("v.errorMessage", ""); 
                   }, 3500);
                return;
            }
            
            var newSr = {FirstName:allRecords[j].thisRelationShip.Object_Contact__r.FirstName, LastName:allRecords[j].thisRelationShip.Object_Contact__r.LastName, mobile:allRecords[j].mobile,email:allRecords[j].email,desg:allRecords[j].desg,exp:allRecords[j].exp,passport:allRecords[j].thisRelationShip.Relationship_Passport__c,bpNumber:allRecords[j].thisRelationShip.Relationship_Passport__c,relationShip:allRecords[j].thisRelationShip.Relationship_Type__c};
            selectedRecords.push(newSr);
        }
        component.set("v.Spinner", true);   
     var action = component.get("c.saveSR");
          action.setParams({
              "selectedId" : JSON.stringify(selectedRecords),
              "objlist": JSON.stringify(component.get("v.objList"))
          });
      
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                   
                 } else {
                     component.set("v.issaved", true); 
                     component.set("v.accountRecord", storeResponse); 
                      component.set("v.Spinner", false);
                 }
            }else{
               
            }
        });
        $A.enqueueAction(action);
    },
    // function for delete the row 
    removeDeletedRow: function(component, event, helper) {
        var getSelectedNumber = component.get("v.selectedCount");
        if(getSelectedNumber == 1){
             component.set("v.showError", true); 
             component.set("v.errorMessage", "Atleast one employee record should be present. ");
            setTimeout(function(){ 
             component.set("v.showError", false); 
             component.set("v.errorMessage", ""); 
            }, 3000);
            
            return;
        }
        else{
             component.set("v.showError", false); 
        
        var selectedItem = event.currentTarget;
        var index = selectedItem.dataset.record;
        var newRecords = component.get("v.ListOfContact");
        var objList  = component.get("v.objList");
            
    
        for (var k = 0; k < objList.length; k++) {
            if (newRecords[index].thisRelationShip.Relationship_Passport__c === objList[k].parentId) {
                objList.splice(k, 1);
            }
         }
    
            
        newRecords.splice(index, 1);
            
        getSelectedNumber--;
        component.set("v.ListOfContact", newRecords); 
        component.set("v.selectedCount", getSelectedNumber);
        }
    },
    
    goBack: function(component, event, helper) {
        component.set("v.isselected", false);
        component.set("v.isPriview", true);
        component.set("v.PageNumber",1);
         component.set("v.disableprev", true);
        var allRecords = component.get("v.ListOfContact");
        var newRecords = []; 
        for(var i=0;i<allRecords.length;i++){
           allRecords[i].isEnabled = true;
           newRecords.push(allRecords[i]);  
        }
        component.set("v.selectedListOfContact", newRecords);  
   },
  
    // this function automatic call by aura:waiting event  
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    
    openSR : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       // window.open('/customers/'+component.get("v.accountRecord.Id"),"_self");
       window.open('/'+component.get("v.accountRecord.Id"),"_self");
    },

    
    viewPricing: function (component, event, helper) {
        
      component.set("v.parentId",event.getSource().get("v.value"));
        component.set("v.addDetails", true);
        var objList  = component.get("v.objList");
        for (var i = 0; i < objList.length; i++) {
            if (event.getSource().get("v.value") === objList[i].parentId) {
               component.set("v.experience", objList[i].experience);
               component.set("v.linkedUrl", objList[i].linkedUrl);
                component.set("v.relevantexperience", objList[i].relevantexperience);
               component.set("v.fileName", objList[i].fileName);
               break;
            }
            else{
               component.set("v.experience","");
               component.set("v.linkedUrl", "");
               component.set("v.relevantexperience", "");
               component.set("v.fileName",'No File Selected..');
            }
        }
    },
    
     closePrice: function (component, event, helper) {
        component.set("v.addDetails", false);
    },
    
     handleSave: function(component, event, helper) {
       var objList  = component.get("v.objList");
        for (var i = 0; i < objList.length; i++) {
            if (component.get("v.parentId") === objList[i].parentId) {
                objList.splice(i, 1);
            }
      }
       
         if(component.get("v.linkedUrl")!== '' && !component.get("v.linkedUrl").startsWith('https://')){
             return;
         }
         
        if (component.get("v.fileName") !== 'No File Selected..') {
            helper.uploadHelper(component, event);
        } else {
       
        var objList  = component.get("v.objList");
      
        var obj = {
            parentId: "",
            fileName: "",
            base64Data: "",
            contentType: "",
            fileId:"",
            experience:"",
            linkedUrl :"",
            relevantexperience :""
        };
            obj.parentId = component.get("v.parentId"),
            obj.fileName = '',
            obj.base64Data = '',
            obj.contentType = '',
            obj.fileId = '',
            obj.experience = component.get("v.experience"),
            obj.linkedUrl = component.get("v.linkedUrl"),
            obj.relevantexperience = component.get("v.relevantexperience")
            
      objList.push(obj);
      component.set("v.addDetails", false);
     }
    },
     
    handleFilesChange: function(component, event, helper) {
        var fileName = 'No File Selected..';
        if (event.getSource().get("v.files").length > 0) {
            fileName = event.getSource().get("v.files")[0]['name'];
        }
        component.set("v.fileName", fileName);
    },  
    lookupchange: function(component, event, helper) {
        component.set("v.selectedLookId",   event.getSource().get("v.value"));
        component.set("v.islookup", "true");
    },  
    
     handleChangeInput: function(component, event, helper) {
      
        component.set("v.fileName", "No File Selected..");
    },  
    closeLookUp: function(component, event, helper) {
        component.set("v.islookup", "false");
    },  
     ondesgchange: function(component, event, helper) {
        component.set("v.selectedLookId",   event.getSource().get("v.name"));
        component.set("v.islookup", "true");  
    },  
    AddLookUp: function(component, event, helper) {

      
        
        var objList  = component.get("v.ListOfContact");
        var newobj = [];
        for (var i = 0; i < objList.length; i++) {
            if (component.get("v.selectedLookId") === objList[i].thisRelationShip.Id) {
                objList[i].desg = component.get("v.selectedLookUpRecord").Name
            }
            newobj.push(objList[i]);
        }
          component.set("v.ListOfContact", newobj);
          component.set("v.islookup", "false");
    }, 
    handleMouseHover: function(component, event, helper) {
         component.set("v.ispopover", true);
    },
    handleMouseOut: function(component, event, helper) {
         component.set("v.ispopover", false);
    }
})