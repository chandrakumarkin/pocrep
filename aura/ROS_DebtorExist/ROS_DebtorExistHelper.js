({
    createToast: function(component,event, errorMessagr){
       var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissable",
      message: errorMessagr ,
      type: "error",
      duration: 500
    });
    toastEvent.fire();
  }  
})