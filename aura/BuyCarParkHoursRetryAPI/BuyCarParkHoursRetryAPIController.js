({
    doinit: function (component, event, helper) {
        try {
            var recordId = component.get("v.recordId");
            console.log('recordId: '+recordId);
            var action = component.get("c.sendDataToKTC");
            var dismissActionPanel = $A.get("e.force:closeQuickAction");
            dismissActionPanel.fire();
            action.setParams({
                srId: recordId
            });
            
            action.setCallback(this, function (response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('response.getReturnValue(): '+response.getReturnValue());
                    
                } else {
                    console.log("@@@@@ Error " + response.getError()[0].message);
                }
            });
            $A.enqueueAction(action);
        } catch (err) {
            console.log(err.message);
        }
    },
})