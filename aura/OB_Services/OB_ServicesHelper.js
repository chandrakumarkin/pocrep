({
  SearchHelper: function(component, event, reqWrapPram, isInitVal) {
    // show spinner message
    //alert('==entered');
    console.log("===SearchHelper--===");
    component.find("Id_spinner").set("v.class", "slds-show");
    var action = component.get("c.getServicesList");
    action.setParams({
      reqWrapPram: JSON.stringify(reqWrapPram)
    });

    action.setCallback(this, function(response) {
      //alert('==inside--');
      // hide spinner when response coming from server
      component.find("Id_spinner").set("v.class", "slds-hide");
      var state = response.getState();
      if (state === "SUCCESS") {
        var storeResponse = response.getReturnValue();
        console.log(storeResponse);
        if (storeResponse.relRec.Access_Level__c == "Read Only") {
          component.set("v.readOnlyAccess", true);
        }
        
        //Added by shoaib
        if (storeResponse.isGsAccessRevoked) {
          component.set("v.isGSAccessRevoked", true);
        }

        //component.set("v.hasSRAccess", storeResponse.hasSRAccess);
        component.set("v.sections", storeResponse);
        component.set("v.sectionsOrder", storeResponse.sections);
        console.log("@@@@@ initail resp " + JSON.stringify(storeResponse));

        console.log("===role===" + storeResponse.userAccessType);
        console.log("=====contractor===" + storeResponse.isContractorOnly);
        console.log(
          "=====userAccessTypeSet===" + storeResponse.userAccessTypeSet
        );
        console.log("=====serviceType===" + storeResponse.serviceType);
        var custs = [];
        var custs1 = [];
        for (var key in storeResponse.valueList) {
          custs1 = [];
          for (var key1 in storeResponse.valueList[key]) {
            custs1.push({
              value: storeResponse.valueList[key][key1],
              key: key1
            });
          }
          custs.push({ value: custs1, key: key });
        }
        component.set("v.serviceNameList", custs);
        if (isInitVal == true) {
          component.set("v.userAccessTypeSet", storeResponse.userAccessTypeSet);
          component.set("v.servicename", storeResponse.serviceType);
          component.set("v.accountROCStatus", storeResponse.accountROCStatus);
        }
        // if storeResponse size is 0 ,display no record found message on screen.
        if (storeResponse.length == 0) {
          component.set("v.Message", true);
        } else {
          component.set("v.Message", false);
        }
          
          

        //Added By Shoaib
        if (storeResponse.serviceType == "Employee Services" &&  component.get("v.searchKeyword") ===undefined) {
             component.set("v.showPopup", storeResponse.isPopup);
         }
          else{
               component.set("v.showPopup", "false");
          }

        // set numberOfRecord attribute value with length of return value from server
        component.set("v.TotalNumberOfRecord", storeResponse.length);

        // set searchResult list with return value from server.
        //component.set("v.searchResult", storeResponse);
      } else if (state === "INCOMPLETE") {
        console.log("Response is Incompleted");
      } else if (state === "ERROR") {
        var errors = response.getError();
        if (errors) {
          if (errors[0] && errors[0].message) {
            console.log("Error message: ", errors[0].message);
            console.log("Error location: ", errors[0].stackTrace);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  }
});