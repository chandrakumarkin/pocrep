({
  onchange: function(component, event, helper) {
     
    component.set("v.servicename", component.find("select").get("v.value"));
    var reqWrapPram = {
      serviceType: component.get("v.servicename"),
      searchKeyWord: component.get("v.searchKeyword"),
      isInit: false,
      userAccessTypeSet: component.get("v.userAccessTypeSet")
    };

    //helper.prepareServiceNameList(component, event,component.find('select').get('v.value'));
    helper.SearchHelper(component, event, reqWrapPram, false);
  },
  callServer: function(component, event, helper) {
    //component.set("v.callInit",event.getParam("serviceTab"));
    //var callInit = component.get("v.callInit");
    console.log("=callInit=");
     
    //if(callInit === true){
    console.log("=DIFC_Services Component=");
    var reqWrapPram = {
      serviceType: component.get("v.servicename"),
      searchKeyWord: component.get("v.searchKeyword"),
      isInit: true
    };
    helper.SearchHelper(component, event, reqWrapPram, true);
    //}
  },
  closeModal: function(component, event, helper) {
    component.set("v.readOnlyAccess", false);
  },
  //Added By Shoaib
  goHome: function(component, event, helper) {
    component.set("v.isGSAccessRevoked", false);
    
  },
  onEnter: function(component, event, helper) {
    //console.log('=event=='+event.which );
    if (event.which == 13) {
      var str = component.get("v.searchKeyword");
      console.log("=search value=" + str);
      var reqWrapPram = {
        serviceType: component.get("v.servicename"),
        searchKeyWord: str,
        isInit: false,
        userAccessTypeSet: component.get("v.userAccessTypeSet")
      };
      // else call helper function
      helper.SearchHelper(component, event, reqWrapPram, false);
    }
  },
  Search: function(component, event, helper) {
    var str = component.get("v.searchKeyword");
    console.log("=search value=" + str);
    var reqWrapPram = {
      serviceType: component.get("v.servicename"),
      searchKeyWord: str,
      isInit: false,
      userAccessTypeSet: component.get("v.userAccessTypeSet")
    };
    // else call helper function
    helper.SearchHelper(component, event, reqWrapPram, false);
  },
  /*Shoaib Added*/
  handleClick: function(component, event, helper) {
    component.set("v.isPopup", "false");
  },
    closepopup: function (component, event, helper) {
       component.set("v.showPopup", false);
  }
});