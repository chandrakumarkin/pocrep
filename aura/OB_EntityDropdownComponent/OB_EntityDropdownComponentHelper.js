({
  getRelEntities: function(component, event, helper) {
    var action = component.get("c.fetchRelatedEntities");
    var requestWrap = {
      variableName: component.get("v.attributeName")
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        component.set("v.respWrap", respWrap);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  }
});