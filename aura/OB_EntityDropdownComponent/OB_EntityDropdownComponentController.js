({
  doInit: function(component, event, helper) {
    helper.getRelEntities(component, event, helper);
  },
  ShowDropdown: function(component, event, helper) {
    component.set("v.showDropDown", true);
  },
  hideDropdown: function(component, event, helper) {
    component.set("v.showDropDown", false);
  },
  navToAllEntity: function(component, event, helper) {
    window.location.href = "manage-assosiated-entity";
  },

  createEntity: function(component, event, helper) {
      try{
          /* if (relSr) {
      window.location.href = "ob-processflow" + navUrl + "&srId=" + relSr.Id;
      return;
    } */

    var action = component.get("c.createNewEntitySr");
    var requestWrap = {
      contactId: component.get("v.respWrap.loggedInUser.ContactId")
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        window.location.href =
          "ob-processflow" +
          respWrap.pageFlowURl +
          "&srId=" +
          respWrap.insertedSr.Id;
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
      component.set("v.showSpinner", false);
    });
    $A.enqueueAction(action);
      }catch(error){
          console.log(error.message);
      }
    component.set("v.showSpinner", true);

    
  }
});