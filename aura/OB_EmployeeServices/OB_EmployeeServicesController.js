({
  doInit: function (component, event, helper) {
    var action = component.get("c.getCurrentUser");
    action.setCallback(this, function (response) {
      if (response.getState() === "SUCCESS") {
        let userObj = response.getReturnValue();
        component.set("v.profileName", userObj.Profile.Name);
        component.set("v.initialAccId", userObj.Contact.AccountId);
        component.set("v.dpoAccount", userObj.Contact.Account.Appointed_a_data_protection_officer__c);
      }
    });
    $A.enqueueAction(action);
  },

  setCPRenewal: function (component, event, helper) {
    component.set("v.showSpinner", true);
    console.log("her?");

    var action = component.get("c.createCPRenewal");
    /* var requestWrap = {
        variableName: component.get("v.attributeName")
      };
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) }); */

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        component.set("v.showSpinner", false);
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        if (respWrap.errorMessage == null) {
          window.location.href = "ob-processflow" + respWrap.pageFlowURl;
        } else {
          helper.showToast(
            component,
            event,
            helper,
            respWrap.errorMessage,
            "error"
          );
        }
      } else {
        component.set("v.showSpinner", false);
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },

  validateAccount: function (component, event, helper) {
    var action = component.get("c.getCurrentUser");
    var ctarget = event.currentTarget;
    var redirectURL = ctarget.dataset.value;
    action.setCallback(this, function (a) {
      var state = a.getState(); // get the response state
      if (state == "SUCCESS") {
        if (a.getReturnValue()) {
          let userObj = a.getReturnValue();
          var currentAccId = userObj.Contact.AccountId;
          var currentAccName = userObj.Contact.Account.Name;
          var initialAccId = component.get("v.initialAccId");
          console.log("currentAccId==>" + currentAccId);
          console.log("initialAccId==>" + initialAccId);
          if (initialAccId == currentAccId) {
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              url: redirectURL,
            });
            urlEvent.fire();
          } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
              title: "Error Message",
              message:
                "The account that you are managing has been changed to " +
                currentAccName +
                ". Please refresh the page to continue.",
              duration: " 5000",
              key: "info_alt",
              type: "error",
              mode: "pester",
            });
            toastEvent.fire();
          }
        }
      } else if (state == "FAILURE") {
        console.log("Some problem ocurred");
      }
    });
    $A.enqueueAction(action);
  },
});