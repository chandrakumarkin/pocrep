({
	onDataProtectionSave : function(cmp, event, helper) {
		var allValid = true;
      cmp.set("v.errorMessage", "");
      cmp.set("v.isError", false);
      
      let fields = cmp.find("dataProtectionkey");
      for (var i = 0; i < fields.length; i++) {
          var inputFieldCompValue = fields[i].get("v.value");
          //console.log(inputFieldCompValue);
          if(!inputFieldCompValue && fields[i] && fields[i].get("v.required")){
              console.log('no value');
              //console.log('bad');
              fields[i].reportValidity();
              allValid = false;
          }   
      }
      if (!allValid) {
          console.log("Error MSG");
          cmp.set("v.errorMessage", "Please fill required field");
          cmp.set("v.isError", true);
          helper.showToast(cmp,event,"Please fill required field",'error');
      } else {
    	console.log('######### onAmedSave ');
       try{
       cmp.set('v.spinner',true);
       var dataProtectObj = cmp.get('v.dataProtection.dataProtectObj');
       var isError = cmp.get("v.isError");
       
      
       
       console.log('===isError==='+isError);
       if(isError != true){
           
       console.log('-----no error---');
        var onAmedSave = cmp.get('c.onDataProtectionSaveDB');
        var srId = cmp.get('v.srId');
        var reqWrapPram  =
            {
                srId:srId,
                jurisdictionWrap: cmp.get('v.dataProtection'),
                srWrap:cmp.get('v.srWrap')
            };
        
        console.log('=====JSON.stringify(srId)===='+srId);
        onAmedSave.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
        
        onAmedSave.setCallback(this, 
        	function(response) {
            	var state = response.getState();
                console.log("callback state: " + state);
                                          
                if (cmp.isValid() && state === "SUCCESS"){
                    var respWrap = response.getReturnValue();
                    console.log('#########respWrap.jurisdictionWrap---1  ',respWrap.jurisdictionWrap);
                    var amedId = respWrap.jurisdictionWrap.dataProtectObj.Id;
                    
                    console.log('######### respWrap.jurisdictionWrap.dataProtectObj.id  ',amedId);
                    
                    if(!$A.util.isEmpty(respWrap.errorMessage) ){
                        console.log('=====error on sve===='+respWrap.errorMessage);
                        cmp.set('v.spinner',false);
                        
                        cmp.set("v.errorMessage",respWrap.errorMessage);
                        cmp.set("v.isError",true);
                        helper.showToast(cmp,event,respWrap.errorMessage,'error');
                    }else{
                        if(respWrap.jurisdictionWrap.dataProtectObj.Id != ''){
                        console.log('==inside===');
                        	cmp.set("v.dataProtection",null);
                            
                            var refeshEvnt = cmp.getEvent("refeshEvnt");
                            var srWrap = respWrap.srWrap;
                            console.log('@@@@@ srWrap ',srWrap);
                            
                            refeshEvnt.setParams({
                                "isViewRemove":"insert",
                                "srWrap" : srWrap });
                            cmp.set('v.spinner',false);
                            refeshEvnt.fire();
                        }
                    }                             
                }
                else
                {
                    console.log('@@@@@ Error '+response.getError()[0].message);
                    cmp.set('v.spinner',false);
                    cmp.set("v.errorMessage",response.getError()[0].message);
                    cmp.set("v.isError",true);
                    helper.showToast(cmp,event,response.getError()[0].message,'error');                        
                }
            }
        		);
            $A.enqueueAction(onAmedSave);
       }
       }catch(err) {
		  console.log('===error=='+ err.message);
          cmp.set('v.spinner',false);
          cmp.set("v.errorMessage",err.message);
          cmp.set("v.isError",true);
          helper.showToast(cmp,event,err.message,'error'); 
		}
      }
	},
    closeForm: function(cmp, event, helper){
        //throw event.
		
        cmp.set('v.spinner',true);
        
        var refeshEvnt = cmp.getEvent("refeshEvnt");
        refeshEvnt.setParams({
            "isViewRemove":"close"});
        cmp.set('v.spinner',false);
        refeshEvnt.fire();
        
    },
})