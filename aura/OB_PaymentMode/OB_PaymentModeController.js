({
    start : function(component, event, helper) {
        console.log('-test--');
    },
    doInit : function(component, event, helper) {

        //Disable the amount field
        let newURL = new URL(window.location.href).searchParams;
        var srID = newURL.get('srId');
        var amount = newURL.get("amountToPay");
        var category = newURL.get("category") ? newURL.get("category") : '';
        var priceItemCode = component.get("v.itemCode") ? component.get("v.itemCode") : newURL.get('itemCode');
        component.set("v.srId",srID);
        //If SRId parameter found, disable the amount field
        if(srID || priceItemCode){ //show credit card, purchase from wallet
            component.set("v.hideOtherModes",true); //hide cash,cheque and bank transfer
            component.set("v.amountDisabled",true);
            component.set("v.showWalletPanel","true");
            component.set("v.showWallet",true);
        }
        else{
            component.set("v.showCard",true); //show credit card, cash, cheque and bank transfter
        }
        
        //Return the querystring part of a URL starting from eg: ?page=233
        var queryString = location.search;    
        component.set("v.pageParameter",queryString);

        //helper.getSRLines(component, event, helper);
        console.log('---topup--ini---');
        /*
        console.log(priceItemCode);
        var priceItemCodeArr = [];
        priceItemCodeArr.push(priceItemCode);
        console.log(priceItemCodeArr);
        component.set("v.itemCode",priceItemCode);
        */
        var action = component.get('c.loadPriceItem');
        action.setParams({
            "srId":srID,
            "category":category,
            "isBalanceRequired" : true
        });
        action.setCallback(this, function(response){
            var state = response.getState(); // get the response state
            console.log('state'+state);
            if(state == 'SUCCESS') {
                var responseResult = response.getReturnValue();
                console.log(responseResult);
                component.set("v.availableBalance",responseResult.totalBalance);
                component.set("v.amount",responseResult.amount);
                component.set("v.amountInAED",responseResult.amountInAED);
                component.set("v.lineItems",responseResult.lstProductInfo);
            }
            else if (state === 'ERROR') { // Handle any error by reporting it
                var errors = response.getError();
                console.log(response.getError());
                
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayToast('Error', errors[0].message, 'error');
                    }
                } else {
                    helper.displayToast('Error', 'Unknown error.', 'error');
                }
            }
        });
        $A.enqueueAction(action);
        helper.getRecordTypeId(component, event, helper);
    },
    showAlert : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/topupcash"+component.get("v.pageParameter"),
        });
        urlEvent.fire();
        // component.set("v.isOpenCash",true);
        // component.set("v.amountDisabled",true);
    },
    showBankTransfer : function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": "/topupwalletbank"+component.get("v.pageParameter"),
        });
        urlEvent.fire();
        // component.set("v.isOpen",true);
        // component.set("v.amountDisabled",true);
    },
    
    enableAmount : function(component, event, helper) {
        component.set("v.showWallet","false");
        component.set("v.showCard","true");
        var elemId = event.currentTarget.dataset.id;
        console.log(component.find(elemId));
        console.log(document.getElementById(event.currentTarget.dataset.id));
        $A.util.addClass(component.find(elemId), 'ob-act-active');
        //component.set("v.amountDisabled",false);
    },
    openWallet:function(component,event,helper){
        component.set("v.showWallet","true");
        component.set("v.showCard","false");
        console.log('openWallet');
        var elemId = event.currentTarget.dataset.id;
        $A.util.addClass(component.find(elemId), 'ob-act-active');
        var action = component.get('c.getTopUpBalanceAmount');
        console.log('set param');
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            console.log(state);
            if (state === "SUCCESS") {
                var responseResult = response.getReturnValue();
                component.set("v.availableBalance",responseResult.portalBalanceValue);
            }
            else if (state === 'ERROR') { // Handle any error by reporting it
                var errors = response.getError();
                console.log(response.getError());
                
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayToast('Error', errors[0].message, 'error');
                    }
                } else {
                    helper.displayToast('Error', 'Unknown error.', 'error');
                }
            }
        });
        $A.enqueueAction(action);
    },
    enableCardPayment : function(component, event, helper) {
        component.set("v.isOpenCard",true);
    },
    
    navigateToCheckout : function(component, event, helper) {
        console.log('navigate');
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');
        var category = newURL.get("category") ? newURL.get("category") : '';
        var amountEnteredByUser = component.get("v.amountInAED");
        if(amountEnteredByUser > 0){
           //alert('Payment Redirection');
            //var url = '/'+$A.get("$Label.c.OB_Site_Prefix")+'/s/difc-checkouttopupbalance?amountToPay='+ amountEnteredByUser+'&srId='+srId+'&category='+category;
           
            var url = '/customers/NewPayment?amountToPay='+ amountEnteredByUser+'&srId='+srId+'&category='+category;
            console.log(url);
            window.location.href=url;
        } 
        else{ 
            helper.displayToast('Error', 'Please enter a valid amount', 'error');
        }
    },
    payWallet:function(component,event,helper){
        
        var availBalance = component.get("v.availableBalance");
        var amount = component.get("v.amount");
        if(availBalance >= amount){
            let newURL = new URL(window.location.href).searchParams;
            var srID = newURL.get('srId');
            var category = newURL.get("category") ? newURL.get("category") : '';
            var action = component.get('c.updatePriceItem');
            action.setParams({
                "srId":srID,
                "category":category
            });
            action.setCallback(this, function(response) {
                //store state of response
                var resp = response.getReturnValue();
                var state = response.getState();
                console.log(state);
                if (state === "SUCCESS") {
                    if(resp.isError){
                        helper.displayToast('Error', resp.errorMessage, 'error');
                    }
                    else{
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": '/paymentwalletconfirmation?srId='+srID+'&category='+category,
                        });
                        urlEvent.fire();
                    }
                }
                else if (state === 'ERROR') { // Handle any error by reporting it
                    var errors = response.getError();
                    console.log(response.getError());
                    
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            helper.displayToast('Error', errors[0].message, 'error');
                        }
                    } else {
                        helper.displayToast('Error', 'Unknown error.', 'error');
                    }
                }
            });
            $A.enqueueAction(action);
        }
        else{
            console.log('error');
            helper.displayToast('Error', 'Please top up your wallet', 'error');
        }
    },
    
    goToReceiptPage : function(component, event, helper) {
        let newURL = new URL(window.location.href).searchParams;
        var srID = newURL.get('srId');
        var category = newURL.get("category") ? newURL.get("category") : '';
        console.log('goToReceiptPage');
        var amountEntered = component.get("v.amount");//component.find("totalToPay").get("v.value");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": '/topupcheque?srId='+srID+'&category='+category
        });
        urlEvent.fire();
        /*
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({ 
            "entityApiName": "Receipt__c",
            "recordTypeId" : component.get("v.chequeRecordType"),
            'defaultFieldValues': {
                    'Amount__c' : amountEntered,
                	'Receipt_Type__c' : 'Cheque',
                	'Payment_Status__c' : 'Pending',
                	
                }
        });
        createRecordEvent.fire();
        */
    },
    handleKeyPress:function(component, event, helper) {
        var amountEntered = component.find("totalToPay").get("v.value");
        if(isNaN(amountEntered)){
            return false;
        }
    },
    onchangeAmountEntered:function(component, event, helper) {
        var amountEntered = component.find("totalToPay").get("v.value");
        
            component.set("v.amountInAED",amountEntered);
        //console.log($A.util.isNumber(amountEntered));
        //component.set("v.amount",amountEntered);
        
        console.log('amountEntered---'+amountEntered);
        /*
        var amtevt = $A.get("e.c:DIFC_NavigateWithinComponent");
        amtevt.setParams({
            "amount" : amountEntered
        });
        amtevt.fire();  
        */
       
    },
    closeModel: function(component, event, helper) { 
        component.set("v.isOpen", false);
        component.set("v.isOpenCash", false);
        component.set("v.isOpenCard", false);
    },
})