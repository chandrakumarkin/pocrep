({
	hlpLaunchComponent: function(cmp,event){
        var navService = cmp.find("launchComponent");
        console.log('hi'+navService);  
        console.log(cmp.find("totalToPay").get("v.value")); 
        var pageReference = {
            "type": "standard__component",
            "attributes": {
                "componentName": "c__DIFC_Checkout"               
            },
            "state": {
               // "c__amount" : cmp.find("totalToPay").get("v.value")
            }
        };
               
        console.log('navTo: '+ pageReference);
        event.preventDefault();
        navService.navigate(pageReference);
    },
    /**
     * Display a message
     */
    displayToast : function (title, message, type) {
		console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'type': type,
                'title': title,
                'message': message
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
	},
    getRecordTypeId : function(component, event, helper){
    	var action = component.get('c.getRecTypeId');
        action.setParams({
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            console.log('state'+state);
            if(state == 'SUCCESS') {
                console.log('rtype'+a.getReturnValue());
                component.set('v.chequeRecordType', a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
    
    getSRLines : function(component, event, helper){
        let newURL = new URL(window.location.href).searchParams;
        var srID = newURL.get('srId');
    	var action = component.get('c.getSRLinesAmount');
        action.setParams({
            "SRID" : srID 
        });
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            console.log('state'+state);
            if(state == 'SUCCESS') {
                console.log('rtype'+a.getReturnValue());
                var v = JSON.parse(a.getReturnValue());
                component.set('v.lineItems', v);
                console.log(v);
                console.log(v.length);
                if(v.length > 0){
                    var totAmt = 0;
                    for(var i = 0 ; i< v.length; i ++){
                        console.log(v[i].price);
                        totAmt += v[i].price;
                    }
                    component.set('v.amount', totAmt);
                    component.set('v.IsamountAvailable', true);
                }
               /* if(a.getReturnValue() > 0 ){
                	component.set('v.amount', a.getReturnValue());
                    component.set('v.IsamountAvailable', true);
                } */
               // component.set('v.chequeRecordType', a.getReturnValue());
            }
        });
        $A.enqueueAction(action);
	},
})