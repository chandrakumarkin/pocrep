({
	sendCommunication : function(component, event, helper) {
		var action = component.get('c.sendSampleCommunication');
        action.setParams({
            emailId : component.get("v.inputEmail"),
            phoneNumber : component.get("v.inputPhone"),
            recordId : component.get("v.recordId")
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('### state '+state);
            var responseValue = response.getReturnValue();
                    console.log('responseValue '+responseValue["Success"]);
                    console.log('responseValue Error '+responseValue['Error']);
            if(state === "SUCCESS"){
                if(responseValue != null && responseValue["Success"] != null){
                    helper.showToast(component,"Success!",responseValue["Success"],"Success");
                }
                else if(responseValue != null && responseValue["Error"] != null){
                    helper.showToast(component,"Error!",responseValue["Error"],"Error");
                } 
                else{
                    helper.showToast(component,"Success!","Communication sent successfully","Success");
                } 
                /*var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
        			"title": "Success!",
        			"message": "Communication has been sent successfully",
                    "type": "Success"
    			});
    			toastEvent.fire();*/                                
            }
            else{
                helper.showToast(component,"Error!","Internal Error Occured. Please contact System Administrator","Error");
                /*var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
        			"title": "Error!",
        			"message": "Internal Error Occured. Please contact System Administrator",
                    "type": "Error"
    			});
    			toastEvent.fire();*/
            }
            $A.get("e.force:closeQuickAction").fire() ;
        });
        $A.enqueueAction(action);
	},
    
    CancelModal : function(component,event,helper){
         $A.get("e.force:closeQuickAction").fire() ;
    },
	
    handleDisableAction : function(component,event,helper){
        var disableProperty = false;
        let inpEmail = component.get("v.inputEmail");
        let inpPhone = component.get("v.inputPhone");
        if((inpEmail != null && inpEmail.length > 0) || (inpPhone != null && inpPhone.length > 0)){
        	disableProperty = false;
        }
        else{
            disableProperty = true;
        }
        console.log('### disableProperty '+disableProperty);
        component.set("v.disableSend",disableProperty);
    }
})