({
    doInit : function(cmp, event, helper) 
    {
        
       
    },
    onAmedSave : function(cmp, event, helper) 
    {
		
        var allValid = true;
        
        let fields = cmp.find("othrLockey"); 
        console.log('@@@@@@@@ fields ',fields.length);
        for (var i = 0; i < fields.length; i++) 
        {
            var inputFieldCompValue = fields[i].get("v.value");
            //console.log(inputFieldCompValue);
            if(!inputFieldCompValue && fields[i] && fields[i].get("v.required")){
                console.log('no value');
                //console.log('bad');
                fields[i].reportValidity();
                allValid = false;
            }   
        }
        
        console.log('@@@@@@@@ allValid ',allValid);
        
        if (allValid == false) 
        {
            console.log("Error MSG");
            // pop up
            helper.showToast(cmp, event, helper,'Please fill all fields.', 'error');
           
        }
        else
        {
            // For declaration.
            /*
            var iAgreeIndividual =  cmp.get("v.amedWrap.amedObj.I_Agree_Reg_Add__c");
            if($A.util.isEmpty(iAgreeIndividual) || $A.util.isUndefined(iAgreeIndividual) || iAgreeIndividual==false)
            {
               alert('Please accept the declaration');
               return;
            }
        	*/
        
            var onAmedSave = cmp.get('c.onAmedSaveDB');
            // amedWrap.amedObj.First_Name__c
            var amedWrapfrt = cmp.get('v.amedWrap').amedObj.First_Name__c;
            console.log('######### amedWrapfrt  ',amedWrapfrt);
            var reqWrapPram  =
                    {
                        amedWrap: cmp.get('v.amedWrap')  
                    };
        
            onAmedSave.setParams(
                {
                    "reqWrapPram": JSON.stringify(reqWrapPram)
                });
        
            onAmedSave.setCallback(this, 
                                          function(response) {
                                              var state = response.getState();
                                              console.log("callback state: " + state);
                                              
                                              if (cmp.isValid() && state === "SUCCESS") 
                                              {
                                                  var respWrap = response.getReturnValue();
                                                     
                                                  // amedObj
                                                  if(respWrap.amedWrap && respWrap.amedWrap.amedObj.Id != '' )
                                                  {
                                                      console.log('######### respWrap.amedWrap.amedObj.id  ',respWrap.amedWrap.amedObj.Id);
                                                      
                                                      var amedId = respWrap.amedWrap.amedObj.Id ;
                                                      cmp.set('v.attachmentParentId',amedId);
                                                      //For attachments.
                                                      var refeshEvnt = cmp.getEvent("refeshEvnt");
                                                       refeshEvnt.setParams({
                                                                            "amedId" : respWrap.amedWrap.amedObj.id 
                                                                            });
                                                        refeshEvnt.fire();
                                                   }
                                                   else
                                                   {
                                                       alert(respWrap.errorMessage);
                                                   }
                                                  
                                              }
                                              else
                                              {
                                                  console.log('@@@@@ Error '+response.getError()[0].message);
                                                 
                                              }
                                          }
                                         );
                    $A.enqueueAction(onAmedSave);
        }
        
        
        
	},
	handleLookupEvent: function (component, event, helper) 
    {
        console.log('handleLookupEvent');
        var accObj = event.getParam("sObject"); 
        console.log(JSON.parse(JSON.stringify(accObj)));
        component.set("v.amedWrap.amedObj.Registered_No__c",accObj.Registration_License_No__c);
       
    },
    clearLookupEvent : function (component, event, helper) 
    {
        
    },
	handleUploadFinished: function (cmp, event) {
        // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        alert("Files uploaded : " + uploadedFiles.length);
    },
    onCancel : function (cmp, event, helper) 
    {
        var refeshEvnt = cmp.getEvent("refeshEvnt");
        refeshEvnt.setParams({
            "amedId" : 'Cancel'
        });
        refeshEvnt.fire();
        
        
    },
	
	
    
})