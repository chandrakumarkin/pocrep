({
	
    onChangeRegHeldBy: function(cmp, event, helper)
    {
        var currentValue = event.getSource().get('v.value');
        console.log('$$$$$$$$$ currentValue ',currentValue );
        //Individual //  Body Corporate
        var amedtype;
        if(currentValue == 'Individual')
        {
            amedtype ='Individual';
        }
        else if(currentValue == 'Body Corporate')
        {
            amedtype ='Body_Corporate';
        }

        console.log('######### amedtype ',amedtype);
        var initAmendment = cmp.get('c.initAmendmentDB');
        
        var reqWrapPram  =
                {
                    srId : cmp.get('v.srWrap').srObj.Id,
                amedtype : amedtype
                };
        
        initAmendment.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
        
        initAmendment.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                              console.log('######### respWrap.amedWrap  ',respWrap.amedWrap);
											  cmp.set('v.amedWrap',respWrap.amedWrap);	  
                                              cmp.set('v.amedId','');	  
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                             
                                          }
                                      }
                                     );
            $A.enqueueAction(initAmendment);
        
        
    },
    fireRegAddOthrLocEvnt : function(cmp, event) 
    {
          	var srWrapOthrLoc = cmp.get('v.srWrap');
        	console.log('@@@@@@@ firing srWrapOthrLoc ',srWrapOthrLoc);
            var appEvent = $A.get("e.c:OB_RegAddOthrLocEvnt");
            appEvent.setParams
            ({
                "srWrapOthrLoc" : srWrapOthrLoc
            });
            appEvent.fire();
        
    },
    
    
})