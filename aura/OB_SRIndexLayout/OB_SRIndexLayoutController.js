({
  doInit: function(component, event, helper) {
    //helper.openModal(component, event, helper);

    var userAction = component.get("c.getUserInfo");
    console.log(userAction);
    userAction.setCallback(this, function(a) {
      var state = a.getState();
      console.log(a);
      if (state === "SUCCESS") {
        var storeResponse = a.getReturnValue();
        console.log(storeResponse);
        // set current user information on userInfo attribute
        component.set("v.userName", storeResponse.Contact.Account.Name);
        component.set("v.blockTab", storeResponse.Contact.OB_Block_Homepage__c);
        component.set("v.loggedInUserInfo", storeResponse);
      }
    });
    $A.enqueueAction(userAction);
  },
  displayService: function(component, event, helper) {
    component.set("v.enableServices", true);
    component.set("v.hometab", false);
    component.set("v.costTab", false);
    component.set("v.ApprovedTab", false);
    component.set("v.RequestTab", false);
    debugger;

    var elements = document.getElementsByClassName("blue-borderBottom");
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove("blue-borderBottom");
    }

    event.currentTarget.className = "slds-context-bar__item blue-borderBottom";
    console.log(JSON.stringify(component.get("v.column3")));
    if (JSON.stringify(component.get("v.column3")) === "[]") {
      console.log("inside if");
      $A.createComponent("c:OB_Services", {}, function(
        childCom,
        status,
        errorMessage
      ) {
        if (status === "SUCCESS") {
          try {
            var body = component.get("v.column3");
            //console.log('$$$$$$$$$ ',JSON.stringify(body));
            body.push(childCom);
            component.set("v.column3", body);
            console.log("service comp created!!");
          } catch (err) {
            console.log(err.message);
          }
        } else if (status === "INCOMPLETE") {
          console.log("No response from server or client is offline.");
          // Show offline error
        } else if (status === "ERROR") {
          console.log("Error: " + errorMessage);
          // Show error message
        }
      });
    }
    /*console.log('fire event--');
        var appEvent = $A.get("e.c:aeEvent");
        appEvent.setParams({
            "serviceTab" : true });
        appEvent.fire();*/
  },
  displayCost: function(component, event, helper) {
    console.log("in cost tab");
    component.set("v.costTab", true);
    component.set("v.enableServices", false);
    component.set("v.hometab", false);
    component.set("v.ApprovedTab", false);
    component.set("v.RequestTab", false);

    var elements = document.getElementsByClassName("blue-borderBottom");
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove("blue-borderBottom");
    }

    event.currentTarget.className = "slds-context-bar__item blue-borderBottom";
    console.log(JSON.stringify(component.get("v.column4")));
    if (JSON.stringify(component.get("v.column4")) === "[]") {
      console.log("inside if");
      $A.createComponent("c:OB_CostTab", {}, function(
        childCom,
        status,
        errorMessage
      ) {
        if (status === "SUCCESS") {
          try {
            var body = component.get("v.column4");
            //console.log('$$$$$$$$$ ',JSON.stringify(body));
            body.push(childCom);
            component.set("v.column4", body);
            console.log("comp created");
          } catch (err) {
            console.log(err.message);
          }
        } else if (status === "INCOMPLETE") {
          console.log("No response from server or client is offline.");
          // Show offline error
        } else if (status === "ERROR") {
          console.log("Error: " + errorMessage);
          // Show error message
        }
      });
    }
  },
  displayApproved: function(component, event, helper) {
    component.set("v.ApprovedTab", true);
    component.set("v.costTab", false);
    component.set("v.enableServices", false);
    component.set("v.hometab", false);
    component.set("v.RequestTab", false);

    var elements = document.getElementsByClassName("blue-borderBottom");
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove("blue-borderBottom");
    }

    event.currentTarget.className = "slds-context-bar__item blue-borderBottom";
    console.log(JSON.stringify(component.get("v.column5")));
    if (JSON.stringify(component.get("v.column5")) === "[]") {
      console.log("inside if");
      $A.createComponent("c:OB_ApprovedTab", {}, function(
        childCom,
        status,
        errorMessage
      ) {
        if (status === "SUCCESS") {
          try {
            var body = component.get("v.column5");
            //console.log('$$$$$$$$$ ',JSON.stringify(body));
            body.push(childCom);
            component.set("v.column5", body);
            console.log("comp created");
          } catch (err) {
            console.log(err.message);
          }
        } else if (status === "INCOMPLETE") {
          console.log("No response from server or client is offline.");
          // Show offline error
        } else if (status === "ERROR") {
          console.log("Error: " + errorMessage);
          // Show error message
        }
      });
    }
  },
  displayHome: function(component, event, helper) {
    component.set("v.enableServices", false);
    component.set("v.hometab", true);
    component.set("v.costTab", false);
    component.set("v.ApprovedTab", false);
    component.set("v.RequestTab", false);
    var elements = document.getElementsByClassName("blue-borderBottom");
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove("blue-borderBottom");
    }
    event.currentTarget.className = "slds-context-bar__item blue-borderBottom";
  },

  displayRequest: function(component, event, helper) {
    console.log("handler");
    debugger;
    component.set("v.enableServices", false);
    component.set("v.hometab", false);
    component.set("v.costTab", false);
    component.set("v.ApprovedTab", false);
    component.set("v.RequestTab", true);
    var elements = document.getElementsByClassName("blue-borderBottom");
    for (var i = 0; i < elements.length; i++) {
      elements[i].classList.remove("blue-borderBottom");
    }
    event.currentTarget.className = "slds-context-bar__item blue-borderBottom";

    /*var appEvent = $A.get("e.c:aeEvent");
          appEvent.setParams({
              "requestTab" : true });
          appEvent.fire();*/
    console.log(JSON.stringify(component.get("v.column6")));
    if (JSON.stringify(component.get("v.column6")) === "[]") {
      console.log("inside if");
      $A.createComponent(
        "c:OB_RequestedServicesList",
        {
          statusValue: "In Progress",
          headerSectionDisplay: true,
          searchSectionDisplay: true,
          inProgressHeaderDisplay: false
        },
        function(childCom, status, errorMessage) {
          if (status === "SUCCESS") {
            try {
              var body = component.get("v.column6");
              //console.log('$$$$$$$$$ ',JSON.stringify(body));
              body.push(childCom);
              component.set("v.column6", body);
              console.log("request comp created!!");
            } catch (err) {
              console.log(err.message);
            }
          } else if (status === "INCOMPLETE") {
            console.log("No response from server or client is offline.");
            // Show offline error
          } else if (status === "ERROR") {
            console.log("Error: " + errorMessage);
            // Show error message
          }
        }
      );
    }
  }
});