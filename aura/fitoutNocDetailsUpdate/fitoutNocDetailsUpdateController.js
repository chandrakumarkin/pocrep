({
	doInit : function(component, event, helper) {
        console.log(component.get("v.stepidValue"));
        var action = component.get("c.getRequestData");
        action.setParams(
        {
            stepId : component.get("v.stepidValue")
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if (state === "SUCCESS"){
                component.set("v.serviceRequest", response.getReturnValue()); 
            }
            else 
            {
                console.log("Failed with state: " + state);
            }
        });
        //send action off to be executed
        $A.enqueueAction(action);
    },
    close : function(component, event, helper) {
         self.close();
    },
    save : function(component, event, helper) {
        var action = component.get("c.saveNOCDetails");
        action.setParams(
        {
            serviceRequestRec : component.get("v.serviceRequest")
        });
        action.setCallback(this,function(response)
        {
            var state = response.getState();
            if (state === "SUCCESS"){
                component.set("v.serviceRequest", response.getReturnValue()); 
                self.close();
            }
            else 
            {
                console.log("Failed with state: " + state);
            }
        });
        //send action off to be executed
        $A.enqueueAction(action);
    },
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   }
})