({
  doInit: function (component, event, helper) {},

  RemoveAction: function (component, event, helper) {
    console.log("IN delete IP");
    try {
      var action = component.get("c.amendRemoveAction");
      var requestWrap = {
        amendObj: component.get("v.amendObj"),
        isAssigned: component.get("v.isAssigned"),
        srId: component.get("v.srId"),
        roleToRemove: component.get("v.roleToRemove"),
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log(response.getReturnValue());
          console.log("IN delete");
          if (response.getReturnValue().errorMessage == "no error recorded") {
            /* component.set(
              "v.AmendListWrapp",
              response.getReturnValue().amendWrapp
            ); */
            component.set("v.respWrap", response.getReturnValue());
            /* var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        mode : "dismissable",
                        message : "updated Succsessfully",
                        type : "success",
                        duration : 500
                    });
                    toastEvent.fire(); */
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },

  viewDetails: function (component, event, helper) {
    component.set("v.amedIdInFocus", component.get("v.amendObjId"));
    console.log(component.get("v.amedIdInFocus"));
  },
});