({
    checkPassword : function(component) {
        
        var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/
        // var charRegex = /^(?=.{10,}$)/
        var charRegex= /^(?=.{8,})/
        var pass = component.find("password").get("v.value");
        var cnfPass =  component.find("verifyPassword").get("v.value");
        var cmpDiv = component.find('8Char');
        console.log('pass============' + pass );
        
        // var pass = 'aAaaa1a[aa';
        console.log('fail: ' + regex.test(pass));
        /*if (charRegex.test(pass)) {
            console.log("password should contain atleast one number and one special character");
            //component.set("v.isMatched", true);
            $A.util.addClass(cmpDiv, 'difc-visible');
            
        }
        else
        {
            $A.util.addClass(cmpDiv, 'difc-hidden');
        }*/
        
        
    },
    
    checkChar :  function(component)
    {
        var pass = component.find("password").get("v.value");
        var charRegex = /^(?=.{8,})/
        var cmpDiv = component.find('8Char');
        console.log('pass============' + pass );
        
        console.log('fail: ' + charRegex.test(pass));
        if( $A.util.isEmpty(pass) && $A.util.isUndefined(pass)){
            $A.util.removeClass(cmpDiv, 'difc-visible');
        }
        else if (charRegex.test(pass)) {
            $A.util.addClass(cmpDiv, 'difc-visible');
            
        }
            else
            {
                $A.util.removeClass(cmpDiv, 'difc-visible');
            }
    },
    
    Checklowercase : function(component)
    {
        var pass = component.find("password").get("v.value");
        var lowercaseRegex = /(?=.*[a-z])/
        var cmpDiv = component.find('lowercase');
        if( $A.util.isEmpty(pass) && $A.util.isUndefined(pass)){
            $A.util.removeClass(cmpDiv, 'difc-visible');
        }
        else if (lowercaseRegex.test(pass)) {
            console.log("password lower case");
            $A.util.addClass(cmpDiv, 'difc-visible');
        }
            else
            {
                $A.util.removeClass(cmpDiv, 'difc-visible');
            }
        
        
    },
    Checkuppercase : function(component)
    {
        var pass = component.find("password").get("v.value");
        var uppercaseRegex = /(?=.*[A-Z])/
        var cmpDiv = component.find('uppercase');
        if( $A.util.isEmpty(pass) && $A.util.isUndefined(pass)){
            $A.util.removeClass(cmpDiv, 'difc-visible');
        }
        else if (uppercaseRegex.test(pass)) {
            $A.util.addClass(cmpDiv, 'difc-visible');
        }
            else
            {
                $A.util.removeClass(cmpDiv, 'difc-visible');
            }
    },
    Checknumber : function(component)
    {
        var pass = component.find("password").get("v.value");
        var numberRegex = /(?=.*[0-9])/
        var cmpDiv = component.find('numbercheck');
        if( $A.util.isEmpty(pass) && $A.util.isUndefined(pass)){
            $A.util.removeClass(cmpDiv, 'difc-visible');
        }
        else if (numberRegex.test(pass)) {
            $A.util.addClass(cmpDiv, 'difc-visible');
        }
            else
            {
                $A.util.removeClass(cmpDiv, 'difc-visible');
            }
    },
    checkSpecialChar: function(component){
        var pass = component.find("password").get("v.value");
        var specialCharRegex = /(?=.*\W)/
        var cmpDiv = component.find('specialChar');
        if( $A.util.isEmpty(pass) && $A.util.isUndefined(pass)){
            $A.util.removeClass(cmpDiv, 'difc-visible');
        }
        else if (specialCharRegex.test(pass)) {
            $A.util.addClass(cmpDiv, 'difc-visible');
        }
            else
            {
                $A.util.removeClass(cmpDiv, 'difc-visible');
            }
    },
    matchPassword : function(component)
    {
        var validRecord = false;
        var pass = component.find("password").get("v.value");
        var cnfPass =  component.find("verifyPassword").get("v.value");
        if( ($A.util.isEmpty(pass) && $A.util.isUndefined(pass)) || ($A.util.isEmpty(cnfPass) && $A.util.isUndefined(cnfPass)))
        {
            component.set("v.passwordMatch",'Donot Match');
        }
        else if ((pass == cnfPass) && !($A.util.isEmpty(pass) && !$A.util.isUndefined(pass))) {
            console.log('match');
            component.set("v.passwordMatch",'Match');
            validRecord = true;
        }
        
            else{
                console.log('dont match');
                validRecord = false;
                
                component.set("v.passwordMatch",'Donot Match');
            }
        console.log('=============validRecord=========' + validRecord);
        return validRecord;
    }
})