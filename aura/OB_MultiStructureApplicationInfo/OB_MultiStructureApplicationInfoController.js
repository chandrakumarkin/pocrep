({
  doInit: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var pageflowId = newURL.get("flowId");
    var pageId = newURL.get("pageId");
    var srId = newURL.get("srId");
    component.set("v.entityRec", null);
    var action = component.get("c.getCurrentEntity");
    console.log("currentSrId " + component.get("v.currentSrId"));
    action.setParams({
      srId: srId,
      currentSrId: component.get("v.currentSrId")
    });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var entityRec = response.getReturnValue();
        console.log("entityRec==>" + JSON.stringify(entityRec));
        component.set("v.entityRec", entityRec);
      } else {
        helper.displayToast("Error", response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },
  handleSubmit: function (component, event, helper) {
    component.set("v.showSpinner", true);
    let newURL = new URL(window.location.href).searchParams;
    var pageflowId = newURL.get("flowId");
    var pageId = newURL.get("pageId");
    var srId = newURL.get("srId");
    console.log("entityRec==>" + JSON.stringify(component.get("v.entityRec")));
    var action = component.get("c.saveEntityRecord");
    action.setParams({
      entityRec: component.get("v.entityRec"),
      srId: srId
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        var insertStatus = response.getReturnValue();
        if (insertStatus.Id != null) {
          component.set("v.showSpinner", false);
          component.set("v.entityRec", insertStatus);
          component.set("v.currentSrId", insertStatus.Id);
        } else {
          component.set("v.showSpinner", false);
          var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
            mode: "dismissable",
            message: insertStatus,
            type: "error",
            duration: 500
          });
          toastEvent.fire();
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  }
});