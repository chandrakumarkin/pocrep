({
    
    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB 
    CHUNK_SIZE: 750000,      //Chunk Max size 750Kb 

    
    getInitiateMonthlySalesView: function(component){
        var action = component.get("c.initiateMonthlySalesView");
        //let currentUser = $A.get("$SObjectType.CurrentUser.Id");
        console.log(component.get("v.userIdValue"));
        var currentDateValue = new Date();
        var currentYearValue = currentDateValue.getFullYear();
        component.set("v.selectedYearValue", currentYearValue);
        
        action.setParams({
            currentLoggedInUser : component.get("v.userIdValue"),
            selectedYearValue : currentYearValue,
            customerLeaseMapVal : component.get("v.customerLeaseMap")
            });
        action.setCallback(this,function(response){
        var state = response.getState();
        if (state === "SUCCESS"){
            component.set("v.customerLeases", response.getReturnValue().customerLeases);  
            console.log(component.get("v.customerLeases"));
            component.set("v.customerLeaseMap", response.getReturnValue().customerleaseMap);
            component.set("v.salesWrapperDataList", response.getReturnValue());
            component.set("v.salesReportList", response.getReturnValue().serviceRequests);
            component.set("v.monthsMap", response.getReturnValue().calanderMonths);
            console.log('Mudasir');
            console.log(component.get("v.monthsMap"));
            component.set("v.yearOptionList", response.getReturnValue().yearOptions);
            component.set("v.monthOptionList", response.getReturnValue().unsubmittedMonths);
            component.set("v.newServiceRequest", response.getReturnValue().createNewServiceRequest);  
            console.log(component.get("v.monthsMap"));
            component.set("v.selectedLeaseNumber", response.getReturnValue().leaseNumberId);
            component.set("v.selectedYearValue", response.getReturnValue().selectedYear);
        }
        else 
        {
            console.log("Failed with state: " + state);
        }
        });
        //send action off to be executed
            $A.enqueueAction(action);
    },
    getDataReleventToThisLease: function(component){
        var action = component.get("c.getMonthlySalesViewForLease");
        console.log(component.get("v.userIdValue"));        
        var currentYearValue  = component.get("v.selectedYearValue");
        console.log('currentYearValue---'+currentYearValue); 
        console.log('leaseId---'+component.get("v.selectedLeaseNumber")); 
        component.set("v.selectedYearValue", currentYearValue);
        action.setParams({
            currentLoggedInUser : component.get("v.userIdValue"),
            selectedYearValue   : currentYearValue,
            leaseId             : component.get("v.selectedLeaseNumber"),
            customerLeaseMapVal : component.get("v.customerLeaseMap")
            });
        action.setCallback(this,function(response){
        var state = response.getState();
        if (state === "SUCCESS"){
            component.set("v.customerLeases", response.getReturnValue().customerLeases); 
            component.set("v.customerLeaseMap", response.getReturnValue().customerleaseMap); 
            console.log(component.get("v.customerLeases"));
            component.set("v.salesWrapperDataList", response.getReturnValue());
            component.set("v.salesReportList", response.getReturnValue().serviceRequests);
            component.set("v.monthsMap", response.getReturnValue().calanderMonths);
            console.log('Mudasir');
            console.log(component.get("v.monthsMap"));
            component.set("v.yearOptionList", response.getReturnValue().yearOptions);
            component.set("v.monthOptionList", response.getReturnValue().unsubmittedMonths);
            component.set("v.newServiceRequest", response.getReturnValue().createNewServiceRequest);  
            console.log(component.get("v.monthsMap"));
            component.set("v.selectedLeaseNumber", response.getReturnValue().leaseNumberId);
            component.set("v.selectedYearValue", response.getReturnValue().selectedYear);
        }
        else {
            console.log("Failed with state: " + state);
             }
        });
        //send action off to be executed
            $A.enqueueAction(action);
    },
    getSalesDataFromServer: function(component){
        var action = component.get("c.getMonthlySalesWrapperData");
        var selectedLeaseID  = component.find("selectedLeaseID").get("v.value");
        //let currentUser = $A.get("$SObjectType.CurrentUser.Id");
        console.log(component.get("v.userIdValue"));
        var currentDateValue = new Date();
        var currentYearValue = currentDateValue.getFullYear();
        component.set("v.selectedYearValue", currentYearValue);
        //alert('---currentYearValue---'+currentYearValue+'---customerIdVal---'+component.get("v.userIdValue"));
        action.setParams({
            currentLoggedInUser : component.get("v.userIdValue"),
            selectedYearValue : currentYearValue,
            LeaseId           : selectedLeaseID,
            customerLeaseMapVal : component.get("v.customerLeaseMap")
            });
        action.setCallback(this,function(response){
        var state = response.getState();
        if (state === "SUCCESS"){
            component.set("v.customerLeases", response.getReturnValue().customerLeases); 
            component.set("v.customerLeaseMap", response.getReturnValue().customerleaseMap);
            component.set("v.salesWrapperDataList", response.getReturnValue());
            component.set("v.salesReportList", response.getReturnValue().serviceRequests);
            component.set("v.monthsMap", response.getReturnValue().calanderMonths);
            console.log('Mudasir');
            console.log(component.get("v.monthsMap"));
            component.set("v.yearOptionList", response.getReturnValue().yearOptions);
            component.set("v.monthOptionList", response.getReturnValue().unsubmittedMonths);
            component.set("v.newServiceRequest", response.getReturnValue().createNewServiceRequest);  
            console.log(component.get("v.monthsMap"));
        }
        else {
            console.log("Failed with state: " + state);
             }
        });
        //send action off to be executed
            $A.enqueueAction(action);
    },
    getDataReleventToThisYear: function(component){
        var action = component.get("c.getMonthlySalesWrapperData");
        console.log(component.get("v.userIdValue"));
        var currentYearValue  = component.find("currentYearID").get("v.value");
        component.set("v.selectedYearValue", currentYearValue);
        var selectedLeaseID  = component.find("selectedLeaseID").get("v.value");        
        component.set("v.selectedLeaseNumber", selectedLeaseID);  
        //alert(component.get("v.selectedLeaseNumber")); 
        action.setParams({
            currentLoggedInUser : component.get("v.userIdValue"),
            selectedYearValue : currentYearValue,
            leaseId : component.get("v.selectedLeaseNumber")
            });
        action.setCallback(this,function(response){
        var state = response.getState();
        if (state === "SUCCESS"){
            component.set("v.customerLeases", response.getReturnValue().customerLeases); 
            component.set("v.customerLeaseMap", response.getReturnValue().customerleaseMap);
            component.set("v.salesWrapperDataList", response.getReturnValue());
            component.set("v.salesReportList", response.getReturnValue().serviceRequests);
            component.set("v.monthsMap", response.getReturnValue().calanderMonths);
            component.set("v.yearOptionList", response.getReturnValue().yearOptions);
            component.set("v.monthOptionList", response.getReturnValue().unsubmittedMonths);
            component.set("v.newServiceRequest", response.getReturnValue().createNewServiceRequest);            
        }
        else {
            console.log("Failed with state: " + state);
             }
        });
        //send action off to be executed
            $A.enqueueAction(action);
    },
    uploadHelper: function(component, event) {
            // get the selected files using aura:id [return array of files]
            var fileInput = component.find("fuploader").get("v.files");
            // get the first file using array index[0]  
            var file = fileInput[0];
            var self = this;
            // check the selected file size, if select file size greter then MAX_FILE_SIZE,
            // then show a alert msg to user,hide the loading spinner and return from function  
            if (file.size > self.MAX_FILE_SIZE) {
                component.set("v.fileName", 'Alert : File size cannot exceed ' + self.MAX_FILE_SIZE + ' bytes.\n' + ' Selected file size: ' + file.size);
                return;
            }
             
            // create a FileReader object 
            var objFileReader = new FileReader();
            // set onload function of FileReader object   
            objFileReader.onload = $A.getCallback(function() {
                var fileContents = objFileReader.result;
                var base64 = 'base64,';
                var dataStart = fileContents.indexOf(base64) + base64.length;
                 
                fileContents = fileContents.substring(dataStart);
                // call the uploadProcess method 
                self.uploadProcess(component, file, fileContents);
            });
             
            objFileReader.readAsDataURL(file);
        },
         
        uploadProcess: function(component, file, fileContents) {
            // set a default size or startpostiton as 0 
            var startPosition = 0;
            // calculate the end size or endPostion using Math.min() function which is return the min. value   
            var endPosition = Math.min(fileContents.length, startPosition + this.CHUNK_SIZE);
             
            // start with the initial chunk, and set the attachId(last parameter)is null in begin
            this.uploadInChunk(component, file, fileContents, startPosition, endPosition, '');
        },
        uploadInChunk: function(component, file, fileContents, startPosition, endPosition, attachId) {
            // call the apex method 'SaveFile'
            var getchunk = fileContents.substring(startPosition, endPosition);
            //component.set("v.newServiceRequest").Month__c = component.find("selectedMonthID").get("v.value");
            var serviceRequest = component.get("v.newServiceRequest");
            //serviceRequest.Year__c = selectedYearValue;
            component.set("v.selectedMonthValue",component.find("selectedMonthID").get("v.value"));
            console.log("selectedMonthValue---" + component.get("v.selectedMonthValue"));
            var selectedLeaseID  = component.get("v.selectedLeaseNumber");
            this.handleMonthlySalesSubmission(component,serviceRequest,file.name,encodeURIComponent(getchunk),file.type,component.get("v.selectedYearValue"),component.get("v.userIdValue"),component.get("v.selectedMonthValue"), selectedLeaseID);
        },
        handleMonthlySalesSubmission : function(component,serviceReqRecord,fileName,base64Data,contentType,selectedYearValue,currentLoggedInUser,selectedMonthValue,selectedLeaseID){   
            var action = component.get("c.createNewMonthlySalesSR");
            action.setParams({
                serviceReqRecord      : serviceReqRecord,
                fileName              : fileName,
                base64Data            : base64Data,
                contentType           : contentType,
                selectedYearValue     : selectedYearValue,
                currentLoggedInUser   : currentLoggedInUser,
                selectedMonth         : selectedMonthValue,
                leaseId               : selectedLeaseID,
                customerLeaseMapVal : component.get("v.customerLeaseMap")
            });
            action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS"){
                component.set("v.submitNewMonthlySales", false); 
                component.set("v.customerLeases", response.getReturnValue().customerLeases); 
                component.set("v.customerLeaseMap", response.getReturnValue().customerleaseMap);
                component.set("v.salesWrapperDataList", response.getReturnValue());
                component.set("v.salesReportList", response.getReturnValue().serviceRequests);
                component.set("v.monthsMap", response.getReturnValue().calanderMonths);
                component.set("v.yearOptionList", response.getReturnValue().yearOptions);
                component.set("v.monthOptionList", response.getReturnValue().unsubmittedMonths);
                component.set("v.newServiceRequest", response.getReturnValue().createNewServiceRequest);           
            }
            else {
                 console.log("Response errored with state : " + state);
                }
            });
            //send action off to be executed
            $A.enqueueAction(action);
         },
})