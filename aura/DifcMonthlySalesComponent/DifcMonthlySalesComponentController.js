({
    doInit : function(component, event, helper) {
        //let currentUser = $A.get("$SObjectType.CurrentUser.Id");
        //Console.log('currentUser---'+currentUser);
        //helper.getSalesDataFromServer(component);
        helper.getInitiateMonthlySalesView(component);
    },
    getDataReleventToThisLease: function(component, event, helper) {
        //alert('getDataReleventToThisLease---'+component.find("selectedLeaseID").get("v.value"));
        component.set("v.selectedLeaseNumber",component.find("selectedLeaseID").get("v.value"));
        helper.getDataReleventToThisLease(component);
    },
    getDataReleventToThisYear: function(component, event, helper) {
        helper.getDataReleventToThisYear(component);
    },
    handleNew: function(component, event, helper) {
        var monthOptionList = component.get("v.monthOptionList");
        if(monthOptionList != undefined && monthOptionList.length > 0)
        {
            component.set("v.submitNewMonthlySales", true);
            component.set("v.selectedMonthValue",component.find("selectedMonthID").get("v.value"));
        }else{
            alert('There are no valid submissions possible for the year and period selected');
        }
      
   },
    refresh: function(component, event, helper) {
        location.reload(true);
     },
     home: function(component, event, helper) {   
       window.open('/digitalOnboarding/s/',"_self");
     },
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
   populateMonth: function(component, event, helper) 
   {
       component.set("v.selectedMonth", component.find("selectedMonthID").get("v.value"));
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    hideMonthlySalesSubmission : function(component,event,helper){
      // make Spinner attribute to false for hide loading spinner    
        //alert('Test--'+component.get("v.submitNewMonthlySales"));
        component.set("v.submitNewMonthlySales", false);
     },
     handleMonthlySalesSubmission : function(component,event,helper){   
        var serviceReqRecord = event.getParam("serviceRequestRec");
        var fileName = event.getParam("fileName");
        var base64Data = event.getParam("base64DataBody");
        var contentType = event.getParam("contentType");

        console.log('serviceReqRecord-------'+serviceReqRecord);
        component.set("v.submitNewMonthlySales", false);
     },
     handleFilesChange: function(component, event, helper) {
          var fileName = 'No File Selected..';
          if (event.getSource().get("v.files").length > 0) {
              fileName = event.getSource().get("v.files")[0]['name'];
          }
          component.set("v.fileName", fileName);
      },
      cancelPopup : function(component, event, helper){
        component.set("v.submitNewMonthlySales", false);
    },
    handleSubmit : function(component, event, helper){
        if (component.find("fuploader").get("v.files").length > 0) {
            helper.uploadHelper(component, event);
        } else {
            alert('Please Select a Valid File');
        }
    }, 
})