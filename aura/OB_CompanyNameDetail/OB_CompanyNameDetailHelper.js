({
    commitNameRecordHelper: function(component, event,helper) 
    {
        //file validation
        var fileError = helper.fileUploadHelper(component, event, helper);
        if(fileError)
        {
            return;
        } 
        let newURL = new URL(window.location.href).searchParams;
        //console.log("inside commitNameApex DIFC_NameCheckblock");
        //console.log(JSON.parse(JSON.stringify(component.get("v.TransactinalStageSR"))));
        console.log(JSON.parse(JSON.stringify(component.get("v.CompanyName"))));
        console.log(JSON.parse(JSON.stringify(component.get("v.docMasterContentDocMap"))));
        var action = component.get("c.commitNameApex");   
        action.setParams({
            srId : newURL.get('srId'),
            pageId : newURL.get('pageId'),
            companyName : component.get("v.CompanyName"),
            docMap: component.get("v.docMasterContentDocMap")
        })
        action.setCallback(this, function(response) {
            var retVal = response.getReturnValue();
            console.log(retVal);
            var lstCompany = retVal.lstCompany;
            var state=response.getState();
            //console.log("response from commitNameApex DIFC_NameCheckblock"+retVal);
            //console.log("state is "+state);
            
            if(state=="SUCCESS" && retVal.lstCompany ){
                var cmpEvent = component.getEvent("companyNameEvent");
                console.log(cmpEvent); 
                cmpEvent.setParams({
                    "showNewPanel" : "false",
                    "companyList" : retVal.lstCompany,
                    "ResponseResult" : retVal 
                });
                cmpEvent.fire();
            }
                else{
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "message": retVal.errorMessage,
                        "type":"error"
                    });
                    toastEvent.fire();
                }
            //console.log("inside success submit for name commitNameRecordHelper");
        });
        
        $A.enqueueAction(action); 
        //helper.expandAndCollapseAccordian(component, event, helper);
    },
    viewSRDocument:function(component,helper){
        console.log('viewSRDocument');
        console.log(component.get("v.CompanyName.Id"));
        var action = component.get("c.viewSRDocs");   
        action.setParams({
            srId : component.get("v.srId"),
            companyNameId : component.get("v.CompanyName.Id")
        })
        action.setCallback(this, function(response) {
            var retVal = response.getReturnValue();
            console.log(retVal);
            var state=response.getState();
            
            if(state=="SUCCESS"){
                if(retVal.Evidence_of_relationship_to_existing_entity)
                    component.set("v.evidenceDocFileName",retVal.Evidence_of_relationship_to_existing_entity.FileName)
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayToast('Error', errors[0].message, 'error');
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        
        $A.enqueueAction(action); 
    },
    translateSourceText:function(component,event, helper, sourceText){
        var translateMap = {};
        translateMap["Trading_Name__c"] = "Arabic_Trading_Name__c";
        translateMap["Entity_Name__c"] = "Arabic_Entity_Name__c";
        var a ;
        if(event)
            a = event.getSource();
        var id ;
        if(sourceText)
            id = sourceText;
        else
            id = a.getLocalId();

        var attributeInstance = "v.CompanyName.";
        var sourceAttributeInstance = attributeInstance + id;
        var destAttributeInstance = attributeInstance + translateMap[id];
        var translateText = component.get(sourceAttributeInstance);
        console.log(sourceAttributeInstance);
        console.log(destAttributeInstance);
        console.log(translateText);
        if (translateText) {
            var action = component.get("c.translateToArabic");
            action.setParams({
                text: translateText,
            });

            // set call back 
            action.setCallback(this, function (response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    component.set(destAttributeInstance,result);
                    console.log(result);
                } else if (state === "INCOMPLETE") {
                    alert("From server: " + response.getReturnValue());
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            helper.displayToast('Error', errors[0].message, 'error');
                            console.log("Error message: " + errors[0].message);
                        }
                    } else {
                        console.log("Unknown error");
                    }
                }
            });
            // enqueue the action 
            $A.enqueueAction(action);
        }
    },
    /**
     * Display a message
     */
    displayToast: function (title, message,type) {
        console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'type':type,
                'title': title,
                'message': message,
                mode: 'dismissable'
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
    },
    
    updateEndsWith: function(component,helper){
        //Setting the Arabic Ends With - Value on change of EndsWith English name.
        console.log(component.get("v.endsWithList"));
        var endsWithValue = component.find("endswithCheck").get("v.value");
        var index = 0;
        var endsWith = component.get("v.endsWithList");
        if(endsWithValue){
            var value = component.get("v.CompanyName.Ends_With__c");
            console.log(endsWith);
            endsWith.forEach(function(v,i,a) {             
                if(v.value == value) {
                    index = i;
                }
            });
            console.log(endsWith[index].arabicValue);
            component.set("v.CompanyName.Arabic_Ends_With__c",endsWith[index].arabicValue);
        }
        
        else{
            console.log('here');
            component.set("v.CompanyName.Arabic_Ends_With__c","");
            if(endsWith && endsWith.length == 2){
                index = 1;
            }
        }
    },
    setEndsWithForSinglePicklistValue : function(component,helper){
        var endsWith = component.get("v.endsWithList");
        var index = 1;
        if(endsWith && endsWith.length == 2){
            component.set("v.CompanyName.Ends_With__c",endsWith[index].value);
            component.set("v.CompanyName.Arabic_Ends_With__c",endsWith[index].arabicValue);
        }
    },
    showHideIdenticalField:function(component,helper){
        console.log("after is is "+component.get("v.CompanyName.Check_Identical_to_existing__c"));
        console.log("inside onSimilarChangeName DIFC_NameCheckblock");
        var selectedValue = component.get("v.CompanyName.Check_Identical_to_existing__c");//event.getSource().get("v.value");
        console.log("selectedValue for showUploadIdenticalName is "+selectedValue);
        if(selectedValue=='Yes'){
            component.set("v.showUploadIdenticalName","true");
        }
        if(selectedValue=='No'){
            component.set("v.showUploadIdenticalName","false");
        }     
    },
    loadTransactionalStageCompNameValues:function(component,helper){
        var response = component.get("v.ResponseResult");
        component.set('v.commandButton', response.ButtonSection);
        component.set("v.TransactinalStageSR",response.sr);
        component.set("v.srId",response.sr.Id);
        component.set("v.isCompanyReserved",response.isCompanyReserved);
        
        var endsWithListItems = response.lstEndsWith;
        var options = [];
        var option=[];
        if(endsWithListItems.length > 0){
            option=[];
            option["value"] = '';
            option["label"] = 'None';
            option["arabicValue"] = '';
            options.push(option);
        }
        for(var i=0;i<endsWithListItems.length;i++){
            option=[];
            option["value"] = endsWithListItems[i].endsWithEnglish;
            option["label"] = endsWithListItems[i].endsWithEnglish;
            option["arabicValue"] = endsWithListItems[i].endsWithArabic;
            options.push(option);
        }
        component.set("v.endsWithList",options);

        //Move to createobjectdata
        //If setup='branch', then the proposed company name should be the same as the parent company name
        
        if(response.sr.Setting_Up__c && response.sr.Setting_Up__c.toLowerCase() == 'branch'){
            var entityName = component.get("v.CompanyName.Entity_Name__c");
            var tradingName = component.get("v.CompanyName.Trading_Name__c");
            component.set("v.isBranchSR","true");


            //Hides the ends with for branch
            component.set("v.endsWithList",[]);

            //if entity name = blank, then populate the foreign name.
            if(!entityName){
                component.set("v.CompanyName.Entity_Name__c",response.sr.Foreign_Entity_Name__c);
                helper.translateSourceText(component,'',helper,'Entity_Name__c');  
            }
             //if trading name = blank, then populate the foreign name.
            if(!tradingName){
                component.set("v.CompanyName.Trading_Name__c",response.sr.Foreign_Entity_Name__c);  
                helper.translateSourceText(component,'',helper,'Trading_Name__c');  
            }
            //If setup=branch and sector = 'representative office', Trading Name must end with the phrase “(DIFC Representative Office)” at the end of your Trading Name
            if(response.sr.Business_Sector__c && response.sr.Business_Sector__c.toLowerCase() == 'representative office'){
                component.set("v.addRepOffice","true");
            }
        }
        //If setup='transfer',Populate the Entity and Trading name with Foreign Name
        else if(response.sr.Setting_Up__c && response.sr.Setting_Up__c.toLowerCase() == 'transfer'){
            
            var entityName = component.get("v.CompanyName.Entity_Name__c");
            var tradingName = component.get("v.CompanyName.Trading_Name__c");

            //if entity name = blank, then populate the foreign name.
            if(!entityName){
                component.set("v.CompanyName.Entity_Name__c",response.sr.Foreign_Entity_Name__c);
                helper.translateSourceText(component,event,helper,'Entity_Name__c');  
            }
             //if trading name = blank, then populate the foreign name.
            if(!tradingName){
                component.set("v.CompanyName.Trading_Name__c",response.sr.Foreign_Entity_Name__c);  
                helper.translateSourceText(component,event,helper,'Trading_Name__c');  
            }
            
            //Hides the ends with for transfer
            component.set("v.endsWithList",[]);

        }
        //The following Persons are not permitted to have Trading Names:hide the trading name selection
        var hideTradingNames = ['non profit incorporated organisation (npio)','foundation','prescribed companies'];
        if(response.sr.Business_Sector__c && 
            hideTradingNames.indexOf(response.sr.Business_Sector__c.toLowerCase()) > -1){
            component.set("v.showTradingName","false");
        }

        //The word trading name should be removed in case of NPIO, Prescribed Company and Foundation
        var removeTradingNameForBusinessSector = ['non profit incorporated organisation (npio)','prescribed companies','foundation'];
        if(response.sr.Business_Sector__c  
            && removeTradingNameForBusinessSector.indexOf(response.sr.Business_Sector__c.toLowerCase() > -1)){
            component.set("v.proposedEntityLabel","Entity");
        }
        
        //Translate the Entity Name on load to Arabic

        if(response.sr.Franchise__c &&
            response.sr.Franchise__c.toLowerCase() == 'yes'){
            component.set("v.isFranchise","true");
        }

        
    },
    clearSRValues : function(component,event,helper){
        //component.set('v.TransactinalStageSR.Check_Identical_to_existing__c','');
        //component.set('Fall_within_existing_entities__c','');
        component.set('v.TransactinalStageSR.Parent_Entity_Name__c','');
        component.set('v.TransactinalStageSR.Former_Name__c','');
        component.set('v.TransactinalStageSR.Date_of_Registration__c','');
        component.set('v.TransactinalStageSR.Place_of_Registration__c','');
        component.set('v.TransactinalStageSR.Country_of_Registration__c','');
        component.set('v.TransactinalStageSR.Registered_No__c','');
        component.set('v.TransactinalStageSR.Family_Group__c','');
        component.set('v.TransactinalStageSR.Registered_address__c','');
        component.set('v.TransactinalStageSR.Address_Line_1__c','');
        component.set('v.TransactinalStageSR.Address_Line_2__c','');
        component.set('v.TransactinalStageSR.Country__c','');
        component.set('v.TransactinalStageSR.City_Town__c','');
        component.set('v.TransactinalStageSR.State_Province_Region__c','');
        component.set('v.TransactinalStageSR.Po_Box_Postal_Code__c','');
    },
    searchName:function(component,event,helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        var nameToBeSent = component.get("v.CompanyName.Entity_Name__c") ? component.get("v.CompanyName.Entity_Name__c") : '';
        var nameId= component.get("v.CompanyName.Id");
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');
        var pageId = newURL.get('pageId');
        var flowId = newURL.get('flowId');
        urlEvent.setParams({
                "url": "/ob-namecheck/?valueTextEntity= " +nameToBeSent+"&NameId="+nameId+"&srId="+srId+"&pageId="+pageId+"&flowId="+flowId
        });
        urlEvent.fire();
    },
    afterRenderExpandPage: function(component,event,helper){
        var urlString = window.location.href;
        var sURLVariables = urlString.split('submit-to-difc/?');
        var passedAttri = sURLVariables[1];
        if(passedAttri.includes("checkAvail")){
            var cmpTar= document.getElementById('nameCheckCard');
            if (cmpTar.classList.contains('ui-form-card--collapsed')) {
                cmpTar.classList.remove('ui-form-card--collapsed');
                cmpTar.closest('.difc-card-section').classList.add('difc-card--expanded');
            }else{
                cmpTar.classList.add('ui-form-card--collapsed');
            }
        }
    },
    
    getURLParameter : function(param) {
        var result=decodeURIComponent
        ((new RegExp('[?|&]' + param + '=' + '([^&;]+?)(&|#|;|$)').
          exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
        //console.log('Param ' + param + ' from URL = ' + result);
        return result;
    },
    
    //for file validation in helper.
	fileUploadHelper : function(cmp, event, helper)
    {
		//New method.
        console.log('########### fileHolder Start');
     
       // For file upload and validation.
       var fileUploadValid = true;
       var fileHolder = cmp.find("filekey");
       
       if(fileHolder && Array.isArray(fileHolder))
       {
            for (var i = 0; i < fileHolder.length; i++)
            {
                if( fileHolder[i].get("v.requiredDocument") )
                {
                   
                    helper.fileUploadProcess(cmp, event, helper,fileHolder[i]);
                    fileUploadValid = fileHolder[i].get("v.fileUploaded");
                    //requiredText
                    if( !fileUploadValid)
                    {
                        fileHolder[i].set('v.requiredText','Please upload document.');
                    }
                   
                }
               
            }
           
       }
       else if(fileHolder)
       {
           console.log('########### fileHolder content docID ',fileHolder.get('v.contentDocumentId'));
           //here
           helper.fileUploadProcess(cmp, event, helper,fileHolder);
           
           
           if( fileHolder.get("v.requiredDocument") )
           {
               fileUploadValid = fileHolder.get("v.fileUploaded");
               console.log('@@@@@@@@@@@ fileUploadValid ',fileUploadValid);
                if( !fileUploadValid)
                {
                    fileHolder.set('v.requiredText','Please upload document.');
                }
           }
           
       }
       
       console.log(' Final file upload ',fileUploadValid);
       if( !fileUploadValid )
       {
            helper.showToast(cmp, event, helper,'Please upload required files', 'error');
            cmp.set('v.spinner',false);
            return 'file error';
       }
       console.log('########### fileHolder end');
    },
    
   
    fileUploadProcess : function(cmp, event, helper,fileHolder)
    {
        //New Method
           var contentDocumentId = fileHolder.get('v.contentDocumentId');
           var documentMaster = fileHolder.get('v.documentMaster');
           
           if(contentDocumentId && documentMaster)
           {
                var mapDocValues = {};
                var docMap = cmp.get("v.docMasterContentDocMap");
                for(var key in docMap)
                {
                    mapDocValues[key] = docMap[key];
                }
               
                mapDocValues[documentMaster] = contentDocumentId;
                console.log(mapDocValues);
        		console.log('@@@@@@ docMasterContentDocMap ');
                cmp.set("v.docMasterContentDocMap",mapDocValues);
                console.log(JSON.parse(JSON.stringify(cmp.get("v.docMasterContentDocMap"))));
           }
       
       
    },
    
    
})