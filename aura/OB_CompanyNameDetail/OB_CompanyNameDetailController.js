({
    doInit: function(component, event, helper) 
    { 
        console.log('Init Method');
        console.log(JSON.parse(JSON.stringify( component.get("v.CompanyName"))));
        let newURL = new URL(window.location.href).searchParams;
        var flowIdParam = ( component.get('v.flowId') ? component.get('v.flowId') : newURL.get('flowId'));     
        var srIdParam = (  component.get('v.srId') ?  component.get('v.srId') : newURL.get('srId'));
        var pageIdParam = (  component.get('v.pageId') ?  component.get('v.pageId') : newURL.get('pageId'));
         // set if comming from url
         component.set("v.pageId",pageIdParam);
         component.set("v.flowId",flowIdParam);
         component.set("v.srId",srIdParam);
          
         console.log('####@@@@@@ pageId ',pageIdParam);  
         console.log('####@@@@@@ flowIdParam ',flowIdParam);   
         console.log('####@@@@@@ srIdParam ',srIdParam);   
        
        console.log('init>>');
        
        
        
    },
    handleFileUploadFinished: function (component, event) 
    {
        // Get the list of uploaded files
        var uploadedFiles = event.getParam("files");
        var fileName = uploadedFiles[0].name;
        
        
        var contentDocumentId = uploadedFiles[0].documentId;
        console.log(contentDocumentId);
        var docMasterCode = event.getSource().get('v.name'); 
        console.log(docMasterCode);
        console.log(component.find(docMasterCode).get("v.value"));
        console.log(fileName);
        if(component.find(docMasterCode))
            component.find(docMasterCode).set("v.value",fileName);

        console.log(component.find(docMasterCode).get("v.value"));
        var mapDocValues = {};
        var docMap = component.get("v.docMasterContentDocMap");
        console.log(docMap);
        for(var key in docMap){
            mapDocValues[key] = docMap[key];
        }
        
        mapDocValues[docMasterCode] = contentDocumentId;
        console.log(mapDocValues);

        component.set("v.docMasterContentDocMap",mapDocValues);
        console.log(JSON.parse(JSON.stringify(component.get("v.docMasterContentDocMap"))));

        //alert("Files uploaded : " + uploadedFiles.length);
    },
    showTradingNameModal:function(component,event,helper){
        console.log('openTradingNameModal');
        component.set("v.openTradingNameModal","true");
        component.set("v.ModalMsg","The trading name must be the same as the Company Name as per the naming policy. However, you can proceed to edit the name.");
    },
    populateName:function(component,event,helper){
        console.log('populateName');
        var response = component.get("v.ResponseResult");
        if(response.sr.Setting_Up__c && response.sr.Setting_Up__c.toLowerCase() == 'branch'){
            var entityName = component.get("v.CompanyName.Entity_Name__c");
            var tradingName = component.get("v.CompanyName.Trading_Name__c");
             //if trading name = blank, then populate the entity name on TradingName Field.
            if(!tradingName){
                component.set("v.CompanyName.Trading_Name__c",entityName);  
                helper.translateSourceText(component,'',helper,'Trading_Name__c');  
            }

        }
    },
    validateSetupType:function(component,event,helper){
        console.log('validateSetupType');
        var returnValueSRObj = component.get("v.TransactinalStageSR");
        if(returnValueSRObj.Setting_Up__c && returnValueSRObj.Setting_Up__c.toLowerCase() == 'branch'){
            component.set("v.openIsBranchModal","true");
            component.set("v.ModalMsg","The entity name must be the same as the Foreign Entity as per the naming policy. However, you can proceed to edit the name.");
        }
        else if(returnValueSRObj.Setting_Up__c && returnValueSRObj.Setting_Up__c.toLowerCase() == 'transfer'){
            component.set("v.openIsBranchModal","true");
            component.set("v.ModalMsg","The entity name must be the same as the Foreign Entity as per the naming policy. However, you can proceed to edit the name.");
            //helper.displayToast("Error","This is the same as the transfer company and trading name however it will end with the structure selected.");
        }
        else{
            helper.searchName(component, event, helper);
        }
        
    },
    navigateToSearch: function(component, event, helper) {
        helper.searchName(component, event, helper);
    },
    HideTradingNameModal:function(component, event, helper) {
        component.set("v.openTradingNameModal","false");
        component.set("v.modalShown","true");
    },
    HideBranchModal:function(component, event, helper) {
        component.set("v.openIsBranchModal","false");
    },
    navigateToTradingSearch: function(component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        var nameToBeSent = component.get("v.CompanyName.Trading_Name__c");
        var nameId= component.get("v.CompanyName.Id");
 		let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');        
        urlEvent.setParams({
            "url": "/ob-tradingnamecheck/?valueTextEntity= " +nameToBeSent+"&NameId="+nameId+"&srId="+srId +"&pageId="+newURL.get('pageId')+"&flowId="+newURL.get('flowId')
        });
        urlEvent.fire();
    },
    
    onSimilarChangeNameshowUpload: function(component, event, helper) {
          helper.showHideIdenticalField(component, event, helper);
    },
     
    commitNameRecord : function(component, event, helper){
        console.log("inside commitNameRecord");
        var allValid = true;
        var requiredFields = ['Entity_Name__c','endswithCheck'];
        var compArr = [];
        for(var field in requiredFields){
            if(component.find(requiredFields[field])){
                console.log(component.find(requiredFields[field]));
                compArr.push(component.find(requiredFields[field]));
            }
        }
        
        var allValid = compArr.reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        console.log("inside commitNameRecord 2");
        
        if (allValid) {
            console.log("inside if allValid commitNameRecord");
            helper.commitNameRecordHelper(component,event,helper);
            
            var cmpTar= document.getElementById('nameCheckCard');
            cmpTar.classList.add('theme_change');
            
        } else {
            //var cmpTar= document.getElementById('nameCheckCard');
            //cmpTar.classList.remove('theme_change');
            
            console.log("inside else allValid commitNameRecord");
            helper.displayToast("Error","Please fill the mandatory fields","error");
            //alert('Please fill the mandatory fields');
        } 
    },
    callChildMethod : function(component, event, helper) {
        alert("mehtod called from parent");
    },
    translate:function(component,event,helper){   
        helper.translateSourceText(component,event,helper,'');     
    },
    
    updateName:function(component,event,helper){
        helper.updateEndsWith(component,helper);
    },
    
    handleUploadFinished: function (component, event) {
        // Get the uploaded file
        var uploadedFile = event.getParam("files")[0];
        console.log(uploadedFile);
        var documentId = uploadedFile.documentId;
        component.set("v.franchiseDoc.File_Name__c",uploadedFile.name);

        // Now perform a DML operation to save the description along with the document. Method will be something like this -
        //saveDescription(documentId, description); // Use aura actions to call this method. This prototype is supposed to be implemented on the apex controller.
    },
    handleEvidenceUploadFinished: function (component, event) {
        // Get the uploaded file
        var uploadedFile = event.getParam("files")[0];
        console.log(uploadedFile);
        var documentId = uploadedFile.documentId;
        component.set("v.evidenceDoc.File_Name__c",uploadedFile.name);

        // Now perform a DML operation to save the description along with the document. Method will be something like this -
        //saveDescription(documentId, description); // Use aura actions to call this method. This prototype is supposed to be implemented on the apex controller.
    },
    handleButtonAct : function(component, event, helper) 
    {
        
        
        try{
            
        let newURL = new URL(window.location.href).searchParams;
        
        var srID = newURL.get('srId');
        var buttonId =  event.target.id;
        console.log(buttonId);
            
        var action = component.get("c.getButtonAction");
        
        action.setParams({
            "SRID": srID,
            "ButtonId": buttonId
        });
        action.setCallback(this, function(response) {
        
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("button succsess")
            	
                console.log(response.getReturnValue());
                window.open(response.getReturnValue().pageActionName,"_self");
            }else{
            	console.log('@@@@@ Error '+response.getError()[0].message);
			}
        });
        $A.enqueueAction(action);
          
        }catch(err) {
           console.log(err.message) 
        } 
        

    },
	cancel:function(component,event,helper){
        console.log('cancel');
        component.set("v.showPanel","false");
        //component.set("v.CompanyNameInstance",[]);
        /*
        var cmpEvent = component.getEvent("companyNameEvent");
        console.log(cmpEvent);
        cmpEvent.setParams({
            "showNewPanel" : "false"
        });
        cmpEvent.fire();
        console.log('cancel done');
        */
        
	},
    
    handleOCREvent : function(cmp, event, helper)
    {
                
        		var ocrWrapper = event.getParam("ocrWrapper");
                var mapFieldApiObject = ocrWrapper.mapFieldApiObject;
                // Change the key here....imp for OCR.
                var fields = cmp.find("directorkey");
                
                //parsing of code.
                for(var key in mapFieldApiObject)
                {
                     for (var i = 0; i < fields.length; i++)
                     {
                         console.log(fields[i].get("v.fieldName"));
                         if(fields[i].get("v.fieldName"))
                         {
                             console.log(fields[i].get("v.fieldName"));
                             if((fields[i].get("v.fieldName")).toLowerCase() == key.toLowerCase() )
                             {
                                fields[i].set('v.value',mapFieldApiObject[key]);
                             }
                     
                        }
                     }
                 
                }

    },
    
})