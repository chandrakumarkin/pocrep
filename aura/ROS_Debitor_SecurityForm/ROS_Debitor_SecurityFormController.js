({
    handleRecordType  : function(cmp, event, helper){
       var index = event.target.dataset.amedtype;
       console.log('###############@@@@@@@@@@'+ index);
       cmp.set("v.recordtypename", index);
       console.log(cmp.get("v.recordtypename"))
       cmp.set("v.isSelectedIndividual", (index == 'Individual') ?  true : false);
       cmp.set("v.isSelectedCorporate", (index == 'Body Corporate') ? true : false);
       cmp.set("v.isIndividual", (index == 'Individual') ?  true : false);
       cmp.set("v.isCorporate", (index == 'Body Corporate') ?  true : false);
        var entityTypeLabel = cmp.get('v.entityTypeLabel');
        var initDebatSecurity = cmp.get('c.initDebatSecurityDB');   
        var reqWrapPram  =
        {
            srId : cmp.get('v.srWrap').srObj.Id,
            recordtypename: cmp.get("v.recordtypename"),
            approvedFormId :cmp.get('v.srWrap').approvedFormOneROS.Id 
        };
        initDebatSecurity.setParams(
        {
            "reqWrapPram": JSON.stringify(reqWrapPram),
        });
       
        initDebatSecurity.setCallback(this,function(response) {
                var state = response.getState();
                console.log("callback state: " + state);
                
                if (cmp.isValid() && state === "SUCCESS"){
                    var respWrap = response.getReturnValue();
                    console.log('######### respWrap.amedWrap  ',respWrap.amedWrap);
                    cmp.set('v.amedWrap',respWrap.amedWrap);	   
                    console.log('######### amedWrap.debObj  ',cmp.get("v.amedWrap"));
                }else{
                    console.log('@@@@@ Error '+response.getError()[0].message);
                    helper.createToast(cmp,event,response.getError()[0].message);
                
                }
            }    
        );
        $A.enqueueAction(initDebatSecurity);
       console.log(cmp.get("v.recordtypename"))
     }
   
})