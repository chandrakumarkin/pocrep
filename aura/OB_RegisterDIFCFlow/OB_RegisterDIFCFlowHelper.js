({
  getFormDetail: function(cmp, event) {
    try {
      console.log("====getformdetail====");
      //var getFormAction = cmp.get('c.getFlowPages');
      var getFormAction = cmp.get("c.getFlowContent");
      // Take the param from attribute or take from URL.
      let newURL = new URL(window.location.href).searchParams;
      var flowIdParam = cmp.get("v.flowId")
        ? cmp.get("v.flowId")
        : newURL.get("flowId");
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");
      var pageIdParam = cmp.get("v.pageId")
        ? cmp.get("v.pageId")
        : newURL.get("pageId");
      // set if comming from url
      cmp.set("v.pageId", pageIdParam);
      cmp.set("v.flowId", flowIdParam);
      cmp.set("v.srId", srIdParam);

      console.log("####@@@@@@ pageId ", pageIdParam);
      console.log("####@@@@@@ flowIdParam ", flowIdParam);
      console.log("####@@@@@@ srIdParam ", srIdParam);

      var reqWrapPram = {
        pageId: pageIdParam,
        flowId: flowIdParam,
        srId: srIdParam
      };

      console.log("======reqWrapPram===" + JSON.stringify(reqWrapPram));
      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      //console.log('@@@@@@@@@@ reqWrapPram init '+reqWrapPram);
      getFormAction.setCallback(this, function(response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          console.log("======respWrap===" + JSON.stringify(respWrap[0].srWrap));

          //Locking and unlocking logic
          console.log("@@@@ locking " + respWrap.isSRLocked);
          if (respWrap.isSRLocked) {
            var pageURL = respWrap.communityReviewPageURL;
            window.open(pageURL, "_self");
          }

          var srWarp = respWrap[0].srWrap;
          /*
                        if(!srWarp.isDraft)
                         {
                                                 var pageURL = srWarp.viewSRURL;
                                                 window.open(pageURL,"_self");
                         }
                         
                         
                    */
          cmp.set("v.pgFlwWrapLst", respWrap);
          cmp.set("v.srId", respWrap[0].ServiceRequestId);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          // helper.hideSpinner(cmp, event, helper);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
    }
  }
});