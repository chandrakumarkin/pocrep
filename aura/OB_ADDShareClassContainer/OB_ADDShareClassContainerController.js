({
	doInit : function(cmp, event, helper) {
		cmp.set('v.spinner',true);
        
		try
        {
            console.log('===pages init===');
            
            var getFormAction = cmp.get('c.getShareClassList');
            
            let newURL = new URL(window.location.href).searchParams;
            var pageId = newURL.get('pageId');
            var flowId = newURL.get('flowId');
			var srIdParam =  (  cmp.get('v.srId') ?  cmp.get('v.srId') : newURL.get('srId'));
            var accountID = (  cmp.get('v.accountID') ?  cmp.get('v.accountID') : newURL.get('accountID'));
            
            cmp.set('v.srId',srIdParam) ;
            cmp.set('v.accountID',accountID) ;
            
            var reqWrapPram  =
                {
                    srId: srIdParam,
                    flowId: flowId,
                    pageID:pageId,
                    accountID:accountID
                };
            
            getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
            console.log('@@@@@@@@@@33 reqWrapPram init '+JSON.stringify(reqWrapPram));
            getFormAction.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                            /*  if(!respWrap.srWrap.isDraft){
                                            	  var pageURL = respWrap.srWrap.viewSRURL;
                                            	  window.open(pageURL,"_self");
                                              }
                                              */
                                              cmp.set('v.srWrap',respWrap.srWrap);
                                              cmp.set('v.accountID',respWrap.accountID);
                                              cmp.set('v.commandButton',respWrap.ButtonSection);
                                              
                                              console.log('######### respWrap ',JSON.stringify(cmp.get('v.srWrap.shareholderAmedWrapLst')) );
                                              cmp.set('v.spinner',false);
                                              
                                          }
                                          else
                                          {
                                              cmp.set('v.spinner',false);
                                              
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                              cmp.set("v.errorMessage",response.getError()[0].message);
                                              cmp.set("v.isError",true);
                                              
                                          }
                                      }
                                     );
            $A.enqueueAction(getFormAction);
        }catch(err){
            console.log('=error==='+err.message);
            cmp.set('v.spinner',false);
            elements1.classList.add("display-none");
        }
	},
    handleRefreshEvnt: function(cmp, event, helper)
    {
        var shareClassID = event.getParam("shareClassID");
        var srWrap = event.getParam("srWrap");
        var isNewAmendment = event.getParam("isNewAmendment");
        var shareClassWrap = event.getParam("shareClassWrap");
		console.log('$$$$$$$$$$$ handleRefreshEvnt== ',JSON.stringify(srWrap));
        console.log('2222222222 shareClassID== ',shareClassID);
        cmp.set('v.shareClassID',shareClassID);
       
        if(!$A.util.isEmpty(srWrap)){
            console.log('====sr wrap assign===');
        	cmp.set('v.srWrap',srWrap);
        }
        if(isNewAmendment == true){
            console.log('====isNew===');
            cmp.set('v.shareClassWrap',shareClassWrap);
        }
        if(isNewAmendment == false){
            console.log('====isNew==false=');
            cmp.set('v.shareClassWrap',shareClassWrap);
        }
    },
    handleButtonAct : function(component, event, helper) {
        //helper.handleButtonActHelper(component, event, helper);
        console.log('buttttton');
        var buttonId =  event.target.id;
        console.log(buttonId);
        var srWrap = component.get('v.srWrap');
        //var shareClassWrapLst = component.get('v.srWrap.shareClassWrapLst');
        console.log('==shareClassWrapLst=='+srWrap.shareClassWrapLst.length);
        if(srWrap.shareClassWrapLst.length === 0){
            component.set('v.spinner',false);                                 
            console.log('@@@@@ Error ');
            component.set("v.errorMessage",'At least one class needs to be added');
            component.set("v.isError",true);
            helper.displayToast('Error','At least one class needs to be added','error');
        }else{
            console.log('==no error ==');
            try{
            
        let newURL = new URL(window.location.href).searchParams;
        
        var srID = newURL.get('srId');
        var pageID = newURL.get('pageId');
                
        var buttonId =  event.target.id;
        
        var action = component.get("c.getButtonAction");
        
        action.setParams({
        
           	"SRID": srID,
            "pageId": pageID,
            "ButtonId": buttonId
        });
     
        action.setCallback(this, function(response) {
        
            var state = response.getState();
            var respWrap = response.getReturnValue();
            if (state === "SUCCESS") {
                console.log("button succsess");
            	if(!$A.util.isEmpty(respWrap.errorMessage) ){
                	console.log('=====error on sve===='+respWrap.errorMessage);
                    helper.displayToast('Error',respWrap.errorMessage,'error');
                }else{
                	window.open(response.getReturnValue().pageActionName,"_self");
                }
            }else{
            	console.log('@@@@@ Error add share class '+response.getError()[0].message);
                helper.displayToast('Error',response.getError()[0].message,'error');
			}
        });
        $A.enqueueAction(action);
          
        }catch(err) {
           console.log(err.message) 
        } 
        }
        

    },
    closeModel : function(cmp, event, helper) {
        //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        console.log('--toast close--');
        cmp.set("v.errorMessage","");
        cmp.set("v.isError",false);
    },
})