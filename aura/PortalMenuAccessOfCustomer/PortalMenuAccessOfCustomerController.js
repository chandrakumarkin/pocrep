({  
	
  cancel : function(component, event, helper) {
    self.close();
  },
  
  doInit  : function(component, event, helper) {

    let newURL = new URL(window.location.href).searchParams;
    component.set("v.accId", newURL.get("accId"));
    component.set("v.isUserHasAccess", newURL.get("isUserHasAccess"));
    var action = component.get("c.fetchPortalAccessList"); 
    var accountId = component.get("v.accId") ? component.get("v.accId") : '';
     
    action.setParams({
      "customerId": accountId            
    });
      
    action.setCallback(this, function(response) {

      if (response.getState() === "SUCCESS"){

        var storeResponse = response.getReturnValue();
        var noRecCheck = (storeResponse.length == 0) ? true : false;
        component.set("v.norecordfound", noRecCheck);
        component.set("v.srList", storeResponse);
        component.set("v.TotalCount", storeResponse.length);

      }else if (response.getState() === "ERROR") {

      }else {}
    });
    $A.enqueueAction(action); 
  },
  
  showSpinner: function(component, event, helper) {
      component.set("v.Spinner", true); 
  },
  
  // this function automatic call by aura:doneWaiting event 
  hideSpinner : function(component,event,helper){
     component.set("v.Spinner", false);
  },

  saveRec : function(component, event, helper) {
    
    var srList = component.get("v.srList");
    var action = component.get("c.saveMenuList");
    var accountId =component.get("v.accId");
    action.setParams({
      "portalMenu": srList,
      "accId": accountId            
    });
      
    action.setCallback(this, function(response) {
    
      var state = response.getState();
      if (state === "SUCCESS"){
          
          var storeResponse = response.getReturnValue();
          var noRecCheck = (storeResponse.length == 0) ? true : false;
          component.set("v.norecordfound", noRecCheck);
          component.set("v.srList", storeResponse);
          component.set("v.TotalCount", storeResponse.length);
          self.close();
          
        }else if (state === "ERROR") {
            
        }else { }
      });
      $A.enqueueAction(action); 
  },
})