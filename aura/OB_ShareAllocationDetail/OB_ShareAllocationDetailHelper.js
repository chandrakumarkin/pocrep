({
	addClass : function(cmp, event, helper)
    {
      	var shrDetailWrapLstParam = cmp.get("v.respWrapInit.shrDetailWrapLst");
        try
        {
           
            var getFormAction = cmp.get('c.addShareClass');
            let newURL = new URL(window.location.href).searchParams;
            
            var srIdParam = cmp.get('v.srId'); 
            var pageId = newURL.get('pageId');
            var amedWrapParam = cmp.get('v.amedWrap');
            console.log('in init inside -->' + JSON.stringify(amedWrapParam));
            
            var reqWrapPram  =	{
                                      srId: srIdParam,
                				    pageId: pageId,
								  amedWrap: amedWrapParam
                
                                }
            
            getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
            console.log('@@@@@@@@@@33121 reqWrapPram init '+JSON.stringify(reqWrapPram));
            getFormAction.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                              console.log('########## respWrap ',respWrap.amedWrap);
                                              cmp.set('v.amedWrap',respWrap.amedWrap);
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                              
                                          }
                                      }
                                     );
            $A.enqueueAction(getFormAction);
        }
        catch(err)
        {
            console.log('=error==='+err.message);
        }
        
        
        
    },

})