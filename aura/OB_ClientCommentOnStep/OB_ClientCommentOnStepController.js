({
	
    loadStepOnLoad : function(cmp, event, helper) 
    {
        console.log('########### on load step ',cmp.get('v.showModal') ); 
        var loadStep = cmp.get('c.loadStepDB');
        // amedWrap.amedObj.First_Name__c
        let newURL = new URL(window.location.href).searchParams;
        var stepIdParam = ( newURL.get('stepId') ? newURL.get('stepId') : cmp.get('v.stepId'));
        
        
        var reqWrapPram  =
                {
                    stepId: stepIdParam
                };
        
        loadStep.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
        
        loadStep.setCallback(this, 
                                      function(response) 
                               			{
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                                 
                                              // amedObj
                                              if( !respWrap.errorMessage )
                                              {
                                                  console.log('######### step respWrap ',respWrap);
                                                  if( !$A.util.isEmpty( respWrap.stepObj ))
                                                  {
                                                      cmp.set('v.stepObj',respWrap.stepObj);
                                                  }
                                                 
                                               }
                                               else
                                               {
                                                   //alert(respWrap.errorMessage);
                                                    cmp.set('v.errorMessage',respWrap.errorMessage);
                                                   //helper.showToast(cmp, event, helper, respWrap.errorMessage,'error'); 
                                               }
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error u... '+JSON.stringify(response.getError()));
                                              console.log('@@@@@ Error... '+response.getError()[0].message);
                                              
                                              // JSON.stringify(response.getError())
                                              //alert('Error ',JSON.stringify(response.getError()));
                                               cmp.set('v.errorMessage',JSON.stringify(response.getError()));
                                              //helper.showToast(cmp, event, helper,JSON.stringify(response.getError()),'error');
                                              
                                          }
                                      }
                                     );
            $A.enqueueAction(loadStep);
    },
    
    
    onStepSave : function(cmp, event, helper) 
    {
		
        var onStepSave = cmp.get('c.onStepSaveDB');
        // amedWrap.amedObj.First_Name__c
        var stepId = cmp.get('v.stepId');
        var stepObj = cmp.get('v.stepObj');
        
        stepObj.HexaBPM__Step_Notes__c = cmp.get('v.customerComment');
        
        var reqWrapPram  =
                {
                    stepId: stepId,
                   stepObj: stepObj
                };
        
        onStepSave.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
        
        onStepSave.setCallback(this, 
                                      function(response) 
                               			{
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                                 
                                              // amedObj
                                              if( ! respWrap.errorMessage )
                                              {
                                                   console.log('######### respWrap ',respWrap);
                                                   //cmp.set('v.showModal',false); 
                                                   helper.onClose(cmp, event, helper);
                                                 
                                               }
                                               else
                                               {
                                                   //alert(respWrap.errorMessage);
                                                   cmp.set('v.errorMessage',respWrap.errorMessage);
                                                   //helper.showToast(cmp, event, helper, respWrap.errorMessage,'error'); 
                                               }
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error u... '+JSON.stringify(response.getError()));
                                              console.log('@@@@@ Error... '+response.getError()[0].message);
                                              
                                              // JSON.stringify(response.getError())
                                              //alert('Error ',JSON.stringify(response.getError()));
                                              cmp.set('v.errorMessage',JSON.stringify(response.getError()));
                                              //helper.showToast(cmp, event, helper,JSON.stringify(response.getError()),'error');
                                              
                                          }
                                      }
                                     );
            $A.enqueueAction(onStepSave);
        
	},
    onClose : function(cmp, event, helper) 
    {
        helper.onClose(cmp, event, helper);
        
    }
    
    
})