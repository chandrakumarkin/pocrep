({
    doInit : function(component, event, helper) {
        console.log("inside method");
        var action = component.get("c.getTopUpBalanceAmount");
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                console.log('return value is trans1234 '+JSON.stringify(a.getReturnValue()));
                if(a.getReturnValue())
                {
                    var returnValue = a.getReturnValue();
                    console.log('returnValue'+returnValue);
                    component.set("v.wrapRecords",returnValue);
                    console.log("balanceUpdated12"+component.get("v.wrapRecords"));
                    component.set("v.displayComponent",returnValue.displayComponent);
                    
                }
            }
            else if(state == 'FAILURE')
            {
                console.log('Some problem ocurred');
            }
        });
        $A.enqueueAction(action);
        
    },
    goToTopUpBalance : function(component, event, helper){
        var action = component.get("c.getCurrentAccountId");
        action.setCallback(this, function(a){
            var state = a.getState(); // get the response state
            if(state == 'SUCCESS') {
                if(a.getReturnValue()){
                    var currentUser = a.getReturnValue();
                    var currentAccId = currentUser.Contact.AccountId;
                    var currentAccName = currentUser.Contact.Account.Name;
                    var initialAccId = component.get('v.wrapRecords.userAccountId');
                    console.log('currentAccId==>'+currentAccId);
                    console.log('initialAccId==>'+initialAccId);
                    if(initialAccId == currentAccId){
                        var urlEvent = $A.get("e.force:navigateToURL");
                        urlEvent.setParams({
                            "url": "/topupbalance/",
                        });
                        urlEvent.fire();
                    } else {
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            title : 'Error Message',
                            message:'The account that you are managing has been changed to '+currentAccName+'. Please refresh the page to continue.',
                            duration:' 5000',
                            key: 'info_alt',
                            type: 'error',
                            mode: 'pester'
                        });
                        toastEvent.fire();
                    }
                }
            }else if(state == 'FAILURE'){
                console.log('Some problem ocurred');
            }
        });
        $A.enqueueAction(action);
    }
})