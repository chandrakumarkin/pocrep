({
    showMenuList : function(component, event, helper){
        var toggleText = component.find("text");
    	$A.util.toggleClass(toggleText, "toggle");
    },
    expandAndCollapse : function(component, event, helper) {
        helper.expandAndCollapseAccordian(component, event, helper);
    },
    backtohome: function (component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": "/"
		});
		urlEvent.fire();
	}
})