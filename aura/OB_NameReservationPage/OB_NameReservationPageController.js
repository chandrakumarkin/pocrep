({
  doInit: function(component, event, helper) {
    helper.fetchNameReservationInfo(component, event, helper);
  },

  navToPaymentPage: function(component, event, helper) {
    var srId = component.get("v.srId");
    window.location.href =
      "topupbalance/?srId=" + srId + "&category=namereserve";
  }
});