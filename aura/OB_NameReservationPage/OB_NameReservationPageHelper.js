({
  fetchNameReservationInfo: function(component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var srId = newURL.get("srId");
    console.log("sr id is " + srId);
    component.set("v.srId", srId);
    var action = component.get("c.getCompanyNameInfo");
    var requestWrap = {
      srId: srId
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        component.set("v.respWrap", respWrap);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  }
});