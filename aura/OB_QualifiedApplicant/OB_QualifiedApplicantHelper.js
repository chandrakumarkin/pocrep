({
  loadData: function(cmp, event) {
    try {
      console.log("===pages init Qualified applicant===");
      var getFormAction = cmp.get("c.createWrap");
      let newURL = new URL(window.location.href).searchParams;
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");
      var pageId = cmp.get("v.pageId")
        ? cmp.get("v.pageId")
        : newURL.get("pageId");
      var flowId = newURL.get("flowId");
      cmp.set("v.srId", srIdParam);
      getFormAction.setParams({
        srId: srIdParam,
        pageId: pageId,
        flowId: flowId
      });
      getFormAction.setCallback(this, function(response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var responseResult = response.getReturnValue();
          console.log("####### responseResult", responseResult);

          cmp.set("v.commandButton", responseResult.ButtonSection);

          cmp.set("v.strucDiagramFile", responseResult.uploadedFileName);

          cmp.set("v.qualifyWraplist", responseResult);
          
          cmp.set("v.Incorporation_affiliated", responseResult.Incorporation_affiliated);
          cmp.set("v.LicensecertificateofIncorporation", responseResult.LicensecertificateofIncorporation);

          cmp.set("v.amendList", responseResult.amendWrapList);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          cmp.set("v.errorMessage", response.getError()[0].message);
          cmp.set("v.isError", true);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
    }
  },

  createNewAmendHelper: function(cmp, event) {
    try {
      console.log("===pages init Qualified applicant===");
      var getFormAction = cmp.get("c.initAmendment");

      getFormAction.setParams({
        srId: cmp.get("v.srId")
      });
      getFormAction.setCallback(this, function(response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var responseResult = response.getReturnValue();

          var amendWrap = responseResult.amendobj;

          cmp.set("v.newAmendWrap", amendWrap);

          cmp.set("v.showFormAmend", true);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
    }
  },
  hideRecordLayout: function(component, event) {
    component.set("v.isRecordOpen", false);
    component.set("v.ShowAddQuaButton", false);
    component.set("v.ShowListofQualType", true);
    component.set("v.isRegisternedNO", false);
    component.set("v.Qualipurposefields", false);
    component.set("v.SaveSection", false);
    component.set("v.isRegisteredWithDIFC", false);
    //component.set("v.ShowViewdetails", true);
    //component.set("v.LicenseActName", "");
  },

  loadSRDocs: function(component, event) {
    try {
      //var amendmentWrapper = component.get("v.amendList");
      //console.log(amendmentWrapper);
      //var amedId = amendmentWrapper[0].Id;
      //console.log(amedId);
      var srId = component.get("v.srId");
      var action = component.get("c.viewSRDocs");
      //var self = this;

      action.setParams({
        srId: srId
      });

      // set call back
      action.setCallback(this, function(response) {
        console.log("here");
        var state = response.getState();
        console.log("@@@@@@@@@@@@state" + state);
        if (state === "SUCCESS") {
          var result = response.getReturnValue();
          console.log("srDocs>>");
          console.log(result.Certificate_of_Incorporation);
          console.log(result);
          //component.set("v.srDocs", result);

          if (result.Certificate_of_Incorporation)
            component.set(
              "v.certificateIncorporationDocDtl",
              result.Certificate_of_Incorporation
            );
        } else if (state === "INCOMPLETE") {
      //    alert("From server: " + response.getReturnValue());
        } else if (state === "ERROR") {
          var errors = response.getError();
          if (errors) {
            if (errors[0] && errors[0].message) {
              helper.displayToast("Error", errors[0].message);
              console.log("Error message: " + errors[0].message);
            }
          } else {
            console.log("Unknown error");
          }
        }
      });
      // enqueue the action
      //$A.enqueueAction(action);
    } catch (err) {
      console.log(err);
      console.log(err.message);
    }
  },
  /**
   * Display a message
   */
  displayToast: function(title, message) {
    console.log("displayToast");
    var toast = $A.get("e.force:showToast");

    // For lightning1 show the toast
    if (toast) {
      //fire the toast event in Salesforce1
      toast.setParams({
        title: title,
        message: message,
        mode: "sticky"
      });

      toast.fire();
    } else {
      // otherwise throw an alert
    //  alert(title + ": " + message);
    }
  },

  associateFile: function(
    component,
    event,
    helper,
    parentId,
    docMasterCode,
    filesArr,
    srDocId
  ) {
    console.log("associateFile");
    var action = component.get("c.saveChunkBlob2");
    var files = filesArr; //component.get("v.fileToBeUploaded");
    var file;
    var self = this;
    if (files && files.length > 0) {
      file = files[0][0];
      console.log(file);
      console.log(file.size);
      if (file.size > self.MAX_FILE_SIZE) {
        component.set(
          "v.fileName",
          "Alert : File size cannot exceed " +
            self.MAX_FILE_SIZE +
            " bytes.\n" +
            " Selected file size: " +
            file.size
        );
        return;
      }
      var objFileReader = new FileReader();
      objFileReader.onloadend = function() {
        var fileContents = objFileReader.result;
        var base64 = "base64,";
        var dataStart = fileContents.indexOf(base64) + base64.length;

        fileContents = fileContents.substring(dataStart);
        console.log("===method 1===");
        console.log(action);
        // call the uploadProcess method
        self.uploadProcess(
          action,
          component,
          file,
          fileContents,
          parentId,
          docMasterCode,
          srDocId
        );
      };
      objFileReader.readAsDataURL(file);
    } else {
    }
  },

  uploadProcess: function(
    action,
    component,
    file,
    fileContents,
    parentId,
    docMasterCode,
    srDocId
  ) {
    // set a default size or startpostiton as 0
    var startPosition = 0;
    // calculate the end size or endPostion using Math.min() function which is return the min. value
    var endPosition = Math.min(
      fileContents.length,
      startPosition + this.CHUNK_SIZE
    );
    console.log("==uploadProcess===");
    console.log(action);
    // start with the initial chunk, and set the attachId(last parameter)is null in begin
    this.uploadInChunk(
      action,
      component,
      file,
      fileContents,
      startPosition,
      endPosition,
      "",
      parentId,
      docMasterCode,
      srDocId
    );
  },
  uploadInChunk: function(
    action,
    component,
    file,
    fileContents,
    startPosition,
    endPosition,
    attachId,
    parentId,
    docMasterCode,
    srDocId
  ) {
    try {
      console.log("==uploadInChunk===");
      console.log(action);
      console.log("component--" + JSON.stringify(component));
      // call the apex method 'saveChunk'
      let newURL = new URL(window.location.href).searchParams;
      var srId = newURL.get("srId");
      var getchunk = fileContents.substring(startPosition, endPosition);
      var self = this;
      var srDocId = srDocId ? srDocId : " ";
      console.log("srDocId>>>" + srDocId);
      console.log("parentId-----" + parentId);
      console.log("documentMasterCode--" + docMasterCode);
      console.log("file.name--" + file.name);
      console.log("file.type--" + file.type);
      console.log("action--" + action);
      console.log(srId);

      //console.log(encodeURIComponent(getchunk));
      action.setParams({
        parentId: parentId,
        srId: srId,
        fileName: file.name,
        base64Data: encodeURIComponent(getchunk),
        contentType: file.type,
        fileId: attachId,
        documentMasterCode: docMasterCode,
        strDocumentId: srDocId
      });
      console.log("am here");
      // set call back
      action.setCallback(this, function(response) {
        // store the response / Attachment Id
        attachId = response.getReturnValue();
        console.log("attachId>>" + attachId);
        var state = response.getState();
        if (state === "SUCCESS") {
          // update the start position with end postion
          console.log("inside----");
          startPosition = endPosition;
          endPosition = Math.min(
            fileContents.length,
            startPosition + this.CHUNK_SIZE
          );
          // check if the start postion is still less then end postion
          // then call again 'uploadInChunk' method ,
          // else, diaply alert msg and hide the loading spinner
          if (startPosition < endPosition) {
            this.uploadInChunk(
              action,
              component,
              file,
              fileContents,
              startPosition,
              endPosition,
              attachId,
              parentId,
              docMasterCode,
              srDocId
            );
          } else {
            console.log("your File is uploaded successfully>>" + attachId);
            //component.set("v.showLoadingSpinner", false);
          }
          // handel the response errors
        } else if (state === "INCOMPLETE") {
          alert("From server: " + response.getReturnValue());
        } else if (state === "ERROR") {
          var errors = response.getError();
          if (errors) {
            if (errors[0] && errors[0].message) {
              self.displayToast("Error", errors[0].message);
              console.log("Error message: " + errors[0].message);
            }
          } else {
            console.log("Unknown error");
          }
        }
      });
      // enqueue the action
      $A.enqueueAction(action);
    } catch (err) {
      console.log("@@@@@ Error " + err.message);
    }
  },

  show: function(cmp, event) {
    var spinner = cmp.find("mySpinner");
    $A.util.removeClass(spinner, "slds-hide");
    $A.util.addClass(spinner, "slds-show");
  },
  hide: function(cmp, event) {
    var spinner = cmp.find("mySpinner");
    $A.util.removeClass(spinner, "slds-show");
    $A.util.addClass(spinner, "slds-hide");
  },

  ClearRegNoFields: function(component, event) {
    component.set(
      "v.qualifyWraplist.amendment.Select_the_Qualified_Applicant_Type__c",
      null
    );
    //Govt Entity Fields
    component.set("v.qualifyWraplist.amendment.Govt_Entity_Name__c", null);
    component.set("v.qualifyWraplist.amendment.Emirate__c", null);
    component.set("v.qualifyWraplist.amendment.Decree_No__c", null);
    // Fund Fields
    component.set("v.qualifyWraplist.amendment.Name_of_the_Fund__c", null);
    component.set("v.qualifyWraplist.amendment.Registration_No__c", null);
    component.set("v.qualifyWraplist.amendment.Jurisdiction__c", null);
    component.set(
      "v.qualifyWraplist.amendment.Financial_Service_Regulator__c",
      null
    );
    component.set(
      "v.qualifyWraplist.amendment.Name_of_the_Fund_Administrator_Manager__c",
      null
    );
    //Authorised Fields
    component.set("v.qualifyWraplist.amendment.Company_Name__c", null);
    component.set("v.qualifyWraplist.amendment.Trading_Name__c", null);
    component.set("v.qualifyWraplist.amendment.Former_Name__c", null);
    component.set("v.qualifyWraplist.amendment.Date_of_Registration__c", null);
    component.set("v.qualifyWraplist.amendment.Place_of_Registration__c", null);
    component.set("v.qualifyWraplist.amendment.Date_of_Registration__c", null);
    component.set(
      "v.qualifyWraplist.amendment.Country_of_Registration__c",
      null
    );
    component.set("v.qualifyWraplist.amendment.Registered_No__c", null);
    component.set("v.qualifyWraplist.amendment.Telephone_No__c", null);
    component.set("v.qualifyWraplist.amendment.Address__c", null);
    component.set(
      "v.qualifyWraplist.amendment.Apartment_or_Villa_Number_c__c",
      null
    );
    component.set("v.qualifyWraplist.amendment.PO_Box__c", null);
    component.set(
      "v.qualifyWraplist.amendment.Emirate_State_Province__c",
      null
    );
    component.set("v.qualifyWraplist.amendment.Permanent_Native_City__c", null);
    component.set(
      "v.qualifyWraplist.amendment.Permanent_Native_Country__c",
      null
    );
  },
  ClearRegYesFields: function(component, event) {
    component.set(
      "v.qualifyWraplist.amendment.Qualified_Applicant_Type__c",
      null
    );
    component.set("v.qualifyWraplist.amendment.Entity_Name__c", null);
    component.set("v.qualifyWraplist.amendment.Registration_No__c", null);
  },
  ClearAviaField: function(component, event) {
    component.set(
      "v.qualifyWraplist.service.Details_of_the_qualifying_purpose__c",
      null
    );
  },
  ClearStrucFields: function(component, event) {
    component.set(
      "v.qualifyWraplist.service.Include_regulated_entity__c",
      null
    );
    component.set(
      "v.qualifyWraplist.service.Name_of_the_Regulated_Firm__c",
      null
    );
    component.set("v.qualifyWraplist.service.Registration_No__c", null);
    component.set("v.qualifyWraplist.service.Jurisdiction__c", null);
    component.set("v.qualifyWraplist.service.I_Agree_Prescribed__c", false);
  },
  createToast: function(component, event, errorMessagr) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissable",
      message: errorMessagr,
      type: "error",
      duration: 500
    });
    toastEvent.fire();
  }
});