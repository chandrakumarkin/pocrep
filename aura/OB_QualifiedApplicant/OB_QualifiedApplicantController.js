({
  doInit: function(cmp, event, helper) {
    helper.loadData(cmp, event);
  },

  createNewAmend: function(component, event, helper) {
    helper.createNewAmendHelper(component, event);
  },

  showToast: function(title, message) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      title: title,
      message: message
    });
    toastEvent.fire();
  },

  closeModel: function(cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },

  handleButtonAct: function(component, event, helper) {
    var Qualifyingtype = component.get(
      "v.qualifyWraplist.service.Qualified_Applicant_Purpose__c"
    );

    if (
      Qualifyingtype == "Qualified Applicant" ||
      Qualifyingtype == "Qualified Applicant/Qualified Purpose"
    ) {
      if (!component.get("v.amendList").length > 0) {
        //component.set("v.errorMessage", "Please add at least one applicant");
        //component.set("v.isError", true);
        helper.createToast(
          component,
          event,
          "Please add at least one applicant"
        );
        return;
      }
    }

    var activityname = component.get(
      "v.qualifyWraplist.service.Select_Qualified_Purpose__c"
    );
    debugger;
    var registered = component.get(
      "v.qualifyWraplist.amendment.Is_this_Entity_registered_with_DIFC__c"
    );
    var QualifyingPurpose = component.get(
      "v.qualifyWraplist.service.Please_state_your_qualifying_purpose__c"
    );

    var allValid = true;
    //component.set("v.errorMessage", "");
    // component.set("v.isError", false);

    let fields = component.find("requiredField");
    for (var i = 0; i < fields.length; i++) {
      var inputFieldCompValue = fields[i].get("v.value");
      //console.log(inputFieldCompValue);
      if (!inputFieldCompValue && fields[i] && fields[i].get("v.required")) {
        console.log("no value");
        //console.log('bad');
        fields[i].reportValidity();
        allValid = false;
      }
    }
    if (!allValid) {
      console.log("Error MSG");
      //component.set("v.errorMessage", "Please fill required field");
      helper.createToast(component, event, "Please fill required field");
      //component.set("v.isError", true);
      return;
    }

    if (Qualifyingtype == "Qualified Applicant") {
      var srWrap = component.get("v.qualifyWraplist.service");

      console.log("clear sr fields");
      const srFieldList = [
        "Jurisdiction__c",
        "Registration_No__c",
        "Name_of_the_Regulated_Firm__c",
        "Include_regulated_entity__c",
        "Details_of_the_qualifying_purpose__c",
        "Select_Qualified_Purpose__c"
      ];

      for (let field of srFieldList) {
        srWrap[field] = "";
      }
      component.set("v.qualifyWraplist.service", srWrap);
    }

    if (Qualifyingtype == "Qualified Purpose") {
      if (activityname == "A structured financing") {
        /* if (
          $A.util.isEmpty(iAgreeIndividual) ||
          $A.util.isUndefined(iAgreeIndividual) ||
          iAgreeIndividual == false
        ) {
          //component.set("v.errorMessage", "Please accept the declaration");
          //component.set("v.isError", true);
          helper.createToast(component, event,"Please accept the declaration");
        } */
        helper.ClearAviaField(component);
      } else if (
        activityname == "A Fintech Structure" ||
        activityname == "A family structure" ||
        activityname == "An Aviation Structure"
      ) {
        helper.ClearStrucFields(component);
      }
    }

    if (registered == "Yes") {
      helper.ClearRegNoFields(component);
    } else if (registered == "No") {
      helper.ClearRegYesFields(component);
    }

    /* var typeofControll = component.find("typeControl");
    if (typeofControll) {
      var cntrl = component.find("typeControl").get("v.value");
      if (!cntrl || cntrl == "" || cntrl.trim().length === 0) {
        console.log("===error==");
        component.set("v.errorMessage", "Please select Type of Control");
        component.set("v.isError", true);
      } else {
        component.set("v.errorMessage", "");
        component.set("v.isError", false);
      }
    } */

    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var isError = component.get("v.isError");
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        ButtonId: buttonId,
        pageId: pageID,
        srOBj: component.get("v.qualifyWraplist.service"),
        docMap: component.get("v.docMasterContentDocMap")
      });

      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");
          var respWrap = response.getReturnValue();
          console.log(response.getReturnValue());
          if (respWrap.errorMessage == null) {
            window.open(response.getReturnValue().pageActionName, "_self");
          } else {
            //component.set("v.errorMessage", respWrap.errorMessage);
            //component.set("v.isError", true);
            helper.createToast(component, event, errorMessagr);
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },
  getIAgree: function(component, event, helper) {
    var agreeStrucFinan = component.find("IAgreeID").get("v.value");
    component.set("v.IsIAgreeStrucFinan", agreeStrucFinan);
    console.log(component.get("v.IsIAgreeStrucFinan"));
  },
  handleFileUploadFinished: function(component, event) {
    // Get the list of uploaded files
    var uploadedFiles = event.getParam("files");
    var fileName = uploadedFiles[0].name;

    var contentDocumentId = uploadedFiles[0].documentId;
    console.log(contentDocumentId);
    var docMasterCode = event.getSource().get("v.name");
    console.log(docMasterCode);
    console.log(component.find(docMasterCode).get("v.value"));
    console.log(fileName);
    if (component.find(docMasterCode))
      component.find(docMasterCode).set("v.value", fileName);

    console.log(component.find(docMasterCode).get("v.value"));
    var mapDocValues = {};
    var docMap = component.get("v.docMasterContentDocMap");
    console.log(docMap);
    for (var key in docMap) {
      mapDocValues[key] = docMap[key];
    }

    mapDocValues[docMasterCode] = contentDocumentId;
    console.log(mapDocValues);

    component.set("v.docMasterContentDocMap", mapDocValues);
    console.log(
      JSON.parse(JSON.stringify(component.get("v.docMasterContentDocMap")))
    );

    //alert("Files uploaded : " + uploadedFiles.length);
  }
});