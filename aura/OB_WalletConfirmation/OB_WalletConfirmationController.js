({
	doInit : function(component, event, helper) {
		let newURL = new URL(window.location.href).searchParams;
		var srId = newURL.get("srId");
		var amount = newURL.get("amount");
		component.set("v.amount",amount);
		var receiptRef = newURL.get("receiptRef");
		component.set("v.receiptRef",receiptRef);
		component.set("v.srId",srId);
		//var itemCode = newURL.get("itemCode");
        var category = newURL.get("category") ? newURL.get("category") : '';
		var action = component.get("c.viewProductInformation");   
		action.setParams({
			"srId" : srId,
			"category":category
		});
		action.setCallback(this, function(response) {
			var retVal = response.getReturnValue();
			console.log(retVal);
			var state= response.getState();
			if(state=="SUCCESS"){
				component.set("v.productInfo",retVal.lstPriceItem);
				component.set("v.responseResult",retVal);
				console.log("inside success submit for name submitForApproval");

				console.log("calling processRenewalSR");
				helper.processRenewalSR(component, event, helper);

			}
			else if (state === "ERROR") {
				// Process error returned by server
				let errors = response.getError();
				let message = 'Unknown error'; // Default error message
				// Retrieve the error message sent by the server
				if (errors && Array.isArray(errors) && errors.length > 0) {
					message = errors[0].message;
				}
				// Display the message
				console.error(message);
			}
		});
		$A.enqueueAction(action); 
	},
	closeModel: function(component, event, helper) { 
        component.set("v.isOpen", false);
	}
	
	
	
})