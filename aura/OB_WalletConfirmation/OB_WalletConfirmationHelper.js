({
	updateSRPriceItems: function(component, fieldName, elementId, srId) {
        var action = component.get("c.updateSRLines");
        action.setParams({
            "srId": srId
        });
        action.setCallback(this, function(response) {
            var state= response.getState();
            if (state == "SUCCESS") {
                var resp = response.getReturnValue();
                console.log(resp);
            }
            else if (state === "ERROR") {
                // Process error returned by server
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
            }
        });
        $A.enqueueAction(action);
    },

    processRenewalSR: function(component, event, helper) {
      
        var action = component.get("c.processNameRenewalSR");
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get("srId");
        console.log('@@@@@@@@@ srId ',srId);
        var requestWrap = {
            "srId" : srId        
          };
         action.setParams({"requestWrapParam": JSON.stringify(requestWrap)});
         action.setCallback(this, function(response) 
         {
      
         var state = response.getState();
             if (state === "SUCCESS") {
                 var respWrap = response.getReturnValue();
                 console.log('@@@@@@@ in processRenewalSR respWrap ',respWrap);
                 
             }else{
                 console.log('@@@@@ Error '+response.getError()[0].message);
                 console.log('@@@@@ Error Location '+response.getError()[0].stackTrace);
             }
         });
         $A.enqueueAction(action);
      }
    
    
})