({
    showAssessment: function (component, event, helper) {
      component.set("v.showSpinner", true);
      console.log("her?");
      var recType = event.currentTarget.dataset.amedtype;
      
      var action = component.get("c.createAssessmentSRs");
      action.setParams({ recType : recType});
      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {     
          component.set("v.showSpinner", false); 
          var respWrap = response.getReturnValue();
          console.log('---------JJJJJ-------'+respWrap);
          if (respWrap.errorMessage == null) {
              window.location.href = "ob-processflow" + respWrap.pageFlowURl;
          } else {
            helper.showToast(
              component,
              event,
              helper,
              respWrap.errorMessage,
              "error"
            );
          }
        } else {
          component.set("v.showSpinner", false);
          alert("@@@@@ Error " + response.getError()[0].message);
          alert(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    },
  });