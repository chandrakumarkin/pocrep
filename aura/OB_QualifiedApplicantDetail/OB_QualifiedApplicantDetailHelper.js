({
  MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB
  CHUNK_SIZE: 7500000, //Chunk Max size 750Kb

  getAccountFilter: function(component, event) {
    component.set("v.soqlFieldName", "");
    component.set("v.objectAPIName", "Account");
    component.set("v.soqlFilterVariable", "");

  

    var qualAppTypevalue = component.get(
      "v.AmendWrap.Qualified_Applicant_Type__c"
    );

    if (!qualAppTypevalue) {
      component.set("v.objectAPIName", "");
    }

   // alert(qualAppTypevalue);

    if (qualAppTypevalue == "Authorised Firm") {
      var query =
        "SELECT Id, Name, Registration_License_No__c  FROM Account WHERE Name like '%searchString%' AND (ROC_Status__c='Active'  OR ROC_Status__c='Not Renewed')  AND Company_Type__c='Financial - related'";
      component.set("v.soqlFilterVariable", query);
      console.log("!!!!!!!!!!!!!!!!!Query" + query);
    }
  
    if (qualAppTypevalue == "Others - DIFC Qualifying Applicant") {
     
      var query =
        "SELECT Id, Name, Registration_License_No__c  FROM Account WHERE Name like '%searchString%' AND (OB_Sector_Classification__c !='Prescribed Companies') AND (Company_Type__c = 'Financial - related' OR Company_Type__c = 'Non - financial') AND (ROC_Status__c='Active' OR ROC_Status__c='Not Renewed')";
      component.set("v.soqlFilterVariable", query);
      console.log("!!!!!!!!DIFC!!!!!!!!!Query" + query);
    }
    
    if (qualAppTypevalue == "Fintech") {
      var query =
        "SELECT Id, Name, Registration_License_No__c  FROM Account WHERE Name like '%searchString%' AND (ROC_Status__c='Active'  OR ROC_Status__c='Not Renewed')  AND OB_Sector_Classification__c='FinTech'";
      component.set("v.soqlFilterVariable", query);

      console.log("!!!!!!!!!!!!!!!!!Query" + query);
    }
    if (qualAppTypevalue == "Holding Company") {
      var query =
        "SELECT Account__r.id,Name, Account__c,Account__r.name,Activity__r.name,Account__r.Registration_License_No__c from License_Activity__c where (Account__r.ROC_Status__c ='Active' OR Account__r.ROC_Status__c='Not Renewed') AND Activity__r.name like '%Holding company%' AND Account__r.name like'%searchString%'";
      component.set("v.soqlFilterVariable", query);
      component.set("v.soqlFieldName", "Account__r.name");
      component.set("v.objectAPIName", "License_Activity__c");
      console.log("!!!!!!!!!!!!!!!!!Query" + query);
    }
    if (qualAppTypevalue == "Family Office") {
      /* var query =
        "SELECT Id, Name, Registration_License_No__c  FROM Account WHERE Name like '%searchString%' AND (ROC_Status__c='Active'  OR ROC_Status__c='Not Renewed') AND Active_License__r.Name like '%Single Family Office%'";
      component.set("v.soqlFilterVariable", query);
      console.log("!!!!!!!!!!!!!!!!!Query" + query); */
      var query =
        "SELECT Account__r.id,Name, Account__c,Account__r.name,Activity__r.name,Account__r.Registration_License_No__c from License_Activity__c where (Account__r.ROC_Status__c ='Active' OR Account__r.ROC_Status__c='Not Renewed') AND Activity__r.name like '%Single Family Office%' AND Account__r.name like'%searchString%'";
      component.set("v.soqlFilterVariable", query);
      component.set("v.soqlFieldName", "Account__r.name");
      component.set("v.objectAPIName", "License_Activity__c");
    }
    if (qualAppTypevalue == "Foundation") {
      var query =
        "SELECT Id, Name, Registration_License_No__c  FROM Account WHERE Name like '%searchString%' AND (ROC_Status__c='Active'  OR ROC_Status__c='Not Renewed')  AND OB_Sector_Classification__c='Foundation'";
      component.set("v.soqlFilterVariable", query);
      console.log("!!!!!!!!!!!!!!!!!Query" + query);
    }
    if (qualAppTypevalue == "Fund") {
      /* var query =
        "SELECT Id,Name,(Select Sector_Classification__c  from License_Activities__r where Sector_Classification__c ='Proprietary Investment') , Registration_License_No__c  FROM Account WHERE Name like '%searchString%' AND (ROC_Status__c='Active'  OR ROC_Status__c='Not Renewed') ";
      component.set("v.soqlFilterVariable", query);
      console.log("!!!!!!!!!!!!!!!!!Query" + query); */
      var query =
        "SELECT Account__r.id,Name, Account__c,Account__r.name,Activity__r.name,Account__r.Registration_License_No__c from License_Activity__c where (Account__r.ROC_Status__c ='Active' OR Account__r.ROC_Status__c='Not Renewed') AND Activity_Name_Eng__c ='Investment Fund' AND Account__r.name like'%searchString%'";
      component.set("v.soqlFilterVariable", query);
      component.set("v.soqlFieldName", "Account__r.name");
      component.set("v.objectAPIName", "License_Activity__c");
    }

    console.log('qualAppTypevalue==>'+qualAppTypevalue);

  },

  getAccountFilterNo: function(component, event) 
  {

    component.set("v.soqlFieldName", "");
    component.set("v.objectAPIName", "Account");
    component.set("v.soqlFilterVariable", "");

    var qualAppTypevalue = component.get("v.AmendWrap.Select_the_Qualified_Applicant_Type__c" );

    if (!qualAppTypevalue) 
    {
      component.set("v.objectAPIName", "");
    }

    if (qualAppTypevalue == "UBO of a DIFC Qualifying applicant" ||
        qualAppTypevalue == "Family owned business with a larger presence in the UAE" ||
        qualAppTypevalue == "Affiliates of a DIFC Qualifying Applicant") 
      {
      var query =
        "SELECT Id, Name, Registration_License_No__c  FROM Account WHERE Name like '%searchString%' AND (ROC_Status__c='Active'  OR ROC_Status__c='Not Renewed')  AND Company_Type__c!='Retail'  and Sub_Legal_Type_of_Entity__c!='Prescribed Company'";
      component.set("v.soqlFilterVariable", query);
      console.log("!!!!!!!!!!!!!!!!!Query" + query);
    }
      else {
          if (qualAppTypevalue == "Shareholder of a DIFC Qualifying applicant")
      {
      var query =
        "SELECT Id, Name, Registration_License_No__c  FROM Account WHERE Name like '%searchString%' AND (ROC_Status__c='Active'  OR ROC_Status__c='Not Renewed')  AND (Legal_Entity_Type__c ='Private Company' OR Legal_Entity_Type__c ='Public Company' OR Legal_Entity_Type__c ='Public Company')";
      component.set("v.soqlFilterVariable", query);
      console.log("!!!!!!!!!!!!!!!!!Query" + query);
     }
      }
   
      if (qualAppTypevalue == "Family owned business with a larger presence in the UAE"){
          component.set("v.AmendWrap.Permanent_Native_Country__c",'United Arab Emirates' );
           component.set("v.AmendWrap.Country_of_Registration__c",'United Arab Emirates' );
          
      }

    

  },

  getListofShareholders: function(component, event,regId) 
  {

    component.set("v.ShareholderNames", "");
    var tempRegistrationNo = regId;
    var qualAppTypevalue = component.get("v.AmendWrap.Select_the_Qualified_Applicant_Type__c" );

   

    if (qualAppTypevalue == "Shareholder of a DIFC Qualifying applicant" && tempRegistrationNo!=null)
    {
 
      console.log("tempRegistrationNo===>" + tempRegistrationNo);

      var action = component.get("c.getComanySharholders");
      action.setParams({
        RegistrationNo : tempRegistrationNo
        });

        action.setCallback(this,function(response){
          var state = response.getState();
          if (state === "SUCCESS")
          {
            var plValues = [];
         
            for (var i = 0; i < response.getReturnValue().length; i++) {
              
                plValues.push({
                    label: response.getReturnValue()[i].optValue,
                    value: response.getReturnValue()[i].recordId
                });
            }
              component.set("v.ShareholderNames", plValues);
          }
          else {
              console.log("Failed with state: " + state);
               }
          });
          //send action off to be executed
              $A.enqueueAction(action);
      }

  },

  viewExisting: function(component, event, helper) {

    component.set("v.showExistingShareholders",true);

    var action = component.get("c.getSelectedShareHolders");
    action.setParams({
      amendIds : component.get("v.AmendWrap.Id")
      });

      action.setCallback(this,function(response){
        var state = response.getState();
      
        if (state === "SUCCESS")
        {
          var plValues = [];
          var selectedValues = [];
        
          for (var i = 0; i < response.getReturnValue().length; i++) {
             if(response.getReturnValue()[i].isSelected){
               selectedValues.push(
               response.getReturnValue()[i].recordId
               );
            }
             
              plValues.push({
                  label: response.getReturnValue()[i].optValue,
                  value: response.getReturnValue()[i].recordId
              });

            
            
          }
          component.set("v.ShareholderNames", plValues);
          component.set("v.selectedGenreList", selectedValues);
        }
        else {
            console.log("Failed with state: " + state);
             }
        });
        //send action off to be executed
            $A.enqueueAction(action);

  },


  associateFile: function(
    component,
    event,
    helper,
    parentId,
    docMasterCode,
    filesArr,
    srDocId
  ) {
    console.log("associateFile");
    var action = component.get("c.saveChunkBlob2");
    var files = filesArr; //component.get("v.fileToBeUploaded");
    var file;
    var self = this;
    debugger;
    if (files && files.length > 0) {
      file = files[0][0];
      console.log(file);
      console.log(file.size);
      if (file.size > self.MAX_FILE_SIZE) {
        component.set(
          "v.fileName",
          "Alert : File size cannot exceed " +
            self.MAX_FILE_SIZE +
            " bytes.\n" +
            " Selected file size: " +
            file.size
        );
        return;
      }
      var objFileReader = new FileReader();
      objFileReader.onloadend = function() {
        var fileContents = objFileReader.result;
        var base64 = "base64,";
        var dataStart = fileContents.indexOf(base64) + base64.length;

        fileContents = fileContents.substring(dataStart);
        console.log("===method 1===");
        console.log(action);
        // call the uploadProcess method
        self.uploadProcess(
          action,
          component,
          file,
          fileContents,
          parentId,
          docMasterCode,
          srDocId
        );
      };
      objFileReader.readAsDataURL(file);
    } else {
    }
  },

  uploadProcess: function(
    action,
    component,
    file,
    fileContents,
    parentId,
    docMasterCode,
    srDocId
  ) {
    // set a default size or startpostiton as 0
    var startPosition = 0;
    // calculate the end size or endPostion using Math.min() function which is return the min. value
    var endPosition = Math.min(
      fileContents.length,
      startPosition + this.CHUNK_SIZE
    );
    console.log("==uploadProcess===");
    console.log(action);
    // start with the initial chunk, and set the attachId(last parameter)is null in begin
    this.uploadInChunk(
      action,
      component,
      file,
      fileContents,
      startPosition,
      endPosition,
      "",
      parentId,
      docMasterCode,
      srDocId
    );
  },
  uploadInChunk: function(
    action,
    component,
    file,
    fileContents,
    startPosition,
    endPosition,
    attachId,
    parentId,
    docMasterCode,
    srDocId
  ) {
    try {
      console.log("==uploadInChunk===");
      console.log(action);
      console.log("component--" + JSON.stringify(component));
      // call the apex method 'saveChunk'
      let newURL = new URL(window.location.href).searchParams;
      var srId = newURL.get("srId");
      var getchunk = fileContents.substring(startPosition, endPosition);
      var self = this;
      var srDocId = srDocId ? srDocId : " ";
      console.log("srDocId>>>" + srDocId);
      console.log("parentId-----" + parentId);
      console.log("documentMasterCode--" + docMasterCode);
      console.log("file.name--" + file.name);
      console.log("file.type--" + file.type);
      console.log("action--" + action);
      console.log(srId);

      //console.log(encodeURIComponent(getchunk));
      action.setParams({
        parentId: parentId,
        srId: srId,
        fileName: file.name,
        base64Data: encodeURIComponent(getchunk),
        contentType: file.type,
        fileId: attachId,
        documentMasterCode: docMasterCode,
        strDocumentId: srDocId
      });
      console.log("am here");
      // set call back
      action.setCallback(this, function(response) {
        // store the response / Attachment Id
        attachId = response.getReturnValue();
        console.log("attachId>>" + attachId);
        var state = response.getState();
        if (state === "SUCCESS") {
          // update the start position with end postion
          console.log("inside----");
          startPosition = endPosition;
          endPosition = Math.min(
            fileContents.length,
            startPosition + this.CHUNK_SIZE
          );
          // check if the start postion is still less then end postion
          // then call again 'uploadInChunk' method ,
          // else, diaply alert msg and hide the loading spinner
          if (startPosition < endPosition) {
            this.uploadInChunk(
              action,
              component,
              file,
              fileContents,
              startPosition,
              endPosition,
              attachId,
              parentId,
              docMasterCode,
              srDocId
            );
          } else {
            console.log("your File is uploaded successfully>>" + attachId);
            //component.set("v.showLoadingSpinner", false);
          }
          // handel the response errors
        } else if (state === "INCOMPLETE") {
          alert("From server: " + response.getReturnValue());
        } else if (state === "ERROR") {
          var errors = response.getError();
          if (errors) {
            if (errors[0] && errors[0].message) {
              self.displayToast("Error", errors[0].message);
              console.log("Error message: " + errors[0].message);
            }
          } else {
            console.log("Unknown error");
          }
        }
      });
      // enqueue the action
      $A.enqueueAction(action);
    } catch (err) {
      console.log("@@@@@ Error " + err.message);
    }
  },

  show: function(cmp, event) {
    var spinner = cmp.find("mySpinner");
    $A.util.removeClass(spinner, "slds-hide");
    $A.util.addClass(spinner, "slds-show");
  },
  hide: function(cmp, event) {
    var spinner = cmp.find("mySpinner");
    $A.util.removeClass(spinner, "slds-show");
    $A.util.addClass(spinner, "slds-hide");
  },
  createToast: function(component, event, errorMessagr) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissable",
      message: errorMessagr,
      type: "error",
      duration: 500
    });
    toastEvent.fire();
  }
});