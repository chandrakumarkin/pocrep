({
  doInit: function(component, event, helper) {
    debugger;
    
    
    let newURL = new URL(window.location.href).searchParams;
    var srId = newURL.get("srId");
    component.set("v.srId", srId);
    console.log(component.get("v.AmendWrap.Id"));

    if (component.get("v.AmendWrap.Id") == null) {
      component.set("v.objectAPIName", "");
    }

    if (
      component.get("v.AmendWrap.Id") != null ||
      component.get("v.AmendWrap.Id") != undefined
    ) {
      var action = component.get("c.viewSRDocs");

      action.setParams({
        srId: component.get("v.srId"),
        amendmentId: component.get("v.AmendWrap.Id")
      });
      action.setCallback(this, function(response) {
        var state = response.getState();
        console.log(state);

        if (state === "SUCCESS") {
          var result = response.getReturnValue();
          
            if(result.Commercial_License_certificate_of_Incorporation !== undefined) {
               component
                .find("Commercial_License_certificate_of_Incorporation")
                .set("v.value", result.Commercial_License_certificate_of_Incorporation.FileName);
            }
           if(result.Certificate_of_Incorporation_affiliated_entity !== undefined) {   
             component
                .find("Certificate_of_Incorporation_affiliated_entity")
                .set("v.value", result.Certificate_of_Incorporation_affiliated_entity.FileName);
           }
            if(result.Certificate_of_Incorporation !== undefined) {  
             component
                .find("Certificate_of_Incorporation")
                .set("v.value", result.Certificate_of_Incorporation.FileName);
            }
          if(result.Transaction_structure_diagram !== undefined) {  
              
             component
                .find("Transaction_structure_diagram")
                .set("v.value", result.Transaction_structure_diagram.FileName);
            }
         if(result.Proof_of_Type_of_Presence_in_UAE !== undefined) {  
           component
                .find("Proof_of_Type_of_Presence_in_UAE")
                .set("v.value", result.Proof_of_Type_of_Presence_in_UAE.FileName);
        
        }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      helper.viewExisting(component, event);
      $A.enqueueAction(action);
     
    }
  },

  newSave: function(component, event, helper) {
    var allValid = true;
    //component.set("v.errorMessage", "");
    //component.set("v.isError", false);
    var qualAppTypevalue = component.get(
      "v.AmendWrap.Select_the_Qualified_Applicant_Type__c"
    );
    var fileUploaded ;
    var fileUploadedDIFCEntity;
    var fileUploadedAffiliatedEntity;
    var groupstructure;
    var proofOfResidance;
    
    if (qualAppTypevalue == "Family owned business with a larger presence in the UAE") {
        fileUploadedAffiliatedEntity = component.find("Commercial_License_certificate_of_Incorporation");
        proofOfResidance   = component.find("Proof_of_Type_of_Presence_in_UAE");
    }
    
    if (qualAppTypevalue == "Affiliates of a DIFC Qualifying Applicant") {
        fileUploadedDIFCEntity = component.find("Certificate_of_Incorporation_affiliated_entity");
        groupstructure = component.find("Transaction_structure_diagram");
    }
      
    if (qualAppTypevalue == "Authorised Firm") {
        fileUploaded = component.find("Certificate_of_Incorporation");
    }
      
    let fields = component.find("requiredField");
      
    for (var i = 0; i < fields.length; i++) {
      var inputFieldCompValue = fields[i].get("v.value");
      //console.log(inputFieldCompValue);
      if (!inputFieldCompValue && fields[i] && fields[i].get("v.required")) {
        console.log("no value");
        //console.log('bad');
        fields[i].reportValidity();
        allValid = false;
      }
      
     console.log("qualAppTypevalue####"+qualAppTypevalue);
        
    }

    if (!allValid) {
      console.log("Error MSG");
      //component.set("v.errorMessage", "Please fill required field");
      //component.set("v.isError", true);
      helper.createToast(component, event, "Please fill required field");
      return;
    }
    
     if (qualAppTypevalue == "Authorised Firm") {
        if (!fileUploaded.get("v.value")) {
          allValid = false;
          component.find("fileMissing").set("v.value", "upload file");
          return;
        }
      }
      if (qualAppTypevalue == "Affiliates of a DIFC Qualifying Applicant") {
         console.log("qualAppTypevalue%%%####"+fileUploadedDIFCEntity.get("v.value"));
          if (!fileUploadedDIFCEntity.get("v.value")) {
          allValid = false;
          component.find("fileMissing").set("v.value", "upload file");
          return;
          }
          else{
              component.find("fileMissing").set("v.value", '');
          }
      } 
      
      if (qualAppTypevalue == "Affiliates of a DIFC Qualifying Applicant") {
         console.log("qualAppTypevalue%%%####"+fileUploadedDIFCEntity.get("v.value"));
          if (!groupstructure.get("v.value")) {
          allValid = false;
          component.find("fileMissing2").set("v.value", "upload file");
           return;
          } 
          else{
              component.find("fileMissing2").set("v.value", '');
          }
      } 
        
      if (qualAppTypevalue == "Family owned business with a larger presence in the UAE") {
         console.log("qualAppTestTypevalue%%%####"+fileUploadedAffiliatedEntity.get("v.value"));
          if (!fileUploadedAffiliatedEntity.get("v.value")) {
          allValid = false;
          component.find("fileMissing").set("v.value", "upload file");
           return;
          }
          
          if (!proofOfResidance.get("v.value")) {
          allValid = false;
          component.find("fileMissing2").set("v.value", "upload file");
           return;
          }
      } 
       
    var typeValidation = component.get(
      "v.AmendWrap.Type_of_presence_in_the_UAE__c"
    );

    if(qualAppTypevalue ==='Family owned business with a larger presence in the UAE' && typeValidation.split(';').length  < 2){
      helper.createToast(component, event, "Type of presence in the UAE should contain atleast two values.");
      return;
    }

   //  alert(component.get("v.AmendWrap.Shareholder_Name__c").split(',')[0]);
    
    try {
      // var isError = component.get("v.isError");

      let newURL = new URL(window.location.href).searchParams;
      var pageId = newURL.get("pageId");
      var flowId = newURL.get("flowId");

      console.log("======flowId=====");
      debugger;
      var selectedShareholderIdValues ='';
      for (var i= 0 ; i < component.get("v.selectedSahareHolder").length ; i++){
          selectedShareholderIdValues = selectedShareholderIdValues +component.get("v.selectedSahareHolder") +',';
       } 
      
        if(qualAppTypevalue == "Shareholder of a DIFC Qualifying applicant" &&  component.get("v.selectedGenreList").length == 0 && selectedShareholderIdValues ===''){
              helper.createToast(component, event, "Please select atleast one shareholder.");
            return;
        }
        
        if(qualAppTypevalue == "Shareholder of a DIFC Qualifying applicant" && component.get("v.selectedSahareHolder").length == 0 && component.get("v.selectedGenreList").length == 0){
             helper.createToast(component, event, "Please select atleast one shareholder.");
             return;
        }
        
        if(qualAppTypevalue == "Fund"){
             component.set(
              "v.AmendWrap.Company_Name__c ", component.get("v.AmendWrap.Name_of_the_Fund__c")
            );
        }
        
         if (qualAppTypevalue == "Shareholder of a DIFC Qualifying applicant" 
         || qualAppTypevalue == "Affiliates of a DIFC Qualifying Applicant"
         || qualAppTypevalue == "UBO of a DIFC Qualifying applicant"){
         
         component.set("v.AmendWrap.Registered_with_DIFC__c", "Yes");
     }
     else{
           component.set("v.AmendWrap.Registered_with_DIFC__c",  component.get("v.AmendWrap.Is_this_Entity_registered_with_DIFC__c"));
     }
       // alert(JSON.stringify(component.get("v.docMasterContentDocMap")));
       
       component.set("v.showSpinner", true);  
       
      var action = component.get("c.amendSave");
      action.setParams({
        lookupvalue: component.get("v.lookupvalue"),
        sericeobj: component.get("v.serviceWrap"),
        amdobj: component.get("v.AmendWrap"),
        pageId: pageId,
        flowId: flowId,
        docMap: component.get("v.docMasterContentDocMap"),
        selectedSahareHolder: selectedShareholderIdValues
      });
        
      action.setCallback(this, function(response) {
        console.log("===123442===");
        var state = response.getState();
        console.log("state--------", state);
        if (component.isValid() && state === "SUCCESS") {
          console.log("in succsess");
          var responsewrap = response.getReturnValue();
          console.log("responsewrap", responsewrap);
           component.set("v.showSpinner", false);
          if (responsewrap.errorMessage == "This Entity already exist") {
            // component.set("v.errorMessage", responsewrap.errorMessage);
            // component.set("v.isError", true);
            helper.createToast(component, event, responsewrap.errorMessage);
          } else if (responsewrap.errorMessage == "Success") {
            component.set(
              "v.amendList",
              responsewrap.ServiceWrapper.amendWrapList
            );
            component.set("v.updatedAmendObj", responsewrap.updatedAmendObj);

            component.set("v.showFormAmend", false);
          } else {
            // component.set("v.errorMessage", responsewrap.errorMessage);
            //component.set("v.isError", true);
            helper.createToast(component, event, responsewrap.errorMessage);
          }
        } else {
             component.set("v.showSpinner", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log("@@@@@ Error " + response.getError()[0].stackTrace);
          //component.set("v.errorMessage", response.getError()[0].message);
          // component.set("v.isError", true);
          helper.createToast(component, event, response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error);
    }
  },
  getQualifiedAppType: function(component, event, helper) {
    helper.getAccountFilter(component, event);
  },

  //Are you registered with DIFC? No
  //Select Qualifying Applicant Type changed 
  getQualifiedAppTypeNo: function(component, event, helper) {
    helper.getAccountFilterNo(component, event);
  },

  //List of Shareholders
  getListofAccountShareholders: function(component, event, helper) {
    helper.getListofShareholders(component, event);
  },


  clearRegistrationNumber: function(component, event, helper) {
    var accObj = event.getParam("instanceId");

    //component.set("v.AmendWrap.Registration_No__c", "");
  },

  getSelectedValue : function(component, event, helper) {
    component.set("v.selectedSahareHolder",[]);
    var selectedOptionValue = event.getParam("value");

    if(!component.get("v.selectedSahareHolder").includes(selectedOptionValue)){
       component.get("v.selectedSahareHolder").push(selectedOptionValue);
    }

  },
   onChange: function (cmp, evt, helper) {
    cmp.set("v.aAmendWrap.Please_select_if_applicable__c", "");
    var selectedValue = evt.getSource().get("v.value");
    console.log(selectedValue + "-- pie is good.");
    cmp.set(
      "v.AmendWrap.Type_of_Ownership_or_Control__c",
      selectedValue
    );
    cmp.set("v.selectedValue", selectedValue);

    if (
      !$A.util.isEmpty(selectedValue) &&
      !$A.util.isUndefined(selectedValue) &&
      (selectedValue ==
        "The individual holds 25% or more of the shares or ownership in the Company" ||
        selectedValue ==
          "The individual holds 25% or more of the voting rights in the Company")
    ) {
      cmp.set("v.isDependent", true);
    } else {
      cmp.set("v.isDependent", false);
    }

    if (
      !$A.util.isEmpty(selectedValue) &&
      !$A.util.isUndefined(selectedValue) &&
      selectedValue !=
        "Where no natural person has been identified as a UBO, each Director of the Company is deemed to be a UBO" &&
      selectedValue !=
        "The individual members of the Council who are deemed to be a UBO" &&
      selectedValue !=
        "The individual members of the governing body who are deemed to be a UBO"
    ) {
      console.log("====indside check==");
      cmp.set("v.isDisplayForm", true);
    } else {
      cmp.set("v.isDisplayForm", false);
    }
    var value = cmp.get("v.AmendWrap.Type_of_Ownership_or_Control__c");
    console.log("--selectedValue----" + value);
  },
 displayEntityField : function(component, event, helper) {
     if (qualAppTypevalue == "Shareholder of a DIFC Qualifying applicant" 
         || qualAppTypevalue == "Affiliates of a DIFC Qualifying Applicant"
         || qualAppTypevalue == "UBO of a DIFC Qualifying applicant"){
         
         component.set("v.AmendWrap.Registered_with_DIFC__c", "Yes");
     }
     else{
           component.set("v.AmendWrap.Registered_with_DIFC__c",  component.get("v.AmendWrap.Is_this_Entity_registered_with_DIFC__c"));
     }
 },
 handleLookupEvent: function(component, event, helper) {
    console.log("handleLookupEvent");
    component.set("v.ShareholderNames", []);
    var accObj = event.getParam("sObject");
    console.log("lookup--" + JSON.stringify(accObj));
    if (accObj.Registration_License_No__c) {
      component.set(
        "v.AmendWrap.Registration_No__c",
        accObj.Registration_License_No__c
      );
    } else {
      if (accObj.Account__r && accObj.Account__r.Registration_License_No__c)
        component.set(
          "v.AmendWrap.Registration_No__c",
          accObj.Account__r.Registration_License_No__c
        );
    }
   
    helper.getListofShareholders(component, event,  accObj.Registration_License_No__c);
  },

  cancel: function(component, event, helper) {
    component.set("v.showFormAmend", false);
  },
  handleFilesChange: function(component, event, helper) {
    console.log("@@@@@@ in handleFilesChange ");
    var fileName = "No File Selected..";
    if (event.getSource().get("v.files").length > 0) {
      fileName = event.getSource().get("v.files")[0].name;
      console.log(fileName);
    }
    component.set("v.fileName", fileName);
  },

  handleFileUploadFinished: function(component, event) {
    // Get the list of uploaded files
   // debugger;
    var uploadedFiles = event.getParam("files");
      console.log('=====uploadedFiles'+uploadedFiles);
    var fileName = uploadedFiles[0].name;
       console.log('=====fileName'+fileName);
    var contentDocumentId = uploadedFiles[0].documentId;
    console.log('=======contentDocumentId'+contentDocumentId);
    var docMasterCode = event.getSource().get("v.name");
    console.log('=====docMasterCode'+docMasterCode);
   // component.find("fileMissing").set("v.value", "");
    component.find(docMasterCode).set("v.value", fileName);
    
    var mapDocValues = {};
    var docMap = component.get("v.docMasterContentDocMap");
      console.log('=====docMapLoop here');
      console.log('=====docMap'+docMap);
    for (var key in docMap) {
        
      mapDocValues[key] = docMap[key];
    }
   // debugger;
    mapDocValues[docMasterCode] = contentDocumentId;
    console.log('======mapDocValues'+mapDocValues);
    component.set("v.docMasterContentDocMap", mapDocValues);
    console.log('========Final',
      JSON.stringify(component.get("v.docMasterContentDocMap"))
    );

    //alert("Files uploaded : " + uploadedFiles.length);
  }
});