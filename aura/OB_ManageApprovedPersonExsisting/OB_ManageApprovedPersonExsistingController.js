({
  doInit: function (component, event, helper) {},

  deleteAmend: function (component, event, helper) {
    component.set("v.showSpinner", true);
    console.log("IN delete IP");
    try {
      var action = component.get("c.amendRemoveAction");
      var requestWrap = {
        amendObj: component.get("v.amendObj"),
        isAssigned: component.get("v.isAssigned"),
        srId: component.get("v.srId")
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap)
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log(response.getReturnValue());
          if (response.getReturnValue().errorMessage == "no error recorded") {
            /*  component.set(
              "v.AmendListWrapp",
              response.getReturnValue().amendWrapp
            ); */
            component.set("v.respWrap", response.getReturnValue());
            component.set("v.showSpinner", false);
            component.set("v.selectedValue", "");

            /* var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
              mode: "dismissable",
              message: "updated Succsessfully",
              type: "success",
              duration: 500
            });
            toastEvent.fire(); */
          }
        } else {
          component.set("v.showSpinner", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log("@@@@@ Error " + response.getError()[0].stackTrace);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },

  viewDetails: function (component, event, helper) {
    component.set("v.amedIdInFocus", component.get("v.amendObjId"));
    console.log(component.get("v.amedIdInFocus"));
  }
});