({
	doInit: function(component, event, helper) {
        var SRId = component.get("v.recordId");
        console.log('SRId==>'+SRId);
        var action = component.get("c.CalculateRisk");
      	action.setParams({
        	SRID: SRId
      	});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                console.log('response==>'+response.getReturnValue());
                const resultObject = response.getReturnValue();
                component.set("v.RiskMatrix", resultObject);
            }
       	});
        $A.enqueueAction(action);
    }
})