({
    doInit:function(cmp, event, helper){
        console.log('init secretary');
        console.log(cmp.get("v.srWrap"));
    },
	initAmendments : function(cmp, event, helper)
    {
        console.log(JSON.parse(JSON.stringify(cmp.get("v.srWrap"))));
       //cmp.set('v.amedWrap',"{ 'amedObj'  : {'sObjectType': 'HexaBPM_Amendment__c'}}"); 
       //{!v.srWrap.srObj.Id}
        console.log('invoke add');
        var initAmendment = cmp.get('c.initAmendmentDB');
       
        
        var reqWrapPram  =
            {
                srId : cmp.get('v.srWrap').srObj.Id,
                
            };
        
        initAmendment.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram),
                 
            });
        
        initAmendment.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                              console.log('######### respWrap.amedWrap  ',respWrap.amedWrap);
											  cmp.set('v.amedWrap',respWrap.amedWrap);	                                              
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                             
                                          }
                                      }
                                     );
            $A.enqueueAction(initAmendment);
       
    }
    
})