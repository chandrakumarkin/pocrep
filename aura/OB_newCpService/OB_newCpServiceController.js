({
  setCPRenewal: function (component, event, helper) {
    component.set("v.showSpinner", true);
    console.log("her?");

    var action = component.get("c.createCPRenewalService");
    /* var requestWrap = {
            variableName: component.get("v.attributeName")
          };
          action.setParams({ requestWrapParam: JSON.stringify(requestWrap) }); */

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        component.set("v.showSpinner", false);
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        if (respWrap.errorMessage == null) {
          window.location.href = "ob-processflow" + respWrap.pageFlowURl;
        } else {
          helper.showToast(
            component,
            event,
            helper,
            respWrap.errorMessage,
            "error"
          );
        }
      } else {
        component.set("v.showSpinner", false);
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },
});