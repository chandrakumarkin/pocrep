({
	toggleClass: function(component,componentId,className) {
        console.log('componentId'+componentId);
		var modal = component.find(componentId);
        console.log('Modal==>1'+modal);
		$A.util.removeClass(modal,className+'hide');
		$A.util.addClass(modal,className+'open');
	},

	toggleClassInverse: function(component,componentId,className) {
		var modal = component.find(componentId);
        console.log('Modal==>2'+modal);
		$A.util.addClass(modal,className+'hide');
		$A.util.removeClass(modal,className+'open');
	},
    getUrlParameter: function(sParam) {
       	var sPageURL = decodeURIComponent(window.location.search.substring(1)),
       		sURLVariables = sPageURL.split('&'),
       		sParameterName,
          	i;
     	for (i = 0; i < sURLVariables.length; i++) {
          	sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    },
    DisableLinks: function(){
        console.log('disabler');
        document.getElementById('ProceedLink').setAttribute("disabled","");
        document.getElementById('ProceedLink').innerHTML = 'Processing..';
        document.getElementById('AcceptReleaseLink').setAttribute("disabled",""); 
        //document.getElementById('AcceptReleaseLink').innerHTML = 'Processing..';
        document.getElementById('CancelLink').setAttribute("disabled","");
        document.getElementById('CancelLink').innerHTML = 'Processing..';
    },
    EnableLinks: function(){
        document.getElementById('ProceedLink').removeAttribute("disabled");
        document.getElementById('ProceedLink').innerHTML = 'Proceed';
        document.getElementById('AcceptReleaseLink').removeAttribute("disabled");
        //document.getElementById('AcceptReleaseLink').innerHTML = 'View Step';
        document.getElementById('CancelLink').removeAttribute("disabled");
        document.getElementById('CancelLink').innerHTML = 'Cancel';
    },
    showToastMsg:function(component,event,message){
        component.find('notifLib').showToast({
            "variant":"info",
            "title": "Success",
            "message": message
        });
    },
    ChangeStatus: function(component,event){
        console.log('changeStatus');
        console.log(component.get("v.SRId"));
        console.log(component.get("v.step"));
        var action = component.get("c.SaveChanges");
        var selTransition = component.get("v.selTransition");
        var RejReason = component.get("v.RejReason");
        var StepNotes = component.get("v.StepNotes");
        action.setParams({
            selTransition: selTransition,
            RejReason: RejReason,
            StepNotes: StepNotes,
            SRID: component.get("v.SRId"),
            step1: component.get("v.step"),
            mapStepTransition: component.get("v.mapStepTransition")
        });
        action.setCallback(this, function(a){
            var wrapperObj = a.getReturnValue();
            console.log('wrapperObj')
            console.log(wrapperObj);
           	component.set("v.flsErrorCheck",wrapperObj.flsErrorCheck);
            component.set("v.flsErrorMessage",wrapperObj.flsErrorMessage);
            if(wrapperObj.flsErrorCheck){
                //helper.displayToast('Error', wrapperObj.flsErrorMessage);
                if(wrapperObj.flsErrorMessage.indexOf('DML')!=-1){
                    component.set("v.dmlErrorCheck",true);
                }
                this.EnableLinks();
                $A.get('e.force:refreshView').fire();
            }else{
            	window.location.href = '/lightning/r/HexaBPM__Service_Request__c/'+component.get("v.SRId")+'/view';
            }
        });
        $A.enqueueAction(action);
    },
    /**
     * Display a message
     */
    displayToast : function (title, message) {
		console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'title': title,
                'message': message,
                mode:'sticky'
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
	}   
})