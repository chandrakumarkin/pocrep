({
    doInit : function(component, event, helper) {
        
		let newURL = new URL(window.location.href).searchParams;
        var action = component.get("c.fetchPageFlowWrapper");
        var requestWrap = {
        
            pageflowId : new URL(location.href).searchParams.get('flowId'),
            srId : newURL.get('srId')
            
        };
        action.setParams({
        
           	"requestWrapParam": JSON.stringify(requestWrap)
        });
     
        action.setCallback(this, function(response) {

            var state = response.getState();
            if (state === "SUCCESS") {
            	component.set("v.currentpageFlowObj", response.getReturnValue().currentPageFlowObj);
                component.set('v.progress',response.getReturnValue().progressCompletion);
                var prgStrng = parseInt(response.getReturnValue().progressCompletion);
                component.set('v.progressClass','width :'+prgStrng+'%;');
                console.log(response.getReturnValue().currentPageFlowObj);
            }
            else{
            	console.log('@@@@@ Error '+response.getError()[0].message);
			}
        });
        $A.enqueueAction(action);
    }
        
        /*
        let newURL = new URL(window.location.href).searchParams;
        var getProgress = component.get('c.getPageProgress');
        getProgress.setParams({
            "srId": newURL.get('srId')
        });
        getProgress.setCallback(this,function(response) {
            var state = response.getState();
            if (state === "SUCCESS"){
                var prgs = response.getReturnValue();
                var prgStrng = parseInt(prgs);
                component.set('v.progress',prgs);
                component.set('v.progressClass','width :'+prgStrng+'%;');
            }            
        });
        $A.enqueueAction(getProgress);
    },
    */
    
})