({
    doInit: function(cmp, event, helper) {
        cmp.set("v.spinner", true);
        try {
            var getFormAction = cmp.get("c.getExistingServiceRequest");
            let newURL = new URL(window.location.href).searchParams;
            var pageId = newURL.get("pageId");
            var srIdParam = cmp.get("v.srId")? cmp.get("v.srId") : newURL.get("srId");
            var flowId = newURL.get("flowId");
            var reqWrapPram = {srId: srIdParam,
                               pageID: pageId,
                               flowId: flowId
                               
                              };
            getFormAction.setParams({
                reqWrapPram: JSON.stringify(reqWrapPram)
            });
            
            getFormAction.setCallback(this, function(response) 
                                      {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS"){
                                              var respWrap = response.getReturnValue();
                                              console.log('respWrap==>'+JSON.stringify(respWrap));
                                              
                                              cmp.set("v.srWrap", respWrap.srList);
                                              cmp.set("v.spinner", false);
                                          } 
                                          else 
                                          {
                                              cmp.set("v.spinner", false);
                                              console.log("@@@@@ Error " + response.getError()[0].message);
                                              //cmp.set("v.errorMessage", response.getError()[0].message);
                                              //cmp.set("v.isError", true);
                                              helper.createToast(cmp, event, response.getError()[0].message);
                                          }
                                      });
            $A.enqueueAction(getFormAction);
        } catch (err) {
            console.log("=error===" + err.message);
            cmp.set("v.spinner", false);
        }
    },
    
    onChange: function(cmp, evt, helper){
        cmp.set("v.showForm", false);
        var currentSRId = cmp.find("select").get("v.value");
         cmp.set("v.currentSRId", null);
        console.log('currentSRId==>'+currentSRId);
        cmp.set("v.currentSRId", currentSRId);
        //$A.get('e.force:refreshView').fire();
    },
     handleRefreshEvnt: function(cmp, event, helper) {
    var amedId = event.getParam("amedId");
    var srWrap = event.getParam("srWrap");
    var isNewAmendment = event.getParam("isNewAmendment");
    var amedWrap = event.getParam("amedWrap");
    console.log("$$$$$$$$$$$ handleRefreshEvnt== ", JSON.stringify(srWrap));
    console.log("2222222222 handleRefreshEvnt== ", amedWrap);
    cmp.set("v.amedId", amedId);
    cmp.set("v.selectedValue", "");

    //for auto scolling
    var activeScreen = document.getElementById("activePage");
    console.log("id of active screen ------>" + activeScreen);
    debugger;

    if (activeScreen) {
      activeScreen.scrollIntoView({
        block: "start",
        behavior: "smooth"
      });
    }

    if (amedId != null && amedId != "") {
      cmp.set("v.isNewPanelShow", false);
    } else {
      cmp.set("v.isNewPanelShow", true);
    }

  },
    
})