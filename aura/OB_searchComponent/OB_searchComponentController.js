({
  onfocus: function (component, event, helper) {
    $A.util.addClass(component.find("mySpinner"), "slds-show");
    var forOpen = component.find("searchRes");
    $A.util.addClass(forOpen, "slds-is-open");
    $A.util.removeClass(forOpen, "slds-is-close");
    var getInputkeyWord = "";
    var sortedLIst = helper.alphabeticalSorting(
      component.get("v.listOfSearchRecordsDB")
    );
    component.set("v.listOfSearchRecords", sortedLIst);
    //helper.searchHelper(component, event, helper, getInputkeyWord);
  },
  onblur: function (component, event, helper) {
    component.set("v.listOfSearchRecords", null);
    var forclose = component.find("searchRes");
    $A.util.addClass(forclose, "slds-is-close");
    $A.util.removeClass(forclose, "slds-is-open");
  },
  keyPressController: function (component, event, helper) {
    try {
      // get the search Input keyword
      var getInputkeyWord = component.get("v.SearchKeyWord");
      var listOfSearchRecords = component.get("v.listOfSearchRecords");
      console.log("=======getInputkeyWord=========");
      console.log(getInputkeyWord);
      // check if getInputKeyWord size id more then 0 then open the lookup result List and
      // call the helper
      // else close the lookup result List part.
      if (getInputkeyWord.length > 0) {
        var forOpen = component.find("searchRes");
        $A.util.addClass(forOpen, "slds-is-open");
        $A.util.removeClass(forOpen, "slds-is-close");

        listOfSearchRecords = listOfSearchRecords.filter(
          (amednWrap) =>
            amednWrap.displayName &&
            amednWrap.displayName
              .toLowerCase()
              .includes(getInputkeyWord.toLowerCase())
        );
        component.set("v.listOfSearchRecords", listOfSearchRecords);
      } else {
        console.log("in else");

        component.set(
          "v.listOfSearchRecords",
          component.get("v.listOfSearchRecordsDB")
        );
        //var forclose = component.find("searchRes");
        //$A.util.addClass(forclose, "slds-is-close");
        //$A.util.removeClass(forclose, "slds-is-open");
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  clear: function (component, event, helper) {
    console.log("IN clear function");
    var pillTarget = component.find("lookup-pill");
    var lookUpTarget = component.find("lookupField");

    $A.util.addClass(pillTarget, "slds-hide");
    $A.util.removeClass(pillTarget, "slds-show");

    $A.util.addClass(lookUpTarget, "slds-show");
    $A.util.removeClass(lookUpTarget, "slds-hide");

    component.set("v.SearchKeyWord", null);
    component.set("v.listOfSearchRecords", null);
    component.set("v.selectedRecord", {});

    var compEvent = component.getEvent("clearEvent");
    // set the Selected sObject Record to the event attribute.
    // fire the event
    compEvent.fire();
  },

  handleComponentEvent: function (component, event, helper) {
    // get the selected License record from the COMPONETNT event
    var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
    console.log("selected");
    console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));

    component.set("v.selectedRecord", selectedLicenseGetFromEvent);

    console.log("=============recordByEvent===========");
    var forclose = component.find("lookup-pill");
    $A.util.addClass(forclose, "slds-show");
    $A.util.removeClass(forclose, "slds-hide");

    var forclose = component.find("searchRes");
    $A.util.addClass(forclose, "slds-is-close");
    $A.util.removeClass(forclose, "slds-is-open");

    var lookUpTarget = component.find("lookupField");
    $A.util.addClass(lookUpTarget, "slds-hide");
    $A.util.removeClass(lookUpTarget, "slds-show");
  },
});