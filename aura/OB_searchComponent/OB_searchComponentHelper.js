({
  alphabeticalSorting: function (listToSort) {
    try {
      console.log("in sort");
      console.log(listToSort);
      var byName = listToSort.slice(0);
      byName.sort(function (a, b) {
        var x = a.displayName ? a.displayName.toLowerCase() : null;
        var y = b.displayName ? b.displayName.toLowerCase() : null;
        return x < y ? -1 : x > y ? 1 : 0;
      });
      console.log(byName);
      return byName;
    } catch (error) {
      console.log(error.message);
    }
  },
});