({
	getContactList: function(component, pageNumber, pageSize) {
	  var action = component.get("c.getEmployeeList");
        action.setParams({
              recordId : component.get("v.recordIds")
          });
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") { 
                var storeResponse = response.getReturnValue();
               //if storeResponse size is equal 0 ,display No Result Found... message on screen.
                if (storeResponse.length == 0) {
                 
                } else {
                    //  component.set("v.ListOfContact", storeResponse.thisRelationShipList);
                      component.set("v.ListOfContact", storeResponse);
              }
            }
 
        });
        $A.enqueueAction(action);	
	},
})