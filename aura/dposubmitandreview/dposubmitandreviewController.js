({
		init: function (component, event, helper) {
			component.set("v.openingDiv", "<div>");
			component.set("v.closingDiv", "</div>");

			let newURL = new URL(window.location.href).searchParams;
			var pageId = newURL.get("pageId");
			helper.getSrFields(component, event, helper);
		},    
  
		onChange: function (component, event, helper) {
			console.log(event.getSource().get("v.value"));
		},
  
		getSelected: function (component, event, helper) {
   
			var contentId = event.currentTarget.getAttribute("data-Id");
			console.log(contentId);
			console.log($A.get("e.lightning:openFiles"));
			var openFileEvent = $A.get("e.lightning:openFiles");
			if (openFileEvent) {
			  $A.get("e.lightning:openFiles").fire({
				recordIds: [contentId]
			  });
			} else {
				var contentRecordPageLink =
				"/lightning/r/ContentDocument/" + contentId + "/view";
				window.open(contentRecordPageLink, "_blank");

				/* window.location.href =
				"/lightning/r/ContentDocument/" + contentId + "/view"; */
			}
		},
		closeModal: function (component, event, helper) {
			// for Close Model, set the "hasModalOpen" attribute to "FALSE"
			component.set("v.hasModalOpen", false);
			component.set("v.selectedDocumentId", null);
		},
		
	/*handleAutoClose: function (cmp, event, helper) {
		try {
			let newURL = new URL(window.location.href).searchParams;
			var srID = newURL.get("srId");
			var pageID = newURL.get("pageId");
			var buttonId = event.target.id;
			console.log(buttonId);
			var action = cmp.get("c.getButtonAction");

			action.setParams({
				SRID: srID,
				pageId: pageID,
				ButtonId: buttonId
			});

			action.setCallback(this, function (response){
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log("button succsess"); 
					console.log(response.getReturnValue());
					window.open(response.getReturnValue().pageActionName, "_self");
				} else {
					console.log("@@@@@ Error " + response.getError()[0].message);
					helper.createToast(cmp, event, response.getError()[0].message);
				}
			});
			$A.enqueueAction(action);
		} catch (err) {
		  console.log(err.message);
		}
	},
	*/
	handleAutoClose: function (cmp, event, helper) {
		console.log('handleButtonAct');
		cmp.set("v.spinner", true);
		try {
		
			var allValid = true;
			var isOtherValidationsDone = true
			
			if(
				allValid && isOtherValidationsDone){
			    
				var srObj = cmp.get("v.srObj");
				let newURL = new URL(window.location.href).searchParams;
				var srID = newURL.get("srId");
				var pageflowId = newURL.get("flowId");
				var pageId = newURL.get("pageId");
				var buttonId = event.target.id;
				cmp.set("v.spinner", true);
				var action = cmp.get('c.onAmendReviewPage');
				
				var reqWrapPram  =
				{
					annualAssesment : cmp.get('v.annualAssessMent'),
					annualMatrixAssesment : cmp.get('v.annualAssessMentRisk'),
					amRec : cmp.get('v.amRec'),
					srId :srID,
					pageID:pageId,
					flowId:pageflowId,
					buttonId:buttonId
				};
				 
				action.setParams(
				{
					"reqWrapPram": JSON.stringify(reqWrapPram)
				});

				action.setCallback(this, function (response){
					var state = response.getState();
					console.log("state " + state);
					var responseObj = response.getReturnValue();
					console.log(responseObj);
					cmp.set('v.spinner',false);
					if (state === "SUCCESS") {
						window.open(responseObj.pageActionName, "_self");
					} else {
						console.log('@@@@@ Error '+response.getError()[0].message);
						//cmp.set('v.spinner',false);
						cmp.set("v.errorMessage",response.getError()[0].message);
						cmp.set("v.isError",true);
						helper.showToast(cmp,event,helper,response.getError()[0].message,'error');
					}
				});
				$A.enqueueAction(action);
			} 
		}catch (err) {
			console.log(err.message);
			cmp.set("v.spinner", false);
		}   
	}, 
 
	openPrint: function (component, event, helper) {
		window.print();
	},
	downloadCsv : function(component,event,helper){
		var stockData = component.get("v.annualAssessMent");
		var csv = helper.convertArrayOfObjectsToCSV(component,stockData);   
		if (csv == null){return;} 
		var hiddenElement = document.createElement('a');
		hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
		hiddenElement.target = '_self'; // 
		hiddenElement.download = 'Risk Matrix.csv';  // CSV file Name* you can change it.[only name not .csv] 
		document.body.appendChild(hiddenElement); // Required for FireFox browser
		hiddenElement.click(); // using click() js function to download csv file
	}, 
});