({
  MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB
  CHUNK_SIZE: 7500000, //Chunk Max size 750Kb
  associateFile: function(
    component,
    event,
    helper,
    parentId,
    docMasterCode,
    filesArr,
    srDocId
  ) {
    console.log("associateFile");
    var action = component.get("c.saveChunkBlob2");
    var files = filesArr; //component.get("v.fileToBeUploaded");
    var file;
    var self = this;
    //helper.show(component);
    if (files && files.length > 0) {
      file = files[0][0];
      console.log(file);
      console.log(file.size);
      if (file.size > self.MAX_FILE_SIZE) {
        component.set(
          "v.fileName",
          "Alert : File size cannot exceed " +
            self.MAX_FILE_SIZE +
            " bytes.\n" +
            " Selected file size: " +
            file.size
        );
        return;
      }
      var objFileReader = new FileReader();
      objFileReader.onloadend = function() {
        var fileContents = objFileReader.result;
        var base64 = "base64,";
        var dataStart = fileContents.indexOf(base64) + base64.length;

        fileContents = fileContents.substring(dataStart);
        console.log("===method 1===");
        console.log(action);
        // call the uploadProcess method
        self.uploadProcess(
          action,
          component,
          file,
          fileContents,
          parentId,
          docMasterCode,
          srDocId
        );
      };
      objFileReader.readAsDataURL(file);
    } else {
      //self.hide(component);
    }
  },

  uploadProcess: function(
    action,
    component,
    file,
    fileContents,
    parentId,
    docMasterCode,
    srDocId
  ) {
    // set a default size or startpostiton as 0
    var startPosition = 0;
    // calculate the end size or endPostion using Math.min() function which is return the min. value
    var endPosition = Math.min(
      fileContents.length,
      startPosition + this.CHUNK_SIZE
    );
    console.log("==uploadProcess===");
    console.log(action);
    // start with the initial chunk, and set the attachId(last parameter)is null in begin
    this.uploadInChunk(
      action,
      component,
      file,
      fileContents,
      startPosition,
      endPosition,
      "",
      parentId,
      docMasterCode,
      srDocId
    );
  },
  uploadInChunk: function(
    action,
    component,
    file,
    fileContents,
    startPosition,
    endPosition,
    attachId,
    parentId,
    docMasterCode,
    srDocId
  ) {
    try {
      console.log("==uploadInChunk===");
      console.log(action);
      console.log("component--" + JSON.stringify(component));
      // call the apex method 'saveChunk'
      let newURL = new URL(window.location.href).searchParams;
      var srId = newURL.get("srId");
      var getchunk = fileContents.substring(startPosition, endPosition);
      var self = this;
      var srDocId = srDocId ? srDocId : " ";
      console.log("srDocId>>>" + srDocId);
      console.log("parentId-----" + parentId);
      console.log("documentMasterCode--" + docMasterCode);
      console.log("file.name--" + file.name);
      console.log("file.type--" + file.type);
      console.log("action--" + action);
      console.log(srId);

      //console.log(encodeURIComponent(getchunk));
      action.setParams({
        parentId: parentId,
        srId: srId,
        fileName: file.name,
        base64Data: encodeURIComponent(getchunk),
        contentType: file.type,
        fileId: attachId,
        documentMasterCode: docMasterCode,
        strDocumentId: srDocId
      });
      console.log("am here");
      // set call back
      action.setCallback(this, function(response) {
        // store the response / Attachment Id
        attachId = response.getReturnValue();
        console.log("attachId>>" + attachId);
        var state = response.getState();
        if (state === "SUCCESS") {
          // update the start position with end postion
          console.log("inside----");
          startPosition = endPosition;
          endPosition = Math.min(
            fileContents.length,
            startPosition + this.CHUNK_SIZE
          );
          // check if the start postion is still less then end postion
          // then call again 'uploadInChunk' method ,
          // else, diaply alert msg and hide the loading spinner
          if (startPosition < endPosition) {
            this.uploadInChunk(
              action,
              component,
              file,
              fileContents,
              startPosition,
              endPosition,
              attachId,
              parentId,
              docMasterCode,
              srDocId
            );
          } else {
            //self.hide(component);
            console.log("your File is uploaded successfully>>" + attachId);
            window.location.href =
              "/" +
              $A.get("$Label.c.OB_Site_Prefix") +
              "/s/ob-searchentityname?srId=" +
              newURL.get("srId") +
              "&pageId=" +
              newURL.get("pageId") +
              "&flowId=" +
              newURL.get("flowId") +
              "&NameId=" +
              parentId;
            //alert("The File is uploaded successfully.");
            component.set("v.isEvidenceDocPresent", "true");
            //component.set("v.showLoadingSpinner", false);
          }
          // handel the response errors
        } else if (state === "INCOMPLETE") {
          alert("From server: " + response.getReturnValue());
          //self.hide(component);
        } else if (state === "ERROR") {
          //self.hide(component);
          var errors = response.getError();
          if (errors) {
            if (errors[0] && errors[0].message) {
              self.displayToast("Error", errors[0].message, "error");
              console.log("Error message: " + errors[0].message);
            }
          } else {
            console.log("Unknown error");
          }
        }
      });
      // enqueue the action
      $A.enqueueAction(action);
    } catch (err) {
      console.log("@@@@@ Error " + err.message);
    }
  },

  getList: function(component, event, helper) {
    var action = component.get("c.checkName");
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        var returnVal = response.getReturnValue();
        component.set("v.retValue", returnVal);
        console.log("name criterias returned", returnVal);
      }
    });
    $A.enqueueAction(action);
  },

  duplicateCheck: function(component, event, helper) {
    var action1 = component.get("c.checkAccountName");
    action1.setCallback(this, function(response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        var returnValDup = response.getReturnValue();
        component.set("v.retAccName", returnValDup);
        console.log("dup values returned", returnValDup);
      }
    });
    $A.enqueueAction(action1);
  },
  /*
    loadInit: function (component, event, helper) {
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');
        var action = cmp.get('c.getNameCheckDetail');
        action.setParams({
            "srId": srId
        });
        action.setCallback(this, function (response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                var responseResult = response.getReturnValue();
                console.log(responseResult);
            } else if (state === 'ERROR') { // Handle any error by reporting it
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayToast('Error', errors[0].message);
                    }
                } else {
                    helper.displayToast('Error', 'Unknown error.');
                }
            }
        });
        $A.enqueueAction(action);
    },
    */
  getReservedNames: function(component, event, helper) {
    var action1 = component.get("c.getReservedNames");
    action1.setCallback(this, function(response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS") {
        var returnValRes = response.getReturnValue();
        component.set("v.retReservedNames", returnValRes);
        console.log(
          "reserved values returned",
          JSON.stringify(component.get("v.retReservedNames"))
        );
      }
    });
    $A.enqueueAction(action1);
  },

  loadSRDocs: function(component, event, helper) {
    var nameId = component.get("v.NameId");
    var srId = component.get("v.srId");
    console.log(nameId);
    if (nameId) {
      var action = component.get("c.viewSRDocs");
      action.setParams({
        srId: srId,
        companyNameId: nameId
      });

      // set call back
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var result = response.getReturnValue();
          console.log("srDocs>>");
          console.log(result);
          component.set("v.srDocs", result);
          if (result.Evidence_of_relationship_and_consent) {
            component.set(
              "v.evidenceRelationDocDtl",
              result.Evidence_of_relationship_and_consent
            );
            component.set("v.isEvidenceDocPresent", "true");
          }
        } else if (state === "INCOMPLETE") {
          alert("From server: " + response.getReturnValue());
        } else if (state === "ERROR") {
          var errors = response.getError();
          if (errors) {
            if (errors[0] && errors[0].message) {
              helper.displayToast("Error", errors[0].message, "error");
              console.log("Error message: " + errors[0].message);
            }
          } else {
            console.log("Unknown error");
          }
        }
      });
      // enqueue the action
      $A.enqueueAction(action);
    }
  },
  /**
   * Display a message
   */
  displayToast: function(title, message, type) {
    console.log("displayToast");
    var toast = $A.get("e.force:showToast");

    // For lightning1 show the toast
    if (toast) {
      //fire the toast event in Salesforce1
      toast.setParams({
        title: title,
        type: type,
        message: message,
        mode: "dismissable"
      });

      toast.fire();
    } else {
      // otherwise throw an alert
      alert(title + ": " + message);
    }
  },
  show: function(cmp) {
    var spinner = cmp.find("mySpinner");
    $A.util.removeClass(spinner, "slds-hide");
    $A.util.addClass(spinner, "slds-show");
  },
  hide: function(cmp) {
    var spinner = cmp.find("mySpinner");
    $A.util.removeClass(spinner, "slds-show");
    $A.util.addClass(spinner, "slds-hide");
  },
  showHSpinner: function(component) {
    console.log("showHSpinner");
    // make Spinner attribute true for display loading spinner
    component.set("v.spinner", true);
  },

  hideHSpinner: function(component) {
    console.log("hideHSpinner");
    // make Spinner attribute to false for hide loading spinner
    component.set("v.spinner", false);
  }
});