({
    
    doInit:function(component,event,helper){
        let newURL = new URL(window.location.href).searchParams;
        var pageId = newURL.get("pageId");
        var flowId = newURL.get("flowId");
        component.set("v.pageId",pageId);
        component.set("v.flowId",flowId);
        
         /*var recordtypename = component.get("v.isIndividual");
         //Invoke the SRDDocs
         if(recordtypename){
             helper.loadSRDocs(component,event,helper);
         }
         //Invoke the SRDDocs
         helper.loadSRDocs(component,event,helper);*/
     }, 
     handleOCREvent : function(cmp, event, helper)
     {
         var ocrWrapper = event.getParam("ocrWrapper");
         var mapFieldApiObject = ocrWrapper.mapFieldApiObject;
         // Change the key here
         var fields = cmp.find("requiredField");
         
         //parsing of code.
         for(var key in mapFieldApiObject)
         {
              for (var i = 0; i < fields.length; i++)
              {
                  console.log(fields[i].get("v.fieldName"));
                  if(fields[i].get("v.fieldName"))
                  {
                      console.log(fields[i].get("v.fieldName"));
                      if((fields[i].get("v.fieldName")).toLowerCase() == key.toLowerCase() )
                      {
                          fields[i].set('v.value',mapFieldApiObject[key]);
                      }
                  
                  }
              }
         
         }
         //helper.hide(cmp, event, helper); 
     },
     handleUploadFinished: function (cmp, event,helper) {
         /*
         
         helper.handleUploadFinished(cmp, event,helper);
         
         var uploadedFiles = event.getParam("files");
         var fileName = uploadedFiles[0].name;
         if(fileName)
         {
             cmp.set("v.fileName", fileName);
         }
         
         var contentDocumentId = uploadedFiles[0].documentId;
         console.log(contentDocumentId);
         var docMasterCode = event.getSource().get('v.name');  
         
         var mapDocValues = {};
         var docMap = cmp.get("v.docMasterContentDocMap");
         for(var key in docMap){
             mapDocValues[key] = docMap[key];
         }
         
         mapDocValues[docMasterCode] = contentDocumentId;
         console.log(mapDocValues);
 
         console.log('@@@@@@ docMasterContentDocMap ');
         cmp.set("v.docMasterContentDocMap",mapDocValues);
         console.log(JSON.parse(JSON.stringify(cmp.get("v.docMasterContentDocMap"))));
         
     */
         
     },
     onChangeValue: function(component, event, helper){
         component.set('v.amedWrap.amedObj.Registration_No__c','');
     }, 
     handleLookupEvent: function (component, event, helper) 
     {
         console.log('handleLookupEvent');
         var accObj = event.getParam("sObject"); 
         console.log('lookup--'+JSON.stringify(accObj));
         component.set("v.amedWrap.amedObj.Registration_No__c",accObj.Registration_License_No__c);
        
     },
     onAmedSave : function(cmp, event, helper) 
     {
         cmp.set("v.isError",false);
         console.log('######### onAmedSave ');
         console.log(cmp.get("v.recordtypename"));
         //debugger;
         console.log('isIndividual'+ cmp.get("v.isIndividual"));
         console.log('isCorporate' + cmp.get("v.isCorporate"));
         
         helper.show(cmp, event);
         var recordtyeindi = cmp.get("v.isIndividual");
         var recordtypecor = cmp.get("v.isCorporate");
         var iAgreeIndividual =  cmp.get("v.amedWrap.amedObj.I_Agree_Guardian__c");
         var regExpEmailformat = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;  
         var regExpPhoneformat = /^[+][(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
         
         if($A.util.isEmpty(iAgreeIndividual) || $A.util.isUndefined(iAgreeIndividual) || iAgreeIndividual==false){
            cmp.set("v.errorMessage",'Please accept the declaration');
             cmp.set("v.isError",true);
             helper.hide(cmp, event);
             helper.showToast(cmp,event, helper,'Please accept the declaration','error');
         }else{
            var fileError = helper.fileUploadHelper(cmp, event, helper);
            if(fileError){
                return;
            }
             if(recordtyeindi){
                  // in save in controller before server call.
                  let isValid = true;
                  let fields = cmp.find("requiredField"); // gets all required fields
                  
                  for (var i = 0; i < fields.length; i++) {
                      //console.log('==fieldName=='+fields[i].get("v.fieldName"));
                      //console.log('==val=='+fields[i].get("v.value"));
                      var inputFieldCompValue = fields[i].get("v.value");
                     //console.log(inputFieldCompValue);
                     if(!inputFieldCompValue && fields[i] && fields[i].get("v.required")){
                         console.log('no value');
                         console.log('bad');
                         fields[i].reportValidity();
                         isValid = false;
                     } 
                      
                  }
                 
                
                console.log(cmp.get('v.fileName')+'valid==error=='+isValid);
                if(isValid == false){
                   cmp.set("v.errorMessage",'Please fill required field');
                   cmp.set("v.isError",true);
                   helper.hide(cmp, event);
                   helper.showToast(cmp,event, helper, 'Please fill required field','error');
                }
             }
             
             else if (recordtypecor){
                let isValid = true;
                let fields = cmp.find("requiredField"); // gets all required fields
             
                  for (var i = 0; i < fields.length; i++) {
                      var inputFieldCompValue = fields[i].get("v.value");
                     //console.log(inputFieldCompValue);
                     if(!inputFieldCompValue && fields[i] && fields[i].get("v.required")){
                         console.log('no value');
                         //console.log('bad');
                         fields[i].reportValidity();
                         isValid = false;
                     }  
                  }
                  
                  if(isValid == false){
                   cmp.set("v.errorMessage",'Please fill required field');
                   cmp.set("v.isError",true);
                   helper.hide(cmp, event);
                   helper.showToast(cmp,event, helper, 'Please fill required field','error');
                }
                
             }else{
                 console.log('===error==');
                 cmp.set("v.errorMessage",'Please select the type of Shareholders');
                 cmp.set("v.isError",true);
                 helper.hide(cmp, event);
                 helper.showToast(cmp,event, helper, 'Please select the type of Shareholders','error');
             }
         }
         var isError = cmp.get("v.isError");
         console.log('====isError===='+isError);
         if(isError != true){
             
             var docMasterContentDocMap = cmp.get("v.docMasterContentDocMap");
             var onAmedSave = cmp.get('c.onAmedSaveDB');
             
             var srId = cmp.get('v.srId');
             console.log('-----no error--srId-'+srId);
             var reqWrapPram  =
                 {
                     srId:srId,
                     amedWrap: cmp.get('v.amedWrap'),
                     recordtypename: cmp.get("v.recordtypename"),
                     docMasterContentDocMap:docMasterContentDocMap
                 };
             
             onAmedSave.setParams(
                 {
                     "reqWrapPram": JSON.stringify(reqWrapPram)
                 });
             
             onAmedSave.setCallback(this, 
                                    function(response) {
                    var state = response.getState();
                    
                    console.log("callback state: " + state);
                    
                    if (cmp.isValid() && state === "SUCCESS"){
                        var respWrap = response.getReturnValue();
                        
                        if(!$A.util.isEmpty(respWrap.errorMessage)){
                            console.log('=====error on sve===='+respWrap.errorMessage);
                            cmp.set('v.spinner',false);
                            //this.showToast("Error",respWrap.errorMessage);
                            cmp.set("v.errorMessage",respWrap.errorMessage);
                            cmp.set("v.isError",true);
                            helper.hide(cmp, event);
                            helper.showToast(cmp,event, helper,respWrap.errorMessage,'error');
                        }else{
                            if(respWrap.amedWrap.amedObj.Id != '' )
                            {
                                var amendID = respWrap.amedWrap.amedObj;
                            
                                console.log('######### respWrap.amedWrap.amedObj.Id  ',respWrap.amedWrap);
                                console.log('######### amendID  ',amendID.Id);
                                
                                cmp.set("v.amedWrap.amedObj",null);
                                cmp.set('v.spinner',false);
                                helper.hide(cmp, event);
                                cmp.set('v.fileName','');
                                var refeshEvnt = cmp.getEvent("refeshEvnt");
                                var srWrap = respWrap.srWrap;
                                console.log('@@@@@ srWrap ',srWrap);
                                refeshEvnt.setParams({
                                    "srWrap" : srWrap });
                                refeshEvnt.fire();
                            }
                        }                             
                    }
                    else
                    {
                        console.log('@@@@@ Error '+response.getError()[0].message);
                        cmp.set('v.spinner',false);
                        helper.hide(cmp, event);
                        cmp.set("v.errorMessage",response.getError()[0].message);
                        cmp.set("v.isError",true); 
                        helper.showToast(cmp,event, helper,response.getError()[0].message,'error');
                    }
                }
           );
             $A.enqueueAction(onAmedSave);
         }
         
     },
     closeModel : function(cmp, event, helper) {
         //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
         console.log('--toast close--');
         cmp.set("v.errorMessage","");
         cmp.set("v.isError",false);
     },
     closeForm: function(cmp, event, helper){
         //throw event.
         cmp.set('v.spinner',true);
         cmp.set("v.isCorporate", false);
         cmp.set("v.isIndividual", false);
         var isNew = cmp.get('v.isNew');
         if(isNew == true){
             console.log('===new record====');
             cmp.set("v.amedWrap.amedObj",null);
         }
         var refeshEvnt = cmp.getEvent("refeshEvnt");
         refeshEvnt.setParams({
             "amedId" : '',
             "isNewAmendment" : isNew,
             "amedWrap":cmp.get("v.amedWrap.amedObj")});
         cmp.set('v.spinner',false);
         refeshEvnt.fire();
         
     },
     handleRecordType  : function(cmp, event, helper){
         var index = event.target.dataset.amedtype;
         console.log('###############@@@@@@@@@@'+ index);
         cmp.set("v.recordtypename", index);
         if(index == 'Individual') {
             cmp.set("v.isIndividual", true);
             cmp.set("v.isCorporate", false);
         } else {
             cmp.set("v.isCorporate", true);
             cmp.set("v.isIndividual", false);
         }
         
         console.log(cmp.get("v.recordtypename"))
         
     },
     onChangeCertifiedPassportCopy : function(cmp, event, helper) 
     {
         console.log('@@@@@@@@@@@@@ onChangeCertifiedPassportCopy ');
         var amedWrap = cmp.get('v.amedWrap');
         var certifiedPassportCopy = amedWrap.amedObj.Certified_passport_copy__c;
         
         if(certifiedPassportCopy == 'No')
         {
             //alert('Please visit DIFC for citation of original passport');
             var toastEvent = $A.get("e.force:showToast");
             toastEvent.setParams({
                 "title": "Information",
                 "message": "Please visit DIFC for citation of original passport."
             });
             toastEvent.fire();
             return;            
         }
     }
 })