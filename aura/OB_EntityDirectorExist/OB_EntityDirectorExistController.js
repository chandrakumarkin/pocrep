({
	viewDetails : function(cmp, event, helper) 
    {
		//throw event.
		var refeshEvnt = cmp.getEvent("refeshEvnt");
        var amedId = cmp.get("v.amedWrap").amedObj.Id ;
        console.log('@@@@@ amedId ',amedId);
        refeshEvnt.setParams({
            "amedId" : amedId });
        refeshEvnt.fire();
	},
    
    remove : function(cmp, event, helper) 
    {
		//remove 
		 var amedIdParam = cmp.get("v.amedWrap").amedObj.Id ;
         try
        {
           
            var getFormAction = cmp.get('c.deleteAmed');
            var reqWrapPram  =	{
                                    amedId: amedIdParam,
                				  amedWrap: cmp.get("v.amedWrap")
                                }
            
            getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
            console.log('@@@@@@@@@@ reqWrapPram init '+JSON.stringify(reqWrapPram));
            getFormAction.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                              
                                              //refreshing 
                                                var refeshEvnt = cmp.getEvent("refeshEvnt");
                                                /*refeshEvnt.setParams({
                                                    "amedId" : amedId });*/
                                                refeshEvnt.fire();
                                              
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                              
                                          }
                                      }
                                     );
            $A.enqueueAction(getFormAction);
        }catch(err){
            console.log('=error==='+err.message);
        }
        
	}
})