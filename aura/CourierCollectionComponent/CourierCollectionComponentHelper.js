({
	
    showToast: function(component, event, helper, msg, type) {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        mode: "sticky",
        message: msg,
        type: type
      });
      toastEvent.fire();
    },     
    searchRecords : function(component, event, helper) {
	
		var searchSR = component.find("searchText").get("v.value");
		var searchStatus = component.get("v.selectedStatus");
		var changeStatus = component.get("v.selectedChangeStatus");
		var action = component.get("c.getResponseWrapperList");
		action.setParams({
		  "searchValue": searchSR,
		  "searchStatus": searchStatus,
		  "changeStatus": changeStatus
		});
		
			action.setCallback(this, function(response) {
				var state = response.getState();
				if(state === "SUCCESS") {
					var storeResponse = response.getReturnValue();
					console.log(storeResponse);
					console.log('************'+JSON.stringify(storeResponse));
					if (storeResponse.passportTrackingList.length == 0) {
            component.set("v.norecordfound", true);
          } else {
            component.set("v.norecordfound", false);
          }

            var allRecords = component.get("v.selectedpassportTracking");
            for(var i=0;i<storeResponse.passportTrackingList.length;i++){
                
                  for(var j=0;j<allRecords.length;j++){
                      
                  if(allRecords[j].passportTrackingList.Id == storeResponse.passportTrackingList[i].Id
                          && allRecords[j].isChecked == true ){
                      
                      storeResponse.passportTrackingList[i].isChecked = true; 
                  }
              }
            }

            if(storeResponse.isOfficeBoy){
                  component.set("v.isRep", true);
            }else{
                component.set("v.isRep", false);
            }

            component.set("v.passportTracking", storeResponse.passportTrackingList);
            component.set("v.TotalCount", storeResponse.passportTrackingList.length);

				}
			});
          $A.enqueueAction(action);
      },
})