({
	searchChange : function(component, event, helper) {
		var selectedValue = component.find("searchBy").get("v.value");
        if(selectedValue !== undefined || selectedValue != ''){
            component.set("v.searchByValue",selectedValue);
            component.set("v.issearchByValue",true);
        }
        else {
            component.set("v.issearchByValue",false);
            component.set("v.searchByValue",'');
        }
	},
    
    cancel : function(component, event, helper) {
       location.reload();
    },
    showSelected : function(component, event, helper) {
       var selectedRecords = [];
       var allRecords = component.get("v.selectedpassportTracking");
        
           for(var j=0;j<allRecords.length;j++){
              if(allRecords[j].isChecked == true ){
                  selectedRecords.push(allRecords[j]); 
            }
        }
        component.set("v.passportTracking", selectedRecords);
        component.set("v.isUpdated", false);

        if (selectedRecords.length == 0) {
          component.set("v.norecordfound", true);
        } else {
          component.set("v.norecordfound", false);
        }

      },
      searchTextChange : function(component, event, helper) {
        helper.searchRecords(component, event, helper);
      },
   
    doInit  : function(component, event, helper) {
      helper.searchRecords(component, event, helper);
    },
    showSpinner: function(component, event, helper) {
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
       component.set("v.Spinner", false);
    },
    changeStatuspickValChangeChange : function(component, event, helper) {
      var selectedValue = event.getSource().get("v.value");
      component.set("v.selectedChangeStatus", selectedValue);
   },
    
    statuspickValChangeChange : function(component, event, helper) {
		   var selectedValue = event.getSource().get("v.value");
       component.set("v.selectedStatus", selectedValue);
       helper.searchRecords(component, event, helper);
    },
  
    checkboxSelect : function(component, event, helper) {
		console.log('----checkboxSelect------');  
		var selectedRec = event.getSource().get("v.value");
		var selectedText = event.getSource().get("v.text");
		var getSelectedNumber = component.get("v.selectedCount");

		var allRecords = component.get("v.passportTracking");
		var newRecords = component.get("v.selectedpassportTracking");
		console.log('----allRecords------'+allRecords.length);  
		console.log('----newRecords------'+newRecords.length);  
		
        for(var i=0;i<allRecords.length;i++){
            if(allRecords[i].passportTrackingList.Id == selectedText){
                if(selectedRec == true){
					allRecords[i].isChecked = true;
					newRecords.push(allRecords[i]);                    
                }
			}
		}
      
		for(var i=0;i<newRecords.length;i++){
			if(newRecords[i].passportTrackingList.Id == selectedText 
				 && selectedRec == false){
				newRecords.splice(i, 1);
			}
		}
        
        if (selectedRec == true) {
            component.set("v.isCheckboxSelected", true);
            getSelectedNumber++;
        } else {
            getSelectedNumber--;
            if(getSelectedNumber ==0){
                  component.set("v.isCheckboxSelected", false);
            }
        }
        component.set("v.selectedpassportTracking", newRecords);
        component.set("v.selectedCount", getSelectedNumber);
    },
    Save: function(component, event, helper) {
        
      var allRecords = component.get("v.passportTracking");
      var isContinue = true;
      var changeStatus = component.get("v.selectedChangeStatus");

      if(changeStatus === ''){
        component.set("v.isUpdated", true);
        component.set("v.message", 'Change Status is required field.');
        isContinue = false;
        return;
      }else{
        component.set("v.isUpdated", false);
      }
      
      if(allRecords.length == 0){
        //component.set("v.isUpdated", true);
        isContinue = false;
        component.set("v.isUpdated", true);
        component.set("v.message", 'Kindly click Show Selected button and proceed to save');
      }   
		
      if(isContinue){
        var selectedRecords = [];
        var isError = false;
        var errorMsg = '';
        for (var i = 0; i < allRecords.length; i++) {
          
          if (allRecords[i].isChecked) {
            console.log(allRecords[i].changeStatusMatch);
            console.log(allRecords[i].passportTrackingList.Status__c);
            console.log(changeStatus);
            if(changeStatus != allRecords[i].changeStatusMatch){
              isError = true;
              errorMsg = 'You are not allowed to updated the SR '+allRecords[i].passportTrackingList.SR_Number__c+' status.Please check current Status.';
              break;
            }
            selectedRecords.push(allRecords[i].passportTrackingList);
            console.log(JSON.stringify(allRecords[i].passportTrackingList));
          }
        }
        if(isError){
          component.set("v.message", errorMsg);
          component.set("v.isUpdated", true); 
          return;
        }
        
      var action = component.get("c.updatePassportTracking");
        action.setParams({
          "selectedId": JSON.stringify(selectedRecords),
          'selectedValue' : component.get("v.selectedChangeStatus")
        });
        // set a callBack    
        action.setCallback(this, function(response) {
          var state = response.getState();
          if (state === "SUCCESS") {
            var storeResponse = response.getReturnValue();
            location.reload();
          }
        });
        // enqueue the Action  
        $A.enqueueAction(action);
      }
    }
    

})