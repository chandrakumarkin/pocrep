({
    onInit : function(component, event, helper) {
        let allParams = window.location.search;
        
        let pgURL = $A.get("$Label.c.ADCB_Portal_PGUrl");
        if(!$A.util.isEmpty(pgURL)){
            if(allParams && allParams.length > 0 ){
                let urlParams = new URLSearchParams(allParams);
                console.log('INIT->urlParams-->',urlParams);
                if(urlParams){
                    let amt = urlParams.get('amountToPay');
					let srId = ( !$A.util.isEmpty(urlParams.get('srId')) ? urlParams.get('srId') : '' );
                    console.log('INIT->n srId-->',srId);                    
                    if(amt && amt > 0){
                        var action = component.get('c.prepareCardRequest');
                        action.setParams({
                            "amount": amt,
                            "srId": srId
                        });
                        action.setCallback(this, function(resp){
                            let state = resp.getState(); // get the response state
                            let respData = resp.getReturnValue();
                            console.log('Apex rresp-->'+JSON.stringify(respData));
                            if(state === 'SUCCESS') {
                                if(respData && respData.length > 0){
                                    component.set("v.showIframe",true);
                                    component.set("v.ccHtmlFormInputs",respData);
                                    
                                    
                                    // document.getElementById("ccSubmit").click();
                                    //Submitting form to payment gateway.
                                    window.setTimeout(
                                        $A.getCallback(function() {
                                            console.log('In setTimeout function...');
                                            //document.getElementById("ccSubmit").click();
                                            let btn = document.getElementById("ccSubmit");
                                            btn.click();
                                            console.log('In setTimeout after btn clk....');
                                        }), 133
                                    ); 
                                    
                                }else{
                                    component.set("v.showIframe",false);
                                    component.set("v.msgTitle","Oops!!");
                                    component.set("v.msgType","error");
                                    component.set("v.message","Payment processing failed, please contact system administrator.");
                                }
                                
                            }else{
                                component.set("v.showIframe",false);
                                component.set("v.msgTitle","Error!!");
                                component.set("v.msgType","error");
                                component.set("v.message","Unknown error occurred, please contact system administrator.");
                            }
                        });
                        $A.enqueueAction(action);
                    }else{
                        component.set("v.showIframe",false);
                        component.set("v.msgTitle","Oops!!");
                        component.set("v.msgType","error");
                        component.set("v.message","Please go back and resubmit the amount correctly.");
                    }
                    
                }else{
                    component.set("v.showIframe",false);
                    component.set("v.msgTitle","Error!!");
                    component.set("v.msgType","error");
                    component.set("v.message","Unknown error occurred, please contact system administrator.");
                }
            }else{
                
                component.set("v.showIframe",false);
                component.set("v.msgTitle","Error!!");
                component.set("v.msgType","error");
                component.set("v.message","Unknown error occurred, please contact system administrator.");
            }
        }else{
            component.set("v.showIframe",false);
            component.set("v.msgTitle","Error!!");
            component.set("v.msgType","error");
            component.set("v.message","Unknown error occurred, please contact system administrator.");
        }
        
        
    }
})