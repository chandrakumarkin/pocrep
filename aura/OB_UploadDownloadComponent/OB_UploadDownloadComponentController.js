({
  doInit: function(component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    component.set("v.SRId", newURL.get("srId"));
    helper.GetLoggedInUser(component, event);
    helper.GetStatusPicklistValues(component, event);
    helper.GetSRDocs(component, event);
    // helper.ShowDocumentPreview(component, event);
  },
  PreviewDocument: function(component, event, helper) {
    var selectedItem = event.currentTarget.dataset.attachmentid;
    var selectedItemName = event.currentTarget.dataset.docname;

    component.set("v.SelectedAttachmentID", selectedItem);
    component.set("v.SRDocName", selectedItemName);

    helper.ShowDocumentPreview(component, event);
    //helper.EnableDisableButtons(component,event);
  },
  showOppmodal: function(component, event, helper) {
    component.find("fileName").set("v.value", "");
    var selectedIndex = event.currentTarget.dataset.index;
    var attachmentObj = component.get("v.SRDocs");

    component.set("v.SRDocument", attachmentObj[selectedIndex]);
    helper.toggleClass(component, "backdrop", "slds-backdrop--");
    helper.toggleClass(component, "modaldialog", "slds-fade-in-");
  },
  hideModal: function(component, event, helper) {
    helper.toggleClassInverse(component, "backdrop", "slds-backdrop--");
    helper.toggleClassInverse(component, "modaldialog", "slds-fade-in-");
  },
  getFileName: function(component, event, helper) {
    component.find("fileName").set("v.value", "");
    var files = document.getElementById("file").files;
    var fileName = "";
    for (var i = 0; i < files.length; i++) fileName += files[i].name + " ";
    console.log(fileName);
    component.find("fileName").set("v.value", fileName);

    /* var fileName = document.getElementById("file").value;
    console.log(fileName);
    //console.log(document.getElementById('file').item(0).name);
    filename = fileName.replace("C:\\fakepath\\", "");
    console.log(filename);
    component.set("v.fileName", filename);
    //document.getElementById('FileName').innerHTML=fileName; */
  },
  Save: function(component, event, helper) {
    helper.saveAttachment(component, event);
  },
  /* showSpinner: function(component, event, helper) {
    component.set("v.Spinner", true);
  },
  hideSpinner: function(component, event, helper) {
    component.set("v.Spinner", false);
  }, */
  ShowNextAttachment: function(component, event, helper) {
    helper.ShowNextAttachment(component, event);
  },
  ShowPreviousAttachment: function(component, event, helper) {
    helper.ShowPreviousAttachment(component, event);
  },
  GoToSR: function(component, event, helper) {
    var SRRecordID = component.get("v.SRId");
    location.href = "/" + SRRecordID;
  },
  SaveSRDocs: function(component, event, helper) {
    helper.SaveSRDocs(component, event);
  },

  handleButtonAct: function(component, event, helper) {
    try {
      component.set("v.Spinner", true);
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");

      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId
      });

      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          component.set("v.Spinner", false);
          console.log("button succsess");

          console.log(response.getReturnValue());
          if (!response.getReturnValue().errorMessage) {
            window.open(response.getReturnValue().pageActionName, "_self");
          } else {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
              mode: "dismissable",
              message: response.getReturnValue().errorMessage,
              type: "error",
              duration: 500
            });
            toastEvent.fire();
          }
        } else {
          component.set("v.Spinner", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  }
});