({
	checkTheValue : function(component, event, helper) {        
        var optionRecListValue = component.get("v.optionRecList");
        for(var i=0; i < optionRecListValue.length; i++){
            var idValue = component.get("v.surveyQuestion.Survey_Question_ID__c")+optionRecListValue[i].label;
            var elementVal = document.getElementById(idValue);
            if(elementVal != 'undefined' && elementVal.checked == true){
                var compValueOfRating = component.get("v.surveyQuestion");
                compValueOfRating.Rating__c =  elementVal.value; 
                compValueOfRating.Remarks__c =  component.get("v.Remarks");
                compValueOfRating.Survey_Question__c =  component.get("v.surveyQuestion.Survey_Question_ID__c"); 
            }
        }
	}
})