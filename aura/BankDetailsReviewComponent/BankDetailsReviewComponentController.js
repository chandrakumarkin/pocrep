({
	init: function(component, event, helper) {
        var action = component.get("c.bankDetailstoReview");
        action.setCallback(this,function(response){
        var state = response.getState();
            console.log('!!@@@'+response.getReturnValue());
            if(state === "SUCCESS"){
            	component.set("v.reviewWrapper",response.getReturnValue());    
            }
        });
        $A.enqueueAction(action);

    },
    gotoURL : function (component, event, helper) {
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      "url": "https://portal.difc.ae/clientportal/s"
    });
    	urlEvent.fire();
	},
   
    sendLeadInformation: function (component, event, helper){
               
        var ischecked = component.find("checkbox");
        var sendOTP = ischecked.get("v.value");
        //#11476
        var turnover = component.get("v.reviewWrapper.turnover");
		if(turnover==null){
			component.set('v.isErrturnover', true);
		}
        console.log('@@###'+ischecked.get("v.value"));
        if(sendOTP == true){
            var action = component.get("c.callOTPpage"); //#11476
            action.setParams({ "turnover" : turnover});
            //var action = component.get("c.otpPage"); //#11476
            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
                "url": "https://portal.difc.ae/clientportal/s/sendotppage"
             });
            urlEvent.fire();
        }
        else{
            
            component.set('v.isError', true);
        }
		$A.enqueueAction(action);
	},
    
    getSelected: function(component, event, helper) {
    
    var contentId = event.currentTarget.getAttribute("data-recid");
    				
    console.log('!!@@'+contentId);
    var openFileEvent = $A.get("e.lightning:openFiles");
    if (openFileEvent) {
      $A.get("e.lightning:openFiles").fire({
        recordIds: [contentId]
      });
    } else {
      var contentRecordPageLink =
        "/lightning/r/ContentDocument/" + contentId + "/view";
      window.open(contentRecordPageLink, "_blank");

    }
  }
    
    
})