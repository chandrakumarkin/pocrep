({
  doInit: function(component, event, helper) {
    console.log("in init");
    let newURL = new URL(window.location.href).searchParams;
    var action = component.get("c.fetchPageFlowWrapper");
    var requestWrap = {
      pageflowId: new URL(location.href).searchParams.get("flowId"),
      srId: newURL.get("srId")
    };
    console.log(requestWrap);
    action.setParams({
      requestWrapParam: JSON.stringify(requestWrap)
    });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        component.set(
          "v.currentpageFlowObj",
          response.getReturnValue().currentPageFlowObj
        );
        component.set(
          "v.progress",
          response.getReturnValue().progressCompletion
        );
        var prgStrng = parseInt(response.getReturnValue().progressCompletion);
        component.set("v.progressClass", "width :" + prgStrng + "%;");
        console.log(response.getReturnValue().currentPageFlowObj);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  }
});