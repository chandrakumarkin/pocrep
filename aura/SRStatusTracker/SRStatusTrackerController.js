({
	
    cancel : function(component, event, helper) {
       location.reload();
    },
    
    searchTextChange : function(component, event, helper) {
        var selectedValue = component.find("searchText").get("v.value");
       // var searchTextOfBPNoValue = component.find("searchTextOfBPNo").get("v.value");
        //var searchCustomerName = component.find("searchTextOfCustomer").get("v.value");
        console.log('=======selectedValue====='+selectedValue);
        //console.log('=======searchTextOfBPNoValue====='+searchTextOfBPNoValue);
        //console.log('=======searchCustomerName====='+searchCustomerName);

       // var isStatusActive = component.get("v.isStatusVisible");
        //var statusVal = (isStatusActive) ? component.find("searchSRStatus").get("v.value") :'';
        //console.log('=======statusVal====='+statusVal);

        //var isAnyOneSearchHasValue = (searchCustomerName =='' && selectedValue ==''&& searchTextOfBPNoValue =='') ? true : false;
       // console.log('=======isAnyOneSearchHasValue====='+isAnyOneSearchHasValue);
           
        var action = component.get("c.fetchSRList");
        action.setParams({ 
            "searchSRnumber": selectedValue
           // "customerBPNo": searchTextOfBPNoValue,
            //"customerName":searchCustomerName, 
            //"statusValue":statusVal,
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                    component.set("v.norecordfound", true);
                    //if(isAnyOneSearchHasValue){
                      //  component.set("v.isStatusVisible", false);
                    //} 
                } else {
                    component.set("v.norecordfound", false);
                    //component.set("v.isStatusVisible", true); 
                } 
                component.set("v.srList", storeResponse);
                component.set("v.TotalCount", storeResponse.length); 
            }
        });
        $A.enqueueAction(action);
	}, 
    
    doInit  : function(component, event, helper) {
        var action = component.get("c.fetchSRList");
      // set param to method  
       var srNumber = '';
      // var customerBPNumber = ''; 
      // var searchCustomerName = ''; 
      // var statusVal = ''; 

       component.set("v.isStatusVisible", false);

        action.setParams({
            "searchSRnumber": srNumber
            //,
           // "customerBPNo": customerBPNumber,
         //   "customerName":searchCustomerName,
          //  "statusValue":statusVal,
          });
        action.setCallback(this, function(response) {
            var state = response.getState();

            if (state === "SUCCESS"){
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                     component.set("v.norecordfound", true);
                } else {
                    component.set("v.norecordfound", false);
                }
               component.set("v.srList", storeResponse);
               component.set("v.TotalCount", storeResponse.length);
            }else if (state === "ERROR") {
            }
            else {
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);
    
    },
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
})