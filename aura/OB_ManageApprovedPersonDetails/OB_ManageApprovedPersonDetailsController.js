({
  doInit: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var flowIdParam = component.get("v.flowId")
      ? component.get("v.flowId")
      : newURL.get("flowId");
    var srIdParam = component.get("v.srId")
      ? component.get("v.srId")
      : newURL.get("srId");
    var pageIdParam = component.get("v.pageId")
      ? component.get("v.pageId")
      : newURL.get("pageId");
    // set if comming from url
    component.set("v.pageId", pageIdParam);
    component.set("v.flowId", flowIdParam);
    component.set("v.srId", srIdParam);

    console.log("####@@@@@@ pageId ", pageIdParam);
    console.log("####@@@@@@ flowIdParam ", flowIdParam);
    console.log("####@@@@@@ srIdParam ", srIdParam);

    //console.log(component.get("v.amendWrap.amedObj.Id"));

    console.log(component.get("v.isNew"));
  },

  handleOCREvent: function (cmp, event, helper) {
    var ocrWrapper = event.getParam("ocrWrapper");
    var mapFieldApiObject = ocrWrapper.mapFieldApiObject;
    console.log("in chache refresh ---> " + mapFieldApiObject);
    console.log(mapFieldApiObject);

    // Change the key here
    var fields = cmp.find("approvedFields");
    console.log("in chache refresh ---> " + fields);

    //parsing of code.
    for (var key in mapFieldApiObject) {
      for (var i = 0; i < fields.length; i++) {
        console.log(fields[i].get("v.fieldName"));
        if (fields[i].get("v.fieldName")) {
          console.log(fields[i].get("v.fieldName"));
          if (fields[i].get("v.fieldName").toLowerCase() == key.toLowerCase()) {
            fields[i].set("v.value", mapFieldApiObject[key]);
          }
        }
      }
    }
    //helper.hideSpinner(cmp, event, helper);
  },

  handleSubmit: function (component, event, helper) {
    //debugger;   
    if (component.get("v.isNew")) {
      var allValid = true;
      var isFileUploaded = true;
      var fileUploaded = component.find("Passport_Copy_Individual");
      //v1.1 Changes
	  var inputCountryValue;
      var approvedPerson = true;
        
      let fields = component.find("approvedFields");
      for (var i = 0; i < fields.length; i++) {
        var inputFieldCompValue = fields[i].get("v.value");
        //v1.1 changes
        var inputFieldName = fields[i].get('v.fieldName');        
          if(inputFieldName == 'Permanent_Native_Country__c'){
              inputCountryValue = fields[i].get('v.value');              
          }        
        if (!inputFieldCompValue && fields[i] && fields[i].get("v.required")) {
          console.log("no value");
          //console.log('bad');
          fields[i].reportValidity();
          allValid = false;
        }
        
          //v1.1 changes
          if(!$A.util.isEmpty(inputCountryValue) && inputCountryValue != 'United Arab Emirates'){
              console.debug('### inp Country '+inputCountryValue);
              approvedPerson = false;
          }
      }

      /* console.log(fileUploaded.get("v.value")); */

      if (!allValid) {
        console.log("Error MSG");

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          mode: "dismissable",
          message: "Please fill required field",
          type: "error",
          duration: 500,
        });
        toastEvent.fire();

        return;
      }
        //v1.1 Changes
        if(!approvedPerson){            
            helper.showToast(component, event, helper, 'Approved Person must be resident of UAE', 'error', 500);
            return;
        }
      //handle err is remaining.(for ocr validation)
      var fileError = helper.fileUploadHelper(component, event, helper);
      console.log(fileError);

      if (fileError) {
        return;
      }
    }

    component.set("v.showSpinner", true);
    /* var toAssign = component.get("v.toAssign");

    if (component.get("v.toAssign")) {
      var role = component.get("v.amendWrap.amedObj.Role__c");
      role += "; Approved Person";
      component.set("v.amendWrap.amedObj.Role__c", role);
    } */

    var role = component.get("v.amendWrap.amedObj.Role__c");
    if (role) {
      if (!role.includes("Approved Person")) {
        role += "; Approved Person";
        component.set("v.amendWrap.amedObj.Role__c", role);
      }
    } else {
      role = "Approved Person";
      component.set("v.amendWrap.amedObj.Role__c", role);
    }

    var action = component.get("c.amenSaveAction");
    var requestWrap = {
      amendObj: component.get("v.amendWrap.amedObj"),
      srId: component.get("v.srId"),
      docMap: component.get("v.docMasterContentDocMap"),
    };
    action.setParams({
      requestWrapParam: JSON.stringify(requestWrap),
    });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        var respWrapp = response.getReturnValue();
        if (response.getReturnValue().errorMessage == "no error recorded") {
          component.set("v.showSpinner", false);
          /* component.set(
            "v.AmendListWrapp",
            response.getReturnValue().amendWrap.amedObjp
          ); */
          component.set("v.respWrap", response.getReturnValue());
          component.set("v.isVisible", false);
          component.set("v.selectedValue", "");
          component.set("v.amedIdInFocus", "none");
        } else {
          component.set("v.showSpinner", false);
          var toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({
            mode: "dismissable",
            message: respWrapp.errorMessage,
            type: "error",
            duration: 500,
          });
          toastEvent.fire();
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },


  handleSuccess: function (component, event, helper) {},

  handleError: function (cmp, event, helper) {
    component.set("v.showSpinner", false);
  },

  handleFileUploadFinished: function (component, event) {
    // Get the list of uploaded files

    var uploadedFiles = event.getParam("files");
    var fileName = uploadedFiles[0].name;
    var contentDocumentId = uploadedFiles[0].documentId;
    console.log(contentDocumentId);
    var docMasterCode = event.getSource().get("v.name");
    component.find("fileMissing").set("v.value", "");
    component.find(docMasterCode).set("v.value", fileName);
    var mapDocValues = {};
    var docMap = component.get("v.docMasterContentDocMap");
    for (var key in docMap) {
      mapDocValues[key] = docMap[key];
    }
    debugger;
    mapDocValues[docMasterCode] = contentDocumentId;
    console.log(mapDocValues);
    component.set("v.docMasterContentDocMap", mapDocValues);
    console.log(
      "Doc@@@@@@$$$$$$$$$$$$$$$$",
      JSON.parse(JSON.stringify(component.get("v.docMasterContentDocMap")))
    );

    //alert("Files uploaded : " + uploadedFiles.length);
  },

  closeForm: function (component, event, helper) {
    component.set("v.amedIdInFocus", "none");
  },

  onChangeCertifiedPassportCopy: function (cmp, event, helper) {
    var certifiedPassportCopy = cmp.get(
      "v.amendWrap.amedObj.Certified_passport_copy__c"
    );

    if (certifiedPassportCopy == "No") {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        title: "Information",
        message: "Please visit DIFC for citation of original passport.",
      });
      toastEvent.fire();
      return;
    }
  },
});