({
  doInit: function (component, event, helper) {
    helper.getExistingAmend(component, event, helper);
  },

  handleComponentEvent: function (cmp, event, helper) {
    try {
      // get the selected License record from the COMPONETNT event
      var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
      console.log("selected in grandparent");
      console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));

      //component.set("v.selectedRecord", selectedLicenseGetFromEvent);

      cmp.set("v.selectedAmend", selectedLicenseGetFromEvent.amedObj);
      cmp.set("v.selectedAmendWrap", selectedLicenseGetFromEvent);
      cmp.set("v.selectedValue", selectedLicenseGetFromEvent.displayName);
      cmp.set("v.amedIdInFocus", "none");
    } catch (error) {
      console.log(error.message);
    }
  },

  handleComponentEventClear: function (cmp, evt, helper) {
    cmp.set("v.selectedValue", "");
    cmp.set("v.amedIdInFocus", "none");
  },

  onChange: function (cmp, evt, helper) {
    console.log("onchange of the picklicst");

    //set selectedValue to the id of the selected amendobj from dropdown
    var otherList = cmp.get("v.respWrap.amendWrappList.AmendListForPicklist");
    var index = cmp.find("select").get("v.value");
    console.log(index);

    var selectedAmend = otherList.find(
      (amendwrap) => amendwrap.amedObj.Id === index
    );
    console.log(selectedAmend);
    cmp.set("v.selectedAmend", selectedAmend.amedObj);
    cmp.set("v.amedIdInFocus", "none");
  },

  showConfirmForm: function (cmp, evt, helper) {
    var selectedAmend = cmp.get("v.selectedAmendWrap");

    cmp.set("v.amedIdInFocus", selectedAmend.displayName);
  },

  createNewAppPerson: function (component, event, helper) {
    console.log("show create new amend rec");

    try {
      var action = component.get("c.initNewAmendment");

      var requestWrap = {
        srId: component.get("v.srId"),
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(response.getReturnValue());
          component.set("v.newAmendWrap", respWrap.newAmendWrappToCreate);
          component.set("v.amedIdInFocus", "newRec");
          component.set("v.ShowNewForm", true);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }

    //component.set("v.amedIdInFocus", "newRec");
  },

  handleButtonAct: function (component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId,
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");

          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },
});