({
    handleRecordType  : function(cmp, event, helper){
       var index = event.target.dataset.amedtype;
       console.log('###############@@@@@@@@@@'+ index);
       cmp.set("v.recordtypename", index);
       console.log(cmp.get("v.recordtypename"))
       //console.log('cmp.srObj.Id::'+cmp.get('v.srWrap').srObj.Id);
       console.log('cmp.srOldWrap.Id::'+cmp.get('v.srOldWrap'));
       console.log('cmp.srOldWrap.Id::'+JSON.stringify(cmp.get('v.srOldWrap.collateralObjLst')));
       //cmp.set("v.isSelectedIndividual", (index == 'Collateral') ?  true : false);
       cmp.set("v.isSelectedCorporate", (index == 'Collateral') ? true : false);
       //cmp.set("v.isIndividual", (index == 'Individual') ?  true : false);
       cmp.set("v.isCollateral", (index == 'Collateral') ?  true : false);
          
        var initDebatSecurity = cmp.get('c.initCollateralDetails');   
        var reqWrapPram  =
        {
            srId : cmp.get('v.srWrap').srObj.Id,
            recordtypename: cmp.get("v.recordtypename"),
            approvedFormId :cmp.get('v.srWrap').approvedFormOneROS.Id,
            //collateralObjLst :cmp.get('v.srOldWrap.collateralObjLst'),
            //rosRelObjLst :cmp.get('v.srOldWrap.rosRelObjList')
        };
        console.log('srId::'+cmp.get('v.srWrap').srObj.Id);
        console.log('recordtypename::'+cmp.get("v.recordtypename"));
        console.log('approvedFormId::'+cmp.get('v.srWrap').approvedFormOneROS.Id);
        initDebatSecurity.setParams(
        {
            "reqWrapPram": JSON.stringify(reqWrapPram),
        });
       
        initDebatSecurity.setCallback(this,function(response) {
                var state = response.getState();
                console.log("callback state: " + state);
                
                if (cmp.isValid() && state === "SUCCESS"){
                    var respWrap = response.getReturnValue();
                    console.log('######### respWrap.amedWrap  ',respWrap.amedWrap);
                    console.log('######### respWrap.collDetailList  ',respWrap.amedWrapList);
                    cmp.set('v.amedWrap',respWrap.amedWrap);
                    cmp.set('v.amedWrapList', respWrap.amedWrapList);
                    console.log('######### amedWrap.collObj  ',cmp.get("v.amedWrapList"));
                    console.log('######### v.amedWrap ',cmp.get("v.amedWrap"));
                }else{
                    console.log('@@@@@ Error '+response.getError()[0].message);
                    helper.createToast(cmp,event,response.getError()[0].message);
                
                }
            }    
        );
        $A.enqueueAction(initDebatSecurity);
       console.log(cmp.get("v.recordtypename"))
     }
   
})