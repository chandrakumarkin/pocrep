({
  approvedData: function(component, event, helper) {
    var action = component.get("c.getStepData");
    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        const responseWrap = response.getReturnValue();
        component.set("v.respWrap", responseWrap.srObjRecWrapList);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log("@@@@@ Error " + response.getError()[0].stacktrace);
      }
    });
    $A.enqueueAction(action);
  }
});