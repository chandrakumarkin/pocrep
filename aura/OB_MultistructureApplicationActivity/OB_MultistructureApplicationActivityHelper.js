({
  searchHelper: function (component, event, getInputkeyWord) {
    let newURL = new URL(window.location.href).searchParams;
    var srID = newURL.get("srId");
    // component.set('v.srId',srID);
    console.log("=========srID========");
    console.log(srID);
    var action = component.get("c.getLicenseActivityMaster");
    // set param to method
    action.setParams({
      searchKeyWord: getInputkeyWord,
      srId: srID,
      childSRObj: component.get("v.entityRec")
    });
    // set a callBack
    action.setCallback(this, function (response) {
      $A.util.removeClass(component.find("mySpinner"), "slds-show");
      var state = response.getState();
      if (state === "SUCCESS") {
        var storeResponse = response.getReturnValue();

        console.log(storeResponse);

        if (storeResponse.length == 0) {
          component.set("v.Message", "No Result Found...");
        } else {
          component.set("v.Message", "");
        }
        // set searchResult list with return value from server.
        component.set("v.listOfSearchRecords", storeResponse);
        console.log("===========listOfSearchRecords============");
      }
    });
    // enqueue the Action
    $A.enqueueAction(action);
  },

  clearsearch: function (component, event, helper) {
    var pillTarget = component.find("lookup-pill");
    var lookUpTarget = component.find("lookupField");

    $A.util.addClass(pillTarget, "slds-hide");
    $A.util.removeClass(pillTarget, "slds-show");

    $A.util.addClass(lookUpTarget, "slds-show");
    $A.util.removeClass(lookUpTarget, "slds-hide");

    component.set("v.SearchKeyWord", null);
    component.set("v.listOfSearchRecords", null);
    component.set("v.selectedRecord", {});
  },
  /**
   * Display a message
   */
  displayToast: function (title, message, type) {
    var toast = $A.get("e.force:showToast");

    // For lightning1 show the toast
    if (toast) {
      //fire the toast event in Salesforce1
      toast.setParams({
        title: title,
        message: message,
        mode: "dismissible",
        type: type,
        duration: 8000
      });

      toast.fire();
    } else {
      // otherwise throw an alert
      alert(title + ": " + message);
    }
  },

  refreshParent: function (component, event, helper) {
    try {
      var refeshEvnt = component.getEvent("refeshEvnt");

      /* refeshEvnt.setParams({
          isHardRefresh: isHardRefresh
        }); */

      refeshEvnt.fire();
    } catch (error) {
      console.log(error.message);
    }
  },

  showHideFields: function (component, event) {
    console.log("in show/hide");

    var serviceRequestObj = JSON.parse(
      JSON.stringify(component.get("v.srObj"))
    );

    var businessSector = serviceRequestObj.Business_Sector__c;
    var entityType = serviceRequestObj.Entity_Type__c;

    //Financial AOR check

    if (entityType) {
      if (entityType == "Financial - related") {
        component.set("v.FinancialAOR", true);
      }
      if (
        entityType == "Non - financial" &&
        businessSector &&
        businessSector == "Investment Fund"
      ) {
        component.set("v.FinancialAOR", true);
      }
    }

    var natureOfBusiness = [
      "prescribed companies",
      "investment fund",
      "general partner for a limited partnership fund",
      "financial - related"
    ];

    var isParentOrEntityBusinessActivities = [
      "single family office",
      "investment fund",
      "general partner for a limited partnership fund"
    ];

    var activityList = JSON.parse(
      JSON.stringify(component.get("v.LicesenceActivityTable"))
    );
    var isOB_DNFBP = false;
    var isBuyOrSell = false;
    var activityMaster;
    var OB_DNFBPArr = [];
    var buyOrSellArr = [];
    var preciousMetal = [];
    console.log(activityList);

    //Hides the Nature of business field for the below condtion
    if (
      (entityType && entityType.toLowerCase() == "financial") ||
      (businessSector &&
        natureOfBusiness.indexOf(businessSector.toLowerCase()) > -1)
    ) {
      component.set("v.isNatureOfBusinessRequired", false);
    }

    //If one of the selected activity master has OB_DNFBP__c = 'Yes', push OB_DNFBPArr array with value
    for (var i = 0; i < activityList.length; i++) {
      activityMaster = activityList[i].Activity__r;

      if (activityMaster.Sys_Code__c == "368") {
        component.set("v.showUpload", true);
      } else {
        component.set("v.showUpload", false);
      }

      if (
        activityMaster.OB_DNFBP__c &&
        activityMaster.OB_DNFBP__c.toLowerCase() == "yes"
      ) {
        OB_DNFBPArr.push(activityMaster.OB_DNFBP__c.toLowerCase());
      }
      //console.log('activityMaster.OB_DNFBP__c'+activityMaster.OB_DNFBP__c);
      //console.log('activityMaster.OB_Sell_or_Buy_Real_Estate__c'+activityMaster.OB_Sell_or_Buy_Real_Estate__c);
      if (
        activityMaster.OB_Sell_or_Buy_Real_Estate__c &&
        activityMaster.OB_Sell_or_Buy_Real_Estate__c.toLowerCase() == "yes"
      ) {
        buyOrSellArr.push(
          activityMaster.OB_Sell_or_Buy_Real_Estate__c.toLowerCase()
        );
      }

      if (
        activityMaster.OB_Trade_Precious_Metal__c &&
        activityMaster.OB_Trade_Precious_Metal__c.toLowerCase() == "yes"
      ) {
        preciousMetal.push(
          activityMaster.OB_Trade_Precious_Metal__c.toLowerCase()
        );
      }
    }
    //Shows the buy or sell property field based on the below condition
    if (buyOrSellArr.length > 0 && buyOrSellArr.indexOf("yes") > -1) {
      component.set("v.isSellOrBuy", true);
      //component.set("v.srObj.DNFBP__c", true);
    } else {
      component.set("v.isSellOrBuy", false);
      component.set("v.srObj.Buying_Selling_Real_Estate_Property__c", "");
      component.set("v.srObj.DNFBP__c", false);
    }

    //Shows dealing with precious metal
    if (preciousMetal.length > 0 && preciousMetal.indexOf("yes") > -1) {
      component.set("v.isDealingWithPreciousMetal", true);
      //component.set("v.srObj.DNFBP__c", true);
    } else {
      component.set("v.isDealingWithPreciousMetal", false);
      component.set("v.srObj.Dealing_in_Precious_Metals__c", "");
      component.set("v.srObj.DNFBP__c", false);
    }

    //Hides the ParentOrEntityBusinessActivities field for the below condtion
    if (
      (entityType && entityType.toLowerCase() == "Financial - related") ||
      (businessSector &&
        isParentOrEntityBusinessActivities.indexOf(
          businessSector.toLowerCase()
        ) > -1) ||
      OB_DNFBPArr.indexOf("yes") > -1
    ) {
      component.set("v.isParentOrEntityBusinessActivitiesRequired", false);
    } else {
      component.set("v.isParentOrEntityBusinessActivitiesRequired", true);
    }
  },

  handleButtonAct: function (component, event, helper) {
    let listAct = component.get("v.LicesenceActivityTable");

    var fieldList = component.find("input-field") || null;
    console.log(fieldList);
    if (fieldList != null) {
      console.log("f");
      try {
        var invalidFields = helper.isFormValid(component);
        console.log(invalidFields);
      } catch (err) {
        console.log(err.message);
      }

      //if (fieldList.get('v.value')=='' || fieldList.get('v.value').trim().length==0 ) {
      //isValid = false;
      //console.log(fieldList.get);
    }

    console.log(listAct.length);

    if (listAct.length < 1) {
      helper.displayToast("Information", "Please add at least 1 activity");
      //return;
    }

    /*
        try{
            
        let newURL = new URL(window.location.href).searchParams;
        
        var srID = newURL.get('srId');
        var buttonId =  event.target.id;
        console.log(buttonId);
            
        var action = component.get("c.getButtonAction");
        
        action.setParams({
            "SRID": srID,
            "serviceRequest":component.get("v.srObj"),
            "ButtonId": buttonId
        });
        console.log(JSON.parse(JSON.stringify(component.get("v.srObj"))));
        action.setCallback(this, function(response) {
        
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("button succsess")
            	
                console.log(response.getReturnValue());
                window.open(response.getReturnValue().pageActionName,"_self");
            }else{
            	console.log('@@@@@ Error '+response.getError()[0].message);
			}
        });
        $A.enqueueAction(action);
          
        }catch(err) {
           console.log(err.message) 
        } 
        */
  },

  formValidator: function (component, event, helper, fieldsToValidate) {
    let fields = component.find(fieldsToValidate);

    if (fields == undefined) {
      return true;
    } else if (!Array.isArray(fields)) {
      if (fields.get("v.value") != undefined) {
        return fields.get("v.value").trim().length > 0 ? true : false;
      } else {
        return false;
      }
    } else if (Array.isArray(fields)) {
      for (const field of fields) {
        if (field.get("v.value") != undefined) {
          if (!field.get("v.value").trim().length > 0) {
            return false;
          }
        } else {
          return false;
        }
      }
      return true;
    }
  }

  /*
    createLicenseonLoad: function(component,event,helper){
        var action =  component.get("c.saveLicense");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log('===========license created============');
            } 
            else {
                console.log('===========license is not created============');
            }
            
            
        });
        
        $A.enqueueAction(action);
        
    }
    */
});