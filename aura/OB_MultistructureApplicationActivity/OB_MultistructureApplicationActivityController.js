({
  doinit: function (component, event, helper) {
    console.log("=================doInit==============="); //
    //helper.createLicenseonLoad(component,event,helper);

    let newURL = new URL(window.location.href).searchParams;

    var pageflowId = newURL.get("flowId");
    var pageId = newURL.get("pageId");
    var srID = component.get("v.currentSrId");
    console.log("srID" + srID);

    var action = component.get("c.getLicenseActivityData");
    var self = this;
    var requestWrap = {
      flowID: pageflowId,
      pageId: pageId,
      srId: srID
    };
    action.setParams({
      requestWrapParam: JSON.stringify(requestWrap)
    });
    action.setCallback(this, function (response) {
      //store state of response
      var state = response.getState();
      if (state === "SUCCESS") {
        var responseResult = response.getReturnValue();
        component.set("v.srObj", responseResult.serviceObj);
        console.log(response.getReturnValue());
        component.set(
          "v.searchBarLable",
          response.getReturnValue().SearchBarLable
        );
        component.set("v.LicesenceActivityTable", responseResult.listLiscAct);
        component.set("v.commandButton", responseResult.ButtonSection);
        component.set("v.filename", responseResult.uploadedFileName);

        component.set(
          "v.licenseApplicationId",
          responseResult.licenseApplicationId
        );
        component.set("v.isSellOrBuy", responseResult.isBuyOrSell);

        helper.showHideFields(component, event);
      } else if (state === "ERROR") {
        // Handle any error by reporting it
        var errors = response.getError();

        if (errors) {
          if (errors[0] && errors[0].message) {
            helper.displayToast("Error", errors[0].message);
          }
        } else {
          helper.displayToast("Error", "Unknown error.");
        }
      }
    });
    $A.enqueueAction(action);
  },
  onfocus: function (component, event, helper) {
    $A.util.addClass(component.find("mySpinner"), "slds-show");
    var forOpen = component.find("searchRes");
    $A.util.addClass(forOpen, "slds-is-open");
    $A.util.removeClass(forOpen, "slds-is-close");
    var getInputkeyWord = "";
    helper.searchHelper(component, event, getInputkeyWord);
  },
  onblur: function (component, event, helper) {
    component.set("v.listOfSearchRecords", null);
    var forclose = component.find("searchRes");
    $A.util.addClass(forclose, "slds-is-close");
    $A.util.removeClass(forclose, "slds-is-open");
  },
  keyPressController: function (component, event, helper) {
    // get the search Input keyword
    var getInputkeyWord = component.get("v.SearchKeyWord");
    console.log("=======getInputkeyWord=========");
    console.log(getInputkeyWord);
    if (getInputkeyWord.length > 0) {
      var forOpen = component.find("searchRes");
      $A.util.addClass(forOpen, "slds-is-open");
      $A.util.removeClass(forOpen, "slds-is-close");
      helper.searchHelper(component, event, getInputkeyWord);
    } else {
      component.set("v.listOfSearchRecords", null);
      var forclose = component.find("searchRes");
      $A.util.addClass(forclose, "slds-is-close");
      $A.util.removeClass(forclose, "slds-is-open");
    }
  },

  // function for clear the Record Selaction
  clear: function (component, event, helper) {
    helper.clearsearch(component, event, helper);
  },

  // This function call when the end User Select any record from the result list.
  handleComponentEvent: function (component, event, helper) {
    debugger;
    // get the selected License record from the COMPONETNT event
    var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
    console.log(selectedLicenseGetFromEvent);
    console.log("selected");
    console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));

    component.set("v.selectedRecord", selectedLicenseGetFromEvent);

    console.log("=============recordByEvent===========");
    var forclose = component.find("lookup-pill");
    $A.util.addClass(forclose, "slds-show");
    $A.util.removeClass(forclose, "slds-hide");

    var forclose = component.find("searchRes");
    $A.util.addClass(forclose, "slds-is-close");
    $A.util.removeClass(forclose, "slds-is-open");

    var lookUpTarget = component.find("lookupField");
    $A.util.addClass(lookUpTarget, "slds-hide");
    $A.util.removeClass(lookUpTarget, "slds-show");
  },

  selectLicenseActivityRecord: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;

    var srID = component.get("v.currentSrId");

    if (component.get("v.entityRec.Business_Sector__c") == "Foundation") {
      const selectedActCode = component.get("v.selectedRecord")
        .Activity_Code__c;
      console.log(selectedActCode);
      var carityActivityCodeList = ["ONC", "OEC"];
      const actTable = component.get("v.LicesenceActivityTable");
      if (carityActivityCodeList.includes(selectedActCode)) {
        for (const activity of actTable) {
          if (
            carityActivityCodeList.includes(
              activity.Activity__r.Activity_Code__c
            ) &&
            activity.Activity_Code__c != selectedActCode
          ) {
            helper.displayToast(
              "Charitable and non charitable activities cannot be selected together",
              " ",
              "error"
            );
            return;
          }
        }
      }
    }

    console.log(component.get("v.selectedRecord").Id);
    console.log(srID);
    if (component.get("v.selectedRecord").Id) {
      //LicenseObj.Activity__c = component.get("v.selectedRecord").Id;
      //console.log('====== LicenseObjActivity__c===' +  JSON.stringify(LicenseObj.Activity__c));
      var action = component.get("c.selectLicenseActivity");
      action.setParams({
        ActivityId: component.get("v.selectedRecord").Id
      });
      action.setCallback(this, function (response) {
        //store state of response
        var state = response.getState();
        if (state === "SUCCESS") {
          var responseResult = response.getReturnValue();
          console.log(response.getReturnValue());
          var selectLicesenceActivityList = component.get(
            "v.LicesenceActivityTable"
          );
          selectLicesenceActivityList.push(response.getReturnValue());
          component.set(
            "v.LicesenceActivityTable",
            selectLicesenceActivityList
          );
          helper.clearsearch(component, event, helper);
          debugger;
          var selectedActivityMaster = JSON.parse(
            JSON.stringify(component.get("v.selectedRecord"))
          );
        } else if (state === "ERROR") {
          // Handle any error by reporting it
          var errors = response.getError();

          if (errors) {
            if (errors[0] && errors[0].message) {
              helper.displayToast("Error", errors[0].message);
            }
          } else {
            helper.displayToast("Error", "Unknown error.");
          }
        }
      });
      $A.enqueueAction(action);
    }
  },

  saveLicenseActivityRecord: function (component, event, helper) {
    debugger;
    let newURL = new URL(window.location.href).searchParams;

    var pageflowId = newURL.get("flowId");
    var pageId = newURL.get("pageId");
    var srID = newURL.get("srId");

    var activityList = component.get("v.LicesenceActivityTable");
    var srRecord = component.get("v.entityRec");
    var srObj = component.get("v.srObj");

    component.set("v.showSpinner", true);
    console.log("srID==>" + srID);
    console.log("entityRec==>" + JSON.stringify(srRecord));
    console.log("activityList==>" + JSON.stringify(activityList));

    if ($A.util.isEmpty(srRecord.Entity_Name__c)) {
      helper.displayToast("Information", "Please fill Entity Name");
      component.set("v.showSpinner", false);
      return;
    }

    if ($A.util.isEmpty(srRecord.Entity_Type__c)) {
      helper.displayToast("Information", "Please fill Entity Type");
      component.set("v.showSpinner", false);
      return;
    }
    if ($A.util.isEmpty(srRecord.Type_of_Entity__c)) {
      helper.displayToast("Information", "Please fill type of entity");
      component.set("v.showSpinner", false);
      return;
    }
    if ($A.util.isEmpty(srRecord.Setting_Up__c)) {
      helper.displayToast("Information", "Are you setting up a?");
      component.set("v.showSpinner", false);
      return;
    }

    if ($A.util.isEmpty(srRecord.Business_Sector__c)) {
      helper.displayToast("Information", "Please fill Business Sector");
      component.set("v.showSpinner", false);
      return;
    }

    if ($A.util.isEmpty(srRecord.legal_structures__c)) {
      helper.displayToast("Information", "Please fill Legal Struct");
      component.set("v.showSpinner", false);
      return;
    }
    if ($A.util.isEmpty(srObj.Nature_of_Business__c)) {
      helper.displayToast(
        "Information",
        " Please elaborate further on the activities that you plan to conduct"
      );
      component.set("v.showSpinner", false);
      return;
    }
    if (activityList.length < 1) {
      helper.displayToast("Information", "Please add at least 1 activity");
      component.set("v.showSpinner", false);
      return;
    }
    srRecord.Nature_of_Business__c = srObj.Nature_of_Business__c;
    var listActivityId = [];
    for (var i = 0; i < activityList.length; i++) {
      listActivityId.push(activityList[i].Activity__r.Id);
    }
    console.log("listActivityId==>" + listActivityId);
    console.log("selectedSr ==>");
    console.log(srRecord);

    var action = component.get("c.saveLicenseActivity");
    action.setParams({
      activityList: listActivityId,
      srRecord: srRecord,
      srId: srID
    });

    action.setCallback(this, function (response) {
      //store state of response
      var state = response.getState();
      if (state === "SUCCESS") {
        var responseResult = response.getReturnValue();
        console.log(responseResult);

        console.log("Response==>" + JSON.stringify(responseResult));
        if (responseResult.hasError == false) {
          component.set("v.showSpinner", false);
          component.set("v.currentSrId", "");
          component.set("v.showAppInfo", false);
          helper.refreshParent(component, event, helper);
        } else {
          component.set("v.showSpinner", false);
          helper.displayToast("Error", responseResult.ErrorMessage);
        }
        // $A.get('e.force:refreshView').fire();
      } else if (state === "ERROR") {
        // Handle any error by reporting it
        component.set("v.showSpinner", false);
        component.set("v.currentSrId", "");
        //component.set("v.showAppInfo", false);
        var errors = response.getError();

        if (errors) {
          if (errors[0] && errors[0].message) {
            helper.displayToast("Error", errors[0].message);
          }
        } else {
          component.set("v.showSpinner", false);
          component.set("v.currentSrId", "");
          component.set("v.showAppInfo", false);
          helper.displayToast("Error", "Unknown error.");
        }
      }
    });
    $A.enqueueAction(action);
  },

  delete: function (component, event, helper) {
    console.log(event.target.id);

    var idOfItemToBeDeleted = event.target.id;
    var index = event.target.dataset.index;
    console.log(index);

    let newURL = new URL(window.location.href).searchParams;
    var srID = newURL.get("srId");
    console.log(component.get("v.licenseApplicationId"));
    $A.util.addClass(component.find("mySpinner"), "slds-show");
    var action = component.get("c.deleteLicenseActivity");

    action.setParams({
      LicenseId: event.target.id,
      srID: srID,
      licenseAppId: component.get("v.licenseApplicationId")
    });

    action.setCallback(this, function (response) {
      console.log("======deleteLicenseActivity======");
      //$A.util.removeClass(component.find("mySpinner"), "slds-show");
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        try {
          var responseResult = response.getReturnValue();
          var licenceTable = component.get("v.LicesenceActivityTable");
          licenceTable.splice(index, 1);
          console.log(licenceTable);

          component.set("v.LicesenceActivityTable", licenceTable);
          //component.set("v.LicesenceActivityTable", responseResult.listLiscAct);
          component.set("v.isSellOrBuy", responseResult.isBuyOrSell);
          if (
            component.get("v.srObj.RecordType.DeveloperName") != "AOR_Financial"
          ) {
            helper.showHideFields(component, event);
          }
        } catch (error) {
          console.log(error.message);
        }

        //helper.clearsearch(component,event,helper);
      } else if (state === "ERROR") {
        // Handle any error by reporting it
        var errors = response.getError();

        if (errors) {
          if (errors[0] && errors[0].message) {
            helper.displayToast("Error", errors[0].message);
          }
        } else {
          helper.displayToast("Error", "Unknown error.");
        }
      }
    });
    $A.enqueueAction(action);
  },

  handleFileUploadFinished: function (component, event) {
    // Get the list of uploaded files
    var uploadedFiles = event.getParam("files");
    var fileName = uploadedFiles[0].name;

    var contentDocumentId = uploadedFiles[0].documentId;
    console.log(contentDocumentId);
    var docMasterCode = event.getSource().get("v.name");
    console.log(docMasterCode);
    console.log(component.find(docMasterCode).get("v.value"));
    console.log(fileName);
    if (component.find(docMasterCode))
      component.find(docMasterCode).set("v.value", fileName);

    console.log(component.find(docMasterCode).get("v.value"));
    var mapDocValues = {};
    var docMap = component.get("v.docMasterContentDocMap");
    console.log(docMap);
    for (var key in docMap) {
      mapDocValues[key] = docMap[key];
    }
    mapDocValues[docMasterCode] = contentDocumentId;
    console.log(mapDocValues);

    component.set("v.docMasterContentDocMap", mapDocValues);
    console.log(
      JSON.parse(JSON.stringify(component.get("v.docMasterContentDocMap")))
    );
  },

  handleButtonAct: function (component, event, helper) {
    console.log(
      $A.util.isEmpty(component.get("v.srObj.Dealing_in_Precious_Metals__c"))
    );

    let listAct = component.get("v.LicesenceActivityTable");

    if (listAct.length < 1) {
      helper.displayToast("Please add at least one activity", " ", "error");
      return;
    }

    if (component.get("v.srObj.RecordType.DeveloperName") != "AOR_Financial") {
      var allValid = true;

      let fields = component.find("reqField");
      if (fields) {
        if (!Array.isArray(fields)) {
          if (!fields.get("v.value") && fields && fields.get("v.required")) {
            console.log("no value");
            //console.log('bad');
            fields.reportValidity();
            allValid = false;
          }
        } else {
          for (var i = 0; i < fields.length; i++) {
            var inputFieldCompValue = fields[i].get("v.value");
            //console.log(inputFieldCompValue);
            if (
              !inputFieldCompValue &&
              fields[i] &&
              fields[i].get("v.required")
            ) {
              console.log("no value");
              //console.log('bad');
              fields[i].reportValidity();
              allValid = false;
            }
          }
        }
      }

      if (!allValid) {
        console.log("Error MSG");
        displayToast("Error", "Please fill required field", "error");
        return;
      }
    }

    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(JSON.parse(JSON.stringify(component.get("v.srObj"))));

      console.log(component.get("v.docMasterContentDocMap"));

      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        serviceRequest: component.get("v.srObj"),
        pageId: pageID,
        ButtonId: buttonId,
        docMap: component.get("v.docMasterContentDocMap")
      });
      console.log(JSON.parse(JSON.stringify(component.get("v.srObj"))));
      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");
          var respwrap = response.getReturnValue();

          console.log(respwrap.errorMessage);
          if (respwrap.errorMessage) {
            helper.displayToast(respwrap.errorMessage, " ", "error");
          }

          if (respwrap.errorMessage == undefined) {
            console.log(response.getReturnValue());
            console.log(response.getReturnValue().updatedSr);

            window.open(response.getReturnValue().pageActionName, "_self");
          } else {
            console.log(error);

            helper.displayToast("Error", respwrap.errorMessage);
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },
  checkDNFBP: function (component, event, helper) {
    console.log(event.getSource().get("v.value"));
    if (event.getSource().get("v.value") == "Yes") {
      helper.displayToast(
        "Information",
        "Please contact DIFC to find the process for obtaining DNFBP approval"
      );
      component.set("v.srObj.DNFBP__c", true);
    } else if (event.getSource().get("v.value") == "No") {
      component.set("v.srObj.DNFBP__c", false);
    }
  },
  hideDetails: function (component, event, helper) {
    component.set("v.currentSrId", "");
    component.set("v.showAppInfo", false);
  }
});