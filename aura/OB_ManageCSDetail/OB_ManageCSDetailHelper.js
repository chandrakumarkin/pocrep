({
  resetAuthIndivalues: function (cmp) {
    cmp.set("v.amedWrap.amedObj.DI_First_Name__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Last_Name__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Email__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Mobile__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Apartment_or_Villa_Number_c__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Building_Name__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Address__c", "");
    cmp.set("v.amedWrap.amedObj.DI_PO_Box__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Post_Code__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Emirate_State_Province__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Permanent_Native_City__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Permanent_Native_Country__c", "");
  },
  /**
   * Display a message
   */
  displayToast: function (title, message, type) {
    console.log("displayToast");
    var toast = $A.get("e.force:showToast");

    // For lightning1 show the toast
    if (toast) {
      //fire the toast event in Salesforce1
      toast.setParams({
        type: type,
        title: title,
        message: message,
        mode: "sticky",
      });

      toast.fire();
    } else {
      // otherwise throw an alert
      alert(title + ": " + message);
    }
  },
  viewSRDocs: function (cmp, event) {
    var action = cmp.get("c.viewSRDocs");
    console.log("viewSRDocs");
    console.log(cmp.get("v.srId"));
    console.log(cmp.get("v.amendId"));
    action.setParams({
      srId: cmp.get("v.srId"),
      amendmentId: cmp.get("v.amendId"),
    });
    action.setCallback(this, function (response) {
      var state = response.getState();
      console.log(state);
      if (state === "SUCCESS") {
        var result = response.getReturnValue();
        console.log(result.length);
        console.log(result);
        for (var key in result) {
          console.log(result[key]);
          var inputFileId = key + "_filename";
          cmp.find(inputFileId).set("v.value", result[key].FileName);
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },
  disableAllFields: function (cmp, event) {
    console.log("disableAllFields");
    var inputFields = cmp.find("requiredField");
    //console.log(cmp.find('requiredField'));
    var field;
    for (var key in inputFields) {
      console.log(key);
      field = inputFields[key];
      //console.log(field);
      field.set("v.disabled", "true");
    }
    //Disable the lookup
    /*
        var lookup = cmp.find("lookup");
        lookup.set("v.disabled","true");
        */

    //Disable the file upload
    cmp.find("filekey").set("v.disabled", "true");
    //cmp.find("requiredFileCertificate").set("v.disabled","true");
  },
  resetAuthCorvalues: function (cmp) {
    cmp.set("v.amedWrap.amedObj.DI_First_Name__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Last_Name__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Email__c", "");
    cmp.set("v.amedWrap.amedObj.DI_Mobile__c", "");
  },
  setRecordTypeChange: function (cmp, event) {
    console.log("setRecordTypeChange");
    var selectedAmendment = cmp.get("v.amedWrap.amedObj");
    var selectedAmendmentWrap = cmp.get("v.amedWrap");
    console.log(selectedAmendment);
    console.log("selectedAmendment");
    console.log(selectedAmendmentWrap);
    if (
      selectedAmendment &&
      selectedAmendment.RecordType &&
      selectedAmendment.RecordType.DeveloperName
    ) {
      var recordTypeName = selectedAmendment.RecordType.DeveloperName;
      if (
        recordTypeName == "Individual" ||
        selectedAmendmentWrap.isIndividual == true
      ) {
        cmp.set("v.amedWrap.amedObj.Amendment_Type__c", "Individual");
        cmp.set("v.isIndividual", true);
        cmp.set("v.isCorporate", false);
      } else {
        cmp.set("v.amedWrap.amedObj.Amendment_Type__c", "Body Corporate");
        cmp.set("v.isCorporate", true);
        cmp.set("v.isIndividual", false);
      }
    } else {
      if (selectedAmendmentWrap.isIndividual == true) {
        cmp.set("v.amedWrap.amedObj.Amendment_Type__c", "Individual");
        cmp.set("v.isIndividual", true);
        cmp.set("v.isCorporate", false);
      } else {
        cmp.set("v.amedWrap.amedObj.Amendment_Type__c", "Body Corporate");
        cmp.set("v.isCorporate", true);
        cmp.set("v.isIndividual", false);
      }
    }
  },
  fileUploadHelper: function (cmp, event, helper) {
    //New method.
    console.log("########### fileHolder Start");
    // For file upload and validation.
    var fileUploadValid = true;
    var fileHolder = cmp.find("filekey");

    if (fileHolder && Array.isArray(fileHolder)) {
      for (var i = 0; i < fileHolder.length; i++) {
        if (fileHolder[i].get("v.requiredDocument")) {
          helper.fileUploadProcess(cmp, event, helper, fileHolder[i]);
          fileUploadValid = fileHolder[i].get("v.fileUploaded");
          //requiredText
          if (!fileUploadValid) {
            fileHolder[i].set("v.requiredText", "Please upload document.");
          }
        }
      }
    } else if (fileHolder) {
      console.log(
        "########### fileHolder content docID ",
        fileHolder.get("v.contentDocumentId")
      );
      //here
      helper.fileUploadProcess(cmp, event, helper, fileHolder);

      if (fileHolder.get("v.requiredDocument")) {
        fileUploadValid = fileHolder.get("v.fileUploaded");
        console.log("@@@@@@@@@@@ fileUploadValid ", fileUploadValid);
        if (!fileUploadValid) {
          fileHolder.set("v.requiredText", "Please upload document.");
        }
      }
    }

    console.log(" Final file upload ", fileUploadValid);
    if (!fileUploadValid) {
      //helper.displayToast('Error', 'Please upload required files.', 'error');
      //helper.showToast(cmp, event, helper,'Please upload required files', 'error');
      cmp.set("v.spinner", false);
      return "file error";
    }
    console.log("########### fileHolder end");
  },
  fileUploadProcess: function (cmp, event, helper, fileHolder) {
    //New Method
    var contentDocumentId = fileHolder.get("v.contentDocumentId");
    var documentMaster = fileHolder.get("v.documentMaster");

    if (contentDocumentId && documentMaster) {
      var mapDocValues = {};
      var docMap = cmp.get("v.docMasterContentDocMap");
      for (var key in docMap) {
        mapDocValues[key] = docMap[key];
      }

      mapDocValues[documentMaster] = contentDocumentId;
      console.log(mapDocValues);
      console.log("@@@@@@ docMasterContentDocMap ");
      cmp.set("v.docMasterContentDocMap", mapDocValues);
      console.log(
        JSON.parse(JSON.stringify(cmp.get("v.docMasterContentDocMap")))
      );
    }
  },
});