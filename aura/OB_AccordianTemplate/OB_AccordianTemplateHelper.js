({
  getPageFlowDetails: function (component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;

      var pageflowId = newURL.get("flowId");
      var pageId = newURL.get("pageId");
      var srId = newURL.get("srId");

      component.set("v.flowId", pageflowId);
      component.set("v.pageId", pageId);
      component.set("v.srId", srId);

      var action = component.get("c.fetchPageFlowDeatils");
      var requestWrap = {
        flowId: pageflowId,
        pageId: pageId,
        srId: srId,
      };
      console.log(requestWrap);

      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("call to apex class succsess");

          //Locking and unlocking logic
          var respWrap = response.getReturnValue();
          console.log("@@@@ locking " + respWrap.isSRLocked);
          console.log(respWrap);

          if (
            respWrap.isSRLocked &&
            respWrap.flowNAme &&
            !(
              (respWrap.flowNAme == "Commercial_Permission_Renewal" &&
                respWrap.srObj.HexaBPM__Internal_Status_Name__c ==
                  "Return for Edit from BD Detailed Review") ||
              respWrap.srObj.HexaBPM__Internal_Status_Name__c ==
                "Return for Edit from RS Officer Review"
            )
          ) {
            var pageURL = respWrap.communityReviewPageURL;
            window.open(pageURL, "_self");
          }

          component.set(
            "v.pageWrapList",
            response.getReturnValue().pageWrapList
          );
          component.set(
            "v.commandButton",
            response.getReturnValue().ButtonSection
          );
          console.log(response.getReturnValue());
          var compName = component.get("v.TestBinding");
          console.log(compName);
          var createComp = "c:" + compName;

          $A.createComponent(createComp, {}, function (
            childCom,
            status,
            errorMessage
          ) {
            //Add the new button to the body array
            if (status === "SUCCESS") {
              console.log("testing");
              try {
                var body = component.get("v.body");
                console.log("$$$$$$$$$ ", JSON.stringify(body));
                body.push(childCom);
                component.set("v.body", body);
                /* var activeScreen = document.getElementById("activePage");
                console.log("id of active screen ------>" + activeScreen);

                activeScreen.scrollIntoView({
                  block: "start",
                  behavior: "smooth"
                }); */
              } catch (err) {
                console.log(err.message);
              }
            } else if (status === "INCOMPLETE") {
              console.log("No response from server or client is offline.");
              // Show offline error
            } else if (status === "ERROR") {
              console.log("Error: " + errorMessage);
              // Show error message
            }
          });
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log("@@@@@ Error " + response.getError()[0].stackTrace);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },

  handleButtonActHelper: function (component, event, helper) {
    /*
        try{
            
        let newURL = new URL(window.location.href).searchParams;
        
        var srID = newURL.get('srId');
        var buttonId =  event.target.id;
        
        var action = component.get("c.fetchPageFlowDeatils");
        var requestWrap = {
        
            flowID : pageflowId,
            pageId : pageId
        };
        action.setParams({
        
           	"requestWrapParam": JSON.stringify(requestWrap)
        });
     
        action.setCallback(this, function(response) {
        
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("call to apex class succsess")
            	component.set("v.pgLst", response.getReturnValue().pageList);
                component.set("v.commandButton", response.getReturnValue().ButtonSection);
                console.log(response.getReturnValue().ButtonSection);
            }else{
            	console.log('@@@@@ Error '+response.getError()[0].message);
			}
        });
        $A.enqueueAction(action);
          
        }catch(err) {
           console.log(err.message) 
        } 
        */
  },
});