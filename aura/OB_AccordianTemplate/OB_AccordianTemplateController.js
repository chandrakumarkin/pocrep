({
  init: function(component, event, helper) {
    helper.getPageFlowDetails(component, event, helper);
  },

  directToPage: function(component, event, helper) {
    //let newURL = new URL(window.location.href).searchParams;

    var pageDeatil = event.currentTarget.dataset;
    var pageId = pageDeatil.pageid;
    var commPageName = pageDeatil.communitypage;
    var urlParam =
      commPageName +
      "?pageId=" +
      pageId +
      "&flowId=" +
      component.get("v.flowId") +
      "&srId=" +
      component.get("v.srId");
    const encodedUrlParam = encodeURI(urlParam);
    console.log(encodedUrlParam);

    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      url: "/" + encodedUrlParam
    });
    urlEvent.fire();
  },

  handleButtonAct: function(component, event, helper) {
    //helper.handleButtonActHelper(component, event, helper);
    //console.log('buttttton');
    //var buttonId =  event.target.id;
    //console.log(buttonId);
  },

  onRender: function(cmp, event, helper) {
    var activeScreen = document.getElementById("activePage");
    console.log("id of active screen ------>" + activeScreen);
    if (activeScreen) {
      activeScreen.scrollIntoView({
        block: "start",
        behavior: "smooth"
      });
    }
  }
});