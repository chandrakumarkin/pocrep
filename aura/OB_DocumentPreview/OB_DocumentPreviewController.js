({
	 doInit: function(cmp, event, helper) 
    {
        try 
        {
          var getFormAction = cmp.get("c.getSRDocs");
          let newURL = new URL(window.location.href).searchParams;
          var srIdParam = newURL.get("srId")
            ? newURL.get("srId")
            : cmp.get("v.srId");
        
    
          var reqWrapPram = {
            srId: srIdParam
           
          };
    
          getFormAction.setParams({
            reqWrapPram: JSON.stringify(reqWrapPram)
          });

      console.log(
        "@@@@@@@@@@33121 reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
          getFormAction.setCallback(this, function(response) 
          {
            var state = response.getState();
            console.log("callback state: " + state);
    
            if (cmp.isValid() && state === "SUCCESS") 
            {
              var respWrap = response.getReturnValue();
              console.log("### draft ", respWrap.SRDocs);
              cmp.set("v.docWrapp", respWrap.SRDocs);
              
            } 
            else 
            {
              console.log("@@@@@ Error in document preview " + response.getError()[0].message);
            }
          });
          $A.enqueueAction(getFormAction);
    } 
    catch (err) 
    {
      console.log("=error in document preview ===" + err.message);
    }
  },
    
  getSelected: function(component, event, helper) {
    /*
      console.log('getSelected');
      component.set("v.hasModalOpen" , true);
      component.set("v.selectedDocumentId" , event.currentTarget.getAttribute("data-Id")); 
      */

    var contentId = event.currentTarget.getAttribute("data-Id");
    console.log(contentId);
    console.log($A.get("e.lightning:openFiles"));
    var openFileEvent = $A.get("e.lightning:openFiles");
    if (openFileEvent) {
      $A.get("e.lightning:openFiles").fire({
        recordIds: [contentId]
      });
    } else {
      window.location.href =
        "/lightning/r/ContentDocument/" + contentId + "/view";
    }
  },  
    
   closeModal: function(component, event, helper) {
    // for Close Model, set the "hasModalOpen" attribute to "FALSE"
    component.set("v.hasModalOpen", false);
    component.set("v.selectedDocumentId", null);
  },  
    
})