({
  doInit: function (component, event, helper) {
    helper.getExistingAmend(component, event, helper);
  },

  handleComponentEvent: function (cmp, event, helper) {
    try {
      // get the selected License record from the COMPONETNT event
      var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
      console.log("selected in grandparent");
      console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));

      //component.set("v.selectedRecord", selectedLicenseGetFromEvent);

      cmp.set("v.selectedAmend", selectedLicenseGetFromEvent.amedObj);
      cmp.set("v.selectedAmendWrap", selectedLicenseGetFromEvent);
      cmp.set("v.selectedValue", selectedLicenseGetFromEvent.displayName);
      cmp.set("v.amedIdInFocus", "none");
    } catch (error) {
      console.log(error.message);
    }
  },

  handleComponentEventClear: function (cmp, evt, helper) {
    cmp.set("v.selectedValue", "");
    cmp.set("v.amedIdInFocus", "none");
  },

  onChange: function (cmp, evt, helper) {
    console.log("onchange of the picklicst");

    //set selectedValue to the id of the selected amendobj from dropdown
    var otherList = cmp.get("v.AmendListWrapp.otherRolesAmendList");
    var index = cmp.find("select").get("v.value");
    console.log(index);

    var selectedAmend = otherList.find((amend) => amend.Id === index);
    console.log(selectedAmend);
    cmp.set("v.selectedAmend", selectedAmend);
    cmp.set("v.amedIdInFocus", "none");
    //set selectedValue to the id of the selected amendobj from dropdown
    /* var otherList = cmp.get("v.AmendListWrapp.otherRolesAmendList");
    var index = cmp.find("select").get("v.value");
    var selectedAmend = otherList[index];
    cmp.set("v.selectedAmend", selectedAmend);
    console.log(selectedAmend);

    if (selectedAmend != undefined) {
      cmp.set("v.selectedValue", selectedAmend.Id);
    } else {
      cmp.set("v.selectedValue", "");
    }
    console.log(cmp.get("v.selectedValue"));
    cmp.set("v.amedIdInFocus", "none"); */
  },

  showConfirmFormForSenManag: function (cmp, evt, helper) {
    var selectedAmend = cmp.get("v.selectedAmendWrap");

    cmp.set("v.amedIdInFocus", selectedAmend.displayName);
    cmp.set("v.roleToAssign", "Senior Management");
  },

  showConfirmFormForGenClass: function (cmp, evt, helper) {
    var selectedAmend = cmp.get("v.selectedAmendWrap");

    cmp.set("v.amedIdInFocus", selectedAmend.displayName);
    cmp.set("v.roleToAssign", "General Classification");
  },
  showConfirmFormForEContact: function (cmp, evt, helper) {
    var selectedAmend = cmp.get("v.selectedAmendWrap");

    cmp.set("v.amedIdInFocus", selectedAmend.displayName);
    cmp.set("v.roleToAssign", "Emergency contact");
  },

  createNewSeniorManagement: function (component, event, helper) {
    try {
      var action = component.get("c.initNewAmendment");

      var requestWrap = {
        srId: component.get("v.srId"),
        roleToAssign: "Senior Management",
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(response.getReturnValue());
          component.set("v.newAmendWrap", respWrap.newAmendWrappToCreate);

          component.set("v.roleToAssign", "Senior Management");
          component.set("v.amedIdInFocus", "newSM");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },

  createNewGenClassification: function (component, event, helper) {
    try {
      var action = component.get("c.initNewAmendment");

      var requestWrap = {
        srId: component.get("v.srId"),
        roleToAssign: "General Classification",
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(response.getReturnValue());
          component.set("v.newAmendWrap", respWrap.newAmendWrappToCreate);

          component.set("v.roleToAssign", "General Classification");
          component.set("v.amedIdInFocus", "newGC");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },

  createNewEmergencyContact: function (component, event, helper) {
    try {
      var action = component.get("c.initNewAmendment");

      var requestWrap = {
        srId: component.get("v.srId"),
        roleToAssign: "Emergency contact",
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(response.getReturnValue());
          component.set("v.newAmendWrap", respWrap.newAmendWrappToCreate);

          component.set("v.roleToAssign", "Emergency contact");
          component.set("v.amedIdInFocus", "newEC");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },

  handleButtonAct: function (component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId,
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");

          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },
});