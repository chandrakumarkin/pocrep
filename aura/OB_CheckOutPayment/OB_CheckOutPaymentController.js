({
	doInit : function(component, event, helper) 
	{
		
		component.set("v.showSpinner", true);

		let newURL = new URL(window.location.href).searchParams;
		var payTokenfun = getUrlParameter("cko-session-id");
		var privateKey = $A.get("$Label.c.Frames_Private_Key");
		component.set("v.paymentToken",payTokenfun);
		var xmlHttp = new XMLHttpRequest();
		
		var url = $A.get("$Label.c.OB_Frames_Payment_URL")+'/payments/'+payTokenfun;
		
		
		xmlHttp.open("GET", url, true);
		xmlHttp.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
		
		xmlHttp.setRequestHeader('Authorization', privateKey);
		xmlHttp.responseType = 'text';
		
		xmlHttp.onload = function () {
			
			if (xmlHttp.readyState === 4) {  
				if (xmlHttp.status === 200) {
					
					var jsonPay = xmlHttp.response;
					var parsedPay = JSON.parse(jsonPay);
					
					var respPay = parsedPay;
					console.log(respPay);
					var amountPaid = respPay.amount;
					//For all other currencies, divide the value by 100 to calculate the charge amount; for example, value = 100 is equivalent to 1 US Dollar.
					amountPaid = amountPaid/100; //https://archive.docs.checkout.com/docs/calculating-the-value
					var last4= respPay.source.last4;
					var createdDate= respPay.requested_on;
					var checkoutPaymentID = respPay.id;
					var cardType = respPay.source.scheme;
					var category = respPay.metadata ? respPay.metadata.category : '';
					var receiptId = respPay.metadata ? respPay.metadata.receiptReference : '';
					var srIdVal = respPay.reference;
					var bankReferenceNumber = respPay.scheme_id;
					component.set('v.amountPaid',amountPaid);
					component.set('v.checkoutPaymentID',checkoutPaymentID);
					component.set('v.cardType',cardType);
					component.set('v.last4',last4);
					component.set('v.createdDate',createdDate);
					component.set("v.bankReferenceNumber",bankReferenceNumber);
					component.set("v.srId",srIdVal);
					component.set("v.receiptId",receiptId);
					if(respPay.approved)
					{

						
						if(!srIdVal || srIdVal == 'null'){
							var productDtl = {name:'Top up Fees',
												total: amountPaid
											};
							component.set("v.productInfo",productDtl);
							helper.updateReceiptRecord(component,event, $A.get("$Label.c.OB_Receipt_Status"));
						}
						else if( category == 'namerenewal' )
						{
							console.log('######### namerenewal ');
							//added by merul for name renewal.
							helper.updateSRPriceItems(component, event, helper, component.get("v.srId"));
							// process name renewal...remiaing 
							console.log("calling processRenewalSR");
							helper.processRenewalSR(component, event, helper);

							component.set("v.showSpinner", false);
						}
						else{
							if(category == 'namereserve'){
								
								var action = component.get("c.createNameReservationSR");   
								action.setParams({
									srId : srIdVal
								});
								action.setCallback(this, function(response) {
									var retVal = response.getReturnValue();
									var state= response.getState();
									
									if(state=="SUCCESS"){
										component.set("v.responseResult",retVal);
										component.set("v.productInfo",retVal.lstPriceItem);
										
                                        if(retVal.isError){
                                            helper.displayToast('Error', retVal.errorMessage,'error');
										}
										if(retVal.totalInAED > 0)
											helper.updateReceiptRecord(component,event, $A.get("$Label.c.OB_Receipt_Status"));
										else	
											component.set("v.showSpinner", false);
									}
									else if (state === "ERROR") {
										// Process error returned by server
										let errors = response.getError();
										let message = 'Unknown error'; // Default error message
										// Retrieve the error message sent by the server
										if (errors && Array.isArray(errors) && errors.length > 0) {
											message = errors[0].message;
										}
										
									}
								});
								$A.enqueueAction(action); 
							}
							else{
								//Updating the PriceItem to blocked and SR Status to submitted for Draft SR's for all other category
								helper.updateSRPriceItems(component, event, helper, component.get("v.srId"));
							}
						}
						
					}
					else{
						helper.updateReceiptRecord(component,event, 'Failure');
						component.set("v.ErrorResp",respPay.status);
						component.set("v.isOpen", true);
						component.set("v.showSpinner", false);
					}
				}
				else{
					component.set("v.ErrorResp","The payment session is expired.");
					component.set("v.isOpen", true);
					component.set("v.showSpinner", false);
				}
			}
			else{
				component.set("v.ErrorResp","Service down");
				component.set("v.isOpen", true);
				component.set("v.showSpinner", false);
			}
		};   
		xmlHttp.send(null);
		function getUrlParameter(name) {
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);
			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
		};
	},
	
	closeModel: function(component, event, helper) { 
        component.set("v.isOpen", false);
    }
})