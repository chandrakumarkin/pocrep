({

    processRenewalSR: function(component, event, helper) {
      
        var action = component.get("c.processNameRenewalSR");
        let newURL = new URL(window.location.href).searchParams;
        var srId = ( newURL.get("srId") ? newURL.get("srId") : component.get("v.srId") );
        console.log('@@@@@@@@@ srId ',srId);
        var requestWrap = {
            "srId" : srId        
          };
         action.setParams({"requestWrapParam": JSON.stringify(requestWrap)});
         action.setCallback(this, function(response) 
         {
      
         var state = response.getState();
             if (state === "SUCCESS") {
                 var respWrap = response.getReturnValue();
                 console.log('@@@@@@@ in processRenewalSR respWrap ',respWrap);
                 
             }else{
                 console.log('@@@@@ Error '+response.getError()[0].message);
                 console.log('@@@@@ Error Location '+response.getError()[0].stackTrace);
             }
         });
         $A.enqueueAction(action);
      },

     updateSRPriceItems: function(component, event, helper, srId) {
        var action = component.get("c.updateSRLines");
        action.setParams({
            "srId": srId
        });
        action.setCallback(this, function(response) {
            var state= response.getState();
            if (state == "SUCCESS") {
                var resp = response.getReturnValue();
                component.set("v.responseResult",resp);
				component.set("v.productInfo",resp.lstPriceItem);
                //component.set("v.productInfo",resp);
                console.log(resp);
                var lstPriceItem = resp.lstPriceItem;
                var i;
                var negativePriceItemConverter = -1;
                var negativePaymentMsg;
                var positiveAmt;
                for(i=0 ; i<lstPriceItem.length ; i++){
                    if(lstPriceItem[i].HexaBPM__Pricing_Line__r.Non_Deductible__c){ 
                        if(lstPriceItem[i].Total_Price_USD__c && lstPriceItem[i].Total_Price_USD__c < 0){
                            positiveAmt = negativePriceItemConverter * lstPriceItem[i].Total_Price_USD__c
                            negativePaymentMsg = 'The payment of USD '+positiveAmt+' for '+lstPriceItem[i].Pricing_Line_Name__c+' is credited to your account upon submission of the application';
                            component.set("v.negativeBalanceMessage",negativePaymentMsg);
                            break;
                        }
                    }
                }
                if(resp.isError){
                    this.displayToast('Error', resp.errorMessage,'error');
                }
                if(resp.totalInAED > 0)
                    helper.updateReceiptRecord(component,event, $A.get("$Label.c.OB_Receipt_Status"));
                else	
					component.set("v.showSpinner", false);
            }
            else if (state === "ERROR") {
                // Process error returned by server
                let errors = response.getError();
                let message = 'Unknown error'; // Default error message
                // Retrieve the error message sent by the server
                if (errors && Array.isArray(errors) && errors.length > 0) {
                    message = errors[0].message;
                }
                // Display the message
                console.error(message);
            }
        });
        $A.enqueueAction(action);
    },
    /**
     * Display a message
     */
    displayToast : function (title, message, type) {
		console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'type': type,
                'title': title,
                'message': message,
                mode:'dismissible',
                duration:500
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
	},
    updateReceiptRecord : function(component, event, receiptStatusValue){
        console.log('########## receiptStatusValue '+receiptStatusValue);
    	console.log('=updateReceiptRecord ======'+component.get('v.amountPaid'));
        console.log(component.get('v.checkoutPaymentID'));
        try{
        var newURL = new URL(window.location.href).searchParams;
        var amountPaid = ( component.get('v.amountPaid') ? component.get('v.amountPaid') : newURL.get('amountPaid')); 
        var srId = component.get("v.srId");
        console.log(srId); 
        console.log(amountPaid);
        component.set('v.amountPaid',amountPaid)
        var reqWrapPram  =
        {
            amount: amountPaid, 
            checkoutPaymentID: component.get('v.checkoutPaymentID'),
            receiptStatus: receiptStatusValue,
            cardType:component.get('v.cardType'),
            bankReferenceNumber: component.get('v.bankReferenceNumber'),
            receiptId:component.get("v.receiptId")
        }
        var action = component.get("c.updateReceipt");
    	action.setParams({
        "reqWrapPram": JSON.stringify(reqWrapPram)});
    	action.setCallback(this, function(response) {
            console.log(response.getReturnValue());
            var state = response.getState();
            if (state === "SUCCESS") {
				var receipt = response.getReturnValue();
                console.log(receipt); 
                component.set("v.accountName",receipt.accountName);     
                component.set("v.receiptRef",receipt.receiptRef);       
                console.log('sucess create receipt');
                component.set("v.showSpinner", false);
            }else if (state === "INCOMPLETE") {
                console.log('Response is Incompleted');
                component.set("v.showSpinner", false);
            }else if (state === "ERROR") {
                component.set("v.showSpinner", false);
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                    errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(action);
        }catch(err){
            console.log(error);
            component.set("v.showSpinner", false);
        }
        
    },
    showSpinner: function (component) {
        console.log('showSpinner');
        var spinner = component.find("myspinner");
        
        console.log(spinner);
        $A.util.removeClass(spinner, "slds-hide");
    },
     
    hideSpinner: function (component) {
        console.log('hideSpinner');
        var spinner = component.find("myspinner");
        $A.util.addClass(spinner, "slds-hide");

    }
})