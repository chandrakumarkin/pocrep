({
    getSrFields: function(component, event, helper) {
        try {
            let newURL = new URL(window.location.href).searchParams;
            
            var pageId = component.get("v.pageId")
            ? component.get("v.pageId")
            : newURL.get("pageId");
            
            var pageflowId = component.get("v.flowId")
            ? component.get("v.flowId")
            : newURL.get("flowId");
            var srId = component.get("v.srId")
            ? component.get("v.srId")
            : newURL.get("srId");
            console.log(srId);
            
            component.set("v.flowId", pageflowId);
            component.set("v.pageId", pageId);
            component.set("v.srId", srId);
            
            var action = component.get("c.fetchSRObjFields");
            var requestWrap = {
                flowID: pageflowId,
                srId: srId,
                pageId: pageId
            };
            action.setParams({
                requestWrapParam: JSON.stringify(requestWrap)
            });
            
            
            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log('----response---------'+response.getReturnValue());
                    const repWrap = response.getReturnValue();
                    component.set("v.respWrapp", repWrap);
                    
                    component.set("v.isBackEndUser", repWrap.isBackendUser);
                    component.set("v.srObj", repWrap.srObj);
                    console.log("#######--repWrap.amountToBePaid-- ",
                                JSON.stringify(repWrap.amountToBePaid));
                    component.set("v.amountToBePaid", repWrap.amountToBePaid);
                    console.log(
                        "#######--repWrap.srObj-- ",
                        JSON.stringify(repWrap.srObj)
                    );
                    component.set("v.commandButton", repWrap.ButtonSection);
                    component.set("v.showDecAndButton", repWrap.ShowDecAndButton);
                    component.set("v.showButton", repWrap.showButton);
                    component.set("v.spinner", false);
                    document
                    .getElementById("difc-review-form")
                    .classList.remove("difc-display-none");
                } else {
                    alert("@@@@@ Error " + response.getError()[0].message);
                    alert("@@@@@ Error " + response.getError()[0].stackTrace);
                }
            });
            $A.enqueueAction(action);
            //component.set("v.spinner", false);
        } catch (error) {
            console.log(error.message);
            console.log(error);
        }
    },
    showToast: function(component, event, helper, msg, type) {
        // Use \n for line breake in string
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            mode: "sticky",
            message: msg,
            type: type
        });
        toastEvent.fire();
    },
})