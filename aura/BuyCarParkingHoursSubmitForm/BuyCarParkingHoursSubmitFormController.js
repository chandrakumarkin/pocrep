({
    init: function (component, event, helper) {
        
        let newURL = new URL(window.location.href).searchParams;
        var pageId = newURL.get("pageId");
        helper.getSrFields(component, event, helper);
    },
    handleAutoClose: function (cmp, event, helper) {
        try {
            let newURL = new URL(window.location.href).searchParams;
            var srID = newURL.get("srId");
            var pageID = newURL.get("pageId");
            var buttonId = event.target.id;
            console.log(buttonId);
            var action = cmp.get("c.getButtonAction");
            
            action.setParams({
                SRID: srID,
                pageId: pageID,
                ButtonId: buttonId
            });
            
            action.setCallback(this, function (response){
                var state = response.getState();
                if (state === "SUCCESS") {
                    console.log("button succsess"); 
                    console.log(response.getReturnValue());
                    window.open(response.getReturnValue().pageActionName, "_self");
                } else {
                    console.log("@@@@@ Error " + response.getError()[0].message);
                    helper.createToast(cmp, event, response.getError()[0].message);
                }
            });
            $A.enqueueAction(action);
        } catch (err) {
            console.log(err.message);
        }
    },
    handleButtonAct: function (component, event, helper) {
        
        component.set("v.spinner", true);
        
        try {
            console.log('entered in try');
            var srObj = component.get("v.srObj");
            let newURL = new URL(window.location.href).searchParams;
            var srID = newURL.get("srId");
            var pageflowId = newURL.get("flowId");
            var pageId = newURL.get("pageId");
            var buttonId = event.target.id;
            console.log(buttonId);
            var action = component.get("c.getButtonAction1");
            action.setParams({
                srObj: srObj,
                SRID: srID,
                pageId: pageId,
                flowId: pageflowId
            });
            action.setCallback(this, function (response) {
                var state = response.getState();
                console.log("state " + state);
                
                if (state === "SUCCESS") {
                    console.log(response.getReturnValue());
                    var responseObj = response.getReturnValue();
                    console.log('responseObj.pageActionName: '+JSON.stringify(responseObj));
                    debugger;
                    if (responseObj.haserror) {
                        helper.showToast(
                            component,
                            event,
                            helper,
                            responseObj.errormessage,
                            "error"
                        );
                    } else {
                        component.set("v.spinner", false);
                        window.open(responseObj.pageActionName, "_self");
                    }
                } else {
                    component.set("v.spinner", false);
                    console.log("@@@@@ Error " + response.getError()[0].message);
                }
            });
            $A.enqueueAction(action);
            
        }
        catch (err) {
            console.log(err.message);
            component.set("v.spinner", false);
        }   
    }
})