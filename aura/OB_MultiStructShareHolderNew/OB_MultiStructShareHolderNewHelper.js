({
	initamendments : function(cmp, event)
    {
       
        
       //cmp.set('v.amedWrap',"{ 'amedObj'  : {'sObjectType': 'HexaBPM_Amendment__c'}}"); 
       //{!v.srWrap.srObj.Id}
        
        var initAmendment = cmp.get('c.initAmendmentDB');
         
         let newURL = new URL(window.location.href).searchParams;
             //var pageId = newURL.get('pageId');
			var srIdParam =  (  cmp.get('v.srId') ?  cmp.get('v.srId') : newURL.get('srId'));
       
        var reqWrapPram  =
            {
                srId : srIdParam
                
            };
        
        initAmendment.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram),
                 
            });
        
        
        initAmendment.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                              //console.log('######### respWrap.amedWrap  ',respWrap.amedWrap);
                                              console.log('######### respWrap  ',respWrap);
											  cmp.set('v.amedWrap',respWrap.amedWrap);
                                          		
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                              this.createToast(cmp,event,response.getError()[0].message);
                                              
                                          }
                                      }
                                     );
            $A.enqueueAction(initAmendment);
       
    },
      createToast: function(component,event, errorMessagr){
         var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        mode: "dismissable",
        message: errorMessagr ,
        type: "error",
        duration: 500
      });
      toastEvent.fire();
    }
})