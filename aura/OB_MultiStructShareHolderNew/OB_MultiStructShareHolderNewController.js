({
     handleRecordType  : function(cmp, event, helper){
        var index = event.target.dataset.amedtype;
        console.log('###############@@@@@@@@@@'+ index);
        cmp.set("v.recordtypename", index);
        console.log(cmp.get("v.recordtypename"))
        if(index == 'Individual') {
            cmp.set("v.isIndividual", true);
            cmp.set("v.isCorporate", false);
            cmp.set("v.isSelectedIndividual",true);
           cmp.set("v.isSelectedCorporate",false);
        } else if(index == 'Body_Corporate')
        {
            cmp.set("v.isCorporate", true);
            cmp.set("v.isIndividual", false);
            cmp.set("v.isSelectedIndividual",false);
            cmp.set("v.isSelectedCorporate",true);
        }
            else{
                cmp.set("v.isSelectedCorporate",false);
                cmp.set("v.isSelectedIndividual",false);
            }
         var initAmendment = cmp.get('c.initAmendmentDB');
        var reqWrapPram  =
            {
                srId : cmp.get('v.srWrap').srObj.Id,
                recordtypename: cmp.get("v.recordtypename")
            };
        initAmendment.setParams(
            {
                 "reqWrapPram": JSON.stringify(reqWrapPram),
                 
            });
        
        initAmendment.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                              console.log('######### respWrap.amedWrap  ',respWrap.amedWrap);
											  cmp.set('v.amedWrap',respWrap.amedWrap);	                                              
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                              helper.createToast(cmp,event,response.getError()[0].message);
                                             
                                          }
                                      }
                                     );
            $A.enqueueAction(initAmendment);
            
        console.log(cmp.get("v.recordtypename"))
        
    }
    
})