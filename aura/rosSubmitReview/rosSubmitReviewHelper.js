({
    getSrFields: function(component, event, helper) {
      try {
        let newURL = new URL(window.location.href).searchParams;
     
        var pageId = component.get("v.pageId")
          ? component.get("v.pageId")
          : newURL.get("pageId");
  
        var pageflowId = component.get("v.flowId")
          ? component.get("v.flowId")
          : newURL.get("flowId");
        var srId = component.get("v.srId")
          ? component.get("v.srId")
          : newURL.get("srId");
        console.log(srId);
  
        component.set("v.flowId", pageflowId);
        component.set("v.pageId", pageId);
        component.set("v.srId", srId);
  
        var action = component.get("c.fetchSRObjFields");
        var requestWrap = {
          flowID: pageflowId,
          srId: srId,
          pageId: pageId
        };
        action.setParams({
          requestWrapParam: JSON.stringify(requestWrap)
        });


        action.setCallback(this, function(response) {
          var state = response.getState();
          if (state === "SUCCESS") {
            console.log('----response---------'+response.getReturnValue());
            const repWrap = response.getReturnValue();
            component.set("v.respWrapp", repWrap);
            component.set("v.secWrapp", repWrap.secDetailWrap);
            
            component.set("v.isBackEndUser", repWrap.isBackendUser);
            component.set("v.docWrapp", repWrap.docWrap);
            component.set("v.srObj", repWrap.srObj);
            component.set("v.debatIndList", repWrap.debatIndList);
            component.set("v.securityFormList", repWrap.securityFormList);
            component.set("v.colDetailList", repWrap.colDetailList);
              
              console.log(
              "#######--repWrap.srObj-- ",
              JSON.stringify(repWrap.srObj)
              );
              console.log(
                "#######--repWrap.approvedForm-- ",
                JSON.stringify(repWrap.approvedForm)
                );
                
                console.log(
                    "#######--repWra------debat list-------- ",
                    JSON.stringify(repWrap.debatIndList)
                    );

            component.set("v.approvedForm", repWrap.approvedForm); 
         
              
            component.set("v.commandButton", repWrap.ButtonSection);
            component.set("v.showDecAndButton", repWrap.ShowDecAndButton);
            component.set("v.showButton", repWrap.showButton);
            
            component.set("v.annualAssessMent", repWrap.amList);
            component.set("v.annualAssessMentRisk", repWrap.amMeasureList);
            component.set("v.amRec", repWrap.amRec);
              
            component.set("v.licenseActWrapp", repWrap.licenseActs);
            document
              .getElementById("difc-review-form")
              .classList.remove("difc-display-none");
            //component.set("v.pgWrapp",response.getReturnValue().pageNameList);
          } else {
            alert("@@@@@ Error " + response.getError()[0].message);
            alert("@@@@@ Error " + response.getError()[0].stackTrace);
          }
        });
        $A.enqueueAction(action);
      } catch (error) {
        console.log(error.message);
        console.log(error);
      }
    },
  
    getButtonDetails: function(component, event, helper, pgId) {
      var action = component.get("c.retrieveCompanyWrapper");
      action.setParams({
        pageId: pgId
      });
  
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var responseResult = response.getReturnValue();
          component.set("v.commandButton", responseResult.ButtonSection);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    },
   
    showToast: function(component, event, helper, msg, type) {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        mode: "sticky",
        message: msg,
        type: type
      });
      toastEvent.fire();
    },
    
    navToDetailPage: function(component, event, helper, fieldsToValidate) {
      var state = res.getState();
      if (state === "SUCCESS") {
        var oppId = res.getReturnValue();
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
          recordId: oppId
        });
        navEvt.fire();
      }
    }
  });