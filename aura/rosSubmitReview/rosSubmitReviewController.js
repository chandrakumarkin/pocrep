({
    init: function (component, event, helper) {
        component.set("v.openingDiv", "<div>");
        component.set("v.closingDiv", "</div>");

        let newURL = new URL(window.location.href).searchParams;
        var pageId = newURL.get("pageId");
        helper.getSrFields(component, event, helper);
    },    

    onChange: function (component, event, helper) {
        console.log(event.getSource().get("v.value"));
    },

    getSelected: function (component, event, helper) {

        var contentId = event.currentTarget.getAttribute("data-Id");
        console.log(contentId);
        console.log($A.get("e.lightning:openFiles"));
        var openFileEvent = $A.get("e.lightning:openFiles");
        if (openFileEvent) {
          $A.get("e.lightning:openFiles").fire({
            recordIds: [contentId]
          });
        } else {
            var contentRecordPageLink =
            "/lightning/r/ContentDocument/" + contentId + "/view";
            window.open(contentRecordPageLink, "_blank");

            /* window.location.href =
            "/lightning/r/ContentDocument/" + contentId + "/view"; */
        }
    },
    closeModal: function (component, event, helper) {
        // for Close Model, set the "hasModalOpen" attribute to "FALSE"
        component.set("v.hasModalOpen", false);
        component.set("v.selectedDocumentId", null);
    },
    
handleAutoClose: function (cmp, event, helper) {
    try {
        let newURL = new URL(window.location.href).searchParams;
        var srID = newURL.get("srId");
        var pageID = newURL.get("pageId");
        var buttonId = event.target.id;
        console.log(buttonId);
        var action = cmp.get("c.getButtonAction");

        action.setParams({
            SRID: srID,
            pageId: pageID,
            ButtonId: buttonId
        });

        action.setCallback(this, function (response){
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log("button succsess"); 
                console.log(response.getReturnValue());
                window.open(response.getReturnValue().pageActionName, "_self");
            } else {
                console.log("@@@@@ Error " + response.getError()[0].message);
                helper.createToast(cmp, event, response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
},

handleButtonAct: function (cmp, event, helper) {
    
    console.log('handleButtonAct');
    cmp.set("v.spinner", true);
    
    try {
        
    
        //let fields = cmp.find("dataProtectionkey");
        var allValid = true;
        /*for (var i = 0; i < fields.length; i++) {
            var inputFieldCompValue = fields[i].get("v.value");
            if(!inputFieldCompValue && fields[i] && fields[i].get("v.required")){
                fields[i].reportValidity();
                allValid = false;
            }   
        }
        console.log('======allValid============='+allValid);
        if(
            !allValid
        ){
            console.log("Error MSG");
            cmp.set("v.spinner", false);
            helper.showToast(cmp,event,helper,'Please fill required fields','error'); 
        }  */   
        var requiredDocsUploaded = cmp.get('v.srObj.HexaBPM__Required_Docs_not_Uploaded__c');
     
        if(requiredDocsUploaded){
            helper.showToast(cmp,event,helper,'Please Upload Mandatory documents','error'); 
            allValid = false;
        } 
        if(allValid){
        
            var srObj = cmp.get("v.srObj");
            let newURL = new URL(window.location.href).searchParams;
            var srID = newURL.get("srId");
            var pageflowId = newURL.get("flowId");
            var pageId = newURL.get("pageId");
            var buttonId = event.target.id;
            //alert(buttonId);
            //cmp.set("v.spinner", true);
            var action = cmp.get('c.onAmedSaveDB2');
          
            action.setParams(
            {
                srObj: srObj,
                srId :srID,
                pageId: pageId,
                flowId: pageflowId
            });
     
            action.setCallback(this, function (response){
                var state = response.getState();
                console.log("state " + state);
				debugger;
                if (state === "SUCCESS") {
                    var responseObj = response.getReturnValue();
                    if (responseObj.haserror) {
                        helper.showToast(
                          cmp,
                          event,
                          helper,
                          responseObj.errormessage,
                          "error"
                        );
                    } else {
                        //debugger;
                        window.open(responseObj.pageActionName, "_self");
                        //window.open('/clientportal/s', "_self");
                        cmp.set("v.spinner", false);
                    }
                } else {
                  console.log("@@@@@ Error " + response.getError()[0].message);
                    cmp.set("v.spinner", false);
                }
            });
            $A.enqueueAction(action);
        } 
    }catch (err) {
        console.log(err.message);
        cmp.set("v.spinner", false);
    }   
},

openPrint: function (component, event, helper) {
    window.print();
}
});