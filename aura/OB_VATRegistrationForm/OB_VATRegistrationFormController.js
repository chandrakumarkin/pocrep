({
  getContactDetails: function(component, event) {
    var action = component.get("c.getConDetails");
    /* var requestWrap = {
      srId: srId,
      flowId: flowId,
      pageId: pageId
    };
    action.setParams({
      requestWrapParam: JSON.stringify(requestWrap)
    }); */

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        debugger;
        console.log(response.getReturnValue());
        var respWrap = response.getReturnValue();
        if (
          respWrap.loggedInUser.Contact.Account.Tax_Registration_Number_TRN__c
        ) {
          var urlEvent = $A.get("e.force:navigateToURL");
          urlEvent.setParams({
            url: "/"
          });
          urlEvent.fire();
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },
  onUpdateAccount: function(component, event, helper) {
    //alert('==entered');
    try {
      debugger;
      if (component.get("v.VATNumber") != null) {
        component.find("Id_spinner").set("v.class", "slds-show");

        var action = component.get("c.updateAccountVatRegistrationNumber");

        action.setParams({
          regestrationNumber: component.get("v.VATNumber")
        });

        action.setCallback(this, function(response) {
          //alert('==inside--');
          // hide spinner when response coming from server
          component.find("Id_spinner").set("v.class", "slds-hide");
          var state = response.getState();
          if (state === "SUCCESS") {
            console.log(response.getReturnValue());

            var urlEvent = $A.get("e.force:navigateToURL");
            urlEvent.setParams({
              url: "/"
            });
            urlEvent.fire();
          } else {
            console.log("@@@@@ Error " + response.getError()[0].message);
            console.log("@@@@@ Error " + response.getError()[0].stackTrace);
          }
        });
        $A.enqueueAction(action);
      } else {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          mode: "dismissable",
          message: "Please enter your Tax Registration Number",
          type: "error",
          duration: 500
        });
        toastEvent.fire();
      }
    } catch (error) {
      console.log(error.message);
    }
  }
});