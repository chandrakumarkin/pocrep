({
    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB
    CHUNK_SIZE: 7500000, //Chunk Max size 750Kb
  
    resetAuthIndivalues: function(cmp) {
      cmp.set("v.amedWrap.amedObj.DI_First_Name__c", "");
      cmp.set("v.amedWrap.amedObj.DI_Last_Name__c", "");
      cmp.set("v.amedWrap.amedObj.DI_Email__c", "");
      cmp.set("v.amedWrap.amedObj.DI_Mobile__c", "");
    },
  
    resetAuthCorvalues: function(cmp) {
      cmp.set("v.amedWrap.amedObj.DI_First_Name__c", "");
      cmp.set("v.amedWrap.amedObj.DI_Last_Name__c", "");
      cmp.set("v.amedWrap.amedObj.DI_Email__c", "");
      cmp.set("v.amedWrap.amedObj.DI_Mobile__c", "");
    },
  
    displayEntityField: function(cmp) {
      var entityfield = cmp.get(
        "v.amedWrap.amedObj.Is_this_Entity_registered_with_DIFC__c"
      );
  
      if (entityfield == "Yes") {
        cmp.set("v.showEntityField", true);
        cmp.set("v.RegWithDIFCno", false);
      } else if (entityfield == "No") {
        cmp.set("v.RegWithDIFCno", true);
        cmp.set("v.showEntityField", false);
      } else {
        cmp.set("v.showEntityField", false);
        cmp.set("v.RegWithDIFCno", false);
      }
    },
    displayPartnerType: function(component) {
      var typeofEntity = component.get("v.srWrap.srObj.Type_of_Entity__c");
      if (
        typeofEntity == "Recognized Limited Partnership (RLP)" ||
        typeofEntity == "Limited Partnership (LP)"
      ) {
        component.set("v.ShowPartnertype", true);
      } else if (
        typeofEntity == "General Partnership (GP)" ||
        typeofEntity == "Recognized Partnership (RP)"
      ) {
        component.set("v.amedWrap.amedObj.Partner_Type__c", "General Partner");
        console.log(component.get("v.amedWrap.amedObj.Partner_Type__c"));
      }
    },
  
    displayContriFields: function(component) {
      var typeofEntity = component.get("v.srWrap.srObj.Type_of_Entity__c");
      if (
        typeofEntity == "Limited Partnership (LP)" ||
        typeofEntity == "General Partnership (GP)" ||
        typeofEntity == "Recognized Limited Partnership (RLP)" ||
        typeofEntity == "Recognized Partnership (RP)"
      ) {
        component.set("v.ShowContributionFields", true);
      }
    },
  
   
    showFieldOnRiskScore: function(cmp, event, type, nationality) {
      console.log("showFieldOnRiskScore");
      console.log(nationality);
      var action = cmp.get("c.getCountryRiskScoring");
      action.setParams({
        nationality: nationality
      });
      action.setCallback(this, function(response) {
        //store state of response
        var state = response.getState();
        if (state === "SUCCESS") {
          var responseResult = response.getReturnValue();
          if (responseResult) {
            var score = responseResult.toLowerCase();
            console.log(score);
            if (score == "high" || score == "very high") {
              console.log("very high>>");
              if (type == "individual")
                cmp.set("v.showIndividualFieldOnRiskScore", "true");
              if (type == "corporate")
                cmp.set("v.showCorporateFieldOnRiskScore", "true");
            } else {
              if (type == "individual")
                cmp.set("v.showIndividualFieldOnRiskScore", "false");
              if (type == "corporate")
                cmp.set("v.showCorporateFieldOnRiskScore", "false");
            }
          }
        } else if (state === "ERROR") {
          // Handle any error by reporting it
          var errors = response.getError();
  
          if (errors) {
            if (errors[0] && errors[0].message) {
              this.createToast(cmp, event, errors[0].message);
            }
          } else {
            this.createToast(cmp, event, "Unknown error.");
          }
        }
      });
      $A.enqueueAction(action);
    },
    associateFile: function(
      component,
      event,
      helper,
      parentId,
      docMasterCode,
      filesArr,
      srDocId
    ) {
      console.log("associateFile");
      var action = component.get("c.saveChunkBlob2");
      var files = filesArr; //component.get("v.fileToBeUploaded");
      var file;
      var self = this;
      if (files && files.length > 0) {
        file = files[0][0];
        console.log(file);
        console.log(file.size);
        if (file.size > self.MAX_FILE_SIZE) {
          component.set(
            "v.fileName",
            "Alert : File size cannot exceed " +
              self.MAX_FILE_SIZE +
              " bytes.\n" +
              " Selected file size: " +
              file.size
          );
          return;
        }
        var objFileReader = new FileReader();
        objFileReader.onloadend = function() {
          var fileContents = objFileReader.result;
          var base64 = "base64,";
          var dataStart = fileContents.indexOf(base64) + base64.length;
          console.log("!!!!!!!!!!!!!dataStart" + dataStart);
          fileContents = fileContents.substring(dataStart);
          console.log("!!!!!!!!!!!!!fileContents" + fileContents);
          console.log("!!!!!!!!!!!!!base64" + base64);
          console.log("===method 1===");
          console.log("!!!!!!!!!!!!!action" + action);
          // call the uploadProcess method
          self.uploadProcess(
            action,
            component,
            file,
            fileContents,
            parentId,
            docMasterCode,
            srDocId
          );
        };
        objFileReader.readAsDataURL(file);
      } else {
      }
    },
  
    uploadProcess: function(
      action,
      component,
      file,
      fileContents,
      parentId,
      docMasterCode,
      srDocId
    ) {
      // set a default size or startpostiton as 0
      var startPosition = 0;
      // calculate the end size or endPostion using Math.min() function which is return the min. value
      var endPosition = Math.min(
        fileContents.length,
        startPosition + this.CHUNK_SIZE
      );
      console.log("==uploadProcess===");
      console.log(action);
      // start with the initial chunk, and set the attachId(last parameter)is null in begin
      this.uploadInChunk(
        action,
        component,
        file,
        fileContents,
        startPosition,
        endPosition,
        "",
        parentId,
        docMasterCode,
        srDocId
      );
    },
    uploadInChunk: function(
      action,
      component,
      file,
      fileContents,
      startPosition,
      endPosition,
      attachId,
      parentId,
      docMasterCode,
      srDocId
    ) {
      try {
        console.log("==uploadInChunk===");
        console.log(action);
        console.log("component--" + JSON.stringify(component));
        // call the apex method 'saveChunk'
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get("srId");
        console.log("@@@@@@@@@@@srId" + srId);
        console.log("@@@@@@@@@@@fileContents" + fileContents);
        var getchunk = fileContents.substring(startPosition, endPosition);
        console.log("@@@@@@@@@@@startPosition" + startPosition);
        console.log("@@@@@@@@@@@endPosition" + endPosition);
        console.log("@@@@@@@@@@@getchunk" + getchunk);
        console.log("@@@@@@@@@@@encodeURIComponent" + encodeURIComponent);
        var self = this;
        var srDocId = srDocId ? srDocId : " ";
        console.log("srDocId>>>" + srDocId);
        console.log("parentId-----" + parentId);
        console.log("documentMasterCode--" + docMasterCode);
        console.log("file.name--" + file.name);
        console.log("file.type--" + file.type);
        console.log("action--" + action);
        console.log(srId);
  
        //console.log(encodeURIComponent(getchunk));
        action.setParams({
          parentId: parentId,
          srId: srId,
          fileName: file.name,
          base64Data: encodeURIComponent(getchunk),
          contentType: file.type,
          fileId: attachId,
          documentMasterCode: docMasterCode,
          strDocumentId: srDocId
        });
        console.log("am here");
        // set call back
        action.setCallback(this, function(response) {
          // store the response / Attachment Id
          attachId = response.getReturnValue();
          console.log("attachId>>" + attachId);
          var state = response.getState();
          if (state === "SUCCESS") {
            // update the start position with end postion
            console.log("inside----");
            startPosition = endPosition;
            endPosition = Math.min(
              fileContents.length,
              startPosition + this.CHUNK_SIZE
            );
            // check if the start postion is still less then end postion
            // then call again 'uploadInChunk' method ,
            // else, diaply alert msg and hide the loading spinner
            if (startPosition < endPosition) {
              this.uploadInChunk(
                action,
                component,
                file,
                fileContents,
                startPosition,
                endPosition,
                attachId,
                parentId,
                docMasterCode,
                srDocId
              );
            } else {
              console.log("your File is uploaded successfully>>" + attachId);
              //component.set("v.showLoadingSpinner", false);
            }
            // handel the response errors
          } else if (state === "INCOMPLETE") {
            alert("From server: " + response.getReturnValue());
          } else if (state === "ERROR") {
            var errors = response.getError();
            if (errors) {
              if (errors[0] && errors[0].message) {
                //self.createToast("Error", errors[0].message);
  
                console.log("Error message: " + errors[0].message);
              }
            } else {
              console.log("Unknown error");
            }
          }
        });
        // enqueue the action
        $A.enqueueAction(action);
      } catch (err) {
        console.log("@@@@@ Error " + err.message);
      }
    },
  
    loadSRDocs: function(component, event, helper) {
     
      var amendmentWrapper = component.get("v.amedWrap");
      console.log("amendmentWrapper----" + amendmentWrapper);
      var amedId = amendmentWrapper.amedObj.Id;
      //var srId = component.get("v.srId");
      var srId = component.get("v.srWrap").srObj.Id;
      console.log(" loadSRDocs srId-----", srId);
      var action = component.get("c.viewSRDocs");
      var self = this;
      action.setParams({
        srId: srId,
        amendmentId: amedId
      });
  
      // set call back
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var result = response.getReturnValue();
          console.log("srDocs>>");
          console.log(result.Passport_Copy_Individual);
          console.log(result.Passport_Copy_Individual.FileName);
          component.set("v.srDocs", result);
          if (result.Passport_Copy_Individual) {
            component.set("v.fileName", result.Passport_Copy_Individual.FileName);
            component.set("v.srDocId", result.Passport_Copy_Individual.Id);
          }
        } else if (state === "INCOMPLETE") {
          alert("From server: " + response.getReturnValue());
        } else if (state === "ERROR") {
          var errors = response.getError();
          if (errors) {
            if (errors[0] && errors[0].message) {
              helper.createToast(component, event, errors[0].message);
              console.log("Error message: " + errors[0].message);
            }
          } else {
            console.log("Unknown error");
          }
        }
      });
      // enqueue the action
      $A.enqueueAction(action);
    },
  
    show: function(cmp, event) {
      var spinner = cmp.find("mySpinner");
      $A.util.removeClass(spinner, "slds-hide");
      $A.util.addClass(spinner, "slds-show");
    },
    hide: function(cmp, event) {
      var spinner = cmp.find("mySpinner");
      $A.util.removeClass(spinner, "slds-show");
      $A.util.addClass(spinner, "slds-hide");
    },
   
    createToast: function(component, event, errorMessagr) {
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        mode: "dismissable",
        message: errorMessagr,
        type: "error",
        duration: 500
      });
      toastEvent.fire();
    },
    fileUploadHelper: function(cmp, event, helper) {
      //New method.
      console.log("########### fileHolder Start");
  
      // For file upload and validation.
      var fileUploadValid = true;
      var fileHolder = cmp.find("filekey");
      console.log('@@@@@@@@@@@@@@ fileHolder '+fileHolder );
      // fileHolder && 
      if (Array.isArray(fileHolder)) 
      {
          console.log( "########### multiple fileUploadValid ",fileUploadValid);
          for (var i = 0; i < fileHolder.length; i++) 
          {
            
            //fileUploadValid = fileUploadValid && fileHolder[i].get("v.fileUploaded"); 
            if (fileHolder[i].get("v.requiredDocument") == 'true' && !fileHolder[i].get("v.fileUploaded") ) 
            {
                fileHolder[i].set("v.requiredText", "Please upload document .");
                fileUploadValid = false;
                break;
            }
            else
            {
                helper.fileUploadProcess(cmp, event, helper, fileHolder[i]);
            }
  
  
          }
          console.log( "########### multiple fileUploadValid end",fileUploadValid);
      } 
      else if (fileHolder) 
      {
          console.log(
            "########### fileHolder content docID ",
            fileHolder.get("v.contentDocumentId")
          );
          //here
          helper.fileUploadProcess(cmp, event, helper, fileHolder);
          console.log( "########### fileHolder requiredDocument ",fileHolder.get("v.requiredDocument") );
          console.log( "########### fileUploadValid ",fileUploadValid);
        
  
         if (fileHolder.get("v.requiredDocument") == 'true' && !fileHolder.get("v.fileUploaded") ) 
         {
             fileHolder.set("v.requiredText", "Please upload document.");
             fileUploadValid = false;
             
         }
         else
         {
             helper.fileUploadProcess(cmp, event, helper, fileHolder);
         }
  
  
      }
  
      console.log(" Final file upload ", fileUploadValid);
      if (!fileUploadValid) 
      {
        helper.showToast(
          cmp,
          event,
          helper,
          "Please upload required files",
          "error"
        );
        cmp.set("v.spinner", false);
        return "file error";
      }
      console.log( "########### fileHolder end",JSON.stringify(cmp.get("v.docMasterContentDocMap") ) );
    },
  
    fileUploadProcess: function(cmp, event, helper, fileHolder) 
    {
      //New Method
      var contentDocumentId = fileHolder.get("v.contentDocumentId");
      var documentMaster = fileHolder.get("v.documentMaster");
  
      console.log('************** fileUploadProcess ');
      console.log('************** final contentDocumentId '+contentDocumentId);
      console.log('************** final documentMaster '+documentMaster);
  
  
      //if (contentDocumentId && documentMaster) 
      if (/*contentDocumentId &&*/ documentMaster) 
      {
        var mapDocValues = {};
        var docMap = cmp.get("v.docMasterContentDocMap");
        
        for (var key in docMap) 
        {
          mapDocValues[key] = docMap[key];
        }
  
        console.log('Document inside @@@@@');
        mapDocValues[documentMaster] = ( contentDocumentId ? contentDocumentId : 'null');
  
  
        //mapDocValues[documentMaster] = 'test';
        cmp.set("v.docMasterContentDocMap", mapDocValues);
        console.log('inside doc process ',JSON.stringify(cmp.get("v.docMasterContentDocMap")) );
      }
    },
    // function automatic called by aura:waiting event
    showSpinner: function(component, event, helper) {
      // make Spinner attribute true for displaying loading spinner
      component.set("v.spinner", true);
    },
  
    // function automatic called by aura:doneWaiting event
    hideSpinner: function(component, event, helper) {
      // make Spinner attribute to false for hiding loading spinner
      component.set("v.spinner", false);
    },
    showToast: function(component, event, helper, msg, type) {
      console.log("######## ocrWrapper.errorMessage inside ", msg);
  
      // Use \n for line breake in string
      var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        mode: "dismissible",
        message: msg,
        type: type
      });
      toastEvent.fire();
    }
  });