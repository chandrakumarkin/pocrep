({
    doInit : function(component, event, helper){ 
        helper.handleChange(component, event, helper);
    }, 
    
    handleChange : function(component, event, helper){
       // helper.handleChange(component, event, helper);
    },
    
    handleSave : function(component, event, helper){    
         helper.handleSave(component, event, helper);   
    },
    
    handleClose : function(component, event, helper) {
        if((typeof sforce != 'undefined') && sforce && (!!sforce.one)) {
            sforce.one.navigateToURL('/'+stepId);
        }
        else {
            // Set the window's URL using a Visualforce expression
            window.location.href = '/'+component.get("v.stepId");
        }
    }
})