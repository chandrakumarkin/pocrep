({
    handleChange : function (component, event, helper){
        console.log('stepId***'+component.get("v.stepId"));
        var action = component.get("c.getViolations");
        action.setParams({
            stepId : component.get("v.stepId")
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                let res = response.getReturnValue();
                component.set("v.violations", res.violations);
                component.set("v.stepRecord", res.stepRecord);
                component.set("v.isDIFCStep", res.isDIFCStep);
            }
            else if(state === "ERROR"){
                console.log('error');
            }
        });
        $A.enqueueAction(action);
    },
    
    handleSave : function (component, event, helper){
        var spinner = component.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        var seletedType = component.get('v.seletedType');
        var violations = component.get("v.violations");
        let selectedWaiver;
        console.log("violations***"+JSON.stringify(violations));
        var waiverType = component.get("v.isDIFCStep");
        for(var violation in violations){
            console.log("violation***"+violations[violation].Waiver_Type_by_DIFC__c);
            if(waiverType == true)  selectedWaiver = violations[violation].Waiver_Type_by_DIFC__c;
            else selectedWaiver = violations[violation].Waiver_Type_by_FOSP__c;
        }
        console.log("selectedWaiver***"+selectedWaiver);
        // var waiverTypebyDIFC = component.find("waiverTypebyDIFC").get("v.value");
        // console.log("waiverTypebyDIFC***"+waiverTypebyDIFC);
        var action = component.get("c.saveViolations");
        action.setParams({
            stepRecord : component.get("v.stepRecord"),
            seletedType : component.get("v.stepRecord.Violation_Type_By_FOSP__c"),
            violations : violations,
            comments : component.get("v.comments"),
            selectedWaiver : selectedWaiver
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log("state***"+state);
            if (state === "SUCCESS"){
                // self.close();
                window.location.href = '/'+component.get("v.stepId");
            }
            else if(state === "ERROR"){
                var errors = action.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        alert(errors[0].message);
                    }
                }
            }
        });
        $A.enqueueAction(action);
    }
})