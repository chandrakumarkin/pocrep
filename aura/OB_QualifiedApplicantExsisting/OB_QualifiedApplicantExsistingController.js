({
  remove: function(component, event, helper) {
    //remove
    //var amedId = component.get("v.qualifyWraplist").Amendmentid;
    var amedId = event.currentTarget.dataset.amendid;

    let newURL = new URL(window.location.href).searchParams;
    var srId = newURL.get("srId");
    var getFormAction = component.get("c.removeAmendment");

    getFormAction.setParams({
      amendmentID: amedId,
      srId: srId
    });

    getFormAction.setCallback(this, function(response) {
      var state = response.getState();
      console.log("callback state: " + state);
      if (component.isValid() && state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        console.log("Success");
        component.set("v.amendList", respWrap.amendWrapList);
        //component.set("v.amendList", respWrap.AmendmentList);
      } else if (state == "ERROR") {
        console.log("@@@@@ Error " + response.getError()[0].message);
        helper.createToast(component,event,response.getError()[0].message);
      }
    });
    $A.enqueueAction(getFormAction);
  },

  viewDetails: function(component, event, helper) {
    component.set("v.showViewAmend", true);
  }
});