({
  doinit: function (component, event, helper) {
    console.log("=================doInit==============="); //
    //helper.createLicenseonLoad(component,event,helper);

    let newURL = new URL(window.location.href).searchParams;

    var pageflowId = newURL.get("flowId");
    var pageId = newURL.get("pageId");
    var srID = newURL.get("srId");

    var action = component.get("c.getLicenseActivityData");
    var self = this;
    var requestWrap = {
      flowID: pageflowId,
      pageId: pageId,
      srId: srID
    };
    action.setParams({
      requestWrapParam: JSON.stringify(requestWrap)
    });
    action.setCallback(this, function (response) {
      //store state of response
      var state = response.getState();
      if (state === "SUCCESS") {
        var responseResult = response.getReturnValue();
        component.set("v.srObj", responseResult.serviceObj);
       
        console.log(response.getReturnValue());
        component.set(
          "v.searchBarLable",
          response.getReturnValue().SearchBarLable
        );
        component.set("v.LicesenceActivityTable", responseResult.listLiscAct);
        component.set("v.commandButton", responseResult.ButtonSection);
        component.set("v.filename", responseResult.uploadedFileName);
       
        for (var i = 0; i < responseResult.listLiscAct.length; i++) {
          if(responseResult.listLiscAct[i].Activity__r.Name == 'Distribution of Advertising Materials & Samples'){
           
            component.set("v.islicenseActivityName", true);
            break;
          }
          else{
            component.set("v.islicenseActivityName", false);
          }
      }

        component.set(
          "v.licenseApplicationId",
          responseResult.licenseApplicationId
        );
        component.set("v.isSellOrBuy", responseResult.isBuyOrSell);

        helper.showHideFields(component, event);

        //let entityType = responseResult.serviceObj.Entity_Type__c;
        //let businessSector = responseResult.serviceObj.Business_Sector__c;

        /*
                var businessSector = responseResult.serviceObj.Business_Sector__c;
                var entityType = responseResult.serviceObj.Entity_Type__c;
                if(entityType && entityType.toLowerCase() == "financial" && businessSector && 
                    natureOfBusinessNotApplicable.indexOf(businessSector.toLowerCase()) > -1){
                        component.set("v.isNatureOfBusinessRequired",false);
                }
                if(entityType && entityType.toLowerCase() == "financial" && 
                    businessSector 
                    && isParentOrEntityBusinessActivitiesNotApplicable.indexOf(businessSector.toLowerCase()) > -1){
                        component.set("v.isParentOrEntityBusinessActivitiesRequired",false);
                    }
                 */
        //console.log(response.getReturnValue());
      } else if (state === "ERROR") {
        // Handle any error by reporting it
        var errors = response.getError();

        if (errors) {
          if (errors[0] && errors[0].message) {
            helper.displayToast("Error", errors[0].message);
          }
        } else {
          helper.displayToast("Error", "Unknown error.");
        }
      }
    });
    $A.enqueueAction(action);
  },
  onfocus: function (component, event, helper) {
    $A.util.addClass(component.find("mySpinner"), "slds-show");
    var forOpen = component.find("searchRes");
    $A.util.addClass(forOpen, "slds-is-open");
    $A.util.removeClass(forOpen, "slds-is-close");
    var getInputkeyWord = "";
    helper.searchHelper(component, event, helper, getInputkeyWord);
  },
  onblur: function (component, event, helper) {
    component.set("v.listOfSearchRecords", null);
    var forclose = component.find("searchRes");
    $A.util.addClass(forclose, "slds-is-close");
    $A.util.removeClass(forclose, "slds-is-open");
  },
  keyPressController: function (component, event, helper) {
    // get the search Input keyword
    var getInputkeyWord = component.get("v.SearchKeyWord");
    console.log("=======getInputkeyWord=========");
    console.log(getInputkeyWord);
    // check if getInputKeyWord size id more then 0 then open the lookup result List and
    // call the helper
    // else close the lookup result List part.
    if (getInputkeyWord.length > 0) {
      var forOpen = component.find("searchRes");
      $A.util.addClass(forOpen, "slds-is-open");
      $A.util.removeClass(forOpen, "slds-is-close");
      helper.searchHelper(component, event, helper, getInputkeyWord);
    } else {
      component.set("v.listOfSearchRecords", null);
      var forclose = component.find("searchRes");
      $A.util.addClass(forclose, "slds-is-close");
      $A.util.removeClass(forclose, "slds-is-open");
    }
  },

  // function for clear the Record Selaction
  clear: function (component, event, helper) {
    helper.clearsearch(component, event, helper);
  },

  // This function call when the end User Select any record from the result list.
  handleComponentEvent: function (component, event, helper) {
    // get the selected License record from the COMPONETNT event
    var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
    console.log(selectedLicenseGetFromEvent);
    console.log("selected");
    console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));

    component.set("v.selectedRecord", selectedLicenseGetFromEvent);

    console.log("=============recordByEvent===========");
    var forclose = component.find("lookup-pill");
    $A.util.addClass(forclose, "slds-show");
    $A.util.removeClass(forclose, "slds-hide");

    var forclose = component.find("searchRes");
    $A.util.addClass(forclose, "slds-is-close");
    $A.util.removeClass(forclose, "slds-is-open");

    var lookUpTarget = component.find("lookupField");
    $A.util.addClass(lookUpTarget, "slds-hide");
    $A.util.removeClass(lookUpTarget, "slds-show");
  },

  saveLicenseActivityRecord: function (component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");

      console.log("======saveLicenseActivityRecord===");
      //var LicenseObj = component.get("v.objLicenseActivity");
      //LicenseObj.Activity__c = null ;
      //console.log('======LicenseObj===' + JSON.stringify(LicenseObj));

      if (component.get("v.srObj.Business_Sector__c") == "Foundation") {
        const selectedActCode = component.get("v.selectedRecord")
          .Activity_Code__c;
        console.log(selectedActCode);
        var carityActivityCodeList = ["ONC", "OEC"];
        const actTable = component.get("v.LicesenceActivityTable");
        if (carityActivityCodeList.includes(selectedActCode)) {
          for (const activity of actTable) {
            if (
              carityActivityCodeList.includes(
                activity.Activity__r.Activity_Code__c
              ) &&
              activity.Activity_Code__c != selectedActCode
            ) {
              helper.displayToast(
                "Charitable and non charitable activities cannot be selected together",
                " ",
                "error"
              );
              return;
            }
          }
        }
      }

      console.log(component.get("v.selectedRecord").Id);
      console.log(srID);
      if (component.get("v.selectedRecord").Id) {
        //LicenseObj.Activity__c = component.get("v.selectedRecord").Id;
        //console.log('====== LicenseObjActivity__c===' +  JSON.stringify(LicenseObj.Activity__c));
        var action = component.get("c.saveLicenseActivity");
        action.setParams({
          ActivityId: component.get("v.selectedRecord").Id,
          srID: srID
        });
        action.setCallback(this, function (response) {
          //store state of response
          var state = response.getState();
          if (state === "SUCCESS") {
            var responseResult = response.getReturnValue();
            console.log(response.getReturnValue());
            component.set(
              "v.LicesenceActivityTable",
              responseResult.listLiscAct
            );

            for (var i = 0; i < responseResult.listLiscAct.length; i++) {
                if(responseResult.listLiscAct[i].Activity__r.Name == 'Operating Representative Office'){
                 
                  component.set("v.islicenseActivityName", true);
                  break;
                }
                else{
                  component.set("v.islicenseActivityName", false);
                }
            }



            debugger;
            var recordType = component.get("v.srObj.RecordType.DeveloperName");
            if (recordType != "Commercial_Permission") {
              //Show the Toast message info based on the selected activities value
              var selectedActivityMaster = JSON.parse(
                JSON.stringify(component.get("v.selectedRecord"))
              );
              console.log(selectedActivityMaster);
              console.log("DNFB ----- " + selectedActivityMaster.OB_DNFBP__c);

              if (
                selectedActivityMaster.OB_DNFBP__c &&
                selectedActivityMaster.OB_DNFBP__c.toLowerCase() == "yes"
              )
                //helper.displayToast("Information","Adding a DNFBP activity requires additional approval, please contact DIFC for additional information");
                helper.displayToast(
                  "Information",
                  "This business activity is a Designated Non-Financial Business or Profession (DNFBP) and requires DNFBP registration with The Dubai Financial Services Authority (DFSA). Please contact your Relationship Manager for more details on the process and requirements"
                );
              /* if (
            selectedActivityMaster.Sector_Classification__c &&
            selectedActivityMaster.Sector_Classification__c.toLowerCase() ==
              "investment fund"
          )
            helper.displayToast(
              "Information",
              "Adding this activity requires additional approval, please contact DIFC for additional information"
            ); */
              /*if (
            selectedActivityMaster.OB_Trade_Precious_Metal__c &&
            selectedActivityMaster.OB_Trade_Precious_Metal__c.toLowerCase() ==
              "yes"
          )
            helper.displayToast(
              "Information",
              "Adding this activity requires additional approval, please contact DIFC for additional information"
            );*/
              if (
                selectedActivityMaster.OB_Government_Entity_Approval_Required__c !=
                null
              )
                helper.displayToast(
                  "Information",
                  "Please obtain a No Objection Certificate or In principle Approval Letter from the " +
                    selectedActivityMaster.OB_Government_Entity_Approval_Required__c
                );
            }

            helper.clearsearch(component, event, helper);
            if (
              component.get("v.srObj.RecordType.DeveloperName") !=
              "AOR_Financial"
            ) {
              helper.showHideFields(component, event);
            }
          } else if (state === "ERROR") {
            // Handle any error by reporting it
            var errors = response.getError();

            if (errors) {
              if (errors[0] && errors[0].message) {
                helper.displayToast("Error", errors[0].message);
              }
            } else {
              helper.displayToast("Error", "Unknown error.");
            }
          }
        });
        $A.enqueueAction(action);
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  delete: function (component, event, helper) {
    console.log(event.target.id);

    var idOfItemToBeDeleted = event.target.id;
    var index = event.target.dataset.index;
    console.log(index);

    let newURL = new URL(window.location.href).searchParams;
    var srID = newURL.get("srId");
    console.log(component.get("v.licenseApplicationId"));

    var action = component.get("c.deleteLicenseActivity");
    $A.util.addClass(component.find("mySpinner"), "slds-show");
    action.setParams({
      LicenseId: event.target.id,
      srID: srID,
      licenseAppId: component.get("v.licenseApplicationId")
    });
    action.setCallback(this, function (response) {
      console.log("======deleteLicenseActivity======");
      //$A.util.removeClass(component.find("mySpinner"), "slds-show");
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        try {
          var responseResult = response.getReturnValue();
          var licenceTable = component.get("v.LicesenceActivityTable");
          licenceTable.splice(index, 1);
          console.log(licenceTable);

          /* for (var lisAct of licenceTable) {
            if (lisAct.Id == idOfItemToBeDeleted) {
              var index = licenceTable.indexOf(lisAct);
              console.log(index);
              licenceTable.splice(index, 1);
              break;
            }
          } */
          component.set("v.LicesenceActivityTable", licenceTable);

          //component.set("v.LicesenceActivityTable", responseResult.listLiscAct);
          component.set("v.isSellOrBuy", responseResult.isBuyOrSell);
          if (
            component.get("v.srObj.RecordType.DeveloperName") != "AOR_Financial"
          ) {
            helper.showHideFields(component, event);
          }
        } catch (error) {
          console.log(error.message);
        }

        //helper.clearsearch(component,event,helper);
      } else if (state === "ERROR") {
        // Handle any error by reporting it
        var errors = response.getError();

        if (errors) {
          if (errors[0] && errors[0].message) {
            helper.displayToast("Error", errors[0].message);
          }
        } else {
          helper.displayToast("Error", "Unknown error.");
        }
      }
    });
    $A.enqueueAction(action);
  },

  handleFileUploadFinished: function (component, event) {
    // Get the list of uploaded files
    var uploadedFiles = event.getParam("files");
    var fileName = uploadedFiles[0].name;

    var contentDocumentId = uploadedFiles[0].documentId;
    console.log(contentDocumentId);
    var docMasterCode = event.getSource().get("v.name");
    console.log(docMasterCode);
    console.log(component.find(docMasterCode).get("v.value"));
    console.log(fileName);
    if (component.find(docMasterCode))
      component.find(docMasterCode).set("v.value", fileName);

    console.log(component.find(docMasterCode).get("v.value"));
    var mapDocValues = {};
    var docMap = component.get("v.docMasterContentDocMap");
    console.log(docMap);
    for (var key in docMap) {
      mapDocValues[key] = docMap[key];
    }
    mapDocValues[docMasterCode] = contentDocumentId;
    console.log(mapDocValues);

    component.set("v.docMasterContentDocMap", mapDocValues);
    console.log(
      JSON.parse(JSON.stringify(component.get("v.docMasterContentDocMap")))
    );
  },

  handleButtonAct: function (component, event, helper) {
    console.log(
      $A.util.isEmpty(component.get("v.srObj.Dealing_in_Precious_Metals__c"))
    );

    let listAct = component.get("v.LicesenceActivityTable");

    if (listAct.length < 1) {
      helper.displayToast("Please add at least one activity", " ", "error");
      return;
    }

    if (component.get("v.srObj.RecordType.DeveloperName") != "AOR_Financial") {
      var allValid = true;

      let fields = component.find("reqField");
      if (fields) {
        if (!Array.isArray(fields)) {
          if (!fields.get("v.value") && fields && fields.get("v.required")) {
            console.log("no value");
            //console.log('bad');
            fields.reportValidity();
            allValid = false;
          }
        } else {
          for (var i = 0; i < fields.length; i++) {
            var inputFieldCompValue = fields[i].get("v.value");
            //console.log(inputFieldCompValue);
            if (
              !inputFieldCompValue &&
              fields[i] &&
              fields[i].get("v.required")
            ) {
              console.log("no value");
              //console.log('bad');
              fields[i].reportValidity();
              allValid = false;
            }
          }
        }
      }

      if (!allValid) {
        console.log("Error MSG");

        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          mode: "dismissable",
          message: "Please fill required field",
          type: "error",
          duration: 500
        });
        toastEvent.fire();

        return;
      }

      /* let formValidity = helper.formValidator(
        component,
        event,
        helper,
        "reqField"
      ); 

      if (!formValidity) {
        helper.displayToast(
          "Please complete the required fields",
          " ",
          "error"
        );
        return;
      }*/
    }

    /*
        var fieldList = ['Buying_Selling_Real_Estate_Property__c', 'Category_of_Service__c', 'Category_of_Service_Explanation__c', 'Nature_of_Business__c', 'Parent_or_Entity_Business_Activities__c'];
        
        fieldList = fieldList.map(x => component.find(x) || null);
        console.log(fieldList);
        var filtered = fieldList.filter(function (el) {
            return el != null;
        });
        console.log(filtered);
        
        var filled = true;
        for(const element of filtered){
            console.log(element.get("v.value"));
            if(element.get("v.value") == '' || element.get("v.value").trim().length === 0){
               filled =  false;
                break;
            }
            
        }
        
        if(!filled){
           helper.displayToast("Please complete the required fields"," ");
           return;
        }
        */

    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      

      console.log(component.get("v.docMasterContentDocMap"));

      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        serviceRequest: component.get("v.srObj"),
        pageId: pageID,
        ButtonId: buttonId,
        docMap: component.get("v.docMasterContentDocMap")
      });
      console.log(JSON.parse(JSON.stringify(component.get("v.srObj"))));
      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");
          var respwrap = response.getReturnValue();

          console.log(respwrap.errorMessage);
          if (respwrap.errorMessage) {
            helper.displayToast(respwrap.errorMessage, " ", "error");
          }

          if (respwrap.errorMessage == undefined) {
            console.log(response.getReturnValue());
            console.log(response.getReturnValue().updatedSr);

            window.open(response.getReturnValue().pageActionName, "_self");
          } else {
            console.log(error);

            helper.displayToast("Error", respwrap.errorMessage);
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },
  checkDNFBP: function (component, event, helper) {
    console.log(event.getSource().get("v.value"));
    if (event.getSource().get("v.value") == "Yes") {
      helper.displayToast(
        "Information",
        "Please contact DIFC to find the process for obtaining DNFBP approval"
      );
      component.set("v.srObj.DNFBP__c", true);
    } else if (event.getSource().get("v.value") == "No") {
      component.set("v.srObj.DNFBP__c", false);
    }
  }
});