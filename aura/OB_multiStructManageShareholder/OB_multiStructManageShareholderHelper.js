({
    onDynamicButtonAction : function(cmp, event, reqWrapPram)
	{
		//Opening Section
		var objRequestMapTemp = cmp.get('v.objRequestMap');
		var dycBtnAction = cmp.get('c.dynamicButtonAction');
		console.log('############ reqWrapPram ',reqWrapPram);
		
	   dycBtnAction.setParams({
			 "reqWrapPram": JSON.stringify(reqWrapPram) 
		 });
		
		 
		 dycBtnAction.setCallback(this, 
			 function(response) {
				 var state = response.getState();
				 console.log("callback state: " + state);
			 
				 if (cmp.isValid() && state === "SUCCESS") 
				 {
                      //cmp.set('v.spinner',false);
					  console.log("resp ",JSON.stringify(response.getReturnValue()) );
                     var respWrap = response.getReturnValue();
					 
                     console.log('######### respWrap.errorMessage ',respWrap.errorMessage);
                     if(!$A.util.isEmpty(respWrap.errorMessage)){
                         console.log('######### inside ',respWrap.errorMessage);
                        // cmp.set("v.errorMessage",respWrap.errorMessage);
                        // cmp.set("v.messageType",'error');
                         
                         //cmp.set("v.isError",true);
                         cmp.set('v.spinner',false);
                         this.createToast(cmp,event,respWrap.errorMessage);
                         //component.find("toastCmp").showToastModel(respWrap.errorMessage, messageType);
                     	//this.showToast(cmp,event,respWrap.errorMessage);
                     }else{
                        // cmp.set("v.isError",false);
                     }
                     var isError = cmp.get("v.isError");
                     console.log('######### isError----- ',isError);
                     if(!$A.util.isEmpty(respWrap.errorMessage)){
                         var pathname = window.location.pathname;
                         console.log('==pathname==='+pathname);
                         
                         var pageURL = '';
                         if(respWrap.isPublicSite == true){// for site
                             
                             var newPathName='/'+respWrap.sitePageName;
                             if(pathname == newPathName){
                                 console.log(pathname+'==inside same path name==='+newPathName);
                                 let newURL = new URL(window.location.href).searchParams;
                                 //console.log('==srID url=='+newURL.get('srId'));
                                 
                                 var appEvent = $A.get("e.c:OB_RenderPageFlowAppEvent");
                                 var srId = respWrap.srId;
                                 var flowId = respWrap.flowId;
                                 var pageId = respWrap.pageId;
                                 
                                 appEvent.setParams({
                                     "pageId": pageId,
                                     "srId" : srId,
                                     "flowId":flowId,
                                     "isButton":true});
                                 console.log('-next button -event fire-');
                                 appEvent.fire();
                             }else{
                                 if( !$A.util.isUndefined(respWrap.sitePageName) &&
                                     !$A.util.isEmpty(respWrap.sitePageName) && 
                                    respWrap.sitePageName.indexOf("?") > -1){
                                     pageURL = respWrap.strBaseUrl+'/'+respWrap.sitePageName+'&srId='+respWrap.srId+'&flowId='+respWrap.flowId+'&pageId='+respWrap.pageId;
                                 }else{
                                    pageURL = respWrap.strBaseUrl+'/'+respWrap.sitePageName+'?srId='+respWrap.srId+'&flowId='+respWrap.flowId+'&pageId='+respWrap.pageId;
                                    console.log('==site url===');
                                 }
                             	window.open(pageURL,"_self");
                             }
                             //console.log('==pageURL==='+pageURL);
                     		 window.open(pageURL,"_self");
                         }else{// for community
                            var newPathName='/'+respWrap.communityName+'/'+respWrap.CommunityPageName;
                             console.log(pathname+'=1=inside same path name==='+newPathName);
                             if(pathname == newPathName){
                                 console.log(pathname+'==inside same path name==='+newPathName);
                                 let newURL = new URL(window.location.href).searchParams;
                                 console.log('==srID url=='+newURL.get('srId'));
                                 
                                 var appEvent = $A.get("e.c:OB_RenderPageFlowAppEvent");
                                 var srId = respWrap.srId;
                                 var flowId = respWrap.flowId;
                                 var pageId = respWrap.pageId;
                                 
                                 appEvent.setParams({
                                     "pageId": pageId,
                                     "srId" : srId,
                                     "flowId":flowId,
                                     "isButton":true});
                                 console.log('-next button -event fire-');
                                 appEvent.fire();
                             }else{ 
                                 if(!$A.util.isUndefined(respWrap.CommunityPageName) &&
                                     !$A.util.isEmpty(respWrap.CommunityPageName) && 
                                    respWrap.CommunityPageName.indexOf("?") > -1){
                                    pageURL = respWrap.strBaseUrl+'/'+respWrap.communityName+'/'+respWrap.CommunityPageName+'&srId='+respWrap.srId+'&flowId='+respWrap.flowId+'&pageId='+respWrap.pageId;
                                 }else{
                                     pageURL = respWrap.strBaseUrl+'/'+respWrap.communityName+'/'+respWrap.CommunityPageName+'?srId='+respWrap.srId+'&flowId='+respWrap.flowId+'&pageId='+respWrap.pageId;
                                 }
                                 //console.log('==pageURL==='+pageURL);
                                 window.open(pageURL,"_self");
                             }
                         }
                     	
					    
                     }
 					
				 }
				  else
				 {
                      cmp.set('v.spinner',false);
					 console.log('@@@@@ Error '+response.getError()[0].message);
                     this.createToast(cmp,event, response.getError()[0].message);
				 }
                 
			 }
		 );
		 $A.enqueueAction(dycBtnAction);
	},
    
    // function automatic called by aura:waiting event  
    showSpinner: function(component, event, helper) {
        // make Spinner attribute true for displaying loading spinner 
        component.set("v.spinner", true); 
    },
     
    // function automatic called by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
        // make Spinner attribute to false for hiding loading spinner    
        component.set("v.spinner", false);
    },
    
    fieldCount : function(cmp, event, sectionWrapperList) {
    	var fieldCound = 0;
        var fieldContainingValue = 0;
        for (var section of sectionWrapperList) {
            if(section.sectionObj.HexaBPM__Section_Type__c != 'CommandButtonSection') {
                for(var field of section.SIWraps) {
                    if(field.bRenderSecDetail) {
                        console.log(field);
                        fieldCound += 1;
                        if(field.FieldValue != '') {
                        	fieldContainingValue += 1;   
                        } 
                    }
                }
            }	
        }
        cmp.set('v.numbOfFields', fieldCound);
        cmp.set('v.numbOfFieldsFilled', fieldContainingValue);
        
	},
      createToast: function(component,event, errorMessagr){
         var toastEvent = $A.get("e.force:showToast");
          if(toastEvent){
      toastEvent.setParams({
        mode: "dismissable",
        message: errorMessagr ,
        type: "error",
        duration: 500
      });
      toastEvent.fire();
          }else{
              alert(errorMessagr);
          }
    }
    
})