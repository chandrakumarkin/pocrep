({
  doInit: function(cmp, event, helper) {
    cmp.set("v.spinner", true);
    try {
      var getFormAction = cmp.get("c.getExistingAmendment");

      let newURL = new URL(window.location.href).searchParams;
      var pageId = newURL.get("pageId");
      var srIdParam = newURL.get("srId");
      var flowId = newURL.get("flowId");
      
      var reqWrapPram = {
                   srId: srIdParam,
                 pageID: pageId,
        		     flowId: flowId
        
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });


      getFormAction.setCallback(this, function(response) 
      {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") 
        {
              var respWrap = response.getReturnValue();
              console.log(JSON.parse(JSON.stringify(respWrap)));
              
              // setting entityTypeLabel 
              if( respWrap.entityTypeLabel )
              {
                 cmp.set('v.entityTypeLabel', respWrap.entityTypeLabel ); 
              }
            
              cmp.set("v.srWrap", respWrap.srWrap);
              cmp.set("v.srList", respWrap.srList);
            console.log(
                "$$$$$$$srList",
                JSON.stringify(respWrap.srList)
              );
              console.log(
                "$$$$$$$$%%%%%%%%%%res",
                JSON.stringify(respWrap.srWrap.srObj)
              );
              
              cmp.set("v.commandButton", respWrap.ButtonSection);
              console.log(
                "#########123456 respWrap ",
                JSON.stringify(cmp.get("v.srWrap.shareholderAmedWrapLst"))
              );
    
              cmp.set("v.spinner", false);
        } 
        else 
        {
              cmp.set("v.spinner", false);
              console.log("@@@@@ Error " + response.getError()[0].message);
              //cmp.set("v.errorMessage", response.getError()[0].message);
              //cmp.set("v.isError", true);
              helper.createToast(cmp, event, response.getError()[0].message);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
      cmp.set("v.spinner", false);
    }
  },
  handleRefreshEvnt: function(cmp, event, helper) {
    var amedId = event.getParam("amedId");
    var srWrap = event.getParam("srWrap");
    var isNewAmendment = event.getParam("isNewAmendment");
    var amedWrap = event.getParam("amedWrap");
    console.log("$$$$$$$$$$$ handleRefreshEvnt== ", JSON.stringify(srWrap));
    console.log("2222222222 handleRefreshEvnt== ", amedWrap);
    cmp.set("v.amedId", amedId);
    cmp.set("v.selectedValue", "");

    //for auto scolling
    var activeScreen = document.getElementById("activePage");
    console.log("id of active screen ------>" + activeScreen);
    debugger;

    if (activeScreen) {
      activeScreen.scrollIntoView({
        block: "start",
        behavior: "smooth"
      });
    }

    if (amedId != null && amedId != "") {
      cmp.set("v.isNewPanelShow", false);
    } else {
      cmp.set("v.isNewPanelShow", true);
    }

    if (!$A.util.isEmpty(srWrap)) {
      console.log("====sr wrap assign===");
      cmp.set("v.srWrap", srWrap);
      cmp.set("v.selectedValue", "");
    }
    if (isNewAmendment == true) {
      console.log("====isNew===");
      cmp.set("v.amedWrap", amedWrap);
      cmp.set("v.selectedValue", "");
    }
    if (isNewAmendment == false) {
      console.log("====isNew===");
      cmp.set("v.amedWrap", amedWrap);
      cmp.set("v.selectedValue", "");
    }
  },
  onChange: function(cmp, evt, helper) 
  {
    cmp.set("v.showForm", false);
    var amedSelWrap;
 
    var amedIdParam = cmp.find("select").get("v.value");
    var shareholderAmedWrapLst = cmp.get("v.srWrap").shareholderAmedWrapLst;
    cmp.set("v.selectedValue", amedIdParam);
    console.log("########### amedIdParam ", amedIdParam);


    
      for (var i = 0; i < shareholderAmedWrapLst.length; i++) 
      {
        var amedWrap = shareholderAmedWrapLst[i];
        if (amedWrap.amedObj.Id == amedIdParam) 
        {
          amedSelWrap = amedWrap;
          break;
        }
      }
      console.log("########### amedSelWrap1 ", amedSelWrap);
      if (amedSelWrap) 
      {
        console.log("########### amedSelWrap2 ", amedSelWrap);
        //cmp.set("v.showForm", true);
        cmp.set("v.amedSelWrap", amedSelWrap);
      }
    


  },
  openForm: function(cmp, evt, helper) 
  {
    console.log('In openForm ',cmp.get("v.amedSelWrap") );
    cmp.set("v.showForm", true);
  }, 

  /* convertToShareHolder: function (cmp, evt, helper) {
        console.log('-convertToShareHolder---');
        cmp.set('v.spinner',true);
        try{
    	var getFormAction = cmp.get("c.getConvertToShareholder");
        var amendmentID =  cmp.get('v.selectedValue');  
        var srIdParam =  (  cmp.get('v.srId') ?  cmp.get('v.srId') : newURL.get('srId'));
            console.log(amendmentID+'===srIdParam==='+srIdParam); 
        var reqWrapPram  =
            {
                srId: srIdParam,
                amendmentID: amendmentID
            }
        getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
        
        getFormAction.setCallback(this, 
			function(response) {
           		var state = response.getState();
                
                if (cmp.isValid() && state === "SUCCESS") 
                {
                    var respWrap = response.getReturnValue();
                    console.log('==respWrap=state='+state);
                    if(!$A.util.isEmpty(respWrap.errorMessage)){
                        console.log('=====error on sve===='+respWrap.errorMessage);
                        //this.showToast("Error",respWrap.errorMessage);
                        cmp.set("v.errorMessage",respWrap.errorMessage);
                       	cmp.set("v.isError",true);
                        cmp.set('v.spinner',false);
                    }else{
                    //helper.hideSpinner(cmp, event, helper);
                    cmp.set('v.srWrap',respWrap.srWrap);
                    console.log('######### sucess '); 
                    cmp.set('v.selectedValue',''); 
                    cmp.set('v.spinner',false);
                    }
                }
            }
		);
        $A.enqueueAction(getFormAction);
        
        }catch(err){
            cmp.set('v.spinner',false);
            console.log('=error==='+err.message);
            cmp.set("v.errorMessage",err.message);
            cmp.set("v.isError",true);
        }
    },
    showToast : function(title,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message
        });
        toastEvent.fire();
	},*/
  closeModel: function(cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },
  handleButtonAct: function(component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId
      });

      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");

          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          helper.createToast(component, event, response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  }
});