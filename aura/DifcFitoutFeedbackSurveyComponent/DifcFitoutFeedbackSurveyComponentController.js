({
	doInit : function(component, event, helper) {
        //alert(component.get("v.stepids"));
        console.log(component.get("v.stepids"));
        console.log(component.get("v.stepids"));
        helper.serveyFeedbackDetails(component);
	},
	submitFeedback : function(component, event, helper){
		helper.submitFeedback(component);
	},
    showSpinner: function(component, event, helper) {
       // make Spinner attribute true for display loading spinner 
        component.set("v.Spinner", true); 
   },
    
 // this function automatic call by aura:doneWaiting event 
    hideSpinner : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner    
       component.set("v.Spinner", false);
    },
    showComponent : function (component, event, helper) {
      $A.util.removeClass(component.find("yourIdentifier"), "slds-hide");
    },
    hideComponent : function (component, event, helper) {
      $A.util.addClass(component.find("yourIdentifier"), "slds-hide");
    }
})