({
    serveyFeedbackDetails: function(component){
        var action = component.get("c.getServeyFeedbackWrapperRecord");
        //alert('Step id in component is ---'+component.get("v.stepids"));
        action.setParams({
                spepIdValue : component.get("v.stepids")
            });
        action.setCallback(this,function(response){
        var state = response.getState();
        if (state === "SUCCESS"){
            component.set("v.serveyFeedbackDetails", response.getReturnValue());
            console.log(response.getReturnValue().feedbackSurveyList);
            component.set("v.spepRecord",response.getReturnValue().stepRecordValue);
            component.set("v.surveyQuestions",response.getReturnValue().feedbackSurveyList);
            component.set("v.openStep",response.getReturnValue().isOpenStep);
            console.log("feedbackSurveyList---: " + component.get("v.serveyFeedbackDetails.feedbackSurveyList[0].Question__c"));
        }
        else {
            console.log("Failed with state: " + state);
             }
        });
        //send action off to be executed
            $A.enqueueAction(action);
    },
    getRelatedQuestions : function(component) {
    	//create the action
        console.log("Helper started");
        var action = component.get("c.getRelatedQuestions");
        //add the callback behavior for when the response is received
        action.setCallback(this,function(response){
        var state = response.getState();
        if (state === "SUCCESS"){
            component.set("v.surveyQuestions", response.getReturnValue());
            console.log(response.getReturnValue());
        }
        else {
            console.log("Failed with state: " + state);
             }
        });
        //send action off to be executed
            $A.enqueueAction(action);
       },
       
       submitFeedback : function(component) {
           var arr = component.get("v.surveyQuestions");
           var i=0;
           var fitoutFeedbackValidations ='';
           for (i = 0; i < component.get("v.surveyQuestions").length; ++i) {
               console.log('jsonStr----'+arr[i].Question__c);
               console.log('jsonStr--Rating__c--'+arr[i].Rating__c);
               if(arr[i].Rating__c=='0'){                  
                  fitoutFeedbackValidations='Yes';
               }
           }
           //alert('fitoutFeedbackValidations---'+fitoutFeedbackValidations);
           //fitoutFeedbackValue = JSON.stringify(component.get("v.surveyQuestions));
           if(fitoutFeedbackValidations == 'Yes'){
               alert('Please select the response against each Feedback Section'); 
           }else{
               //create the action
                //alert("submitFeedback started");
                console.log("submitFeedback started");
                var action = component.get("c.insertFitoutFeedback");
                var surveyQuestList = component.get("v.surveyQuestions");
        		for(var i=0; i < surveyQuestList.length; i++){ 
                    surveyQuestList[i].Remarks__c = component.get("v.Remarks");
                }
                component.set("v.surveyQuestions", surveyQuestList);
                action.setParams({
                     feedbackSurveys : component.get("v.surveyQuestions")
                 });
                //add the callback behavior for when the response is received
                action.setCallback(this,function(response){
                var state = response.getState();
                if (state === "SUCCESS"){  
                    window.setTimeout(this.redirect(),15000);
                }
                else {
                    console.log("Failed with state: " + state);
                     }
                });
                //send action off to be executed
               
           }
    	 $A.enqueueAction(action);
       },
       cancel : function(component,event,helper){
            window.open('/digitalOnboarding/s/',"_self");
        },
        redirect : function(component,event,helper){
            alert('Feedback submitted successfully');
            window.open('/digitalOnboarding/s/',"_self");
        },
})