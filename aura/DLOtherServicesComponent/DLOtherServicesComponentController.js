({
    
     doinits : function(component,event,helper){
     // make Spinner attribute to false for hide loading spinner  
       
           helper.doInit(component, event, helper); 
        
     },
      saveotherRequests: function (cmp, event, helper) {
        var allValid = cmp.find('field').reduce(
                         function(validSoFar, inputCmp) 
            {
            inputCmp.showHelpMessageIfInvalid();

           return validSoFar && inputCmp.get('v.validity').valid;
            }, true);

         if (!allValid) {
             cmp.set("v.isError",true);
             setTimeout(function(){ cmp.set("v.isError",false); }, 4500); 
             cmp.set("v.successmsg",'All Required Fields Should Be Populated!'); 
             return;
         }
        cmp.set("v.Spinner", true);   
        var action = cmp.get("c.saveRequest");
            action.setParams({
                newServiceRequest : cmp.get("v.serviceRqst")
            });
           
            action.setCallback(this, function (a) {
                var state = a.getState();
                
             
                if (state == "SUCCESS") {
                    if(a.getReturnValue().startsWith('Error')){
                        cmp.set("v.isError",true);
                         setTimeout(function(){cmp.set("v.isError",false);}, 4500);
                         cmp.set("v.successmsg",JSON.stringify(a.getReturnValue())); 
                         cmp.set("v.Spinner", false);
                    }
                    else{
                        
                         cmp.set("v.issaved", true); 
                         window.open(a.getReturnValue()+'?isSR=GS&nooverride=1&RecordType=0123N000001SPY0QAO',"_self");
                     }
                 } else {
                  
                    cmp.set("v.Spinner", false);
                 }
            });
            $A.enqueueAction(action);
     
    },
    
     changeExpress : function (component, event, helper) {
       component.set("v.serviceRqst.Express_Service__c",event.getSource().get('v.checked'));
    },
        changenationality1 : function (component, event, helper) {
       
      component.set("v.serviceRqst.Nationality_list__c",component.find("nationality").get("v.value"));
      if(component.find("nationality").get("v.value") === 'United Arab Emirates' ||component.find("nationality").get("v.value") === 'Kuwait'
         || component.find("nationality").get("v.value") === 'Saudi Arabia' ||component.find("nationality").get("v.value") === 'Oman'
         || component.find("nationality").get("v.value") === 'Bahrain' || component.find("nationality").get("v.value") === 'Qatar'  ){
          
          component.set("v.isGCC",true);
           component.set("v.serviceRqst.Residence_Visa_No__c", '');
           component.set("v.serviceRqst.Residence_Visa_Expiry_Date__c", '');
      }
        else{
            component.set("v.isGCC",false);
      }
        
        
    },
    
    lookupchange: function(component, event, helper) {
        component.set("v.selectedLookId",   event.getSource().get("v.value"));
        component.set("v.islookup", "true");
    },
    
     typeOfamendChange: function(component, event, helper) {
        var selectedOptionValue = event.getParam("value");
   
        if(selectedOptionValue.toString().includes("Name")){
            component.set("v.isNameDisabled",false);
        }
         else{
             component.set("v.isNameDisabled",true);
         }
         
         if(selectedOptionValue.toString().includes("Nationality")){
            component.set("v.NationalityDisabled",false);
        }
         else{
             component.set("v.NationalityDisabled",true);
         }
         
          component.set("v.serviceRqst.Type_of_Amendment__c",event.getSource().get("v.value"));
    },
    
     closeLookUp: function(component, event, helper) {
        component.set("v.islookup", "false");
        component.set("v.islookupOther", "false");
       // location.reload(true);
    }, 
    
   
    
    closeLookUp: function(component, event, helper) {
        component.set("v.islookup", "false");
        component.set("v.islookupOther", "false");
    }, 
    
      AddLookUp: function(component, event, helper) {
          if(component.get("v.selectedLookUpRecord").Name == null){
              return;
          }
          component.set("v.serviceRqst.Occupation_GS__c", component.get("v.selectedLookUpRecord").Name);
          component.find("occup").set("v.value",component.get("v.selectedLookUpRecord").Name);
          component.set("v.islookup", "false");
    }, 
     changeRegis : function (component, event, helper) {
       component.set("v.isReg",event.getSource().get('v.checked'));
       component.set("v.serviceRqst.Use_Registered_Address__c",event.getSource().get('v.checked'));
        
       var action = component.get("c.getRegistratedAccounts");
            action.setParams({
                accountId : component.get("v.accountNew.Id")
            });
           
            action.setCallback(this, function (a) {
                var state = a.getState();
                if (state == "SUCCESS") {
                   component.set("v.registiredAddress",a.getReturnValue()); 
               } 
            });
            $A.enqueueAction(action);
    },
    mobilevalidation: function (component, event, helper) {
      if (component.get("v.serviceRqst.Courier_Mobile_Number__c") != '' && !component.get("v.serviceRqst.Courier_Mobile_Number__c").startsWith("+971")) {
        component.set("v.mobileValidation1", true);
      } else {
        component.set("v.mobileValidation1", false);
      }
    },
    mobilevalidation1: function (component, event, helper) {
      if (component.get("v.serviceRqst.Courier_Cell_Phone__c") != '' && !component.get("v.serviceRqst.Courier_Cell_Phone__c").startsWith("+971")) {
        component.set("v.mobileValidation2", true);
      } else {
        component.set("v.mobileValidation2", false);
      }
    },

    
	
})