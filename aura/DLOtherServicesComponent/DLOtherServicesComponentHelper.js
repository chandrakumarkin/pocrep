({
	doInit: function (cmp, event, helper) {  
        if(cmp.get("v.otherServiceName")== 'Renewal'){
            
            cmp.set("v.NationalityDisabled",false);
            cmp.set("v.occupationDisabled",false);
            cmp.set("v.DateofBirthDisabled",false);
            cmp.set("v.PassportDateofExpiryDisabled",false);
            cmp.set("v.PassportDateofissueDisabled",false);
            cmp.set("v.DateofBirthDisabled",false);
            cmp.set("v.PassportDateofissueDisabled",false);
            cmp.set("v.isPassportDisabled",false);
            cmp.set("v.visaDisabled",false);
            cmp.set("v.visaExpiryDisabled",false);
            
        }
        
          var action = cmp.get("c.getSelectedContact");
            action.setParams({
                contactId : cmp.get("v.contactId")
            });
            action.setCallback(this, function (a) {
                var state = a.getState();
                
                if (state == "SUCCESS") {
               
                   var result = a.getReturnValue();
                    
                       cmp.set("v.accountNew",result.newAccount);
                       cmp.set("v.optionList",result.optionList);
                       cmp.set("v.occupationList",result.occuptationList);
                       cmp.set("v.selectedTitle",result.newSR.Title__c);
                        cmp.set("v.serviceRqst",result.newSR);
                       cmp.set("v.selectedNationality",result.newSR.Nationality_list__c);
                       cmp.set("v.occuptionSelected",'test');
                       cmp.find("Title").set("v.value",result.newSR.Title__c);
                       cmp.find("nationality").set("v.value",result.newSR.Nationality_list__c);
                       cmp.find("occup").set("v.value",result.newSR.Commercial_Activity__c);
                       cmp.set("v.serviceRqst.Occupation_GS__c", result.newSR.Commercial_Activity__c);
                       
               if(cmp.get("v.otherServiceName")== 'Renewal'){
                   cmp.set("v.serviceRqst.Service_Category__c", 'Renewal');
               }
               if(cmp.get("v.otherServiceName")== 'Amendment'){
                   cmp.set("v.serviceRqst.Service_Category__c", 'Amendment');
               }
               if(cmp.get("v.otherServiceName")== 'Cancellation'){
                   cmp.set("v.serviceRqst.Service_Category__c", 'Cancellation');
               }
               if(cmp.get("v.otherServiceName")== 'Lost Replacement'){
                   cmp.set("v.serviceRqst.Service_Category__c", 'Lost / Replacement');
               }
                       
                 } else {
                 alert(cmp.get("v.contactId"));
                }
              
            });
            $A.enqueueAction(action);               
    },
})