({
  doInit: function (component, event, helper) {},

  addCessationDate: function (component, event, helper) {
    var cessationDate = component.get("v.cessationDate");
    if (cessationDate) {
      var amend = component.get("v.amendObj");
      amend.cessation_date__c = cessationDate;
      var action = component.get("c.updateCessationDate");
      var requestWrap = {
        amendObj: component.get("v.amendObj"),
        srId: component.get("v.srId"),
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log(response.getReturnValue());

          if (response.getReturnValue().errorMessage == "no error recorded") {
            /* component.set(
              "v.AmendListWrapp",
              response.getReturnValue().amendWrapp
            ); */
            component.set("v.respWrap", response.getReturnValue());
            component.set("v.showModal", false);
            component.set("v.showSpinner", false);
            component.set("v.selectedValue", "");

            /* var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
              mode: "dismissable",
              message: "updated Succsessfully",
              type: "success",
              duration: 500
            });
            toastEvent.fire(); */
          }
        } else {
          component.set("v.showSpinner", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log("@@@@@ Error " + response.getError()[0].stackTrace);
        }
      });
      $A.enqueueAction(action);
    } else {
      helper.createToast(component, event, "Please enter a valid date");
    }
  },

  closeBlockedModal: function (component, event, helper) {
    component.set("v.showModal", false);
  },

  deleteAmend: function (component, event, helper) {
    console.log("IN delete IP");
    var amendObj = component.get("v.amendObj");
    console.log(JSON.parse(JSON.stringify(amendObj)));
    console.log(amendObj);
    if (amendObj.Relationship_ID__c != null) {
      component.set("v.showModal", true);
      return;
    }

    component.set("v.showSpinner", true);
    try {
      var action = component.get("c.amendRemoveAction");
      var requestWrap = {
        amendObj: component.get("v.amendObj"),
        isAssigned: component.get("v.isAssigned"),
        srId: component.get("v.srId"),
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log(response.getReturnValue());

          if (response.getReturnValue().errorMessage == "no error recorded") {
            /*  component.set(
              "v.AmendListWrapp",
              response.getReturnValue().amendWrapp
            ); */
            component.set("v.respWrap", response.getReturnValue());
            component.set("v.showSpinner", false);
            component.set("v.selectedValue", "");

            /* var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
              mode: "dismissable",
              message: "updated Succsessfully",
              type: "success",
              duration: 500
            });
            toastEvent.fire(); */
          }
        } else {
          component.set("v.showSpinner", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log("@@@@@ Error " + response.getError()[0].stackTrace);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
        component.set("v.showSpinner", false);
      console.log(err.message);
    }
  },

  viewDetails: function (component, event, helper) {
    component.set("v.amedIdInFocus", component.get("v.amendObjId"));
  },
});