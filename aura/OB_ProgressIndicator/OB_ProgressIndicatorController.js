({
	doInit: function (component, event, helper) {

		var action = component.get("c.getStepGroupWrapper");
		action.setParams({
			serviceRequestId: component.get("v.recordId")
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (component.isValid() && state === "SUCCESS") {
                console.log(response.getReturnValue());
				component.set("v.lstGroupWrap", response.getReturnValue().lstLtngGroupWrapper);
				var groupSize = (100 / response.getReturnValue().lstLtngGroupWrapper.length) + '%';
				component.set("v.GroupWidth", groupSize);
				component.set("v.totalTime", response.getReturnValue().totalTime);
				if (response.getReturnValue().lstLtngGroupWrapper[0])
					component.set("v.SLA", response.getReturnValue().lstLtngGroupWrapper[0].SLA);
			} else {
				console.log("Fail");
			}
		});
		$A.enqueueAction(action);

	},
	navigateStep: function (component, event, helper) {
		var stepId = event.currentTarget.dataset.step;
		var navEvt = $A.get("e.force:navigateToSObject");
		navEvt.setParams({
			"recordId": stepId,
			"slideDevName": "related"
		});
		navEvt.fire();

	},
	navigateToDocURL: function (component, event, helper) {
		var stepId = event.currentTarget.dataset.step;
		var srId = event.currentTarget.dataset.servicerequest;
		var urlPath = event.currentTarget.dataset.urlpath;
		var navURL = '/apex/' + urlPath + '?SRID=' + srId + '&StepId=' + stepId;
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": navURL
		});
		urlEvent.fire();

	},
	navigateToStepTransitionURL: function (component, event, helper) {
		var stepId = event.currentTarget.dataset.step;
		var srId = event.currentTarget.dataset.servicerequest;
		var urlPath = event.currentTarget.dataset.urlpath;
		var navURL = '/apex/' + urlPath + '?Id=' + srId + '&StepId=' + stepId;
		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": navURL
		});
		urlEvent.fire();
	},
	navigateToCreditScoreURL: function (component, event, helper) {
		var stepId = event.currentTarget.dataset.step;
		var srId = event.currentTarget.dataset.srid;
		var urlPath = event.currentTarget.dataset.urlpath;
		var navURL = '/apex/' + urlPath + '?Id=' + srId + '&StepId=' + stepId;

		var urlEvent = $A.get("e.force:navigateToURL");
		urlEvent.setParams({
			"url": navURL
		});
		urlEvent.fire();
	},
	display: function (component, event, helper) {
		helper.toggleHelper(component, event);
	},

	displayOut: function (component, event, helper) {
		helper.toggleHelper(component, event);
	}
})