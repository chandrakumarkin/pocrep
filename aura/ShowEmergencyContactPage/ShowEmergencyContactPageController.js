({
	doInit : function(component, event, helper) {
		console.log('### recordId '+component.get('v.recordId'));
        component.set('v.columns',[
            {label:'Contact', fieldName :'ContactName',type : "Text"},
            {label : 'Client', fieldName :'AccountName',type:'Text'},
            {label : 'Entity Type', fieldName :'Entity_Type__c', type: 'Text'},
            {label : 'Building Name', fieldName :'Building_Name__c', type: 'Text'},
            {label : 'Email', fieldName :'Email__c', type: 'Email'},
            {label : 'Phone', fieldName :'Phone__c', type: 'Phone'}
        ]);
        var action = component.get('c.getDetails');
        action.setParams({
            recordId : component.get('v.recordId')
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('### state '+state);            
            if(state === 'SUCCESS'){
                var responseValue = response.getReturnValue();
                //setting columns for datatable
                var rows = responseValue.recepientList;
                var smsCount=0;
                for(var i=0; i<rows.length; i++){
                    var row = rows[i];
                    if(row.AccountLookup__c){
                        row.AccountName = row.AccountLookup__r.Name;
                    }
                    if(row.Contact__c){
                        row.ContactName = row.Contact__r.Name;
                    }
                    if(row.Phone__c){
                        smsCount++;
                    }
                }
                console.log('rows '+JSON.stringify(rows));
                //end
                console.log('### responseValue '+JSON.stringify(responseValue));
                component.set('v.communicationObject',responseValue.commObject);
                component.set('v.recepients',rows);
                console.log('recepientLength '+responseValue.recepientList.length);	
                component.set('v.recepientCount',responseValue.recepientList.length);
                console.log('### sms count '+smsCount);
                component.set('v.smsRecepients',smsCount);
                var selectedRowsId = [];
                for(var i=0; i < rows.length; i++){
                    selectedRowsId.push(rows[i].Id);
                }
                console.log('### selectedRowsId '+JSON.stringify(selectedRowsId));
                component.set('v.selectedRowsToShow',selectedRowsId);
                console.log('### value set '+JSON.stringify(component.get('v.selectedRowsToShow')));
                console.log('### responseValue.commObject.Status__c  '+responseValue.commObject.Status__c );
                var doNotSend = (responseValue.commObject != null && responseValue.commObject.Status__c == 'Submitted') ?
                    true : false;
                console.log('### doNotSend '+doNotSend);
                component.set('v.doNotSend',doNotSend);
                component.set('v.disableSend',doNotSend);
                component.set('v.additionalEmails',responseValue.commObject.AdditionalEmails__c);
            }
            else{
                console.log('### response Error ',response.getError());
            }
        });
        $A.enqueueAction(action);
        //window.location.reload();
	},
    
    showRecepients : function(component,event,helper){
        component.set('v.showData',true);
    },
    
    updateSelectedText: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        console.log('### rowsLength '+selectedRows.length);
        console.log('### selectedRows '+JSON.stringify(selectedRows));
        //cmp.set('v.recepientCount', selectedRows.length);
        cmp.set('v.selectedRecepients',selectedRows); 
    },
    
    backToReview : function(component,event,helper){
        component.set('v.showData',false);
        if(component.get('v.selectedRecepients').length > 0){
            component.set('v.recepients',component.get('v.selectedRecepients'));
            component.set('v.recepientCount',component.get('v.recepients').length);
        }
    },
    
    sendCommunication : function(component,event,helper){
        //validate if additional Emails are entered	
        var sendEmail = true;	
        if(!$A.util.isEmpty(component.get("v.additionalEmails"))){	
           sendEmail = helper.validateAdditionalEmail(component,event);	
        }	
        console.log('### sendEmail '+sendEmail);
        console.log('### in send Communication'+JSON.stringify(component.get('v.selectedRecepients')));
        var recepientList = (component.get('v.selectedRecepients') != null && component.get('v.selectedRecepients').length > 0) ?
            component.get('v.selectedRecepients') : (component.get("v.recepients") != null && component.get("v.recepients").length > 0) ? 
            component.get("v.recepients") : [];
        if($A.util.isEmpty(recepientList)){	
            console.log('### Error : Please select atleast one recepient');	
        }	
        if(sendEmail){
        var action = component.get('c.sendMessage');
        component.set('v.disableSend',true);
        action.setParams({
            recepientList : recepientList,
            commObj : component.get('v.communicationObject'),
            additionalEmails : component.get('v.additionalEmails')
        });
        action.setCallback(this,function(response){
           var state = response.getState();
            console.log('### state '+state);
            if(state === "SUCCESS"){
                console.log('communication has been sent');
                console.log('response received',response.getReturnValue());
                console.log('response received 1',JSON.stringify(response.getReturnValue()));
                var responseValue = response.getReturnValue();
                console.log('responseValue '+responseValue["Success"]);
                if(responseValue != null && responseValue["Success"] != null){
                    helper.showToast(component,"Success!",responseValue["Success"],"Success");
                }
                else if(responseValue != null && responseValue["Error"] != null){
                    helper.showToast(component,"Error!",responseValue["Error"],"Error");
                }
                else{
                    helper.showToast(component,"Success!","Communication sent successfully","Success");
                }                
                $A.get("e.force:closeQuickAction").fire() ;
                window.location.reload();
                
            }
            else{
                helper.showToast(component,"Error!","Something went wrong. Please contact your system administrator","Error");
                /*var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
        			"title": "Error!",
        			"message": "Internal Error Occured. Please contact system administrator.",
                    "type": "Error"
    			});
    			toastEvent.fire();*/
                $A.get("e.force:closeQuickAction").fire() ;
                window.location.reload();
            }
        });
        $A.enqueueAction(action);
        }
        else{
            console.log('Error in additional email');
        }
    },
    
    cancelSending : function(component,event,helper){        
        $A.get("e.force:closeQuickAction").fire() ;
    },
    
    
})