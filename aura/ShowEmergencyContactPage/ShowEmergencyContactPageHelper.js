({
    showToast : function(component,title,message,type) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message,
            "type": type,
            "mode" : "pester"
        });
        toastEvent.fire();        
    },
    
     validateAdditionalEmail : function(component,event){	
        console.log('### in additional Email');	
        var additionalEmail = component.get('v.additionalEmails');	
        var isValidEmail = true;	
        if(!$A.util.isEmpty(additionalEmail)){	
            var additionalEmailList = additionalEmail.split(';');	
            for(let j=0; j < additionalEmailList.length; j++){	
                const regexExpr = new RegExp("^($|[a-zA-Z0-9_\\.\\+-]+@[a-zA-Z0-9-]+\\.(com|org|net|ae|gov|in|co.in|co.uk|fr|it))$", "i");	
                const regexResult = regexExpr.test(additionalEmailList[j]);	
                if(regexResult.toString() === "false"){	
                    isValidEmail = false;	
                }	
            }	
            console.log('### isValidEmail '+isValidEmail);	
            if(!isValidEmail){	
                var emailField = component.find("emailValues");	
                console.log('### emailField '+emailField);	
                $A.util.addClass(emailField, 'slds-has-error');                	
                emailField.setCustomValidity("Please Enter a Valid Email Addresses");	
                emailField.reportValidity();                	
            }	
            else{	
                var emailField = component.find("emailValues");	
                console.log('### email Field '+emailField);
                $A.util.removeClass(emailField, 'slds-has-error');	
                emailField.setCustomValidity("");	
                emailField.reportValidity(); 	
            }	
        }	
        return isValidEmail;	
    }
})