({
  doInit: function (component, event, helper) {
    try {
      helper.fetchRel(component, event, helper);

      let newURLParam = new URL(window.location.href).searchParams;
      var reload = newURLParam.get("reload");
      let newURL = window.location.href;
      if (newURL.includes("ob-srindex") && !reload) {
        window.location.href = window.location.href + "&reload=false";
      } else if (newURL.includes("ob-srindex")) {
        /*  var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          url: "/",
        });
        urlEvent.fire(); */
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  recordUpdate: function (component, event, helper) {
    var isRefreshCompleate = component.get("v.refreshCompleate");

    if (isRefreshCompleate === false) {
      component.find("recordLoader").reloadRecord(true);
      component.set("v.refreshCompleate", true);
    }

    function refresher() {
      //helper.fetchRel(component, event, helper);
      component.find("recordLoader").reloadRecord(true);
      component.set("v.refreshCompleate", true);
    }

    setTimeout(refresher, 2000);
  },

  RelRecordUpdate: function (component, event, helper) {
    function refresher() {
      component.find("relRecordLoader").reloadRecord(true);
      var isRefreshCompleate = component.get("v.currentRel");
      if (isRefreshCompleate.IsActive == false) {
        helper.navToBLockedPage(component, event, helper);
      }
    }
    setTimeout(refresher, 2000);
  },
  /*Shoaib Added*/
  handleClick: function (component, event, helper) {
    component.set("v.isPopup", "false");
  },
});