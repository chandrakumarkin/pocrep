({
  fetchRel: function (component, event, helper) {
    var action = component.get("c.getRelation");
    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        component.set("v.respWrap", respWrap);
        if (respWrap.relObj && respWrap.relObj.IsActive == false) {
          helper.navToBLockedPage(component, event, helper);
        }
        let newURL = window.location.href;
        if (
          newURL.includes("ob-srindex") &&
          respWrap.loggedInUser &&
          respWrap.loggedInUser.Contact.OB_Block_Homepage__c == false
        ) {
          var urlEvent = $A.get("e.force:navigateToURL");
          urlEvent.setParams({
            url: "/",
          });
          urlEvent.fire();
        }
        if (respWrap.relObj && respWrap.relObj.IsActive == false) {
          helper.navToBLockedPage(component, event, helper);
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },

  navToBLockedPage: function (component, event, helper) {
    let newURL = window.location.href;
    if (!newURL.includes("manage-assosiated-entity")) {
      window.location.href = "manage-assosiated-entity?blocked=true";
    }
  },
});