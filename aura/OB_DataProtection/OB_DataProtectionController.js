({
  doInit: function(component, event, helper) {
    try {
      let newURL = new URL(window.location.href).searchParams;
      var pageId = newURL.get("pageId");
      var flowId = newURL.get("flowId");
      var srIdParam = component.get("v.srId")
        ? component.get("v.srId")
        : newURL.get("srId");
      var action = component.get("c.getDataController");

      component.set("v.srId", srIdParam);
      component.set("v.spinner", true);

      var reqWrapPram = {
        flowId: flowId,
        srId: srIdParam,
        pageID: pageId
      };

      action.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      console.log(
        "@@@@@@@@@@33 reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
      action.setCallback(this, function(response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (component.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();

          //  if(!respWrap.srWrap.isDraft){
          //   var pageURL = respWrap.srWrap.viewSRURL;
          //   window.open(pageURL,"_self");
          // }

          component.set("v.srWrap", respWrap.srWrap);
          component.set("v.dpoAmendWrap", respWrap.dpoAmendWrap);
          component.set("v.commandButton", respWrap.ButtonSection);

          var category = component.get(
            "v.srWrap.srObj.Category_of_Article_3__c"
          );
          category = category ? category : "";
          var option1Value =
            "a change in Processing including the adoption or deploying of new / different IT or methods, which may increase the risk to Data Subjects or render it more difficult for Data Subjects to exercise their rights";
          var option2Value =
            "a considerable amount of Personal Data will be Processed that is likely to result in a high risk to the Data Subject";
          var option3Value =
            "the Processing will involve a systematic and extensive evaluation of personal aspects relating to natural persons, based on automated Processing, including Profiling, and resulting in decisions having a legal effect on or similarly significantly affects the natural person";
          var option4Value =
            "a material amount of Special Categories of Personal Data is to be Processed";

          if (category.includes(option1Value)) {
            component.set("v.option1", true);
          }
          if (category.includes(option2Value)) {
            component.set("v.option2", true);
          }
          if (category.includes(option3Value)) {
            component.set("v.option3", true);
          }
          if (category.includes(option4Value)) {
            component.set("v.option4", true);
          }
          debugger;
          //console.log("######### onload category ", category);
          component.set("v.spinner", false);
        } else {
          component.set("v.spinner", false);

          console.log("@@@@@ Error " + response.getError()[0].message);
          component.set("v.errorMessage", response.getError()[0].message);
          component.set("v.isError", true);
          helper.showToast(component,event,response.getError()[0].message,'error');
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      component.set("v.spinner", false);
      console.log("@@@@@ Error " + err.message);
      component.set("v.errorMessage", err.message);
      component.set("v.isError", true);
      helper.showToast(component,event,err.message,'error');
    }
  },
  onCheckboxChange: function(component, event, helper) {
    var option1 = component.get("v.option1");
    var option2 = component.get("v.option2");
    var option3 = component.get("v.option3");
    var option4 = component.get("v.option4");
    console.log('========option1======'+option1);
    console.log('========option2======'+option2);
    console.log('========option3======'+option3);
    console.log('========option4======'+option4);
    var option1Value =
      "a change in Processing including the adoption or deploying of new / different IT or methods, which may increase the risk to Data Subjects or render it more difficult for Data Subjects to exercise their rights";
    var option2Value =
      "a considerable amount of Personal Data will be Processed that is likely to result in a high risk to the Data Subject";
    var option3Value =
      "the Processing will involve a systematic and extensive evaluation of personal aspects relating to natural persons, based on automated Processing, including Profiling, and resulting in decisions having a legal effect on or similarly significantly affects the natural person";
    var option4Value =
      "a material amount of Special Categories of Personal Data is to be Processed";
    var category = component.get("v.srWrap.srObj.Category_of_Article_3__c");
    
    if(!category){
      category = '';
    }
    console.log('========category======'+category);

    if (option1 && !category.includes(option1Value)) {
      console.log('=====inside===option1======');
      category = category + option1Value + ";";
    } else if (!option1) {
      category = category.replace(option1Value, "");
    }

    if (option2 && !category.includes(option2Value)) {
      category = category + option2Value + ";";
    } else if (!option2) {
      category = category.replace(option2Value, "");
    }
    if (option3 && !category.includes(option3Value)) {
      category = category + option3Value + ";";
    } else if (!option3) {
      category = category.replace(option3Value, "");
    }

    if (option4 && !category.includes(option4Value)) {
      category = category + option4Value + ";";
    } else if (!option4) {
      category = category.replace(option4Value, "");
    }
    console.log("====category====" + category);
    component.set("v.srWrap.srObj.Category_of_Article_3__c", category);
  },
  onChange: function(component, event, helper) {
    try {
      //component.set("v.spinner", true);
      //var purposeValue = event.getSource().get("v.value");
      var processingValue = component.get(
        "v.srWrap.srObj.Purpose_of_Processing_Data__c"
      );
      var personalValue = component.get(
        "v.srWrap.srObj.Data_Subjects_Personal_Data__c"
      );

      console.log("===on change==purposeValue====" + processingValue);
      console.log("===on change==purposeValue====" + personalValue);

      // processing
      if (
        !$A.util.isEmpty(processingValue) &&
        !$A.util.isUndefined(processingValue) &&
        processingValue.includes("Other, Please Specify")
      ) {
        component.set("v.isOtherValueProcessingData", true);
      } else {
        component.set("v.isOtherValueProcessingData", false);
      }
      // personal
      if (
        !$A.util.isEmpty(personalValue) &&
        !$A.util.isUndefined(personalValue) &&
        personalValue.includes("Other, Please Specify")
      ) {
        component.set("v.isOtherValuePersonalData", true);
      } else {
        component.set("v.isOtherValuePersonalData", false);
      }
    } catch (err) {
      component.set("v.spinner", false);
      console.log("@@@@@ Error " + err.message);
      component.set("v.errorMessage", err.message);
      component.set("v.isError", true);
      helper.showToast(component,event,err.message,'error');
    }
  },
  handleRefreshEvnt: function(cmp, event, helper) {
    var isViewRemove = event.getParam("isViewRemove");

    console.log("=====isViewRemove======" + isViewRemove);
    if (isViewRemove == "view") {
      var dataProtectionId = event.getParam("dataProtectionId");
      console.log("====dataProtectionId=======" + dataProtectionId);
      cmp.set("v.dataProtectionId", dataProtectionId);
      // if view existing record then close the new one if open
      cmp.set("v.newJurisdictionWrap", null);
    }
    if (isViewRemove == "remove") {
      var srWrap = event.getParam("srWrap");
      cmp.set("v.srWrap.jurisdictionList", srWrap.jurisdictionList);
    }
    if (isViewRemove == "New") {
      var newJurisdictionWrap = event.getParam("newJurisdictionWrap");
      cmp.set("v.newJurisdictionWrap", newJurisdictionWrap);
      cmp.set("v.dataProtectionId", "");
    }
    if (isViewRemove == "insert") {
      var srWrap = event.getParam("srWrap");
      cmp.set("v.srWrap.jurisdictionList", srWrap.jurisdictionList);
      cmp.set("v.dataProtectionId", null);
      cmp.set("v.newJurisdictionWrap", null);
    }
    if (isViewRemove == "close") {
      cmp.set("v.dataProtectionId", null);
      cmp.set("v.newJurisdictionWrap", null);
    }
    if (isViewRemove == "errorMessage") {
    	cmp.set("v.isError", true);
    }
  },
  handleButtonAct: function(cmp, event, helper) {
      console.log("==handleButtonAct===");
      var allValid = true;
      cmp.set("v.errorMessage", "");
      cmp.set("v.isError", false);
      cmp.set("v.spinner", true);
      var childComponent = cmp.find("childQuestion");
        	childComponent.getValidateMethod();
      
      var change = cmp.get("v.isError");
      console.log('===change======'+change);
      if(change == true){
    	  allValid = false;
    	  cmp.set("v.spinner", false);
      }
      let fields = cmp.find("dataProtectionkey");
      for (var i = 0; i < fields.length; i++) {
          var inputFieldCompValue = fields[i].get("v.value");
          console.log(fields[i].get("v.fieldName"));
          console.log(fields[i].get("v.required"));
          //console.log(inputFieldCompValue);
          if(!inputFieldCompValue && fields[i] && fields[i].get("v.required")){
              console.log('no value');
              //console.log('bad');
              fields[i].reportValidity();
              allValid = false;
          }   
      }
      if (!allValid) {
          console.log("Error MSG");
          cmp.set("v.spinner", false);
          cmp.set("v.errorMessage", "Please fill required field");
          cmp.set("v.isError", true);
          helper.showToast(cmp,event,'Please fill required fields','error');
      } else {
          try {
              helper.onAmedSave(cmp, event,helper);
              
              var isError = cmp.get("v.isError");
              
              console.log("parent==isError==="+isError);
              console.log("buttttton-parent--");
              var buttonId = event.target.id;
              console.log(buttonId);
              
              if (isError != true) {
                console.log("buttttton-parent no error--");
              }
          } catch (err) {
        	  cmp.set("v.spinner", false);
        	  cmp.set("v.isError", true);
              console.log(err.message);
          }
      }
  },
  closeModel: function(cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  }
});