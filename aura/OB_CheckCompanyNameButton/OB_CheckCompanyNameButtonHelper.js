({
  checkTradeName: function(component, event, helper) {
    component.set("v.showSpinner", true);
    var action = component.get("c.checkName");
    var requestWrap = {
      companyNameID: component.get("v.recordId")
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        component.set("v.showSpinner", false);
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        //component.set("v.respWrap", respWrap);
        if (respWrap.errormessage) {
          helper.showToast(
            component,
            event,
            helper,
            respWrap.errormessage,
            "error"
          );
          $A.get("e.force:closeQuickAction").fire();
        } else {
          helper.showToast(
            component,
            event,
            helper,
            "Check Complete",
            "success"
          );
          $A.get("e.force:refreshView").fire();
          $A.get("e.force:closeQuickAction").fire();
        }
      } else {
        component.set("v.showSpinner", false);
        helper.showToast(
          component,
          event,
          helper,
          response.getError()[0].message,
          "error"
        );
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
        $A.get("e.force:closeQuickAction").fire();
      }
    });
    $A.enqueueAction(action);
  },

  showToast: function(component, event, helper, msg, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissible",
      message: msg,
      type: type
    });
    toastEvent.fire();
  }
});