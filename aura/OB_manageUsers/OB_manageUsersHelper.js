({
  GetUserData: function (component, event, helper) {
    var action = component.get("c.fetchUserData");
    var requestWrap = {};
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        if (!respWrap.isSuperUser) {
          var urlEvent = $A.get("e.force:navigateToURL");
          urlEvent.setParams({
            url: "/"
          });
          urlEvent.fire();
          return;
        }
        helper.SetRoleFilterOptions(component, event, helper, respWrap);
        helper.splitRoles(component, event, helper, respWrap);
        helper.hasPromoteSr(component, event, helper, respWrap);
        console.log(respWrap);
        component.set("v.respWrap", respWrap);
        component.set("v.unfilteredUserList", respWrap.relatedUserList);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },

  SetRoleFilterOptions: function (component, event, helper, respWrap) {
    try {
      respWrap.userComunityRolesValues = [];
      for (var rel of respWrap.relatedUserList) {
        if (rel.Roles) {
          var roleList = rel.Roles.split(";");
          for (var role of roleList) {
            if (!respWrap.userComunityRolesValues.includes(role)) {
              respWrap.userComunityRolesValues.push(role);
            }
          }
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  splitRoles: function (component, event, helper, respWrap) {
    //add is super user
    try {
      for (var rel of respWrap.relatedUserList) {
        if (rel.Roles) {
          var roleList = rel.Roles.split(";");
          rel.RolesListView = [];
          for (var role of roleList) {
            rel.RolesListView.push(role);
          }
          if (rel.RolesListView.includes("Super User")) {
            rel.isSuperUser = "true";
          } else {
            rel.isSuperUser = "false";
          }
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  hasPromoteSr: function (component, event, helper, respWrap) {
    try {
      for (var rel of respWrap.relatedUserList) {
        let relSr = respWrap.relatedPromoteSr.find(
          (sr) => sr.HexaBPM__Contact__c === rel.ContactId
        );
        if (relSr) {
          rel.hasPromoteSr = true;
          rel.PromoteSr = relSr.Id;
        }
        /* if (
          respWrap.relatedPromoteSr.some(
            sr => sr.HexaBPM__Contact__c === rel.ContactId
          )
        ) {
          rel.hasPromoteSr = true;
        } */
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  promoteUserHelper: function (component, event, helper, message, type) {
    try {
      component.set("v.serverCallInProgress", true);

      /* var promoteSrID = event.currentTarget.dataset.promotesr;
      var promoteURL = component.get("v.respWrap.promoteNavUrl");
      if (promoteSrID) {
        window.location.href =
          component.get("v.respWrap.promoteNavUrl") + "&srId=" + promoteSrID;

        return;
      }
 */
      var contactID = event.currentTarget.dataset.contactid;
      var Roles = event.currentTarget.dataset.role;
      var mode = event.currentTarget.dataset.mode;
      if (mode == "Promote") {
        if (Roles && Roles.indexOf("Super User") != -1) {
          helper.showToast(
            component,
            event,
            helper,
            "Seleted user already has a super user access",
            "error"
          );
          component.set("v.serverCallInProgress", false);
          return;
        }

        Roles =
          Roles == null
            ? "Super User"
            : Roles.indexOf("Super User") == -1
            ? Roles + ";Super User"
            : Roles;
      }
      console.log(Roles);

      var user = component.get("v.respWrap.loggedInUser");
      console.log(user);
      var action = component.get("c.createPromoteUserSr");
      var requestWrap = {
        accId: component.get("v.respWrap.accId"),
        contactID: contactID,
        role: Roles,
        loggedInUser: user,
        mode: mode
      };

      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          window.location.href =
            respWrap.promoteNavUrl + "&srId=" + respWrap.insertedSr.Id;
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }

        component.set("v.serverCallInProgress", false);
      });

      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
    }
  },

  showToast: function (component, event, helper, message, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissable",
      message: message,
      type: type,
      duration: 500
    });
    toastEvent.fire();
  },

  openModal: function (component, event, helper) {
    var dataSet = event.currentTarget.dataset;
    console.log(dataSet);

    $A.createComponent(
      "c:OB_AddNewUser",
      {
        modalHeader: dataSet.modalheader,
        mode: dataSet.mode,
        relToUpdate: dataSet.relid,
        accId: component.get("v.respWrap.accId")
      },
      function (modal) {
        if (component.isValid()) {
          var targetCmp = component.find("ModalDialogPlaceholder");
          var body = targetCmp.get("v.body");
          body.push(modal);
          targetCmp.set("v.body", body);
        }
      }
    );
  }
});