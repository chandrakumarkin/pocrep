({
	doInit : function(cmp, event, helper) 
    {
		try
        {
           
            var getFormAction = cmp.get('c.getExistingAmendment');
            let newURL = new URL(window.location.href).searchParams;
            var srIdParam = (newURL.get('srId')? newURL.get('srId') : cmp.get('v.srId')); 
            var pageId = (newURL.get('pageId') ? newURL.get('pageId') : cmp.get('v.pageId') );
            cmp.set('v.srId',srIdParam); 
            console.log('in init -->' + srIdParam);
            
            var reqWrapPram  =	{
                                    srId: srIdParam,
                					pageId:pageId
                                }
            
            getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
            console.log('@@@@@@@@@@33121 reqWrapPram init '+JSON.stringify(reqWrapPram));
            getFormAction.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                              cmp.set('v.srWrap',respWrap.srWrap);
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                              
                                          }
                                      }
                                     );
            $A.enqueueAction(getFormAction);
        }catch(err){
            console.log('=error==='+err.message);
        }
	},
    handleRefreshEvnt: function(cmp, event, helper)
    {
        var amedId = event.getParam("amedId");
		console.log('$$$$$$$$$$$ refresh ',amedId);
        cmp.set('v.amedId',amedId);
        cmp.set("v.amedSelWrap",[]);
       
       	$A.enqueueAction(cmp.get('c.doInit'));
        
    },
    onChange: function (cmp, evt, helper) 
    {
		   var amedIdParam = cmp.find('select').get('v.value');
           var amedShrHldrWrapLst = cmp.get('v.srWrap').amedShrHldrWrapLst;
           var amedSelWrap;
            
          
           for(let i = 0; i < amedShrHldrWrapLst.length; i++)
           {
    		    var amedWrap = amedShrHldrWrapLst[i];
                if(amedWrap.amedObj.Id == amedIdParam )
                {
                    amedSelWrap = amedWrap;
                    break;
                }
              
            }
            console.log('########### amedSelWrap ',amedSelWrap);
            if(amedSelWrap)
            {
                
                cmp.set("v.showButton",true);
                cmp.set("v.amedSelWrap",amedSelWrap);
            }
			
            
    },
  
   
    
    
    
})