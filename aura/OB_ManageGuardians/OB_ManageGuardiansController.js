({
  doInit: function (component, event, helper) {
    try {
      var getFormAction = component.get("c.getExistingAmendment");

      let newURL = new URL(window.location.href).searchParams;
      var pageId = newURL.get("pageId");
      var flowId = newURL.get("flowId");
      var srIdParam = component.get("v.srId")
        ? component.get("v.srId")
        : newURL.get("srId");
      component.set("v.srId", srIdParam);
      var reqWrapPram = {
        flowId: flowId,
        srId: srIdParam,
        pageID: pageId,
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram),
      });

      console.log(
        "############## reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
      getFormAction.setCallback(this, function (response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (component.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          //   if(!respWrap.srWrap.isDraft){
          // 	  var pageURL = respWrap.srWrap.viewSRURL;
          // 	  window.open(pageURL,"_self");
          //   }
          console.log(respWrap);
          console.log(respWrap.ButtonSection);
          component.set("v.commandButton", respWrap.ButtonSection);
          component.set("v.srWrap", respWrap.srWrap);

          console.log(
            "#########123456 respWrap ",
            component.get("v.srWrap.shareholderAmedWrapLst").length
          );
          component.set("v.spinner", false);
        } else {
          component.set("v.spinner", false);
          console.log("@@@@@ Error " + response.getError()[0].message);
          component.set("v.errorMessage", response.getError()[0].message);
          component.set("v.isError", true);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
      component.set("v.spinner", false);
    }
  },
  handleRefreshEvnt: function (component, event, helper) {
    var amedId = event.getParam("amedId");
    var srWrap = event.getParam("srWrap");
    var isNewAmendment = event.getParam("isNewAmendment");
    var amedWrap = event.getParam("amedWrap");

    //console.log('$$$$$$$$$$$ handleRefreshEvnt== ',JSON.stringify(srWrap));
    //console.log('2222222222 handleRefreshEvnt== ',amedWrap);

    component.set("v.amedId", amedId);
    component.set("v.selectedValue", "");
    component.set("v.addPerson", false);
    component.set("v.AssignButton", false);
    component.set("v.uboamedWrap", null);

    /* if(amedId != null && amedId != '') {
           component.set('v.isNewPanelShow',false);
       } else {
           component.set('v.isNewPanelShow',true);
       }*/

    if (!$A.util.isEmpty(srWrap)) {
      console.log("====sr wrap assign===");
      component.set("v.srWrap", srWrap);
    }
    if (isNewAmendment == true) {
      console.log("====isNew===");
      component.set("v.amedWrap", amedWrap);
    }
    if (isNewAmendment == false) {
      console.log("==not==isNew===");
      component.set("v.amedWrap", amedWrap);
    }
  },
  closeModel: function (component, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    component.set("v.errorMessage", "");
    component.set("v.isError", false);
  },
  handleButtonAct: function (component, event, helper) {
    //helper.handleButtonActHelper(component, event, helper);
    console.log("buttttton");
    var buttonId = event.target.id;
    console.log(buttonId);

    try {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");
      var buttonId = event.target.id;

      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId,
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          console.log("button succsess");

          console.log(response.getReturnValue());
          window.open(response.getReturnValue().pageActionName, "_self");
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },

  handleComponentEventClear: function (cmp, evt, helper) {
    cmp.set("v.AssignButton", false);
    cmp.set("v.selectedValue", "");
    cmp.set("v.amedSelWrap", null);
  },

  handleComponentEvent: function (cmp, event, helper) {
    try {
      // get the selected License record from the COMPONETNT event
      var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
      console.log("selected in grandparent");
      console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));

      //component.set("v.selectedRecord", selectedLicenseGetFromEvent);

      cmp.set("v.amedSelWrap", selectedLicenseGetFromEvent);
      cmp.set("v.selectedValue", selectedLicenseGetFromEvent.displayName);
      //cmp.set("v.amedIdInFocus", "none");
      cmp.set("v.AssignButton", true);
    } catch (error) {
      console.log(error.message);
    }
  },

  onChange: function (cmp, evt, helper) {
    cmp.set("v.spinner", true);
    console.log("-convert UBO--onChange-" + cmp.find("select").get("v.value"));
    //cmp.set("v.selectedValue", cmp.find("select").get("v.value"));
    var selectedVal = cmp.find("select").get("v.value");
    console.log("====selectedVal==" + selectedVal);
    if (!$A.util.isEmpty(selectedVal)) {
      cmp.set("v.AssignButton", true);
    } else {
      cmp.set("v.AssignButton", false);
    }
    cmp.set("v.amedId", "");
    cmp.set("v.amedWrap", null);
    cmp.set("v.spinner", false);
  },
  convertToGuardian: function (cmp, evt, helper) {
    console.log("-convert UBO---");
    //cmp.set("v.spinner", true);
    //var index = cmp.get("v.selectedValue");

    //var shareholderAmedWrapLst = cmp.get("v.srWrap.shareholderAmedWrapLst");
    /*
      for (var i = 0; i < shareholderAmedWrapLst.length; i++) 
      {
        if (shareholderAmedWrapLst[i].amedObj.Id == amendmentID) 
        {
          var SIWraps = shareholderAmedWrapLst[i];
          cmp.set("v.amedWrap", null);
          cmp.set("v.uboamedWrap", SIWraps);
          if (          cmp.get("v.uboamedWrap").amedObj.RecordType.DeveloperName =="Individual") 
          {
            cmp.set("v.isIndividual", true);
            cmp.set("v.isCorporate", false);
          } 
          else 
          {
            cmp.set("v.isIndividual", false);
            cmp.set("v.isCorporate", true);
          }


          cmp.set("v.addPerson", true);
          break;
        }
      }
    */

    //var SIWraps = shareholderAmedWrapLst[index];
    var SIWraps = cmp.get("v.amedSelWrap");
    cmp.set("v.amedWrap", null);
    cmp.set("v.uboamedWrap", SIWraps);

    //if (          cmp.get("v.uboamedWrap").amedObj.RecordType.DeveloperName =="Individual")
    if (SIWraps.isIndividual) {
      cmp.set("v.isIndividual", true);
      cmp.set("v.isCorporate", false);
    } else {
      cmp.set("v.isIndividual", false);
      cmp.set("v.isCorporate", true);
    }

    cmp.set("v.addPerson", true);

    console.log("======" + JSON.stringify(cmp.get("v.uboamedWrap")));

    // cmp.set("v.spinner", false);
  },
});