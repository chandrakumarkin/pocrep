({
	viewDetails : function(cmp, event, helper) 
    {
		//throw event.
		var refeshEvnt = cmp.getEvent("refeshEvnt");
        var amedId = cmp.get("v.amedWrap").amedObj.Id ;
        
        if(cmp.get("v.amedWrap").amedObj.RecordType.DeveloperName == 'Individual') {
            cmp.set("v.isIndividual", true);
            cmp.set("v.isCorporate", false);
            
        } else {
            cmp.set("v.isCorporate", true);
            cmp.set("v.isIndividual", false);
             
        }
        
        console.log('@@@@@ amedId ',amedId);
        refeshEvnt.setParams({
            "amedId" : amedId,
            "isNewAmendment" : false,
            "amedWrap":null});
        cmp.set('v.spinner',false);
        refeshEvnt.fire();
	},
    remove : function(component, event, helper) 
    {
		//remove 
		var amedId = component.get("v.amedWrap").amedObj.Id ;
        var srId = component.get("v.srId");
        let newURL = new URL(window.location.href).searchParams;
        var srIdParam =  (  component.get('v.srId') ?  component.get('v.srId') : newURL.get('srId'));
        var getFormAction = component.get("c.removeAmendment");
        var reqWrapPram  =
            {
                srId: srIdParam,
                amendmentID:amedId,
                amedWrap :component.get("v.amedWrap")
            }
            
            getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
            console.log('@@@@@@@@@@33 reqWrapPram init '+JSON.stringify(reqWrapPram));
            getFormAction.setCallback(this, 
            	function(response) {
                	var state = response.getState();
					console.log("callback state: " + state);
                    if (component.isValid() && state === "SUCCESS") 
                    {
                        var respWrap = response.getReturnValue();
                        console.log('===respWrap========'+respWrap);
                        if(!$A.util.isEmpty(respWrap.errorMessage)){
                            
                            component.set("v.errorMessage",respWrap.errorMessage);
                        	component.set("v.isError",true);
                        }else{
                            //this.showToast("Success",'This is removed');
                            //throw event.
                            var refeshEvnt = component.getEvent("refeshEvnt");
                            var srWrap = respWrap.srWrap;
                            console.log('@@@@@ srWrap ',srWrap);
                            refeshEvnt.setParams({
                                "srWrap" : srWrap });
                            refeshEvnt.fire();
                        }
                                              
                    }else{
                        console.log('@@@@@ Error '+response.getError()[0].message);
                                              
                    }
                }
			);
            $A.enqueueAction(getFormAction);
        
	},
    showToast : function(title,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message
        });
        toastEvent.fire();
	},
    closeModel : function(component, event, helper) {
        //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        console.log('--toast close--');
        component.set("v.errorMessage","");
        component.set("v.isError",false);
    },
})