({
    
    /* handleLogin: function (username,password) {
        var username = username;
        var password = password;
        var action = component.get("c.login");
        var startUrl = component.get("v.startUrl");
        
        startUrl = decodeURIComponent(startUrl);
        
        action.setParams({
            username: username,
            password: password,
            startUrl: startUrl
        });
        action.setCallback(this, function (a) {
                               var rtnValue = a.getReturnValue();
                               if (rtnValue !==null) {
                                   component.set("v.errorMessage", rtnValue);
                                   component.set("v.showError", true);
                               }
                           });
        $A.enqueueAction(action);
    },
    
    */
    handleLogin: function(component,event,helper){
        let validRecord = true;
        var user = component.get("v.Username");
        var Pass = component.get("v.Password");
        var username = component.find('username');
        var uname = username.get('v.value');
        if ($A.util.isEmpty(uname)){
            validRecord = false;
            username.set('v.errors', [{message:'Please enter Username'}]);
            
        }
        else {
            username.set('v.errors', null);
        }
        var password = component.find('Password');
        var pass = password.get('v.value');
        if ($A.util.isEmpty(pass)){
            validRecord = false;
            password.set('v.errors', [{message:'Please enter Password'}]);
            
        }
        else {
            password.set('v.errors', null);
        }
        
        var action = component.get("c.checkPortal");
        action.setParams({
            username: user,
            password: Pass
        });
        console.log('===get input=====');
        action.setCallback(this,function (response) {
            var state =response.getState();
            console.log(state);
            var rtnValue =response.getReturnValue();
            console.log(rtnValue);
            if (rtnValue !=null && state==='SUCCESS') {
                console.log('@@@@@@rtnValue',rtnValue);
            }
            if (rtnValue == "Your login attempt has failed. Make sure the username and password are correct.") { 
                 component.set("v.errorMessage",rtnValue);
                component.set("v.isError",true);
                window.setTimeout( $A.getCallback(function(){  
                                           
                                component.set("v.isError",false);
                                             
                                          }), 3000 );
                
                
                //helper.createToast(component,event,rtnValue);
               /* var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    title:"title",
                    message:"rtnValue",
                    mode: "sticky",
                    type: "error"
                });
                toastEvent.fire();
                */
                
            }
            
            else{
                console.log("@@@@@ Error " + response.getError()[0].message);
            }
        });
        $A.enqueueAction(action);
        
    },
    createToast: function(component,event, errorMessagr)
    {
       
         var toastEvent = $A.get("e.force:showToast");
      toastEvent.setParams({
        mode: "dismissable",
        message: errorMessagr,
        type: "error"
       
      });
      toastEvent.fire();
        
        
    },
    
    
})