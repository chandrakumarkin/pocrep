({
    doInit: function (component, event, helper) {
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get("srId");
        var action = component.get("c.fetchSRStatus");
        
        action.setParams({
            srId: srId
        });
        
        action.setCallback(this, function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                console.log(response.getReturnValue());
                var res = response.getReturnValue();
                component.set("v.returnForEdit", res);
                //component.set("v.authorizeSign", res.Authorized_Signatory_Combinations__c);
                
            } else {
                console.log("@@@@@ Error " + response.getError()[0].message);
                console.log("@@@@@ Error " + response.getError()[0].stackTrace);
            }
        });
        $A.enqueueAction(action);
        helper.getExistingAmend(component, event, helper);
    },

  handleComponentEvent: function (cmp, event, helper) {
    try {
      // get the selected License record from the COMPONETNT event
      var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
      console.log("selected in grandparent");
      console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));

      //component.set("v.selectedRecord", selectedLicenseGetFromEvent);

      cmp.set("v.selectedAmend", selectedLicenseGetFromEvent.amedObj);
      cmp.set("v.selectedAmendWrap", selectedLicenseGetFromEvent);

      cmp.set("v.selectedValue", selectedLicenseGetFromEvent.displayName);
      cmp.set("v.amedIdInFocus", "none");
    } catch (error) {
      console.log(error.message);
    }
  },

  handleComponentEventClear: function (cmp, evt, helper) {
    cmp.set("v.selectedValue", "");
    cmp.set("v.amedIdInFocus", "none");
  },

  onChange: function (cmp, evt, helper) {
    console.log("onchange of the picklicst");

    //set selectedValue to the id of the selected amendobj from dropdown
    var otherList = cmp.get("v.AmendListWrapp.otherRolesAmendList");
    var index = cmp.find("select").get("v.value");
    console.log(index);

    var selectedAmend = otherList.find((amend) => amend.Id === index);
    console.log(selectedAmend);
    cmp.set("v.selectedAmend", selectedAmend);
    cmp.set("v.amedIdInFocus", "none");
    //set selectedValue to the id of the selected amendobj from dropdown
    /* var otherList = cmp.get("v.AmendListWrapp.otherRolesAmendList");
    var index = cmp.find("select").get("v.value");
    var selectedAmend = otherList[index];
    cmp.set("v.selectedAmend", selectedAmend);
    console.log(selectedAmend);

    if (selectedAmend != undefined) {
      cmp.set("v.selectedValue", selectedAmend.Id);
    } else {
      cmp.set("v.selectedValue", "");
    }
    console.log(cmp.get("v.selectedValue"));
    cmp.set("v.amedIdInFocus", "none"); */
  },

  showConfirmForm: function (cmp, evt, helper) {
    var selectedAmend = cmp.get("v.selectedAmendWrap");

    cmp.set("v.amedIdInFocus", selectedAmend.displayName);
  },

  createNewAmdRec: function (component, event, helper) {
    console.log("show create new amend rec");

    try {
      var action = component.get("c.initNewAmendment");

      var requestWrap = {
        srId: component.get("v.srId"),
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(response.getReturnValue());
          component.set("v.newAmendWrap", respWrap.newAmendWrappToCreate);
          component.set("v.amedIdInFocus", "newRec");
          component.set("v.ShowNewForm", true);
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
        }
      });
      $A.enqueueAction(action);
    } catch (err) {
      console.log(err.message);
    }
  },

    handleButtonAct: function (component, event, helper) {
        try {
            if (helper.jointAuthorityValidation(component, event, helper) == false) {
                helper.showToast(
                    component,
                    event,
                    helper,
                    "Please add at least two authorised signatory",
                    "error"
                );
                return;
            }
            var amendCheck = true;
            var amendList = component.get("v.respWrap");
            console.log('amendList: '+amendList);
            if(amendList.amendWrappList.AddedAmendList.length > 2){
                console.log('field: '+component.get("v.authorizeSign"));                
                if(component.get("v.authorizeSign") == undefined || 
                   component.get("v.authorizeSign") == '' || 
                   component.get("v.authorizeSign").trim().length == 0 )
                {
                    amendCheck = false;
                }
                else{
                    amendCheck = true;
                }
            }
            console.log('amendCheck: '+amendCheck);
            if(amendCheck){
                let newURL = new URL(window.location.href).searchParams;
                
                var srID = newURL.get("srId");
                var pageID = newURL.get("pageId");
                
                var buttonId = event.target.id;
                console.log(buttonId);
                var action = component.get("c.getButtonAction");
                
                action.setParams({
                    SRID: srID,
                    pageId: pageID,
                    ButtonId: buttonId,
                });
                
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log("button succsess");
                        
                        console.log(response.getReturnValue());
                        window.open(response.getReturnValue().pageActionName, "_self");
                    } else {
                        console.log("@@@@@ Error " + response.getError()[0].message);
                    }
                });
                $A.enqueueAction(action);
                if(component.get("v.authorizeSign") != '' && component.get("v.authorizeSign") != null && component.get("v.authorizeSign") != undefined){
                    var updateSR = component.get("c.updateSR");
                    updateSR.setParams({
                        SRID: srID,
                        authorizeSignatories: component.get("v.authorizeSign")
                    });
                    
                    updateSR.setCallback(this, function (response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            console.log("button succsess");
                            
                            console.log(response.getReturnValue());
                            window.open(response.getReturnValue().pageActionName, "_self");
                        } else {
                            console.log("@@@@@ Error " + response.getError()[0].message);
                        }
                    });
                    $A.enqueueAction(updateSR);
                }
            }
            else{
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    mode: "dismissible",
                    message: 'Please fill Authorized Signatory field.',
                    type: "error",
                });
                toastEvent.fire();
            }
        } catch (err) {
            console.log(err.message);
        }
    },
    
});