({
  getExistingAmend: function (component, event, helper) {
    let newURL = new URL(window.location.href).searchParams;
    var flowId = newURL.get("flowId");
    var pageId = newURL.get("pageId");
    var srId = newURL.get("srId");

    component.set("v.flowId", flowId);
    component.set("v.pageId", pageId);
    component.set("v.srId", srId);
    var action = component.get("c.fetchAmendRec");
    var requestWrap = {
      srId: srId,
      pageId: pageId,
      flowId: flowId,
    };
    action.setParams({
      requestWrapParam: JSON.stringify(requestWrap),
    });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        var respWrap = response.getReturnValue();
        // if (!respWrap.srWrap.isDraft) {
        //   var pageURL = respWrap.srWrap.viewSRURL;
        //   window.open(pageURL, "_self");
        // }
        component.set("v.respWrap", response.getReturnValue());
        console.log(component.get("v.respWrap.srObj.Id"));
		component.set("v.authorizeSign", response.getReturnValue().srObj.Authorized_Signatory_Combinations__c);
        //component.set("v.AmendListWrapp", response.getReturnValue().amendWrapp);
        component.set("v.respWrap", response.getReturnValue());
        component.set(
          "v.commandButton",
          response.getReturnValue().ButtonSection
        );
        component.set("v.recTypeId", response.getReturnValue().recordId);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log("@@@@@ Error " + response.getError()[0].stackTrace);
      }
    });
    $A.enqueueAction(action);
  },

  jointAuthorityValidation: function (component, event, helper) {
    var amendWrap = component.get("v.respWrap.amendWrappList.AddedAmendList");
    console.log(amendWrap);
    var addedAmendList = amendWrap;
    console.log(addedAmendList);
    var HasJointAutohrity = false;
    for (var amend of addedAmendList) {
      console.log(amend);
      if (amend.amedObj.Type_of_Authority__c == "Jointly") {
        HasJointAutohrity = true;
        break;
      }
    }
    if (
      HasJointAutohrity == true &&
      addedAmendList &&
      addedAmendList.length == 1
    ) {
      return false;
    } else {
      return true;
    }
  },
  showToast: function (component, event, helper, msg, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissible",
      message: msg,
      type: type,
    });
    toastEvent.fire();
  },
});