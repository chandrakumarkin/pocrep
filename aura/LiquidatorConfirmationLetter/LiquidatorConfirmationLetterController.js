({
    navigateToFlow : function(component, event, helper) {
        component.set("v.showSpinner", true);
        console.log('### navigateToFlow ');
        var action = component.get('c.launchFlowLiquidator');
        console.log('### action '+action);
        action.setCallback(this,function(response){
            var state = response.getState();
            console.log('### state '+state);
            if(state === 'SUCCESS'){
                var responseValue = response.getReturnValue();
                console.log('### responseValue '+JSON.stringify(responseValue));
                if($A.util.isEmpty(responseValue.errorMessage)){
                    //component.set('v.pageFlowURL',responseValue.pageFlowURL);
                    var url = "ob-processflow" + responseValue.pageFlowURL;
                    console.log('### url '+url);
                    window.location.href = url;
                }      
                else {
                    helper.showToast(
                        component,
                        event,
                        helper,
                        respWrap.errorMessage,
                        "error"
                    );
                }
                component.set("v.showSpinner", false);
            }
        });
        $A.enqueueAction(action);
    }
})