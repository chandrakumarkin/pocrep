({
    doInit: function(component, event, helper) {
      var action = component.get("c.voidEnvelope");
      var requestWrap = {
        evelopeId: component.get("v.recordId")
      };
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });
  
      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          if (respWrap.errorMessage) {
            helper.showToast(
              component,
              event,
              helper,
              respWrap.errorMessage,
              "error"
            );
            $A.get("e.force:closeQuickAction").fire();
          } else {
            helper.showToast(
              component,
              event,
              helper,
              "Envelope successfully voided",
              "success"
            );
            $A.get("e.force:closeQuickAction").fire();
          }
        } else {
          helper.showToast(
            component,
            event,
            helper,
            response.getError()[0].message,
            "error"
          );
  
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
          $A.get("e.force:closeQuickAction").fire();
        }
      });
      $A.enqueueAction(action);
    }
  });