({
	initAmendment : function(cmp, event, helper)
    {
       //console.log('######### doInit of amend new'); 
       //cmp.set('v.amedWrap',"{ 'amedObj'  : {'sObjectType': 'HexaBPM_Amendment__c'}}"); 
       //{!v.srWrap.srObj.Id}
       // cmp.set('v.spinner',true);
        var index = event.target.dataset.amedtype;
        var initAmendment = cmp.get('c.initAmendmentDB');
        cmp.set('v.spinner',true);
        cmp.set('v.recordtypename',index);
        
        
        var reqWrapPram  =
            {
                srId : cmp.get('v.srWrap').srObj.Id,
                recordtypename: index
            };
        
        initAmendment.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
        
        initAmendment.setCallback(this, 
                                      function(response) {
                                          var state = response.getState();
                                          console.log("callback state:new ubo " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              cmp.set('v.spinner',false);    
                                              var respWrap = response.getReturnValue();
                                              console.log('######### new uBO respWrap.amedWrap  ',respWrap.amedWrap);
											  //cmp.set('v.amedWrap',respWrap.amedWrap);	
											  //throw event. 
											                                              
											  var refeshEvnt = cmp.getEvent("refeshEvnt");
                                               console.log('@@@@@ event refeshEvnt ');
                                               refeshEvnt.setParams({
                                                    "isNewAmendment" : true,
                                                   "amedWrap":respWrap.amedWrap});
                                               
                                               refeshEvnt.fire();                                              
                                             
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error '+response.getError()[0].message);
                                              cmp.set('v.spinner',false);
                                          }
                                      }
                                     );
            $A.enqueueAction(initAmendment);
       
    },
})