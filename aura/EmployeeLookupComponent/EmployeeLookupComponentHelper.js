({
	searchHelper : function(component,event,getInputkeyWord) {
	  // call the apex class method 
    var action; 
    if(component.get("v.objectAPIName") == 'Relationship__c' ){
      action = component.get("c.getcontacts");
          action.setParams({
            'searchKeyWord' : getInputkeyWord,
            'relationshipType' : component.get("v.reType"),
            'selectedType' : component.get("v.serviceTypeVal")
          });
    }
    else{
      action = component.get("c.getJobTitle");
        action.setParams({
            'searchKeyWord' : getInputkeyWord,
          });
    } 
      // set param to method  
      
      // set a callBack    
        action.setCallback(this, function(response) {
          $A.util.removeClass(component.find("mySpinner"), "slds-show");
            var state = response.getState();
            if (state === "SUCCESS") {
               // alert(JSON.stringify(response.getReturnValue()));
                var storeResponse = response.getReturnValue();
              // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
                console.log('^^^^^^^contacts list^^^^^^^^^'+storeResponse);  
                if (storeResponse.length == 0) {
                    component.set("v.Message", 'No Result Found...');
                } else {
                    component.set("v.Message", '');
                }
                // set searchResult list with return value from server.
                component.set("v.listOfSearchRecords", storeResponse);
            }
            else{
               // alert(response.getReturnValue());
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);
    
	},
})