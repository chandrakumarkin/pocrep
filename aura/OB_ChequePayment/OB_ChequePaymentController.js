({
	doInit : function(component, event, helper) {
		let newURL = new URL(window.location.href).searchParams;
		var srID = newURL.get('srId');
		var category = newURL.get("category") ? newURL.get("category") : '';
		component.set("v.amount",getUrlParameter('amount'));
		console.log('init cheque');
		if(srID && category){
			var action = component.get("c.loadPriceItem");
			action.setParams({
				"srId":srID,
				"category":category
			});
			action.setCallback(this, function(response) {
			
				var state = response.getState();
				if (state === "SUCCESS") {
					console.log("button succsess")
					var result = response.getReturnValue();
					component.set("v.recordTypeId",result.receiptRecordTypeId);
					component.set("v.amount",result.amount);
					component.find("amount").set("v.disabled",true);
					console.log(response.getReturnValue());
				}else if (state === 'ERROR') { // Handle any error by reporting it
					var errors = response.getError();
					console.log(response.getError());
					
					if (errors) {
						if (errors[0] && errors[0].message) {
							helper.displayToast('Error', errors[0].message);
						}
					} else {
						helper.displayToast('Error', 'Unknown error.');
					}
				}
			});
			$A.enqueueAction(action);
		}
		function getUrlParameter(name) {
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
			var results = regex.exec(location.search);
			return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
		};
	},
	onRecordSubmit: function(component, event, helper) {
		console.log('onRecordSubmit');
		event.preventDefault(); // stop form submission
		var eventFields = event.getParam("fields");
		// console.log(eventFields);
		
		// console.log(eventFields["OB_Cheque_Number__c"]);
		eventFields["Bank_Name__c"] = eventFields["OB_Cheque_Number__c"] + '-' + eventFields["OB_Bank_Name__c"];
		console.log(JSON.parse(JSON.stringify(eventFields)));
		// Show error messages if required fields are blank
		var allValid = true;
		
		allValid = component.find('requiredField').reduce(function (validFields,inputCmp) {
			inputCmp.showHelpMessageIfInvalid();
			return validFields && inputCmp.get('v.validity').valid;
		}, true);
		console.log(allValid);
		
		if(allValid){
			console.log('submit');
			component.find('chequeform').submit(eventFields);
		}
		else{
			helper.displayToast('Error','Please fill the mandatory fields','error');
		}
	},
	handleError: function(cmp, event, helper) {
		console.log(JSON.stringify(event.getParam('detail')));
    },
	navigateToPaymentSelection : function(component, event, helper) {
		window.history.back();
	},
	popWarning: function(component, event, helper) {
		console.log(event.getSource().get("v.value"));
		var chequeDate = event.getSource().get("v.value");
		var CurrentDate = new Date();
		chequeDate = new Date(chequeDate);

		if(chequeDate > CurrentDate){
			helper.displayToast('Warning', 'Please contact DIFC for approval of payment with future date','warning');
		}
	},
	createCheque : function(component, event, helper) {
		console.log('createCheque');
		var allValid = true;

		var arrObj = component.find('requiredField');
		if(arrObj && Array.isArray(arrObj)){
			for(var key in arrObj){
				var inputFieldCompValue = arrObj[key].get("v.value");
				console.log(inputFieldCompValue);
				if(!inputFieldCompValue && arrObj[key] && arrObj[key].get("v.required")){
					console.log('bad' + inputFieldCompValue);
					arrObj[key].reportValidity();
					allValid = false;
				}
			}
		}
		else{
			if(arrObj && arrObj.get("v.required") && !arrObj.get("v.value")){
				arrObj.reportValidity();
				allValid = false;
			}
		}
		console.log(JSON.parse(JSON.stringify(component.get('v.Receipt'))));
		if(allValid){
			var action = component.get('c.createReceiptForCheque');
			action.setParams({
				"receipt":component.get("v.Receipt")
			});
			action.setCallback(this, function(response) {
				//store state of response
				var state = response.getState();
				console.log(state);
				if (state === "SUCCESS") {
					var responseResult = response.getReturnValue();
					console.log(responseResult);
					if(responseResult.errorMessage){
						helper.displayToast('Error', responseResult.errorMessage,'error');
					}
					else{
						var urlEvent = $A.get("e.force:navigateToURL");
						urlEvent.setParams({
							"url": '/paymentwalletconfirmation?receiptRef='+responseResult.referenceNo+'&amount='+component.get("v.Receipt.Amount__c"),
						});
						urlEvent.fire();
					}
				}
				else if (state === 'ERROR') { // Handle any error by reporting it
					var errors = response.getError();
					console.log(response.getError());
					
					if (errors) {
						if (errors[0] && errors[0].message) {
							helper.displayToast('Error', errors[0].message,'error');
						}
					} else {
						helper.displayToast('Error', 'Unknown error.','error');
					}
				}
			});
			$A.enqueueAction(action);
		}
		

        
	}
})