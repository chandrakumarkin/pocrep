({
	 /**
     * Display a message
     */
    displayToast : function (title, message, type) {
		console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'type': type,
                'title': title,
                'message': message,
                mode:'dismissible'
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
	},
})