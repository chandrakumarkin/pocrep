({
	viewDetails : function(cmp, event, helper) 
    {
		//throw event.
		cmp.set('v.spinner',true);
		var refeshEvnt = cmp.getEvent("refeshEvnt");
        var dataProtectionId = cmp.get("v.dataProtection").dataProtectObj.Id;
        console.log('@@@@@ dataProtectionId ',dataProtectionId);
        
        refeshEvnt.setParams({
            "dataProtectionId" : dataProtectionId,
        	"isViewRemove":"view"});
        cmp.set('v.spinner',false);
        refeshEvnt.fire();
	},
    remove : function(component, event, helper) 
    {
        console.log('====remove====');
		//remove 
		component.set('v.spinner',true);
		
        var getFormAction = component.get("c.removeDataProtection");
        var jurisdictionId = component.get("v.dataProtection").dataProtectObj.Id;
        var srId=component.get("v.srId");
        console.log('==remove==srId====='+srId);
        var reqWrapPram  =
            {
                srId:srId,
                jurisdictionId:jurisdictionId,
                srWrap:component.get('v.srWrap')
            }
            
            getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
            console.log('@@@@@@@@@@33 reqWrapPram init '+JSON.stringify(reqWrapPram));
            getFormAction.setCallback(this, 
            	function(response) {
                	var state = response.getState();
					console.log("callback state: " + state);
                    if (component.isValid() && state === "SUCCESS") 
                    {
                        var respWrap = response.getReturnValue();
                        console.log('===respWrap========'+JSON.stringify(respWrap));
                        if(!$A.util.isEmpty(respWrap.errorMessage)){
                             component.set('v.spinner',false);                     
                            //this.showToast("Error",respWrap.errorMessage);
                            component.set("v.errorMessage",respWrap.errorMessage);
                        	component.set("v.isError",true);
                            
                        }else{
                            //this.showToast("Success",'This is removed');
                            //throw event.
                            var refeshEvnt = component.getEvent("refeshEvnt");
                            var srWrap = respWrap.srWrap;
                            console.log('@@@@@ srWrap ',JSON.stringify(srWrap));
                            refeshEvnt.setParams({
                                "srWrap" : srWrap,
                                "isViewRemove":"remove"});
                            component.set('v.spinner',false);
                            refeshEvnt.fire();
                        }
                                              
                    }else{
                        console.log('@@@@@ Error '+response.getError()[0].message);
                        component.set('v.spinner',false); 
                        component.set("v.errorMessage",response.getError()[0].message);
                        component.set("v.isError",true);
                    }
                }
			);
            $A.enqueueAction(getFormAction);
        
	},
    closeModel : function(cmp, event, helper) {
        //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        console.log('--toast close--');
        cmp.set("v.errorMessage","");
        cmp.set("v.isError",false);
    },
})