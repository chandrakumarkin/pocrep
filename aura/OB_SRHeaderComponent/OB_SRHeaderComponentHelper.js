({
  autoOpen: function (cmp, event, helper, pageFlowWrapLst) {
    console.log("tbc");
  },

  initHelper: function (cmp, event, helper) {
    //var sobjectName = cmp.get('v.sObjectName');
    var getFormAction = cmp.get("c.loadPageFlow");
    getFormAction.setCallback(this, function (response) {
      var state = response.getState();
      console.log("callback state: " + state);

      if (cmp.isValid() && state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(
          "######### respWrap ",
          JSON.stringify(respWrap.pageFlowWrapLst[0].relatedLatestSR)
        );
        helper.withdrawButtonRenderCheck(
          cmp,
          event,
          helper,
          respWrap.pageFlowWrapLst
        );
        cmp.set("v.pageFlowWrapLst", respWrap.pageFlowWrapLst);
        cmp.set("v.respWrap", respWrap);

        helper.autoOpen(cmp, event, helper, respWrap.pageFlowWrapLst);
        console.log(respWrap.pageFlowWrapLst);
        console.log(respWrap);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log("@@@@@ Error " + response.getError()[0].stackTrace);
      }
    });
    $A.enqueueAction(getFormAction);
  },

  withdrawButtonRenderCheck: function (cmp, event, helper, srList) {
    try {
      var statusToShowTheWithdraButton = [
        "Submitted",
        "Awaiting Information",
        "In Progress"
      ];
      var recTypeToShowTheWithdraButton = [
        "In_Principle",
        "AOR_Financial",
        "AOR_NF_R"
      ];
      for (var sr of srList) {
        if (
          sr.relatedLatestSR &&
          statusToShowTheWithdraButton.includes(
            sr.relatedLatestSR.HexaBPM__Internal_Status_Name__c
          ) &&
          recTypeToShowTheWithdraButton.includes(
            sr.relatedLatestSR.HexaBPM__Record_Type_Name__c
          ) &&
          !sr.relatedLatestSR.HexaBPM__Steps_SR__r
        ) {
          sr.ShowWithdrawButton = true;
        } else {
          sr.ShowWithdrawButton = false;
          if (sr.relatedLatestSR.HexaBPM__Steps_SR__r) {
            sr.hasSubmittedWithdrwal = true;
          }
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  },
  withdrawSelctedSr: function (component, event, helper) {
    component.set("v.serverCallInProgress", true);
    var withdrawReason = component.get("v.withDrawalReason");
    if (withdrawReason) {
      var action = component.get("c.withdrawApplication");
      var requestWrap = {
        srToWithDraw: component.get("v.IdOfApplicationToWithdraw"),
        withdrawalReason: withdrawReason
      };
      console.log(requestWrap);

      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          if (!respWrap.errorMessage) {
            component.set("v.isOpenModal", false);
            helper.initHelper(component, event, helper);
          } else {
            helper.showToast(
              component,
              event,
              helper,
              respWrap.errorMessage,
              "error"
            );
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
        component.set("v.serverCallInProgress", false);
      });
      $A.enqueueAction(action);
    } else {
      helper.showToast(
        component,
        event,
        helper,
        "please enter the reason for withdrawal",
        "error"
      );
    }
  },

  showToast: function (component, event, helper, msg, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissible",
      message: msg,
      type: type
    });
    toastEvent.fire();
  }
});