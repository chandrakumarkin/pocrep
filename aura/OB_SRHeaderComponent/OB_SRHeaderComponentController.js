({
  init: function(cmp, event, helper) {
    //var sobjectName = cmp.get('v.sObjectName');
    var getFormAction = cmp.get("c.loadPageFlow");
    getFormAction.setCallback(this, function(response) {
      var state = response.getState();
      console.log("callback state: " + state);

      if (cmp.isValid() && state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log("######### respWrap ",JSON.stringify(respWrap));
        //console.log("######### respWrap ",JSON.stringify(respWrap.pageFlowWrapLst[0].relatedLatestSR));
        helper.withdrawButtonRenderCheck(
          cmp,
          event,
          helper,
          respWrap.pageFlowWrapLst
        );
        cmp.set("v.pageFlowWrapLst", respWrap.pageFlowWrapLst);
        cmp.set("v.respWrap", respWrap);

        helper.autoOpen(cmp, event, helper, respWrap.pageFlowWrapLst);
        console.log(respWrap.pageFlowWrapLst);
        console.log(respWrap);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log("@@@@@ Error " + response.getError()[0].stackTrace);
      }
    });
    $A.enqueueAction(getFormAction);
  },

  showPagesList: function(cmp, event, helper) {
    try {
      var currentPageFlowDiv = event.currentTarget.name;
        console.log('currentPageFlowDiv: '+currentPageFlowDiv);
      console.log(currentPageFlowDiv);
      document
        .getElementById(currentPageFlowDiv)
        .classList.toggle("difc-disp-none");
    } catch (err) {
      console.log(err.message);
    }

    //cmp.set("v.viewPageIcon",cmp.get("v.viewPageIcon")=="utility:down" ? "utility:up": "utility:down");
    //cmp.set("v.showPages",cmp.get("v.showPages")== true ? false : true);
  },

  handleApplicationWithdrawal: function(cmp, event, helper) {
    var index = event.getSource().get("v.value");
    console.log(index);

    var srLsit = cmp.get("v.pageFlowWrapLst");
    console.log(srLsit[index].relatedLatestSR);

    cmp.set("v.IdOfApplicationToWithdraw", srLsit[index].relatedLatestSR.Id);

    cmp.set("v.isOpenModal", true);
  },

  viewChat: function(cmp, event, helper) {
    var index = event.getSource().get("v.value");
    console.log(index);

    var srLsit = cmp.get("v.pageFlowWrapLst");
    console.log(srLsit[index].relatedLatestSR);

    cmp.set("v.IdOfApplicationToViewChat", srLsit[index].relatedLatestSR.Id);
    console.log(cmp.get("v.IdOfApplicationToViewChat"));

    cmp.set("v.isOpenChatModal", true);
  },

  closeModel: function(component, event, helper) {
    component.set("v.isOpenModal", false);
  },

  closeChatModel: function(component, event, helper) {
    component.set("v.isOpenChatModal", false);
  },

  navToChatHistory: function(component, event, helper) {
    window.location.href =
      "chat-history/?srId=" + component.get("v.IdOfApplicationToViewChat");
  },

  withdrawSelectedSr: function(component, event, helper) {
    console.log(component.get("v.IdOfApplicationToWithdraw"));

    helper.withdrawSelctedSr(component, event, helper);
  },

  onRender: function(component, event, helper) {
    console.log("in renderrrr");

    var respWrap = component.get("v.respWrap");
    var isrenderComp = component.get("v.isRenderCompleate");
    console.log(isrenderComp);

    if (respWrap != null && isrenderComp == false) {
      console.log("in render 2");
      helper.initHelper(component, event, helper);
      component.set("v.isRenderCompleate", true);
    }
  }
});