({
	closeModel : function(cmp, event, helper) {
        //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        console.log('--toast close--');
        cmp.set("v.errorMessage","");
        cmp.set("v.isError",false);
    },
    viewDetails : function(cmp, event, helper) 
    {
		//throw event.
		cmp.set('v.spinner',true);
		var refeshEvnt = cmp.getEvent("refeshEvnt");
        var shareClassID = cmp.get("v.shareClassWrap").shareClassObj.Id ;
        console.log('@@@@@ shareClassID ',shareClassID);
        
        refeshEvnt.setParams({
            "shareClassID" : shareClassID,
            "isNewAmendment" : false,
            "shareClassWrap":null});
        cmp.set('v.spinner',false);
        refeshEvnt.fire();
	},
    
    remove : function(component, event, helper) 
    {
        console.log('====remove====');
		//remove 
		component.set('v.spinner',true);
		var shareClassID = component.get("v.shareClassWrap").shareClassObj.Id ;
        var srId = component.get("v.srId");
        var getFormAction = component.get("c.removeShareClass");
        console.log('==remove==srId====='+srId);
        var reqWrapPram  =
            {
                srId: srId,
                delShareClassID:shareClassID
            }
            
            getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
            console.log('@@@@@@@@@@33 reqWrapPram init '+JSON.stringify(reqWrapPram));
            getFormAction.setCallback(this, 
            	function(response) {
                	var state = response.getState();
					console.log("callback state: " + state);
                    if (component.isValid() && state === "SUCCESS") 
                    {
                        var respWrap = response.getReturnValue();
                        console.log('===respWrap========'+JSON.stringify(respWrap));
                        if(!$A.util.isEmpty(respWrap.errorMessage)){
                             component.set('v.spinner',false);                     
                            //this.showToast("Error",respWrap.errorMessage);
                            component.set("v.errorMessage",respWrap.errorMessage);
                        	component.set("v.isError",true);
                            
                        }else{
                            //this.showToast("Success",'This is removed');
                            //throw event.
                            var refeshEvnt = component.getEvent("refeshEvnt");
                            var srWrap = respWrap.srWrap;
                            console.log('@@@@@ srWrap ',JSON.stringify(srWrap));
                            refeshEvnt.setParams({
                                "srWrap" : srWrap });
                            component.set('v.spinner',false);
                            refeshEvnt.fire();
                        }
                                              
                    }else{
                        console.log('@@@@@ Error '+response.getError()[0].message);
                        component.set('v.spinner',false); 
                        component.set("v.errorMessage",response.getError()[0].message);
                        component.set("v.isError",true);
                    }
                }
			);
            $A.enqueueAction(getFormAction);
        
	},
})