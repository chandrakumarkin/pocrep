({
	doInit  : function(component, event, helper) {
        var action = component.get("c.getSRDocList");
          console.log('Test');
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                console.log(storeResponse.length+'storeResponse');
                console.log('****'+JSON.stringify(storeResponse));

                if (storeResponse.length == 0) {
                     component.set("v.norecordfound", true);
                }
                 else {
                   component.set("v.norecordfound", false);
                }
               component.set("v.GDRFATracking", storeResponse);
               component.set("v.TotalCount", storeResponse.length);
             
            }
 
        });
        // enqueue the Action  
        $A.enqueueAction(action);
    
    },
    
    
    
    searchTextChange : function(component, event, helper) {
       var selectedValue = component.find("searchText").get("v.value");
      if(selectedValue !== undefined || selectedValue != ''){
            
       var action = component.get("c.getSRDocList");
      // set param to method  
        action.setParams({
            "searchValue": selectedValue,
          });
      // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                if (storeResponse.length == 0) {
                     component.set("v.norecordfound", true);
                } else {
                   component.set("v.norecordfound", false);
                }
                 var allRecords = component.get("v.selectedGDRFATracking");
                 
               component.set("v.GDRFATracking", storeResponse);
               component.set("v.TotalCount", storeResponse.length);
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);

        }
       
	},
     cancel : function(component, event, helper) {
       location.reload();
    },
    
    getSelectedSRDoc : function(component, event,helper) { 
        var selectedId='';
                     
        selectedId = event.target.getAttribute('id');
        if(document.getElementById(selectedId).checked && component.get("v.SelectedSRDOC").indexOf(selectedId) < 0)
            component.get('v.SelectedSRDOC').push(selectedId);
        else{
            var index = component.get("v.SelectedSRDOC").indexOf(selectedId);
            if (index > -1) {
                component.get("v.SelectedSRDOC").splice(index, 1); 
            }
        }
        
        var a = component.get('c.deleteSRDOC');
        $A.enqueueAction(a);
        //this.deleteSRDOC(component, event, helper);
    },
     deleteSRDOC:function(component,event,helper){
        var SelectedSRDOC=component.get("v.SelectedSRDOC");
        if(SelectedSRDOC.length>0){
            
            helper.removeSRDoc(component,SelectedSRDOC);
            //alert('selectedAccount'+selectedAccount);
        }
        else{
            alert('please select account to delete.')
        }
    },
    
})