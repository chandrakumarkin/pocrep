({
  onSelect: function (cmp, event, helper) {
    cmp.set("v.serverCallInProgress", true);
    var amendWrap = cmp.get("v.amendWrapper");
    console.log(amendWrap);
    if (amendWrap && amendWrap.length > 0) {
      helper.showToast(
        cmp,
        event,
        helper,
        "Only one registered address can be selected",
        "error"
      );
      cmp.set("v.serverCallInProgress", false);
    } else {
      var onAmedSave = cmp.get("c.onAmedSaveToDB");
      var reqWrapPram = {
        oprLocWrap: cmp.get("v.oprLocWrap"),
        srWrap: cmp.get("v.srWrap")
      };
      console.log("########### reqWrapPram ", JSON.stringify(reqWrapPram));
      onAmedSave.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      onAmedSave.setCallback(this, function (response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log("@@@@@ respWrap " + JSON.stringify(respWrap));
          cmp.set("v.serverCallInProgress", false);
          if (respWrap.errorMessage) {
            helper.showToast(
              cmp,
              event,
              helper,
              respWrap.errorMessage,
              "error"
            );
            //alert(respWrap.errorMessage);
          } else {
            //helper.showToast(cmp, event, helper, 'Address Added', 'info');
            //alert('Address Added.');
            cmp.set("v.serverCallInProgress", false);
            var refeshEvnt = cmp.getEvent("refeshEvnt");
            refeshEvnt.setParams({
              message: "refresh event"
            });
            refeshEvnt.fire();
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          cmp.set("v.serverCallInProgress", false);
        }
      });

      $A.enqueueAction(onAmedSave);
    }
  },
    doInit: function (cmp, event, helper) {
      var objSR = cmp.get("v.srWrap");
      console.log('objSR==>'+JSON.stringify(objSR));
      /* V1.1 - For Pocket Shops, Upserting the amendment for operating locations on load*/
      if(objSR!=null && objSR.srObj.Type_of_commercial_permission__c=='Gate Avenue Pocket Shops'){
      cmp.set("v.serverCallInProgress", true);
      var onAmedSave = cmp.get("c.onAmedSaveToDB");
      var reqWrapPram = {
        oprLocWrap: cmp.get("v.oprLocWrap"),
        srWrap: cmp.get("v.srWrap")
      };
      console.log("########### reqWrapPram ", JSON.stringify(reqWrapPram));
      onAmedSave.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      onAmedSave.setCallback(this, function (response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log("@@@@@ respWrap " + JSON.stringify(respWrap));
          cmp.set("v.serverCallInProgress", false);
          if (respWrap.errorMessage) {
            helper.showToast(
              cmp,
              event,
              helper,
              respWrap.errorMessage,
              "error"
            );
            //alert(respWrap.errorMessage);
          } else {
            //helper.showToast(cmp, event, helper, 'Address Added', 'info');
            //alert('Address Added.');
            cmp.set("v.serverCallInProgress", false);
            var refeshEvnt = cmp.getEvent("refeshEvnt");
            refeshEvnt.setParams({
              message: "refresh event"
            });
            refeshEvnt.fire();
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          cmp.set("v.serverCallInProgress", false);
        }
      });

      $A.enqueueAction(onAmedSave);
    }
    
  }
});