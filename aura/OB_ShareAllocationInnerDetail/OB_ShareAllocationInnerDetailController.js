({
	onSave : function(cmp, event, helper) 
    {
		try
        {
            helper.showSpinner(cmp, event, helper);
            //v.shrDetailWrap.shareHolderDetObj.No_of_Shares_Allotted__c
            
            var noOfAllotted = cmp.get('v.shrDetailWrap.shareHolderDetObj.No_of_Shares_Allotted__c');
            if( !noOfAllotted || noOfAllotted < 1 )
                {
                    console.log('default ');
                    helper.hideSpinner(cmp, event, helper);
                    helper.showToast(cmp, event, helper,'Allocated share should be greater than 0.' , 'error');
                    return;
                }
            
            
            var getFormAction = cmp.get('c.saveShareHolderDetails');
            let newURL = new URL(window.location.href).searchParams;
            
            //var srIdParam = newURL.get('srId'); 
            var srIdParam = ( newURL.get('srId') ? newURL.get('srId') : cmp.get('v.srId') ); 
            var pageId = newURL.get('pageId');
            cmp.set('v.srId',srIdParam); 
            console.log('in init-->' + srIdParam);
            
            var reqWrapPram  =	{
                                          srId: srIdParam,
                                        pageId: pageId,
                                 shrDetailWrap: cmp.get('v.shrDetailWrap')
                                }
            
            getFormAction.setParams({
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
            
            console.log('@@@@@@@@@@33121 reqWrapPram init '+JSON.stringify(reqWrapPram));
            getFormAction.setCallback(this, 
                                      function(response) 
                                      {
                                          var state = response.getState();
                                          console.log("callback state: " + state);
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                              var respWrap = response.getReturnValue();
                                              if( respWrap.errorMsg )
                                              {
                                                  helper.hideSpinner(cmp, event, helper);
                                                  helper.showToast(cmp, event, helper, respWrap.errorMsg , 'error');
                                              	  return;
                                              }
                                              
                                              console.log('########## respWrap ',respWrap);
                                              cmp.set('v.shrDetailWrap',respWrap.shrDetailWrap);
                                              helper.hideSpinner(cmp, event, helper);
                                              helper.showToast(cmp, event, helper, 'Shares Allocated.' , 'info');
                                              
                                              
                                          }
                                          else
                                          {
                                               console.log('@@@@@ Error '+JSON.stringify(response.getError()));
                                               //alert('Error.',JSON.stringify(response.getError()) );
                                              helper.hideSpinner(cmp, event, helper); 
                                              helper.showToast(cmp, event, helper, response.getError() , 'error');
                                          }
                                      }
                                     );
            $A.enqueueAction(getFormAction);
        }
        catch(err)
        {
             //console.log('=error==='+err.message);
             helper.showToast(cmp, event, helper, err.message , 'error');
        }
	},
    onRemove : function(cmp, event, helper) 
    {
       
			helper.showSpinner(cmp, event, helper);            
            var getFormAction = cmp.get('c.deleteShareHolderDetails');
            let newURL = new URL(window.location.href).searchParams;
            
            //var srIdParam = newURL.get('srId'); 
            var srIdParam = ( newURL.get('srId') ? newURL.get('srId') : cmp.get('v.srId') ); 
            var pageId = newURL.get('pageId');
            var shrDetailWrapParam = cmp.get('v.shrDetailWrap');
            var shrDetailWrapLst = cmp.get('v.shrDetailWrapLst');
            var index = cmp.get('v.index');
            
            if( shrDetailWrapParam.shareHolderDetObj.Id )
            {
                
               var reqWrapPram  =	{
                                      srId: srIdParam,
                					pageId: pageId,
                             shrDetailWrap: shrDetailWrapParam
                                }
            
                    getFormAction.setParams({
                        "reqWrapPram": JSON.stringify(reqWrapPram)
                    });
                    
                    console.log('@@@@@@@@@@33121 reqWrapPram init '+JSON.stringify(reqWrapPram));
                    getFormAction.setCallback(this, 
                                              function(response) {
                                                  var state = response.getState();
                                                  console.log("callback state: " + state);
                                                  
                                                  if (cmp.isValid() && state === "SUCCESS") 
                                                  {
                                                      var respWrap = response.getReturnValue();
                                                      if( respWrap.errorMsg )
                                                      {
                                                           helper.showToast(cmp, event, helper, response.errorMsg , 'error');
                                                      	   return;
                                                      }
                                                      
                                                      console.log('########## respWrap ',respWrap);
                                                      console.log('######## index to delete index ',index);
                                                      shrDetailWrapLst.splice(index,1);
                                                      cmp.set('v.shrDetailWrapLst',shrDetailWrapLst);
                                                      helper.hideSpinner(cmp, event, helper);
                                                      helper.showToast(cmp, event, helper, 'Shares Removed.' , 'info');
                                                      
                                                      
                                                      
        										  }
                                                  else
                                                  {
                                                      console.log('@@@@@ Error '+JSON.stringify(response.getError()));
                                                      //alert('Error.',JSON.stringify(response.getError()) );
                                                  	  helper.hideSpinner(cmp, event, helper);
                                                      helper.showToast(cmp, event, helper, response.getError() , 'error'); 
                                                  }
                                              }
                                             );
                    $A.enqueueAction(getFormAction);
                    
                    
            }
            else
            {
				      
                
                 console.log('######## index to delete index ',index);
                 console.log('######## shrDetailWrapLst length bfr  ',shrDetailWrapLst.length);
                 shrDetailWrapLst.splice(index,1);
                 console.log('######## shrDetailWrapLst length aftr ',shrDetailWrapLst.length);
                 cmp.set('v.shrDetailWrapLst',shrDetailWrapLst);
                 helper.hideSpinner(cmp, event, helper);
               
                /*
                var refeshEvnt = cmp.getEvent("refeshEvnt");
                                                      refeshEvnt.setParams({
                                                          "message" : 'Refreshing share allocation.'
                                                      });
                                                      refeshEvnt.fire();         */ 
            }
            
    }

})