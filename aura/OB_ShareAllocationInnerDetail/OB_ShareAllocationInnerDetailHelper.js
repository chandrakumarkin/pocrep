({
	showToast: function(component, event, helper, msg, type) 
    {
        // Use \n for line breake in string
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          mode: "dismissible",
          message: msg,
          type: type
        });
        toastEvent.fire();
  },
   // function automatic called by aura:waiting event  
        showSpinner: function(component, event, helper) {
            console.log('$$$$$ showSpinner ');
            // make Spinner attribute true for displaying loading spinner 
            component.set("v.spinner", true); 
        },
         
        // function automatic called by aura:doneWaiting event 
        hideSpinner : function(component,event,helper){
            
            // make Spinner attribute to false for hiding loading spinner    
            component.set("v.spinner", false);
        },
    
})