({
	showToast: function(component, event, msg, type) 
    {
        console.log('######## errorMessage inside ',msg);
                                                    
        // Use \n for line breake in string
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          mode: "dismissible",
          message: msg,
          type: type
        });
        toastEvent.fire();
 	}
})