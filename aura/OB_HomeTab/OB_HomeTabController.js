({
  callServer: function(component, event, helper) {
    //helper.onchekVATTaxNumber(component, event, helper);
    helper.getContactDetails(component, event, helper);
  },
  close: function(component, event, helper) {
    console.log("inside controller");
    helper.closeWelcomeScreen(component, event, helper);
  },
  openRegisteredForm: function(component, event, helper) {
    // for Display Model,set the "isOpen" attribute to "true"
    component.set("v.isOpen", true);
  },
  openVatRegisteredForm: function(component, event, helper) {
    var urlEvent = $A.get("e.force:navigateToURL");
    urlEvent.setParams({
      url: "/ob-vatregistration"
    });

    urlEvent.fire();
  },
  closeModel: function(component, event, helper) {
    // for Hide/Close Model,set the "isOpen" attribute to "Fasle"
    component.set("v.isOpen", false);
  },
  updateAccountVatField: function(component, event, helper) {
    helper.updateAccount(component, event, helper);
  },
  notRegested: function(component, event, helper) {
    helper.updateContact(component, event, helper);
  },
  closepopup: function (component, event, helper) {
    component.set("v.showPopup", false);
  }
});