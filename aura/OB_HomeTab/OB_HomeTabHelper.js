({
  getContactDetails: function(component, event) {
    var action = component.get("c.getConDetails");
    /* var requestWrap = {
      srId: srId,
      flowId: flowId,
      pageId: pageId
    };
    action.setParams({
      requestWrapParam: JSON.stringify(requestWrap)
    }); */

    action.setCallback(this, function(response) {
      var state = response.getState(); 
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        var result = response.getReturnValue();
        console.log('^^^^^^^^userProfileUpdationFields^^^^^^^^'+result.userProfileUpdationFields);
        console.log('^^^^^^^^userProfileSRURL^^^^^^^^'+result.userProfileSRURL);
        if(result.userProfileUpdationFields != '' && result.isPopupAllowedToshow == 'true'){
          component.set("v.showPopup", true);
          var arrayList = result.userProfileUpdationFields.split(',');
          component.set('v.portalMissedList', arrayList);
          component.set('v.userProfileSRURL', result.userProfileSRURL);
        }
        component.set("v.respWrap", response.getReturnValue());
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
      }
    });
    $A.enqueueAction(action);
  },

  updateContact: function(component, event) {
    var action = component.get("c.updateLoggedinContact");
    /* var requestWrap = {
      srId: srId,
      flowId: flowId,
      pageId: pageId
    };*/

    action.setParams({
      loggedInUser: component.get("v.respWrap.loggedInUser")
    });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        console.log(response.getReturnValue());
        component.set("v.respWrap", response.getReturnValue());
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log("@@@@@ Error " + response.getError()[0].stackTrace);
      }
    });
    $A.enqueueAction(action);
  },

  onchekVATTaxNumber: function(component, event) {
    component.find("Id_spinner").set("v.class", "slds-show");
    var action = component.get("c.checkVATNumber");

    action.setCallback(this, function(response) {
      //alert('==inside--');
      // hide spinner when response coming from server
      component.find("Id_spinner").set("v.class", "slds-hide");
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        component.set("v.isRegistration", respWrap.isVATNotRegistered);
      } else if (state === "INCOMPLETE") {
        alert("Response is Incompleted");
      } else if (state === "ERROR") {
        alert("Unknown error");
      }
    });
    $A.enqueueAction(action);
  },
  closeWelcomeScreen: function(component, event) {
    console.log("test");
    document.getElementById("welcomeUSer").style.display = "none";
    document.getElementById("welcomeUSer").style.display = "none";
  },
  updateAccount: function(component, event, helper) {
    //alert('==entered');
    component.find("Id_spinner").set("v.class", "slds-show");
    var action = component.get("c.updateAccountField");

    action.setCallback(this, function(response) {
      //alert('==inside--');
      // hide spinner when response coming from server
      component.find("Id_spinner").set("v.class", "slds-hide");
      var state = response.getState();
      if (state === "SUCCESS") {
        component.set("v.isOpen", false);
        helper.closeWelcomeScreen(component, event, helper);
      } else if (state === "INCOMPLETE") {
        alert("Response is Incompleted");
      } else if (state === "ERROR") {
        alert("Unknown error");
      }
    });
    $A.enqueueAction(action);
  }
});