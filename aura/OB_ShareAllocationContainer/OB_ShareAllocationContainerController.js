({
  doInit: function(cmp, event, helper) 
  {
    try {
      var getFormAction = cmp.get("c.getExistingAmendShareHolder");
      let newURL = new URL(window.location.href).searchParams;

      //var srIdParam = newURL.get('srId');
      var srIdParam = newURL.get("srId")? newURL.get("srId"): cmp.get("v.srId");
      var pageId = newURL.get("pageId");
      var flowId = newURL.get("flowId")? newURL.get("flowId"): cmp.get("v.flowId");

      cmp.set("v.srId", srIdParam);
      console.log("in init-->" + srIdParam);

      var reqWrapPram = {
        srId: srIdParam,
        pageId: pageId,
        flowId: flowId
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram)
      });

      console.log(
        "@@@@@@@@@@33121 reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
      getFormAction.setCallback(this, function(response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();

          console.log("########## respWrap ", respWrap);
          cmp.set("v.commandButton", respWrap.ButtonSection);
          cmp.set("v.respWrapInit", respWrap);
        } 
        else 
        {
          console.log("@@@@@ Error " + response.getError()[0].message);
          helper.showToast(cmp, event, helper,response.getError()[0].message,'error'); 
        }
      });
      $A.enqueueAction(getFormAction);
    } 
    catch (err) 
    {
      console.log("=error===" + err.message);
      helper.showToast(cmp, event, helper,err.message,'error'); 
      
    }
  },

  handleRefreshEvnt: function(cmp, event, helper) 
  {
    var message = event.getParam("message");
    console.log("$$$$$$$$$$$ refresh message  ", message);
    $A.enqueueAction(cmp.get("c.doInit"));
  },

  handleButtonAct: function(component, event, helper) 
  {
    try 
    {
      let newURL = new URL(window.location.href).searchParams;

      var srID = newURL.get("srId");
      var pageID = newURL.get("pageId");

      var buttonId = event.target.id;
      console.log(buttonId);
      var action = component.get("c.getButtonAction");

      action.setParams({
        SRID: srID,
        pageId: pageID,
        ButtonId: buttonId
      });

      action.setCallback(this, function(response) 
      {
        var state = response.getState();
        if (state === "SUCCESS") 
        {
          console.log("button succsess");
          var respVar = response.getReturnValue();

          if(respVar.errorMsg)
          {
              console.log('@@@@@@@@@2',respVar.errorMsg);
              helper.showToast(component, event, helper,respVar.errorMsg,'error'); 
          }
          else
          {
           	  window.open(response.getReturnValue().pageActionName, "_self");   
          }
          //console.log();
            
        } 
        else 
        {
          console.log("@@@@@ Error " + response.getError()[0].message);
          helper.showToast(component, event, helper,response.getError()[0].message,'error');
          
        }
      });
      $A.enqueueAction(action);
    } 
    catch (err) 
    {
      console.log(err.message);
      helper.showToast(component, event, helper,err.message,'error');
          
    }
  }
});