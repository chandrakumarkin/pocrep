({
  GetEntityData: function (component, event, helper) {
    component.set("v.showSpinner", true);
    try {
      console.log("init start");

      var action = component.get("c.fetchEntityData");

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);

          component.set("v.showSpinner", false);
          component.set("v.respWrap", respWrap);
          component.set("v.unfilteredAccList", respWrap.relatedAccounts);

          respWrap.pendingActionWrapper = helper.getSteps(
            component,
            event,
            helper,
            response.getReturnValue()
          );
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
        component.set("v.showSpinner", false);
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
      component.set("v.showSpinner", false);
    }
  },

  getSteps: function (component, event, helper, respWrap) {
    try {
      console.log(respWrap.loggedInUser);
      console.log(respWrap);
      console.log("second call started");

      var action = component.get("c.getPendingActions");

      var requestWrap = {
        relatedAccountIdsSet: respWrap.relatedAccountIdsSet,
        LoggedInUser: respWrap.loggedInUser,
      };
      action.setParams({
        requestWrapParam: JSON.stringify(requestWrap),
      });
      console.log(requestWrap);

      action.setCallback(this, function (response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          try {
            var respWrap2 = response.getReturnValue();
            console.log("respWrap2");

            console.log(respWrap2);

            respWrap.pendingActionWrapper = respWrap2;

            helper.csStepFilterLogic(
              component,
              event,
              helper,
              respWrap.pendingActionWrapper
            );
            helper.setFilterVals(
              component,
              event,
              helper,
              respWrap.relatedAccounts
            );

            helper.setPendingActionFilterVals(component, event, helper);
            helper.setActionItemCount(component, event, helper, respWrap);
            helper.linkAssosiatedActions(component, event, helper, respWrap);
            helper.splitRoles(component, event, helper, respWrap);
            /*helper.setKpiData(
              component,
              event,
              helper,
              respWrap.relatedAccounts
            );*/
            component.set("v.respWrap", respWrap);
            component.set("v.unfilteredAccList", respWrap.relatedAccounts);
          } catch (error) {
            console.log(error.message);
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
    }
  },

  csStepFilterLogic: function (component, event, helper, pendingActionWrapper) {
    console.log(pendingActionWrapper);
    var communityRoles = pendingActionWrapper.communityRole;
    if (communityRoles && communityRoles == "Fit-Out Services") {
      pendingActionWrapper.stepList.filter(
        (step) =>
          (communityRoles.includes("Company Services") &&
            (step.SR_Group__c == "Fit-Out & Events" ||
              step.SR_Group__c == "ROC" ||
              step.SR_Group__c == "RORP" ||
              step.SR_Group__c == "BC")) ||
          (communityRoles.includes("Company Services") == false &&
            communityRoles.includes("Property Services") &&
            step.SR_Group__c == "RORP") ||
          (step.SR_Group__c == "GS" &&
            communityRoles.includes("Employee Services") &&
            step.SR__r.External_Status_Name__c != "Cancelled") ||
          (step.SR_Group__c == "IT" && communityRoles.includes("IT Services"))(
            step.SR_Group__c == "Listing" &&
              communityRoles.includes("Listing Services")
          ) ||
          (step.SR_Group__c == "Fit-Out & Events" &&
            (communityRoles.includes("Fit-Out Services") ||
              communityRoles.includes("Company Services"))) ||
          (step.SR_Group__c == "Fit-Out & Events" &&
            communityRoles.includes("Event Services")) ||
          step.SR_Group__c == "Other" ||
          (step.SR_Group__c == "RORP" &&
            (communityRoles.includes("Mortgage Registration") ||
              communityRoles.includes("Discharge of Mortgage") ||
              communityRoles.includes("Variation of Mortgage")))
      );
    } else {
      pendingActionWrapper.stepList.filter(
        (step) =>
          (communityRoles.includes("Company Services") &&
            (step.SR_Group__c == "Fit-Out & Events" ||
              step.SR_Group__c == "ROC" ||
              step.SR_Group__c == "RORP" ||
              step.SR_Group__c == "BC")) ||
          (communityRoles.includes("Company Services") == false &&
            communityRoles.includes("Property Services") &&
            step.SR_Group__c == "RORP") ||
          (step.SR_Group__c == "GS" &&
            communityRoles.includes("Employee Services") &&
            step.SR__r.External_Status_Name__c != "Cancelled") ||
          (step.SR_Group__c == "IT" &&
            communityRoles.includes("IT Services")) ||
          (step.SR_Group__c == "Listing" &&
            communityRoles.includes("Listing Services")) ||
          (step.SR_Group__c == "Fit-Out & Events" &&
            (communityRoles.includes("Fit-Out Services") ||
              communityRoles.includes("Company Services"))) ||
          (step.SR_Group__c == "Fit-Out & Events" &&
            communityRoles.includes("Event Services")) ||
          step.SR_Group__c == "Other" ||
          (step.SR_Group__c == "RORP" &&
            (communityRoles.includes("Mortgage Registration") ||
              communityRoles.includes("Discharge of Mortgage") ||
              communityRoles.includes("Variation of Mortgage")))
      );
    }
  },

  splitRoles: function (component, event, helper, respWrap) {
    //add is super user
    try {
      for (var acc of respWrap.relatedAccounts) {
        if (acc.AccountContactRelations) {
          for (var rel of acc.AccountContactRelations) {
            if (rel && rel.Roles) {
              var roleList = rel.Roles.split(";");
              rel.RolesListView = [];
              for (var role of roleList) {
                rel.RolesListView.push(role);
              }
            }
          }
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  linkAssosiatedActions: function (component, event, helper, respWrap) {
    try {
      for (var account of respWrap.relatedAccounts) {
        account.relatedSteps = [];

        account.hasViolation = respWrap.emplymentViolationMap[account.Id];
        var listrelatedStep = respWrap.pendingActionWrapper.stepList
          .filter((step) => step.SR__r.Customer__c == account.Id)
          .concat(
            respWrap.pendingActionWrapper.actionItemList.filter(
              (actionItem) =>
                actionItem.HexaBPM__SR__r.HexaBPM__Customer__c == account.Id
            )
          );
        account.relatedSteps = listrelatedStep;
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  setFilterVals: function (component, event, helper, accList) {
    var options = [
      {
        label: "All",
        value: "All",
      },
    ];
    for (const account of accList) {
      if (account.Company_Type__c) {
        if (!options.find((obj) => obj.label === account.Company_Type__c)) {
          options.push({
            label: account.Company_Type__c,
            value: account.Company_Type__c,
          });
        }
      }
    }

    component.set("v.options", options);
  },

  setActionItemCount: function (component, event, helper, respWrap) {
    try {
      for (var account of respWrap.relatedAccounts) {
        var actionItemCount = respWrap.pendingActionWrapper.actionItemList.filter(
          function (actItem) {
            return actItem.HexaBPM__SR__r.HexaBPM__Customer__c === account.Id;
          }
        ).length;

        var stepItemCount = respWrap.pendingActionWrapper.stepList.filter(
          function (actItem) {
            return actItem.SR__r.Customer__c === account.Id;
          }
        ).length;

        var complianceCount = 0;
        if (account.Compliances__r) {
          complianceCount = account.Compliances__r.length;
        }

        account.pendingActionItem =
          actionItemCount + stepItemCount + complianceCount;
        //console.log("counter = " + account.pendingActionItem);
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  showToast: function (component, event, helper, type, message) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      message: message,
      duration: " 5000",
      key: "info_alt",
      type: type,
      mode: "pester",
    });
    toastEvent.fire();
  },

  setPendingActionFilterVals: function (component, event, helper, accList) {
    var options = [
      {
        label: "All",
        value: "All",
      },

      {
        label: "Returned SRs",
        value: "Returned SRs",
      },
      {
        label: "Upcoming Filings and required submission",
        value: "Upcoming Filings and required submission",
      },
      {
        label: "Employment violations",
        value: "Employment violations",
      },
      {
        label: "Fines",
        value: "Fines",
      },
      {
        label: "Others",
        value: "Others",
      },
    ];

    component.set("v.pendingActionOptions", options);
  },

  setKpiData: function (component, event, helper, listAccount) {
    try {
      console.log("list ofaccount");
      console.log(listAccount);
      var KpiData = [];

      //License renewal and Confirmation statement due within a month
      var kpiData1 = listAccount.filter(
        (acc) =>
          acc.Compliances__r &&
          acc.Compliances__r.filter(
            (comp) =>
              (comp.Name == "License Renewal" ||
                comp.Name == "Confirmation Statement") &&
              comp.Status__c == "Open"
          ).length > 0
      ).length;

      var fineData = {
        title: "License renewal and Confirmation statement due", //within a month,
        kpi: kpiData1,
      };
      KpiData.push(fineData);

      //Filing of Audited account due within a month
      var fineList = listAccount.filter(
        (step) => step.Service_Type__c && step.Service_Type__c == "Issue Fine"
      ).length;
      console.log(fineList);
      var fineData = {
        title: "Filing of Audited account due within a month",
        kpi: fineList,
      };
      KpiData.push(fineData);

      //Entitles with Issued fines
      var fineList = listAccount.filter(
        (acc) =>
          acc.relatedSteps.filter(
            (step) =>
              step.Service_Type__c && step.Service_Type__c == "Issue Fine"
          ).length > 0
      ).length;

      var fineData = {
        title: "Entitles with Issued fines",
        kpi: fineList,
      };
      KpiData.push(fineData);

      //Expired visa and work permit
      var fineList = listAccount.filter(
        (acc) =>
          (acc.Utilize_Visa_Quota__c != null &&
            acc.Calculated_Visa_Quota__c != null &&
            acc.Utilize_Visa_Quota__c - acc.Calculated_Visa_Quota__c > 0) ||
          //Expired establishment card
          (acc.Index_Card__r &&
            acc.Index_Card__r.Valid_To__c &&
            Date.parse(acc.Index_Card__r.Valid_To__c) <
              Date.parse(new Date())) ||
          (acc.hasViolation && acc.hasViolation == true)
      ).length;
      console.log(fineList);
      var fineData = {
        title: "Expired visa and work permit",
        kpi: fineList,
      };
      KpiData.push(fineData);

      //Entities with inactive lease
      var fineList = listAccount.filter((acc) => acc.Lease__r).length;
      console.log(fineList);
      var fineData = {
        title: "Entities with inactive lease",
        kpi: fineList,
      };
      KpiData.push(fineData);

      component.set("v.KpiCardData", KpiData);
    } catch (error) {
      console.log(error.message);
    }
  },
});