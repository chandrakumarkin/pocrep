({
  doInit: function (component, event, helper) {
    let newURLParam = new URL(window.location.href).searchParams;
    var blocked = newURLParam.get("blocked");
    if (blocked) {
      component.set("v.showblockedModal", true);
    }
    helper.GetEntityData(component, event, helper);
    //helper.setPendingActionFilterVals(component, event, helper);
  },

  handleFilter: function (component, event, helper) {
    try {
      var relAccountList = component.get("v.unfilteredAccList");
      var searchText = component.get("v.searchString");
      var filteredType = component.get("v.selectedValue");
      var PemdingActionfilteredType = component.get(
        "v.selectedPendingActionValue"
      );
      var returenSrCode = [
        "RE_UPLOAD_DOCUMENT",
        "AWAITING_INFORMATION_FROM_ENTITY",
      ];

      console.log(filteredType);
      console.log(PemdingActionfilteredType);

      if (searchText && searchText.trim()) {
        relAccountList = relAccountList.filter((acc) =>
          acc.Name.toLowerCase().includes(searchText.toLowerCase())
        );
      }
      if (filteredType && filteredType != "All") {
        relAccountList = relAccountList.filter((acc) =>
          acc.Company_Type__c.includes(filteredType)
        );
      }

      if (PemdingActionfilteredType && PemdingActionfilteredType != "All") {
        if (PemdingActionfilteredType == "Returned SRs") {
          relAccountList = relAccountList.filter(
            (acc) =>
              acc.relatedSteps.filter(
                (act) =>
                  act.Step_Template__r &&
                  act.Step_Template__r.Code__c &&
                  (act.Step_Template__r.Code__c.includes("RETURNED") ||
                    act.Step_Template__r.Code__c.includes("RETURN") ||
                    act.Step_Template__r.Code__c.includes("REQUIRE") ||
                    returenSrCode.includes(act.Step_Template__r.Code__c))
              ).length > 0 ||
              acc.relatedSteps.filter(
                (act) =>
                  act.HexaBPM__SR_Step__r &&
                  act.HexaBPM__SR_Step__r.HexaBPM__Step_Template_Code__c &&
                  returenSrCode.includes(
                    act.HexaBPM__SR_Step__r.HexaBPM__Step_Template_Code__c
                  )
              ).length > 0
          );
        } else if (
          PemdingActionfilteredType ==
          "Upcoming Filings and required submission"
        ) {
          relAccountList = relAccountList.filter(
            (acc) =>
              /*acc.Compliances__r ||*/
              (acc.Lease_Expiry_Date__c &&
                Math.round(
                  (Date.parse(acc.Lease_Expiry_Date__c) -
                    Date.parse(new Date())) /
                    (1000 * 60 * 60 * 24)
                ) < 30 &&
                Date.parse(acc.Lease_Expiry_Date__c) >
                  Date.parse(new Date())) ||
              //Notification renewal for DP
              (acc.Compliances__r &&
                acc.Compliances__r.filter(
                  (comp) =>
                    (comp.Name == "Notification Renewal" ||
                      comp.Name == "License Renewal" ||
                      comp.Name == "Confirmation Statement") &&
                    comp.Status__c == "Open"
                ).length > 0)
          );
        } else if (PemdingActionfilteredType == "Employment violations") {
          relAccountList = relAccountList.filter(
            (acc) =>
              (acc.Utilize_Visa_Quota__c != null &&
                acc.Calculated_Visa_Quota__c != null &&
                acc.Utilize_Visa_Quota__c - acc.Calculated_Visa_Quota__c > 0) ||
              //Expired establishment card
              (acc.Index_Card__r &&
                acc.Index_Card__r.Valid_To__c &&
                Date.parse(acc.Index_Card__r.Valid_To__c) <
                  Date.parse(new Date())) ||
              //Expired visas
              (acc.hasViolation && acc.hasViolation == true)
          );
        } else if (PemdingActionfilteredType == "Fines") {
          relAccountList = relAccountList.filter(
            (acc) =>
              acc.relatedSteps.filter(
                (step) =>
                  step.Service_Type__c && step.Service_Type__c == "Issue Fine"
              ).length > 0
          );
        } else if (PemdingActionfilteredType == "Others") {
          relAccountList = relAccountList.filter(
            (acc) => acc.Excess_PSA_Deposit__c && acc.Excess_PSA_Deposit__c < 0
          );
        }
      }

      component.set("v.respWrap.relatedAccounts", relAccountList);

      console.log(relAccountList);
    } catch (error) {
      console.log(error.message);
    }
  },

  handleSort: function (component, event, helper) {
    try {
      var fieldToSort = event.target.dataset.fieldname;
      var relAccountList = component.get("v.respWrap.relatedAccounts");
      var sortOrder = component.get("v.sortOrder");

      console.log(fieldToSort);

      relAccountList.sort(compareValues(fieldToSort, sortOrder));

      function compareValues(key, order = "asc") {
        return function innerSort(a, b) {
          console.log(typeof a[key]);

          if (!a.hasOwnProperty(key) && !b.hasOwnProperty(key)) {
            return 0;
          }

          // nulls sort after anything else
          if (!a.hasOwnProperty(key) || !a[key]) {
            return 1;
          } else if (!b.hasOwnProperty(key) || !b[key]) {
            return -1;
          }

          const varA =
            typeof a[key] === "string" ? a[key].toUpperCase() : a[key];
          const varB =
            typeof b[key] === "string" ? b[key].toUpperCase() : b[key];

          let comparison = 0;
          if (varA > varB) {
            comparison = 1;
          } else if (varA < varB) {
            comparison = -1;
          }
          return order === "desc" ? comparison * -1 : comparison;
        };
      }

      console.log(relAccountList);
      component.set("v.respWrap.relatedAccounts", relAccountList);
      sortOrder = sortOrder == "asc" ? "desc" : "asc";
      component.set("v.sortOrder", sortOrder);
    } catch (error) {
      console.log(error.message);
    }
  },

  createEntity: function (component, event, helper) {
    component.set("v.showSpinner", true);

    /* if (relSr) {
      window.location.href = "ob-processflow" + navUrl + "&srId=" + relSr.Id;
      return;
    } */

    var action = component.get("c.createNewEntitySr");
    var requestWrap = {
      contactId: component.get("v.respWrap.loggedInUser.ContactId"),
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        window.location.href =
          "ob-processflow" +
          respWrap.pageFlowURl +
          "&srId=" +
          respWrap.insertedSr.Id;
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
      component.set("v.showSpinner", false);
    });
    $A.enqueueAction(action);
  },

  viewAsSelectedAccount: function (component, event, helper) {
    component.set("v.selectedAccount", event.currentTarget.dataset.selaccid);
    component.set("v.showModal", true);
  },

  closeModal: function (component, event, helper) {
    component.set("v.showModal", false);
  },
  closeBlockedModal: function (component, event, helper) {
    component.set("v.showblockedModal", false);
  },

  viewAsSelectedAccountHelper: function (component, event, helper) {
    component.set("v.showSpinner", true);
    //var selectedAccount = event.currentTarget.dataset.selaccid;
    var selectedAccount = component.get("v.selectedAccount");
    var contactId = component.get("v.respWrap.loggedInUser.ContactId");

    var action = component.get("c.setContactMainAccount");
    var requestWrap = {
      mainAccIdToSet: selectedAccount,
      contactId: contactId,
    };
    console.log(requestWrap);
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        if (!respWrap.errorMessage) {
          /*  var urlEvent = $A.get("e.force:navigateToURL");
          urlEvent.setParams({
            url:
              "/"
          });
          urlEvent.fire(); */
          try {
            var communityHomePage = $A.get("$Label.c.OB_Community_URL");
            console.log(communityHomePage);

            window.location.href = communityHomePage;
          } catch (error) {
            console.log(error.message);
            component.set("v.showSpinner", false);
          }
        } else {
          helper.showToast(
            component,
            event,
            helper,
            "error",
            respWrap.errorMessage
          );
          component.set("v.showSpinner", false);
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
        component.set("v.showSpinner", false);
      }
    });
    $A.enqueueAction(action);
  },

  handlePendingActionFilter: function (component, event, helper) {
    var filteredType = component.get("v.selectedPendingActionValue");
    console.log(filteredType);
  },
});