({
  doInit: function (component, event, helper) {
    var action = component.get("c.viewContactInfo");

    action.setCallback(this, function (response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var responseResult = response.getReturnValue();
        component.set("v.QuickContact", responseResult.quickContact);
        component.set("v.respWrap", responseResult);
        component.set("v.displayComponent", responseResult.displayComponent);

        console.log(responseResult);
      } else if (state === "INCOMPLETE") {
        console.log("Response is Incompleted");
      } else if (state === "ERROR") {
        var errors = response.getError();
        console.log(errors);
        if (errors) {
          if (errors[0] && errors[0].message) {
            //alert("Error message: " + errors[0].message);
          }
        } else {
          console.log("Unknown error");
        }
      }
    });
    $A.enqueueAction(action);
  },

  closeModel: function (component, event, helper) {
    component.set("v.isOpenModal", false);
  },

  handleContact: function (component, event, helper) {
    var pageFlowDetails = component.get("v.pageFlowDeatils");
    var type = event.currentTarget.dataset.type;

    if (pageFlowDetails) {
      helper.navToChatter(component, event, helper, pageFlowDetails, type);
    } else {
      helper.getPageFlows(component, event, helper, type);
    }
  },

  createApplicationComm: function (component, event, helper) {
    if (helper.formValidator(component, event, helper, "inputField")) {
      helper.createApplicationCommHelper(component, event, helper);
    } else {
      console.log("in else");

      helper.reportValididty(component, event, helper, "inputField");
    }
  }
});