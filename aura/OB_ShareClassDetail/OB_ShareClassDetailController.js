({
	handleLookupEvent: function (component, event, helper) 
    {
        console.log('handleLookupEvent');
        var accObj = event.getParam("sObject"); 
        console.log('lookup--'+JSON.stringify(accObj));
        component.set("v.shareClassWrap.shareClassObj.Currency__c",accObj.Id);
       
    },
    clearLookupEvent : function (component, event, helper) 
    {
        
    },closeForm: function(cmp, event, helper){
        //throw event.
        cmp.set('v.spinner',true);
        var isNew = cmp.get('v.isNew');
        if(isNew == true){
            console.log('===new record====');
        	cmp.set("v.shareClassWrap.shareClassObj",null);
        }
        var refeshEvnt = cmp.getEvent("refeshEvnt");
        refeshEvnt.setParams({
            "shareClassID" : '',
        	"isNewAmendment" : isNew,
            "shareClassWrap":cmp.get("v.shareClassWrap.shareClassObj")});
        cmp.set('v.spinner',false);
        refeshEvnt.fire();
        
    },
    closeModel : function(cmp, event, helper) {
        //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        console.log('--toast close--');
        cmp.set("v.errorMessage","");
        cmp.set("v.isError",false);
    },
    onSave : function(cmp, event, helper) 
    {
	   console.log('######### onSave ');
       cmp.set('v.spinner',true);
       cmp.set("v.isError",false);
       var isError = cmp.get("v.isError");
        
       let fields = cmp.find("requiredField"); // gets all required fields
       console.log('######### isError '+isError);
       
       var currencyId = cmp.get('v.srWrap.currencyId');
       var shareClassWrap = cmp.get('v.shareClassWrap');
       
       var currency = shareClassWrap.shareClassObj.Currency__c;
       console.log(currencyId+'===currency=='+currency);

       if(!$A.util.isEmpty(currencyId) && !$A.util.isUndefined(currencyId)){
       
    	   shareClassWrap.shareClassObj.Currency__c= currencyId;
       }
       
       if(!$A.util.isEmpty(currency) && !$A.util.isUndefined(currency)){
       
    	   currencyId = currency;
       }
       for (var i = 0; i < fields.length; i++) {                
            var inputFieldCompValue = fields[i].get("v.value");
            if(!inputFieldCompValue && fields[i] && fields[i].get("v.required")){
                console.log('no value');
                fields[i].reportValidity();
                
                cmp.set("v.errorMessage",'Please fill required field');
		  		cmp.set("v.isError",true); 
		        isError = true;
            }  
       }
        
	   
        
       if($A.util.isEmpty(currencyId) || $A.util.isUndefined(currencyId)){
        console.log('===error=true=');
       	cmp.set("v.errorMessage",'Please fill required field');
        cmp.set("v.isError",true); 
          
        isError = true;
       }        
       console.log('====isError===='+isError);
       if(isError == true){
        cmp.set('v.spinner',false);
        helper.displayToast('Error',"Please fill required field",'error');
       }
       var numberShare = shareClassWrap.shareClassObj.No_of_shares_per_class__c;
       var nominalValue = shareClassWrap.shareClassObj.Nominal_Value__c;
       if(isError == false && (numberShare <= 0 || nominalValue <= 0)){
        isError = true;
        cmp.set('v.spinner',false);
        helper.displayToast('Error',"Please provide positive value",'error');
       }
       
       if(isError != true){
           
       console.log('-----no error---');
        var onSave = cmp.get('c.onSaveDB');
        var srId = cmp.get('v.srId');
        var accountID = cmp.get('v.accountID');
        
        
        var reqWrapPram  =
            {
                srId:srId,
                accountID:accountID,
                shareClassWrap: shareClassWrap  
            }
        
        onSave.setParams(
            {
                "reqWrapPram": JSON.stringify(reqWrapPram)
            });
        
        onSave.setCallback(this, 
        	function(response) {
            	var state = response.getState();
                console.log("callback state: " + state);
                                          
                if (cmp.isValid() && state === "SUCCESS"){
                    var respWrap = response.getReturnValue();
                    
                    if(!$A.util.isEmpty(respWrap.errorMessage) ){
                        console.log('=====error on sve===='+respWrap.errorMessage);
                        cmp.set('v.spinner',false);
                        //this.showToast("Error",respWrap.errorMessage);
                        cmp.set("v.errorMessage",respWrap.errorMessage);
                        cmp.set("v.isError",true);
                        helper.displayToast('Error',respWrap.errorMessage,'error');
                    }else{
                        if(respWrap.shareClassWrap.shareClassObj.id != '' )
                        {
                            console.log('=====next exlse====');
                            //this.closeForm(cmp, event, helper);
                        	cmp.set("v.shareClassWrap.shareClassObj",null);
                            var refeshEvnt = cmp.getEvent("refeshEvnt");
                            var srWrap = respWrap.srWrap;
                            console.log('@@@@@ srWrap ',srWrap);
                            refeshEvnt.setParams({
                                "srWrap" : srWrap });
                            cmp.set('v.spinner',false);
                            refeshEvnt.fire();
                        }
                    }                             
                }
                else
                {
                    console.log('@@@@@ Error '+response.getError()[0].message);
                    cmp.set('v.spinner',false);
                    cmp.set("v.errorMessage",response.getError()[0].message);
                    cmp.set("v.isError",true);   
                    helper.displayToast('Error',response.getError()[0].message,'error');
                }
            	}
			);
            $A.enqueueAction(onSave);
       }
        
	},
})