({
	doInit: function (cmp, event, helper) {
            cmp.set("v.Spinner",true); 
            setTimeout(function(){ cmp.set("v.Spinner",false); }, 1000);      
            var action = cmp.get("c.getserviceRequest");
            action.setParams({
                recordId : cmp.get("v.recordIds")
            });
            action.setCallback(this, function (a) {
                var state = a.getState();
                if (state == "SUCCESS") {
              
                   var result = a.getReturnValue();
                       
                    if(result.newSR.Id !== '' && result.newSR.Id !== null && result.newSR.Id !== undefined ){
                        
                       cmp.set("v.selectedTitle",result.newSR.Title__c);
                       cmp.set("v.selectedNationality",result.newSR.Nationality_list__c);
                       //cmp.set("v.selectedcourier",result.newSR.Would_you_like_to_opt_our_free_couri__c);
                      // cmp.set("v.durationselected",result.newSR.Duration__c);
                       cmp.set("v.occuptionSelected",result.newSR.Occupation__c);
                        
                       cmp.find("Title").set("v.value",result.newSR.Title__c);
                       //cmp.find("Duration").set("v.value",result.newSR.Duration__c);
                       cmp.find("nationality").set("v.value",result.newSR.Nationality_list__c);
                      // cmp.find("couriersel").set("v.value",result.newSR.Would_you_like_to_opt_our_free_couri__c);
                       cmp.find("occup").set("v.value",result.newSR.Occupation__c);
                        
                        if(result.newSR.Would_you_like_to_opt_our_free_couri__c =='Yes'){
                           cmp.set("v.isCourierSelected",true); 
                        }
                        else{
                           cmp.set("v.isCourierSelected",false);
                        }
                        
                         if(result.newSR.Service_Category__c =='Cancellation' || result.newSR.Service_Category__c =='Lost / Replacement'){
                              cmp.set("v.NationalityDisabled",true);
                              cmp.set("v.occupationDisabled",true);
                              cmp.set("v.DateofBirthDisabled",true);
                              cmp.set("v.PassportDateofExpiryDisabled",true);
                              cmp.set("v.PassportDateofissueDisabled",true);
                              cmp.set("v.DateofBirthDisabled",true);
                              cmp.set("v.PassportDateofissueDisabled",true);
                              cmp.set("v.isPassportDisabled",true);
                              cmp.set("v.visaDisabled",true);
                              cmp.set("v.visaExpiryDisabled",true);
                              cmp.set("v.isNameDisabled",true);
                              cmp.set("v.istitleDisable",true);

                         }
                        
                         if(result.newSR.Service_Category__c =='Amendment'){
                              cmp.set("v.NationalityDisabled",true);
                              cmp.set("v.occupationDisabled",true);
                              cmp.set("v.DateofBirthDisabled",true);
                              cmp.set("v.PassportDateofExpiryDisabled",true);
                              cmp.set("v.PassportDateofissueDisabled",true);
                              cmp.set("v.DateofBirthDisabled",true);
                              cmp.set("v.PassportDateofissueDisabled",true);
                              cmp.set("v.isPassportDisabled",true);
                              cmp.set("v.visaDisabled",true);
                              cmp.set("v.visaExpiryDisabled",true);
                              cmp.set("v.isNameDisabled",true);
                              cmp.set("v.istitleDisable",true);

                         }
                        
                         if(result.newSR.Service_Category__c =='Renewal'){
                              cmp.set("v.NationalityDisabled",false);
                              cmp.set("v.occupationDisabled",false);
                              cmp.set("v.DateofBirthDisabled",true);
                              cmp.set("v.PassportDateofExpiryDisabled",false);
                              cmp.set("v.PassportDateofissueDisabled",false);
                              cmp.set("v.DateofBirthDisabled",true);
                              cmp.set("v.PassportDateofissueDisabled",true);
                              cmp.set("v.isPassportDisabled",true);
                              cmp.set("v.visaDisabled",false);
                              cmp.set("v.visaExpiryDisabled",false);
                              cmp.set("v.isNameDisabled",true);
                              cmp.set("v.istitleDisable",true);

                         }
                        
                       if(result.newSR.Would_you_like_to_opt_our_free_couri__c =='Yes'){
                           cmp.set("v.isCourierSelected",true); 
                        }
                        else{
                           cmp.set("v.isCourierSelected",false); 
                        }

                   if(result.newSR.Nationality_list__c === 'United Arab Emirates' ||result.newSR.Nationality_list__c === 'Kuwait'
                        || result.newSR.Nationality_list__c === 'Saudi Arabia' ||result.newSR.Nationality_list__c === 'Oman'
                        || result.newSR.Nationality_list__c === 'Bahrain' || result.newSR.Nationality_list__c === 'Qatar'  ){
          
                           cmp.set("v.isGCC",true);
                   }
                   else{
                     cmp.set("v.isGCC",false);
                   }
               }
                   cmp.set("v.serviceRqst",result.newSR);
                   cmp.set("v.accountNew",result.newAccount);
                   cmp.set("v.optionList",result.optionList);
                   cmp.set("v.occupationList",result.occuptationList);
                 } else {
                 
                }
             /* if(result.LinkedSR !== undefined ){
                cmp.set("v.selectedLookUpRecord",result.LinkedSR);
              }*/
            });
            $A.enqueueAction(action);               
    },

})