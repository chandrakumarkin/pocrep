({
    doInit: function (cmp, event, helper) {
        cmp.set("v.spinner", true);
        try {
            console.log("===pages init manage Shareholders===");
            
            var getFormAction = cmp.get("c.getExistingAmendment");
            
            let newURL = new URL(window.location.href).searchParams;
            var pageId = newURL.get("pageId");
            var srIdParam = cmp.get("v.srId")
            ? cmp.get("v.srId")
            : newURL.get("srId");
            var flowId = newURL.get("flowId");
            
            var reqWrapPram = {
                srId: srIdParam,
                pageID: pageId,
                flowId: flowId
            };
            console.log(">>>>>JSON.stringify(reqWrapPram)>>>>>>>>>"+JSON.stringify(reqWrapPram));
            getFormAction.setParams({
                reqWrapPram: JSON.stringify(reqWrapPram)
            });
            
            console.log(">>>>>>>>>>>>>>");
            
            getFormAction.setCallback(this, function (response) {
                var state = response.getState();
                console.log("callback state: " + state);
                
                if (cmp.isValid() && state === "SUCCESS") {
                    var respWrap = response.getReturnValue();
                    //console.log(JSON.parse(JSON.stringify(respWrap)));
                    console.log("respWrap is here"+JSON.stringify(respWrap.srWrap.approvedFormOneROS));
                    
                    // setting entityTypeLabel
                    if (respWrap.entityTypeLabel) {
                        cmp.set(
                            "v.entityTypeLabel",
                            respWrap.entityTypeLabel.toLowerCase()
                        );
                    }
                    console.log("respWrap is here:::"+JSON.stringify(respWrap.srWrap.debtObjLst));
                    console.log("securityPartyObjLst is here:::"+JSON.stringify(respWrap.srWrap.securityPartyObjLst));
                    console.log("v.srWrap is here:::"+respWrap.srWrap);
                    cmp.set("v.srWrap", respWrap.srWrap);
                    cmp.set("v.approvedForm", respWrap.srWrap.approvedFormOneROS);
                    cmp.set("v.options", respWrap.srWrap.SrFsObjList);
                    /*if(respWrap.srWrap.debtObjLst != null){
                        cmp.set("v.isDebt", true);
                    }*/
                    /*if(respWrap.srWrap.securityPartyObjLst != null){
                        cmp.set("v.isSecObj", true);
                    }*/
                    /*console.log(
                        "$$$$$$$$%approvedForm%%%%%%%%%"+
                        JSON.stringify(respWrap.srWrap.approvedFormOneROS.Initial_Financial_Statement__c)
                    );*/
                    
                    console.log(
                        "$$$$$$$$%%%%%%%%%%res",
                        JSON.stringify(respWrap.srWrap.srObj)
                    );
                    
                    cmp.set("v.commandButton", respWrap.ButtonSection);
                    console.log(
                        "#########123456 respWrap ",
                        JSON.stringify(cmp.get("v.srWrap.shareholderAmedWrapLst"))
                    );
                    
                    cmp.set("v.spinner", false);
                } else {
                    cmp.set("v.spinner", false);
                    console.log("@@@@@ Error " + response.getError()[0].message);
                    //cmp.set("v.errorMessage", response.getError()[0].message);
                    //cmp.set("v.isError", true);
                    helper.createToast(cmp, event, response.getError()[0].message);
                }
            });
            $A.enqueueAction(getFormAction);
        } catch (err) {
            console.log("=error===" + err.message);
            cmp.set("v.spinner", false);
        }
    },
    
    handleRefreshEvnt: function (cmp, event, helper) {
        var amedId = event.getParam("amedId");
        var srWrap = event.getParam("srWrap");
        var hideOldWrap = event.getParam("hideOldWrap");
        
        var isNewAmendment = event.getParam("isNewAmendment");
        var amedWrap = event.getParam("amedWrap");
        console.log("$$$$$$$$$$$ handleRefreshEvnt== ", JSON.stringify(srWrap));
        console.log("$$$$$$$$$$$ hideOldWrap== ", hideOldWrap);
        console.log("2222222222 handleRefreshEvnt== ", amedWrap);
        cmp.set("v.amedId", amedId);
        if(hideOldWrap != undefined){
            cmp.set("v.hideOldWrap", hideOldWrap);}
        cmp.set("v.selectedValue", "");
        
        //for auto scolling
        var activeScreen = document.getElementById("activePage");
        console.log("id of active screen ------>" + activeScreen);
        debugger;
        
        if (activeScreen) {
            activeScreen.scrollIntoView({
                block: "start",
                behavior: "smooth"
            });
        }
        
        if (amedId != null && amedId != "") {
            cmp.set("v.isNewPanelShow", false);
        } else {
            cmp.set("v.isNewPanelShow", true);
        }
        
        if (!$A.util.isEmpty(srWrap)) {
            console.log("====sr wrap assign===");
            cmp.set("v.srWrap", srWrap);
            cmp.set("v.selectedValue", "");
        }
        if (isNewAmendment == true) {
            console.log("====isNew===");
            cmp.set("v.amedWrap", amedWrap);
            cmp.set("v.selectedValue", "");
        }
        if (isNewAmendment == false) {
            console.log("====isNew===");
            cmp.set("v.amedWrap", amedWrap);
            cmp.set("v.selectedValue", "");
        }
    },
    
    closeModel: function (cmp, event, helper) {
        //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        console.log("--toast close--");
        cmp.set("v.errorMessage", "");
        cmp.set("v.isError", false);
    },
    
    onChange: function (cmp, event, helper) {
        
        //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        var selectedAppValue=  cmp.get("v.selectedAppValue");
        
        //component.set("v.isLoading", true);
        if(selectedAppValue != null && selectedAppValue != ''){
            cmp.set("v.spinner", true);
            console.log('selectedAppValue:'+selectedAppValue);
            var action = cmp.get("c.getDebtorList");
            action.setParams({
                'srSelectedValue':selectedAppValue
            });
            action.setCallback(this,function(response) {
                var state = response.getState();
                //component.set("v.isLoading", false);
                if (state === "SUCCESS") {
                    var respWrap = response.getReturnValue();
                    console.log(respWrap.srWrap.collateralObjLst);
                    console.log(respWrap.srWrap.rosRelObjList);
                    console.log(respWrap.srWrap.debtObjLst);
                    
                    cmp.set("v.spinner", false);
                    cmp.set("v.srOldWrap", respWrap.srWrap);
                    console.log('srOldWrap::'+JSON.stringify(respWrap.srWrap));
                    //console.log('srOldWrap:::'+JSON.stringify(cmp.get("v.srOldWrap.collateralObjLst")));
                    console.log('rosRelObjDebtorList:::'+JSON.stringify(cmp.get("v.srOldWrap.rosRelObjDebtorList")));
                    console.log('rosRelObjCollateralList:::'+JSON.stringify(cmp.get("v.srOldWrap.rosRelObjCollateralList")));
                    console.log('rosRelObjScPList:::'+JSON.stringify(cmp.get("v.srOldWrap.rosRelObjScPList")));
                    /* if(resultData.length < component.get("v.pageSize")){
                        component.set("v.isLastPage", true);
                    } else{
                        component.set("v.isLastPage", false);
                    }
                    component.set("v.dataSize", resultData.length);
                    component.set("v.data", resultData);*/
                }else{
                    cmp.set("v.spinner", false);
                    console.log("@@@@@ Error " + response.getError()[0].message);
                    //cmp.set("v.errorMessage", response.getError()[0].message);
                    //cmp.set("v.isError", true);
                    helper.createToast(cmp, event, response.getError()[0].message);
                }
            });
            $A.enqueueAction(action);
        }
        if(selectedAppValue == ''){
            cmp.set("v.srOldWrap", '');
        }
    },
    handleButtonAct: function (component, event, helper) {
        try {
            console.log('component.get("v.selectedAppValue")'+component.get("v.selectedAppValue"));
            if(component.get("v.selectedAppValue") != null){
                component.set('v.srWrap.approvedFormOneROS.Initial_Financial_Statement__c',component.get("v.selectedAppValue") );
            }
            var allValid = true;
            let fields = component.find("approvedFormFields");
            for (var i = 0; i < fields.length; i++) {
                var inputFldName = fields[i].get("v.fieldName");
                var inputFieldCompValue = fields[i].get("v.value");
                //console.log(inputFieldCompValue);
                if (
                    !inputFieldCompValue &&
                    inputFieldCompValue != "0" &&
                    fields[i] &&
                    fields[i].get("v.required")
                ) {
                    console.log("no value", inputFldName);
                    //console.log('bad');
                    fields[i].reportValidity();
                    allValid = false;
                }
            }    
            
            if (allValid == false) {
                console.log("Error MSG");
                // cmp.set("v.errorMessage", "Please fill required field");
                // cmp.set("v.isError", true);
                helper.hideSpinner(component, event, helper);
                helper.createToast(component, event, "Please fill required field");
            }else{
                
                let newURL = new URL(window.location.href).searchParams;
                
                var srID = newURL.get("srId");
                var pageID = newURL.get("pageId");
                var buttonId = event.target.id;
                console.log(buttonId);
                var action = component.get("c.getButtonAction");
                
                var reqWrapPram = {
                    SRID: srID,
                    pageId: pageID, 
                    ButtonId: buttonId,
                    debObj :component.get("v.srWrap.approvedFormOneROS")
                };
                
                action.setParams({
                    SRID: srID,
                    pageId: pageID, 
                    ButtonId: buttonId,
                    reqWrapPram: JSON.stringify(reqWrapPram),
                    debObj :component.get("v.srWrap.approvedFormOneROS")
                });
                
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log("button succsess");
                        
                        console.log(response.getReturnValue());
                        window.open(response.getReturnValue().pageActionName, "_self");
                    } else {
                        console.log("@@@@@ Error " + response.getError()[0].message);
                        helper.createToast(component, event, response.getError()[0].message);
                    }
                });
                $A.enqueueAction(action);
            }
        } catch (err) {
            console.log(err.message);
        }
    },
    
    TypeOfSelectedAmend : function(c, e, h){
        let SelectedAmendment = c.find("SelectedAmend").get("v.value");
        let TypeOfAmendment = c.find("TypeOfAmend").get("v.value");
        console.log('TypeOfAmendment::'+TypeOfAmendment);
        console.log('SelectedAmendment::'+SelectedAmendment);
        if(TypeOfAmendment != ''){
            c.set('v.TypeOfAmendment',TypeOfAmendment);
            
        }
        if(SelectedAmendment != ''){
            c.set('v.SelectedAmendment',SelectedAmendment);
            
        }
    },
    handleRecordType  : function(cmp, event, helper){
       /*var index = event.target.dataset.amedtype;
       console.log('###############@@@@@@@@@@'+ index);
       cmp.set("v.recordtypename", 'General');
       console.log(cmp.get("v.recordtypename"))
       cmp.set("v.isSelectedIndividual", (index == 'Individual') ?  true : false);
       cmp.set("v.isSelectedCorporate", (index == 'Body Corporate') ? true : false);
       cmp.set("v.isIndividual", (index == 'Individual') ?  true : false);
       cmp.set("v.isCorporate", (index == 'Body Corporate') ?  true : false);*/
        //var entityTypeLabel = cmp.get('v.entityTypeLabel');
        var initDebatSecurity = cmp.get('c.initDebatSecurityDB');   
        var reqWrapPram  =
        {
            srId : cmp.get('v.srWrap').srObj.Id,
            recordtypename: cmp.get("v.recordtypename"),
            approvedFormId :cmp.get('v.srWrap').approvedFormOneROS.Id 
        };
        initDebatSecurity.setParams(
        {
            "reqWrapPram": JSON.stringify(reqWrapPram),
        });
       
        initDebatSecurity.setCallback(this,function(response) {
                var state = response.getState();
                console.log("callback state: " + state);
                
                if (cmp.isValid() && state === "SUCCESS"){
                    var respWrap = response.getReturnValue();
                    console.log('######### respWrap.amedWrap  ',respWrap.amedWrap);
                    cmp.set('v.amedWrap',respWrap.amedWrap);	   
                    console.log('######### amedWrap.debObj  ',cmp.get("v.amedWrap"));
                }else{
                    console.log('@@@@@ Error '+response.getError()[0].message);
                    helper.createToast(cmp,event,response.getError()[0].message);
                
                }
            }    
        );
        $A.enqueueAction(initDebatSecurity);
       console.log(cmp.get("v.recordtypename"))
     }
    
})