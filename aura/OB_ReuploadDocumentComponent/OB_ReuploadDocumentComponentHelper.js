({
  /*** Method to check is the logged in user is community user or not ***/
  GetLoggedInUser: function(component, event) {
    var action = component.get("c.CheckIfCommunityUser");
    action.setCallback(this, function(response) {
      console.log("Logged Stat===>" + response.getState());
      console.log("Logged In User===>" + response.getReturnValue());
      if (response.getState() == "SUCCESS")
        component.set("v.IsCommunityUser", response.getReturnValue());
      console.log("check==>" + component.get("v.IsCommunityUser"));
    });

    $A.enqueueAction(action);
  },
  /*** Method to fetch SR Docs related to SR ***/
  GetSRDocs: function(component, event) {
    var SRRecordID = component.get("v.SRId");
    var action = component.get("c.getSRDocs");
    console.log(SRRecordID);
    action.setParams({ SRId: SRRecordID });
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        var respvar = response.getReturnValue();
        var uSRd = [];
        var gSRd = [];
        for (var i = 0; i < respvar.length; i++) {
          if (respvar[i].HexaBPM__Letter_Template_Id__c) {
            gSRd.push(respvar[i]);
          } else {
            uSRd.push(respvar[i]);
          }
        }
        component.set("v.SRDocs", uSRd);
        component.set("v.SRGenDocs", gSRd);

        console.log(uSRd);
        console.log(gSRd);
        //var v = response.getReturnValue();
        // console.log(v[0].HexaBPM__Letter_Template_Id__c);
        //if(component.get("v.SRDocs")[0].HexaBPM__Doc_ID__c!='' && component.get("v.SRDocs")[0].HexaBPM__Doc_ID__c!=null)
        //component.set("v.SRDocName",component.get("v.SRDocs")[0].Name);
        //console.log('In SRDoc===>'+JSON.stringify(response.getReturnValue());
      }
    });
    $A.enqueueAction(action);
  },
  /*** Method to dynamically fetch the status picklist values ***/
  GetStatusPicklistValues: function(component, event) {
    var action = component.get("c.getSRDocStatus");
    var opts = [];
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        for (var i = 0; i < response.getReturnValue().length; i++) {
          opts.push({
            class: "optionClass",
            label: response.getReturnValue()[i],
            value: response.getReturnValue()[i]
          });
        }
        component.set("v.statusNameValues", opts);
        console.log("In Picklist==>" + component.get("v.statusNameValues"));
      }
    });

    $A.enqueueAction(action);
  },
  /*** Method to get the Attachment URL to display in preview section***/
  ShowDocumentPreview: function(component, event) {
    component.set("v.AttachmentURL", "");
    var SRRecordID = component.get("v.SRId");
    var DocID = component.get("v.SelectedAttachmentID");
    var action = component.get("c.getAttachmentURL");
    action.setParams({
      SRID: SRRecordID,
      AttachmentID: DocID
    });
    action.setCallback(this, function(response) {
      if (response.getState() == "SUCCESS") {
        var ResponseElements = response.getReturnValue().split("&");
        component.set(
          "v.AttachmentURL",
          ResponseElements[0] + ResponseElements[1]
        );
        component.set("v.SelectedAttachmentID", ResponseElements[1]);
        if (ResponseElements[1] != "")
          document.getElementById("showNext").disabled = true;
      }
    });
    $A.enqueueAction(action);
  },
  /*** Method to show the toggle to pop up ***/
  toggleClass: function(component, componentId, className) {
    var modal = component.find(componentId);
    $A.util.removeClass(modal, className + "hide");
    $A.util.addClass(modal, className + "open");
  },
  /*** Method to toggle inverse the pop up ***/
  toggleClassInverse: function(component, componentId, className) {
    var modal = component.find(componentId);
    $A.util.addClass(modal, className + "hide");
    $A.util.removeClass(modal, className + "open");
  },
  /*** Method to fetch the next attachment ***/
  ShowNextAttachment: function(component, event) {
    component.set("v.AttachmentURL", "");
    var SRRecordID = component.get("v.SRId");
    var CurrentAttachmentID = component.get("v.SelectedAttachmentID");
    var action = component.get("c.getNextAttachment");
    action.setParams({
      SRID: SRRecordID,
      AttachmentID: CurrentAttachmentID
    });
    action.setCallback(this, function(response) {
      if (response.getState() === "SUCCESS") {
        document.getElementById("showPrevious").disabled = false;

        var AttachmentLength = response.getReturnValue().length;
        var returnedResponse = response.getReturnValue();

        var ResponseElements = returnedResponse.split("&");
        if (
          ResponseElements[3] != "" &&
          ResponseElements[3] == "LastAttachment"
        ) {
          document.getElementById("showNext").disabled = true;
        }
        component.set("v.SRDocName", ResponseElements[2]);
        component.set("v.SelectedAttachmentID", ResponseElements[1]);
        component.set(
          "v.AttachmentURL",
          ResponseElements[0] + ResponseElements[1]
        );
      } else {
        $A.log("Errors", response.getError());
      }
    });
    $A.enqueueAction(action);
  },
  /*** Method to show the previous attachment ***/
  ShowPreviousAttachment: function(component, event) {
    component.set("v.AttachmentURL", "");
    var SRRecordID = component.get("v.SRId");
    var CurrentAttachmentID = component.get("v.SelectedAttachmentID");
    var action = component.get("c.getPreviousAttachment");
    action.setParams({
      SRID: SRRecordID,
      AttachmentID: CurrentAttachmentID
    });
    action.setCallback(this, function(response) {
      if (response.getState() === "SUCCESS") {
        document.getElementById("showPrevious").disabled = false;

        var AttachmentLength = response.getReturnValue().length;
        var returnedResponse = response.getReturnValue();
        var ResponseElements = returnedResponse.split("&");
        if (
          ResponseElements[3] != "" &&
          ResponseElements[3] == "FirstAttachment"
        ) {
          document.getElementById("showPrevious").disabled = true;
          document.getElementById("showNext").disabled = false;
        }
        component.set("v.SRDocName", ResponseElements[2]);
        component.set("v.SelectedAttachmentID", ResponseElements[1]);
        component.set(
          "v.AttachmentURL",
          ResponseElements[0] + ResponseElements[1]
        );
      } else {
        $A.log("Errors", response.getError());
      }
    });
    $A.enqueueAction(action);
  },
  /*** Method to Save the SR Docs when Save is clicked from the header section ***/
  SaveSRDocs: function(component, event) {
    var SRID = component.get("v.SRId");
    var StepID = component.get("v.StepId");
    var action = component.get("c.UpdateSRDocs");
    //var Slist = $A.util.json.encode(component.get("v.SRDocs"))
    action.setParams({
      SRDocList: JSON.stringify(component.get("v.SRDocs")),
      SRId: SRID
    });
    action.setCallback(this, function(response) {
      if (response.getState() === "SUCCESS") {
        /*  location.href = "/digitalOnboarding/s/"; */
        location.href = "/" + $A.get("$Label.c.OB_Site_Prefix") + "/s/";
      }
    });
    $A.enqueueAction(action);
  },
  /*** Method to enable disable Next & Previous buttons in the preview section ***/
  EnableDisableButtons: function(component, event) {
    var SRRecordID = component.get("v.SRId");
    var selectedItemID = event.currentTarget.dataset.attachmentid;
    var action = component.get("c.EnableDisableButtons");
    action.setParams({
      SRID: SRRecordID,
      AttachmentID: selectedItemID
    });
    action.setCallback(this, function(response) {
      var result = response.getReturnValue();
      if (result == "disablePrevious-disableNext") {
        document.getElementById("showPrevious").disabled = true;
        document.getElementById("showNext").disabled = true;
      } else if (result == "disablePrevious-enableNext") {
        document.getElementById("showPrevious").disabled = true;
        document.getElementById("showNext").disabled = false;
      } else if (result == "disableNext-enablePrevious") {
        document.getElementById("showPrevious").disabled = false;
        document.getElementById("showNext").disabled = true;
      } else if (result == "enablePrevious-enableNext") {
        document.getElementById("showPrevious").disabled = false;
        document.getElementById("showNext").disabled = false;
      }
    });
    $A.enqueueAction(action);
  },
  /*** Method to save the attachment under the selected SR Doc from the Upload pop up ***/
  saveAttachment: function(component, event) {
      try{
        console.log("saveAttachment");
    var MAX_FILE_SIZE = 750000; /* 1 000 000 * 3/4 to account for base64 */
    var fileInput = document.getElementById("file");
    var file = fileInput.files[0];
    console.log(file);
    if (file.size > this.MAX_FILE_SIZE) {
      alert(
        "File size cannot exceed " +
          this.MAX_FILE_SIZE +
          " bytes.\n" +
          "Selected file size: " +
          file.size
      );
      return;
    }
    var fr = new FileReader();
    var self = this;
    var comments = "";
    if (component.get("v.IsCommunityUser"))
      comments = component.get("v.SRDocument.HexaBPM__Customer_Comments__c");
    else comments = component.get("v.SRDocument.HexaBPM__Rejection_Reason__c");
    fr.onload = function() {
      var fileContents = fr.result;
      var base64Mark = "base64,";
      var dataStart = fileContents.indexOf(base64Mark) + base64Mark.length;
      fileContents = fileContents.substring(dataStart);
      var action = component.get("c.saveTheFile");
      action.setParams({
        SRId: component.get("v.SRId"),
        parentId: component.get("v.SRDocument.Id"),
        fileName: file.name,
        base64Data: encodeURIComponent(fileContents),
        contentType: file.type,
        CustomerComments: comments
      });
        
      action.setCallback(this, function(a) {
        if (a.getState() === "SUCCESS") {
          var v = a.getReturnValue();
          console.log(v);
          console.log("file onsucess");
          self.GetSRDocs(component, event);
          self.toggleClassInverse(component, "backdrop", "slds-backdrop--");
          self.toggleClassInverse(component, "modaldialog", "slds-fade-in-");
        } else if (a.getState() === "ERROR") {
          $A.log("Errors", a.getError());
             console.log("@@@@@ Error " + a.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + a.getError()[0].stackTrace
        );
          console.log("a==>" + a);
        }
      });
      $A.getCallback(function() {
        $A.enqueueAction(action);
      })();
    };
    fr.readAsDataURL(file);  
      }catch(error){
          console.log(error.message);
      }
    
  }
});