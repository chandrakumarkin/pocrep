({
  doInit: function(component, event, helper) {
    console.log("######## in initof reupload");
    let newURL = new URL(window.location.href).searchParams;
    component.set("v.SRId", newURL.get("srId"));
    helper.GetLoggedInUser(component, event);
    helper.GetStatusPicklistValues(component, event);
    helper.GetSRDocs(component, event);
    helper.ShowDocumentPreview(component, event);
  },
  PreviewDocument: function(component, event, helper) {
    var selectedItem = event.currentTarget.dataset.attachmentid;
    /*
            var selectedItemName = event.currentTarget.dataset.docname;
            
            component.set("v.SelectedAttachmentID",selectedItem);
            component.set("v.SRDocName",selectedItemName);
             
            helper.ShowDocumentPreview(component, event);
            helper.EnableDisableButtons(component,event);
        */
    var contentId = selectedItem;
    $A.get("e.lightning:openFiles").fire({
      recordIds: [contentId]
    });
  },
  showOppmodal: function(component, event, helper) {
    component.find("fileName").set("v.value", "");
    var selectedIndex = event.currentTarget.dataset.index;
    var attachmentObj = component.get("v.SRDocs");

    component.set("v.SRDocument", attachmentObj[selectedIndex]);
    helper.toggleClass(component, "backdrop", "slds-backdrop--");
    helper.toggleClass(component, "modaldialog", "slds-fade-in-");
  },
  hideModal: function(component, event, helper) {
    helper.toggleClassInverse(component, "backdrop", "slds-backdrop--");
    helper.toggleClassInverse(component, "modaldialog", "slds-fade-in-");
  },
  getFileName: function(component, event, helper) {
    // var fileName = document.getElementById('file').value;
    // filename = fileName.replace(/^.*\\/, "");
    // document.getElementById('FileName').innerHTML=fileName;
    component.find("fileName").set("v.value", "");
    var files = document.getElementById("file").files;
    var fileName = "";
    for (var i = 0; i < files.length; i++) fileName += files[i].name + " ";
    console.log(fileName);
    component.find("fileName").set("v.value", fileName);
  },
  Save: function(component, event, helper) {
    helper.saveAttachment(component, event);
  },
  showSpinner: function(component, event, helper) {
    component.set("v.Spinner", true);
  },
  hideSpinner: function(component, event, helper) {
    component.set("v.Spinner", false);
  },
  ShowNextAttachment: function(component, event, helper) {
    helper.ShowNextAttachment(component, event);
  },
  ShowPreviousAttachment: function(component, event, helper) {
    helper.ShowPreviousAttachment(component, event);
  },
  GoToSR: function(component, event, helper) {
    var SRRecordID = component.get("v.SRId");
    //location.href = '/' + SRRecordID;
    /* location.href = "/digitalOnboarding/s/"; */
    location.href = "/" + $A.get("$Label.c.OB_Site_Prefix") + "/s/";
  },
  SaveSRDocs: function(component, event, helper) {
    helper.SaveSRDocs(component, event);
  }
});