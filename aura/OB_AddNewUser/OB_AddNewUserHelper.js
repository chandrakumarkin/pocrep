({
  getContactAccessInfo: function(component, event, helper) {
    component.set("v.serverCallInProgress", true);
    var action = component.get("c.getConAccess");
    var requestWrap = {
      relId: component.get("v.relToUpdate")
    };
    console.log("requestWrap" + requestWrap);

    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        component.set("v.respWrap", respWrap);

        helper.setCheckboxField(component, event, helper, respWrap);
        helper.setPickList(
          component,
          event,
          helper,
          respWrap.userAccsessValue,
          respWrap.updatedRelObj.Access_Level__c
        );
        //component.set("v.isScreenLoadingCompleate", true);
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
      component.set("v.serverCallInProgress", false);
    });

    $A.enqueueAction(action);
  },

  createBlankContact: function(component, event, helper) {
    var action = component.get("c.initContact");

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        component.set("v.NewContact", respWrap.initContact);
        component.set("v.respWrap", respWrap);
        helper.setPickList(
          component,
          event,
          helper,
          respWrap.userAccsessValue,
          null
        );
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
    });
    $A.enqueueAction(action);
  },

  EditContactAccess: function(component, event, helper) {
    component.set("v.serverCallInProgress", true);

    var toDeact = component.get("v.mode") == "Deactivate" ? true : false;
    var toAct = component.get("v.mode") == "reactivate" ? true : false;

    console.log(toDeact);

    var action = component.get("c.updateRelationship");
    var requestWrap = {
      relId: component.get("v.relToUpdate"),
      AccessLevel: component.get("v.selectedAccess"),
      RolesSelected: component.get("v.rolesSelected"),
      toDeactivate: toDeact,
      toActivate: toAct
    };
    action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

    action.setCallback(this, function(response) {
      var state = response.getState();
      if (state === "SUCCESS") {
        var respWrap = response.getReturnValue();
        console.log(respWrap);
        if (!respWrap.errorMessage) {
          helper.showToast(
            component,
            event,
            helper,
            "User updated",
            "information"
          );
          helper.refreshParent(component, event, helper);
          component.destroy();
        } else {
          helper.showToast(
            component,
            event,
            helper,
            respWrap.errorMessage,
            "error"
          );
        }
      } else {
        console.log("@@@@@ Error " + response.getError()[0].message);
        console.log(
          "@@@@@ Error Location " + response.getError()[0].stackTrace
        );
      }
      component.set("v.serverCallInProgress", false);
    });
    $A.enqueueAction(action);
  },

  checkPhonePattern: function(component, event, helper, Phone) {
    //var regPhoneFormat = /^[+][(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
    var regPhoneFormat = /^\+(?:[0-9]\ {0,1}){6,14}[0-9]$/g;
    return Phone.match(regPhoneFormat) == null;
  },

  createNewContact: function(component, event, helper) {
    try {
      var phoneCheck = helper.checkPhonePattern(
        component,
        event,
        helper,
        component.get("v.NewContact").Phone
      );
      console.log(component.get("v.NewContact").Phone);

      console.log(phoneCheck);

      if (phoneCheck == false) {
        component.set("v.serverCallInProgress", true);

        var action = component.get("c.insertNewCont");

        var requestWrap = {
          contToInsert: component.get("v.NewContact"),
          AccessLevel: component.get("v.selectedAccess"),
          RolesSelected: component.get("v.rolesSelected"),
          accIdToRelate: component.get("v.accId")
        };
        action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

        action.setCallback(this, function(response) {
          var state = response.getState();
          if (state === "SUCCESS") {
            var respWrap = response.getReturnValue();
            console.log(respWrap);
            if (!respWrap.errorMessage) {
              helper.showToast(
                component,
                event,
                helper,
                "User Created",
                "information"
              );
              helper.refreshParent(component, event, helper, true);
              component.destroy();
            } else {
              if (respWrap.errorMessage == "Dup User") {
                component.set("v.currentDupUser", respWrap.duplicateContact);
                component.set("v.mode", "Conformation");
              } else if (respWrap.errorMessage == "Deativated User") {
                component.set("v.currentDupUser", respWrap.duplicateContact);
                component.set("v.relToUpdate", respWrap.relToAct);
                component.set("v.mode", "reactivate");
              } else {
                helper.showToast(
                  component,
                  event,
                  helper,
                  respWrap.errorMessage,
                  "error"
                );
              }
            }
          } else {
            console.log("@@@@@ Error " + response.getError()[0].message);
            console.log(
              "@@@@@ Error Location " + response.getError()[0].stackTrace
            );
          }
          component.set("v.serverCallInProgress", false);
        });
        $A.enqueueAction(action);
      } else {
        helper.showToast(
          component,
          event,
          helper,
          "Please enter a valid phone number in the format (i.e +XXXXXXXXXXXX)",
          "error"
        );
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  confirmDup: function(component, event, helper) {
    try {
      component.set("v.serverCallInProgress", true);

      var action = component.get("c.addDupContact");

      var requestWrap = {
        duplicateContact: component.get("v.currentDupUser"),
        AccessLevel: component.get("v.selectedAccess"),
        RolesSelected: component.get("v.rolesSelected"),
        accIdToRelate: component.get("v.accId")
      };
      action.setParams({ requestWrapParam: JSON.stringify(requestWrap) });

      action.setCallback(this, function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          if (!respWrap.errorMessage) {
            helper.showToast(
              component,
              event,
              helper,
              "User Created",
              "information"
            );
            helper.refreshParent(component, event, helper, true);
            component.destroy();
          } else {
            helper.showToast(
              component,
              event,
              helper,
              respWrap.errorMessage,
              "error"
            );
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(
            "@@@@@ Error Location " + response.getError()[0].stackTrace
          );
        }
        component.set("v.serverCallInProgress", false);
      });
      $A.enqueueAction(action);
    } catch (error) {
      console.log(error.message);
    }
  },

  showToast: function(component, event, helper, message, type) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      mode: "dismissable",
      message: message,
      type: type,
      duration: 500
    });
    toastEvent.fire();
  },

  refreshParent: function(component, event, helper, isHardRefresh) {
    try {
      var refeshEvnt = component.getEvent("refeshEvnt");
      if (isHardRefresh) {
        refeshEvnt.setParams({
          isHardRefresh: isHardRefresh
        });
      }
      refeshEvnt.fire();
    } catch (error) {
      console.log(error.message);
    }
  },

  setPickList: function(component, event, helper, options, selectedOption) {
    var optionList = [];
    console.log(selectedOption);
    for (const opt of options) {
      var optionOBj = {};
      optionOBj.text = opt;
      optionOBj.value = opt;

      if (selectedOption && selectedOption == opt) {
        optionOBj.selected = true;
      }

      optionList.push(optionOBj);
    }

    console.log(optionList);

    component.set("v.accessObj", optionList);
  },

  setCheckboxField: function(component, event, helper, relOBj) {
    try {
      debugger;
      if (relOBj.updatedRelObj.Roles) {
        var selcRolesList = relOBj.updatedRelObj.Roles.split(";");

        for (var checkBOx of component.find("checkBox")) {
          if (selcRolesList.includes(checkBOx.get("v.value"))) {
            checkBOx.set("v.checked", true);
          }

          if (selcRolesList.includes("Super User")) {
            component.set("v.showSuperUser", true);
          }
        }
        console.log("selected Roles :" + " " + relOBj.updatedRelObj.Roles);

        component.set("v.rolesSelected", relOBj.updatedRelObj.Roles);
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  formValidator: function(component, event, helper, fieldsToValidate) {
    try {
      let fields = component.find(fieldsToValidate);
      console.log(fields);

      if (fields == undefined) {
        return true;
      } else if (!Array.isArray(fields)) {
        if (fields.get("v.type") == "checkbox") {
          return fields.get("v.checked");
        } else if (fields.get("v.value") != undefined) {
          return fields.get("v.value").trim().length > 0 ? true : false;
        } else {
          return false;
        }
      } else if (Array.isArray(fields)) {
        for (const field of fields) {
          if (field.get("v.type") == "checkbox") {
            if (!field.get("v.checked")) {
              return false;
            }
          } else if (field.get("v.value") != undefined) {
            if (!field.get("v.value").trim().length > 0) {
              return false;
            }
          } else {
            return false;
          }
        }
        return true;
      }
    } catch (error) {
      console.log(error.message);
      console.log(error.stackTrace);
    }
  },

  reportValididty: function(component, event, helper, fieldsToReport) {
    try {
      let fields = component.find(fieldsToReport);

      if (!Array.isArray(fields)) {
        fields.reportValidity();
      } else {
        for (const field of fields) {
          field.reportValidity();
        }
      }
    } catch (error) {
      console.log(error.message);
    }
  }
});