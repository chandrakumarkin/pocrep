({
  doInit: function(component, event, helper) {
    var mode = component.get("v.mode");

    if (mode == "Edit") {
      helper.getContactAccessInfo(component, event, helper);
    } else if (mode == "New") {
      helper.createBlankContact(component, event, helper);
    }
  },
  serverAction: function(component, event, helper) {
    var mode = component.get("v.mode");

    if (mode == "Edit" || mode == "Deactivate" || mode == "reactivate") {
      helper.EditContactAccess(component, event, helper);
    } else if (mode == "New") {
      if (helper.formValidator(component, event, helper, "newConField")) {
        helper.createNewContact(component, event, helper);
      } else {
        helper.reportValididty(component, event, helper, "newConField");
      }
    } else if (mode == "Conformation") {
      helper.confirmDup(component, event, helper);
    }
  },
  manageDec: function(component, event, helper) {},

  closeModal: function(component, event, helper) {
    var mode = component.get("v.mode");
    if (mode == "Conformation") {
      component.set("v.mode", "New");
    } else {
      component.destroy();
    }
  },
  closeConfirmation: function(component, event, helper) {
    component.set("v.showConformation", false);
  },
  manageRoleSelection: function(component, event, helper) {
    var rolesSelectedString = component.get("v.rolesSelected");
    var checked = event.getSource().get("v.checked");
    var value = event.getSource().get("v.value");

    if (rolesSelectedString) {
      var selectedRoleList = rolesSelectedString.split(";");
      console.log(selectedRoleList);
    }

    if (selectedRoleList && selectedRoleList.length > 0) {
      if (checked == true && !selectedRoleList.includes(value)) {
        selectedRoleList.push(value);
      } else if (checked == false && selectedRoleList.includes(value)) {
        var index = selectedRoleList.indexOf(value);
        if (index !== -1) selectedRoleList.splice(index, 1);
      }

      rolesSelectedString = selectedRoleList.join(";");
    } else {
      //string is null
      if (event.getSource().get("v.checked") == true) {
        rolesSelectedString = event.getSource().get("v.value");
      }
    }

    console.log(rolesSelectedString);
    component.set("v.rolesSelected", rolesSelectedString);
  }
});