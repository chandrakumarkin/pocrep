({
    viewDetails : function(cmp, event, helper) 
    {
        let newURL = new URL(window.location.href).searchParams;
        var srId = newURL.get('srId');
        //throw event.
        var refeshEvnt = cmp.getEvent("refeshEvnt");
        var amedId = cmp.get("v.amedWrap").rosObj.Id ;
        console.log('viewDetails');
        console.log(JSON.parse(JSON.stringify(cmp.get("v.amedWrap"))));
        if(cmp.get("v.amedWrap").rosObj.Shareholder_Type__c == 'Individual') {
            cmp.set("v.isIndividual", true);
            cmp.set("v.isCorporate", false);
        }else {
            cmp.set("v.isCorporate", true);
            cmp.set("v.isIndividual", false);
        }
         cmp.set("v.isAmendment", true);
       console.log('@@@@@ isAmendment ',cmp.get("v.isAmendment"));
        console.log('@@@@@ amedId ',amedId);
        refeshEvnt.setParams({
            "amedId" : amedId,
            //"srDocs" : result,
            "isNewAmendment" : false,
            "amedWrap":null});
        refeshEvnt.fire(); 
    },
   
    remove : function(component, event, helper) 
    {
        //remove 
        var amedId = component.get("v.amedWrap").debObj.Id ;
        var srId = component.get("v.srId");
         let newURL = new URL(window.location.href).searchParams;
        var srIdParam =  (  component.get('v.srId') ?  component.get('v.srId') : newURL.get('srId'));
        var getFormAction = component.get("c.removeAmendment");
        var reqWrapPram  =
            {
                srId: srIdParam,
                amendmentID:amedId
            }
        
        getFormAction.setParams({
            "reqWrapPram": JSON.stringify(reqWrapPram)
        });
        
        console.log('@@@@@@@@@@33 reqWrapPram init '+JSON.stringify(reqWrapPram));
        getFormAction.setCallback(this, 
                                  function(response) {
                                      var state = response.getState();
                                      console.log("callback state: " + state);
                                      if (component.isValid() && state === "SUCCESS") 
                                      {
                                          var respWrap = response.getReturnValue();
                                          console.log('===respWrap========'+respWrap);
                                          if(!$A.util.isEmpty(respWrap.errorMessage)){
                                              
                                              //this.showToast("Error",respWrap.errorMessage);
                                             // cmp.set("v.errorMessage",respWrap.errorMessage);
                                              //cmp.set("v.isError",true);
                                              helper.createToast(component,event,respWrap.errorMessage);
                                          }else{
                                              //this.showToast("Success",'This is removed');
                                              //throw event.
                                              var refeshEvnt = component.getEvent("refeshEvnt");
                                              var srWrap = respWrap.srWrap;
                                              console.log('@@@@@ srWrap ',srWrap);
                                              refeshEvnt.setParams({
                                                  "srWrap" : srWrap });
                                              refeshEvnt.fire();
                                          }
                                          
                                      }else{
                                          console.log('@@@@@ Error '+response.getError()[0].message);
                                          helper.createToast(component,event,response.getError()[0].message);
                                          
                                      }
                                  }
                                 );
        $A.enqueueAction(getFormAction);
        
    },
    updateRemoveStatus : function(component, event, helper) 
    {
        //remove 
        var amedId = component.get("v.amedWrap").rosObj.Id ;
        var amedWrap = component.get("v.amedWrap") ;
        var rosRelObjScPList = component.get('v.srOldWrap.rosRelObjScPList');
         //let newURL = new URL(window.location.href).searchParams;
         //var srId = component.get("v.srId") ? component.get("v.srId") : newURL.get("srId");
        //console.log('srId::'+srId);
        console.log('amedId::'+amedId);
        console.log('amedWrap::'+JSON.stringify(amedWrap));
        console.log('rosRelObjScPList::'+component.get('v.srOldWrap.rosRelObjScPList'));
        console.log('Collateral_Value__c::'+component.get("v.amedWrap").rosObj.value_of_collateral__c);
        console.log('Description__c::'+component.get("v.amedWrap").rosObj.Description__c);
        let newURL = new URL(window.location.href).searchParams;
        var srIdParam =  (  component.get('v.srId') ?  component.get('v.srId') : newURL.get('srId'));
        console.log('srIdParam::'+srIdParam);
        var getFormAction = component.get("c.updateStatusToRemoveSecParty");
        var reqWrapPram  =
            {
                srId: srIdParam,
                amendmentID:amedId,
                amedWrap: amedWrap,
                rosRelObjScPList: rosRelObjScPList
            }
        
        getFormAction.setParams({
            "reqWrapPram": JSON.stringify(reqWrapPram)
        });
        
        console.log('@@@@@@@@@@33 reqWrapPram init '+JSON.stringify(reqWrapPram));
        getFormAction.setCallback(this, 
                                  function(response) {
                                      var state = response.getState();
                                      console.log("callback state: " + state);
                                      if (component.isValid() && state === "SUCCESS") 
                                      {
                                          var respWrap = response.getReturnValue();
                                          console.log('===respWrap========'+JSON.stringify(respWrap));
                                          if(!$A.util.isEmpty(respWrap.errorMessage)){
                                              
                                              //this.showToast("Error",respWrap.errorMessage);
                                             // cmp.set("v.errorMessage",respWrap.errorMessage);
                                              //cmp.set("v.isError",true);
                                              helper.createToast(component,event,respWrap.errorMessage);
                                          }else{
                                              //this.showToast("Success",'This is removed');
                                              //throw event.
                                              var refeshEvnt = component.getEvent("refeshEvnt");
                                              var srWrap = respWrap.srWrap;
                                              console.log('@@@@@ srWrap ',srWrap);
                                              refeshEvnt.setParams({
                                                  "srWrap" : srWrap,
                                              hideOldWrap: true});
                                              refeshEvnt.fire();
                                          }
                                          
                                      }else{
                                          console.log('@@@@@ Error '+response.getError()[0].message);
                                          helper.createToast(component,event,response.getError()[0].message);
                                          
                                      }
                                  }
                                 );
        $A.enqueueAction(getFormAction);
        
    },
    showToast : function(title,message) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": title,
            "message": message
        });
        toastEvent.fire();
    },    
    closeModel : function(cmp, event, helper) {
        //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
        console.log('--toast close--');
        cmp.set("v.errorMessage","");
        cmp.set("v.isError",false);
    },
})