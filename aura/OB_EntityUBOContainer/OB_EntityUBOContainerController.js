({
  handleFilesChange: function (cmp, event, helper) {},
  doInit: function (cmp, event, helper) {
    cmp.set("v.spinner", true);

    try {
      console.log("===pages init===");

      var getFormAction = cmp.get("c.getExistingAmendment");

      let newURL = new URL(window.location.href).searchParams;
      var pageId = newURL.get("pageId");
      var flowId = newURL.get("flowId");
      var srIdParam = cmp.get("v.srId")
        ? cmp.get("v.srId")
        : newURL.get("srId");
      cmp.set("v.srId", srIdParam);
      var reqWrapPram = {
        flowId: flowId,
        srId: srIdParam,
        pageID: pageId,
      };

      getFormAction.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram),
      });

      console.log(
        "@@@@@@@@@@33 reqWrapPram init " + JSON.stringify(reqWrapPram)
      );
      getFormAction.setCallback(this, function (response) {
        var state = response.getState();
        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          cmp.set("v.srWrap", respWrap.srWrap);
          cmp.set("v.commandButton", respWrap.ButtonSection);
          //helper.loadSRDocs(cmp, event, helper);
          console.log(
            "######### respWrap ",
            JSON.stringify(cmp.get("v.srWrap"))
          );
          cmp.set("v.spinner", false);
        } else {
          cmp.set("v.spinner", false);

          console.log("@@@@@ Error " + response.getError()[0].message);
          cmp.set("v.errorMessage", response.getError()[0].message);
          cmp.set("v.isError", true);
        }
      });
      $A.enqueueAction(getFormAction);
    } catch (err) {
      console.log("=error===" + err.message);
      cmp.set("v.spinner", false);
      elements1.classList.add("display-none");
    }
  },

  handleComponentEventClear: function (cmp, evt, helper) {
    cmp.set("v.AssignUBOButton", false);
    cmp.set("v.selectedValue", "");
    cmp.set("v.amedSelWrap", null);
    cmp.set("v.typeofRel", "");
    cmp.set("v.uboamedWrap", null);
  },

  handleComponentEvent: function (cmp, event, helper) {
    try {
      // get the selected License record from the COMPONETNT event
      var selectedLicenseGetFromEvent = event.getParam("recordByEvent");
      console.log("selected in grandparent");
      console.log(JSON.parse(JSON.stringify(selectedLicenseGetFromEvent)));

      //component.set("v.selectedRecord", selectedLicenseGetFromEvent);

      /*  var index = cmp.get("v.selectedValue");
    console.log("$$$$$$ index ", index);
    var amedSel = shareholderAmedWrapLst[index];
    console.log("$$$$$$ amedSel ", amedSel);
    var recType = amedSel.isIndividual ? "Individual" : "Body_Corporate";
    console.log("$$$$$$ recType ", recType);
    cmp.set("v.typeofRel", recType);

    cmp.set("v.selectedValue", cmp.find("select").get("v.value")); */

      //cmp.set("v.amedSelWrap", selectedLicenseGetFromEvent);
      cmp.set("v.uboamedWrap", selectedLicenseGetFromEvent);
      cmp.set("v.selectedValue", selectedLicenseGetFromEvent.displayName);
      cmp.set("v.AssignUBOButton", true);
      if (selectedLicenseGetFromEvent.isIndividual) {
        cmp.set("v.typeofRel", "Individual");
      } else {
        cmp.set("v.typeofRel", "Body_Corporate");
      }
    } catch (error) {
      console.log(error.message);
    }
  },

  handleRefreshEvnt: function (cmp, event, helper) {
    var amedId = event.getParam("amedId");
    var srWrap = event.getParam("srWrap");
    var isNewAmendment = event.getParam("isNewAmendment");
    var amedWrap = event.getParam("amedWrap");
    console.log("$$$$$$$$$$$ handleRefreshEvnt== ", JSON.stringify(srWrap));
    console.log("2222222222 handleRefreshEvnt== ", amedWrap);
    cmp.set("v.amedId", amedId);
    cmp.set("v.selectedValue", "");
    cmp.set("v.addUBO", false);
    cmp.set("v.AssignUBOButton", false);
    cmp.set("v.typeofRel", "");
    cmp.set("v.uboamedWrap", null);
    if (!$A.util.isEmpty(srWrap)) {
      console.log("====sr wrap assign===");
      cmp.set("v.srWrap", srWrap);
      cmp.set("v.selectedValue", "");
    }
    if (isNewAmendment == true) {
      console.log("====isNew===");
      cmp.set("v.amedWrap", amedWrap);
      cmp.set("v.selectedValue", "");
    }
    if (isNewAmendment == false) {
      console.log("====isNew===");
      cmp.set("v.amedWrap", amedWrap);
      cmp.set("v.selectedValue", "");
    }
  },
  onChange: function (cmp, evt, helper) {
    cmp.set("v.spinner", true);
    console.log("-convert UBO--onChange-" + cmp.find("select").get("v.value"));
    cmp.set("v.selectedValue", cmp.find("select").get("v.value"));
    cmp.set("v.AssignUBOButton", true);
    cmp.set("v.spinner", false);
    cmp.set("v.amedId", "");
    cmp.set("v.typeofRel", "");
    var shareholderAmedWrapLst = cmp.get("v.srWrap.shareholderAmedWrapLst");
    /*
      var amendmentID = cmp.get("v.selectedValue");
      var shareholderAmedWrapLst = cmp.get("v.srWrap.shareholderAmedWrapLst");
      for (var i = 0; i < shareholderAmedWrapLst.length; i++) 
      {
        if (shareholderAmedWrapLst[i].amedObj.Id == amendmentID) 
        {
          cmp.set("v.typeofRel", shareholderAmedWrapLst[i].amedObj.RecordType.DeveloperName);
          break;
        }
      }
    */

    var index = cmp.get("v.selectedValue");
    console.log("$$$$$$ index ", index);
    var amedSel = shareholderAmedWrapLst[index];
    console.log("$$$$$$ amedSel ", amedSel);
    var recType = amedSel.isIndividual ? "Individual" : "Body_Corporate";
    console.log("$$$$$$ recType ", recType);
    cmp.set("v.typeofRel", recType);
  },
  convertToUBO: function (cmp, evt, helper) {
    console.log("-convert UBO---");
    cmp.set("v.spinner", true);
    try {
      //var amendmentID = cmp.get("v.selectedValue");

      var shareholderAmedWrapLst = cmp.get("v.srWrap.shareholderAmedWrapLst");
      /*
      for (var i = 0; i < shareholderAmedWrapLst.length; i++) 
      {
        if (shareholderAmedWrapLst[i].amedObj.Id == amendmentID) {
          var SIWraps = shareholderAmedWrapLst[i];
          cmp.set("v.uboamedWrap", SIWraps);
          cmp.set("v.addUBO", true);
          break;
        }
      }
      */
      var index = cmp.get("v.selectedValue");
      //cmp.set("v.uboamedWrap", shareholderAmedWrapLst[index]);
      cmp.set("v.addUBO", true);

      console.log("======" + JSON.stringify(cmp.get("v.uboamedWrap")));

      cmp.set("v.spinner", false);
    } catch (err) {
      cmp.set("v.spinner", false);
      console.log("=error===" + err.message);
      cmp.set("v.errorMessage", err.message);
      cmp.set("v.isError", true);
    }
  },
  showToast: function (title, message) {
    var toastEvent = $A.get("e.force:showToast");
    toastEvent.setParams({
      title: title,
      message: message,
    });
    toastEvent.fire();
  },
  closeModel: function (cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },

  handleButtonAct: function (component, event, helper) {
      
    //helper.handleButtonActHelper(component, event, helper);
    var typeOfInf = component.get("v.srWrap.srObj.Type_of_UBO_Information__c");
      if ($A.util.isEmpty(typeOfInf) || $A.util.isUndefined(typeOfInf)) {
          component.set("v.errorMessage", "Please fill required field");
          component.set("v.isError", true);
          helper.handleSave(component, event);
      }
      //Sabeeha - DIFC2-17680 Changes
      else if(typeOfInf == 'System'){
          let newURL = new URL(window.location.href).searchParams;
          
          var srID = newURL.get("srId");
          console.log('hi'+srID);
          
          var amedaction = component.get("c.fetchAmendment");
          
          amedaction.setParams({
              srId: srID
          });
          
          amedaction.setCallback(this, function (response) {
              var state = response.getState();
              if (state === "SUCCESS") {
                  if(response.getReturnValue() == true){
                      component.set("v.isError", false);
                  }
                  else{
                      component.set("v.errorMessage", "Please Add Beneficial Owner");
                      component.set("v.isError", true);
                  }
                  helper.handleSave(component, event);
                  console.log('res: '+response.getReturnValue());
              } else {
                  console.log("@@@@@ Error " + response.getError()[0].message);
              }
          });
          $A.enqueueAction(amedaction);
      } //ends here     
          else{
              helper.handleSave(component, event);
          }
  },
  handleUploadFinished: function (cmp, event, helper) {
    //helper.handleUploadFinished(cmp, event, helper);
  },
});