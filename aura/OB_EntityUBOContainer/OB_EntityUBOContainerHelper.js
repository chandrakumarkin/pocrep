({
	loadSRDocs : function(component,event, helper){ 
        //var amendmentWrapper = component.get("v.amedWrap");
        //console.log('amendmentWrapper----'+amendmentWrapper);
        //var amedId = amendmentWrapper.amedObj.Id;
        //console.log('amedId-----'); 
        
        
    },
    /**
     * Display a message
     */
    displayToast : function (title, message) {
		console.log('displayToast');
        var toast = $A.get('e.force:showToast');

        // For lightning1 show the toast
        if (toast) {
            //fire the toast event in Salesforce1
            toast.setParams({
                'title': title,
                'message': message,
                mode:'sticky'
            });

            toast.fire();
        } else { // otherwise throw an alert        
            alert(title + ': ' + message);
        }
	},
    show: function (cmp, event) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-hide");
        $A.util.addClass(spinner, "slds-show");
    },
    hide:function (cmp) {
        var spinner = cmp.find("mySpinner");
        $A.util.removeClass(spinner, "slds-show");
        $A.util.addClass(spinner, "slds-hide");
    },
    handleUploadFinished: function (cmp, event,helper) 
    {
        
    },
    handleSave: function (component, event) 
    {
        var isError = component.get("v.isError");
        console.log("===isError===" + isError);
        
        if (isError != true) {
            console.log("buttttton");
            var buttonId = event.target.id;
            console.log(buttonId);
            
            
            try {
                let newURL = new URL(window.location.href).searchParams;
                
                var srID = newURL.get("srId");
                var pageID = newURL.get("pageId");
                var buttonId = event.target.id;
                
                var action = component.get("c.getButtonAction");
                
                action.setParams({
                    SRID: srID,
                    objRequest: component.get("v.srWrap.srObj"),
                    pageId: pageID,
                    ButtonId: buttonId,
                });
                
                action.setCallback(this, function (response) {
                    var state = response.getState();
                    if (state === "SUCCESS") {
                        console.log("button succsess");
                        
                        console.log(response.getReturnValue());
                        window.open(response.getReturnValue().pageActionName, "_self");
                    } else {
                        console.log("@@@@@ Error " + response.getError()[0].message);
                    }
                });
                $A.enqueueAction(action);
            } catch (err) {
                console.log(err.message);
            }
        }
    }
})