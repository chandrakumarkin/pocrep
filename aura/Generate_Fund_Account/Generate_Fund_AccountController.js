({
	createFundAccount : function(component, event, helper) {
            var toggleText = component.find("text");
            $A.util.toggleClass(toggleText, "toggle");        
        	//	alert('start');
		//	try {
           var relationshipType = component.find('relationshipType').get('v.value');	
          // alert('relationshipType'+relationshipType);     
           //var accountId = helper.getUrlVars(component, event, helper)["id"];            
           var actionNext=component.get("c.processAccountRecords");
           var recordIds = component.get("v.recordId");
        var methodName = 'createFundAccount';
        actionNext.setParams({"accountId" : recordIds,"methodName" :methodName, "params" : relationshipType});
        // actionNext.setParams({"methodName" : createFundAccount()});
         //  actionNext.setParams({"params" : relationshipType});
           // alert('record Ids '+accountId);
        actionNext.setCallback(this, function(response) {
           //  alert('Hello'+response.getReturnValue());
             var state = response.getState();            
            // alert('state' +state);
             if (component.isValid() && state === "SUCCESS") {
               //  alert(actionNext.toString().indexOf('Error'));
				if(actionNext.toString().indexOf('Error') != -1){
					alert(actionNext);
				}
				else {
                    component.set("v.message", response.getReturnValue());
                    
				//	alert("Fund account successfully generated!");
					//opener.location.href = '/' +  result;
				//	window.close();
				}			
            }

                else {
                    alert('Something went wrong while processing your request. Please try again later.');
                }
            
        });
         $A.enqueueAction(actionNext);

   },
    removeCSS: function(cmp, event) {
       // var cmpTarget = cmp.find('MainDiv');
      //  $A.util.removeClass(cmpTarget, 'slds-modal__container');
      $A.get("e.force:closeQuickAction").fire();  
     
    }
})