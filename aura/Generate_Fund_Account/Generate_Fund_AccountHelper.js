({
	 getUrlVars : function(component, event, helper) {
	
			var vars = {};
			
			var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, 
			
				function(m,key,value) {
					
					if(vars[key]){
					
						if(vars[key] instanceof Array){
							vars[key].push(value);
						}else{
							vars[key] = [vars[key], value];
						}
					}else{
						vars[key] = value;
					}
                }
			
			); 
		
			return vars; 
		}
})