({
  doInit: function (component, event, helper) {
    console.log("in here init");
    const srWrap = component.get("v.srWrap.srObj");

    if (
      srWrap.Business_Sector__c == "Investment Fund" &&
      srWrap.Entity_Type__c == "Non - financial"
    ) {
      component.set("v.hideInvestmentFund", true);
    }

    var entityfield = component.get(
      "v.amedWrap.amedObj.Is_this_Entity_registered_with_DIFC__c"
    );

    if (entityfield != undefined || entityfield != null) {
      helper.displayEntityField(component);
    }

    var typeofEnt = component.get("v.srWrap.srObj.Type_of_Entity__c");
    if (typeofEnt != undefined || typeofEnt != null) {
      helper.displayPartnerType(component);
      helper.displayContriFields(component);
    }

    let newURL = new URL(window.location.href).searchParams;
    // component.set("v.srId", newURL.get("srId"));
    var flowIdParam = component.get("v.flowId")
      ? component.get("v.flowId")
      : newURL.get("flowId");
    var pageIdParam = component.get("v.pageId")
      ? component.get("v.pageId")
      : newURL.get("pageId");
    component.set("v.pageId", pageIdParam);
    component.set("v.flowId", flowIdParam);

    //Firing the onchange event for Nationality
    var individualNationalityAction = component.get("c.selectNationality");
    $A.enqueueAction(individualNationalityAction);
    var corporateNationalityAction = component.get(
      "c.showHideCorporateRiskFields"
    );
    $A.enqueueAction(corporateNationalityAction);

    //Invoke the SRDDocs
    //helper.loadSRDocs(component, event, helper);
    var selectedSR = component.get(
      "v.amedWrap.amedObj.OB_Linked_Multistructure_Applications__c"
    );
    console.log("selectedSR===>" + selectedSR);
    if (selectedSR) {
      var srIdList = selectedSR.split(",");
      var firstSelectedSR = srIdList[0];
      component.set("v.firstSelectedSR", firstSelectedSR);
      console.log("firstSelectedSR==>" + firstSelectedSR);
    }
    component.set("v.loadSRDoc", true);

    var parentSrId = newURL.get("srId");
    var srList = component.get("v.srList");
    var plValues = [];
    var cbValues = [];

    for (var i = 0; i < srList.length; i++) {
      if (srList[i].Id != parentSrId) {
        plValues.push({
          label: srList[i].Entity_Name__c,
          value: srList[i].Id,
        });
      }

      cbValues.push({
        label: srList[i].Entity_Name__c,
        value: srList[i].Entity_Name__c,
      });
    }
    component.set("v.listOptions", plValues);
    component.set("v.comboboxOptions", cbValues);
    console.log("srList==>" + JSON.stringify(component.get("v.listOptions")));
    if (!$A.util.isEmpty(selectedSR)) {
      var selectedValues = [];
      var selectedSrList = selectedSR.split(",");
      for (var i = 0; i < selectedSrList.length; i++) {
        selectedValues.push(selectedSrList[i]);
      }
      component.set("v.defaultOptions", selectedValues);
    }
  },

  onAmedSave: function (cmp, event, helper) {
    helper.showSpinner(cmp, event, helper);
    //event.preventDefault();
    // cmp.set("v.errorMessage", "");
    //cmp.set("v.isError", false);
    var allValid = true;
    let fields = cmp.find("shareholderkey");
    for (var i = 0; i < fields.length; i++) {
      var inputFldName = fields[i].get("v.fieldName");
      var inputFieldCompValue = fields[i].get("v.value");
      //console.log(inputFieldCompValue);
      if (
        !inputFieldCompValue &&
        inputFieldCompValue != "0" &&
        fields[i] &&
        fields[i].get("v.required")
      ) {
        console.log("no value", inputFldName);
        //console.log('bad');
        fields[i].reportValidity();
        allValid = false;
      }

      if (
        inputFldName &&
        inputFldName.toLowerCase() == "amount_of_contribution__c"
      ) {
        console.log("@@@@@@@@ amount_of_contribution__c");
        if (!inputFieldCompValue || inputFieldCompValue < 0) {
          console.log("@@@@@@@@ amount_of_contribution__c less than zero.");
          helper.hideSpinner(cmp, event, helper);
          helper.showToast(
            cmp,
            event,
            helper,
            "Amount of contribution cannot be negative.",
            "error"
          );
          return;
        }
      }
    }

    if (allValid == false) {
      console.log("Error MSG");
      // cmp.set("v.errorMessage", "Please fill required field");
      // cmp.set("v.isError", true);
      helper.hideSpinner(cmp, event, helper);
      helper.createToast(cmp, event, "Please fill required field");
    } else {
      //console.log(JSON.stringify(cmp.find("fileId").get("v.files"))); fileToBeUploaded
      console.log("######### onAmedSave ");
      console.log(cmp.get("v.recordtypename"));
      console.log("isIndividual" + cmp.get("v.isIndividual"));
      console.log("isCorporate" + cmp.get("v.isCorporate"));
      var recordtyeindi = cmp.get("v.isIndividual");
      var recordtypecor = cmp.get("v.isCorporate");
      var iAgreeIndividual = cmp.get(
        "v.amedWrap.amedObj.I_Agree_Shareholder__c"
      );
      var toggleValIndi = cmp.get(
        "v.amedWrap.amedObj.Will_Individual_Sign_AoA__c"
      );
      var nationality = cmp.get("v.showIndividualFieldOnRiskScore");
      if (
        $A.util.isEmpty(iAgreeIndividual) ||
        $A.util.isUndefined(iAgreeIndividual) ||
        iAgreeIndividual == false
      ) {
        //cmp.set("v.errorMessage", "Please accept the declaration");
        //cmp.set("v.isError", true);
        helper.hideSpinner(cmp, event, helper);
        helper.createToast(cmp, event, "Please accept the declaration");
        return;
      }

      //custom fields validation
      if (helper.formValidator(cmp, event, helper, "selectOptions") == false) {
        helper.reportValididty(cmp, event, helper, "selectOptions");
        helper.hideSpinner(cmp, event, helper);
        helper.createToast(cmp, event, "Please fill all requred fields");
        return;
      }

      // var isError = cmp.get("v.isError");
      //console.log("====isError >====" + isError);

      console.log("-----no error---");

      var list = cmp.get(
        "v.amedWrap.amedObj.OB_Linked_Multistructure_Applications__c"
      );
      var entAnme = cmp.get("v.amedWrap.amedObj.Company_Name__c");
      var srList = cmp.get("v.srList");

      console.log(list);
      console.log(entAnme);
      console.log(srList);

      var getId = "";

      for (var sr of srList) {
        if (sr.Entity_Name__c == entAnme) {
          console.log(sr);

          getId = sr.Id;
          break;
        }
      }
      console.log(getId);
      if (getId) {
        if (list.includes(getId)) {
          helper.hideSpinner(cmp, event, helper);
          helper.createToast(
            cmp,
            event,
            "the selected under formation entity and the selected structure entity cannot be the same"
          );
          return;
        }
      }

      var onAmedSave = cmp.get("c.onAmedSaveDB");

      let newURL = new URL(window.location.href).searchParams;
      var srId = newURL.get("srId");

      //handle err is remaining.
      var fileError = helper.fileUploadHelper(cmp, event, helper);
      if (fileError) {
        helper.hideSpinner(cmp, event, helper);
        return;
      }
      cmp.set("v.amedWrap.amedObj.OB_MultiStructure_Parent_SR__c", srId);
      var docMasterContentDocMap = cmp.get("v.docMasterContentDocMap");
      console.log("########## docMasterContentDocMap ", docMasterContentDocMap);
      var reqWrapPram = {
        srId: srId,
        amedWrap: cmp.get("v.amedWrap"),
        recordtypename: cmp.get("v.recordtypename"),
        docMasterContentDocMap: docMasterContentDocMap,
      };
      console.log("recordtype" + cmp.get("v.recordtypename"));
      console.log(
        "amedWrap==>" + JSON.parse(JSON.stringify(cmp.get("v.amedWrap")))
      );
      console.log(JSON.parse(JSON.stringify(reqWrapPram)));
      onAmedSave.setParams({
        reqWrapPram: JSON.stringify(reqWrapPram),
      });
      console.log(
        "amedWrap2==>" + JSON.parse(JSON.stringify(cmp.get("v.amedWrap")))
      );
      onAmedSave.setCallback(this, function (response) {
        helper.hideSpinner(cmp, event, helper);
        var state = response.getState();

        console.log("callback state: " + state);

        if (cmp.isValid() && state === "SUCCESS") {
          var respWrap = response.getReturnValue();
          console.log(respWrap);
          if (!$A.util.isEmpty(respWrap.errorMessage)) {
            console.log("=====error on sve====" + respWrap.errorMessage);
            helper.hideSpinner(cmp, event, helper);
            //this.showToast("Error",respWrap.errorMessage);
            //cmp.set("v.errorMessage", respWrap.errorMessage);
            //cmp.set("v.isError", true);
            helper.createToast(cmp, event, respWrap.errorMessage);
          } else {
            if (respWrap.amedWrap.amedObj.id != "") {
              console.log(
                "######### respWrap.amedWrap.amedObj.id  ",
                respWrap.amedWrap.amedObj.id
              );
              console.log(
                "######### respWrap.amedWrap.amedObj.Is_this_member_a_designated_Member__c",
                respWrap.amedWrap.amedObj.Is_this_member_a_designated_Member__c
              );

              /*Document Upload Section*/
              console.log("amendmentId>>" + respWrap.amedWrap.amedObj.Id);
              cmp.set("v.attachmentParentId", respWrap.amedWrap.amedObj.Id);

              /*
                          //Bank_statements_for_three_months Document
                          if (cmp.get("v.showIndividualFieldOnRiskScore") == "true")
                              helper.associateFile(
                                  cmp,
                                  event,
                                  helper,
                                  cmp.get("v.attachmentParentId"),
                                  "Bank_statements_for_three_months",
                                  cmp.get("v.fileToBeUploaded"),
                                  ""
                              );
                          
                          //Tax_returns_for_two_years Document
                          if (cmp.get("v.showCorporateFieldOnRiskScore") == "true")
                              helper.associateFile(
                                  cmp,
                                  event,
                                  helper,
                                  cmp.get("v.attachmentParentId"),
                                  "Tax_returns_for_two_years",
                                  cmp.get("v.taxDocument"),
                                  ""
                              );
                          
                          //Power of Attorney Document
                          helper.associateFile(
                              cmp,
                              event,
                              helper,
                              cmp.get("v.attachmentParentId"),
                              "Power_of_attorney",
                              cmp.get("v.attorneyDocument"),
                              ""
                          );
                          //Biography of founding Members
                          helper.associateFile(
                              cmp,
                              event,
                              helper,
                              cmp.get("v.attachmentParentId"),
                              "BIOGRAPHY_OF_FOUNDING_MEMBER",
                              cmp.get("v.biographydocument"),
                              ""
                          );
                          
                          //Certificate of Incorporation Document
                          var certificateSRDocId = "";
                          var certificateSRDocDtl = cmp.get(
                              "v.certificateIncorporationDocDtl"
                          );
                          if (certificateSRDocDtl)
                              certificateSRDocId = certificateSRDocDtl.Id;
                          helper.associateFile(
                              cmp,
                              event,
                              helper,
                              cmp.get("v.attachmentParentId"),
                              "Certificate_of_Incorporation",
                              cmp.get("v.certificateIncorporationDocument"),
                              certificateSRDocId
                          );
                          
                          //Passport Document
                          helper.associateFile(
                              cmp,
                              event,
                              helper,
                              cmp.get("v.attachmentParentId"),
                              "Passport_Copy_Individual",
                              cmp.get("v.passportDocument"),
                              certificateSRDocId
                          );
                          */
              /* Document Upload Section Ends */

              cmp.set("v.amedWrap.amedObj", null);
              //cmp.set("v.authfieldShow",true);
              cmp.set("v.defaultOptions", []);
              var refeshEvnt = cmp.getEvent("refeshEvnt");
              var srWrap = respWrap.srWrap;
              console.log("@@@@@ srWrap ", srWrap);

              // resetting docMasterContentDocMap
              cmp.set("v.docMasterContentDocMap", null);
              var docMasterContentDocMap = cmp.get("v.docMasterContentDocMap");
              console.log(
                "########## reset docMasterContentDocMap ",
                JSON.stringify(docMasterContentDocMap)
              );
              refeshEvnt.setParams({
                srWrap: srWrap,
              });
              refeshEvnt.fire();
              cmp.set("v.isSelectedIndividual", true);
              cmp.set("v.isSelectedCorporate", true);
            }
          }
        } else {
          console.log("@@@@@ Error " + response.getError()[0].message);
          console.log(response.getError()[0].stackTrace);

          helper.hideSpinner(cmp, event, helper);
          //cmp.set("v.errorMessage", response.getError()[0].message);
          //cmp.set("v.isError", true);
          helper.createToast(cmp, event, response.getError()[0].message);
        }
      });
      $A.enqueueAction(onAmedSave);
    }
  },
  closeForm: function (cmp, event, helper) {
    //throw event.
    /*var refeshEvnt = cmp.getEvent("refeshEvnt");
        var amedId = cmp.get("v.amedWrap").amedObj.Id ;
        console.log('@@@@@ amedId ',amedId);
        refeshEvnt.setParams({
            "amedId" : '' });
        refeshEvnt.fire();*/

    cmp.set("v.spinner", true);
    var isNew = cmp.get("v.isNew");
    cmp.set("v.isCorporate", false);
    cmp.set("v.isIndividual", false);
    cmp.set("v.isSelectedIndividual", true);
    cmp.set("v.isSelectedCorporate", true);
    if (isNew == true) {
      console.log("===new record====");
      cmp.set("v.amedWrap.amedObj", null);
    }
    var refeshEvnt = cmp.getEvent("refeshEvnt");
    refeshEvnt.setParams({
      amedId: "",
      isNewAmendment: isNew,
      amedWrap: cmp.get("v.amedWrap.amedObj"),
    });
    cmp.set("v.spinner", false);
    refeshEvnt.fire();
  },
  closeModel: function (cmp, event, helper) {
    //$A.util.addClass( component.find( 'toastModel' ), 'slds-hide' );
    console.log("--toast close--");
    cmp.set("v.errorMessage", "");
    cmp.set("v.isError", false);
  },

  handleRecordType: function (cmp, event, helper) {
    var index = event.target.dataset.amedtype;
    console.log("###############@@@@@@@@@@" + index);
    cmp.set("v.recordtypename", index);
    console.log(cmp.get("v.recordtypename"));
    if (index == "Individual") {
      cmp.set("v.isIndividual", true);
      cmp.set("v.isCorporate", false);
    } else {
      cmp.set("v.isCorporate", true);
      cmp.set("v.isIndividual", false);
    }
  },

  showAuthFields: function (cmp, event, helper) {},
  showAuthFieldsCor: function (cmp, event, helper) {},
  selectNationality: function (cmp, event, helper) {
    var nationality = cmp.get("v.amedWrap.amedObj.Nationality_list__c");
    var resident = cmp.get(
      "v.amedWrap.amedObj.Are_you_a_resident_in_the_U_A_E__c"
    );
    if (nationality)
      helper.showFieldOnRiskScore(cmp, event, "individual", nationality);
  },
  showHideCorporateRiskFields: function (cmp, event, helper) {
    var nationality = cmp.get("v.amedWrap.amedObj.Country_of_Registration__c");
    console.log("nationality" + nationality);
    if (nationality)
      helper.showFieldOnRiskScore(cmp, event, "corporate", nationality);
  },

  residentCheck: function (cmp, event, helper) {
    //cmp.set("v.residentUAE", cmp.find("UAEresidentCheck").get("v.value"));
    //cmp.set("v.isUAEResident", cmp.get("v.amedWrap.amedObj.Are_you_a_resident_in_the_U_A_E__c"));

    var resident = cmp.get(
      "v.amedWrap.amedObj.Are_you_a_resident_in_the_U_A_E__c"
    );
    var nationality = cmp.get("v.amedWrap.amedObj.Nationality_list__c");

    if (resident == "Yes" || nationality == "United Arab Emirates") {
      cmp.set("v.showEIDNo", true);
    } else if (nationality != "United Arab Emirates" && resident == "Yes") {
      cmp.set("v.showEIDNo", true);
    } else {
      cmp.set("v.showEIDNo", false);
    }
  },

  showOtherContriType: function (cmp, event, helper) {
    var type = cmp.get("v.amedWrap.amedObj.Type_of_Contribution__c");

    if (type == "Other") {
      cmp.set("v.showOtherContri", true);
    } else {
      cmp.set("v.showOtherContri", false);
    }
  },
  showOtherContriTypeCorporate: function (cmp, event, helper) {
    var type = cmp.get("v.amedWrap.amedObj.Type_of_Contribution__c");

    if (type == "Other") {
      cmp.set("v.showOtherContriCor", true);
    } else {
      cmp.set("v.showOtherContriCor", false);
    }
  },

  showEntityfield: function (cmp, event, helper) {
    var entityfield = cmp.get(
      "v.amedWrap.amedObj.Is_the_shareholder_under_formation__c"
    );
    if (entityfield == "No") {
      cmp.set("v.amedWrap.amedObj.Company_Name__c", null);
    }

    helper.displayEntityField(cmp);
  },

  handleLookupEvent: function (component, event, helper) {
    console.log("handleLookupEvent");
    var accObj = event.getParam("sObject");
    console.log("lookup--" + JSON.stringify(accObj));
    component.set(
      "v.amedWrap.amedObj.Registration_No__c",
      accObj.Registration_License_No__c
    );
  },

  clearRegistrationNumber: function (component, event, helper) {
    console.log("clearRegistrationNumber");
    var accObj = event.getParam("instanceId");
    console.log("lookup--" + JSON.stringify(accObj));
    component.set("v.amedWrap.amedObj.Registration_No__c", "");
  },
  handleFilesChange: function (component, event, helper) {
    console.log("@@@@@@ in handleFilesChange ");
    var fileName = "No File Selected..";
    if (event.getSource().get("v.files").length > 0) {
      fileName = event.getSource().get("v.files")[0][0].name;
    }

    //console.log('@@@@@@ in handleFilesChange after fileName ',event.getSource().get("v.files")[0] );
    console.log(fileName);
    component.set("v.fileName", fileName);
  },
  handleFilesChangeAttorney: function (component, event, helper) {
    var fileNameAttorney = "No File Selected..";
    if (event.getSource().get("v.files").length > 0) {
      fileNameAttorney = event.getSource().get("v.files")[0][0].name;
    }
    component.set("v.fileNameAttorney", fileNameAttorney);
  },

  handleFilesChangeCertificationIncorporation: function (
    component,
    event,
    helper
  ) {
    var fileNameCertificationIncorporation = "No File Selected..";
    if (event.getSource().get("v.files").length > 0) {
      fileNameCertificationIncorporation = event
        .getSource()
        .get("v.files")[0][0].name;
    }
    component.set(
      "v.fileNameCertificationIncorporation",
      fileNameCertificationIncorporation
    );
  },
  handleFilesChangeBankStatement: function (component, event, helper) {
    var fileNameBankStatement = "No File Selected..";
    if (event.getSource().get("v.files").length > 0) {
      fileNameBankStatement = event.getSource().get("v.files")[0][0].name;
    }
    component.set("v.fileNameBankStatement", fileNameBankStatement);
  },
  handleFilesChangeTaxDoc: function (component, event, helper) {
    var fileNameTaxDoc = "No File Selected..";
    if (event.getSource().get("v.files").length > 0) {
      fileNameTaxDoc = event.getSource().get("v.files")[0][0].name;
    }
    component.set("v.fileNameTaxDoc", fileNameTaxDoc);
  },
  handleFilesChangeBiography: function (component, event, helper) {
    var fileNameBiography = "No File Selected..";
    if (event.getSource().get("v.files").length > 0) {
      fileNameBiography = event.getSource().get("v.files")[0][0].name;
    }
    component.set("v.fileNameBiography", fileNameBiography);
  },
  ShowCertificationMesage: function (component, event, helper) {
    var certifc = component.get(
      "v.amedWrap.amedObj.Certified_passport_copy__c"
    );
    if (certifc == "No") {
      //component.set("v.errorMessage","Please visit DIFC for citation of original passport");
      //component.set("v.isError", true);
      helper.showToast(
        component,
        event,
        helper,
        "Please visit DIFC for citation of original passport",
        "information"
      );
    } else {
      // component.set("v.errorMessage", "");
      //component.set("v.isError", false);
    }
  },
  handleOCREvent: function (cmp, event, helper) {
    console.log("@@@@@@@@@ handleOCREvent ");
    var ocrWrapper = event.getParam("ocrWrapper");
    var mapFieldApiObject = ocrWrapper.mapFieldApiObject;
    // Change the key here
    var fields = cmp.find("shareholderkey");
    var nationality;
    //parsing of code.
    for (var key in mapFieldApiObject) {
      for (var i = 0; i < fields.length; i++) {
        //console.log(fields[i].get("v.fieldName"));
        var fieldName = fields[i].get("v.fieldName");
        if (fieldName) {
          console.log(fieldName);
          if (fieldName.toLowerCase() == key.toLowerCase()) {
            fields[i].set("v.value", mapFieldApiObject[key]);
          }
        }

        // for risk score
        if (fieldName && fieldName.toLowerCase() == "nationality_list__c") {
          nationality = fields[i].get("v.value");
        }
      }
    }

    console.log("####### nationality ", nationality);
    console.log("####### nationality ");

    if (nationality)
      helper.showFieldOnRiskScore(cmp, event, "individual", nationality);
  },
  handleChange: function (cmp, event) {
    // Get the list of the "value" attribute on all the selected options
    var selectedOptionsList = event.getParam("value");
    var selectedSR = selectedOptionsList.join(",");
    cmp.set("v.selectedSR", selectedSR);
    cmp.set(
      "v.amedWrap.amedObj.OB_Linked_Multistructure_Applications__c",
      selectedSR
    );
  },
  handleComboChange: function (cmp, event) {
    // Get the list of the "value" attribute on all the selected options
    var selectedSrName = event.getParam("value");
    console.log("selectedSr" + selectedSrName);
    cmp.set("v.amedWrap.amedObj.Company_Name__c", selectedSrName);
    cmp.set("v.selectedcombobox", selectedSrName);
  },
  handleFormationChange: function (cmp, event) {
    var formation = cmp.get(
      "v.amedWrap.amedObj.Is_the_shareholder_under_formation__c"
    );
    if (formation == "Yes") {
      cmp.set(
        "v.amedWrap.amedObj.Company_Name__c",
        cmp.get("v.selectedcombobox")
      );
    } else if (formation == "No") {
      cmp.set("v.amedWrap.amedObj.Company_Name__c", "");
    }
  },
});