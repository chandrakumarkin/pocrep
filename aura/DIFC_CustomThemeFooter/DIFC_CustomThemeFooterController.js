({
    BackToTop: function (component, event, helper) {
        document.documentElement.scrollTop = 0;
    },

    goToFAQ: function (component, event, helper) {
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": '/faq',
            "isredirect": "true"
        });
        urlEvent.fire();
    },
    scriptsLoaded: function (component, event, helper) {

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');

            console.log("====ca====" + ca);

            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        var FirstName = '';
        var LastName = '';
        var Email = '';
        var MobilePhone = '';
        var Full_Name = '';

        function checkCookie() {

            var user = getCookie("usernamed");
            console.log("====user====" + user);

            if (user != "") {
                var UserData = user.split('#$#');

                if (UserData.length > 3)
                //for(var i = 0; i < UserData.length; i++)
                {
                    FirstName = UserData[0];
                    LastName = UserData[1];
                    Email = UserData[2];
                    MobilePhone = UserData[3].replace(' ', '');
                    Full_Name = UserData[4];


                }
            }

            console.log("====FirstName====" + FirstName);
            console.log("====LastName====" + LastName);
            console.log("====Email====" + Email);
            console.log("====MobilePhone====" + MobilePhone);
            console.log("====Full_Name====" + Full_Name);


        }
        checkCookie();







        var initESW = function (gslbBaseURL) {
            embedded_svc.settings.displayHelpButton = true; //Or false
            embedded_svc.settings.language = ''; //For example, enter 'en' or 'en-US'

            embedded_svc.settings.disabledMinimizedText = '<b onclick="Contactus();" class="contactus" style="cursor: pointer;">Contact Us</b>'; //(Defaults to Agent Offline)

            embedded_svc.settings.widgetFontSize = '12px';
            embedded_svc.settings.defaultMinimizedText = 'How can we help?'; //(Defaults to Chat with an Expert)
            //embedded_svc.settings.disabledMinimizedText = '...'; //(Defaults to Agent Offline)

            embedded_svc.settings.loadingText = 'Loading...'; //(Defaults to Loading)
            //embedded_svc.settings.storageDomain = 'yourdomain.com'; //(Sets the domain for your deployment so that visitors can navigate subdomains during a chat session)

            // Settings for Live Agent
            //embedded_svc.settings.directToButtonRouting = function(prechatFormData) {
            // Dynamically changes the button ID based on what the visitor enters in the pre-chat form.
            // Returns a valid button ID.
            //};
            //embedded_svc.settings.prepopulatedPrechatFields = {}; //Sets the auto-population of pre-chat form fields
            //embedded_svc.settings.fallbackRouting = []; //An array of button IDs, user IDs, or userId_buttonId
            //embedded_svc.settings.offlineSupportMinimizedText = '...'; //(Defaults to Contact Us)

            embedded_svc.settings.extraPrechatFormDetails = [{
                "label": "Email",
                "transcriptFields": ["Visitor_Email__c"]
            }]; //Copy the Email value to Visitor_Email__c
            embedded_svc.settings.extraPrechatInfo = [{
                "entityName": "Contact",
                "entityFieldMaps": [{
                        "isExactMatch": false,
                        "fieldName": "FirstName",
                        "doCreate": false,
                        "doFind": false,
                        "label": "First Name"
                    },
                    {
                        "isExactMatch": false,
                        "fieldName": "LastName",
                        "doCreate": false,
                        "doFind": false,
                        "label": "Last Name"
                    },
                    {
                        "isExactMatch": false,
                        "fieldName": "Email",
                        "doCreate": false,
                        "doFind": false,
                        "label": "Email"
                    }
                ]
            }, {
                "entityName": "Case",
                "showOnCreate": true,
                "saveToTranscript": "Case",
                "entityFieldMaps": [{
                        "isExactMatch": false,
                        "fieldName": "GN_First_Name__c",
                        "doCreate": true,
                        "doFind": false,
                        "label": "First Name"
                    }, {
                        "isExactMatch": false,
                        "fieldName": "GN_Last_Name__c",
                        "doCreate": true,
                        "doFind": false,
                        "label": "Last Name"
                    },
                    {
                        "isExactMatch": false,
                        "fieldName": "Phone_Number__c",
                        "doCreate": true,
                        "doFind": false,
                        "label": "Mobile"
                    }, {
                        "isExactMatch": false,
                        "fieldName": "Subject",
                        "doCreate": true,
                        "doFind": false,
                        "label": "Message"
                    },
                    {
                        "isExactMatch": false,
                        "fieldName": "Email__c",
                        "doCreate": true,
                        "doFind": false,
                        "label": "Email"
                    },
                    {
                        "isExactMatch": false,
                        "fieldName": "Full_Name__c",
                        "doCreate": true,
                        "doFind": false,
                        "label": "Full Name"
                    },
                    {
                        "isExactMatch": false,
                        "fieldName": "Origin",
                        "doCreate": true,
                        "doFind": false,
                        "label": "Case Origin"
                    }
                ]

            }];

            embedded_svc.settings.prepopulatedPrechatFields = {

                FirstName: FirstName,
                LastName: LastName,
                Email: Email,
                MobilePhone: MobilePhone

            };



            embedded_svc.settings.enabledFeatures = ['LiveAgent'];
            embedded_svc.settings.entryFeature = 'LiveAgent';

            embedded_svc.init('https://difc.my.salesforce.com', 'https://portal.difc.ae', gslbBaseURL, '00D20000000nkZ5', 'Contact_Center', {
                baseLiveAgentContentURL: 'https://c.la1-c1-par.salesforceliveagent.com/content',
                deploymentId: '5720J000000Goj0',
                buttonId: '5730J000000GpNG',
                baseLiveAgentURL: 'https://d.la1-c1-par.salesforceliveagent.com/chat',
                eswLiveAgentDevName: 'EmbeddedServiceLiveAgent_Parent04I0J000000TN1FUAW_1650ebb2cfc',
                isOfflineSupportEnabled: false
            });
        };
        if (!window.embedded_svc) {
            console.log('embedded_svc');
            var s = document.createElement('script');
            s.setAttribute('src', 'https://difc.my.salesforce.com/embeddedservice/5.0/esw.min.js');
            s.onload = function () {
                console.log('here');
                initESW(null);
            };
            document.body.appendChild(s);
        } else {
            console.log('service.force.com');
            initESW('https://service.force.com');
        }

    }
})