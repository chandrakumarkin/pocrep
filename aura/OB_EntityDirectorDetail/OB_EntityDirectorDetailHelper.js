//OB_EntityDirectorDetailHelper

({
	
    MAX_FILE_SIZE: 4500000, //Max file size 4.5 MB
    CHUNK_SIZE: 7500000,      //Chunk Max size 750Kb
    
    showIsthisindividualaPEP : function(cmp, event) 
    {
		var showIsthisindividualaPEP = cmp.get('v.showIsthisindividualaPEP');
        var srObj = cmp.get('v.srWrap').srObj;
        //Resolving the IsthisindividualaPEP display.
        
        var entityType = ['Financial - related'];
        var businessSector = ['Legal Services','Single Family Office','Investment Fund'];
        var typeOfEntity = [
            			    'General Partnership (GP)', 
                            'Limited Partnership (LP)',
                            'Limited Liability Partnership (LLP)',
                            'Recognized Partnership (RP)',
							'Recognized Limited Partnership (RLP)',
                            'Recognized Limited Liability Partnership (RLLP)'
                           ];
        
        if( entityType.indexOf(srObj.Entity_Type__c) == -1
           		&& businessSector.indexOf(srObj.Business_Sector__c ) == -1
           					&& typeOfEntity.indexOf(srObj.Type_of_Entity__c ) == -1 )
        {
            showIsthisindividualaPEP = true;
        }
        else
        {
            showIsthisindividualaPEP = false;
        }
        
        console.log('########## showIsthisindividualaPEP ',showIsthisindividualaPEP);
        cmp.set('v.showIsthisindividualaPEP',showIsthisindividualaPEP);
	},
    
    handleUploadFinished:function(cmp, event,helper) 
    {
    	/*
            //handleUploadFinished(cmp, event,helper);
            console.log('helper calling handleUploadFinished');
            var uploadedFile = event.getParam("files")[0];
            var documentIdParam = uploadedFile.documentId;
            
            //if(  !$A.util.isEmpty(documentIdParam) )
            //{
                console.log('helper calling handleUploadFinished documentIdParam ',documentIdParam);
                helper.callOCR(cmp, event,helper,documentIdParam); 
            //}
        	//
        */
	},
   
    
    callOCR: function (cmp, event,helper,documentIdParam) 
    {
        				/*
        				//Calling OCR comes here.....parseFile == true.... remaining
                    	console.log('==call OCR===');
                        helper.showSpinner(cmp, event, helper);
                        var OCRAction = cmp.get("c.processOCR");
						
        				var reqWrapPram  =	{
                                                fileId: documentIdParam
                                            }
        
                        OCRAction.setParams({
                                                
                            					 "reqWrapPram": JSON.stringify(reqWrapPram)
                                            });
                        
                         OCRAction.setCallback(this, function(response)
                                    {
                                      
                                          console.log('@@@@@@@@@@@@ respOCR ',JSON.stringify(response.getReturnValue()) );
                                          var state = response.getState();
                                          
                                          if (cmp.isValid() && state === "SUCCESS") 
                                          {
                                             	var ocrWrapper = response.getReturnValue();
                                                if(!$A.util.isEmpty(ocrWrapper) 
                                                   			&& $A.util.isEmpty(ocrWrapper.errorMessage) )
                                                {
													//ocrWrapper = ocrWrapper.mapFieldApiObject;
                                                    var mapFieldApiObject = ocrWrapper.mapFieldApiObject;
                                                    
                                                    let fields = cmp.find("directorkey"); 
                                                    
                                                    //parsing of code.
                                                      for(var key in mapFieldApiObject)
                                                      {
                                                          for (var i = 0; i < fields.length; i++) {
                                                              console.log(fields[i].get("v.fieldName"));
                                                              if(fields[i].get("v.fieldName")) {
                                                                  console.log(fields[i].get("v.fieldName"));
                                                                  if((fields[i].get("v.fieldName")).toLowerCase() == key.toLowerCase() ) {
                                                                      fields[i].set('v.value',mapFieldApiObject[key]); 
                                                                  }
                                                                  
                                                              }
                                                          }
                                                          
                                                        
                                                       }
                                                       helper.hideSpinner(cmp, event, helper);                                                    
                                                }
                                              	else
                                                {
                                                    console.log('######## ocrWrapper.errorMessage ',ocrWrapper.errorMessage);
                                                    helper.showToast(cmp, 
                                                                  event, 
                                                                  helper, 
                                                                  ocrWrapper.errorMessage, 
                                                  				  'error');
                                                    helper.hideSpinner(cmp, event, helper);
                                                }
                                              
                                          }
                                          else
                                          {
                                              console.log('@@@@@ Error ');
                                                // console.log('@@@@@ Error '+response.getError()[0].message);
                                                 helper.showToast(cmp, 
                                                                  event, 
                                                                  helper, 
                                                                  response.getError()[0].message, 
                                                  				  'error');
                                               helper.hideSpinner(cmp, event, helper);
                                              
                                          }
                                        
                                        
                                    }  
                            );        
                         $A.enqueueAction(OCRAction);
                         */
           
    },
    loadSRDocs : function(component,event, helper)
	{ 
        /*
        var amendmentWrapper = component.get("v.amedWrap");
        console.log('amendmentWrapper----'+amendmentWrapper);
        var amedId = amendmentWrapper.amedObj.Id;
       //var srId = component.get("v.srId");
        var srId = component.get("v.srWrap").srObj.Id;
          console.log(' loadSRDocs srId-----',srId); 
        var action = component.get("c.viewSRDocs");
        var self = this;
        action.setParams({
            srId: srId,
            amendmentId: amedId
        });
        
        // set call back 
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                console.log('srDocs>>');
                console.log(result.Passport_Copy_Individual)
                console.log(result.Passport_Copy_Individual.FileName);
                component.set("v.srDocs",result);
                if(result.Passport_Copy_Individual){
                    component.set("v.fileName",result.Passport_Copy_Individual.FileName);
                    component.set("v.srDocId",result.Passport_Copy_Individual.Id);
                }
            } else if (state === "INCOMPLETE") {
                alert("From server: " + response.getReturnValue());
            } else if (state === "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        helper.displayToast('Error', errors[0].message);
                        console.log("Error message: " + errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        // enqueue the action 
        $A.enqueueAction(action);
        */
        
    },
    fileUploadHelper: function(cmp, event, helper) 
  {
    //New method.
    console.log("########### fileHolder Start");

    // For file upload and validation.
    var fileUploadValid = true;
    var fileHolder = cmp.find("filekey");

    if (fileHolder && Array.isArray(fileHolder)) {
      for (var i = 0; i < fileHolder.length; i++) {
        if (fileHolder[i].get("v.requiredDocument") == 'true') {
          helper.fileUploadProcess(cmp, event, helper, fileHolder[i]);
          fileUploadValid =
            fileUploadValid && fileHolder[i].get("v.fileUploaded");
          //requiredText
          if (!fileUploadValid && !fileHolder[i].get("v.fileUploaded")) {
            fileHolder[i].set("v.requiredText", "Please upload document.");
          }
        }
      }
    } else if (fileHolder) {
      console.log(
        "########### fileHolder content docID ",
        fileHolder.get("v.contentDocumentId")
      );
      //here
      helper.fileUploadProcess(cmp, event, helper, fileHolder);

      if (fileHolder.get("v.requiredDocument") == 'true') {
        fileUploadValid = fileHolder.get("v.fileUploaded");
        console.log("@@@@@@@@@@@ fileUploadValid ", fileUploadValid);
        if (!fileUploadValid) {
          fileHolder.set("v.requiredText", "Please upload document.");
        }
      }
    }

    console.log(" Final file upload ", fileUploadValid);
    if (!fileUploadValid) {
      helper.showToast(
        cmp,
        event,
        helper,
        "Please upload required files",
        "error"
      );
      cmp.set("v.spinner", false);
      return "file error";
    }
    console.log("########### fileHolder end");
  },

  fileUploadProcess: function(cmp, event, helper, fileHolder) {
    //New Method
    var contentDocumentId = fileHolder.get("v.contentDocumentId");
    var documentMaster = fileHolder.get("v.documentMaster");

    //if (contentDocumentId && documentMaster) 
    if (/*contentDocumentId &&*/ documentMaster) 
    {
      var mapDocValues = {};
      var docMap = cmp.get("v.docMasterContentDocMap");
      for (var key in docMap) {
        mapDocValues[key] = docMap[key];
      }

      mapDocValues[documentMaster] = ( contentDocumentId ? contentDocumentId : 'null');
      console.log(mapDocValues);
      console.log("@@@@@@ docMasterContentDocMap ");
      cmp.set("v.docMasterContentDocMap", mapDocValues);
      console.log(
        JSON.parse(JSON.stringify(cmp.get("v.docMasterContentDocMap")))
      );
    }
  },
    showToast: function(component, event, helper, msg, type) 
    {
        console.log('######## ocrWrapper.errorMessage inside ',msg);
                                                    
        // Use \n for line breake in string
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
          mode: "dismissible",
          message: msg,
          type: type
        });
        toastEvent.fire();
 	},
        // function automatic called by aura:waiting event  
        showSpinner: function(component, event, helper) {
            // make Spinner attribute true for displaying loading spinner 
            component.set("v.spinner", true); 
        },
         
        // function automatic called by aura:doneWaiting event 
        hideSpinner : function(component,event,helper){
            // make Spinner attribute to false for hiding loading spinner    
            component.set("v.spinner", false);
        },
    
    
    
})